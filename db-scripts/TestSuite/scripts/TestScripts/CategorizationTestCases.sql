/* ***********************************************************************************************************************
*
*	MAK 03/29/2010		This script mimics the TestSuite procedures NOT including
*						Market.Listing.Vehicle__ModelConfiguration#Accuracy
*						Market.Listing.Vehicle_History__ModelConfiguration#Accuracy
*
*						which are separate.						
*
*******************************************************************************************************************************/


 
SET NOCOUNT ON
DECLARE @TestName VARCHAR(200)

		SET @TestName ='VehicleCatalog.[Categorization_Configuration__Completeness]'
        
        DECLARE @RowCount INT, @NullStringReplacement VARCHAR(50)
        
        SET @NullStringReplacement = 'Ooby Dooby, Ooby Dooby, Wah doo, wah doo, wah'
        
        SELECT  @RowCount = COUNT(*)
        FROM    (SELECT DISTINCT
                        ModelYear=V.ModelYear,
                        PassengerDoors=NULLIF(V.Doors,0),
                        Transmission=NULLIF(T.Transmission,'UNKNOWN'),
                        DriveTrain=NULLIF(NULLIF(D.DriveTypeCode,'UNK'),'N/A'),
                        FuelType=NULLIF(F.FuelType,'-'),
                        Engine=NULLIF(E.Engine,'UNKNOWN')
                FROM    VehicleCatalog.Firstlook.VehicleCatalog V
                LEFT JOIN VehicleCatalog.Firstlook.Transmission T ON T.Transmission = V.Transmission
                LEFT JOIN VehicleCatalog.Firstlook.DriveTypeCode D ON D.DriveTypeCode = V.DriveTypeCode
                LEFT JOIN VehicleCatalog.Firstlook.FuelType F ON F.FuelType = V.FuelType
                LEFT JOIN VehicleCatalog.Firstlook.Engine E ON E.Engine = V.Engine) S
        FULL OUTER JOIN
                (SELECT DISTINCT
                        ModelYear=C.ModelYear,
                        PassengerDoors=C.PassengerDoors,
                        Transmission=T.Name,
                        DriveTrain=D.Name,
                        FuelType=F.Code,
                        Engine=E.Name
                FROM    VehicleCatalog.Categorization.Configuration C
                LEFT JOIN VehicleCatalog.Categorization.Transmission T ON T.TransmissionID = C.TransmissionID
                LEFT JOIN VehicleCatalog.Categorization.DriveTrain D ON D.DriveTrainID = C.DriveTrainID
                LEFT JOIN VehicleCatalog.Categorization.FuelType F ON F.FuelTypeID = C.FuelTypeID
                LEFT JOIN VehicleCatalog.Categorization.Engine E ON E.EngineID = C.EngineID
                WHERE   IsPermutation = 0
                ) D     ON      D.ModelYear = S.ModelYear
                        AND     ISNULL(D.PassengerDoors,127) = ISNULL(S.PassengerDoors,127)
                        AND     ISNULL(D.Transmission,@NullStringReplacement) = ISNULL(S.Transmission,@NullStringReplacement)
                        AND     ISNULL(D.DriveTrain,@NullStringReplacement) = ISNULL(S.DriveTrain,@NullStringReplacement)
                        AND     ISNULL(D.FuelType,@NullStringReplacement) = ISNULL(S.FuelType,@NullStringReplacement)
                        AND     ISNULL(D.Engine,@NullStringReplacement) = ISNULL(S.Engine,@NullStringReplacement)
        WHERE   D.ModelYear IS NULL
        OR      S.ModelYear IS NULL
        
         IF (@RowCount <> 0) 
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+'.'
        ELSE
				PRINT @TestName +' succeeded.'
GO


        SET NOCOUNT ON
        
        DECLARE @RowCount INT, @NullStringReplacement VARCHAR(50)
        

   SET NOCOUNT ON
DECLARE @TestName VARCHAR(200)

		SET @TestName ='[VehicleCatalog].[Categorization_VinPattern__Completeness]'
        SET @NullStringReplacement = 'Ooby Dooby, Ooby Dooby, Wah doo, wah doo, wah'
        
        SELECT  @RowCount = COUNT(*)
        FROM    (SELECT DISTINCT
                        VinPattern
                FROM    VehicleCatalog.Firstlook.VehicleCatalog V) S
        FULL OUTER JOIN
                (SELECT DISTINCT
                        VinPattern
                FROM    VehicleCatalog.Categorization.VinPattern) D
        ON      D.VinPattern = S.VinPattern
        WHERE   D.VinPattern IS NULL
        OR      S.VinPattern IS NULL
        
         IF (@RowCount <> 0) 
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+'.'
        ELSE
				PRINT @TestName +' succeeded.'
GO

 
        SET NOCOUNT ON
DECLARE @TestName VARCHAR(200)

		SET @TestName ='[VehicleCatalog].[Categorization_ModelConfiguration_VehicleCatalog__Correctness]'   
        DECLARE @RowCount INT, @NullStringReplacement VARCHAR(50)
        
        SET @NullStringReplacement = 'No soup for you!'
        
        SELECT  @RowCount = COUNT(*)
        FROM    (SELECT DISTINCT
                        VehicleCatalogID=V.VehicleCatalogID,
                        -- model
                        Make=M.Make,
                        Line=L.Line,
                        ModelFamily=F.Model,
                        Series=V.Series,
                        Segment=S.Segment,
                        BodyType=B.BodyType,
                        -- configuration
                        ModelYear=V.ModelYear,
                        PassengerDoors=NULLIF(V.Doors,0),
                        Transmission=NULLIF(T.Transmission,'UNKNOWN'),
                        DriveTrain=NULLIF(NULLIF(D.DriveTypeCode,'UNK'),'N/A'),
                        FuelType=NULLIF(FT.FuelType,'-'),
                        Engine=NULLIF(E.Engine,'UNKNOWN')
                FROM    VehicleCatalog.Firstlook.VehicleCatalog V
                JOIN    VehicleCatalog.Firstlook.Line L ON L.LineID = V.LineID
                JOIN    VehicleCatalog.Firstlook.Make M ON L.MakeID = M.MakeID
                JOIN    VehicleCatalog.Firstlook.Model F ON F.ModelID = V.ModelID
                LEFT JOIN VehicleCatalog.Firstlook.Segment S ON S.SegmentID = NULLIF(V.SegmentID,1)
                LEFT JOIN VehicleCatalog.Firstlook.BodyType B ON B.BodyTypeID = NULLIF(V.BodyTypeID,0)
                LEFT JOIN VehicleCatalog.Firstlook.Transmission T ON T.Transmission = V.Transmission
                LEFT JOIN VehicleCatalog.Firstlook.DriveTypeCode D ON D.DriveTypeCode = V.DriveTypeCode
                LEFT JOIN VehicleCatalog.Firstlook.FuelType FT ON FT.FuelType = V.FuelType
                LEFT JOIN VehicleCatalog.Firstlook.Engine E ON E.Engine = V.Engine
                WHERE   Line <> 'Unknown'
                AND     Make <> 'Unknown') S
        FULL OUTER JOIN
                (SELECT DISTINCT
                        VehicleCatalogID=MCV.VehicleCatalogID,
                        -- model
                        Make=M.Name,
                        Line=L.Name,
                        ModelFamily=F.Name,
                        Series=S.Name,
                        Segment=G.Name,
                        BodyType=B.Name,
                        -- configuration
                        ModelYear=C.ModelYear,
                        PassengerDoors=C.PassengerDoors,
                        Transmission=T.Name,
                        DriveTrain=D.Name,
                        FuelType=FT.Code,
                        Engine=E.Name
                FROM    VehicleCatalog.Categorization.ModelConfiguration_VehicleCatalog MCV
                JOIN    VehicleCatalog.Categorization.ModelConfiguration MC ON MC.ModelConfigurationID = MCV.ModelConfigurationID
                JOIN    VehicleCatalog.Categorization.Model MO ON MO.ModelID = MC.ModelID
                JOIN    VehicleCatalog.Categorization.Make M ON M.MakeID = MO.MakeID
                JOIN    VehicleCatalog.Categorization.Line L ON L.LineID = MO.LineID
                JOIN    VehicleCatalog.Categorization.ModelFamily F ON F.ModelFamilyID = MO.ModelFamilyID
                LEFT JOIN VehicleCatalog.Categorization.Series S ON MO.SeriesID = S.SeriesID
                LEFT JOIN VehicleCatalog.Categorization.Segment G ON MO.SegmentID = G.SegmentID
                LEFT JOIN VehicleCatalog.Categorization.BodyType B ON MO.BodyTypeID = B.BodyTypeID
                JOIN    VehicleCatalog.Categorization.Configuration C ON C.ConfigurationID = MC.ConfigurationID
                LEFT JOIN VehicleCatalog.Categorization.Transmission T ON T.TransmissionID = C.TransmissionID
                LEFT JOIN VehicleCatalog.Categorization.DriveTrain D ON D.DriveTrainID = C.DriveTrainID
                LEFT JOIN VehicleCatalog.Categorization.FuelType FT ON FT.FuelTypeID = C.FuelTypeID
                LEFT JOIN VehicleCatalog.Categorization.Engine E ON E.EngineID = C.EngineID
                WHERE   MC.IsPermutation = 0
                ) D     ON      D.VehicleCatalogID = S.VehicleCatalogID
                        AND     D.Make = S.Make
                        AND     D.Line = S.Line
                        AND     D.ModelFamily = S.ModelFamily
                        AND     ISNULL(D.Series,@NullStringReplacement) = ISNULL(S.Series,@NullStringReplacement)
                        AND     ISNULL(D.BodyType,@NullStringReplacement) = ISNULL(S.BodyType,@NullStringReplacement)
                        AND     ISNULL(D.Segment,@NullStringReplacement) = ISNULL(S.Segment,@NullStringReplacement)
                        AND     D.ModelYear = S.ModelYear
                        AND     ISNULL(D.PassengerDoors,127) = ISNULL(S.PassengerDoors,127)
                        AND     ISNULL(D.Transmission,@NullStringReplacement) = ISNULL(S.Transmission,@NullStringReplacement)
                        AND     ISNULL(D.DriveTrain,@NullStringReplacement) = ISNULL(S.DriveTrain,@NullStringReplacement)
                        AND     ISNULL(D.FuelType,@NullStringReplacement) = ISNULL(S.FuelType,@NullStringReplacement)
                        AND     ISNULL(D.Engine,@NullStringReplacement) = ISNULL(S.Engine,@NullStringReplacement)
        WHERE   D.ModelYear IS NULL
        OR      S.ModelYear IS NULL
        
        IF (@RowCount <> 0) 
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+'.'
        ELSE
				PRINT @TestName +' succeeded.'
GO

        SET NOCOUNT ON
        
        DECLARE @RowCount INT, @NullStringReplacement VARCHAR(50)
        
        SET @NullStringReplacement = 'No soup for you!'
        
        SELECT  @RowCount = COUNT(*)
        FROM    (SELECT DISTINCT
                        VinPattern=V.VinPattern,
                        CountryID=V.CountryCode,
                        -- model
                        Make=M.Make,
                        Line=L.Line,
                        ModelFamily=F.Model,
                        Series=V.Series,
                        Segment=S.Segment,
                        BodyType=B.BodyType,
                        -- configuration
                        ModelYear=V.ModelYear,
                        PassengerDoors=NULLIF(V.Doors,0),
                        Transmission=NULLIF(T.Transmission,'UNKNOWN'),
                        DriveTrain=NULLIF(NULLIF(D.DriveTypeCode,'UNK'),'N/A'),
                        FuelType=NULLIF(FT.FuelType,'-'),
                        Engine=NULLIF(E.Engine,'UNKNOWN')
                FROM    VehicleCatalog.Firstlook.VehicleCatalog V
                JOIN    VehicleCatalog.Firstlook.Line L ON L.LineID = V.LineID
                JOIN    VehicleCatalog.Firstlook.Make M ON L.MakeID = M.MakeID
                JOIN    VehicleCatalog.Firstlook.Model F ON F.ModelID = V.ModelID
                LEFT JOIN VehicleCatalog.Firstlook.Segment S ON S.SegmentID = NULLIF(V.SegmentID,1)
                LEFT JOIN VehicleCatalog.Firstlook.BodyType B ON B.BodyTypeID = NULLIF(V.BodyTypeID,0)
                LEFT JOIN VehicleCatalog.Firstlook.Transmission T ON T.Transmission = V.Transmission
                LEFT JOIN VehicleCatalog.Firstlook.DriveTypeCode D ON D.DriveTypeCode = V.DriveTypeCode
                LEFT JOIN VehicleCatalog.Firstlook.FuelType FT ON FT.FuelType = V.FuelType
                LEFT JOIN VehicleCatalog.Firstlook.Engine E ON E.Engine = V.Engine
                WHERE   V.VehicleCatalogLevelID = 2
                AND     Line <> 'Unknown'
                AND     Make <> 'Unknown') S
        FULL OUTER JOIN
                (SELECT DISTINCT
                        VinPattern=VP.VinPattern,
                        CountryID=VPM.CountryID,
                        -- model
                        Make=M.Name,
                        Line=L.Name,
                        ModelFamily=F.Name,
                        Series=S.Name,
                        Segment=G.Name,
                        BodyType=B.Name,
                        -- configuration
                        ModelYear=C.ModelYear,
                        PassengerDoors=C.PassengerDoors,
                        Transmission=T.Name,
                        DriveTrain=D.Name,
                        FuelType=FT.Code,
                        Engine=E.Name
                FROM    VehicleCatalog.Categorization.VinPattern_ModelConfiguration VPM
                JOIN    VehicleCatalog.Categorization.VinPattern VP ON VP.VinPatternID = VPM.VinPatternID
                JOIN    VehicleCatalog.Categorization.ModelConfiguration MC ON MC.ModelConfigurationID = VPM.ModelConfigurationID
                JOIN    VehicleCatalog.Categorization.Model MO ON MO.ModelID = MC.ModelID
                JOIN    VehicleCatalog.Categorization.Make M ON M.MakeID = MO.MakeID
                JOIN    VehicleCatalog.Categorization.Line L ON L.LineID = MO.LineID
                JOIN    VehicleCatalog.Categorization.ModelFamily F ON F.ModelFamilyID = MO.ModelFamilyID
                LEFT JOIN VehicleCatalog.Categorization.Series S ON MO.SeriesID = S.SeriesID
                LEFT JOIN VehicleCatalog.Categorization.Segment G ON MO.SegmentID = G.SegmentID
                LEFT JOIN VehicleCatalog.Categorization.BodyType B ON MO.BodyTypeID = B.BodyTypeID
                JOIN    VehicleCatalog.Categorization.Configuration C ON C.ConfigurationID = MC.ConfigurationID
                LEFT JOIN VehicleCatalog.Categorization.Transmission T ON T.TransmissionID = C.TransmissionID
                LEFT JOIN VehicleCatalog.Categorization.DriveTrain D ON D.DriveTrainID = C.DriveTrainID
                LEFT JOIN VehicleCatalog.Categorization.FuelType FT ON FT.FuelTypeID = C.FuelTypeID
                LEFT JOIN VehicleCatalog.Categorization.Engine E ON E.EngineID = C.EngineID
                WHERE   MC.IsPermutation = 0
                ) D     ON      D.VinPattern = S.VinPattern
                        AND     D.CountryID = S.CountryID
                        AND     D.Make = S.Make
                        AND     D.Line = S.Line
                        AND     D.ModelFamily = S.ModelFamily
                        AND     ISNULL(D.Series,@NullStringReplacement) = ISNULL(S.Series,@NullStringReplacement)
                        AND     ISNULL(D.BodyType,@NullStringReplacement) = ISNULL(S.BodyType,@NullStringReplacement)
                        AND     ISNULL(D.Segment,@NullStringReplacement) = ISNULL(S.Segment,@NullStringReplacement)
                        AND     D.ModelYear = S.ModelYear
                        AND     ISNULL(D.PassengerDoors,127) = ISNULL(S.PassengerDoors,127)
                        AND     ISNULL(D.Transmission,@NullStringReplacement) = ISNULL(S.Transmission,@NullStringReplacement)
                        AND     ISNULL(D.DriveTrain,@NullStringReplacement) = ISNULL(S.DriveTrain,@NullStringReplacement)
                        AND     ISNULL(D.FuelType,@NullStringReplacement) = ISNULL(S.FuelType,@NullStringReplacement)
                        AND     ISNULL(D.Engine,@NullStringReplacement) = ISNULL(S.Engine,@NullStringReplacement)
        WHERE   D.ModelYear IS NULL
        OR      S.ModelYear IS NULL
        
		DECLARE @TestName VARCHAR(200)
		SET @TestName =  '[VehicleCatalog].[Categorization_VinPattern_ModelConfiguration__Correctness]'

         IF (@RowCount <> 0) 
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+'.'
        ELSE
				PRINT @TestName +' succeeded.'
       
 

GO

        SET NOCOUNT ON
        
        DECLARE @RowCount INT, @NullStringReplacement VARCHAR(50)
        
        SET @NullStringReplacement = 'No soup for you!'
        
        SELECT  @RowCount = COUNT(*)
        FROM    (SELECT DISTINCT
                        -- model
                        Make=M.Make,
                        Line=L.Line,
                        ModelFamily=F.Model,
                        Series=V.Series,
                        Segment=S.Segment,
                        BodyType=B.BodyType,
                        -- configuration
                        ModelYear=V.ModelYear,
                        PassengerDoors=NULLIF(V.Doors,0),
                        Transmission=NULLIF(T.Transmission,'UNKNOWN'),
                        DriveTrain=NULLIF(NULLIF(D.DriveTypeCode,'UNK'),'N/A'),
                        FuelType=NULLIF(FT.FuelType,'-'),
                        Engine=NULLIF(E.Engine,'UNKNOWN')
                FROM    VehicleCatalog.Firstlook.VehicleCatalog V
                JOIN    VehicleCatalog.Firstlook.Line L ON L.LineID = V.LineID
                JOIN    VehicleCatalog.Firstlook.Make M ON L.MakeID = M.MakeID
                JOIN    VehicleCatalog.Firstlook.Model F ON F.ModelID = V.ModelID
                LEFT JOIN VehicleCatalog.Firstlook.Segment S ON S.SegmentID = NULLIF(V.SegmentID,1)
                LEFT JOIN VehicleCatalog.Firstlook.BodyType B ON B.BodyTypeID = NULLIF(V.BodyTypeID,0)
                LEFT JOIN VehicleCatalog.Firstlook.Transmission T ON T.Transmission = V.Transmission
                LEFT JOIN VehicleCatalog.Firstlook.DriveTypeCode D ON D.DriveTypeCode = V.DriveTypeCode
                LEFT JOIN VehicleCatalog.Firstlook.FuelType FT ON FT.FuelType = V.FuelType
                LEFT JOIN VehicleCatalog.Firstlook.Engine E ON E.Engine = V.Engine) S
        FULL OUTER JOIN
                (SELECT DISTINCT
                        -- model
                        Make=M.Name,
                        Line=L.Name,
                        ModelFamily=F.Name,
                        Series=S.Name,
                        Segment=G.Name,
                        BodyType=B.Name,
                        -- configuration
                        ModelYear=C.ModelYear,
                        PassengerDoors=C.PassengerDoors,
                        Transmission=T.Name,
                        DriveTrain=D.Name,
                        FuelType=FT.Code,
                        Engine=E.Name
                FROM    VehicleCatalog.Categorization.ModelConfiguration MC
                JOIN    VehicleCatalog.Categorization.Model MO ON MO.ModelID = MC.ModelID
                JOIN    VehicleCatalog.Categorization.Make M ON M.MakeID = MO.MakeID
                JOIN    VehicleCatalog.Categorization.Line L ON L.LineID = MO.LineID
                JOIN    VehicleCatalog.Categorization.ModelFamily F ON F.ModelFamilyID = MO.ModelFamilyID
                LEFT JOIN VehicleCatalog.Categorization.Series S ON MO.SeriesID = S.SeriesID
                LEFT JOIN VehicleCatalog.Categorization.Segment G ON MO.SegmentID = G.SegmentID
                LEFT JOIN VehicleCatalog.Categorization.BodyType B ON MO.BodyTypeID = B.BodyTypeID
                JOIN    VehicleCatalog.Categorization.Configuration C ON C.ConfigurationID = MC.ConfigurationID
                LEFT JOIN VehicleCatalog.Categorization.Transmission T ON T.TransmissionID = C.TransmissionID
                LEFT JOIN VehicleCatalog.Categorization.DriveTrain D ON D.DriveTrainID = C.DriveTrainID
                LEFT JOIN VehicleCatalog.Categorization.FuelType FT ON FT.FuelTypeID = C.FuelTypeID
                LEFT JOIN VehicleCatalog.Categorization.Engine E ON E.EngineID = C.EngineID
                WHERE   MC.IsPermutation = 0
                ) D     ON      D.Make = S.Make
                        AND     D.Line = S.Line
                        AND     D.ModelFamily = S.ModelFamily
                        AND     ISNULL(D.Series,@NullStringReplacement) = ISNULL(S.Series,@NullStringReplacement)
                        AND     ISNULL(D.BodyType,@NullStringReplacement) = ISNULL(S.BodyType,@NullStringReplacement)
                        AND     ISNULL(D.Segment,@NullStringReplacement) = ISNULL(S.Segment,@NullStringReplacement)
                        AND     D.ModelYear = S.ModelYear
                        AND     ISNULL(D.PassengerDoors,127) = ISNULL(S.PassengerDoors,127)
                        AND     ISNULL(D.Transmission,@NullStringReplacement) = ISNULL(S.Transmission,@NullStringReplacement)
                        AND     ISNULL(D.DriveTrain,@NullStringReplacement) = ISNULL(S.DriveTrain,@NullStringReplacement)
                        AND     ISNULL(D.FuelType,@NullStringReplacement) = ISNULL(S.FuelType,@NullStringReplacement)
                        AND     ISNULL(D.Engine,@NullStringReplacement) = ISNULL(S.Engine,@NullStringReplacement)
        WHERE   D.ModelYear IS NULL
        OR      S.ModelYear IS NULL
        
        DECLARE @TestName VARCHAR(200)

		SET @TestName ='[VehicleCatalog].[Categorization_ModelConfiguration__Completeness]'

		IF (@RowCount <> 0)  
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+'.'
        ELSE
				PRINT @TestName +' succeeded.'

 GO
        SET NOCOUNT ON
        
        DECLARE @RowCount INT, @NullStringReplacement VARCHAR(50)

        -- Ensure thate Every VC has at least one MC which is not a permuation

		SELECT @RowCount = COUNT(*) 
		FROM	(
				SELECT	DISTINCT VC.VehicleCatalogID
				FROM	VehicleCatalog.FirstLook.VehicleCatalog VC
				LEFT JOIN	 VehicleCatalog.Categorization.ModelConfiguration_VehicleCatalog MCVC ON VC.VehicleCatalogID =MCVC.VehicleCatalogID
				WHERE	MCVC.VehicleCatalogID IS NULL AND VC.ModelID IS NOT NULL ) X

        IF (@RowCount <> 0)            RAISERROR('%d VehicleCatalogIDs with No non-permutation ModelConfiguration values.', 16, 1, @RowCount)
 
         

		 -- Ensure thate Every VC has no more than one MC which is not a permuation
		
		SELECT @RowCount = COUNT(*) 
		FROM	(
				SELECT	VC.VehicleCatalogID
				FROM	VehicleCatalog.FirstLook.VehicleCatalog VC
				JOIN	VehicleCatalog.Categorization.ModelConfiguration_VehicleCatalog MCVC ON VC.VehicleCatalogID =MCVC.VehicleCatalogID
				JOIN	VehicleCatalog.Categorization.ModelConfiguration MC ON MCVC.ModelConfigurationID =MC.ModelConfigurationID AND MC.IsPermutation =0
				GROUP BY VC.VehicleCatalogID
				HAVING COUNT(*) >1 )X
	
        
        IF (@RowCount <> 0) 
                RAISERROR('%d VehicleCatalogIDs having multiple non-permutation ModelConfiguration values.', 16, 1, @RowCount)
                
         

		 -- Ensure thate Every Non MC permutation has one or more VehicleCatalogIDs.
		
		SELECT @RowCount = COUNT(*) 
		FROM	(
				SELECT	MC.ModelConfigurationID		
				FROM	VehicleCatalog.Categorization.ModelConfiguration MC
				LEFT JOIN VehicleCatalog.Categorization.ModelConfiguration_VehicleCatalog MCVC ON MC.ModelConfigurationID =MCVC.ModelConfigurationID
				LEFT JOIN	VehicleCatalog.FirstLook.VehicleCatalog VC ON MCVC.VehicleCatalogID =VC.VehicleCatalogID
				WHERE	MC.IsPermutation =0 
					AND MCVC.ModelConfigurationID IS NULL ) X
		 

      
		DECLARE @TestName VARCHAR(200)

		SET @TestName ='[VehicleCatalog].[Categorization_ModelConfiguration_VehicleCatalog__Completeness]'

		IF (@RowCount <> 0)  
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+'.'
        ELSE
				PRINT @TestName +' succeeded.'
 
GO
        DECLARE @RowCount INT, @NullStringReplacement VARCHAR(50)
        
        SET @NullStringReplacement = 'Ooby Dooby, Ooby Dooby, Wah doo, wah doo, wah'
        
       SELECT  @RowCount = COUNT(*) 
       	FROM    (SELECT DISTINCT V.VinPattern,
			VCMK.Make as Make,
			VCL.Line as Line,
			VCM.Model as ModelFamily,
			COALESCE(V.SegmentID,0) as SegmentID,
			COALESCE(NULLIF(VBT.BodyType,'Unknown'),@NullStringReplacement) as  BodyType,
			COALESCE(V.Series,@NullStringReplacement) as Series,
			V.ModelYear,
			COALESCE(V.Doors,0) as Doors,
			COALESCE(NULLIF(V.Transmission,'Unknown'),@NullStringReplacement) AS Transmission,
			COALESCE(NULLIF(V.DriveTypeCode,'N/A'),@NullStringReplacement) AS DriveTypeCode,
			COALESCE(V.FuelTYpe,@NullStringReplacement) AS FuelType,
			COALESCE(V.Engine,@NullStringReplacement) AS Engine					
                FROM    VehicleCatalog.Firstlook.VehicleCatalog V
		JOIN	VehicleCatalog.Firstlook.Model VCM ON V.ModelID =VCM.ModelID
		JOIN	VehicleCatalog.Firstlook.Line VCL ON V.LineID =VCL.LIneID
		JOIN	VehicleCatalog.FIrstlook.Make VCMK ON VCL.MakeID =VCMK.MakeID
		LEFT JOIN VehicleCatalog.Firstlook.BodyType VBT ON V.BodyTypeID =VBT.BodyTypeID
		WHERE	V.VehicleCatalogLevelID =2 ) S

        FULL OUTER JOIN
                (SELECT  DISTINCT V.VinPattern,
				MM.Name as Make,
				LL.Name as Line,
				MF.Name as ModelFamily,
				COALESCE(M.SegmentID,0) as SegmentID,
				COALESCE(BT.Name,@NullStringReplacement) AS BodyType,
				COALESCE(SS.Name,@NullStringReplacement) as Series,
				C.ModelYear,
				COALESCE(C.PassengerDoors,0) as Doors,
				COALESCE(TT.Name,@NullStringReplacement) as Transmission,
				COALESCE(DT.Name,@NullStringReplacement) as DriveTypeCode,
				COALESCE(FT.Code,@NullStringReplacement) as FuelType,	
				COALESCE(EE.Name,@NullStringReplacement) as Engine                
		FROM    VehicleCatalog.Categorization.VinPattern_ModelConfiguration VMC
		JOIN	VehicleCatalog.Categorization.VinPattern V ON VMC.VinPatternID =V.VinPatternID
		JOIN	VehicleCatalog.Categorization.ModelConfiguration MC ON VMC.ModeLConfigurationID =MC.ModelConfigurationID
		JOIN	VehicleCatalog.Categorization.Model M   ON MC.ModelID =M.MOdelID
		JOIN	VehicleCatalog.Categorization.ModelFamily MF ON M.ModelFamilyID =MF.ModelFamilyID
		JOIN	VehicleCatalog.Categorization.Line LL ON  M.LineID =LL.LineID AND M.LineID =MF.LineID
		JOIN	VehicleCatalog.Categorization.Make MM ON M.MakeID =MM.MakeID AND LL.MakeID =MM.MakeID
		JOIN	VehicleCatalog.Categorization.Configuration C ON MC.ConfigurationID =C.ConfigurationID
		LEFT JOIN VehicleCatalog.Categorization.Series SS ON M.SeriesID =SS.SeriesID AND SS.ModelFamilyID =MF.ModelFamilyID
		LEFT JOIN VehicleCatalog.Categorization.Transmission TT ON C.TransmissionID =TT.TransmissionID
		LEFT JOIN VehicleCatalog.Categorization.DriveTrain DT ON C.DriveTrainID =DT.DriveTrainID
		LEFT JOIN VehicleCatalog.Categorization.FuelType FT ON C.FUelTypeID =FT.FuelTYpeID
		LEFT JOIN VehicleCatalog.Categorization.Engine EE ON C.EngineID =EE.EngineID
		LEFT JOIN VehicleCatalog.Categorization.BodyType BT ON M.BodyTypeID =BT.BodyTYpeID
		WHERE	MC.IsPermutation=0
)  D
        ON      D.VinPattern = S.VinPattern
		AND D.Make =S.Make AND D.Line =S.Line AND D.ModelFamily=S.ModelFamily 
				AND D.BodyType =S.BodyType AND D.Series =S.Series AND D.ModelYear =S.ModelYear
					AND D.Doors =S.Doors AND D.Transmission =S.Transmission AND D.DriveTypeCode =S.DriveTypeCode
					AND	D.FuelType =S.FuelType AND D.Engine =S.Engine
        WHERE   D.VinPattern IS NULL
        OR      S.VinPattern IS NULL
        
        DECLARE @TestName VARCHAR(200)

		SET @TestName ='[VehicleCatalog].[Categorization_VinPattern_ModelConfiguration__Completeness]'

  IF (@RowCount <> 0) 
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+'.'
        ELSE
				PRINT @TestName +' succeeded.'


        
GO


   SET NOCOUNT ON
        
        DECLARE @RowCount INT, @NullStringReplacement VARCHAR(50)
        
        SET @NullStringReplacement = 'Ooby Dooby, Ooby Dooby, Wah doo, wah doo, wah'
        
SELECT  @RowCount = COUNT(*) FROM 
        (SELECT   S.VINPattern as VehicleCatalogVinPattern,D.VINPattern as CategorizationViNPattern  
   
		FROM    (SELECT V.VinPattern,
						VCMK.Make as Make,
						VCL.Line as Line,
						VCM.Model as ModelFamily,
						COALESCE(V.SegmentID,0) as SegmentID,
						COALESCE(NULLIF(VBT.BodyType,'Unknown'),@NullStringReplacement) as  BodyType,
						COALESCE(V.Series,@NullStringReplacement) as Series,
						V.ModelYear,
						COALESCE(V.Doors,0) as Doors,
						COALESCE(NULLIF(V.Transmission,'Unknown'),@NullStringReplacement) AS Transmission,
						COALESCE(NULLIF(V.DriveTypeCode,'N/A'),@NullStringReplacement) AS DriveTypeCode,
						COALESCE(V.FuelTYpe,@NullStringReplacement) AS FuelType,
						COALESCE(V.Engine,@NullStringReplacement) AS Engine		
                FROM    VehicleCatalog.Firstlook.VehicleCatalog V
				JOIN	VehicleCatalog.Firstlook.Model VCM ON V.ModelID =VCM.ModelID
				JOIN	VehicleCatalog.Firstlook.Line VCL ON V.LineID =VCL.LIneID
				JOIN	VehicleCatalog.FIrstlook.Make VCMK ON VCL.MakeID =VCMK.MakeID
				LEFT JOIN VehicleCatalog.Firstlook.BodyType VBT ON V.BodyTypeID =VBT.BodyTypeID
				WHERE	V.VehicleCatalogLevelID =1 AND V.CountryCode =1) S

        FULL OUTER JOIN
                (SELECT V.VinPattern,
						MM.Name as Make,
						LL.Name as Line,
						MF.Name as ModelFamily,
						COALESCE(M.SegmentID,0) as SegmentID,
						COALESCE(BT.Name,@NullStringReplacement) AS BodyType,
						COALESCE(SS.Name,@NullStringReplacement) as Series,
						C.ModelYear,
						COALESCE(C.PassengerDoors,0) as Doors,
						COALESCE(TT.Name,@NullStringReplacement) as Transmission,
						COALESCE(DT.Name,@NullStringReplacement) as DriveTypeCode,
						COALESCE(FT.Code,@NullStringReplacement) as FuelType,	
						COALESCE(EE.Name,@NullStringReplacement) as Engine                
		FROM    VehicleCatalog.Categorization.VinPattern_ModelConfiguration_Lookup VMC
		JOIN	VehicleCatalog.Categorization.VinPattern V ON VMC.VinPatternID =V.VinPatternID
		JOIN	VehicleCatalog.Categorization.ModelConfiguration MC ON VMC.ModeLConfigurationID =MC.ModelConfigurationID
		JOIN	VehicleCatalog.Categorization.Model M   ON MC.ModelID =M.MOdelID
		JOIN	VehicleCatalog.Categorization.ModelFamily MF ON M.ModelFamilyID =MF.ModelFamilyID
		JOIN	VehicleCatalog.Categorization.Line LL ON  M.LineID =LL.LineID AND M.LineID =MF.LineID
		JOIN	VehicleCatalog.Categorization.Make MM ON M.MakeID =MM.MakeID AND LL.MakeID =MM.MakeID
		JOIN	VehicleCatalog.Categorization.Configuration C ON MC.ConfigurationID =C.ConfigurationID
		LEFT JOIN VehicleCatalog.Categorization.Series SS ON M.SeriesID =SS.SeriesID AND SS.ModelFamilyID =MF.ModelFamilyID
		LEFT JOIN VehicleCatalog.Categorization.Transmission TT ON C.TransmissionID =TT.TransmissionID
		LEFT JOIN VehicleCatalog.Categorization.DriveTrain DT ON C.DriveTrainID =DT.DriveTrainID
		LEFT JOIN VehicleCatalog.Categorization.FuelType FT ON C.FUelTypeID =FT.FuelTYpeID
		LEFT JOIN VehicleCatalog.Categorization.Engine EE ON C.EngineID =EE.EngineID
		LEFT JOIN VehicleCatalog.Categorization.BodyType BT ON M.BodyTypeID =BT.BodyTYpeID
		WHERE		VMC.CountryID =1
)  D
     ON      D.VinPattern = S.VinPattern
					AND D.Make =S.Make AND D.Line =S.Line AND D.ModelFamily=S.ModelFamily 
					AND D.BodyType =S.BodyType AND D.Series =S.Series AND D.ModelYear =S.ModelYear
					AND D.Doors =S.Doors AND D.Transmission =S.Transmission AND D.DriveTypeCode =S.DriveTypeCode
					AND	D.FuelType =S.FuelType AND D.Engine =S.Engine
        WHERE   D.VinPattern IS NULL
        OR      S.VinPattern IS NULL
		GROUP BY D.VINPattern,S.VINPattern
 )XX 
        

DECLARE @TestName VARCHAR(200)

		SET @TestName ='[VehicleCatalog].[Categorization_VinPattern_ModelConfiguration_Lookup__Completeness] '

  IF (@RowCount <> 0) 
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+'.'
        ELSE
				PRINT @TestName +' succeeded.'


GO


        SET NOCOUNT ON
        
        DECLARE @RowCount INT
		DECLARE @TotalCount INT
	 
		SELECT @TotalCount = COUNT(*)
		FROM    [Market].Listing.Vehicle V	

        SELECT  @RowCount = COUNT(*)
        FROM    [Market].Listing.Vehicle V
        LEFT
        JOIN    [VehicleCatalog].Categorization.ModelConfiguration M ON M.ModelConfigurationID = V.ModelConfigurationID
        WHERE   V.ModelConfigurationID IS NOT NULL
			AND     M.ModelConfigurationID IS NULL


        DECLARE @TestName VARCHAR(200)

		SET @TestName ='[Market].[Listing_Vehicle__ModelConfiguration#Integrity]'

		IF (@RowCount <> 0)  
			BEGIN
		
				DECLARE @PercentFail VARCHAR(20)
				SET @PercentFail =CAST((CAST(@RowCount AS FLOAT) *100)/CAST(@TotalCount AS FLOAT)AS VARCHAR(20))
				
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+ ' out of '+ CAST(@TotalCount as VARCHAR(20)) +'.  Percent Fail: '+ @PercentFail +'%' 

			END

        ELSE
				PRINT @TestName +' succeeded.'


     
GO

        SET NOCOUNT ON
        
        DECLARE @RowCount INT
		DECLARE @TotalCount INT
 

		SELECT @TotalCount = COUNT(*)
		FROM    [Market].Listing.Vehicle_History V	

        SELECT  @RowCount = COUNT(*)
        FROM    [Market].Listing.Vehicle_History V
        LEFT
        JOIN    [VehicleCatalog].Categorization.ModelConfiguration M ON M.ModelConfigurationID = V.ModelConfigurationID
        WHERE   V.ModelConfigurationID IS NOT NULL
			AND     M.ModelConfigurationID IS NULL

		DECLARE @TestName VARCHAR(200)

		SET @TestName =' [Market].[Listing_Vehicle_History__ModelConfiguration#Integrity]'
        
   

				IF (@RowCount <> 0)  
			BEGIN
		
				DECLARE @PercentFail VARCHAR(20)
				SET @PercentFail =CAST((CAST(@RowCount AS FLOAT) *100)/CAST(@TotalCount AS FLOAT)AS VARCHAR(20))
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+ ' out of ' + CAST(@TotalCount as VARCHAR(20)) +'.  Percent Fail: '+ @PercentFail +'%' 

			END
			ELSE PRINT @TestName +' succeeded.'
GO
 
        SET NOCOUNT ON
        
        DECLARE @RowCount INT
		DECLARE @TotalCount INT

        SELECT  @RowCount = COUNT(*)
        FROM    [Market].Listing.Vehicle_Sales V
        LEFT
        JOIN    [VehicleCatalog].Categorization.ModelConfiguration M ON M.ModelConfigurationID = V.ModelConfigurationID
        WHERE   V.ModelConfigurationID IS NOT NULL
        AND     M.ModelConfigurationID IS NULL

		SELECT 	@TotalCount =COUNT(*)
		FROM	Market.Listing.Vehicle_Sales
        DECLARE @TestName VARCHAR(200)

		SET @TestName ='[Market].[Listing_Vehicle_Sales__ModelConfiguration#Integrity]'

  IF (@RowCount <> 0) 
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+' out of '+ CAST(@TotalCount AS VARCHAR(20)) +'.'
        ELSE
				PRINT @TestName +' succeeded.'
               
                
       
GO


        SET NOCOUNT ON
        
        DECLARE @RowCount INT
        DECLARE @TotalCount INT
        
		SELECT  @TotalCount =COUNT(*)
		FROM	Market.Listing.VehicleDecoded_F

        SELECT  @RowCount = COUNT(*)
        FROM    [Market].Listing.VehicleDecoded_F V
        LEFT
        JOIN    [VehicleCatalog].Categorization.ModelConfiguration M ON M.ModelConfigurationID = V.ModelConfigurationID
        WHERE   V.ModelConfigurationID IS NOT NULL
        AND     M.ModelConfigurationID IS NULL
        

DECLARE @TestName VARCHAR(200)

		SET @TestName ='[Market].[Listing_VehicleDecoded_F__ModelConfiguration#Integrity]'
       IF (@RowCount <> 0) 
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+' out of '+ CAST(@TotalCount AS VARCHAR(20)) +'.'
        ELSE
				PRINT @TestName +' succeeded.'

GO
 


        SET NOCOUNT ON
        
        DECLARE @RowCount INT
        DECLARE @TotalCount INT
        
        SELECT  @RowCount = COUNT(*)
        FROM    Market.Listing.VehicleDecoded_F F
        JOIN    Market.Listing.Vehicle V ON V.VehicleID = F.VehicleID
        WHERE   F.ModelConfigurationID <> V.ModelConfigurationID

		SELECT @TotalCount =COUNT(*)
		FROM	Market.Listing.VehicleDecoded_F
        

		DECLARE @TestName VARCHAR(200)

		SET @TestName ='[Market].[Listing_VehicleDecoded_F__ModelConfiguration#Accuracy]'

       	IF (@RowCount <> 0) 
		BEGIN
				DECLARE @PercentFail VARCHAR(20)
				SET @PercentFail =CAST((CAST(@RowCount AS FLOAT) *100)/CAST(@TotalCount AS FLOAT)AS VARCHAR(20))
			
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+ ' out of ' + CAST(@TotalCount as VARCHAR(20)) +'.  Percent Fail: '+ @PercentFail + '%. '
        END
		ELSE
				PRINT @TestName +' succeeded.'

    
GO

        SET NOCOUNT ON
        
        DECLARE @RowCount INT
        
        SELECT  @RowCount = COUNT(*)
        FROM    Market.Listing.Vehicle_Sales S
        JOIN    Market.Listing.Vehicle_History H ON H.VehicleID = S.VehicleID
        WHERE   S.ModelConfigurationID <> H.ModelConfigurationID
        
        DECLARE @TotalCount INT
        
        SELECT 	@TotalCount =COUNT(*)
        FROM	Market.Listing.Vehicle_Sales	
        	
        
 
DECLARE @TestName VARCHAR(200)

		SET @TestName ='[Market].[Listing_Vehicle_Sales__ModelConfiguration#Accuracy]'
       	IF (@RowCount <> 0)  
			BEGIN
				DECLARE @PercentFail VARCHAR(20)
				SET @PercentFail =CAST((CAST(@RowCount AS FLOAT) *100)/CAST(@TotalCount AS FLOAT)AS VARCHAR(20))
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+ ' out of ' + CAST(@TotalCount as VARCHAR(20)) +'.  Percent Fail: '+ @PercentFail + '%. '

			END
        ELSE
				PRINT @TestName +' succeeded.'

 
GO


        SET NOCOUNT ON
        
        DECLARE @RowCount INT
       
		SELECT  @RowCount = COUNT(*)
        FROM    [Market].Listing.VehicleDecoded_F V
        WHERE	Latitude =0 OR Longitude =0
        
      
 
DECLARE @TestName VARCHAR(200)

		SET @TestName ='[Market].[Listing_VehicleDecoded_F_#BadLatitudeLongitude]'
  IF (@RowCount <> 0) 
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+'.'
        ELSE
				PRINT @TestName +' succeeded.'

       
GO

        SET NOCOUNT ON
        
        DECLARE @RowCount INT

        SELECT  @RowCount = COUNT(*)
        FROM    (SELECT DISTINCT Make FROM VehicleCatalog.Firstlook.Make WHERE Make <> 'Unknown') S FULL OUTER JOIN
                (SELECT DISTINCT Make=Name FROM VehicleCatalog.Categorization.Make) D ON D.Make = S.Make
        WHERE   S.Make IS NULL
        OR      D.Make IS NULL
        
     
 
DECLARE @TestName VARCHAR(200)

		SET @TestName =' [VehicleCatalog].[Categorization_Make__Completeness] '
  IF (@RowCount <> 0) 
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+'.'
        ELSE
				PRINT @TestName +' succeeded.'

       
GO
 
        
        DECLARE @RowCount INT

        SELECT  @RowCount = COUNT(*)
        FROM    (SELECT DISTINCT
                        Make,
                        Line
                FROM    VehicleCatalog.Firstlook.Make M
                JOIN    VehicleCatalog.Firstlook.Line L ON L.MakeID = M.MakeID
                WHERE   Line <> 'Unknown'
                AND     Make <> 'Unknown') S
        FULL OUTER JOIN
                (SELECT DISTINCT
                        Make=M.Name,
                        Line=L.Name
                FROM    VehicleCatalog.Categorization.Line L
                JOIN    VehicleCatalog.Categorization.Make M ON M.MakeID = L.MakeID
                ) D ON D.Make = S.Make AND D.Line = S.Line
        WHERE   S.Line IS NULL
        OR      D.Line IS NULL
        
     
DECLARE @TestName VARCHAR(200)

		SET @TestName ='[VehicleCatalog].[Categorization_Line__Completeness]'
  IF (@RowCount <> 0) 
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+'.'
        ELSE
				PRINT @TestName +' succeeded.'

GO
 

        SET NOCOUNT ON
        
        DECLARE @RowCount INT

        SELECT  @RowCount = COUNT(*)
        FROM    (SELECT DISTINCT
                        Make=M.Make,
                        Line=L.Line,
                        ModelFamily=F.Model
                FROM    VehicleCatalog.Firstlook.Make M
                JOIN    VehicleCatalog.Firstlook.Line L ON L.MakeID = M.MakeID
                JOIN    VehicleCatalog.Firstlook.Model F ON F.LineID = L.LineID
                WHERE   Line <> 'Unknown'
                AND     Make <> 'Unknown') S
        FULL OUTER JOIN
                (SELECT DISTINCT
                        Make=M.Name,
                        Line=L.Name,
                        ModelFamily=F.Name
                FROM    VehicleCatalog.Categorization.ModelFamily F
                JOIN    VehicleCatalog.Categorization.Line L ON F.LineID = L.LineID
                JOIN    VehicleCatalog.Categorization.Make M ON M.MakeID = L.MakeID
                ) D ON D.Make = S.Make AND D.Line = S.Line AND D.ModelFamily = S.ModelFamily
        WHERE   S.Line IS NULL
        OR      D.Line IS NULL
        
 
      DECLARE @TestName VARCHAR(200)

		SET @TestName ='[VehicleCatalog].[Categorization_ModelFamily__Completeness] '
  IF (@RowCount <> 0) 
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+'.'
        ELSE
				PRINT @TestName +' succeeded.'

 GO      

        SET NOCOUNT ON
        
        DECLARE @RowCount INT

        SELECT  @RowCount = COUNT(*)
        FROM    (SELECT DISTINCT
                        Make=M.Make,
                        Line=L.Line,
                        ModelFamily=F.Model,
                        Series=V.Series
                FROM    VehicleCatalog.Firstlook.Make M
                JOIN    VehicleCatalog.Firstlook.Line L ON L.MakeID = M.MakeID
                JOIN    VehicleCatalog.Firstlook.Model F ON F.LineID = L.LineID
                JOIN    VehicleCatalog.Firstlook.VehicleCatalog V ON V.ModelID = F.ModelID
                WHERE   Line <> 'Unknown'
                AND     Make <> 'Unknown'
                AND     Series IS NOT NULL) S
        FULL OUTER JOIN
                (SELECT DISTINCT
                        Make=M.Name,
                        Line=L.Name,
                        ModelFamily=F.Name,
                        Series=S.Name
                FROM    VehicleCatalog.Categorization.Series S
                JOIN    VehicleCatalog.Categorization.ModelFamily F ON F.ModelFamilyID = S.ModelFamilyID
                JOIN    VehicleCatalog.Categorization.Line L ON F.LineID = L.LineID
                JOIN    VehicleCatalog.Categorization.Make M ON M.MakeID = L.MakeID
                ) D ON D.Make = S.Make AND D.Line = S.Line AND D.ModelFamily = S.ModelFamily AND D.Series = S.Series
        WHERE   S.Series IS NULL
        OR      D.Series IS NULL
        
   DECLARE @TestName VARCHAR(200)

		SET @TestName ='[VehicleCatalog].[Categorization_Series__Completeness]'
  IF (@RowCount <> 0) 
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+'.'
        ELSE
				PRINT @TestName +' succeeded.'

       
GO

        SET NOCOUNT ON
        
        DECLARE @RowCount INT

        SELECT  @RowCount = COUNT(*)
        FROM    (SELECT DISTINCT
                        BodyType=B.BodyType
                FROM    VehicleCatalog.Firstlook.BodyType B
                WHERE   B.BodyType <> 'Unknown') S
        FULL OUTER JOIN
                (SELECT DISTINCT
                        BodyType=B.Name
                FROM    VehicleCatalog.Categorization.BodyType B
                ) D ON D.BodyType = S.BodyType
        WHERE   S.BodyType IS NULL
        OR      D.BodyType IS NULL
        
        DECLARE @TestName VARCHAR(200)

		SET @TestName ='[VehicleCatalog].[Categorization_BodyType__Completeness] '
  IF (@RowCount <> 0) 
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+'.'
        ELSE
				PRINT @TestName +' succeeded.'
 
GO


        SET NOCOUNT ON
        
        DECLARE @RowCount INT, @NullStringReplacement VARCHAR(50)
        
        SET @NullStringReplacement = 'Leopard Skin Pill Box Hat'

        SELECT  @RowCount = COUNT(*)
        FROM    (SELECT DISTINCT
                        Make=M.Make,
                        Line=L.Line,
                        ModelFamily=F.Model,
                        Series=V.Series,
                        Segment=S.Segment,
                        BodyType=B.BodyType
                FROM    VehicleCatalog.Firstlook.Make M
                JOIN    VehicleCatalog.Firstlook.Line L ON L.MakeID = M.MakeID
                JOIN    VehicleCatalog.Firstlook.Model F ON F.LineID = L.LineID
                JOIN    VehicleCatalog.Firstlook.VehicleCatalog V ON V.ModelID = F.ModelID
                LEFT
                JOIN    VehicleCatalog.Firstlook.Segment S ON S.SegmentID = NULLIF(V.SegmentID,1)
                LEFT
                JOIN    VehicleCatalog.Firstlook.BodyType B ON B.BodyTypeID = NULLIF(V.BodyTypeID,0)
                WHERE   Line <> 'Unknown'
                AND     Make <> 'Unknown') S
        FULL OUTER JOIN
                (SELECT DISTINCT
                        Make=M.Name,
                        Line=L.Name,
                        ModelFamily=F.Name,
                        Series=S.Name,
                        Segment=G.Name,
                        BodyType=B.Name
                FROM    VehicleCatalog.Categorization.Model MO
                LEFT
                JOIN    VehicleCatalog.Categorization.Series S ON MO.SeriesID = S.SeriesID
                LEFT
                JOIN    VehicleCatalog.Categorization.Segment G ON MO.SegmentID = G.SegmentID
                LEFT
                JOIN    VehicleCatalog.Categorization.BodyType B ON MO.BodyTypeID = B.BodyTypeID
                JOIN    VehicleCatalog.Categorization.ModelFamily F ON F.ModelFamilyID = MO.ModelFamilyID
                JOIN    VehicleCatalog.Categorization.Line L ON L.LineID = MO.LineID
                JOIN    VehicleCatalog.Categorization.Make M ON M.MakeID = MO.MakeID
                WHERE   MO.IsPermutation = 0
                ) D     ON      D.Make = S.Make
                        AND     D.Line = S.Line
                        AND     D.ModelFamily = S.ModelFamily
                        AND     ISNULL(D.Series,@NullStringReplacement) = ISNULL(S.Series,@NullStringReplacement)
                        AND     ISNULL(D.BodyType,@NullStringReplacement) = ISNULL(S.BodyType,@NullStringReplacement)
                        AND     ISNULL(D.Segment,@NullStringReplacement) = ISNULL(S.Segment,@NullStringReplacement)
        WHERE   S.Make IS NULL
        OR      D.Make IS NULL
        
        DECLARE @TestName VARCHAR(200)

		SET @TestName ='[VehicleCatalog].[Categorization_Model__Completeness]'
  IF (@RowCount <> 0) 
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+'.'
        ELSE
				PRINT @TestName +' succeeded.'

      
GO


        SET NOCOUNT ON
        
        DECLARE @RowCount INT
        
        SELECT  @RowCount = COUNT(*)
        FROM    (SELECT DISTINCT
                        ModelYear=V.ModelYear
                FROM    VehicleCatalog.Firstlook.VehicleCatalog V) S
        LEFT OUTER JOIN
                (SELECT DISTINCT
                        ModelYear=Y.ModelYear
                FROM    VehicleCatalog.Categorization.ModelYear Y
                ) D     ON      D.ModelYear = S.ModelYear
        WHERE   D.ModelYear IS NULL
        
        DECLARE @TestName VARCHAR(200)

		SET @TestName ='[VehicleCatalog].[Categorization_ModelYear__Completeness]'
  
		IF (@RowCount <> 0)  
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+'.'
        ELSE
				PRINT @TestName +' succeeded.'

     
GO
 
        SET NOCOUNT ON
        
        DECLARE @RowCount INT
        
        SELECT  @RowCount = COUNT(*)
        FROM    (SELECT DISTINCT
                        PassengerDoors=V.Doors
                FROM    VehicleCatalog.Firstlook.VehicleCatalog V
                WHERE   Doors <> 0
                AND     Doors IS NOT NULL) S
        FULL OUTER JOIN
                (SELECT DISTINCT
                        PassengerDoors=D.PassengerDoors
                FROM    VehicleCatalog.Categorization.PassengerDoors D
                ) D     ON      D.PassengerDoors = S.PassengerDoors
        WHERE   D.PassengerDoors IS NULL
        	OR S.PassengerDoors IS NULL
        
       DECLARE @TestName VARCHAR(200)

		SET @TestName =' [VehicleCatalog].[Categorization_PassengerDoors__Completeness]'
  IF (@RowCount <> 0) 
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+'.'
        ELSE
				PRINT @TestName +' succeeded.'

        
GO
 

        SET NOCOUNT ON
        
        DECLARE @RowCount INT
        
        SELECT  @RowCount = COUNT(*)
        FROM    (SELECT DISTINCT
                        Transmission=T.Transmission
                FROM    VehicleCatalog.Firstlook.VehicleCatalog V
                JOIN    VehicleCatalog.Firstlook.Transmission T ON T.Transmission = V.Transmission
                WHERE   V.Transmission <> 'UNKNOWN'
                AND     V.Transmission IS NOT NULL) S
        FULL OUTER JOIN
                (SELECT DISTINCT
                        Transmission=T.Name
                FROM    VehicleCatalog.Categorization.Transmission T
                ) D     ON      D.Transmission = S.Transmission
        WHERE   D.Transmission IS NULL
        OR      S.Transmission IS NULL
        
       DECLARE @TestName VARCHAR(200)

		SET @TestName ='[VehicleCatalog].[Categorization_Transmission__Completeness]'
  IF (@RowCount <> 0) 
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+'.'
        ELSE
				PRINT @TestName +' succeeded.'

      

GO
        SET NOCOUNT ON
       
		DECLARE @TestName VARCHAR(200)

		SET @TestName ='[VehicleCatalog].[Categorization_FuelType__Completeness]  '

        DECLARE @RowCount INT
        
        SELECT  @RowCount = COUNT(*)
        FROM    (SELECT DISTINCT
                        FuelType=F.FuelType
                FROM    VehicleCatalog.Firstlook.VehicleCatalog V
                JOIN    VehicleCatalog.Firstlook.FuelType F ON F.FuelType = V.FuelType
                WHERE   V.FuelType <> '-'
                AND     V.FuelType IS NOT NULL) S
        FULL OUTER JOIN
                (SELECT DISTINCT
                        FuelType=T.Code
                FROM    VehicleCatalog.Categorization.FuelType T
				WHERE	T.Code <>'-'
                ) D     ON      D.FuelType = S.FuelType
        WHERE   D.FuelType IS NULL
        OR      S.FuelType IS NULL
        
        IF (@RowCount <> 0) 
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+'.'
        ELSE
				PRINT @TestName +' succeeded.'
GO
   SET NOCOUNT ON
DECLARE @TestName VARCHAR(200)

		SET @TestName =' [VehicleCatalog].[Categorization_Engine__Completeness]'

      
        
        DECLARE @RowCount INT
        
        SELECT  @RowCount = COUNT(*)
        FROM    (SELECT DISTINCT
                        Engine=E.Engine
                FROM    VehicleCatalog.Firstlook.VehicleCatalog V
                JOIN    VehicleCatalog.Firstlook.Engine E ON E.Engine = V.Engine
                WHERE   V.Engine <> 'UNKNOWN'
                AND     V.Engine IS NOT NULL) S
        FULL OUTER JOIN
                (SELECT DISTINCT
                        Engine=E.Name
                FROM    VehicleCatalog.Categorization.Engine E
                ) D     ON      D.Engine = S.Engine
        WHERE   D.Engine IS NULL
        OR      S.Engine IS NULL
        
         IF (@RowCount <> 0) 
				PRINT @TestName +' failed. Expected 0 row-count got '+CAST( @RowCount AS VARCHAR(20))+'.'
        ELSE
				PRINT @TestName +' succeeded.'
GO
