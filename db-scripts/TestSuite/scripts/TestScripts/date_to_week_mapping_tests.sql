
if object_id('#dates') is not null
	drop table #dates
	
create table #dates(
	[DateTime] datetime,
	[Expected_WeekStarting] datetime,
	[Expected_WeekEnding] datetime );
	
insert into #dates values ('2013-06-01 12:00:00', '2013-05-27 00:00:00', '2013-06-03 00:00:00');
insert into #dates values ('2013-06-02 12:00:00', '2013-05-27 00:00:00', '2013-06-03 00:00:00');
insert into #dates values ('2013-06-03 12:00:00', '2013-06-03 00:00:00', '2013-06-10 00:00:00');
insert into #dates values ('2013-06-04 12:00:00', '2013-06-03 00:00:00', '2013-06-10 00:00:00');
insert into #dates values ('2013-06-05 12:00:00', '2013-06-03 00:00:00', '2013-06-10 00:00:00');
insert into #dates values ('2013-06-06 12:00:00', '2013-06-03 00:00:00', '2013-06-10 00:00:00');
insert into #dates values ('2013-06-07 12:00:00', '2013-06-03 00:00:00', '2013-06-10 00:00:00');
insert into #dates values ('2013-06-08 12:00:00', '2013-06-03 00:00:00', '2013-06-10 00:00:00');
insert into #dates values ('2013-06-09 12:00:00', '2013-06-03 00:00:00', '2013-06-10 00:00:00');
insert into #dates values ('2013-06-10 12:00:00', '2013-06-10 00:00:00', '2013-06-17 00:00:00');
insert into #dates values ('2013-06-11 12:00:00', '2013-06-10 00:00:00', '2013-06-17 00:00:00');
insert into #dates values ('2013-06-12 12:00:00', '2013-06-10 00:00:00', '2013-06-17 00:00:00');
insert into #dates values ('2013-06-13 12:00:00', '2013-06-10 00:00:00', '2013-06-17 00:00:00');
insert into #dates values ('2013-06-14 12:00:00', '2013-06-10 00:00:00', '2013-06-17 00:00:00');
insert into #dates values ('2013-06-15 12:00:00', '2013-06-10 00:00:00', '2013-06-17 00:00:00');
insert into #dates values ('2013-06-16 12:00:00', '2013-06-10 00:00:00', '2013-06-17 00:00:00');
insert into #dates values ('2013-06-17 12:00:00', '2013-06-17 00:00:00', '2013-06-24 00:00:00');
insert into #dates values ('2013-06-18 12:00:00', '2013-06-17 00:00:00', '2013-06-24 00:00:00');
insert into #dates values ('2013-06-19 12:00:00', '2013-06-17 00:00:00', '2013-06-24 00:00:00');
insert into #dates values ('2013-06-20 12:00:00', '2013-06-17 00:00:00', '2013-06-24 00:00:00');
insert into #dates values ('2013-06-21 12:00:00', '2013-06-17 00:00:00', '2013-06-24 00:00:00');
insert into #dates values ('2013-06-22 12:00:00', '2013-06-17 00:00:00', '2013-06-24 00:00:00');
insert into #dates values ('2013-06-23 12:00:00', '2013-06-17 00:00:00', '2013-06-24 00:00:00');
insert into #dates values ('2013-06-24 12:00:00', '2013-06-24 00:00:00', '2013-07-01 00:00:00');
insert into #dates values ('2013-06-25 12:00:00', '2013-06-24 00:00:00', '2013-07-01 00:00:00');
insert into #dates values ('2013-06-26 12:00:00', '2013-06-24 00:00:00', '2013-07-01 00:00:00');
insert into #dates values ('2013-06-27 12:00:00', '2013-06-24 00:00:00', '2013-07-01 00:00:00');
insert into #dates values ('2013-06-28 12:00:00', '2013-06-24 00:00:00', '2013-07-01 00:00:00');

select 
	[DateTime],
	[Expected_WeekStarting],
	[Expected_WeekEnding],
	[Calculated_WeekStarting]    = dateadd(week, datediff(week, 0, dateadd(day, -1, [DateTime]))  , 0),
	[Calculated_WeekEnding]      = dateadd(week, datediff(week, 0, dateadd(day, -1, [DateTime]))+1, 0),
	[Pass] = 
		(case when (
		([Expected_WeekStarting] = dateadd(week, datediff(week, 0, dateadd(day, -1, [DateTime]))  , 0)) and
		([Expected_WeekEnding]   = dateadd(week, datediff(week, 0, dateadd(day, -1, [DateTime]))+1, 0)) )
		then 1 else 0 end)
from #dates

