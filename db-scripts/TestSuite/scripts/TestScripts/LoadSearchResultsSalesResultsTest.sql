/*******************************************************************************************************************
*
*	mak	12/10/2009		This script runs aggregations for 10 searches of 10 different owners in different zipcodes
*						The purpose of this script is to verify that the pre V5_18_05 and the post V5_18_05 aggregation
*						procedures yield the same results.
*
**********************************************************************************************************************/

DECLARE @TestSearches TABLE
(	idx			INT Identity(1,1) ,
	SearchID	INT,
	OwnerID		INT,
	ZipCode		CHAR(5),
	DistanceBucketID INT,
	NumberOfListings INT,
	NumberOfSales INT
)

 
 
INSERT INTO	@TestSearches (OwnerID,SearchID,DistanceBucketID)
SELECT MAX(S.OwnerID), Max(SearchID),1
FROM	Pricing.Owner O
JOIN	Pricing.Search S ON O.OwnerID =S.OwnerID
WHERE	O.ZipCode like'0%'

INSERT INTO	@TestSearches (OwnerID,SearchID,DistanceBucketID)
SELECT MAX(S.OwnerID), Max(SearchID),2
FROM	Pricing.Owner O
JOIN	Pricing.Search S ON O.OwnerID =S.OwnerID
WHERE	O.ZipCode like'1%'

INSERT INTO	@TestSearches (OwnerID,SearchID,DistanceBucketID)
SELECT MAX(S.OwnerID), Max(SearchID),3
FROM	Pricing.Owner O
JOIN	Pricing.Search S ON O.OwnerID =S.OwnerID
WHERE	O.ZipCode like'2%'

INSERT INTO	@TestSearches (OwnerID,SearchID,DistanceBucketID)
SELECT MAX(S.OwnerID), Max(SearchID),4
FROM	Pricing.Owner O
JOIN	Pricing.Search S ON O.OwnerID =S.OwnerID
WHERE	O.ZipCode like'3%'

INSERT INTO	@TestSearches (OwnerID,SearchID,DistanceBucketID)
SELECT MAX(S.OwnerID), Max(SearchID),5
FROM	Pricing.Owner O
JOIN	Pricing.Search S ON O.OwnerID =S.OwnerID
WHERE	O.ZipCode like'4%'

INSERT INTO	@TestSearches (OwnerID,SearchID,DistanceBucketID)
SELECT MAX(S.OwnerID), Max(SearchID),6
FROM	Pricing.Owner O
JOIN	Pricing.Search S ON O.OwnerID =S.OwnerID
WHERE	O.ZipCode like'5%' 

INSERT INTO	@TestSearches (OwnerID,SearchID,DistanceBucketID)
SELECT MAX(S.OwnerID), Max(SearchID),7
FROM	Pricing.Owner O
JOIN	Pricing.Search S ON O.OwnerID =S.OwnerID
WHERE	O.ZipCode like'6%'

INSERT INTO	@TestSearches (OwnerID,SearchID,DistanceBucketID)
SELECT MAX(S.OwnerID), Max(SearchID),8
FROM	Pricing.Owner O
JOIN	Pricing.Search S ON O.OwnerID =S.OwnerID
WHERE	O.ZipCode like'7%'

INSERT INTO	@TestSearches (OwnerID,SearchID,DistanceBucketID)
SELECT MAX(S.OwnerID), Max(SearchID),9
FROM	Pricing.Owner O
JOIN	Pricing.Search S ON O.OwnerID =S.OwnerID
WHERE	O.ZipCode like'8%'
INSERT INTO	@TestSearches (OwnerID,SearchID,DistanceBucketID)
SELECT MAX(S.OwnerID), Max(SearchID),10
FROM	Pricing.Owner O
JOIN	Pricing.Search S ON O.OwnerID =S.OwnerID
WHERE	O.ZipCode like'9%'

/* I already get searches above, but with this statement, I can switch between min and max to get new test searches.*/

UPDATE	T
SET		SearchID = MaxSearchID
FROM	@TestSearches T
JOIN
	(SELECT MIN(S.SearchID) as MaxSearchID, OwnerID
	  FROM	Pricing.Search S GROUP BY OwnerID) x
ON	T.OwnerID =X.OwnerID

UPDATE S
SET		DistanceBucketID =T.DistanceBucketID
FROM	@TestSearches T
JOIN	Pricing.Search S ON S.SearchID =T.SearchID

DBCC DROPCLEANBUFFERS
DBCC FREEPROCCACHE

DECLARE @i INT
DECLARE @c INT
DECLARE @OwnerID INT, @SearchID INT
SET @i =1
SELECT @c = MAX(idx) FROM @TestSearches

WHILE (@i <=@c)
BEGIN

	SELECT	@OwnerID =OwnerID,
			@SearchID=SearchID
	FROM	@TestSearches WHERE idx =@i

	EXEC Pricing.LoadSearchResult_F_ByOwnerID @OwnerID, 3, @SearchID,NULL,0
	EXEC Pricing.LoadSearchSales_F_ByOwnerID @OwnerID, 3, @SearchID,NULL,0
	
	SET @i =@i+1
END

UPDATE T
	SET NumberOfListings =F.Units 
	FROM  @TestSearches T
	JOIN	Pricing.SearchResult_F F ON T.SearchID =F.SearchID

UPDATE T
	SET NumberOfSales =F.SalesInBasePeriod 
	FROM  @TestSearches T
	JOIN	Pricing.SearchSales_F F ON T.SearchID =F.SearchID

 
SELECT * FROM @TestSearches T  