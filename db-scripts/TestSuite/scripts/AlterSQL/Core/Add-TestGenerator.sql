
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Core].[TestGenerator]') AND type in (N'U'))
BEGIN

CREATE TABLE Core.TestGenerator (
        TestGeneratorID INT IDENTITY(1,1) NOT NULL,
        Name            VARCHAR(200) NOT NULL,
        ProcedureName   VARCHAR(899) NOT NULL,
        CONSTRAINT PK_Core_TestGenerator PRIMARY KEY (TestGeneratorID),
        CONSTRAINT UK_Core_TestGenerator__Name UNIQUE (Name),
        CONSTRAINT UK_Core_TestGenerator__ProcedureName UNIQUE (ProcedureName)
)

INSERT INTO Core.TestGenerator (Name, ProcedureName) VALUES (
        'Generated: [Market] Inventory Pricing Analyzer (Dealer)',
        'TestSuite.Market.Pricing__GenerateTestOnError#Dealer_InventoryPricingAnalyzer')

INSERT INTO Core.TestGenerator (Name, ProcedureName) VALUES (
        'Generated: [Market] Vehicle Pricing Analyzer (Dealer; Inventory)',
        'TestSuite.Market.Pricing__GenerateTestOnError#Dealer_VehiclePricingAnalyzer_Inventory')

INSERT INTO Core.TestGenerator (Name, ProcedureName) VALUES (
        'Generated: [Market] Vehicle Pricing Analyzer (Dealer; Appraisal)',
        'TestSuite.Market.Pricing__GenerateTestOnError#Dealer_VehiclePricingAnalyzer_Appraisal')

INSERT INTO Core.TestGenerator (Name, ProcedureName) VALUES (
        'Generated: [Market] Vehicle Pricing Analyzer (Dealer; ATC)',
        'TestSuite.Market.Pricing__GenerateTestOnError#Dealer_VehiclePricingAnalyzer_ATC')

INSERT INTO Core.TestGenerator (Name, ProcedureName) VALUES (
        'Generated: [Market] Vehicle Pricing Analyzer (Dealer; In Group)',
        'TestSuite.Market.Pricing__GenerateTestOnError#Dealer_VehiclePricingAnalyzer_InGroup')

INSERT INTO Core.TestGenerator (Name, ProcedureName) VALUES (
        'Generated: [Market] Inventory Pricing Analyzer (Seller)',
        'TestSuite.Market.Pricing__GenerateTestOnError#Seller_InventoryPricingAnalyzer')

INSERT INTO Core.TestGenerator (Name, ProcedureName) VALUES (
        'Generated: [Market] Vehicle Pricing Analyzer (Seller)',
        'TestSuite.Market.Pricing__GenerateTestOnError#Seller_VehiclePricingAnalyzer')

END
GO
