
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='Core')
EXEC('CREATE SCHEMA Core')
GO

CREATE TABLE Core.TestSuite (
        TestSuiteID     INT IDENTITY(1,1) NOT NULL,
        Name            VARCHAR(200) NOT NULL,
        CONSTRAINT PK_Core_TestSuite PRIMARY KEY (TestSuiteID),
        CONSTRAINT CK_Core_TestSuite__Name CHECK (LEN(Name) > 0)
)
GO

CREATE TABLE Core.TestCase (
        TestCaseID      INT IDENTITY(1,1) NOT NULL,
        TestSuiteID     INT NOT NULL,
        Name            VARCHAR(200) NOT NULL,
        ProcedureName   VARCHAR(2000) NOT NULL,
        CONSTRAINT PK_Core_TestCase
                PRIMARY KEY (TestCaseID),
        CONSTRAINT FK_Core_TestCase__TestSuite
                FOREIGN KEY (TestSuiteID)
                REFERENCES Core.TestSuite (TestSuiteID),
        CONSTRAINT CK_Core_TestCase__Name
                CHECK (LEN(Name) > 0),
        CONSTRAINT CK_Core_TestCase__ProcedureName
                CHECK (LEN(ProcedureName) > 0)
)
GO

CREATE TABLE Core.TestCase__GenerateOnError (
        TestCaseID      INT NOT NULL,
        ErrorNumber     INT NULL,
        ErrorMessage    NVARCHAR(4000) NULL,
        ErrorLine       INT NULL,
        ErrorProcedure  NVARCHAR(126) NULL,        
        CONSTRAINT PK_Core_TestCase__GenerateOnError
                PRIMARY KEY (TestCaseID),
        CONSTRAINT FK_Core_TestCase__GenerateOnError__TestCase
                FOREIGN KEY (TestCaseID)
                REFERENCES Core.TestCase (TestCaseID)
)
GO

CREATE TABLE Core.TestRun (
        TestRunID       INT IDENTITY(1,1) NOT NULL,
        TestSuiteID     INT NOT NULL,
        StartTime       DATETIME NOT NULL,
        EndTime         DATETIME NULL,
        Successful      BIT NULL,
        CONSTRAINT PK_Core_TestRun
                PRIMARY KEY (TestRunID),
        CONSTRAINT FK_Core_TestRun__TestSuite
                FOREIGN KEY (TestSuiteID)
                REFERENCES Core.TestSuite (TestSuiteID),
        CONSTRAINT CK_Core_TestRun__Times
                CHECK (EndTime IS NULL OR EndTime >= StartTime)
)
GO

CREATE TABLE Core.TestResult (
        TestResultID    INT IDENTITY(1,1) NOT NULL,
        TestRunID       INT NOT NULL,
        TestCaseID      INT NOT NULL,
        Result          BIT NOT NULL,
        ErrorNumber     INT NULL,
        ErrorMessage    NVARCHAR(4000) NULL,
        ErrorLine       INT NULL,
        ErrorProcedure  NVARCHAR(126) NULL,
        CONSTRAINT PK_Core_TestResult
                PRIMARY KEY (TestResultID),
        CONSTRAINT FK_Core_TestResult__TestCase
                FOREIGN KEY (TestCaseID)
                REFERENCES Core.TestCase (TestCaseID),
        CONSTRAINT FK_Core_TestResult__TestRun
                FOREIGN KEY (TestRunID)
                REFERENCES Core.TestRun (TestRunID)
)
GO
