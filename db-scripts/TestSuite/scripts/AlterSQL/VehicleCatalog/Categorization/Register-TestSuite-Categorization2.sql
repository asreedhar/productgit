/* MAK 02/15/2010  Register additional test cases in Categorization suite.*/

DECLARE @TestSuiteID INT

SELECT @TestSuiteID =TestSuiteID
FROM	Core.TestSuite 
WHERE	Name ='VehicleCatalog.Categorization'



INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'VinPattern / ModelConfiguration Completeness',
        'VehicleCatalog.Categorization_VinPattern_ModelConfiguration__Completeness')


INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'VinPattern / ModelConfiguration Lookup Completeness',
        'VehicleCatalog.Categorization_VinPattern_ModelConfiguration_Lookup__Completeness')
       

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'ModelConfiguration / VehicleCatalog Completeness',
        'VehicleCatalog.Categorization_ModelConfiguration_VehicleCatalog__Completeness')

GO
