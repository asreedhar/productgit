
DECLARE @TestSuiteID INT

INSERT INTO Core.TestSuite (Name) VALUES ('VehicleCatalog.Categorization')

SELECT @TestSuiteID = SCOPE_IDENTITY()

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Make Completeness',
        'VehicleCatalog.Categorization_Make__Completeness')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Line Completeness',
        'VehicleCatalog.Categorization_Line__Completeness')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'ModelFamily Completeness',
        'VehicleCatalog.Categorization_ModelFamily__Completeness')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Series Completeness',
        'VehicleCatalog.Categorization_Series__Completeness')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'BodyType Completeness',
        'VehicleCatalog.Categorization_BodyType__Completeness')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Model Completeness',
        'VehicleCatalog.Categorization_Model__Completeness')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'ModelYear Completeness',
        'VehicleCatalog.Categorization_ModelYear__Completeness')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'PassengerDoors Completeness',
        'VehicleCatalog.Categorization_PassengerDoors__Completeness')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Transmission Completeness',
        'VehicleCatalog.Categorization_Transmission__Completeness')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'FuelType Completeness',
        'VehicleCatalog.Categorization_FuelType__Completeness')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Engine Completeness',
        'VehicleCatalog.Categorization_Engine__Completeness')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Configuration Completeness',
        'VehicleCatalog.Categorization_Configuration__Completeness')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'VinPattern Completeness',
        'VehicleCatalog.Categorization_VinPattern__Completeness')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'ModelConfiguration Completeness',
        'VehicleCatalog.Categorization_ModelConfiguration__Completeness')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'VinPattern / ModelConfiguration Correctness',
        'VehicleCatalog.Categorization_VinPattern_ModelConfiguration__Correctness')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'ModelConfiguration / VehicleCatalog Correctness',
        'VehicleCatalog.Categorization_ModelConfiguration_VehicleCatalog__Correctness')

GO
