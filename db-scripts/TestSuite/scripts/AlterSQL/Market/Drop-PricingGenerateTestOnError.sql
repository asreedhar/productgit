
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Market].[Pricing__GenerateTestOnError]') AND type in (N'P', N'PC'))
DROP PROCEDURE Market.Pricing__GenerateTestOnError
GO
