
DECLARE @TestSuiteID INT

SELECT @TestSuiteID = TestSuiteID
FROM	Core.TestSuite
WHERE	Name ='Market.Listing'

IF (@TestSuiteID IS NULL)
BEGIN
	
	INSERT 
	INTO	Core.TestSuite 
			(Name) 
	VALUES ('Market.Listing')

	SELECT @TestSuiteID = SCOPE_IDENTITY()

END
-- integrity

INSERT 
INTO	Core.TestCase 
		(TestSuiteID, 
		Name, 
		ProcedureName) 
VALUES (
        @TestSuiteID,
        'VehicleDecoded_F Latitude\Longitude',
        'Market.Listing_VehicleDecoded_F_#BadLatitudeLongitude')
GO

