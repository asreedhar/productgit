
DECLARE @TestSuiteID INT

INSERT INTO Core.TestSuite (Name) VALUES ('Market.Listing')

SELECT @TestSuiteID = SCOPE_IDENTITY()

-- integrity

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Vehicle ModelConfiguration Integrity',
        'Market.Listing_Vehicle__ModelConfiguration#Integrity')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Vehicle History ModelConfiguration Integrity',
        'Market.Listing_Vehicle_History__ModelConfiguration#Integrity')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Vehicle Sales ModelConfiguration Integrity',
        'Market.Market.Listing_Vehicle_Sales__ModelConfiguration#Integrity')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Vehicle Decoded ModelConfiguration Integrity',
        'Market.Listing_VehicleDecoded_F__ModelConfiguration#Integrity')

-- accuracy

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Vehicle ModelConfiguration Accuracy',
        'Market.Listing_Vehicle__ModelConfiguration#Accuracy')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Vehicle History ModelConfiguration Accuracy',
        'Market.Listing_Vehicle_History__ModelConfiguration#Accuracy')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Vehicle Sales ModelConfiguration Accuracy',
        'Market.Market.Listing_Vehicle_Sales__ModelConfiguration#Accuracy')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Vehicle Decoded ModelConfiguration Accuracy',
        'Market.Listing_VehicleDecoded_F__ModelConfiguration#Accuracy')
