/* 03/17/2010  MAK  Repair typos in test cases */

UPDATE C
SET ProcedureName = 'Market.Listing_Vehicle_Sales__ModelConfiguration#Integrity'
FROM	Core.TestCase C
WHERE	Name ='Vehicle Sales ModelConfiguration Integrity'

GO

UPDATE C
SET	ProcedureName  = 'Market.Listing_Vehicle_Sales__ModelConfiguration#Accuracy'
FROM	Core.TestCase C
WHERE	Name ='Vehicle Sales ModelConfiguration Accuracy'

GO