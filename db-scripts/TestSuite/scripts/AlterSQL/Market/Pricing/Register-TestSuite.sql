
DECLARE @TestSuiteID INT

INSERT INTO Core.TestSuite (Name) VALUES ('Market.Pricing')

SELECT @TestSuiteID = SCOPE_IDENTITY()

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Owner Completeness',
        'Market.Pricing_Owner__Completeness')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Owner Correctness',
        'Market.Pricing_Owner__Correctness')

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Search Completeness',
        'Market.Pricing_Search__Completeness')

