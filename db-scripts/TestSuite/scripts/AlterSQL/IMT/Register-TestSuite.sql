
DECLARE @TestSuiteID INT

INSERT INTO Core.TestSuite (Name) VALUES ('IMT')

SELECT @TestSuiteID = SCOPE_IDENTITY()

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Auto Check Credentials Reset',
        'IMT.AutoCheck_CheckMemberCredentials')
        
INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Carfax Check Credentials Reset',
        'IMT.Carfax_Check_CheckMemberCredentials')        

INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'Check Member Emails reset.',
        'IMT.dbo_CheckMembersEmailAddresses')
        
INSERT INTO Core.TestCase (TestSuiteID, Name, ProcedureName) VALUES (
        @TestSuiteID,
        'No duplicate Auto renew Carfax reports.',
        'IMT.Carfax_RepurchaseExpiredReports#NoDuplicates_GenerateTestOnError')      
GO
