/* 03/17/2010  MAK  Repair typos in test cases */

UPDATE	TC
SET		ProcedureName ='IMT.Carfax_CheckMemberCredentials'
FROM	Core.TestCase TC
WHERE	ProcedureName ='IMT.Carfax_Check_CheckMemberCredentials'

GO

UPDATE TC
SET		ProcedureName ='IMT.Carfax_RepurchaseExpiredReports#NoDuplicates'
FROM	Core.TestCase TC
WHERE	ProcedureName = 'IMT.Carfax_RepurchaseExpiredReports#NoDuplicates_GenerateTestOnError'
GO