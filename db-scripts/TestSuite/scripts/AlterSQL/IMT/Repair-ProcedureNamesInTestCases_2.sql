/* 03/17/2010  MAK  Repair typos in test cases */

UPDATE TC
SET		ProcedureName ='IMT.Carfax_RepurchaseExpiredReports_NoDuplicates'
FROM	Core.TestCase TC
WHERE	ProcedureName = 'IMT.Carfax_RepurchaseExpiredReports#NoDuplicates'
GO