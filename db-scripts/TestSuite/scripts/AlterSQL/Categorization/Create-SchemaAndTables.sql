
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='Categorization')
EXEC('CREATE SCHEMA Categorization')
GO

CREATE TABLE [Categorization].[Vehicle](
	[VehicleOrRowID] [int] IDENTITY(1,1) NOT NULL,
	[VIN] [varchar](17) NOT NULL,
	[Series] [varchar](50) NULL,
	[Transmission] [varchar](100) NULL,
	[FuelType] [varchar](50) NULL,
	[Engine] [varchar](50) NULL,
	[DriveTrain] [varchar](50) NULL,
	[TestDescription] [varchar](500) NULL,
	[ModelID] [int] NULL,
	[ConfigurationID] [int] NULL
) ON [PRIMARY]

GO

CREATE TABLE [Categorization].[Vehicle__Categorization](
	[Series] [varchar](50) NULL,
	[_Categorization_RowID] [int] NOT NULL,
	[_Categorization_VINPatternID] [int] NULL,
	[_Categorization_MakeID] [tinyint] NULL,
	[_Categorization_LineID] [smallint] NULL,
	[_Categorization_ModelFamilyID] [smallint] NULL,
	[_Categorization_SegmentID] [tinyint] NULL,
	[_Categorization_BodyTypeID] [tinyint] NULL,
	[_Categorization_SeriesID] [int] NULL,
	[_Categorization_ModelYear] [smallint] NULL,
	[_Categorization_PassengerDoors] [tinyint] NULL,
	[_Categorization_TransmissionID] [tinyint] NULL,
	[_Categorization_DriveTrainID] [tinyint] NULL,
	[_Categorization_FuelTypeID] [tinyint] NULL,
	[_Categorization_EngineID] [smallint] NULL,
	[_Categorization_ModelID] [int] NULL,
	[_Categorization_ConfigurationID] [int] NULL,
	[_Categorization_ModelConfigurationID] [int] NULL,
	[_Categorization_PossibleModelPermutation] [bit] NULL,
	[_Categorization_PossibleConfigurationPermutation] [bit] NULL
) ON [PRIMARY]

GO
