/*

Use this template when create a ReportWarehouse database instance.  (May need some debugging
around the alter script creation part...)

--  Undo
DROP DATABASE ReportWarehouse

*/


CREATE DATABASE ReportWarehouse
  on primary
   (name = ReportWarehouse,      filename = '<drive>\<path>\ReportWarehouse.mdf'
    ,size = 50MB, maxsize = unlimited, filegrowth = 50)
  log on
   (name = ReportWarehouse_log,  filename = '<drive>\<path>\ReportWarehouse_log.ldf'
    ,size = 100MB, maxsize = unlimited, filegrowth = 20%)

ALTER DATABASE ReportWarehouse
 set
   auto_create_statistics on
  ,auto_update_statistics on
  ,recovery simple

GO


CREATE TABLE Alter_Script
 (
   AlterScriptName  varchar(50)  not null
    constraint PK_Alter_Script
     primary key clustered
  ,ApplicationDate  datetime     not null 
    constraint DF_Alter_Script_ApplicationDate
     default CURRENT_TIMESTAMP
 )

INSERT Alter_Script (AlterScriptName) values ('Database created and initialized')

GO
