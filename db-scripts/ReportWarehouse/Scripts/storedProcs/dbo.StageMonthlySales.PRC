IF objectproperty(object_id('StageMonthlySales'), 'isProcedure') = 1
    DROP PROCEDURE StageMonthlySales

GO
/******************************************************************************
**
**  Procedure: StageMonthlySales
**  Description: This routine will calculate and load the Staging_MonthlySales
**   table with values for all dealers currently present in the Staging_Dealer
**   table for the desired year/month.  (The StageDealers routine must first be
**   run to add dealers to the Staging_Dealer table.)  Calculated values will
**   remain in the Staging_MonthlySales table until loaded to the live tables
**   with stored procedure LoadMonthlySales.
**
**
**  Return values:  0 if succesful, 1 if not
**
**  Input parameters:   See below
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  12/05/2006  PKelley   Procedure created.
**  12/21/2006  PKelley   Split dealer staging to its own procedure
**
*****************************************************************************/
CREATE PROCEDURE dbo.StageMonthlySales

    @Year              smallint
       --  Year to load data for

   ,@Month             tinyint
       --  Month to load data for

   ,@OriginatingLogID  int  = null
       --  Enable log chaining

AS

    SET NOCOUNT on

    DECLARE 
      @FirstOfMonth      smalldatetime
     ,@FirstOfNextMonth  smalldatetime
     ,@YearMonthKey      smallint
     ,@Message           varchar(1000)
     ,@RowCount          int
     ,@Error             int
     ,@DBName            sysname
     ,@SPName            sysname
     ,@IMTAsOf           char(12)


    --  Initialize values
    SET @DBName = db_name()
    SET @SPName = object_name(@@procid)
    SET @Message = 'Staging Monthly Sales data for year = ' + isnull(cast(@Year as varchar(10)), '<null>')
     + ', month = ' + isnull(cast(@Month as varchar(10)), '<null>')

    EXECUTE Utility.dbo.LogEvent 'I', @DBName, @SPName, @Message, @OriginatingLogID OUTPUT

    --  Get the appropriate datetimes
    SET @Message = ''
    SELECT
       @FirstOfMonth     = FirstDayOfMonth
      ,@FirstOfNextMonth = dateadd(mm, 1, FirstDayOfMonth)
      ,@YearMonthKey     = YearMonthKey
     from YearMonth
     where Year = @Year
      and MonthNumber = @Month
    
    IF @@rowcount = 0
        SET @Message = 'Invalid year/month'

    IF @Year is null or @Month is null
        --  If either is null, clarify that error message
        SET @Message = 'Both the Year and the Month must be set'

    IF (select Status
         from StagingStatus
         where StagingTable = 'MonthlySales') = 'Completed'
        --  This means the last configured set of data has been loaded, and
        --  the dealer(s) to load for the next set have not yet been specified
        SET @Message = 'The dealers to be processed must first be staged'


    --  If anything's wrong, @Message has been set
    IF @Message <> '' GOTO _Failed_


    --  Are we clear to run the long query?
    IF exists (select 1
                from Staging_Dealer sd
                 inner join Staging_MonthlySales ms
                  on ms.YearMonthKey = @YearMonthKey
                   and ms.DealershipID = sd.DealershipID)
     BEGIN
        --  Data for the year/month and at least one of the targeted dealers is already in the table
        SET @Message = 'Monthly sales data is already be present in the Staging table for one or more of the staged dealers'
        GOTO _Failed_
     END


    --  Attempt to stage fact data
    INSERT Staging_MonthlySales
      (
         YearMonthKey
        ,DealershipID
        ,VehicleDescriptionKey
        ,Units
       ,UnitCost
        ,FrontEndGross
        ,BackEndGross
        ,LoadStatus
      )
     select
         @YearMonthKey
        ,sd.DealershipID
        ,vd.VehicleDescriptionKey
        ,mosh.Units
       ,mosh.UnitCost
        ,mosh.FrontEndGross
        ,mosh.BackEndGross
        ,0    --  Default to "don't load"
      from (
             select
               inv.BusinessUnitID
              ,inv.InventoryType
              ,inv.TradeOrPurchase
              ,vs.SaleDescription
              ,case
                 when datediff(dd, inv.InventoryReceivedDate, vs.DealDate) < 30 then '<30'
                 else '>29'
               end DaysToSale
              ,count(*)                                 Units
              ,sum(inv.UnitCost)                        UnitCost
              ,sum(vs.FrontEndGross)                    FrontEndGross
              ,sum(vs.BackEndGross)                     BackEndGross
             from vInventory inv
              inner join vtbl_VehicleSale vs
               on vs.InventoryID = inv.InventoryID
             where inv.BusinessUnitID in (select BusinessUnitID from Staging_Dealer)
              and inv.TradeOrPurchase between 1 and 2
              and vs.SaleDescription in ('R', 'W')
              and vs.DealDate >= @FirstOfMonth
              and vs.DealDate < @FirstOfNextMonth
             group by
               inv.BusinessUnitID
              ,inv.InventoryType
              ,inv.TradeOrPurchase
              ,vs.SaleDescription
              ,case
                 when datediff(dd, inv.InventoryReceivedDate, vs.DealDate) < 30 then '<30'
                 else '>29'
               end
         ) mosh
        inner join Staging_Dealer sd
         on sd.DealershipID = mosh.BusinessUnitID
        inner join VehicleDescription vd
         on vd.NewOrUsed = case mosh.InventoryType when 1 then 'New' else 'Used' end
          and vd.TradeOrPurchase = case mosh.TradeOrPurchase when 1 then 'Purchase' else 'Trade' end
          and vd.RetailOrWholesale = case mosh.SaleDescription when 'R' then 'Retail' else 'Wholesale' end
          and vd.DaysToSale = mosh.DaysToSale


    --  Check if it worked (situation should already have been caught, but check anyway)
    SELECT @Error = @@error, @RowCount = @@rowcount

    IF @Error <> 0
     BEGIN
        IF @Error = 2627
            SET @Message = 'Primary key violation: cannot load data for a year/month if any data for that year/month is staged but not yet loaded'
        ELSE
            SET @Message = 'Error while loading data to the staging table'
        GOTO _Failed_
     END


    IF @RowCount > 0
     BEGIN
        --  Revise the staging status only if potentially relevant data was found

        --  Identify the "as of" datetime of the source (IMT) database
        IF object_id('Utility.dbo.InformalLogShipping') is not null
            --  Informal Log Shipping utility is present.  Check if the warehouse source database
            --  is being logged shipped, and if so get the datetime of the last restored file.
            SELECT @IMTAsOf = LastLogFileDateTime
             from Utility.dbo.InformalLogShipping
             where DBName = (select TargetDB from ViewMaster where TargetTable = 'BusinessUnit')
    
        IF @IMTAsOf is null
            --  The warehouse source database is not being logged shipped 
            SET @IMTAsOf = 'Live Source'


        UPDATE StagingStatus
         set
           StatusSetAt                = getdate()
          ,SourceRestoredThru_Interim = @IMTAsOf
         where StagingTable = 'MonthlySales'
     END

    SET @Message = 'Staged ' + cast(@RowCount as varchar(10)) + ' rows of data'
    EXECUTE Utility.dbo.LogEvent 'I', @DBName, @SPName, @Message, @OriginatingLogID

RETURN 0

    _Failed_:
 
    EXECUTE Utility.dbo.LogEvent 'E', @DBName, @SPName, @Message, @OriginatingLogID
    RAISERROR(@Message, 11, 1)

RETURN 1

GO
