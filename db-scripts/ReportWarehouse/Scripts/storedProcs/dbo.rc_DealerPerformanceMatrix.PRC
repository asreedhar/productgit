IF objectproperty(object_id('rc_DealerPerformanceMatrix'), 'isProcedure') is null
 BEGIN
    --  Procedure (or function) does not exist, create a dummy placeholder
    DECLARE @Placeholder varchar(100)
    SET @Placeholder = 'CREATE PROCEDURE dbo.rc_DealerPerformanceMatrix AS RETURN 0'
    EXEC(@PlaceHolder)

    --  Configure access rights
    GRANT EXECUTE on rc_DealerPerformanceMatrix TO WarehouseUser
 END

GO
ALTER PROCEDURE dbo.rc_DealerPerformanceMatrix

---Description--------------------------------------------------------------------------------------
--
-- 	This is used for a report called, "Year Over Year Analysis - Dealer Group Summary Rollup"
--
---Parameters---------------------------------------------------------------------------------------
--
 @varDealerGroupID int,
 @varFirstMonth1 int,
 @varFirstYear1 int,
 @varLastMonth1 int,
 @varLastYear1 int,
 @varFirstMonth2 int,
 @varFirstYear2 int,
 @varLastMonth2 int,
 @varLastYear2 int
--
---History----------------------------------------------------------------------
--	
--	JAA	11/20/2006	Initial design/developemt
--
------------------------------------------------------------------------------	

AS

SET NOCOUNT ON

Declare @inFirstDayOfMonth1 smalldatetime
Declare @inFirstYearMonthKey1 smallint
Declare @inLastDayOfMonth1 smalldatetime
Declare @inLastYearMonthKey1 smallint

SELECT @inFirstYearMonthKey1 = YearMonthKey , @inFirstDayOfMonth1 = FirstDayOfMonth
FROM  YearMonth
WHERE [Year] = @varFirstYear1
  And [MonthNumber] = @varFirstMonth1

SELECT @inLastYearMonthKey1 = YearMonthKey , @inLastDayOfMonth1 = LastDayOfMonth
FROM  YearMonth
WHERE [Year] = @varLastYear1
  And [MonthNumber] = @varLastMonth1

PRINT @inFirstYearMonthKey1
PRINT @inFirstDayOfMonth1
PRINT @inLastYearMonthKey1
PRINT @inLastDayOfMonth1

Declare @inFirstDayOfMonth2 smalldatetime
Declare @inFirstYearMonthKey2 smallint
Declare @inLastDayOfMonth2 smalldatetime
Declare @inLastYearMonthKey2 smallint

SELECT @inFirstYearMonthKey2 = YearMonthKey , @inFirstDayOfMonth2 = FirstDayOfMonth
FROM  YearMonth
WHERE [Year] = @varFirstYear2
  And [MonthNumber] = @varFirstMonth2

SELECT @inLastYearMonthKey2 = YearMonthKey , @inLastDayOfMonth2 = LastDayOfMonth
FROM  YearMonth
WHERE [Year] = @varLastYear2
  And [MonthNumber] = @varLastMonth2

PRINT @inFirstYearMonthKey2
PRINT @inFirstDayOfMonth2
PRINT @inLastYearMonthKey2
PRINT @inLastDayOfMonth2


-----------------------------------------------------------------------
SELECT
	d.DealerKey , d.DealerGroup , d.DealerGroupID , d.Dealership , d.DealershipID , d.GoLiveDate  , d.LastUpdated ,
	a.DealerKeyA , a.PriorRetailUnits , a.PriorRetailUnitCost , a.PriorRetailFrontEndGross , a.PriorRetailBackEndGross , a.PriorRetailTotalGross , a.PriorRetailMonths ,
	b.DealerKeyB , b.RetailUnits , b.RetailUnitCost , b.RetailFrontEndGross , b.RetailBackEndGross , b.RetailTotalGross , b.RetailMonths ,
	c.DealerKeyC , c.PriorNoSaleUnits , c.PriorNoSaleTotalGross , c.PriorNoSaleMonths ,
	e.DealerKeyE , e.NoSaleUnits , e.NoSaleTotalGross , e.NoSaleMonths ,
	f.DealerKeyF , f.PriorImmediateWholesaleUnits , f.PriorImmediateWholesaleTotalGross , f.PriorImmediateWholesaleMonths ,
	g.DealerKeyG , g.ImmediateWholesaleUnits , g.ImmediateWholesaleTotalGross , g.ImmediateWholesaleMonths ,
	h.DealerKeyH , h.PriorTotalWholesaleUnits , h.PriorTotalWholesaleTotalGross , h.PriorTotalWholesaleMonths ,
	i.DealerKeyI , i.TotalWholesaleUnits , i.TotalWholesaleTotalGross , i.TotalWholesaleMonths

FROM Dealer d
-----------------------------------------------------------------------
LEFT JOIN
-----------------------------------------------------------------------
(
	SELECT ms.DealerKey 'DealerKeyA' , SUM( ms.Units ) 'PriorRetailUnits' , SUM( ms.UnitCost ) 'PriorRetailUnitCost' , SUM( ms.FrontEndGross ) 'PriorRetailFrontEndGross',
			 SUM( ms.BackEndGross ) 'PriorRetailBackEndGross' , SUM( ms.TotalGross ) 'PriorRetailTotalGross' , COUNT( DISTINCT( ms.YearMonthKey ) ) 'PriorRetailMonths'
	FROM MonthlySales ms
	JOIN VehicleDescription vd ON ( ms.VehicleDescriptionKey = vd.VehicleDescriptionKey )
	WHERE YearMonthKey Between @inFirstYearMonthKey1 And @inLastYearMonthKey1
	  And vd.RetailOrWholesale = 'Retail'
	  And vd.NewOrUsed = 'Used'
	GROUP BY ms.DealerKey
) a ON ( d.DealerKey = a.DealerKeyA )
------------------------------------------------------------------------
LEFT JOIN
------------------------------------------------------------------------
(
	SELECT ms.DealerKey 'DealerKeyB' , SUM( ms.Units ) 'RetailUnits' , SUM( ms.UnitCost ) 'RetailUnitCost' , SUM( ms.FrontEndGross ) 'RetailFrontEndGross',
			 SUM( ms.BackEndGross ) 'RetailBackEndGross' , SUM( ms.TotalGross ) 'RetailTotalGross'  , COUNT( DISTINCT( ms.YearMonthKey ) ) 'RetailMonths'
	FROM MonthlySales ms
	JOIN VehicleDescription vd ON ( ms.VehicleDescriptionKey = vd.VehicleDescriptionKey )
	WHERE YearMonthKey Between @inFirstYearMonthKey2 And @inLastYearMonthKey2
	  And vd.RetailOrWholesale = 'Retail'
	  And vd.NewOrUsed = 'Used'
	GROUP BY ms.DealerKey
) b ON ( d.DealerKey  = b.DealerKeyB )
------------------------------------------------------------------------
LEFT JOIN
------------------------------------------------------------------------
(
	SELECT ms.DealerKey 'DealerKeyC' , SUM( ms.Units ) 'PriorNoSaleUnits' , SUM( ms.UnitCost ) 'PriorNoSaleUnitCost' , SUM( ms.FrontEndGross ) 'PriorNoSaleFrontEndGross',
			 SUM( ms.BackEndGross ) 'PriorNoSaleBackEndGross' , SUM( ms.TotalGross ) 'PriorNoSaleTotalGross'  , COUNT( DISTINCT( ms.YearMonthKey ) ) 'PriorNoSaleMonths'
	FROM MonthlySales ms
	JOIN VehicleDescription vd ON ( ms.VehicleDescriptionKey = vd.VehicleDescriptionKey )
	WHERE YearMonthKey Between @inFirstYearMonthKey1 And @inLastYearMonthKey1
	  And vd.RetailOrWholesale = 'Wholesale'
	  And vd.NewOrUsed = 'Used'
	  And vd.DaysToSale = '>29'
	GROUP BY ms.DealerKey
) c ON ( d.DealerKey  = c.DealerKeyC )
------------------------------------------------------------------------
LEFT JOIN
------------------------------------------------------------------------
(
	SELECT ms.DealerKey 'DealerKeyE' , SUM( ms.Units ) 'NoSaleUnits' , SUM( ms.UnitCost ) 'NoSaleUnitCost' , SUM( ms.FrontEndGross ) 'NoSaleFrontEndGross',
			 SUM( ms.BackEndGross ) 'NoSaleBackEndGross' , SUM( ms.TotalGross ) 'NoSaleTotalGross'  , COUNT( DISTINCT( ms.YearMonthKey ) ) 'NoSaleMonths'
	FROM MonthlySales ms
	JOIN VehicleDescription vd ON ( ms.VehicleDescriptionKey = vd.VehicleDescriptionKey )
	WHERE YearMonthKey Between @inFirstYearMonthKey2 And @inLastYearMonthKey2
	  And vd.RetailOrWholesale = 'Wholesale'
	  And vd.NewOrUsed = 'Used'
	  And vd.DaysToSale = '>29'
	GROUP BY ms.DealerKey
) e ON ( d.DealerKey  = e.DealerKeyE )
------------------------------------------------------------------------
LEFT JOIN
------------------------------------------------------------------------
(
	SELECT ms.DealerKey 'DealerKeyF' , SUM( ms.Units ) 'PriorImmediateWholesaleUnits' , SUM( ms.UnitCost ) 'PriorImmediateWholesaleUnitCost' , SUM( ms.FrontEndGross ) 'PriorImmediateWholesaleFrontEndGross',
			 SUM( ms.BackEndGross ) 'PriorImmediateWholesaleBackEndGross' , SUM( ms.TotalGross ) 'PriorImmediateWholesaleTotalGross'  , COUNT( DISTINCT( ms.YearMonthKey ) ) 'PriorImmediateWholesaleMonths'
	FROM MonthlySales ms

	JOIN VehicleDescription vd ON ( ms.VehicleDescriptionKey = vd.VehicleDescriptionKey )
	WHERE YearMonthKey Between @inFirstYearMonthKey1 And @inLastYearMonthKey1
	  And vd.RetailOrWholesale = 'Wholesale'
	  And vd.DaysToSale = '<30'
	GROUP BY ms.DealerKey
) f ON ( d.DealerKey  = f.DealerKeyF )
------------------------------------------------------------------------
LEFT JOIN
------------------------------------------------------------------------
(
	SELECT ms.DealerKey 'DealerKeyG' , SUM( ms.Units ) 'ImmediateWholesaleUnits' , SUM( ms.UnitCost ) 'ImmediateWholesaleUnitCost' , SUM( ms.FrontEndGross ) 'ImmediateWholesaleFrontEndGross',
			 SUM( ms.BackEndGross ) 'ImmediateWholesaleBackEndGross' , SUM( ms.TotalGross ) 'ImmediateWholesaleTotalGross'  , COUNT( DISTINCT( ms.YearMonthKey ) ) 'ImmediateWholesaleMonths'
	FROM MonthlySales ms
	JOIN VehicleDescription vd ON ( ms.VehicleDescriptionKey = vd.VehicleDescriptionKey )
	WHERE YearMonthKey Between @inFirstYearMonthKey2 And @inLastYearMonthKey2
	  And vd.RetailOrWholesale = 'Wholesale'
	  And vd.DaysToSale = '<30'
	GROUP BY ms.DealerKey
) g ON ( d.DealerKey  = g.DealerKeyG )
------------------------------------------------------------------------
LEFT JOIN
------------------------------------------------------------------------
(
	SELECT ms.DealerKey 'DealerKeyH' , SUM( ms.Units ) 'PriorTotalWholesaleUnits' , SUM( ms.UnitCost ) 'PriorTotalWholesaleUnitCost' , SUM( ms.FrontEndGross ) 'PriorTotalWholesaleFrontEndGross',
			 SUM( ms.BackEndGross ) 'PriorTotalWholesaleBackEndGross' , SUM( ms.TotalGross ) 'PriorTotalWholesaleTotalGross'  , COUNT( DISTINCT( ms.YearMonthKey ) ) 'PriorTotalWholesaleMonths'
	FROM MonthlySales ms
	JOIN VehicleDescription vd ON ( ms.VehicleDescriptionKey = vd.VehicleDescriptionKey )
	WHERE YearMonthKey Between @inFirstYearMonthKey1 And @inLastYearMonthKey1
	  And vd.RetailOrWholesale = 'Wholesale'
	GROUP BY ms.DealerKey
) h ON ( d.DealerKey  = h.DealerKeyH )
------------------------------------------------------------------------
LEFT JOIN
------------------------------------------------------------------------
(
	SELECT ms.DealerKey 'DealerKeyI' , SUM( ms.Units ) 'TotalWholesaleUnits' , SUM( ms.UnitCost ) 'TotalWholesaleUnitCost' , SUM( ms.FrontEndGross ) 'TotalWholesaleFrontEndGross',
			 SUM( ms.BackEndGross ) 'TotalWholesaleBackEndGross' , SUM( ms.TotalGross ) 'TotalWholesaleTotalGross'  , COUNT( DISTINCT( ms.YearMonthKey ) ) 'TotalWholesaleMonths'
	FROM MonthlySales ms
	JOIN VehicleDescription vd ON ( ms.VehicleDescriptionKey = vd.VehicleDescriptionKey )
	WHERE YearMonthKey Between @inFirstYearMonthKey2 And @inLastYearMonthKey2
	  And vd.RetailOrWholesale = 'Wholesale'
	GROUP BY ms.DealerKey
) i ON ( d.DealerKey  = i.DealerKeyI )

WHERE DealerGroupID = @varDealerGroupID

GO
