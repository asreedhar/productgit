/*

Log shipping utilities and (admittedly lightweight) documentation.

Informal log shipping settings are found in table UTILITY.dbo.InformalLogShipping:
 - One row per database being log shipped
 - DBName is the log shipping destination, SourceDBName is the original DB name
 - Set column StandbyFile to (empty) to switch the log-shipped database to norecovery
    (i.e. can't read the data) mode
 - Set LastLogFileDateTime to the "date/time string" from the last applied backup
    file; subsequent restores will start with files stamped after this


--  What's in there now
SELECT * from Utility.dbo.InformalLogShipping


Use these if you need to reset the log-shipped database between "standby" and
"norecovery" modes (where standby allows read-only acces and norecovery does not).
The changes come into effect the next time log shipping restores are performed.

--  Norecovery mode
UPDATE InformalLogShipping
 set StandbyFile = ''

--  Standby mode
UPDATE InformalLogShipping
 set StandbyFile = 'H:\SQL_DataFiles\IMT_copy_standby.stb'

*/


/*

Routine to rebuild IMT_Copy on proddb02, for use with the "Informal Log Shipping"
routines as used to support the ReportWarehouse database on proddb02, based on
system configurations as of July 2007.  Use it as follows:
 - Run with @Live = 0, to confirm that files are found AND to get the datetime
    string of the differential file applied (or that of the complete file, if
    there is no applicable differential file
 - Run with @Live = 1, to restore the database
 - Run the following bit to update InformalLogShipping.LastLogFileDateTime with
    the datetime string of the last applied file.  (That, or drop and recreate
    the row.)

*/


DECLARE @ReturnVal  int

EXECUTE @ReturnVal = sp_ComprehensiveRestore_SLS
  @SourceDatabase           = 'IMT'
 ,@TargetDatabase           = 'IMT_copy'
 ,@FinalState               = 2  --  Standby mode
 ,@StandbyFile              = 'H:\SQL_DataFiles\IMT_copy_standby.stb'
 ,@CompleteBackupFolder     = '\\ord4dc01\proddb01sql\databasebackups\imt'
 ,@DifferentialBackupFolder = '\\ord4dc01\proddb01sql\databasebackups\imt'
 ,@FileLocations            = '
   move "Data1" to "H:\SQL_Datafiles\IMT_copy.mdf"
  ,move "Data2" to "H:\SQL_Datafiles\IMT_copy_Data2.ndf"
  ,move "IDX1"  to "H:\SQL_Datafiles\IMT_copy_IDX.ndf"
  ,move "Log"   to "I:\SQL_Datafiles\IMT_copy_Log.ldf"
'  --   "move" functionality not amenable to quick templates...
 ,@Live                     = 0  --  Switch to 1 to do the work

PRINT @ReturnVal


----  Reset the date/time of the last applied restore file
--UPDATE InformalLogShipping
-- set LastLogFileDateTime = 'YYYYMMDDhhmm'
-- where DBName = 'IMT_Copy'


/*

--  Use this to configure "from scratch" InformalLogShipping entry for the IMT to
--  IMT_Copy log shipping process.
INSERT InformalLogShipping (DBName, LastUpdated, LastLogFileDateTime, SourceDBName, BackupFolder, StandbyFile)
 values
  (
    'IMT_copy'
   ,getdate()
   ,'200701100230'    --  datetime string from last applied differential or complete backup
   ,'IMT'
   ,'\\ord4dc01\proddb01sql\databasebackups\imt'  --  Where are the t-log backups
   ,'H:\SQL_DataFiles\IMT_copy_standby.stb'            --  Where to place the standby file
  )


--  Template to use for changing settings
UPDATE InformalLogShipping
 set
   StandbyFile = 'H:\SQL_DataFiles\IMT_copy_standby.stb'
  ,BackupFolder = '\\ord4dc01\proddb01sql\databasebackups\imt'
 where DBName = 'IMT_Copy'



--  Sample calls to log shipping routine
EXECUTE dbo.RunInformalLogShipping 'IMT', 1

*/
