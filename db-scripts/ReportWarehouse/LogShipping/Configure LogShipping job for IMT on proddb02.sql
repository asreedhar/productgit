/*

Configure job "Log Shipping: IMT_copy" for proddb02
 - disabled

*/


BEGIN TRANSACTION            
  DECLARE @JobID BINARY(16)  
  DECLARE @ReturnCode INT    
  SELECT @ReturnCode = 0     
IF (SELECT COUNT(*) FROM msdb.dbo.syscategories WHERE name = N'LogShipping') < 1 
  EXECUTE msdb.dbo.sp_add_category @name = N'LogShipping'
IF (SELECT COUNT(*) FROM msdb.dbo.sysjobs WHERE name = N'Log Shipping: IMT_copy') > 0 
  PRINT N'The job "Log Shipping: IMT_copy" already exists so will not be replaced.'
ELSE
BEGIN 

  -- Add the job
  EXECUTE @ReturnCode = msdb.dbo.sp_add_job @job_id = @JobID OUTPUT , @job_name = N'Log Shipping: IMT_copy', @owner_login_name = N'sa', @description = N'IMT informal log shipping', @category_name = N'LogShipping', @enabled = 0, @notify_level_email = 0, @notify_level_page = 0, @notify_level_netsend = 0, @notify_level_eventlog = 2, @delete_level= 0
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

  -- Add the job steps
  EXECUTE @ReturnCode = msdb.dbo.sp_add_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'Apply backup files', @command = N'EXECUTE dbo.RunInformalLogShipping ''IMT_copy'', 2, 1', @database_name = N'Utility', @server = N'', @database_user_name = N'', @subsystem = N'TSQL', @cmdexec_success_code = 0, @flags = 0, @retry_attempts = 0, @retry_interval = 1, @output_file_name = N'C:\AuditLogs\SQLAgentJobs\InformalLogShipping_IMT_copy.log', @on_success_step_id = 0, @on_success_action = 3, @on_fail_step_id = 3, @on_fail_action = 4
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

  EXECUTE @ReturnCode = msdb.dbo.sp_add_jobstep @job_id = @JobID, @step_id = 2, @step_name = N'Timestamp and cycle audit logs (on success)', @command = N'--  Timestamp audit log files ,delete sufficiently aged ones
EXECUTE sp_FileTimestampAndCycle ''C:\AuditLogs\SQLAgentJobs'', ''InformalLogShipping_IMT_copy.log'', 21', @database_name = N'master', @server = N'', @database_user_name = N'', @subsystem = N'TSQL', @cmdexec_success_code = 0, @flags = 0, @retry_attempts = 0, @retry_interval = 1, @output_file_name = N'', @on_success_step_id = 0, @on_success_action = 1, @on_fail_step_id = 0, @on_fail_action = 2
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

  EXECUTE @ReturnCode = msdb.dbo.sp_add_jobstep @job_id = @JobID, @step_id = 3, @step_name = N'Timestamp and cycle audit logs (on failure)', @command = N'--  Timestamp audit log files ,delete sufficiently aged ones
EXECUTE sp_FileTimestampAndCycle ''C:\AuditLogs\SQLAgentJobs'', ''InformalLogShipping_IMT_copy.log'', 21', @database_name = N'master', @server = N'', @database_user_name = N'', @subsystem = N'TSQL', @cmdexec_success_code = 0, @flags = 0, @retry_attempts = 0, @retry_interval = 1, @output_file_name = N'', @on_success_step_id = 0, @on_success_action = 2, @on_fail_step_id = 0, @on_fail_action = 2
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 


  EXECUTE @ReturnCode = msdb.dbo.sp_update_job @job_id = @JobID, @start_step_id = 1 
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

  -- Add the job schedules
  EXECUTE @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id = @JobID, @name = N'Interlaced with dataload runs', @enabled = 1, @freq_type = 4, @active_start_date = 20061229, @active_start_time = 1500, @freq_interval = 1, @freq_subday_type = 8, @freq_subday_interval = 5, @freq_relative_interval = 0, @freq_recurrence_factor = 0, @active_end_date = 99991231, @active_end_time = 235959
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

  -- Add the Target Servers
  EXECUTE @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @JobID, @server_name = N'(local)' 
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

END
COMMIT TRANSACTION          
GOTO   EndSave              
QuitWithRollback:
  IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION 
EndSave: 

