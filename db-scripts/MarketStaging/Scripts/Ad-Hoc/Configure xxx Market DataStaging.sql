/*

[Template derived in Dec 2--7 from July 2007 Photo URL "_Template"]

Configure xxx dataStaging:
 - Create DBASTAT entry for datafeed
 - Create the job *template* for the dataStaging

Start by searching and replacing on
 - "xxx" with the datafeed code
 - "yyy" with the Datasources ID value


Work over the DBASTAT insert command with descriptions, designated
extraction procedures, whatever.
 - Be sure to check FileExtension (defaults to "txt")

After running, adjust the job with schedules, notifications, and so forth.


--  Results
SELECT * from DBASTAT..Datasources
  where datasourceid = yyy
SELECT * from DBASTAT.dbo.DatasourceProperties
  where datasourceid = yyy

--  Undo
SET NOCOUNT off
DELETE DBASTAT..StagingEventLoad
  where StagingEventID in (select StagingEventID from DBASTAT..StagingEvent where datasourceid = yyy)
DELETE DBASTAT..StagingEvent
  where datasourceid = yyy
DELETE DBASTAT.dbo.DatasourceProperties
  where datasourceid = yyy
DELETE DBASTAT..Datasources
  where datasourceid = yyy
PRINT '...and delete the job, too'

*/

USE tempdb

SET NOCOUNT on

DECLARE
  @DatafeedCode          varchar(20)
 ,@DatafeedDescript      varchar(500)
 ,@JobName               varchar(128)
 ,@JobDescription        varchar(500)
 ,@AuditLogFolder        varchar(200)
 ,@AuditLogRetainDays    int
 ,@Datafeed_Database     varchar(128)
 ,@ProcessData_Command   varchar(4000)
 ,@ProcessData_Database  varchar(128)
 ,@DataLoadingProcedure  varchar(128)
 ,@SSISLoadPackage       varchar(200)
 ,@JobFailureCode        tinyint
 ,@Datasources_ID        int
 ,@Datasources_Path      varchar(200)


--  Common Settings  ----------------------------------------------------------

--  Common Parameters
SET @DatafeedCode         = 'xxx'
  --  The datafeed code assigned to this datafeed

SET @Datasources_Path     = 'X:\Datafeeds\xxx'
  --  The staging folder where files are copied to/loaded from 


--  DataStaging Settings  -----------------------------------------------------

SET @Datasources_ID       = yyy
  --  ID used in DBASTAT.Datasources to identify the data source

SET @JobName              = 'DataStaging: xxx Market data'
  --  The name to assign the job.

SET @JobDescription       = 'xxx data loading routine'
  --  Job description

SET @ProcessData_Database = 'DBASTAT'
  --  The databases to start the "process data command" in (probably DBASTAT)

SET @DataLoadingProcedure = 'LoadMarketData'
  --  The procecure called from DBASTAT that actually loads the data

SET @SSISLoadPackage = 'Dataloads\Market\Load_xxx.dtsx'
  --  Path (relative to Environment.SSISPackages) and name of SSIS loading package

SET @AuditLogFolder       = 'C:\AuditLogs\DataLoadJobs'
  --  Where to store the audit logs for this job

SET @AuditLogRetainDays   = 7
  --  How long to keep the audit jobs; set as appropriate to the frequency of the jobs


--  Configure DBASTAT  ------------------------------------------------------------------

IF not exists (SELECT 'x' from DBASTAT..Datasources where DatasourceID = yyy)
 BEGIN
    INSERT DBASTAT.dbo.DataSources
      (
         DatasourceID
        ,Category
        ,Description
        ,Path
       ,ProcessorName
        ,LoadedResultCD
        ,FileExtension
        ,DeleteFileOnLoad
        ,FilenameDecodeRuleset
      )
     values
      (
         yyy
        ,'Market'
        ,'xxx Market data'
        ,@Datasources_Path
       ,@DataLoadingProcedure
        ,5                     --  Code for properly loaded/completed Market data file
        ,'txt'                 --  File extension
        ,1                     --  1 for delete file on load, 0 for Malthusian excesses
        ,5                     --  Market Data identifier
      )

    --  Configure SSIS info
    INSERT DBASTAT.dbo.DatasourceProperties (DatasourceID, Property, FileTypeID, Value)
     values (@Datasources_ID, 'SSIS Load Package', 5, @SSISLoadPackage)  --  "Market" file type
 END

-- Create job  --------------------------------------------------------------------------


BEGIN TRANSACTION            

DECLARE
  @JobID         binary(16)  
 ,@ReturnCode    int    
 ,@Command       varchar(1000)
 ,@AuditLogFile  varchar(500)

SET @ReturnCode = 0
SET @AuditLogFile = @AuditLogFolder + '\' + @DatafeedCode + '.log'


IF (SELECT COUNT(*) FROM msdb.dbo.syscategories WHERE name = N'DataStaging') < 1 
    EXECUTE msdb.dbo.sp_add_category @name = N'DataStaging'

IF (SELECT COUNT(*) FROM msdb.dbo.sysjobs WHERE name = @JobName) > 0 
    PRINT N'The job "' + @JobName + '" already exists so will not be replaced.'
ELSE
BEGIN  ------------------------------------------------------------------------


-- Add the job
EXECUTE @ReturnCode = msdb.dbo.sp_add_job
  @job_id = @JobID OUTPUT 
 ,@job_name = @JobName
 ,@owner_login_name = N'sa'
 ,@description = @JobDescription
 ,@category_name = N'DataStaging'
 ,@enabled = 0
 ,@notify_level_email = 0
 ,@notify_level_page = 0
 ,@notify_level_netsend = 0
 ,@notify_level_eventlog = 2
 ,@delete_level= 0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 


--  Configure the main call
SET @ProcessData_Command  = 'DECLARE @Error int
EXECUTE @Error = dbo.ETL_ProcessDatasource ' + cast(@Datasources_ID as varchar(3)) + '

IF @Error <> 0
    RAISERROR(''Error loading ' + @DatafeedCode + ' data'', 11, 1)'


-- Add the job steps
EXECUTE @ReturnCode = msdb.dbo.sp_add_jobstep
  @job_id = @JobID
 ,@step_id = 1
 ,@step_name = N'Stage the data'
 ,@command = @ProcessData_Command
 ,@database_name = @ProcessData_Database
 ,@server = N''
 ,@database_user_name = N''
 ,@subsystem = N'TSQL'
 ,@cmdexec_success_code = 0
 ,@flags = 0
 ,@retry_attempts = 0
 ,@retry_interval = 1
 ,@output_file_name = @AuditLogFile
 ,@on_success_step_id = 0
 ,@on_success_action = 3
 ,@on_fail_step_id = 3
 ,@on_fail_action = 4
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 


SET @Command = '--  Timestamp audit log files, delete sufficiently aged ones
EXECUTE sp_FileTimestampAndCycle ''' + @AuditLogFolder + ''', ''' + @DatafeedCode + '.log'', '
 + cast(@AuditLogRetainDays as varchar(4)) + '
'

EXECUTE @ReturnCode = msdb.dbo.sp_add_jobstep
  @job_id = @JobID
 ,@step_id = 2
 ,@step_name = N'Timestamp and cycle audit logs (on success)'
 ,@command = @Command
 ,@database_name = N'master'
 ,@server = N''
 ,@database_user_name = N''
 ,@subsystem = N'TSQL'
 ,@cmdexec_success_code = 0
 ,@flags = 0
 ,@retry_attempts = 0
 ,@retry_interval = 1
 ,@output_file_name = N''
 ,@on_success_step_id = 0
 ,@on_success_action = 1
 ,@on_fail_step_id = 0
 ,@on_fail_action = 2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

EXECUTE @ReturnCode = msdb.dbo.sp_add_jobstep
  @job_id = @JobID
 ,@step_id = 3
 ,@step_name = N'Timestamp and cycle audit logs (on failure)'
 ,@command = @Command
 ,@database_name = N'master'
 ,@server = N''
 ,@database_user_name = N''
 ,@subsystem = N'TSQL'
 ,@cmdexec_success_code = 0
 ,@flags = 0
 ,@retry_attempts = 0
 ,@retry_interval = 1
 ,@output_file_name = N''
 ,@on_success_step_id = 0
 ,@on_success_action = 2
 ,@on_fail_step_id = 0
 ,@on_fail_action = 2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 


--  Set the first job step
EXECUTE @ReturnCode = msdb.dbo.sp_update_job
  @job_id = @JobID
 ,@start_step_id = 1 
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 


-- Add the Target Servers
EXECUTE @ReturnCode = msdb.dbo.sp_add_jobserver
  @job_id = @JobID
 ,@server_name = N'(local)' 
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 


END  --------------------------------------------------------------------------

COMMIT TRANSACTION
GOTO EndSave

QuitWithRollback:
IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION

EndSave:
