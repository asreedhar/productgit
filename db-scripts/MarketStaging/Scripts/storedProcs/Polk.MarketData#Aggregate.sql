IF objectproperty(object_id('Polk.MarketData#Aggregate'), 'isProcedure') = 1
    DROP PROCEDURE Polk.MarketData#Aggregate

GO

/******************************************************************************
**
**  Procedure: Polk.MarketData#Aggregate
**  Description: Clears and Aggregates all Market Data
**
**  Return values:  none
** 
**  Input parameters:  None
**
**  Output parameters:  none
**
**  Rows returned: none

*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author	Description:
**  ----------  ------	-------------------------------------------
**  2009.04.07  ffoiii	Procedure created.
**  2009.05.05  ffoiii	Improved delineation between main/sub table flags
**  2011.03.09  wgh     Removed sub-tables.  Deprecated.
**  2012.07.24  wgh	Limit to two years of data, remove SubSegment
*******************************************************************************/
CREATE PROCEDURE Polk.MarketData#Aggregate
AS

SET NOCOUNT ON

DECLARE @sErrMsg VARCHAR(255),
	@sprocname sysname,
	@bTrue bit,
	@bFalse bit
SELECT	@sProcName = object_name(@@procid),
	@bTrue = 1,
	@bFalse = 0,
	@sErrMsg = ''

TRUNCATE TABLE Market.dbo.Market_Summary

INSERT 
INTO	Market.dbo.Market_Summary (DealerNumber, ReportDt, Zip, Yr, MMGId, SegmentId, Cnt)
SELECT 	DealerId DealerNumber,
	CONVERT(DATETIME, RIGHT(RegistrationYearMonth, 2) + '/1/' + LEFT(RegistrationYearMonth,4)) as ReportYR,
	RegistrationZip Zip,
	ModelYear YR,
	MakeModelGroupId MMGID,
	SegmentId,
	SUM(UnitCount) as Cnt
FROM 	Polk.DMVRegistration 
WHERE	RegistrationYearMonth >= YEAR(DATEADD(MM, -24, DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0))) * 100 + MONTH(DATEADD(MM, -24, DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0))) 	
GROUP 
BY 	DealerId,
	CONVERT(DATETIME, RIGHT(RegistrationYearMonth, 2) + '/1/' + LEFT(RegistrationYearMonth,4)),
	RegistrationZip,
	ModelYear,
	MakeModelGroupID,
	SegmentID

RETURN 0

Failed:
IF (@sErrMsg = '')
	BEGIN
		SET @sErrMsg = 'Unspecified Error.'
	END
EXEC sp_LogEvent 'E', @sProcName, @sErrMsg, null
RETURN -2