
--************************************************
--JDPowerUsedRetailMarketSales_Staging
--TradeACV = TradeInActualCashValue
--************************************************
ALTER TABLE JDPower.JDPowerUsedRetailMarketSales_Staging
	ADD TRADE_ACV DECIMAL(9,2)
ALTER TABLE JDPower.JDPowerUsedRetailMarketSales_Staging
	ADD TRADE_SQUISHVIN VARCHAR(10)
ALTER TABLE JDPower.JDPowerUsedRetailMarketSales_Staging
	ADD TRADE_FUELTYPE VARCHAR(20)
ALTER TABLE JDPower.JDPowerUsedRetailMarketSales_Staging
	ADD TRADE_CYLINDERS VARCHAR(20)
ALTER TABLE JDPower.JDPowerUsedRetailMarketSales_Staging
	ADD TRADE_DRIVETRAIN VARCHAR(20)
ALTER TABLE JDPower.JDPowerUsedRetailMarketSales_Staging
	ADD TRADE_DISPLACEMENT VARCHAR(20)
ALTER TABLE JDPower.JDPowerUsedRetailMarketSales_Staging
	ADD TRADE_ODOMETER INT
ALTER TABLE JDPower.JDPowerUsedRetailMarketSales_Staging
	ADD TRADE_COLOR VARCHAR(20)
--************************************************

GO

CREATE TABLE [JDPower].[ImportFailure](
	ImportFailureId INT IDENTITY(1,1) CONSTRAINT PK_ImportFailure PRIMARY KEY CLUSTERED,
	SourceFile VARCHAR(200),
	ImportDate DATETIME,
	[MONTH_YEAR] [smalldatetime] NULL,
	[SQUISHVIN] [varchar](10) NULL,
	[RETAILER_VERSION_PK] [int] NULL,
	[MARKET] [varchar](240) NULL,
	[DMA] [varchar](200) NULL,
	[CUSTZIP] [varchar](5) NULL,
	[SAMEFRANCHISE] [varchar](2) NULL,
	[SEGMENTATION] [varchar](30) NULL,
	[MAKE] [varchar](25) NULL,
	[MODEL] [varchar](30) NULL,
	[MODELYEAR] [int] NULL,
	[TRIMLEVEL] [varchar](50) NULL,
	[BODYTYPE] [varchar](20) NULL,
	[FUELTYPE] [varchar](20) NULL,
	[CYLINDERS] [varchar](20) NULL,
	[DISPLACEMENT] [varchar](20) NULL,
	[DRIVETYPE] [varchar](10) NULL,
	[COLOR] [varchar](20) NULL,
	[TRANSMISSION] [varchar](20) NULL,
	[DOORS] [varchar](15) NULL,
	[VEH_PRICE] [decimal](9, 2) NULL,
	[VEH_COST] [decimal](9, 2) NULL,
	[VEH_GROSS_PROFIT] [decimal](9, 2) NULL,
	[FINANCE_LEASE_RESERVE] [decimal](9, 2) NULL,
	[SVC_CONTRACT_INCOME] [decimal](9, 2) NULL,
	[LIFE_INS_INCOME] [decimal](9, 2) NULL,
	[DISABILITY_INS_INCOME] [decimal](9, 2) NULL,
	[TOTAL_PIN_GROSS_PROFIT] [decimal](9, 2) NULL,
	[DAYS_TO_TURN] [smallint] NULL,
	[VEH_ODOMETER] [int] NULL,
	[CUST_AGE] [tinyint] NULL,
	[CUST_GENDER_NAME] [varchar](20) NULL,
	[TRADE_SQUISHVIN] [varchar](10) NULL,
	[TRADE_ACV] [decimal](9, 2) NULL,
	[TRADE_FUELTYPE] [varchar](20) NULL,
	[TRADE_CYLINDERS] [varchar](20) NULL,
	[TRADE_DRIVETRAIN] [varchar](20) NULL,
	[TRADE_DISPLACEMENT] [varchar](20) NULL,
	[TRADE_ODOMETER] [int] NULL,
	[TRADE_COLOR] [varchar](20) NULL,
	[ACURA] [tinyint] NOT NULL,
	[Aston Martin] [tinyint] NOT NULL,
	[AUDI] [tinyint] NOT NULL,
	[BENTLEY] [tinyint] NOT NULL,
	[BMW] [tinyint] NOT NULL,
	[BUICK] [tinyint] NOT NULL,
	[CADILLAC] [tinyint] NOT NULL,
	[CHEVROLET] [tinyint] NOT NULL,
	[CHRYSLER] [tinyint] NOT NULL,
	[DODGE] [tinyint] NOT NULL,
	[FERRARI] [tinyint] NOT NULL,
	[FORD] [tinyint] NOT NULL,
	[GMC] [tinyint] NOT NULL,
	[HONDA] [tinyint] NOT NULL,
	[HUMMER] [tinyint] NOT NULL,
	[HYUNDAI] [tinyint] NOT NULL,
	[INFINITI] [tinyint] NOT NULL,
	[ISUZU] [tinyint] NOT NULL,
	[JAGUAR] [tinyint] NOT NULL,
	[JEEP] [tinyint] NOT NULL,
	[KIA] [tinyint] NOT NULL,
	[LAMBORGHINI] [tinyint] NOT NULL,
	[Land Rover] [tinyint] NOT NULL,
	[LEXUS] [tinyint] NOT NULL,
	[LINCOLN] [tinyint] NOT NULL,
	[LOTUS] [tinyint] NOT NULL,
	[MAZDA] [tinyint] NOT NULL,
	[Mercedes-Benz] [tinyint] NOT NULL,
	[MERCURY] [tinyint] NOT NULL,
	[MINI] [tinyint] NOT NULL,
	[MITSUBISHI] [tinyint] NOT NULL,
	[NISSAN] [tinyint] NOT NULL,
	[PONTIAC] [tinyint] NOT NULL,
	[PORSCHE] [tinyint] NOT NULL,
	[Rolls-Royce] [tinyint] NOT NULL,
	[SAAB] [tinyint] NOT NULL,
	[SATURN] [tinyint] NOT NULL,
	[SCION] [tinyint] NOT NULL,
	[SUBARU] [tinyint] NOT NULL,
	[SUZUKI] [tinyint] NOT NULL,
	[TOYOTA] [tinyint] NOT NULL,
	[VOLKSWAGEN] [tinyint] NOT NULL,
	[VOLVO] [tinyint] NOT NULL,
	[ErrorDescription] varchar(100),
	[ErrorColumn] varchar(100),
	[ErrorCode] varchar(100)
) ON [PRIMARY]

