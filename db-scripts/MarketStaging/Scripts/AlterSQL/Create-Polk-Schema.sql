SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='Polk')
EXEC('CREATE SCHEMA Polk')
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Polk].[DMVRegistration]') AND type in (N'U'))
begin
	CREATE TABLE [Polk].[DMVRegistration](
		[Load_Id] [int] NOT NULL,
		[VIN] [varchar](17) NOT NULL,
		[RegistrationZip] [char](5) NOT NULL,
		[RegistrationState] [char](2) NOT NULL,
		[RegistrationCounty] [varchar](50) NOT NULL,
		[RegistrationYearMonth] [int] NOT NULL,
		[Make] [varchar](50) NULL,
		[Model] [varchar](50) NULL,
		[Segment] [varchar](50) NULL,
		[BodyStyle] [varchar](50) NULL,
		[DriveType] [varchar](50) NULL,
		[EngineSize] [varchar](50) NULL,
		[DealerName] [varchar](50) NOT NULL,
		[DealerCity] [varchar](50) NOT NULL,
		[DealerState] [varchar](20) NOT NULL,
		[DealerZip] [varchar](10) NOT NULL,
		[DealerCounty] [varchar](50) NOT NULL,
		[UnitCount] [tinyint] NOT NULL,
		[DealerId] [int] NOT NULL,
		[VehicleCatalogId] [int] NOT NULL,
		[SegmentId] [int] NOT NULL,
		[Modelyear] [int] NOT NULL,
		[MakeModelGroupId] [int] NOT NULL,
		[SquishVin] [varchar](10) NOT NULL
	)

	CREATE CLUSTERED INDEX [pk_PolkDMVRegistration] ON [Polk].[DMVRegistration] ([DealerId], [RegistrationYearMonth], [RegistrationZip], [Modelyear], [MakeModelGroupId])
end
GO



IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Polk].[DMVRegistration_Exception]') AND type in (N'U'))
CREATE TABLE [Polk].[DMVRegistration_Exception](
	[Load_Id] [int] NOT NULL,
	[VIN] [varchar](17) NULL,
	[RegistrationZip] [varchar](5) NULL,
	[RegistrationState] [varchar](20) NULL,
	[RegistrationCounty] [varchar](50) NULL,
	[RegistrationYearMonth] [varchar](20) NULL,
	[Make] [varchar](50) NULL,
	[Model] [varchar](50) NULL,
	[Segment] [varchar](50) NULL,
	[BodyStyle] [varchar](50) NULL,
	[DriveType] [varchar](50) NULL,
	[EngineSize] [varchar](50) NULL,
	[DealerName] [varchar](50) NULL,
	[DealerCity] [varchar](50) NULL,
	[DealerState] [varchar](20) NULL,
	[DealerZip] [varchar](10) NULL,
	[DealerCounty] [varchar](50) NULL,
	[UnitCount] [int] NULL,
	[DealerId] [int] NULL,
	[VehicleCatalogId] [int] NULL,
	[SegmentId] [int] NULL,
	[SubSegmentId] [int] NULL,
	[Modelyear] [int] NULL,
	[MakeModelGroupId] [int] NULL,
	[SquishVin] [varchar](10) NULL
)
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Polk].[DMVRegistration_Staging]') AND type in (N'U'))
CREATE TABLE [Polk].[DMVRegistration_Staging](
	[Load_Id] [int] NOT NULL,
	[VIN] [varchar](17) NULL,
	[RegistrationZip] [varchar](5) NULL,
	[RegistrationState] [varchar](20) NULL,
	[RegistrationCounty] [varchar](50) NULL,
	[RegistrationYearMonth] [varchar](20) NULL,
	[Make] [varchar](50) NULL,
	[Model] [varchar](50) NULL,
	[Segment] [varchar](50) NULL,
	[BodyStyle] [varchar](50) NULL,
	[DriveType] [varchar](50) NULL,
	[EngineSize] [varchar](50) NULL,
	[DealerName] [varchar](50) NULL,
	[DealerCity] [varchar](50) NULL,
	[DealerState] [varchar](20) NULL,
	[DealerZip] [varchar](10) NULL,
	[DealerCounty] [varchar](50) NULL,
	[UnitCount] [int] NULL,
	[DealerId] [int] NULL,
	[VehicleCatalogId] [int] NULL,
	[SegmentId] [int] NULL,
	[SubSegmentId] [int] NULL,
	[Modelyear] [int] NULL,
	[MakeModelGroupId] [int] NULL,
	[SquishVin] [varchar](10) NULL
)
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
SET ANSI_PADDING OFF
GO
