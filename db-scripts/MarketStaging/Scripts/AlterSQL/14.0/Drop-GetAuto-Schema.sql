IF OBJECTPROPERTY(OBJECT_ID('GetAuto.Image#Extract'), 'isProcedure') = 1
	DROP PROCEDURE GetAuto.Image#Extract
GO

IF OBJECTPROPERTY(OBJECT_ID('GetAuto.LotData'), 'isTable') = 1
	DROP TABLE GetAuto.LotData
GO

IF OBJECTPROPERTY(OBJECT_ID('GetAuto.Vehicle'), 'isTable') = 1
	DROP TABLE GetAuto.Vehicle
GO

IF OBJECTPROPERTY(OBJECT_ID('GetAuto.SystemWarranties'), 'isTable') = 1
	DROP TABLE GetAuto.SystemWarranties
GO

IF OBJECTPROPERTY(OBJECT_ID('GetAuto.ImageURL#Generate'), 'IsTableFunction') = 1
	DROP FUNCTION GetAuto.ImageURL#Generate
GO

IF EXISTS (SELECT 1 FROM sys.schemas S WHERE name = 'GetAuto')
	DROP SCHEMA GetAuto
GO