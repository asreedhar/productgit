

TRUNCATE TABLE Listing.Vehicle_Interface
GO
ALTER TABLE Listing.Vehicle_Interface ADD 
	SetKeyHash	BINARY(16) NULL,
	UniqueKeyHash	BINARY(16) NULL
