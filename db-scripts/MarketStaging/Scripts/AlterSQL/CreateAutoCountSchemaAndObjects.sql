/*

Recreate the (used) Autocount dataload tables from the Market database
Populate the relevant ones from the Market database on the same server:
  AutoCount.Datafeeds
  AutoCount.Market_AutoCount_Detail
  AutoCount.Market_CrossSell_Detail
  AutoCount.Market_AutoCount_Exceptions
There's a lot of data to copy, so this will take a while


--  Undo
DROP TABLE AutoCount.Market_CrossSell_Detail
DROP TABLE AutoCount.Market_AutoCount_Detail
DROP TABLE AutoCount.Market_AutoCount_Exceptions
DROP TABLE AutoCount.Market_AutoCount_Staging_1
DROP TABLE AutoCount.Market_AutoCount
DROP TABLE AutoCount.Datafeeds

DROP SCHEMA AutoCount

*/

CREATE SCHEMA AutoCount


CREATE TABLE  AutoCount.Datafeeds
  (
	 FileID   int  IDENTITY (15, 1) NOT NULL ,
	 FileName   varchar  (100) NOT NULL ,
	 Start_Date_ID   int  NOT NULL CONSTRAINT  DF_Datafeeds__Start_Date_ID  DEFAULT (0),
	 End_Date_ID   int  NOT NULL CONSTRAINT  DF_Datafeeds__End_Date_ID  DEFAULT (0),
	 Geo_ID   int  NOT NULL CONSTRAINT  DF_Datafeeds__Geo_ID  DEFAULT (0),
	 File_Type_CD   tinyint  NOT NULL CONSTRAINT  DF_Datafeeds__File_Type_CD  DEFAULT (1),
	 Status_CD   tinyint  NOT NULL CONSTRAINT  DF_Datafeeds__Status_CD  DEFAULT (0),
	 Source_CD   tinyint  NOT NULL CONSTRAINT  DF_Datafeeds__Source_CD  DEFAULT (1),
	 TimeStamp   datetime  NOT NULL CONSTRAINT  DF_Datafeeds__TimeStamp  DEFAULT (getdate()),
	CONSTRAINT  PK_Datafeeds  PRIMARY KEY  CLUSTERED 
	(FileID)
)


CREATE TABLE  AutoCount.Market_AutoCount
  (
	 TITLENUMBER   varchar  (75) NULL ,
	 TITLECOUNTY   varchar  (75) NULL ,
	 TITLESTATE   varchar  (75) NULL ,
	 REPORTINGPERIOD   varchar  (75) NULL ,
	 VIN   varchar  (75) NULL ,
	 VEHICLEMAKE   varchar  (75) NULL ,
	 VEHICLEMODEL   varchar  (75) NULL ,
	 VEHICLESEGMENT   varchar  (75) NULL ,
	 VEHICLEBODY   varchar  (75) NULL ,
	 VEHICLEYEAR   varchar  (75) NULL ,
	 ODOMETERREADING   float  NULL ,
	 NEWUSEDINDICATOR   varchar  (75) NULL ,
	 FLEETINDICATOR   varchar  (75) NULL ,
	 LEASEINDICATOR   varchar  (75) NULL ,
	 DEALERNUMBER   varchar  (75) NULL ,
	 DEALERNAME   varchar  (75) NULL ,
	 DEALERADDRESS   varchar  (75) NULL ,
	 DEALERCITY   varchar  (75) NULL ,
	 DEALERSTATE   varchar  (75) NULL ,
	 DEALERZIPCODE   varchar  (75) NULL ,
	 DEALERTYPE   varchar  (75) NULL ,
	 DEALERCOUNTY   varchar  (75) NULL ,
	 OWNERNAME   varchar  (75) NULL ,
	 OWNERADDRESS   varchar  (75) NULL ,
	 OWNERCITY   varchar  (75) NULL ,
	 OWNERSTATE   varchar  (75) NULL ,
	 OWNERZIPCODE   varchar  (75) NULL ,
	 OWNERCOUNTY   varchar  (75) NULL ,
	 LIENHOLDERNAMECORRECTED   varchar  (75) NULL ,
	 LIENHOLDERADDRESS   varchar  (75) NULL ,
	 LIENHOLDERCITY   varchar  (75) NULL ,
	 LIENHOLDERSTATE   varchar  (75) NULL ,
	 LIENHOLDERZIPCODE   varchar  (75) NULL ,
	 FILEID   int  NULL 
)


CREATE TABLE  AutoCount.Market_AutoCount_Staging_1
  (
	 VIN   varchar  (17) NULL ,
	 Squish_vin   varchar  (17) NULL ,
	 SegmentID   tinyint  NULL ,
	 SubSegmentID   tinyint  NULL ,
	 YR   int  NULL ,
	 Make   varchar  (50) NULL ,
	 Model   varchar  (50) NULL ,
	 MMGID   int  NULL ,
	 Zip   varchar  (5) NULL ,
	 Odometer   int  NULL ,
	 OrigYear   varchar  (4) NULL ,
	 OrigMake   varchar  (20) NULL ,
	 OrigModel   varchar  (30) NULL ,
	 DealerNumber   int  NOT NULL ,
	 DealerCounty   varchar  (50) NULL ,
	 TitleState   varchar  (50) NULL ,
	 ReportingPeriod   varchar  (6) NULL ,
	 NewUsedIndicator   varchar  (1) NULL ,
	 FileID   int  NOT NULL ,
	CONSTRAINT  FK_Market_AutoCount_Staging1__Datafeeds  FOREIGN KEY 
	(FileID) REFERENCES  AutoCount.Datafeeds  (FileID)
)


CREATE TABLE AutoCount.Market_AutoCount_Detail (
	VIN varchar (17) NOT NULL ,
	Squish_vin varchar (17) NOT NULL ,
	SegmentID tinyint NULL ,
	SubSegmentID tinyint NULL ,
	YR int NOT NULL ,
	Make varchar (50) NOT NULL ,
	Model varchar (50) NOT NULL ,
	MMGID int NOT NULL ,
	Zip varchar (5) NOT NULL ,
	Odometer int NULL ,
	OrigYear varchar (4) NULL ,
	OrigMake varchar (20) NULL ,
	OrigModel varchar (30) NULL ,
	DealerNumber int NOT NULL ,
	DealerCounty varchar (50) NULL ,
	TitleState varchar (50) NULL ,
	ReportingPeriod varchar (6) NOT NULL ,
	NewUsedIndicator varchar (1) NULL ,
	FileID int NOT NULL ,
	CONSTRAINT FK_Market_AutoCount_Detail__Datafeeds FOREIGN KEY 
	(FileID) REFERENCES AutoCount.Datafeeds (FileID),
)


CREATE TABLE  AutoCount.Market_AutoCount_Exceptions
  (
	 VIN   varchar  (17) NULL ,
	 Squish_vin   varchar  (17) NULL ,
	 YR   int  NULL ,
	 Make   varchar  (50) NULL ,
	 Model   varchar  (50) NULL ,
	 MMGID   int  NULL ,
	 Zip   varchar  (5) NULL ,
	 Odometer   int  NULL ,
	 OrigYear   varchar  (4) NULL ,
	 OrigMake   varchar  (20) NULL ,
	 OrigModel   varchar  (30) NULL ,
	 DealerNumber   int  NULL ,
	 DealerCounty   varchar  (50) NULL ,
	 TitleState   varchar  (50) NULL ,
	 ReportingPeriod   varchar  (6) NULL ,
	 NewUsedIndicator   varchar  (1) NULL ,
	 FileID   int  NOT NULL ,
	 ExceptionDate   datetime  NOT NULL ,
	 ExceptionComment   varchar  (100) NULL ,
	CONSTRAINT  FK_Market_AutoCount_Exceptions__Datafeeds  FOREIGN KEY 
	(FileID) REFERENCES  AutoCount.Datafeeds  (FileID)
)


CREATE TABLE AutoCount.Market_CrossSell_Detail (
	VIN varchar (17) NOT NULL ,
	Squish_vin varchar (17) NOT NULL ,
	SegmentID tinyint NULL ,
	SubSegmentID tinyint NULL ,
	YR int NOT NULL ,
	Make varchar (20) NOT NULL ,
	Model varchar (20) NOT NULL ,
	MMGID int NOT NULL ,
	Zip varchar (5) NOT NULL ,
	Odometer varchar (10) NULL ,
	OrigYear varchar (4) NULL ,
	OrigMake varchar (20) NULL ,
	OrigModel varchar (30) NULL ,
	DealerNumber int NOT NULL ,
	MON varchar (6) NOT NULL ,
	S_T varchar (1) NULL ,
	Sprice varchar (6) NULL ,
	FileID int NOT NULL ,
	CONSTRAINT FK_Market_CrossSell_Detail_Datafeeds FOREIGN KEY 
	(FileID) REFERENCES AutoCount.Datafeeds (FileID),
)


GO
