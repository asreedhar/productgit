
CREATE SCHEMA Listing
GO

CREATE TABLE Listing.VehicleOption_Interface	(
		ProviderID tinyint NOT NULL,
		SourceRowID int NOT NULL,
		VIN varchar (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		OptionDescription varchar (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		VehicleID int NULL,
		OptionID int NULL
	) 
GO
ALTER TABLE Listing.VehicleOption_Interface ADD CONSTRAINT PK_ListingVehicleOption_Interface PRIMARY KEY CLUSTERED  (ProviderID, SourceRowID, VIN, OptionDescription) WITH (FILLFACTOR=90)
GO


CREATE TABLE [Listing].[Vehicle_Interface]
(
[SourceRowID] [int] NOT NULL,
[Source] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerName] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerAddress1] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerAddress2] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerCity] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerState] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerZipCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerURL] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerPhoneNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerID] [int] NULL,
[VIN] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Make] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModelYear] [smallint] NULL,
[Trim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExteriorColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DriveTrain] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Engine] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Transmission] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FuelType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListPrice] [int] NULL,
[ListingDate] [smalldatetime] NULL,
[Mileage] [int] NULL,
[MSRP] [int] NULL,
[StockNumber] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Certified] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProviderID] [tinyint] NOT NULL,
[DeltaFlag] [tinyint] NOT NULL,
[SourceID] [tinyint] NULL,
[VehicleID] [int] NULL,
[Options] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerDescription] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DealerType] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StockType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerEMail] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[_Categorization_RowID] [int] NOT NULL IDENTITY(1, 1),
[_Categorization_SquishVin] AS (CONVERT([char](9),substring([VIN],(1),(8))+substring([VIN],(10),(1)),(0))) PERSISTED
)
GO
ALTER TABLE [Listing].[Vehicle_Interface] ADD CONSTRAINT [PK_ListingVehicle_Interface] PRIMARY KEY CLUSTERED  ([ProviderID], [SourceRowID], [VIN]) WITH (FILLFACTOR=90)
GO
CREATE NONCLUSTERED INDEX [IX_Listing_Vehicle_Interface__VehicleIDDeltaFlag] ON [Listing].[Vehicle_Interface] ([VehicleID], [DeltaFlag]) WITH (FILLFACTOR=90)
GO
CREATE NONCLUSTERED INDEX [IX_Listing_Vehicle_Interface__SquishVin] ON [Listing].[Vehicle_Interface] ([_Categorization_SquishVin]) INCLUDE ([VIN]) WITH (FILLFACTOR=90)
GO