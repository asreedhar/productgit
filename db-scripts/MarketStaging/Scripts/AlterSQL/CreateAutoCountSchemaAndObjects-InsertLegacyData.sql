
--  Copy over data from original tables
SET Identity_Insert AutoCount.Datafeeds on

INSERT AutoCount.Datafeeds
  (
    FileID
   ,FileName
   ,Start_Date_ID
   ,End_Date_ID
   ,Geo_ID
   ,File_Type_CD
   ,Status_CD
   ,Source_CD
   ,TimeStamp
  )
 select *
  from Market.dbo.Datafeeds

SET Identity_Insert AutoCount.Datafeeds off


INSERT AutoCount.Market_AutoCount_Exceptions
 select * from Market.dbo.Market_AutoCount_Exceptions


INSERT AutoCount.Market_AutoCount_Detail
 select *
  from Market.dbo.Market_AutoCount_Detail


INSERT AutoCount.Market_CrossSell_Detail
 select *
  from Market.dbo.Market_CrossSell_Detail

GO

--  Odds are the data copies above bloated the log file; this will
--  shrink it back down to the initial size
dbcc shrinkfile(2, 2000)
