
----------------------------------------------------------------------------------------------------------------
--	CREATE PK'S AND/OR INDEXES TO ASSIST LOOKUP TRANSFORMATIONS IN THE SSIS LOADER PACKAGES (SPECIFICALLY,
--	WE NEED TO COMPARE THE CURRENT LISTING VEHICLES VS. THE STAGING TO DETERMINE "DELETES"
----------------------------------------------------------------------------------------------------------------


ALTER TABLE GetAuto.Vehicle ALTER COLUMN cvin CHAR(17) NOT NULL
ALTER TABLE GetAuto.Vehicle ADD CONSTRAINT PK_GetAutoVehicle PRIMARY KEY CLUSTERED (RowID)

CREATE INDEX IX_GetAutoVehicle__cvin ON GetAuto.Vehicle(cvin)
GO 

----------------------------------------------------------------------------------------------------------------
--	NOTE: CAR_ID COULD PROBABLY BE A PK BUT WE NEED TO VERIFY W/AutoTrader.  DON'T WANT TO SET AND
--	AND THEN HAVE A LOAD BREAK DUE TO DUPES IN THE FEED.  FOR NOW, THIS INDEX SHOULD COVER THE LOOKUP
--	WE NEED.
----------------------------------------------------------------------------------------------------------------
CREATE INDEX IX_AutoTraderVehicleStaging__CAR_ID ON AutoTrader.VehicleStaging(CAR_ID)
GO
