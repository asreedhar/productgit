
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'GetAuto.LotData') AND type in (N'U'))

DROP TABLE GetAuto.LotData

CREATE TABLE GetAuto.LotData(
	DealerID int  NOT NULL,
	DealerName varchar(100)  NULL,
	Address1 varchar(100)  NULL,
	Address2 varchar(100)  NULL,
	City varchar(100)  NULL,
	State varchar(50)  NULL,
	Zip varchar(10)  NULL,
	Phone varchar(255)  NULL,
	Fax varchar(255)  NULL,
	Email varchar(255)  NULL,
	DealerContact varchar(100)  NULL,
	DealerComments varchar(3500)  NULL,
	DealerWebsite varchar(255)  NULL,
	DealerBrand varchar(255)  NULL,
	DealerVideo varchar(255)  NULL,
	DealerAmenities varchar(400)  NULL,
	ShowRoomHours varchar(400)  NULL,
	ServiceHours varchar(400)  NULL,
	PartsHours varchar(400)  NULL,
	AdminHours varchar(400)  NULL,
	Reserved1 varchar(255)  NULL,
	Reserved2 varchar(255)  NULL,
	Reserved3 varchar(255)  NULL,
	Reserved4 varchar(255)  NULL,
	Reserved5 varchar(255)  NULL,
	Reserved6 varchar(255)  NULL
) 

 ALTER TABLE GetAuto.LotData ADD  CONSTRAINT [PK_GetAutoLotData] PRIMARY KEY CLUSTERED 
(
	DealerID ASC
)