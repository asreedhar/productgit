--------------------------------------------------------------------------------
--	EACH LISTING GETS A SCHEMA
--------------------------------------------------------------------------------

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='CarsDotCom')
EXEC('CREATE SCHEMA CarsDotCom')
GO

--------------------------------------------------------------------------------
--	VEHICLE LOAD STATUS
--
--	STAGING POST-PROCESS FLAGS INVALID VINS TO PREVENT LOADING TO 
--	Market.Listing SCHEMA
--------------------------------------------------------------------------------

IF NOT EXISTS (
	SELECT	*
	FROM	sys.objects
	WHERE	object_id = OBJECT_ID(N'[CarsDotCom].[VehicleLoadStatus]') AND type in (N'U'))
		 
CREATE TABLE CarsDotCom.VehicleLoadStatus (
		VehicleLoadStatusID	TINYINT NOT NULL CONSTRAINT PK_CarsDotComVehicleLoadStatus PRIMARY KEY,
		Description		VARCHAR(20)
		)
GO

IF ((SELECT COUNT(*) FROM CarsDotCom.VehicleLoadStatus) = 0) BEGIN

	INSERT INTO CarsDotCom.VehicleLoadStatus (VehicleLoadStatusID, Description)
		VALUES (0, 'Unprocessed')
	INSERT INTO CarsDotCom.VehicleLoadStatus (VehicleLoadStatusID, Description)
		VALUES (1, 'Ready')
	INSERT INTO CarsDotCom.VehicleLoadStatus (VehicleLoadStatusID, Description)
		VALUES (2, 'Duplicate VIN')
	INSERT INTO CarsDotCom.VehicleLoadStatus (VehicleLoadStatusID, Description)
		VALUES (3, 'Invalid VIN')

END
GO

IF NOT EXISTS (
	SELECT	*
	FROM	sys.objects
	WHERE	object_id = OBJECT_ID(N'[CarsDotCom].[Vehicle]') AND type in (N'U'))

CREATE TABLE CarsDotCom.Vehicle (

	VEHICLE_ID		INT NOT NULL,
	VIN			VARCHAR(20),
	STOCK_NUMBER		VARCHAR(100),
	STOCK_TYPE		VARCHAR(6),
	CPO_FLAG		VARCHAR(6),
	PRIVATE_PARTY_FLAG	VARCHAR(3),
	MAKE			VARCHAR(64),
	MODEL			VARCHAR(64),
	YEAR			SMALLINT,
	TRIM			VARCHAR(64),
	PRICE			DECIMAL(11,2),
	MILEAGE			INT,
	EXTERIOR_COLOR		VARCHAR(64),
	BODYSTYLE		VARCHAR(64),
	DOOR_COUNT		TINYINT,
	ENGINE			VARCHAR(100),
	TRANSMISSION		VARCHAR(64),
	DRIVETRAIN		VARCHAR(64),
	WHEELBASE		SMALLINT,
	SELLER_NAME		VARCHAR(255),
	CITY			VARCHAR(64),
	STATE			VARCHAR(5),
	ZIP_CODE		VARCHAR(10),
	SELLER_NOTES		VARCHAR(4000),
	LISTING_DATE		INT,			-- DATES SENT AS YYYYMMDD, STORE AS INT SINCE WE CAN LEVERAGE THAT VS. STANDARD TIME DIMENSION TABLE
	STANDARD_FEATURES	VARCHAR(4000),
	INTERIOR_COLOR		VARCHAR(64),
	MAINT_DATE		INT,
	PHOTO_COUNT		TINYINT,
	OPTIONS 		VARCHAR(2000) NULL,
	VehicleLoadStatusID TINYINT NULL CONSTRAINT DF_CarsDotCom_Vehicle__VehicleLoadStatusID DEFAULT (0),
	CONSTRAINT PK_CarsDotCom_Vehicle PRIMARY KEY CLUSTERED (VEHICLE_ID),
	CONSTRAINT FK_CarsDotCom_Vehicle__VehicleLoadStatus FOREIGN KEY (VehicleLoadStatusID) REFERENCES CarsDotCom.VehicleLoadStatus(VehicleLoadStatusID)
)
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[CarsDotCom].[Vehicle]') AND name = N'IX_CarsDotCom_Vehicle__VIN')
CREATE INDEX IX_CarsDotCom_Vehicle__VIN ON CarsDotCom.Vehicle(VIN)
GO
