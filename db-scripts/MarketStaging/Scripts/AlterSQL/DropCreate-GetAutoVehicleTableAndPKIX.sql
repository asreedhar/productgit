

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'GetAuto.Vehicle') AND type in (N'U'))
DROP TABLE GetAuto.Vehicle

CREATE TABLE GetAuto.Vehicle (
	RowID int IDENTITY(1,1) NOT NULL,
	DealerID int,
	VIN VARCHAR(17),
	StockNumber VARCHAR(25),
	Status VARCHAR(1),
	VehicleType int,
	Year int,
	Make VARCHAR(100),
	Model VARCHAR(100),
	Trim VARCHAR(100),
	Body VARCHAR(100),
	VehicleCategory VARCHAR(100),
	Mileage int,
	Transmission VARCHAR(100),
	EngineDisplacement VARCHAR(25),
	EngineSize VARCHAR(100),
	Induction VARCHAR(100),
	DriveTrain VARCHAR(10),
	FuelType  VARCHAR(100),
	FuelEconomyCity DECIMAL(12,5),
	FuelEconomyHighway DECIMAL(12,5),
	FuelEconomyCombined DECIMAL(12,5),
	Doors int,
	OemColorCode VARCHAR(10),
	ExternalColor VARCHAR(50),
	InternalColor VARCHAR(50),
	GenericColor VARCHAR(50),
	InternetPrice int,
	ComparisonPrice int,
	OemModelCode VARCHAR(50),
	HasWarranty VARCHAR(1),
	CertificationWarranty int,
	WarrantyMonth int,
	WarrantyMiles int,
	CertificationNumber VARCHAR(20),
	ServiceContract VARCHAR(1),
	InServiceDate DATETIME,
	CertificationDate DATETIME,
	DateManufactured DATETIME,
	DateCreated DATETIME,
	DateUpdated DATETIME,
	DateRemoved DATETIME,
	Photos VARCHAR(8),
	SuperSizePhotos VARCHAR(50),
	AddendumDetails VARCHAR(1800),
	DepartmentComments VARCHAR(255),
	VehicleComments VARCHAR(2000),
	Options VARCHAR(8000),
	PurchasePayment DECIMAL(12,2),
	PurchaseDownPayment DECIMAL(12,2),
	PurchaseTerm VARCHAR(50),
	PurchaseDisclosure VARCHAR(500),
	PurchaseRate INT,
	LeasePayment DECIMAL(12,2),
	LeaseDownPayment DECIMAL(12,2),
	LeaseTerm INT,
	LeaseDisclosure VARCHAR(500),
	LeaseRate INT,
	Reserved1  VARCHAR(255),
	Reserved2  VARCHAR(255),
	Reserved3 VARCHAR(255),
	Reserved4  VARCHAR(255),
	Reserved5  VARCHAR(255),
	Reserved6  VARCHAR(255)
	)
ALTER TABLE [GetAuto].[Vehicle] ADD  CONSTRAINT [PK_GetAutoVehicle] PRIMARY KEY CLUSTERED 
(
	[RowID] ASC
)

CREATE NONCLUSTERED INDEX [IX_GetAutoVehicle__Vin] ON [GetAuto].[Vehicle ] 
(
	[VIN] ASC
)