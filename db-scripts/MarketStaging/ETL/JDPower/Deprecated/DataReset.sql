--IMT TABLES
DELETE FROM IMT.dbo.DealerPreference_JDPowerSettings
DBCC CHECKIDENT('IMT.dbo.DealerPreference_JDPowerSettings', RESEED, 0)

DELETE FROM IMT.dbo.JDPower_DealerMarketArea
DBCC CHECKIDENT('IMT.dbo.JDPower_DealerMarketArea', RESEED, 0)

DELETE FROM IMT.dbo.JDPower_PowerRegion
DBCC CHECKIDENT('IMT.dbo.JDPower_PowerRegion', RESEED, 0)

--Market TABLES
TRUNCATE TABLE Market.JDPower.UsedRetailMarketSales_F

TRUNCATE TABLE Market.JDPower.UsedRetailMarketSales_F_LastMonthBackup

TRUNCATE TABLE Market.JDPower.FranchisesInFranchiseGrouping

DELETE FROM Market.JDPower.DealerMarketArea_D
DBCC CHECKIDENT('Market.JDPower.DealerMarketArea_D', RESEED, 0)

DELETE FROM Market.JDPower.PowerRegion_D
DBCC CHECKIDENT('Market.JDPower.PowerRegion_D', RESEED, 0)

DELETE FROM Market.JDPower.FranchiseGrouping_D
DBCC CHECKIDENT('Market.JDPower.FranchiseGrouping_D', RESEED, 0)

DELETE FROM Market.JDPower.Franchise_D
DBCC CHECKIDENT('Market.JDPower.Franchise_D', RESEED, 0)
