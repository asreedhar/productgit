For each of the package, a debug run will fail when the package property ProtectionLevel is set to DontSaveSensitive because
the IDE will ignore saving the Password field for the database connection string.

To debug in the IDE, either set to EncryptSensitiveWithUserKey and re-enter the password (don't forget to not check this in),
or in the package configuration, add the field Password in the database connection string.  Do not use the IDE to update the
configuration file, as it will remove the password field.