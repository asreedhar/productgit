/*

Create "track" schema and contained ojbects

*/

CREATE SCHEMA [track]
GO

CREATE TABLE [track].[User] (
	[Id]               INT IDENTITY(1,1) NOT NULL,
	[Domain]           INT NOT NULL,
	[Identifier]       INT NOT NULL,
	[SystemIdentifier] INT NULL,
	CONSTRAINT PK_User PRIMARY KEY NONCLUSTERED (
		[Id]
	),
	CONSTRAINT UK_User UNIQUE CLUSTERED (
		[Domain],
		[Identifier]
	)
)
GO

-- browser and its reference tables

CREATE TABLE [track].[ScreenResolution] (
	[Id]   INT IDENTITY(1,1) NOT NULL,
	[Name] VARCHAR(9) NOT NULL,
	CONSTRAINT PK_ScreenResolution PRIMARY KEY NONCLUSTERED (
		[Id]
	),
	CONSTRAINT UK_ScreenResolution UNIQUE CLUSTERED (
		[Name]
	)
)
GO

CREATE TABLE [track].[ColorDepth] (
	[Id]    INT IDENTITY(1,1) NOT NULL,
	[Name]  VARCHAR(255) NOT NULL
	CONSTRAINT PK_ColorDepth PRIMARY KEY NONCLUSTERED (
		[Id]
	),
	CONSTRAINT UK_ColorDepth UNIQUE CLUSTERED (
		[Name]
	)
)
GO

CREATE TABLE [track].[Language] (
	[Id]   INT IDENTITY(1,1) NOT NULL,
	[Name] VARCHAR(255) NOT NULL,
	CONSTRAINT PK_Language PRIMARY KEY NONCLUSTERED (
		[Id]
	),
	CONSTRAINT UK_Language UNIQUE CLUSTERED (
		[Name]
	)
)
GO

CREATE TABLE [track].[CharacterSet] (
	[Id]   INT IDENTITY(1,1) NOT NULL,
	[Name] VARCHAR(255) NOT NULL,
	CONSTRAINT PK_CharacterSet PRIMARY KEY NONCLUSTERED (
		[Id]
	),
	CONSTRAINT UK_CharacterSet UNIQUE CLUSTERED (
		[Name]
	)
)
GO

CREATE TABLE [track].[ShockwaveVersion] (
	[Id]   INT IDENTITY(1,1) NOT NULL,
	[Name] VARCHAR(255) NOT NULL,
	CONSTRAINT PK_ShockwaveVersion PRIMARY KEY NONCLUSTERED (
		[Id]
	),
	CONSTRAINT UK_ShockwaveVersion UNIQUE CLUSTERED (
		[Name]
	)
)
GO

CREATE TABLE [track].[Browser] (
	[Id]                 INT IDENTITY(1,1) NOT NULL,
	[ScreenResolutionId] INT NULL,
	[ColorDepthId]       INT NULL,
	[LanguageId]         INT NULL,
	[CharacterSetId]     INT NULL,
	[JavaEnabled]        BIT NULL,
	[ShockwaveVersionId] INT NULL,
	CONSTRAINT PK_Browser PRIMARY KEY (
		[Id]
	),
	CONSTRAINT FK_Browser_ScreenResolution FOREIGN KEY (
		[ScreenResolutionId]
	)
	REFERENCES [track].[ScreenResolution] (
		[Id]
	),
	CONSTRAINT FK_Browser_ColorDepth FOREIGN KEY (
		[ColorDepthId]
	)
	REFERENCES [track].[ColorDepth] (
		[Id]
	),
	CONSTRAINT FK_Browser_Language FOREIGN KEY (
		[LanguageId]
	)
	REFERENCES [track].[Language] (
		[Id]
	),
	CONSTRAINT FK_Browser_CharacterSet FOREIGN KEY (
		[CharacterSetId]
	)
	REFERENCES [track].[CharacterSet] (
		[Id]
	),
	CONSTRAINT FK_Browser_ShockwaveVersion FOREIGN KEY (
		[ShockwaveVersionId]
	)
	REFERENCES [track].[ShockwaveVersion] (
		[Id]
	)
)
GO

-- session entry and its reference tables

CREATE TABLE [track].[Host] (
	[Id]   INT IDENTITY(1,1) NOT NULL,
	[Name] VARCHAR(255) NOT NULL,
	CONSTRAINT PK_Host PRIMARY KEY NONCLUSTERED (
		[Id]
	),
	CONSTRAINT UK_Host UNIQUE CLUSTERED (
		[Name]
	)
)
GO

CREATE TABLE [track].[ResourceTitle] (
	[Id]         INT IDENTITY(1,1) NOT NULL,
	[Name]       VARCHAR(255) NOT NULL,
	CONSTRAINT PK_ResourceTitle PRIMARY KEY NONCLUSTERED (
		[Id]
	),
	CONSTRAINT UK_ResourceTitle UNIQUE CLUSTERED (
		[Name]
	)
)
GO

CREATE TABLE [track].[Resource] (
	[Id]     INT IDENTITY(1,1) NOT NULL,
	[Name]   VARCHAR(1024) NOT NULL,
	CONSTRAINT PK_Resource PRIMARY KEY NONCLUSTERED (
		[Id]
	),
	CONSTRAINT UK_Resource UNIQUE CLUSTERED (
		[Name]
	)
)
GO

-- session

CREATE TABLE [track].[Session] (
	[Id]          INT IDENTITY(1,1) NOT NULL,
	[UserId]      INT NOT NULL,
	[Sequence]    INT NOT NULL,
	[BrowserId]   INT NOT NULL,
	[RemoteAddr]  VARCHAR(15) NULL,
	[Active]      BIT NOT NULL,
	[CreatedRoot] DATETIME NULL,
	[CreatedPrev] DATETIME NULL,
	[CreatedThis] DATETIME NOT NULL,
	CONSTRAINT PK_Session PRIMARY KEY NONCLUSTERED (
		[Id]
	),
	CONSTRAINT UK_Session UNIQUE CLUSTERED (
		[UserId],
		[Sequence]
	),
	CONSTRAINT FK_Session_User FOREIGN KEY (
		[UserId]
	)
	REFERENCES [track].[User] (
		[Id]
	),
	CONSTRAINT FK_Session_Browser FOREIGN KEY (
		[BrowserId]
	)
	REFERENCES [track].[Browser] (
		[Id]
	)
)
GO

-- action

CREATE TABLE [track].[Action] (
	[Id]              INT IDENTITY(1,1) NOT NULL,
	[SessionId]       INT NOT NULL,
	[ResourceId]      INT NOT NULL,
	[ResourceTitleId] INT NULL,
	[ReferrerId]      INT NULL,
	[HostId]          INT NULL,
	[Timestamp]       DATETIME NOT NULL,
	CONSTRAINT PK_Action PRIMARY KEY NONCLUSTERED (
		[Id]
	),
	CONSTRAINT FK_Action_Session FOREIGN KEY (
		[SessionId]
	)
	REFERENCES [track].[Session] (
		[Id]
	),
	CONSTRAINT FK_Action_Resource FOREIGN KEY (
		[ResourceId]
	)
	REFERENCES [track].[Resource] (
		[Id]
	),
	CONSTRAINT FK_Action_ResourceTitle FOREIGN KEY (
		[ResourceTitleId]
	)
	REFERENCES [track].[ResourceTitle] (
		[Id]
	),
	CONSTRAINT FK_Action_Referrer FOREIGN KEY (
		[ReferrerId]
	)
	REFERENCES [track].[Resource] (
		[Id]
	),
	CONSTRAINT FK_Action_Host FOREIGN KEY (
		[HostId]
	)
	REFERENCES [track].[Host] (
		[Id]
	)
)
GO

CREATE CLUSTERED INDEX IDX_Action_Session ON [track].[Action] ([SessionId])
GO

CREATE NONCLUSTERED INDEX IDX_Action_Resource ON [track].[Action] ([ResourceId])
GO

CREATE NONCLUSTERED INDEX IDX_Action_Referrer ON [track].[Action] ([ReferrerId])
GO


--  Configure access rights
GRANT insert ON SCHEMA :: track to Logger
GRANT update on track.Session to Logger
GO
