set nocount on

if exists(select * from InteractionLog.dbo.Alter_Script where AlterScriptName = '1_0_0/Initial-Objects.sql')
begin
	update InteractionLog.dbo.Alter_Script set
		AlterScriptName = '1_0_0\Initial-Objects.sql'
	where AlterScriptName = '1_0_0/Initial-Objects.sql'
end	