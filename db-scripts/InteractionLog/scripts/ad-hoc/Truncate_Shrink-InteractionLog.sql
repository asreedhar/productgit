SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'track.FK_Action_Host') AND parent_object_id = OBJECT_ID(N'track.Action'))
ALTER TABLE track.[Action] DROP CONSTRAINT FK_Action_Host
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'track.FK_Action_Referrer') AND parent_object_id = OBJECT_ID(N'track.Action'))
ALTER TABLE track.[Action] DROP CONSTRAINT FK_Action_Referrer
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'track.FK_Action_Resource') AND parent_object_id = OBJECT_ID(N'track.Action'))
ALTER TABLE track.[Action] DROP CONSTRAINT FK_Action_Resource
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'track.FK_Action_ResourceTitle') AND parent_object_id = OBJECT_ID(N'track.Action'))
ALTER TABLE track.[Action] DROP CONSTRAINT FK_Action_ResourceTitle
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'track.FK_Action_Session') AND parent_object_id = OBJECT_ID(N'track.Action'))
ALTER TABLE track.[Action] DROP CONSTRAINT FK_Action_Session
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'track.FK_Browser_CharacterSet') AND parent_object_id = OBJECT_ID(N'track.Browser'))
ALTER TABLE track.Browser DROP CONSTRAINT FK_Browser_CharacterSet
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'track.FK_Browser_ColorDepth') AND parent_object_id = OBJECT_ID(N'track.Browser'))
ALTER TABLE track.Browser DROP CONSTRAINT FK_Browser_ColorDepth
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'track.FK_Browser_Language') AND parent_object_id = OBJECT_ID(N'track.Browser'))
ALTER TABLE track.Browser DROP CONSTRAINT FK_Browser_Language
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'track.FK_Browser_ScreenResolution') AND parent_object_id = OBJECT_ID(N'track.Browser'))
ALTER TABLE track.Browser DROP CONSTRAINT FK_Browser_ScreenResolution
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'track.FK_Browser_ShockwaveVersion') AND parent_object_id = OBJECT_ID(N'track.Browser'))
ALTER TABLE track.Browser DROP CONSTRAINT FK_Browser_ShockwaveVersion
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'track.FK_Session_Browser') AND parent_object_id = OBJECT_ID(N'track.[Session]'))
ALTER TABLE track.[Session] DROP CONSTRAINT FK_Session_Browser
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'track.FK_Session_User') AND parent_object_id = OBJECT_ID(N'track.[Session]'))
ALTER TABLE track.[Session] DROP CONSTRAINT FK_Session_User
GO











TRUNCATE TABLE track.[Action]
TRUNCATE TABLE track.Browser
TRUNCATE TABLE track.CharacterSet
TRUNCATE TABLE track.ColorDepth
TRUNCATE TABLE track.Host
TRUNCATE TABLE track.[Language]
TRUNCATE TABLE track.[Resource]
TRUNCATE TABLE track.ResourceTitle
TRUNCATE TABLE track.ScreenResolution
TRUNCATE TABLE track.[Session]
TRUNCATE TABLE track.ShockwaveVersion
TRUNCATE TABLE track.[User]
GO













ALTER TABLE track.[Action] WITH CHECK ADD  CONSTRAINT FK_Action_Host FOREIGN KEY(HostId)
REFERENCES track.Host (Id)
GO

ALTER TABLE track.[Action] WITH CHECK ADD  CONSTRAINT FK_Action_Referrer FOREIGN KEY(ReferrerId)
REFERENCES track.Resource (Id)
GO

ALTER TABLE track.[Action] WITH CHECK ADD  CONSTRAINT FK_Action_Resource FOREIGN KEY(ResourceId)
REFERENCES track.Resource (Id)
GO

ALTER TABLE track.[Action] WITH CHECK ADD  CONSTRAINT FK_Action_ResourceTitle FOREIGN KEY(ResourceTitleId)
REFERENCES track.ResourceTitle (Id)
GO

ALTER TABLE track.[Action] WITH CHECK ADD  CONSTRAINT FK_Action_Session FOREIGN KEY(SessionId)
REFERENCES track.[Session] (Id)
GO

ALTER TABLE track.Browser  WITH CHECK ADD  CONSTRAINT FK_Browser_CharacterSet FOREIGN KEY(CharacterSetId)
REFERENCES track.CharacterSet (Id)
GO

ALTER TABLE track.Browser  WITH CHECK ADD  CONSTRAINT FK_Browser_ColorDepth FOREIGN KEY(ColorDepthId)
REFERENCES track.ColorDepth (Id)
GO

ALTER TABLE track.Browser  WITH CHECK ADD  CONSTRAINT FK_Browser_Language FOREIGN KEY(LanguageId)
REFERENCES track.[Language] (Id)
GO

ALTER TABLE track.Browser  WITH CHECK ADD  CONSTRAINT FK_Browser_ScreenResolution FOREIGN KEY(ScreenResolutionId)
REFERENCES track.ScreenResolution (Id)
GO

ALTER TABLE track.Browser  WITH CHECK ADD  CONSTRAINT FK_Browser_ShockwaveVersion FOREIGN KEY(ShockwaveVersionId)
REFERENCES track.ShockwaveVersion (Id)
GO

ALTER TABLE track.[Session]  WITH CHECK ADD  CONSTRAINT FK_Session_Browser FOREIGN KEY(BrowserId)
REFERENCES track.Browser (Id)
GO

ALTER TABLE track.[Session]  WITH CHECK ADD  CONSTRAINT FK_Session_User FOREIGN KEY(UserId)
REFERENCES track.[User] (Id)
GO



DBCC SHRINKDATABASE('InteractionLog')
