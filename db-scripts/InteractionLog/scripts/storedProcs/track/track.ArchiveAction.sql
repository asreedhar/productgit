IF OBJECT_ID('[track].[ArchiveAction]', 'P') IS NOT NULL
    DROP PROCEDURE [track].[ArchiveAction]
GO

CREATE PROCEDURE [track].[ArchiveAction]
    (
      @deleteBatchSize INT = 10
    , @fetchBatchSize INT = 1000000
    , @archiveDate DATETIME2(0) = NULL
	)
AS
    BEGIN
        SET NOCOUNT ON
        SET TRAN ISOLATION LEVEL READ UNCOMMITTED
        DECLARE @total INT
        SET @archiveDate = ISNULL(@archiveDate, DATEADD(MONTH, -1, CAST(GETDATE() AS DATE)))
        DECLARE @toBeDeleted TABLE
            (
              Id INT NOT NULL
                     PRIMARY KEY
            )

        DECLARE @deleteBatch TABLE
            (
              Id INT NOT NULL
                     PRIMARY KEY
            )

        INSERT  INTO @toBeDeleted
                ( [Id]
                )
                SELECT TOP ( @fetchBatchSize )
                        [a].[Id]
                FROM    [InteractionLog].[track].[Action] a
                WHERE   [a].[Timestamp] < @archiveDate    

        SELECT  @total = @@ROWCOUNT

        WHILE ( @total > 0 )
            BEGIN

                DELETE TOP ( @deleteBatchSize )
                        a
                OUTPUT  DELETED.Id
                        INTO @deleteBatch
                FROM    @toBeDeleted a

                SET @total = @total - @@ROWCOUNT

                DELETE  a
                OUTPUT  DELETED.*
                        INTO [Archive].[track].[Action]
                FROM    [InteractionLog].[track].[Action] a
                        INNER JOIN @deleteBatch b ON [b].[Id] = [a].[Id]

                DELETE  @deleteBatch

            END

        DELETE  @toBeDeleted

        INSERT  INTO @toBeDeleted
                ( [Id]
                )
                SELECT TOP ( @fetchBatchSize )
                        [s].[Id]
                FROM    [InteractionLog].[track].[Session] s
                WHERE   NOT EXISTS ( SELECT 1
                                     FROM   [InteractionLog].[track].[Action] a
                                     WHERE  [a].[SessionId] = [s].[Id] )
                        AND [s].[CreatedRoot] < @archiveDate



        SELECT  @total = @@ROWCOUNT

        WHILE ( @total > 0 )
            BEGIN

                DELETE TOP ( @deleteBatchSize )
                        a
                OUTPUT  DELETED.Id
                        INTO @deleteBatch
                FROM    @toBeDeleted a

                SET @total = @total - @@ROWCOUNT

                DELETE  a
                OUTPUT  DELETED.*
                        INTO [Archive].[track].[Session]
                FROM    [InteractionLog].[track].[Session] a
                        INNER JOIN @deleteBatch b ON [b].[Id] = [a].[Id]

                DELETE  @deleteBatch

            END

        DELETE  @toBeDeleted

        SET TRAN ISOLATION LEVEL READ COMMITTED
        SET NOCOUNT OFF
    END

GO
