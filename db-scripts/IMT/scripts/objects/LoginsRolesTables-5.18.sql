/*
Run this script on:

        WHUMMEL02.IMT    -  This database will be modified

to synchronize it with:

        PRODDB01SQL.IMT

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.1.0 from Red Gate Software Ltd at 9/8/2010 6:08:10 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [Distribution].[AccountPreferences]'
GO
ALTER TABLE [Distribution].[AccountPreferences] DROP
CONSTRAINT [FK_Distribution_AccountPreferences__ProviderMessageType],
CONSTRAINT [FK_Distribution_AccountPreferences__BusinessUnit]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [Distribution].[ProviderMessageType]'
GO
ALTER TABLE [Distribution].[ProviderMessageType] DROP
CONSTRAINT [FK_Distribution_ProviderMessageType__Provider]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [Distribution].[ContentText]'
GO
ALTER TABLE [Distribution].[ContentText] DROP CONSTRAINT [CK_Distribution_ContentText__Text]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [Distribution].[AccountPreferences]'
GO
ALTER TABLE [Distribution].[AccountPreferences] DROP CONSTRAINT [UQ_Distribution_AccountPreferences__Dealer_Provider_MessageType]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [Distribution].[ProviderMessageType]'
GO
ALTER TABLE [Distribution].[ProviderMessageType] DROP CONSTRAINT [PK_Distribution_ProviderMessageType]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [Distribution].[AccountPreferences]'
GO
EXEC sp_rename N'[Distribution].[AccountPreferences].[MessageTypeFlag]', N'MessageTypes', 'COLUMN'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [Distribution].[ProviderMessageType]'
GO
EXEC sp_rename N'[Distribution].[ProviderMessageType].[MessageTypeFlag]', N'MessageTypes', 'COLUMN'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_ProviderMessageType] on [Distribution].[ProviderMessageType]'
GO
ALTER TABLE [Distribution].[ProviderMessageType] ADD CONSTRAINT [PK_Distribution_ProviderMessageType] PRIMARY KEY CLUSTERED  ([ProviderID], [MessageTypes])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [Distribution].[ContentText]'
GO
ALTER TABLE [Distribution].[ContentText] ALTER COLUMN [Text] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [Distribution].[Exception]'
GO
ALTER TABLE [Distribution].[Exception] ADD
[ExceptionUserID] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [Distribution].[Message]'
GO
ALTER TABLE [Distribution].[Message] ADD
[MessageTypes] [int] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [Distribution].[SubmissionError]'
GO
EXEC sp_rename N'[Distribution].[SubmissionError].[MessageTypeFlag]', N'MessageTypes', 'COLUMN'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [Distribution].[SubmissionSuccess]'
GO
EXEC sp_rename N'[Distribution].[SubmissionSuccess].[MessageTypeFlag]', N'MessageTypes', 'COLUMN'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [Marketing].[MarketListingPreference]'
GO
ALTER TABLE [Marketing].[MarketListingPreference] ADD
[DisplayLimit] [int] NOT NULL CONSTRAINT [DF__MarketLis__Displ__75D15A31] DEFAULT ((6))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [Marketing].[MarketListingVehiclePreference]'
GO
ALTER TABLE [Marketing].[MarketListingVehiclePreference] ADD
[DisplayLimit] [int] NOT NULL CONSTRAINT [DF__MarketLis__Displ__76C57E6A] DEFAULT ((6))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [Marketing].[DealerGeneralPreference]'
GO
ALTER TABLE [Marketing].[DealerGeneralPreference] ADD
[Url] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__DealerGener__Url__74DD35F8] DEFAULT ('')
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Distribution].[AccountPreferences]'
GO
ALTER TABLE [Distribution].[AccountPreferences] ADD CONSTRAINT [UQ_Distribution_AccountPreferences__Dealer_Provider_MessageType] UNIQUE NONCLUSTERED  ([DealerID], [ProviderID], [MessageTypes])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Distribution].[AccountPreferences]'
GO
ALTER TABLE [Distribution].[AccountPreferences] ADD
CONSTRAINT [FK_Distribution_AccountPreferences__ProviderMessageType] FOREIGN KEY ([ProviderID], [MessageTypes]) REFERENCES [Distribution].[ProviderMessageType] ([ProviderID], [MessageTypes]),
CONSTRAINT [FK_Distribution_AccountPreferences__BusinessUnit] FOREIGN KEY ([DealerID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Distribution].[ProviderMessageType]'
GO
ALTER TABLE [Distribution].[ProviderMessageType] ADD
CONSTRAINT [FK_Distribution_ProviderMessageType__Provider] FOREIGN KEY ([ProviderID]) REFERENCES [Distribution].[Provider] ([ProviderID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO


