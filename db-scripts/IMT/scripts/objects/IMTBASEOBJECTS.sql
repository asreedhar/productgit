
/****** Object:  User dbo    Script Date: 7/14/2005 11:24:49 AM ******/
/****** Object:  User firstlook    Script Date: 7/14/2005 11:24:49 AM ******/
if not exists (select * from dbo.sysusers where name = N'firstlook' and uid < 16382)
	EXEC sp_grantdbaccess N'firstlook', N'firstlook'
GO

/****** Object:  User FIRSTLOOK\dev    Script Date: 7/14/2005 11:24:49 AM ******/
if not exists (select * from dbo.sysusers where name = N'FIRSTLOOK\dev' and uid < 16382)
	EXEC sp_grantdbaccess N'FIRSTLOOK\dev', N'FIRSTLOOK\dev'
GO

/****** Object:  User firstlook    Script Date: 7/14/2005 11:24:49 AM ******/
exec sp_addrolemember N'db_owner', N'firstlook'
GO

/****** Object:  User FIRSTLOOK\dev    Script Date: 7/14/2005 11:24:49 AM ******/
exec sp_addrolemember N'db_owner', N'FIRSTLOOK\dev'
GO

/****** Object:  Table [dbo].[Alter_Script]    Script Date: 7/14/2005 11:24:57 AM ******/
CREATE TABLE [dbo].[Alter_Script] (
	[AlterScriptName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ApplicationDate] [datetime] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[ApplicationEvent]    Script Date: 7/14/2005 11:24:59 AM ******/
CREATE TABLE [dbo].[ApplicationEvent] (
	[ApplicationEventID] [int] IDENTITY (100000, 1) NOT NULL ,
	[EventType] [int] NOT NULL ,
	[MemberID] [int] NOT NULL ,
	[EventDescription] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CreateTimestamp] [datetime] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[Appraisal]    Script Date: 7/14/2005 11:24:59 AM ******/
CREATE TABLE [dbo].[Appraisal] (
	[VehicleGuideBookID] [int] IDENTITY (1, 1) NOT NULL ,
	[Vin] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[MemberID] [int] NOT NULL ,
	[Price] [int] NULL ,
	[Color] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Mileage] [int] NULL ,
	[ConditionDisclosure] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ReconditioningNotes] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Make] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Body] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Year] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SelectedKey] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TradeAnalyzerDate] [datetime] NOT NULL ,
	[BookPrice] [int] NULL ,
	[EngineDescription] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DriveTrainDescription] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Transmission] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[InteriorColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[InteriorDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CylinderCount] [int] NULL ,
	[DoorCount] [int] NULL ,
	[FuelType] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ActionID] [int] NULL ,
	[Sold] [tinyint] NULL ,
	[EdStyleID] [int] NULL ,
	[ActualTrim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[KelleyEngineCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[KelleyTransmissionCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[KelleyDriveTrainCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[KelleyMakeCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[KelleyModelCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AppraisalValue] [int] NULL ,
	[IsTrimFixed] [tinyint] NULL ,
	[MakeModelGroupingId] [int] NOT NULL ,
	[AppraisalInitials] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[GuideBookID] [tinyint] NOT NULL ,
	[LastBookOutDate] [smalldatetime] NULL ,
	[Weight] [int] NULL ,
	[GuideBookFooter] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MileageCostAdjustment] [int] NULL ,
	[MSRP] [int] NULL ,
	[GuideBookDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[GuideBookRegion] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[KelleyPublishInfo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Trim] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[AppraisalBump]    Script Date: 7/14/2005 11:25:01 AM ******/
CREATE TABLE [dbo].[AppraisalBump] (
	[VehicleGuideBookBumpID] [int] IDENTITY (1, 1) NOT NULL ,
	[VehicleGuideBookID] [int] NOT NULL ,
	[Bump] [int] NULL ,
	[Initials] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[AppraisalGuideBookOptions]    Script Date: 7/14/2005 11:25:01 AM ******/
CREATE TABLE [dbo].[AppraisalGuideBookOptions] (
	[VehicleGuideBookOptionsID] [int] IDENTITY (1, 1) NOT NULL ,
	[Vin] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OptionName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OptionKey] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[IsSelected] [tinyint] NOT NULL ,
	[VehicleGuideBookID] [int] NULL ,
	[OptionValue] [int] NOT NULL ,
	[Type] [tinyint] NOT NULL ,
	[WholesaleValue] [int] NULL ,
	[standardOption] [tinyint] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[AppraisalGuideBookValues]    Script Date: 7/14/2005 11:25:02 AM ******/
CREATE TABLE [dbo].[AppraisalGuideBookValues] (
	[VehicleGuideBookValuesID] [int] IDENTITY (1, 1) NOT NULL ,
	[VehicleGuideBookID] [int] NOT NULL ,
	[Value] [int] NULL ,
	[Title] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ValueType] [tinyint] NOT NULL ,
	[valid] [tinyint] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[AppraisalInGroupDemand]    Script Date: 7/14/2005 11:25:02 AM ******/
CREATE TABLE [dbo].[AppraisalInGroupDemand] (
	[VehicleGuideBookInGroupDemandID] [int] IDENTITY (1, 1) NOT NULL ,
	[VehicleGuideBookID] [int] NOT NULL ,
	[BusinessUnitID] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[AppraisalWindowSticker]    Script Date: 7/14/2005 11:25:03 AM ******/
CREATE TABLE [dbo].[AppraisalWindowSticker] (
	[AppraisalWindowStickerId] [int] IDENTITY (1, 1) NOT NULL ,
	[AppraisalId] [int] NOT NULL ,
	[StockNumber] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SellingPrice] [int] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[AuditBookOuts]    Script Date: 7/14/2005 11:25:04 AM ******/
CREATE TABLE [dbo].[AuditBookOuts] (
	[AuditId] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitId] [int] NOT NULL ,
	[MemberId] [int] NOT NULL ,
	[ProductId] [int] NOT NULL ,
	[ReferenceId] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BookId] [tinyint] NOT NULL ,
	[BookOutTimeStamp] [smalldatetime] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[Audit_Bookouts_Inventory]    Script Date: 7/14/2005 11:25:04 AM ******/
CREATE TABLE [dbo].[Audit_Bookouts_Inventory] (
	[BookOutReferenceID] [int] NOT NULL ,
	[BookOutSourceID] [tinyint] NOT NULL ,
	[BookOutTimeStamp] [datetime] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[Audit_Bookouts_TradeAnalyzer]    Script Date: 7/14/2005 11:25:04 AM ******/
CREATE TABLE [dbo].[Audit_Bookouts_TradeAnalyzer] (
	[BookOutReferenceID] [int] NOT NULL ,
	[BookOutSourceID] [tinyint] NOT NULL ,
	[BookOutTimeStamp] [datetime] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[Audit_Exceptions]    Script Date: 7/14/2005 11:25:04 AM ******/
CREATE TABLE [dbo].[Audit_Exceptions] (
	[Audit_ID] [int] NOT NULL ,
	[SourceTableID] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ExceptionRuleID] [int] NOT NULL ,
	[TimeStamp] [datetime] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[Audit_Exceptions_Debug]    Script Date: 7/14/2005 11:25:05 AM ******/
CREATE TABLE [dbo].[Audit_Exceptions_Debug] (
	[Audit_ID] [int] NOT NULL ,
	[SourceTableID] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ExceptionRuleID] [int] NOT NULL ,
	[TimeStamp] [datetime] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[Audit_Exceptions_Test]    Script Date: 7/14/2005 11:25:05 AM ******/
CREATE TABLE [dbo].[Audit_Exceptions_Test] (
	[Audit_ID] [int] NOT NULL ,
	[SourceTableID] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ExceptionRuleID] [int] NOT NULL ,
	[TimeStamp] [datetime] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[Audit_Inventory]    Script Date: 7/14/2005 11:25:05 AM ******/
CREATE TABLE [dbo].[Audit_Inventory] (
	[AUDIT_ID] [int] NOT NULL ,
	[STATUS] [tinyint] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[Audit_Sales]    Script Date: 7/14/2005 11:25:05 AM ******/
CREATE TABLE [dbo].[Audit_Sales] (
	[AUDIT_ID] [int] NOT NULL ,
	[STATUS] [tinyint] NOT NULL ,
	[PACK_ADJUST] [decimal](8, 2) NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[BookOutTypes]    Script Date: 7/14/2005 11:25:05 AM ******/
CREATE TABLE [dbo].[BookOutTypes] (
	[BookOutTypeId] [int] NOT NULL ,
	[BookOutDescription] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[BusinessUnit]    Script Date: 7/14/2005 11:25:06 AM ******/
CREATE TABLE [dbo].[BusinessUnit] (
	[BusinessUnitID] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitTypeID] [int] NOT NULL ,
	[BusinessUnit] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[BusinessUnitShortName] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BusinessUnitCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address1] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address2] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[City] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ZipCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[OfficePhone] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[OfficeFax] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Active] [tinyint] NOT NULL
) ON [DATA]
GO

/****** Object:  Table [dbo].[BusinessUnitRelationship]    Script Date: 7/14/2005 11:25:07 AM ******/
CREATE TABLE [dbo].[BusinessUnitRelationship] (
	[BusinessUnitID] [int] NOT NULL ,
	[ParentID] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[BusinessUnitType]    Script Date: 7/14/2005 11:25:07 AM ******/
CREATE TABLE [dbo].[BusinessUnitType] (
	[BusinessUnitTypeID] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitType] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[CIAEngineFailures]    Script Date: 7/14/2005 11:25:07 AM ******/
CREATE TABLE [dbo].[CIAEngineFailures] (
	[BusinessUnitID] [int] NOT NULL ,
	[TimeStamp] [datetime] NOT NULL ,
	[Message] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[CIAMakeModelMinMax]    Script Date: 7/14/2005 11:25:08 AM ******/
CREATE TABLE [dbo].[CIAMakeModelMinMax] (
	[CIAMakeModelMinMaxID] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[MakeModelGroupingID] [int] NOT NULL ,
	[MinDays] [int] NOT NULL ,
	[MaxDays] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[CIAMakeModelSuppression]    Script Date: 7/14/2005 11:25:08 AM ******/
CREATE TABLE [dbo].[CIAMakeModelSuppression] (
	[CIAMakeModelSuppressionID] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[MakeModelGroupingID] [int] NOT NULL ,
	[Comments] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[CIAPricePoint]    Script Date: 7/14/2005 11:25:08 AM ******/
CREATE TABLE [dbo].[CIAPricePoint] (
	[CIAPricePointID] [int] IDENTITY (1, 1) NOT NULL ,
	[CIAReportID] [int] NOT NULL ,
	[GroupingDescriptionID] [int] NOT NULL ,
	[RangeMax] [int] NOT NULL ,
	[RangeMin] [int] NOT NULL ,
	[Increment] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[CIAReport]    Script Date: 7/14/2005 11:25:08 AM ******/
CREATE TABLE [dbo].[CIAReport] (
	[CIAReportID] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[ManagersChoiceMake] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ManagersChoiceModel] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MakeModelGroupingID] [int] NOT NULL ,
	[RankID] [int] NULL ,
	[ZoneID] [int] NULL ,
	[TargetRange] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TargetBuy] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[UnitsInStock] [int] NULL ,
	[TrimNotes] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ColorNotes] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[YearNotes] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[UnitsToBuy] [int] NULL ,
	[Trim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VehicleYear] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PricePoint] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PricePointNotes] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MinTargetRange] [int] NULL ,
	[MaxTargetRange] [int] NULL ,
	[Status] [tinyint] NOT NULL ,
	[Date] [datetime] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[CIAReportAverages]    Script Date: 7/14/2005 11:25:09 AM ******/
CREATE TABLE [dbo].[CIAReportAverages] (
	[CiaReportAveragesID] [int] IDENTITY (10000, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[UnitsSold] [int] NULL ,
	[AverageGrossProfit] [int] NULL ,
	[DaysToSale] [int] NULL ,
	[NoSales] [int] NULL ,
	[UnitsInStock] [int] NULL ,
	[WeeklyUnitsSold] [int] NULL ,
	[Status] [tinyint] NOT NULL ,
	[Date] [datetime] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[CIAReportMakeModelAttributes]    Script Date: 7/14/2005 11:25:09 AM ******/
CREATE TABLE [dbo].[CIAReportMakeModelAttributes] (
	[CIAReportMakeModelAttributesID] [int] IDENTITY (1, 1) NOT NULL ,
	[CIAReportID] [int] NOT NULL ,
	[Value] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Type] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Prefer] [tinyint] NULL ,
	[Avoid] [tinyint] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[CIAReportPerformance]    Script Date: 7/14/2005 11:25:10 AM ******/
CREATE TABLE [dbo].[CIAReportPerformance] (
	[CIAReportPerformanceID] [int] IDENTITY (1, 1) NOT NULL ,
	[CIAReportID] [int] NOT NULL ,
	[PerformanceDescription] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[CarfaxAvailability]    Script Date: 7/14/2005 11:25:10 AM ******/
CREATE TABLE [dbo].[CarfaxAvailability] (
	[UserName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Vin] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Expires] [smalldatetime] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[ChangedVics]    Script Date: 7/14/2005 11:25:10 AM ******/
CREATE TABLE [dbo].[ChangedVics] (
	[ChangedVicID] [int] IDENTITY (100000, 1) NOT NULL ,
	[OldVic] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[NewVic] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[CredentialType]    Script Date: 7/14/2005 11:25:10 AM ******/
CREATE TABLE [dbo].[CredentialType] (
	[CredentialTypeID] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[DMI_STAGING_INVENTORY]    Script Date: 7/14/2005 11:25:11 AM ******/
CREATE TABLE [dbo].[DMI_STAGING_INVENTORY] (
	[StockNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[VehicleID] [int] NOT NULL ,
	[InventoryActive] [tinyint] NOT NULL ,
	[InventoryReceivedDate] [smalldatetime] NOT NULL ,
	[DeleteDt] [smalldatetime] NULL ,
	[ModifiedDT] [smalldatetime] NULL ,
	[DMSReferenceDt] [smalldatetime] NULL ,
	[UnitCost] [decimal](8, 2) NULL ,
	[AcquisitionPrice] [decimal](8, 2) NULL ,
	[Pack] [decimal](8, 2) NULL ,
	[MileageReceived] [int] NULL ,
	[TradeOrPurchase] [tinyint] NOT NULL ,
	[ListPrice] [decimal](8, 2) NULL ,
	[InitialVehicleLight] [int] NULL ,
	[Level4Analysis] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[InventoryType] [tinyint] NULL ,
	[ReconditionCost] [decimal](8, 2) NULL ,
	[UsedSellingPrice] [decimal](8, 2) NULL ,
	[Certified] [int] NULL ,
	[VehicleLocation] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CurrentVehicleLight] [tinyint] NULL ,
	[FLRecFollowed] [tinyint] NOT NULL ,
	[Audit_ID] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[DataSource]    Script Date: 7/14/2005 11:25:11 AM ******/
CREATE TABLE [dbo].[DataSource] (
	[DataSourceID] [int] IDENTITY (1, 1) NOT NULL ,
	[DataSource] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[DealerCIAPreferences]    Script Date: 7/14/2005 11:25:11 AM ******/
CREATE TABLE [dbo].[DealerCIAPreferences] (
	[DealerCIAPreferencesID] [int] IDENTITY (100000, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[ShortAgeBandID] [int] NULL ,
	[LongAgeBandID] [int] NULL ,
	[ForecastAgeBandID] [int] NULL ,
	[ShortAgeBandWeight] [int] NULL ,
	[LongAgeBandWeight] [int] NULL ,
	[ForecastAgeBandWeight] [int] NULL ,
	[NumberOfPowerZoneMakeModels] [int] NULL ,
	[MinRecommendedDaysSupply] [int] NULL ,
	[MaxRecommendedDaysSupply] [int] NULL ,
	[ApplyNoSalePctThreshold] [tinyint] NULL ,
	[NoSalePctThreshold] [int] NULL ,
	[ThresholdOnDaysSupply] [decimal](8, 1) NULL ,
	[MinRecommendedStockingLevel] [int] NULL ,
	[MaxRecommendedStockingLevel] [int] NULL ,
	[ShortAgeBandUnitsSoldThreshold] [int] NULL ,
	[LongAgeBandUnitsSoldThreshold] [int] NULL ,
	[ForecastAgeBandUnitsSoldThreshold] [int] NULL ,
	[MetricAverageGrossProfit] [tinyint] NULL ,
	[MetricAverageFrontEndGrossProfitPercentage] [int] NULL ,
	[MetricAverageBackEndGrossProfitPercentage] [int] NULL ,
	[MetricAverageCombinedGrossProfitPercentage] [int] NULL ,
	[MetricAverageGrossProfitCombined] [tinyint] NULL ,
	[MetricAverageUnitCost] [tinyint] NULL ,
	[MetricAverageMileage] [tinyint] NULL ,
	[MetricAverageSalePrice] [tinyint] NULL ,
	[MetricAverageDaysToSale] [tinyint] NULL ,
	[MetricAverageMargin] [tinyint] NULL ,
	[MetricPercentOutOfStock] [tinyint] NULL ,
	[MetricAdjustedGrossProfit] [tinyint] NULL ,
	[MetricContribution] [tinyint] NULL ,
	[MetricContributionFrontEndPercentage] [int] NULL ,
	[MetricContributionBackEndPercentage] [int] NULL ,
	[MetricContributionCombinedPercentage] [int] NULL ,
	[MetricContributionCombined] [tinyint] NULL ,
	[MetricContributionNoSaleLossFactor] [tinyint] NULL ,
	[MetricAverageUnitCostPercentage] [int] NULL ,
	[MetricAverageMileagePercentage] [int] NULL ,
	[MetricAverageSalePricePercentage] [int] NULL ,
	[MetricAverageDaysToSalePercentage] [int] NULL ,
	[MetricAverageMarginPercentage] [int] NULL ,
	[MetricPercentOutOfStockPercentage] [int] NULL ,
	[MetricAdjustedGrossProfitPercentage] [int] NULL ,
	[MetricInventoryHoldingAmount] [int] NULL ,
	[WinnerScoreThreshold] [int] NULL ,
	[MaxCIAVehicles] [int] NULL ,
	[PowerZoneParentRankPercentage] [int] NULL ,
	[MarketRankPercentage] [int] NULL ,
	[ModelPerformance] [tinyint] NULL ,
	[ModelPerformancePercentage] [int] NULL ,
	[SalesPerMonthFirstLook] [tinyint] NULL ,
	[SalesPerMonthFirstLookPercentage] [int] NULL ,
	[SalesPerMonthSimilar] [tinyint] NULL ,
	[SalesPerMonthSimilarPercentage] [int] NULL ,
	[MinWeightVariable] [int] NULL ,
	[MaxWeightVariable] [int] NULL ,
	[DisplayGoodBets] [tinyint] NULL ,
	[GoodBetScoreThreshold] [int] NULL ,
	[ExcludeRedLightVehicle] [tinyint] NULL ,
	[GoodBetMinTargetRange] [int] NULL ,
	[GoodBetMaxTargetRange] [int] NULL ,
	[UseInventoryHolding] [tinyint] NOT NULL ,
	[RunDayOfWeek] [int] NOT NULL ,
	[Modifiable] [tinyint] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[DealerFacts]    Script Date: 7/14/2005 11:25:12 AM ******/
CREATE TABLE [dbo].[DealerFacts] (
	[DealerFactID] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[DateCreated] [datetime] NOT NULL ,
	[LastPolledDate] [datetime] NULL ,
	[LastDMSReferenceDateNew] [datetime] NULL ,
	[LastDMSReferenceDateUsed] [datetime] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[DealerFinancialStatement]    Script Date: 7/14/2005 11:25:12 AM ******/
CREATE TABLE [dbo].[DealerFinancialStatement] (
	[DealerFinancialStatementID] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[MonthNumber] [int] NOT NULL ,
	[YearNumber] [int] NOT NULL ,
	[RetailNumberOfDeals] [int] NULL ,
	[RetailTotalGrossProfit] [decimal](9, 2) NULL ,
	[RetailFrontEndGrossProfit] [decimal](9, 2) NULL ,
	[RetailBackEndGrossProfit] [decimal](9, 2) NULL ,
	[RetailAverageGrossProfit] [decimal](9, 2) NULL ,
	[WholesaleNumberOfDeals] [int] NULL ,
	[WholesaleTotalGrossProfit] [decimal](9, 2) NULL ,
	[WholesaleFrontEndGrossProfit] [decimal](9, 2) NULL ,
	[WholesaleBackEndGrossProfit] [decimal](9, 2) NULL ,
	[WholesaleAverageGrossProfit] [decimal](9, 2) NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[DealerFranchise]    Script Date: 7/14/2005 11:25:13 AM ******/
CREATE TABLE [dbo].[DealerFranchise] (
	[DealerFranchiseID] [int] IDENTITY (10000, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[FranchiseID] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[DealerGridThresholds]    Script Date: 7/14/2005 11:25:13 AM ******/
CREATE TABLE [dbo].[DealerGridThresholds] (
	[DealerGridThresholdsId] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[firstValuationThreshold] [int] NOT NULL ,
	[secondValuationThreshold] [int] NOT NULL ,
	[thirdValuationThreshold] [int] NOT NULL ,
	[fourthValuationThreshold] [int] NOT NULL ,
	[firstDealThreshold] [int] NOT NULL ,
	[secondDealThreshold] [int] NOT NULL ,
	[thirdDealThreshold] [int] NOT NULL ,
	[fourthDealThreshold] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[DealerGridValues]    Script Date: 7/14/2005 11:25:13 AM ******/
CREATE TABLE [dbo].[DealerGridValues] (
	[DealerGridValuesId] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[LightValue] [int] NOT NULL ,
	[TypeValue] [int] NOT NULL ,
	[indexKey] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[DealerGroupPotentialEvent]    Script Date: 7/14/2005 11:25:14 AM ******/
CREATE TABLE [dbo].[DealerGroupPotentialEvent] (
	[DealerGroupPotentialEventID] [int] IDENTITY (100000, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[TradeAnalyzerEventID] [int] NOT NULL ,
	[UnitsInStock] [int] NULL ,
	[CreateTimestamp] [datetime] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[DealerGroupPreference]    Script Date: 7/14/2005 11:25:14 AM ******/
CREATE TABLE [dbo].[DealerGroupPreference] (
	[DealerGroupPreferenceID] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[AgingPolicy] [int] NULL ,
	[AgingPolicyAdjustment] [int] NULL ,
	[WholesaleDays] [int] NULL ,
	[IncludeRedistribution] [tinyint] NULL ,
	[InventoryExchangeAgeLimit] [int] NULL ,
	[IncludeRoundTable] [tinyint] NULL ,
	[IncludeCIA] [tinyint] NULL ,
	[IncludeRedLightAgingPlan] [tinyint] NULL ,
	[PricePointDealsThreshold] [int] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[DealerPack]    Script Date: 7/14/2005 11:25:14 AM ******/
CREATE TABLE [dbo].[DealerPack] (
	[DealerPackID] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[PackTypeCD] [tinyint] NOT NULL ,
	[PackAmount] [decimal](8, 2) NULL ,
	[PackJavaClass] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[DealerPreference]    Script Date: 7/14/2005 11:25:15 AM ******/
CREATE TABLE [dbo].[DealerPreference] (
	[DealerPreferenceID] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[CustomHomePageMessage] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DashboardColumnDisplayPreference] [int] NOT NULL ,
	[UnitsSoldThreshold4Wks] [int] NOT NULL ,
	[UnitsSoldThreshold8Wks] [int] NOT NULL ,
	[UnitsSoldThreshold13Wks] [int] NOT NULL ,
	[UnitsSoldThreshold26Wks] [int] NOT NULL ,
	[UnitsSoldThreshold52Wks] [int] NOT NULL ,
	[DefaultTrendingView] [int] NULL ,
	[DefaultTrendingWeeks] [int] NULL ,
	[DefaultForecastingWeeks] [int] NULL ,
	[GuideBookID] [int] NULL ,
	[NADARegionCode] [int] NOT NULL ,
	[InceptionDate] [datetime] NULL ,
	[AgingInventoryTrackingDisplayPref] [tinyint] NULL ,
	[PackAmount] [decimal](8, 2) NOT NULL ,
	[StockOrVinPreference] [tinyint] NULL ,
	[MaxSalesHistoryDate] [datetime] NULL ,
	[UnitCostOrListPricePreference] [tinyint] NULL ,
	[ListPricePreference] [tinyint] NULL ,
	[AgeBandTarget1] [int] NULL ,
	[AgeBandTarget2] [int] NULL ,
	[AgeBandTarget3] [int] NULL ,
	[AgeBandTarget4] [int] NULL ,
	[AgeBandTarget5] [int] NULL ,
	[AgeBandTarget6] [int] NULL ,
	[DaysSupply12WeekWeight] [int] NULL ,
	[DaysSupply26WeekWeight] [int] NULL ,
	[BookOut] [tinyint] NOT NULL ,
	[BookOutPreferenceId] [int] NOT NULL ,
	[UnitCostThreshold] [decimal](10, 2) NOT NULL ,
	[BookOutSecondPreferenceId] [int] NULL ,
	[GuideBook2Id] [int] NULL ,
	[GuideBook2BookOutPreferenceId] [int] NULL ,
	[GuideBook2SecondBookOutPreferenceId] [int] NULL ,
	[UnitCostUpdateOnSale] [bit] NOT NULL ,
	[UnwindDaysThreshold] [tinyint] NOT NULL ,
	[AuctionAreaId] [int] NULL ,
	[showLotLocationStatus] [tinyint] NULL ,
	[PopulateClassName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BookOutPreferenceSecondId] [int] NULL ,
	[RunDayOfWeek] [int] NOT NULL ,
	[UnitCostThresholdLower] [int] NULL ,
	[UnitCostThresholdUpper] [int] NULL ,
	[FEGrossProfitThreshold] [int] NULL ,
	[MarginPercentile] [smallint] NULL ,
	[DaysToSalePercentile] [smallint] NULL ,
	[ProgramType_CD] [tinyint] NULL ,
	[SellThroughRate] [int] NOT NULL ,
	[IncludeBackEndInValuation] [tinyint] NOT NULL ,
	[UnitsSoldThresholdInvOverview] [int] NULL ,
	[CIATargetDaysSupply] [int] NULL ,
	[CIAMarketPerformerInStockThreshold] [int] NULL ,
	[GoLiveDate] [smalldatetime] NULL ,
	[VehicleSaleThresholdForCoreMarketPenetration] [float] NULL ,
	[ATCEnabled] [tinyint] NULL ,
	[ApplyPriorAgingNotes] [bit] NOT NULL ,
	[averageInventoryAgeRedThreshold] [int] NULL ,
	[averageDaysSupplyRedThreshold] [int] NULL ,
	[PurchasingDistanceFromDealer] [int] NOT NULL ,
	[DisplayUnitCostToDealerGroup] [tinyint] NOT NULL ,
	[UnitsSoldThreshold12Wks] [int] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[DealerPreferenceDataload]    Script Date: 7/14/2005 11:25:15 AM ******/
CREATE TABLE [dbo].[DealerPreferenceDataload] (
	[DealerPreferenceDataloadID] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[TradeorPurchaseFromSales] [tinyint] NOT NULL ,
	[TradeorPurchaseFromSalesDaysThreshold] [smallint] NOT NULL ,
	[GenerateInventoryDeltas] [tinyint] NOT NULL ,
	[CalculateUnitCost] [bit] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[DealerRisk]    Script Date: 7/14/2005 11:25:16 AM ******/
CREATE TABLE [dbo].[DealerRisk] (
	[BusinessUnitId] [int] NOT NULL ,
	[RiskLevelNumberOfWeeks] [int] NOT NULL ,
	[RiskLevelDealsThreshold] [int] NOT NULL ,
	[RiskLevelNumberOfContributors] [int] NOT NULL ,
	[RedLightNoSaleThreshold] [int] NOT NULL ,
	[RedLightGrossProfitThreshold] [int] NOT NULL ,
	[GreenLightNoSaleThreshold] [int] NOT NULL ,
	[GreenLightGrossProfitThreshold] [decimal](8, 2) NOT NULL ,
	[GreenLightMarginThreshold] [decimal](8, 2) NOT NULL ,
	[GreenLightDaysPercentage] [int] NOT NULL ,
	[HighMileageYear] [int] NOT NULL ,
	[HighMileageOverall] [int] NOT NULL ,
	[RiskLevelYearInitialTimePeriod] [int] NOT NULL ,
	[RiskLevelYearSecondaryTimePeriod] [int] NOT NULL ,
	[RiskLevelYearRollOverMonth] [int] NOT NULL ,
	[RedLightTarget] [int] NOT NULL ,
	[YellowLightTarget] [int] NOT NULL ,
	[GreenLightTarget] [int] NOT NULL ,
	[HighMileageRedLight] [int] NOT NULL 
) ON [IDX]
GO

/****** Object:  Table [dbo].[DealerTarget]    Script Date: 7/14/2005 11:25:16 AM ******/
CREATE TABLE [dbo].[DealerTarget] (
	[DealerTargetId] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitId] [int] NOT NULL ,
	[TargetCD] [smallint] NOT NULL ,
	[StartDT] [smalldatetime] NOT NULL ,
	[TargetValue] [int] NOT NULL ,
	[Active] [bit] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[DealerUpgrade]    Script Date: 7/14/2005 11:25:17 AM ******/
CREATE TABLE [dbo].[DealerUpgrade] (
	[BusinessUnitID] [int] NOT NULL ,
	[DealerUpgradeCD] [tinyint] NOT NULL ,
	[StartDate] [smalldatetime] NULL ,
	[EndDate] [smalldatetime] NULL ,
	[Active] [tinyint] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[DecodedFirstLookSquishVINs]    Script Date: 7/14/2005 11:25:17 AM ******/
CREATE TABLE [dbo].[DecodedFirstLookSquishVINs] (
	[squish_vin] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[edstyleid] [int] NULL ,
	[year] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[make] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MakeModelGroupingID] [int] NULL ,
	[trim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[bodyType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[class] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[doors] [tinyint] NULL ,
	[fuel_type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[engine] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[drive_type_code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[trans] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[cylinder_qty] [tinyint] NULL ,
	[DisplayBodyTypeID] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[DecodedSquishVINs]    Script Date: 7/14/2005 11:25:17 AM ******/
CREATE TABLE [dbo].[DecodedSquishVINs] (
	[squish_vin] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[edstyleid] [int] NULL ,
	[year] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[make] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MakeModelGroupingID] [int] NULL ,
	[trim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[bodyType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[class] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[doors] [tinyint] NULL ,
	[fuel_type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[engine] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[drive_type_code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[trans] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[cylinder_qty] [tinyint] NULL ,
	[dsvSourceID] [tinyint] NOT NULL ,
	[DisplayBodyTypeID] [int] NOT NULL ,
	[EdmundsBodyTypeCode] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[ErrorHandler]    Script Date: 7/14/2005 11:25:17 AM ******/
CREATE TABLE [dbo].[ErrorHandler] (
	[ErrorHandlerID] [int] IDENTITY (10000, 1) NOT NULL ,
	[Timestamp] [datetime] NOT NULL ,
	[ErrorMessage] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ApplicationPage] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MemberID] [int] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[FirstLookRegion]    Script Date: 7/14/2005 11:25:18 AM ******/
CREATE TABLE [dbo].[FirstLookRegion] (
	[FirstLookRegionID] [int] IDENTITY (1, 1) NOT NULL ,
	[FirstLookRegionName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[FirstLookRegionToZip]    Script Date: 7/14/2005 11:25:18 AM ******/
CREATE TABLE [dbo].[FirstLookRegionToZip] (
	[FirstLookRegionID] [int] NOT NULL ,
	[ZipCode] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[Franchise]    Script Date: 7/14/2005 11:25:18 AM ******/
CREATE TABLE [dbo].[Franchise] (
	[FranchiseID] [int] IDENTITY (10000, 1) NOT NULL ,
	[FranchiseDescription] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[FranchiseAlias]    Script Date: 7/14/2005 11:25:18 AM ******/
CREATE TABLE [dbo].[FranchiseAlias] (
	[FranchiseAliasID] [int] IDENTITY (1, 1) NOT NULL ,
	[FranchiseID] [int] NOT NULL ,
	[FranchiseAlias] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[GDLight]    Script Date: 7/14/2005 11:25:19 AM ******/
CREATE TABLE [dbo].[GDLight] (
	[GroupingDescriptionLightId] [int] IDENTITY (1, 1) NOT NULL ,
	[GroupingDescriptionId] [int] NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[InventoryVehicleLightID] [tinyint] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[GlobalParam]    Script Date: 7/14/2005 11:25:19 AM ******/
CREATE TABLE [dbo].[GlobalParam] (
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	[name] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[value] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[GuideBook]    Script Date: 7/14/2005 11:25:19 AM ******/
CREATE TABLE [dbo].[GuideBook] (
	[GuideBookID] [int] IDENTITY (1, 1) NOT NULL ,
	[GuideBookName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[GuideBookCategory] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[GuideBookValue]    Script Date: 7/14/2005 11:25:19 AM ******/
CREATE TABLE [dbo].[GuideBookValue] (
	[GuideBookValueID] [int] IDENTITY (1, 1) NOT NULL ,
	[InventoryID] [int] NOT NULL ,
	[GuideBookID] [int] NOT NULL ,
	[InitialValue] [int] NULL ,
	[CurrentValue] [int] NULL ,
	[InitialDT] [datetime] NOT NULL ,
	[CurrentDT] [datetime] NOT NULL ,
	[valid] [tinyint] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[Inventory]    Script Date: 7/14/2005 11:25:20 AM ******/
CREATE TABLE [dbo].[Inventory] (
	[InventoryID] [int] IDENTITY (1, 1) NOT NULL ,
	[StockNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[VehicleID] [int] NOT NULL ,
	[InventoryActive] [tinyint] NOT NULL ,
	[InventoryReceivedDate] [smalldatetime] NOT NULL ,
	[DeleteDt] [smalldatetime] NULL ,
	[ModifiedDT] [smalldatetime] NOT NULL ,
	[DMSReferenceDt] [smalldatetime] NULL ,
	[UnitCost] [decimal](9, 2) NOT NULL ,
	[AcquisitionPrice] [decimal](8, 2) NULL ,
	[Pack] [decimal](8, 2) NULL ,
	[MileageReceived] [int] NULL ,
	[TradeOrPurchase] [tinyint] NOT NULL ,
	[ListPrice] [decimal](8, 2) NULL ,
	[InitialVehicleLight] [int] NULL ,
	[Level4Analysis] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[InventoryType] [tinyint] NOT NULL ,
	[ReconditionCost] [decimal](8, 2) NULL ,
	[UsedSellingPrice] [decimal](8, 2) NULL ,
	[Certified] [tinyint] NOT NULL ,
	[VehicleLocation] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CurrentVehicleLight] [tinyint] NULL ,
	[FLRecFollowed] [tinyint] NOT NULL ,
	[Audit_ID] [int] NOT NULL ,
	[InventoryStatusCD] [smallint] NOT NULL ,
	[ListPriceLock] [tinyint] NOT NULL ,
	[SpecialFinance] [tinyint] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[InventoryBucketRanges]    Script Date: 7/14/2005 11:25:21 AM ******/
CREATE TABLE [dbo].[InventoryBucketRanges] (
	[InventoryBucketRangeID] [int] IDENTITY (1, 1) NOT NULL ,
	[InventoryBucketID] [int] NOT NULL ,
	[RangeID] [tinyint] NOT NULL ,
	[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Low] [int] NOT NULL ,
	[High] [int] NULL ,
	[Lights] [tinyint] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[InventoryBuckets]    Script Date: 7/14/2005 11:25:21 AM ******/
CREATE TABLE [dbo].[InventoryBuckets] (
	[InventoryBucketID] [int] IDENTITY (1, 1) NOT NULL ,
	[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[InventoryType] [tinyint] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[InventoryStatusCodes]    Script Date: 7/14/2005 11:25:21 AM ******/
CREATE TABLE [dbo].[InventoryStatusCodes] (
	[InventoryStatusCD] [smallint] IDENTITY (1, 1) NOT NULL ,
	[ShortDescription] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[InventoryStocking]    Script Date: 7/14/2005 11:25:21 AM ******/
CREATE TABLE [dbo].[InventoryStocking] (
	[InventoryStockingID] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[DisplayBodyTypeID] [int] NOT NULL ,
	[CIASummaryId] [int] NOT NULL ,
	[TargetInventory] [int] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[InventoryStockingModel]    Script Date: 7/14/2005 11:25:22 AM ******/
CREATE TABLE [dbo].[InventoryStockingModel] (
	[InventoryStockingModelID] [int] IDENTITY (1, 1) NOT NULL ,
	[InventoryStockingID] [int] NOT NULL ,
	[GroupingDescriptionID] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[InventoryZeroLights]    Script Date: 7/14/2005 11:25:22 AM ******/
CREATE TABLE [dbo].[InventoryZeroLights] (
	[InventoryID] [int] IDENTITY (1, 1) NOT NULL ,
	[StockNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[VehicleID] [int] NOT NULL ,
	[InventoryActive] [tinyint] NOT NULL ,
	[InventoryReceivedDate] [smalldatetime] NOT NULL ,
	[DeleteDt] [smalldatetime] NULL ,
	[ModifiedDT] [smalldatetime] NOT NULL ,
	[DMSReferenceDt] [smalldatetime] NULL ,
	[UnitCost] [decimal](9, 2) NOT NULL ,
	[AcquisitionPrice] [decimal](8, 2) NULL ,
	[Pack] [decimal](8, 2) NULL ,
	[MileageReceived] [int] NULL ,
	[TradeOrPurchase] [tinyint] NOT NULL ,
	[ListPrice] [decimal](8, 2) NULL ,
	[InitialVehicleLight] [int] NULL ,
	[Level4Analysis] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[InventoryType] [tinyint] NOT NULL ,
	[ReconditionCost] [decimal](8, 2) NULL ,
	[UsedSellingPrice] [decimal](8, 2) NULL ,
	[Certified] [tinyint] NOT NULL ,
	[VehicleLocation] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CurrentVehicleLight] [tinyint] NULL ,
	[FLRecFollowed] [tinyint] NOT NULL ,
	[Audit_ID] [int] NOT NULL ,
	[InventoryStatusCD] [smallint] NOT NULL ,
	[ListPriceLock] [tinyint] NOT NULL ,
	[SpecialFinance] [tinyint] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[JobTitle]    Script Date: 7/14/2005 11:25:22 AM ******/
CREATE TABLE [dbo].[JobTitle] (
	[id] [int] NOT NULL ,
	[title] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[KelleyBadVin]    Script Date: 7/14/2005 11:25:23 AM ******/
CREATE TABLE [dbo].[KelleyBadVin] (
	[KelleyBadVinId] [int] IDENTITY (10000, 1) NOT NULL ,
	[VIN] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[MakeModelGroupingMigration]    Script Date: 7/14/2005 11:25:23 AM ******/
CREATE TABLE [dbo].[MakeModelGroupingMigration] (
	[MakeModelGroupingID] [int] IDENTITY (100000, 1) NOT NULL ,
	[GroupingDescriptionID] [int] NULL ,
	[Make] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[OriginID] [int] NULL ,
	[CategoryID] [int] NULL ,
	[EdmundsBodyType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DisplayBodyTypeID] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[MarketShare]    Script Date: 7/14/2005 11:25:23 AM ******/
CREATE TABLE [dbo].[MarketShare] (
	[MarketShareID] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[GroupingDescriptionID] [int] NOT NULL ,
	[VehicleYear] [int] NOT NULL ,
	[MarketShare] [decimal](8, 2) NOT NULL ,
	[NumberOfWeeks] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[MarketToZip]    Script Date: 7/14/2005 11:25:24 AM ******/
CREATE TABLE [dbo].[MarketToZip] (
	[ZipCode] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[BusinessUnitId] [int] NOT NULL ,
	[Ring] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[Member]    Script Date: 7/14/2005 11:25:24 AM ******/
CREATE TABLE [dbo].[Member] (
	[MemberID] [int] IDENTITY (1, 1) NOT NULL ,
	[Login] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Password] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Salutation] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FirstName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[PreferredFirstName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MiddleInitial] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LastName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[JobTitle] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[OfficePhoneNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OfficePhoneExtension] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[OfficeFaxNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MobilePhoneNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PagerNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EmailAddress] [varchar] (129) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ReportMethod] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Notes] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DashboardRowDisplay] [int] NULL ,
	[ReportPreference] [int] NULL ,
	[LoginStatus] [int] NULL ,
	[MemberType] [int] NULL ,
	[CreateDate] [datetime] NOT NULL ,
	[UserRoleCD] [tinyint] NOT NULL ,
	[DefaultDealerGroupID] [int] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[MemberAccess]    Script Date: 7/14/2005 11:25:24 AM ******/
CREATE TABLE [dbo].[MemberAccess] (
	[MemberID] [int] NOT NULL ,
	[BusinessUnitID] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[MemberCredential]    Script Date: 7/14/2005 11:25:25 AM ******/
CREATE TABLE [dbo].[MemberCredential] (
	[MemberID] [int] NOT NULL ,
	[CredentialTypeID] [tinyint] NOT NULL ,
	[Username] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Password] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[MemberRole]    Script Date: 7/14/2005 11:25:25 AM ******/
CREATE TABLE [dbo].[MemberRole] (
	[MemberRoleID] [int] IDENTITY (1, 1) NOT NULL ,
	[MemberID] [int] NOT NULL ,
	[RoleID] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[ModelBodyStyleOverrides]    Script Date: 7/14/2005 11:25:25 AM ******/
CREATE TABLE [dbo].[ModelBodyStyleOverrides] (
	[Make] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Model] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[BodyType] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[EnhancedModel] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[OJB_DLIST]    Script Date: 7/14/2005 11:25:26 AM ******/
CREATE TABLE [dbo].[OJB_DLIST] (
	[ID] [int] NOT NULL ,
	[SIZE_] [int] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[OJB_DLIST_ENTRIES]    Script Date: 7/14/2005 11:25:26 AM ******/
CREATE TABLE [dbo].[OJB_DLIST_ENTRIES] (
	[ID] [int] NOT NULL ,
	[DLIST_ID] [int] NULL ,
	[POSITION_] [int] NULL ,
	[OID_] [image] NULL 
) ON [DATA] TEXTIMAGE_ON [DATA]
GO

/****** Object:  Table [dbo].[OJB_DMAP]    Script Date: 7/14/2005 11:25:26 AM ******/
CREATE TABLE [dbo].[OJB_DMAP] (
	[ID] [int] NOT NULL ,
	[SIZE_] [int] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[OJB_DMAP_ENTRIES]    Script Date: 7/14/2005 11:25:27 AM ******/
CREATE TABLE [dbo].[OJB_DMAP_ENTRIES] (
	[ID] [int] NOT NULL ,
	[DMAP_ID] [int] NULL ,
	[KEY_OID] [image] NULL ,
	[VALUE_OID] [image] NULL 
) ON [DATA] TEXTIMAGE_ON [DATA]
GO

/****** Object:  Table [dbo].[OJB_DSET]    Script Date: 7/14/2005 11:25:27 AM ******/
CREATE TABLE [dbo].[OJB_DSET] (
	[ID] [int] NOT NULL ,
	[SIZE_] [int] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[OJB_DSET_ENTRIES]    Script Date: 7/14/2005 11:25:27 AM ******/
CREATE TABLE [dbo].[OJB_DSET_ENTRIES] (
	[ID] [int] NOT NULL ,
	[DLIST_ID] [int] NULL ,
	[POSITION_] [int] NULL ,
	[OID_] [image] NULL 
) ON [DATA] TEXTIMAGE_ON [DATA]
GO

/****** Object:  Table [dbo].[OJB_HL_SEQ]    Script Date: 7/14/2005 11:25:28 AM ******/
CREATE TABLE [dbo].[OJB_HL_SEQ] (
	[TABLENAME] [varchar] (175) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[FIELDNAME] [varchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[MAX_KEY] [int] NULL ,
	[GRAB_SIZE] [int] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[OJB_LOCKENTRY]    Script Date: 7/14/2005 11:25:28 AM ******/
CREATE TABLE [dbo].[OJB_LOCKENTRY] (
	[OID_] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TX_ID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TIMESTAMP_] [timestamp] NULL ,
	[ISOLATIONLEVEL] [int] NULL ,
	[LOCKTYPE] [int] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[OJB_NRM]    Script Date: 7/14/2005 11:25:28 AM ******/
CREATE TABLE [dbo].[OJB_NRM] (
	[NAME] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OID_] [image] NULL 
) ON [DATA] TEXTIMAGE_ON [DATA]
GO

/****** Object:  Table [dbo].[OptionDetail]    Script Date: 7/14/2005 11:25:28 AM ******/
CREATE TABLE [dbo].[OptionDetail] (
	[OptionDetailID] [int] IDENTITY (1, 1) NOT NULL ,
	[OptionName] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OptionKey] [int] NOT NULL ,
	[DataSourceID] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[PurchasingCenterChannels]    Script Date: 7/14/2005 11:25:29 AM ******/
CREATE TABLE [dbo].[PurchasingCenterChannels] (
	[ChannelID] [int] IDENTITY (1, 1) NOT NULL ,
	[ChannelName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SearchFunctionName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[HasAccessGroups] [bit] NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[RedistributionAction]    Script Date: 7/14/2005 11:25:29 AM ******/
CREATE TABLE [dbo].[RedistributionAction] (
	[RedistributionActionID] [int] IDENTITY (1, 1) NOT NULL ,
	[Action] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[Role]    Script Date: 7/14/2005 11:25:29 AM ******/
CREATE TABLE [dbo].[Role] (
	[RoleID] [int] NOT NULL ,
	[Type] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Code] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[SingleVINDecodeTarget]    Script Date: 7/14/2005 11:25:30 AM ******/
CREATE TABLE [dbo].[SingleVINDecodeTarget] (
	[VIN] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[VehicleTrim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VehicleYear] [int] NULL ,
	[Make] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MakeModelGroupingID] [int] NULL ,
	[DoorCount] [int] NULL ,
	[VehicleBodyStyleID] [int] NULL ,
	[VehicleDriveTrain] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VehicleEngine] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CylinderCount] [int] NULL ,
	[FuelType] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VehicleTransmission] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[InteriorColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[OriginalBodyStyle] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[InteriorDescription] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[SiteAuditEvent]    Script Date: 7/14/2005 11:25:30 AM ******/
CREATE TABLE [dbo].[SiteAuditEvent] (
	[SiteAuditEventID] [int] IDENTITY (100000, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[MemberID] [int] NOT NULL ,
	[EventName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[EventData1] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EventData2] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EventData3] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EventData4] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EventData5] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EventData6] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CreateTimestamp] [datetime] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[Target]    Script Date: 7/14/2005 11:25:30 AM ******/
CREATE TABLE [dbo].[Target] (
	[TargetCD] [smallint] NOT NULL ,
	[TargetName] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TargetMin] [int] NULL ,
	[TargetMax] [int] NULL ,
	[SectionCD] [smallint] NOT NULL ,
	[TargetTypeCD] [tinyint] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[TargetSection]    Script Date: 7/14/2005 11:25:31 AM ******/
CREATE TABLE [dbo].[TargetSection] (
	[SectionCD] [smallint] NOT NULL ,
	[SectionName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[InventoryVehicleTypeCD] [tinyint] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[TargetType]    Script Date: 7/14/2005 11:25:31 AM ******/
CREATE TABLE [dbo].[TargetType] (
	[TargetTypeCD] [tinyint] NOT NULL ,
	[TargetTypeName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[TradeAnalyzerEvent]    Script Date: 7/14/2005 11:25:33 AM ******/
CREATE TABLE [dbo].[TradeAnalyzerEvent] (
	[TradeAnalyzerEventID] [int] IDENTITY (100000, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[MemberID] [int] NOT NULL ,
	[Vin] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Mileage] [int] NULL ,
	[Make] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Trim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[GuideBookID] [int] NOT NULL ,
	[GuideBookYear] [int] NULL ,
	[GuideBookMake] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[GuideBookSeries] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[GuideBookOptions] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[GuideBookValue1] [int] NOT NULL ,
	[GuideBookValue2] [int] NOT NULL ,
	[GuideBookValue3] [int] NOT NULL ,
	[GuideBookValue4] [int] NOT NULL ,
	[GeneralAverageGrossProfit] [int] NULL ,
	[GeneralAverageDaysToSale] [int] NULL ,
	[GeneralAverageMileage] [int] NULL ,
	[GeneralUnitsSold] [int] NULL ,
	[GeneralUnitsInStock] [int] NULL ,
	[CreateTimestamp] [datetime] NOT NULL ,
	[SpecificAverageDaysToSale] [int] NULL ,
	[SpecificAverageGrossProfit] [int] NULL ,
	[SpecificAverageMileage] [int] NULL ,
	[SpecificUnitsSold] [int] NULL ,
	[SpecificUnitsInStock] [int] NULL ,
	[SpecificNoSales] [int] NULL ,
	[GeneralNoSales] [int] NULL ,
	[ViewDealsDealerShipTrim] [tinyint] NULL ,
	[ViewDealsDealerShipOverall] [tinyint] NULL ,
	[ViewDealsDealerGroupTrim] [tinyint] NULL ,
	[ViewDealsDealerGroupOverall] [tinyint] NULL ,
	[ShowDealerGroupResults] [tinyint] NULL ,
	[HighGrossProfit] [tinyint] NULL ,
	[AvgGrossProfit] [tinyint] NULL ,
	[LowGrossProfit] [tinyint] NULL ,
	[FasterDaysToSell] [tinyint] NULL ,
	[AvgDaysToSell] [tinyint] NULL ,
	[SlowerDaysToSell] [tinyint] NULL ,
	[FrequentNoSales] [tinyint] NULL ,
	[HighMargin] [tinyint] NULL ,
	[AvgMargin] [tinyint] NULL ,
	[LowMargin] [tinyint] NULL ,
	[NoHistoricalPerformance] [tinyint] NULL ,
	[LimitedHistoricalPeformance] [tinyint] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[VehicleBodyStyle]    Script Date: 7/14/2005 11:25:34 AM ******/
CREATE TABLE [dbo].[VehicleBodyStyle] (
	[StandardBodyStyleID] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[VehicleBodyStyle] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[VehicleBodyStyleMapping]    Script Date: 7/14/2005 11:25:34 AM ******/
CREATE TABLE [dbo].[VehicleBodyStyleMapping] (
	[VehicleBodyStyleMappingID] [int] IDENTITY (1, 1) NOT NULL ,
	[StandardBodyStyleID] [tinyint] NOT NULL ,
	[OriginalVehicleBodyStyle] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[VehicleCategory]    Script Date: 7/14/2005 11:25:35 AM ******/
CREATE TABLE [dbo].[VehicleCategory] (
	[VehicleCategoryID] [int] IDENTITY (1, 1) NOT NULL ,
	[VehicleCategory] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VehicleCategoryCode] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[VehicleClass]    Script Date: 7/14/2005 11:25:35 AM ******/
CREATE TABLE [dbo].[VehicleClass] (
	[VehicleClassID] [int] IDENTITY (1, 1) NOT NULL ,
	[VehicleClass] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[VehicleCombination]    Script Date: 7/14/2005 11:25:35 AM ******/
CREATE TABLE [dbo].[VehicleCombination] (
	[VehicleCombinationID] [int] IDENTITY (1, 1) NOT NULL ,
	[VehicleYear] [int] NOT NULL ,
	[VehicleMake] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[VehicleModel] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[VehicleTrim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BodyDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [IDX]
GO

/****** Object:  Table [dbo].[VehicleLocatorContactDealerEvent]    Script Date: 7/14/2005 11:25:36 AM ******/
CREATE TABLE [dbo].[VehicleLocatorContactDealerEvent] (
	[VehicleLocatorContactDealerEventID] [int] IDENTITY (100000, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[SiteAuditEventID] [int] NOT NULL ,
	[VehicleID] [int] NULL ,
	[CreateTimestamp] [datetime] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[VehicleOption]    Script Date: 7/14/2005 11:25:36 AM ******/
CREATE TABLE [dbo].[VehicleOption] (
	[VehicleOptionID] [int] IDENTITY (1, 1) NOT NULL ,
	[VehicleID] [int] NOT NULL ,
	[OptionDetailID] [int] NOT NULL ,
	[OptionValue] [tinyint] NOT NULL ,
	[CreateDt] [datetime] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[VehicleOrigin]    Script Date: 7/14/2005 11:25:36 AM ******/
CREATE TABLE [dbo].[VehicleOrigin] (
	[VehicleOriginID] [int] IDENTITY (1, 1) NOT NULL ,
	[VehicleOriginCode] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[VehicleOrigin] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[VehiclePhoto]    Script Date: 7/14/2005 11:25:37 AM ******/
CREATE TABLE [dbo].[VehiclePhoto] (
	[VehicleID] [int] NOT NULL ,
	[VehiclePhotoFile] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[VehiclePlanTracking]    Script Date: 7/14/2005 11:25:37 AM ******/
CREATE TABLE [dbo].[VehiclePlanTracking] (
	[VehiclePlanTrackingID] [int] IDENTITY (100000, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[InventoryID] [int] NOT NULL ,
	[VehiclePlanTrackingHeaderID] [int] NOT NULL ,
	[Approach] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Date] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Created] [datetime] NULL ,
	[Notes] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Spiffs] [tinyint] NULL ,
	[Promos] [tinyint] NULL ,
	[Advertise] [tinyint] NULL ,
	[SalePending] [tinyint] NULL ,
	[Other] [tinyint] NULL ,
	[Wholesaler] [tinyint] NULL ,
	[Auction] [tinyint] NULL ,
	[NameText] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RetailDate] [datetime] NULL ,
	[WholesaleDate] [datetime] NULL ,
	[RangeID] [int] NULL ,
	[CurrentPlanVehicleAge] [int] NULL ,
	[Level4Analysis] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RETAIL] [tinyint] NULL ,
	[WHOLESALE] [tinyint] NULL ,
	[Reprice] [decimal](8, 2) NULL ,
	[SpiffNotes] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[VehiclePlanTrackingHeader]    Script Date: 7/14/2005 11:25:38 AM ******/
CREATE TABLE [dbo].[VehiclePlanTrackingHeader] (
	[VehiclePlanTrackingHeaderID] [int] IDENTITY (10000, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[VehicleCount] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ModelCount] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TotalInventoryDollars] [int] NULL ,
	[Highlights] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Status] [tinyint] NOT NULL ,
	[Date] [smalldatetime] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[VehicleSaleCommissions]    Script Date: 7/14/2005 11:25:38 AM ******/
CREATE TABLE [dbo].[VehicleSaleCommissions] (
	[InventoryID] [int] NOT NULL ,
	[SalesCommission] [decimal](8, 2) NULL ,
	[OtherCommission] [decimal](8, 2) NULL ,
	[SalespersonLastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SalesPersonFirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SalesPersonID] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[VehicleSalesHistoryAudit]    Script Date: 7/14/2005 11:25:39 AM ******/
CREATE TABLE [dbo].[VehicleSalesHistoryAudit] (
	[AuditID] [int] IDENTITY (100000, 1) NOT NULL ,
	[BusinessUnitID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SaleRefNum] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DealerStatus] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VIN] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[StockNum] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Year] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Make] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ModelPkg] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BodyType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VehClass] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ExtColor] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Mileage] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DealDT] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NetProfit] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NetFIIncome] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AcvTrade1] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AcvTrade2] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AcvTrade3] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LeaseFlag] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[GrossMargin] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SalePrice] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SaleDesc] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DealNumFI] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RefDT] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ExceptionCode] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ReceivedDT] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RawText] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ProcessDate] [datetime] NULL ,
	[ExistedPreviously] [tinyint] NULL ,
	[AcceptanceCode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AcceptanceMessage] [varchar] (3000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[VehicleSegment]    Script Date: 7/14/2005 11:25:39 AM ******/
CREATE TABLE [dbo].[VehicleSegment] (
	[VehicleSegmentID] [int] IDENTITY (10000, 1) NOT NULL ,
	[VehicleSegment] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_AccessGroupActor]    Script Date: 7/14/2005 11:25:40 AM ******/
CREATE TABLE [dbo].[lu_AccessGroupActor] (
	[AccessGroupActorId] [int] NOT NULL ,
	[Description] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_AccessGroupPrice]    Script Date: 7/14/2005 11:25:40 AM ******/
CREATE TABLE [dbo].[lu_AccessGroupPrice] (
	[AccessGroupPriceId] [int] NOT NULL ,
	[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_AnalyticalEngineStatus]    Script Date: 7/14/2005 11:25:40 AM ******/
CREATE TABLE [dbo].[lu_AnalyticalEngineStatus] (
	[analyticalenginestatus_CD] [tinyint] NOT NULL ,
	[analyticalenginestatus_DESC] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_AppraisalGuideBookValueTypes]    Script Date: 7/14/2005 11:25:41 AM ******/
CREATE TABLE [dbo].[lu_AppraisalGuideBookValueTypes] (
	[Id] [tinyint] NOT NULL ,
	[Description] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_AuditStatus]    Script Date: 7/14/2005 11:25:41 AM ******/
CREATE TABLE [dbo].[lu_AuditStatus] (
	[AuditStatusCD] [tinyint] NOT NULL ,
	[AuditStatusDESC] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ShortDesc] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_CIACategory]    Script Date: 7/14/2005 11:25:41 AM ******/
CREATE TABLE [dbo].[lu_CIACategory] (
	[CIACategoryId] [int] IDENTITY (1, 1) NOT NULL ,
	[CIACategoryDescription] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_CIAPlanType]    Script Date: 7/14/2005 11:25:42 AM ******/
CREATE TABLE [dbo].[lu_CIAPlanType] (
	[CIAPlanTypeId] [int] IDENTITY (1, 1) NOT NULL ,
	[Description] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_CIATimePeriod]    Script Date: 7/14/2005 11:25:42 AM ******/
CREATE TABLE [dbo].[lu_CIATimePeriod] (
	[CIATimePeriodId] [int] IDENTITY (1, 1) NOT NULL ,
	[Days] [int] NOT NULL ,
	[Forecast] [tinyint] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_CustomerType]    Script Date: 7/14/2005 11:25:42 AM ******/
CREATE TABLE [dbo].[lu_CustomerType] (
	[CustomerTypeCD] [tinyint] NOT NULL ,
	[CustomerTypeDESC] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_DMIVehicleStatus]    Script Date: 7/14/2005 11:25:42 AM ******/
CREATE TABLE [dbo].[lu_DMIVehicleStatus] (
	[DMIVehicleStatusCD] [smallint] IDENTITY (1, 1) NOT NULL ,
	[DMIVehicleStatusDESC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_DealStatus]    Script Date: 7/14/2005 11:25:43 AM ******/
CREATE TABLE [dbo].[lu_DealStatus] (
	[DealStatusCD] [tinyint] NOT NULL ,
	[DealStatusDesc] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_DealerPackType]    Script Date: 7/14/2005 11:25:43 AM ******/
CREATE TABLE [dbo].[lu_DealerPackType] (
	[PackTypeCD] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[PackTypeDESC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_DealerUpgrade]    Script Date: 7/14/2005 11:25:43 AM ******/
CREATE TABLE [dbo].[lu_DealerUpgrade] (
	[DealerUpgradeCD] [tinyint] NOT NULL ,
	[DealerUpgradeDESC] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_DecodedSquishVinSource]    Script Date: 7/14/2005 11:25:44 AM ******/
CREATE TABLE [dbo].[lu_DecodedSquishVinSource] (
	[dsvSourceID] [tinyint] NOT NULL ,
	[dsvSourceDESC] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_DisplayBodyType]    Script Date: 7/14/2005 11:25:44 AM ******/
CREATE TABLE [dbo].[lu_DisplayBodyType] (
	[DisplayBodyTypeID] [int] IDENTITY (1, 1) NOT NULL ,
	[DisplayBodyType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_Disposition]    Script Date: 7/14/2005 11:25:44 AM ******/
CREATE TABLE [dbo].[lu_Disposition] (
	[DispositionCD] [tinyint] NOT NULL ,
	[DispositionDESC] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_Edmunds_Colors_New]    Script Date: 7/14/2005 11:25:45 AM ******/
CREATE TABLE [dbo].[lu_Edmunds_Colors_New] (
	[color_id] [int] NULL ,
	[ed_style_id] [int] NULL ,
	[basic_color_name] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[mfgr_color_name] [varchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[mfgr_color_code] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[interior_exterior_flag] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[r_code] [int] NULL ,
	[g_code] [int] NULL ,
	[b_code] [int] NULL ,
	[c_code] [int] NULL ,
	[m_code] [int] NULL ,
	[y_code] [int] NULL ,
	[k_code] [int] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_Edmunds_Colors_New_Backup]    Script Date: 7/14/2005 11:25:45 AM ******/
CREATE TABLE [dbo].[lu_Edmunds_Colors_New_Backup] (
	[color_id] [int] NULL ,
	[ed_style_id] [int] NULL ,
	[basic_color_name] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[mfgr_color_name] [varchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[mfgr_color_code] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[interior_exterior_flag] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[r_code] [int] NULL ,
	[g_code] [int] NULL ,
	[b_code] [int] NULL ,
	[c_code] [int] NULL ,
	[m_code] [int] NULL ,
	[y_code] [int] NULL ,
	[k_code] [int] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_Edmunds_Colors_Used]    Script Date: 7/14/2005 11:25:45 AM ******/
CREATE TABLE [dbo].[lu_Edmunds_Colors_Used] (
	[color_id] [int] NULL ,
	[ed_style_id] [int] NULL ,
	[basic_color_name] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[mfgr_color_name] [varchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[mfgr_color_code] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[interior_exterior_flag] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[r_code] [int] NULL ,
	[g_code] [int] NULL ,
	[b_code] [int] NULL ,
	[c_code] [int] NULL ,
	[m_code] [int] NULL ,
	[y_code] [int] NULL ,
	[k_code] [int] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_Edmunds_Colors_Used_Backup]    Script Date: 7/14/2005 11:25:45 AM ******/
CREATE TABLE [dbo].[lu_Edmunds_Colors_Used_Backup] (
	[color_id] [int] NULL ,
	[ed_style_id] [int] NULL ,
	[basic_color_name] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[mfgr_color_name] [varchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[mfgr_color_code] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[interior_exterior_flag] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[r_code] [int] NULL ,
	[g_code] [int] NULL ,
	[b_code] [int] NULL ,
	[c_code] [int] NULL ,
	[m_code] [int] NULL ,
	[y_code] [int] NULL ,
	[k_code] [int] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_Edmunds_Squishvin]    Script Date: 7/14/2005 11:25:45 AM ******/
CREATE TABLE [dbo].[lu_Edmunds_Squishvin] (
	[squish_vin] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ed_style_id] [int] NULL ,
	[used_ed_style_id] [int] NULL ,
	[ed_model_id] [int] NULL ,
	[year] [int] NULL ,
	[make] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[model] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[style] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[mfr_style_code] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[doors] [tinyint] NULL ,
	[body_type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[drive_type_code] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[standard_flag] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[engine_block_configuration] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[cylinder_qty] [tinyint] NULL ,
	[engine_displacement] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[engine_displacement_uom] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[cam_type] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[fuel_induction] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[valves_cylinder] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[aspiration] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[fuel_type] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[tran_type] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[tran_speed] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[restraint_system] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[gvwr] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[plant] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_Edmunds_Style_New]    Script Date: 7/14/2005 11:25:46 AM ******/
CREATE TABLE [dbo].[lu_Edmunds_Style_New] (
	[ed_style_id] [int] NOT NULL ,
	[ed_model_id] [int] NULL ,
	[make] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[model] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[style] [varchar] (65) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[year] [int] NULL ,
	[midyear] [tinyint] NULL ,
	[trim_line] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[mfr_style_code] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[model_year_link_code] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[vehicle_type] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[vehicle_subtype] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[doors] [int] NULL 
) ON [DATA]
GO


/****** Object:  Table [dbo].[lu_Edmunds_Style_Used]    Script Date: 7/14/2005 11:25:46 AM ******/
CREATE TABLE [dbo].[lu_Edmunds_Style_Used] (
	[ed_style_id] [int] NOT NULL ,
	[ed_model_id] [int] NULL ,
	[make] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[model] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[style] [varchar] (65) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[year] [int] NULL ,
	[midyear] [tinyint] NULL ,
	[trim_line] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[mfr_style_code] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[model_year_link_code] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[vehicle_type] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[vehicle_subtype] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[doors] [int] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_FuelType]    Script Date: 7/14/2005 11:25:47 AM ******/
CREATE TABLE [dbo].[lu_FuelType] (
	[FuelTypeCD] [smallint] IDENTITY (1, 1) NOT NULL ,
	[FuelTypeDESC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_GuideBook]    Script Date: 7/14/2005 11:25:47 AM ******/
CREATE TABLE [dbo].[lu_GuideBook] (
	[GuideBookId] [tinyint] NOT NULL ,
	[GuideBookDescription] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[GuideBookName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_InventoryVehicleLight]    Script Date: 7/14/2005 11:25:48 AM ******/
CREATE TABLE [dbo].[lu_InventoryVehicleLight] (
	[InventoryVehicleLightCD] [tinyint] NOT NULL ,
	[InventoryVehicleLightDesc] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_InventoryVehicleType]    Script Date: 7/14/2005 11:25:48 AM ******/
CREATE TABLE [dbo].[lu_InventoryVehicleType] (
	[InventoryVehicleTypeCD] [tinyint] NOT NULL ,
	[InventoryVehicleTypeDescription] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_OptionType]    Script Date: 7/14/2005 11:25:48 AM ******/
CREATE TABLE [dbo].[lu_OptionType] (
	[Id] [tinyint] NOT NULL ,
	[Description] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_ProgramType]    Script Date: 7/14/2005 11:25:49 AM ******/
CREATE TABLE [dbo].[lu_ProgramType] (
	[ProgramType_CD] [tinyint] NOT NULL ,
	[ProgramTypeName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_RedistributionSource]    Script Date: 7/14/2005 11:25:49 AM ******/
CREATE TABLE [dbo].[lu_RedistributionSource] (
	[RedistributionSourceId] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[Description] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_SaleDescription]    Script Date: 7/14/2005 11:25:49 AM ******/
CREATE TABLE [dbo].[lu_SaleDescription] (
	[SaleDescriptionCD] [tinyint] NOT NULL ,
	[SaleDescription] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_States]    Script Date: 7/14/2005 11:25:49 AM ******/
CREATE TABLE [dbo].[lu_States] (
	[StateID] [tinyint] NOT NULL ,
	[StateShort] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[StateLong] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_Status]    Script Date: 7/14/2005 11:25:50 AM ******/
CREATE TABLE [dbo].[lu_Status] (
	[StatusId] [tinyint] NOT NULL ,
	[StatusName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_ThirdParty]    Script Date: 7/14/2005 11:25:50 AM ******/
CREATE TABLE [dbo].[lu_ThirdParty] (
	[ThirdPartyId] [tinyint] NOT NULL ,
	[ThirdPartyDescription] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ThirdPartyName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DefaultFooter] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_TradeOrPurchase]    Script Date: 7/14/2005 11:25:51 AM ******/
CREATE TABLE [dbo].[lu_TradeOrPurchase] (
	[TradeOrPurchaseCD] [tinyint] NOT NULL ,
	[TradeOrPurchaseDesc] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_UserRole]    Script Date: 7/14/2005 11:25:51 AM ******/
CREATE TABLE [dbo].[lu_UserRole] (
	[UserRoleCD] [tinyint] NOT NULL ,
	[UserRoleDesc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_VehicleLight]    Script Date: 7/14/2005 11:25:51 AM ******/
CREATE TABLE [dbo].[lu_VehicleLight] (
	[InventoryVehicleLightID] [tinyint] NOT NULL ,
	[InventoryVehicleLightDESC] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_VehicleSegment]    Script Date: 7/14/2005 11:25:52 AM ******/
CREATE TABLE [dbo].[lu_VehicleSegment] (
	[vehiclesegmentid] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[vehiclesegmentdesc] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_VehicleSubSegment]    Script Date: 7/14/2005 11:25:52 AM ******/
CREATE TABLE [dbo].[lu_VehicleSubSegment] (
	[VehicleSubSegmentID] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[VehicleSubSegmentDESC] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_VehicleSubType]    Script Date: 7/14/2005 11:25:52 AM ******/
CREATE TABLE [dbo].[lu_VehicleSubType] (
	[VehicleSubTypeID] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[VehicleSubTypeDESC] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[lu_VehicleType]    Script Date: 7/14/2005 11:25:53 AM ******/
CREATE TABLE [dbo].[lu_VehicleType] (
	[VehicleTypeID] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[VehicleTypeDesc] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[map_BusinessUnitToMarketDealer]    Script Date: 7/14/2005 11:25:53 AM ******/
CREATE TABLE [dbo].[map_BusinessUnitToMarketDealer] (
	[ID] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[DealerNumber] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[map_FLTypeClassToSegments]    Script Date: 7/14/2005 11:25:53 AM ******/
CREATE TABLE [dbo].[map_FLTypeClassToSegments] (
	[FLTypeClassID] [smallint] IDENTITY (1, 1) NOT NULL ,
	[VehicleSegmentID] [tinyint] NOT NULL ,
	[VehicleSubSegmentID] [tinyint] NOT NULL ,
	[FLTypeClassOrder] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[map_MemberToInvStatusFilter]    Script Date: 7/14/2005 11:25:54 AM ******/
CREATE TABLE [dbo].[map_MemberToInvStatusFilter] (
	[MemberToInvStatusFilterId] [int] IDENTITY (1, 1) NOT NULL ,
	[MemberID] [int] NOT NULL ,
	[InventoryStatusCD] [smallint] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[map_VehicleSubTypeToSubSegment]    Script Date: 7/14/2005 11:25:54 AM ******/
CREATE TABLE [dbo].[map_VehicleSubTypeToSubSegment] (
	[VehicleSubTypeID] [tinyint] NOT NULL ,
	[VehicleSubSegmentID] [tinyint] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[map_VehicleTypeToSegment]    Script Date: 7/14/2005 11:25:54 AM ******/
CREATE TABLE [dbo].[map_VehicleTypeToSegment] (
	[VehicleTypeID] [tinyint] NOT NULL ,
	[VehicleSegmentID] [tinyint] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[rundayofweek]    Script Date: 7/14/2005 11:25:55 AM ******/
CREATE TABLE [dbo].[rundayofweek] (
	[businessunitid] [int] NOT NULL ,
	[rundayofweek] [int] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_BookOutMod]    Script Date: 7/14/2005 11:25:55 AM ******/
CREATE TABLE [dbo].[tbl_BookOutMod] (
	[WasActive] [bit] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_CIABodyTypeDetail]    Script Date: 7/14/2005 11:25:55 AM ******/
CREATE TABLE [dbo].[tbl_CIABodyTypeDetail] (
	[CIABodyTypeDetailId] [int] IDENTITY (1, 1) NOT NULL ,
	[DisplayBodyTypeId] [int] NOT NULL ,
	[CIASummaryId] [int] NOT NULL ,
	[DisplayOrder] [int] NULL ,
	[NonCoreTargetUnits] [int] NOT NULL ,
	[TotalTargetUnits] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_CIACompositeTimePeriod]    Script Date: 7/14/2005 11:25:56 AM ******/
CREATE TABLE [dbo].[tbl_CIACompositeTimePeriod] (
	[CIACompositeTimePeriodId] [int] IDENTITY (1, 1) NOT NULL ,
	[PriorTimePeriodId] [int] NOT NULL ,
	[ForecastTimePeriodId] [int] NOT NULL ,
	[Description] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_CIAGroupingItem]    Script Date: 7/14/2005 11:25:57 AM ******/
CREATE TABLE [dbo].[tbl_CIAGroupingItem] (
	[CIAGroupingItemId] [int] IDENTITY (1, 1) NOT NULL ,
	[CIAInventoryStockingId] [int] NOT NULL ,
	[GroupingDescriptionId] [int] NOT NULL ,
	[CIACategoryId] [int] NOT NULL ,
	[MarketShare] [decimal](4, 4) NULL ,
	[Valuation] [decimal](10, 2) NULL ,
	[MarketPenetration] [decimal](10, 4) NULL ,
	[Notes] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_CIAInventoryStocking]    Script Date: 7/14/2005 11:25:58 AM ******/
CREATE TABLE [dbo].[tbl_CIAInventoryStocking] (
	[CIAInventoryStockingId] [int] IDENTITY (1, 1) NOT NULL ,
	[CIACategoryId] [int] NOT NULL ,
	[CIABodyTypeDetailId] [int] NOT NULL ,
	[TargetUnits] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_CIAPlan]    Script Date: 7/14/2005 11:25:58 AM ******/
CREATE TABLE [dbo].[tbl_CIAPlan] (
	[CIAPlanId] [int] IDENTITY (1, 1) NOT NULL ,
	[CIAGroupingItemId] [int] NOT NULL ,
	[Avoid] [bit] NULL ,
	[Prefer] [bit] NULL ,
	[CIAPlanTypeId] [int] NOT NULL ,
	[Value] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_CIAPreferences]    Script Date: 7/14/2005 11:25:59 AM ******/
CREATE TABLE [dbo].[tbl_CIAPreferences] (
	[BusinessUnitId] [int] NOT NULL ,
	[PowerZoneGroupingThreshold] [int] NOT NULL ,
	[BucketAllocationMinimumThreshold] [int] NOT NULL ,
	[TargetDaysSupply] [int] NOT NULL ,
	[MarketPerformersDisplayThreshold] [int] NOT NULL ,
	[MarketPerformersInStockThreshold] [int] NOT NULL ,
	[MarketPerformersUnitsThreshold] [int] NOT NULL ,
	[MarketPerformersZipCodeThreshold] [decimal](9, 2) NOT NULL ,
	[CIAAllocationTimePeriodId] [int] NOT NULL ,
	[CIATargetUnitsTimePeriodId] [int] NOT NULL ,
	[GDLightProcessorTimePeriodId] [int] NOT NULL ,
	[SalesHistoryDisplayTimePeriodId] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_CIASummary]    Script Date: 7/14/2005 11:25:59 AM ******/
CREATE TABLE [dbo].[tbl_CIASummary] (
	[CIASummaryId] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitId] [int] NOT NULL ,
	[StatusId] [tinyint] NOT NULL ,
	[DT] [smalldatetime] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_CIAUnitCostPointPlan]    Script Date: 7/14/2005 11:26:00 AM ******/
CREATE TABLE [dbo].[tbl_CIAUnitCostPointPlan] (
	[CIAUnitCostPointRangeId] [int] NOT NULL ,
	[BuyAmount] [int] NULL ,
	[Avoid] [bit] NULL ,
	[MinValue] [int] NULL ,
	[MaxValue] [int] NULL ,
	[UserSelected] [tinyint] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_CIAUnitCostPointRange]    Script Date: 7/14/2005 11:26:01 AM ******/
CREATE TABLE [dbo].[tbl_CIAUnitCostPointRange] (
	[CIAUnitCostPointRangeId] [int] IDENTITY (1, 1) NOT NULL ,
	[CIAUnitCostPointRangeIndex] [int] NOT NULL ,
	[CIAGroupingItemId] [int] NOT NULL ,
	[RangeLevelTargetUnits] [int] NOT NULL ,
	[UpperRange] [int] NOT NULL ,
	[LowerRange] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_DealerATCAccessGroups]    Script Date: 7/14/2005 11:26:02 AM ******/
CREATE TABLE [dbo].[tbl_DealerATCAccessGroups] (
	[DealerATCAccessGroupsId] [int] IDENTITY (10000, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[AccessGroupId] [int] NOT NULL ,
	[AccessGroupPriceId] [int] NOT NULL ,
	[AccessGroupActorId] [int] NOT NULL ,
	[AccessGroupValue] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_DealerValuation]    Script Date: 7/14/2005 11:26:02 AM ******/
CREATE TABLE [dbo].[tbl_DealerValuation] (
	[DealerValuationId] [int] IDENTITY (1, 1) NOT NULL ,
	[DealerId] [int] NOT NULL ,
	[DiscountRate] [decimal](6, 4) NULL ,
	[TimeHorizon] [decimal](8, 4) NULL ,
	[Date] [datetime] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_GroupingDescription]    Script Date: 7/14/2005 11:26:02 AM ******/
CREATE TABLE [dbo].[tbl_GroupingDescription] (
	[GroupingDescriptionID] [int] IDENTITY (100000, 1) NOT NULL ,
	[GroupingDescription] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SuppressMarketPerformer] [bit] NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_GroupingPromotionPlan]    Script Date: 7/14/2005 11:26:03 AM ******/
CREATE TABLE [dbo].[tbl_GroupingPromotionPlan] (
	[GroupingPromotionPlanId] [int] IDENTITY (1, 1) NOT NULL ,
	[VehiclePlanTrackingHeaderId] [int] NOT NULL ,
	[GroupingDescriptionId] [int] NOT NULL ,
	[PromotionStartDate] [smalldatetime] NOT NULL ,
	[PromotionEndDate] [smalldatetime] NOT NULL ,
	[Notes] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_InventoryOverstocking]    Script Date: 7/14/2005 11:26:03 AM ******/
CREATE TABLE [dbo].[tbl_InventoryOverstocking] (
	[InventoryOverstockingId] [int] IDENTITY (1, 1) NOT NULL ,
	[CIAUnitCostPointRangeId] [int] NULL ,
	[InventoryId] [int] NOT NULL ,
	[StatusId] [tinyint] NOT NULL ,
	[OverstockingDate] [smalldatetime] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_MakeModelGrouping]    Script Date: 7/14/2005 11:26:03 AM ******/
CREATE TABLE [dbo].[tbl_MakeModelGrouping] (
	[MakeModelGroupingID] [int] IDENTITY (100000, 1) NOT NULL ,
	[GroupingDescriptionID] [int] NULL ,
	[Make] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[OriginID] [int] NULL ,
	[CategoryID] [int] NULL ,
	[EdmundsBodyType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DisplayBodyTypeID] [int] NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[tbl_NewCarScoreCard]    Script Date: 7/14/2005 11:26:04 AM ******/
CREATE TABLE [dbo].[tbl_NewCarScoreCard] (
	[NewCarScoreCardId] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitId] [int] NOT NULL ,
	[Active] [tinyint] NOT NULL ,
	[WeekEnding] [smalldatetime] NOT NULL ,
	[WeekNumber] [int] NOT NULL ,
	[OverallUnitSaleCurrent] [int] NOT NULL ,
	[OverallUnitSalePrior] [int] NOT NULL ,
	[OverallUnitSaleThreeMonth] [decimal](8, 2) NOT NULL ,
	[OverallRetailAvgGrossProfitTrend] [decimal](8, 2) NOT NULL ,
	[OverallRetailAvgGrossProfit12Week] [decimal](8, 2) NOT NULL ,
	[OverallFAndIAvgGrossProfitTrend] [decimal](8, 2) NOT NULL ,
	[OverallFAndIAvgGrossProfit12Week] [decimal](8, 2) NOT NULL ,
	[OverallAvgDaysToSaleTrend] [decimal](8, 2) NOT NULL ,
	[OverallAvgDaysToSale12Week] [decimal](8, 2) NOT NULL ,
	[OverallAvgInventoryAgeCurrent] [decimal](8, 2) NOT NULL ,
	[OverallAvgInventoryAgePrior] [decimal](8, 2) NOT NULL ,
	[OverallCurrentDaysSupplyCurrent] [decimal](8, 2) NOT NULL ,
	[OverallCurrentDaysSupplyPrior] [decimal](8, 2) NOT NULL ,
	[AgingOver90Current] [decimal](8, 2) NOT NULL ,
	[AgingOver90Prior] [decimal](8, 2) NOT NULL ,
	[Aging76To90Current] [decimal](8, 2) NOT NULL ,
	[Aging76To90Prior] [decimal](8, 2) NOT NULL ,
	[Aging61To75Current] [decimal](8, 2) NOT NULL ,
	[Aging61To75Prior] [decimal](8, 2) NOT NULL ,
	[Aging31To60Current] [decimal](8, 2) NOT NULL ,
	[Aging31To60Prior] [decimal](8, 2) NOT NULL ,
	[Aging0To30Current] [decimal](8, 2) NOT NULL ,
	[Aging0To30Prior] [decimal](8, 2) NOT NULL ,
	[CoupePercentOfSalesTrend] [int] NOT NULL ,
	[CoupePercentOfSales12Week] [int] NOT NULL ,
	[CoupeRetailAvgGrossProfitTrend] [int] NOT NULL ,
	[CoupeRetailAvgGrossProfit12Week] [int] NOT NULL ,
	[CoupeRetailAvgDaysToSaleTrend] [int] NOT NULL ,
	[CoupeRetailAvgDaysToSale12Week] [int] NOT NULL ,
	[SedanPercentOfSalesTrend] [int] NOT NULL ,
	[SedanPercentOfSales12Week] [int] NOT NULL ,
	[SedanRetailAvgGrossProfitTrend] [int] NOT NULL ,
	[SedanRetailAvgGrossProfit12Week] [int] NOT NULL ,
	[SedanRetailAvgDaysToSaleTrend] [int] NOT NULL ,
	[SedanRetailAvgDaysToSale12Week] [int] NOT NULL ,
	[SUVPercentOfSalesTrend] [int] NOT NULL ,
	[SUVPercentOfSales12Week] [int] NOT NULL ,
	[SUVRetailAvgGrossProfitTrend] [int] NOT NULL ,
	[SUVRetailAvgGrossProfit12Week] [int] NOT NULL ,
	[SUVRetailAvgDaysToSaleTrend] [int] NOT NULL ,
	[SUVRetailAvgDaysToSale12Week] [int] NOT NULL ,
	[TruckPercentOfSalesTrend] [int] NOT NULL ,
	[TruckPercentOfSales12Week] [int] NOT NULL ,
	[TruckRetailAvgGrossProfitTrend] [int] NOT NULL ,
	[TruckRetailAvgGrossProfit12Week] [int] NOT NULL ,
	[TruckRetailAvgDaysToSaleTrend] [int] NOT NULL ,
	[TruckRetailAvgDaysToSale12Week] [int] NOT NULL ,
	[VanPercentOfSalesTrend] [int] NOT NULL ,
	[VanPercentOfSales12Week] [int] NOT NULL ,
	[VanRetailAvgGrossProfitTrend] [int] NOT NULL ,
	[VanRetailAvgGrossProfit12Week] [int] NOT NULL ,
	[VanRetailAvgDaysToSaleTrend] [int] NOT NULL ,
	[VanRetailAvgDaysToSale12Week] [int] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_RedistributionLog]    Script Date: 7/14/2005 11:26:04 AM ******/
CREATE TABLE [dbo].[tbl_RedistributionLog] (
	[RedistributionLogId] [int] IDENTITY (1, 1) NOT NULL ,
	[VIN] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OriginatingDealerId] [int] NOT NULL ,
	[DestinationDealerId] [int] NULL ,
	[RedistributionSourceId] [tinyint] NOT NULL ,
	[ReceivedDate] [smalldatetime] NULL ,
	[OriginatingDealerStockNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DestinationDealerStockNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MemberId] [int] NOT NULL ,
	[TransactionDate] [smalldatetime] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_UsedCarScoreCard]    Script Date: 7/14/2005 11:26:05 AM ******/
CREATE TABLE [dbo].[tbl_UsedCarScoreCard] (
	[UsedCarScoreCardId] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitId] [int] NOT NULL ,
	[Active] [tinyint] NOT NULL ,
	[WeekEnding] [smalldatetime] NOT NULL ,
	[WeekNumber] [int] NOT NULL ,
	[OverallUnitSaleCurrent] [int] NOT NULL ,
	[OverallUnitSalePrior] [int] NOT NULL ,
	[OverallUnitSaleThreeMonth] [decimal](8, 2) NOT NULL ,
	[OverallRetailAvgGrossProfitTrend] [decimal](8, 2) NOT NULL ,
	[OverallRetailAvgGrossProfit12Week] [decimal](8, 2) NOT NULL ,
	[OverallFAndIAvgGrossProfitTrend] [decimal](8, 2) NOT NULL ,
	[OverallFAndIAvgGrossProfit12Week] [decimal](8, 2) NOT NULL ,
	[OverallPercentSellThroughTrend] [decimal](8, 2) NOT NULL ,
	[OverallPercentSellThrough12Week] [decimal](8, 2) NOT NULL ,
	[OverallAvgDaysToSaleTrend] [decimal](8, 2) NOT NULL ,
	[OverallAvgDaysToSale12Week] [decimal](8, 2) NOT NULL ,
	[OverallAvgInventoryAgeCurrent] [decimal](8, 2) NOT NULL ,
	[OverallAvgInventoryAgePrior] [decimal](8, 2) NOT NULL ,
	[OverallCurrentDaysSupplyCurrent] [decimal](8, 2) NOT NULL ,
	[OverallCurrentDaysSupplyPrior] [decimal](8, 2) NOT NULL ,
	[Aging60PlusCurrent] [decimal](8, 2) NOT NULL ,
	[Aging60PlusPrior] [decimal](8, 2) NOT NULL ,
	[Aging50To59Current] [decimal](8, 2) NOT NULL ,
	[Aging50To59Prior] [decimal](8, 2) NOT NULL ,
	[Aging40To49Current] [decimal](8, 2) NOT NULL ,
	[Aging40To49Prior] [decimal](8, 2) NOT NULL ,
	[Aging30To39Current] [decimal](8, 2) NOT NULL ,
	[Aging30To39Prior] [decimal](8, 2) NOT NULL ,
	[PurchasedPercentWinnersTrend] [decimal](8, 2) NOT NULL ,
	[PurchasedPercentWinners12Week] [decimal](8, 2) NOT NULL ,
	[PurchasedRetailAvgGrossProfitTrend] [decimal](8, 2) NOT NULL ,
	[PurchasedRetailAvgGrossProfit12Week] [decimal](8, 2) NOT NULL ,
	[PurchasedPercentSellThroughTrend] [decimal](8, 2) NOT NULL ,
	[PurchasedPercentSellThrough12Week] [decimal](8, 2) NOT NULL ,
	[PurchasedAvgDaysToSaleTrend] [decimal](8, 2) NOT NULL ,
	[PurchasedAvgDaysToSale12Week] [decimal](8, 2) NOT NULL ,
	[PurchasedAvgInventoryAgeCurrent] [decimal](8, 2) NOT NULL ,
	[PurchasedAvgInventoryAgePrior] [decimal](8, 2) NOT NULL ,
	[TradeInPercentNonWinnersWholesaledTrend] [decimal](8, 2) NOT NULL ,
	[TradeInPercentNonWinnersWholesaled12Week] [decimal](8, 2) NOT NULL ,
	[TradeInRetailAvgGrossProfitTrend] [decimal](8, 2) NOT NULL ,
	[TradeInRetailAvgGrossProfit12Week] [decimal](8, 2) NOT NULL ,
	[TradeInPercentSellThroughTrend] [decimal](8, 2) NOT NULL ,
	[TradeInPercentSellThrough12Week] [decimal](8, 2) NOT NULL ,
	[TradeInAvgDaysToSaleTrend] [decimal](8, 2) NOT NULL ,
	[TradeInAvgDaysToSale12Week] [decimal](8, 2) NOT NULL ,
	[TradeInAvgInventoryAgeCurrent] [decimal](8, 2) NOT NULL ,
	[TradeInAvgInventoryAgePrior] [decimal](8, 2) NOT NULL ,
	[WholesaleImmediateAvgGrossProfitTrend] [decimal](8, 2) NOT NULL ,
	[WholesaleImmediateAvgGrossProfit12Week] [decimal](8, 2) NOT NULL ,
	[WholesaleAgedInventoryAvgGrossProfitTrend] [decimal](8, 2) NOT NULL ,
	[WholesaleAgedInventoryAvgGrossProfit12Week] [decimal](8, 2) NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_Vehicle]    Script Date: 7/14/2005 11:26:05 AM ******/
CREATE TABLE [dbo].[tbl_Vehicle] (
	[VehicleID] [int] IDENTITY (1, 1) NOT NULL ,
	[Vin] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[VehicleYear] [int] NOT NULL ,
	[MakeModelGroupingID] [int] NOT NULL ,
	[VehicleTrim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[VehicleBodyStyleID] [int] NOT NULL ,
	[VehicleEngine] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[VehicleDriveTrain] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[VehicleTransmission] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CylinderCount] [int] NULL ,
	[DoorCount] [int] NULL ,
	[BaseColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[InteriorColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FuelType] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[InteriorDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CreateDt] [smalldatetime] NOT NULL ,
	[OriginalMake] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[OriginalModel] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[OriginalYear] [smallint] NULL ,
	[OriginalTrim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[OriginalBodyStyle] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Audit_ID] [int] NOT NULL
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_VehicleAttributes]    Script Date: 7/14/2005 11:26:06 AM ******/
CREATE TABLE [dbo].[tbl_VehicleAttributes] (
	[VehicleID] [int] NOT NULL ,
	[VehicleSegmentID] [tinyint] NOT NULL ,
	[VehicleSubSegmentID] [tinyint] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_VehicleGuideBookIdentifier]    Script Date: 7/14/2005 11:26:06 AM ******/
CREATE TABLE [dbo].[tbl_VehicleGuideBookIdentifier] (
	[VehicleID] [int] NOT NULL ,
	[GuideBookID] [tinyint] NOT NULL ,
	[Identifier] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_VehicleSale]    Script Date: 7/14/2005 11:26:07 AM ******/
CREATE TABLE [dbo].[tbl_VehicleSale] (
	[InventoryID] [int] NOT NULL ,
	[SalesReferenceNumber] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[VehicleMileage] [int] NULL ,
	[DealDate] [smalldatetime] NOT NULL ,
	[BackEndGross] [decimal](8, 2) NOT NULL ,
	[NetFinancialInsuranceIncome] [decimal](8, 2) NULL ,
	[OriginalFrontEndGross] [decimal](9, 2) NULL ,
	[FrontEndGross] [decimal](9, 2) NOT NULL ,
	[SalePrice] [decimal](9, 2) NOT NULL ,
	[SaleDescription] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[FinanceInsuranceDealNumber] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LastPolledDate] [smalldatetime] NOT NULL ,
	[AnalyticalEngineStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AnalyticalEngineStatus_CD] [int] NULL ,
	[LeaseFlag] [tinyint] NULL ,
	[Pack] [decimal](8, 2) NULL ,
	[AfterMarketGross] [decimal](8, 2) NOT NULL ,
	[TotalGross] [decimal](8, 2) NOT NULL ,
	[VehiclePromotionAmount] [decimal](8, 2) NULL ,
	[DealStatusCD] [tinyint] NOT NULL ,
	[PackAdjust] [decimal](10, 2) NULL ,
	[Audit_ID] [int] NOT NULL ,
	[Valuation] [float] NULL ,
	[CustomerZip] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_VehicleSaleReversals]    Script Date: 7/14/2005 11:26:08 AM ******/
CREATE TABLE [dbo].[tbl_VehicleSaleReversals] (
	[InventoryID] [int] NOT NULL ,
	[SalesReferenceNumber] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VehicleMileage] [int] NULL ,
	[DealDate] [smalldatetime] NOT NULL ,
	[BackEndGross] [decimal](8, 2) NULL ,
	[NetFinancialInsuranceIncome] [decimal](18, 0) NULL ,
	[OriginalFrontEndGross] [decimal](8, 2) NULL ,
	[FrontEndGross] [decimal](18, 0) NOT NULL ,
	[SalePrice] [decimal](9, 2) NOT NULL ,
	[SaleDescription] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[FinanceInsuranceDealNumber] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LastPolledDate] [smalldatetime] NOT NULL ,
	[AnalyticalEngineStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AnalyticalEngineStatus_CD] [int] NULL ,
	[LeaseFlag] [tinyint] NULL ,
	[Pack] [decimal](8, 2) NULL ,
	[AfterMarketGross] [decimal](8, 2) NULL ,
	[TotalGross] [decimal](8, 2) NOT NULL ,
	[VehiclePromotionAmount] [decimal](8, 2) NULL ,
	[DealStatusCD] [tinyint] NULL ,
	[PackAdjust] [decimal](10, 2) NULL ,
	[AUDIT_ID] [int] NOT NULL ,
	[valuation] [float] NULL ,
	[CustomerZip] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_VehicleSegmentOverrides]    Script Date: 7/14/2005 11:26:08 AM ******/
CREATE TABLE [dbo].[tbl_VehicleSegmentOverrides] (
	[VehicleSegmentID] [tinyint] NOT NULL ,
	[VehicleSubSegmentID] [tinyint] NOT NULL ,
	[VehicleSegmentIDOverride] [tinyint] NOT NULL ,
	[VehicleSubSegmentIDOverride] [tinyint] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_dbVersion]    Script Date: 7/14/2005 11:26:09 AM ******/
CREATE TABLE [dbo].[tbl_dbVersion] (
	[dbVersionID] [int] NOT NULL ,
	[dbVersionTS] [datetime] NOT NULL 
) ON [DATA]
GO

/****** Object:  Table [dbo].[tbl_tmpRunDayOfWeek]    Script Date: 7/14/2005 11:26:10 AM ******/
CREATE TABLE [dbo].[tbl_tmpRunDayOfWeek] (
	[businessunitid] [int] NULL ,
	[rundayofweek] [int] NULL 
) ON [DATA]
GO

ALTER TABLE [dbo].[AuditBookOuts] WITH NOCHECK ADD 
	CONSTRAINT [PK_AuditBookOuts] PRIMARY KEY  CLUSTERED 
	(
		[AuditId]
	)  ON [DATA] 
GO

ALTER TABLE [dbo].[Audit_Exceptions] WITH NOCHECK ADD 
	CONSTRAINT [PK_Audit_Exceptions] PRIMARY KEY  CLUSTERED 
	(
		[Audit_ID],
		[SourceTableID],
		[ExceptionRuleID]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

ALTER TABLE [dbo].[Audit_Inventory] WITH NOCHECK ADD 
	CONSTRAINT [PK_Audit_Inventory] PRIMARY KEY  CLUSTERED 
	(
		[AUDIT_ID]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

ALTER TABLE [dbo].[Audit_Sales] WITH NOCHECK ADD 
	CONSTRAINT [PK_Audit_Sales] PRIMARY KEY  CLUSTERED 
	(
		[AUDIT_ID]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

ALTER TABLE [dbo].[BookOutTypes] WITH NOCHECK ADD 
	CONSTRAINT [PK_BookOutTypes] PRIMARY KEY  CLUSTERED 
	(
		[BookOutTypeId]
	)  ON [DATA] 
GO

ALTER TABLE [dbo].[DealerPack] WITH NOCHECK ADD 
	CONSTRAINT [PK_DealerPack] PRIMARY KEY  CLUSTERED 
	(
		[DealerPackID]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

ALTER TABLE [dbo].[DealerRisk] WITH NOCHECK ADD 
	CONSTRAINT [PK_DealerRisk] PRIMARY KEY  CLUSTERED 
	(
		[BusinessUnitId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[DecodedSquishVINs] WITH NOCHECK ADD 
	CONSTRAINT [PK_DecodedSquishVINs] PRIMARY KEY  CLUSTERED 
	(
		[squish_vin]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

ALTER TABLE [dbo].[FirstLookRegionToZip] WITH NOCHECK ADD 
	CONSTRAINT [PK_FirstLookRegionToZip] PRIMARY KEY  CLUSTERED 
	(
		[FirstLookRegionID],
		[ZipCode]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

ALTER TABLE [dbo].[GlobalParam] WITH NOCHECK ADD 
	CONSTRAINT [PK_GlobalParam] PRIMARY KEY  CLUSTERED 
	(
		[id]
	)  ON [DATA] 
GO

ALTER TABLE [dbo].[Inventory] WITH NOCHECK ADD 
	CONSTRAINT [PK_Inventory] PRIMARY KEY  CLUSTERED 
	(
		[InventoryID]
	)  ON [DATA] 
GO

ALTER TABLE [dbo].[InventoryBucketRanges] WITH NOCHECK ADD 
	CONSTRAINT [PK_InventoryBucketRanges] PRIMARY KEY  CLUSTERED 
	(
		[InventoryBucketRangeID]
	)  ON [DATA] 
GO

ALTER TABLE [dbo].[InventoryBuckets] WITH NOCHECK ADD 
	CONSTRAINT [PK_InventoryBuckets] PRIMARY KEY  CLUSTERED 
	(
		[InventoryBucketID]
	)  ON [DATA] 
GO

ALTER TABLE [dbo].[InventoryStatusCodes] WITH NOCHECK ADD 
	CONSTRAINT [PK_InventoryStatusCodes] PRIMARY KEY  CLUSTERED 
	(
		[InventoryStatusCD]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

ALTER TABLE [dbo].[MemberRole] WITH NOCHECK ADD 
	CONSTRAINT [PK_MemberRole] PRIMARY KEY  CLUSTERED 
	(
		[MemberRoleID]
	)  ON [DATA] 
GO

ALTER TABLE [dbo].[ModelBodyStyleOverrides] WITH NOCHECK ADD 
	CONSTRAINT [PK_ModelNameOverrides] PRIMARY KEY  CLUSTERED 
	(
		[Make],
		[Model],
		[BodyType]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

ALTER TABLE [dbo].[PurchasingCenterChannels] WITH NOCHECK ADD 
	CONSTRAINT [PK_PurchasingCenterChannels] PRIMARY KEY  CLUSTERED 
	(
		[ChannelID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Role] WITH NOCHECK ADD 
	CONSTRAINT [PK_Role] PRIMARY KEY  CLUSTERED 
	(
		[RoleID]
	)  ON [DATA] 
GO

ALTER TABLE [dbo].[VehicleCombination] WITH NOCHECK ADD 
	CONSTRAINT [UCK_VehicleCombination] UNIQUE  CLUSTERED 
	(
		[VehicleYear],
		[VehicleMake],
		[VehicleModel],
		[VehicleTrim],
		[BodyDescription]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_AppraisalGuideBookValueTypes] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[Id]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

ALTER TABLE [dbo].[lu_DealerPackType] WITH NOCHECK ADD 
	CONSTRAINT [PK_lu_DealerPackType] PRIMARY KEY  CLUSTERED 
	(
		[PackTypeCD]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

ALTER TABLE [dbo].[lu_Edmunds_Style_New] WITH NOCHECK ADD 
	CONSTRAINT [PK_lu_Edmunds_Style_New] PRIMARY KEY  CLUSTERED 
	(
		[ed_style_id]
	)  ON [DATA] 
GO

ALTER TABLE [dbo].[lu_Edmunds_Style_Used] WITH NOCHECK ADD 
	CONSTRAINT [PK_lu_Edmunds_Style_Used] PRIMARY KEY  CLUSTERED 
	(
		[ed_style_id]
	)  ON [DATA] 
GO

ALTER TABLE [dbo].[lu_OptionType] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[Id]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

ALTER TABLE [dbo].[map_FLTypeClassToSegments] WITH NOCHECK ADD 
	CONSTRAINT [PK_map_FLTypeClassToSegments] PRIMARY KEY  CLUSTERED 
	(
		[FLTypeClassID]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

ALTER TABLE [dbo].[tbl_GroupingPromotionPlan] WITH NOCHECK ADD 
	CONSTRAINT [PK_GroupingPromotionPlan] PRIMARY KEY  CLUSTERED 
	(
		[GroupingPromotionPlanId]
	)  ON [DATA] 
GO

ALTER TABLE [dbo].[tbl_Vehicle] WITH NOCHECK ADD 
	CONSTRAINT [PK_tbl_Vehicle] PRIMARY KEY  CLUSTERED 
	(
		[VehicleID]
	)  ON [DATA] 
GO

ALTER TABLE [dbo].[tbl_VehicleSale] WITH NOCHECK ADD 
	CONSTRAINT [PK_tbl_VehicleSale] PRIMARY KEY  CLUSTERED 
	(
		[InventoryID]
	) WITH  FILLFACTOR = 95  ON [DATA] 
GO

ALTER TABLE [dbo].[tbl_VehicleSegmentOverrides] WITH NOCHECK ADD 
	CONSTRAINT [PK_tbl_VehicleSegmentOverrides] PRIMARY KEY  CLUSTERED 
	(
		[VehicleSegmentID],
		[VehicleSubSegmentID]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

 CREATE  CLUSTERED  INDEX [IX_AppraisalBump__VehicleGuideBookID] ON [dbo].[AppraisalBump]([VehicleGuideBookID]) WITH  FILLFACTOR = 95 ON [DATA]
GO

 CREATE  CLUSTERED  INDEX [IX_AppraisalWindowSticker__AppraisalID] ON [dbo].[AppraisalWindowSticker]([AppraisalId]) ON [DATA]
GO

 CREATE  UNIQUE  CLUSTERED  INDEX [IX_DecodedFirstLookSquishVINs] ON [dbo].[DecodedFirstLookSquishVINs]([squish_vin]) WITH  FILLFACTOR = 80 ON [DATA]
GO

 CREATE  CLUSTERED  INDEX [GuideBookValue3] ON [dbo].[GuideBookValue]([GuideBookID]) WITH  FILLFACTOR = 80 ON [DATA]
GO

 CREATE  CLUSTERED  INDEX [IDX_c_MakeModelGrouping_GroupingMMG] ON [dbo].[tbl_MakeModelGrouping]([GroupingDescriptionID], [MakeModelGroupingID]) WITH  FILLFACTOR = 80 ON [PRIMARY]
GO

ALTER TABLE [dbo].[Alter_Script] ADD 
	CONSTRAINT [DF_Alter_Script_ApplicationDate] DEFAULT (getdate()) FOR [ApplicationDate]
GO

ALTER TABLE [dbo].[ApplicationEvent] ADD 
	CONSTRAINT [PK_ApplicationEvent] PRIMARY KEY  NONCLUSTERED 
	(
		[ApplicationEventID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[Appraisal] ADD 
	CONSTRAINT [PK_VehicleGuideBook] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleGuideBookID]
	) WITH  FILLFACTOR = 90  ON [IDX] ,
	CONSTRAINT [UNK_VehicleGuideBook_Vin_BU_GuideBook] UNIQUE  NONCLUSTERED 
	(
		[Vin],
		[BusinessUnitID],
		[GuideBookID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

 CREATE  INDEX [Appraisal26] ON [dbo].[Appraisal]([BusinessUnitID], [Vin], [TradeAnalyzerDate]) WITH  FILLFACTOR = 80 ON [DATA]
GO

ALTER TABLE [dbo].[AppraisalBump] ADD 
	CONSTRAINT [PK_VehicleGuideBookBump] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleGuideBookBumpID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[AppraisalGuideBookOptions] ADD 
	CONSTRAINT [DF__appraisal__stand__7291CD77] DEFAULT (0) FOR [standardOption],
	CONSTRAINT [PK_VehicleGuideBookOptions] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleGuideBookOptionsID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[AppraisalGuideBookValues] ADD 
	CONSTRAINT [DF__appraisal__valid__263B8EAF] DEFAULT (1) FOR [valid],
	CONSTRAINT [PK_VehicleGuideBookValues] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleGuideBookValuesID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

 CREATE  INDEX [IX_AppraisalGuideBookValues__VehicleGuideBookID] ON [dbo].[AppraisalGuideBookValues]([VehicleGuideBookID]) ON [IDX]
GO

ALTER TABLE [dbo].[AppraisalInGroupDemand] ADD 
	CONSTRAINT [PK_VehicleGuideBookInGroupDemand] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleGuideBookInGroupDemandID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[AppraisalWindowSticker] ADD 
	CONSTRAINT [PK_AppraisalWindowSticker] PRIMARY KEY  NONCLUSTERED 
	(
		[AppraisalWindowStickerId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[AuditBookOuts] ADD 
	CONSTRAINT [DF_BookOutTimeStamp] DEFAULT (getdate()) FOR [BookOutTimeStamp]
GO

 CREATE  INDEX [IX_Audit_Bookouts_TradeAnalyzer__BookOutReferenceID] ON [dbo].[Audit_Bookouts_TradeAnalyzer]([BookOutReferenceID]) ON [IDX]
GO

ALTER TABLE [dbo].[Audit_Exceptions] ADD 
	CONSTRAINT [DF_Audit_Exceptions_TimeStamp] DEFAULT (getdate()) FOR [TimeStamp]
GO

ALTER TABLE [dbo].[BusinessUnit] ADD 
	CONSTRAINT [DF_BusinessUnit_Active] DEFAULT (1) FOR [Active],
	CONSTRAINT [PK_BusinessUnit] PRIMARY KEY  NONCLUSTERED 
	(
		[BusinessUnitID]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UQ_BusinessUnit_BusinessUnitCode] UNIQUE  NONCLUSTERED 
	(
		[BusinessUnitCode]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

ALTER TABLE [dbo].[BusinessUnitRelationship] ADD 
	CONSTRAINT [PK_BusinessUnitRelationship] PRIMARY KEY  NONCLUSTERED 
	(
		[BusinessUnitID],
		[ParentID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[BusinessUnitType] ADD 
	CONSTRAINT [PK_BusinessUnitType] PRIMARY KEY  NONCLUSTERED 
	(
		[BusinessUnitTypeID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[CIAEngineFailures] ADD 
	CONSTRAINT [DF_ExpertSystemFailures_TimeStamp] DEFAULT (getdate()) FOR [TimeStamp]
GO

ALTER TABLE [dbo].[CIAMakeModelMinMax] ADD 
	CONSTRAINT [PK_CIAMakeModelMinMax] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAMakeModelMinMaxID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[CIAMakeModelSuppression] ADD 
	CONSTRAINT [PK_CIAMakeModelSuppression] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAMakeModelSuppressionID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[CIAPricePoint] ADD 
	CONSTRAINT [PK_CIAPricePoint] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAPricePointID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[CIAReport] ADD 
	CONSTRAINT [PK_CIAReport] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAReportID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[CIAReportAverages] ADD 
	CONSTRAINT [PK_CiaReportAverages] PRIMARY KEY  NONCLUSTERED 
	(
		[CiaReportAveragesID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[CIAReportMakeModelAttributes] ADD 
	CONSTRAINT [PK_CIAReportMakeModelAttributes] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAReportMakeModelAttributesID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[CIAReportPerformance] ADD 
	CONSTRAINT [PK_CIAReportPerformance] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAReportPerformanceID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[CarfaxAvailability] ADD 
	CONSTRAINT [PK_CarfaxAvailability] PRIMARY KEY  NONCLUSTERED 
	(
		[UserName],
		[Vin]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[ChangedVics] ADD 
	CONSTRAINT [PK_ChangedVics] PRIMARY KEY  NONCLUSTERED 
	(
		[ChangedVicID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[CredentialType] ADD 
	CONSTRAINT [PK_CredentialType] PRIMARY KEY  NONCLUSTERED 
	(
		[CredentialTypeID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[DMI_STAGING_INVENTORY] ADD 
	CONSTRAINT [DF_DMI_STAGING_INVENTORY_AUDIT_ID] DEFAULT (0) FOR [Audit_ID]
GO

ALTER TABLE [dbo].[DataSource] ADD 
	CONSTRAINT [PK_DataSource] PRIMARY KEY  NONCLUSTERED 
	(
		[DataSourceID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[DealerCIAPreferences] ADD 
	CONSTRAINT [DF_ShortAgeBandID] DEFAULT (0) FOR [ShortAgeBandID],
	CONSTRAINT [DF_LongAgeBandID] DEFAULT (0) FOR [LongAgeBandID],
	CONSTRAINT [DF_ForecastAgeBandID] DEFAULT (0) FOR [ForecastAgeBandID],
	CONSTRAINT [DF_ShortAgeBandWeight] DEFAULT (0) FOR [ShortAgeBandWeight],
	CONSTRAINT [DF_LongAgeBandWeight] DEFAULT (0) FOR [LongAgeBandWeight],
	CONSTRAINT [DF_ForecastAgeBandWeight] DEFAULT (0) FOR [ForecastAgeBandWeight],
	CONSTRAINT [DF_NumberOfPowerZoneMakeModels] DEFAULT (0) FOR [NumberOfPowerZoneMakeModels],
	CONSTRAINT [DF_MinRecommendedDaysSupply] DEFAULT (0) FOR [MinRecommendedDaysSupply],
	CONSTRAINT [DF_MaxRecommendedDaysSupply] DEFAULT (0) FOR [MaxRecommendedDaysSupply],
	CONSTRAINT [DF_ApplyNoSalePctThreshold] DEFAULT (0) FOR [ApplyNoSalePctThreshold],
	CONSTRAINT [DF_NoSalePctThreshold] DEFAULT (0) FOR [NoSalePctThreshold],
	CONSTRAINT [DF_MinRecommendedStockingLevel] DEFAULT (0) FOR [MinRecommendedStockingLevel],
	CONSTRAINT [DF_MaxRecommendedStockingLevel] DEFAULT (0) FOR [MaxRecommendedStockingLevel],
	CONSTRAINT [DF_ShortAgeBandUnitsSoldThreshold] DEFAULT (0) FOR [ShortAgeBandUnitsSoldThreshold],
	CONSTRAINT [DF_LongAgeBandUnitsSoldThreshold] DEFAULT (0) FOR [LongAgeBandUnitsSoldThreshold],
	CONSTRAINT [DF_ForecastAgeBandUnitsSoldThreshold] DEFAULT (0) FOR [ForecastAgeBandUnitsSoldThreshold],
	CONSTRAINT [DF__DealerCIA__GoodB__094B159D] DEFAULT (8) FOR [GoodBetScoreThreshold],
	CONSTRAINT [PK_DealerCIAPreferences] PRIMARY KEY  NONCLUSTERED 
	(
		[DealerCIAPreferencesID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[DealerFacts] ADD 
	CONSTRAINT [PK_DealerFacts] PRIMARY KEY  NONCLUSTERED 
	(
		[DealerFactID]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

ALTER TABLE [dbo].[DealerFinancialStatement] ADD 
	CONSTRAINT [PK_DealerFinancialStatement] PRIMARY KEY  NONCLUSTERED 
	(
		[DealerFinancialStatementID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[DealerFranchise] ADD 
	CONSTRAINT [PK_DealerFranchise] PRIMARY KEY  NONCLUSTERED 
	(
		[DealerFranchiseID]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UNK_DealerFranchiseUnique_BU_FranchiseID] UNIQUE  NONCLUSTERED 
	(
		[BusinessUnitID],
		[FranchiseID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[DealerGridThresholds] ADD 
	CONSTRAINT [PK_DealerGridThresholds] PRIMARY KEY  NONCLUSTERED 
	(
		[DealerGridThresholdsId]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UQ_DealerGridThresholdsBusinessUnit] UNIQUE  NONCLUSTERED 
	(
		[BusinessUnitID]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

ALTER TABLE [dbo].[DealerGridValues] ADD 
	CONSTRAINT [PK_DealerGridValues] PRIMARY KEY  NONCLUSTERED 
	(
		[DealerGridValuesId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[DealerGroupPotentialEvent] ADD 
	CONSTRAINT [PK_DealerGroupPotentialEvent] PRIMARY KEY  NONCLUSTERED 
	(
		[DealerGroupPotentialEventID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[DealerGroupPreference] ADD 
	CONSTRAINT [DF_DealerGroupPreference_WholesaleDays] DEFAULT (0) FOR [WholesaleDays],
	CONSTRAINT [DF_DealerGroupPreference_IncludeRedistribution] DEFAULT (0) FOR [IncludeRedistribution],
	CONSTRAINT [DF_DealerGroupPreference_InventoryExchangeAgeLimit] DEFAULT (0) FOR [InventoryExchangeAgeLimit],
	CONSTRAINT [PK_DealerGroupPreference] PRIMARY KEY  NONCLUSTERED 
	(
		[DealerGroupPreferenceID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

 CREATE  UNIQUE  INDEX [IX_DealerGroupPreference] ON [dbo].[DealerGroupPreference]([BusinessUnitID]) WITH  FILLFACTOR = 80 ON [IDX]
GO

ALTER TABLE [dbo].[DealerPreference] ADD 
	CONSTRAINT [DF_DealerPreference_UnitCostThreshold] DEFAULT (1000) FOR [UnitCostThreshold],
	CONSTRAINT [DF_DealerPreference_UnitCostUpdateOnSale] DEFAULT (1) FOR [UnitCostUpdateOnSale],
	CONSTRAINT [DF_DealerPreference_UnwindDaysThreshold] DEFAULT (45) FOR [UnwindDaysThreshold],
	CONSTRAINT [DF__DealerPre__FEGro__2CC890AD] DEFAULT ((-1000000)) FOR [FEGrossProfitThreshold],
	CONSTRAINT [DF__DealerPre__SellT__53E25DCE] DEFAULT (90) FOR [SellThroughRate],
	CONSTRAINT [DF__DealerPre__Inclu__54D68207] DEFAULT (1) FOR [IncludeBackEndInValuation],
	CONSTRAINT [DF_DealerPreference_UnitsSoldThresholdInvOverview] DEFAULT (6) FOR [UnitsSoldThresholdInvOverview],
	CONSTRAINT [DF_DealerPreference_ApplyPriorAgingNotes] DEFAULT (1) FOR [ApplyPriorAgingNotes],
	CONSTRAINT [DF_DealerPreference_AverageInventoryAgeRedThreshold] DEFAULT (30) FOR [averageInventoryAgeRedThreshold],
	CONSTRAINT [DF_DealerPreference_AverageDaysSupplyRedThreshold] DEFAULT (50) FOR [averageDaysSupplyRedThreshold],
	CONSTRAINT [DF_DealerPreference_PurchasingDistanceFromDealer] DEFAULT ((-1)) FOR [PurchasingDistanceFromDealer],
	CONSTRAINT [DF_DealerPreference_UnitsSoldThreshold12Wks] DEFAULT (1) FOR [UnitsSoldThreshold12Wks],
	CONSTRAINT [PK_DealerPreference] PRIMARY KEY  NONCLUSTERED 
	(
		[DealerPreferenceID]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [IX_DealerPreference] UNIQUE  NONCLUSTERED 
	(
		[BusinessUnitID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[DealerPreferenceDataload] ADD 
	CONSTRAINT [DF_DealerPreferenceDataload_TradeorPurchaseFromSales] DEFAULT (0) FOR [TradeorPurchaseFromSales],
	CONSTRAINT [DF_DealerPreferenceDataload_TradeorPurchaseFromSalesDaysThreshold] DEFAULT (0) FOR [TradeorPurchaseFromSalesDaysThreshold],
	CONSTRAINT [DF_DealerPreferenceDataload_GenerateInventroyDeltas] DEFAULT (0) FOR [GenerateInventoryDeltas],
	CONSTRAINT [DF__DealerPre__Calcu__6FB560CC] DEFAULT (0) FOR [CalculateUnitCost],
	CONSTRAINT [PK_DealerPreferenceDataload] PRIMARY KEY  NONCLUSTERED 
	(
		[DealerPreferenceDataloadID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

 CREATE  UNIQUE  INDEX [IX_DealerPreferenceDataload] ON [dbo].[DealerPreferenceDataload]([BusinessUnitID]) WITH  FILLFACTOR = 80 ON [IDX]
GO

ALTER TABLE [dbo].[DealerRisk] ADD 
	CONSTRAINT [DF_DealerRisk_RiskLevelNumberOfWeeks] DEFAULT (13) FOR [RiskLevelNumberOfWeeks],
	CONSTRAINT [DF_DealerRisk_RiskLevelDealsThreshold] DEFAULT (3) FOR [RiskLevelDealsThreshold],
	CONSTRAINT [DF_DealerRisk_RiskLevelNumberOfContributors] DEFAULT (4) FOR [RiskLevelNumberOfContributors],
	CONSTRAINT [DF_DealerRisk_RedLightNoSaleThreshold] DEFAULT (60) FOR [RedLightNoSaleThreshold],
	CONSTRAINT [DF_DealerRisk_RedLightGrossProfitThreshold] DEFAULT (50) FOR [RedLightGrossProfitThreshold],
	CONSTRAINT [DF_DealerRisk_GreenLightNoSaleThreshold] DEFAULT (40) FOR [GreenLightNoSaleThreshold],
	CONSTRAINT [DF_DealerRisk_GreenLightGrossProfitThreshold] DEFAULT (75) FOR [GreenLightGrossProfitThreshold],
	CONSTRAINT [DF_DealerRisk_GreenLightMarginThreshold] DEFAULT (25.0) FOR [GreenLightMarginThreshold],
	CONSTRAINT [DF_DealerRisk_GreenLightDaysPercentage] DEFAULT (25) FOR [GreenLightDaysPercentage],
	CONSTRAINT [DF_DealerRisk_HighMileageYear] DEFAULT (70000) FOR [HighMileageYear],
	CONSTRAINT [DF_DealerRisk_HighMileageOverall] DEFAULT (70000) FOR [HighMileageOverall],
	CONSTRAINT [DF_DealerRisk_RiskLevelYearInitialTimePeriod] DEFAULT (6) FOR [RiskLevelYearInitialTimePeriod],
	CONSTRAINT [DF_DealerRisk_RiskLevelYearSecondaryTimePeriod] DEFAULT (5) FOR [RiskLevelYearSecondaryTimePeriod],
	CONSTRAINT [DF_DealerRisk_RiskLevelYearRollOverMonth] DEFAULT (9) FOR [RiskLevelYearRollOverMonth],
	CONSTRAINT [DF_DealerRisk_RedLightTarget] DEFAULT (10) FOR [RedLightTarget],
	CONSTRAINT [DF_DealerRisk_YellowLightTarget] DEFAULT (35) FOR [YellowLightTarget],
	CONSTRAINT [DF_DealerRisk_GreenLightTarget] DEFAULT (55) FOR [GreenLightTarget],
	CONSTRAINT [DF_DealerRisk_HighMileageRedLight] DEFAULT (100000) FOR [HighMileageRedLight]
GO

ALTER TABLE [dbo].[DealerTarget] ADD 
	CONSTRAINT [PK_DealerTarget] PRIMARY KEY  NONCLUSTERED 
	(
		[DealerTargetId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[DealerUpgrade] ADD 
	CONSTRAINT [PK_DealerUpgrade] PRIMARY KEY  NONCLUSTERED 
	(
		[BusinessUnitID],
		[DealerUpgradeCD]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[DecodedFirstLookSquishVINs] ADD 
	CONSTRAINT [DF__DecodedFi__Displ__7E97B1A9] DEFAULT (1) FOR [DisplayBodyTypeID]
GO

ALTER TABLE [dbo].[DecodedSquishVINs] ADD 
	CONSTRAINT [DF__DecodedSq__Displ__52EE3995] DEFAULT (1) FOR [DisplayBodyTypeID]
GO

ALTER TABLE [dbo].[ErrorHandler] ADD 
	CONSTRAINT [PK_ErrorHandler] PRIMARY KEY  NONCLUSTERED 
	(
		[ErrorHandlerID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[FirstLookRegion] ADD 
	CONSTRAINT [PK_FirstLookRegion] PRIMARY KEY  NONCLUSTERED 
	(
		[FirstLookRegionID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[Franchise] ADD 
	CONSTRAINT [PK_Franchise] PRIMARY KEY  NONCLUSTERED 
	(
		[FranchiseID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[FranchiseAlias] ADD 
	CONSTRAINT [PK_FranchiseAlias] PRIMARY KEY  NONCLUSTERED 
	(
		[FranchiseAliasID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[GDLight] ADD 
	CONSTRAINT [PK_MMGLight] PRIMARY KEY  NONCLUSTERED 
	(
		[GroupingDescriptionLightId]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UQ_GDLight] UNIQUE  NONCLUSTERED 
	(
		[GroupingDescriptionId],
		[BusinessUnitID],
		[InventoryVehicleLightID]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

ALTER TABLE [dbo].[GuideBook] ADD 
	CONSTRAINT [PK_GuideBook] PRIMARY KEY  NONCLUSTERED 
	(
		[GuideBookID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[GuideBookValue] ADD 
	CONSTRAINT [DF__guidebook__valid__272FB2E8] DEFAULT (1) FOR [valid],
	CONSTRAINT [PK_GuideBookValue] PRIMARY KEY  NONCLUSTERED 
	(
		[GuideBookValueID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

 CREATE  INDEX [GuideBookValue14] ON [dbo].[GuideBookValue]([InventoryID]) WITH  FILLFACTOR = 80 ON [DATA]
GO

ALTER TABLE [dbo].[Inventory] ADD 
	CONSTRAINT [DF_InventoryReceivedDate] DEFAULT (convert(smalldatetime,getdate())) FOR [InventoryReceivedDate],
	CONSTRAINT [DF_tbl_Inventory] DEFAULT (0) FOR [Audit_ID],
	CONSTRAINT [DF_Inventory_InventoryStatusCD] DEFAULT (0) FOR [InventoryStatusCD],
	CONSTRAINT [DF__inventory__ListP__7D5974AD] DEFAULT (0) FOR [ListPriceLock],
	CONSTRAINT [UQ_Inventory__VehicleID_BusinessUnitID_StockNumber] UNIQUE  NONCLUSTERED 
	(
		[VehicleID],
		[BusinessUnitID],
		[StockNumber]
	) WITH  FILLFACTOR = 90  ON [IDX] ,
	CONSTRAINT [CK_Inventory_FLRecFollowed] CHECK ([FLRecFollowed] = 1 or [FLRecFollowed] = 0)
GO

 CREATE  INDEX [IX_Inventory__BusinessUnitID_EtAl] ON [dbo].[Inventory]([BusinessUnitID], [InventoryActive], [InventoryType]) WITH  FILLFACTOR = 90 ON [IDX]
GO

 CREATE  INDEX [IX_Inventory__InventoryReceivedDate_EtAl] ON [dbo].[Inventory]([InventoryReceivedDate], [BusinessUnitID], [InventoryType], [TradeOrPurchase]) WITH  FILLFACTOR = 95 ON [IDX]
GO

ALTER TABLE [dbo].[InventoryBucketRanges] ADD 
	CONSTRAINT [DF_InventoryBucketRanges_Lights] DEFAULT (0) FOR [Lights]
GO

ALTER TABLE [dbo].[InventoryBuckets] ADD 
	CONSTRAINT [DF_InventoryBuckets_InventoryType] DEFAULT (2) FOR [InventoryType]
GO

ALTER TABLE [dbo].[InventoryStocking] ADD 
	CONSTRAINT [PK_InventoryStocking] PRIMARY KEY  NONCLUSTERED 
	(
		[InventoryStockingID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[InventoryStockingModel] ADD 
	CONSTRAINT [PK_InventoryStockingModel] PRIMARY KEY  NONCLUSTERED 
	(
		[InventoryStockingModelID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[KelleyBadVin] ADD 
	CONSTRAINT [PK_KelleyBadVin] PRIMARY KEY  NONCLUSTERED 
	(
		[KelleyBadVinId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[MarketShare] ADD 
	CONSTRAINT [DF_MarketShare_NumberOfWeeks] DEFAULT (26) FOR [NumberOfWeeks],
	CONSTRAINT [PK_MarketShare] PRIMARY KEY  NONCLUSTERED 
	(
		[MarketShareID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[MarketToZip] ADD 
	CONSTRAINT [PK_MarketToZip] PRIMARY KEY  NONCLUSTERED 
	(
		[BusinessUnitId],
		[Ring],
		[ZipCode]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[Member] ADD 
	CONSTRAINT [DF_Member_MemberType] DEFAULT (0) FOR [MemberType],
	CONSTRAINT [DF_Member_CreateDate] DEFAULT (getdate()) FOR [CreateDate],
	CONSTRAINT [PK_Member] PRIMARY KEY  NONCLUSTERED 
	(
		[MemberID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

 CREATE  INDEX [IX_Member__Login] ON [dbo].[Member]([Login]) ON [IDX]
GO

ALTER TABLE [dbo].[MemberAccess] ADD 
	CONSTRAINT [PK_MemberAccess] PRIMARY KEY  NONCLUSTERED 
	(
		[MemberID],
		[BusinessUnitID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[MemberCredential] ADD 
	CONSTRAINT [PK_MemberCredential] PRIMARY KEY  NONCLUSTERED 
	(
		[MemberID],
		[CredentialTypeID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[MemberRole] ADD 
	CONSTRAINT [UQ_MemberRole] UNIQUE  NONCLUSTERED 
	(
		[MemberID],
		[RoleID]
	)  ON [DATA] 
GO

ALTER TABLE [dbo].[OJB_DLIST] ADD 
	 PRIMARY KEY  NONCLUSTERED 
	(
		[ID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[OJB_DLIST_ENTRIES] ADD 
	CONSTRAINT [PK_OJB_DLIST_ENTRIES] PRIMARY KEY  NONCLUSTERED 
	(
		[ID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[OJB_DMAP] ADD 
	CONSTRAINT [PK_OJB_DMAP] PRIMARY KEY  NONCLUSTERED 
	(
		[ID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[OJB_DMAP_ENTRIES] ADD 
	CONSTRAINT [PK_OJB_DMAP_ENTRIES] PRIMARY KEY  NONCLUSTERED 
	(
		[ID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[OJB_DSET] ADD 
	CONSTRAINT [PK_OJB_DSET] PRIMARY KEY  NONCLUSTERED 
	(
		[ID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[OJB_DSET_ENTRIES] ADD 
	CONSTRAINT [PK_OJB_DSET_ENTRIES] PRIMARY KEY  NONCLUSTERED 
	(
		[ID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[OJB_HL_SEQ] ADD 
	CONSTRAINT [PK_OJB_HL_SEQ] PRIMARY KEY  NONCLUSTERED 
	(
		[TABLENAME],
		[FIELDNAME]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[OJB_LOCKENTRY] ADD 
	CONSTRAINT [PK_OJB_LOCKENTRY] PRIMARY KEY  NONCLUSTERED 
	(
		[OID_],
		[TX_ID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[OJB_NRM] ADD 
	CONSTRAINT [PK_OJB_NRM] PRIMARY KEY  NONCLUSTERED 
	(
		[NAME]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[OptionDetail] ADD 
	CONSTRAINT [DF_OptionDetail_OptionKey] DEFAULT (0) FOR [OptionKey],
	CONSTRAINT [PK_OptionDetail] PRIMARY KEY  NONCLUSTERED 
	(
		[OptionDetailID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[PurchasingCenterChannels] ADD 
	CONSTRAINT [DF_PurchasingCenterChannels_HasAccessGroups] DEFAULT (0) FOR [HasAccessGroups]
GO

ALTER TABLE [dbo].[RedistributionAction] ADD 
	CONSTRAINT [PK_RedistributionAction] PRIMARY KEY  NONCLUSTERED 
	(
		[RedistributionActionID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[SiteAuditEvent] ADD 
	CONSTRAINT [PK_SiteAuditEvent] PRIMARY KEY  NONCLUSTERED 
	(
		[SiteAuditEventID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[Target] ADD 
	CONSTRAINT [PK_Target] PRIMARY KEY  NONCLUSTERED 
	(
		[TargetCD]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[TargetSection] ADD 
	CONSTRAINT [PK_TargetSection] PRIMARY KEY  NONCLUSTERED 
	(
		[SectionCD]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[TargetType] ADD 
	CONSTRAINT [PK_TargetType] PRIMARY KEY  NONCLUSTERED 
	(
		[TargetTypeCD]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[TradeAnalyzerEvent] ADD 
	CONSTRAINT [PK_TradeAnalyzerEvent] PRIMARY KEY  NONCLUSTERED 
	(
		[TradeAnalyzerEventID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[VehicleBodyStyle] ADD 
	CONSTRAINT [PK_VehicleBodyStyle] PRIMARY KEY  NONCLUSTERED 
	(
		[StandardBodyStyleID]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UQ_VehicleBodyStyle_VehicleBodyStyle] UNIQUE  NONCLUSTERED 
	(
		[VehicleBodyStyle]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[VehicleBodyStyleMapping] ADD 
	CONSTRAINT [DF_UnknownBodyStyleID] DEFAULT (99) FOR [StandardBodyStyleID],
	CONSTRAINT [PK_VehicleBodyStyleAlias] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleBodyStyleMappingID]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UQ_VehicleBodyStyleAlias] UNIQUE  NONCLUSTERED 
	(
		[OriginalVehicleBodyStyle]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[VehicleCategory] ADD 
	CONSTRAINT [PK_VehicleCategory] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleCategoryID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[VehicleClass] ADD 
	CONSTRAINT [PK_VehicleClass] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleClassID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[VehicleCombination] ADD 
	CONSTRAINT [PK_VehicleCombination] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleCombinationID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[VehicleLocatorContactDealerEvent] ADD 
	CONSTRAINT [PK_VehicleLocatorContactDealerEvent] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleLocatorContactDealerEventID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[VehicleOrigin] ADD 
	CONSTRAINT [PK_VehicleOrigin] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleOriginID]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UNK_VehicleOrigin_VehicleOriginCode] UNIQUE  NONCLUSTERED 
	(
		[VehicleOriginCode]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[VehiclePhoto] ADD 
	CONSTRAINT [PK_VehiclePhoto] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[VehiclePlanTracking] ADD 
	CONSTRAINT [DF_VehiclePlanTracking_Created] DEFAULT (getdate()) FOR [Created],
	CONSTRAINT [PK_VehiclePlanTracking] PRIMARY KEY  NONCLUSTERED 
	(
		[VehiclePlanTrackingID]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UQ_VehiclePlanTrackingUnique_InventoryID_BU] UNIQUE  NONCLUSTERED 
	(
		[InventoryID],
		[BusinessUnitID],
		[VehiclePlanTrackingHeaderID]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

ALTER TABLE [dbo].[VehiclePlanTrackingHeader] ADD 
	CONSTRAINT [PK_VehiclePlanTrackingHeader] PRIMARY KEY  NONCLUSTERED 
	(
		[VehiclePlanTrackingHeaderID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[VehicleSalesHistoryAudit] ADD 
	CONSTRAINT [PK_VehicleSalesHistoryAudit] PRIMARY KEY  NONCLUSTERED 
	(
		[AuditID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[VehicleSegment] ADD 
	CONSTRAINT [PK_VehicleSegment] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleSegmentID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_AccessGroupActor] ADD 
	CONSTRAINT [PK_lu_AccessGroupActor] PRIMARY KEY  NONCLUSTERED 
	(
		[AccessGroupActorId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_AccessGroupPrice] ADD 
	CONSTRAINT [PK_lu_AccessGroupPrice] PRIMARY KEY  NONCLUSTERED 
	(
		[AccessGroupPriceId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_AuditStatus] ADD 
	CONSTRAINT [PK_lu_AuditStatus] PRIMARY KEY  NONCLUSTERED 
	(
		[AuditStatusCD]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_CIACategory] ADD 
	CONSTRAINT [PK_tbl_CIACategory] PRIMARY KEY  NONCLUSTERED 
	(
		[CIACategoryId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_CIAPlanType] ADD 
	CONSTRAINT [PK_lu_CIAPlanType] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAPlanTypeId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_CIATimePeriod] ADD 
	CONSTRAINT [PK_lu_CIATimePeriod] PRIMARY KEY  NONCLUSTERED 
	(
		[CIATimePeriodId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_CustomerType] ADD 
	CONSTRAINT [PK_lu_CustomerType] PRIMARY KEY  NONCLUSTERED 
	(
		[CustomerTypeCD]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_DMIVehicleStatus] ADD 
	CONSTRAINT [PK_lu_lu_DMIVehicleStatus] PRIMARY KEY  NONCLUSTERED 
	(
		[DMIVehicleStatusCD]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UQ_lu_DMIVehicleStatus_DMIVehicleStatusDESC] UNIQUE  NONCLUSTERED 
	(
		[DMIVehicleStatusDESC]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_DealStatus] ADD 
	CONSTRAINT [PK_lu_DealStatus] PRIMARY KEY  NONCLUSTERED 
	(
		[DealStatusCD]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_DealerPackType] ADD 
	CONSTRAINT [IX_lu_DealerPackType] UNIQUE  NONCLUSTERED 
	(
		[PackTypeDESC]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

ALTER TABLE [dbo].[lu_DealerUpgrade] ADD 
	CONSTRAINT [PK_lu_DealerUpgrade] PRIMARY KEY  NONCLUSTERED 
	(
		[DealerUpgradeCD]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_DecodedSquishVinSource] ADD 
	CONSTRAINT [PK_lu_DecodedSquishVinSource] PRIMARY KEY  NONCLUSTERED 
	(
		[dsvSourceID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_DisplayBodyType] ADD 
	CONSTRAINT [PK_DisplayBodyType] PRIMARY KEY  NONCLUSTERED 
	(
		[DisplayBodyTypeID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_FuelType] ADD 
	CONSTRAINT [PK_lu_FuelType] PRIMARY KEY  NONCLUSTERED 
	(
		[FuelTypeCD]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UQ_lu_FuelType_FuelTypeDESC] UNIQUE  NONCLUSTERED 
	(
		[FuelTypeCD]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_GuideBook] ADD 
	CONSTRAINT [PK_lu_GuideBook] PRIMARY KEY  NONCLUSTERED 
	(
		[GuideBookId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_InventoryVehicleLight] ADD 
	CONSTRAINT [PK_lu_InventoryVehicleLight] PRIMARY KEY  NONCLUSTERED 
	(
		[InventoryVehicleLightCD]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_InventoryVehicleType] ADD 
	CONSTRAINT [PK_lu_InventoryVehicleType] PRIMARY KEY  NONCLUSTERED 
	(
		[InventoryVehicleTypeCD]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_ProgramType] ADD 
	CONSTRAINT [PK_lu_ProgramType] PRIMARY KEY  NONCLUSTERED 
	(
		[ProgramType_CD]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_RedistributionSource] ADD 
	CONSTRAINT [PK_lu_RedistributionSource] PRIMARY KEY  NONCLUSTERED 
	(
		[RedistributionSourceId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_SaleDescription] ADD 
	CONSTRAINT [PK_lu_SaleDescription] PRIMARY KEY  NONCLUSTERED 
	(
		[SaleDescriptionCD]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

 CREATE  INDEX [UQ_lu_SaleDescription_SaleDescription] ON [dbo].[lu_SaleDescription]([SaleDescription]) WITH  FILLFACTOR = 80 ON [IDX]
GO

ALTER TABLE [dbo].[lu_States] ADD 
	CONSTRAINT [PK_lu_States] PRIMARY KEY  NONCLUSTERED 
	(
		[StateID]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UQ_lu_States_StateLong] UNIQUE  NONCLUSTERED 
	(
		[StateLong]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UQ_lu_States_StateShort] UNIQUE  NONCLUSTERED 
	(
		[StateShort]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_Status] ADD 
	CONSTRAINT [PK_lu_Status] PRIMARY KEY  NONCLUSTERED 
	(
		[StatusId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_ThirdParty] ADD 
	CONSTRAINT [PK_lu_ThirdParty] PRIMARY KEY  NONCLUSTERED 
	(
		[ThirdPartyId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_UserRole] ADD 
	CONSTRAINT [PK_lu_UserRole] PRIMARY KEY  NONCLUSTERED 
	(
		[UserRoleCD]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UQ_lu_UserRole_UserRoleDesc] UNIQUE  NONCLUSTERED 
	(
		[UserRoleDesc]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_VehicleLight] ADD 
	CONSTRAINT [PK_lu_VehicleLight] PRIMARY KEY  NONCLUSTERED 
	(
		[InventoryVehicleLightID]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UQ_lu_VehicleLight_InventoryVehicleLightDESC] UNIQUE  NONCLUSTERED 
	(
		[InventoryVehicleLightDESC]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

ALTER TABLE [dbo].[lu_VehicleSegment] ADD 
	CONSTRAINT [PK_lu_VehicleSegment] PRIMARY KEY  NONCLUSTERED 
	(
		[vehiclesegmentid]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UQ_lu_VehicleSegment] UNIQUE  NONCLUSTERED 
	(
		[vehiclesegmentdesc]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_VehicleSubSegment] ADD 
	CONSTRAINT [PK_lu_vehicleSubSegment] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleSubSegmentID]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UQ_lu_vehicleSubSegment] UNIQUE  NONCLUSTERED 
	(
		[VehicleSubSegmentDESC]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_VehicleSubType] ADD 
	CONSTRAINT [PK_lu_VehicleSubType] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleSubTypeID]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UQ_lu_VehicleSubType] UNIQUE  NONCLUSTERED 
	(
		[VehicleSubTypeDESC]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_VehicleType] ADD 
	CONSTRAINT [PK_lu_VehicleType] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleTypeID]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UQ_lu_VehicleType] UNIQUE  NONCLUSTERED 
	(
		[VehicleTypeDesc]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[map_BusinessUnitToMarketDealer] ADD 
	CONSTRAINT [PK_map_BusinessUnitToMarketDealer] PRIMARY KEY  NONCLUSTERED 
	(
		[ID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[map_VehicleSubTypeToSubSegment] ADD 
	CONSTRAINT [PK_map_VehicleSubTypeToSubSegment] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleSubTypeID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[map_VehicleTypeToSegment] ADD 
	CONSTRAINT [PK_map_VehicleTypeToSegment] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleTypeID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_CIABodyTypeDetail] ADD 
	CONSTRAINT [PK_tbl_CIABodyTypeDetail] PRIMARY KEY  NONCLUSTERED 
	(
		[CIABodyTypeDetailId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_CIACompositeTimePeriod] ADD 
	CONSTRAINT [PK_tbl_CIACompositeTimePeriod] PRIMARY KEY  NONCLUSTERED 
	(
		[CIACompositeTimePeriodId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_CIAGroupingItem] ADD 
	CONSTRAINT [PK_tbl_CIAGroupingItem] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAGroupingItemId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_CIAInventoryStocking] ADD 
	CONSTRAINT [PK_tbl_CIAInventoryStocking] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAInventoryStockingId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_CIAPreferences] ADD 
	CONSTRAINT [DF_tbl_CIAPreferences_PowerZoneGroupingThreshold] DEFAULT (10) FOR [PowerZoneGroupingThreshold],
	CONSTRAINT [DF_tbl_CIAPreferences_BucketAllocationMinimumThreshold] DEFAULT (5) FOR [BucketAllocationMinimumThreshold],
	CONSTRAINT [DF__tbl_CIAPr__Targe__0B3292B8] DEFAULT (45) FOR [TargetDaysSupply],
	CONSTRAINT [DF_tbl_CIAPreferences_MarketPerformersDisplayThreshold] DEFAULT (3) FOR [MarketPerformersDisplayThreshold],
	CONSTRAINT [DF_tbl_CIAPreferences_MarketPerformersInStockThreshold] DEFAULT (3) FOR [MarketPerformersInStockThreshold],
	CONSTRAINT [DF__tbl_CIAPr__Marke__1980B20F] DEFAULT (6) FOR [MarketPerformersUnitsThreshold],
	CONSTRAINT [DF__tbl_CIAPr__Marke__1A74D648] DEFAULT (0.8) FOR [MarketPerformersZipCodeThreshold],
	CONSTRAINT [DF_CIAAllocationTimePeriodId] DEFAULT (5) FOR [CIAAllocationTimePeriodId],
	CONSTRAINT [DF_CIATargetUnitsTimePeriodId] DEFAULT (1) FOR [CIATargetUnitsTimePeriodId],
	CONSTRAINT [DF_GDLightProcessorTimePeriodId] DEFAULT (1) FOR [GDLightProcessorTimePeriodId],
	CONSTRAINT [DF_SalesHistoryDisplayTimePeriodId] DEFAULT (5) FOR [SalesHistoryDisplayTimePeriodId],
	CONSTRAINT [PK_tbl_CIAPreferences] PRIMARY KEY  NONCLUSTERED 
	(
		[BusinessUnitId]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

ALTER TABLE [dbo].[tbl_CIASummary] ADD 
	CONSTRAINT [PK_tbl_CIASummary] PRIMARY KEY  NONCLUSTERED 
	(
		[CIASummaryId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

 CREATE  INDEX [IX_tbl_CIASummary_BusinessUnitID] ON [dbo].[tbl_CIASummary]([BusinessUnitId]) WITH  FILLFACTOR = 80 ON [DATA]
GO

ALTER TABLE [dbo].[tbl_CIAUnitCostPointPlan] ADD 
	CONSTRAINT [PK_tbl_CIAUnitCostPointPlan] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAUnitCostPointRangeId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_CIAUnitCostPointRange] ADD 
	CONSTRAINT [PK_tbl_CIAUnitCostPointRange] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAUnitCostPointRangeId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_DealerATCAccessGroups] ADD 
	CONSTRAINT [DF_lu_AccessGroups_AccessGroupPriceId] DEFAULT (1) FOR [AccessGroupPriceId],
	CONSTRAINT [DF_lu_AccessGroups_AccessGroupActorId] DEFAULT (3) FOR [AccessGroupActorId],
	CONSTRAINT [DF_lu_AccessGroups_AccessGroupValue] DEFAULT (1) FOR [AccessGroupValue],
	CONSTRAINT [PK_DealerATCAccessGroups] PRIMARY KEY  NONCLUSTERED 
	(
		[DealerATCAccessGroupsId]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UNK_DealerATCAccessGroupsUnique_BU_AccessGroupsID] UNIQUE  NONCLUSTERED 
	(
		[BusinessUnitID],
		[AccessGroupId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_DealerValuation] ADD 
	CONSTRAINT [PK_tbl_DealerValuation] PRIMARY KEY  NONCLUSTERED 
	(
		[DealerValuationId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_GroupingDescription] ADD 
	CONSTRAINT [DF__tbl_Group__Suppr__2215F810] DEFAULT (0) FOR [SuppressMarketPerformer],
	CONSTRAINT [PK_GroupingDescription] PRIMARY KEY  NONCLUSTERED 
	(
		[GroupingDescriptionID]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UNK_GroupingDescription_GroupingDesc] UNIQUE  NONCLUSTERED 
	(
		[GroupingDescription]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_InventoryOverstocking] ADD 
	CONSTRAINT [PK_tbl_InventoryOverstocking] PRIMARY KEY  NONCLUSTERED 
	(
		[InventoryOverstockingId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_MakeModelGrouping] ADD 
	CONSTRAINT [DF__tbl_MakeM__Displ__51FA155C] DEFAULT (1) FOR [DisplayBodyTypeID],
	CONSTRAINT [PK_MakeModelGrouping] PRIMARY KEY  NONCLUSTERED 
	(
		[MakeModelGroupingID]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UNK_MakeModelBodyType] UNIQUE  NONCLUSTERED 
	(
		[Make],
		[Model],
		[EdmundsBodyType]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_NewCarScoreCard] ADD 
	CONSTRAINT [PK_NewCarScoreCard] PRIMARY KEY  NONCLUSTERED 
	(
		[NewCarScoreCardId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_RedistributionLog] ADD 
	CONSTRAINT [PK_tbl_RedistributionLog] PRIMARY KEY  NONCLUSTERED 
	(
		[RedistributionLogId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_UsedCarScoreCard] ADD 
	CONSTRAINT [PK_UsedCarScoreCard] PRIMARY KEY  NONCLUSTERED 
	(
		[UsedCarScoreCardId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_Vehicle] ADD 
	CONSTRAINT [DF_tbl_Vehicle_CreateDt] DEFAULT (convert(smalldatetime,getdate())) FOR [CreateDt],
	CONSTRAINT [DF_tbl_Vehicle] DEFAULT (0) FOR [Audit_ID],
	CONSTRAINT [UQ_tbl_Vehicle_Vin] UNIQUE  NONCLUSTERED 
	(
		[Vin]
	) WITH  FILLFACTOR = 90  ON [IDX] 
GO

 CREATE  INDEX [IX_MakeModelGroupingID] ON [dbo].[tbl_Vehicle]([MakeModelGroupingID]) WITH  FILLFACTOR = 95 ON [IDX]
GO

ALTER TABLE [dbo].[tbl_VehicleAttributes] ADD 
	CONSTRAINT [PK_VehicleAttributes] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_VehicleGuideBookIdentifier] ADD 
	CONSTRAINT [PK_tbl_VehicleGuideBookIdentifier] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleID],
		[GuideBookID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_VehicleSale] ADD 
	CONSTRAINT [DF_DealStatusCD] DEFAULT (2) FOR [DealStatusCD],
	CONSTRAINT [DF_tbl_VehicleSale] DEFAULT (0) FOR [Audit_ID]
GO

 CREATE  INDEX [IX_tbl_VehicleSale__DealDate_EtAl] ON [dbo].[tbl_VehicleSale]([DealDate], [SaleDescription], [DealStatusCD]) ON [IDX]
GO

 CREATE  INDEX [IX_tbl_VehicleSale__LastPolledDate] ON [dbo].[tbl_VehicleSale]([LastPolledDate]) ON [IDX]
GO

ALTER TABLE [dbo].[tbl_dbVersion] ADD 
	CONSTRAINT [DF_tbl_dbVersion_dbVersionTS] DEFAULT (getdate()) FOR [dbVersionTS]
GO

ALTER TABLE [dbo].[Appraisal] ADD 
	CONSTRAINT [FK_Appraisal__MakeModelGroupingId__MakeModelGroupingId] FOREIGN KEY 
	(
		[MakeModelGroupingId]
	) REFERENCES [dbo].[tbl_MakeModelGrouping] (
		[MakeModelGroupingID]
	),
	CONSTRAINT [FK_Appraisal_BusinessUnit] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_Appraisal_GuideBookId] FOREIGN KEY 
	(
		[GuideBookID]
	) REFERENCES [dbo].[lu_GuideBook] (
		[GuideBookId]
	),
	CONSTRAINT [FK_VehicleGuideBook_MemberID] FOREIGN KEY 
	(
		[MemberID]
	) REFERENCES [dbo].[Member] (
		[MemberID]
	)
GO


ALTER TABLE [dbo].[AppraisalBump] ADD 
	CONSTRAINT [FK_VehicleGuideBookBump_VehicleGuideBookID] FOREIGN KEY 
	(
		[VehicleGuideBookID]
	) REFERENCES [dbo].[Appraisal] (
		[VehicleGuideBookID]
	)
GO

ALTER TABLE [dbo].[AppraisalGuideBookOptions] ADD 
	CONSTRAINT [FK_Type_lu_OptionType] FOREIGN KEY 
	(
		[Type]
	) REFERENCES [dbo].[lu_OptionType] (
		[Id]
	)
GO

ALTER TABLE [dbo].[AppraisalGuideBookValues] ADD 
	CONSTRAINT [FK_AppraisalGuideBookValues__VehicleGuideBookID__VehicleGuideBookID] FOREIGN KEY 
	(
		[VehicleGuideBookID]
	) REFERENCES [dbo].[Appraisal] (
		[VehicleGuideBookID]
	),
	CONSTRAINT [FK_Type_lu_AppraisalValueType] FOREIGN KEY 
	(
		[ValueType]
	) REFERENCES [dbo].[lu_AppraisalGuideBookValueTypes] (
		[Id]
	)
GO

ALTER TABLE [dbo].[AppraisalInGroupDemand] ADD 
	CONSTRAINT [FK_AppraisalInGroupDemand__BusinessUnitID__BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_AppraisalInGroupDemand__VehicleGuideBookInGroupDemandID__VehicleGuideBookID] FOREIGN KEY 
	(
		[VehicleGuideBookInGroupDemandID]
	) REFERENCES [dbo].[Appraisal] (
		[VehicleGuideBookID]
	)
GO

ALTER TABLE [dbo].[AppraisalWindowSticker] ADD 
	CONSTRAINT [FK_AppraisalId_Appraisal] FOREIGN KEY 
	(
		[AppraisalId]
	) REFERENCES [dbo].[Appraisal] (
		[VehicleGuideBookID]
	)
GO

ALTER TABLE [dbo].[AuditBookOuts] ADD 
	CONSTRAINT [FK_AuditBookOuts_BookId] FOREIGN KEY 
	(
		[BookId]
	) REFERENCES [dbo].[lu_GuideBook] (
		[GuideBookId]
	),
	CONSTRAINT [FK_AuditBookOuts_BusinessUnitId] FOREIGN KEY 
	(
		[BusinessUnitId]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_AuditBookOuts_MemberId] FOREIGN KEY 
	(
		[MemberId]
	) REFERENCES [dbo].[Member] (
		[MemberID]
	),
	CONSTRAINT [FK_AuditBookOuts_ProductId] FOREIGN KEY 
	(
		[ProductId]
	) REFERENCES [dbo].[BookOutTypes] (
		[BookOutTypeId]
	)
GO

ALTER TABLE [dbo].[Audit_Bookouts_Inventory] ADD 
	CONSTRAINT [FK_Audit_BookOuts_Inventory_Inventory] FOREIGN KEY 
	(
		[BookOutReferenceID]
	) REFERENCES [dbo].[Inventory] (
		[InventoryID]
	),
	CONSTRAINT [FK_Audit_Bookouts_Inventory_lu_GuideBook] FOREIGN KEY 
	(
		[BookOutSourceID]
	) REFERENCES [dbo].[lu_GuideBook] (
		[GuideBookId]
	)
GO

ALTER TABLE [dbo].[Audit_Bookouts_TradeAnalyzer] ADD 
	CONSTRAINT [FK_Audit_BookOuts_TradeAnalyzer_Appraisal] FOREIGN KEY 
	(
		[BookOutReferenceID]
	) REFERENCES [dbo].[Appraisal] (
		[VehicleGuideBookID]
	),
	CONSTRAINT [FK_Audit_Bookouts_TradeAnalyzer_lu_GuideBook] FOREIGN KEY 
	(
		[BookOutSourceID]
	) REFERENCES [dbo].[lu_GuideBook] (
		[GuideBookId]
	)
GO

ALTER TABLE [dbo].[Audit_Inventory] ADD 
	CONSTRAINT [FK_Audit_Inventory_lu_AuditStatus] FOREIGN KEY 
	(
		[STATUS]
	) REFERENCES [dbo].[lu_AuditStatus] (
		[AuditStatusCD]
	) ON UPDATE CASCADE 
GO

ALTER TABLE [dbo].[Audit_Sales] ADD 
	CONSTRAINT [FK_Audit_Sales_lu_AuditStatus] FOREIGN KEY 
	(
		[STATUS]
	) REFERENCES [dbo].[lu_AuditStatus] (
		[AuditStatusCD]
	) ON UPDATE CASCADE 
GO

ALTER TABLE [dbo].[BusinessUnit] ADD 
	CONSTRAINT [FK_BusinessUnit_BusinessUnitType] FOREIGN KEY 
	(
		[BusinessUnitTypeID]
	) REFERENCES [dbo].[BusinessUnitType] (
		[BusinessUnitTypeID]
	)
GO

ALTER TABLE [dbo].[BusinessUnitRelationship] ADD 
	CONSTRAINT [FK_BusinessUnitRelationship_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_BusinessUnitRelationship_ParentID] FOREIGN KEY 
	(
		[ParentID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	)
GO

ALTER TABLE [dbo].[CIAMakeModelMinMax] ADD 
	CONSTRAINT [FK_CIAMakeModelMinMax_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	)
GO

ALTER TABLE [dbo].[CIAMakeModelSuppression] ADD 
	CONSTRAINT [FK_CIAMakeModelSuppression_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	)
GO

ALTER TABLE [dbo].[CIAPricePoint] ADD 
	CONSTRAINT [FK_CIAPricePoint_CIAReport] FOREIGN KEY 
	(
		[CIAReportID]
	) REFERENCES [dbo].[CIAReport] (
		[CIAReportID]
	)
GO

ALTER TABLE [dbo].[CIAReport] ADD 
	CONSTRAINT [FK_CIAReport_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	)
GO

ALTER TABLE [dbo].[CIAReportAverages] ADD 
	CONSTRAINT [FK_CIAReportAverages_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	)
GO

ALTER TABLE [dbo].[CIAReportMakeModelAttributes] ADD 
	CONSTRAINT [FK_CIAReportMakeModelAttributes_CIAReportID] FOREIGN KEY 
	(
		[CIAReportID]
	) REFERENCES [dbo].[CIAReport] (
		[CIAReportID]
	)
GO

ALTER TABLE [dbo].[DealerCIAPreferences] ADD 
	CONSTRAINT [FK_DealerCIAPreferences_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	)
GO

ALTER TABLE [dbo].[DealerFacts] ADD 
	CONSTRAINT [FK_DealerFacts_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	)
GO

ALTER TABLE [dbo].[DealerFinancialStatement] ADD 
	CONSTRAINT [FK_DealerFinancialStatement_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	)
GO

ALTER TABLE [dbo].[DealerGridThresholds] ADD 
	CONSTRAINT [FK_DealerGridThresholds_BusinessUnit] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	)
GO

ALTER TABLE [dbo].[DealerGridValues] ADD 
	CONSTRAINT [FK_DealerGridValues_BusinessUnit] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	)
GO

ALTER TABLE [dbo].[DealerGroupPotentialEvent] ADD 
	CONSTRAINT [FK_DealerGroupPotentialEvent_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_DealerGroupPotentialEvent_TradeAnalyzerEvent] FOREIGN KEY 
	(
		[TradeAnalyzerEventID]
	) REFERENCES [dbo].[TradeAnalyzerEvent] (
		[TradeAnalyzerEventID]
	)
GO

ALTER TABLE [dbo].[DealerGroupPreference] ADD 
	CONSTRAINT [FK_DealerGroupPreference_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	)
GO

ALTER TABLE [dbo].[DealerPack] ADD 
	CONSTRAINT [FK_DealerPack_BusinessUnit] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	) ON DELETE CASCADE  ON UPDATE CASCADE ,
	CONSTRAINT [FK_DealerPack_lu_DealerPackType] FOREIGN KEY 
	(
		[PackTypeCD]
	) REFERENCES [dbo].[lu_DealerPackType] (
		[PackTypeCD]
	) ON UPDATE CASCADE 
GO

ALTER TABLE [dbo].[DealerPreference] ADD 
	CONSTRAINT [FK_DealerPreference_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_DealerPreference_GuideBookId] FOREIGN KEY 
	(
		[BookOutPreferenceId]
	) REFERENCES [dbo].[GuideBook] (
		[GuideBookID]
	),
	CONSTRAINT [FK_DealerPreference_lu_ProgramType] FOREIGN KEY 
	(
		[ProgramType_CD]
	) REFERENCES [dbo].[lu_ProgramType] (
		[ProgramType_CD]
	)
GO

ALTER TABLE [dbo].[DealerPreferenceDataload] ADD 
	CONSTRAINT [FK_DealerPreferenceDataload_DealerPreference] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[DealerPreference] (
		[BusinessUnitID]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

ALTER TABLE [dbo].[DealerRisk] ADD 
	CONSTRAINT [FK_DealerRisk_DealerPreference] FOREIGN KEY 
	(
		[BusinessUnitId]
	) REFERENCES [dbo].[DealerPreference] (
		[BusinessUnitID]
	) ON DELETE CASCADE 
GO

ALTER TABLE [dbo].[DealerUpgrade] ADD 
	CONSTRAINT [FK_DealerUpgrade_lu_DealerUpgrade] FOREIGN KEY 
	(
		[DealerUpgradeCD]
	) REFERENCES [dbo].[lu_DealerUpgrade] (
		[DealerUpgradeCD]
	)
GO

ALTER TABLE [dbo].[DecodedSquishVINs] ADD 
	CONSTRAINT [FK_DecodedSquishVINs_lu_DisplayBodyType] FOREIGN KEY 
	(
		[DisplayBodyTypeID]
	) REFERENCES [dbo].[lu_DisplayBodyType] (
		[DisplayBodyTypeID]
	)
GO

ALTER TABLE [dbo].[FirstLookRegionToZip] ADD 
	CONSTRAINT [FK_FirstLookRegionToZip_FirstLookRegionID] FOREIGN KEY 
	(
		[FirstLookRegionID]
	) REFERENCES [dbo].[FirstLookRegion] (
		[FirstLookRegionID]
	)
GO

ALTER TABLE [dbo].[FranchiseAlias] ADD 
	CONSTRAINT [FK_FranchiseAlias_FranchiseID] FOREIGN KEY 
	(
		[FranchiseID]
	) REFERENCES [dbo].[Franchise] (
		[FranchiseID]
	)
GO

ALTER TABLE [dbo].[GDLight] ADD 
	CONSTRAINT [FK_GDLight_BusinessUnit] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_GDLight_GroupingDescription] FOREIGN KEY 
	(
		[GroupingDescriptionId]
	) REFERENCES [dbo].[tbl_GroupingDescription] (
		[GroupingDescriptionID]
	) ON UPDATE CASCADE ,
	CONSTRAINT [FK_GDLight_lu_VehicleLight] FOREIGN KEY 
	(
		[InventoryVehicleLightID]
	) REFERENCES [dbo].[lu_VehicleLight] (
		[InventoryVehicleLightID]
	)
GO

ALTER TABLE [dbo].[GuideBookValue] ADD 
	CONSTRAINT [FK_GuideBookValue_GuideBookID] FOREIGN KEY 
	(
		[GuideBookID]
	) REFERENCES [dbo].[GuideBook] (
		[GuideBookID]
	),
	CONSTRAINT [FK_GuideBookValue_Inventory] FOREIGN KEY 
	(
		[InventoryID]
	) REFERENCES [dbo].[Inventory] (
		[InventoryID]
	)
GO

ALTER TABLE [dbo].[Inventory] ADD 
	CONSTRAINT [FK_Inventory_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_Inventory_InventoryType] FOREIGN KEY 
	(
		[InventoryType]
	) REFERENCES [dbo].[lu_InventoryVehicleType] (
		[InventoryVehicleTypeCD]
	),
	CONSTRAINT [FK_Inventory_tbl_Vehicle_VehicleID] FOREIGN KEY 
	(
		[VehicleID]
	) REFERENCES [dbo].[tbl_Vehicle] (
		[VehicleID]
	)
GO

ALTER TABLE [dbo].[InventoryBucketRanges] ADD 
	CONSTRAINT [FK_InventoryBucketRanges_InventoryBuckets] FOREIGN KEY 
	(
		[InventoryBucketID]
	) REFERENCES [dbo].[InventoryBuckets] (
		[InventoryBucketID]
	)
GO

ALTER TABLE [dbo].[InventoryStocking] ADD 
	CONSTRAINT [FK_BusinessUnitId] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_CIASummaryId] FOREIGN KEY 
	(
		[CIASummaryId]
	) REFERENCES [dbo].[tbl_CIASummary] (
		[CIASummaryId]
	),
	CONSTRAINT [FK_DisplayBodyTypeID] FOREIGN KEY 
	(
		[DisplayBodyTypeID]
	) REFERENCES [dbo].[lu_DisplayBodyType] (
		[DisplayBodyTypeID]
	)
GO

ALTER TABLE [dbo].[InventoryStockingModel] ADD 
	CONSTRAINT [FK_InventoryStockingModel_GroupingDescriptionID] FOREIGN KEY 
	(
		[GroupingDescriptionID]
	) REFERENCES [dbo].[tbl_GroupingDescription] (
		[GroupingDescriptionID]
	),
	CONSTRAINT [FK_InventoryStockingModel_InventoryStockingID] FOREIGN KEY 
	(
		[InventoryStockingID]
	) REFERENCES [dbo].[InventoryStocking] (
		[InventoryStockingID]
	)
GO

ALTER TABLE [dbo].[MarketShare] ADD 
	CONSTRAINT [FK_MarketShare_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_MarketShare_GroupingDescriptionID] FOREIGN KEY 
	(
		[GroupingDescriptionID]
	) REFERENCES [dbo].[tbl_GroupingDescription] (
		[GroupingDescriptionID]
	)
GO

ALTER TABLE [dbo].[MarketToZip] ADD 
	CONSTRAINT [FK_MarketToZip_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitId]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	)
GO

ALTER TABLE [dbo].[Member] ADD 
	CONSTRAINT [FK_Member_lu_UserRole] FOREIGN KEY 
	(
		[UserRoleCD]
	) REFERENCES [dbo].[lu_UserRole] (
		[UserRoleCD]
	)
GO

ALTER TABLE [dbo].[MemberAccess] ADD 
	CONSTRAINT [FK_MemberAccess_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_MemberAccess_MemberID] FOREIGN KEY 
	(
		[MemberID]
	) REFERENCES [dbo].[Member] (
		[MemberID]
	)
GO

ALTER TABLE [dbo].[MemberCredential] ADD 
	CONSTRAINT [FK_MemberCredential_CredentialTypeID] FOREIGN KEY 
	(
		[CredentialTypeID]
	) REFERENCES [dbo].[CredentialType] (
		[CredentialTypeID]
	) ON DELETE CASCADE  ON UPDATE CASCADE ,
	CONSTRAINT [FK_MemberCredential_MemberID] FOREIGN KEY 
	(
		[MemberID]
	) REFERENCES [dbo].[Member] (
		[MemberID]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

ALTER TABLE [dbo].[MemberRole] ADD 
	CONSTRAINT [FK_MemberRole_Member] FOREIGN KEY 
	(
		[MemberID]
	) REFERENCES [dbo].[Member] (
		[MemberID]
	),
	CONSTRAINT [FK_MemberRole_Role] FOREIGN KEY 
	(
		[RoleID]
	) REFERENCES [dbo].[Role] (
		[RoleID]
	)
GO

ALTER TABLE [dbo].[OptionDetail] ADD 
	CONSTRAINT [FK_OptionDetail_DataSourceID] FOREIGN KEY 
	(
		[DataSourceID]
	) REFERENCES [dbo].[DataSource] (
		[DataSourceID]
	)
GO

ALTER TABLE [dbo].[SiteAuditEvent] ADD 
	CONSTRAINT [FK_SiteAuditEvent_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_SiteAuditEvent_MemberID] FOREIGN KEY 
	(
		[MemberID]
	) REFERENCES [dbo].[Member] (
		[MemberID]
	)
GO

ALTER TABLE [dbo].[Target] ADD 
	CONSTRAINT [FK_Target_TargetType] FOREIGN KEY 
	(
		[TargetTypeCD]
	) REFERENCES [dbo].[TargetType] (
		[TargetTypeCD]
	)
GO

ALTER TABLE [dbo].[TradeAnalyzerEvent] ADD 
	CONSTRAINT [FK_TradeAnalyzerEvent_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_TradeAnalyzerEvent_MemberID] FOREIGN KEY 
	(
		[MemberID]
	) REFERENCES [dbo].[Member] (
		[MemberID]
	)
GO

ALTER TABLE [dbo].[VehicleBodyStyleMapping] ADD 
	CONSTRAINT [FK_VehicleBodyStyleMapping_VehicleBodyStyle_StandardBodyStyleID] FOREIGN KEY 
	(
		[StandardBodyStyleID]
	) REFERENCES [dbo].[VehicleBodyStyle] (
		[StandardBodyStyleID]
	)
GO

ALTER TABLE [dbo].[VehicleLocatorContactDealerEvent] ADD 
	CONSTRAINT [FK_VehicleLocatorContactDealerEvent_tbl_Vehicle_VehicleID] FOREIGN KEY 
	(
		[VehicleID]
	) REFERENCES [dbo].[tbl_Vehicle] (
		[VehicleID]
	),
	CONSTRAINT [VehicleLocatorContactDealerEvent_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [VehicleLocatorContactDealerEvent_SiteAuditEventID] FOREIGN KEY 
	(
		[SiteAuditEventID]
	) REFERENCES [dbo].[SiteAuditEvent] (
		[SiteAuditEventID]
	)
GO

ALTER TABLE [dbo].[VehicleOption] ADD 
	CONSTRAINT [FK_VehicleOption_tbl_Vehicle_VehicleID] FOREIGN KEY 
	(
		[VehicleID]
	) REFERENCES [dbo].[tbl_Vehicle] (
		[VehicleID]
	)
GO

ALTER TABLE [dbo].[VehiclePhoto] ADD 
	CONSTRAINT [FK_VehiclePhoto_tbl_Vehicle_VehicleID] FOREIGN KEY 
	(
		[VehicleID]
	) REFERENCES [dbo].[tbl_Vehicle] (
		[VehicleID]
	)
GO

ALTER TABLE [dbo].[VehiclePlanTracking] ADD 
	CONSTRAINT [FK_VehiclePlanTracking_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_VehiclePlanTracking_InventoryID] FOREIGN KEY 
	(
		[InventoryID]
	) REFERENCES [dbo].[Inventory] (
		[InventoryID]
	),
	CONSTRAINT [FK_VehiclePlanTracking_VehiclePlanTrackingHeaderID] FOREIGN KEY 
	(
		[VehiclePlanTrackingHeaderID]
	) REFERENCES [dbo].[VehiclePlanTrackingHeader] (
		[VehiclePlanTrackingHeaderID]
	)
GO

ALTER TABLE [dbo].[VehiclePlanTrackingHeader] ADD 
	CONSTRAINT [FK_VehiclePlanTrackingHeader_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	)
GO

ALTER TABLE [dbo].[map_MemberToInvStatusFilter] ADD 
	CONSTRAINT [FK_MemberToInvStatusFilters_InventoryStatusCodes] FOREIGN KEY 
	(
		[InventoryStatusCD]
	) REFERENCES [dbo].[InventoryStatusCodes] (
		[InventoryStatusCD]
	),
	CONSTRAINT [FK_MemberToInvStatusFilters_Member] FOREIGN KEY 
	(
		[MemberID]
	) REFERENCES [dbo].[Member] (
		[MemberID]
	)
GO

ALTER TABLE [dbo].[map_VehicleSubTypeToSubSegment] ADD 
	CONSTRAINT [FK_map_VehicleSubTypeToSubSegment_lu_vehicleSubSegment] FOREIGN KEY 
	(
		[VehicleSubSegmentID]
	) REFERENCES [dbo].[lu_VehicleSubSegment] (
		[VehicleSubSegmentID]
	),
	CONSTRAINT [FK_map_VehicleSubTypeToSubSegment_lu_VehicleSubType] FOREIGN KEY 
	(
		[VehicleSubTypeID]
	) REFERENCES [dbo].[lu_VehicleSubType] (
		[VehicleSubTypeID]
	)
GO

ALTER TABLE [dbo].[map_VehicleTypeToSegment] ADD 
	CONSTRAINT [FK_map_VehicleTypeToSegment_lu_VehicleSegment] FOREIGN KEY 
	(
		[VehicleSegmentID]
	) REFERENCES [dbo].[lu_VehicleSegment] (
		[vehiclesegmentid]
	),
	CONSTRAINT [FK_map_VehicleTypeToSegment_lu_VehicleType] FOREIGN KEY 
	(
		[VehicleTypeID]
	) REFERENCES [dbo].[lu_VehicleType] (
		[VehicleTypeID]
	)
GO

ALTER TABLE [dbo].[tbl_CIABodyTypeDetail] ADD 
	CONSTRAINT [FK_tbl_CIABodyTypeDetail_lu_DisplayBodyType] FOREIGN KEY 
	(
		[DisplayBodyTypeId]
	) REFERENCES [dbo].[lu_DisplayBodyType] (
		[DisplayBodyTypeID]
	) ON DELETE CASCADE  ON UPDATE CASCADE ,
	CONSTRAINT [FK_tbl_CIABodyTypeDetail_tbl_CIASummary] FOREIGN KEY 
	(
		[CIASummaryId]
	) REFERENCES [dbo].[tbl_CIASummary] (
		[CIASummaryId]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

ALTER TABLE [dbo].[tbl_CIACompositeTimePeriod] ADD 
	CONSTRAINT [FK_tbl_CIACompositeTimePeriod_BusinessUnit] FOREIGN KEY 
	(
		[ForecastTimePeriodId]
	) REFERENCES [dbo].[lu_CIATimePeriod] (
		[CIATimePeriodId]
	),
	CONSTRAINT [FK_tbl_CIACompositeTimePeriod_lu_CIATimePeriod_Prior] FOREIGN KEY 
	(
		[PriorTimePeriodId]
	) REFERENCES [dbo].[lu_CIATimePeriod] (
		[CIATimePeriodId]
	)
GO

ALTER TABLE [dbo].[tbl_CIAGroupingItem] ADD 
	CONSTRAINT [FK_tbl_CIAGroupingItem_CIACategory] FOREIGN KEY 
	(
		[CIACategoryId]
	) REFERENCES [dbo].[lu_CIACategory] (
		[CIACategoryId]
	),
	CONSTRAINT [FK_tbl_CIAGroupingItem_GroupingDescription] FOREIGN KEY 
	(
		[GroupingDescriptionId]
	) REFERENCES [dbo].[tbl_GroupingDescription] (
		[GroupingDescriptionID]
	),
	CONSTRAINT [FK_tbl_CIAGroupingItem_tbl_CIAInventoryStocking] FOREIGN KEY 
	(
		[CIAInventoryStockingId]
	) REFERENCES [dbo].[tbl_CIAInventoryStocking] (
		[CIAInventoryStockingId]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

ALTER TABLE [dbo].[tbl_CIAInventoryStocking] ADD 
	CONSTRAINT [FK_tbl_CIAInventoryStocking_lu_CIACategory] FOREIGN KEY 
	(
		[CIACategoryId]
	) REFERENCES [dbo].[lu_CIACategory] (
		[CIACategoryId]
	),
	CONSTRAINT [FK_tbl_CIAInventoryStocking_tbl_CIABodyTypeDetail] FOREIGN KEY 
	(
		[CIABodyTypeDetailId]
	) REFERENCES [dbo].[tbl_CIABodyTypeDetail] (
		[CIABodyTypeDetailId]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

ALTER TABLE [dbo].[tbl_CIAPlan] ADD 
	CONSTRAINT [FK_tbl_CIAPlan_lu_CIAPlanType] FOREIGN KEY 
	(
		[CIAPlanTypeId]
	) REFERENCES [dbo].[lu_CIAPlanType] (
		[CIAPlanTypeId]
	),
	CONSTRAINT [FK_tbl_CIAPlan_tbl_CIAGroupingItem] FOREIGN KEY 
	(
		[CIAGroupingItemId]
	) REFERENCES [dbo].[tbl_CIAGroupingItem] (
		[CIAGroupingItemId]
	)
GO

ALTER TABLE [dbo].[tbl_CIAPreferences] ADD 
	CONSTRAINT [FK_tbl_CIAPreferences_BusinessUnit] FOREIGN KEY 
	(
		[BusinessUnitId]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	) ON DELETE CASCADE  ON UPDATE CASCADE ,
	CONSTRAINT [FK_tbl_CIAPreferences_CIABucketAllocationTimePeriod] FOREIGN KEY 
	(
		[CIAAllocationTimePeriodId]
	) REFERENCES [dbo].[tbl_CIACompositeTimePeriod] (
		[CIACompositeTimePeriodId]
	),
	CONSTRAINT [FK_tbl_CIAPreferences_CIAgdLightsProcessorTimePeriod] FOREIGN KEY 
	(
		[GDLightProcessorTimePeriodId]
	) REFERENCES [dbo].[tbl_CIACompositeTimePeriod] (
		[CIACompositeTimePeriodId]
	),
	CONSTRAINT [FK_tbl_CIAPreferences_CIASalesHistoryDisplayTimePeriodId] FOREIGN KEY 
	(
		[SalesHistoryDisplayTimePeriodId]
	) REFERENCES [dbo].[tbl_CIACompositeTimePeriod] (
		[CIACompositeTimePeriodId]
	),
	CONSTRAINT [FK_tbl_CIAPreferences_CIATargetUnitsTimePeriod] FOREIGN KEY 
	(
		[CIATargetUnitsTimePeriodId]
	) REFERENCES [dbo].[tbl_CIACompositeTimePeriod] (
		[CIACompositeTimePeriodId]
	)
GO

ALTER TABLE [dbo].[tbl_CIASummary] ADD 
	CONSTRAINT [FK_tbl_CIASummary_BusinessUnit] FOREIGN KEY 
	(
		[BusinessUnitId]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_tbl_CIASummary_lu_Status] FOREIGN KEY 
	(
		[StatusId]
	) REFERENCES [dbo].[lu_Status] (
		[StatusId]
	)
GO

ALTER TABLE [dbo].[tbl_CIAUnitCostPointPlan] ADD 
	CONSTRAINT [FK_tbl_CIAUnitCostPointPlan_tbl_CIAUnitCostPointRange] FOREIGN KEY 
	(
		[CIAUnitCostPointRangeId]
	) REFERENCES [dbo].[tbl_CIAUnitCostPointRange] (
		[CIAUnitCostPointRangeId]
	)
GO

ALTER TABLE [dbo].[tbl_CIAUnitCostPointRange] ADD 
	CONSTRAINT [FK_tbl_CIAUnitCostPointRange_tbl_CIAGroupingItem] FOREIGN KEY 
	(
		[CIAGroupingItemId]
	) REFERENCES [dbo].[tbl_CIAGroupingItem] (
		[CIAGroupingItemId]
	)
GO

ALTER TABLE [dbo].[tbl_DealerValuation] ADD 
	CONSTRAINT [FK_DealerValuation_BusinessUnit] FOREIGN KEY 
	(
		[DealerId]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	)
GO

ALTER TABLE [dbo].[tbl_GroupingPromotionPlan] ADD 
	CONSTRAINT [FK_GroupingPromotionPlan_tbl_GroupingDescription] FOREIGN KEY 
	(
		[GroupingDescriptionId]
	) REFERENCES [dbo].[tbl_GroupingDescription] (
		[GroupingDescriptionID]
	),
	CONSTRAINT [FK_GroupingPromotionPlan_VehiclePlanTrackingHeader] FOREIGN KEY 
	(
		[VehiclePlanTrackingHeaderId]
	) REFERENCES [dbo].[VehiclePlanTrackingHeader] (
		[VehiclePlanTrackingHeaderID]
	)
GO

ALTER TABLE [dbo].[tbl_InventoryOverstocking] ADD 
	CONSTRAINT [FK_tbl_InventoryOverstocking__Inventory] FOREIGN KEY 
	(
		[InventoryId]
	) REFERENCES [dbo].[Inventory] (
		[InventoryID]
	)
GO

ALTER TABLE [dbo].[tbl_MakeModelGrouping] ADD 
	CONSTRAINT [FK_MakeModelGrouping_GroupingDescriptionID] FOREIGN KEY 
	(
		[GroupingDescriptionID]
	) REFERENCES [dbo].[tbl_GroupingDescription] (
		[GroupingDescriptionID]
	),
	CONSTRAINT [FK_MakeModelGrouping_VehicleCategoryID] FOREIGN KEY 
	(
		[CategoryID]
	) REFERENCES [dbo].[VehicleCategory] (
		[VehicleCategoryID]
	),
	CONSTRAINT [FK_tbl_MakeModelGrouping_lu_DisplayBodyType] FOREIGN KEY 
	(
		[DisplayBodyTypeID]
	) REFERENCES [dbo].[lu_DisplayBodyType] (
		[DisplayBodyTypeID]
	)
GO

ALTER TABLE [dbo].[tbl_NewCarScoreCard] ADD 
	CONSTRAINT [FK_NewCarScoreCard_BusinessUnit] FOREIGN KEY 
	(
		[BusinessUnitId]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	)
GO

ALTER TABLE [dbo].[tbl_RedistributionLog] ADD 
	CONSTRAINT [FK_tbl_RedistributionLog_Dealer] FOREIGN KEY 
	(
		[OriginatingDealerId]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_tbl_RedistributionLog_lu_RedistributionSource] FOREIGN KEY 
	(
		[RedistributionSourceId]
	) REFERENCES [dbo].[lu_RedistributionSource] (
		[RedistributionSourceId]
	),
	CONSTRAINT [FK_tbl_RedistributionLog_Member] FOREIGN KEY 
	(
		[MemberId]
	) REFERENCES [dbo].[Member] (
		[MemberID]
	)
GO

ALTER TABLE [dbo].[tbl_UsedCarScoreCard] ADD 
	CONSTRAINT [FK_UsedCarScoreCard_BusinessUnit] FOREIGN KEY 
	(
		[BusinessUnitId]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	)
GO

ALTER TABLE [dbo].[tbl_Vehicle] ADD 
	CONSTRAINT [FK_tbl_Vehicle_MakeModelGrouping] FOREIGN KEY 
	(
		[MakeModelGroupingID]
	) REFERENCES [dbo].[tbl_MakeModelGrouping] (
		[MakeModelGroupingID]
	),
	CONSTRAINT [FK_tbl_Vehicle_VehicleBodyStyleMapping] FOREIGN KEY 
	(
		[VehicleBodyStyleID]
	) REFERENCES [dbo].[VehicleBodyStyleMapping] (
		[VehicleBodyStyleMappingID]
	)
GO

ALTER TABLE [dbo].[tbl_VehicleAttributes] ADD 
	CONSTRAINT [FK_tbl_VehicleAttributes_tbl_Vehicle] FOREIGN KEY 
	(
		[VehicleID]
	) REFERENCES [dbo].[tbl_Vehicle] (
		[VehicleID]
	),
	CONSTRAINT [FK_VehicleAttributes_lu_VehicleSegment] FOREIGN KEY 
	(
		[VehicleSegmentID]
	) REFERENCES [dbo].[lu_VehicleSegment] (
		[vehiclesegmentid]
	),
	CONSTRAINT [FK_VehicleAttributes_lu_vehicleSubSegment] FOREIGN KEY 
	(
		[VehicleSubSegmentID]
	) REFERENCES [dbo].[lu_VehicleSubSegment] (
		[VehicleSubSegmentID]
	)
GO

ALTER TABLE [dbo].[tbl_VehicleGuideBookIdentifier] ADD 
	CONSTRAINT [FK_tbl_VehicleGuideBookIdentifier_lu_GuideBook] FOREIGN KEY 
	(
		[GuideBookID]
	) REFERENCES [dbo].[lu_GuideBook] (
		[GuideBookId]
	),
	CONSTRAINT [FK_tbl_VehicleGuideBookIdentifier_tbl_Vehicle] FOREIGN KEY 
	(
		[VehicleID]
	) REFERENCES [dbo].[tbl_Vehicle] (
		[VehicleID]
	)
GO

ALTER TABLE [dbo].[tbl_VehicleSale] ADD 
	CONSTRAINT [FK_tbl_VehicleSale_Inventory] FOREIGN KEY 
	(
		[InventoryID]
	) REFERENCES [dbo].[Inventory] (
		[InventoryID]
	)
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

/****** Object:  Trigger dbo.TR_NewDealerRisk    Script Date: 7/14/2005 11:26:10 AM ******/
CREATE TRIGGER [TR_NewDealerRisk] ON dbo.DealerPreference 
FOR INSERT
AS
INSERT INTO [dbo].[DealerRisk] (BusinessUnitId) SELECT i.BusinessUnitId FROM inserted i

GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

