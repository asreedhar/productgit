/*
Run this script on:

        WHUMMEL02.IMT    -  This database will be modified

to synchronize it with:

        PRODDB01SQL.IMT

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.1.0 from Red Gate Software Ltd at 8/20/2010 3:57:36 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'firstlook')
CREATE LOGIN [firstlook] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [firstlook] FOR LOGIN [firstlook] WITH DEFAULT_SCHEMA=[dbo]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Firstlook\QA')
CREATE LOGIN [Firstlook\QA] FROM WINDOWS
GO
CREATE USER [Firstlook\QA] FOR LOGIN [Firstlook\QA]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'FirstlookReports')
CREATE LOGIN [FirstlookReports] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [FirstlookReports] FOR LOGIN [FirstlookReports] WITH DEFAULT_SCHEMA=[FirstlookReports]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Firstlook\Analytics')
CREATE LOGIN [Firstlook\Analytics] FROM WINDOWS
GO
CREATE USER [Firstlook\Analytics] FOR LOGIN [Firstlook\Analytics]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'FIRSTLOOK\Engineering')
CREATE LOGIN [FIRSTLOOK\Engineering] FROM WINDOWS
GO
CREATE USER [FIRSTLOOK\Engineering] FOR LOGIN [FIRSTLOOK\Engineering]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Firstlook\SQLAdminReports')
CREATE LOGIN [Firstlook\SQLAdminReports] FROM WINDOWS
GO
CREATE USER [Firstlook\SQLAdminReports] FOR LOGIN [Firstlook\SQLAdminReports]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'DataDictionaryUser')
CREATE LOGIN [DataDictionaryUser] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [DataDictionaryUser] FOR LOGIN [DataDictionaryUser] WITH DEFAULT_SCHEMA=[DataDictionaryUser]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Firstlook\SQLSupport')
CREATE LOGIN [Firstlook\SQLSupport] FROM WINDOWS
GO
CREATE USER [Firstlook\SQLSupport] FOR LOGIN [Firstlook\SQLSupport]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'qa_admin')
CREATE LOGIN [qa_admin] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [qa_admin] FOR LOGIN [qa_admin] WITH DEFAULT_SCHEMA=[qa_admin]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'ProcessLogin')
CREATE LOGIN [ProcessLogin] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [ProcessLogin] FOR LOGIN [ProcessLogin] WITH DEFAULT_SCHEMA=[ProcessLogin]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'asc_admin')
CREATE LOGIN [asc_admin] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [asc_admin] FOR LOGIN [asc_admin] WITH DEFAULT_SCHEMA=[asc_admin]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'qwestnms')
CREATE LOGIN [qwestnms] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [qwestnms] FOR LOGIN [qwestnms] WITH DEFAULT_SCHEMA=[dbo]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'SalesForceExtractorService')
CREATE LOGIN [SalesForceExtractorService] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [SalesForceExtractorService] FOR LOGIN [SalesForceExtractorService] WITH DEFAULT_SCHEMA=[dbo]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'FIRSTLOOK\DataManagementReader')
CREATE LOGIN [FIRSTLOOK\DataManagementReader] FROM WINDOWS
GO
CREATE USER [FIRSTLOOK\DataManagementReader] FOR LOGIN [FIRSTLOOK\DataManagementReader]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'FIRSTLOOK\DMSi')
CREATE LOGIN [FIRSTLOOK\DMSi] FROM WINDOWS
GO
CREATE USER [Firstlook\DMSi] FOR LOGIN [FIRSTLOOK\DMSi]
GO
CREATE USER [Firstlook\DataManagement] WITHOUT LOGIN
GO
REVOKE CONNECT FROM [Firstlook\DataManagement]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'FIRSTLOOK\ApplicationSupport')
CREATE LOGIN [FIRSTLOOK\ApplicationSupport] FROM WINDOWS
GO
CREATE USER [FIRSTLOOK\ApplicationSupport] FOR LOGIN [FIRSTLOOK\ApplicationSupport]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'orion')
CREATE LOGIN [orion] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [orion] FOR LOGIN [orion] WITH DEFAULT_SCHEMA=[dbo]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'SIS_Reader')
CREATE LOGIN [SIS_Reader] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [SIS_Reader] FOR LOGIN [SIS_Reader] WITH DEFAULT_SCHEMA=[dbo]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'FIRSTLOOK\DMSi#DataloadAdmin')
CREATE LOGIN [FIRSTLOOK\DMSi#DataloadAdmin] FROM WINDOWS
GO
CREATE USER [FIRSTLOOK\DMSi#DataloadAdmin] FOR LOGIN [FIRSTLOOK\DMSi#DataloadAdmin]
REVOKE CONNECT FROM [FIRSTLOOK\DMSi#DataloadAdmin]
GO
REVOKE CONNECT FROM [FIRSTLOOK\DMSi#DataloadAdmin]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'MerchandisingInterface')
CREATE LOGIN [MerchandisingInterface] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [MerchandisingInterface] FOR LOGIN [MerchandisingInterface] WITH DEFAULT_SCHEMA=[dbo]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'DataReader')
CREATE LOGIN [DataReader] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [DataReader] FOR LOGIN [DataReader] WITH DEFAULT_SCHEMA=[dbo]
GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'FIRSTLOOK\DatabaseEngineers')
CREATE LOGIN [FIRSTLOOK\DatabaseEngineers] FROM WINDOWS
GO
CREATE USER [FIRSTLOOK\DatabaseEngineers] FOR LOGIN [FIRSTLOOK\DatabaseEngineers]
GO
--CREATE USER [ServiceBrokerActivationUser] FOR CERTIFICATE [ServiceBrokerActivationCertificate]
--GO
IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'FIRSTLOOK\CustomerOperations')
CREATE LOGIN [FIRSTLOOK\CustomerOperations] FROM WINDOWS
GO
CREATE USER [FIRSTLOOK\CustomerOperations] FOR LOGIN [FIRSTLOOK\CustomerOperations]
GO
PRINT N'Creating role ApplicationSupportManager'
GO
CREATE ROLE [ApplicationSupportManager]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'ApplicationSupportManager', N'FIRSTLOOK\ApplicationSupport'
GO
PRINT N'Creating role CarfaxProcessorService'
GO
CREATE ROLE [CarfaxProcessorService]
AUTHORIZATION [dbo]
GO
PRINT N'Creating role MerchandisingUser'
GO
CREATE ROLE [MerchandisingUser]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'MerchandisingUser', N'MerchandisingInterface'
GO
PRINT N'Creating role DataManagement'
GO
CREATE ROLE [DataManagement]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'DataManagement', N'FIRSTLOOK\ApplicationSupport'
GO
EXEC sp_addrolemember N'DataManagement', N'Firstlook\DataManagement'
GO
EXEC sp_addrolemember N'DataManagement', N'Firstlook\DMSi'
GO
PRINT N'Creating role CarfaxUser'
GO
CREATE ROLE [CarfaxUser]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'CarfaxUser', N'FIRSTLOOK\Engineering'
GO
PRINT N'Creating role AutoCheckUser'
GO
CREATE ROLE [AutoCheckUser]
AUTHORIZATION [firstlook]
GO
EXEC sp_addrolemember N'AutoCheckUser', N'FIRSTLOOK\Engineering'
GO
PRINT N'Creating role ApplicationUser'
GO
CREATE ROLE [ApplicationUser]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'ApplicationUser', N'firstlook'
GO
PRINT N'Creating role DataloadAdmin'
GO
CREATE ROLE [DataloadAdmin]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'DataloadAdmin', N'FIRSTLOOK\DMSi#DataloadAdmin'
GO
PRINT N'Creating role PurchasingUser'
GO
CREATE ROLE [PurchasingUser]
AUTHORIZATION [dbo]
GO
PRINT N'Creating role MarketingUser'
GO
CREATE ROLE [MarketingUser]
AUTHORIZATION [dbo]
GO
PRINT N'Creating role CustomerOperations'
GO
CREATE ROLE [CustomerOperations]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'CustomerOperations', N'FIRSTLOOK\CustomerOperations'
GO
PRINT N'Creating role AdminReports'
GO
CREATE ROLE [AdminReports]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'AdminReports', N'Firstlook\SQLAdminReports'
GO
PRINT N'Creating role ReportCenter'
GO
CREATE ROLE [ReportCenter]
AUTHORIZATION [dbo]
GO
EXEC sp_addrolemember N'ReportCenter', N'FirstlookReports'
GO
PRINT N'Altering members of role db_owner'
GO
EXEC sp_addrolemember N'db_owner', N'DataReader'
GO
EXEC sp_addrolemember N'db_owner', N'firstlook'
GO
PRINT N'Altering members of role db_datareader'
GO
EXEC sp_addrolemember N'db_datareader', N'asc_admin'
GO
EXEC sp_addrolemember N'db_datareader', N'CustomerOperations'
GO
EXEC sp_addrolemember N'db_datareader', N'DataDictionaryUser'
GO
EXEC sp_addrolemember N'db_datareader', N'DataReader'
GO
EXEC sp_addrolemember N'db_datareader', N'firstlook'
GO
EXEC sp_addrolemember N'db_datareader', N'Firstlook\Analytics'
GO
EXEC sp_addrolemember N'db_datareader', N'FIRSTLOOK\ApplicationSupport'
GO
EXEC sp_addrolemember N'db_datareader', N'FIRSTLOOK\DatabaseEngineers'
GO
EXEC sp_addrolemember N'db_datareader', N'FIRSTLOOK\DataManagementReader'
GO
EXEC sp_addrolemember N'db_datareader', N'FIRSTLOOK\Engineering'
GO
EXEC sp_addrolemember N'db_datareader', N'Firstlook\QA'
GO
EXEC sp_addrolemember N'db_datareader', N'Firstlook\SQLSupport'
GO
EXEC sp_addrolemember N'db_datareader', N'FirstlookReports'
GO
EXEC sp_addrolemember N'db_datareader', N'MerchandisingInterface'
GO
EXEC sp_addrolemember N'db_datareader', N'orion'
GO
EXEC sp_addrolemember N'db_datareader', N'ProcessLogin'
GO
EXEC sp_addrolemember N'db_datareader', N'qa_admin'
GO
--EXEC sp_addrolemember N'db_datareader', N'ServiceBrokerActivationUser'
--GO
EXEC sp_addrolemember N'db_datareader', N'SIS_Reader'
GO
PRINT N'Altering members of role db_datawriter'
GO
EXEC sp_addrolemember N'db_datawriter', N'firstlook'
GO
EXEC sp_addrolemember N'db_datawriter', N'ProcessLogin'
GO
BEGIN TRANSACTION
GO
PRINT N'Creating schemata'
GO
CREATE SCHEMA [firstlook]
AUTHORIZATION [firstlook]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [Firstlook\QA]
AUTHORIZATION [Firstlook\QA]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [FirstlookReports]
AUTHORIZATION [FirstlookReports]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [Firstlook\Analytics]
AUTHORIZATION [Firstlook\Analytics]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [Bookout]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [Firstlook\SQLAdminReports]
AUTHORIZATION [Firstlook\SQLAdminReports]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [DataDictionaryUser]
AUTHORIZATION [DataDictionaryUser]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [Firstlook\SQLSupport]
AUTHORIZATION [Firstlook\SQLSupport]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [qa_admin]
AUTHORIZATION [qa_admin]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [ProcessLogin]
AUTHORIZATION [ProcessLogin]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [Utility]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [asc_admin]
AUTHORIZATION [asc_admin]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [Carfax]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [AutoCheck]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [Extract]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [Purchasing]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [Certified]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [Marketing]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [Configuration]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [Firstlook\DataManagement]
AUTHORIZATION [Firstlook\DataManagement]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [Distribution]
AUTHORIZATION [dbo]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [FIRSTLOOK\DMSi#DataloadAdmin]
AUTHORIZATION [FIRSTLOOK\DMSi#DataloadAdmin]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [AdminReports]
AUTHORIZATION [AdminReports]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
CREATE SCHEMA [ReportCenter]
AUTHORIZATION [ReportCenter]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering schemata'
GO
ALTER AUTHORIZATION ON SCHEMA::db_datareader
TO [AdminReports]
GO
PRINT N'Creating [Marketing].[VehicleEquipmentProvider]'
GO
CREATE TABLE [Marketing].[VehicleEquipmentProvider]
(
[OwnerID] [int] NOT NULL,
[VehicleEntityTypeID] [tinyint] NOT NULL,
[VehicleEntityID] [int] NOT NULL,
[EquipmentProviderID] [tinyint] NOT NULL,
[Visible] [bit] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUser] [int] NULL,
[UpdateDate] [datetime] NULL,
[Version] [timestamp] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Marketing_VehicleEquipmentProvider] on [Marketing].[VehicleEquipmentProvider]'
GO
ALTER TABLE [Marketing].[VehicleEquipmentProvider] ADD CONSTRAINT [PK_Marketing_VehicleEquipmentProvider] PRIMARY KEY CLUSTERED  ([OwnerID], [VehicleEntityTypeID], [VehicleEntityID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InternetAdvertiserBuildLogDetail]'
GO
CREATE TABLE [dbo].[InternetAdvertiserBuildLogDetail]
(
[InternetAdvertiserBuildLogID] [int] NOT NULL,
[InventoryID] [int] NOT NULL,
[ListedPrice] [int] NOT NULL,
[ListedPriceSource] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Highlights] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SendZeroesAsNull] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InternetAdvertiserBuildLogDetail] on [dbo].[InternetAdvertiserBuildLogDetail]'
GO
ALTER TABLE [dbo].[InternetAdvertiserBuildLogDetail] ADD CONSTRAINT [PK_InternetAdvertiserBuildLogDetail] PRIMARY KEY CLUSTERED  ([InternetAdvertiserBuildLogID], [InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InternetAdvertiser_ThirdPartyEntity]'
GO
CREATE TABLE [dbo].[InternetAdvertiser_ThirdPartyEntity]
(
[InternetAdvertiserID] [int] NOT NULL,
[DatafeedCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InternetAdvertiser_ThirdPartyEntity] on [dbo].[InternetAdvertiser_ThirdPartyEntity]'
GO
ALTER TABLE [dbo].[InternetAdvertiser_ThirdPartyEntity] ADD CONSTRAINT [PK_InternetAdvertiser_ThirdPartyEntity] PRIMARY KEY CLUSTERED  ([InternetAdvertiserID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Segment]'
GO
CREATE TABLE [dbo].[Segment]
(
[SegmentID] [int] NOT NULL,
[Segment] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Segment] on [dbo].[Segment]'
GO
ALTER TABLE [dbo].[Segment] ADD CONSTRAINT [PK_Segment] PRIMARY KEY NONCLUSTERED  ([SegmentID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[ConsumerHighlight]'
GO
CREATE TABLE [Marketing].[ConsumerHighlight]
(
[ConsumerHighlightId] [int] NOT NULL IDENTITY(1, 1),
[ConsumerHighlightVehiclePreferenceId] [int] NOT NULL,
[HighlightProviderId] [tinyint] NOT NULL,
[Text] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Rank] [int] NOT NULL,
[IsDisplayed] [bit] NOT NULL,
[ExpirationDate] [datetime] NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUser] [int] NULL,
[UpdateDate] [datetime] NULL,
[ProviderSourceRowId] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Marketing_ConsumerHighlight] on [Marketing].[ConsumerHighlight]'
GO
ALTER TABLE [Marketing].[ConsumerHighlight] ADD CONSTRAINT [PK_Marketing_ConsumerHighlight] PRIMARY KEY CLUSTERED  ([ConsumerHighlightId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[MemberAccess]'
GO
CREATE TABLE [dbo].[MemberAccess]
(
[MemberID] [int] NOT NULL,
[BusinessUnitID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MemberAccess] on [dbo].[MemberAccess]'
GO
ALTER TABLE [dbo].[MemberAccess] ADD CONSTRAINT [PK_MemberAccess] PRIMARY KEY CLUSTERED  ([MemberID], [BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[MemberRole]'
GO
CREATE TABLE [dbo].[MemberRole]
(
[MemberRoleID] [int] NOT NULL IDENTITY(1, 1),
[MemberID] [int] NOT NULL,
[RoleID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MemberRole] on [dbo].[MemberRole]'
GO
ALTER TABLE [dbo].[MemberRole] ADD CONSTRAINT [PK_MemberRole] PRIMARY KEY CLUSTERED  ([MemberRoleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_CIATimePeriod]'
GO
CREATE TABLE [dbo].[lu_CIATimePeriod]
(
[CIATimePeriodId] [int] NOT NULL IDENTITY(1, 1),
[Days] [int] NOT NULL,
[Forecast] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_CIATimePeriod] on [dbo].[lu_CIATimePeriod]'
GO
ALTER TABLE [dbo].[lu_CIATimePeriod] ADD CONSTRAINT [PK_lu_CIATimePeriod] PRIMARY KEY NONCLUSTERED  ([CIATimePeriodId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tbl_CIACompositeTimePeriod]'
GO
CREATE TABLE [dbo].[tbl_CIACompositeTimePeriod]
(
[CIACompositeTimePeriodId] [int] NOT NULL IDENTITY(1, 1),
[PriorTimePeriodId] [int] NOT NULL,
[ForecastTimePeriodId] [int] NOT NULL,
[Description] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tbl_CIACompositeTimePeriod] on [dbo].[tbl_CIACompositeTimePeriod]'
GO
ALTER TABLE [dbo].[tbl_CIACompositeTimePeriod] ADD CONSTRAINT [PK_tbl_CIACompositeTimePeriod] PRIMARY KEY NONCLUSTERED  ([CIACompositeTimePeriodId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[HighlightProvider]'
GO
CREATE TABLE [Marketing].[HighlightProvider]
(
[HighlightProviderId] [tinyint] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConsumerHighlightSectionId] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Marketing_HighlightProvider] on [Marketing].[HighlightProvider]'
GO
ALTER TABLE [Marketing].[HighlightProvider] ADD CONSTRAINT [PK_Marketing_HighlightProvider] PRIMARY KEY CLUSTERED  ([HighlightProviderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[ConsumerHighlightVehiclePreference]'
GO
CREATE TABLE [Marketing].[ConsumerHighlightVehiclePreference]
(
[ConsumerHighlightVehiclePreferenceId] [int] NOT NULL IDENTITY(1, 1),
[OwnerId] [int] NOT NULL,
[VehicleEntityId] [int] NOT NULL,
[VehicleEntityTypeID] [int] NOT NULL,
[IsDisplayed] [bit] NOT NULL,
[IncludeCarfaxOneOwnerIcon] [bit] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUser] [int] NULL,
[UpdateDate] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Marketing_ConsumerHighlightVehiclePreference] on [Marketing].[ConsumerHighlightVehiclePreference]'
GO
ALTER TABLE [Marketing].[ConsumerHighlightVehiclePreference] ADD CONSTRAINT [PK_Marketing_ConsumerHighlightVehiclePreference] PRIMARY KEY CLUSTERED  ([ConsumerHighlightVehiclePreferenceId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerInsightTargetPreference]'
GO
CREATE TABLE [dbo].[DealerInsightTargetPreference]
(
[DealerInsightTargetPreferenceID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[InsightTargetID] [int] NOT NULL,
[Value] [int] NULL,
[Min] [int] NULL,
[Max] [int] NULL,
[Threshold] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DealerInsightTargetPreference] on [dbo].[DealerInsightTargetPreference]'
GO
ALTER TABLE [dbo].[DealerInsightTargetPreference] ADD CONSTRAINT [PK_DealerInsightTargetPreference] PRIMARY KEY CLUSTERED  ([DealerInsightTargetPreferenceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InsightTarget]'
GO
CREATE TABLE [dbo].[InsightTarget]
(
[InsightTargetID] [int] NOT NULL IDENTITY(1, 1),
[InsightTargetTypeID] [int] NOT NULL,
[InsightTargetValueTypeID] [int] NOT NULL,
[InsightTypeID] [int] NOT NULL,
[InsightContext] [int] NULL,
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Value] [int] NULL,
[Min] [int] NULL,
[Max] [int] NULL,
[Threshold] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InsightTarget] on [dbo].[InsightTarget]'
GO
ALTER TABLE [dbo].[InsightTarget] ADD CONSTRAINT [PK_InsightTarget] PRIMARY KEY CLUSTERED  ([InsightTargetID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[map_BusinessUnitToMarketDealer]'
GO
CREATE TABLE [dbo].[map_BusinessUnitToMarketDealer]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[DealerNumber] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_map_BusinessUnitToMarketDealer] on [dbo].[map_BusinessUnitToMarketDealer]'
GO
ALTER TABLE [dbo].[map_BusinessUnitToMarketDealer] ADD CONSTRAINT [PK_map_BusinessUnitToMarketDealer] PRIMARY KEY NONCLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ThirdPartyEntity]'
GO
CREATE TABLE [dbo].[ThirdPartyEntity]
(
[ThirdPartyEntityID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[ThirdPartyEntityTypeID] [tinyint] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ThirdPartyEntity] on [dbo].[ThirdPartyEntity]'
GO
ALTER TABLE [dbo].[ThirdPartyEntity] ADD CONSTRAINT [PK_ThirdPartyEntity] PRIMARY KEY CLUSTERED  ([ThirdPartyEntityID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[TargetSection]'
GO
CREATE TABLE [dbo].[TargetSection]
(
[SectionCD] [smallint] NOT NULL,
[SectionName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InventoryVehicleTypeCD] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_TargetSection] on [dbo].[TargetSection]'
GO
ALTER TABLE [dbo].[TargetSection] ADD CONSTRAINT [PK_TargetSection] PRIMARY KEY NONCLUSTERED  ([SectionCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Target]'
GO
CREATE TABLE [dbo].[Target]
(
[TargetCD] [smallint] NOT NULL,
[TargetName] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TargetMin] [int] NULL,
[TargetMax] [int] NULL,
[SectionCD] [smallint] NOT NULL,
[TargetTypeCD] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Target] on [dbo].[Target]'
GO
ALTER TABLE [dbo].[Target] ADD CONSTRAINT [PK_Target] PRIMARY KEY NONCLUSTERED  ([TargetCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerTarget]'
GO
CREATE TABLE [dbo].[DealerTarget]
(
[DealerTargetId] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitId] [int] NOT NULL,
[TargetCD] [smallint] NOT NULL,
[StartDT] [smalldatetime] NOT NULL,
[TargetValue] [int] NOT NULL,
[Active] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DealerTarget] on [dbo].[DealerTarget]'
GO
ALTER TABLE [dbo].[DealerTarget] ADD CONSTRAINT [PK_DealerTarget] PRIMARY KEY CLUSTERED  ([DealerTargetId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AppraisalPhotos]'
GO
CREATE TABLE [dbo].[AppraisalPhotos]
(
[AppraisalID] [int] NOT NULL,
[PhotoID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AppraisalPhotos] on [dbo].[AppraisalPhotos]'
GO
ALTER TABLE [dbo].[AppraisalPhotos] ADD CONSTRAINT [PK_AppraisalPhotos] PRIMARY KEY CLUSTERED  ([AppraisalID], [PhotoID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AppraisalBusinessUnitGroupDemand]'
GO
CREATE TABLE [dbo].[AppraisalBusinessUnitGroupDemand]
(
[AppraisalBusinessUnitGroupDemandID] [int] NOT NULL IDENTITY(1, 1),
[AppraisalID] [int] NOT NULL,
[BusinessUnitID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AppraisalBusinessUnitGroupDemand] on [dbo].[AppraisalBusinessUnitGroupDemand]'
GO
ALTER TABLE [dbo].[AppraisalBusinessUnitGroupDemand] ADD CONSTRAINT [PK_AppraisalBusinessUnitGroupDemand] PRIMARY KEY CLUSTERED  ([AppraisalID], [BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AppraisalCustomer]'
GO
CREATE TABLE [dbo].[AppraisalCustomer]
(
[AppraisalID] [int] NOT NULL,
[Email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Gender] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumber] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AppraisalID] on [dbo].[AppraisalCustomer]'
GO
ALTER TABLE [dbo].[AppraisalCustomer] ADD CONSTRAINT [PK_AppraisalID] PRIMARY KEY CLUSTERED  ([AppraisalID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AppraisalBookouts]'
GO
CREATE TABLE [dbo].[AppraisalBookouts]
(
[BookoutID] [int] NOT NULL,
[AppraisalID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AppraisalBookouts] on [dbo].[AppraisalBookouts]'
GO
ALTER TABLE [dbo].[AppraisalBookouts] ADD CONSTRAINT [PK_AppraisalBookouts] PRIMARY KEY CLUSTERED  ([BookoutID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_AppraisalBookouts__AppraisalID] on [dbo].[AppraisalBookouts]'
GO
CREATE NONCLUSTERED INDEX [IX_AppraisalBookouts__AppraisalID] ON [dbo].[AppraisalBookouts] ([AppraisalID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AppraisalWindowStickers]'
GO
CREATE TABLE [dbo].[AppraisalWindowStickers]
(
[AppraisalWindowStickerID] [int] NOT NULL IDENTITY(1, 1),
[AppraisalID] [int] NOT NULL,
[StockNumber] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellingPrice] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AppraisalWindowStickers] on [dbo].[AppraisalWindowStickers]'
GO
ALTER TABLE [dbo].[AppraisalWindowStickers] ADD CONSTRAINT [PK_AppraisalWindowStickers] PRIMARY KEY NONCLUSTERED  ([AppraisalWindowStickerID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Staging_Bookouts]'
GO
CREATE TABLE [dbo].[Staging_Bookouts]
(
[Staging_BookoutID] [int] NOT NULL IDENTITY(1, 1),
[AppraisalID] [int] NOT NULL,
[ThirdPartyID] [tinyint] NOT NULL,
[ThirdPartyVehicleCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_StagingBookouts] on [dbo].[Staging_Bookouts]'
GO
ALTER TABLE [dbo].[Staging_Bookouts] ADD CONSTRAINT [PK_StagingBookouts] PRIMARY KEY CLUSTERED  ([Staging_BookoutID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Staging_BookoutValues]'
GO
CREATE TABLE [dbo].[Staging_BookoutValues]
(
[Staging_BookoutValueID] [int] NOT NULL IDENTITY(1, 1),
[Staging_BookoutID] [int] NOT NULL,
[ThirdPartyCategoryID] [int] NOT NULL,
[BaseValue] [int] NOT NULL,
[MileageAdjustment] [int] NULL,
[FinalValue] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_StagingBookoutValues] on [dbo].[Staging_BookoutValues]'
GO
ALTER TABLE [dbo].[Staging_BookoutValues] ADD CONSTRAINT [PK_StagingBookoutValues] PRIMARY KEY CLUSTERED  ([Staging_BookoutValueID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Staging_BookoutOptions]'
GO
CREATE TABLE [dbo].[Staging_BookoutOptions]
(
[Staging_BookoutOptionID] [int] NOT NULL IDENTITY(1, 1),
[Staging_BookoutID] [int] NOT NULL,
[ThirdPartyCategoryID] [int] NULL,
[ThirdPartyOptionTypeID] [tinyint] NOT NULL,
[OptionKey] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OptionName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[isSelected] [tinyint] NOT NULL,
[Value] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_StagingBookoutOptions] on [dbo].[Staging_BookoutOptions]'
GO
ALTER TABLE [dbo].[Staging_BookoutOptions] ADD CONSTRAINT [PK_StagingBookoutOptions] PRIMARY KEY CLUSTERED  ([Staging_BookoutOptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Staging_VehicleHistoryReport]'
GO
CREATE TABLE [dbo].[Staging_VehicleHistoryReport]
(
[Staging_VehicleHistoryReportId] [int] NOT NULL IDENTITY(1, 1),
[AppraisalID] [int] NOT NULL,
[CredentialTypeID] [tinyint] NOT NULL,
[UserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_StagingVehicleHistoryReport] on [dbo].[Staging_VehicleHistoryReport]'
GO
ALTER TABLE [dbo].[Staging_VehicleHistoryReport] ADD CONSTRAINT [PK_StagingVehicleHistoryReport] PRIMARY KEY CLUSTERED  ([Staging_VehicleHistoryReportId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerUpgrade]'
GO
CREATE TABLE [dbo].[DealerUpgrade]
(
[BusinessUnitID] [int] NOT NULL,
[DealerUpgradeCD] [tinyint] NOT NULL,
[StartDate] [smalldatetime] NULL,
[EndDate] [smalldatetime] NULL,
[Active] [tinyint] NOT NULL,
[EffectiveDateActive] AS (CONVERT([tinyint],case when dateadd(day,datediff(day,(0),getdate()),(0))>=dateadd(day,datediff(day,(0),coalesce([StartDate],getdate())),(0)) AND dateadd(day,datediff(day,(0),getdate()),(0))<=dateadd(day,datediff(day,(0),coalesce([EndDate],getdate())),(0)) then (1) else (0) end&[Active],0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DealerUpgrade] on [dbo].[DealerUpgrade]'
GO
ALTER TABLE [dbo].[DealerUpgrade] ADD CONSTRAINT [PK_DealerUpgrade] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [DealerUpgradeCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Photos]'
GO
CREATE TABLE [dbo].[Photos]
(
[PhotoID] [int] NOT NULL IDENTITY(1, 1),
[PhotoTypeID] [tinyint] NOT NULL,
[PhotoProviderID] [int] NULL,
[PhotoStatusCode] [tinyint] NOT NULL,
[PhotoURL] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OriginalPhotoURL] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateCreated] [datetime] NOT NULL,
[PhotoUpdated] [datetime] NOT NULL,
[IsPrimaryPhoto] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Photos] on [dbo].[Photos]'
GO
ALTER TABLE [dbo].[Photos] ADD CONSTRAINT [PK_Photos] PRIMARY KEY CLUSTERED  ([PhotoID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryPhotos]'
GO
CREATE TABLE [dbo].[InventoryPhotos]
(
[InventoryID] [int] NOT NULL,
[PhotoID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryPhotos] on [dbo].[InventoryPhotos]'
GO
ALTER TABLE [dbo].[InventoryPhotos] ADD CONSTRAINT [PK_InventoryPhotos] PRIMARY KEY CLUSTERED  ([InventoryID], [PhotoID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PhotoProvider_ThirdPartyEntity]'
GO
CREATE TABLE [dbo].[PhotoProvider_ThirdPartyEntity]
(
[PhotoProviderID] [int] NOT NULL,
[DatafeedCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PhotoStorageCode] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PhotoProvider_ThirdPartyEntity] on [dbo].[PhotoProvider_ThirdPartyEntity]'
GO
ALTER TABLE [dbo].[PhotoProvider_ThirdPartyEntity] ADD CONSTRAINT [PK_PhotoProvider_ThirdPartyEntity] PRIMARY KEY CLUSTERED  ([PhotoProviderID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ThirdPartyOptions]'
GO
CREATE TABLE [dbo].[ThirdPartyOptions]
(
[ThirdPartyOptionID] [int] NOT NULL IDENTITY(1, 1),
[OptionName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OptionKey] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ThirdPartyOptionTypeID] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ThirdPartyOptions] on [dbo].[ThirdPartyOptions]'
GO
ALTER TABLE [dbo].[ThirdPartyOptions] ADD CONSTRAINT [PK_ThirdPartyOptions] PRIMARY KEY CLUSTERED  ([ThirdPartyOptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ThirdPartyOptions__OptionKeyOptionName] on [dbo].[ThirdPartyOptions]'
GO
CREATE NONCLUSTERED INDEX [IX_ThirdPartyOptions__OptionKeyOptionName] ON [dbo].[ThirdPartyOptions] ([OptionKey], [OptionName])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_InventoryVehicleLight]'
GO
CREATE TABLE [dbo].[lu_InventoryVehicleLight]
(
[InventoryVehicleLightCD] [tinyint] NOT NULL,
[InventoryVehicleLightDesc] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_InventoryVehicleLight] on [dbo].[lu_InventoryVehicleLight]'
GO
ALTER TABLE [dbo].[lu_InventoryVehicleLight] ADD CONSTRAINT [PK_lu_InventoryVehicleLight] PRIMARY KEY NONCLUSTERED  ([InventoryVehicleLightCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_InventoryVehicleType]'
GO
CREATE TABLE [dbo].[lu_InventoryVehicleType]
(
[InventoryVehicleTypeCD] [tinyint] NOT NULL,
[InventoryVehicleTypeDescription] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_InventoryVehicleType] on [dbo].[lu_InventoryVehicleType]'
GO
ALTER TABLE [dbo].[lu_InventoryVehicleType] ADD CONSTRAINT [PK_lu_InventoryVehicleType] PRIMARY KEY NONCLUSTERED  ([InventoryVehicleTypeCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_TradeOrPurchase]'
GO
CREATE TABLE [dbo].[lu_TradeOrPurchase]
(
[TradeOrPurchaseCD] [tinyint] NOT NULL,
[TradeOrPurchaseDesc] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[PhotoVehiclePreference]'
GO
CREATE TABLE [Marketing].[PhotoVehiclePreference]
(
[OwnerID] [int] NOT NULL,
[VehicleEntityTypeID] [tinyint] NOT NULL,
[VehicleEntityID] [int] NOT NULL,
[IsDisplayed] [bit] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUser] [int] NULL,
[UpdateDate] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Marketing_PhotoVehiclePreference] on [Marketing].[PhotoVehiclePreference]'
GO
ALTER TABLE [Marketing].[PhotoVehiclePreference] ADD CONSTRAINT [PK_Marketing_PhotoVehiclePreference] PRIMARY KEY CLUSTERED  ([OwnerID], [VehicleEntityTypeID], [VehicleEntityID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[Account_Dealer]'
GO
CREATE TABLE [Carfax].[Account_Dealer]
(
[DealerID] [int] NOT NULL,
[AccountID] [int] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_Account_Dealer] on [Carfax].[Account_Dealer]'
GO
ALTER TABLE [Carfax].[Account_Dealer] ADD CONSTRAINT [PK_Carfax_Account_Dealer] PRIMARY KEY CLUSTERED  ([DealerID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[ReportProcessorCommand]'
GO
CREATE TABLE [Carfax].[ReportProcessorCommand]
(
[ReportProcessorCommandID] [int] NOT NULL IDENTITY(1, 1),
[DealerID] [int] NOT NULL,
[VehicleID] [int] NOT NULL,
[VehicleEntityTypeID] [tinyint] NOT NULL,
[ReportProcessorCommandTypeID] [tinyint] NOT NULL,
[ReportProcessorCommandStatusID] [tinyint] NOT NULL,
[ProcessAfterDate] [datetime] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUser] [int] NULL,
[UpdateDate] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_ReportProcessorCommand] on [Carfax].[ReportProcessorCommand]'
GO
ALTER TABLE [Carfax].[ReportProcessorCommand] ADD CONSTRAINT [PK_Carfax_ReportProcessorCommand] PRIMARY KEY CLUSTERED  ([ReportProcessorCommandID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CredentialType]'
GO
CREATE TABLE [dbo].[CredentialType]
(
[CredentialTypeID] [tinyint] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_CredentialType] on [dbo].[CredentialType]'
GO
ALTER TABLE [dbo].[CredentialType] ADD CONSTRAINT [PK_CredentialType] PRIMARY KEY NONCLUSTERED  ([CredentialTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BusinessUnitCredential]'
GO
CREATE TABLE [dbo].[BusinessUnitCredential]
(
[BusinessUnitID] [int] NOT NULL,
[CredentialTypeID] [tinyint] NOT NULL,
[Username] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReynoldsAreaNumber] [int] NULL,
[ReynoldsDealerNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReynoldsStoreNumber] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BusinessUnitCredential] on [dbo].[BusinessUnitCredential]'
GO
ALTER TABLE [dbo].[BusinessUnitCredential] ADD CONSTRAINT [PK_BusinessUnitCredential] PRIMARY KEY NONCLUSTERED  ([BusinessUnitID], [CredentialTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tbl_DealerValuation]'
GO
CREATE TABLE [dbo].[tbl_DealerValuation]
(
[DealerValuationId] [int] NOT NULL IDENTITY(1, 1),
[DealerId] [int] NOT NULL,
[DiscountRate] [decimal] (6, 4) NULL,
[TimeHorizon] [decimal] (8, 4) NULL,
[Date] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tbl_DealerValuation__DealerIdDate] on [dbo].[tbl_DealerValuation]'
GO
CREATE CLUSTERED INDEX [IX_tbl_DealerValuation__DealerIdDate] ON [dbo].[tbl_DealerValuation] ([DealerId], [Date] DESC)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tbl_DealerValuation] on [dbo].[tbl_DealerValuation]'
GO
ALTER TABLE [dbo].[tbl_DealerValuation] ADD CONSTRAINT [PK_tbl_DealerValuation] PRIMARY KEY NONCLUSTERED  ([DealerValuationId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ApplicationEvent]'
GO
CREATE TABLE [dbo].[ApplicationEvent]
(
[ApplicationEventID] [int] NOT NULL IDENTITY(100000, 1),
[EventType] [int] NOT NULL,
[MemberID] [int] NOT NULL,
[EventDescription] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateTimestamp] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ApplicationEvent] on [dbo].[ApplicationEvent]'
GO
ALTER TABLE [dbo].[ApplicationEvent] ADD CONSTRAINT [PK_ApplicationEvent] PRIMARY KEY CLUSTERED  ([ApplicationEventID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[Provider]'
GO
CREATE TABLE [Distribution].[Provider]
(
[ProviderID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_Provider] on [Distribution].[Provider]'
GO
ALTER TABLE [Distribution].[Provider] ADD CONSTRAINT [PK_Distribution_Provider] PRIMARY KEY CLUSTERED  ([ProviderID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BusinessUnitRelationship]'
GO
CREATE TABLE [dbo].[BusinessUnitRelationship]
(
[BusinessUnitID] [int] NOT NULL,
[ParentID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BusinessUnitRelationship] on [dbo].[BusinessUnitRelationship]'
GO
ALTER TABLE [dbo].[BusinessUnitRelationship] ADD CONSTRAINT [PK_BusinessUnitRelationship] PRIMARY KEY CLUSTERED  ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_BusinessUnitRelationship_ParentID] on [dbo].[BusinessUnitRelationship]'
GO
CREATE NONCLUSTERED INDEX [IX_BusinessUnitRelationship_ParentID] ON [dbo].[BusinessUnitRelationship] ([ParentID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ThirdParties]'
GO
CREATE TABLE [dbo].[ThirdParties]
(
[ThirdPartyID] [tinyint] NOT NULL,
[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefaultFooter] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImageName] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ThirdParties] on [dbo].[ThirdParties]'
GO
ALTER TABLE [dbo].[ThirdParties] ADD CONSTRAINT [PK_ThirdParties] PRIMARY KEY NONCLUSTERED  ([ThirdPartyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Purchasing].[ListItem]'
GO
CREATE TABLE [Purchasing].[ListItem]
(
[ListItemID] [int] NOT NULL IDENTITY(1, 1),
[ListID] [int] NOT NULL,
[ModelYear] [smallint] NOT NULL,
[ModelFamilyID] [smallint] NOT NULL,
[SegmentID] [tinyint] NOT NULL,
[ModelConfigurationID] [int] NULL,
[Quantity] [tinyint] NOT NULL,
[Notes] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InsertDate] [datetime] NOT NULL,
[InsertUser] [int] NOT NULL,
[UpdateDate] [datetime] NULL,
[UpdateUser] [int] NULL,
[Version] [timestamp] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Purchasing_ListItem__ListItemID] on [Purchasing].[ListItem]'
GO
ALTER TABLE [Purchasing].[ListItem] ADD CONSTRAINT [PK_Purchasing_ListItem__ListItemID] PRIMARY KEY CLUSTERED  ([ListItemID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UK_Purchasing_ListItem__ListModelYearModelFamilyIDSegmentIDModelConfigurationID] on [Purchasing].[ListItem]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_Purchasing_ListItem__ListModelYearModelFamilyIDSegmentIDModelConfigurationID] ON [Purchasing].[ListItem] ([ListID], [ModelYear], [ModelFamilyID], [SegmentID], [ModelConfigurationID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Purchasing].[List]'
GO
CREATE TABLE [Purchasing].[List]
(
[ListID] [int] NOT NULL IDENTITY(1, 1),
[OwnerID] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[InsertUser] [int] NOT NULL,
[UpdateDate] [datetime] NULL,
[UpdateUser] [int] NULL,
[Version] [timestamp] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Purchasing_List__ListID] on [Purchasing].[List]'
GO
ALTER TABLE [Purchasing].[List] ADD CONSTRAINT [PK_Purchasing_List__ListID] PRIMARY KEY CLUSTERED  ([ListID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[AccountPreferences]'
GO
CREATE TABLE [Distribution].[AccountPreferences]
(
[DealerID] [int] NOT NULL,
[ProviderID] [int] NOT NULL,
[MessageTypeFlag] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inventory_Advertising]'
GO
CREATE TABLE [dbo].[Inventory_Advertising]
(
[InventoryID] [int] NOT NULL,
[Description] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdPrice] [decimal] (9, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Inventory_Advertising] on [dbo].[Inventory_Advertising]'
GO
ALTER TABLE [dbo].[Inventory_Advertising] ADD CONSTRAINT [PK_Inventory_Advertising] PRIMARY KEY CLUSTERED  ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[Report]'
GO
CREATE TABLE [Carfax].[Report]
(
[RequestID] [int] NOT NULL,
[Report] [xml] NOT NULL,
[ExpirationDate] [datetime] NOT NULL,
[OwnerCount] [tinyint] NULL,
[HasProblem] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_Report] on [Carfax].[Report]'
GO
ALTER TABLE [Carfax].[Report] ADD CONSTRAINT [PK_Carfax_Report] PRIMARY KEY CLUSTERED  ([RequestID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[Request]'
GO
CREATE TABLE [Carfax].[Request]
(
[RequestID] [int] NOT NULL IDENTITY(1, 1),
[VehicleID] [int] NOT NULL,
[AccountID] [int] NOT NULL,
[TrackingCode] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RequestType] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportType] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WindowSticker] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayInHotList] [bit] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_Request] on [Carfax].[Request]'
GO
ALTER TABLE [Carfax].[Request] ADD CONSTRAINT [PK_Carfax_Request] PRIMARY KEY CLUSTERED  ([RequestID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[Vehicle]'
GO
CREATE TABLE [Carfax].[Vehicle]
(
[VehicleID] [int] NOT NULL IDENTITY(1, 1),
[VIN] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsHotListEligible] [bit] NOT NULL,
[RequestID] [int] NULL,
[InsertDate] [datetime] NOT NULL,
[InsertUser] [int] NOT NULL,
[UpdateDate] [datetime] NULL,
[UpdateUser] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_Vehicle] on [Carfax].[Vehicle]'
GO
ALTER TABLE [Carfax].[Vehicle] ADD CONSTRAINT [PK_Carfax_Vehicle] PRIMARY KEY CLUSTERED  ([VehicleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UK_Carfax_Vehicle__VIN] on [Carfax].[Vehicle]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_Carfax_Vehicle__VIN] ON [Carfax].[Vehicle] ([VIN])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[Vehicle_Dealer]'
GO
CREATE TABLE [Carfax].[Vehicle_Dealer]
(
[VehicleID] [int] NOT NULL,
[DealerID] [int] NOT NULL,
[RequestID] [int] NOT NULL,
[IsHotListEnabled] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_Vehicle_Dealer] on [Carfax].[Vehicle_Dealer]'
GO
ALTER TABLE [Carfax].[Vehicle_Dealer] ADD CONSTRAINT [PK_Carfax_Vehicle_Dealer] PRIMARY KEY CLUSTERED  ([VehicleID], [DealerID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FirstLookRegionToZip]'
GO
CREATE TABLE [dbo].[FirstLookRegionToZip]
(
[FirstLookRegionID] [int] NOT NULL,
[ZipCode] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_FirstLookRegionToZip] on [dbo].[FirstLookRegionToZip]'
GO
ALTER TABLE [dbo].[FirstLookRegionToZip] ADD CONSTRAINT [PK_FirstLookRegionToZip] PRIMARY KEY CLUSTERED  ([FirstLookRegionID], [ZipCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FirstLookRegion]'
GO
CREATE TABLE [dbo].[FirstLookRegion]
(
[FirstLookRegionID] [int] NOT NULL IDENTITY(1, 1),
[FirstLookRegionName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_FirstLookRegion] on [dbo].[FirstLookRegion]'
GO
ALTER TABLE [dbo].[FirstLookRegion] ADD CONSTRAINT [PK_FirstLookRegion] PRIMARY KEY NONCLUSTERED  ([FirstLookRegionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[MarketToZip]'
GO
CREATE TABLE [dbo].[MarketToZip]
(
[ZipCode] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BusinessUnitId] [int] NOT NULL,
[Ring] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MarketToZip] on [dbo].[MarketToZip]'
GO
ALTER TABLE [dbo].[MarketToZip] ADD CONSTRAINT [PK_MarketToZip] PRIMARY KEY NONCLUSTERED  ([BusinessUnitId], [Ring], [ZipCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

PRINT N'Creating [Distribution].[ProviderMessageType]'
GO
CREATE TABLE [Distribution].[ProviderMessageType]
(
[ProviderID] [int] NOT NULL,
[MessageTypeFlag] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_ProviderMessageType] on [Distribution].[ProviderMessageType]'
GO
ALTER TABLE [Distribution].[ProviderMessageType] ADD CONSTRAINT [PK_Distribution_ProviderMessageType] PRIMARY KEY CLUSTERED  ([ProviderID], [MessageTypeFlag])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BusinessUnitPhotos]'
GO
CREATE TABLE [dbo].[BusinessUnitPhotos]
(
[BusinessUnitID] [int] NOT NULL,
[PhotoID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BusinessUnitPhotos] on [dbo].[BusinessUnitPhotos]'
GO
ALTER TABLE [dbo].[BusinessUnitPhotos] ADD CONSTRAINT [PK_BusinessUnitPhotos] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [PhotoID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[AutoTrader_Error]'
GO
CREATE TABLE [Distribution].[AutoTrader_Error]
(
[ErrorID] [int] NOT NULL,
[ErrorTypeID] [int] NOT NULL,
[ErrorMessage] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_AutoTraderError] on [Distribution].[AutoTrader_Error]'
GO
ALTER TABLE [Distribution].[AutoTrader_Error] ADD CONSTRAINT [PK_Distribution_AutoTraderError] PRIMARY KEY CLUSTERED  ([ErrorID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[AutoUplink_Error]'
GO
CREATE TABLE [Distribution].[AutoUplink_Error]
(
[ErrorID] [int] NOT NULL,
[ErrorTypeID] [int] NOT NULL,
[ErrorCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_AutoUplink_Error] on [Distribution].[AutoUplink_Error]'
GO
ALTER TABLE [Distribution].[AutoUplink_Error] ADD CONSTRAINT [PK_Distribution_AutoUplink_Error] PRIMARY KEY CLUSTERED  ([ErrorID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[ContentText]'
GO
CREATE TABLE [Distribution].[ContentText]
(
[ContentID] [int] NOT NULL,
[Text] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_ContentText] on [Distribution].[ContentText]'
GO
ALTER TABLE [Distribution].[ContentText] ADD CONSTRAINT [PK_Distribution_ContentText] PRIMARY KEY CLUSTERED  ([ContentID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[Content]'
GO
CREATE TABLE [Distribution].[Content]
(
[ContentID] [int] NOT NULL IDENTITY(1, 1),
[ContentTypeID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_Content] on [Distribution].[Content]'
GO
ALTER TABLE [Distribution].[Content] ADD CONSTRAINT [PK_Distribution_Content] PRIMARY KEY CLUSTERED  ([ContentID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[Advertisement_Content]'
GO
CREATE TABLE [Distribution].[Advertisement_Content]
(
[MessageID] [int] NOT NULL,
[ContentID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_Advertisement_Content] on [Distribution].[Advertisement_Content]'
GO
ALTER TABLE [Distribution].[Advertisement_Content] ADD CONSTRAINT [PK_Distribution_Advertisement_Content] PRIMARY KEY CLUSTERED  ([MessageID], [ContentID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[Exception]'
GO
CREATE TABLE [Distribution].[Exception]
(
[ExceptionID] [int] NOT NULL IDENTITY(1, 1),
[ExceptionTime] [datetime] NOT NULL,
[MachineName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Message] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExceptionType] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Details] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PublicationID] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_Exception] on [Distribution].[Exception]'
GO
ALTER TABLE [Distribution].[Exception] ADD CONSTRAINT [PK_Distribution_Exception] PRIMARY KEY CLUSTERED  ([ExceptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BookoutProcessorRunLog]'
GO
CREATE TABLE [dbo].[BookoutProcessorRunLog]
(
[BookoutProcessorRunId] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitId] [int] NOT NULL,
[ThirdPartyId] [tinyint] NOT NULL,
[BookoutProcessorModeId] [tinyint] NOT NULL,
[BookoutProcessorStatusId] [tinyint] NOT NULL,
[LoadTime] [smalldatetime] NULL,
[StartTime] [smalldatetime] NULL,
[EndTime] [smalldatetime] NULL,
[ServerName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuccessfulBookouts] [int] NULL,
[FailedBookouts] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BookoutProcessorRun] on [dbo].[BookoutProcessorRunLog]'
GO
ALTER TABLE [dbo].[BookoutProcessorRunLog] ADD CONSTRAINT [PK_BookoutProcessorRun] PRIMARY KEY CLUSTERED  ([BookoutProcessorRunId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[MemberInsightTypePreference]'
GO
CREATE TABLE [dbo].[MemberInsightTypePreference]
(
[MemberInsightTypePreference] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[MemberID] [int] NOT NULL,
[InsightTypeID] [int] NOT NULL,
[Active] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MemberInsightType] on [dbo].[MemberInsightTypePreference]'
GO
ALTER TABLE [dbo].[MemberInsightTypePreference] ADD CONSTRAINT [PK_MemberInsightType] PRIMARY KEY CLUSTERED  ([MemberInsightTypePreference])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InsightType]'
GO
CREATE TABLE [dbo].[InsightType]
(
[InsightTypeID] [int] NOT NULL,
[InsightCategoryID] [int] NOT NULL,
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InsightType] on [dbo].[InsightType]'
GO
ALTER TABLE [dbo].[InsightType] ADD CONSTRAINT [PK_InsightType] PRIMARY KEY CLUSTERED  ([InsightTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[Message]'
GO
CREATE TABLE [Distribution].[Message]
(
[MessageID] [int] NOT NULL IDENTITY(1, 1),
[DealerID] [int] NOT NULL,
[VehicleEntityID] [int] NOT NULL,
[VehicleEntityTypeID] [int] NOT NULL,
[InsertUserID] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_Message] on [Distribution].[Message]'
GO
ALTER TABLE [Distribution].[Message] ADD CONSTRAINT [PK_Distribution_Message] PRIMARY KEY CLUSTERED  ([MessageID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[Price]'
GO
CREATE TABLE [Distribution].[Price]
(
[MessageID] [int] NOT NULL,
[PriceTypeID] [int] NOT NULL,
[Value] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_Price] on [Distribution].[Price]'
GO
ALTER TABLE [Distribution].[Price] ADD CONSTRAINT [PK_Distribution_Price] PRIMARY KEY CLUSTERED  ([MessageID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inventory_Tracking]'
GO
CREATE TABLE [dbo].[Inventory_Tracking]
(
[InventoryID] [int] NOT NULL,
[Source] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AuditID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Inventory_Source] on [dbo].[Inventory_Tracking]'
GO
ALTER TABLE [dbo].[Inventory_Tracking] ADD CONSTRAINT [PK_Inventory_Source] PRIMARY KEY CLUSTERED  ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_VehicleLight]'
GO
CREATE TABLE [dbo].[lu_VehicleLight]
(
[InventoryVehicleLightID] [tinyint] NOT NULL,
[InventoryVehicleLightDESC] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_VehicleLight] on [dbo].[lu_VehicleLight]'
GO
ALTER TABLE [dbo].[lu_VehicleLight] ADD CONSTRAINT [PK_lu_VehicleLight] PRIMARY KEY NONCLUSTERED  ([InventoryVehicleLightID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CIAGroupingItems]'
GO
CREATE TABLE [dbo].[CIAGroupingItems]
(
[CIAGroupingItemID] [int] NOT NULL IDENTITY(1, 1),
[CIABodyTypeDetailID] [int] NOT NULL,
[GroupingDescriptionID] [int] NOT NULL,
[CIACategoryID] [tinyint] NOT NULL,
[MarketShare] [decimal] (4, 4) NULL,
[Valuation] [decimal] (10, 2) NULL,
[MarketPenetration] [decimal] (10, 4) NULL,
[Notes] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_CIAGroupingItems] on [dbo].[CIAGroupingItems]'
GO
ALTER TABLE [dbo].[CIAGroupingItems] ADD CONSTRAINT [PK_CIAGroupingItems] PRIMARY KEY CLUSTERED  ([CIAGroupingItemID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_CIAGroupingItems_CIAGBodyTypeDetailIdCIAGroupingItemIdGroupingDescriptionId] on [dbo].[CIAGroupingItems]'
GO
CREATE NONCLUSTERED INDEX [IX_CIAGroupingItems_CIAGBodyTypeDetailIdCIAGroupingItemIdGroupingDescriptionId] ON [dbo].[CIAGroupingItems] ([CIABodyTypeDetailID], [CIAGroupingItemID], [GroupingDescriptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CIABodyTypeDetails]'
GO
CREATE TABLE [dbo].[CIABodyTypeDetails]
(
[CIABodyTypeDetailID] [int] NOT NULL IDENTITY(1, 1),
[SegmentID] [int] NOT NULL,
[CIASummaryID] [int] NOT NULL,
[NonCoreTargetUnits] [int] NOT NULL,
[DisplaySortOrder] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_CIABodyTypeDetails] on [dbo].[CIABodyTypeDetails]'
GO
ALTER TABLE [dbo].[CIABodyTypeDetails] ADD CONSTRAINT [PK_CIABodyTypeDetails] PRIMARY KEY CLUSTERED  ([CIABodyTypeDetailID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_CIABodyTypeDetails_CIASummaryIdCIABodyTypeDetailID] on [dbo].[CIABodyTypeDetails]'
GO
CREATE NONCLUSTERED INDEX [IX_CIABodyTypeDetails_CIASummaryIdCIABodyTypeDetailID] ON [dbo].[CIABodyTypeDetails] ([CIASummaryID], [CIABodyTypeDetailID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_CIABodyTypeDetails__CIASummaryID] on [dbo].[CIABodyTypeDetails]'
GO
CREATE NONCLUSTERED INDEX [IX_CIABodyTypeDetails__CIASummaryID] ON [dbo].[CIABodyTypeDetails] ([CIASummaryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CIASummary]'
GO
CREATE TABLE [dbo].[CIASummary]
(
[CIASummaryID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[CIASummaryStatusID] [tinyint] NOT NULL,
[DateCreated] [smalldatetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_CIASummary] on [dbo].[CIASummary]'
GO
ALTER TABLE [dbo].[CIASummary] ADD CONSTRAINT [PK_CIASummary] PRIMARY KEY CLUSTERED  ([CIASummaryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_CIASummary_BusinessUnitID] on [dbo].[CIASummary]'
GO
CREATE NONCLUSTERED INDEX [IX_CIASummary_BusinessUnitID] ON [dbo].[CIASummary] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[Publication]'
GO
CREATE TABLE [Distribution].[Publication]
(
[PublicationID] [int] NOT NULL IDENTITY(1, 1),
[MessageID] [int] NOT NULL,
[PublicationStatusID] [int] NOT NULL,
[InsertUserID] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUserID] [int] NULL,
[UpdateDate] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_Publication] on [Distribution].[Publication]'
GO
ALTER TABLE [Distribution].[Publication] ADD CONSTRAINT [PK_Distribution_Publication] PRIMARY KEY CLUSTERED  ([PublicationID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_DealerUpgrade]'
GO
CREATE TABLE [dbo].[lu_DealerUpgrade]
(
[DealerUpgradeCD] [tinyint] NOT NULL,
[DealerUpgradeDESC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_DealerUpgrade] on [dbo].[lu_DealerUpgrade]'
GO
ALTER TABLE [dbo].[lu_DealerUpgrade] ADD CONSTRAINT [PK_lu_DealerUpgrade] PRIMARY KEY NONCLUSTERED  ([DealerUpgradeCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DMSExport_ThirdPartyEntity]'
GO
CREATE TABLE [dbo].[DMSExport_ThirdPartyEntity]
(
[DMSExportID] [int] NOT NULL,
[Description] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DMSExport_ThirdPartyEntity] on [dbo].[DMSExport_ThirdPartyEntity]'
GO
ALTER TABLE [dbo].[DMSExport_ThirdPartyEntity] ADD CONSTRAINT [PK_DMSExport_ThirdPartyEntity] PRIMARY KEY CLUSTERED  ([DMSExportID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DMSExportBusinessUnitPreference]'
GO
CREATE TABLE [dbo].[DMSExportBusinessUnitPreference]
(
[DMSExportBusinessUnitPreferenceID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[DMSExportID] [int] NOT NULL,
[isActive] [tinyint] NULL,
[ClientSystemID] [varchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DMSIPAddress] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DMSDialUpPhone1] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DMSDialUpPhone2] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DMSLogin] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DMSPassword] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountLogon] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FinanceLogon] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StoreNumber] [int] NOT NULL,
[BranchNumber] [int] NULL,
[DMSExportFrequencyID] [tinyint] NOT NULL,
[DMSExportDailyHourOfDay] [int] NULL,
[DealerName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DealerPhoneOffice] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DealerPhoneCell] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DealerEmail] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DMSExportInternetPriceMappingID] [int] NULL,
[DMSExportLotPriceMappingID] [int] NULL,
[AreaNumber] [int] NULL,
[ReynoldsDealerNo] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DMSExportBusinessUnitPreference] on [dbo].[DMSExportBusinessUnitPreference]'
GO
ALTER TABLE [dbo].[DMSExportBusinessUnitPreference] ADD CONSTRAINT [PK_DMSExportBusinessUnitPreference] PRIMARY KEY CLUSTERED  ([DMSExportBusinessUnitPreferenceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[RepriceExportStatus]'
GO
CREATE TABLE [dbo].[RepriceExportStatus]
(
[RepriceExportStatusID] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_RepriceExportStatus] on [dbo].[RepriceExportStatus]'
GO
ALTER TABLE [dbo].[RepriceExportStatus] ADD CONSTRAINT [PK_RepriceExportStatus] PRIMARY KEY CLUSTERED  ([RepriceExportStatusID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PhotoProviderDealership]'
GO
CREATE TABLE [dbo].[PhotoProviderDealership]
(
[PhotoProviderID] [int] NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[PhotoProvidersDealershipCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PhotoStorageCode_Override] [tinyint] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PhotoProviderDealership] on [dbo].[PhotoProviderDealership]'
GO
ALTER TABLE [dbo].[PhotoProviderDealership] ADD CONSTRAINT [PK_PhotoProviderDealership] PRIMARY KEY CLUSTERED  ([PhotoProviderID], [BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[Submission]'
GO
CREATE TABLE [Distribution].[Submission]
(
[SubmissionID] [int] NOT NULL IDENTITY(1, 1),
[PublicationID] [int] NOT NULL,
[ProviderID] [int] NOT NULL,
[SubmissionStatusID] [int] NOT NULL,
[PushedOnQueue] [datetime] NOT NULL,
[PoppedOffQueue] [datetime] NULL,
[SubmissionSent] [datetime] NULL,
[ResponseReceived] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_Submission] on [Distribution].[Submission]'
GO
ALTER TABLE [Distribution].[Submission] ADD CONSTRAINT [PK_Distribution_Submission] PRIMARY KEY CLUSTERED  ([SubmissionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GDLight]'
GO
CREATE TABLE [dbo].[GDLight]
(
[GroupingDescriptionLightId] [int] NOT NULL IDENTITY(1, 1),
[GroupingDescriptionId] [int] NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[InventoryVehicleLightID] [tinyint] NOT NULL,
[GridCell] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_GDLight] on [dbo].[GDLight]'
GO
ALTER TABLE [dbo].[GDLight] ADD CONSTRAINT [PK_GDLight] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [GroupingDescriptionId], [InventoryVehicleLightID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Certified].[CertifiedProgram_Make]'
GO
CREATE TABLE [Certified].[CertifiedProgram_Make]
(
[MakeID] [int] NOT NULL,
[CertifiedProgramID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Certified_CertifiedProgram_Make] on [Certified].[CertifiedProgram_Make]'
GO
ALTER TABLE [Certified].[CertifiedProgram_Make] ADD CONSTRAINT [PK_Certified_CertifiedProgram_Make] PRIMARY KEY CLUSTERED  ([MakeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Certified].[CertifiedProgram]'
GO
CREATE TABLE [Certified].[CertifiedProgram]
(
[CertifiedProgramID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Text] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Active] [bit] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUser] [int] NULL,
[UpdateDate] [datetime] NULL,
[Icon] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Certified_CertifiedProgram] on [Certified].[CertifiedProgram]'
GO
ALTER TABLE [Certified].[CertifiedProgram] ADD CONSTRAINT [PK_Certified_CertifiedProgram] PRIMARY KEY CLUSTERED  ([CertifiedProgramID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inventory_Insight]'
GO
CREATE TABLE [dbo].[Inventory_Insight]
(
[InventoryID] [int] NOT NULL,
[MileageInsightID] [tinyint] NULL,
[AgeInsightID] [tinyint] NULL,
[MileagePerYearInsightID] [tinyint] NULL,
[WeakPerformingVehicleInsightID] [tinyint] NULL,
[SalesHistoryInsightID] [tinyint] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Inventory_Insight] on [dbo].[Inventory_Insight]'
GO
ALTER TABLE [dbo].[Inventory_Insight] ADD CONSTRAINT [PK_Inventory_Insight] PRIMARY KEY CLUSTERED  ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryBookouts]'
GO
CREATE TABLE [dbo].[InventoryBookouts]
(
[BookoutID] [int] NOT NULL,
[InventoryID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryBookouts] on [dbo].[InventoryBookouts]'
GO
ALTER TABLE [dbo].[InventoryBookouts] ADD CONSTRAINT [PK_InventoryBookouts] PRIMARY KEY CLUSTERED  ([BookoutID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_InventoryBookouts_InventoryID] on [dbo].[InventoryBookouts]'
GO
CREATE NONCLUSTERED INDEX [IX_InventoryBookouts_InventoryID] ON [dbo].[InventoryBookouts] ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CIAInventoryOverstocking]'
GO
CREATE TABLE [dbo].[CIAInventoryOverstocking]
(
[CIAInventoryOverstockingID] [int] NOT NULL IDENTITY(1, 1),
[CIASummaryID] [int] NOT NULL,
[InventoryID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_CIAInventoryOverstocking] on [dbo].[CIAInventoryOverstocking]'
GO
ALTER TABLE [dbo].[CIAInventoryOverstocking] ADD CONSTRAINT [PK_CIAInventoryOverstocking] PRIMARY KEY CLUSTERED  ([CIAInventoryOverstockingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tbl_VehicleSaleReversals]'
GO
CREATE TABLE [dbo].[tbl_VehicleSaleReversals]
(
[InventoryID] [int] NOT NULL,
[SalesReferenceNumber] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VehicleMileage] [int] NULL,
[DealDate] [smalldatetime] NOT NULL,
[BackEndGross] [decimal] (8, 2) NULL,
[NetFinancialInsuranceIncome] [decimal] (18, 0) NULL,
[OriginalFrontEndGross] [decimal] (8, 2) NULL,
[FrontEndGross] [decimal] (18, 0) NULL,
[SalePrice] [decimal] (9, 2) NULL,
[SaleDescription] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FinanceInsuranceDealNumber] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastPolledDate] [smalldatetime] NOT NULL,
[AnalyticalEngineStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AnalyticalEngineStatus_CD] [int] NULL,
[LeaseFlag] [tinyint] NULL,
[Pack] [decimal] (8, 2) NULL,
[AfterMarketGross] [decimal] (8, 2) NULL,
[TotalGross] [decimal] (8, 2) NULL,
[VehiclePromotionAmount] [decimal] (8, 2) NULL,
[DealStatusCD] [tinyint] NULL,
[PackAdjust] [decimal] (10, 2) NULL,
[AUDIT_ID] [int] NOT NULL,
[valuation] [float] NULL,
[CustomerZip] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[SubmissionErrorType]'
GO
CREATE TABLE [Distribution].[SubmissionErrorType]
(
[ErrorTypeID] [int] NOT NULL IDENTITY(0, 1),
[Description] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ConcernsUser] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_SubmissionErrorType] on [Distribution].[SubmissionErrorType]'
GO
ALTER TABLE [Distribution].[SubmissionErrorType] ADD CONSTRAINT [PK_Distribution_SubmissionErrorType] PRIMARY KEY CLUSTERED  ([ErrorTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[SubmissionError]'
GO
CREATE TABLE [Distribution].[SubmissionError]
(
[ErrorID] [int] NOT NULL IDENTITY(1, 1),
[ErrorTypeID] [int] NOT NULL,
[SubmissionID] [int] NOT NULL,
[OccuredOn] [datetime] NOT NULL,
[MessageTypeFlag] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_Error] on [Distribution].[SubmissionError]'
GO
ALTER TABLE [Distribution].[SubmissionError] ADD CONSTRAINT [PK_Distribution_Error] PRIMARY KEY CLUSTERED  ([ErrorID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PhotoTypes]'
GO
CREATE TABLE [dbo].[PhotoTypes]
(
[PhotoTypeID] [tinyint] NOT NULL,
[Description] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PhotoTypes] on [dbo].[PhotoTypes]'
GO
ALTER TABLE [dbo].[PhotoTypes] ADD CONSTRAINT [PK_PhotoTypes] PRIMARY KEY CLUSTERED  ([PhotoTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Certified].[CertifiedProgramPhoto]'
GO
CREATE TABLE [Certified].[CertifiedProgramPhoto]
(
[CertifiedProgramID] [int] NOT NULL,
[PhotoID] [int] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Certified_CertifiedProgramPhoto] on [Certified].[CertifiedProgramPhoto]'
GO
ALTER TABLE [Certified].[CertifiedProgramPhoto] ADD CONSTRAINT [PK_Certified_CertifiedProgramPhoto] PRIMARY KEY CLUSTERED  ([CertifiedProgramID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerFacts]'
GO
CREATE TABLE [dbo].[DealerFacts]
(
[DealerFactID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[DateCreated] [datetime] NOT NULL,
[LastPolledDate] [datetime] NULL,
[LastDMSReferenceDateNew] [datetime] NULL,
[LastDMSReferenceDateUsed] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DealerFacts] on [dbo].[DealerFacts]'
GO
ALTER TABLE [dbo].[DealerFacts] ADD CONSTRAINT [PK_DealerFacts] PRIMARY KEY CLUSTERED  ([DealerFactID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[SubmissionSuccess]'
GO
CREATE TABLE [Distribution].[SubmissionSuccess]
(
[SubmissionID] [int] NOT NULL,
[MessageTypeFlag] [int] NOT NULL,
[OccuredOn] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ThirdPartyCategories]'
GO
CREATE TABLE [dbo].[ThirdPartyCategories]
(
[ThirdPartyCategoryID] [int] NOT NULL IDENTITY(1, 1),
[ThirdPartyID] [tinyint] NOT NULL,
[Category] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ThirdPartyCategories] on [dbo].[ThirdPartyCategories]'
GO
ALTER TABLE [dbo].[ThirdPartyCategories] ADD CONSTRAINT [PK_ThirdPartyCategories] PRIMARY KEY CLUSTERED  ([ThirdPartyCategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CIABuckets]'
GO
CREATE TABLE [dbo].[CIABuckets]
(
[CIABucketID] [int] NOT NULL IDENTITY(1, 1),
[CIABucketGroupID] [tinyint] NOT NULL,
[Rank] [tinyint] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_CIABuckets] on [dbo].[CIABuckets]'
GO
ALTER TABLE [dbo].[CIABuckets] ADD CONSTRAINT [PK_CIABuckets] PRIMARY KEY CLUSTERED  ([CIABucketID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[Vehicle]'
GO
CREATE TABLE [Distribution].[Vehicle]
(
[VehicleEntityID] [int] NOT NULL,
[VehicleEntityTypeID] [int] NOT NULL,
[InsertUserID] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_Vehicle] on [Distribution].[Vehicle]'
GO
ALTER TABLE [Distribution].[Vehicle] ADD CONSTRAINT [PK_Distribution_Vehicle] PRIMARY KEY CLUSTERED  ([VehicleEntityID], [VehicleEntityTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Certified].[CertifiedProgramBenefit]'
GO
CREATE TABLE [Certified].[CertifiedProgramBenefit]
(
[CertifiedProgramBenefitID] [int] NOT NULL IDENTITY(1, 1),
[CertifiedProgramID] [int] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Text] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Rank] [smallint] NOT NULL,
[IsProgramHighlight] [bit] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUser] [int] NULL,
[UpdateDate] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Certified_CertifiedProgramBenefit] on [Certified].[CertifiedProgramBenefit]'
GO
ALTER TABLE [Certified].[CertifiedProgramBenefit] ADD CONSTRAINT [PK_Certified_CertifiedProgramBenefit] PRIMARY KEY CLUSTERED  ([CertifiedProgramBenefitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Certified].[OwnerCertifiedProgramBenefit]'
GO
CREATE TABLE [Certified].[OwnerCertifiedProgramBenefit]
(
[OwnerCertifiedProgramBenefitID] [int] NOT NULL IDENTITY(1, 1),
[OwnerCertifiedProgramID] [int] NOT NULL,
[CertifiedProgramBenefitID] [int] NOT NULL,
[Rank] [smallint] NOT NULL,
[IsProgramHighlight] [bit] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUser] [int] NULL,
[UpdateDate] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Certified_OwnerCertifiedProgramBenefit] on [Certified].[OwnerCertifiedProgramBenefit]'
GO
ALTER TABLE [Certified].[OwnerCertifiedProgramBenefit] ADD CONSTRAINT [PK_Certified_OwnerCertifiedProgramBenefit] PRIMARY KEY CLUSTERED  ([OwnerCertifiedProgramBenefitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[VehicleInformation]'
GO
CREATE TABLE [Distribution].[VehicleInformation]
(
[MessageID] [int] NOT NULL,
[Mileage] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_VehicleInformation] on [Distribution].[VehicleInformation]'
GO
ALTER TABLE [Distribution].[VehicleInformation] ADD CONSTRAINT [PK_Distribution_VehicleInformation] PRIMARY KEY CLUSTERED  ([MessageID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Certified].[OwnerCertifiedProgram]'
GO
CREATE TABLE [Certified].[OwnerCertifiedProgram]
(
[OwnerCertifiedProgramID] [int] NOT NULL IDENTITY(1, 1),
[OwnerId] [int] NOT NULL,
[CertifiedProgramID] [int] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Certified_OwnerCertifiedProgram] on [Certified].[OwnerCertifiedProgram]'
GO
ALTER TABLE [Certified].[OwnerCertifiedProgram] ADD CONSTRAINT [PK_Certified_OwnerCertifiedProgram] PRIMARY KEY CLUSTERED  ([OwnerCertifiedProgramID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[ConsumerHighlightPreference]'
GO
CREATE TABLE [Marketing].[ConsumerHighlightPreference]
(
[OwnerId] [int] NOT NULL,
[MinimumJDPowerCircleRating] [float] NOT NULL,
[InsertUserId] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUserId] [int] NULL,
[UpdateDate] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Marketing_ConsumerHighlightPreference] on [Marketing].[ConsumerHighlightPreference]'
GO
ALTER TABLE [Marketing].[ConsumerHighlightPreference] ADD CONSTRAINT [PK_Marketing_ConsumerHighlightPreference] PRIMARY KEY CLUSTERED  ([OwnerId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SubscriptionFrequencyDetail]'
GO
CREATE TABLE [dbo].[SubscriptionFrequencyDetail]
(
[SubscriptionFrequencyDetailID] [tinyint] NOT NULL,
[SubscriptionFrequencyID] [tinyint] NOT NULL,
[Rank] [tinyint] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SubscriptionFrequencyDetail] on [dbo].[SubscriptionFrequencyDetail]'
GO
ALTER TABLE [dbo].[SubscriptionFrequencyDetail] ADD CONSTRAINT [PK_SubscriptionFrequencyDetail] PRIMARY KEY CLUSTERED  ([SubscriptionFrequencyDetailID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BuyingAlertsRunList]'
GO
CREATE TABLE [dbo].[BuyingAlertsRunList]
(
[BuyingAlertsRunListId] [int] NOT NULL IDENTITY(1, 1),
[SubscriptionId] [int] NOT NULL,
[BuyingAlertsStatusId] [tinyint] NOT NULL,
[SendDate] [smalldatetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BuyingAlertsRunList] on [dbo].[BuyingAlertsRunList]'
GO
ALTER TABLE [dbo].[BuyingAlertsRunList] ADD CONSTRAINT [PK_BuyingAlertsRunList] PRIMARY KEY NONCLUSTERED  ([BuyingAlertsRunListId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[ConsumerHighlightSection]'
GO
CREATE TABLE [Marketing].[ConsumerHighlightSection]
(
[ConsumerHighlightSectionId] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DefaultRank] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Marketing_ConsumerHighlightSection] on [Marketing].[ConsumerHighlightSection]'
GO
ALTER TABLE [Marketing].[ConsumerHighlightSection] ADD CONSTRAINT [PK_Marketing_ConsumerHighlightSection] PRIMARY KEY CLUSTERED  ([ConsumerHighlightSectionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[ConsumerHighlightSectionPreference]'
GO
CREATE TABLE [Marketing].[ConsumerHighlightSectionPreference]
(
[OwnerId] [int] NOT NULL,
[ConsumerHighlightSectionId] [int] NOT NULL,
[Rank] [int] NOT NULL,
[InsertUserId] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUserId] [int] NULL,
[UpdateDate] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Marketing_ConsumerHighlightSectionPreference] on [Marketing].[ConsumerHighlightSectionPreference]'
GO
ALTER TABLE [Marketing].[ConsumerHighlightSectionPreference] ADD CONSTRAINT [PK_Marketing_ConsumerHighlightSectionPreference] PRIMARY KEY CLUSTERED  ([OwnerId], [ConsumerHighlightSectionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[JobTitle]'
GO
CREATE TABLE [dbo].[JobTitle]
(
[JobTitleID] [tinyint] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_JobTitle] on [dbo].[JobTitle]'
GO
ALTER TABLE [dbo].[JobTitle] ADD CONSTRAINT [PK_JobTitle] PRIMARY KEY CLUSTERED  ([JobTitleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerFranchise]'
GO
CREATE TABLE [dbo].[DealerFranchise]
(
[DealerFranchiseID] [int] NOT NULL IDENTITY(10000, 1),
[BusinessUnitID] [int] NOT NULL,
[FranchiseID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DealerFranchise] on [dbo].[DealerFranchise]'
GO
ALTER TABLE [dbo].[DealerFranchise] ADD CONSTRAINT [PK_DealerFranchise] PRIMARY KEY NONCLUSTERED  ([DealerFranchiseID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[SnippetSourcePreference]'
GO
CREATE TABLE [Marketing].[SnippetSourcePreference]
(
[OwnerId] [int] NOT NULL,
[SnippetSourceId] [int] NOT NULL,
[Rank] [int] NOT NULL,
[IsDisplayed] [bit] NOT NULL,
[InsertUserId] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUserId] [int] NULL,
[UpdateDate] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Marketing_SnippetSourcePreference] on [Marketing].[SnippetSourcePreference]'
GO
ALTER TABLE [Marketing].[SnippetSourcePreference] ADD CONSTRAINT [PK_Marketing_SnippetSourcePreference] PRIMARY KEY CLUSTERED  ([SnippetSourceId], [OwnerId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerGridValues]'
GO
CREATE TABLE [dbo].[DealerGridValues]
(
[DealerGridValuesId] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[LightValue] [int] NOT NULL,
[TypeValue] [int] NOT NULL,
[indexKey] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DealerGridValues] on [dbo].[DealerGridValues]'
GO
ALTER TABLE [dbo].[DealerGridValues] ADD CONSTRAINT [PK_DealerGridValues] PRIMARY KEY NONCLUSTERED  ([DealerGridValuesId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_DealerGridValuesBusinessUnitIdIndexKey] on [dbo].[DealerGridValues]'
GO
CREATE NONCLUSTERED INDEX [IX_DealerGridValuesBusinessUnitIdIndexKey] ON [dbo].[DealerGridValues] ([BusinessUnitID], [indexKey]) INCLUDE ([TypeValue])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerGridThresholds]'
GO
CREATE TABLE [dbo].[DealerGridThresholds]
(
[DealerGridThresholdsId] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[firstValuationThreshold] [int] NOT NULL,
[secondValuationThreshold] [int] NOT NULL,
[thirdValuationThreshold] [int] NOT NULL,
[fourthValuationThreshold] [int] NOT NULL,
[firstDealThreshold] [int] NOT NULL,
[secondDealThreshold] [int] NOT NULL,
[thirdDealThreshold] [int] NOT NULL,
[fourthDealThreshold] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DealerGridThresholds] on [dbo].[DealerGridThresholds]'
GO
ALTER TABLE [dbo].[DealerGridThresholds] ADD CONSTRAINT [PK_DealerGridThresholds] PRIMARY KEY CLUSTERED  ([DealerGridThresholdsId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BookoutThirdPartyVehicles]'
GO
CREATE TABLE [dbo].[BookoutThirdPartyVehicles]
(
[ThirdPartyVehicleID] [int] NOT NULL,
[BookoutID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BookoutThirdPartyVehicles] on [dbo].[BookoutThirdPartyVehicles]'
GO
ALTER TABLE [dbo].[BookoutThirdPartyVehicles] ADD CONSTRAINT [PK_BookoutThirdPartyVehicles] PRIMARY KEY CLUSTERED  ([ThirdPartyVehicleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_BookoutThirdPartyVehicles__BookoutID] on [dbo].[BookoutThirdPartyVehicles]'
GO
CREATE NONCLUSTERED INDEX [IX_BookoutThirdPartyVehicles__BookoutID] ON [dbo].[BookoutThirdPartyVehicles] ([BookoutID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[MarketListingVehiclePreferenceSearchOverrides]'
GO
CREATE TABLE [Marketing].[MarketListingVehiclePreferenceSearchOverrides]
(
[MarketListingVehiclePreferenceId] [int] NOT NULL,
[VehicleId] [int] NOT NULL,
[InsertUserId] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUserId] [int] NULL,
[UpdateDate] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MarketListingVehiclePreferenceSearchOverrides] on [Marketing].[MarketListingVehiclePreferenceSearchOverrides]'
GO
ALTER TABLE [Marketing].[MarketListingVehiclePreferenceSearchOverrides] ADD CONSTRAINT [PK_MarketListingVehiclePreferenceSearchOverrides] PRIMARY KEY CLUSTERED  ([MarketListingVehiclePreferenceId], [VehicleId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[CertifiedVehicleBenefitPreference]'
GO
CREATE TABLE [Marketing].[CertifiedVehicleBenefitPreference]
(
[CertifiedVehicleBenefitPreferenceID] [int] NOT NULL IDENTITY(1, 1),
[OwnerID] [int] NOT NULL,
[VehicleEntityTypeID] [tinyint] NOT NULL,
[VehicleEntityID] [int] NOT NULL,
[IsDisplayed] [bit] NOT NULL,
[OwnerCertifiedProgramID] [int] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUser] [int] NULL,
[UpdateDate] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Marketing_CertifiedVehicleBenefitPreference] on [Marketing].[CertifiedVehicleBenefitPreference]'
GO
ALTER TABLE [Marketing].[CertifiedVehicleBenefitPreference] ADD CONSTRAINT [PK_Marketing_CertifiedVehicleBenefitPreference] PRIMARY KEY CLUSTERED  ([CertifiedVehicleBenefitPreferenceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ModelBodyStyleOverrides]'
GO
CREATE TABLE [dbo].[ModelBodyStyleOverrides]
(
[Make] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Model] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BodyType] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EnhancedModel] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ModelNameOverrides] on [dbo].[ModelBodyStyleOverrides]'
GO
ALTER TABLE [dbo].[ModelBodyStyleOverrides] ADD CONSTRAINT [PK_ModelNameOverrides] PRIMARY KEY CLUSTERED  ([Make], [Model], [BodyType])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[VehicleBookoutStateThirdPartyVehicles]'
GO
CREATE TABLE [dbo].[VehicleBookoutStateThirdPartyVehicles]
(
[ThirdPartyVehicleID] [int] NOT NULL,
[VehicleBookoutStateID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__VehicleBookoutSt__2E1E3E47] on [dbo].[VehicleBookoutStateThirdPartyVehicles]'
GO
ALTER TABLE [dbo].[VehicleBookoutStateThirdPartyVehicles] ADD CONSTRAINT [PK__VehicleBookoutSt__2E1E3E47] PRIMARY KEY CLUSTERED  ([ThirdPartyVehicleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[MarketAnalysisVehiclePreferencePriceProvider]'
GO
CREATE TABLE [Marketing].[MarketAnalysisVehiclePreferencePriceProvider]
(
[MarketAnalysisVehiclePreferenceId] [int] NOT NULL,
[MarketAnalysisPriceProviderId] [tinyint] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Marketing_MarketAnalysisVehiclePreferencePriceProvider] on [Marketing].[MarketAnalysisVehiclePreferencePriceProvider]'
GO
ALTER TABLE [Marketing].[MarketAnalysisVehiclePreferencePriceProvider] ADD CONSTRAINT [PK_Marketing_MarketAnalysisVehiclePreferencePriceProvider] PRIMARY KEY CLUSTERED  ([MarketAnalysisVehiclePreferenceId], [MarketAnalysisPriceProviderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[Account_Member]'
GO
CREATE TABLE [Carfax].[Account_Member]
(
[AccountID] [int] NOT NULL,
[MemberID] [int] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_Account_Member] on [Carfax].[Account_Member]'
GO
ALTER TABLE [Carfax].[Account_Member] ADD CONSTRAINT [PK_Carfax_Account_Member] PRIMARY KEY CLUSTERED  ([AccountID], [MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[MarketAnalysisPriceProvider]'
GO
CREATE TABLE [Marketing].[MarketAnalysisPriceProvider]
(
[PriceProviderId] [tinyint] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MarketAnalysisPriceProvider] on [Marketing].[MarketAnalysisPriceProvider]'
GO
ALTER TABLE [Marketing].[MarketAnalysisPriceProvider] ADD CONSTRAINT [PK_MarketAnalysisPriceProvider] PRIMARY KEY CLUSTERED  ([PriceProviderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[PrintAudit]'
GO
CREATE TABLE [Marketing].[PrintAudit]
(
[PrintPDF] [varbinary] (max) NULL,
[PrintXML] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OwnerID] [int] NULL,
[VehicleEntityID] [int] NULL,
[VehicleEntityTypeID] [int] NULL,
[InsertUserID] [int] NULL,
[InsertDate] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[MarketListingPreference]'
GO
CREATE TABLE [Marketing].[MarketListingPreference]
(
[MarketListingPreferenceId] [int] NOT NULL IDENTITY(1, 1),
[OwnerId] [int] NULL,
[IsDisplayed] [bit] NOT NULL,
[ShowDealerName] [bit] NOT NULL,
[ShowCertifiedIndicator] [bit] NOT NULL,
[MileageDeltaBelow] [int] NOT NULL,
[MileageDeltaAbove] [int] NOT NULL,
[PriceDeltaAbove] [int] NOT NULL,
[PriceDeltaBelow] [int] NOT NULL,
[InsertUserId] [int] NULL,
[InsertDate] [datetime] NULL,
[UpdateUserId] [int] NULL,
[UpdateDate] [datetime] NULL,
[Version] [timestamp] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MarketListingPreference] on [Marketing].[MarketListingPreference]'
GO
ALTER TABLE [Marketing].[MarketListingPreference] ADD CONSTRAINT [PK_MarketListingPreference] PRIMARY KEY CLUSTERED  ([MarketListingPreferenceId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[Account]'
GO
CREATE TABLE [Carfax].[Account]
(
[AccountID] [int] NOT NULL IDENTITY(1, 1),
[AccountStatusID] [tinyint] NOT NULL,
[AccountTypeID] [tinyint] NOT NULL,
[UserName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Password] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUser] [int] NULL,
[UpdateDate] [datetime] NULL,
[DeleteUser] [int] NULL,
[DeleteDate] [datetime] NULL,
[Version] [timestamp] NOT NULL,
[Active] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_Account] on [Carfax].[Account]'
GO
ALTER TABLE [Carfax].[Account] ADD CONSTRAINT [PK_Carfax_Account] PRIMARY KEY CLUSTERED  ([AccountID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[AccountType]'
GO
CREATE TABLE [Carfax].[AccountType]
(
[AccountTypeID] [tinyint] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_AccountType] on [Carfax].[AccountType]'
GO
ALTER TABLE [Carfax].[AccountType] ADD CONSTRAINT [PK_Carfax_AccountType] PRIMARY KEY CLUSTERED  ([AccountTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UK_Carfax_AccountType__Name] on [Carfax].[AccountType]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_Carfax_AccountType__Name] ON [Carfax].[AccountType] ([Name])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[Account_Audit]'
GO
CREATE TABLE [Distribution].[Account_Audit]
(
[AccountAuditID] [int] NOT NULL IDENTITY(1, 1),
[DealerID] [int] NOT NULL,
[ProviderID] [int] NOT NULL,
[UserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DealershipCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Active] [bit] NOT NULL,
[ChangeUserID] [int] NOT NULL,
[ChangeDate] [datetime] NOT NULL,
[ValidFrom] [datetime] NOT NULL,
[ValidUntil] [datetime] NOT NULL,
[WasInsert] [bit] NOT NULL,
[WasUpdate] [bit] NOT NULL,
[WasDelete] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_AccountAudit] on [Distribution].[Account_Audit]'
GO
ALTER TABLE [Distribution].[Account_Audit] ADD CONSTRAINT [PK_Distribution_AccountAudit] PRIMARY KEY CLUSTERED  ([AccountAuditID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[Account]'
GO
CREATE TABLE [Distribution].[Account]
(
[DealerID] [int] NOT NULL,
[ProviderID] [int] NOT NULL,
[UserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DealershipCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Active] [bit] NOT NULL,
[InsertUserID] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUserID] [int] NULL,
[UpdateDate] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_Account] on [Distribution].[Account]'
GO
ALTER TABLE [Distribution].[Account] ADD CONSTRAINT [PK_Distribution_Account] PRIMARY KEY CLUSTERED  ([DealerID], [ProviderID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[ValueAnalyzerVehiclePreference]'
GO
CREATE TABLE [Marketing].[ValueAnalyzerVehiclePreference]
(
[OwnerID] [int] NOT NULL,
[VehicleEntityTypeID] [tinyint] NOT NULL,
[VehicleEntityID] [int] NOT NULL,
[DisplayVehicleDescription] [bit] NOT NULL,
[UseLongMarketingListingLayout] [bit] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUser] [int] NULL,
[UpdateDate] [datetime] NULL,
[ShortUrl] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LongUrl] [varchar] (2083) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Marketing_ValueAnalyzerVehiclePreference] on [Marketing].[ValueAnalyzerVehiclePreference]'
GO
ALTER TABLE [Marketing].[ValueAnalyzerVehiclePreference] ADD CONSTRAINT [PK_Marketing_ValueAnalyzerVehiclePreference] PRIMARY KEY CLUSTERED  ([OwnerID], [VehicleEntityTypeID], [VehicleEntityID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DemoDealerBaseline_Inventory]'
GO
CREATE TABLE [dbo].[DemoDealerBaseline_Inventory]
(
[InventoryID] [int] NOT NULL,
[PlanReminderDate] [smalldatetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DemoDealerBaseline_Inventory] on [dbo].[DemoDealerBaseline_Inventory]'
GO
ALTER TABLE [dbo].[DemoDealerBaseline_Inventory] ADD CONSTRAINT [PK_DemoDealerBaseline_Inventory] PRIMARY KEY CLUSTERED  ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SearchCandidateOption]'
GO
CREATE TABLE [dbo].[SearchCandidateOption]
(
[SearchCandidateOptionID] [int] NOT NULL IDENTITY(1, 1),
[SearchCandidateID] [int] NOT NULL,
[ModelYear] [int] NULL,
[Expires] [datetime] NULL,
[Suppress] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SearchCandidateOption] on [dbo].[SearchCandidateOption]'
GO
ALTER TABLE [dbo].[SearchCandidateOption] ADD CONSTRAINT [PK_SearchCandidateOption] PRIMARY KEY NONCLUSTERED  ([SearchCandidateOptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SearchCandidate]'
GO
CREATE TABLE [dbo].[SearchCandidate]
(
[SearchCandidateID] [int] NOT NULL IDENTITY(1, 1),
[SearchCandidateListID] [int] NOT NULL,
[Suppress] [bit] NOT NULL,
[ModelID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SearchCandidate] on [dbo].[SearchCandidate]'
GO
ALTER TABLE [dbo].[SearchCandidate] ADD CONSTRAINT [PK_SearchCandidate] PRIMARY KEY NONCLUSTERED  ([SearchCandidateID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SearchCandidateList]'
GO
CREATE TABLE [dbo].[SearchCandidateList]
(
[SearchCandidateListID] [int] NOT NULL IDENTITY(1, 1),
[SearchCandidateListTypeID] [int] NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[MemberID] [int] NOT NULL,
[Created] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SearchCandidateList] on [dbo].[SearchCandidateList]'
GO
ALTER TABLE [dbo].[SearchCandidateList] ADD CONSTRAINT [PK_SearchCandidateList] PRIMARY KEY NONCLUSTERED  ([SearchCandidateListID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryStatusCodes]'
GO
CREATE TABLE [dbo].[InventoryStatusCodes]
(
[InventoryStatusCD] [smallint] NOT NULL IDENTITY(1, 1),
[ShortDescription] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryStatusCodes] on [dbo].[InventoryStatusCodes]'
GO
ALTER TABLE [dbo].[InventoryStatusCodes] ADD CONSTRAINT [PK_InventoryStatusCodes] PRIMARY KEY CLUSTERED  ([InventoryStatusCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BookoutStatus]'
GO
CREATE TABLE [dbo].[BookoutStatus]
(
[BookoutStatusID] [int] NOT NULL,
[Description] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsAccurate] [tinyint] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BookoutStatus] on [dbo].[BookoutStatus]'
GO
ALTER TABLE [dbo].[BookoutStatus] ADD CONSTRAINT [PK_BookoutStatus] PRIMARY KEY CLUSTERED  ([BookoutStatusID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerPreference_JDPowerSettings]'
GO
CREATE TABLE [dbo].[DealerPreference_JDPowerSettings]
(
[BusinessUnitID] [int] NOT NULL,
[PowerRegionID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DP_JDPowerSettings_BusinessUnitID] on [dbo].[DealerPreference_JDPowerSettings]'
GO
ALTER TABLE [dbo].[DealerPreference_JDPowerSettings] ADD CONSTRAINT [PK_DP_JDPowerSettings_BusinessUnitID] PRIMARY KEY CLUSTERED  ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SoftwareSystemComponentState]'
GO
CREATE TABLE [dbo].[SoftwareSystemComponentState]
(
[SoftwareSystemComponentStateID] [int] NOT NULL IDENTITY(1, 1),
[SoftwareSystemComponentID] [int] NOT NULL,
[AuthorizedMemberID] [int] NOT NULL,
[DealerGroupID] [int] NOT NULL,
[DealerID] [int] NULL,
[MemberID] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SoftwareSystemComponentState] on [dbo].[SoftwareSystemComponentState]'
GO
ALTER TABLE [dbo].[SoftwareSystemComponentState] ADD CONSTRAINT [PK_SoftwareSystemComponentState] PRIMARY KEY CLUSTERED  ([SoftwareSystemComponentStateID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tbl_UsedCarScoreCard]'
GO
CREATE TABLE [dbo].[tbl_UsedCarScoreCard]
(
[UsedCarScoreCardId] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitId] [int] NOT NULL,
[Active] [tinyint] NOT NULL,
[WeekEnding] [smalldatetime] NOT NULL,
[WeekNumber] [int] NOT NULL,
[OverallUnitSaleCurrent] [int] NOT NULL,
[OverallUnitSalePrior] [int] NOT NULL,
[OverallUnitSaleThreeMonth] [decimal] (8, 2) NOT NULL,
[OverallRetailAvgGrossProfitTrend] [decimal] (8, 2) NOT NULL,
[OverallRetailAvgGrossProfit12Week] [decimal] (8, 2) NOT NULL,
[OverallFAndIAvgGrossProfitTrend] [decimal] (8, 2) NOT NULL,
[OverallFAndIAvgGrossProfit12Week] [decimal] (8, 2) NOT NULL,
[OverallPercentSellThroughTrend] [decimal] (8, 2) NOT NULL,
[OverallPercentSellThrough12Week] [decimal] (8, 2) NOT NULL,
[OverallAvgDaysToSaleTrend] [decimal] (8, 2) NOT NULL,
[OverallAvgDaysToSale12Week] [decimal] (8, 2) NOT NULL,
[OverallAvgInventoryAgeCurrent] [decimal] (8, 2) NOT NULL,
[OverallAvgInventoryAgePrior] [decimal] (8, 2) NOT NULL,
[OverallCurrentDaysSupplyCurrent] [decimal] (8, 2) NOT NULL,
[OverallCurrentDaysSupplyPrior] [decimal] (8, 2) NOT NULL,
[Aging60PlusCurrent] [decimal] (8, 2) NOT NULL,
[Aging60PlusPrior] [decimal] (8, 2) NOT NULL,
[Aging50To59Current] [decimal] (8, 2) NOT NULL,
[Aging50To59Prior] [decimal] (8, 2) NOT NULL,
[Aging40To49Current] [decimal] (8, 2) NOT NULL,
[Aging40To49Prior] [decimal] (8, 2) NOT NULL,
[Aging30To39Current] [decimal] (8, 2) NOT NULL,
[Aging30To39Prior] [decimal] (8, 2) NOT NULL,
[PurchasedPercentWinnersTrend] [decimal] (8, 2) NOT NULL,
[PurchasedPercentWinners12Week] [decimal] (8, 2) NOT NULL,
[PurchasedRetailAvgGrossProfitTrend] [decimal] (8, 2) NOT NULL,
[PurchasedRetailAvgGrossProfit12Week] [decimal] (8, 2) NOT NULL,
[PurchasedPercentSellThroughTrend] [decimal] (8, 2) NOT NULL,
[PurchasedPercentSellThrough12Week] [decimal] (8, 2) NOT NULL,
[PurchasedAvgDaysToSaleTrend] [decimal] (8, 2) NOT NULL,
[PurchasedAvgDaysToSale12Week] [decimal] (8, 2) NOT NULL,
[PurchasedAvgInventoryAgeCurrent] [decimal] (8, 2) NOT NULL,
[PurchasedAvgInventoryAgePrior] [decimal] (8, 2) NOT NULL,
[TradeInPercentNonWinnersWholesaledTrend] [decimal] (8, 2) NOT NULL,
[TradeInPercentNonWinnersWholesaled12Week] [decimal] (8, 2) NOT NULL,
[TradeInRetailAvgGrossProfitTrend] [decimal] (8, 2) NOT NULL,
[TradeInRetailAvgGrossProfit12Week] [decimal] (8, 2) NOT NULL,
[TradeInPercentSellThroughTrend] [decimal] (8, 2) NOT NULL,
[TradeInPercentSellThrough12Week] [decimal] (8, 2) NOT NULL,
[TradeInAvgDaysToSaleTrend] [decimal] (8, 2) NOT NULL,
[TradeInAvgDaysToSale12Week] [decimal] (8, 2) NOT NULL,
[TradeInAvgInventoryAgeCurrent] [decimal] (8, 2) NOT NULL,
[TradeInAvgInventoryAgePrior] [decimal] (8, 2) NOT NULL,
[WholesaleImmediateAvgGrossProfitTrend] [decimal] (8, 2) NOT NULL,
[WholesaleImmediateAvgGrossProfit12Week] [decimal] (8, 2) NOT NULL,
[WholesaleAgedInventoryAvgGrossProfitTrend] [decimal] (8, 2) NOT NULL,
[WholesaleAgedInventoryAvgGrossProfit12Week] [decimal] (8, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tbl_UsedCarScorecard] on [dbo].[tbl_UsedCarScoreCard]'
GO
ALTER TABLE [dbo].[tbl_UsedCarScoreCard] ADD CONSTRAINT [PK_tbl_UsedCarScorecard] PRIMARY KEY CLUSTERED  ([UsedCarScoreCardId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tbl_RedistributionLog]'
GO
CREATE TABLE [dbo].[tbl_RedistributionLog]
(
[RedistributionLogId] [int] NOT NULL IDENTITY(1, 1),
[VIN] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OriginatingDealerId] [int] NOT NULL,
[DestinationDealerId] [int] NULL,
[RedistributionSourceId] [tinyint] NOT NULL,
[ReceivedDate] [smalldatetime] NULL,
[OriginatingDealerStockNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DestinationDealerStockNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MemberId] [int] NOT NULL,
[TransactionDate] [smalldatetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tbl_RedistributionLog] on [dbo].[tbl_RedistributionLog]'
GO
ALTER TABLE [dbo].[tbl_RedistributionLog] ADD CONSTRAINT [PK_tbl_RedistributionLog] PRIMARY KEY CLUSTERED  ([RedistributionLogId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tbl_NewCarScoreCard]'
GO
CREATE TABLE [dbo].[tbl_NewCarScoreCard]
(
[NewCarScoreCardId] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitId] [int] NOT NULL,
[Active] [tinyint] NOT NULL,
[WeekEnding] [smalldatetime] NOT NULL,
[WeekNumber] [int] NOT NULL,
[OverallUnitSaleCurrent] [int] NOT NULL,
[OverallUnitSalePrior] [int] NOT NULL,
[OverallUnitSaleThreeMonth] [decimal] (8, 2) NOT NULL,
[OverallRetailAvgGrossProfitTrend] [decimal] (8, 2) NOT NULL,
[OverallRetailAvgGrossProfit12Week] [decimal] (8, 2) NOT NULL,
[OverallFAndIAvgGrossProfitTrend] [decimal] (8, 2) NOT NULL,
[OverallFAndIAvgGrossProfit12Week] [decimal] (8, 2) NOT NULL,
[OverallAvgDaysToSaleTrend] [decimal] (8, 2) NOT NULL,
[OverallAvgDaysToSale12Week] [decimal] (8, 2) NOT NULL,
[OverallAvgInventoryAgeCurrent] [decimal] (8, 2) NOT NULL,
[OverallAvgInventoryAgePrior] [decimal] (8, 2) NOT NULL,
[OverallCurrentDaysSupplyCurrent] [decimal] (8, 2) NOT NULL,
[OverallCurrentDaysSupplyPrior] [decimal] (8, 2) NOT NULL,
[AgingOver90Current] [decimal] (8, 2) NOT NULL,
[AgingOver90Prior] [decimal] (8, 2) NOT NULL,
[Aging76To90Current] [decimal] (8, 2) NOT NULL,
[Aging76To90Prior] [decimal] (8, 2) NOT NULL,
[Aging61To75Current] [decimal] (8, 2) NOT NULL,
[Aging61To75Prior] [decimal] (8, 2) NOT NULL,
[Aging31To60Current] [decimal] (8, 2) NOT NULL,
[Aging31To60Prior] [decimal] (8, 2) NOT NULL,
[Aging0To30Current] [decimal] (8, 2) NOT NULL,
[Aging0To30Prior] [decimal] (8, 2) NOT NULL,
[CoupePercentOfSalesTrend] [int] NOT NULL,
[CoupePercentOfSales12Week] [int] NOT NULL,
[CoupeRetailAvgGrossProfitTrend] [int] NOT NULL,
[CoupeRetailAvgGrossProfit12Week] [int] NOT NULL,
[CoupeRetailAvgDaysToSaleTrend] [int] NOT NULL,
[CoupeRetailAvgDaysToSale12Week] [int] NOT NULL,
[SedanPercentOfSalesTrend] [int] NOT NULL,
[SedanPercentOfSales12Week] [int] NOT NULL,
[SedanRetailAvgGrossProfitTrend] [int] NOT NULL,
[SedanRetailAvgGrossProfit12Week] [int] NOT NULL,
[SedanRetailAvgDaysToSaleTrend] [int] NOT NULL,
[SedanRetailAvgDaysToSale12Week] [int] NOT NULL,
[SUVPercentOfSalesTrend] [int] NOT NULL,
[SUVPercentOfSales12Week] [int] NOT NULL,
[SUVRetailAvgGrossProfitTrend] [int] NOT NULL,
[SUVRetailAvgGrossProfit12Week] [int] NOT NULL,
[SUVRetailAvgDaysToSaleTrend] [int] NOT NULL,
[SUVRetailAvgDaysToSale12Week] [int] NOT NULL,
[TruckPercentOfSalesTrend] [int] NOT NULL,
[TruckPercentOfSales12Week] [int] NOT NULL,
[TruckRetailAvgGrossProfitTrend] [int] NOT NULL,
[TruckRetailAvgGrossProfit12Week] [int] NOT NULL,
[TruckRetailAvgDaysToSaleTrend] [int] NOT NULL,
[TruckRetailAvgDaysToSale12Week] [int] NOT NULL,
[VanPercentOfSalesTrend] [int] NOT NULL,
[VanPercentOfSales12Week] [int] NOT NULL,
[VanRetailAvgGrossProfitTrend] [int] NOT NULL,
[VanRetailAvgGrossProfit12Week] [int] NOT NULL,
[VanRetailAvgDaysToSaleTrend] [int] NOT NULL,
[VanRetailAvgDaysToSale12Week] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tbl_NewCarScorecard] on [dbo].[tbl_NewCarScoreCard]'
GO
ALTER TABLE [dbo].[tbl_NewCarScoreCard] ADD CONSTRAINT [PK_tbl_NewCarScorecard] PRIMARY KEY CLUSTERED  ([NewCarScoreCardId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tbl_MemberATCAccessGroupList]'
GO
CREATE TABLE [dbo].[tbl_MemberATCAccessGroupList]
(
[MemberATCAccessGroupListID] [int] NOT NULL IDENTITY(1, 1),
[MemberATCAccessGroupListTypeID] [int] NOT NULL,
[MemberID] [int] NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[MaxDistanceFromDealer] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MemberATCAccessGroupList] on [dbo].[tbl_MemberATCAccessGroupList]'
GO
ALTER TABLE [dbo].[tbl_MemberATCAccessGroupList] ADD CONSTRAINT [PK_MemberATCAccessGroupList] PRIMARY KEY CLUSTERED  ([MemberATCAccessGroupListID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tbl_MemberATCAccessGroup]'
GO
CREATE TABLE [dbo].[tbl_MemberATCAccessGroup]
(
[MemberATCAccessGroupID] [int] NOT NULL IDENTITY(1, 1),
[MemberATCAccessGroupListID] [int] NOT NULL,
[MaxDistanceFromDealer] [int] NOT NULL,
[Position] [int] NOT NULL,
[Suppress] [bit] NOT NULL,
[AccessGroupID] [int] NOT NULL,
[MaxVehicleMileage] [int] NULL,
[MinVehicleMileage] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tbl_MemberATCAccessGroup] on [dbo].[tbl_MemberATCAccessGroup]'
GO
ALTER TABLE [dbo].[tbl_MemberATCAccessGroup] ADD CONSTRAINT [PK_tbl_MemberATCAccessGroup] PRIMARY KEY CLUSTERED  ([MemberATCAccessGroupID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SearchCandidateOptionAnnotation]'
GO
CREATE TABLE [dbo].[SearchCandidateOptionAnnotation]
(
[SearchCandidateOptionID] [int] NOT NULL,
[SearchCandidateOptionAnnotationTypeID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SearchCandidateOptionAnnotation] on [dbo].[SearchCandidateOptionAnnotation]'
GO
ALTER TABLE [dbo].[SearchCandidateOptionAnnotation] ADD CONSTRAINT [PK_SearchCandidateOptionAnnotation] PRIMARY KEY NONCLUSTERED  ([SearchCandidateOptionID], [SearchCandidateOptionAnnotationTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SearchCandidateCIACategoryAnnotation]'
GO
CREATE TABLE [dbo].[SearchCandidateCIACategoryAnnotation]
(
[SearchCandidateID] [int] NOT NULL,
[CIACategoryID] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SearchCandidateCIACategoryAnnotation] on [dbo].[SearchCandidateCIACategoryAnnotation]'
GO
ALTER TABLE [dbo].[SearchCandidateCIACategoryAnnotation] ADD CONSTRAINT [PK_SearchCandidateCIACategoryAnnotation] PRIMARY KEY NONCLUSTERED  ([SearchCandidateID], [CIACategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Person]'
GO
CREATE TABLE [dbo].[Person]
(
[PersonID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[MemberID] [int] NULL,
[LastName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MiddleInitial] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NickName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Salutation] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Person] on [dbo].[Person]'
GO
ALTER TABLE [dbo].[Person] ADD CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED  ([PersonID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PersonPosition]'
GO
CREATE TABLE [dbo].[PersonPosition]
(
[PersonID] [int] NOT NULL,
[PositionID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PersonPosition] on [dbo].[PersonPosition]'
GO
ALTER TABLE [dbo].[PersonPosition] ADD CONSTRAINT [PK_PersonPosition] PRIMARY KEY CLUSTERED  ([PersonID], [PositionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PackAdjustRules]'
GO
CREATE TABLE [dbo].[PackAdjustRules]
(
[PackAdjustRuleID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[SaleDescription] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VehicleSource] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UnitCostThresholdLow] [int] NULL,
[UnitCostThresholdHigh] [int] NULL,
[PackAmount] [decimal] (9, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PackAdjustRules] on [dbo].[PackAdjustRules]'
GO
ALTER TABLE [dbo].[PackAdjustRules] ADD CONSTRAINT [PK_PackAdjustRules] PRIMARY KEY CLUSTERED  ([PackAdjustRuleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerPack]'
GO
CREATE TABLE [dbo].[DealerPack]
(
[DealerPackID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[PackTypeCD] [tinyint] NOT NULL,
[PackAmount] [decimal] (8, 2) NULL,
[PackJavaClass] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DealerPack] on [dbo].[DealerPack]'
GO
ALTER TABLE [dbo].[DealerPack] ADD CONSTRAINT [PK_DealerPack] PRIMARY KEY CLUSTERED  ([DealerPackID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerFinancialStatement]'
GO
CREATE TABLE [dbo].[DealerFinancialStatement]
(
[DealerFinancialStatementID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[MonthNumber] [int] NOT NULL,
[YearNumber] [int] NOT NULL,
[RetailNumberOfDeals] [int] NULL,
[RetailTotalGrossProfit] [decimal] (9, 2) NULL,
[RetailFrontEndGrossProfit] [decimal] (9, 2) NULL,
[RetailBackEndGrossProfit] [decimal] (9, 2) NULL,
[RetailAverageGrossProfit] [decimal] (9, 2) NULL,
[WholesaleNumberOfDeals] [int] NULL,
[WholesaleTotalGrossProfit] [decimal] (9, 2) NULL,
[WholesaleFrontEndGrossProfit] [decimal] (9, 2) NULL,
[WholesaleBackEndGrossProfit] [decimal] (9, 2) NULL,
[WholesaleAverageGrossProfit] [decimal] (9, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DealerFinancialStatement] on [dbo].[DealerFinancialStatement]'
GO
ALTER TABLE [dbo].[DealerFinancialStatement] ADD CONSTRAINT [PK_DealerFinancialStatement] PRIMARY KEY NONCLUSTERED  ([DealerFinancialStatementID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerAuctionPreference]'
GO
CREATE TABLE [dbo].[DealerAuctionPreference]
(
[BusinessUnitID] [int] NOT NULL,
[AuctionSearchEnabled] [tinyint] NOT NULL,
[MaxMilesAway] [smallint] NOT NULL,
[MaxDaysAhead] [smallint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DealerAuctionPreference] on [dbo].[DealerAuctionPreference]'
GO
ALTER TABLE [dbo].[DealerAuctionPreference] ADD CONSTRAINT [PK_DealerAuctionPreference] PRIMARY KEY CLUSTERED  ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Marketplace_ThirdPartyEntity]'
GO
CREATE TABLE [dbo].[Marketplace_ThirdPartyEntity]
(
[MarketplaceID] [int] NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Marketplace] on [dbo].[Marketplace_ThirdPartyEntity]'
GO
ALTER TABLE [dbo].[Marketplace_ThirdPartyEntity] ADD CONSTRAINT [PK_Marketplace] PRIMARY KEY CLUSTERED  ([MarketplaceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[MemberBusinessUnitSet]'
GO
CREATE TABLE [dbo].[MemberBusinessUnitSet]
(
[MemberBusinessUnitSetID] [int] NOT NULL IDENTITY(1, 1),
[MemberBusinessUnitSetTypeID] [int] NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[MemberID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MemberBusinessUnitSet] on [dbo].[MemberBusinessUnitSet]'
GO
ALTER TABLE [dbo].[MemberBusinessUnitSet] ADD CONSTRAINT [PK_MemberBusinessUnitSet] PRIMARY KEY CLUSTERED  ([MemberBusinessUnitSetID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[MemberBusinessUnitSetItem]'
GO
CREATE TABLE [dbo].[MemberBusinessUnitSetItem]
(
[MemberBusinessUnitSetID] [int] NOT NULL,
[BusinessUnitID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MemberBusinessUnitSetItem] on [dbo].[MemberBusinessUnitSetItem]'
GO
ALTER TABLE [dbo].[MemberBusinessUnitSetItem] ADD CONSTRAINT [PK_MemberBusinessUnitSetItem] PRIMARY KEY CLUSTERED  ([MemberBusinessUnitSetID], [BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[WindowSticker]'
GO
CREATE TABLE [Carfax].[WindowSticker]
(
[WindowSticker] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_WindowSticker] on [Carfax].[WindowSticker]'
GO
ALTER TABLE [Carfax].[WindowSticker] ADD CONSTRAINT [PK_Carfax_WindowSticker] PRIMARY KEY CLUSTERED  ([WindowSticker])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UK_Carfax_WindowSticker__Description] on [Carfax].[WindowSticker]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_Carfax_WindowSticker__Description] ON [Carfax].[WindowSticker] ([Description])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[RequestType]'
GO
CREATE TABLE [Carfax].[RequestType]
(
[RequestType] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_RequestType] on [Carfax].[RequestType]'
GO
ALTER TABLE [Carfax].[RequestType] ADD CONSTRAINT [PK_Carfax_RequestType] PRIMARY KEY CLUSTERED  ([RequestType])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UK_Carfax_RequestType__Description] on [Carfax].[RequestType]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_Carfax_RequestType__Description] ON [Carfax].[RequestType] ([Description])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[TrackingCode]'
GO
CREATE TABLE [Carfax].[TrackingCode]
(
[TrackingCode] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_TrackingCode] on [Carfax].[TrackingCode]'
GO
ALTER TABLE [Carfax].[TrackingCode] ADD CONSTRAINT [PK_Carfax_TrackingCode] PRIMARY KEY CLUSTERED  ([TrackingCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UK_Carfax_TrackingCode_Description] on [Carfax].[TrackingCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_Carfax_TrackingCode_Description] ON [Carfax].[TrackingCode] ([Description])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[Response]'
GO
CREATE TABLE [Carfax].[Response]
(
[RequestID] [int] NOT NULL,
[ResponseCode] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReceivedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_Response] on [Carfax].[Response]'
GO
ALTER TABLE [Carfax].[Response] ADD CONSTRAINT [PK_Carfax_Response] PRIMARY KEY CLUSTERED  ([RequestID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[MarketAnalysisOwnerPriceProvider]'
GO
CREATE TABLE [Marketing].[MarketAnalysisOwnerPriceProvider]
(
[OwnerPriceProviderID] [int] NOT NULL IDENTITY(1, 1),
[OwnerId] [int] NULL,
[PriceProviderId] [tinyint] NOT NULL,
[Rank] [int] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUser] [int] NULL,
[UpdateDate] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MarketAnalysisOwnerPriceProvider] on [Marketing].[MarketAnalysisOwnerPriceProvider]'
GO
ALTER TABLE [Marketing].[MarketAnalysisOwnerPriceProvider] ADD CONSTRAINT [PK_MarketAnalysisOwnerPriceProvider] PRIMARY KEY CLUSTERED  ([OwnerPriceProviderID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[ReportInspectionResult]'
GO
CREATE TABLE [Carfax].[ReportInspectionResult]
(
[RequestID] [int] NOT NULL,
[ReportInspectionID] [tinyint] NOT NULL,
[Selected] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_ReportInspectionResult] on [Carfax].[ReportInspectionResult]'
GO
ALTER TABLE [Carfax].[ReportInspectionResult] ADD CONSTRAINT [PK_Carfax_ReportInspectionResult] PRIMARY KEY CLUSTERED  ([RequestID], [ReportInspectionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[ResponseCode]'
GO
CREATE TABLE [Carfax].[ResponseCode]
(
[ResponseCode] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsError] [bit] NOT NULL,
[IsTryAgain] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_ResponseCode] on [Carfax].[ResponseCode]'
GO
ALTER TABLE [Carfax].[ResponseCode] ADD CONSTRAINT [PK_Carfax_ResponseCode] PRIMARY KEY CLUSTERED  ([ResponseCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UK_Carfax_ResponseCode__Description] on [Carfax].[ResponseCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_Carfax_ResponseCode__Description] ON [Carfax].[ResponseCode] ([Description])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryOverrideHistory]'
GO
CREATE TABLE [dbo].[InventoryOverrideHistory]
(
[InventoryID] [int] NOT NULL,
[StockNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[VehicleID] [int] NOT NULL,
[InventoryActive] [tinyint] NOT NULL,
[InventoryReceivedDate] [smalldatetime] NOT NULL,
[DeleteDt] [smalldatetime] NULL,
[ModifiedDT] [smalldatetime] NOT NULL,
[DMSReferenceDt] [smalldatetime] NULL,
[UnitCost] [decimal] (9, 2) NOT NULL,
[AcquisitionPrice] [decimal] (8, 2) NULL,
[Pack] [decimal] (8, 2) NULL,
[MileageReceived] [int] NULL,
[TradeOrPurchase] [tinyint] NOT NULL,
[ListPrice] [decimal] (8, 2) NULL,
[InitialVehicleLight] [int] NULL,
[Level4Analysis] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InventoryType] [tinyint] NOT NULL,
[ReconditionCost] [decimal] (8, 2) NULL,
[UsedSellingPrice] [decimal] (8, 2) NULL,
[Certified] [tinyint] NOT NULL,
[VehicleLocation] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrentVehicleLight] [tinyint] NULL,
[FLRecFollowed] [tinyint] NOT NULL,
[Audit_ID] [int] NOT NULL,
[InventoryStatusCD] [smallint] NOT NULL,
[ListPriceLock] [tinyint] NOT NULL,
[SpecialFinance] [tinyint] NULL,
[eStockCardLock] [tinyint] NOT NULL,
[DateBookoutLocked] [smalldatetime] NULL,
[PlanReminderDate] [smalldatetime] NULL,
[NewStockNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateCreated] [smalldatetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[ReportInspection]'
GO
CREATE TABLE [Carfax].[ReportInspection]
(
[ReportInspectionID] [tinyint] NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[XPath] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_ReportInspectionID] on [Carfax].[ReportInspection]'
GO
ALTER TABLE [Carfax].[ReportInspection] ADD CONSTRAINT [PK_Carfax_ReportInspectionID] PRIMARY KEY CLUSTERED  ([ReportInspectionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UK_Carfax_ReportInspection__Description] on [Carfax].[ReportInspection]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_Carfax_ReportInspection__Description] ON [Carfax].[ReportInspection] ([Description])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SystemErrors]'
GO
CREATE TABLE [dbo].[SystemErrors]
(
[SystemErrorID] [int] NOT NULL IDENTITY(1, 1),
[Trace] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateCreated] [smalldatetime] NOT NULL,
[BusinessUnitID] [int] NULL,
[MemberID] [int] NULL,
[HTTPSessionID] [varchar] (127) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Product] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Source] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HalSessionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SystemErrors] on [dbo].[SystemErrors]'
GO
ALTER TABLE [dbo].[SystemErrors] ADD CONSTRAINT [PK_SystemErrors] PRIMARY KEY CLUSTERED  ([SystemErrorID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[ReportPreference]'
GO
CREATE TABLE [Carfax].[ReportPreference]
(
[DealerID] [int] NOT NULL,
[VehicleEntityTypeID] [tinyint] NOT NULL,
[AutoPurchase] [bit] NOT NULL,
[DisplayInHotList] [bit] NOT NULL,
[ReportType] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUser] [int] NULL,
[UpdateDate] [datetime] NULL,
[Version] [timestamp] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_ReportPreferences] on [Carfax].[ReportPreference]'
GO
ALTER TABLE [Carfax].[ReportPreference] ADD CONSTRAINT [PK_Carfax_ReportPreferences] PRIMARY KEY CLUSTERED  ([DealerID], [VehicleEntityTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[ReportProcessorCommandStatus]'
GO
CREATE TABLE [Carfax].[ReportProcessorCommandStatus]
(
[ReportProcessorCommandStatusID] [tinyint] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_ReportProcessorCommandStatus] on [Carfax].[ReportProcessorCommandStatus]'
GO
ALTER TABLE [Carfax].[ReportProcessorCommandStatus] ADD CONSTRAINT [PK_Carfax_ReportProcessorCommandStatus] PRIMARY KEY CLUSTERED  ([ReportProcessorCommandStatusID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UK_Carfax_ReportProcessorCommandStatus__Name] on [Carfax].[ReportProcessorCommandStatus]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_Carfax_ReportProcessorCommandStatus__Name] ON [Carfax].[ReportProcessorCommandStatus] ([Name])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[DealerGeneralPreference]'
GO
CREATE TABLE [Marketing].[DealerGeneralPreference]
(
[OwnerID] [int] NOT NULL,
[AddressLine1] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AddressLine2] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[City] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StateCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ZipCode] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SalesEmailAddress] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SalesPhoneNumber] [varchar] (14) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExtendedTagLine] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DaysValidFor] [int] NOT NULL,
[Disclaimer] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUser] [int] NULL,
[UpdateDate] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Marketing_DealerGeneralPreference] on [Marketing].[DealerGeneralPreference]'
GO
ALTER TABLE [Marketing].[DealerGeneralPreference] ADD CONSTRAINT [PK_Marketing_DealerGeneralPreference] PRIMARY KEY CLUSTERED  ([OwnerID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[VehicleEntityType]'
GO
CREATE TABLE [Carfax].[VehicleEntityType]
(
[VehicleEntityTypeID] [tinyint] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_VehicleEntityType] on [Carfax].[VehicleEntityType]'
GO
ALTER TABLE [Carfax].[VehicleEntityType] ADD CONSTRAINT [PK_Carfax_VehicleEntityType] PRIMARY KEY CLUSTERED  ([VehicleEntityTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UK_Carfax_VehicleEntityType__Name] on [Carfax].[VehicleEntityType]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_Carfax_VehicleEntityType__Name] ON [Carfax].[VehicleEntityType] ([Name])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[ReportType]'
GO
CREATE TABLE [Carfax].[ReportType]
(
[ReportType] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_ReportType] on [Carfax].[ReportType]'
GO
ALTER TABLE [Carfax].[ReportType] ADD CONSTRAINT [PK_Carfax_ReportType] PRIMARY KEY CLUSTERED  ([ReportType])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UK_Carfax_ReportType__Name] on [Carfax].[ReportType]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_Carfax_ReportType__Name] ON [Carfax].[ReportType] ([Name])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[ReportProcessorCommand_Request]'
GO
CREATE TABLE [Carfax].[ReportProcessorCommand_Request]
(
[ReportProcessorCommandID] [int] NOT NULL,
[RequestID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_ReportProcessorCommand_Request] on [Carfax].[ReportProcessorCommand_Request]'
GO
ALTER TABLE [Carfax].[ReportProcessorCommand_Request] ADD CONSTRAINT [PK_Carfax_ReportProcessorCommand_Request] PRIMARY KEY CLUSTERED  ([ReportProcessorCommandID], [RequestID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[ReportProcessorCommand_Exception]'
GO
CREATE TABLE [Carfax].[ReportProcessorCommand_Exception]
(
[ReportProcessorCommandID] [int] NOT NULL,
[ExceptionID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_ReportProcessorCommand_Exception] on [Carfax].[ReportProcessorCommand_Exception]'
GO
ALTER TABLE [Carfax].[ReportProcessorCommand_Exception] ADD CONSTRAINT [PK_Carfax_ReportProcessorCommand_Exception] PRIMARY KEY CLUSTERED  ([ReportProcessorCommandID], [ExceptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[Exception]'
GO
CREATE TABLE [Carfax].[Exception]
(
[ExceptionID] [int] NOT NULL IDENTITY(1, 1),
[ExceptionTime] [datetime] NOT NULL,
[MachineName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Message] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExceptionType] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Details] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequestID] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_Exception] on [Carfax].[Exception]'
GO
ALTER TABLE [Carfax].[Exception] ADD CONSTRAINT [PK_Carfax_Exception] PRIMARY KEY CLUSTERED  ([ExceptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[ReportProcessorCommand_DataMigration]'
GO
CREATE TABLE [Carfax].[ReportProcessorCommand_DataMigration]
(
[ReportProcessorCommandID] [int] NOT NULL,
[ReportType] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsHotListed] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_ReportProcessorCommand_DataMigration] on [Carfax].[ReportProcessorCommand_DataMigration]'
GO
ALTER TABLE [Carfax].[ReportProcessorCommand_DataMigration] ADD CONSTRAINT [PK_Carfax_ReportProcessorCommand_DataMigration] PRIMARY KEY CLUSTERED  ([ReportProcessorCommandID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CIABucketRules]'
GO
CREATE TABLE [dbo].[CIABucketRules]
(
[CIABucketRuleID] [int] NOT NULL IDENTITY(1, 1),
[CIABucketID] [int] NOT NULL,
[CIABucketRuleTypeID] [tinyint] NOT NULL,
[Value] [int] NOT NULL,
[Value2] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_CIABucketRules] on [dbo].[CIABucketRules]'
GO
ALTER TABLE [dbo].[CIABucketRules] ADD CONSTRAINT [PK_CIABucketRules] PRIMARY KEY CLUSTERED  ([CIABucketRuleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CIABucketGroups]'
GO
CREATE TABLE [dbo].[CIABucketGroups]
(
[CIABucketGroupID] [tinyint] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_CIABucketGroups] on [dbo].[CIABucketGroups]'
GO
ALTER TABLE [dbo].[CIABucketGroups] ADD CONSTRAINT [PK_CIABucketGroups] PRIMARY KEY CLUSTERED  ([CIABucketGroupID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[Equipment]'
GO
CREATE TABLE [Marketing].[Equipment]
(
[EquipmentListID] [int] NOT NULL,
[EquipmentID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Value] [int] NULL,
[Selected] [bit] NOT NULL,
[Position] [tinyint] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUser] [int] NULL,
[UpdateDate] [datetime] NULL,
[Version] [timestamp] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Marketing_Equipment__EquipmentList] on [Marketing].[Equipment]'
GO
CREATE CLUSTERED INDEX [IX_Marketing_Equipment__EquipmentList] ON [Marketing].[Equipment] ([EquipmentListID], [EquipmentID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Marketing_Equipment] on [Marketing].[Equipment]'
GO
ALTER TABLE [Marketing].[Equipment] ADD CONSTRAINT [PK_Marketing_Equipment] PRIMARY KEY NONCLUSTERED  ([EquipmentID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[EquipmentList]'
GO
CREATE TABLE [Marketing].[EquipmentList]
(
[EquipmentListID] [int] NOT NULL IDENTITY(1, 1),
[EquipmentProviderID] [tinyint] NOT NULL,
[OwnerID] [int] NOT NULL,
[VehicleEntityTypeID] [tinyint] NOT NULL,
[VehicleEntityID] [int] NOT NULL,
[IdentityValue] [bigint] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Marketing_EquipmentList] on [Marketing].[EquipmentList]'
GO
ALTER TABLE [Marketing].[EquipmentList] ADD CONSTRAINT [PK_Marketing_EquipmentList] PRIMARY KEY CLUSTERED  ([EquipmentListID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Marketing_EquipmentList__Owner_VehicleEntityType_VehicleEntity] on [Marketing].[EquipmentList]'
GO
CREATE NONCLUSTERED INDEX [IX_Marketing_EquipmentList__Owner_VehicleEntityType_VehicleEntity] ON [Marketing].[EquipmentList] ([OwnerID], [VehicleEntityTypeID], [VehicleEntityID], [EquipmentProviderID]) INCLUDE ([EquipmentListID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[EquipmentProvider]'
GO
CREATE TABLE [Marketing].[EquipmentProvider]
(
[EquipmentProviderID] [tinyint] NOT NULL,
[Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ThirdPartyID] [tinyint] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Marketing_EquipmentProvider] on [Marketing].[EquipmentProvider]'
GO
ALTER TABLE [Marketing].[EquipmentProvider] ADD CONSTRAINT [PK_Marketing_EquipmentProvider] PRIMARY KEY CLUSTERED  ([EquipmentProviderID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[EquipmentProviderAssignmentList]'
GO
CREATE TABLE [Marketing].[EquipmentProviderAssignmentList]
(
[EquipmentProviderAssignmentListID] [int] NOT NULL IDENTITY(1, 1),
[OwnerID] [int] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Marketing_EquipmentProviderAssignmentList] on [Marketing].[EquipmentProviderAssignmentList]'
GO
ALTER TABLE [Marketing].[EquipmentProviderAssignmentList] ADD CONSTRAINT [PK_Marketing_EquipmentProviderAssignmentList] PRIMARY KEY CLUSTERED  ([EquipmentProviderAssignmentListID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [AutoCheck].[Account_Member]'
GO
CREATE TABLE [AutoCheck].[Account_Member]
(
[AccountID] [int] NOT NULL,
[MemberID] [int] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AutoCheck_Account_Member] on [AutoCheck].[Account_Member]'
GO
ALTER TABLE [AutoCheck].[Account_Member] ADD CONSTRAINT [PK_AutoCheck_Account_Member] PRIMARY KEY CLUSTERED  ([AccountID], [MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[RepriceSource]'
GO
CREATE TABLE [dbo].[RepriceSource]
(
[RepriceSourceID] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_RepriceSource] on [dbo].[RepriceSource]'
GO
ALTER TABLE [dbo].[RepriceSource] ADD CONSTRAINT [PK_RepriceSource] PRIMARY KEY CLUSTERED  ([RepriceSourceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [AutoCheck].[Account]'
GO
CREATE TABLE [AutoCheck].[Account]
(
[AccountID] [int] NOT NULL IDENTITY(1, 1),
[AccountStatusID] [tinyint] NOT NULL,
[AccountTypeID] [tinyint] NOT NULL,
[UserName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Password] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUser] [int] NULL,
[UpdateDate] [datetime] NULL,
[DeleteUser] [int] NULL,
[DeleteDate] [datetime] NULL,
[Version] [timestamp] NOT NULL,
[Active] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AutoCheck_Account] on [AutoCheck].[Account]'
GO
ALTER TABLE [AutoCheck].[Account] ADD CONSTRAINT [PK_AutoCheck_Account] PRIMARY KEY CLUSTERED  ([AccountID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [AutoCheck].[Report]'
GO
CREATE TABLE [AutoCheck].[Report]
(
[RequestID] [int] NOT NULL,
[Report] [xml] NOT NULL,
[Score] [int] NULL,
[CompareScoreRangeLow] [int] NULL,
[CompareScoreRangeHigh] [int] NULL,
[Assured] [bit] NULL,
[OwnerCount] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AutoCheck_Report] on [AutoCheck].[Report]'
GO
ALTER TABLE [AutoCheck].[Report] ADD CONSTRAINT [PK_AutoCheck_Report] PRIMARY KEY CLUSTERED  ([RequestID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [AutoCheck].[Vehicle]'
GO
CREATE TABLE [AutoCheck].[Vehicle]
(
[VehicleID] [int] NOT NULL IDENTITY(1, 1),
[VIN] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RequestID] [int] NULL,
[InsertDate] [datetime] NOT NULL,
[InsertUser] [int] NOT NULL,
[UpdateDate] [datetime] NULL,
[UpdateUser] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AutoCheck_Vehicle] on [AutoCheck].[Vehicle]'
GO
ALTER TABLE [AutoCheck].[Vehicle] ADD CONSTRAINT [PK_AutoCheck_Vehicle] PRIMARY KEY CLUSTERED  ([VehicleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UK_AutoCheck_Vehicle__VIN] on [AutoCheck].[Vehicle]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_AutoCheck_Vehicle__VIN] ON [AutoCheck].[Vehicle] ([VIN])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [AutoCheck].[Request]'
GO
CREATE TABLE [AutoCheck].[Request]
(
[RequestID] [int] NOT NULL IDENTITY(1, 1),
[VehicleID] [int] NOT NULL,
[AccountID] [int] NOT NULL,
[ReportTypeID] [tinyint] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AutoCheck_Request] on [AutoCheck].[Request]'
GO
ALTER TABLE [AutoCheck].[Request] ADD CONSTRAINT [PK_AutoCheck_Request] PRIMARY KEY CLUSTERED  ([RequestID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [AutoCheck].[AccountType]'
GO
CREATE TABLE [AutoCheck].[AccountType]
(
[AccountTypeID] [tinyint] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AutoCheck_AccountType] on [AutoCheck].[AccountType]'
GO
ALTER TABLE [AutoCheck].[AccountType] ADD CONSTRAINT [PK_AutoCheck_AccountType] PRIMARY KEY CLUSTERED  ([AccountTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UK_AutoCheck_AccountType__Name] on [AutoCheck].[AccountType]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_AutoCheck_AccountType__Name] ON [AutoCheck].[AccountType] ([Name])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [AutoCheck].[Account_Dealer]'
GO
CREATE TABLE [AutoCheck].[Account_Dealer]
(
[DealerID] [int] NOT NULL,
[AccountID] [int] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AutoCheck_Account_Dealer] on [AutoCheck].[Account_Dealer]'
GO
ALTER TABLE [AutoCheck].[Account_Dealer] ADD CONSTRAINT [PK_AutoCheck_Account_Dealer] PRIMARY KEY CLUSTERED  ([DealerID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[EquipmentProviderAssignment]'
GO
CREATE TABLE [Marketing].[EquipmentProviderAssignment]
(
[EquipmentProviderAssignmentListID] [int] NOT NULL,
[EquipmentProviderID] [tinyint] NOT NULL,
[IsDefault] [bit] NOT NULL,
[Position] [tinyint] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUser] [int] NULL,
[UpdateDate] [datetime] NULL,
[Version] [timestamp] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Marketing_EquipmentProviderAssignment] on [Marketing].[EquipmentProviderAssignment]'
GO
ALTER TABLE [Marketing].[EquipmentProviderAssignment] ADD CONSTRAINT [PK_Marketing_EquipmentProviderAssignment] PRIMARY KEY CLUSTERED  ([EquipmentProviderAssignmentListID], [EquipmentProviderID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [AutoCheck].[Exception]'
GO
CREATE TABLE [AutoCheck].[Exception]
(
[ExceptionID] [int] NOT NULL IDENTITY(1, 1),
[ExceptionTime] [datetime] NOT NULL,
[MachineName] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Message] [nvarchar] (1024) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExceptionType] [nvarchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Details] [ntext] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequestID] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AutoCheck_Exception] on [AutoCheck].[Exception]'
GO
ALTER TABLE [AutoCheck].[Exception] ADD CONSTRAINT [PK_AutoCheck_Exception] PRIMARY KEY CLUSTERED  ([ExceptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [AutoCheck].[Account_ServiceType]'
GO
CREATE TABLE [AutoCheck].[Account_ServiceType]
(
[ServiceTypeID] [tinyint] NOT NULL,
[AccountID] [int] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AutoCheck_Account_ServiceType] on [AutoCheck].[Account_ServiceType]'
GO
ALTER TABLE [AutoCheck].[Account_ServiceType] ADD CONSTRAINT [PK_AutoCheck_Account_ServiceType] PRIMARY KEY CLUSTERED  ([ServiceTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [AutoCheck].[ServiceType]'
GO
CREATE TABLE [AutoCheck].[ServiceType]
(
[ServiceTypeID] [tinyint] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AutoCheck_ServiceType] on [AutoCheck].[ServiceType]'
GO
ALTER TABLE [AutoCheck].[ServiceType] ADD CONSTRAINT [PK_AutoCheck_ServiceType] PRIMARY KEY CLUSTERED  ([ServiceTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UK_AutoCheck_ServiceType__Name] on [AutoCheck].[ServiceType]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_AutoCheck_ServiceType__Name] ON [AutoCheck].[ServiceType] ([Name])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SubscriptionTypeDealerUpgrade]'
GO
CREATE TABLE [dbo].[SubscriptionTypeDealerUpgrade]
(
[SubscriptionTypeID] [tinyint] NOT NULL,
[DealerUpgradeCD] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SubscriptionTypeDealerUpgrade] on [dbo].[SubscriptionTypeDealerUpgrade]'
GO
ALTER TABLE [dbo].[SubscriptionTypeDealerUpgrade] ADD CONSTRAINT [PK_SubscriptionTypeDealerUpgrade] PRIMARY KEY CLUSTERED  ([SubscriptionTypeID], [DealerUpgradeCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [AutoCheck].[Vehicle_Dealer]'
GO
CREATE TABLE [AutoCheck].[Vehicle_Dealer]
(
[VehicleID] [int] NOT NULL,
[DealerID] [int] NOT NULL,
[RequestID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AutoCheck_Vehicle_Dealer] on [AutoCheck].[Vehicle_Dealer]'
GO
ALTER TABLE [AutoCheck].[Vehicle_Dealer] ADD CONSTRAINT [PK_AutoCheck_Vehicle_Dealer] PRIMARY KEY CLUSTERED  ([VehicleID], [DealerID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [AutoCheck].[ReportInspectionResult]'
GO
CREATE TABLE [AutoCheck].[ReportInspectionResult]
(
[RequestID] [int] NOT NULL,
[ReportInspectionID] [tinyint] NOT NULL,
[Selected] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AutoCheck_ReportInspectionResult] on [AutoCheck].[ReportInspectionResult]'
GO
ALTER TABLE [AutoCheck].[ReportInspectionResult] ADD CONSTRAINT [PK_AutoCheck_ReportInspectionResult] PRIMARY KEY CLUSTERED  ([RequestID], [ReportInspectionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [AutoCheck].[Response]'
GO
CREATE TABLE [AutoCheck].[Response]
(
[RequestID] [int] NOT NULL,
[ResponseCode] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReceivedDate] [datetime] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AutoCheck_Response] on [AutoCheck].[Response]'
GO
ALTER TABLE [AutoCheck].[Response] ADD CONSTRAINT [PK_AutoCheck_Response] PRIMARY KEY CLUSTERED  ([RequestID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [AutoCheck].[ResponseCode]'
GO
CREATE TABLE [AutoCheck].[ResponseCode]
(
[ResponseCode] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsError] [bit] NOT NULL,
[IsTryAgain] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AutoCheck_ResponseCode] on [AutoCheck].[ResponseCode]'
GO
ALTER TABLE [AutoCheck].[ResponseCode] ADD CONSTRAINT [PK_AutoCheck_ResponseCode] PRIMARY KEY CLUSTERED  ([ResponseCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UK_AutoCheck_ResponseCode__Description] on [AutoCheck].[ResponseCode]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_AutoCheck_ResponseCode__Description] ON [AutoCheck].[ResponseCode] ([Description])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [AutoCheck].[ReportInspection]'
GO
CREATE TABLE [AutoCheck].[ReportInspection]
(
[ReportInspectionID] [tinyint] NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[XPath] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AutoCheck_ReportInspectionID] on [AutoCheck].[ReportInspection]'
GO
ALTER TABLE [AutoCheck].[ReportInspection] ADD CONSTRAINT [PK_AutoCheck_ReportInspectionID] PRIMARY KEY CLUSTERED  ([ReportInspectionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UK_AutoCheck_ReportInspection__Description] on [AutoCheck].[ReportInspection]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_AutoCheck_ReportInspection__Description] ON [AutoCheck].[ReportInspection] ([Description])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BusinessUnitType]'
GO
CREATE TABLE [dbo].[BusinessUnitType]
(
[BusinessUnitTypeID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitType] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BusinessUnitType] on [dbo].[BusinessUnitType]'
GO
ALTER TABLE [dbo].[BusinessUnitType] ADD CONSTRAINT [PK_BusinessUnitType] PRIMARY KEY NONCLUSTERED  ([BusinessUnitTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[TransferPriceInventoryAgeRange]'
GO
CREATE TABLE [dbo].[TransferPriceInventoryAgeRange]
(
[TransferPriceInventoryAgeRangeID] [int] NOT NULL IDENTITY(1, 1),
[Low] [int] NOT NULL,
[High] [int] NOT NULL,
[Description] AS (((CONVERT([varchar],[Low],0)+' to ')+case when [HIGH]=(2147483647) then 'Unlimited' else CONVERT([varchar],[High],0) end)+' days')
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_TransferPriceInventoryAgeRange] on [dbo].[TransferPriceInventoryAgeRange]'
GO
ALTER TABLE [dbo].[TransferPriceInventoryAgeRange] ADD CONSTRAINT [PK_TransferPriceInventoryAgeRange] PRIMARY KEY CLUSTERED  ([TransferPriceInventoryAgeRangeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerTransferAllowRetailOnlyRule]'
GO
CREATE TABLE [dbo].[DealerTransferAllowRetailOnlyRule]
(
[DealerTransferAllowRetailOnlyRuleID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[TransferPriceInventoryAgeRangeID] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_DealerTransferAllowRetailOnlyRule_BusinessUnitID] on [dbo].[DealerTransferAllowRetailOnlyRule]'
GO
CREATE CLUSTERED INDEX [IX_DealerTransferAllowRetailOnlyRule_BusinessUnitID] ON [dbo].[DealerTransferAllowRetailOnlyRule] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DealerTransferAllowRetailOnlyRule] on [dbo].[DealerTransferAllowRetailOnlyRule]'
GO
ALTER TABLE [dbo].[DealerTransferAllowRetailOnlyRule] ADD CONSTRAINT [PK_DealerTransferAllowRetailOnlyRule] PRIMARY KEY NONCLUSTERED  ([DealerTransferAllowRetailOnlyRuleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerTransferPriceUnitCostRule]'
GO
CREATE TABLE [dbo].[DealerTransferPriceUnitCostRule]
(
[DealerTransferPriceUnitCostRuleID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[TransferPriceUnitCostValueID] [int] NOT NULL,
[TransferPriceInventoryAgeRangeID] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_DealerTransferPriceUnitCostRule_BusinessUnitID] on [dbo].[DealerTransferPriceUnitCostRule]'
GO
CREATE CLUSTERED INDEX [IX_DealerTransferPriceUnitCostRule_BusinessUnitID] ON [dbo].[DealerTransferPriceUnitCostRule] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DealerTransferPriceUnitCostRule] on [dbo].[DealerTransferPriceUnitCostRule]'
GO
ALTER TABLE [dbo].[DealerTransferPriceUnitCostRule] ADD CONSTRAINT [PK_DealerTransferPriceUnitCostRule] PRIMARY KEY NONCLUSTERED  ([DealerTransferPriceUnitCostRuleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_DealerTransferPriceUnitCostRule_TransferPriceUnitCostValueID] on [dbo].[DealerTransferPriceUnitCostRule]'
GO
CREATE NONCLUSTERED INDEX [IX_DealerTransferPriceUnitCostRule_TransferPriceUnitCostValueID] ON [dbo].[DealerTransferPriceUnitCostRule] ([TransferPriceUnitCostValueID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_DealerTransferPriceUnitCostRule_TransferPriceInventoryAgeRangeID] on [dbo].[DealerTransferPriceUnitCostRule]'
GO
CREATE NONCLUSTERED INDEX [IX_DealerTransferPriceUnitCostRule_TransferPriceInventoryAgeRangeID] ON [dbo].[DealerTransferPriceUnitCostRule] ([TransferPriceInventoryAgeRangeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [AutoCheck].[ReportType]'
GO
CREATE TABLE [AutoCheck].[ReportType]
(
[ReportTypeID] [tinyint] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AutoCheck_ReportType] on [AutoCheck].[ReportType]'
GO
ALTER TABLE [AutoCheck].[ReportType] ADD CONSTRAINT [PK_AutoCheck_ReportType] PRIMARY KEY CLUSTERED  ([ReportTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UK_AutoCheck_ReportType__Name] on [AutoCheck].[ReportType]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_AutoCheck_ReportType__Name] ON [AutoCheck].[ReportType] ([Name])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerPreference_WindowSticker]'
GO
CREATE TABLE [dbo].[DealerPreference_WindowSticker]
(
[BusinessUnitID] [int] NOT NULL,
[WindowStickerTemplateId] [int] NOT NULL,
[WindowStickerBuyersGuideTemplateId] [int] NOT NULL CONSTRAINT [DF_DP_WindowStickerBuyingGuideTemplateId] DEFAULT ((1))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DP_WindowSticker_BusinessUnitID] on [dbo].[DealerPreference_WindowSticker]'
GO
ALTER TABLE [dbo].[DealerPreference_WindowSticker] ADD CONSTRAINT [PK_DP_WindowSticker_BusinessUnitID] PRIMARY KEY CLUSTERED  ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PurchasingCenterChannels]'
GO
CREATE TABLE [dbo].[PurchasingCenterChannels]
(
[ChannelID] [int] NOT NULL IDENTITY(1, 1),
[ChannelName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HasAccessGroups] [bit] NOT NULL CONSTRAINT [DF_PurchasingCenterChannels_HasAccessGroups] DEFAULT (0)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PurchasingCenterChannels] on [dbo].[PurchasingCenterChannels]'
GO
ALTER TABLE [dbo].[PurchasingCenterChannels] ADD CONSTRAINT [PK_PurchasingCenterChannels] PRIMARY KEY CLUSTERED  ([ChannelID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[EdmundsBodyTypes]'
GO
CREATE TABLE [dbo].[EdmundsBodyTypes]
(
[EdmundsBodyType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExtendsVehicleBodyStyleDescription] [bit] NOT NULL CONSTRAINT [DF__EdmundsBo__Exten__1E662E14] DEFAULT (0)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__EdmundsBodyTypes__1D7209DB] on [dbo].[EdmundsBodyTypes]'
GO
ALTER TABLE [dbo].[EdmundsBodyTypes] ADD CONSTRAINT [PK__EdmundsBodyTypes__1D7209DB] PRIMARY KEY CLUSTERED  ([EdmundsBodyType])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[MemberAppraisalReviewPreferences]'
GO
CREATE TABLE [dbo].[MemberAppraisalReviewPreferences]
(
[MemberAppraisalReviewPreferenceID] [int] NOT NULL IDENTITY(1, 1),
[MemberID] [int] NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[DaysBack] [int] NOT NULL CONSTRAINT [DF_MemberAppraisalReviewPreferences_DaysBack] DEFAULT ((10))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MemberAppraisalReviewPreferences] on [dbo].[MemberAppraisalReviewPreferences]'
GO
ALTER TABLE [dbo].[MemberAppraisalReviewPreferences] ADD CONSTRAINT [PK_MemberAppraisalReviewPreferences] PRIMARY KEY NONCLUSTERED  ([MemberAppraisalReviewPreferenceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[OptionDetail]'
GO
CREATE TABLE [dbo].[OptionDetail]
(
[OptionDetailID] [int] NOT NULL IDENTITY(1, 1),
[OptionName] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OptionKey] [int] NOT NULL CONSTRAINT [DF_OptionDetail_OptionKey] DEFAULT (0),
[DataSourceID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_OptionDetail] on [dbo].[OptionDetail]'
GO
ALTER TABLE [dbo].[OptionDetail] ADD CONSTRAINT [PK_OptionDetail] PRIMARY KEY NONCLUSTERED  ([OptionDetailID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BusinessUnitStatusCode]'
GO
CREATE TABLE [dbo].[BusinessUnitStatusCode]
(
[BusinessUnitStatusCodeID] [tinyint] NOT NULL,
[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_BusinessUnitStatus__IsActive] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BusinessUnitStatusCode] on [dbo].[BusinessUnitStatusCode]'
GO
ALTER TABLE [dbo].[BusinessUnitStatusCode] ADD CONSTRAINT [PK_BusinessUnitStatusCode] PRIMARY KEY CLUSTERED  ([BusinessUnitStatusCodeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryBookoutFailures]'
GO
CREATE TABLE [dbo].[InventoryBookoutFailures]
(
[InventoryID] [int] NULL,
[BookoutID] [int] NULL,
[DateCreate] [smalldatetime] NOT NULL CONSTRAINT [DF__Inventory__DateC__4C62CE45] DEFAULT (getdate())
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[MemberLicenseAcceptance]'
GO
CREATE TABLE [dbo].[MemberLicenseAcceptance]
(
[MemberID] [int] NOT NULL,
[LicenseID] [int] NOT NULL,
[DateCreated] [smalldatetime] NOT NULL CONSTRAINT [FK_MemberLicenseAcceptance_DateCreated] DEFAULT (getdate())
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MemberLicenseAcceptance] on [dbo].[MemberLicenseAcceptance]'
GO
ALTER TABLE [dbo].[MemberLicenseAcceptance] ADD CONSTRAINT [PK_MemberLicenseAcceptance] PRIMARY KEY CLUSTERED  ([MemberID], [LicenseID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AIP_EventCategory]'
GO
CREATE TABLE [dbo].[AIP_EventCategory]
(
[AIP_EventCategoryID] [tinyint] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AIP_EventCategory] on [dbo].[AIP_EventCategory]'
GO
ALTER TABLE [dbo].[AIP_EventCategory] ADD CONSTRAINT [PK_AIP_EventCategory] PRIMARY KEY CLUSTERED  ([AIP_EventCategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ThirdPartyEntityType]'
GO
CREATE TABLE [dbo].[ThirdPartyEntityType]
(
[ThirdPartyEntityTypeID] [tinyint] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ThirdPartyEntityType] on [dbo].[ThirdPartyEntityType]'
GO
ALTER TABLE [dbo].[ThirdPartyEntityType] ADD CONSTRAINT [PK_ThirdPartyEntityType] PRIMARY KEY CLUSTERED  ([ThirdPartyEntityTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AppraisalStatus]'
GO
CREATE TABLE [dbo].[AppraisalStatus]
(
[AppraisalStatusID] [tinyint] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AppraisalStatus] on [dbo].[AppraisalStatus]'
GO
ALTER TABLE [dbo].[AppraisalStatus] ADD CONSTRAINT [PK_AppraisalStatus] PRIMARY KEY CLUSTERED  ([AppraisalStatusID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AppraisalActionTypes]'
GO
CREATE TABLE [dbo].[AppraisalActionTypes]
(
[AppraisalActionTypeID] [tinyint] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AppraisalActionTypes] on [dbo].[AppraisalActionTypes]'
GO
ALTER TABLE [dbo].[AppraisalActionTypes] ADD CONSTRAINT [PK_AppraisalActionTypes] PRIMARY KEY CLUSTERED  ([AppraisalActionTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AppraisalSource]'
GO
CREATE TABLE [dbo].[AppraisalSource]
(
[AppraisalSourceID] [tinyint] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AppraisalSource] on [dbo].[AppraisalSource]'
GO
ALTER TABLE [dbo].[AppraisalSource] ADD CONSTRAINT [PK_AppraisalSource] PRIMARY KEY CLUSTERED  ([AppraisalSourceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AppraisalType]'
GO
CREATE TABLE [dbo].[AppraisalType]
(
[AppraisalTypeID] [int] NOT NULL,
[Description] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AppraisalType] on [dbo].[AppraisalType]'
GO
ALTER TABLE [dbo].[AppraisalType] ADD CONSTRAINT [PK_AppraisalType] PRIMARY KEY CLUSTERED  ([AppraisalTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_AuditStatus]'
GO
CREATE TABLE [dbo].[lu_AuditStatus]
(
[AuditStatusCD] [tinyint] NOT NULL,
[AuditStatusDESC] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ShortDesc] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_AuditStatus] on [dbo].[lu_AuditStatus]'
GO
ALTER TABLE [dbo].[lu_AuditStatus] ADD CONSTRAINT [PK_lu_AuditStatus] PRIMARY KEY NONCLUSTERED  ([AuditStatusCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Audit_Inventory]'
GO
CREATE TABLE [dbo].[Audit_Inventory]
(
[AUDIT_ID] [int] NOT NULL,
[STATUS] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Audit_Inventory] on [dbo].[Audit_Inventory]'
GO
ALTER TABLE [dbo].[Audit_Inventory] ADD CONSTRAINT [PK_Audit_Inventory] PRIMARY KEY CLUSTERED  ([AUDIT_ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Audit_Sales]'
GO
CREATE TABLE [dbo].[Audit_Sales]
(
[AUDIT_ID] [int] NOT NULL,
[STATUS] [tinyint] NOT NULL,
[PACK_ADJUST] [decimal] (8, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Audit_Sales] on [dbo].[Audit_Sales]'
GO
ALTER TABLE [dbo].[Audit_Sales] ADD CONSTRAINT [PK_Audit_Sales] PRIMARY KEY CLUSTERED  ([AUDIT_ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BookOutTypes]'
GO
CREATE TABLE [dbo].[BookOutTypes]
(
[BookOutTypeId] [int] NOT NULL,
[BookOutDescription] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BookOutTypes] on [dbo].[BookOutTypes]'
GO
ALTER TABLE [dbo].[BookOutTypes] ADD CONSTRAINT [PK_BookOutTypes] PRIMARY KEY CLUSTERED  ([BookOutTypeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [AutoCheck].[AccountStatus]'
GO
CREATE TABLE [AutoCheck].[AccountStatus]
(
[AccountStatusID] [tinyint] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AutoCheck_AccountStatus] on [AutoCheck].[AccountStatus]'
GO
ALTER TABLE [AutoCheck].[AccountStatus] ADD CONSTRAINT [PK_AutoCheck_AccountStatus] PRIMARY KEY CLUSTERED  ([AccountStatusID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UK_AutoCheck_AccountStatus__Name] on [AutoCheck].[AccountStatus]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_AutoCheck_AccountStatus__Name] ON [AutoCheck].[AccountStatus] ([Name])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BookoutProcessorMode]'
GO
CREATE TABLE [dbo].[BookoutProcessorMode]
(
[BookoutProcessorModeId] [tinyint] NOT NULL,
[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BookoutProcessorMode] on [dbo].[BookoutProcessorMode]'
GO
ALTER TABLE [dbo].[BookoutProcessorMode] ADD CONSTRAINT [PK_BookoutProcessorMode] PRIMARY KEY CLUSTERED  ([BookoutProcessorModeId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BookoutProcessorStatus]'
GO
CREATE TABLE [dbo].[BookoutProcessorStatus]
(
[BookoutProcessorStatusId] [tinyint] NOT NULL,
[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BookoutProcessorStatus] on [dbo].[BookoutProcessorStatus]'
GO
ALTER TABLE [dbo].[BookoutProcessorStatus] ADD CONSTRAINT [PK_BookoutProcessorStatus] PRIMARY KEY CLUSTERED  ([BookoutProcessorStatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BookoutSources]'
GO
CREATE TABLE [dbo].[BookoutSources]
(
[BookoutSourceID] [tinyint] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BookoutSources] on [dbo].[BookoutSources]'
GO
ALTER TABLE [dbo].[BookoutSources] ADD CONSTRAINT [PK_BookoutSources] PRIMARY KEY CLUSTERED  ([BookoutSourceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BookoutValueTypes]'
GO
CREATE TABLE [dbo].[BookoutValueTypes]
(
[BookoutValueTypeID] [tinyint] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BookoutValueTypes] on [dbo].[BookoutValueTypes]'
GO
ALTER TABLE [dbo].[BookoutValueTypes] ADD CONSTRAINT [PK_BookoutValueTypes] PRIMARY KEY CLUSTERED  ([BookoutValueTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ThirdPartySubCategory]'
GO
CREATE TABLE [dbo].[ThirdPartySubCategory]
(
[ThirdPartySubCategoryID] [int] NOT NULL IDENTITY(1, 1),
[ThirdPartyCategoryID] [int] NOT NULL,
[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ThirdPartySubCategory] on [dbo].[ThirdPartySubCategory]'
GO
ALTER TABLE [dbo].[ThirdPartySubCategory] ADD CONSTRAINT [PK_ThirdPartySubCategory] PRIMARY KEY CLUSTERED  ([ThirdPartySubCategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BuyingAlertsStatus]'
GO
CREATE TABLE [dbo].[BuyingAlertsStatus]
(
[BuyingAlertsStatusId] [tinyint] NOT NULL,
[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BuyingAlertsStatus] on [dbo].[BuyingAlertsStatus]'
GO
ALTER TABLE [dbo].[BuyingAlertsStatus] ADD CONSTRAINT [PK_BuyingAlertsStatus] PRIMARY KEY CLUSTERED  ([BuyingAlertsStatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[AccountStatus]'
GO
CREATE TABLE [Carfax].[AccountStatus]
(
[AccountStatusID] [tinyint] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_AccountStatus] on [Carfax].[AccountStatus]'
GO
ALTER TABLE [Carfax].[AccountStatus] ADD CONSTRAINT [PK_Carfax_AccountStatus] PRIMARY KEY CLUSTERED  ([AccountStatusID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UK_Carfax_AccountStatus__Name] on [Carfax].[AccountStatus]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_Carfax_AccountStatus__Name] ON [Carfax].[AccountStatus] ([Name])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CIABucketRuleTypes]'
GO
CREATE TABLE [dbo].[CIABucketRuleTypes]
(
[CIABucketRuleTypeID] [tinyint] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ColumnName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_CIABucketRuleTypes] on [dbo].[CIABucketRuleTypes]'
GO
ALTER TABLE [dbo].[CIABucketRuleTypes] ADD CONSTRAINT [PK_CIABucketRuleTypes] PRIMARY KEY CLUSTERED  ([CIABucketRuleTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CIAGroupingItemDetailLevels]'
GO
CREATE TABLE [dbo].[CIAGroupingItemDetailLevels]
(
[CIAGroupingItemDetailLevelID] [tinyint] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_CIAGroupingItemDetailLevel] on [dbo].[CIAGroupingItemDetailLevels]'
GO
ALTER TABLE [dbo].[CIAGroupingItemDetailLevels] ADD CONSTRAINT [PK_CIAGroupingItemDetailLevel] PRIMARY KEY NONCLUSTERED  ([CIAGroupingItemDetailLevelID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CIAGroupingItemDetailTypes]'
GO
CREATE TABLE [dbo].[CIAGroupingItemDetailTypes]
(
[CIAGroupingItemDetailTypeID] [tinyint] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_CIAGroupingItemDetailTypes] on [dbo].[CIAGroupingItemDetailTypes]'
GO
ALTER TABLE [dbo].[CIAGroupingItemDetailTypes] ADD CONSTRAINT [PK_CIAGroupingItemDetailTypes] PRIMARY KEY CLUSTERED  ([CIAGroupingItemDetailTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CIACategories]'
GO
CREATE TABLE [dbo].[CIACategories]
(
[CIACategoryID] [tinyint] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_CIACategory] on [dbo].[CIACategories]'
GO
ALTER TABLE [dbo].[CIACategories] ADD CONSTRAINT [PK_CIACategory] PRIMARY KEY NONCLUSTERED  ([CIACategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CIASummaryStatus]'
GO
CREATE TABLE [dbo].[CIASummaryStatus]
(
[CIASummaryStatusID] [tinyint] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_CIASummaryStatus] on [dbo].[CIASummaryStatus]'
GO
ALTER TABLE [dbo].[CIASummaryStatus] ADD CONSTRAINT [PK_CIASummaryStatus] PRIMARY KEY CLUSTERED  ([CIASummaryStatusID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerInternetAdvertisementBuilderOptionProvider]'
GO
CREATE TABLE [dbo].[DealerInternetAdvertisementBuilderOptionProvider]
(
[BusinessUnitID] [int] NOT NULL,
[InternetAdvertisementBuilderOptionProviderID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DealerInternetAdvertisementBuilderOptionProvider] on [dbo].[DealerInternetAdvertisementBuilderOptionProvider]'
GO
ALTER TABLE [dbo].[DealerInternetAdvertisementBuilderOptionProvider] ADD CONSTRAINT [PK_DealerInternetAdvertisementBuilderOptionProvider] PRIMARY KEY CLUSTERED  ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InternetAdvertisementBuilderOptionProvider]'
GO
CREATE TABLE [dbo].[InternetAdvertisementBuilderOptionProvider]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InternetAdvertisementBuilderOptionProvider] on [dbo].[InternetAdvertisementBuilderOptionProvider]'
GO
ALTER TABLE [dbo].[InternetAdvertisementBuilderOptionProvider] ADD CONSTRAINT [PK_InternetAdvertisementBuilderOptionProvider] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_DealerPackType]'
GO
CREATE TABLE [dbo].[lu_DealerPackType]
(
[PackTypeCD] [tinyint] NOT NULL IDENTITY(1, 1),
[PackTypeDESC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_DealerPackType] on [dbo].[lu_DealerPackType]'
GO
ALTER TABLE [dbo].[lu_DealerPackType] ADD CONSTRAINT [PK_lu_DealerPackType] PRIMARY KEY CLUSTERED  ([PackTypeCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_ProgramType]'
GO
CREATE TABLE [dbo].[lu_ProgramType]
(
[ProgramType_CD] [tinyint] NOT NULL,
[ProgramTypeName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_ProgramType] on [dbo].[lu_ProgramType]'
GO
ALTER TABLE [dbo].[lu_ProgramType] ADD CONSTRAINT [PK_lu_ProgramType] PRIMARY KEY NONCLUSTERED  ([ProgramType_CD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerSalesChannel]'
GO
CREATE TABLE [dbo].[DealerSalesChannel]
(
[BusinessUnitID] [int] NOT NULL,
[SalesChannelID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DealerSalesChannel] on [dbo].[DealerSalesChannel]'
GO
ALTER TABLE [dbo].[DealerSalesChannel] ADD CONSTRAINT [PK_DealerSalesChannel] PRIMARY KEY CLUSTERED  ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_DealerSalesChannel_SalesChannelID] on [dbo].[DealerSalesChannel]'
GO
CREATE NONCLUSTERED INDEX [IX_DealerSalesChannel_SalesChannelID] ON [dbo].[DealerSalesChannel] ([SalesChannelID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SalesChannel]'
GO
CREATE TABLE [dbo].[SalesChannel]
(
[SalesChannelID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SalesChannel] on [dbo].[SalesChannel]'
GO
ALTER TABLE [dbo].[SalesChannel] ADD CONSTRAINT [PK_SalesChannel] PRIMARY KEY CLUSTERED  ([SalesChannelID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[TransferPriceUnitCostValue]'
GO
CREATE TABLE [dbo].[TransferPriceUnitCostValue]
(
[TransferPriceUnitCostValueID] [int] NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_TransferPriceUnitCostValue] on [dbo].[TransferPriceUnitCostValue]'
GO
ALTER TABLE [dbo].[TransferPriceUnitCostValue] ADD CONSTRAINT [PK_TransferPriceUnitCostValue] PRIMARY KEY CLUSTERED  ([TransferPriceUnitCostValueID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[AutoTrader_ErrorType]'
GO
CREATE TABLE [Distribution].[AutoTrader_ErrorType]
(
[ErrorTypeID] [int] NOT NULL IDENTITY(0, 1),
[Description] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_AutoTrader_ErrorType] on [Distribution].[AutoTrader_ErrorType]'
GO
ALTER TABLE [Distribution].[AutoTrader_ErrorType] ADD CONSTRAINT [PK_Distribution_AutoTrader_ErrorType] PRIMARY KEY CLUSTERED  ([ErrorTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[AutoUplink_ErrorType]'
GO
CREATE TABLE [Distribution].[AutoUplink_ErrorType]
(
[ErrorTypeID] [int] NOT NULL IDENTITY(0, 1),
[Description] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_AutoUplink_ErrorType] on [Distribution].[AutoUplink_ErrorType]'
GO
ALTER TABLE [Distribution].[AutoUplink_ErrorType] ADD CONSTRAINT [PK_Distribution_AutoUplink_ErrorType] PRIMARY KEY CLUSTERED  ([ErrorTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[ContentType]'
GO
CREATE TABLE [Distribution].[ContentType]
(
[ContentTypeID] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_ContentType] on [Distribution].[ContentType]'
GO
ALTER TABLE [Distribution].[ContentType] ADD CONSTRAINT [PK_Distribution_ContentType] PRIMARY KEY CLUSTERED  ([ContentTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[PriceType]'
GO
CREATE TABLE [Distribution].[PriceType]
(
[PriceTypeID] [int] NOT NULL IDENTITY(0, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_PriceType] on [Distribution].[PriceType]'
GO
ALTER TABLE [Distribution].[PriceType] ADD CONSTRAINT [PK_Distribution_PriceType] PRIMARY KEY CLUSTERED  ([PriceTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[PublicationStatus]'
GO
CREATE TABLE [Distribution].[PublicationStatus]
(
[PublicationStatusID] [int] NOT NULL IDENTITY(0, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_PublicationStatus] on [Distribution].[PublicationStatus]'
GO
ALTER TABLE [Distribution].[PublicationStatus] ADD CONSTRAINT [PK_Distribution_PublicationStatus] PRIMARY KEY CLUSTERED  ([PublicationStatusID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[SubmissionStatus]'
GO
CREATE TABLE [Distribution].[SubmissionStatus]
(
[SubmissionStatusID] [int] NOT NULL IDENTITY(0, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_SubmissionStatus] on [Distribution].[SubmissionStatus]'
GO
ALTER TABLE [Distribution].[SubmissionStatus] ADD CONSTRAINT [PK_Distribution_SubmissionStatus] PRIMARY KEY CLUSTERED  ([SubmissionStatusID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[VehicleEntityType]'
GO
CREATE TABLE [Distribution].[VehicleEntityType]
(
[VehicleEntityTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_VehicleEntityType] on [Distribution].[VehicleEntityType]'
GO
ALTER TABLE [Distribution].[VehicleEntityType] ADD CONSTRAINT [PK_Distribution_VehicleEntityType] PRIMARY KEY CLUSTERED  ([VehicleEntityTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DMSExportFrequency]'
GO
CREATE TABLE [dbo].[DMSExportFrequency]
(
[DMSExportFrequencyID] [tinyint] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__DMSExportFrequen__6F37D48D] on [dbo].[DMSExportFrequency]'
GO
ALTER TABLE [dbo].[DMSExportFrequency] ADD CONSTRAINT [PK__DMSExportFrequen__6F37D48D] PRIMARY KEY CLUSTERED  ([DMSExportFrequencyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DMSExportPriceMapping]'
GO
CREATE TABLE [dbo].[DMSExportPriceMapping]
(
[DMSExportPriceMappingID] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__DMSExportPriceMa__71201CFF] on [dbo].[DMSExportPriceMapping]'
GO
ALTER TABLE [dbo].[DMSExportPriceMapping] ADD CONSTRAINT [PK__DMSExportPriceMa__71201CFF] PRIMARY KEY CLUSTERED  ([DMSExportPriceMappingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[JDPower_PowerRegion]'
GO
CREATE TABLE [dbo].[JDPower_PowerRegion]
(
[PowerRegionID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (240) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_JDPower_PowerRegion_PowerRegionID] on [dbo].[JDPower_PowerRegion]'
GO
ALTER TABLE [dbo].[JDPower_PowerRegion] ADD CONSTRAINT [PK_JDPower_PowerRegion_PowerRegionID] PRIMARY KEY CLUSTERED  ([PowerRegionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[WindowStickerBuyersGuideTemplate]'
GO
CREATE TABLE [dbo].[WindowStickerBuyersGuideTemplate]
(
[WindowStickerBuyersGuideTemplateId] [int] NOT NULL,
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_WindowStickerBuyersGuideTemplate] on [dbo].[WindowStickerBuyersGuideTemplate]'
GO
ALTER TABLE [dbo].[WindowStickerBuyersGuideTemplate] ADD CONSTRAINT [PK_WindowStickerBuyersGuideTemplate] PRIMARY KEY CLUSTERED  ([WindowStickerBuyersGuideTemplateId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[WindowStickerTemplate]'
GO
CREATE TABLE [dbo].[WindowStickerTemplate]
(
[WindowStickerTemplateId] [int] NOT NULL IDENTITY(1, 1),
[WindowStickerTemplateName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_WindowStickerTemplate] on [dbo].[WindowStickerTemplate]'
GO
ALTER TABLE [dbo].[WindowStickerTemplate] ADD CONSTRAINT [PK_WindowStickerTemplate] PRIMARY KEY CLUSTERED  ([WindowStickerTemplateId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FLUSAN_EventType]'
GO
CREATE TABLE [dbo].[FLUSAN_EventType]
(
[FLUSAN_EventTypeCode] [tinyint] NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_FLUSAN_EventTypes] on [dbo].[FLUSAN_EventType]'
GO
ALTER TABLE [dbo].[FLUSAN_EventType] ADD CONSTRAINT [PK_FLUSAN_EventTypes] PRIMARY KEY CLUSTERED  ([FLUSAN_EventTypeCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FranchiseAlias]'
GO
CREATE TABLE [dbo].[FranchiseAlias]
(
[FranchiseAliasID] [int] NOT NULL IDENTITY(1, 1),
[FranchiseID] [int] NOT NULL,
[FranchiseAlias] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_FranchiseAlias] on [dbo].[FranchiseAlias]'
GO
ALTER TABLE [dbo].[FranchiseAlias] ADD CONSTRAINT [PK_FranchiseAlias] PRIMARY KEY NONCLUSTERED  ([FranchiseAliasID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InsightTargetType]'
GO
CREATE TABLE [dbo].[InsightTargetType]
(
[InsightTargetTypeID] [int] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InsightTargetType] on [dbo].[InsightTargetType]'
GO
ALTER TABLE [dbo].[InsightTargetType] ADD CONSTRAINT [PK_InsightTargetType] PRIMARY KEY CLUSTERED  ([InsightTargetTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InsightTargetValueType]'
GO
CREATE TABLE [dbo].[InsightTargetValueType]
(
[InsightTargetValueTypeID] [int] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InsightTargetValueType] on [dbo].[InsightTargetValueType]'
GO
ALTER TABLE [dbo].[InsightTargetValueType] ADD CONSTRAINT [PK_InsightTargetValueType] PRIMARY KEY CLUSTERED  ([InsightTargetValueTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InsightCategory]'
GO
CREATE TABLE [dbo].[InsightCategory]
(
[InsightCategoryID] [int] NOT NULL,
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InsightCategory] on [dbo].[InsightCategory]'
GO
ALTER TABLE [dbo].[InsightCategory] ADD CONSTRAINT [PK_InsightCategory] PRIMARY KEY CLUSTERED  ([InsightCategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InternetAdvertiserExportStatus]'
GO
CREATE TABLE [dbo].[InternetAdvertiserExportStatus]
(
[ExportStatusCD] [tinyint] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ExportStatus] on [dbo].[InternetAdvertiserExportStatus]'
GO
ALTER TABLE [dbo].[InternetAdvertiserExportStatus] ADD CONSTRAINT [PK_ExportStatus] PRIMARY KEY CLUSTERED  ([ExportStatusCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[JDPower_DealerMarketArea]'
GO
CREATE TABLE [dbo].[JDPower_DealerMarketArea]
(
[DealerMarketAreaID] [int] NOT NULL IDENTITY(1, 1),
[PowerRegionID] [int] NOT NULL,
[Name] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_JDPower_DealerMarketArea_DealerMarketAreaID] on [dbo].[JDPower_DealerMarketArea]'
GO
ALTER TABLE [dbo].[JDPower_DealerMarketArea] ADD CONSTRAINT [PK_JDPower_DealerMarketArea_DealerMarketAreaID] PRIMARY KEY CLUSTERED  ([DealerMarketAreaID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[KBBArchiveDatasetOverride]'
GO
CREATE TABLE [dbo].[KBBArchiveDatasetOverride]
(
[KBBArchiveDatasetOverrideID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[MemberID] [int] NOT NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[Active] AS (CONVERT([tinyint],case when getdate()>=[StartDate] AND getdate()<=[EndDate] then (1) else (0) end,0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[MarketplaceSubmissionStatusCode]'
GO
CREATE TABLE [dbo].[MarketplaceSubmissionStatusCode]
(
[MarketplaceSubmissionStatusCD] [tinyint] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MarketplaceSubmissionStatusCode] on [dbo].[MarketplaceSubmissionStatusCode]'
GO
ALTER TABLE [dbo].[MarketplaceSubmissionStatusCode] ADD CONSTRAINT [PK_MarketplaceSubmissionStatusCode] PRIMARY KEY CLUSTERED  ([MarketplaceSubmissionStatusCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[HomePage]'
GO
CREATE TABLE [dbo].[HomePage]
(
[HomePageID] [tinyint] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_HomePage_HomePageRegionID] on [dbo].[HomePage]'
GO
ALTER TABLE [dbo].[HomePage] ADD CONSTRAINT [PK_HomePage_HomePageRegionID] PRIMARY KEY CLUSTERED  ([HomePageID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_UserRole]'
GO
CREATE TABLE [dbo].[lu_UserRole]
(
[UserRoleCD] [tinyint] NOT NULL,
[UserRoleDesc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_UserRole] on [dbo].[lu_UserRole]'
GO
ALTER TABLE [dbo].[lu_UserRole] ADD CONSTRAINT [PK_lu_UserRole] PRIMARY KEY NONCLUSTERED  ([UserRoleCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tbl_MemberATCAccessGroupListType]'
GO
CREATE TABLE [dbo].[tbl_MemberATCAccessGroupListType]
(
[MemberATCAccessGroupListTypeID] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MemberATCAccessGroupListType] on [dbo].[tbl_MemberATCAccessGroupListType]'
GO
ALTER TABLE [dbo].[tbl_MemberATCAccessGroupListType] ADD CONSTRAINT [PK_MemberATCAccessGroupListType] PRIMARY KEY NONCLUSTERED  ([MemberATCAccessGroupListTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[MemberBusinessUnitSetType]'
GO
CREATE TABLE [dbo].[MemberBusinessUnitSetType]
(
[MemberBusinessUnitSetTypeID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MemberBusinessUnitSetType] on [dbo].[MemberBusinessUnitSetType]'
GO
ALTER TABLE [dbo].[MemberBusinessUnitSetType] ADD CONSTRAINT [PK_MemberBusinessUnitSetType] PRIMARY KEY CLUSTERED  ([MemberBusinessUnitSetTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[MemberCredential]'
GO
CREATE TABLE [dbo].[MemberCredential]
(
[MemberID] [int] NOT NULL,
[CredentialTypeID] [tinyint] NOT NULL,
[Username] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MemberCredentialID] [int] NOT NULL IDENTITY(1, 1)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MemberCredential] on [dbo].[MemberCredential]'
GO
ALTER TABLE [dbo].[MemberCredential] ADD CONSTRAINT [PK_MemberCredential] PRIMARY KEY CLUSTERED  ([MemberCredentialID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[License]'
GO
CREATE TABLE [dbo].[License]
(
[LicenseID] [int] NOT NULL IDENTITY(1, 1),
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__License__5A3CB7A7] on [dbo].[License]'
GO
ALTER TABLE [dbo].[License] ADD CONSTRAINT [PK__License__5A3CB7A7] PRIMARY KEY CLUSTERED  ([LicenseID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Role]'
GO
CREATE TABLE [dbo].[Role]
(
[RoleID] [int] NOT NULL,
[Type] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Code] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Role] on [dbo].[Role]'
GO
ALTER TABLE [dbo].[Role] ADD CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED  ([RoleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[map_MemberToInvStatusFilter]'
GO
CREATE TABLE [dbo].[map_MemberToInvStatusFilter]
(
[MemberToInvStatusFilterId] [int] NOT NULL IDENTITY(1, 1),
[MemberID] [int] NOT NULL,
[InventoryStatusCD] [smallint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[NADARegions]'
GO
CREATE TABLE [dbo].[NADARegions]
(
[NADARegionCode] [tinyint] NOT NULL,
[NADARegionName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_NADARegions] on [dbo].[NADARegions]'
GO
ALTER TABLE [dbo].[NADARegions] ADD CONSTRAINT [PK_NADARegions] PRIMARY KEY CLUSTERED  ([NADARegionCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DataSource]'
GO
CREATE TABLE [dbo].[DataSource]
(
[DataSourceID] [int] NOT NULL IDENTITY(1, 1),
[DataSource] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DataSource] on [dbo].[DataSource]'
GO
ALTER TABLE [dbo].[DataSource] ADD CONSTRAINT [PK_DataSource] PRIMARY KEY NONCLUSTERED  ([DataSourceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Position]'
GO
CREATE TABLE [dbo].[Position]
(
[PositionID] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Position] on [dbo].[Position]'
GO
ALTER TABLE [dbo].[Position] ADD CONSTRAINT [PK_Position] PRIMARY KEY CLUSTERED  ([PositionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PhotoStorage]'
GO
CREATE TABLE [dbo].[PhotoStorage]
(
[PhotoStorageCode] [tinyint] NOT NULL,
[Description] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PhotoStorage] on [dbo].[PhotoStorage]'
GO
ALTER TABLE [dbo].[PhotoStorage] ADD CONSTRAINT [PK_PhotoStorage] PRIMARY KEY CLUSTERED  ([PhotoStorageCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PhotoStatus]'
GO
CREATE TABLE [dbo].[PhotoStatus]
(
[PhotoStatusCode] [tinyint] NOT NULL,
[Description] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PhotoStatus] on [dbo].[PhotoStatus]'
GO
ALTER TABLE [dbo].[PhotoStatus] ADD CONSTRAINT [PK_PhotoStatus] PRIMARY KEY CLUSTERED  ([PhotoStatusCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PING_Provider]'
GO
CREATE TABLE [dbo].[PING_Provider]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[name] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[code] [nvarchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[available] [bit] NOT NULL,
[sortOrder] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PING_Provider] on [dbo].[PING_Provider]'
GO
ALTER TABLE [dbo].[PING_Provider] ADD CONSTRAINT [PK_PING_Provider] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PING_Code]'
GO
CREATE TABLE [dbo].[PING_Code]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[name] [nvarchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[value] [nvarchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[providerId] [int] NOT NULL,
[parentId] [int] NULL,
[type] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PING_Code] on [dbo].[PING_Code]'
GO
ALTER TABLE [dbo].[PING_Code] ADD CONSTRAINT [PK_PING_Code] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_PING_Code__ProviderId_ParentId_Id] on [dbo].[PING_Code]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_PING_Code__ProviderId_ParentId_Id] ON [dbo].[PING_Code] ([providerId], [parentId], [id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PrintAdvertiserTextOption]'
GO
CREATE TABLE [dbo].[PrintAdvertiserTextOption]
(
[PrintAdvertiserTextOptionID] [tinyint] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PrintAdvertiserTextOption] on [dbo].[PrintAdvertiserTextOption]'
GO
ALTER TABLE [dbo].[PrintAdvertiserTextOption] ADD CONSTRAINT [PK_PrintAdvertiserTextOption] PRIMARY KEY CLUSTERED  ([PrintAdvertiserTextOptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SearchCandidateListType]'
GO
CREATE TABLE [dbo].[SearchCandidateListType]
(
[SearchCandidateListTypeID] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SearchCandidateListType] on [dbo].[SearchCandidateListType]'
GO
ALTER TABLE [dbo].[SearchCandidateListType] ADD CONSTRAINT [PK_SearchCandidateListType] PRIMARY KEY NONCLUSTERED  ([SearchCandidateListTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SearchCandidateOptionAnnotationType]'
GO
CREATE TABLE [dbo].[SearchCandidateOptionAnnotationType]
(
[SearchCandidateOptionAnnotationTypeID] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SearchCandidateOptionAnnotationType] on [dbo].[SearchCandidateOptionAnnotationType]'
GO
ALTER TABLE [dbo].[SearchCandidateOptionAnnotationType] ADD CONSTRAINT [PK_SearchCandidateOptionAnnotationType] PRIMARY KEY NONCLUSTERED  ([SearchCandidateOptionAnnotationTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SoftwareSystemComponent]'
GO
CREATE TABLE [dbo].[SoftwareSystemComponent]
(
[SoftwareSystemComponentID] [int] NOT NULL,
[ParentID] [int] NULL,
[Name] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Token] [varchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SoftwareSystemComponent] on [dbo].[SoftwareSystemComponent]'
GO
ALTER TABLE [dbo].[SoftwareSystemComponent] ADD CONSTRAINT [PK_SoftwareSystemComponent] PRIMARY KEY CLUSTERED  ([SoftwareSystemComponentID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ThirdPartyOptionTypes]'
GO
CREATE TABLE [dbo].[ThirdPartyOptionTypes]
(
[ThirdPartyOptionTypeID] [tinyint] NOT NULL IDENTITY(1, 1),
[OptionTypeName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ThirdPartyVehicleOptionTypes_1] on [dbo].[ThirdPartyOptionTypes]'
GO
ALTER TABLE [dbo].[ThirdPartyOptionTypes] ADD CONSTRAINT [PK_ThirdPartyVehicleOptionTypes_1] PRIMARY KEY CLUSTERED  ([ThirdPartyOptionTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SubscriptionFrequency]'
GO
CREATE TABLE [dbo].[SubscriptionFrequency]
(
[SubscriptionFrequencyID] [tinyint] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SubscriptionFrequencyTypes] on [dbo].[SubscriptionFrequency]'
GO
ALTER TABLE [dbo].[SubscriptionFrequency] ADD CONSTRAINT [PK_SubscriptionFrequencyTypes] PRIMARY KEY CLUSTERED  ([SubscriptionFrequencyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SubscriberTypes]'
GO
CREATE TABLE [dbo].[SubscriberTypes]
(
[SubscriberTypeID] [tinyint] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SubscriberTypes] on [dbo].[SubscriberTypes]'
GO
ALTER TABLE [dbo].[SubscriberTypes] ADD CONSTRAINT [PK_SubscriberTypes] PRIMARY KEY CLUSTERED  ([SubscriberTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SubscriptionDeliveryTypes]'
GO
CREATE TABLE [dbo].[SubscriptionDeliveryTypes]
(
[SubscriptionDeliveryTypeID] [tinyint] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SubscriptionDeliveryTypes] on [dbo].[SubscriptionDeliveryTypes]'
GO
ALTER TABLE [dbo].[SubscriptionDeliveryTypes] ADD CONSTRAINT [PK_SubscriptionDeliveryTypes] PRIMARY KEY CLUSTERED  ([SubscriptionDeliveryTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SubscriptionTypeSubscriptionDeliveryType]'
GO
CREATE TABLE [dbo].[SubscriptionTypeSubscriptionDeliveryType]
(
[SubscriptionTypeSubscriptionDeliveryTypeID] [tinyint] NOT NULL IDENTITY(1, 1),
[SubscriptionTypeID] [tinyint] NOT NULL,
[SubscriptionDeliveryTypeID] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SubscriptionTypeSubscriptionDeliveryType] on [dbo].[SubscriptionTypeSubscriptionDeliveryType]'
GO
ALTER TABLE [dbo].[SubscriptionTypeSubscriptionDeliveryType] ADD CONSTRAINT [PK_SubscriptionTypeSubscriptionDeliveryType] PRIMARY KEY CLUSTERED  ([SubscriptionTypeSubscriptionDeliveryTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SubscriptionTypeSubscriptionFrequencyDetail]'
GO
CREATE TABLE [dbo].[SubscriptionTypeSubscriptionFrequencyDetail]
(
[SubscriptionTypeSubscriptionFrequencyDetailID] [tinyint] NOT NULL IDENTITY(1, 1),
[SubscriptionTypeID] [tinyint] NOT NULL,
[SubscriptionFrequencyDetailID] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SubscriptionTypeSubscriptionFrequencyDetail] on [dbo].[SubscriptionTypeSubscriptionFrequencyDetail]'
GO
ALTER TABLE [dbo].[SubscriptionTypeSubscriptionFrequencyDetail] ADD CONSTRAINT [PK_SubscriptionTypeSubscriptionFrequencyDetail] PRIMARY KEY CLUSTERED  ([SubscriptionTypeSubscriptionFrequencyDetailID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[TargetType]'
GO
CREATE TABLE [dbo].[TargetType]
(
[TargetTypeCD] [tinyint] NOT NULL,
[TargetTypeName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_TargetType] on [dbo].[TargetType]'
GO
ALTER TABLE [dbo].[TargetType] ADD CONSTRAINT [PK_TargetType] PRIMARY KEY NONCLUSTERED  ([TargetTypeCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_RedistributionSource]'
GO
CREATE TABLE [dbo].[lu_RedistributionSource]
(
[RedistributionSourceId] [tinyint] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_RedistributionSource] on [dbo].[lu_RedistributionSource]'
GO
ALTER TABLE [dbo].[lu_RedistributionSource] ADD CONSTRAINT [PK_lu_RedistributionSource] PRIMARY KEY NONCLUSTERED  ([RedistributionSourceId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[VehicleSources]'
GO
CREATE TABLE [dbo].[VehicleSources]
(
[VehicleSourceID] [tinyint] NOT NULL IDENTITY(1, 1),
[Description] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_VehicleSources] on [dbo].[VehicleSources]'
GO
ALTER TABLE [dbo].[VehicleSources] ADD CONSTRAINT [PK_VehicleSources] PRIMARY KEY CLUSTERED  ([VehicleSourceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_CustomerType]'
GO
CREATE TABLE [dbo].[lu_CustomerType]
(
[CustomerTypeCD] [tinyint] NOT NULL,
[CustomerTypeDESC] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_CustomerType] on [dbo].[lu_CustomerType]'
GO
ALTER TABLE [dbo].[lu_CustomerType] ADD CONSTRAINT [PK_lu_CustomerType] PRIMARY KEY NONCLUSTERED  ([CustomerTypeCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ThirdPartyVehicleOptionValueTypes]'
GO
CREATE TABLE [dbo].[ThirdPartyVehicleOptionValueTypes]
(
[ThirdPartyOptionValueTypeID] [tinyint] NOT NULL IDENTITY(1, 1),
[OptionValueTypeName] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ThirdPartyVehicleOptionTypes] on [dbo].[ThirdPartyVehicleOptionValueTypes]'
GO
ALTER TABLE [dbo].[ThirdPartyVehicleOptionValueTypes] ADD CONSTRAINT [PK_ThirdPartyVehicleOptionTypes] PRIMARY KEY CLUSTERED  ([ThirdPartyOptionValueTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AppraisalActions]'
GO
CREATE TABLE [dbo].[AppraisalActions]
(
[AppraisalActionID] [int] NOT NULL IDENTITY(1, 1),
[AppraisalID] [int] NOT NULL,
[AppraisalActionTypeID] [tinyint] NOT NULL,
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_AppraisalActions_DateCreated] DEFAULT (getdate()),
[WholesalePrice] [decimal] (12, 2) NULL,
[EstimatedReconditioningCost] [decimal] (12, 2) NULL,
[ConditionDescription] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransferPrice] [decimal] (9, 2) NULL,
[TransferForRetailOnly] [bit] NOT NULL CONSTRAINT [DF__Appraisal__Trans__01EB81A5] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AppraisalActions] on [dbo].[AppraisalActions]'
GO
ALTER TABLE [dbo].[AppraisalActions] ADD CONSTRAINT [PK_AppraisalActions] PRIMARY KEY CLUSTERED  ([AppraisalActionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Appraisals]'
GO
CREATE TABLE [dbo].[Appraisals]
(
[AppraisalID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[VehicleID] [int] NOT NULL,
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_Appraisals_DateCreated] DEFAULT (getdate()),
[MemberID] [int] NULL,
[DealCompleted] [tinyint] NULL,
[Sold] [tinyint] NULL,
[Color] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Mileage] [int] NOT NULL CONSTRAINT [DF__Appraisal__Milea__1705036E] DEFAULT (0),
[DealTrackStockNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DealTrackNewOrUsed] [tinyint] NULL CONSTRAINT [DF__Appraisal__DealT__18ED4BE0] DEFAULT (0),
[DateLocked] [smalldatetime] NULL,
[Locked] AS (convert(tinyint,case when ([DateLocked] <= getdate()) then 1 else 0 end)),
[DateModified] [smalldatetime] NOT NULL CONSTRAINT [DF_Appraisals__DateModified] DEFAULT (getdate()),
[DealTrackSalespersonID] [int] NULL,
[AppraisalSourceID] [tinyint] NOT NULL CONSTRAINT [DF_Appraisals_AppraisalSourceID] DEFAULT (1),
[EdmundsTMV] [decimal] (8, 2) NULL,
[AppraisalStatusID] [tinyint] NOT NULL CONSTRAINT [DF_Appraisal_AppraisalStatus] DEFAULT ((1)),
[AppraisalTypeID] [int] NOT NULL CONSTRAINT [DF__Appraisal__Appra__7C9CBCA3] DEFAULT ((1)),
[SelectedBuyerId] [int] NULL CONSTRAINT [DF__Appraisal__Selec__7E850515] DEFAULT (NULL)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Appraisals] on [dbo].[Appraisals]'
GO
ALTER TABLE [dbo].[Appraisals] ADD CONSTRAINT [PK_Appraisals] PRIMARY KEY CLUSTERED  ([AppraisalID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerPreference]'
GO
CREATE TABLE [dbo].[DealerPreference]
(
[DealerPreferenceID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[CustomHomePageMessage] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DashboardColumnDisplayPreference] [int] NOT NULL,
[UnitsSoldThreshold4Wks] [int] NOT NULL,
[UnitsSoldThreshold8Wks] [int] NOT NULL,
[UnitsSoldThreshold13Wks] [int] NOT NULL,
[UnitsSoldThreshold26Wks] [int] NOT NULL,
[UnitsSoldThreshold52Wks] [int] NOT NULL,
[DefaultTrendingView] [int] NULL,
[DefaultTrendingWeeks] [int] NULL,
[DefaultForecastingWeeks] [int] NULL,
[GuideBookID] [tinyint] NOT NULL,
[NADARegionCode] [int] NOT NULL,
[InceptionDate] [datetime] NULL,
[AgingInventoryTrackingDisplayPref] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_AgingInventoryTrackingDisplayPref] DEFAULT ((1)),
[PackAmount] [decimal] (8, 2) NOT NULL,
[StockOrVinPreference] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_StockOrVinPreference] DEFAULT ((1)),
[MaxSalesHistoryDate] [datetime] NULL,
[UnitCostOrListPricePreference] [tinyint] NULL,
[ListPricePreference] [tinyint] NULL,
[AgeBandTarget1] [int] NULL,
[AgeBandTarget2] [int] NULL,
[AgeBandTarget3] [int] NULL,
[AgeBandTarget4] [int] NULL,
[AgeBandTarget5] [int] NULL,
[AgeBandTarget6] [int] NULL,
[DaysSupply12WeekWeight] [int] NULL,
[DaysSupply26WeekWeight] [int] NULL,
[BookOut] [tinyint] NOT NULL,
[BookOutPreferenceId] [int] NOT NULL,
[UnitCostThreshold] [decimal] (10, 2) NOT NULL CONSTRAINT [DF_DealerPreference_UnitCostThreshold] DEFAULT (1000),
[BookOutSecondPreferenceId] [int] NULL,
[GuideBook2Id] [tinyint] NULL,
[GuideBook2BookOutPreferenceId] [int] NULL,
[GuideBook2SecondBookOutPreferenceId] [int] NULL,
[UnitCostUpdateOnSale] [bit] NOT NULL CONSTRAINT [DF_DealerPreference_UnitCostUpdateOnSale] DEFAULT (1),
[UnwindDaysThreshold] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_UnwindDaysThreshold] DEFAULT (45),
[AuctionAreaId] [int] NOT NULL CONSTRAINT [DF_DealerPreference_AuctionAreaID] DEFAULT ((1)),
[showLotLocationStatus] [tinyint] NULL,
[PopulateClassName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BookOutPreferenceSecondId] [int] NULL,
[RunDayOfWeek] [int] NOT NULL,
[UnitCostThresholdLower] [int] NULL,
[UnitCostThresholdUpper] [int] NULL,
[FEGrossProfitThreshold] [int] NULL CONSTRAINT [DF__DealerPre__FEGro__2CC890AD] DEFAULT ((-1000000)),
[MarginPercentile] [smallint] NULL,
[DaysToSalePercentile] [smallint] NULL,
[ProgramType_CD] [tinyint] NULL,
[SellThroughRate] [int] NOT NULL CONSTRAINT [DF__DealerPre__SellT__53E25DCE] DEFAULT (90),
[IncludeBackEndInValuation] [tinyint] NOT NULL CONSTRAINT [DF__DealerPre__Inclu__54D68207] DEFAULT (1),
[UnitsSoldThresholdInvOverview] [int] NULL CONSTRAINT [DF_DealerPreference_UnitsSoldThresholdInvOverview] DEFAULT (6),
[CIATargetDaysSupply] [int] NULL,
[CIAMarketPerformerInStockThreshold] [int] NULL,
[GoLiveDate] [smalldatetime] NULL,
[VehicleSaleThresholdForCoreMarketPenetration] [float] NULL,
[ATCEnabled] [tinyint] NULL,
[ApplyPriorAgingNotes] [bit] NOT NULL CONSTRAINT [DF_DealerPreference_ApplyPriorAgingNotes] DEFAULT (1),
[averageInventoryAgeRedThreshold] [int] NULL CONSTRAINT [DF_DealerPreference_AverageInventoryAgeRedThreshold] DEFAULT (30),
[averageDaysSupplyRedThreshold] [int] NULL CONSTRAINT [DF_DealerPreference_AverageDaysSupplyRedThreshold] DEFAULT (50),
[PurchasingDistanceFromDealer] [int] NOT NULL CONSTRAINT [DF_DealerPreference_PurchasingDistanceFromDealer] DEFAULT (1000),
[DisplayUnitCostToDealerGroup] [tinyint] NOT NULL,
[UnitsSoldThreshold12Wks] [int] NULL CONSTRAINT [DF_DealerPreference_UnitsSoldThreshold12Wks] DEFAULT (1),
[SearchInactiveInventoryDaysBackThreshold] [int] NOT NULL CONSTRAINT [DF_DealerPreference_SearchInactiveInventoryDaysBackThreshold] DEFAULT (90),
[CalculateAverageBookValue] [tinyint] NOT NULL CONSTRAINT [DF__DealerPre__Calcu__1F5A524D] DEFAULT (1),
[CarfaxListVINsOnWebsite] [tinyint] NOT NULL CONSTRAINT [DF__DealerPre__Carfa__4A0FA628] DEFAULT (0),
[TFSEnabled] [tinyint] NOT NULL CONSTRAINT [DF__DealerPre__TFSEn__143DA55C] DEFAULT (0),
[GMACEnabled] [tinyint] NOT NULL CONSTRAINT [DF__DealerPre__GMACE__57BEA701] DEFAULT (0),
[TradeManagerDaysFilter] [int] NOT NULL CONSTRAINT [DF_DealerGroupPreference_TradeManagerDaysFilter] DEFAULT (0),
[ShowroomDaysFilter] [int] NOT NULL CONSTRAINT [DF_DealerGroupPreference_ShowroomDaysFilter] DEFAULT (3),
[bucketJumperAIPApproach] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_BucketJumperAIPApproach] DEFAULT (0),
[aipRunDayOfWeek] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_aipRunDayOfWeek] DEFAULT (1),
[FlashLocatorHideUnitCostDays] [tinyint] NOT NULL CONSTRAINT [DF__DealerPre__Flash__3D7FB211] DEFAULT (0),
[AppraisalRequirementLevel] [tinyint] NOT NULL CONSTRAINT [DF__DealerPre__Appra__3E73D64A] DEFAULT (0),
[CheckAppraisalHistoryForIMPPlanning] [tinyint] NOT NULL CONSTRAINT [DF__DealerPre__Check__78C07370] DEFAULT (1),
[ApplyDefaultPlanToGreenLightsInIMP] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_ApplyDefaultPlanToGreenLightsInIMP] DEFAULT ((0)),
[AppraisalFormShowOptions] [tinyint] NOT NULL CONSTRAINT [DF__DealerPre__Appra__2D343BAF] DEFAULT (1),
[AppraisalFormShowPhotos] [tinyint] NOT NULL CONSTRAINT [DF__DealerPre__Appra__2E285FE8] DEFAULT (1),
[AppraisalFormValidDate] [int] NOT NULL CONSTRAINT [DF__DealerPre__Appra__2F1C8421] DEFAULT (14),
[AppraisalFormValidMileage] [int] NOT NULL CONSTRAINT [DF__DealerPre__Appra__3010A85A] DEFAULT (150),
[AppraisalFormDisclaimer] [varchar] (180) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__DealerPre__Appra__3104CC93] DEFAULT ('The owner of this vehicle herby affirms that it has not been damaged by flood or had frame damage.'),
[AppraisalFormMemo] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__DealerPre__Appra__31F8F0CC] DEFAULT ('Trade in value for purchase of a vehicle.'),
[ShowAppraisalForm] [tinyint] NOT NULL CONSTRAINT [DF__DealerPre__ShowA__46F40DB2] DEFAULT (0),
[LiveAuctionDistanceFromDealer] [int] NOT NULL CONSTRAINT [DF_DealerPreference_LiveAuctionDistanceFromDealer] DEFAULT (1000),
[RepricePercentChangeThreshold] [int] NOT NULL CONSTRAINT [DF__DealerPre__Repri__477E1D97] DEFAULT (10),
[RepriceConfirmation] [tinyint] NOT NULL CONSTRAINT [DF__DealerPre__Repri__487241D0] DEFAULT (0),
[InternetAdvertiser_AdPriceEnabled] [bit] NOT NULL CONSTRAINT [DF_DealerPreference__InternetAdvertiser_AdPriceEnabled] DEFAULT (0),
[InternetAdvertiser_SendZeroesAsNull] [bit] NOT NULL CONSTRAINT [DF_DealerPreference__InternetAdvertiser_SendZeroesAsNull] DEFAULT (0),
[KBBInventoryBookoutDatasetPreference] [tinyint] NOT NULL CONSTRAINT [DF__DealerPre__KBBIn__26922B18] DEFAULT ((1)),
[KBBAppraisalBookoutDatasetPreference] [tinyint] NOT NULL CONSTRAINT [DF__DealerPre__KBBAp__27864F51] DEFAULT ((0)),
[KBBInventoryDefaultCondition] [tinyint] NOT NULL CONSTRAINT [DF__DealerPre__KBBIn__287A738A] DEFAULT ((1)),
[GroupAppraisalSearchWeeks] [int] NOT NULL CONSTRAINT [DF__DealerPre__Group__4F2A2C57] DEFAULT ((6)),
[ShowAppraisalValueGroup] [tinyint] NOT NULL CONSTRAINT [DF__DealerPre__ShowA__501E5090] DEFAULT ((1)),
[ShowAppraisalFormOfferGroup] [tinyint] NOT NULL CONSTRAINT [DF__DealerPre__ShowA__511274C9] DEFAULT ((1)),
[RedistributionNumTopDealers] [int] NOT NULL CONSTRAINT [DF__DealerPre__Redis__67F5DA21] DEFAULT ((10)),
[RedistributionDealerDistance] [int] NOT NULL CONSTRAINT [DF__DealerPre__Redis__68E9FE5A] DEFAULT ((500)),
[RedistributionROI] [tinyint] NOT NULL CONSTRAINT [DF__DealerPre__Redis__69DE2293] DEFAULT ((0)),
[RedistributionUnderstock] [tinyint] NOT NULL CONSTRAINT [DF__DealerPre__Redis__6AD246CC] DEFAULT ((1)),
[AuctionTimePeriodID] [int] NOT NULL CONSTRAINT [DF_DealerPreference_AuctionTimePeriod] DEFAULT ((11)),
[OVEEnabled] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_OveEnabled] DEFAULT ((0)),
[ShowCheckOnAppraisalForm] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_ShowCheckOnAppraisalForm] DEFAULT ((1)),
[ShowInactiveAppraisals] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_ShowInactiveAppraisals] DEFAULT ((0)),
[AppraisalFormValuesTPCBitMask] [int] NOT NULL CONSTRAINT [DF_DealerPreference_AppraisalFormValuesTPCBitMask] DEFAULT ((674)),
[AppraisalFormIncludeMileageAdjustment] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_AppraisalFormIncludeMileageAdjustment] DEFAULT ((1)),
[DefaultLithiaCarCenterMemberID] [int] NULL,
[twixURL] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UseLotPrice] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference__UseLotPrice] DEFAULT ((0)),
[SearchAppraisalDaysBackThreshold] [int] NOT NULL CONSTRAINT [DF__DealerPre__Searc__79C04FF8] DEFAULT ((45)),
[AppraisalLookBackPeriod] [int] NOT NULL CONSTRAINT [DF__DealerPre__Appra__016171C0] DEFAULT ((30)),
[AppraisalLookForwardPeriod] [int] NOT NULL CONSTRAINT [DF__DealerPre__Appra__025595F9] DEFAULT ((1)),
[GuideBookBitMask] AS (CONVERT([tinyint],power((2),[GuideBookID]-(1))|coalesce(power((2),[GuideBook2ID]-(1)),(0)),0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DealerPreference] on [dbo].[DealerPreference]'
GO
ALTER TABLE [dbo].[DealerPreference] ADD CONSTRAINT [PK_DealerPreference] PRIMARY KEY CLUSTERED  ([DealerPreferenceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inventory]'
GO
CREATE TABLE [dbo].[Inventory]
(
[InventoryID] [int] NOT NULL IDENTITY(1, 1),
[StockNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[VehicleID] [int] NOT NULL,
[InventoryActive] [tinyint] NOT NULL,
[InventoryReceivedDate] [smalldatetime] NOT NULL CONSTRAINT [DF_InventoryReceivedDate] DEFAULT (convert(smalldatetime,getdate())),
[DeleteDt] [smalldatetime] NULL,
[ModifiedDT] [smalldatetime] NOT NULL,
[DMSReferenceDt] [smalldatetime] NULL,
[UnitCost] [decimal] (9, 2) NOT NULL,
[AcquisitionPrice] [decimal] (8, 2) NULL,
[Pack] [decimal] (8, 2) NULL,
[MileageReceived] [int] NULL,
[TradeOrPurchase] [tinyint] NOT NULL,
[ListPrice] [decimal] (8, 2) NULL,
[InitialVehicleLight] [int] NULL,
[InventoryType] [tinyint] NOT NULL,
[ReconditionCost] [decimal] (8, 2) NULL,
[UsedSellingPrice] [decimal] (8, 2) NULL,
[Certified] [tinyint] NOT NULL,
[VehicleLocation] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CurrentVehicleLight] [tinyint] NULL,
[FLRecFollowed] [tinyint] NOT NULL,
[Audit_ID] [int] NOT NULL CONSTRAINT [DF_tbl_Inventory] DEFAULT (0),
[InventoryStatusCD] [smallint] NOT NULL CONSTRAINT [DF_Inventory_InventoryStatusCD] DEFAULT (0),
[ListPriceLock] [tinyint] NOT NULL CONSTRAINT [DF__inventory__ListP__7D5974AD] DEFAULT (0),
[SpecialFinance] [tinyint] NULL,
[eStockCardLock] [tinyint] NOT NULL CONSTRAINT [DF__Inventory__eStoc__0A943F91] DEFAULT (0),
[DateBookoutLocked] [smalldatetime] NULL,
[BookoutLocked] AS (convert(tinyint,case when ([DateBookoutLocked] <= getdate()) then 1 else 0 end)),
[PlanReminderDate] [smalldatetime] NULL,
[VersionNumber] [int] NOT NULL CONSTRAINT [DF_Inventory_VersionNumber] DEFAULT (0),
[RowVersion] [timestamp] NOT NULL,
[EdmundsTMV] [decimal] (8, 2) NULL,
[BookoutRequired] [tinyint] NOT NULL CONSTRAINT [DF_Inventory__BookoutRequired] DEFAULT ((0)),
[MileageReceivedLock] [tinyint] NOT NULL CONSTRAINT [DF_Inventory__MileageReceivedLock] DEFAULT ((0)),
[LotPrice] [int] NOT NULL CONSTRAINT [DF_Inventory__LotPrice] DEFAULT ((0)),
[TransferPrice] [decimal] (9, 2) NULL,
[TransferForRetailOnly] [bit] NOT NULL CONSTRAINT [DF_Inventory_TransferForRetailOnly] DEFAULT ((1)),
[TransferForRetailOnlyEnabled] [bit] NOT NULL CONSTRAINT [DF_Inventory_TransferForRetailOnlyEnabled] DEFAULT ((1)),
[AgeInDays] AS (datediff(day,[InventoryReceivedDate],getdate()))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Inventory] on [dbo].[Inventory]'
GO
ALTER TABLE [dbo].[Inventory] ADD CONSTRAINT [PK_Inventory] PRIMARY KEY CLUSTERED  ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Inventory__BusinessUnitID_EtAl] on [dbo].[Inventory]'
GO
CREATE NONCLUSTERED INDEX [IX_Inventory__BusinessUnitID_EtAl] ON [dbo].[Inventory] ([BusinessUnitID], [InventoryActive], [InventoryType], [StockNumber])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Inventory__InventoryReceivedDate_EtAl] on [dbo].[Inventory]'
GO
CREATE NONCLUSTERED INDEX [IX_Inventory__InventoryReceivedDate_EtAl] ON [dbo].[Inventory] ([InventoryReceivedDate], [BusinessUnitID], [InventoryType], [TradeOrPurchase])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_InventoryVehicleIdBusinessUnitIdInventoryReceivedDate] on [dbo].[Inventory]'
GO
CREATE NONCLUSTERED INDEX [IX_InventoryVehicleIdBusinessUnitIdInventoryReceivedDate] ON [dbo].[Inventory] ([VehicleID], [BusinessUnitID]) INCLUDE ([InventoryReceivedDate])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InternetAdvertiserDealership]'
GO
CREATE TABLE [dbo].[InternetAdvertiserDealership]
(
[InternetAdvertiserID] [int] NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[InternetAdvertisersDealershipCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsLive] [bit] NOT NULL,
[Notes] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactFirstName] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactLastName] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactPhoneNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContactEmail] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Active] [tinyint] NULL,
[status] [tinyint] NULL,
[VerifiedBy] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifiedDate] [smalldatetime] NULL,
[IncrementalExport] [bit] NOT NULL CONSTRAINT [DF_InternetAdvertiserDealership_IncExport] DEFAULT ((0)),
[ExportClientID] [int] NULL,
[ExportGuid] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DistributionActive] [bit] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InternetAdvertiserDealership] on [dbo].[InternetAdvertiserDealership]'
GO
ALTER TABLE [dbo].[InternetAdvertiserDealership] ADD CONSTRAINT [PK_InternetAdvertiserDealership] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [InternetAdvertiserID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BusinessUnit]'
GO
CREATE TABLE [dbo].[BusinessUnit]
(
[BusinessUnitID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitTypeID] [int] NOT NULL,
[BusinessUnit] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BusinessUnitShortName] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BusinessUnitCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ZipCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OfficePhone] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OfficeFax] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Active] [tinyint] NOT NULL CONSTRAINT [DF_BusinessUnit_Active] DEFAULT (1)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_BusinessUnitClustered] on [dbo].[BusinessUnit]'
GO
CREATE CLUSTERED INDEX [IX_BusinessUnitClustered] ON [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BusinessUnit] on [dbo].[BusinessUnit]'
GO
ALTER TABLE [dbo].[BusinessUnit] ADD CONSTRAINT [PK_BusinessUnit] PRIMARY KEY NONCLUSTERED  ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InternetAdvertiserBuildLog]'
GO
CREATE TABLE [dbo].[InternetAdvertiserBuildLog]
(
[InternetAdvertiserBuildLogID] [int] NOT NULL IDENTITY(1, 1),
[InternetAdvertiserID] [int] NOT NULL,
[Created] [datetime] NOT NULL CONSTRAINT [DF_InternetAdvertiserBuildLog__Created] DEFAULT (getdate()),
[TestForBusinessUnitID] [int] NULL,
[ExceptionReportsSent] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InternetAdvertiserBuildLog] on [dbo].[InternetAdvertiserBuildLog]'
GO
ALTER TABLE [dbo].[InternetAdvertiserBuildLog] ADD CONSTRAINT [PK_InternetAdvertiserBuildLog] PRIMARY KEY CLUSTERED  ([InternetAdvertiserBuildLogID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[MakeModelGrouping]'
GO
CREATE TABLE [dbo].[MakeModelGrouping]
(
[MakeModelGroupingID] [int] NOT NULL IDENTITY(100000, 1),
[GroupingDescriptionID] [int] NOT NULL CONSTRAINT [DF_GroupingDescription] DEFAULT ((0)),
[Make] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SegmentID] [int] NOT NULL CONSTRAINT [DF__tbl_MakeM__Displ__51FA155C] DEFAULT (1),
[Line] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModelID] [int] NULL,
[DateCreated] [smalldatetime] NULL CONSTRAINT [DF_MakeModelGrouping__DateCreated] DEFAULT (getdate()),
[DisplayBodyTypeID] AS ([SegmentID]),
[OriginalModel] AS ([Line])
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_MakeModelGrouping__GroupingDescriptionID_MakeModelGroupingID] on [dbo].[MakeModelGrouping]'
GO
CREATE CLUSTERED INDEX [IX_MakeModelGrouping__GroupingDescriptionID_MakeModelGroupingID] ON [dbo].[MakeModelGrouping] ([GroupingDescriptionID], [MakeModelGroupingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MakeModelGrouping] on [dbo].[MakeModelGrouping]'
GO
ALTER TABLE [dbo].[MakeModelGrouping] ADD CONSTRAINT [PK_MakeModelGrouping] PRIMARY KEY NONCLUSTERED  ([MakeModelGroupingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_MakeModelGrouping__ModelID] on [dbo].[MakeModelGrouping]'
GO
CREATE NONCLUSTERED INDEX [IX_MakeModelGrouping__ModelID] ON [dbo].[MakeModelGrouping] ([ModelID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Member]'
GO
CREATE TABLE [dbo].[Member]
(
[MemberID] [int] NOT NULL IDENTITY(1, 1),
[Login] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Password] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Salutation] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PreferredFirstName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MiddleInitial] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OfficePhoneNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OfficePhoneExtension] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OfficeFaxNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MobilePhoneNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PagerNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailAddress] [varchar] (129) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_Member_EmailAddress] DEFAULT (''),
[ReportMethod] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DashboardRowDisplay] [int] NOT NULL CONSTRAINT [DF_Member_DashboardRowDisplay] DEFAULT ((10)),
[ReportPreference] [int] NULL,
[LoginStatus] [int] NULL,
[MemberType] [int] NULL CONSTRAINT [DF_Member_MemberType] DEFAULT (0),
[CreateDate] [datetime] NOT NULL CONSTRAINT [DF_Member_CreateDate] DEFAULT (getdate()),
[UserRoleCD] [tinyint] NOT NULL,
[DefaultDealerGroupID] [int] NULL,
[sms_address] [varchar] (129) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobTitleID] [tinyint] NOT NULL,
[SearchHomePage] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InventoryOverviewSortOrderType] [tinyint] NOT NULL CONSTRAINT [DF_Member__InventoryOverviewSortOrderType] DEFAULT ((0)),
[GroupHomePageID] [tinyint] NOT NULL CONSTRAINT [DF_Member__GroupHomePageID] DEFAULT ((1)),
[ActiveInventoryLaunchTool] [tinyint] NOT NULL CONSTRAINT [DF_Member__ActiveInventoryLaunchTool] DEFAULT ((0)),
[Active] [bit] NOT NULL CONSTRAINT [DF_Member__Active] DEFAULT ((1))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Member] on [dbo].[Member]'
GO
ALTER TABLE [dbo].[Member] ADD CONSTRAINT [PK_Member] PRIMARY KEY NONCLUSTERED  ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AIP_EventType]'
GO
CREATE TABLE [dbo].[AIP_EventType]
(
[AIP_EventTypeID] [tinyint] NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AIP_EventCategoryID] [tinyint] NOT NULL,
[CurrentWhenSingleDayPlan] [bit] NOT NULL CONSTRAINT [DF_AIP_Event__CurrentWhenSingleDayPlan] DEFAULT (0),
[CurrentAtCreateDate] [bit] NOT NULL CONSTRAINT [DF_AIP_Event__CurrentAtCreateDate] DEFAULT (0),
[ThirdPartyEntityTypeID] [tinyint] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AIP_EventType] on [dbo].[AIP_EventType]'
GO
ALTER TABLE [dbo].[AIP_EventType] ADD CONSTRAINT [PK_AIP_EventType] PRIMARY KEY CLUSTERED  ([AIP_EventTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AIP_Event]'
GO
CREATE TABLE [dbo].[AIP_Event]
(
[AIP_EventID] [int] NOT NULL IDENTITY(1, 1),
[InventoryID] [int] NOT NULL,
[AIP_EventTypeID] [tinyint] NOT NULL,
[BeginDate] [smalldatetime] NOT NULL,
[EndDate] [smalldatetime] NULL,
[Notes] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes2] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ThirdPartyEntityID] [int] NULL,
[Value] [decimal] (9, 2) NULL,
[Created] [smalldatetime] NOT NULL CONSTRAINT [DF_AIP_Event_Created] DEFAULT (getdate()),
[CreatedBy] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_AIP_Event_CreatedBy] DEFAULT (suser_sname()),
[LastModified] [smalldatetime] NOT NULL CONSTRAINT [DF_AIP_Event_LastModified] DEFAULT (getdate()),
[LastModifiedBy] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_AIP_Event_LastModifiedBy] DEFAULT (suser_sname()),
[VersionNumber] [int] NOT NULL CONSTRAINT [DF_AIP_Event_VersionNumber] DEFAULT (0),
[UserBeginDate] [smalldatetime] NULL,
[UserEndDate] [smalldatetime] NULL,
[Notes3] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RepriceSourceID] [int] NULL CONSTRAINT [DF__AIP_Event__Repri__7AA98739] DEFAULT (NULL)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_AIP_Event__InventoryID_AIP_EventTypeID_BeginDate] on [dbo].[AIP_Event]'
GO
CREATE CLUSTERED INDEX [IX_AIP_Event__InventoryID_AIP_EventTypeID_BeginDate] ON [dbo].[AIP_Event] ([InventoryID], [AIP_EventTypeID], [BeginDate])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AIP_Event] on [dbo].[AIP_Event]'
GO
ALTER TABLE [dbo].[AIP_Event] ADD CONSTRAINT [PK_AIP_Event] PRIMARY KEY NONCLUSTERED  ([AIP_EventID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_AIP_Event__ThirdPartyEntityIDEndDate] on [dbo].[AIP_Event]'
GO
CREATE NONCLUSTERED INDEX [IX_AIP_Event__ThirdPartyEntityIDEndDate] ON [dbo].[AIP_Event] ([ThirdPartyEntityID], [EndDate])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[NADARegionAreas]'
GO
CREATE TABLE [dbo].[NADARegionAreas]
(
[NADARegionAreaID] [smallint] NOT NULL IDENTITY(1, 1),
[NADARegionCode] [tinyint] NOT NULL,
[State] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[County] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IncludeOrExclude] [bit] NOT NULL CONSTRAINT [DF__NADARegio__Inclu__01A9F3D5] DEFAULT (1)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_NADARegionAreas] on [dbo].[NADARegionAreas]'
GO
ALTER TABLE [dbo].[NADARegionAreas] ADD CONSTRAINT [PK_NADARegionAreas] PRIMARY KEY CLUSTERED  ([NADARegionAreaID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[RepriceExport]'
GO
CREATE TABLE [dbo].[RepriceExport]
(
[RepriceExportID] [int] NOT NULL IDENTITY(1, 1),
[RepriceEventID] [int] NOT NULL,
[RepriceExportStatusID] [int] NOT NULL,
[ThirdPartyEntityID] [int] NOT NULL,
[FailureReason] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FailureCount] [int] NOT NULL CONSTRAINT [DF_RepriceExport_FailureCount] DEFAULT ((0)),
[DateCreated] [datetime] NULL,
[DateSent] [datetime] NULL,
[StatusModifiedDate] [datetime] NOT NULL CONSTRAINT [DF_RepriceExport_StatusModifiedDate] DEFAULT (getdate())
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_RepriceExport] on [dbo].[RepriceExport]'
GO
ALTER TABLE [dbo].[RepriceExport] ADD CONSTRAINT [PK_RepriceExport] PRIMARY KEY CLUSTERED  ([RepriceExportID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [ix_RepriceExport__RepriceEventId] on [dbo].[RepriceExport]'
GO
CREATE NONCLUSTERED INDEX [ix_RepriceExport__RepriceEventId] ON [dbo].[RepriceExport] ([RepriceEventID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InternetAdvertiserBuildList]'
GO
CREATE TABLE [dbo].[InternetAdvertiserBuildList]
(
[InternetAdvertiserID] [int] NOT NULL,
[InventoryID] [int] NOT NULL,
[ExportStatusCD] [tinyint] NOT NULL,
[Highlights] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UseAdPrice] [bit] NOT NULL CONSTRAINT [DF__InternetA__UseAd__7544E847] DEFAULT (0),
[IncludePhotos] [bit] NOT NULL CONSTRAINT [DF__InternetA__Inclu__31EECF2B] DEFAULT ((1))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InternetAdvertiserBuildList] on [dbo].[InternetAdvertiserBuildList]'
GO
ALTER TABLE [dbo].[InternetAdvertiserBuildList] ADD CONSTRAINT [PK_InternetAdvertiserBuildList] PRIMARY KEY CLUSTERED  ([InternetAdvertiserID], [InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_InternetAdvertiserBuildList__InventoryID] on [dbo].[InternetAdvertiserBuildList]'
GO
CREATE NONCLUSTERED INDEX [IX_InternetAdvertiserBuildList__InventoryID] ON [dbo].[InternetAdvertiserBuildList] ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tbl_VehicleSale]'
GO
CREATE TABLE [dbo].[tbl_VehicleSale]
(
[InventoryID] [int] NOT NULL,
[SalesReferenceNumber] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VehicleMileage] [int] NULL,
[DealDate] [smalldatetime] NOT NULL,
[BackEndGross] [decimal] (8, 2) NOT NULL,
[NetFinancialInsuranceIncome] [decimal] (8, 2) NULL,
[OriginalFrontEndGross] [decimal] (9, 2) NULL,
[FrontEndGross] [decimal] (9, 2) NOT NULL,
[SalePrice] [decimal] (9, 2) NOT NULL,
[SaleDescription] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FinanceInsuranceDealNumber] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastPolledDate] [smalldatetime] NOT NULL,
[AnalyticalEngineStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AnalyticalEngineStatus_CD] [int] NULL,
[LeaseFlag] [tinyint] NULL,
[Pack] [decimal] (8, 2) NULL,
[AfterMarketGross] [decimal] (8, 2) NOT NULL,
[TotalGross] [decimal] (8, 2) NOT NULL,
[VehiclePromotionAmount] [decimal] (8, 2) NULL,
[DealStatusCD] [tinyint] NOT NULL CONSTRAINT [DF_DealStatusCD] DEFAULT (2),
[PackAdjust] [decimal] (10, 2) NULL,
[Audit_ID] [int] NOT NULL CONSTRAINT [DF_tbl_VehicleSale] DEFAULT (0),
[Valuation] [float] NULL,
[CustomerZip] [varchar] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tbl_VehicleSale] on [dbo].[tbl_VehicleSale]'
GO
ALTER TABLE [dbo].[tbl_VehicleSale] ADD CONSTRAINT [PK_tbl_VehicleSale] PRIMARY KEY CLUSTERED  ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tbl_VehicleSale__DealDate_EtAl] on [dbo].[tbl_VehicleSale]'
GO
CREATE NONCLUSTERED INDEX [IX_tbl_VehicleSale__DealDate_EtAl] ON [dbo].[tbl_VehicleSale] ([DealDate], [SaleDescription], [DealStatusCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Extract].[DealerConfiguration]'
GO
CREATE TABLE [Extract].[DealerConfiguration]
(
[BusinessUnitID] [int] NOT NULL,
[DestinationName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExternalIdentifier] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [smalldatetime] NOT NULL,
[EndDate] [smalldatetime] NULL,
[Active] [bit] NOT NULL CONSTRAINT [DF_Extract_DealerConfiguration__IsActive] DEFAULT ((0)),
[EffectiveDateActive] AS (CONVERT([tinyint],case when getdate()>=[StartDate] AND getdate()<=[EndDate] then (1) when getdate()>[StartDate] AND [EndDate] IS NULL then (1) else (0) end&[Active],(0))),
[IncludeHistorical] [bit] NOT NULL CONSTRAINT [DF_Extract_DealerConfiguration__IncludeHistorical] DEFAULT ((0)),
[InsertDate] [datetime] NOT NULL CONSTRAINT [DF_Extract_DealerConfiguration__InsertDate] DEFAULT (getdate()),
[InsertUser] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_Extract_DealerConfiguration__InsertUser] DEFAULT (suser_sname()),
[UpdateDate] [datetime] NOT NULL CONSTRAINT [DF_Extract_DealerConfiguration__UpdateDate] DEFAULT (getdate()),
[UpdateUser] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_Extract_DealerConfiguration__UpdateUser] DEFAULT (suser_sname())
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Extract_DealerConfiguration__BusinessUnitIDDestinationName] on [Extract].[DealerConfiguration]'
GO
ALTER TABLE [Extract].[DealerConfiguration] ADD CONSTRAINT [PK_Extract_DealerConfiguration__BusinessUnitIDDestinationName] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [DestinationName])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UQ_Extract_DealerConfiguration__DestinationNameExternalIdentifier] on [Extract].[DealerConfiguration]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_Extract_DealerConfiguration__DestinationNameExternalIdentifier] ON [Extract].[DealerConfiguration] ([DestinationName], [ExternalIdentifier])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Extract].[Destination]'
GO
CREATE TABLE [Extract].[Destination]
(
[Name] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DefaultIncludeHistorical] [bit] NOT NULL CONSTRAINT [DF_ExtractDestination__DefaultIncludeHistorical] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Extract_Destination] on [Extract].[Destination]'
GO
ALTER TABLE [Extract].[Destination] ADD CONSTRAINT [PK_Extract_Destination] PRIMARY KEY NONCLUSTERED  ([Name])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AppraisalValues]'
GO
CREATE TABLE [dbo].[AppraisalValues]
(
[AppraisalValueID] [int] NOT NULL IDENTITY(1, 1),
[AppraisalID] [int] NULL,
[SequenceNumber] [int] NOT NULL CONSTRAINT [DF_AppraisalValues_SequenceNumber] DEFAULT (1),
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_AppraisalValues_DateCreated] DEFAULT (getdate()),
[Value] [int] NOT NULL CONSTRAINT [DF_AppraisalValues_Value] DEFAULT (0),
[AppraiserName] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AppraisalValues] on [dbo].[AppraisalValues]'
GO
ALTER TABLE [dbo].[AppraisalValues] ADD CONSTRAINT [PK_AppraisalValues] PRIMARY KEY CLUSTERED  ([AppraisalValueID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AppraisalFormOptions]'
GO
CREATE TABLE [dbo].[AppraisalFormOptions]
(
[AppraisalFormID] [int] NOT NULL IDENTITY(1, 1),
[AppraisalID] [int] NULL,
[AppraisalFormOffer] [int] NULL,
[AppraisalFormHistory] [int] NULL,
[showEquipmentOptions] [tinyint] NULL,
[showPhotos] [tinyint] NULL,
[includeMileageAdjustment] [tinyint] NOT NULL CONSTRAINT [DF__Appraisal__inclu__296E97C3] DEFAULT ((1)),
[showKbbConsumerValue] [tinyint] NOT NULL CONSTRAINT [DF_AppraisalFormOptions__showKbbConsumerValue] DEFAULT ((0)),
[CategoriesToDisplayBitMask] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AppraisalFormOptions] on [dbo].[AppraisalFormOptions]'
GO
ALTER TABLE [dbo].[AppraisalFormOptions] ADD CONSTRAINT [PK_AppraisalFormOptions] PRIMARY KEY CLUSTERED  ([AppraisalFormID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tbl_Vehicle]'
GO
CREATE TABLE [dbo].[tbl_Vehicle]
(
[VehicleID] [int] NOT NULL IDENTITY(1, 1),
[Vin] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VehicleYear] [int] NOT NULL,
[MakeModelGroupingID] [int] NOT NULL,
[VehicleTrim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VehicleEngine] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VehicleDriveTrain] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VehicleTransmission] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CylinderCount] [tinyint] NULL,
[DoorCount] [tinyint] NULL,
[BaseColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InteriorColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FuelType] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InteriorDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreateDt] [smalldatetime] NOT NULL CONSTRAINT [DF_tbl_Vehicle_CreateDt] DEFAULT (convert(smalldatetime,getdate())),
[OriginalMake] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalModel] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalYear] [smallint] NULL,
[OriginalTrim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalBodyStyle] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Audit_ID] [int] NOT NULL CONSTRAINT [DF_tbl_Vehicle] DEFAULT (0),
[VehicleSourceID] [tinyint] NOT NULL CONSTRAINT [DF_tbl_Vehicle__VehicleSourceID] DEFAULT ((1)),
[CatalogKey] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VIC] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VehicleCatalogID] [int] NULL,
[BodyTypeID] [tinyint] NOT NULL CONSTRAINT [DF_tbl_Vehicle__BodyTypeID] DEFAULT ((0)),
[ExtColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModelCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tbl_Vehicle] on [dbo].[tbl_Vehicle]'
GO
ALTER TABLE [dbo].[tbl_Vehicle] ADD CONSTRAINT [PK_tbl_Vehicle] PRIMARY KEY CLUSTERED  ([VehicleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tbl_VehicleVehicleIdVehicleCatalogId] on [dbo].[tbl_Vehicle]'
GO
CREATE NONCLUSTERED INDEX [IX_tbl_VehicleVehicleIdVehicleCatalogId] ON [dbo].[tbl_Vehicle] ([VehicleID], [VehicleCatalogID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_MakeModelGroupingID] on [dbo].[tbl_Vehicle]'
GO
CREATE NONCLUSTERED INDEX [IX_MakeModelGroupingID] ON [dbo].[tbl_Vehicle] ([MakeModelGroupingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tbl_GroupingDescription]'
GO
CREATE TABLE [dbo].[tbl_GroupingDescription]
(
[GroupingDescriptionID] [int] NOT NULL IDENTITY(100000, 1),
[GroupingDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SuppressMarketPerformer] [bit] NULL CONSTRAINT [DF__tbl_Group__Suppr__2215F810] DEFAULT (0),
[DateCreated] [smalldatetime] NULL CONSTRAINT [DF_GroupingDescription__DateCreated] DEFAULT (getdate())
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tbl_GroupingDescription] on [dbo].[tbl_GroupingDescription]'
GO
ALTER TABLE [dbo].[tbl_GroupingDescription] ADD CONSTRAINT [PK_tbl_GroupingDescription] PRIMARY KEY CLUSTERED  ([GroupingDescriptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Bookouts]'
GO
CREATE TABLE [dbo].[Bookouts]
(
[BookoutID] [int] NOT NULL IDENTITY(1, 1),
[BookoutSourceID] [tinyint] NOT NULL CONSTRAINT [DF_Bookouts_BookoutSourceID] DEFAULT (1),
[ThirdPartyID] [tinyint] NOT NULL,
[DateCreated] [datetime] NULL CONSTRAINT [DF_Bookouts_DateCreated] DEFAULT (getdate()),
[Region] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DatePublished] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BookPrice] [int] NULL,
[Weight] [int] NULL,
[MileageCostAdjustment] [int] NULL,
[MSRP] [int] NULL,
[BookoutStatusID] [tinyint] NOT NULL CONSTRAINT [DF_Bookouts_BookoutStatusID] DEFAULT ((1)),
[IsAccurate] AS (CONVERT([tinyint],case when [BookoutStatusID]=(1) then (1) else (0) end,0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Bookouts] on [dbo].[Bookouts]'
GO
ALTER TABLE [dbo].[Bookouts] ADD CONSTRAINT [PK_Bookouts] PRIMARY KEY CLUSTERED  ([BookoutID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BusinessUnitPreference_Bookout]'
GO
CREATE TABLE [dbo].[BusinessUnitPreference_Bookout]
(
[BusinessUnitID] [int] NOT NULL,
[ProcessorPriority] [tinyint] NOT NULL CONSTRAINT [DF_BusinessUnitPreference_Bookout__ProcessorPriority] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BusinessUnitPreference_Bookout] on [dbo].[BusinessUnitPreference_Bookout]'
GO
ALTER TABLE [dbo].[BusinessUnitPreference_Bookout] ADD CONSTRAINT [PK_BusinessUnitPreference_Bookout] PRIMARY KEY CLUSTERED  ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InternetAdvertisement]'
GO
CREATE TABLE [dbo].[InternetAdvertisement]
(
[InventoryID] [int] NOT NULL,
[AdvertisementText] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateUser] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_InternetAdvertisement__LastUpdatedBy] DEFAULT (suser_sname()),
[UpdateDate] [datetime] NOT NULL CONSTRAINT [DF_InternetAdvertisement__UpdateDate] DEFAULT (getdate())
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InternetAdvertisement] on [dbo].[InternetAdvertisement]'
GO
ALTER TABLE [dbo].[InternetAdvertisement] ADD CONSTRAINT [PK_InternetAdvertisement] PRIMARY KEY CLUSTERED  ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CIAGroupingItemDetails]'
GO
CREATE TABLE [dbo].[CIAGroupingItemDetails]
(
[CIAGroupingItemDetailID] [int] NOT NULL IDENTITY(1, 1),
[CIAGroupingItemID] [int] NOT NULL,
[CIAGroupingItemDetailLevelID] [tinyint] NOT NULL,
[CIAGroupingItemDetailTypeID] [tinyint] NOT NULL CONSTRAINT [DF_CIAGroupingItemDetails_CIAGroupingItemDetailTypeID] DEFAULT (0),
[Value] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LowRange] [int] NULL,
[HighRange] [int] NULL,
[Units] [int] NULL CONSTRAINT [DF_CIAGroupingItemDetails_Units] DEFAULT (0),
[DateCreated] [smalldatetime] NOT NULL CONSTRAINT [DF_CIAGroupingItemDetails_DateCreated] DEFAULT (getdate())
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_CIAGroupingItemDetails] on [dbo].[CIAGroupingItemDetails]'
GO
ALTER TABLE [dbo].[CIAGroupingItemDetails] ADD CONSTRAINT [PK_CIAGroupingItemDetails] PRIMARY KEY CLUSTERED  ([CIAGroupingItemDetailID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_CIAGroupingItemDetails__CIAGroupingItemID] on [dbo].[CIAGroupingItemDetails]'
GO
CREATE NONCLUSTERED INDEX [IX_CIAGroupingItemDetails__CIAGroupingItemID] ON [dbo].[CIAGroupingItemDetails] ([CIAGroupingItemID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_CIAGroupingItemDetailsGroupingItemIdGroupingItemDetailLevelIdGroupingItemDetailTypeId] on [dbo].[CIAGroupingItemDetails]'
GO
CREATE NONCLUSTERED INDEX [IX_CIAGroupingItemDetailsGroupingItemIdGroupingItemDetailLevelIdGroupingItemDetailTypeId] ON [dbo].[CIAGroupingItemDetails] ([CIAGroupingItemID], [CIAGroupingItemDetailLevelID], [CIAGroupingItemDetailTypeID]) INCLUDE ([Units])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[AuditBookOuts]'
GO
CREATE TABLE [dbo].[AuditBookOuts]
(
[AuditId] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitId] [int] NOT NULL,
[MemberId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[ReferenceId] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BookId] [tinyint] NOT NULL,
[BookOutTimeStamp] [smalldatetime] NOT NULL CONSTRAINT [DF_BookOutTimeStamp] DEFAULT (getdate()),
[BookOutId] [int] NULL CONSTRAINT [DF_DefaultBookOutId] DEFAULT ((-1))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_AuditBookOuts] on [dbo].[AuditBookOuts]'
GO
ALTER TABLE [dbo].[AuditBookOuts] ADD CONSTRAINT [PK_AuditBookOuts] PRIMARY KEY CLUSTERED  ([AuditId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_AuditBookouts__BookoutID] on [dbo].[AuditBookOuts]'
GO
CREATE NONCLUSTERED INDEX [IX_AuditBookouts__BookoutID] ON [dbo].[AuditBookOuts] ([BookOutId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BookoutThirdPartyCategories]'
GO
CREATE TABLE [dbo].[BookoutThirdPartyCategories]
(
[BookoutThirdPartyCategoryID] [int] NOT NULL IDENTITY(1, 1),
[BookoutID] [int] NOT NULL,
[ThirdPartyCategoryID] [int] NOT NULL,
[IsValid] [tinyint] NOT NULL CONSTRAINT [DF_Bookouts_Valid] DEFAULT (1),
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_BookoutThirdPartyCategories_DateCreated] DEFAULT (getdate()),
[DateModified] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BookoutThirdPartyCategories] on [dbo].[BookoutThirdPartyCategories]'
GO
ALTER TABLE [dbo].[BookoutThirdPartyCategories] ADD CONSTRAINT [PK_BookoutThirdPartyCategories] PRIMARY KEY CLUSTERED  ([BookoutThirdPartyCategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_BookoutThirdPartyCategories_BookoutID] on [dbo].[BookoutThirdPartyCategories]'
GO
CREATE NONCLUSTERED INDEX [IX_BookoutThirdPartyCategories_BookoutID] ON [dbo].[BookoutThirdPartyCategories] ([BookoutID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BookoutValues]'
GO
CREATE TABLE [dbo].[BookoutValues]
(
[BookoutValueID] [int] NOT NULL IDENTITY(1, 1),
[BookoutThirdPartyCategoryID] [int] NOT NULL,
[BookoutValueTypeID] [tinyint] NOT NULL,
[ThirdPartySubCategoryID] [int] NULL,
[Value] [int] NULL,
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_BookoutValues_DateCreated] DEFAULT (getdate()),
[DateModified] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BookoutValues] on [dbo].[BookoutValues]'
GO
ALTER TABLE [dbo].[BookoutValues] ADD CONSTRAINT [PK_BookoutValues] PRIMARY KEY CLUSTERED  ([BookoutValueID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_BookoutValues_ValueTypeCategoryID] on [dbo].[BookoutValues]'
GO
CREATE NONCLUSTERED INDEX [IX_BookoutValues_ValueTypeCategoryID] ON [dbo].[BookoutValues] ([BookoutThirdPartyCategoryID], [BookoutValueTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ThirdPartyVehicles]'
GO
CREATE TABLE [dbo].[ThirdPartyVehicles]
(
[ThirdPartyVehicleID] [int] NOT NULL IDENTITY(1, 1),
[ThirdPartyID] [tinyint] NOT NULL,
[ThirdPartyVehicleCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MakeCode] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModelCode] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [tinyint] NOT NULL CONSTRAINT [DF_ThirdPartyVehicles_Status] DEFAULT (0),
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_ThirdPartyVehicles_DateCreated_1] DEFAULT (getdate()),
[DateModified] [datetime] NULL,
[EngineType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Make] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Body] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ThirdPartyVehicles] on [dbo].[ThirdPartyVehicles]'
GO
ALTER TABLE [dbo].[ThirdPartyVehicles] ADD CONSTRAINT [PK_ThirdPartyVehicles] PRIMARY KEY CLUSTERED  ([ThirdPartyVehicleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ThirdPartyVehicles__ThirdPartyVehicleCode] on [dbo].[ThirdPartyVehicles]'
GO
CREATE NONCLUSTERED INDEX [IX_ThirdPartyVehicles__ThirdPartyVehicleCode] ON [dbo].[ThirdPartyVehicles] ([ThirdPartyVehicleCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ThirdPartyVehicleOptions]'
GO
CREATE TABLE [dbo].[ThirdPartyVehicleOptions]
(
[ThirdPartyVehicleOptionID] [int] NOT NULL IDENTITY(1, 1),
[ThirdPartyVehicleID] [int] NOT NULL,
[ThirdPartyOptionID] [int] NOT NULL,
[IsStandardOption] [tinyint] NOT NULL CONSTRAINT [DF_ThirdPartyOptions_IsStandardOption] DEFAULT (0),
[Status] [tinyint] NULL CONSTRAINT [DF_ThirdPartyVehicleOptions_Status] DEFAULT (0),
[SortOrder] [int] NULL CONSTRAINT [DF_ThirdPartyVehicleOptions_SortOrder] DEFAULT ((0)),
[DateCreated] [smalldatetime] NOT NULL CONSTRAINT [DF_ThirdPartyVehicleOptions_DateCreated] DEFAULT (getdate())
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_VehicleThirdPartyVehicleOptions] on [dbo].[ThirdPartyVehicleOptions]'
GO
ALTER TABLE [dbo].[ThirdPartyVehicleOptions] ADD CONSTRAINT [PK_VehicleThirdPartyVehicleOptions] PRIMARY KEY CLUSTERED  ([ThirdPartyVehicleOptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ThirdPartyVehicleOptions__ThirdPartyVehicleID] on [dbo].[ThirdPartyVehicleOptions]'
GO
CREATE NONCLUSTERED INDEX [IX_ThirdPartyVehicleOptions__ThirdPartyVehicleID] ON [dbo].[ThirdPartyVehicleOptions] ([ThirdPartyVehicleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ThirdPartyVehicleOptionValues]'
GO
CREATE TABLE [dbo].[ThirdPartyVehicleOptionValues]
(
[ThirdPartyVehicleOptionValueID] [int] NOT NULL IDENTITY(1, 1),
[ThirdPartyVehicleOptionID] [int] NOT NULL,
[ThirdPartyOptionValueTypeID] [tinyint] NULL CONSTRAINT [DF_ThirdPartyVehicleOptionValues_ThirdPartyOptionValueTypeID] DEFAULT (0),
[Value] [smallint] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_ThirdPartyVehicleOptionValues__ThirdPartyVehicleOptionID] on [dbo].[ThirdPartyVehicleOptionValues]'
GO
CREATE CLUSTERED INDEX [IX_ThirdPartyVehicleOptionValues__ThirdPartyVehicleOptionID] ON [dbo].[ThirdPartyVehicleOptionValues] ([ThirdPartyVehicleOptionID], [ThirdPartyOptionValueTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ThirdPartyVehicleOptionValues] on [dbo].[ThirdPartyVehicleOptionValues]'
GO
ALTER TABLE [dbo].[ThirdPartyVehicleOptionValues] ADD CONSTRAINT [PK_ThirdPartyVehicleOptionValues] PRIMARY KEY NONCLUSTERED  ([ThirdPartyVehicleOptionValueID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealFinance]'
GO
CREATE TABLE [dbo].[DealFinance]
(
[InventoryId] [int] NOT NULL,
[FinancedAmount] [money] NOT NULL,
[TermInMonths] [tinyint] NULL,
[AnnualPercentageRate] [decimal] (4, 2) NULL,
[MonthlyPaymentAmount] [money] NULL,
[LenderName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ManufacturerSuggestedRetailPrice] [money] NULL,
[BalloonPaymentAmount] [money] NULL,
[BuyRate] [decimal] (4, 2) NULL,
[CashDownAmount] [money] NULL,
[DeferredDownPaymentAmount] [money] NULL,
[RebateAmount] [money] NULL,
[InsertDate] [smalldatetime] NOT NULL CONSTRAINT [DF_DealFinance__InsertDate] DEFAULT (getdate()),
[InsertUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_DealFinance__InsertUser] DEFAULT (suser_sname()),
[UpdateDate] [smalldatetime] NOT NULL CONSTRAINT [DF_DealFinance__UpdateDate] DEFAULT (getdate()),
[UpdateUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_DealFinance__UpdateUser] DEFAULT (suser_sname()),
[InvoiceAmount] [decimal] (9, 2) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [PK_DealFinance] on [dbo].[DealFinance]'
GO
CREATE UNIQUE CLUSTERED INDEX [PK_DealFinance] ON [dbo].[DealFinance] ([InventoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealLease]'
GO
CREATE TABLE [dbo].[DealLease]
(
[InventoryId] [int] NOT NULL,
[AllowedAnnualMileage] [int] NULL,
[ResidualAmount] [money] NULL,
[MoneyFactor] [decimal] (6, 6) NULL,
[CapitalReductionAmount] [money] NULL,
[DriveOffAmount] [money] NULL,
[InsertDate] [smalldatetime] NOT NULL CONSTRAINT [DF_DealLease__InsertDate] DEFAULT (getdate()),
[InsertUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_DealLease__InsertUser] DEFAULT (suser_sname()),
[UpdateDate] [smalldatetime] NOT NULL CONSTRAINT [DF_DealLease__UpdateDate] DEFAULT (getdate()),
[UpdateUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_DealLease__UpdateUser] DEFAULT (suser_sname())
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [PK_DealLease] on [dbo].[DealLease]'
GO
CREATE UNIQUE CLUSTERED INDEX [PK_DealLease] ON [dbo].[DealLease] ([InventoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealTradeIn]'
GO
CREATE TABLE [dbo].[DealTradeIn]
(
[InventoryId] [int] NOT NULL,
[VIN] [char] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VehicleCatalogId] [int] NULL,
[Make] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModelYear] [int] NOT NULL,
[BodyType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DoorCount] [tinyint] NULL,
[ExteriorColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DriveType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EngineDisplacement] [decimal] (4, 1) NULL,
[FuelType] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Transmission] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Trim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Mileage] [int] NULL,
[CreditAmount] [decimal] (9, 2) NULL,
[ActualCashValue] [decimal] (9, 2) NULL,
[LienAmount] [int] NULL,
[InsertDate] [smalldatetime] NOT NULL CONSTRAINT [DF_DealTradeIn__InsertDate] DEFAULT (getdate()),
[InsertUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_DealTradeIn__InsertUser] DEFAULT (suser_sname()),
[UpdateDate] [smalldatetime] NOT NULL CONSTRAINT [DF_DealTradeIn__UpdateDate] DEFAULT (getdate()),
[UpdateUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_DealTradeIn__UpdateUser] DEFAULT (suser_sname())
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [PK_DealTradeIn_InventoryIdVIN] on [dbo].[DealTradeIn]'
GO
CREATE UNIQUE CLUSTERED INDEX [PK_DealTradeIn_InventoryIdVIN] ON [dbo].[DealTradeIn] ([InventoryId], [VIN])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_DealTradeIn__VIN] on [dbo].[DealTradeIn]'
GO
CREATE NONCLUSTERED INDEX [IX_DealTradeIn__VIN] ON [dbo].[DealTradeIn] ([VIN])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerPreference_Dataload]'
GO
CREATE TABLE [dbo].[DealerPreference_Dataload]
(
[BusinessUnitID] [int] NOT NULL,
[TradeorPurchaseFromSales] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_Dataload_TradeorPurchaseFromSales] DEFAULT ((0)),
[TradeorPurchaseFromSalesDaysThreshold] [smallint] NOT NULL CONSTRAINT [DF_DealerPreference_Dataload_TradeorPurchaseFromSalesDaysThreshold] DEFAULT ((0)),
[GenerateInventoryDeltas] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_Dataload_GenerateInventroyDeltas] DEFAULT ((1)),
[CalculateUnitCost] [bit] NOT NULL CONSTRAINT [DF_DealerPreference_Dataload__CalculateUnitCost] DEFAULT ((0)),
[VINDecodeCountryCodePrimary] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_Dataload__VINDecodeCountryCodePrimary] DEFAULT ((1)),
[VINDecodeCountryCodeSecondary] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_Dataload__VINDecodeCountryCodeSecondary] DEFAULT ((2)),
[ReuseStockNumbers] [bit] NOT NULL CONSTRAINT [DF_DealerPreference_Dataload__ReuseStockNumbers] DEFAULT ((0)),
[eStockCardLockOnListPrice] [bit] NOT NULL CONSTRAINT [DF_DealerPreference_Dataload__eStockCardLockOnListPrice] DEFAULT ((1)),
[ResetFutureReceivedDateThreshold] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_Dataload__ResetFutureReceivedDateThreshold] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DealerPreference_Dataload] on [dbo].[DealerPreference_Dataload]'
GO
ALTER TABLE [dbo].[DealerPreference_Dataload] ADD CONSTRAINT [PK_DealerPreference_Dataload] PRIMARY KEY CLUSTERED  ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Audit_Exceptions]'
GO
CREATE TABLE [dbo].[Audit_Exceptions]
(
[Audit_ID] [int] NOT NULL,
[SourceTableID] [tinyint] NOT NULL,
[ExceptionRuleID] [int] NOT NULL,
[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_Audit_Exceptions_TimeStamp] DEFAULT (getdate())
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Audit_Exceptions] on [dbo].[Audit_Exceptions]'
GO
ALTER TABLE [dbo].[Audit_Exceptions] ADD CONSTRAINT [PK_Audit_Exceptions] PRIMARY KEY CLUSTERED  ([Audit_ID], [SourceTableID], [ExceptionRuleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerRisk]'
GO
CREATE TABLE [dbo].[DealerRisk]
(
[BusinessUnitId] [int] NOT NULL,
[RiskLevelNumberOfWeeks] [int] NOT NULL CONSTRAINT [DF_DealerRisk_RiskLevelNumberOfWeeks] DEFAULT (13),
[RiskLevelDealsThreshold] [int] NOT NULL CONSTRAINT [DF_DealerRisk_RiskLevelDealsThreshold] DEFAULT (3),
[RiskLevelNumberOfContributors] [int] NOT NULL CONSTRAINT [DF_DealerRisk_RiskLevelNumberOfContributors] DEFAULT (4),
[RedLightNoSaleThreshold] [int] NOT NULL CONSTRAINT [DF_DealerRisk_RedLightNoSaleThreshold] DEFAULT (60),
[RedLightGrossProfitThreshold] [int] NOT NULL CONSTRAINT [DF_DealerRisk_RedLightGrossProfitThreshold] DEFAULT (50),
[GreenLightNoSaleThreshold] [int] NOT NULL CONSTRAINT [DF_DealerRisk_GreenLightNoSaleThreshold] DEFAULT (40),
[GreenLightGrossProfitThreshold] [decimal] (8, 2) NOT NULL CONSTRAINT [DF_DealerRisk_GreenLightGrossProfitThreshold] DEFAULT (75),
[GreenLightMarginThreshold] [decimal] (8, 2) NOT NULL CONSTRAINT [DF_DealerRisk_GreenLightMarginThreshold] DEFAULT (25.0),
[GreenLightDaysPercentage] [int] NOT NULL CONSTRAINT [DF_DealerRisk_GreenLightDaysPercentage] DEFAULT (25),
[HighMileagePerYearThreshold] [int] NOT NULL CONSTRAINT [DF_DealerRisk__HighMileagePerYearThreshold] DEFAULT ((70000)),
[HighMileageThreshold] [int] NOT NULL CONSTRAINT [DF_DealerRisk__HighMileageThreshold] DEFAULT ((70000)),
[RiskLevelYearInitialTimePeriod] [int] NOT NULL CONSTRAINT [DF_DealerRisk_RiskLevelYearInitialTimePeriod] DEFAULT (6),
[RiskLevelYearSecondaryTimePeriod] [int] NOT NULL CONSTRAINT [DF_DealerRisk_RiskLevelYearSecondaryTimePeriod] DEFAULT (5),
[RiskLevelYearRollOverMonth] [int] NOT NULL CONSTRAINT [DF_DealerRisk_RiskLevelYearRollOverMonth] DEFAULT (9),
[RedLightTarget] [int] NOT NULL CONSTRAINT [DF_DealerRisk_RedLightTarget] DEFAULT (10),
[YellowLightTarget] [int] NOT NULL CONSTRAINT [DF_DealerRisk_YellowLightTarget] DEFAULT (35),
[GreenLightTarget] [int] NOT NULL CONSTRAINT [DF_DealerRisk_GreenLightTarget] DEFAULT (55),
[ExcessiveMileageThreshold] [int] NOT NULL CONSTRAINT [DF_DealerRisk__ExcessiveMileageThreshold] DEFAULT ((100000)),
[HighAgeThreshold] [tinyint] NOT NULL CONSTRAINT [DF_Dealer_Risk__HighAgeThreshold] DEFAULT ((6)),
[ExcessiveAgeThreshold] [tinyint] NOT NULL CONSTRAINT [DF_Dealer_Risk__ExcessiveAgeThreshold] DEFAULT ((7)),
[ExcessiveMileagePerYearThreshold] [int] NOT NULL CONSTRAINT [DF_Dealer_Risk__ExcessiveMileagePerYearThreshold] DEFAULT ((100000))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DealerRisk] on [dbo].[DealerRisk]'
GO
ALTER TABLE [dbo].[DealerRisk] ADD CONSTRAINT [PK_DealerRisk] PRIMARY KEY CLUSTERED  ([BusinessUnitId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[FLUSAN_EventLog]'
GO
CREATE TABLE [dbo].[FLUSAN_EventLog]
(
[FLUSAN_EventLogID] [int] NOT NULL IDENTITY(1, 1),
[FLUSAN_EventTypeCode] [tinyint] NOT NULL,
[BusinessUnitID] [int] NULL,
[MemberID] [int] NULL,
[ChannelID] [int] NULL,
[VIN] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[URL] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_FLUSAN_EventLog_DateCreated] DEFAULT (getdate())
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_FLUSAN_FLUSAN_EventLog] on [dbo].[FLUSAN_EventLog]'
GO
ALTER TABLE [dbo].[FLUSAN_EventLog] ADD CONSTRAINT [PK_FLUSAN_FLUSAN_EventLog] PRIMARY KEY CLUSTERED  ([FLUSAN_EventLogID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SubscriptionTypes]'
GO
CREATE TABLE [dbo].[SubscriptionTypes]
(
[SubscriptionTypeID] [tinyint] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Notes] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserSelectable] [tinyint] NOT NULL CONSTRAINT [DF__Subscript__UserS__60BE0156] DEFAULT (1),
[DefaultSubscriptionFrequencyDetailID] [tinyint] NOT NULL CONSTRAINT [DF__Subscript__Defau__61B2258F] DEFAULT (0),
[Active] [tinyint] NOT NULL CONSTRAINT [DF__Subscript__Activ__486754D8] DEFAULT (1)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_SubscriptionTypes] on [dbo].[SubscriptionTypes]'
GO
ALTER TABLE [dbo].[SubscriptionTypes] ADD CONSTRAINT [PK_SubscriptionTypes] PRIMARY KEY CLUSTERED  ([SubscriptionTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Subscriptions]'
GO
CREATE TABLE [dbo].[Subscriptions]
(
[SubscriptionID] [int] NOT NULL IDENTITY(1, 1),
[SubscriberID] [int] NOT NULL,
[SubscriberTypeID] [tinyint] NOT NULL,
[SubscriptionTypeID] [tinyint] NOT NULL,
[SubscriptionDeliveryTypeID] [tinyint] NOT NULL,
[BusinessUnitID] [int] NOT NULL CONSTRAINT [DF__Subscript__Busin__36FCD1B4] DEFAULT (0),
[SubscriptionFrequencyDetailID] [tinyint] NOT NULL CONSTRAINT [DF__Subscript__Subsc__37F0F5ED] DEFAULT (0)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Subscriptions] on [dbo].[Subscriptions]'
GO
ALTER TABLE [dbo].[Subscriptions] ADD CONSTRAINT [PK_Subscriptions] PRIMARY KEY CLUSTERED  ([SubscriptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ReportCenterSessions]'
GO
CREATE TABLE [dbo].[ReportCenterSessions]
(
[ReportCenterSessionId] [int] NOT NULL IDENTITY(1, 1),
[SessionId] [varchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[rcSessionId] [varchar] (42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BusinessUnitId] [int] NOT NULL,
[MemberId] [int] NOT NULL,
[AccessTime] [smalldatetime] NOT NULL CONSTRAINT [DF_ReportCenterSessions_RequestTime] DEFAULT (getdate()),
[DoAuthentication] [tinyint] NOT NULL CONSTRAINT [DF_ReportCenterSessions_DoAuthenication] DEFAULT (1)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ReportCenterSession] on [dbo].[ReportCenterSessions]'
GO
ALTER TABLE [dbo].[ReportCenterSessions] ADD CONSTRAINT [PK_ReportCenterSession] PRIMARY KEY CLUSTERED  ([ReportCenterSessionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Franchise]'
GO
CREATE TABLE [dbo].[Franchise]
(
[FranchiseID] [int] NOT NULL IDENTITY(10000, 1),
[FranchiseDescription] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ManufacturerID] [tinyint] NOT NULL CONSTRAINT [DF_Franchise_Manufacturer] DEFAULT (0)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Franchise] on [dbo].[Franchise]'
GO
ALTER TABLE [dbo].[Franchise] ADD CONSTRAINT [PK_Franchise] PRIMARY KEY NONCLUSTERED  ([FranchiseID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tbl_DealerATCAccessGroups]'
GO
CREATE TABLE [dbo].[tbl_DealerATCAccessGroups]
(
[DealerATCAccessGroupsId] [int] NOT NULL IDENTITY(10000, 1),
[BusinessUnitID] [int] NOT NULL,
[AccessGroupId] [int] NOT NULL,
[AccessGroupPriceId] [int] NOT NULL CONSTRAINT [DF_lu_AccessGroups_AccessGroupPriceId] DEFAULT (1),
[AccessGroupActorId] [int] NOT NULL CONSTRAINT [DF_lu_AccessGroups_AccessGroupActorId] DEFAULT (3),
[AccessGroupValue] [int] NOT NULL CONSTRAINT [DF_lu_AccessGroups_AccessGroupValue] DEFAULT (1)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tbl_DealerATCAccessGroups] on [dbo].[tbl_DealerATCAccessGroups]'
GO
ALTER TABLE [dbo].[tbl_DealerATCAccessGroups] ADD CONSTRAINT [PK_tbl_DealerATCAccessGroups] PRIMARY KEY CLUSTERED  ([DealerATCAccessGroupsId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[CertifiedVehicleBenefitLineItem]'
GO
CREATE TABLE [Marketing].[CertifiedVehicleBenefitLineItem]
(
[CertifiedVehicleBenefitPreferenceID] [int] NOT NULL,
[OwnerCertifiedProgramBenefitID] [int] NOT NULL,
[IsHighlight] [bit] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUser] [int] NULL,
[UpdateDate] [datetime] NULL,
[Rank] [int] NOT NULL CONSTRAINT [DF__CertifiedV__Rank__118E833F] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Marketing_CertifiedVehicleBenefitLineItem] on [Marketing].[CertifiedVehicleBenefitLineItem]'
GO
ALTER TABLE [Marketing].[CertifiedVehicleBenefitLineItem] ADD CONSTRAINT [PK_Marketing_CertifiedVehicleBenefitLineItem] PRIMARY KEY CLUSTERED  ([CertifiedVehicleBenefitPreferenceID], [OwnerCertifiedProgramBenefitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PhotosToBeDeleted]'
GO
CREATE TABLE [dbo].[PhotosToBeDeleted]
(
[PhotoID] [int] NOT NULL,
[LoggedOn] [datetime] NOT NULL CONSTRAINT [DF_PhotosToBeDeleted__LoggedOn] DEFAULT (getdate()),
[Reason] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PhotosToBeDeleted] on [dbo].[PhotosToBeDeleted]'
GO
ALTER TABLE [dbo].[PhotosToBeDeleted] ADD CONSTRAINT [PK_PhotosToBeDeleted] PRIMARY KEY CLUSTERED  ([PhotoID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[VehicleBookoutState]'
GO
CREATE TABLE [dbo].[VehicleBookoutState]
(
[VehicleBookoutStateID] [int] NOT NULL IDENTITY(1, 1),
[VIN] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF__VehicleBo__DateC__286564F1] DEFAULT (getdate())
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_VehicleBookoutState] on [dbo].[VehicleBookoutState]'
GO
ALTER TABLE [dbo].[VehicleBookoutState] ADD CONSTRAINT [PK_VehicleBookoutState] PRIMARY KEY CLUSTERED  ([VehicleBookoutStateID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[VehicleBookoutAdditionalState]'
GO
CREATE TABLE [dbo].[VehicleBookoutAdditionalState]
(
[VehicleBookoutStateID] [int] NOT NULL,
[ThirdPartySubCategoryID] [int] NOT NULL,
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF_VehicleBookoutAdditionalState__DateUpdated] DEFAULT (getdate())
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__VehicleBookoutAd__2A4DAD63] on [dbo].[VehicleBookoutAdditionalState]'
GO
ALTER TABLE [dbo].[VehicleBookoutAdditionalState] ADD CONSTRAINT [PK__VehicleBookoutAd__2A4DAD63] PRIMARY KEY CLUSTERED  ([VehicleBookoutStateID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[MarketAnalysisVehiclePreference]'
GO
CREATE TABLE [Marketing].[MarketAnalysisVehiclePreference]
(
[MarketAnalysisVehiclePreferenceId] [int] NOT NULL IDENTITY(1, 1),
[IsDisplayed] [bit] NOT NULL,
[ShowPricingGauge] [bit] NOT NULL,
[OwnerId] [int] NOT NULL,
[VehicleEntityId] [int] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUser] [int] NULL,
[UpdateDate] [datetime] NULL,
[VehicleEntityTypeId] [tinyint] NOT NULL,
[HasOverriddenPriceProviders] [bit] NOT NULL CONSTRAINT [DF_Marketing_MarketAnalysisVehiclePreference__HasOverriddenPriceProviders] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MarketAnalysisVehiclePreference] on [Marketing].[MarketAnalysisVehiclePreference]'
GO
ALTER TABLE [Marketing].[MarketAnalysisVehiclePreference] ADD CONSTRAINT [PK_MarketAnalysisVehiclePreference] PRIMARY KEY CLUSTERED  ([MarketAnalysisVehiclePreferenceId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tbl_CIAPreferences]'
GO
CREATE TABLE [dbo].[tbl_CIAPreferences]
(
[BusinessUnitId] [int] NOT NULL,
[PowerZoneGroupingThreshold] [int] NOT NULL CONSTRAINT [DF_tbl_CIAPreferences_PowerZoneGroupingThreshold] DEFAULT (10),
[BucketAllocationMinimumThreshold] [int] NOT NULL CONSTRAINT [DF_tbl_CIAPreferences_BucketAllocationMinimumThreshold] DEFAULT (5),
[TargetDaysSupply] [int] NOT NULL CONSTRAINT [DF__tbl_CIAPr__Targe__0B3292B8] DEFAULT (45),
[MarketPerformersDisplayThreshold] [int] NOT NULL CONSTRAINT [DF_tbl_CIAPreferences_MarketPerformersDisplayThreshold] DEFAULT (3),
[MarketPerformersInStockThreshold] [int] NOT NULL CONSTRAINT [DF_tbl_CIAPreferences_MarketPerformersInStockThreshold] DEFAULT (3),
[MarketPerformersUnitsThreshold] [int] NOT NULL CONSTRAINT [DF__tbl_CIAPr__Marke__1980B20F] DEFAULT (6),
[MarketPerformersZipCodeThreshold] [decimal] (9, 2) NOT NULL CONSTRAINT [DF__tbl_CIAPr__Marke__1A74D648] DEFAULT (0.8),
[GDLightProcessorTimePeriodId] [int] NOT NULL CONSTRAINT [DF_GDLightProcessorTimePeriodId] DEFAULT (1),
[SalesHistoryDisplayTimePeriodId] [int] NOT NULL CONSTRAINT [DF_SalesHistoryDisplayTimePeriodId] DEFAULT (5),
[CiaStoreTargetInventoryBasisPeriodId] [int] NOT NULL CONSTRAINT [DF_CiaStoreTargetInventoryBasisPeriodId] DEFAULT ((8)),
[CiaCoreModelDeterminationBasisPeriodId] [int] NOT NULL CONSTRAINT [DF_CiaCoreModelDeterminationBasisPeriodId] DEFAULT ((8)),
[CiaPowerzoneModelTargetInventoryBasisPeriodId] [int] NOT NULL CONSTRAINT [DF_CiaPowerzoneModelTargetInventoryBasisPeriodId] DEFAULT ((8)),
[CiaCoreModelYearAllocationBasisPeriodId] [int] NOT NULL CONSTRAINT [DF_CiaCoreModelYearAllocationBasisPeriodId] DEFAULT ((8))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tbl_CIAPreferences] on [dbo].[tbl_CIAPreferences]'
GO
ALTER TABLE [dbo].[tbl_CIAPreferences] ADD CONSTRAINT [PK_tbl_CIAPreferences] PRIMARY KEY CLUSTERED  ([BusinessUnitId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryBuckets]'
GO
CREATE TABLE [dbo].[InventoryBuckets]
(
[InventoryBucketID] [int] NOT NULL IDENTITY(1, 1),
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InventoryType] [tinyint] NOT NULL CONSTRAINT [DF_InventoryBuckets_InventoryType] DEFAULT (2)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryBuckets] on [dbo].[InventoryBuckets]'
GO
ALTER TABLE [dbo].[InventoryBuckets] ADD CONSTRAINT [PK_InventoryBuckets] PRIMARY KEY CLUSTERED  ([InventoryBucketID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryBucketRanges]'
GO
CREATE TABLE [dbo].[InventoryBucketRanges]
(
[InventoryBucketRangeID] [int] NOT NULL IDENTITY(1, 1),
[InventoryBucketID] [int] NOT NULL,
[RangeID] [tinyint] NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Low] [int] NOT NULL,
[High] [int] NULL,
[Lights] [tinyint] NOT NULL CONSTRAINT [DF_InventoryBucketRanges_Lights] DEFAULT (0),
[BusinessUnitID] [int] NOT NULL CONSTRAINT [DF_InventoryBucketRanges_BusinessUnitID] DEFAULT (100150),
[LongDescription] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SortOrder] [tinyint] NOT NULL CONSTRAINT [DF_InventoryBucketRanges_SortOrder] DEFAULT (0),
[Value1] [smallint] NULL CONSTRAINT [DF__Inventory__Value__65AE6A53] DEFAULT (NULL),
[Value2] [smallint] NULL CONSTRAINT [DF__Inventory__Value__66A28E8C] DEFAULT (NULL),
[Value3] [smallint] NULL CONSTRAINT [DF__Inventory__Value__6796B2C5] DEFAULT (NULL)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryBucketRanges] on [dbo].[InventoryBucketRanges]'
GO
ALTER TABLE [dbo].[InventoryBucketRanges] ADD CONSTRAINT [PK_InventoryBucketRanges] PRIMARY KEY CLUSTERED  ([InventoryBucketRangeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UQ_InventoryBucketRanges] on [dbo].[InventoryBucketRanges]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_InventoryBucketRanges] ON [dbo].[InventoryBucketRanges] ([InventoryBucketID], [RangeID], [BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[MarketListingVehiclePreference]'
GO
CREATE TABLE [Marketing].[MarketListingVehiclePreference]
(
[MarketListingVehiclePreferenceId] [int] NOT NULL IDENTITY(1, 1),
[VehicleEntityId] [int] NOT NULL,
[VehicleEntityTypeId] [tinyint] NOT NULL,
[Ownerid] [int] NOT NULL,
[IsDisplayed] [bit] NOT NULL,
[ShowDealerName] [bit] NOT NULL,
[ShowCertifiedIndicator] [bit] NOT NULL,
[MileageLow] [int] NOT NULL,
[MileageHigh] [int] NOT NULL,
[PriceLow] [int] NOT NULL,
[PriceHigh] [int] NOT NULL,
[InsertUserID] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUserId] [int] NOT NULL,
[UpdateDate] [datetime] NOT NULL,
[Version] [timestamp] NOT NULL,
[SortCode] [int] NOT NULL CONSTRAINT [DF__MarketLis__SortC__2B194B18] DEFAULT ((1)),
[SortAscending] [bit] NOT NULL CONSTRAINT [DF__MarketLis__SortA__2C0D6F51] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MarketListingVehiclePreference] on [Marketing].[MarketListingVehiclePreference]'
GO
ALTER TABLE [Marketing].[MarketListingVehiclePreference] ADD CONSTRAINT [PK_MarketListingVehiclePreference] PRIMARY KEY CLUSTERED  ([MarketListingVehiclePreferenceId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[HalSessions]'
GO
CREATE TABLE [dbo].[HalSessions]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[HalSessionID] [varchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BusinessUnitID] [int] NOT NULL,
[DateModified] [smalldatetime] NOT NULL CONSTRAINT [DF_HalSession_Modified] DEFAULT (getdate())
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__HalSessions__6CCF9DD7] on [dbo].[HalSessions]'
GO
ALTER TABLE [dbo].[HalSessions] ADD CONSTRAINT [PK__HalSessions__6CCF9DD7] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tbl_GroupingPromotionPlan]'
GO
CREATE TABLE [dbo].[tbl_GroupingPromotionPlan]
(
[GroupingPromotionPlanId] [int] NOT NULL IDENTITY(1, 1),
[GroupingDescriptionId] [int] NOT NULL,
[PromotionStartDate] [smalldatetime] NOT NULL,
[PromotionEndDate] [smalldatetime] NOT NULL,
[Notes] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BusinessUnitID] [int] NOT NULL,
[Created] [smalldatetime] NOT NULL CONSTRAINT [DF_GroupingPromotionPlan_Created] DEFAULT (getdate())
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_GroupingPromotionPlan] on [dbo].[tbl_GroupingPromotionPlan]'
GO
ALTER TABLE [dbo].[tbl_GroupingPromotionPlan] ADD CONSTRAINT [PK_GroupingPromotionPlan] PRIMARY KEY CLUSTERED  ([GroupingPromotionPlanId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[MarketShare]'
GO
CREATE TABLE [dbo].[MarketShare]
(
[MarketShareID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[GroupingDescriptionID] [int] NOT NULL,
[VehicleYear] [int] NOT NULL,
[MarketShare] [decimal] (8, 2) NOT NULL,
[NumberOfWeeks] [int] NOT NULL CONSTRAINT [DF_MarketShare_NumberOfWeeks] DEFAULT (26)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MarketShare] on [dbo].[MarketShare]'
GO
ALTER TABLE [dbo].[MarketShare] ADD CONSTRAINT [PK_MarketShare] PRIMARY KEY CLUSTERED  ([MarketShareID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[MarketplaceSubmission]'
GO
CREATE TABLE [dbo].[MarketplaceSubmission]
(
[MarketplaceSubmissionID] [int] NOT NULL IDENTITY(1, 1),
[MarketplaceID] [int] NOT NULL,
[InventoryID] [int] NOT NULL,
[MemberID] [int] NOT NULL,
[MarketplaceSubmissionStatusCD] [tinyint] NOT NULL,
[URL] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BeginEffectiveDate] [datetime] NOT NULL,
[EndEffectiveDate] [datetime] NOT NULL,
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_MarketplaceSubmission_DateCreated] DEFAULT (getdate())
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MarketplaceSubmission] on [dbo].[MarketplaceSubmission]'
GO
ALTER TABLE [dbo].[MarketplaceSubmission] ADD CONSTRAINT [PK_MarketplaceSubmission] PRIMARY KEY CLUSTERED  ([MarketplaceSubmissionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[EquityAnalyzerSearches]'
GO
CREATE TABLE [dbo].[EquityAnalyzerSearches]
(
[EquityAnalyzerSearchID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ThirdPartyCategoryID] [int] NOT NULL,
[PercentMultiplier] [tinyint] NOT NULL CONSTRAINT [DF_EquityAnalyzerSearches_Multiplier] DEFAULT (100)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_EquityAnalyzerSearches] on [dbo].[EquityAnalyzerSearches]'
GO
ALTER TABLE [dbo].[EquityAnalyzerSearches] ADD CONSTRAINT [PK_EquityAnalyzerSearches] PRIMARY KEY NONCLUSTERED  ([EquityAnalyzerSearchID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerGroupPreference]'
GO
CREATE TABLE [dbo].[DealerGroupPreference]
(
[DealerGroupPreferenceID] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitID] [int] NOT NULL,
[AgingPolicy] [int] NULL,
[AgingPolicyAdjustment] [int] NULL,
[IncludeRedistribution] [tinyint] NULL CONSTRAINT [DF_DealerGroupPreference_IncludeRedistribution] DEFAULT (0),
[InventoryExchangeAgeLimit] [int] NULL CONSTRAINT [DF_DealerGroupPreference_InventoryExchangeAgeLimit] DEFAULT (0),
[IncludeRoundTable] [tinyint] NULL,
[IncludeCIA] [tinyint] NULL,
[IncludeRedLightAgingPlan] [tinyint] NULL,
[PricePointDealsThreshold] [int] NULL,
[LithiaStore] [tinyint] NOT NULL CONSTRAINT [DF_DealerGroupPreference_LithiaStore] DEFAULT (0),
[ShowAppraisalFormOffer] [tinyint] NULL,
[ShowAppraisalValue] [tinyint] NULL,
[IncludePerformanceManagementCenter] [bit] NOT NULL,
[IncludeTransferPricing] [bit] NOT NULL CONSTRAINT [DF_DealerGroupPreference_IncludeTransferPricing] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DealerGroupPreference] on [dbo].[DealerGroupPreference]'
GO
ALTER TABLE [dbo].[DealerGroupPreference] ADD CONSTRAINT [PK_DealerGroupPreference] PRIMARY KEY CLUSTERED  ([DealerGroupPreferenceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_DealerGroupPreference] on [dbo].[DealerGroupPreference]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_DealerGroupPreference] ON [dbo].[DealerGroupPreference] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerCIAPreferences]'
GO
CREATE TABLE [dbo].[DealerCIAPreferences]
(
[DealerCIAPreferencesID] [int] NOT NULL IDENTITY(100000, 1),
[BusinessUnitID] [int] NOT NULL,
[ShortAgeBandID] [int] NULL CONSTRAINT [DF_ShortAgeBandID] DEFAULT (0),
[LongAgeBandID] [int] NULL CONSTRAINT [DF_LongAgeBandID] DEFAULT (0),
[ForecastAgeBandID] [int] NULL CONSTRAINT [DF_ForecastAgeBandID] DEFAULT (0),
[ShortAgeBandWeight] [int] NULL CONSTRAINT [DF_ShortAgeBandWeight] DEFAULT (0),
[LongAgeBandWeight] [int] NULL CONSTRAINT [DF_LongAgeBandWeight] DEFAULT (0),
[ForecastAgeBandWeight] [int] NULL CONSTRAINT [DF_ForecastAgeBandWeight] DEFAULT (0),
[NumberOfPowerZoneMakeModels] [int] NULL CONSTRAINT [DF_NumberOfPowerZoneMakeModels] DEFAULT (0),
[MinRecommendedDaysSupply] [int] NULL CONSTRAINT [DF_MinRecommendedDaysSupply] DEFAULT (0),
[MaxRecommendedDaysSupply] [int] NULL CONSTRAINT [DF_MaxRecommendedDaysSupply] DEFAULT (0),
[ApplyNoSalePctThreshold] [tinyint] NULL CONSTRAINT [DF_ApplyNoSalePctThreshold] DEFAULT (0),
[NoSalePctThreshold] [int] NULL CONSTRAINT [DF_NoSalePctThreshold] DEFAULT (0),
[ThresholdOnDaysSupply] [decimal] (8, 1) NULL,
[MinRecommendedStockingLevel] [int] NULL CONSTRAINT [DF_MinRecommendedStockingLevel] DEFAULT (0),
[MaxRecommendedStockingLevel] [int] NULL CONSTRAINT [DF_MaxRecommendedStockingLevel] DEFAULT (0),
[ShortAgeBandUnitsSoldThreshold] [int] NULL CONSTRAINT [DF_ShortAgeBandUnitsSoldThreshold] DEFAULT (0),
[LongAgeBandUnitsSoldThreshold] [int] NULL CONSTRAINT [DF_LongAgeBandUnitsSoldThreshold] DEFAULT (0),
[ForecastAgeBandUnitsSoldThreshold] [int] NULL CONSTRAINT [DF_ForecastAgeBandUnitsSoldThreshold] DEFAULT (0),
[MetricAverageGrossProfit] [tinyint] NULL,
[MetricAverageFrontEndGrossProfitPercentage] [int] NULL,
[MetricAverageBackEndGrossProfitPercentage] [int] NULL,
[MetricAverageCombinedGrossProfitPercentage] [int] NULL,
[MetricAverageGrossProfitCombined] [tinyint] NULL,
[MetricAverageUnitCost] [tinyint] NULL,
[MetricAverageMileage] [tinyint] NULL,
[MetricAverageSalePrice] [tinyint] NULL,
[MetricAverageDaysToSale] [tinyint] NULL,
[MetricAverageMargin] [tinyint] NULL,
[MetricPercentOutOfStock] [tinyint] NULL,
[MetricAdjustedGrossProfit] [tinyint] NULL,
[MetricContribution] [tinyint] NULL,
[MetricContributionFrontEndPercentage] [int] NULL,
[MetricContributionBackEndPercentage] [int] NULL,
[MetricContributionCombinedPercentage] [int] NULL,
[MetricContributionCombined] [tinyint] NULL,
[MetricContributionNoSaleLossFactor] [tinyint] NULL,
[MetricAverageUnitCostPercentage] [int] NULL,
[MetricAverageMileagePercentage] [int] NULL,
[MetricAverageSalePricePercentage] [int] NULL,
[MetricAverageDaysToSalePercentage] [int] NULL,
[MetricAverageMarginPercentage] [int] NULL,
[MetricPercentOutOfStockPercentage] [int] NULL,
[MetricAdjustedGrossProfitPercentage] [int] NULL,
[MetricInventoryHoldingAmount] [int] NULL,
[WinnerScoreThreshold] [int] NULL,
[MaxCIAVehicles] [int] NULL,
[PowerZoneParentRankPercentage] [int] NULL,
[MarketRankPercentage] [int] NULL,
[ModelPerformance] [tinyint] NULL,
[ModelPerformancePercentage] [int] NULL,
[SalesPerMonthFirstLook] [tinyint] NULL,
[SalesPerMonthFirstLookPercentage] [int] NULL,
[SalesPerMonthSimilar] [tinyint] NULL,
[SalesPerMonthSimilarPercentage] [int] NULL,
[MinWeightVariable] [int] NULL,
[MaxWeightVariable] [int] NULL,
[DisplayGoodBets] [tinyint] NULL,
[GoodBetScoreThreshold] [int] NULL CONSTRAINT [DF__DealerCIA__GoodB__094B159D] DEFAULT (8),
[ExcludeRedLightVehicle] [tinyint] NULL,
[GoodBetMinTargetRange] [int] NULL,
[GoodBetMaxTargetRange] [int] NULL,
[UseInventoryHolding] [tinyint] NOT NULL,
[RunDayOfWeek] [int] NOT NULL,
[Modifiable] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DealerCIAPreferences] on [dbo].[DealerCIAPreferences]'
GO
ALTER TABLE [dbo].[DealerCIAPreferences] ADD CONSTRAINT [PK_DealerCIAPreferences] PRIMARY KEY NONCLUSTERED  ([DealerCIAPreferencesID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PrintAdvertiser_ThirdPartyEntity]'
GO
CREATE TABLE [dbo].[PrintAdvertiser_ThirdPartyEntity]
(
[PrintAdvertiserID] [int] NOT NULL,
[ContactEmail] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FaxNumber] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisplayPrice] [tinyint] NOT NULL CONSTRAINT [DF_PrintAdvertiser_ThirdPartyEntity_DisplayPrice] DEFAULT (1),
[VehicleOptionThirdPartyID] [tinyint] NOT NULL,
[QuickAdvertiser] [tinyint] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PrintAdvertiser_ThirdPartyEntity] on [dbo].[PrintAdvertiser_ThirdPartyEntity]'
GO
ALTER TABLE [dbo].[PrintAdvertiser_ThirdPartyEntity] ADD CONSTRAINT [PK_PrintAdvertiser_ThirdPartyEntity] PRIMARY KEY CLUSTERED  ([PrintAdvertiserID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PrintAdvertiserTextOptionDefault]'
GO
CREATE TABLE [dbo].[PrintAdvertiserTextOptionDefault]
(
[PrintAdvertiserTextOptionDefaultID] [int] NOT NULL IDENTITY(1, 1),
[PrintAdvertiserID] [int] NOT NULL,
[PrintAdvertiserTextOptionID] [tinyint] NOT NULL,
[DisplayOrder] [tinyint] NOT NULL,
[Status] [tinyint] NOT NULL CONSTRAINT [DF_PrintAdvertiserTextOptionDefault_Status] DEFAULT (0)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PrintAdvertiserTextOptionDefault] on [dbo].[PrintAdvertiserTextOptionDefault]'
GO
ALTER TABLE [dbo].[PrintAdvertiserTextOptionDefault] ADD CONSTRAINT [PK_PrintAdvertiserTextOptionDefault] PRIMARY KEY CLUSTERED  ([PrintAdvertiserTextOptionDefaultID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Marketing].[MarketAnalysisPreference]'
GO
CREATE TABLE [Marketing].[MarketAnalysisPreference]
(
[MarketAnalysisPreferenceId] [int] NOT NULL IDENTITY(1, 1),
[OwnerId] [int] NULL,
[NumberOfPrices] [tinyint] NOT NULL,
[UseCustomCurrentInternetPrice] [bit] NOT NULL CONSTRAINT [DF_Marketing_MarketAnalysisPreference__UseCustomCurrentInternetPrice] DEFAULT ((0)),
[CurrentInternetPriceDisplayName] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UseCustomOfferPrice] [bit] NOT NULL CONSTRAINT [DF_Marketing_MarketAnalysisPreference__UseCustomOfferPrice] DEFAULT ((0)),
[OfferPriceDisplayName] [varchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UseCustomComparisonPricePriority] [bit] NOT NULL,
[InsertUser] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUser] [int] NULL,
[UpdateDate] [datetime] NULL,
[OriginalListPriceCalculationOverride] [bit] NOT NULL CONSTRAINT [DF__MarketAna__Origi__1EB37433] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_MarketAnalysisPreference] on [Marketing].[MarketAnalysisPreference]'
GO
ALTER TABLE [Marketing].[MarketAnalysisPreference] ADD CONSTRAINT [PK_MarketAnalysisPreference] PRIMARY KEY CLUSTERED  ([MarketAnalysisPreferenceId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BusinessUnitStatus]'
GO
CREATE TABLE [dbo].[BusinessUnitStatus]
(
[BusinessUnitID] [int] NOT NULL,
[BusinessUnitStatusCodeID] [tinyint] NOT NULL CONSTRAINT [DF_BusinessUnitStatus__BusinessUnitStatusCodeID] DEFAULT ((0)),
[DateDeleted] [smalldatetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_BusinessUnitStatus] on [dbo].[BusinessUnitStatus]'
GO
ALTER TABLE [dbo].[BusinessUnitStatus] ADD CONSTRAINT [PK_BusinessUnitStatus] PRIMARY KEY CLUSTERED  ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inventory_ListPriceHistory]'
GO
CREATE TABLE [dbo].[Inventory_ListPriceHistory]
(
[ListPriceHistoryID] [int] NOT NULL IDENTITY(1, 1),
[InventoryID] [int] NOT NULL,
[DMSReferenceDT] [smalldatetime] NOT NULL,
[ListPrice] [decimal] (8, 2) NULL,
[UpdateLocked] [bit] NOT NULL CONSTRAINT [DF_Inventory_ListPriceHistory_UpdateLocked] DEFAULT (0)
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_Inventory_ListPriceHistory_InventoryID] on [dbo].[Inventory_ListPriceHistory]'
GO
CREATE CLUSTERED INDEX [IX_Inventory_ListPriceHistory_InventoryID] ON [dbo].[Inventory_ListPriceHistory] ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Inventory_ListPriceHistory] on [dbo].[Inventory_ListPriceHistory]'
GO
ALTER TABLE [dbo].[Inventory_ListPriceHistory] ADD CONSTRAINT [PK_Inventory_ListPriceHistory] PRIMARY KEY NONCLUSTERED  ([ListPriceHistoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PurgeManagement]'
GO
CREATE TABLE [dbo].[PurgeManagement]
(
[Subject] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Period] [tinyint] NOT NULL,
[PeriodType] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Routine] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastCall] [smalldatetime] NOT NULL CONSTRAINT [DF_PurgeManagement] DEFAULT ('Jan 1, 1980')
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PurgeManagement] on [dbo].[PurgeManagement]'
GO
ALTER TABLE [dbo].[PurgeManagement] ADD CONSTRAINT [PK_PurgeManagement] PRIMARY KEY CLUSTERED  ([Subject])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PurgeLog]'
GO
CREATE TABLE [dbo].[PurgeLog]
(
[Subject] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Executed] [datetime] NOT NULL CONSTRAINT [DF_PurgeLog__Executed] DEFAULT (getdate()),
[ItemsDeleted] [int] NOT NULL,
[First] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Last] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PurgeLog] on [dbo].[PurgeLog]'
GO
ALTER TABLE [dbo].[PurgeLog] ADD CONSTRAINT [PK_PurgeLog] PRIMARY KEY NONCLUSTERED  ([Subject], [Executed])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InventoryTransferPriceHistory]'
GO
CREATE TABLE [dbo].[InventoryTransferPriceHistory]
(
[InventoryTransferHistoryID] [int] NOT NULL IDENTITY(1, 1),
[InventoryID] [int] NOT NULL,
[TransferPrice_Old] [decimal] (9, 2) NULL,
[TransferPrice_New] [decimal] (9, 2) NULL,
[Created] [datetime] NOT NULL CONSTRAINT [DF_InventoryTransferHistory_Created] DEFAULT (getdate()),
[CreatedBy] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InventoryTransferHistory] on [dbo].[InventoryTransferPriceHistory]'
GO
ALTER TABLE [dbo].[InventoryTransferPriceHistory] ADD CONSTRAINT [PK_InventoryTransferHistory] PRIMARY KEY CLUSTERED  ([InventoryTransferHistoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[OldBuyingAlertsSubscriptions]'
GO
CREATE TABLE [dbo].[OldBuyingAlertsSubscriptions]
(
[SubscriptionID] [int] NOT NULL,
[SubscriberID] [int] NOT NULL,
[SubscriberTypeID] [tinyint] NOT NULL,
[SubscriptionTypeID] [tinyint] NOT NULL,
[SubscriptionDeliveryTypeID] [tinyint] NOT NULL,
[BusinessUnitID] [int] NOT NULL CONSTRAINT [DF__OldBuying__Busin__303B90E3] DEFAULT ((0)),
[SubscriptionFrequencyDetailID] [tinyint] NOT NULL CONSTRAINT [DF__OldBuying__Subsc__312FB51C] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_OldBuyingAlertsSubscriptions] on [dbo].[OldBuyingAlertsSubscriptions]'
GO
ALTER TABLE [dbo].[OldBuyingAlertsSubscriptions] ADD CONSTRAINT [PK_OldBuyingAlertsSubscriptions] PRIMARY KEY CLUSTERED  ([SubscriptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerPreference_KBBConsumerTool]'
GO
CREATE TABLE [dbo].[DealerPreference_KBBConsumerTool]
(
[BusinessUnitID] [int] NOT NULL,
[ShowRetail] [bit] NOT NULL CONSTRAINT [DF_DealerPreference_KBBConsumerTool__ShowRetail] DEFAULT ((0)),
[ShowTradeIn] [bit] NOT NULL CONSTRAINT [DF_DealerPreference_KBBConsumerTool__ShowTradeIn] DEFAULT ((1))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DealerPreference_KBBConsumerTool] on [dbo].[DealerPreference_KBBConsumerTool]'
GO
ALTER TABLE [dbo].[DealerPreference_KBBConsumerTool] ADD CONSTRAINT [PK_DealerPreference_KBBConsumerTool] PRIMARY KEY CLUSTERED  ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerPreference_InternetAdvertisementBuilder]'
GO
CREATE TABLE [dbo].[DealerPreference_InternetAdvertisementBuilder]
(
[BusinessUnitID] [int] NOT NULL,
[EnableBuilder] [bit] NOT NULL CONSTRAINT [DF_DP_IAB_EnableBuilder] DEFAULT ((1)),
[EnableTagLine] [bit] NOT NULL CONSTRAINT [DF_DP_IAB_EnableTagLine] DEFAULT ((0)),
[TagLine] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SpacerText] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HighValueEquipmentThreshold] [smallint] NULL,
[HighValueEquipmentPrefix] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StandardEquipmentPrefix] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DP_InternetAdvertisementBuilder_BusinessUnitID] on [dbo].[DealerPreference_InternetAdvertisementBuilder]'
GO
ALTER TABLE [dbo].[DealerPreference_InternetAdvertisementBuilder] ADD CONSTRAINT [PK_DP_InternetAdvertisementBuilder_BusinessUnitID] PRIMARY KEY CLUSTERED  ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[DealerPreference_Pricing]'
GO
CREATE TABLE [dbo].[DealerPreference_Pricing]
(
[BusinessUnitID] [int] NOT NULL,
[PingII_DefaultSearchRadius] [int] NULL CONSTRAINT [DF__DealerPre__PingI__7A9478A0] DEFAULT (NULL),
[PingII_MaxSearchRadius] [int] NULL CONSTRAINT [DF__DealerPre__PingI__7B889CD9] DEFAULT (NULL),
[PingII_SupressSellerName] [tinyint] NULL CONSTRAINT [DF__DealerPre__PingI__7C7CC112] DEFAULT (NULL),
[PingII_ExcludeNoPriceFromCalc] [tinyint] NULL CONSTRAINT [DF__DealerPre__PingI__7D70E54B] DEFAULT (NULL),
[PingII_ExcludeLowPriceOutliersMultiplier] [decimal] (4, 2) NULL CONSTRAINT [DF__DealerPre__PingI__7E650984] DEFAULT (NULL),
[PingII_ExcludeHighPriceOutliersMultiplier] [decimal] (4, 2) NULL CONSTRAINT [DF__DealerPre__PingI__7F592DBD] DEFAULT (NULL),
[PingII_GuideBookId] [tinyint] NULL,
[MarketDaysSupplyBasePeriod] [int] NULL,
[VehicleSalesBasePeriod] [int] NULL,
[VehicleUnwindPeriod] [int] NULL,
[PackageID] [tinyint] NOT NULL,
[MatchCertifiedByDefault] [bit] NOT NULL,
[SuppressTrimMatchStatus] [bit] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_DP_Pricing_BusinessUnitId] on [dbo].[DealerPreference_Pricing]'
GO
ALTER TABLE [dbo].[DealerPreference_Pricing] ADD CONSTRAINT [PK_DP_Pricing_BusinessUnitId] PRIMARY KEY CLUSTERED  ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Carfax].[ReportProcessorCommandType]'
GO
CREATE TABLE [Carfax].[ReportProcessorCommandType]
(
[ReportProcessorCommandTypeID] [tinyint] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Carfax_ReportProcessorCommandType] on [Carfax].[ReportProcessorCommandType]'
GO
ALTER TABLE [Carfax].[ReportProcessorCommandType] ADD CONSTRAINT [PK_Carfax_ReportProcessorCommandType] PRIMARY KEY CLUSTERED  ([ReportProcessorCommandTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UK_Carfax_ReportProcessorCommandType__Name] on [Carfax].[ReportProcessorCommandType]'
GO
CREATE UNIQUE NONCLUSTERED INDEX [UK_Carfax_ReportProcessorCommandType__Name] ON [Carfax].[ReportProcessorCommandType] ([Name])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[BookoutProcessorRunLog_Failures]'
GO
CREATE TABLE [dbo].[BookoutProcessorRunLog_Failures]
(
[BookoutProcessorRunId] [int] NOT NULL IDENTITY(1, 1),
[BusinessUnitId] [int] NOT NULL,
[ThirdPartyId] [tinyint] NOT NULL,
[BookoutProcessorModeId] [tinyint] NOT NULL,
[BookoutProcessorStatusId] [tinyint] NOT NULL,
[LoadTime] [smalldatetime] NULL,
[StartTime] [smalldatetime] NULL,
[EndTime] [smalldatetime] NULL,
[ServerName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SuccessfulBookouts] [int] NULL,
[FailedBookouts] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[CarFaxRerun]'
GO
CREATE TABLE [dbo].[CarFaxRerun]
(
[VIN] [char] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Username] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[pw] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HotListed] [bit] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ChangedVics]'
GO
CREATE TABLE [dbo].[ChangedVics]
(
[ChangedVicID] [int] NOT NULL IDENTITY(100000, 1),
[OldVic] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NewVic] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ChangedVics] on [dbo].[ChangedVics]'
GO
ALTER TABLE [dbo].[ChangedVics] ADD CONSTRAINT [PK_ChangedVics] PRIMARY KEY NONCLUSTERED  ([ChangedVicID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[GlobalParam]'
GO
CREATE TABLE [dbo].[GlobalParam]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[name] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[value] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_GlobalParam] on [dbo].[GlobalParam]'
GO
ALTER TABLE [dbo].[GlobalParam] ADD CONSTRAINT [PK_GlobalParam] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InsightAdjectival]'
GO
CREATE TABLE [dbo].[InsightAdjectival]
(
[InsightAdjectivalID] [int] NOT NULL,
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InsightAdjectival] on [dbo].[InsightAdjectival]'
GO
ALTER TABLE [dbo].[InsightAdjectival] ADD CONSTRAINT [PK_InsightAdjectival] PRIMARY KEY CLUSTERED  ([InsightAdjectivalID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[InsightReflection]'
GO
CREATE TABLE [dbo].[InsightReflection]
(
[InsightReflectionID] [int] NOT NULL,
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_InsightReflection] on [dbo].[InsightReflection]'
GO
ALTER TABLE [dbo].[InsightReflection] ADD CONSTRAINT [PK_InsightReflection] PRIMARY KEY CLUSTERED  ([InsightReflectionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Inventory_Level4Analysis]'
GO
CREATE TABLE [dbo].[Inventory_Level4Analysis]
(
[InventoryID] [int] NOT NULL,
[Level4Analysis] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Inventory_Level4Analysis] on [dbo].[Inventory_Level4Analysis]'
GO
ALTER TABLE [dbo].[Inventory_Level4Analysis] ADD CONSTRAINT [PK_Inventory_Level4Analysis] PRIMARY KEY CLUSTERED  ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[KelleyBadVin]'
GO
CREATE TABLE [dbo].[KelleyBadVin]
(
[KelleyBadVinId] [int] NOT NULL IDENTITY(10000, 1),
[VIN] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_KelleyBadVin] on [dbo].[KelleyBadVin]'
GO
ALTER TABLE [dbo].[KelleyBadVin] ADD CONSTRAINT [PK_KelleyBadVin] PRIMARY KEY NONCLUSTERED  ([KelleyBadVinId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_AccessGroupActor]'
GO
CREATE TABLE [dbo].[lu_AccessGroupActor]
(
[AccessGroupActorId] [int] NOT NULL,
[Description] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_AccessGroupActor] on [dbo].[lu_AccessGroupActor]'
GO
ALTER TABLE [dbo].[lu_AccessGroupActor] ADD CONSTRAINT [PK_lu_AccessGroupActor] PRIMARY KEY NONCLUSTERED  ([AccessGroupActorId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_AccessGroupPrice]'
GO
CREATE TABLE [dbo].[lu_AccessGroupPrice]
(
[AccessGroupPriceId] [int] NOT NULL,
[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_AccessGroupPrice] on [dbo].[lu_AccessGroupPrice]'
GO
ALTER TABLE [dbo].[lu_AccessGroupPrice] ADD CONSTRAINT [PK_lu_AccessGroupPrice] PRIMARY KEY NONCLUSTERED  ([AccessGroupPriceId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_AnalyticalEngineStatus]'
GO
CREATE TABLE [dbo].[lu_AnalyticalEngineStatus]
(
[analyticalenginestatus_CD] [tinyint] NOT NULL,
[analyticalenginestatus_DESC] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_DealStatus]'
GO
CREATE TABLE [dbo].[lu_DealStatus]
(
[DealStatusCD] [tinyint] NOT NULL,
[DealStatusDesc] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_DealStatus] on [dbo].[lu_DealStatus]'
GO
ALTER TABLE [dbo].[lu_DealStatus] ADD CONSTRAINT [PK_lu_DealStatus] PRIMARY KEY NONCLUSTERED  ([DealStatusCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_DecodedSquishVinSource]'
GO
CREATE TABLE [dbo].[lu_DecodedSquishVinSource]
(
[dsvSourceID] [tinyint] NOT NULL,
[dsvSourceDESC] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_DecodedSquishVinSource] on [dbo].[lu_DecodedSquishVinSource]'
GO
ALTER TABLE [dbo].[lu_DecodedSquishVinSource] ADD CONSTRAINT [PK_lu_DecodedSquishVinSource] PRIMARY KEY NONCLUSTERED  ([dsvSourceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_Disposition]'
GO
CREATE TABLE [dbo].[lu_Disposition]
(
[DispositionCD] [tinyint] NOT NULL,
[DispositionDESC] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_DMIVehicleStatus]'
GO
CREATE TABLE [dbo].[lu_DMIVehicleStatus]
(
[DMIVehicleStatusCD] [smallint] NOT NULL IDENTITY(1, 1),
[DMIVehicleStatusDESC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_lu_DMIVehicleStatus] on [dbo].[lu_DMIVehicleStatus]'
GO
ALTER TABLE [dbo].[lu_DMIVehicleStatus] ADD CONSTRAINT [PK_lu_lu_DMIVehicleStatus] PRIMARY KEY NONCLUSTERED  ([DMIVehicleStatusCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_FuelType]'
GO
CREATE TABLE [dbo].[lu_FuelType]
(
[FuelTypeCD] [smallint] NOT NULL IDENTITY(1, 1),
[FuelTypeDESC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_FuelType] on [dbo].[lu_FuelType]'
GO
ALTER TABLE [dbo].[lu_FuelType] ADD CONSTRAINT [PK_lu_FuelType] PRIMARY KEY NONCLUSTERED  ([FuelTypeCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_OptionType]'
GO
CREATE TABLE [dbo].[lu_OptionType]
(
[Id] [tinyint] NOT NULL,
[Description] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__lu_OptionType__3726238F] on [dbo].[lu_OptionType]'
GO
ALTER TABLE [dbo].[lu_OptionType] ADD CONSTRAINT [PK__lu_OptionType__3726238F] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_SaleDescription]'
GO
CREATE TABLE [dbo].[lu_SaleDescription]
(
[SaleDescriptionCD] [tinyint] NOT NULL,
[SaleDescription] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_SaleDescription] on [dbo].[lu_SaleDescription]'
GO
ALTER TABLE [dbo].[lu_SaleDescription] ADD CONSTRAINT [PK_lu_SaleDescription] PRIMARY KEY NONCLUSTERED  ([SaleDescriptionCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [UQ_lu_SaleDescription_SaleDescription] on [dbo].[lu_SaleDescription]'
GO
CREATE NONCLUSTERED INDEX [UQ_lu_SaleDescription_SaleDescription] ON [dbo].[lu_SaleDescription] ([SaleDescription])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_States]'
GO
CREATE TABLE [dbo].[lu_States]
(
[StateID] [tinyint] NOT NULL,
[StateShort] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StateLong] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_States] on [dbo].[lu_States]'
GO
ALTER TABLE [dbo].[lu_States] ADD CONSTRAINT [PK_lu_States] PRIMARY KEY NONCLUSTERED  ([StateID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[lu_Status]'
GO
CREATE TABLE [dbo].[lu_Status]
(
[StatusId] [tinyint] NOT NULL,
[StatusName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_Status] on [dbo].[lu_Status]'
GO
ALTER TABLE [dbo].[lu_Status] ADD CONSTRAINT [PK_lu_Status] PRIMARY KEY NONCLUSTERED  ([StatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[Manufacturer]'
GO
CREATE TABLE [dbo].[Manufacturer]
(
[ManufacturerID] [tinyint] NOT NULL,
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ShortName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[MessageCenter]'
GO
CREATE TABLE [dbo].[MessageCenter]
(
[Ordering] [tinyint] NOT NULL,
[Title] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Body] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HasPriority] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[MessageCenterAlert]'
GO
CREATE TABLE [dbo].[MessageCenterAlert]
(
[Ordering] [tinyint] NOT NULL,
[Title] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Body] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HasPriority] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PING_GroupingCodeMapping]'
GO
CREATE TABLE [dbo].[PING_GroupingCodeMapping]
(
[CodeID] [int] NULL,
[GroupingDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Make] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SegmentID] [tinyint] NULL,
[Line] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstlookModel] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstlookLine] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstlookMake] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PING_GroupingCodes]'
GO
CREATE TABLE [dbo].[PING_GroupingCodes]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[CodeId] [int] NOT NULL,
[MakeModelGroupingId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_PING_GroupingCodes] on [dbo].[PING_GroupingCodes]'
GO
CREATE UNIQUE CLUSTERED INDEX [IX_PING_GroupingCodes] ON [dbo].[PING_GroupingCodes] ([CodeId], [MakeModelGroupingId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PING_GroupingCodes] on [dbo].[PING_GroupingCodes]'
GO
ALTER TABLE [dbo].[PING_GroupingCodes] ADD CONSTRAINT [PK_PING_GroupingCodes] PRIMARY KEY NONCLUSTERED  ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[PING_System]'
GO
CREATE TABLE [dbo].[PING_System]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[active] [bit] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_PING_System] on [dbo].[PING_System]'
GO
ALTER TABLE [dbo].[PING_System] ADD CONSTRAINT [PK_PING_System] PRIMARY KEY CLUSTERED  ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[ReliableMessageQueue]'
GO
CREATE TABLE [dbo].[ReliableMessageQueue]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[QueueName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Message] [varbinary] (max) NULL,
[Ready] [bit] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_ReliableMessageQueue] on [dbo].[ReliableMessageQueue]'
GO
ALTER TABLE [dbo].[ReliableMessageQueue] ADD CONSTRAINT [PK_ReliableMessageQueue] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[rundayofweek]'
GO
CREATE TABLE [dbo].[rundayofweek]
(
[businessunitid] [int] NOT NULL,
[rundayofweek] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SingleVINDecodeTarget]'
GO
CREATE TABLE [dbo].[SingleVINDecodeTarget]
(
[VIN] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VehicleTrim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VehicleYear] [int] NULL,
[Make] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MakeModelGroupingID] [int] NULL,
[DoorCount] [int] NULL,
[VehicleBodyStyleID] [int] NULL,
[VehicleDriveTrain] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VehicleEngine] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CylinderCount] [int] NULL,
[FuelType] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VehicleTransmission] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InteriorColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalBodyStyle] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InteriorDescription] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[SISVinAndStockMapping]'
GO
CREATE TABLE [dbo].[SISVinAndStockMapping]
(
[SISVin] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SISStockNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Vin] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StockNumber] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BusinessUnitID] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[staging_messagecenter]'
GO
CREATE TABLE [dbo].[staging_messagecenter]
(
[Ordering] [tinyint] NOT NULL,
[Title] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Body] [varchar] (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HasPriority] [tinyint] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[tbl_tmpRunDayOfWeek]'
GO
CREATE TABLE [dbo].[tbl_tmpRunDayOfWeek]
(
[businessunitid] [int] NULL,
[rundayofweek] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[VehicleAttributeSource]'
GO
CREATE TABLE [dbo].[VehicleAttributeSource]
(
[VehicleAttributeSourceID] [tinyint] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_VehicleAttributeSource] on [dbo].[VehicleAttributeSource]'
GO
ALTER TABLE [dbo].[VehicleAttributeSource] ADD CONSTRAINT [PK_VehicleAttributeSource] PRIMARY KEY CLUSTERED  ([VehicleAttributeSourceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[VehicleSalesHistoryAudit]'
GO
CREATE TABLE [dbo].[VehicleSalesHistoryAudit]
(
[AuditID] [int] NOT NULL IDENTITY(100000, 1),
[BusinessUnitID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaleRefNum] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DealerStatus] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VIN] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StockNum] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Year] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Make] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModelPkg] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BodyType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VehClass] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExtColor] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Mileage] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DealDT] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NetProfit] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NetFIIncome] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcvTrade1] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcvTrade2] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcvTrade3] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeaseFlag] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GrossMargin] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SalePrice] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SaleDesc] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DealNumFI] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RefDT] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExceptionCode] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReceivedDT] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RawText] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProcessDate] [datetime] NULL,
[ExistedPreviously] [tinyint] NULL,
[AcceptanceCode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcceptanceMessage] [varchar] (3000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_VehicleSalesHistoryAudit] on [dbo].[VehicleSalesHistoryAudit]'
GO
ALTER TABLE [dbo].[VehicleSalesHistoryAudit] ADD CONSTRAINT [PK_VehicleSalesHistoryAudit] PRIMARY KEY NONCLUSTERED  ([AuditID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[VehicleSubSegment]'
GO
CREATE TABLE [dbo].[VehicleSubSegment]
(
[VehicleSubSegmentID] [tinyint] NOT NULL IDENTITY(1, 1),
[VehicleSubSegmentDESC] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_lu_vehicleSubSegment] on [dbo].[VehicleSubSegment]'
GO
ALTER TABLE [dbo].[VehicleSubSegment] ADD CONSTRAINT [PK_lu_vehicleSubSegment] PRIMARY KEY NONCLUSTERED  ([VehicleSubSegmentID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Distribution].[MessageType]'
GO
CREATE TABLE [Distribution].[MessageType]
(
[MessageTypeID] [int] NOT NULL IDENTITY(0, 1),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FlagValue] AS (case when [MessageTypeID]<(31) then power((2),[MessageTypeID]) else (0) end) PERSISTED
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Distribution_MessageType] on [Distribution].[MessageType]'
GO
ALTER TABLE [Distribution].[MessageType] ADD CONSTRAINT [PK_Distribution_MessageType] PRIMARY KEY CLUSTERED  ([MessageTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Extract].[AULTec#Inventory]'
GO
CREATE TABLE [Extract].[AULTec#Inventory]
(
[DealerID] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StockNumber] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VIN] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NewOrUsedFlag] [tinyint] NOT NULL,
[InventoryDisposition] [int] NULL,
[PublicationStatus] [int] NULL,
[InventoryDate] [smalldatetime] NOT NULL,
[DMSStatus] [int] NULL,
[PackAmount] [decimal] (8, 2) NULL,
[HoldbackAmount] [int] NULL,
[CertificationNumber] [int] NULL,
[TagNumber] [int] NULL,
[TagState] [int] NULL,
[Warranty] [int] NULL,
[DateEntered] [smalldatetime] NOT NULL,
[DaysInStock] [int] NULL,
[Restraint] [int] NULL,
[LotLocation] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Mileage] [int] NULL,
[Certified] [tinyint] NOT NULL,
[CertificationProgram] [int] NULL,
[InternetPrice] [int] NULL,
[OriginalInternetPrice] [int] NULL,
[OnSpecial] [int] NULL,
[SpecialPrice] [int] NULL,
[MSRP] [int] NULL,
[InvoicePrice] [decimal] (9, 2) NULL,
[PriceReducedFlag] [int] NULL,
[ModelYear] [int] NOT NULL,
[Make] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModelCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Trim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BodyStyle] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StyleID] [int] NULL,
[NumCylinders] [int] NULL,
[Engine] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EngineVolume] [decimal] (4, 1) NULL,
[DriveTrain] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NumDoors] [int] NULL,
[Transmission] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BedLength] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WheelBase] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FuelType] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GasMileageCity] [decimal] (11, 1) NULL,
[GasMileageHighway] [decimal] (11, 1) NULL,
[ExtColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IntColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExtColorCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[INTColorCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InteriorType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StandardEquipment] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OptionCodes] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WebSiteHTMLDescription] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Keywords] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImageModifiedDate] [smalldatetime] NULL,
[ImageCount] [int] NULL,
[ImageURLs] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Condition] [int] NULL,
[InventoryID] [int] NULL,
[Options] [int] NULL,
[PackageCodes] [int] NULL,
[MerchandisingDescription] [varchar] (6000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DealerGlobalComment] [int] NULL,
[CommentSpanish] [int] NULL,
[CalloutText] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhotoSequencing] [int] NULL,
[TiledImageURL] [int] NULL,
[ImageURLsNoText] [int] NULL,
[VideoURL] [int] NULL,
[HasOneOwner] [int] NULL,
[BusinessUnitID] [int] NULL,
[BodyType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TireTreadRemaining] [int] NULL,
[vehicleCondition] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HaveOriginalManuals] [tinyint] NULL,
[HaveServiceRecords] [tinyint] NULL,
[DealerMaintained] [tinyint] NULL,
[FullyDetailed] [tinyint] NULL,
[VisibleDents] [tinyint] NULL,
[VisibleRust] [bit] NULL,
[KnownAccidents] [bit] NULL,
[KnownBodywork] [bit] NULL,
[PassedDealerInspection] [bit] NULL,
[NoKnownMechProblems] [bit] NULL,
[ReqMaintPerformed] [bit] NULL,
[HaveAllKeys] [bit] NULL,
[exteriorCondition] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PaintCondition] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrimCondition] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GlassCondition] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InteriorCondition] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CarpetsCondition] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SeatsCondition] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HeadlinerCondition] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DashboardCondition] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Extract].[EdmundsMigration]'
GO
CREATE TABLE [Extract].[EdmundsMigration]
(
[Status] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BusinessUnitCode] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BusinessUnitID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExternalIdentifier] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Extract].[GMAC#DealerList]'
GO
CREATE TABLE [Extract].[GMAC#DealerList]
(
[SmartAuction_ID] [int] NULL,
[PDN] [int] NULL,
[FSC] [int] NULL,
[DealerName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [Extract].[GMAC#QuickTurnDealers]'
GO
CREATE TABLE [Extract].[GMAC#QuickTurnDealers]
(
[Account ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Business Unit ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Business Code] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GMAC PDN] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SmartAuction ID] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Account Name] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Launch Date] [datetime] NULL,
[GMAC Sales Rep] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[QT Region] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [firstlook].[PERL_DBD_TEST]'
GO
CREATE TABLE [firstlook].[PERL_DBD_TEST]
(
[COL_A] [smallint] NOT NULL,
[COL_B] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COL_C] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[COL_D] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK__PERL_DBD_TEST__350B32F8] on [firstlook].[PERL_DBD_TEST]'
GO
ALTER TABLE [firstlook].[PERL_DBD_TEST] ADD CONSTRAINT [PK__PERL_DBD_TEST__350B32F8] PRIMARY KEY CLUSTERED  ([COL_A])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[Inventory]'
GO
ALTER TABLE [dbo].[Inventory] WITH NOCHECK ADD CONSTRAINT [CK_Inventory_FLRecFollowed] CHECK (([FLRecFollowed] = 1 or [FLRecFollowed] = 0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [AutoCheck].[Account]'
GO
ALTER TABLE [AutoCheck].[Account] ADD CONSTRAINT [CK_AutoCheck_Account__LenUserName] CHECK ((len([UserName])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [AutoCheck].[Account] ADD CONSTRAINT [CK_AutoCheck_Account__LenPassword] CHECK (([Password] IS NULL OR len([Password])>(0) AND len([Password])<=(20)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [AutoCheck].[Account] ADD CONSTRAINT [CK_AutoCheck_Account__DeleteDateGreaterThanInsertDate] CHECK (([DeleteDate] IS NULL OR [DeleteDate]>=coalesce([UpdateDate],[InsertDate])))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [AutoCheck].[Account] ADD CONSTRAINT [CK_AutoCheck_Account__DeleteUserDate] CHECK (([DeleteDate] IS NULL AND [DeleteUser] IS NULL OR [DeleteDate] IS NOT NULL AND [DeleteUser] IS NOT NULL))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [AutoCheck].[Account] ADD CONSTRAINT [CK_AutoCheck_Account__NullPassword] CHECK ((([AccountTypeID]=(2) OR [AccountTypeID]=(1)) AND [Password] IS NULL OR [AccountTypeID]=(3) AND [Password] IS NOT NULL))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [AutoCheck].[Account] ADD CONSTRAINT [CK_AutoCheck_Account__UpdateDateGreaterThanInsertDate] CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [AutoCheck].[Account] ADD CONSTRAINT [CK_AutoCheck_Account__UpdateUserDate] CHECK (([UpdateDate] IS NULL AND [UpdateUser] IS NULL OR [UpdateDate] IS NOT NULL AND [UpdateUser] IS NOT NULL))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [AutoCheck].[AccountStatus]'
GO
ALTER TABLE [AutoCheck].[AccountStatus] ADD CONSTRAINT [CK_AutoCheck_AccountStatus__LenName] CHECK ((len([Name])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [AutoCheck].[AccountType]'
GO
ALTER TABLE [AutoCheck].[AccountType] ADD CONSTRAINT [CK_AutoCheck_AccountType__LenName] CHECK ((len([Name])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [AutoCheck].[ReportInspection]'
GO
ALTER TABLE [AutoCheck].[ReportInspection] ADD CONSTRAINT [CK_AutoCheck_AutoCheck_ReportInspection__LenDescription] CHECK ((len([Description])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [AutoCheck].[ReportType]'
GO
ALTER TABLE [AutoCheck].[ReportType] ADD CONSTRAINT [CK_AutoCheck_ReportType__LenName] CHECK ((len([Name])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [AutoCheck].[ResponseCode]'
GO
ALTER TABLE [AutoCheck].[ResponseCode] ADD CONSTRAINT [CK_AutoCheck_ResponseCode__LenResponseCode] CHECK ((len([ResponseCode])=(3)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [AutoCheck].[ResponseCode] ADD CONSTRAINT [CK_AutoCheck_ResponseCode__LenDescription] CHECK ((len([Description])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [AutoCheck].[ServiceType]'
GO
ALTER TABLE [AutoCheck].[ServiceType] ADD CONSTRAINT [CK_AutoCheck_ServiceType__LenName] CHECK ((len([Name])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [AutoCheck].[Vehicle]'
GO
ALTER TABLE [AutoCheck].[Vehicle] ADD CONSTRAINT [CK_AutoCheck_Vehicle__LenVin] CHECK ((len([VIN])=(17)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [AutoCheck].[Vehicle] ADD CONSTRAINT [CK_AutoCheck_Vehicle__UpdateDateGreaterThanInsertDate] CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [AutoCheck].[Vehicle] ADD CONSTRAINT [CK_AutoCheck_Vehicle__UpdateUserAndDateExists] CHECK (([UpdateUser] IS NULL AND [UpdateDate] IS NULL OR [UpdateUser] IS NOT NULL AND [UpdateDate] IS NOT NULL))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Carfax].[Account]'
GO
ALTER TABLE [Carfax].[Account] ADD CONSTRAINT [CK_Carfax_Account__LenUserName] CHECK ((len([UserName])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Carfax].[Account] ADD CONSTRAINT [CK_Carfax_Account__LenPassword] CHECK ((len([Password])>(0) AND len([Password])<=(5)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Carfax].[Account] ADD CONSTRAINT [CK_Carfax_Account__DeleteDateGreaterThanInsertDate] CHECK (([DeleteDate] IS NULL OR [DeleteDate]>=coalesce([UpdateDate],[InsertDate])))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Carfax].[Account] ADD CONSTRAINT [CK_Carfax_Account__DeleteUserDate] CHECK (([DeleteDate] IS NULL AND [DeleteUser] IS NULL OR [DeleteDate] IS NOT NULL AND [DeleteUser] IS NOT NULL))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Carfax].[Account] ADD CONSTRAINT [CK_Carfax_Account__UpdateDateGreaterThanInsertDate] CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Carfax].[Account] ADD CONSTRAINT [CK_Carfax_Account__UpdateUserDate] CHECK (([UpdateDate] IS NULL AND [UpdateUser] IS NULL OR [UpdateDate] IS NOT NULL AND [UpdateUser] IS NOT NULL))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Carfax].[AccountStatus]'
GO
ALTER TABLE [Carfax].[AccountStatus] ADD CONSTRAINT [CK_Carfax_AccountStatus__LenName] CHECK ((len([Name])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Carfax].[AccountType]'
GO
ALTER TABLE [Carfax].[AccountType] ADD CONSTRAINT [CK_Carfax_AccountType__LenName] CHECK ((len([Name])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Carfax].[ReportInspection]'
GO
ALTER TABLE [Carfax].[ReportInspection] ADD CONSTRAINT [CK_Carfax_ReportInspection__LenDescription] CHECK ((len([Description])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Carfax].[ReportPreference]'
GO
ALTER TABLE [Carfax].[ReportPreference] ADD CONSTRAINT [CK_Carfax_ReportPreference__UpdateDate] CHECK (([UpdateDate] IS NULL AND [UpdateUser] IS NULL OR [UpdateDate] IS NOT NULL AND [UpdateUser] IS NOT NULL))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Carfax].[ReportPreference] ADD CONSTRAINT [CK_Carfax_ReportPreference__UpdateInsertDate] CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Carfax].[ReportProcessorCommand]'
GO
ALTER TABLE [Carfax].[ReportProcessorCommand] ADD CONSTRAINT [CK_Carfax_ReportProcessorCommand__UpdateDateGreaterThanInsertDate] CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Carfax].[ReportProcessorCommand] ADD CONSTRAINT [CK_Carfax_ReportProcessorCommand__UpdateUserAndDate] CHECK (([UpdateUser] IS NULL AND [UpdateDate] IS NULL OR [UpdateUser] IS NOT NULL AND [UpdateDate] IS NOT NULL))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Carfax].[ReportProcessorCommandStatus]'
GO
ALTER TABLE [Carfax].[ReportProcessorCommandStatus] ADD CONSTRAINT [CK_Carfax_ReportProcessorCommandStatus__LenName] CHECK ((len([Name])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Carfax].[ReportProcessorCommandType]'
GO
ALTER TABLE [Carfax].[ReportProcessorCommandType] ADD CONSTRAINT [CK_Carfax_ReportProcessorCommandType__LenName] CHECK ((len([Name])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Carfax].[ReportType]'
GO
ALTER TABLE [Carfax].[ReportType] ADD CONSTRAINT [CK_Carfax_ReportType__LenReportType] CHECK ((len([ReportType])=(3)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Carfax].[ReportType] ADD CONSTRAINT [CK_Carfax_ReportType__LenName] CHECK ((len([Name])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Carfax].[RequestType]'
GO
ALTER TABLE [Carfax].[RequestType] ADD CONSTRAINT [CK_Carfax_RequestType__LenRequestType] CHECK ((len([RequestType])=(3)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Carfax].[RequestType] ADD CONSTRAINT [CK_Carfax_RequestType__LenDescription] CHECK ((len([Description])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Carfax].[ResponseCode]'
GO
ALTER TABLE [Carfax].[ResponseCode] ADD CONSTRAINT [CK_Carfax_ResponseCode__LenResponseCode] CHECK ((len([ResponseCode])=(3)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Carfax].[ResponseCode] ADD CONSTRAINT [CK_Carfax_ResponseCode__LenDescription] CHECK ((len([Description])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Carfax].[TrackingCode]'
GO
ALTER TABLE [Carfax].[TrackingCode] ADD CONSTRAINT [CK_Carfax_TrackingCode__LenTrackingCode] CHECK ((len([TrackingCode])=(3)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Carfax].[TrackingCode] ADD CONSTRAINT [CK_Carfax_TrackingCode__LenDescription] CHECK ((len([Description])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Carfax].[Vehicle]'
GO
ALTER TABLE [Carfax].[Vehicle] ADD CONSTRAINT [CK_Carfax_Vehicle__LenVin] CHECK ((len([VIN])=(17)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Carfax].[Vehicle] ADD CONSTRAINT [CK_Carfax_Vehicle__UpdateDateGreaterThanInsertDate] CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Carfax].[Vehicle] ADD CONSTRAINT [CK_Carfax_Vehicle__UpdateUserAndDateExists] CHECK (([UpdateUser] IS NULL AND [UpdateDate] IS NULL OR [UpdateUser] IS NOT NULL AND [UpdateDate] IS NOT NULL))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Carfax].[VehicleEntityType]'
GO
ALTER TABLE [Carfax].[VehicleEntityType] ADD CONSTRAINT [CK_Carfax_VehicleEntityType__LenName] CHECK ((len([Name])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Carfax].[WindowSticker]'
GO
ALTER TABLE [Carfax].[WindowSticker] ADD CONSTRAINT [CK_Carfax_WindowSticker__LenWindowSticker] CHECK ((len([WindowSticker])=(3)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Carfax].[WindowSticker] ADD CONSTRAINT [CK_Carfax_WindowSticker__LenDescription] CHECK ((len([Description])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Certified].[CertifiedProgram]'
GO
ALTER TABLE [Certified].[CertifiedProgram] ADD CONSTRAINT [CK_Certified_CertifiedProgram__Name] CHECK ((len(rtrim(ltrim([Name])))>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Certified].[CertifiedProgram] ADD CONSTRAINT [CK_Certified_CertifiedProgram__UpdateDateGreaterThanInsertDate] CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Certified].[CertifiedProgram] ADD CONSTRAINT [UQ_Certified_CertifiedProgram__Name] UNIQUE NONCLUSTERED  ([Name])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Certified].[CertifiedProgramBenefit]'
GO
ALTER TABLE [Certified].[CertifiedProgramBenefit] ADD CONSTRAINT [CK_Certified_CertifiedProgramBenefit__Name] CHECK ((len(rtrim(ltrim([Name])))>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Certified].[CertifiedProgramBenefit] ADD CONSTRAINT [CK_Certified_CertifiedProgramBenefit__Text] CHECK ((len(rtrim(ltrim([Text])))>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Certified].[CertifiedProgramBenefit] ADD CONSTRAINT [CK_Certified_CertifiedProgramBenefit__UpdateDateGreaterThanInsertDate] CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Certified].[CertifiedProgramBenefit] ADD CONSTRAINT [UQ_Certified_CertifiedProgramBenefit__CertifiedProgramIDName] UNIQUE NONCLUSTERED  ([CertifiedProgramID], [Name])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Certified].[CertifiedProgramBenefit] ADD CONSTRAINT [UQ_Certified_CertifiedProgramBenefit__CertifiedProgramIDRank] UNIQUE NONCLUSTERED  ([CertifiedProgramID], [Rank])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Certified].[OwnerCertifiedProgramBenefit]'
GO
ALTER TABLE [Certified].[OwnerCertifiedProgramBenefit] ADD CONSTRAINT [CK_Certified_OwnerCertifiedProgramBenefit__UpdateDateGreaterThanInsertDate] CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Certified].[OwnerCertifiedProgramBenefit] ADD CONSTRAINT [UQ_Certified_OwnerCertifiedProgramBenefit__OwnerCertifiedProgramIDCertifiedProgramBenefitID] UNIQUE NONCLUSTERED  ([OwnerCertifiedProgramID], [CertifiedProgramBenefitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Certified].[OwnerCertifiedProgramBenefit] ADD CONSTRAINT [UQ_Certified_OwnerCertifiedProgramBenefit__OwnerCertifiedProgramIDRank] UNIQUE NONCLUSTERED  ([OwnerCertifiedProgramID], [Rank])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[AIP_Event]'
GO
ALTER TABLE [dbo].[AIP_Event] ADD CONSTRAINT [CK_AIP_Event__BeginDate] CHECK ((substring(convert(binary(4),[BeginDate]),3,2) & (-1) = 0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[AIP_Event] ADD CONSTRAINT [CK_AIP_Event__EndDate] CHECK ((substring(convert(binary(4),[EndDate]),3,2) & (-1) = 0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[AIP_Event] ADD CONSTRAINT [CK_AIP_Event__UserBeginDate] CHECK ((substring(convert(binary(4),[UserBeginDate]),3,2) & (-1) = 0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[AIP_Event] ADD CONSTRAINT [CK_AIP_Event__UserEndDate] CHECK ((substring(convert(binary(4),[UserEndDate]),3,2) & (-1) = 0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[AIP_Event] ADD CONSTRAINT [CK_AIP_Event__BeginEndConsistency] CHECK ((case when ([EndDate] is null) then 1 when ([EndDate] >= [BeginDate]) then 1 else 0 end = 1))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[AIP_Event] ADD CONSTRAINT [CK_AIP_Event__UserBeginEndConsistency] CHECK ((case when ([UserEndDate] is null) then 1 when ([UserEndDate] >= [UserBeginDate]) then 1 else 0 end = 1))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[DealerPreference_Pricing]'
GO
ALTER TABLE [dbo].[DealerPreference_Pricing] ADD CONSTRAINT [CK_DealerPreference_Pricing_PackageID] CHECK (([PackageID]>=(1) AND [PackageID]<=(6)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[InternetAdvertiserBuildLogDetail]'
GO
ALTER TABLE [dbo].[InternetAdvertiserBuildLogDetail] ADD CONSTRAINT [CK_InternetAdvertiserBuildLogDetail__ListedPriceSource] CHECK (([ListedPriceSource] = 'L' or [ListedPriceSource] = 'A'))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[Inventory]'
GO
ALTER TABLE [dbo].[Inventory] ADD CONSTRAINT [CK_Inventory__PlanReminderDate] CHECK ((substring(convert(binary(4),[PlanReminderDate]),3,2) & (-1) = 0))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[Inventory] ADD CONSTRAINT [UQ_Inventory__VehicleID_BusinessUnitID_StockNumber] UNIQUE NONCLUSTERED  ([VehicleID], [BusinessUnitID], [StockNumber])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[Member]'
GO
ALTER TABLE [dbo].[Member] ADD CONSTRAINT [CK_Member__InventoryOverviewSortOrderType] CHECK (([InventoryOverviewSortOrderType]=(1) OR [InventoryOverviewSortOrderType]=(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[Member] ADD CONSTRAINT [CK_Member__ActiveInventoryLaunchTool] CHECK (([ActiveInventoryLaunchTool]=(1) OR [ActiveInventoryLaunchTool]=(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[Member] ADD CONSTRAINT [UQ_Member__Login] UNIQUE NONCLUSTERED  ([Login])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[PING_Code]'
GO
ALTER TABLE [dbo].[PING_Code] ADD CONSTRAINT [CK_PING_Code__Type] CHECK (([type]='K' OR [type]='D'))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[SoftwareSystemComponent]'
GO
ALTER TABLE [dbo].[SoftwareSystemComponent] ADD CONSTRAINT [CK_SoftwareSystemComponent_Name] CHECK ((len(ltrim(rtrim([Name])))>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[SoftwareSystemComponent] ADD CONSTRAINT [CK_SoftwareSystemComponent_Token] CHECK ((len(ltrim(rtrim([Token])))>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[SoftwareSystemComponent] ADD CONSTRAINT [UK_SoftwareSystemComponent_Name] UNIQUE NONCLUSTERED  ([Name])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[SoftwareSystemComponent] ADD CONSTRAINT [UK_SoftwareSystemComponent_Token] UNIQUE NONCLUSTERED  ([Token])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[TransferPriceInventoryAgeRange]'
GO
ALTER TABLE [dbo].[TransferPriceInventoryAgeRange] ADD CONSTRAINT [CK_TransferPriceInventoryAgeRange_LowHigh] CHECK (([Low]>(-1) AND [Low]<(2147483647) AND [Low]<=[High]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[TransferPriceInventoryAgeRange] ADD CONSTRAINT [UK_TransferPriceInventoryAgeRange] UNIQUE NONCLUSTERED  ([Low], [High])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Distribution].[Account]'
GO
ALTER TABLE [Distribution].[Account] ADD CONSTRAINT [CK_Distribution_Account__Dates] CHECK (([UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Distribution].[Account_Audit]'
GO
ALTER TABLE [Distribution].[Account_Audit] ADD CONSTRAINT [CK_Distribution_AccountAudit__ValidDates] CHECK (([ValidFrom]<=[ValidUntil]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Distribution].[Account_Audit] ADD CONSTRAINT [CK_Distribution_AccountAudit__WasInsertUpdateDelete] CHECK (([WasInsert]=(1) AND [WasUpdate]=(0) AND [WasDelete]=(0) OR [WasInsert]=(0) AND [WasUpdate]=(1) AND [WasDelete]=(0) OR [WasInsert]=(0) AND [WasUpdate]=(0) AND [WasDelete]=(1)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Distribution].[AutoTrader_ErrorType]'
GO
ALTER TABLE [Distribution].[AutoTrader_ErrorType] ADD CONSTRAINT [CK_Distribution_AutoTrader_ErrorType__Description] CHECK ((len([Description])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Distribution].[AutoUplink_ErrorType]'
GO
ALTER TABLE [Distribution].[AutoUplink_ErrorType] ADD CONSTRAINT [CK_Distribution_AutoUplink_ErrorType__Description] CHECK ((len([Description])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Distribution].[ContentText]'
GO
ALTER TABLE [Distribution].[ContentText] ADD CONSTRAINT [CK_Distribution_ContentText__Text] CHECK ((len([Text])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Distribution].[ContentType]'
GO
ALTER TABLE [Distribution].[ContentType] ADD CONSTRAINT [CK_Distribution_ContentType__Description] CHECK ((len([Description])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Distribution].[MessageType]'
GO
ALTER TABLE [Distribution].[MessageType] ADD CONSTRAINT [CK_Distribution_MessageType__MessageTypeID] CHECK (([MessageTypeID]<(31)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Distribution].[MessageType] ADD CONSTRAINT [CK_Distribution_MessageType__Name] CHECK ((len([Name])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Distribution].[Price]'
GO
ALTER TABLE [Distribution].[Price] ADD CONSTRAINT [CK_Distribution_Price__Value] CHECK (([Value]>=(0) AND [Value]<=(999999)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Distribution].[PriceType]'
GO
ALTER TABLE [Distribution].[PriceType] ADD CONSTRAINT [CK_Distribution_PriceType__Name] CHECK ((len([Name])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Distribution].[Provider]'
GO
ALTER TABLE [Distribution].[Provider] ADD CONSTRAINT [CK_Distribution_Provider__Name] CHECK ((len([Name])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Distribution].[Publication]'
GO
ALTER TABLE [Distribution].[Publication] ADD CONSTRAINT [CK_Distribution_Publication__InsertDates] CHECK (([UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Distribution].[PublicationStatus]'
GO
ALTER TABLE [Distribution].[PublicationStatus] ADD CONSTRAINT [CK_Distribution_PublicationStatus__Name] CHECK ((len([Name])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Distribution].[Submission]'
GO
ALTER TABLE [Distribution].[Submission] ADD CONSTRAINT [CK_Distribution_Submission__Dates] CHECK (([ResponseReceived]>=[SubmissionSent] AND [SubmissionSent]>=[PoppedOffQueue] AND [PoppedOffQueue]>=[PushedOnQueue]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Distribution].[Submission] ADD CONSTRAINT [UQ_Distribution_Submission__Publication_Provider] UNIQUE NONCLUSTERED  ([PublicationID], [ProviderID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Distribution].[SubmissionErrorType]'
GO
ALTER TABLE [Distribution].[SubmissionErrorType] ADD CONSTRAINT [CK_Distribution_SubmissionErrorType__Description] CHECK ((len([Description])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Distribution].[SubmissionStatus]'
GO
ALTER TABLE [Distribution].[SubmissionStatus] ADD CONSTRAINT [CK_Distibution_SubmissionStatus__Name] CHECK ((len([Name])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Distribution].[VehicleEntityType]'
GO
ALTER TABLE [Distribution].[VehicleEntityType] ADD CONSTRAINT [CK_Distribution_VehicleEntityType__Name] CHECK ((len([Name])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Distribution].[VehicleInformation]'
GO
ALTER TABLE [Distribution].[VehicleInformation] ADD CONSTRAINT [CK_Distribution_VehicleInformation__Mileage] CHECK (([Mileage]>=(0) AND [Mileage]<=(999999)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Marketing].[CertifiedVehicleBenefitLineItem]'
GO
ALTER TABLE [Marketing].[CertifiedVehicleBenefitLineItem] ADD CONSTRAINT [CK_Marketing_CertifiedVehicleBenefitLineItem__UpdateDateGreaterThanInsertDate] CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Marketing].[CertifiedVehicleBenefitPreference]'
GO
ALTER TABLE [Marketing].[CertifiedVehicleBenefitPreference] ADD CONSTRAINT [CK_Marketing_CertifiedVehicleBenefitPreference__VehicleEntityTypeID] CHECK (([VehicleEntityTypeID]=(1) OR [VehicleEntityTypeID]=(5)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[CertifiedVehicleBenefitPreference] ADD CONSTRAINT [CK_Marketing_CertifiedVehicleBenefitPreference__UpdateDateGreaterThanInsertDate] CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[CertifiedVehicleBenefitPreference] ADD CONSTRAINT [UQ_Marketing_CertifiedVehicleBenefitPreference__OwnerID_VehicleEntityTypeID_VehicleEntityID] UNIQUE NONCLUSTERED  ([OwnerID], [VehicleEntityTypeID], [VehicleEntityID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Marketing].[ConsumerHighlightPreference]'
GO
ALTER TABLE [Marketing].[ConsumerHighlightPreference] ADD CONSTRAINT [CK_Marketing_ConsumerHighlightPreference__CircleRating] CHECK (([MinimumJDPowerCircleRating]=(1.0) OR [MinimumJDPowerCircleRating]=(1.5) OR [MinimumJDPowerCircleRating]=(2.0) OR [MinimumJDPowerCircleRating]=(2.5) OR [MinimumJDPowerCircleRating]=(3.0) OR [MinimumJDPowerCircleRating]=(3.5) OR [MinimumJDPowerCircleRating]=(4.0) OR [MinimumJDPowerCircleRating]=(4.5) OR [MinimumJDPowerCircleRating]=(5.0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[ConsumerHighlightPreference] ADD CONSTRAINT [CK_Marketing_ConsumerHighlightPreference__Dates] CHECK (([UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Marketing].[ConsumerHighlightSection]'
GO
ALTER TABLE [Marketing].[ConsumerHighlightSection] ADD CONSTRAINT [CK_Marketing_ConsumerHighlightSection__Name] CHECK ((len([Name])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[ConsumerHighlightSection] ADD CONSTRAINT [CK_Marketing_ConsumerHighlightSection__Rank] CHECK (([DefaultRank]>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[ConsumerHighlightSection] ADD CONSTRAINT [UQ_Marketing_ConsumerHighlightSection__Rank] UNIQUE NONCLUSTERED  ([DefaultRank])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Marketing].[ConsumerHighlightSectionPreference]'
GO
ALTER TABLE [Marketing].[ConsumerHighlightSectionPreference] ADD CONSTRAINT [CK_Marketing_ConsumerHighlightSectionPreference__Rank] CHECK (([Rank]>=(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[ConsumerHighlightSectionPreference] ADD CONSTRAINT [CK_Marketing_ConsumerHighlightSectionPreference__Dates] CHECK (([UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[ConsumerHighlightSectionPreference] ADD CONSTRAINT [UQ_Marketing_ConsumerHighlightSectionPreference__OwnerRank] UNIQUE NONCLUSTERED  ([OwnerId], [Rank])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Marketing].[Equipment]'
GO
ALTER TABLE [Marketing].[Equipment] ADD CONSTRAINT [CK_Marketing_Equipment__Name] CHECK ((len([Name])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[Equipment] ADD CONSTRAINT [CK_Marketing_Equipment__Position] CHECK (([Position]>=(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[Equipment] ADD CONSTRAINT [CK_Marketing_Equipment__UpdateDateGreaterThanInsertDate] CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[Equipment] ADD CONSTRAINT [CK_Marketing_Equipment__UpdateUserDate] CHECK (([UpdateDate] IS NULL AND [UpdateUser] IS NULL OR [UpdateDate] IS NOT NULL AND [UpdateUser] IS NOT NULL))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[Equipment] ADD CONSTRAINT [UK_Marketing_Equipment__EquipmentList_Name] UNIQUE NONCLUSTERED  ([EquipmentListID], [Name])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Marketing].[EquipmentProvider]'
GO
ALTER TABLE [Marketing].[EquipmentProvider] ADD CONSTRAINT [CK_Marketing_EquipmentProvider__Name] CHECK ((len([Name])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[EquipmentProvider] ADD CONSTRAINT [UK_Marketing_EquipmentProvider__Name] UNIQUE NONCLUSTERED  ([Name])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Marketing].[EquipmentProviderAssignment]'
GO
ALTER TABLE [Marketing].[EquipmentProviderAssignment] ADD CONSTRAINT [CK_Marketing_EquipmentProviderAssignment__Position] CHECK (([Position]>=(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[EquipmentProviderAssignment] ADD CONSTRAINT [CK_Marketing_EquipmentProviderAssignment__UpdateDateGreaterThanInsertDate] CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[EquipmentProviderAssignment] ADD CONSTRAINT [CK_Marketing_EquipmentProviderAssignment__UpdateUserDate] CHECK (([UpdateDate] IS NULL AND [UpdateUser] IS NULL OR [UpdateDate] IS NOT NULL AND [UpdateUser] IS NOT NULL))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Marketing].[HighlightProvider]'
GO
ALTER TABLE [Marketing].[HighlightProvider] ADD CONSTRAINT [CK_Marketing_HighlightProvider__Name] CHECK ((len([Name])>(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[HighlightProvider] ADD CONSTRAINT [UK_Marketing_HighlightProvider__Name] UNIQUE NONCLUSTERED  ([Name])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Marketing].[PhotoVehiclePreference]'
GO
ALTER TABLE [Marketing].[PhotoVehiclePreference] ADD CONSTRAINT [CK_Marketing_PhotoVehiclePreference__UpdateDateGreaterThanInsertDate] CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[PhotoVehiclePreference] ADD CONSTRAINT [CK_Marketing_PhotoVehiclePreference__UpdateUserDate] CHECK (([UpdateDate] IS NULL AND [UpdateUser] IS NULL OR [UpdateDate] IS NOT NULL AND [UpdateUser] IS NOT NULL))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Marketing].[SnippetSourcePreference]'
GO
ALTER TABLE [Marketing].[SnippetSourcePreference] ADD CONSTRAINT [CK_Marketing_SnippetSourcePreference__Rank] CHECK (([Rank]>=(0)))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[SnippetSourcePreference] ADD CONSTRAINT [CK_Marketing_SnippetSourcePreference__Dates] CHECK (([UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[SnippetSourcePreference] ADD CONSTRAINT [UQ_Marketing_SnippetSourcePreference__OwnerRank] UNIQUE NONCLUSTERED  ([OwnerId], [Rank], [IsDisplayed])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Marketing].[ValueAnalyzerVehiclePreference]'
GO
ALTER TABLE [Marketing].[ValueAnalyzerVehiclePreference] ADD CONSTRAINT [CK_Marketing_ValueAnalyzerVehiclePreference__UpdateDateGreaterThanInsertDate] CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[ValueAnalyzerVehiclePreference] ADD CONSTRAINT [CK_Marketing_ValueAnalyzerVehiclePreference__UpdateUserDate] CHECK (([UpdateDate] IS NULL AND [UpdateUser] IS NULL OR [UpdateDate] IS NOT NULL AND [UpdateUser] IS NOT NULL))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Marketing].[VehicleEquipmentProvider]'
GO
ALTER TABLE [Marketing].[VehicleEquipmentProvider] ADD CONSTRAINT [CK_Marketing_VehicleEquipmentProvider__UpdateDateGreaterThanInsertDate] CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[VehicleEquipmentProvider] ADD CONSTRAINT [CK_Marketing_VehicleEquipmentProvider__UpdateUserDate] CHECK (([UpdateDate] IS NULL AND [UpdateUser] IS NULL OR [UpdateDate] IS NOT NULL AND [UpdateUser] IS NOT NULL))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Purchasing].[List]'
GO
ALTER TABLE [Purchasing].[List] ADD CONSTRAINT [CK_Purchasing_List__UpdateUser] CHECK (([UpdateUser] IS NULL AND [UpdateDate] IS NULL OR [UpdateUser] IS NOT NULL AND [UpdateDate] IS NOT NULL))
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Certified].[OwnerCertifiedProgram]'
GO
ALTER TABLE [Certified].[OwnerCertifiedProgram] ADD CONSTRAINT [UQ_Certified_OwnerCertifiedProgram__OwnerIdCertifiedProgramID] UNIQUE NONCLUSTERED  ([OwnerId], [CertifiedProgramID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[AppraisalActions]'
GO
ALTER TABLE [dbo].[AppraisalActions] ADD CONSTRAINT [UK_AppraisalActions_AppraisalID] UNIQUE NONCLUSTERED  ([AppraisalID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[AppraisalFormOptions]'
GO
ALTER TABLE [dbo].[AppraisalFormOptions] ADD CONSTRAINT [UK_AppraisalFormOptions_AppraisalID] UNIQUE NONCLUSTERED  ([AppraisalID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[Appraisals]'
GO
ALTER TABLE [dbo].[Appraisals] ADD CONSTRAINT [UK_Appraisals] UNIQUE NONCLUSTERED  ([BusinessUnitID], [VehicleID], [DateCreated])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[AppraisalStatus]'
GO
ALTER TABLE [dbo].[AppraisalStatus] ADD CONSTRAINT [UK_AppraisalStatus_Description] UNIQUE NONCLUSTERED  ([Description])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[AppraisalValues]'
GO
ALTER TABLE [dbo].[AppraisalValues] ADD CONSTRAINT [UK_AppraisalValues] UNIQUE NONCLUSTERED  ([AppraisalID], [SequenceNumber])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[AppraisalWindowStickers]'
GO
ALTER TABLE [dbo].[AppraisalWindowStickers] ADD CONSTRAINT [UK_AppraisalWindowStickers_AppraisalID] UNIQUE NONCLUSTERED  ([AppraisalID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[BusinessUnit]'
GO
ALTER TABLE [dbo].[BusinessUnit] ADD CONSTRAINT [UQ_BusinessUnit_BusinessUnitCode] UNIQUE NONCLUSTERED  ([BusinessUnitCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[BuyingAlertsRunList]'
GO
ALTER TABLE [dbo].[BuyingAlertsRunList] ADD CONSTRAINT [UK_BuyingAlertsRunList] UNIQUE NONCLUSTERED  ([SubscriptionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[DealerFranchise]'
GO
ALTER TABLE [dbo].[DealerFranchise] ADD CONSTRAINT [UNK_DealerFranchiseUnique_BU_FranchiseID] UNIQUE NONCLUSTERED  ([BusinessUnitID], [FranchiseID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[DealerGridThresholds]'
GO
ALTER TABLE [dbo].[DealerGridThresholds] ADD CONSTRAINT [UQ_DealerGridThresholdsBusinessUnit] UNIQUE NONCLUSTERED  ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[DealerPreference]'
GO
ALTER TABLE [dbo].[DealerPreference] ADD CONSTRAINT [IX_DealerPreference] UNIQUE NONCLUSTERED  ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[DealerTransferAllowRetailOnlyRule]'
GO
ALTER TABLE [dbo].[DealerTransferAllowRetailOnlyRule] ADD CONSTRAINT [UK_DealerTransferAllowRetailOnlyRule] UNIQUE NONCLUSTERED  ([BusinessUnitID], [TransferPriceInventoryAgeRangeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[DealerTransferPriceUnitCostRule]'
GO
ALTER TABLE [dbo].[DealerTransferPriceUnitCostRule] ADD CONSTRAINT [UK_DealerTransferPriceUnitCostRule] UNIQUE NONCLUSTERED  ([BusinessUnitID], [TransferPriceUnitCostValueID], [TransferPriceInventoryAgeRangeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[DMSExport_ThirdPartyEntity]'
GO
ALTER TABLE [dbo].[DMSExport_ThirdPartyEntity] ADD CONSTRAINT [UQ__DMSExport_ThirdP__697EFB37] UNIQUE NONCLUSTERED  ([DMSExportID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[DMSExportPriceMapping]'
GO
ALTER TABLE [dbo].[DMSExportPriceMapping] ADD CONSTRAINT [UQ__DMSExportPriceMa__72144138] UNIQUE NONCLUSTERED  ([Description])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[GDLight]'
GO
ALTER TABLE [dbo].[GDLight] ADD CONSTRAINT [UQ_GDLight__GroupingDescriptionLightId] UNIQUE NONCLUSTERED  ([GroupingDescriptionLightId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[HalSessions]'
GO
ALTER TABLE [dbo].[HalSessions] ADD CONSTRAINT [UQ__HalSessions__6DC3C210] UNIQUE NONCLUSTERED  ([HalSessionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[InternetAdvertiser_ThirdPartyEntity]'
GO
ALTER TABLE [dbo].[InternetAdvertiser_ThirdPartyEntity] ADD CONSTRAINT [UQ_InternetAdvertiser__DatafeedCode] UNIQUE NONCLUSTERED  ([DatafeedCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[License]'
GO
ALTER TABLE [dbo].[License] ADD CONSTRAINT [UK_License_Name] UNIQUE NONCLUSTERED  ([Name])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[lu_DealerPackType]'
GO
ALTER TABLE [dbo].[lu_DealerPackType] ADD CONSTRAINT [IX_lu_DealerPackType] UNIQUE NONCLUSTERED  ([PackTypeDESC])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[lu_DealerUpgrade]'
GO
ALTER TABLE [dbo].[lu_DealerUpgrade] ADD CONSTRAINT [UK_DealerUpgrade_DealerUpgradeDESC] UNIQUE NONCLUSTERED  ([DealerUpgradeDESC])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[lu_DMIVehicleStatus]'
GO
ALTER TABLE [dbo].[lu_DMIVehicleStatus] ADD CONSTRAINT [UQ_lu_DMIVehicleStatus_DMIVehicleStatusDESC] UNIQUE NONCLUSTERED  ([DMIVehicleStatusDESC])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[lu_FuelType]'
GO
ALTER TABLE [dbo].[lu_FuelType] ADD CONSTRAINT [UQ_lu_FuelType_FuelTypeDESC] UNIQUE NONCLUSTERED  ([FuelTypeCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[lu_States]'
GO
ALTER TABLE [dbo].[lu_States] ADD CONSTRAINT [UQ_lu_States_StateShort] UNIQUE NONCLUSTERED  ([StateShort])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [dbo].[lu_States] ADD CONSTRAINT [UQ_lu_States_StateLong] UNIQUE NONCLUSTERED  ([StateLong])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[lu_UserRole]'
GO
ALTER TABLE [dbo].[lu_UserRole] ADD CONSTRAINT [UQ_lu_UserRole_UserRoleDesc] UNIQUE NONCLUSTERED  ([UserRoleDesc])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[lu_VehicleLight]'
GO
ALTER TABLE [dbo].[lu_VehicleLight] ADD CONSTRAINT [UQ_lu_VehicleLight_InventoryVehicleLightDESC] UNIQUE NONCLUSTERED  ([InventoryVehicleLightDESC])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[MakeModelGrouping]'
GO
ALTER TABLE [dbo].[MakeModelGrouping] ADD CONSTRAINT [UQ_MakeModelGrouping] UNIQUE NONCLUSTERED  ([Make], [Model], [SegmentID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[MemberAppraisalReviewPreferences]'
GO
ALTER TABLE [dbo].[MemberAppraisalReviewPreferences] ADD CONSTRAINT [UK_MemberappraisalReviewPreferenceList] UNIQUE CLUSTERED  ([MemberID], [BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[MemberBusinessUnitSet]'
GO
ALTER TABLE [dbo].[MemberBusinessUnitSet] ADD CONSTRAINT [UK_MemberBusinessUnitList] UNIQUE NONCLUSTERED  ([MemberBusinessUnitSetTypeID], [BusinessUnitID], [MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[MemberInsightTypePreference]'
GO
ALTER TABLE [dbo].[MemberInsightTypePreference] ADD CONSTRAINT [UK_MemberInsightType] UNIQUE NONCLUSTERED  ([BusinessUnitID], [MemberID], [InsightTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[MemberRole]'
GO
ALTER TABLE [dbo].[MemberRole] ADD CONSTRAINT [UQ_MemberRole] UNIQUE NONCLUSTERED  ([MemberID], [RoleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[PhotoProvider_ThirdPartyEntity]'
GO
ALTER TABLE [dbo].[PhotoProvider_ThirdPartyEntity] ADD CONSTRAINT [UQ_PhotoProvider_ThirdPartyEntity__DatafeedCode] UNIQUE NONCLUSTERED  ([DatafeedCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[SalesChannel]'
GO
ALTER TABLE [dbo].[SalesChannel] ADD CONSTRAINT [UK_SalesChannel_Name] UNIQUE NONCLUSTERED  ([Name])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[SearchCandidate]'
GO
ALTER TABLE [dbo].[SearchCandidate] ADD CONSTRAINT [UQ_SearchCandidate] UNIQUE NONCLUSTERED  ([SearchCandidateListID], [ModelID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[SearchCandidateList]'
GO
ALTER TABLE [dbo].[SearchCandidateList] ADD CONSTRAINT [UQ_SearchCandidateList] UNIQUE NONCLUSTERED  ([SearchCandidateListTypeID], [BusinessUnitID], [MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[SearchCandidateOption]'
GO
ALTER TABLE [dbo].[SearchCandidateOption] ADD CONSTRAINT [UQ_SearchCandidateOption] UNIQUE NONCLUSTERED  ([SearchCandidateID], [ModelYear])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[SoftwareSystemComponentState]'
GO
ALTER TABLE [dbo].[SoftwareSystemComponentState] ADD CONSTRAINT [UK_SoftwareSystemComponentState] UNIQUE NONCLUSTERED  ([SoftwareSystemComponentID], [AuthorizedMemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[tbl_DealerATCAccessGroups]'
GO
ALTER TABLE [dbo].[tbl_DealerATCAccessGroups] ADD CONSTRAINT [UNK_DealerATCAccessGroupsUnique_BU_AccessGroupsID] UNIQUE NONCLUSTERED  ([BusinessUnitID], [AccessGroupId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[tbl_GroupingDescription]'
GO
ALTER TABLE [dbo].[tbl_GroupingDescription] ADD CONSTRAINT [UNK_GroupingDescription_GroupingDesc] UNIQUE NONCLUSTERED  ([GroupingDescription])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[tbl_Vehicle]'
GO
ALTER TABLE [dbo].[tbl_Vehicle] ADD CONSTRAINT [UQ_tbl_Vehicle_Vin] UNIQUE NONCLUSTERED  ([Vin])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[ThirdPartyCategories]'
GO
ALTER TABLE [dbo].[ThirdPartyCategories] ADD CONSTRAINT [UC_ThirdPartyCategories] UNIQUE NONCLUSTERED  ([ThirdPartyID], [Category])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[VehicleSubSegment]'
GO
ALTER TABLE [dbo].[VehicleSubSegment] ADD CONSTRAINT [UQ_lu_vehicleSubSegment] UNIQUE NONCLUSTERED  ([VehicleSubSegmentDESC])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Distribution].[AccountPreferences]'
GO
ALTER TABLE [Distribution].[AccountPreferences] ADD CONSTRAINT [UQ_Distribution_AccountPreferences__Dealer_Provider_MessageType] UNIQUE NONCLUSTERED  ([DealerID], [ProviderID], [MessageTypeFlag])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Distribution].[Vehicle]'
GO
ALTER TABLE [Distribution].[Vehicle] ADD CONSTRAINT [UQ_Distribution_Vehicle__VehicleEntityID] UNIQUE NONCLUSTERED  ([VehicleEntityID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Marketing].[ConsumerHighlightVehiclePreference]'
GO
ALTER TABLE [Marketing].[ConsumerHighlightVehiclePreference] ADD CONSTRAINT [UK_Marketing_ConsumerHighlightVehiclePreference__OwnerId_VehicleEntityId_VehicleEntityTypeID] UNIQUE NONCLUSTERED  ([OwnerId], [VehicleEntityId], [VehicleEntityTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Marketing].[EquipmentProviderAssignmentList]'
GO
ALTER TABLE [Marketing].[EquipmentProviderAssignmentList] ADD CONSTRAINT [UK_Marketing_EquipmentProviderAssignmentList__Owner] UNIQUE NONCLUSTERED  ([OwnerID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Marketing].[MarketAnalysisOwnerPriceProvider]'
GO
ALTER TABLE [Marketing].[MarketAnalysisOwnerPriceProvider] ADD CONSTRAINT [UQ_MarketAnalysisOwnerPriceProvider_OwnerIdProviderId] UNIQUE NONCLUSTERED  ([OwnerId], [PriceProviderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
ALTER TABLE [Marketing].[MarketAnalysisOwnerPriceProvider] ADD CONSTRAINT [UQ_MarketAnalysisOwnerPriceProvider_OwnerIdRank] UNIQUE NONCLUSTERED  ([OwnerId], [Rank])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Marketing].[MarketAnalysisPreference]'
GO
ALTER TABLE [Marketing].[MarketAnalysisPreference] ADD CONSTRAINT [UQ_MarketAnalysisPreference_OwnerId] UNIQUE NONCLUSTERED  ([OwnerId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [Marketing].[MarketListingPreference]'
GO
ALTER TABLE [Marketing].[MarketListingPreference] ADD CONSTRAINT [UQ_MarketListingPreference_OwnerId] UNIQUE NONCLUSTERED  ([OwnerId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Audit_Inventory]'
GO
ALTER TABLE [dbo].[Audit_Inventory] WITH NOCHECK ADD
CONSTRAINT [FK_Audit_Inventory__lu_AuditStatus] FOREIGN KEY ([STATUS]) REFERENCES [dbo].[lu_AuditStatus] ([AuditStatusCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Audit_Sales]'
GO
ALTER TABLE [dbo].[Audit_Sales] WITH NOCHECK ADD
CONSTRAINT [FK_Audit_Sales__lu_AuditStatus] FOREIGN KEY ([STATUS]) REFERENCES [dbo].[lu_AuditStatus] ([AuditStatusCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[BuyingAlertsRunList]'
GO
ALTER TABLE [dbo].[BuyingAlertsRunList] WITH NOCHECK ADD
CONSTRAINT [FK_BuyingAlertsRunList_BuyingAlertsStatus] FOREIGN KEY ([BuyingAlertsStatusId]) REFERENCES [dbo].[BuyingAlertsStatus] ([BuyingAlertsStatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[MarketShare]'
GO
ALTER TABLE [dbo].[MarketShare] WITH NOCHECK ADD
CONSTRAINT [FK_MarketShare_GroupingDescriptionID] FOREIGN KEY ([GroupingDescriptionID]) REFERENCES [dbo].[tbl_GroupingDescription] ([GroupingDescriptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [AutoCheck].[Account_Dealer]'
GO
ALTER TABLE [AutoCheck].[Account_Dealer] ADD
CONSTRAINT [FK_AutoCheck_Account_Dealer__AccountID] FOREIGN KEY ([AccountID]) REFERENCES [AutoCheck].[Account] ([AccountID]),
CONSTRAINT [FK_AutoCheck_Account_Dealer__BusinessUnitID] FOREIGN KEY ([DealerID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_AutoCheck_Account_Dealer__InsertMember] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [AutoCheck].[Account_Member]'
GO
ALTER TABLE [AutoCheck].[Account_Member] ADD
CONSTRAINT [FK_AutoCheck_Account_Member__MemberID_AccountID] FOREIGN KEY ([AccountID]) REFERENCES [AutoCheck].[Account] ([AccountID]),
CONSTRAINT [FK_AutoCheck_Account_Member__MemberID] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_AutoCheck_Account_Member__MemberID_InsertUser] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [AutoCheck].[Account_ServiceType]'
GO
ALTER TABLE [AutoCheck].[Account_ServiceType] ADD
CONSTRAINT [FK_AutoCheck_Account_ServiceType__AccountID] FOREIGN KEY ([AccountID]) REFERENCES [AutoCheck].[Account] ([AccountID]),
CONSTRAINT [FK_AutoCheck_Account_ServiceType__ServiceTypeID] FOREIGN KEY ([ServiceTypeID]) REFERENCES [AutoCheck].[ServiceType] ([ServiceTypeID]),
CONSTRAINT [FK_AutoCheck_Account_ServiceType__InsertMember] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [AutoCheck].[Request]'
GO
ALTER TABLE [AutoCheck].[Request] ADD
CONSTRAINT [FK_AutoCheck_Request__Account] FOREIGN KEY ([AccountID]) REFERENCES [AutoCheck].[Account] ([AccountID]),
CONSTRAINT [FK_AutoCheck_Request__ReportType] FOREIGN KEY ([ReportTypeID]) REFERENCES [AutoCheck].[ReportType] ([ReportTypeID]),
CONSTRAINT [FK_AutoCheck_Request__VehicleID] FOREIGN KEY ([VehicleID]) REFERENCES [AutoCheck].[Vehicle] ([VehicleID]),
CONSTRAINT [FK_AutoCheck_Request__Member_InsertUser] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [AutoCheck].[Account]'
GO
ALTER TABLE [AutoCheck].[Account] ADD
CONSTRAINT [FK_AutoCheck_Account__AccountStatus] FOREIGN KEY ([AccountStatusID]) REFERENCES [AutoCheck].[AccountStatus] ([AccountStatusID]),
CONSTRAINT [FK_AutoCheck_Account__AccountType] FOREIGN KEY ([AccountTypeID]) REFERENCES [AutoCheck].[AccountType] ([AccountTypeID]),
CONSTRAINT [FK_AutoCheck_Account__InsertMember] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_AutoCheck_Account__UpdateMember] FOREIGN KEY ([UpdateUser]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_AutoCheck_Account__DeleteMember] FOREIGN KEY ([DeleteUser]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [AutoCheck].[Exception]'
GO
ALTER TABLE [AutoCheck].[Exception] ADD
CONSTRAINT [FK_AutoCheck_Exception__RequestID] FOREIGN KEY ([RequestID]) REFERENCES [AutoCheck].[Request] ([RequestID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [AutoCheck].[Report]'
GO
ALTER TABLE [AutoCheck].[Report] ADD
CONSTRAINT [FK_AutoCheck_Report__RequestID] FOREIGN KEY ([RequestID]) REFERENCES [AutoCheck].[Request] ([RequestID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [AutoCheck].[ReportInspectionResult]'
GO
ALTER TABLE [AutoCheck].[ReportInspectionResult] ADD
CONSTRAINT [FK_AutoCheck_ReportInspectionResult__Report] FOREIGN KEY ([RequestID]) REFERENCES [AutoCheck].[Report] ([RequestID]),
CONSTRAINT [FK_AutoCheck_ReportInspectionResult__ReportInspectionID] FOREIGN KEY ([ReportInspectionID]) REFERENCES [AutoCheck].[ReportInspection] ([ReportInspectionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [AutoCheck].[Response]'
GO
ALTER TABLE [AutoCheck].[Response] ADD
CONSTRAINT [FK_AutoCheck_Response__RequestID] FOREIGN KEY ([RequestID]) REFERENCES [AutoCheck].[Request] ([RequestID]),
CONSTRAINT [FK_AutoCheck_Response__ResponseCode] FOREIGN KEY ([ResponseCode]) REFERENCES [AutoCheck].[ResponseCode] ([ResponseCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [AutoCheck].[Vehicle_Dealer]'
GO
ALTER TABLE [AutoCheck].[Vehicle_Dealer] ADD
CONSTRAINT [FK_AutoCheck_Vehicle_Dealer__RequestID] FOREIGN KEY ([RequestID]) REFERENCES [AutoCheck].[Request] ([RequestID]),
CONSTRAINT [FK_AutoCheck_Vehicle_Dealer__VehicleID] FOREIGN KEY ([VehicleID]) REFERENCES [AutoCheck].[Vehicle] ([VehicleID]),
CONSTRAINT [FK_AutoCheck_Vehicle_Dealer__BusinessUnitID] FOREIGN KEY ([DealerID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [AutoCheck].[Vehicle]'
GO
ALTER TABLE [AutoCheck].[Vehicle] ADD
CONSTRAINT [FK_AutoCheck_Vehicle__Member_InsertUser] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_AutoCheck_Vehicle__Member_UpdateUser] FOREIGN KEY ([UpdateUser]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Carfax].[Account_Dealer]'
GO
ALTER TABLE [Carfax].[Account_Dealer] ADD
CONSTRAINT [FK_Account_Dealer__Account] FOREIGN KEY ([AccountID]) REFERENCES [Carfax].[Account] ([AccountID]),
CONSTRAINT [FK_Account_Dealer__BusinessUnit] FOREIGN KEY ([DealerID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_Account_Dealer__InsertMember] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Carfax].[Account_Member]'
GO
ALTER TABLE [Carfax].[Account_Member] ADD
CONSTRAINT [FK_Account_Member__Account] FOREIGN KEY ([AccountID]) REFERENCES [Carfax].[Account] ([AccountID]),
CONSTRAINT [FK_Account_Member__Member] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Account_Member__InsertUser] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Carfax].[Request]'
GO
ALTER TABLE [Carfax].[Request] ADD
CONSTRAINT [FK_Request_Account] FOREIGN KEY ([AccountID]) REFERENCES [Carfax].[Account] ([AccountID]),
CONSTRAINT [FK_Request__ReportType] FOREIGN KEY ([ReportType]) REFERENCES [Carfax].[ReportType] ([ReportType]),
CONSTRAINT [FK_Carfax_Request__VehicleID] FOREIGN KEY ([VehicleID]) REFERENCES [Carfax].[Vehicle] ([VehicleID]),
CONSTRAINT [FK_Carfax_Request__TrackingCode] FOREIGN KEY ([TrackingCode]) REFERENCES [Carfax].[TrackingCode] ([TrackingCode]),
CONSTRAINT [FK_Carfax_Request__RequestType] FOREIGN KEY ([RequestType]) REFERENCES [Carfax].[RequestType] ([RequestType]),
CONSTRAINT [FK_Carfax_Request__WindowSticker] FOREIGN KEY ([WindowSticker]) REFERENCES [Carfax].[WindowSticker] ([WindowSticker]),
CONSTRAINT [FK_Request__InsertUser] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Carfax].[Account]'
GO
ALTER TABLE [Carfax].[Account] ADD
CONSTRAINT [FK_Carfax_Account__AccountStatus] FOREIGN KEY ([AccountStatusID]) REFERENCES [Carfax].[AccountStatus] ([AccountStatusID]),
CONSTRAINT [FK_Carfax_Account__AccountType] FOREIGN KEY ([AccountTypeID]) REFERENCES [Carfax].[AccountType] ([AccountTypeID]),
CONSTRAINT [FK_Carfax_Account__InsertMember] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Carfax_Account__UpdateMember] FOREIGN KEY ([UpdateUser]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Carfax_Account__DeleteMember] FOREIGN KEY ([DeleteUser]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Carfax].[ReportProcessorCommand_Exception]'
GO
ALTER TABLE [Carfax].[ReportProcessorCommand_Exception] ADD
CONSTRAINT [FK_Carfax_ReportProcessorCommand_Exception__ExceptionID] FOREIGN KEY ([ExceptionID]) REFERENCES [Carfax].[Exception] ([ExceptionID]),
CONSTRAINT [FK_Carfax_ReportProcessorCommand_Exception__ReportProcessorCommandID] FOREIGN KEY ([ReportProcessorCommandID]) REFERENCES [Carfax].[ReportProcessorCommand] ([ReportProcessorCommandID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Carfax].[Exception]'
GO
ALTER TABLE [Carfax].[Exception] ADD
CONSTRAINT [FK_Exception__RequestID] FOREIGN KEY ([RequestID]) REFERENCES [Carfax].[Request] ([RequestID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Carfax].[Report]'
GO
ALTER TABLE [Carfax].[Report] ADD
CONSTRAINT [FK_Carfax_Report__Request] FOREIGN KEY ([RequestID]) REFERENCES [Carfax].[Request] ([RequestID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Carfax].[ReportInspectionResult]'
GO
ALTER TABLE [Carfax].[ReportInspectionResult] ADD
CONSTRAINT [FK_Carfax_ReportInspectionResult__Report] FOREIGN KEY ([RequestID]) REFERENCES [Carfax].[Report] ([RequestID]),
CONSTRAINT [FK_Carfax_ReportInspectionResult__ReportInspection] FOREIGN KEY ([ReportInspectionID]) REFERENCES [Carfax].[ReportInspection] ([ReportInspectionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Carfax].[ReportPreference]'
GO
ALTER TABLE [Carfax].[ReportPreference] ADD
CONSTRAINT [FK_Carfax_ReportPreference__BusinessUnit] FOREIGN KEY ([DealerID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_Carfax_ReportPreference__VehicleEntityTypeID] FOREIGN KEY ([VehicleEntityTypeID]) REFERENCES [Carfax].[VehicleEntityType] ([VehicleEntityTypeID]),
CONSTRAINT [FK_Carfax_ReportPreference__ReportType] FOREIGN KEY ([ReportType]) REFERENCES [Carfax].[ReportType] ([ReportType]),
CONSTRAINT [FK_Carfax_ReportPreference__InsertUser] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Carfax_ReportPreference__UpdateUser] FOREIGN KEY ([UpdateUser]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Carfax].[ReportProcessorCommand_DataMigration]'
GO
ALTER TABLE [Carfax].[ReportProcessorCommand_DataMigration] ADD
CONSTRAINT [FK_Carfax_ReportProcessorCommand_DataMigration__ReportProcessorCommandID] FOREIGN KEY ([ReportProcessorCommandID]) REFERENCES [Carfax].[ReportProcessorCommand] ([ReportProcessorCommandID]),
CONSTRAINT [FK_Carfax_ReportProcessorCommand_DataMigration__ReportType] FOREIGN KEY ([ReportType]) REFERENCES [Carfax].[ReportType] ([ReportType])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Carfax].[ReportProcessorCommand_Request]'
GO
ALTER TABLE [Carfax].[ReportProcessorCommand_Request] ADD
CONSTRAINT [FK_Carfax_ReportProcessorCommand_Request__ReportProcessorCommandID] FOREIGN KEY ([ReportProcessorCommandID]) REFERENCES [Carfax].[ReportProcessorCommand] ([ReportProcessorCommandID]),
CONSTRAINT [FK_Carfax_ReportProcessorCommand_Request__RequestID] FOREIGN KEY ([RequestID]) REFERENCES [Carfax].[Request] ([RequestID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Carfax].[ReportProcessorCommand]'
GO
ALTER TABLE [Carfax].[ReportProcessorCommand] ADD
CONSTRAINT [FK_Carfax_ReportProcessorCommand__BusinessUnitID] FOREIGN KEY ([DealerID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_Carfax_ReportProcessorCommand_Member__InsertUser] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Carfax_ReportProcessorCommand_Member__UpdateUser] FOREIGN KEY ([UpdateUser]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Carfax].[Response]'
GO
ALTER TABLE [Carfax].[Response] ADD
CONSTRAINT [FK_Carfax_Response__RequestID] FOREIGN KEY ([RequestID]) REFERENCES [Carfax].[Request] ([RequestID]),
CONSTRAINT [FK_Carfax_Response__ResponseCode] FOREIGN KEY ([ResponseCode]) REFERENCES [Carfax].[ResponseCode] ([ResponseCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Carfax].[Vehicle_Dealer]'
GO
ALTER TABLE [Carfax].[Vehicle_Dealer] ADD
CONSTRAINT [FK_Carfax_Vehicle_Dealer__Request] FOREIGN KEY ([RequestID]) REFERENCES [Carfax].[Request] ([RequestID]),
CONSTRAINT [FK_Carfax_Vehicle_Dealer__Vehicle] FOREIGN KEY ([VehicleID]) REFERENCES [Carfax].[Vehicle] ([VehicleID]),
CONSTRAINT [FK_Carfax_Vehicle_Dealer__BusinessUnit] FOREIGN KEY ([DealerID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Carfax].[Vehicle]'
GO
ALTER TABLE [Carfax].[Vehicle] ADD
CONSTRAINT [FK_Carfax_Vehicle_Member__InsertUser] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Carfax_Vehicle_Member__UpdateUser] FOREIGN KEY ([UpdateUser]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Certified].[CertifiedProgram_Make]'
GO
ALTER TABLE [Certified].[CertifiedProgram_Make] ADD
CONSTRAINT [FK_Certified_CertifiedProgram_Make__CertifiedProgramID] FOREIGN KEY ([CertifiedProgramID]) REFERENCES [Certified].[CertifiedProgram] ([CertifiedProgramID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Certified].[CertifiedProgramBenefit]'
GO
ALTER TABLE [Certified].[CertifiedProgramBenefit] ADD
CONSTRAINT [FK_Certified_CertifiedProgramBenefit__CertifiedProgramID] FOREIGN KEY ([CertifiedProgramID]) REFERENCES [Certified].[CertifiedProgram] ([CertifiedProgramID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Certified].[CertifiedProgramPhoto]'
GO
ALTER TABLE [Certified].[CertifiedProgramPhoto] ADD
CONSTRAINT [FK_Certified_CertifiedProgramPhoto__CertifiedProgramID] FOREIGN KEY ([CertifiedProgramID]) REFERENCES [Certified].[CertifiedProgram] ([CertifiedProgramID]),
CONSTRAINT [FK_Certified_CertifiedProgramPhoto__PhotoID] FOREIGN KEY ([PhotoID]) REFERENCES [dbo].[Photos] ([PhotoID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Certified].[OwnerCertifiedProgram]'
GO
ALTER TABLE [Certified].[OwnerCertifiedProgram] ADD
CONSTRAINT [FK_Certified_OwnerCertifiedProgram__CertifiedProgramID] FOREIGN KEY ([CertifiedProgramID]) REFERENCES [Certified].[CertifiedProgram] ([CertifiedProgramID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Certified].[OwnerCertifiedProgramBenefit]'
GO
ALTER TABLE [Certified].[OwnerCertifiedProgramBenefit] ADD
CONSTRAINT [FK_Certified_OwnerCertifiedProgramBenefit__CertifiedProgramBenefitID] FOREIGN KEY ([CertifiedProgramBenefitID]) REFERENCES [Certified].[CertifiedProgramBenefit] ([CertifiedProgramBenefitID]),
CONSTRAINT [FK_Certified_OwnerCertifiedProgramBenefit__OwnerCertifiedProgramID] FOREIGN KEY ([OwnerCertifiedProgramID]) REFERENCES [Certified].[OwnerCertifiedProgram] ([OwnerCertifiedProgramID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Marketing].[CertifiedVehicleBenefitPreference]'
GO
ALTER TABLE [Marketing].[CertifiedVehicleBenefitPreference] ADD
CONSTRAINT [FK_Marketing_CertifiedVehicleBenefitPreference__OwnerCertifiedProgramID] FOREIGN KEY ([OwnerCertifiedProgramID]) REFERENCES [Certified].[OwnerCertifiedProgram] ([OwnerCertifiedProgramID]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Marketing].[CertifiedVehicleBenefitLineItem]'
GO
ALTER TABLE [Marketing].[CertifiedVehicleBenefitLineItem] ADD
CONSTRAINT [FK_Marketing_CertifiedVehicleBenefitLineItem__OwnerCertifiedProgramBenefitID] FOREIGN KEY ([OwnerCertifiedProgramBenefitID]) REFERENCES [Certified].[OwnerCertifiedProgramBenefit] ([OwnerCertifiedProgramBenefitID]) ON DELETE CASCADE,
CONSTRAINT [FK_Marketing_CertifiedVehicleBenefitLineItem__CertifiedVehicleBenefitPreferenceID] FOREIGN KEY ([CertifiedVehicleBenefitPreferenceID]) REFERENCES [Marketing].[CertifiedVehicleBenefitPreference] ([CertifiedVehicleBenefitPreferenceID]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[RepriceExport]'
GO
ALTER TABLE [dbo].[RepriceExport] ADD
CONSTRAINT [FK_RepriceExport_AIP_Event] FOREIGN KEY ([RepriceEventID]) REFERENCES [dbo].[AIP_Event] ([AIP_EventID]),
CONSTRAINT [FK_RepriceExport_RepriceExportStatus] FOREIGN KEY ([RepriceExportStatusID]) REFERENCES [dbo].[RepriceExportStatus] ([RepriceExportStatusID]),
CONSTRAINT [FK_RepriceExport_ThirdPartyEntityID] FOREIGN KEY ([ThirdPartyEntityID]) REFERENCES [dbo].[ThirdPartyEntity] ([ThirdPartyEntityID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[AIP_Event]'
GO
ALTER TABLE [dbo].[AIP_Event] ADD
CONSTRAINT [FK_AIP_Event_InventoryID] FOREIGN KEY ([InventoryID]) REFERENCES [dbo].[Inventory] ([InventoryID]),
CONSTRAINT [FK_AIP_Event_AIP_EventTypeID] FOREIGN KEY ([AIP_EventTypeID]) REFERENCES [dbo].[AIP_EventType] ([AIP_EventTypeID]),
CONSTRAINT [FK_AIP_Event__ThirdPartyEntityID] FOREIGN KEY ([ThirdPartyEntityID]) REFERENCES [dbo].[ThirdPartyEntity] ([ThirdPartyEntityID]),
CONSTRAINT [FK_AIP_Event_RepriceSource] FOREIGN KEY ([RepriceSourceID]) REFERENCES [dbo].[RepriceSource] ([RepriceSourceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[AIP_EventType]'
GO
ALTER TABLE [dbo].[AIP_EventType] ADD
CONSTRAINT [FK_AIP_EventType_AIP_EventCategoryID] FOREIGN KEY ([AIP_EventCategoryID]) REFERENCES [dbo].[AIP_EventCategory] ([AIP_EventCategoryID]),
CONSTRAINT [FK_AIP_EventType_ThirdPartyEntityTypeID] FOREIGN KEY ([ThirdPartyEntityTypeID]) REFERENCES [dbo].[ThirdPartyEntityType] ([ThirdPartyEntityTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[AppraisalActions]'
GO
ALTER TABLE [dbo].[AppraisalActions] ADD
CONSTRAINT [FK_AppraisalActions_Appraisals] FOREIGN KEY ([AppraisalID]) REFERENCES [dbo].[Appraisals] ([AppraisalID]),
CONSTRAINT [FK_AppraisalActions_AppraisalActionTypes] FOREIGN KEY ([AppraisalActionTypeID]) REFERENCES [dbo].[AppraisalActionTypes] ([AppraisalActionTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[AppraisalBookouts]'
GO
ALTER TABLE [dbo].[AppraisalBookouts] ADD
CONSTRAINT [FK_AppraisalBookouts_Bookouts] FOREIGN KEY ([BookoutID]) REFERENCES [dbo].[Bookouts] ([BookoutID]),
CONSTRAINT [FK_AppraisalBookouts_Appraisals] FOREIGN KEY ([AppraisalID]) REFERENCES [dbo].[Appraisals] ([AppraisalID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[AppraisalBusinessUnitGroupDemand]'
GO
ALTER TABLE [dbo].[AppraisalBusinessUnitGroupDemand] ADD
CONSTRAINT [FK_AppraisalBusinessUnitGroupDemand_Appraisals] FOREIGN KEY ([AppraisalID]) REFERENCES [dbo].[Appraisals] ([AppraisalID]),
CONSTRAINT [FK_AppraisalBusinessUnitGroupDemand_BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[AppraisalCustomer]'
GO
ALTER TABLE [dbo].[AppraisalCustomer] ADD
CONSTRAINT [FK_AppraisalCustomer_Appraisals] FOREIGN KEY ([AppraisalID]) REFERENCES [dbo].[Appraisals] ([AppraisalID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[AppraisalFormOptions]'
GO
ALTER TABLE [dbo].[AppraisalFormOptions] ADD
CONSTRAINT [FK_AppraisalFormOptions_Appraisals] FOREIGN KEY ([AppraisalID]) REFERENCES [dbo].[Appraisals] ([AppraisalID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[AppraisalPhotos]'
GO
ALTER TABLE [dbo].[AppraisalPhotos] ADD
CONSTRAINT [FK_AppraisalPhotos__Appraisal] FOREIGN KEY ([AppraisalID]) REFERENCES [dbo].[Appraisals] ([AppraisalID]),
CONSTRAINT [FK_AppraisalPhotos__Photos] FOREIGN KEY ([PhotoID]) REFERENCES [dbo].[Photos] ([PhotoID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[AppraisalWindowStickers]'
GO
ALTER TABLE [dbo].[AppraisalWindowStickers] ADD
CONSTRAINT [FK_AppraisalID_Appraisals] FOREIGN KEY ([AppraisalID]) REFERENCES [dbo].[Appraisals] ([AppraisalID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[AppraisalValues]'
GO
ALTER TABLE [dbo].[AppraisalValues] ADD
CONSTRAINT [FK_AppraisalValues_Appraisals] FOREIGN KEY ([AppraisalID]) REFERENCES [dbo].[Appraisals] ([AppraisalID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Staging_Bookouts]'
GO
ALTER TABLE [dbo].[Staging_Bookouts] ADD
CONSTRAINT [FK_StagingBookout_AppraisalID] FOREIGN KEY ([AppraisalID]) REFERENCES [dbo].[Appraisals] ([AppraisalID]),
CONSTRAINT [FK_StagingBookout_ThirdParty] FOREIGN KEY ([ThirdPartyID]) REFERENCES [dbo].[ThirdParties] ([ThirdPartyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Staging_VehicleHistoryReport]'
GO
ALTER TABLE [dbo].[Staging_VehicleHistoryReport] ADD
CONSTRAINT [FK_StagingVehicleHistoryReport_Appraisals] FOREIGN KEY ([AppraisalID]) REFERENCES [dbo].[Appraisals] ([AppraisalID]),
CONSTRAINT [FK_StagingVehicleHistoryReport_CredentialType] FOREIGN KEY ([CredentialTypeID]) REFERENCES [dbo].[CredentialType] ([CredentialTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Appraisals]'
GO
ALTER TABLE [dbo].[Appraisals] ADD
CONSTRAINT [FK_Appraisals_BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_Appraisals_tbl_Vehicle] FOREIGN KEY ([VehicleID]) REFERENCES [dbo].[tbl_Vehicle] ([VehicleID]),
CONSTRAINT [FK_Appraisals_lu_InventoryVehicleType] FOREIGN KEY ([DealTrackNewOrUsed]) REFERENCES [dbo].[lu_InventoryVehicleType] ([InventoryVehicleTypeCD]),
CONSTRAINT [FK_Appraisals_Person] FOREIGN KEY ([DealTrackSalespersonID]) REFERENCES [dbo].[Person] ([PersonID]),
CONSTRAINT [FK_Appraisals_AppraisalSource] FOREIGN KEY ([AppraisalSourceID]) REFERENCES [dbo].[AppraisalSource] ([AppraisalSourceID]),
CONSTRAINT [FK_Appraisal_AppraisalStatus] FOREIGN KEY ([AppraisalStatusID]) REFERENCES [dbo].[AppraisalStatus] ([AppraisalStatusID]),
CONSTRAINT [FK_Appraisals_AppraisalType] FOREIGN KEY ([AppraisalTypeID]) REFERENCES [dbo].[AppraisalType] ([AppraisalTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[AuditBookOuts]'
GO
ALTER TABLE [dbo].[AuditBookOuts] ADD
CONSTRAINT [FK_AuditBookOuts_BusinessUnitId] FOREIGN KEY ([BusinessUnitId]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_AuditBookOuts_MemberId] FOREIGN KEY ([MemberId]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_AuditBookOuts_ProductId] FOREIGN KEY ([ProductId]) REFERENCES [dbo].[BookOutTypes] ([BookOutTypeId]),
CONSTRAINT [FK_AuditBookOuts_BookId] FOREIGN KEY ([BookId]) REFERENCES [dbo].[ThirdParties] ([ThirdPartyID]),
CONSTRAINT [FK_AuditBookOuts_BookOutId] FOREIGN KEY ([BookOutId]) REFERENCES [dbo].[Bookouts] ([BookoutID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[BookoutProcessorRunLog]'
GO
ALTER TABLE [dbo].[BookoutProcessorRunLog] ADD
CONSTRAINT [FK_BookoutProcessorRun_BookoutProcessorMode] FOREIGN KEY ([BookoutProcessorModeId]) REFERENCES [dbo].[BookoutProcessorMode] ([BookoutProcessorModeId]),
CONSTRAINT [FK_BookoutProcessorRun_BusinessUnit] FOREIGN KEY ([BusinessUnitId]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_BookoutProcessorRun_ThirdParty] FOREIGN KEY ([ThirdPartyId]) REFERENCES [dbo].[ThirdParties] ([ThirdPartyID]),
CONSTRAINT [FK_BookoutProcessorRun_BookoutProcessorStatus] FOREIGN KEY ([BookoutProcessorStatusId]) REFERENCES [dbo].[BookoutProcessorStatus] ([BookoutProcessorStatusId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[BookoutThirdPartyCategories]'
GO
ALTER TABLE [dbo].[BookoutThirdPartyCategories] ADD
CONSTRAINT [FK_BookoutThirdPartyCategories_Bookouts] FOREIGN KEY ([BookoutID]) REFERENCES [dbo].[Bookouts] ([BookoutID]),
CONSTRAINT [FK_BookoutValues_ThirdPartyCategories] FOREIGN KEY ([ThirdPartyCategoryID]) REFERENCES [dbo].[ThirdPartyCategories] ([ThirdPartyCategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[BookoutThirdPartyVehicles]'
GO
ALTER TABLE [dbo].[BookoutThirdPartyVehicles] ADD
CONSTRAINT [FK_BookoutThirdPartyVehicles_BookoutID] FOREIGN KEY ([BookoutID]) REFERENCES [dbo].[Bookouts] ([BookoutID]),
CONSTRAINT [FK_BookoutThirdPartyVehicles_ThirdPartyVehicles] FOREIGN KEY ([ThirdPartyVehicleID]) REFERENCES [dbo].[ThirdPartyVehicles] ([ThirdPartyVehicleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[InventoryBookouts]'
GO
ALTER TABLE [dbo].[InventoryBookouts] ADD
CONSTRAINT [FK_InventoryBookouts_Bookouts] FOREIGN KEY ([BookoutID]) REFERENCES [dbo].[Bookouts] ([BookoutID]),
CONSTRAINT [FK_InventoryBookouts__Inventory] FOREIGN KEY ([InventoryID]) REFERENCES [dbo].[Inventory] ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Bookouts]'
GO
ALTER TABLE [dbo].[Bookouts] ADD
CONSTRAINT [FK_Bookouts_BookoutSources] FOREIGN KEY ([BookoutSourceID]) REFERENCES [dbo].[BookoutSources] ([BookoutSourceID]),
CONSTRAINT [FK_Bookouts_ThirdParties] FOREIGN KEY ([ThirdPartyID]) REFERENCES [dbo].[ThirdParties] ([ThirdPartyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[BookoutValues]'
GO
ALTER TABLE [dbo].[BookoutValues] ADD
CONSTRAINT [FK_BookoutValues_BookoutThirdPartyCategories] FOREIGN KEY ([BookoutThirdPartyCategoryID]) REFERENCES [dbo].[BookoutThirdPartyCategories] ([BookoutThirdPartyCategoryID]),
CONSTRAINT [FK_BookoutValues_BookoutValueTypes] FOREIGN KEY ([BookoutValueTypeID]) REFERENCES [dbo].[BookoutValueTypes] ([BookoutValueTypeID]),
CONSTRAINT [FK_BookoutValues_ThirdPartySubCategory] FOREIGN KEY ([ThirdPartySubCategoryID]) REFERENCES [dbo].[ThirdPartySubCategory] ([ThirdPartySubCategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[HalSessions]'
GO
ALTER TABLE [dbo].[HalSessions] ADD
CONSTRAINT [FK__HalSessio__Busin__6EB7E649] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[BusinessUnitCredential]'
GO
ALTER TABLE [dbo].[BusinessUnitCredential] ADD
CONSTRAINT [FK_BusinessUnitCredential_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_BusinessUnitCredential_CredentialTypeID] FOREIGN KEY ([CredentialTypeID]) REFERENCES [dbo].[CredentialType] ([CredentialTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[BusinessUnitPhotos]'
GO
ALTER TABLE [dbo].[BusinessUnitPhotos] ADD
CONSTRAINT [FK_BusinessUnitPhotos__BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_BusinessUnitPhotos__Photos] FOREIGN KEY ([PhotoID]) REFERENCES [dbo].[Photos] ([PhotoID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[BusinessUnitPreference_Bookout]'
GO
ALTER TABLE [dbo].[BusinessUnitPreference_Bookout] ADD
CONSTRAINT [FK_BusinessUnitPreference_Bookout__BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[BusinessUnitRelationship]'
GO
ALTER TABLE [dbo].[BusinessUnitRelationship] ADD
CONSTRAINT [FK_BusinessUnitRelationship_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_BusinessUnitRelationship_ParentID] FOREIGN KEY ([ParentID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[BusinessUnitStatus]'
GO
ALTER TABLE [dbo].[BusinessUnitStatus] ADD
CONSTRAINT [FK_BusinessUnitStatus__BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_BusinessUnitStatus__BusinessUnitStatusCode] FOREIGN KEY ([BusinessUnitStatusCodeID]) REFERENCES [dbo].[BusinessUnitStatusCode] ([BusinessUnitStatusCodeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[CIASummary]'
GO
ALTER TABLE [dbo].[CIASummary] ADD
CONSTRAINT [FK_CIASummary_BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_CIASummary_CIASummaryStatus] FOREIGN KEY ([CIASummaryStatusID]) REFERENCES [dbo].[CIASummaryStatus] ([CIASummaryStatusID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerAuctionPreference]'
GO
ALTER TABLE [dbo].[DealerAuctionPreference] ADD
CONSTRAINT [FK_DealerAuctionPreference__BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerCIAPreferences]'
GO
ALTER TABLE [dbo].[DealerCIAPreferences] ADD
CONSTRAINT [FK_DealerCIAPreferences_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerFacts]'
GO
ALTER TABLE [dbo].[DealerFacts] ADD
CONSTRAINT [FK_DealerFacts_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerFinancialStatement]'
GO
ALTER TABLE [dbo].[DealerFinancialStatement] ADD
CONSTRAINT [FK_DealerFinancialStatement_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerGridThresholds]'
GO
ALTER TABLE [dbo].[DealerGridThresholds] ADD
CONSTRAINT [FK_DealerGridThresholds_BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerGridValues]'
GO
ALTER TABLE [dbo].[DealerGridValues] ADD
CONSTRAINT [FK_DealerGridValues_BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerGroupPreference]'
GO
ALTER TABLE [dbo].[DealerGroupPreference] ADD
CONSTRAINT [FK_DealerGroupPreference_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerInsightTargetPreference]'
GO
ALTER TABLE [dbo].[DealerInsightTargetPreference] ADD
CONSTRAINT [FK_DealerInsightTargetPreference_BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_DealerInsightTargetPreference_InsightTarget] FOREIGN KEY ([InsightTargetID]) REFERENCES [dbo].[InsightTarget] ([InsightTargetID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerPack]'
GO
ALTER TABLE [dbo].[DealerPack] ADD
CONSTRAINT [FK_DealerPack__BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_DealerPack__lu_DealerPackType] FOREIGN KEY ([PackTypeCD]) REFERENCES [dbo].[lu_DealerPackType] ([PackTypeCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerPreference]'
GO
ALTER TABLE [dbo].[DealerPreference] ADD
CONSTRAINT [FK_DealerPreference__BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_DealerPreference_GuideBookID] FOREIGN KEY ([GuideBookID]) REFERENCES [dbo].[ThirdParties] ([ThirdPartyID]),
CONSTRAINT [FK_DealerPreference_BookOutPreferenceID] FOREIGN KEY ([BookOutPreferenceId]) REFERENCES [dbo].[ThirdPartyCategories] ([ThirdPartyCategoryID]),
CONSTRAINT [FK_DealerPreference_GuideBook2ID] FOREIGN KEY ([GuideBook2Id]) REFERENCES [dbo].[ThirdParties] ([ThirdPartyID]),
CONSTRAINT [FK_DealerPreference_GuideBook2BookoutPreferenceId] FOREIGN KEY ([GuideBook2BookOutPreferenceId]) REFERENCES [dbo].[ThirdPartyCategories] ([ThirdPartyCategoryID]),
CONSTRAINT [FK_DealerPreference_GuideBook2SecondBookoutPreferenceId] FOREIGN KEY ([GuideBook2SecondBookOutPreferenceId]) REFERENCES [dbo].[ThirdPartyCategories] ([ThirdPartyCategoryID]),
CONSTRAINT [FK_DealerPreference_BookoutPreferenceSecondId] FOREIGN KEY ([BookOutPreferenceSecondId]) REFERENCES [dbo].[ThirdPartyCategories] ([ThirdPartyCategoryID]),
CONSTRAINT [FK_DealerPreference__lu_ProgramType] FOREIGN KEY ([ProgramType_CD]) REFERENCES [dbo].[lu_ProgramType] ([ProgramType_CD]),
CONSTRAINT [FK_DealerPreferenceLithiaCarCenterMember_Member] FOREIGN KEY ([DefaultLithiaCarCenterMemberID]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerSalesChannel]'
GO
ALTER TABLE [dbo].[DealerSalesChannel] ADD
CONSTRAINT [FK_DealerSalesChannel_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_DealerSalesChannel_SalesChannelID] FOREIGN KEY ([SalesChannelID]) REFERENCES [dbo].[SalesChannel] ([SalesChannelID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerTransferAllowRetailOnlyRule]'
GO
ALTER TABLE [dbo].[DealerTransferAllowRetailOnlyRule] ADD
CONSTRAINT [FK_DealerTransferAllowRetailOnlyRule_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_DealerTransferAllowRetailOnlyRule_TransferPriceInventoryAgeRangeID] FOREIGN KEY ([TransferPriceInventoryAgeRangeID]) REFERENCES [dbo].[TransferPriceInventoryAgeRange] ([TransferPriceInventoryAgeRangeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerTransferPriceUnitCostRule]'
GO
ALTER TABLE [dbo].[DealerTransferPriceUnitCostRule] ADD
CONSTRAINT [FK_DealerTransferPriceUnitCostRule_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_DealerTransferPriceUnitCostRule_TransferPriceUnitCostValueID] FOREIGN KEY ([TransferPriceUnitCostValueID]) REFERENCES [dbo].[TransferPriceUnitCostValue] ([TransferPriceUnitCostValueID]),
CONSTRAINT [FK_DealerTransferPriceUnitCostRule_TransferPriceInventoryAgeRangeID] FOREIGN KEY ([TransferPriceInventoryAgeRangeID]) REFERENCES [dbo].[TransferPriceInventoryAgeRange] ([TransferPriceInventoryAgeRangeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[tbl_DealerValuation]'
GO
ALTER TABLE [dbo].[tbl_DealerValuation] ADD
CONSTRAINT [FK_DealerValuation_BusinessUnit] FOREIGN KEY ([DealerId]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Distribution].[Account]'
GO
ALTER TABLE [Distribution].[Account] ADD
CONSTRAINT [FK_Distribution_Account__BusinessUnit] FOREIGN KEY ([DealerID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_Distribution_Account__InsertMember] FOREIGN KEY ([InsertUserID]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Distribution_Account__UpdateMember] FOREIGN KEY ([UpdateUserID]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Distribution_Account__Provider] FOREIGN KEY ([ProviderID]) REFERENCES [Distribution].[Provider] ([ProviderID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Distribution].[Account_Audit]'
GO
ALTER TABLE [Distribution].[Account_Audit] ADD
CONSTRAINT [FK_Distribution_AccountAudit__BusinessUnit] FOREIGN KEY ([DealerID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_Distribution_AccountAudit__Member] FOREIGN KEY ([ChangeUserID]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Distribution_AccountAudit__Provider] FOREIGN KEY ([ProviderID]) REFERENCES [Distribution].[Provider] ([ProviderID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Distribution].[AccountPreferences]'
GO
ALTER TABLE [Distribution].[AccountPreferences] ADD
CONSTRAINT [FK_Distribution_AccountPreferences__BusinessUnit] FOREIGN KEY ([DealerID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_Distribution_AccountPreferences__ProviderMessageType] FOREIGN KEY ([ProviderID], [MessageTypeFlag]) REFERENCES [Distribution].[ProviderMessageType] ([ProviderID], [MessageTypeFlag])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Distribution].[Message]'
GO
ALTER TABLE [Distribution].[Message] ADD
CONSTRAINT [FK_Distribution_Message__BusinessUnit] FOREIGN KEY ([DealerID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_Distribution_Message__Member] FOREIGN KEY ([InsertUserID]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Distribution_Message__Vehicle] FOREIGN KEY ([VehicleEntityID], [VehicleEntityTypeID]) REFERENCES [Distribution].[Vehicle] ([VehicleEntityID], [VehicleEntityTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DMSExportBusinessUnitPreference]'
GO
ALTER TABLE [dbo].[DMSExportBusinessUnitPreference] ADD
CONSTRAINT [FK_DMSExportBusinessUnitPreference_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_DMSExportBusinessUnitPreference_DMSExport_ThirdPartyEntity] FOREIGN KEY ([DMSExportID]) REFERENCES [dbo].[DMSExport_ThirdPartyEntity] ([DMSExportID]),
CONSTRAINT [FK_DMSExportBusinessUnitPreference_DMSExportFrequency] FOREIGN KEY ([DMSExportFrequencyID]) REFERENCES [dbo].[DMSExportFrequency] ([DMSExportFrequencyID]),
CONSTRAINT [FK_DMSExportBusinessUnitPreference_DMSExportInternetPriceMapping] FOREIGN KEY ([DMSExportInternetPriceMappingID]) REFERENCES [dbo].[DMSExportPriceMapping] ([DMSExportPriceMappingID]),
CONSTRAINT [FK_DMSExportBusinessUnitPreference_DMSExportLotPriceMapping] FOREIGN KEY ([DMSExportLotPriceMappingID]) REFERENCES [dbo].[DMSExportPriceMapping] ([DMSExportPriceMappingID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerPreference_InternetAdvertisementBuilder]'
GO
ALTER TABLE [dbo].[DealerPreference_InternetAdvertisementBuilder] ADD
CONSTRAINT [FK_DP_InternetAdvertisementBuilder_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerPreference_JDPowerSettings]'
GO
ALTER TABLE [dbo].[DealerPreference_JDPowerSettings] ADD
CONSTRAINT [FK_DP_JDPowerSettings_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_DP_JDPowerSettings_PowerRegionID] FOREIGN KEY ([PowerRegionID]) REFERENCES [dbo].[JDPower_PowerRegion] ([PowerRegionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerPreference_Pricing]'
GO
ALTER TABLE [dbo].[DealerPreference_Pricing] ADD
CONSTRAINT [FK_DP_Pricing_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerPreference_WindowSticker]'
GO
ALTER TABLE [dbo].[DealerPreference_WindowSticker] ADD
CONSTRAINT [FK_DP_WindowSticker_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_DP_WindowSticker_WindowStickerTemplate] FOREIGN KEY ([WindowStickerTemplateId]) REFERENCES [dbo].[WindowStickerTemplate] ([WindowStickerTemplateId]),
CONSTRAINT [FK_DP_WindowSticker_WindowStickerBuyersGuideTemplate] FOREIGN KEY ([WindowStickerBuyersGuideTemplateId]) REFERENCES [dbo].[WindowStickerBuyersGuideTemplate] ([WindowStickerBuyersGuideTemplateId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[EquityAnalyzerSearches]'
GO
ALTER TABLE [dbo].[EquityAnalyzerSearches] ADD
CONSTRAINT [FK_EquityAnalyzerSearches_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_EquityAnalyzerSearchesThirdPartyCategoryID] FOREIGN KEY ([ThirdPartyCategoryID]) REFERENCES [dbo].[ThirdPartyCategories] ([ThirdPartyCategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Extract].[DealerConfiguration]'
GO
ALTER TABLE [Extract].[DealerConfiguration] ADD
CONSTRAINT [FK_Extract_DealerConfiguration__BusinessUnitId] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_Extract_DealerConfiguration__DestinationName] FOREIGN KEY ([DestinationName]) REFERENCES [Extract].[Destination] ([Name])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[FLUSAN_EventLog]'
GO
ALTER TABLE [dbo].[FLUSAN_EventLog] ADD
CONSTRAINT [FK_FLUSAN_EventLog_BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_FLUSAN_EventLog_FLUSAN_EventType] FOREIGN KEY ([FLUSAN_EventTypeCode]) REFERENCES [dbo].[FLUSAN_EventType] ([FLUSAN_EventTypeCode]),
CONSTRAINT [FK_FLUSAN_EventLog_Member] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_FLUSAN_EventLog_PurchasingCenterChannels] FOREIGN KEY ([ChannelID]) REFERENCES [dbo].[PurchasingCenterChannels] ([ChannelID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[GDLight]'
GO
ALTER TABLE [dbo].[GDLight] ADD
CONSTRAINT [FK_GDLight_BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_GDLight__tbl_GroupingDescription] FOREIGN KEY ([GroupingDescriptionId]) REFERENCES [dbo].[tbl_GroupingDescription] ([GroupingDescriptionID]),
CONSTRAINT [FK_GDLight_lu_VehicleLight] FOREIGN KEY ([InventoryVehicleLightID]) REFERENCES [dbo].[lu_VehicleLight] ([InventoryVehicleLightID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[tbl_GroupingPromotionPlan]'
GO
ALTER TABLE [dbo].[tbl_GroupingPromotionPlan] ADD
CONSTRAINT [FK_GroupingPromotionPlan_BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_tbl_GroupingPromotionPlan__tbl_GroupingDescription] FOREIGN KEY ([GroupingDescriptionId]) REFERENCES [dbo].[tbl_GroupingDescription] ([GroupingDescriptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[InternetAdvertiserDealership]'
GO
ALTER TABLE [dbo].[InternetAdvertiserDealership] ADD
CONSTRAINT [FK_InternetAdvertiserDealership__BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_InternetAdvertiserDealership__InternetAdvertiser] FOREIGN KEY ([InternetAdvertiserID]) REFERENCES [dbo].[InternetAdvertiser_ThirdPartyEntity] ([InternetAdvertiserID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Inventory]'
GO
ALTER TABLE [dbo].[Inventory] ADD
CONSTRAINT [FK_Inventory__BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_Inventory__tbl_Vehicle] FOREIGN KEY ([VehicleID]) REFERENCES [dbo].[tbl_Vehicle] ([VehicleID]),
CONSTRAINT [FK_Inventory__lu_InventoryVehicleType] FOREIGN KEY ([InventoryType]) REFERENCES [dbo].[lu_InventoryVehicleType] ([InventoryVehicleTypeCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[InventoryBucketRanges]'
GO
ALTER TABLE [dbo].[InventoryBucketRanges] ADD
CONSTRAINT [FK_InventoryBucketRanges_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_InventoryBucketRanges_InventoryBuckets] FOREIGN KEY ([InventoryBucketID]) REFERENCES [dbo].[InventoryBuckets] ([InventoryBucketID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[KBBArchiveDatasetOverride]'
GO
ALTER TABLE [dbo].[KBBArchiveDatasetOverride] ADD
CONSTRAINT [FK_KBBArchiveDatasetOverride_BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_KBBArchiveDatasetOverride_Member] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[MarketShare]'
GO
ALTER TABLE [dbo].[MarketShare] ADD
CONSTRAINT [FK_MarketShare_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[MarketToZip]'
GO
ALTER TABLE [dbo].[MarketToZip] ADD
CONSTRAINT [FK_MarketToZip_BusinessUnitID] FOREIGN KEY ([BusinessUnitId]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[MemberAccess]'
GO
ALTER TABLE [dbo].[MemberAccess] ADD
CONSTRAINT [FK_MemberAccess_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_MemberAccess_MemberID] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[MemberAppraisalReviewPreferences]'
GO
ALTER TABLE [dbo].[MemberAppraisalReviewPreferences] ADD
CONSTRAINT [FK_MemberAppraisalReviewPreferences_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_MemberAppraisalReviewPreferences_MemberID] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[tbl_MemberATCAccessGroupList]'
GO
ALTER TABLE [dbo].[tbl_MemberATCAccessGroupList] ADD
CONSTRAINT [FK_MemberATCAccessGroupList_BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_MemberATCAccessGroupList_Member] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_MemberATCAccessGroupList_MemberATCAccessGroupListType] FOREIGN KEY ([MemberATCAccessGroupListTypeID]) REFERENCES [dbo].[tbl_MemberATCAccessGroupListType] ([MemberATCAccessGroupListTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[MemberBusinessUnitSet]'
GO
ALTER TABLE [dbo].[MemberBusinessUnitSet] ADD
CONSTRAINT [FK_MemberBusinessUnitSet_BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_MemberBusinessUnitSet_Member] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_MemberBusinessUnitSet_Type] FOREIGN KEY ([MemberBusinessUnitSetTypeID]) REFERENCES [dbo].[MemberBusinessUnitSetType] ([MemberBusinessUnitSetTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[MemberBusinessUnitSetItem]'
GO
ALTER TABLE [dbo].[MemberBusinessUnitSetItem] ADD
CONSTRAINT [FK_MemberBusinessUnitSetItem_BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_MemberBusinessUnitSetItem_Set] FOREIGN KEY ([MemberBusinessUnitSetID]) REFERENCES [dbo].[MemberBusinessUnitSet] ([MemberBusinessUnitSetID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[MemberInsightTypePreference]'
GO
ALTER TABLE [dbo].[MemberInsightTypePreference] ADD
CONSTRAINT [FK_MemberInsightType_BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_MemberInsightType_InsightType] FOREIGN KEY ([InsightTypeID]) REFERENCES [dbo].[InsightType] ([InsightTypeID]),
CONSTRAINT [FK_MemberInsightType_Member] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[tbl_NewCarScoreCard]'
GO
ALTER TABLE [dbo].[tbl_NewCarScoreCard] ADD
CONSTRAINT [FK_NewCarScoreCard_BusinessUnit] FOREIGN KEY ([BusinessUnitId]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[PackAdjustRules]'
GO
ALTER TABLE [dbo].[PackAdjustRules] ADD
CONSTRAINT [FK_PackAdjustRules_BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Person]'
GO
ALTER TABLE [dbo].[Person] ADD
CONSTRAINT [FK_Person_BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_Person_Member] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[PhotoProviderDealership]'
GO
ALTER TABLE [dbo].[PhotoProviderDealership] ADD
CONSTRAINT [FK_PhotoProviderDealership__BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_PhotoProviderDealership__PhotoProvider_ThirdPartyEntity] FOREIGN KEY ([PhotoProviderID]) REFERENCES [dbo].[PhotoProvider_ThirdPartyEntity] ([PhotoProviderID]),
CONSTRAINT [FK_PhotoProviderDealership__PhotoStorage] FOREIGN KEY ([PhotoStorageCode_Override]) REFERENCES [dbo].[PhotoStorage] ([PhotoStorageCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ReportCenterSessions]'
GO
ALTER TABLE [dbo].[ReportCenterSessions] ADD
CONSTRAINT [FK_ReportCenterSession_BusinessUnit] FOREIGN KEY ([BusinessUnitId]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_ReportCenterSession_Member] FOREIGN KEY ([MemberId]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[SearchCandidateList]'
GO
ALTER TABLE [dbo].[SearchCandidateList] ADD
CONSTRAINT [FK_SearchCandidateList_BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_SearchCandidateList_Member] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_SearchCandidateList_SearchCandidateListType] FOREIGN KEY ([SearchCandidateListTypeID]) REFERENCES [dbo].[SearchCandidateListType] ([SearchCandidateListTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[SoftwareSystemComponentState]'
GO
ALTER TABLE [dbo].[SoftwareSystemComponentState] ADD
CONSTRAINT [FK_SoftwareSystemComponentState_BusinessUnit_Dealer] FOREIGN KEY ([DealerID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_SoftwareSystemComponentState_BusinessUnit_DealerGroup] FOREIGN KEY ([DealerGroupID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_SoftwareSystemComponentState_Member_Authorized] FOREIGN KEY ([AuthorizedMemberID]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_SoftwareSystemComponentState_Member_Impersonating] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_SoftwareSystemComponentState_SoftwareSystemComponent] FOREIGN KEY ([SoftwareSystemComponentID]) REFERENCES [dbo].[SoftwareSystemComponent] ([SoftwareSystemComponentID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[SystemErrors]'
GO
ALTER TABLE [dbo].[SystemErrors] ADD
CONSTRAINT [FK_SystemErrors_BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_SystemErrors_Memeber] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[tbl_CIAPreferences]'
GO
ALTER TABLE [dbo].[tbl_CIAPreferences] ADD
CONSTRAINT [FK_tbl_CIAPreferences__BusinessUnit] FOREIGN KEY ([BusinessUnitId]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_tbl_CIAPreferences_CiaCoreModelDeterminationBasisPeriodId] FOREIGN KEY ([CiaCoreModelDeterminationBasisPeriodId]) REFERENCES [dbo].[tbl_CIACompositeTimePeriod] ([CIACompositeTimePeriodId]),
CONSTRAINT [FK_tbl_CIAPreferences_CiaCoreModelYearAllocationBasisPeriodId] FOREIGN KEY ([CiaCoreModelYearAllocationBasisPeriodId]) REFERENCES [dbo].[tbl_CIACompositeTimePeriod] ([CIACompositeTimePeriodId]),
CONSTRAINT [FK_tbl_CIAPreferences_CIAgdLightsProcessorTimePeriod] FOREIGN KEY ([GDLightProcessorTimePeriodId]) REFERENCES [dbo].[tbl_CIACompositeTimePeriod] ([CIACompositeTimePeriodId]),
CONSTRAINT [FK_tbl_CIAPreferences_CiaPowerzoneModelTargetInventoryBasisPeriodId] FOREIGN KEY ([CiaPowerzoneModelTargetInventoryBasisPeriodId]) REFERENCES [dbo].[tbl_CIACompositeTimePeriod] ([CIACompositeTimePeriodId]),
CONSTRAINT [FK_tbl_CIAPreferences_CIASalesHistoryDisplayTimePeriodId] FOREIGN KEY ([SalesHistoryDisplayTimePeriodId]) REFERENCES [dbo].[tbl_CIACompositeTimePeriod] ([CIACompositeTimePeriodId]),
CONSTRAINT [FK_tbl_CIAPreferences_CiaStoreTargetInventoryBasisPeriodId] FOREIGN KEY ([CiaStoreTargetInventoryBasisPeriodId]) REFERENCES [dbo].[tbl_CIACompositeTimePeriod] ([CIACompositeTimePeriodId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[tbl_RedistributionLog]'
GO
ALTER TABLE [dbo].[tbl_RedistributionLog] ADD
CONSTRAINT [FK_tbl_RedistributionLog_Dealer] FOREIGN KEY ([OriginatingDealerId]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_tbl_RedistributionLog_lu_RedistributionSource] FOREIGN KEY ([RedistributionSourceId]) REFERENCES [dbo].[lu_RedistributionSource] ([RedistributionSourceId]),
CONSTRAINT [FK_tbl_RedistributionLog_Member] FOREIGN KEY ([MemberId]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ThirdPartyEntity]'
GO
ALTER TABLE [dbo].[ThirdPartyEntity] ADD
CONSTRAINT [FK_ThirdPartyEntity_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID]),
CONSTRAINT [FK_ThirdPartyEntity_ThirdPartyEntityTypeID] FOREIGN KEY ([ThirdPartyEntityTypeID]) REFERENCES [dbo].[ThirdPartyEntityType] ([ThirdPartyEntityTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[tbl_UsedCarScoreCard]'
GO
ALTER TABLE [dbo].[tbl_UsedCarScoreCard] ADD
CONSTRAINT [FK_UsedCarScoreCard_BusinessUnit] FOREIGN KEY ([BusinessUnitId]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[VehicleBookoutState]'
GO
ALTER TABLE [dbo].[VehicleBookoutState] ADD
CONSTRAINT [FK_VehicleBookoutState_BusinessUnit] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerPreference_KBBConsumerTool]'
GO
ALTER TABLE [dbo].[DealerPreference_KBBConsumerTool] ADD
CONSTRAINT [PK_DealerPreference_KBBConsumerTool__BusinessUnitId] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[BusinessUnit]'
GO
ALTER TABLE [dbo].[BusinessUnit] ADD
CONSTRAINT [FK_BusinessUnit_BusinessUnitType] FOREIGN KEY ([BusinessUnitTypeID]) REFERENCES [dbo].[BusinessUnitType] ([BusinessUnitTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[CIAGroupingItems]'
GO
ALTER TABLE [dbo].[CIAGroupingItems] ADD
CONSTRAINT [FK_CIAGroupingItems_CIABodyTypeDetails] FOREIGN KEY ([CIABodyTypeDetailID]) REFERENCES [dbo].[CIABodyTypeDetails] ([CIABodyTypeDetailID]),
CONSTRAINT [FK_CIAGroupingItems_CIACategories] FOREIGN KEY ([CIACategoryID]) REFERENCES [dbo].[CIACategories] ([CIACategoryID]),
CONSTRAINT [FK_CIAGroupingItems_GroupingDescriptionID] FOREIGN KEY ([GroupingDescriptionID]) REFERENCES [dbo].[tbl_GroupingDescription] ([GroupingDescriptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[CIABodyTypeDetails]'
GO
ALTER TABLE [dbo].[CIABodyTypeDetails] ADD
CONSTRAINT [FK_CIABodyTypeDetails__Segment] FOREIGN KEY ([SegmentID]) REFERENCES [dbo].[Segment] ([SegmentID]),
CONSTRAINT [FK_CIABodyTypeDetail_CIASummary] FOREIGN KEY ([CIASummaryID]) REFERENCES [dbo].[CIASummary] ([CIASummaryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[CIABuckets]'
GO
ALTER TABLE [dbo].[CIABuckets] ADD
CONSTRAINT [FK_CIABuckets_CIABucketGroups] FOREIGN KEY ([CIABucketGroupID]) REFERENCES [dbo].[CIABucketGroups] ([CIABucketGroupID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[CIABucketRules]'
GO
ALTER TABLE [dbo].[CIABucketRules] ADD
CONSTRAINT [FK_CIABucketRules_CIABuckets] FOREIGN KEY ([CIABucketID]) REFERENCES [dbo].[CIABuckets] ([CIABucketID]),
CONSTRAINT [FK_CIABucketRules_CIABucketRuleTypes] FOREIGN KEY ([CIABucketRuleTypeID]) REFERENCES [dbo].[CIABucketRuleTypes] ([CIABucketRuleTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[SearchCandidateCIACategoryAnnotation]'
GO
ALTER TABLE [dbo].[SearchCandidateCIACategoryAnnotation] ADD
CONSTRAINT [FK_SearchCandidateCIACategoryAnnotation_CIACategories] FOREIGN KEY ([CIACategoryID]) REFERENCES [dbo].[CIACategories] ([CIACategoryID]),
CONSTRAINT [FK_SearchCandidateCIACategoryAnnotation_SearchCandidate] FOREIGN KEY ([SearchCandidateID]) REFERENCES [dbo].[SearchCandidate] ([SearchCandidateID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[CIAGroupingItemDetails]'
GO
ALTER TABLE [dbo].[CIAGroupingItemDetails] ADD
CONSTRAINT [FK_CIAGroupingItemDetails_CIAGroupingItemDetailLevels] FOREIGN KEY ([CIAGroupingItemDetailLevelID]) REFERENCES [dbo].[CIAGroupingItemDetailLevels] ([CIAGroupingItemDetailLevelID]),
CONSTRAINT [FK_CIAGroupingItemDetails_CIAGroupingItemID] FOREIGN KEY ([CIAGroupingItemID]) REFERENCES [dbo].[CIAGroupingItems] ([CIAGroupingItemID]),
CONSTRAINT [FK_CIAGroupingItemDetails_CIAGroupingItemDetailTypes] FOREIGN KEY ([CIAGroupingItemDetailTypeID]) REFERENCES [dbo].[CIAGroupingItemDetailTypes] ([CIAGroupingItemDetailTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[CIAInventoryOverstocking]'
GO
ALTER TABLE [dbo].[CIAInventoryOverstocking] ADD
CONSTRAINT [FK_CIAInventoryOverstocking_CIASummary] FOREIGN KEY ([CIASummaryID]) REFERENCES [dbo].[CIASummary] ([CIASummaryID]),
CONSTRAINT [FK_CIAInventoryOverstocking__Inventory] FOREIGN KEY ([InventoryID]) REFERENCES [dbo].[Inventory] ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[MemberCredential]'
GO
ALTER TABLE [dbo].[MemberCredential] ADD
CONSTRAINT [FK_MemberCredential__CredentialType] FOREIGN KEY ([CredentialTypeID]) REFERENCES [dbo].[CredentialType] ([CredentialTypeID]),
CONSTRAINT [FK_MemberCredential__Member] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[OptionDetail]'
GO
ALTER TABLE [dbo].[OptionDetail] ADD
CONSTRAINT [FK_OptionDetail_DataSourceID] FOREIGN KEY ([DataSourceID]) REFERENCES [dbo].[DataSource] ([DataSourceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerInternetAdvertisementBuilderOptionProvider]'
GO
ALTER TABLE [dbo].[DealerInternetAdvertisementBuilderOptionProvider] ADD
CONSTRAINT [FK_DealerInternetAdvertisementBuilderOptionProvider_BusinessUnitID] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[DealerPreference_InternetAdvertisementBuilder] ([BusinessUnitID]),
CONSTRAINT [FK_DealerInternetAdvertisementBuilderOptionProvider_InternetAdvertisementBuilderOptionProviderID] FOREIGN KEY ([InternetAdvertisementBuilderOptionProviderID]) REFERENCES [dbo].[InternetAdvertisementBuilderOptionProvider] ([Id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerPreference_Dataload]'
GO
ALTER TABLE [dbo].[DealerPreference_Dataload] ADD
CONSTRAINT [FK_DealerPreference_Dataload__DealerPreference] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[DealerPreference] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerRisk]'
GO
ALTER TABLE [dbo].[DealerRisk] ADD
CONSTRAINT [FK_DealerRisk__DealerPreference] FOREIGN KEY ([BusinessUnitId]) REFERENCES [dbo].[DealerPreference] ([BusinessUnitID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealerUpgrade]'
GO
ALTER TABLE [dbo].[DealerUpgrade] ADD
CONSTRAINT [FK_DealerUpgrade_lu_DealerUpgrade] FOREIGN KEY ([DealerUpgradeCD]) REFERENCES [dbo].[lu_DealerUpgrade] ([DealerUpgradeCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealFinance]'
GO
ALTER TABLE [dbo].[DealFinance] ADD
CONSTRAINT [FK_DealFinance__InventoryID] FOREIGN KEY ([InventoryId]) REFERENCES [dbo].[Inventory] ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealLease]'
GO
ALTER TABLE [dbo].[DealLease] ADD
CONSTRAINT [FK_DealLease__InventoryID] FOREIGN KEY ([InventoryId]) REFERENCES [dbo].[Inventory] ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DealTradeIn]'
GO
ALTER TABLE [dbo].[DealTradeIn] ADD
CONSTRAINT [FK_DealTradeIn__InventoryID] FOREIGN KEY ([InventoryId]) REFERENCES [dbo].[Inventory] ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DemoDealerBaseline_Inventory]'
GO
ALTER TABLE [dbo].[DemoDealerBaseline_Inventory] ADD
CONSTRAINT [FK_DemoDealerBaseline_Inventory__Inventory] FOREIGN KEY ([InventoryID]) REFERENCES [dbo].[Inventory] ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[DMSExport_ThirdPartyEntity]'
GO
ALTER TABLE [dbo].[DMSExport_ThirdPartyEntity] ADD
CONSTRAINT [FK_DMSExport_ThirdPartyEntity_ThirdPartyEntity] FOREIGN KEY ([DMSExportID]) REFERENCES [dbo].[ThirdPartyEntity] ([ThirdPartyEntityID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[FirstLookRegionToZip]'
GO
ALTER TABLE [dbo].[FirstLookRegionToZip] ADD
CONSTRAINT [FK_FirstLookRegionToZip_FirstLookRegionID] FOREIGN KEY ([FirstLookRegionID]) REFERENCES [dbo].[FirstLookRegion] ([FirstLookRegionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[FranchiseAlias]'
GO
ALTER TABLE [dbo].[FranchiseAlias] ADD
CONSTRAINT [FK_FranchiseAlias__Franchise] FOREIGN KEY ([FranchiseID]) REFERENCES [dbo].[Franchise] ([FranchiseID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Member]'
GO
ALTER TABLE [dbo].[Member] ADD
CONSTRAINT [FK_Member_GroupHomepageID] FOREIGN KEY ([GroupHomePageID]) REFERENCES [dbo].[HomePage] ([HomePageID]),
CONSTRAINT [FK_Member_JobTitle] FOREIGN KEY ([JobTitleID]) REFERENCES [dbo].[JobTitle] ([JobTitleID]),
CONSTRAINT [FK_Member_lu_UserRole] FOREIGN KEY ([UserRoleCD]) REFERENCES [dbo].[lu_UserRole] ([UserRoleCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[InsightType]'
GO
ALTER TABLE [dbo].[InsightType] ADD
CONSTRAINT [FK_InsightType_InsightCategory] FOREIGN KEY ([InsightCategoryID]) REFERENCES [dbo].[InsightCategory] ([InsightCategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[InsightTarget]'
GO
ALTER TABLE [dbo].[InsightTarget] ADD
CONSTRAINT [FK_InsightTarget_InsightTargetType] FOREIGN KEY ([InsightTargetTypeID]) REFERENCES [dbo].[InsightTargetType] ([InsightTargetTypeID]),
CONSTRAINT [FK_InsightTarget_InsightTargetValueType] FOREIGN KEY ([InsightTargetValueTypeID]) REFERENCES [dbo].[InsightTargetValueType] ([InsightTargetValueTypeID]),
CONSTRAINT [FK_InsightTarget_InsightType] FOREIGN KEY ([InsightTypeID]) REFERENCES [dbo].[InsightType] ([InsightTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[InternetAdvertisement]'
GO
ALTER TABLE [dbo].[InternetAdvertisement] ADD
CONSTRAINT [FK_InternetAdvertisement_InventoryID] FOREIGN KEY ([InventoryID]) REFERENCES [dbo].[Inventory] ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[InternetAdvertiser_ThirdPartyEntity]'
GO
ALTER TABLE [dbo].[InternetAdvertiser_ThirdPartyEntity] ADD
CONSTRAINT [FK_InternetAdvertiser_ThirdPartyEntity_InternetAdvertiserID] FOREIGN KEY ([InternetAdvertiserID]) REFERENCES [dbo].[ThirdPartyEntity] ([ThirdPartyEntityID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[InternetAdvertiserBuildList]'
GO
ALTER TABLE [dbo].[InternetAdvertiserBuildList] ADD
CONSTRAINT [FK_InternetAdvertiserBuildList__InternetAdvertiser] FOREIGN KEY ([InternetAdvertiserID]) REFERENCES [dbo].[InternetAdvertiser_ThirdPartyEntity] ([InternetAdvertiserID]),
CONSTRAINT [FK_InternetAdvertiserBuildList__Inventory] FOREIGN KEY ([InventoryID]) REFERENCES [dbo].[Inventory] ([InventoryID]),
CONSTRAINT [FK_InternetAdvertiserBuildList_InternetAdvertiserExportStatus] FOREIGN KEY ([ExportStatusCD]) REFERENCES [dbo].[InternetAdvertiserExportStatus] ([ExportStatusCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[InternetAdvertiserBuildLog]'
GO
ALTER TABLE [dbo].[InternetAdvertiserBuildLog] ADD
CONSTRAINT [FK_InternetAdvertiserBuildLog__InternetAdvertiser] FOREIGN KEY ([InternetAdvertiserID]) REFERENCES [dbo].[InternetAdvertiser_ThirdPartyEntity] ([InternetAdvertiserID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[InternetAdvertiserBuildLogDetail]'
GO
ALTER TABLE [dbo].[InternetAdvertiserBuildLogDetail] ADD
CONSTRAINT [FK_InternetAdvertiserBuildLogDetail__InternetAdvertiserBuildLog] FOREIGN KEY ([InternetAdvertiserBuildLogID]) REFERENCES [dbo].[InternetAdvertiserBuildLog] ([InternetAdvertiserBuildLogID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Inventory_Advertising]'
GO
ALTER TABLE [dbo].[Inventory_Advertising] ADD
CONSTRAINT [FK_Inventory_Advertising_InventoryID] FOREIGN KEY ([InventoryID]) REFERENCES [dbo].[Inventory] ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Inventory_Insight]'
GO
ALTER TABLE [dbo].[Inventory_Insight] ADD
CONSTRAINT [FK_Inventory_Insight__InventoryID] FOREIGN KEY ([InventoryID]) REFERENCES [dbo].[Inventory] ([InventoryID]),
CONSTRAINT [FK_Inventory_Level4Analysis__InventoryID] FOREIGN KEY ([InventoryID]) REFERENCES [dbo].[Inventory] ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Inventory_Tracking]'
GO
ALTER TABLE [dbo].[Inventory_Tracking] ADD
CONSTRAINT [FK_Inventory_Tracking__InventoryID] FOREIGN KEY ([InventoryID]) REFERENCES [dbo].[Inventory] ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[InventoryPhotos]'
GO
ALTER TABLE [dbo].[InventoryPhotos] ADD
CONSTRAINT [FK_InventoryPhotos__Inventory] FOREIGN KEY ([InventoryID]) REFERENCES [dbo].[Inventory] ([InventoryID]),
CONSTRAINT [FK_InventoryPhotos__Photos] FOREIGN KEY ([PhotoID]) REFERENCES [dbo].[Photos] ([PhotoID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[MarketplaceSubmission]'
GO
ALTER TABLE [dbo].[MarketplaceSubmission] ADD
CONSTRAINT [FK_MarketplaceSubmission_Inventory] FOREIGN KEY ([InventoryID]) REFERENCES [dbo].[Inventory] ([InventoryID]),
CONSTRAINT [FK_MarketplaceSubmission_Marketplace_ThirdPartyEntity] FOREIGN KEY ([MarketplaceID]) REFERENCES [dbo].[Marketplace_ThirdPartyEntity] ([MarketplaceID]),
CONSTRAINT [FK_MarketplaceSubmission_Member] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_MarketplaceSubmission_MarketplaceSubmissionStatusCode] FOREIGN KEY ([MarketplaceSubmissionStatusCD]) REFERENCES [dbo].[MarketplaceSubmissionStatusCode] ([MarketplaceSubmissionStatusCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[tbl_VehicleSale]'
GO
ALTER TABLE [dbo].[tbl_VehicleSale] ADD
CONSTRAINT [FK_tbl_VehicleSale__Inventory] FOREIGN KEY ([InventoryID]) REFERENCES [dbo].[Inventory] ([InventoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[map_MemberToInvStatusFilter]'
GO
ALTER TABLE [dbo].[map_MemberToInvStatusFilter] ADD
CONSTRAINT [FK_MemberToInvStatusFilters_InventoryStatusCodes] FOREIGN KEY ([InventoryStatusCD]) REFERENCES [dbo].[InventoryStatusCodes] ([InventoryStatusCD]),
CONSTRAINT [FK_MemberToInvStatusFilters_Member] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[JDPower_DealerMarketArea]'
GO
ALTER TABLE [dbo].[JDPower_DealerMarketArea] ADD
CONSTRAINT [FK_JDPower_DealerMarketArea_PowerRegionID] FOREIGN KEY ([PowerRegionID]) REFERENCES [dbo].[JDPower_PowerRegion] ([PowerRegionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[MemberLicenseAcceptance]'
GO
ALTER TABLE [dbo].[MemberLicenseAcceptance] ADD
CONSTRAINT [FK_MemberLicenseAcceptance_LicenseID] FOREIGN KEY ([LicenseID]) REFERENCES [dbo].[License] ([LicenseID]),
CONSTRAINT [FK_MemberLicenseAcceptance_MemberID] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[tbl_CIACompositeTimePeriod]'
GO
ALTER TABLE [dbo].[tbl_CIACompositeTimePeriod] ADD
CONSTRAINT [FK_tbl_CIACompositeTimePeriod_BusinessUnit] FOREIGN KEY ([ForecastTimePeriodId]) REFERENCES [dbo].[lu_CIATimePeriod] ([CIATimePeriodId]),
CONSTRAINT [FK_tbl_CIACompositeTimePeriod_lu_CIATimePeriod_Prior] FOREIGN KEY ([PriorTimePeriodId]) REFERENCES [dbo].[lu_CIATimePeriod] ([CIATimePeriodId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[SubscriptionTypeDealerUpgrade]'
GO
ALTER TABLE [dbo].[SubscriptionTypeDealerUpgrade] ADD
CONSTRAINT [FK_SubscriptionTypeDealerUpgrade_DealerUpgradeCD] FOREIGN KEY ([DealerUpgradeCD]) REFERENCES [dbo].[lu_DealerUpgrade] ([DealerUpgradeCD]),
CONSTRAINT [FK_SubscriptionTypeDealerUpgrade_SubscriptionTypeID] FOREIGN KEY ([SubscriptionTypeID]) REFERENCES [dbo].[SubscriptionTypes] ([SubscriptionTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[tbl_Vehicle]'
GO
ALTER TABLE [dbo].[tbl_Vehicle] ADD
CONSTRAINT [FK_tbl_Vehicle_MakeModelGrouping] FOREIGN KEY ([MakeModelGroupingID]) REFERENCES [dbo].[MakeModelGrouping] ([MakeModelGroupingID]),
CONSTRAINT [FK_tbl_Vehicle__VehicleSources] FOREIGN KEY ([VehicleSourceID]) REFERENCES [dbo].[VehicleSources] ([VehicleSourceID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[MakeModelGrouping]'
GO
ALTER TABLE [dbo].[MakeModelGrouping] ADD
CONSTRAINT [FK_tbl_MakeModelGrouping__tbl_GroupingDescription] FOREIGN KEY ([GroupingDescriptionID]) REFERENCES [dbo].[tbl_GroupingDescription] ([GroupingDescriptionID]),
CONSTRAINT [FK_MakeModelGrouping__Segment] FOREIGN KEY ([SegmentID]) REFERENCES [dbo].[Segment] ([SegmentID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Distribution].[Publication]'
GO
ALTER TABLE [Distribution].[Publication] ADD
CONSTRAINT [FK_Distribution_Publication__InsertMember] FOREIGN KEY ([InsertUserID]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Distribution_Publication__UpdateMember] FOREIGN KEY ([UpdateUserID]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Distribution_Publication__Message] FOREIGN KEY ([MessageID]) REFERENCES [Distribution].[Message] ([MessageID]),
CONSTRAINT [FK_Distribution_Publication__PublicationStatus] FOREIGN KEY ([PublicationStatusID]) REFERENCES [Distribution].[PublicationStatus] ([PublicationStatusID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Distribution].[Vehicle]'
GO
ALTER TABLE [Distribution].[Vehicle] ADD
CONSTRAINT [FK_Distribution_Vehicle__Member] FOREIGN KEY ([InsertUserID]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Distribution_Vehicle__VehicleEntityType] FOREIGN KEY ([VehicleEntityTypeID]) REFERENCES [Distribution].[VehicleEntityType] ([VehicleEntityTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Marketing].[ConsumerHighlightPreference]'
GO
ALTER TABLE [Marketing].[ConsumerHighlightPreference] ADD
CONSTRAINT [FK_Marketing_ConsumerHighlightPreference__InsertMember] FOREIGN KEY ([InsertUserId]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Marketing_ConsumerHighlightPreference__UpdateMember] FOREIGN KEY ([UpdateUserId]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Marketing].[ConsumerHighlightSectionPreference]'
GO
ALTER TABLE [Marketing].[ConsumerHighlightSectionPreference] ADD
CONSTRAINT [FK_Marketing_ConsumerHighlightSectionPreference__InsertMember] FOREIGN KEY ([InsertUserId]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Marketing_ConsumerHighlightSectionPreference__UpdateMember] FOREIGN KEY ([UpdateUserId]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Marketing_ConsumerHighlightSectionPreference__ConsumerHighlightSection] FOREIGN KEY ([ConsumerHighlightSectionId]) REFERENCES [Marketing].[ConsumerHighlightSection] ([ConsumerHighlightSectionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Marketing].[Equipment]'
GO
ALTER TABLE [Marketing].[Equipment] ADD
CONSTRAINT [FK_Marketing_Equipment__InsertUser] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Marketing_Equipment__UpdateUser] FOREIGN KEY ([UpdateUser]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Marketing_Equipment__EquipmentList] FOREIGN KEY ([EquipmentListID]) REFERENCES [Marketing].[EquipmentList] ([EquipmentListID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Marketing].[EquipmentList]'
GO
ALTER TABLE [Marketing].[EquipmentList] ADD
CONSTRAINT [FK_Marketing_EquipmentList__InsertUser] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Marketing_EquipmentList__EquipmentProvider] FOREIGN KEY ([EquipmentProviderID]) REFERENCES [Marketing].[EquipmentProvider] ([EquipmentProviderID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Marketing].[EquipmentProviderAssignment]'
GO
ALTER TABLE [Marketing].[EquipmentProviderAssignment] ADD
CONSTRAINT [FK_Marketing_EquipmentProviderAssignment__InsertUser] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Marketing_EquipmentProviderAssignment__UpdateUser] FOREIGN KEY ([UpdateUser]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Marketing_EquipmentProviderAssignment__EquipmentProvider] FOREIGN KEY ([EquipmentProviderID]) REFERENCES [Marketing].[EquipmentProvider] ([EquipmentProviderID]),
CONSTRAINT [FK_Marketing_EquipmentProviderAssignment__EquipmentProviderAssignmentList] FOREIGN KEY ([EquipmentProviderAssignmentListID]) REFERENCES [Marketing].[EquipmentProviderAssignmentList] ([EquipmentProviderAssignmentListID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Marketing].[EquipmentProviderAssignmentList]'
GO
ALTER TABLE [Marketing].[EquipmentProviderAssignmentList] ADD
CONSTRAINT [FK_Marketing_EquipmentProviderAssignmentList__InsertUser] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Marketing].[MarketListingPreference]'
GO
ALTER TABLE [Marketing].[MarketListingPreference] ADD
CONSTRAINT [FK_Marketing_MarketListingPreference_Member_InsertUser] FOREIGN KEY ([InsertUserId]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Marketing_MarketListingPreference_Member_UpdateUser] FOREIGN KEY ([UpdateUserId]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Marketing].[MarketListingVehiclePreference]'
GO
ALTER TABLE [Marketing].[MarketListingVehiclePreference] ADD
CONSTRAINT [FK_Marketing_MarketListingVehiclePreference_Member_InsertUser] FOREIGN KEY ([InsertUserID]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Marketing_MarketListingVehiclePreference_Member_UpdateUser] FOREIGN KEY ([UpdateUserId]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Marketing].[MarketListingVehiclePreferenceSearchOverrides]'
GO
ALTER TABLE [Marketing].[MarketListingVehiclePreferenceSearchOverrides] ADD
CONSTRAINT [FK_Marketing_MarketListingVehiclePreferenceSearchOverrides_Member_InsertUserId] FOREIGN KEY ([InsertUserId]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Marketing_MarketListingVehiclePreferenceSearchOverrides_Member_UpdateUserId] FOREIGN KEY ([UpdateUserId]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Marketing_MarketListingVehiclePreferenceSearchOverrides_MarketListingVehiclePreference_MarketListingVehiclePreferenceId] FOREIGN KEY ([MarketListingVehiclePreferenceId]) REFERENCES [Marketing].[MarketListingVehiclePreference] ([MarketListingVehiclePreferenceId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Marketing].[PhotoVehiclePreference]'
GO
ALTER TABLE [Marketing].[PhotoVehiclePreference] ADD
CONSTRAINT [FK_Marketing_PhotoVehiclePreference__InsertUser] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Marketing_PhotoVehiclePreference__UpdateUser] FOREIGN KEY ([UpdateUser]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Marketing].[SnippetSourcePreference]'
GO
ALTER TABLE [Marketing].[SnippetSourcePreference] ADD
CONSTRAINT [FK_Marketing_SnippetSourcePreference__InsertMember] FOREIGN KEY ([InsertUserId]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Marketing_SnippetSourcePreference__UpdateMember] FOREIGN KEY ([UpdateUserId]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Marketing].[ValueAnalyzerVehiclePreference]'
GO
ALTER TABLE [Marketing].[ValueAnalyzerVehiclePreference] ADD
CONSTRAINT [FK_Marketing_ValueAnalyzerVehiclePreference__InsertUser] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Marketing_ValueAnalyzerVehiclePreference__UpdateUser] FOREIGN KEY ([UpdateUser]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Marketing].[VehicleEquipmentProvider]'
GO
ALTER TABLE [Marketing].[VehicleEquipmentProvider] ADD
CONSTRAINT [FK_Marketing_VehicleEquipmentProvider__InsertUser] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Marketing_VehicleEquipmentProvider__UpdateUser] FOREIGN KEY ([UpdateUser]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Marketing_VehicleEquipmentProvider__EquipmentProvider] FOREIGN KEY ([EquipmentProviderID]) REFERENCES [Marketing].[EquipmentProvider] ([EquipmentProviderID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[MemberRole]'
GO
ALTER TABLE [dbo].[MemberRole] ADD
CONSTRAINT [FK_MemberRole_Member] FOREIGN KEY ([MemberID]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_MemberRole_Role] FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Role] ([RoleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Purchasing].[List]'
GO
ALTER TABLE [Purchasing].[List] ADD
CONSTRAINT [FK_Purchasing_List__InsertUser] FOREIGN KEY ([InsertUser]) REFERENCES [dbo].[Member] ([MemberID]),
CONSTRAINT [FK_Purchasing_List__UpdateUser] FOREIGN KEY ([UpdateUser]) REFERENCES [dbo].[Member] ([MemberID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[NADARegionAreas]'
GO
ALTER TABLE [dbo].[NADARegionAreas] ADD
CONSTRAINT [FK_NADARegionAreas_NADARegions] FOREIGN KEY ([NADARegionCode]) REFERENCES [dbo].[NADARegions] ([NADARegionCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[PersonPosition]'
GO
ALTER TABLE [dbo].[PersonPosition] ADD
CONSTRAINT [FK_PersonPosition_Person] FOREIGN KEY ([PersonID]) REFERENCES [dbo].[Person] ([PersonID]),
CONSTRAINT [FK_PersonPosition_Position] FOREIGN KEY ([PositionID]) REFERENCES [dbo].[Position] ([PositionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[PhotoProvider_ThirdPartyEntity]'
GO
ALTER TABLE [dbo].[PhotoProvider_ThirdPartyEntity] ADD
CONSTRAINT [FK_PhotoProvider_ThirdPartyEntity__ThirdPartyEntity] FOREIGN KEY ([PhotoProviderID]) REFERENCES [dbo].[ThirdPartyEntity] ([ThirdPartyEntityID]),
CONSTRAINT [FK_PhotoProvider_ThirdPartyEntity__PhotoStorage] FOREIGN KEY ([PhotoStorageCode]) REFERENCES [dbo].[PhotoStorage] ([PhotoStorageCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Photos]'
GO
ALTER TABLE [dbo].[Photos] ADD
CONSTRAINT [FK_Photos__PhotoProvider_ThirdPartyEntity] FOREIGN KEY ([PhotoProviderID]) REFERENCES [dbo].[PhotoProvider_ThirdPartyEntity] ([PhotoProviderID]),
CONSTRAINT [FK_Photos__PhotoTypes] FOREIGN KEY ([PhotoTypeID]) REFERENCES [dbo].[PhotoTypes] ([PhotoTypeID]),
CONSTRAINT [FK_Photos__PhotoStatus] FOREIGN KEY ([PhotoStatusCode]) REFERENCES [dbo].[PhotoStatus] ([PhotoStatusCode])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[PING_Code]'
GO
ALTER TABLE [dbo].[PING_Code] ADD
CONSTRAINT [FK_PING_Code__ParentID] FOREIGN KEY ([parentId]) REFERENCES [dbo].[PING_Code] ([id]),
CONSTRAINT [FK_PING_Code__ProviderID] FOREIGN KEY ([providerId]) REFERENCES [dbo].[PING_Provider] ([id])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[PrintAdvertiser_ThirdPartyEntity]'
GO
ALTER TABLE [dbo].[PrintAdvertiser_ThirdPartyEntity] ADD
CONSTRAINT [FK_PrintAdvertiser_ThirdPartyEntity_PrintAdvertiserID] FOREIGN KEY ([PrintAdvertiserID]) REFERENCES [dbo].[ThirdPartyEntity] ([ThirdPartyEntityID]),
CONSTRAINT [FK_PrintAdvertiser_ThirdPartyEntity_VehicleOptionThirdPartyID] FOREIGN KEY ([VehicleOptionThirdPartyID]) REFERENCES [dbo].[ThirdParties] ([ThirdPartyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[PrintAdvertiserTextOptionDefault]'
GO
ALTER TABLE [dbo].[PrintAdvertiserTextOptionDefault] ADD
CONSTRAINT [FK_PrintAdvertiserTextOptionDefault_ThirdPartyEntityID] FOREIGN KEY ([PrintAdvertiserID]) REFERENCES [dbo].[PrintAdvertiser_ThirdPartyEntity] ([PrintAdvertiserID]),
CONSTRAINT [FK_PrintAdvertiserTextOptionDefault_PrintAdvertiserTextOptionID] FOREIGN KEY ([PrintAdvertiserTextOptionID]) REFERENCES [dbo].[PrintAdvertiserTextOption] ([PrintAdvertiserTextOptionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[PurgeLog]'
GO
ALTER TABLE [dbo].[PurgeLog] ADD
CONSTRAINT [FK_PurgeLog__PurgeManagement] FOREIGN KEY ([Subject]) REFERENCES [dbo].[PurgeManagement] ([Subject])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[SearchCandidateOption]'
GO
ALTER TABLE [dbo].[SearchCandidateOption] ADD
CONSTRAINT [FK_SearchCandidateOption_SearchCandidate] FOREIGN KEY ([SearchCandidateID]) REFERENCES [dbo].[SearchCandidate] ([SearchCandidateID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[SearchCandidate]'
GO
ALTER TABLE [dbo].[SearchCandidate] ADD
CONSTRAINT [FK_SearchCandidate_SearchCandidateList] FOREIGN KEY ([SearchCandidateListID]) REFERENCES [dbo].[SearchCandidateList] ([SearchCandidateListID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[SearchCandidateOptionAnnotation]'
GO
ALTER TABLE [dbo].[SearchCandidateOptionAnnotation] ADD
CONSTRAINT [FK_SearchCandidateOptionAnnotation_SearchCandidateOption] FOREIGN KEY ([SearchCandidateOptionID]) REFERENCES [dbo].[SearchCandidateOption] ([SearchCandidateOptionID]),
CONSTRAINT [FK_SearchCandidateOptionAnnotation_SearchCandidateOptionAnnotationType] FOREIGN KEY ([SearchCandidateOptionAnnotationTypeID]) REFERENCES [dbo].[SearchCandidateOptionAnnotationType] ([SearchCandidateOptionAnnotationTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[SoftwareSystemComponent]'
GO
ALTER TABLE [dbo].[SoftwareSystemComponent] ADD
CONSTRAINT [FK_SoftwareSystemComponent_SoftwareSystemComponent] FOREIGN KEY ([ParentID]) REFERENCES [dbo].[SoftwareSystemComponent] ([SoftwareSystemComponentID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Staging_BookoutOptions]'
GO
ALTER TABLE [dbo].[Staging_BookoutOptions] ADD
CONSTRAINT [FK_StagingBookoutOptions_StagingBookouts] FOREIGN KEY ([Staging_BookoutID]) REFERENCES [dbo].[Staging_Bookouts] ([Staging_BookoutID]),
CONSTRAINT [FK_StagingBookoutOptions_ThirdPartyCategory] FOREIGN KEY ([ThirdPartyCategoryID]) REFERENCES [dbo].[ThirdPartyCategories] ([ThirdPartyCategoryID]),
CONSTRAINT [FK_StagingBookoutOptions_ThirdPartyOptionType] FOREIGN KEY ([ThirdPartyOptionTypeID]) REFERENCES [dbo].[ThirdPartyOptionTypes] ([ThirdPartyOptionTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Staging_BookoutValues]'
GO
ALTER TABLE [dbo].[Staging_BookoutValues] ADD
CONSTRAINT [FK_StagingBookoutValues_StagingBookouts] FOREIGN KEY ([Staging_BookoutID]) REFERENCES [dbo].[Staging_Bookouts] ([Staging_BookoutID]),
CONSTRAINT [FK_StagingBookoutValue_ThirdPartyCategory] FOREIGN KEY ([ThirdPartyCategoryID]) REFERENCES [dbo].[ThirdPartyCategories] ([ThirdPartyCategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Subscriptions]'
GO
ALTER TABLE [dbo].[Subscriptions] ADD
CONSTRAINT [FK_Subscriptions_SubscriberTypes] FOREIGN KEY ([SubscriberTypeID]) REFERENCES [dbo].[SubscriberTypes] ([SubscriberTypeID]),
CONSTRAINT [FK_Subscriptions_SubscriptionDeliveryTypes] FOREIGN KEY ([SubscriptionDeliveryTypeID]) REFERENCES [dbo].[SubscriptionDeliveryTypes] ([SubscriptionDeliveryTypeID]),
CONSTRAINT [FK_Subscriptions_SubscriptionFrequencyDetail] FOREIGN KEY ([SubscriptionFrequencyDetailID]) REFERENCES [dbo].[SubscriptionFrequencyDetail] ([SubscriptionFrequencyDetailID]),
CONSTRAINT [FK_Subscriptions_SubscriptionTypes] FOREIGN KEY ([SubscriptionTypeID]) REFERENCES [dbo].[SubscriptionTypes] ([SubscriptionTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[SubscriptionTypeSubscriptionDeliveryType]'
GO
ALTER TABLE [dbo].[SubscriptionTypeSubscriptionDeliveryType] ADD
CONSTRAINT [FK_SubscriptionTypeSubscriptionDeliveryType__SubscriptionDeliveryTypeID] FOREIGN KEY ([SubscriptionDeliveryTypeID]) REFERENCES [dbo].[SubscriptionDeliveryTypes] ([SubscriptionDeliveryTypeID]),
CONSTRAINT [FK_SubscriptionTypeSubscriptionDeliveryType__SubscriptionTypeID] FOREIGN KEY ([SubscriptionTypeID]) REFERENCES [dbo].[SubscriptionTypes] ([SubscriptionTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[SubscriptionFrequencyDetail]'
GO
ALTER TABLE [dbo].[SubscriptionFrequencyDetail] ADD
CONSTRAINT [FK_SubscriptionFrequencyDetail_SubscriptionFrequency] FOREIGN KEY ([SubscriptionFrequencyID]) REFERENCES [dbo].[SubscriptionFrequency] ([SubscriptionFrequencyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[SubscriptionTypeSubscriptionFrequencyDetail]'
GO
ALTER TABLE [dbo].[SubscriptionTypeSubscriptionFrequencyDetail] ADD
CONSTRAINT [FK_SubscriptionTypeSubscriptionFrequencyDetail__SubscriptionFrequencyDetailID] FOREIGN KEY ([SubscriptionFrequencyDetailID]) REFERENCES [dbo].[SubscriptionFrequencyDetail] ([SubscriptionFrequencyDetailID]),
CONSTRAINT [FK_SubscriptionTypeSubscriptionFrequencyDetail__SubscriptionTypeID] FOREIGN KEY ([SubscriptionTypeID]) REFERENCES [dbo].[SubscriptionTypes] ([SubscriptionTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[Target]'
GO
ALTER TABLE [dbo].[Target] ADD
CONSTRAINT [FK_Target__TargetType] FOREIGN KEY ([TargetTypeCD]) REFERENCES [dbo].[TargetType] ([TargetTypeCD])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[tbl_MemberATCAccessGroup]'
GO
ALTER TABLE [dbo].[tbl_MemberATCAccessGroup] ADD
CONSTRAINT [FK_MemberATCAccessGroup_MemberATCAccessGroupList] FOREIGN KEY ([MemberATCAccessGroupListID]) REFERENCES [dbo].[tbl_MemberATCAccessGroupList] ([MemberATCAccessGroupListID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Marketing].[EquipmentProvider]'
GO
ALTER TABLE [Marketing].[EquipmentProvider] ADD
CONSTRAINT [FK_Marketing_EquipmentProvider__ThirdParty] FOREIGN KEY ([ThirdPartyID]) REFERENCES [dbo].[ThirdParties] ([ThirdPartyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ThirdPartyCategories]'
GO
ALTER TABLE [dbo].[ThirdPartyCategories] ADD
CONSTRAINT [FK_ThirdPartyCategories_ThirdParties] FOREIGN KEY ([ThirdPartyID]) REFERENCES [dbo].[ThirdParties] ([ThirdPartyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ThirdPartyVehicles]'
GO
ALTER TABLE [dbo].[ThirdPartyVehicles] ADD
CONSTRAINT [FK_VehicleThirdPartyVehicles_ThirdParties] FOREIGN KEY ([ThirdPartyID]) REFERENCES [dbo].[ThirdParties] ([ThirdPartyID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ThirdPartySubCategory]'
GO
ALTER TABLE [dbo].[ThirdPartySubCategory] ADD
CONSTRAINT [FK_ThirdPartySubCategory_ThirdPartyCategories] FOREIGN KEY ([ThirdPartyCategoryID]) REFERENCES [dbo].[ThirdPartyCategories] ([ThirdPartyCategoryID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ThirdPartyVehicleOptions]'
GO
ALTER TABLE [dbo].[ThirdPartyVehicleOptions] ADD
CONSTRAINT [FK_ThirdPartyVehicleOptions_ThirdPartyOptions] FOREIGN KEY ([ThirdPartyOptionID]) REFERENCES [dbo].[ThirdPartyOptions] ([ThirdPartyOptionID]),
CONSTRAINT [FK_VehicleThirdPartyVehicleOptions_VehicleThirdPartyVehicles] FOREIGN KEY ([ThirdPartyVehicleID]) REFERENCES [dbo].[ThirdPartyVehicles] ([ThirdPartyVehicleID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ThirdPartyOptions]'
GO
ALTER TABLE [dbo].[ThirdPartyOptions] ADD
CONSTRAINT [FK_ThirdPartyOptions_ThirdPartyOptionTypes] FOREIGN KEY ([ThirdPartyOptionTypeID]) REFERENCES [dbo].[ThirdPartyOptionTypes] ([ThirdPartyOptionTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[VehicleBookoutAdditionalState]'
GO
ALTER TABLE [dbo].[VehicleBookoutAdditionalState] ADD
CONSTRAINT [FK_VehicleBookoutAdditionalState_ThirdPartySubCategory] FOREIGN KEY ([ThirdPartySubCategoryID]) REFERENCES [dbo].[ThirdPartySubCategory] ([ThirdPartySubCategoryID]),
CONSTRAINT [FK_VehicleBookoutAdditionalState_VehicleBookoutState] FOREIGN KEY ([VehicleBookoutStateID]) REFERENCES [dbo].[VehicleBookoutState] ([VehicleBookoutStateID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[ThirdPartyVehicleOptionValues]'
GO
ALTER TABLE [dbo].[ThirdPartyVehicleOptionValues] ADD
CONSTRAINT [FK_VehicleThirdPartyVehicleOptionValues_VehicleThirdPartyVehicleOptions] FOREIGN KEY ([ThirdPartyVehicleOptionID]) REFERENCES [dbo].[ThirdPartyVehicleOptions] ([ThirdPartyVehicleOptionID]),
CONSTRAINT [FK_VehicleThirdPartyVehicleOptionValues_ThirdPartyVehicleOptionTypes] FOREIGN KEY ([ThirdPartyOptionValueTypeID]) REFERENCES [dbo].[ThirdPartyVehicleOptionValueTypes] ([ThirdPartyOptionValueTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[VehicleBookoutStateThirdPartyVehicles]'
GO
ALTER TABLE [dbo].[VehicleBookoutStateThirdPartyVehicles] ADD
CONSTRAINT [FK_VehicleBookoutStateThirdPartyVehicles_ThirdPartyVehicles] FOREIGN KEY ([ThirdPartyVehicleID]) REFERENCES [dbo].[ThirdPartyVehicles] ([ThirdPartyVehicleID]),
CONSTRAINT [FK_VehicleBookoutStateThirdPartyVehicles_VehicleBookoutState] FOREIGN KEY ([VehicleBookoutStateID]) REFERENCES [dbo].[VehicleBookoutState] ([VehicleBookoutStateID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Distribution].[Advertisement_Content]'
GO
ALTER TABLE [Distribution].[Advertisement_Content] ADD
CONSTRAINT [FK_Distribution_Advertisement_Content__Message] FOREIGN KEY ([MessageID]) REFERENCES [Distribution].[Message] ([MessageID]),
CONSTRAINT [FK_Distribution_Advertisement_Content__Content] FOREIGN KEY ([ContentID]) REFERENCES [Distribution].[Content] ([ContentID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Distribution].[AutoTrader_Error]'
GO
ALTER TABLE [Distribution].[AutoTrader_Error] ADD
CONSTRAINT [FK_Distribution_AutoTraderError__SubmissionError] FOREIGN KEY ([ErrorID]) REFERENCES [Distribution].[SubmissionError] ([ErrorID]),
CONSTRAINT [FK_Distribution_AutoTraderError__AutoTrader_ErrorType] FOREIGN KEY ([ErrorTypeID]) REFERENCES [Distribution].[AutoTrader_ErrorType] ([ErrorTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Distribution].[AutoUplink_Error]'
GO
ALTER TABLE [Distribution].[AutoUplink_Error] ADD
CONSTRAINT [FK_Distribution_AutoUplink_Error__SubmissionError] FOREIGN KEY ([ErrorID]) REFERENCES [Distribution].[SubmissionError] ([ErrorID]),
CONSTRAINT [FK_Distribution_AutoUplink_Error__AutoUplink_ErrorType] FOREIGN KEY ([ErrorTypeID]) REFERENCES [Distribution].[AutoUplink_ErrorType] ([ErrorTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Distribution].[ContentText]'
GO
ALTER TABLE [Distribution].[ContentText] ADD
CONSTRAINT [FK_Distribution_ContextText__Content] FOREIGN KEY ([ContentID]) REFERENCES [Distribution].[Content] ([ContentID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Distribution].[Content]'
GO
ALTER TABLE [Distribution].[Content] ADD
CONSTRAINT [FK_Distribution_Content__ContentType] FOREIGN KEY ([ContentTypeID]) REFERENCES [Distribution].[ContentType] ([ContentTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Distribution].[Exception]'
GO
ALTER TABLE [Distribution].[Exception] ADD
CONSTRAINT [FK_Distribution_Exception__Publication] FOREIGN KEY ([PublicationID]) REFERENCES [Distribution].[Publication] ([PublicationID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Distribution].[Price]'
GO
ALTER TABLE [Distribution].[Price] ADD
CONSTRAINT [FK_Distribution_Price__Message] FOREIGN KEY ([MessageID]) REFERENCES [Distribution].[Message] ([MessageID]),
CONSTRAINT [FK_Distribution_Price__PriceType] FOREIGN KEY ([PriceTypeID]) REFERENCES [Distribution].[PriceType] ([PriceTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Distribution].[VehicleInformation]'
GO
ALTER TABLE [Distribution].[VehicleInformation] ADD
CONSTRAINT [FK_Distribution_VehicleInformation__Message] FOREIGN KEY ([MessageID]) REFERENCES [Distribution].[Message] ([MessageID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Distribution].[ProviderMessageType]'
GO
ALTER TABLE [Distribution].[ProviderMessageType] ADD
CONSTRAINT [FK_Distribution_ProviderMessageType__Provider] FOREIGN KEY ([ProviderID]) REFERENCES [Distribution].[Provider] ([ProviderID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Distribution].[Submission]'
GO
ALTER TABLE [Distribution].[Submission] ADD
CONSTRAINT [FK_Distribution_Submission__Provider] FOREIGN KEY ([ProviderID]) REFERENCES [Distribution].[Provider] ([ProviderID]),
CONSTRAINT [FK_Distribution_Submission__Publication] FOREIGN KEY ([PublicationID]) REFERENCES [Distribution].[Publication] ([PublicationID]),
CONSTRAINT [FK_Distribution_Submission__SubmissionStatus] FOREIGN KEY ([SubmissionStatusID]) REFERENCES [Distribution].[SubmissionStatus] ([SubmissionStatusID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Distribution].[SubmissionError]'
GO
ALTER TABLE [Distribution].[SubmissionError] ADD
CONSTRAINT [FK_Distribution_SubmissionError__Submission] FOREIGN KEY ([SubmissionID]) REFERENCES [Distribution].[Submission] ([SubmissionID]),
CONSTRAINT [FK_Distribution_SubmissionError__SubmissionErrorType] FOREIGN KEY ([ErrorTypeID]) REFERENCES [Distribution].[SubmissionErrorType] ([ErrorTypeID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Distribution].[SubmissionSuccess]'
GO
ALTER TABLE [Distribution].[SubmissionSuccess] ADD
CONSTRAINT [FK_Distribution_SubmissionSuccess__Submission] FOREIGN KEY ([SubmissionID]) REFERENCES [Distribution].[Submission] ([SubmissionID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Marketing].[ConsumerHighlight]'
GO
ALTER TABLE [Marketing].[ConsumerHighlight] ADD
CONSTRAINT [FK_Marketing_ConsumerHighlight__ConsumerHighlightVehiclePreference] FOREIGN KEY ([ConsumerHighlightVehiclePreferenceId]) REFERENCES [Marketing].[ConsumerHighlightVehiclePreference] ([ConsumerHighlightVehiclePreferenceId]) ON DELETE CASCADE,
CONSTRAINT [FK_Marketing_ConsumerHighlight__HighlightProvider] FOREIGN KEY ([HighlightProviderId]) REFERENCES [Marketing].[HighlightProvider] ([HighlightProviderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Marketing].[HighlightProvider]'
GO
ALTER TABLE [Marketing].[HighlightProvider] ADD
CONSTRAINT [FK_Marketing_HighlightProvider__ConsumerHighlight] FOREIGN KEY ([ConsumerHighlightSectionId]) REFERENCES [Marketing].[ConsumerHighlightSection] ([ConsumerHighlightSectionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Marketing].[MarketAnalysisOwnerPriceProvider]'
GO
ALTER TABLE [Marketing].[MarketAnalysisOwnerPriceProvider] ADD
CONSTRAINT [FK_MarketAnalysisOwnerPriceProvider_MarketAnalysisPreference] FOREIGN KEY ([OwnerId]) REFERENCES [Marketing].[MarketAnalysisPreference] ([OwnerId]),
CONSTRAINT [FK_MarketAnalysisOwnerPriceProvider_MarketAnalysisPriceProvider] FOREIGN KEY ([PriceProviderId]) REFERENCES [Marketing].[MarketAnalysisPriceProvider] ([PriceProviderId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Marketing].[MarketAnalysisVehiclePreferencePriceProvider]'
GO
ALTER TABLE [Marketing].[MarketAnalysisVehiclePreferencePriceProvider] ADD
CONSTRAINT [FK_Marketing_MarketAnalysisVehiclePreferencePriceProvider__MarketAnalysisPriceProvider] FOREIGN KEY ([MarketAnalysisPriceProviderId]) REFERENCES [Marketing].[MarketAnalysisPriceProvider] ([PriceProviderId]) ON DELETE CASCADE,
CONSTRAINT [FK_Marketing_MarketAnalysisVehiclePreferencePriceProvider__MarketAnalysisVehiclePreference] FOREIGN KEY ([MarketAnalysisVehiclePreferenceId]) REFERENCES [Marketing].[MarketAnalysisVehiclePreference] ([MarketAnalysisVehiclePreferenceId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [Purchasing].[ListItem]'
GO
ALTER TABLE [Purchasing].[ListItem] ADD
CONSTRAINT [FK_Purchasing_ListItem__ListID] FOREIGN KEY ([ListID]) REFERENCES [Purchasing].[List] ([ListID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[TR_D_Appraisals] on [dbo].[Appraisals]'
GO

----------------------------------------------------------------------------------------------------------------
--	THE APPRAISAL_F's PK IS ESSENTIALLY BusinessUnitID + VehicleID
--	
--	SOMETIMES WE DELETE AN APPRAISAL AND THEN CREATE ANOTHER WITH THE SAME
--	BUSINESS UNIT-VEHICLE; WE NEED TO UPDATE THE APPRAISAL FACT TABLE IN FLDW.
----------------------------------------------------------------------------------------------------------------

CREATE TRIGGER dbo.TR_D_Appraisals ON dbo.Appraisals
AFTER DELETE NOT FOR REPLICATION
AS

----------------------------------------------------------------------------------------------------------------
--	DELETE Appraisal_F
----------------------------------------------------------------------------------------------------------------

DELETE	F
FROM	[FLDW]..Appraisal_F F
	JOIN deleted D ON F.AppraisalID = D.AppraisalID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[TR_UI_AppraisalValues] on [dbo].[AppraisalValues]'
GO

----------------------------------------------------------------------------------------------------------------
--	THE APPRAISAL_F's PK IS ESSENTIALLY BusinessUnitID + VehicleID
--	THIS IS MORE LIKE A Vehicle_F TABLE, STORING THE LATEST APPRAISAL INFORMATION FOR THE VEHICLE
--	MIGHT NEED TO BE RENAMED FOR CLARITY.
----------------------------------------------------------------------------------------------------------------

CREATE TRIGGER dbo.TR_UI_AppraisalValues ON dbo.AppraisalValues
AFTER INSERT, UPDATE NOT FOR REPLICATION
AS

----------------------------------------------------------------------------------------------------------------
--	UPDATE Appraisal_F
----------------------------------------------------------------------------------------------------------------

UPDATE	F
SET	F.Value			= AV.Value,
	F.AppraisalValueID 	= AV.AppraisalValueID,
	F.AppraisalID		= A.AppraisalID

FROM	inserted AV
	JOIN Appraisals A ON AV.AppraisalID = A.AppraisalID
	JOIN [FLDW]..Appraisal_F F ON A.BusinessUnitID = F.BusinessUnitID AND A.VehicleID = F.VehicleID

----------------------------------------------------------------------------------------------------------------
--	INSERT Appraisal_F
----------------------------------------------------------------------------------------------------------------

IF @@ROWCOUNT = 0

	INSERT
	INTO	[FLDW]..Appraisal_F  (BusinessUnitID, AppraisalID, VehicleID, AppraisalValueID, Value, AppraisalTypeID, DateCreated, DateModified, Mileage)
	
	SELECT	A.BusinessUnitID, A.AppraisalID, A.VehicleID, AV.AppraisalValueID, AV.Value, A.AppraisalTypeID, A.DateCreated, A.DateModified, A.Mileage
	
	FROM	inserted AV
		JOIN Appraisals A ON AV.AppraisalID = A.AppraisalID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[TR_U_Bookouts] on [dbo].[Bookouts]'
GO

CREATE TRIGGER dbo.TR_U_Bookouts ON dbo.Bookouts
AFTER UPDATE NOT FOR REPLICATION
AS

IF UPDATE(BookoutStatusID) 


	UPDATE	F
	SET	IsAccurate 	= COALESCE(BS.IsAccurate, B.IsAccurate),
		BookoutStatusID	= B.BookoutStatusID	
	FROM	inserted I
		JOIN Bookouts B ON I.BookoutID = B.BookoutID
		JOIN BookoutStatus BS ON B.BookoutStatusID = BS.BookoutStatusID
		JOIN [FLDW]..InventoryBookout_F F ON B.BookoutID = F.BookoutID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[TR_UI_BookValues] on [dbo].[BookoutValues]'
GO

CREATE TRIGGER dbo.TR_UI_BookValues ON dbo.BookoutValues
AFTER INSERT, UPDATE NOT FOR REPLICATION
AS

----------------------------------------------------------------------------------------------------------------
--	UPDATE InventoryBookout_F
----------------------------------------------------------------------------------------------------------------

UPDATE	F
SET	BookoutID			= B.BookoutID,
	BookoutValueCreatedTimeID 	= T.TimeID,
	BookValue   			= BV.Value, 
	IsValid 			= BTPC.IsValid, 
	IsAccurate 			= B.IsAccurate,
	BookoutStatusID			= B.BookoutStatusID	

FROM	inserted BV
 	JOIN BookoutThirdPartyCategories BTPC ON BV.BookoutThirdPartyCategoryID = BTPC.BookoutThirdPartyCategoryID
 	JOIN ThirdPartyCategories TPC ON BTPC.ThirdPartyCategoryID = TPC.ThirdPartyCategoryID
 	JOIN Bookouts B ON BTPC.BookoutID = B.BookoutID
	JOIN InventoryBookouts IB ON B.BookoutID = IB.BookoutID
	JOIN [FLDW]..Time_D T ON T.TypeCD = 1 AND cast(convert(varchar(10),BV.DateCreated,101) as datetime) = T.BeginDate
	JOIN [FLDW]..InventoryBookout_F F ON IB.InventoryID = F.InventoryID AND TPC.ThirdPartyCategoryID = F.ThirdPartyCategoryID AND BV.BookoutValueTypeID = F.BookoutValueTypeID
	
WHERE	BV.BookoutValueTypeID = 2


----------------------------------------------------------------------------------------------------------------
--	UPDATE AppraisalBookout_F 
----------------------------------------------------------------------------------------------------------------

UPDATE	F
SET	BookoutID			= B.BookoutID,
	BookoutValueCreatedTimeID 	= T.TimeID,
	Value   			= BV.Value, 
	IsValid 			= BTPC.IsValid, 
	IsAccurate 			= B.IsAccurate

FROM	inserted BV
 	JOIN BookoutThirdPartyCategories BTPC ON BV.BookoutThirdPartyCategoryID = BTPC.BookoutThirdPartyCategoryID
 	JOIN ThirdPartyCategories TPC ON BTPC.ThirdPartyCategoryID = TPC.ThirdPartyCategoryID
 	JOIN Bookouts B ON BTPC.BookoutID = B.BookoutID
	JOIN AppraisalBookouts AB ON B.BookoutID = AB.BookoutID
	JOIN Appraisals A ON AB.AppraisalID = A.AppraisalID
	JOIN [FLDW]..Time_D T ON T.TypeCD = 1 AND cast(convert(varchar(10),BV.DateCreated,101) as datetime) = T.BeginDate
	JOIN [FLDW]..AppraisalBookout_F F ON A.BusinessUnitID = F.BusinessUnitID AND AB.AppraisalID = F.AppraisalID AND TPC.ThirdPartyCategoryID = F.ThirdPartyCategoryID AND BV.BookoutValueTypeID = F.BookoutValueTypeID

WHERE	BV.BookoutValueTypeID = 2

----------------------------------------------------------------------------------------------------------------
--	INSERT AppraisalBookout_F IF THE UPDATE DIDN'T FIND ANY ROWS
----------------------------------------------------------------------------------------------------------------

IF @@ROWCOUNT = 0

	INSERT
	INTO	[FLDW]..AppraisalBookout_F (BusinessUnitID, AppraisalID, BookoutID, ThirdPartyCategoryID, BookoutValueTypeID, BookoutValueCreatedTimeID, Value, IsValid, IsAccurate)
	
	SELECT	A.BusinessUnitID, A.AppraisalID, AB.BookoutID, TPC.ThirdPartyCategoryID, BV.BookoutValueTypeID, T.TimeID, BV.Value, BTPC.IsValid, B.IsAccurate
	
	FROM	inserted BV
	 	JOIN BookoutThirdPartyCategories BTPC ON BV.BookoutThirdPartyCategoryID = BTPC.BookoutThirdPartyCategoryID
	 	JOIN ThirdPartyCategories TPC ON BTPC.ThirdPartyCategoryID = TPC.ThirdPartyCategoryID
	 	JOIN Bookouts B ON BTPC.BookoutID = B.BookoutID
		JOIN AppraisalBookouts AB ON B.BookoutID = AB.BookoutID
		JOIN Appraisals A ON AB.AppraisalID = A.AppraisalID
		JOIN [FLDW]..Time_D T ON T.TypeCD = 1 AND cast(convert(varchar(10),BV.DateCreated,101) as datetime) = T.BeginDate
	
	WHERE	BV.BookoutValueTypeID = 2
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[TR_NewDealerRisk] on [dbo].[DealerPreference]'
GO
CREATE TRIGGER [TR_NewDealerRisk] ON dbo.DealerPreference 
FOR INSERT
AS
INSERT INTO [dbo].[DealerRisk] (BusinessUnitId) SELECT i.BusinessUnitId FROM inserted i
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[TR_U_Inventory] on [dbo].[Inventory]'
GO

CREATE TRIGGER dbo.TR_U_Inventory ON dbo.Inventory
AFTER UPDATE NOT FOR REPLICATION
AS
IF UPDATE(ListPrice) OR UPDATE(MileageReceived) OR UPDATE(CurrentVehicleLight) 
  OR UPDATE(PlanReminderDate) OR UPDATE(EdmundsTMV) 
  OR UPDATE(TransferPrice) OR UPDATE(TransferForRetailOnly) OR UPDATE(TransferForRetailOnlyEnabled)
  OR UPDATE(Certified)
	UPDATE	A
	SET	A.ListPrice 		= T.ListPrice,
		A.MileageReceived 	= T.MileageReceived,
		A.CurrentVehicleLight 	= T.CurrentVehicleLight,
		A.PlanReminderDate 	= T.PlanReminderDate,
		A.EdmundsTMV 		= T.EdmundsTMV,
		A.LotPrice		= T.LotPrice,
		A.TransferPrice 	= T.TransferPrice,
		A.TransferForRetailOnly = T.TransferForRetailOnly,
		A.Certified		= T.Certified
	FROM	inserted T
		JOIN [FLDW]..InventoryActive A ON T.InventoryID = A.InventoryID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[TR_UID_InventoryBucketRanges] on [dbo].[InventoryBucketRanges]'
GO

CREATE TRIGGER dbo.TR_UID_InventoryBucketRanges ON InventoryBucketRanges
FOR INSERT, UPDATE, DELETE
AS
	
IF SUSER_SNAME() = 'firstlook' 
   AND (EXISTS(SELECT 1 FROM deleted WHERE BusinessUnitID = 100150 AND InventoryBucketID <> 7)
	OR EXISTS(SELECT 1 FROM inserted WHERE BusinessUnitID = 100150 AND InventoryBucketID <> 7)) BEGIN

	RAISERROR('The user ''firstlook'' may not modify default bucket ranges', 11, 1)
	ROLLBACK TRANSACTION
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[RepriceExportStatusModifiedUpdate] on [dbo].[RepriceExport]'
GO

	CREATE TRIGGER RepriceExportStatusModifiedUpdate
	ON RepriceExport
	FOR UPDATE
	AS IF UPDATE(RepriceExportStatusID)
		UPDATE RE
		SET StatusModifiedDate = getdate()
		FROM inserted i
		JOIN RepriceExport RE on i.RepriceExportID = RE.RepriceExportID 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [dbo].[TR_U_Vehicle] on [dbo].[tbl_Vehicle]'
GO

CREATE TRIGGER dbo.TR_U_Vehicle ON dbo.tbl_Vehicle
AFTER UPDATE NOT FOR REPLICATION
AS
IF UPDATE(BaseColor) OR UPDATE(VehicleTrim) --OR UPDATE(VehicleBodyStyleID)
	UPDATE	A
	SET	A.BaseColor = T.BaseColor,
		A.VehicleTrim = ISNULL(NULLIF(T.VehicleTrim,''),'UNKNOWN')
--		A.VehicleBodyStyleID = T.VehicleBodyStyleID
	FROM	inserted T
		JOIN [FLDW].dbo.Vehicle A ON T.VehicleID = A.VehicleID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [Distribution].[TR_D_Account] on [Distribution].[Account]'
GO

-- Trigger on deletes.
create trigger [Distribution].[TR_D_Account] on [Distribution].[Account]
after delete
as
begin
    set nocount on;
    
    declare @DealerID int, @ProviderID int, @ValidFrom datetime
    
    select @DealerID = d.DealerID, @ProviderID = d.ProviderID
    from deleted d
    
    set @ValidFrom = getdate()
    
    -- Change the ValidUntil date of the last entry for this dealer and provider.
    update [Distribution].[Account_Audit]
    set [ValidUntil] = @ValidFrom
    where [DealerID] = @DealerID
    and [ProviderID] = @ProviderID
    and [ValidUntil] = '01/01/2040'
    
    -- Insert into the audit table.
    insert into [Distribution].[Account_Audit]  
	(DealerID, ProviderID, UserName, Password, DealershipCode, Active, ChangeUserID, ChangeDate, ValidFrom, ValidUntil, WasInsert, WasUpdate, WasDelete)    
    select DealerID, ProviderID, UserName, Password, DealershipCode, Active, UpdateUserID, UpdateDate, @ValidFrom, '01/01/2040', 0, 0, 1
    from deleted  
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [Distribution].[TR_I_Account] on [Distribution].[Account]'
GO

-- Trigger on inserts.
create trigger [Distribution].[TR_I_Account] on [Distribution].[Account]
for insert
as
begin
    set nocount on;
    
    declare @DealerID int, @ProviderID int, @ValidFrom datetime
    
    select @DealerID = i.DealerID, @ProviderID = i.ProviderID
    from inserted i
    
    set @ValidFrom = getdate()
    
    -- Change the ValidUntil date of the last entry for this dealer and provider.
    update [Distribution].[Account_Audit]
    set [ValidUntil] = @ValidFrom
    where [DealerID] = @DealerID
    and [ProviderID] = @ProviderID
    and [ValidUntil] = '01/01/2040'
    
    -- Insert into the audit table.
    insert into [Distribution].[Account_Audit] 
    (DealerID, ProviderID, UserName, Password, DealershipCode, Active, ChangeUserID, ChangeDate, ValidFrom, ValidUntil, WasInsert, WasUpdate, WasDelete)
    select DealerID, ProviderID, UserName, Password, DealershipCode, Active, InsertUserID, InsertDate, @ValidFrom, '01/01/2040', 1, 0, 0
    from inserted
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating trigger [Distribution].[TR_U_Account] on [Distribution].[Account]'
GO

-- Trigger on updates.
create trigger [Distribution].[TR_U_Account] on [Distribution].[Account]
after update
as
begin
    set nocount on;
    
    declare @DealerID int, @ProviderID int, @ValidFrom datetime
    
    select @DealerID = i.DealerID, @ProviderID = i.ProviderID
    from inserted i
    
    set @ValidFrom = getdate()
    
    -- Change the ValidUntil date of the last entry for this dealer and provider.
    update [Distribution].[Account_Audit]
    set [ValidUntil] = @ValidFrom
    where [DealerID] = @DealerID
    and [ProviderID] = @ProviderID
    and [ValidUntil] = '01/01/2040'
    
    -- Insert into the audit table.
    insert into [Distribution].[Account_Audit]  
    (DealerID, ProviderID, UserName, Password, DealershipCode, Active, ChangeUserID, ChangeDate, ValidFrom, ValidUntil, WasInsert, WasUpdate, WasDelete)    
    select DealerID, ProviderID, UserName, Password, DealershipCode, Active, UpdateUserID, UpdateDate, @ValidFrom, '01/01/2040', 0, 1, 0
    from inserted  
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating extended properties'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The provider specified to retrieve vehicle options to use in auto advertisment generation', 'SCHEMA', N'dbo', 'TABLE', N'DealerInternetAdvertisementBuilderOptionProvider', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_addextendedproperty N'MS_Description', N'Internet Advertisement preferences at a Dealer level', 'SCHEMA', N'dbo', 'TABLE', N'DealerPreference_InternetAdvertisementBuilder', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_addextendedproperty N'MS_Description', N'Specifies if automatic generation of an advertisement is on or off', 'SCHEMA', N'dbo', 'TABLE', N'DealerPreference_InternetAdvertisementBuilder', 'COLUMN', N'EnableBuilder'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_addextendedproperty N'MS_Description', N'Indicates which SalesChannel lead to the creation of the Dealership.', 'SCHEMA', N'dbo', 'TABLE', N'DealerSalesChannel', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_addextendedproperty N'MS_Description', N'Metadata describing available data to include in a system generated advertisement', 'SCHEMA', N'dbo', 'TABLE', N'InternetAdvertisementBuilderOptionProvider', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated and can be dropped.', 'SCHEMA', N'dbo', 'TABLE', N'InventoryBookoutFailures', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_addextendedproperty N'MS_Description', N'Once used for ATC auction list price manipulation, this table can probably be deprecated.', 'SCHEMA', N'dbo', 'TABLE', N'lu_AccessGroupActor', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_addextendedproperty N'MS_Description', N'Deprecated and can be dropped.', 'SCHEMA', N'dbo', 'TABLE', N'lu_AnalyticalEngineStatus', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_addextendedproperty N'Loader', N'IMT.dbo.PopulateMakeModelGroupingDescriptionTables', 'SCHEMA', N'dbo', 'TABLE', N'MakeModelGrouping', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_addextendedproperty N'MS_Description', N'Original table of makes/models for the Firstlook application suite. Loaded exclusively from the Firstlook VehicleCatalog with the stored procedure IMT.dbo.PopulateMakeModelGroupingDescriptionTables', 'SCHEMA', N'dbo', 'TABLE', N'MakeModelGrouping', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_addextendedproperty N'MS_Description', N'Channels that sell the FirstLook product.', 'SCHEMA', N'dbo', 'TABLE', N'SalesChannel', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO


