SET QUOTED_IDENTIFIER ON 
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetCompositeBasisPeriod]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetCompositeBasisPeriod]

GO
CREATE FUNCTION dbo.GetCompositeBasisPeriod 
--------------------------------------------------------------------------------
--
--	Returns the composite basis period for the passed business unit id 
--	AND base date.
--	
--	Note that the question of whether or not to include the base date as
--	the true end date is still open -- currently, a basis period of 7 days
--	computes a start date of (base date - 7 days) AND an end date of the
--	base date; this is a date range of eight days if the boundaries are 
--	inclusive (as in the standard SQL "between" operator.)
--
---Parameters-------------------------------------------------------------------
(
	@business_unit_id	as int = null,
	@base_date 		as datetime,    -- Base date for the basis period
	@basisPeriod_desc	as varchar(50) = 'StoreTargetInventory'
)
---History----------------------------------------------------------------------
--	
--	WGH	10/28/2004	Initial design/development
--		11/02/2004	Modified to answer above question: end date is
--				NOT inclusive 
--		01/14/2005	Added return of BU AND nullable BU parameter 		
--
--		02/04/2005	adjusted function to take in certain basis
--				periods as a parameter
--	BYF	09/07/2006	Expanded basisPeriod_desc to 50 AND updated 
--					default
--	PHK	08/28/2007	Refactored to move ToDate out of the SELECT 
--				statement
--	WGH	11/03/2008	Converted to inline table-valued function
--
------------------------------------------------------------------------------
RETURNS TABLE
AS
RETURN
(
	

	SELECT 	BP.BusinessUnitID, Forecast, DATEADD(dd, -TP.Days, DATEADD(DD, DATEDIFF(DD, 0, @base_date), 0)) BeginDate, DATEADD(dd,-1,DATEADD(DD, DATEDIFF(DD, 0, @base_date), 0)) EndDate
	FROM	vw_CIAEngineBasisPeriods BP
		JOIN tbl_CIACompositeTimePeriod CTP ON BP.CIACompositeTimePeriodId = CTP.CIACompositeTimePeriodId 
		JOIN lu_CIATimePeriod TP ON CTP.PriorTimePeriodId = TP.CIATimePeriodId
	WHERE	(@business_unit_id IS NULL OR BP.BusinessUnitID = @business_unit_id)
		AND BP.CIAEngineStageBasisPeriodDescription = @basisPeriod_desc
	UNION ALL
	SELECT 	BP.BusinessUnitID, Forecast, DATEADD(wk,-52,DATEADD(DD, DATEDIFF(DD, 0, @base_date), 0)) BeginDate,  DATEADD(wk,-52,DATEADD(dd, TP.Days, DATEADD(dd,-1,DATEADD(DD, DATEDIFF(DD, 0, @base_date), 0)))) EndDate
	FROM	vw_CIAEngineBasisPeriods BP
		JOIN tbl_CIACompositeTimePeriod CTP ON BP.CIACompositeTimePeriodId = CTP.CIACompositeTimePeriodId
		JOIN lu_CIATimePeriod TP ON CTP.ForecastTimePeriodId = TP.CIATimePeriodId
	WHERE	(@business_unit_id IS NULL OR BP.BusinessUnitID = @business_unit_id)
		AND BP.CIAEngineStageBasisPeriodDescription = @basisPeriod_desc
)


GO

