SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetDiscountRateTimeHorizonValue]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetDiscountRateTimeHorizonValue]
GO


CREATE FUNCTION dbo.GetDiscountRateTimeHorizonValue
--------------------------------------------------------------------------------
--
--	Returns DiscountRate OR TimeHorizon Value based on the passed parameters
--	
--	Use a common procedure since the calculation of both measures rely on 
--	computing the "PERCENTILE" value of the set.
--	Since the preference PERCENTILE will rarely, if ever, fall directly on
--	a member of the set's running PCT proportion of the set, we do a simple
--	linear interpolation between the selected values of the bounding items
--	in the set.
--
---Parmeters--------------------------------------------------------------------
(	
	@BusinessUnitID		INT,
	@Mode			TINYINT,	-- 1 = Discount Rate, 2 = Time 
						-- Horizon
	@BaseDate		DATETIME
)
---History----------------------------------------------------------------------
--	
--	WGH	12/19/2005	Initial design/development
--	
--------------------------------------------------------------------------------
RETURNS DECIMAL(12,5)
AS
BEGIN
	
	DECLARE @Percentile 	DECIMAL(12,5),
		@Rows		DECIMAL(12,5),
		@Return		DECIMAL(12,5)
	
	SELECT	@Percentile = CASE @Mode WHEN 1 THEN MarginPercentile ELSE DaysToSalePercentile END / 100.0
	FROM	DealerPreference
	WHERE	BusinessUnitID = @BusinessUnitID
	
	DECLARE	@PercentileRank TABLE (RankID INT IDENTITY(1,1), Value DECIMAL(12,5))
	
	INSERT
	INTO	@PercentileRank (Value)
	SELECT 	CASE @Mode WHEN 1 THEN Margin ELSE DaysToSale END
	FROM 	GetDiscountRateTimeHorizonSales(@BusinessUnitID,@BaseDate)
	ORDER
	BY	CASE @Mode WHEN 1 THEN Margin ELSE DaysToSale END
	
	SELECT	@Rows = @@ROWCOUNT - 1						-- SINCE WE START AT ZERO, DECREMENT THE COUNT
	
	SELECT	--PR1.RankID, @MarginPercentile, 
		--(PR1.RankID - 1) / @Rows, (PR2.RankID - 1) / @Rows, 
		@Return = PR1.Value + (PR2.Value - PR1.Value) * (@Percentile - (PR1.RankID - 1) / @Rows) / ((PR2.RankID - 1) / @Rows - (PR1.RankID - 1) / @Rows)	-- LINEAR INTERPOLATION
		--PR1.Value, PR2.Value
	FROM	@PercentileRank PR1
		JOIN @PercentileRank PR2 ON PR1.RankID = PR2.RankID - 1		-- JOIN TO THE NEXT UP IN THE ORDERED SET
	WHERE	@Rows > 0							-- IF ZERO, THIS ONLY ONE SALE; NOT ENOUGH TO CALCULATE THE VALUE
		AND @Percentile >= (PR1.RankID - 1) / @Rows  			-- FIND WHERE THE PERCENTILE PREFERENCE FALLS
		AND (@Percentile < (PR2.RankID - 1) / @Rows
			OR (@Percentile = 1.0 AND PR1.RankID = @Rows)		-- HANDLE END-BOUNDARY CASE 
			)

	RETURN CASE WHEN @Mode = 1 AND @Return < 0.0 THEN 0.0 ELSE @Return END
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

