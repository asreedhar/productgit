SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInventoryPhotoListForProvider]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetInventoryPhotoListForProvider]
GO

/********************************************************************************
**
**  Procedure: GetInventoryPhotoListForProvider
**  Description: Return a comma-delimited string of all the photo URLs
**  (if any) for the passed in InventoryID that are NOT sourced from the 
**  passed @PhotoProvider.  This is done to prevent self-loops back to the 
**  provider. 
**
**  Return values:  varchar(8000) comma delimited list (empty string if none)
**
**  Input parameters:   See below
**
*********************************************************************************
**  Version History
*********************************************************************************
**  Date:       Author:		Description:
**  ----------  --------	-------------------------------------------------
**  11/16/2007  WGH		Somewhat forked from predecessors	
**  09/23/2009	WGH		Updated provider exclusion filter
**  09/25/2009	WGH		Added photo "source" flag
**  12/12/2009  WGH             Added case to handle when PhotoProviderID is NULL
**				(Firstlook) for @SourceFlag = 1
**  03/31/2010  WGH		Updated Local URL to http instead of https
**                              when "Original URL" is set
**  07/22/2010  SVU		Added ORDER BY P.IsPrimaryPhoto DESC, P.PhotoID
**  01/24/2011  WGH		Changed order to P.IsPrimaryPhoto DESC, 
**				P.SequenceNumber, P.PhotoID
********************************************************************************/
CREATE FUNCTION dbo.GetInventoryPhotoListForProvider
(	
@InventoryID		INT,
@PhotoProviderID	INT,
@SourceFlag		BIT = 0	-- 0 = Processed URL, 1 = Original URL
)
RETURNS VARCHAR(8000)
BEGIN
	DECLARE @LocalURL VARCHAR(50)
	DECLARE @PhotoList varchar(8000)

	SET @LocalURL = CASE WHEN @SourceFlag = 1 THEN 'http' ELSE 'https' END + '://www.firstlook.biz/digitalimages/'		-- SHOULD GET FROM CONFIG TABLE

        SELECT	@PhotoList = COALESCE(@PhotoList + ',', '') 
                                  + CASE WHEN @SourceFlag = 0 AND P.PhotoStatusCode = 1 THEN @LocalURL + P.PhotoURL
                                         WHEN @SourceFlag = 0 AND P.PhotoStatusCode <> 1 THEN P.PhotoURL
                                         WHEN @SourceFlag = 1 AND P.PhotoProviderID IS NULL THEN @LocalURL + P.OriginalPhotoURL
                                         ELSE P.OriginalPhotoURL
                                    END
                                  
	FROM	dbo.InventoryPhotos IP
		INNER JOIN dbo.Inventory I ON IP.InventoryID = I.InventoryID
		INNER JOIN dbo.Photos P on IP.PhotoID = P.PhotoID
		
	WHERE	IP.InventoryID = @InventoryID
		AND P.PhotoStatusCode BETWEEN 1 and 2 -- Local storage / Remote storage
		AND (P.PhotoProviderID IS NULL 
			OR P.PhotoProviderID <> @PhotoProviderID
			)
	ORDER BY P.IsPrimaryPhoto DESC, P.SequenceNumber, P.PhotoID
			
        RETURN(@PhotoList)
END        

GO