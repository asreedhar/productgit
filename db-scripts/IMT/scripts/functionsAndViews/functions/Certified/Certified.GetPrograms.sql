IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[GetPrograms]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Certified].[GetPrograms]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Daniel Hillis
-- Create date: 2014-05-14
-- Notes:
-- Assumes it is passed a valid OwnerId and OwnerEntityId
-- =============================================


CREATE FUNCTION [Certified].[GetPrograms]
(	
	@Make varchar(50),
	@OwnerID int
)
RETURNS TABLE 
AS
RETURN 
(
	select 'MFR' as ProgramType, cp.CertifiedProgramID as ProgramId, cp.Name
	,case 
		when mfr.ManufacturerID = 6 -- GM
		then cast(1 as bit)
		else cast(0 as bit)
	end as RequiresCertifiedId
	,case 
		when mfr.ManufacturerID = 5 -- BMW
		then cast(1 as bit)
		else cast(0 as bit)
	end as ManfacturerReportsCertifications
	from imt.Certified.CertifiedProgram cp
		join imt.Certified.CertifiedProgram_Make cpm on cp.CertifiedProgramID = cpm.CertifiedProgramID
		join imt.Certified.OwnerCertifiedProgram ocp on cp.CertifiedProgramID = ocp.CertifiedProgramID
		join VehicleCatalog.Firstlook.Make m on cpm.MakeID = m.MakeID
		join VehicleCatalog.Chrome.Divisions d on m.ChromeDivisionID = d.DivisionID and d.CountryCode = 1
		join VehicleCatalog.Chrome.Manufacturers mfr on d.CountryCode = mfr.CountryCode AND d.ManufacturerID = mfr.ManufacturerID
	where m.Make = @Make
		and cp.Active = 1
		and ocp.OwnerId = @OwnerID

	union all

	select 'GRP', dp.CertifiedProgramID, dp.Name,cast(0 as bit), cast(0 as bit)
	from imt.Certified.DealerPrograms dp
	join Market.Pricing.Owner o on o.OwnerEntityID = dp.BusinessUnitID
	where o.OwnerID = @OwnerID
		and Active = 1
		and ProgramScope = 'Group'

	union all 

	select 'DLR', dp.CertifiedProgramID, dp.Name,cast(0 as bit), cast(0 as bit)
	from imt.Certified.DealerPrograms dp
	join Market.Pricing.Owner o on o.OwnerEntityID = dp.BusinessUnitID
	where o.OwnerID = @OwnerID
		and Active = 1
		and ProgramScope = 'Dealer'
		and not exists 
			( -- don't use dealer programs if group programs exist
				select * 
				from imt.Certified.DealerPrograms gp 
				where gp.BusinessUnitID = dp.BusinessUnitID 
					and gp.ProgramScope = 'Group' 
					and gp.Active = 1
			)		
)

GO


