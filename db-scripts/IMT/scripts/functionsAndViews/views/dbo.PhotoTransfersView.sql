USE [IMT]
GO
/****** Object:  View [dbo].[PhotoTransfersView]    Script Date: 10/30/2012 16:00:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PhotoTransfersView]') and OBJECTPROPERTY(id, N'IsView') = 1)
	drop view [dbo].[PhotoTransfersView]
GO

CREATE VIEW [dbo].[PhotoTransfersView]
AS

SELECT     t.srcInventoryId, t.dstInventoryId, veh.BaseColor, veh.OriginalYear, veh.OriginalMake, veh.OriginalModel, veh.Vin, srcBu.BusinessUnit AS 'src', src.StockNumber AS 'src_stock', src.InventoryActive AS 'src_active', 
           dstBu.BusinessUnit AS 'dst', dst.StockNumber AS 'dst_stock', dst.InventoryActive AS 'dst_active', dst.InventoryReceivedDate AS 'dst_received',
			queuedDt 'Queued On', status, description, statusDt 'Status Updated On'

FROM         dbo.PhotoTransfers AS t INNER JOIN
                      dbo.Inventory AS src ON src.InventoryID = t.SrcInventoryID INNER JOIN
                      dbo.BusinessUnit AS srcBu ON srcBu.BusinessUnitID = src.BusinessUnitID INNER JOIN
                      dbo.Inventory AS dst ON dst.InventoryID = t.DstInventoryID INNER JOIN
                      dbo.BusinessUnit AS dstBu ON dstBu.BusinessUnitID = dst.BusinessUnitID INNER JOIN
                      dbo.tbl_Vehicle AS veh ON veh.VehicleID = src.VehicleID INNER JOIN
						dbo.photoTransferStatus ts on ts.code = t.status

