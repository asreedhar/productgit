if exists (select * from dbo.sysobjects where id = object_id(N'Extract.Inventory#AULtec') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view Extract.Inventory#AULtec
GO

CREATE VIEW Extract.Inventory#AULtec
--------------------------------------------------------------------------------------------------------
--
--	Inventory (IMT) extract for the AULtec export process.  Used by the SSIS package 
--	Extract-FlatFile-AULTec.dtsx
--
---History----------------------------------------------------------------------------------------------
--
--	WGH	11/??/2009	Initial design/development (based on original
--				extract proc)
--		11/18/2009	Added EngineVolume parsed from VC.Engine
--		12/14/2009	Updated Engine to source from parsed VC.Engine
--		12/15/2009	Mapped MSRP --> LotPrice
--		12/22/2009	Updated selected vehicle attributes to pull from VC
--	SVU	12/31/2009	Use FLDW.dbo.InventoryActive & FLDW.dbo.Vehicle
--	SVU	12/31/2009	Removed InventoryType filter to allow New and Used vehicles
--	WGH	01/06/2010	Updated Transmission standardization
--	SVU	01/28/2010	Removed Join to dbo.PhotoProviderDealership and added UPPER to VIN
--	WGH	02/09/2010	Update to hit FLDW.dbo.InventoryPhoto_F	
--		02/17/2010	Removed Carfax metric
--		02/26/2010	Added Carfax metric from FLDW 
--	SVU	06/03/2010	Added DRank to prevent Duplicate VIN's
--		06/30/2010	Added dup rows back in and set DoNotPostFlag=1
--	SVU	07/23/2010	Added two columns CVAURL & EnhancedVehicleHighlights-001
--	WGH	10/21/2010	Sourced option list to FLDW.  Updated incorrect
--				OriginalInternetPrice metric.
--	SVU	10/26/2010	Added join to IMT.Extract.DealerConfiguration near MAX(InventoryID)
--	SVU	11/09/2010	Added value to EnhancedVehicleHighlights-001
--	SVU	12/10/2010	Removed Pipes from Merchandising Description
--	WGH	03/08/2011	Added temporary export of NADA Clean via SpecialPrice.  Note: This is to
--				be removed once the M x N book values are added to the export
--		05/05/2011	Changed HoldbackAmount to NULL, added LotPrice
--		05/09/2011	Removed mapping of SpecialPrice to KBB BookValue
--				Added 15 bookout value fields	
--				Mapped MSRP to DMS MSRP
--		07/08/2011	Mapped MSRP to FLDW.dbo.InventoryActive.MSRP
--		07/12/2011	Removed "de-dup" logic (via DoNotPostFlag) for AULtec photo integration
--		08/30/2012	Updated Engine parsing to limit to Chrome-sourced VC entries only
--		12/18/2012	Updated column names for consistency w/other source views
--		01/24/2013	Added pass-through data
--		02/12/2013	Added de-dup logic
--		02/25/2013	Integrated FLDW.dbo.Inventory_F
--		05/14/2013	Integrated Kelley Consumer Value
--		05/30/2013	Added DMS Option Codes from VehicleCatalog (V1)
--		08/02/2013	FBz 26705: Add StoreNumber
--		03/03/2014	Added AuditID
--		10/27/2014	Sourced OptionCodes from FLDW.dbo.Inventory_F
-- 
--------------------------------------------------------------------------------------------------------
AS

SELECT	BusinessUnitID		= I.BusinessUnitID,
	BusinessUnitCode	= BU.BusinessUnitCode,
	DealerId		= DC.ExternalIdentifier,
	InventoryID		= I.InventoryID,
	StockNumber		= I.StockNumber,
	VIN			= UPPER(V.VIN),
	NewOrUsedFlag		= I.InventoryType,
	InventoryDisposition	= NULL,				
	PublicationStatus	= F.PublicationStatus,	-- 1 online, 0 = not online
	InventoryDate		= I.InventoryReceivedDate,
	DMSStatus		= NULLIF(ISC.ShortDescription, '-'),		-- Short description is the raw DMS value (if mapped)
	PackAmount		= CAST(I.Pack AS INT),
	HoldbackAmount		= NULL,	
	CertificationNumber	= F.CertificationNumber,
	TagNumber		= CAST(NULL AS VARCHAR(15)),
	TagState		= CAST(NULL AS VARCHAR(2)),
	Warranty		= NULL,
	DateEntered		= I.InventoryReceivedDate,
	DaysInStock		= I.AgeInDays,
	Restraint		= NULL,
	LotLocation		= RTRIM(LTRIM(I.VehicleLocation)),
	Mileage			= CASE WHEN I.MileageReceived >= 0 THEN I.MileageReceived ELSE NULL END,
	Certified		= CASE WHEN NULLIF(F.CertificationNumber,'') IS NOT NULL THEN 1
				       ELSE I.Certified
				  END,
	CertificationProgram	= NULL, 
	InternetPrice		= CAST(I.ListPrice AS INTEGER),
	OriginalInternetPrice	= F.OriginalInternetPrice,
	OnSpecial		= CAST(NULL AS BIT),
	SpecialPrice		= CAST(NULL AS INT),
	MSRP			= I.MSRP,
	InvoicePrice		= I.UnitCost,
	PriceReducedFlag	= CAST(CASE WHEN I.ListPrice < F.OriginalInternetPrice THEN 1 ELSE 0 END AS BIT),	
	ModelYear		= V.VehicleYear,
	Make			= MK.Make,
	Model			= MD.Model + COALESCE(' ' + VC.Submodel, ''),
	ModelCode		= CAST(LEFT(V.ModelCode,10) AS VARCHAR(10)),
	Trim			= NULLIF(NULLIF(V.VehicleTrim, 'N/A'), 'UNKNOWN'),
	BodyStyle		= VC.BodyStyle,
	StyleID			= CAST(NULL AS INT),	
	NumCylinders		= V.CylinderCount,
	Engine			= CASE WHEN VC.SourceID = 3 THEN SUBSTRING(VC.Engine, CHARINDEX(' ', VC.Engine) + 1,50) ELSE VC.Engine END,

	-- ADDED CHECK for CHARINDEX(' ', VC.Engine) to allow for UNKNOWN engines
	EngineVolume		= CASE WHEN VC.SourceID = 3 AND CHARINDEX(' ', VC.Engine) > 0 THEN CAST(LEFT(VC.Engine, CHARINDEX(' ', VC.Engine) - 2) AS DECIMAL(4,1)) ELSE NULL END,	

	DriveTrain		= NULLIF(V.VehicleDrivetrain, 'N/A'),
	NumDoors		= V.DoorCount,
	Transmission		= UPPER(NULLIF(VC.Transmission, 'UNKNOWN')),

	BedLength		= CAST(NULL AS VARCHAR),
	WheelBase		= CAST(NULL AS VARCHAR),
	FuelType		= NULLIF(V.FuelType, 'N/A'),

	GasMileageCity		= VC.FuelEconomyCity,	
	GasMileageHighway	= VC.FuelEconomyHighway, 

	ExtColor		= REPLACE(NULLIF(V.BaseColor, 'UNKNOWN'), '|', '\'),
	IntColor		= REPLACE(NULLIF(NULLIF(V.InteriorColor,'N/A'), 'UNKNOWN'), '|', '\'),

	ExtColorCode		= CAST(V.ExteriorColorCode AS VARCHAR),
	IntColorCode		= CAST(V.InteriorColorCode AS VARCHAR),
	
	InteriorType		= NULLIF(NULLIF(V.InteriorDescription,'N/A'), 'UNKNOWN'),
	StandardEquipment	= LEFT(OL.OptionList, 2000),									-- NEED TO UPDATE SSIS PACKAGE TO 2000 WIDE
	Options			= CAST(NULL AS VARCHAR),
	OptionCodes		= LEFT(CASE WHEN DC.ExportDmsOptionCodes = 1 THEN F.OptionCodes ELSE NULL END, 300),		
	PackageCodes		= CAST(NULL AS VARCHAR),

	------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	-- NOT SURE HOW MUCH THIS BUYS US, BUT THE IDEA IS TO REMOVE ALL UDFS IN THE SELECT TO SPEED 'ER UP (F'N CALLS IN SELECT = RBAR (USUALLY!))
	------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	-- MerchandisingDescription= CAST(REPLACE(Utility.dbo.ScrubTextForFlatFile(COALESCE(IAF.AdvertisementText, IAF.Body + ' ' + IAF.Footer)),'|','') AS VARCHAR(8000)),

	MerchandisingDescription	= CAST(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(COALESCE(IAF.AdvertisementText, IAF.Body + ' ' + IAF.Footer), CHAR(9), CHAR(32)), CHAR(10), CHAR(32)), CHAR(13), CHAR(32)), CHAR(145), CHAR(39)), CHAR(146), CHAR(39)), CHAR(147), CHAR(34)), CHAR(148), CHAR(34)), '|', ',') AS VARCHAR(8000)),
	
	WebSiteHTMLDescription		= CAST(NULL AS VARCHAR),

	DealerGlobalComment		= CAST(NULL AS VARCHAR),
	CommentSpanish			= CAST(NULL AS VARCHAR),
	CalloutText			= CAST(NULL AS VARCHAR),
	Keywords			= CAST(NULL AS VARCHAR),
	
	ImageModifiedDate		= cast(NULL as VARCHAR),
	ImageCount			= cast(NULL as SMALLINT),
	ImageURLs			= cast(NULL as VARCHAR),
                  
	PhotoSequencing			= CAST(NULL AS VARCHAR),		
	TiledImageURL			= CAST(NULL AS VARCHAR),
	ImageURLsNoText			= CAST(NULL AS VARCHAR),

	VideoURL			= CAST(NULL AS VARCHAR),
	Condition			= CAST(NULL AS VARCHAR),		
	HasOneOwner			= CAST(CASE WHEN F.CarfaxOwnerCount = 1 THEN 1 ELSE 0 END AS TINYINT), 
	TireTreadRemaining		= CAST(NULL AS INT),
	BodyType			= NULLIF(V.BodyType, 'Unknown'),
	------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	DoNotPostFlag			= CAST(0 AS BIT), 		-- Needs to flow through as the filtering occurs in the SSIS package
	------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	ExteriorCondition		= CAST(NULL AS VARCHAR(1)),
	FullyDetailed			= CAST(NULL AS BIT),
	PaintCondition			= CAST(NULL AS VARCHAR(1)),
	TrimCondition			= CAST(NULL AS VARCHAR(1)),
	GlassCondition			= CAST(NULL AS VARCHAR(1)),
	HasNoPanelScratches		= CAST(NULL AS BIT),
	HasNoVisibleDents		= CAST(NULL AS BIT),
	HasNoVisibleRust		= CAST(NULL AS BIT),
	HasNoKnownAccidents		= CAST(NULL AS BIT),
	HasNoKnownBodywork		= CAST(NULL AS BIT),
	InteriorCondition		= CAST(NULL AS VARCHAR(1)),
	CarpetsCondition		= CAST(NULL AS VARCHAR(1)),
	SeatsCondition			= CAST(NULL AS VARCHAR(1)),
	DashboardCondition		= CAST(NULL AS VARCHAR(1)),
	HeadlinerCondition		= CAST(NULL AS VARCHAR(1)),
	PassedDealerInspection		= CAST(NULL AS BIT),
	HaveAllKeys			= CAST(NULL AS BIT),
	HasNoKnownMechProblems		= CAST(NULL AS BIT),
	ReqMaintPerformed		= CAST(NULL AS BIT),
	IsDealerMaintained		= CAST(NULL AS BIT),
	HaveServiceRecords		= CAST(NULL AS BIT),
	HaveOriginalManuals		= CAST(NULL AS BIT),
	ConsumerValueAnalysisURL	= CAST(VAVP.LongURL AS VARCHAR(2100)),
	[EnhancedVehicleHighlights-001]	= CAST(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(COALESCE(IAF.AdvertisementText, IAF.Body + ' ' + IAF.Footer), CHAR(9), CHAR(32)), CHAR(10), CHAR(32)), CHAR(13), CHAR(32)), CHAR(145), CHAR(39)), CHAR(146), CHAR(39)), CHAR(147), CHAR(34)), CHAR(148), CHAR(34)), '|', ',') AS VARCHAR(8000)),
	------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	LotPrice			= CAST(ROUND(I.LotPrice, 0) AS INT),
	------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	BlackBookAverage		= IBV.BlackBookAverage,
	BlackBookClean			= IBV.BlackBookClean,
	BlackBookExtraClean		= IBV.BlackBookExtraClean,
	BlackBookFinanceAdvance		= IBV.BlackBookFinanceAdvance,
	BlackBookRough			= IBV.BlackBookRough,
	GalvesTradeIn			= IBV.GalvesTradeIn,
	GalvesWholesale			= IBV.GalvesWholesale,
	KBBRetail			= COALESCE(CASE WHEN DC.ExportKelleyConsumerValue = 1 THEN KCV.BookValue ELSE NULL END, IBV.KBBRetail),
	KBBTradeIn			= IBV.KBBTradeIn,
	KBBWholesale			= IBV.KBBWholesale,
	NADAAverageTradeIn		= IBV.NADAAverageTradeIn,
	NADALoan			= IBV.NADALoan,
	NADARetail			= IBV.NADARetail,
	NADARoughTradeIn		= IBV.NADARoughTradeIn,
	NADATradeIn			= IBV.NADATradeIn,
	StoreNumber			= I.StoreNumber,
	------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	AuditID				= I.Audit_ID
	
	
FROM	FLDW.dbo.InventoryActive I
	INNER JOIN FLDW.dbo.Vehicle V ON I.BusinessUnitID = V.BusinessUnitID AND V.VehicleID = I.VehicleID
	INNER JOIN FLDW.dbo.Inventory_F F ON I.BusinessUnitID = F.BusinessUnitID AND I.InventoryID = F.InventoryID		-- this filters out duplicates
	
	INNER JOIN Extract.DealerConfiguration#ExtendedProperties DC on I.BusinessUnitID = DC.BusinessUnitID And DC.DestinationName ='AULTec' AND DC.Active = 1	
	
	INNER JOIN dbo.BusinessUnit BU ON BU.BusinessUnitID = I.BusinessUnitID
	INNER JOIN dbo.DealerPreference DP ON I.BusinessUnitID = DP.BusinessUnitID 
	INNER JOIN dbo.PhotoProvider_ThirdPartyEntity TPE ON DatafeedCode ='AutoUplinkImage' 
	
	INNER JOIN VehicleCatalog.Firstlook.Vehiclecatalog VC ON VC.VehicleCatalogID = V.VehicleCatalogID
	INNER JOIN VehicleCatalog.Firstlook.Model MD ON MD.ModelID = VC.ModelID
	INNER JOIN VehicleCatalog.Firstlook.Line L ON L.LineID = MD.LineID
	INNER JOIN VehicleCatalog.Firstlook.Make MK ON MK.MakeID = L.MakeID
	
	LEFT JOIN FLDW.dbo.InventoryAdvertisement_F IAF ON I.BusinessUnitID = IAF.BusinessUnitID AND I.InventoryID = IAF.InventoryID					


	LEFT JOIN FLDW.dbo.InventoryBookoutSelectedOptionList_F OL ON I.BusinessUnitID = OL.BusinessUnitID AND I.InventoryID = OL.InventoryID AND DP.GuideBookID = OL.ThirdPartyID
	
	LEFT JOIN Market.Pricing.Owner O ON O.OwnerEntityID = I.BusinessUnitID AND O.OwnerTypeID = 1
	LEFT JOIN Marketing.ValueAnalyzerVehiclePreference VAVP ON O.OwnerID = VAVP.OwnerID AND I.InventoryID = VAVP.VehicleEntityID AND VAVP.VehicleEntityTypeID = 1
	
	LEFT JOIN Extract.InventoryBookoutValues#AULtec IBV ON I.BusinessUnitID = IBV.BusinessUnitID
								AND I.InventoryID = IBV.InventoryID 
	
	LEFT JOIN FLDW.dbo.InventoryStatusCode_D ISC ON I.InventoryStatusCD = ISC.InventoryStatusCodeID  -- I have no idea why the dimension table doesn't match the IMT source for IDs.  Sorry, on the fix list.
	
	LEFT JOIN FLDW.dbo.InventoryKbbConsumerValue_F KCV ON I.BusinessUnitID = KCV.BusinessUnitID AND I.InventoryID = KCV.InventoryID
		
WHERE	BU.Active = 1	 

UNION ALL

SELECT	BusinessUnitID				= CAST(PT.BusinessUnitID AS INT),
	BusinessUnitCode			= CAST(BusinessUnitCode AS VARCHAR(20)),
	DealerId				= CAST(DC.ExternalIdentifier AS VARCHAR(30)),
	InventoryID				= CAST(NULL AS INT),
	StockNumber				= CAST(StockNumber AS VARCHAR(15)),
	VIN					= CAST(VIN AS VARCHAR(17)),
	NewOrUsedFlag				= CAST(NewOrUsedFlag AS TINYINT),
	InventoryDisposition			= CAST(InventoryDisposition AS INT),
	PublicationStatus			= CAST(PublicationStatus AS BIT),
	InventoryDate				= CAST(InventoryDate AS SMALLDATETIME),
	DMSStatus				= CAST(DMSStatus AS VARCHAR(2)),
	PackAmount				= CAST(PackAmount AS INT),
	HoldbackAmount				= CAST(HoldbackAmount AS INT),
	CertificationNumber			= CAST(CertificationNumber AS VARCHAR(20)),
	TagNumber				= CAST(TagNumber AS VARCHAR(15)),
	TagState				= CAST(TagState AS VARCHAR(2)),
	Warranty				= CAST(Warranty AS INT),
	DateEntered				= CAST(DateEntered AS SMALLDATETIME),
	DaysInStock				= CAST(DaysInStock AS INT),
	Restraint				= CAST(Restraint AS INT),
	LotLocation				= CAST(LotLocation AS VARCHAR(20)),
	Mileage					= CAST(Mileage AS INT),
	Certified				= CAST(Certified AS TINYINT),
	CertificationProgram			= CAST(CertificationProgram AS INT),
	InternetPrice				= CAST(InternetPrice AS INT),
	OriginalInternetPrice			= CAST(OriginalInternetPrice AS INT),
	OnSpecial				= CAST(OnSpecial AS BIT),
	SpecialPrice				= CAST(SpecialPrice AS INT),
	MSRP					= CAST(MSRP AS INT),
	InvoicePrice				= CAST(InvoicePrice AS DECIMAL(9,2)),
	PriceReducedFlag			= CAST(PriceReducedFlag AS BIT),
	ModelYear				= CAST(ModelYear AS INT),
	Make					= CAST(Make AS VARCHAR(50)),
	Model					= CAST(Model AS VARCHAR(50)),
	ModelCode				= CAST(ModelCode AS VARCHAR(10)),
	Trim					= CAST(Trim AS VARCHAR(50)),
	BodyStyle				= CAST(BodyStyle AS VARCHAR(50)),
	StyleID					= CAST(StyleID AS INT),
	NumCylinders				= CAST(NumCylinders AS TINYINT),
	Engine					= CAST(Engine AS VARCHAR(50)),
	EngineVolume				= CAST(EngineVolume AS DECIMAL(4,1)),
	DriveTrain				= CAST(DriveTrain AS VARCHAR(10)),
	NumDoors				= CAST(NumDoors AS TINYINT),
	Transmission				= CAST(Transmission AS VARCHAR(25)),
	BedLength				= CAST(BedLength AS VARCHAR(30)),
	WheelBase				= CAST(WheelBase AS VARCHAR(30)),
	FuelType				= CAST(FuelType AS VARCHAR(3)),
	GasMileageCity				= CAST(GasMileageCity AS DECIMAL(4,1)),
	GasMileageHighway			= CAST(GasMileageHighway AS DECIMAL(4,1)),
	ExtColor				= CAST(ExtColor AS VARCHAR(50)),
	IntColor				= CAST(IntColor AS VARCHAR(50)),
	ExtColorCode				= CAST(ExtColorCode AS VARCHAR(30)),
	IntColorCode				= CAST(IntColorCode AS VARCHAR(30)),
	InteriorType				= CAST(InteriorType AS VARCHAR(50)),
	StandardEquipment			= CAST(StandardEquipment AS VARCHAR(1000)),
	Options					= CAST(Options AS VARCHAR(30)),
	OptionCodes				= CAST(OptionCodes AS VARCHAR(30)),
	PackageCodes				= CAST(PackageCodes AS VARCHAR(30)),
	MerchandisingDescription		= CAST(MerchandisingDescription AS VARCHAR(8000)),
	WebSiteHTMLDescription			= CAST(WebSiteHTMLDescription AS VARCHAR(30)),
	DealerGlobalComment			= CAST(DealerGlobalComment AS VARCHAR(30)),
	CommentSpanish				= CAST(CommentSpanish AS VARCHAR(30)),
	CalloutText				= CAST(CalloutText AS VARCHAR(30)),
	Keywords				= CAST(Keywords AS VARCHAR(30)),
	ImageModifiedDate			= CAST(ImageModifiedDate AS DATETIME),
	ImageCount				= CAST(ImageCount AS SMALLINT),
	ImageURLs				= CAST(ImageURLs AS VARCHAR(30)),
	PhotoSequencing				= CAST(PhotoSequencing AS VARCHAR(30)),
	TiledImageURL				= CAST(TiledImageURL AS VARCHAR(30)),
	ImageURLsNoText				= CAST(ImageURLsNoText AS VARCHAR(30)),
	VideoURL				= CAST(VideoURL AS VARCHAR(30)),
	Condition				= CAST(Condition AS VARCHAR(30)),
	HasOneOwner				= CAST(HasOneOwner AS TINYINT),
	TireTreadRemaining			= CAST(TireTreadRemaining AS INT),
	BodyType				= CAST(BodyType AS VARCHAR(50)),
	DoNotPostFlag				= CAST(DoNotPostFlag AS BIT),
	ExteriorCondition			= CAST(ExteriorCondition AS VARCHAR(1)),
	FullyDetailed				= CAST(FullyDetailed AS BIT),
	PaintCondition				= CAST(PaintCondition AS VARCHAR(1)),
	TrimCondition				= CAST(TrimCondition AS VARCHAR(1)),
	GlassCondition				= CAST(GlassCondition AS VARCHAR(1)),
	HasNoPanelScratches			= CAST(PanelScratches AS BIT),
	HasNoVisibleDents			= CAST(VisibleDents AS BIT),
	HasNoVisibleRust			= CAST(VisibleRust AS BIT),
	HasNoKnownAccidents			= CAST(KnownAccidents AS BIT),
	HasNoKnownBodywork			= CAST(KnownBodywork AS BIT),
	InteriorCondition			= CAST(InteriorCondition AS VARCHAR(1)),
	CarpetsCondition			= CAST(CarpetsCondition AS VARCHAR(1)),
	SeatsCondition				= CAST(SeatsCondition AS VARCHAR(1)),
	DashboardCondition			= CAST(DashboardCondition AS VARCHAR(1)),
	HeadlinerCondition			= CAST(HeadlinerCondition AS VARCHAR(1)),
	PassedDealerInspection			= CAST(PassedDealerInspection AS BIT),
	HaveAllKeys				= CAST(HaveAllKeys AS BIT),
	HasNoKnownMechProblems			= CAST(NoKnownMechProblems AS BIT),
	ReqMaintPerformed			= CAST(ReqMaintPerformed AS BIT),
	IsDealerMaintained			= CAST(DealerMaintained AS BIT),
	HaveServiceRecords			= CAST(HaveServiceRecords AS BIT),
	HaveOriginalManuals			= CAST(HaveOriginalManuals AS BIT),
	ConsumerValueAnalysisURL		= CAST(ConsumerValueAnalysisURL AS VARCHAR(2100)),
	[EnhancedVehicleHighlights-001]		= CAST([EnhancedVehicleHighlights-001] AS VARCHAR(8000)),
	LotPrice				= CAST(LotPrice AS INT),
	BlackBookAverage			= CAST(BlackBookAverage AS INT),
	BlackBookClean				= CAST(BlackBookClean AS INT),
	BlackBookExtraClean			= CAST(BlackBookExtraClean AS INT),
	BlackBookFinanceAdvance			= CAST(BlackBookFinanceAdvance AS INT),
	BlackBookRough				= CAST(BlackBookRough AS INT),
	GalvesTradeIn				= CAST(GalvesTradeIn AS INT),
	GalvesWholesale				= CAST(GalvesWholesale AS INT),
	KBBRetail				= CAST(KBBRetail AS INT),
	KBBTradeIn				= CAST(KBBTradeIn AS INT),
	KBBWholesale				= CAST(KBBWholesale AS INT),
	NADAAverageTradeIn			= CAST(NADAAverageTradeIn AS INT),
	NADALoan				= CAST(NADALoan AS INT),
	NADARetail				= CAST(NADARetail AS INT),
	NADARoughTradeIn			= CAST(NADARoughTradeIn AS INT),
	NADATradeIn				= CAST(NADATradeIn AS INT),
	StoreNumber				= CAST(NULL AS VARCHAR(10)),
	------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	AuditID					= CAST(NULL AS INT)	

FROM	Datafeeds.Extract.AULtecGID#PassThrough	PT
	INNER JOIN IMT.Extract.DealerConfiguration DC on PT.BusinessUnitID = DC.BusinessUnitID And DC.DestinationName ='AULTec' AND DC.Active = 1	
	
	
GO

EXEC dbo.sp_SetViewDescription 'Extract.Inventory#AULtec',
'V2 Inventory (IMT) extract for the AULtec export process.  Used by the SSIS package Extract-FlatFile-AULTec.dtsx'