IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[Extract].[Inventory#Groove]')
                    AND OBJECTPROPERTY(id, N'IsView') = 1 ) 
    DROP VIEW [Extract].[Inventory#Groove]
GO
CREATE VIEW [Extract].[Inventory#Groove]
AS
    SELECT  v.[Vin]
          , i.[StockNumber]
          , ar.[merchandisingDescription]
          , i.[BusinessUnitID]
    FROM    IMT.dbo.[Inventory] i WITH ( NOLOCK )
            INNER JOIN IMT.dbo.[tbl_Vehicle] v WITH ( NOLOCK ) ON [i].[VehicleID] = [v].[VehicleID]
            LEFT OUTER JOIN [MerchandisingInterface].[Interface].[AdReleases] ar WITH ( NOLOCK ) ON i.[BusinessUnitID] = ar.[businessUnitId]
                                                                                                    AND i.[InventoryID] = ar.[inventoryId]
    WHERE   i.[BusinessUnitID] IN ( 106363, 106876, 106877, 106855 )
            AND i.[InventoryActive] = 1
			
GO
