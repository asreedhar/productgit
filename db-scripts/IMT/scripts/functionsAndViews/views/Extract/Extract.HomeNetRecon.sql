IF OBJECT_ID('[Extract].[HomeNetRecon]', 'V') IS NOT NULL
    DROP VIEW [Extract].[HomeNetRecon]
GO
CREATE VIEW [Extract].[HomeNetRecon]
AS
    WITH    cte ( [LoadId] )
              AS ( SELECT   MAX([dh].[Load_ID]) AS [LoadId]
                   FROM     [DBASTAT].[dbo].[Dataload_History] dh
                   WHERE    [dh].[DatafeedCode] = 'HomeNetListing'
                            AND [dh].[DatafeedFileType] = 'Recon'
                            AND [dh].[ProcessResult] = 3
                 )
    SELECT  [rs].[VIN]
          , [dh].[ProcessDT] AS FileTimestamp
          , [bu].[BusinessUnitID]
          , [bu].[BusinessUnitCode]
          , [rs].[PhotoCount] AS ImageCnt
          , [rs].[AdLength] AS Comments
          , [rs].[ListPrice] AS InternetPrice
          , [dh].[ProcessDT] AS LoadDt
    FROM    [DBASTAT].[dbo].[ReconStandard] rs WITH ( NOLOCK )
            INNER JOIN [DBASTAT].[dbo].[Dataload_History] dh ON [dh].[Load_ID] = [rs].[LoadId]
            INNER JOIN cte c ON [rs].[LoadId] = [c].[LoadId]
            LEFT OUTER JOIN [IMT].[dbo].[PhotoProviderDealership] ppd ON [ppd].[PhotoProvidersDealershipCode] = [rs].[DealerId]
            LEFT OUTER JOIN [IMT].[dbo].[PhotoProvider_ThirdPartyEntity] pptpe ON [pptpe].[PhotoProviderID] = [ppd].[PhotoProviderID]
                                                                                  AND [pptpe].[DatafeedCode] = [dh].[DatafeedCode]
            LEFT OUTER JOIN [IMT].[dbo].[ThirdPartyEntity] tpe ON [tpe].[ThirdPartyEntityID] = [pptpe].[PhotoProviderID]
            LEFT OUTER JOIN [IMT].[dbo].[BusinessUnit] bu ON [bu].[BusinessUnitID] = [ppd].[BusinessUnitID]
    WHERE   [tpe].[Name] = 'Home Net'
GO
