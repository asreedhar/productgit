SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'Extract.Sales#Hendrick') AND OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW Extract.Sales#Hendrick
GO
CREATE VIEW Extract.Sales#Hendrick
AS
    SELECT  DealDate
           ,InventoryReceivedDate
           ,i.BusinessUnitID
		   ,InventoryType = it.InventoryVehicleTypeDescription
           ,BusinessUnit
           ,SaleDescription
           ,VIN
           ,make
           ,Model
           ,VehicleYear
           ,BaseColor
           ,DATEDIFF(dd, InventoryReceivedDate, DealDate) AS 'Days to Sale'
           ,UnitCost
           ,SalePrice
           ,FrontEndGross
           ,BackEndGross
           ,CASE WHEN TradeOrPurchase = 1 THEN 'P'
                 ELSE 'T'
            END AS 'T/P'
           ,MileageReceived
           ,StockNumber
    FROM    Inventory (NOLOCK) AS I
            JOIN IMT.dbo.tbl_VehicleSale AS TVS ON I.InventoryID = TVS.InventoryID
            JOIN Vehicle (NOLOCK) AS V ON v.VehicleID = I.VehicleID
            JOIN BusinessUnit (NOLOCK) AS BU ON I.BusinessUnitID = BU.BusinessUnitID
            JOIN BusinessUnitRelationship (NOLOCK) AS BUR ON BU.BusinessUnitID = BUR.BusinessUnitID
		    JOIN IMT.dbo.lu_InventoryVehicleType (NOLOCK) it ON it.InventoryVehicleTypeCD=i.InventoryType
    WHERE   ParentID = 100068
            AND InventoryType = 2
            AND bu.active = 1
			AND DealDate >= DATEADD(yy, DATEDIFF(yy,0,GETDATE()), 0)  
			AND DealDate < DATEADD(dd,0, DATEADD(yy, DATEDIFF(yy,0,GETDATE())+1, 0) )

GO


