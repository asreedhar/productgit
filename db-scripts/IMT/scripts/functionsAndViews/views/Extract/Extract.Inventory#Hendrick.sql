SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'Extract.Inventory#Hendrick') AND OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW  Extract.Inventory#Hendrick
GO
CREATE VIEW Extract.Inventory#Hendrick
AS
SELECT  BusinessUnit ,
        BusinessUnitCode ,
         I.InventoryID
               ,I.StockNumber
               ,I.BusinessUnitID
               ,I.VehicleID
               ,I.InventoryActive
               ,I.InventoryReceivedDate
               ,I.DeleteDt
               ,I.ModifiedDT
               ,I.DMSReferenceDt
               ,I.UnitCost
               ,I.AcquisitionPrice
               ,I.Pack
               ,I.MileageReceived
               ,I.TradeOrPurchase
               ,I.ListPrice
               ,I.InitialVehicleLight
               ,InventoryType = it.InventoryVehicleTypeDescription
               ,I.ReconditionCost
               ,I.UsedSellingPrice
               ,I.Certified
               ,I.VehicleLocation
               ,I.CurrentVehicleLight
               ,I.FLRecFollowed
               ,I.Audit_ID
               ,I.InventoryStatusCD
               ,I.ListPriceLock
               ,I.SpecialFinance
               ,I.eStockCardLock
               ,I.DateBookoutLocked
               ,I.BookoutLocked
               ,I.PlanReminderDate
               ,I.VersionNumber
               ,I.RowVersion
               ,I.EdmundsTMV
               ,I.BookoutRequired
               ,I.MileageReceivedLock
               ,I.LotPrice
               ,I.TransferPrice
               ,I.TransferForRetailOnly
               ,I.TransferForRetailOnlyEnabled
               ,I.AgeInDays
               ,I.InsertDate
               ,I.InventoryStateID
               ,I.BranchNumber
               ,I.DealershipIdentifier
               ,I.MSRP
               ,I.StoreNumber
               ,I.UsedGroup
               ,I.ImageModifiedDate
               ,I.CertifiedLock
               ,I.BaseColorLock 
         --TV.VehicleID
               ,TV.Vin
               ,TV.VehicleYear
               ,TV.MakeModelGroupingID
               ,TV.VehicleTrim
               ,TV.VehicleEngine
               ,TV.VehicleDriveTrain
               ,TV.VehicleTransmission
               ,TV.CylinderCount
               ,TV.DoorCount
               ,TV.BaseColor
               ,TV.InteriorColor
               ,TV.FuelType
               ,TV.InteriorDescription
               ,TV.CreateDt
               ,TV.OriginalMake
               ,TV.OriginalModel
               ,TV.OriginalYear
               ,TV.OriginalTrim
               ,TV.OriginalBodyStyle
               --,TV.Audit_ID
               ,TV.VehicleSourceID
               ,TV.CatalogKey
               ,TV.VIC
               ,TV.VehicleCatalogID
               ,TV.BodyTypeID
               ,TV.ExtColor
               ,TV.ModelCode
               --,TV.RowVersion
               ,TV.ExteriorColorCode
               ,TV.InteriorColorCode
FROM    IMT.dbo.Inventory (NOLOCK) AS I
        JOIN IMT.dbo.tbl_Vehicle (NOLOCK) AS TV ON I.VehicleID = TV.VehicleID
        JOIN IMT.dbo.BusinessUnit (NOLOCK) AS BU ON i.BusinessUnitID = bu.BusinessUnitID
        JOIN IMT.dbo.BusinessUnitRelationship (NOLOCK) AS BUR ON BU.BusinessUnitID = BUR.BusinessUnitID
		JOIN IMT.dbo.lu_InventoryVehicleType (NOLOCK) it ON it.InventoryVehicleTypeCD=i.InventoryType
WHERE   ParentID = 100068
        AND InventoryActive = 1
		AND bu.active = 1

