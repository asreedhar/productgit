IF EXISTS ( SELECT  *
            FROM    [sys].[sysobjects]
            WHERE   id = OBJECT_ID(N'Extract.Inventory#AIM')
                    AND OBJECTPROPERTY(id, N'IsView') = 1 )
    DROP VIEW [Extract].[Inventory#AIM]
GO
CREATE VIEW [Extract].[Inventory#AIM]
AS
    SELECT  [bu].[BusinessUnitCode] AS DealerID
          , [i].[StockNumber] AS [StockNumber]
          , [v].[Vin] AS [VIN]
          , [ivt].[InventoryVehicleTypeDescription] AS [Type]
          , [v].[VehicleYear] AS [Year]
          , [MK].[Make]
          , [MD].[Model]
          , [ar].[Trim]
          , [VC].[BodyStyle] AS [Series]
          , NULLIF([BT].[BodyType], 'Unknown') AS [Body]
          , [v].[DoorCount] AS [Doors]
          , [ar].[ExteriorColor]
          , [ar].[InteriorColor]
          , [ar].[ExteriorColorCode] AS [ExtColorCode]
          , [ar].[InteriorColorCode] AS [IntColorCode]
          , [ar].[ModelCode] AS [ModelNumber]
          , NULLIF([i].[MileageReceived], -1) AS [Mileage]
          , [i].[ListPrice] AS [InternetPrice]
          , [i].[LotPrice] AS [StickerPrice]
          , [i].[MSRP]
          , '' AS [BookValue]
          , '' AS [MiscPrice]
          , '' AS [Invoice]
          , CASE [i].[Certified]
              WHEN 1 THEN 'Yes'
              ELSE 'No'
            END AS [Certified]
          , CASE WHEN [VC].[SourceID] = 3 THEN SUBSTRING([VC].[Engine], CHARINDEX(' ', [VC].[Engine]) + 1, 50)
                 ELSE [VC].[Engine]
            END AS EngineDescription
          , [v].[CylinderCount] AS [EngineCylinders]
          , CASE WHEN [VC].[SourceID] = 3
                      AND CHARINDEX(' ', [VC].[Engine]) > 0 THEN CAST(LEFT([VC].[Engine], CHARINDEX(' ', [VC].[Engine]) - 2) AS DECIMAL(4, 1))
                 ELSE NULL
            END AS [EngineDisplacement]
          , CASE WHEN [DC].[UseMaxAdTransmission] = 1 THEN LEFT([CS].[UserFriendlyName], 50)
                 ELSE CAST([VC].[Transmission] AS VARCHAR(50))
            END AS [Transmission]
          , [v].[VehicleDriveTrain] AS [DriveTrain]
          , [v].[FuelType] AS [FuelType]
          , [ar].[GasMileageCity] AS [MPGCity]
          , [ar].[GasMileageHighway] AS [MPGHighway]
          , [i].[InventoryReceivedDate] AS [InventoryStartDate]
          , [i].[AgeInDays] AS [DaysInStock]
          , [ar].[HtmlMerchandisingDescription] AS [Description]
          , CAST([ar].[optionsList] AS VARCHAR(5000)) AS [Options]
          , [ar].[InteriorType] AS [Interior Upholstery]
          , LEFT([ar].[OptionCodes], 50) AS [PackageCodes]
          , CAST(GETDATE() AS DATE) AS [ImageModifiedDate]
          , [Staging].[AULtec].[PhotosFlattened]([i].[BusinessUnitID], [i].[InventoryID]) AS [ImageURLs]
          , '' AS [Video URL]
          , CAST([i].[ModifiedDT] AS DATE) AS [DateModified]
          , [bu].[BusinessUnitShortName] AS [DealerName]
          , [bu].[Address1] AS [DealerAddress]
          , [bu].[City] AS [DealerCity]
          , [bu].[State] AS [DealerState]
          , [bu].[ZipCode] AS [DealerZip]
          , '' AS [DealerPhone]
          , '' AS [DealerEmail]
          , '' AS [DealerWebSite]
          , '' AS [Location]
          , '' AS [Vehicle Type]
          , 'http://hendrickcarsofcharleston.com/#!/ps/' + [v].[Vin] AS DigitalShowroomUrl
    FROM    [IMT].[dbo].[Inventory] i
            INNER JOIN [IMT].[dbo].[BusinessUnit] bu ON [i].[BusinessUnitID] = [bu].[BusinessUnitID]
            INNER JOIN [IMT].[dbo].[tbl_Vehicle] v ON [i].[VehicleID] = [v].[VehicleID]
            INNER JOIN [VehicleCatalog].[Firstlook].[VehicleCatalog] VC ON [VC].[VehicleCatalogID] = [v].[VehicleCatalogID]
            INNER JOIN [VehicleCatalog].[Firstlook].[Model] MD ON [MD].[ModelID] = [VC].[ModelID]
            INNER JOIN [VehicleCatalog].[Firstlook].[Line] L ON [L].[LineID] = [MD].[LineID]
            INNER JOIN [VehicleCatalog].[Firstlook].[Make] MK ON [MK].[MakeID] = [L].[MakeID]
            INNER JOIN [IMT].[dbo].[lu_InventoryVehicleType] ivt ON [ivt].[InventoryVehicleTypeCD] = [i].[InventoryType]
            INNER JOIN [IMT].[Extract].[DealerConfiguration#ExtendedProperties] DC ON [i].[BusinessUnitID] = [DC].[BusinessUnitID]
                                                                                      AND [DC].[DestinationName] = 'AULTec'
                                                                                      AND [DC].[Active] = 1
            LEFT JOIN [VehicleCatalog].[Firstlook].[BodyType] BT ON [v].[BodyTypeID] = [BT].[BodyTypeID]
            LEFT OUTER JOIN [MerchandisingInterface].[Interface].[AdReleases] ar ON [i].[InventoryID] = [ar].[inventoryId]
                                                                                    AND [ar].[businessUnitId] = [i].[BusinessUnitID]
            LEFT JOIN ( [Merchandising].[builder].[NewEquipmentMapping] EM
                        INNER JOIN [VehicleCatalog].[Chrome].[Categories] CS ON [EM].[TransmissionTypeCategoryID] = [CS].[CategoryID]
                                                                                AND [CS].[CountryCode] = 1
                      ) ON [EM].[inventoryID] = [i].[InventoryID]
                           AND [EM].[businessUnitID] = [i].[BusinessUnitID]
    WHERE   [i].[InventoryActive] = 1
            AND [i].[BusinessUnitID] IN ( 104873, 100590, 101203, 102744, 104811 )
            AND [i].[InventoryType] = 2
GO
