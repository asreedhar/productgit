IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Certified].[InventorySavedCertifiedProgram]'))
DROP VIEW [Certified].[InventorySavedCertifiedProgram]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create view [Certified].[InventorySavedCertifiedProgram]
as

select i.InventoryID, cn.CertifiedProgramId
from imt.dbo.Inventory_CertificationNumber cn
join imt.dbo.Inventory i on cn.InventoryID = i.InventoryID
where exists (
	select *
	from imt.Certified.DealerValidPrograms vp
	where vp.BusinessUnitID = i.BusinessUnitID
	and vp.CertifiedProgramID = cn.CertifiedProgramId
)

GO


