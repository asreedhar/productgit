IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Certified].[InventoryMfrFeedCertifiedProgram]'))
DROP VIEW [Certified].[InventoryMfrFeedCertifiedProgram]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create view [Certified].[InventoryMfrFeedCertifiedProgram]
as

select ia.InventoryID, m.Make, cp.CertifiedProgramID
from fldw.dbo.InventoryActive ia
join fldw.dbo.Vehicle v on ia.BusinessUnitID = v.BusinessUnitID and ia.VehicleID = v.VehicleID
join imt.dbo.MakeModelGrouping mmg on v.MakeModelGroupingID = mmg.MakeModelGroupingID
join VehicleCatalog.Firstlook.Make m on mmg.Make = m.Make
join VehicleCatalog.vds.VehicleAttribute va ON va.vin=ia.vin AND va.AttributeId=3
join IMT.Certified.CertifiedProgram_Make cpm ON m.MakeID = cpm.MakeID AND cpm.ExternalID=va.AttributeValue
join IMT.Certified.CertifiedProgram cp ON cp.CertifiedProgramID=cpm.CertifiedProgramID AND cp.Active=1 AND cp.ProgramType=1	
-- we used to check for Inventory.Certified = 1 here, but we changed the logic to flip that bit based on this feed
-- So we now return vehicles regardless of their Inventory.Certified state.  (This was intentional)
WHERE 1=1 -- m.Make = 'BMW'
and exists (
	select *
	from imt.Certified.DealerValidPrograms vp
	where vp.BusinessUnitID = ia.BusinessUnitID
	and vp.CertifiedProgramID = cp.CertifiedProgramID
)


GO

