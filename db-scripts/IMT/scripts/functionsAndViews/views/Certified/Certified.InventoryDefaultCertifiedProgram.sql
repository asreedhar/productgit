IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Certified].[InventoryDefaultCertifiedProgram]'))
DROP VIEW [Certified].[InventoryDefaultCertifiedProgram]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create view [Certified].[InventoryDefaultCertifiedProgram]
as

 with ActiveProgramsWithMakes as
(
	-- returns programs with any associated makes and the business unit scope (limitedToBUID)
	select cp.CertifiedProgramID, cp.Name, cpm.MakeID, cp.BusinessUnitId as LimitedToBUID
	from imt.Certified.CertifiedProgram cp
	left join imt.Certified.CertifiedProgram_Make cpm on cp.CertifiedProgramID = cpm.CertifiedProgramID
	where cp.Active = 1
)
,DealerManufacturerPrograms as 
( 
	-- returns Mfr programs 
	select ocp.OwnerId, o.OwnerEntityID as BusinessUnitId, cp.CertifiedProgramID, cp.name as ProgramName, cp.MakeID
	from ActiveProgramsWithMakes cp
	join imt.Certified.OwnerCertifiedProgram ocp on cp.CertifiedProgramID = ocp.CertifiedProgramID
	join Market.Pricing.Owner o on ocp.OwnerId = o.OwnerID and o.OwnerTypeID = 1 -- dealer
	where cp.LimitedToBUID is null -- designates a program as a manufacturer program
	and MakeID is not null -- also designates a mfr program
)
,DealerMfrProgramsAutoSelect as
(
	-- this returns the owner (dealer) and the makes for which the dealer participates in exactly one program.
	-- it is these that can be auto-selected
	select OwnerId, BusinessUnitId, MakeID, max(CertifiedProgramID) as AutoSelectCertifiedProgramId, max(ProgramName) as ProgramName
	from DealerManufacturerPrograms
	where MakeID is not null -- Manufacturer programs only
	group by OwnerId, BusinessUnitId, MakeID
	having count(*) = 1
)

select ia.InventoryID, autosel.AutoSelectCertifiedProgramId
from fldw.dbo.InventoryActive ia
join fldw.dbo.Vehicle v on ia.BusinessUnitID = v.BusinessUnitID and ia.VehicleID = v.VehicleID
join imt.dbo.MakeModelGrouping mmg on v.MakeModelGroupingID = mmg.MakeModelGroupingID
join VehicleCatalog.Firstlook.Make m on mmg.Make = m.Make
join DealerMfrProgramsAutoSelect autosel on ia.BusinessUnitID = autosel.BusinessUnitId and m.MakeID = autosel.MakeID
where ia.Certified = 1
and m.Make != 'BMW' -- must get this from the Mfr. feed
and exists (
	select *
	from imt.Certified.DealerValidPrograms vp
	where vp.BusinessUnitID = ia.BusinessUnitID
	and vp.CertifiedProgramID = autosel.AutoSelectCertifiedProgramId
)

GO



