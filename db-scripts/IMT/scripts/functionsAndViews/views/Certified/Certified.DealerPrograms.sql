if exists ( select   *
            from     dbo.sysobjects
            where    id = object_id(N'Certified.DealerPrograms')
                     and objectproperty(id, N'IsView') = 1 ) 
   drop view Certified.DealerPrograms
GO

create view Certified.DealerPrograms
as
select 
	case 
		when cp.BusinessUnitID = bur.ParentID then 1
		when cp.BusinessUnitId = bur.BusinessUnitID then 2
	end as ProgramPriority
,	case 
		when cp.BusinessUnitID = bur.ParentID then 'Group'
		when cp.BusinessUnitId = bur.BusinessUnitID then 'Dealer'
	end as ProgramScope
, bur.BusinessUnitID, bur.ParentID, cp.CertifiedProgramID, cp.Name, cp.Active
from imt.Certified.CertifiedProgram cp
join imt.dbo.BusinessUnitRelationship bur 
on cp.BusinessUnitID = bur.BusinessUnitID
or cp.BusinessUnitId = bur.ParentID

GO
