IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Certified].[DealerValidPrograms]'))
DROP VIEW [Certified].[DealerValidPrograms]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create view [Certified].[DealerValidPrograms]
as
with CPODealers as 
(
	select bu.BusinessUnitID, o.OwnerID
	from imt.dbo.BusinessUnit bu
	join Market.Pricing.Owner o on bu.BusinessUnitID = o.OwnerEntityID and o.OwnerTypeID = 1
	where exists(
		-- dealer has upgrades required to use CPO
		select *
		from imt.dbo.DealerUpgrade du
		where du.DealerUpgradeCD in (23,24)
		and du.EffectiveDateActive = 1
		and du.BusinessUnitID = bu.BusinessUnitID
	)
	and bu.Active = 1
)

select dlr.BusinessUnitID, cp.CertifiedProgramID
from imt.Certified.OwnerCertifiedProgram ocp
join CPODealers dlr on ocp.OwnerId = dlr.OwnerID
join imt.Certified.CertifiedProgram cp on ocp.CertifiedProgramID = cp.CertifiedProgramID
where cp.BusinessUnitID is null -- mfr programs
and cp.Active = 1
union all
select dp.BusinessUnitID, dp.CertifiedProgramID
from imt.Certified.DealerPrograms dp -- dealer programs
where dp.Active = 1

GO