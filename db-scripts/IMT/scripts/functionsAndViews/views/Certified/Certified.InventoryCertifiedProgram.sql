IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[Certified].[InventoryCertifiedProgram]'))
DROP VIEW [Certified].[InventoryCertifiedProgram]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create view [Certified].[InventoryCertifiedProgram]
as

with InvCertification as 
(
	select 
		ia.BusinessUnitID, 
		ia.InventoryID,
		ia.Certified as ManufacturerCertified,
		case 
			when ia.Certified = 0 
			and coalesce(s.CertifiedProgramId, d.AutoSelectCertifiedProgramId, mf.CertifiedProgramID) is not null 
			then cast(1 as bit)
			else cast(0 as bit) 
		end as DealerCertified,
		coalesce(s.CertifiedProgramId, d.AutoSelectCertifiedProgramId, mf.CertifiedProgramID) as CertifiedProgramId,
		ProgramIsPersisted = 
			case 
				when s.CertifiedProgramId is not null then cast(1 as bit)
				when d.AutoSelectCertifiedProgramId is not null then cast(0 as bit)
				when mf.CertifiedProgramId is not null then cast(0 as bit)
			end,
		IsManufacturerSpecified =
			case 
				when s.CertifiedProgramId is null
					and d.AutoSelectCertifiedProgramId is null
					and mf.CertifiedProgramID is not null
					then cast(1 as bit)
				else cast(0 as bit)
			end
	from fldw.dbo.InventoryActive ia
	left join imt.Certified.InventorySavedCertifiedProgram s on ia.InventoryID = s.InventoryID
	left join imt.Certified.InventoryDefaultCertifiedProgram d on ia.InventoryId = d.InventoryID
	left join imt.Certified.InventoryMfrFeedCertifiedProgram mf on ia.InventoryID = mf.InventoryID
)

select ic.*, cp.Name as CertifiedProgramName
from InvCertification ic
left join imt.Certified.CertifiedProgram cp 
	on ic.CertifiedProgramId = cp.CertifiedProgramID


GO


