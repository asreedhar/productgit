if object_id('dbo.InventoryActivePartialPhotoState') is not null
	drop view dbo.InventoryActivePartialPhotoState
go


create view dbo.InventoryActivePartialPhotoState
as
select
	states.InventoryID,
	NeedScrape = case when states.HasScrape = 1 and (states.HasDelete = 0 or states.HasOthers = 1) then 1 else 0 end,
	NeedDelete = case when states.HasDelete = 1 and (states.HasScrape = 0 or states.HasOthers = 1) then 1 else 0 end,
	NeedReplace = case when states.HasScrape = 1 and states.HasDelete = 1 and states.HasOthers = 0 then 1 else 0 end
from
	(
		select
			ip.InventoryID,
			sign(sum(case when p.PhotoStatusCode = 3 then 1 else 0 end)) as HasScrape,
			sign(sum(case when p.PhotoStatusCode = 6 then 1 else 0 end)) as HasDelete,
			sign(sum(case when p.PhotoStatusCode not in (3,6) then 1 else 0 end)) as HasOthers
		from 
			IMT.dbo.Photos p
			join IMT.dbo.InventoryPhotos ip
				on p.PhotoID = ip.PhotoID
		group by ip.InventoryID
	) states
	join FLDW.dbo.InventoryActive ia
		on states.InventoryID = ia.InventoryID
go