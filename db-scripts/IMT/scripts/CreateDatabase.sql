IF EXISTS(SELECT name FROM master.dbo.sysdatabases WHERE name = N'__DATABASE__')
	DROP DATABASE __DATABASE__
go


CREATE DATABASE __DATABASE__
	ON PRIMARY
		(name = '__DATABASE__', filename = '__SQL_SERVER_DATA__\__DATABASE__-Primary.mdf', size = 100MB, maxsize = unlimited, filegrowth = 50MB),
	FILEGROUP [DATA]
		(name = '__DATABASE__-Data', filename = '__SQL_SERVER_DATA__\__DATABASE__-Data.mdf', size = 100MB, maxsize = unlimited, filegrowth = 50MB),
	FILEGROUP [IDX]
		(name = '__DATABASE__-IDX', filename = '__SQL_SERVER_DATA__\__DATABASE__-IDX.ndf', size = 100MB, maxsize = unlimited, filegrowth = 50MB)
	LOG ON
		(name = '__DATABASE__-Log', filename = '__SQL_SERVER_LOG__\__DATABASE__-Log.ldf', size = 25MB, maxsize = unlimited, filegrowth = 25MB)

ALTER DATABASE __DATABASE__ SET
	AUTO_CREATE_STATISTICS ON
	, AUTO_UPDATE_STATISTICS ON
	, RECOVERY simple

go


CREATE TABLE __DATABASE__.dbo.Alter_Script
(
	AlterScriptName VARCHAR(100) NOT NULL
	, ApplicationDate DATETIME NOT NULL CONSTRAINT DF_Alter_Script_ApplicationDate DEFAULT (GETDATE())
)

INSERT __DATABASE__.dbo.Alter_Script (AlterScriptName) VALUES ('Database Created')
go
