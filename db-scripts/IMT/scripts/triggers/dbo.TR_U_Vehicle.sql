IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TR_U_Vehicle]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].[TR_U_Vehicle]
GO

CREATE TRIGGER dbo.TR_U_Vehicle ON dbo.tbl_Vehicle
AFTER UPDATE NOT FOR REPLICATION
AS
IF UPDATE(BaseColor) OR UPDATE(VehicleTrim) OR UPDATE(VehicleCatalogID) --OR UPDATE(VehicleBodyStyleID)
	UPDATE	A
	SET	A.BaseColor		= T.BaseColor,
		A.VehicleTrim		= ISNULL(NULLIF(T.VehicleTrim,''),'UNKNOWN'),
		A.VehicleCatalogID	= T.VehicleCatalogID 
--		A.VehicleBodyStyleID = T.VehicleBodyStyleID
	FROM	inserted T
		JOIN [FLDW].dbo.Vehicle A ON T.VehicleID = A.VehicleID
GO


/* 	TODO:
	
	NEED TO UPDATE THE ColorID and TrimID ON FLDW.dbo.Vehicle
	
*/	