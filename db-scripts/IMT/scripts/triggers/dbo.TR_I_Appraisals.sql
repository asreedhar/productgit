IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TR_I_Appraisals]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].[TR_I_Appraisals]
GO

----------------------------------------------------------------------------------------------------------------
--	THE APPRAISAL_F's PK IS BusinessUnitID + VehicleID
--	
--	AT THIS POINT THERE MAY NOT BE A VALUE SO SET THE VALUE ID AND VALUE TO NULL
--	THE AppraisalValue TRIGGER WILL FIRE AND UPDATE THE ID AND VALUE WHEN THERE IS A PROPER VALUE
----------------------------------------------------------------------------------------------------------------

CREATE TRIGGER dbo.TR_I_Appraisals ON dbo.Appraisals
AFTER INSERT NOT FOR REPLICATION
AS

----------------------------------------------------------------------------------------------------------------
--	INSERT Appraisal_F
----------------------------------------------------------------------------------------------------------------

DELETE	F
FROM	FLDW.dbo.Appraisal_F F
	INNER JOIN inserted I ON F.BusinessUnitID = I.BusinessUnitID AND F.VehicleID = I.VehicleID

INSERT
INTO	FLDW.dbo.Appraisal_F  (BusinessUnitID, AppraisalID, VehicleID, AppraisalValueID, Value, AppraisalTypeID, DateCreated, DateModified, Mileage, AppraisalActionTypeID)

SELECT	I.BusinessUnitID, I.AppraisalID, I.VehicleID, NULL, NULL, I.AppraisalTypeID, I.DateCreated, I.DateModified, I.Mileage, 0
FROM	inserted I

GO