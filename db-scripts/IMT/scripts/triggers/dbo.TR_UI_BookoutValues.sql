------------------------------------------------------------------------------------------------
--	10/2006
--	Experimental trigger to keep FLDW in synch with bookouts during the supported workday.
--	This will need a bucket load of testing to make sure the triggers don't bog down
--	live bookouts.
--	Additionally, will need a to check whether the bookout process is running; if so, 
--	disable the trigger.
--
--	04 Deployment notes (10/24/2006):
--	Trigger tested with bookout process in UAT and ran successfully.
--	Will need to monitor affect in production on bookout throughput.
--
--	"Edge Prime" Deployment notes (01/17/2007)
--	Added AppraisalBookout_F
--
--	V5_18_01 Deployment notes (10/14/2008)
--	Removed Insert InventoryBookout_F, now handled in Bookout.InventoryBookout#Save.
--	
--	04/30/2014	JBF
--	Removed InventoryBookout_F completely; using indexed view now
------------------------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TR_UI_BookValues]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].[TR_UI_BookValues]
GO

CREATE TRIGGER dbo.TR_UI_BookValues ON dbo.BookoutValues
AFTER INSERT, UPDATE NOT FOR REPLICATION
AS

----------------------------------------------------------------------------------------------------------------
--	UPDATE AppraisalBookout_F 
----------------------------------------------------------------------------------------------------------------

UPDATE	F
SET	BookoutID			= B.BookoutID,
	BookoutValueCreatedTimeID 	= T.TimeID,
	Value   			= BV.Value, 
	IsValid 			= BTPC.IsValid, 
	IsAccurate 			= B.IsAccurate

FROM	inserted BV
 	JOIN BookoutThirdPartyCategories BTPC ON BV.BookoutThirdPartyCategoryID = BTPC.BookoutThirdPartyCategoryID
 	JOIN ThirdPartyCategories TPC ON BTPC.ThirdPartyCategoryID = TPC.ThirdPartyCategoryID
 	JOIN Bookouts B ON BTPC.BookoutID = B.BookoutID
	JOIN AppraisalBookouts AB ON B.BookoutID = AB.BookoutID
	JOIN Appraisals A ON AB.AppraisalID = A.AppraisalID
	JOIN [FLDW]..Time_D T ON T.TypeCD = 1 AND cast(convert(varchar(10),BV.DateCreated,101) as datetime) = T.BeginDate
	JOIN [FLDW]..AppraisalBookout_F F ON A.BusinessUnitID = F.BusinessUnitID AND AB.AppraisalID = F.AppraisalID AND TPC.ThirdPartyCategoryID = F.ThirdPartyCategoryID AND BV.BookoutValueTypeID = F.BookoutValueTypeID

WHERE	BV.BookoutValueTypeID = 2

----------------------------------------------------------------------------------------------------------------
--	INSERT AppraisalBookout_F IF THE UPDATE DIDN'T FIND ANY ROWS
----------------------------------------------------------------------------------------------------------------

IF @@ROWCOUNT = 0

	INSERT
	INTO	[FLDW]..AppraisalBookout_F (BusinessUnitID, AppraisalID, BookoutID, ThirdPartyCategoryID, BookoutValueTypeID, BookoutValueCreatedTimeID, Value, IsValid, IsAccurate)
	
	SELECT	A.BusinessUnitID, A.AppraisalID, AB.BookoutID, TPC.ThirdPartyCategoryID, BV.BookoutValueTypeID, T.TimeID, BV.Value, BTPC.IsValid, B.IsAccurate
	
	FROM	inserted BV
	 	JOIN BookoutThirdPartyCategories BTPC ON BV.BookoutThirdPartyCategoryID = BTPC.BookoutThirdPartyCategoryID
	 	JOIN ThirdPartyCategories TPC ON BTPC.ThirdPartyCategoryID = TPC.ThirdPartyCategoryID
	 	JOIN Bookouts B ON BTPC.BookoutID = B.BookoutID
		JOIN AppraisalBookouts AB ON B.BookoutID = AB.BookoutID
		JOIN Appraisals A ON AB.AppraisalID = A.AppraisalID
		JOIN [FLDW]..Time_D T ON T.TypeCD = 1 AND cast(convert(varchar(10),BV.DateCreated,101) as datetime) = T.BeginDate
	
	WHERE	BV.BookoutValueTypeID = 2
GO
