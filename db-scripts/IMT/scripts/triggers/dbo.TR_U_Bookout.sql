IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TR_U_Bookouts]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].[TR_U_Bookouts]
GO

CREATE TRIGGER dbo.TR_U_Bookouts ON dbo.Bookouts
AFTER UPDATE NOT FOR REPLICATION
AS

IF UPDATE(BookoutStatusID) 


	UPDATE	F
	SET	IsAccurate 	= COALESCE(BS.IsAccurate, B.IsAccurate),
		BookoutStatusID	= B.BookoutStatusID	
	FROM	inserted I
		JOIN Bookouts B ON I.BookoutID = B.BookoutID
		JOIN BookoutStatus BS ON B.BookoutStatusID = BS.BookoutStatusID
		JOIN [FLDW]..InventoryBookout_F F ON B.BookoutID = F.BookoutID

GO

