/****** Object:  Trigger [TR_U_Inventory]    Script Date: 03/10/2014 17:28:40 ******/
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[TR_U_Inventory]'))
DROP TRIGGER [dbo].[TR_U_Inventory]
GO

USE [IMT]
GO

/****** Object:  Trigger [dbo].[TR_U_Inventory]    Script Date: 03/10/2014 17:28:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER [dbo].[TR_U_Inventory] ON [dbo].[Inventory]
AFTER UPDATE NOT FOR REPLICATION
AS
IF UPDATE(ListPrice) OR UPDATE(MileageReceived) OR UPDATE(CurrentVehicleLight) 
  OR UPDATE(PlanReminderDate) OR UPDATE(EdmundsTMV) 
  OR UPDATE(TransferPrice) OR UPDATE(TransferForRetailOnly) OR UPDATE(TransferForRetailOnlyEnabled)
  OR UPDATE(Certified) OR UPDATE(MSRP) OR UPDATE(unitCost)
	UPDATE	A
	SET	A.ListPrice 		= T.ListPrice,
		A.MileageReceived 	= T.MileageReceived,
		A.CurrentVehicleLight 	= T.CurrentVehicleLight,
		A.PlanReminderDate 	= T.PlanReminderDate,
		A.EdmundsTMV 		= T.EdmundsTMV,
		A.LotPrice		= T.LotPrice,
		A.TransferPrice 	= T.TransferPrice,
		A.TransferForRetailOnly = T.TransferForRetailOnly,
		A.Certified		= T.Certified,
		A.MSRP			= T.MSRP,
		A.UnitCost		= T.UnitCost
	FROM	inserted T
		JOIN [FLDW]..InventoryActive A ON T.InventoryID = A.InventoryID	
	
GO