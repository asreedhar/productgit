

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TR_I_DealerPreference]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].[TR_I_DealerPreference]
GO

----------------------------------------------------------------------------------------------------------------
--
--	This should be integrated into the process flow in FLADMIN rather than live as a trigger.
--	Oh well, gotta do what ya gotta do...
--
----------------------------------------------------------------------------------------------------------------

CREATE TRIGGER dbo.TR_I_DealerPreference ON dbo.DealerPreference
AFTER INSERT NOT FOR REPLICATION
AS

----------------------------------------------------------------------------------------------------------------
--	INSERT dbo.DealerPreference_Dataload w/defaults
----------------------------------------------------------------------------------------------------------------

INSERT
INTO	dbo.DealerPreference_Dataload  (BusinessUnitID)

SELECT	I.BusinessUnitID
FROM	inserted I

GO