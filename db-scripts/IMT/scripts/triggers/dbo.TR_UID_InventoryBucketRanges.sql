
------------------------------------------------------------------------------------------------
--
--	This trigger is a "failsafe" to prevent accidental deletion of the default inventory
--	bucket ranges.  Normally, this logic would be in the admin application but somehow
--	the defaults were deleted and "no bugs" were found in the current code base...
--
--	Perhaps, at a later date, this code should be removed once we are sure the admin app
--	will not delete the defaults...
--
--	...six years later, it's still here.
--	Augmenting w/FLDW DealerBucket update (7/3/2012) to cover FLADMIN updates
--	
--	Note if the trigger fires while the FLDW loader for FLDW.dbo.DealerBucket is mid-build
--	(2-3 seconds), the changes will be lost as the trigger only targets the active buffer.
--	Something to work on...this is a 99.9% solution.
--
------------------------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TR_UID_InventoryBucketRanges]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].[TR_UID_InventoryBucketRanges]
GO

CREATE TRIGGER dbo.TR_UID_InventoryBucketRanges ON InventoryBucketRanges
FOR UPDATE,INSERT, DELETE
AS
SET NOCOUNT ON 
	
IF SUSER_SNAME() = 'firstlook' 
   AND (EXISTS(SELECT 1 FROM deleted WHERE BusinessUnitID = 100150 AND InventoryBucketID <> 7)
	OR EXISTS(SELECT 1 FROM inserted WHERE BusinessUnitID = 100150 AND InventoryBucketID <> 7)) BEGIN

	RAISERROR('The user ''firstlook'' may not modify default bucket ranges', 11, 1)
	ROLLBACK TRANSACTION
END

BEGIN TRY
	
	
	BEGIN TRANSACTION
	
	CREATE TABLE #IBR (BusinessUnitID INT, InventoryBucketID INT)

	INSERT
	INTO	#IBR (BusinessUnitID, InventoryBucketID)
	SELECT	BusinessUnitID, InventoryBucketID
	FROM	DELETED
	UNION
	SELECT	BusinessUnitID, InventoryBucketID
	FROM	INSERTED					
	
	------------------------------------------------------------------------------------
	--	IF WE CHANGE THE Firstlook DEFAULT BUCKETS, WE NEED TO REBUILD COMPLETELY.
	--	THIS WILL NEVER HAPPEN VIA THE APPLICATION, ONLY HERE TO SUPPORT DEPLOYMENTS
	------------------------------------------------------------------------------------	
	
	IF EXISTS(SELECT 1 FROM #IBR WHERE BusinessUnitID = 100150) BEGIN
		
		DELETE	
		FROM	FLDW.dbo.DealerBucket 
	
		INSERT
		INTO	FLDW.dbo.DealerBucket (BusinessUnitID,InventoryBucketID,RangeID,Low,High,Lights,SortOrder,Value1,Value2,Value3,InventoryBucketRangeID)		
		SELECT	X.BusinessUnitID, X.InventoryBucketID, X.RangeID, X.Low, X.High, X.Lights, X.SortOrder, X.Value1, X.Value2, X.Value3, X.InventoryBucketRangeID
		FROM	FLDW.dbo.DealerBucket#Extract X
	END
	ELSE BEGIN
	
		------------------------------------------------------------------------------------
		--	THE CHANGE OCCURED ON A SINGLE BUSINESSUNIT + BUCKET
		------------------------------------------------------------------------------------
	
		DELETE	DB
		FROM	FLDW.dbo.DealerBucket DB 
			INNER JOIN #IBR IBR ON DB.BusinessUnitID = IBR.BusinessUnitID
						AND DB.InventoryBucketID = IBR.InventoryBucketID
						
		INSERT
		INTO	FLDW.dbo.DealerBucket (BusinessUnitID,InventoryBucketID,RangeID,Low,High,Lights,SortOrder,Value1,Value2,Value3,InventoryBucketRangeID)		
		SELECT	X.BusinessUnitID, X.InventoryBucketID, X.RangeID, X.Low, X.High, X.Lights, X.SortOrder, X.Value1, X.Value2, X.Value3, X.InventoryBucketRangeID
		FROM	FLDW.dbo.DealerBucket#Extract X
			INNER JOIN #IBR IBR ON X.BusinessUnitID = IBR.BusinessUnitID
						AND X.InventoryBucketID = IBR.InventoryBucketID
	
	END	
		
	COMMIT TRANSACTION
	
END TRY
BEGIN CATCH
	IF XACT_STATE() <> 0
		ROLLBACK TRANSACTION
					
END CATCH					


GO