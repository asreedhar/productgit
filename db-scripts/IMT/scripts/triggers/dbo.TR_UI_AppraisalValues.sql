IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TR_UI_AppraisalValues]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].[TR_UI_AppraisalValues]
GO

----------------------------------------------------------------------------------------------------------------
--	THE APPRAISAL_F's PK IS  BusinessUnitID + VehicleID
--	THIS IS MORE LIKE A Vehicle_F TABLE, STORING THE LATEST APPRAISAL INFORMATION FOR THE VEHICLE
--	MIGHT NEED TO BE RENAMED FOR CLARITY.
--
--	06/13/2014	Added AppraisalActionTypeID (left join to AppraisalActions to be safe)
----------------------------------------------------------------------------------------------------------------

CREATE TRIGGER dbo.TR_UI_AppraisalValues ON dbo.AppraisalValues
AFTER INSERT, UPDATE NOT FOR REPLICATION
AS

----------------------------------------------------------------------------------------------------------------
--	UPDATE Appraisal_F
----------------------------------------------------------------------------------------------------------------

UPDATE	F
SET	Value			= AV.Value,
	AppraisalValueID 	= AV.AppraisalValueID,
	AppraisalID		= A.AppraisalID,
	AppraisalActionTypeID	= COALESCE(AA.AppraisalActionTypeID, 0)

FROM	inserted AV
	INNER JOIN dbo.Appraisals A ON AV.AppraisalID = A.AppraisalID
	INNER JOIN FLDW.dbo.Appraisal_F F ON A.BusinessUnitID = F.BusinessUnitID AND A.VehicleID = F.VehicleID
	LEFT JOIN dbo.AppraisalActions AA ON A.AppraisalID = AA.AppraisalID  

----------------------------------------------------------------------------------------------------------------
--	INSERT Appraisal_F
----------------------------------------------------------------------------------------------------------------

IF @@ROWCOUNT = 0

	INSERT
	INTO	[FLDW]..Appraisal_F  (BusinessUnitID, AppraisalID, VehicleID, AppraisalValueID, Value, AppraisalTypeID, DateCreated, DateModified, Mileage, AppraisalActionTypeID)
	
	SELECT	A.BusinessUnitID, A.AppraisalID, A.VehicleID, AV.AppraisalValueID, AV.Value, A.AppraisalTypeID, A.DateCreated, A.DateModified, A.Mileage, COALESCE(AA.AppraisalActionTypeID, 0)
	
	FROM	inserted AV
		INNER JOIN Appraisals A ON AV.AppraisalID = A.AppraisalID
		LEFT JOIN dbo.AppraisalActions AA ON A.AppraisalID = AA.AppraisalID 

GO


