IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TR_D_Appraisals]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].[TR_D_Appraisals]
GO

----------------------------------------------------------------------------------------------------------------
--	THE APPRAISAL_F's PK IS ESSENTIALLY BusinessUnitID + VehicleID
--	
--	SOMETIMES WE DELETE AN APPRAISAL AND THEN CREATE ANOTHER WITH THE SAME
--	BUSINESS UNIT-VEHICLE; WE NEED TO UPDATE THE APPRAISAL FACT TABLE IN FLDW.
----------------------------------------------------------------------------------------------------------------

CREATE TRIGGER dbo.TR_D_Appraisals ON dbo.Appraisals
AFTER DELETE NOT FOR REPLICATION
AS

----------------------------------------------------------------------------------------------------------------
--	DELETE Appraisal_F
----------------------------------------------------------------------------------------------------------------

DELETE	F
FROM	[FLDW]..Appraisal_F F
	INNER JOIN deleted D ON F.BusinessUnitID = D.BusinessUnitID AND F.AppraisalID = D.AppraisalID

GO