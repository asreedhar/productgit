IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TR_I_DealerGroupPreference]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].[TR_I_DealerGroupPreference]
GO
CREATE TRIGGER [dbo].[TR_I_DealerGroupPreference] ON [dbo].[DealerGroupPreference]
    AFTER INSERT
AS
/*

  AAAA   AAAA   AAAA   AAAA  RRRRR   GGGG  HH  HH  ######
 AA  AA AA  AA AA  AA AA  AA RR  RR GG  GG HH  HH  ######
 AA  AA AA  AA AA  AA AA  AA RR  RR GG     HH  HH   ####
 AAAAAA AAAAAA AAAAAA AAAAAA RRRRR  GG GGG HHHHHH    ##
 AA  AA AA  AA AA  AA AA  AA RR  RR GG  GG HH  HH
 AA  AA AA  AA AA  AA AA  AA RR  RR  GGG G HH  HH    ##

*/
UPDATE  dgp
SET     CarSearchTitle = 'Group Car Search'
FROM    inserted I
        INNER JOIN [dbo].[DealerGroupPreference] dgp ON dgp.DealerGroupPreferenceID = i.DealerGroupPreferenceID
WHERE   i.CarSearchTitle IS NULL
GO



