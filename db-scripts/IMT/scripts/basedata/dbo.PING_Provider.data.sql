SET IDENTITY_INSERT IMT.dbo.PING_Provider ON
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.PING_Provider WHERE 1=1 AND [id] = 2 )	INSERT IMT.dbo.PING_Provider ([id],[name],[code],[available],[sortOrder])	VALUES (2,'Cars.com','carsDotCom',1,2)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.PING_Provider WHERE 1=1 AND [id] = 3 )	INSERT IMT.dbo.PING_Provider ([id],[name],[code],[available],[sortOrder])	VALUES (3,'Cars Direct','carsDirect',1,3)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.PING_Provider WHERE 1=1 AND [id] = 4 )	INSERT IMT.dbo.PING_Provider ([id],[name],[code],[available],[sortOrder])	VALUES (4,'Autotrader.com','autotrader',1,1)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.PING_Provider WHERE 1=1 AND [id] = 5 )	INSERT IMT.dbo.PING_Provider ([id],[name],[code],[available],[sortOrder])	VALUES (5,'Carsoup.com','carsoup',1,4)
SET IDENTITY_INSERT IMT.dbo.PING_Provider OFF
