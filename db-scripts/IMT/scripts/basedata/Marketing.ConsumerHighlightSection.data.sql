SET IDENTITY_INSERT IMT.Marketing.ConsumerHighlightSection ON
IF NOT EXISTS (SELECT 1 FROM IMT.Marketing.ConsumerHighlightSection WHERE 1=1 AND [ConsumerHighlightSectionId] = 1 )	INSERT IMT.Marketing.ConsumerHighlightSection ([ConsumerHighlightSectionId],[Name],[DefaultRank])	VALUES (1,'JDPower.com Ratings',1)
IF NOT EXISTS (SELECT 1 FROM IMT.Marketing.ConsumerHighlightSection WHERE 1=1 AND [ConsumerHighlightSectionId] = 2 )	INSERT IMT.Marketing.ConsumerHighlightSection ([ConsumerHighlightSectionId],[Name],[DefaultRank])	VALUES (2,'Expert Reviews',2)
IF NOT EXISTS (SELECT 1 FROM IMT.Marketing.ConsumerHighlightSection WHERE 1=1 AND [ConsumerHighlightSectionId] = 3 )	INSERT IMT.Marketing.ConsumerHighlightSection ([ConsumerHighlightSectionId],[Name],[DefaultRank])	VALUES (3,'Vehicle History Report Highlights',3)
IF NOT EXISTS (SELECT 1 FROM IMT.Marketing.ConsumerHighlightSection WHERE 1=1 AND [ConsumerHighlightSectionId] = 4 )	INSERT IMT.Marketing.ConsumerHighlightSection ([ConsumerHighlightSectionId],[Name],[DefaultRank])	VALUES (4,'Additional Highlights',4)
SET IDENTITY_INSERT IMT.Marketing.ConsumerHighlightSection OFF
