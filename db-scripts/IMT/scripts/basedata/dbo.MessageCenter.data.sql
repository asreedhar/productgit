IF NOT EXISTS (SELECT 1 FROM IMT.dbo.MessageCenter WHERE 1=1)	INSERT IMT.dbo.MessageCenter ([Ordering],[Title],[Body],[HasPriority])	VALUES (5,'<h3 style="display:block;color: #cd1f26;border-bottom:1px dashed #CCC;margin-right:0px;"><strong><span style="font-size:14px; color:#FDD017;">NEW!</span> PINGII MARKET PRICING ANALYZER</strong></h3>','<p style="padding:2px 0 2px 0;font-size:14px;font-weight: bold;">Rank higher in consumer Internet searches through optimal market pricing</p><span style="font-size:12"><ul><li>Real-time market pricing for every vehicle in your inventory</li><li>Side-by-side comparisons of competitors'' pricing for similar vehicles in your market</li><li>Compare vs. vehicles with identical equipment</li><li>Know where you can capture profit and margin</li><li>Daily Over-Pricing Risk Alerts</li></ul></span>',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.MessageCenter WHERE 1=1)	INSERT IMT.dbo.MessageCenter ([Ordering],[Title],[Body],[HasPriority])	VALUES (7,'<h3 style="display:block;color:#333;border-bottom:1px dashed #CCC;margin-right:0px;"> <strong> Upgrade Adobe Reader to Adobe Reader 8</strong> </h3>','For the <b><i>BEST</i></b> experience using The EDGE, please upgrade your Adobe Reader to <a href="http://download.adobe.com/pub/adobe/reader/win/8.x/8.0/enu/AdbeRdr80_en_US.exe">Adobe Reader 8</a> (click <a href="http://download.adobe.com/pub/adobe/reader/win/8.x/8.0/enu/AdbeRdr80_en_US.exe">here</a> to download).  <br>  This upgrade will resolve the issues with viewing and printing the Customer Appraisal Offer Form.',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.MessageCenter WHERE 1=1)	INSERT IMT.dbo.MessageCenter ([Ordering],[Title],[Body],[HasPriority])	VALUES (3,'<h3 style="display:block;color: #cd1f26;border-bottom:1px dashed #CCC;margin-right:0px;">
<strong><span style="font-size:14px; color:#FDD017;">NEW!</span> JD POWER''S POWER MARKET APPRAISER</strong></h3>','<p style="padding:2px 0 2px 0;font-size:14px;font-weight: bold;">Close more appraisals with JD Power''s Power Market Appraiser</p>

<div class="releaseNotes">
<font color="#000">Power Market Appraiser allows you to get an instant, one-click "market" appraisal to know what other dealers in your market are selling similar vehicles for, their profit and inventory turns. <a href="javascript:void(0)" onclick="document.getElementById(''JDPower'').style.display = ''block'';">Click here</a> for more details.</font><br />
<div id="JDPower" style="width: 270px; height: 120px; display: none; background: #eee; border:solid 1px black; padding:5px;" >     
<div>
<a href="#"  class="toggle" onclick="document.getElementById(''JDPower'').style.display = ''none'';">Close</a><br />
<UL style="font-size:10px;">
	<li style="list-style-type:square;">To be eligible you need to be already enrolled or enroll in JD Power''s Power Information Network (PIN). Enrollment involves no additional fees or costs.</li>
	<li style="list-style-type:square;">Please contact the First Look Help Desk at 1-800-730-5665 to learn more.</li>
	</ul>
</div>
</div>
</div>',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.MessageCenter WHERE 1=1)	INSERT IMT.dbo.MessageCenter ([Ordering],[Title],[Body],[HasPriority])	VALUES (2,'<div class="title" style="border:0;margin-bottom:0;padding-bottom:0;"><p><h3 style="display:block;color: #cd1f26;border-bottom:1px dashed #CCC;margin-right:0px;"><strong><span style="font-size:14px; color:#FDD017;">JUST RELEASED!</span><br/>PINGII Enhancements with DMS Writeback</strong></h3></div>','<div class="title" style="border:0;margin-top:0;padding-top:0;">  <span style="font-size:12px;">  <ul style="border-top:0;">  <li style="list-style:disc;">Improved Appraisal Calculator in PINGII (access from Trade Analyzer)</li>  <li style="list-style:disc;">Improved 360&deg; Pricing with QUICK access to potential profit</li>  <li style="list-style:disc;">One-click access to Auction Data including NAAA and MMR</li>  <li style="list-style:disc;">Access more details on Market Listings (VIN, Stock#, Notes)</li>  <li style="list-style:disc;">Automatically matches equipment when the market has sufficient listings</li>  <li style="list-style:disc;">Add Pricing Notes to your vehicle and print reports with notes</li>  <li style="list-style:disc;">Adjust your Internet Price by Rank</li>  <li style="list-style:disc;">Easier access to PINGII from the Inventory Pricing Analyzer: The entire vehicle is clickable</li>  <li style="list-style:disc;">Writeback your price changes to ADP </li>  </ul></span>  </p>  <p style="margin-top:4px;"><strong>Sign Up for Over and Under Pricing Alerts <a href="javascript:openMemberProfileWindow();">Here</a></strong>  - Override pricing alerts if vehicle is priced right.</p>  </div>',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.MessageCenter WHERE 1=1)	INSERT IMT.dbo.MessageCenter ([Ordering],[Title],[Body],[HasPriority])	VALUES (1,'<h3 style="display:block;color: #cd1f26;border-bottom:1px dashed #CCC;margin-right:0px;">
<strong>Available For Select Markets, Used Vehicle Locator (UVL) in the First Look Search Engine:</strong></h3>','<p style="padding:2px 0 2px 0;font-size:12px;">AutoSales UVL helps you get the Right Cars when you need them - 
<A target="_blank" href="http://uvl.autosales.com/">Click Here</A> to learn more about UVL.&nbsp;</p>
<div class="releaseNotes">
<font color="#000">
Available Markets Include:  California, Florida, Illinois / Indiana & North Carolina 
<br/>
Contact the <B><I>First Look Help Desk</I></B> to add 
<B>UVL</B> <A 
href="mailto:helpdesk@firstlook.biz?subject=Add%20UVL%20to%20First%20Look%20Search%20Engine">Click 
here to email us</A> or call 
<STRONG>1-800-730-5665</STRONG>
</font></div>',1)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.MessageCenter WHERE 1=1)	INSERT IMT.dbo.MessageCenter ([Ordering],[Title],[Body],[HasPriority])	VALUES (6,'<h3 style="display:block;color:#333;border-bottom:1px dashed #CCC;margin-right:0px;">
		<strong>  On August 15, 2007, an updated version of The EDGE was released!</strong>
		</h3>','<div class="releaseNotes"><font color="#2B3856">
	This release enhances the capabilities of the Appraisal Center and the Inventory Management Plan.</font><br/><br>
	<strong>Appraisal Center Enhancements</strong><br />
		<UL>
		<li>Automatically see <a href="javascript:void(0)" onclick="window.open(''http://images.firstlook.biz/kbbAnnouncement.pdf'')">Kelley''s Consumer Trade-In value</a> from the web on every appraisal and present your customers with a Kelley branded, professional print page.  Call the First Look Help Desk to upgrade.</li>
		<li>Galves Customers - See both the Galves <i>Market Ready</i> and <i>Trade-In</i> values when appraising your customer''s trade-in.</li>
		</ul>
	<br>
	<strong>Inventory Management Plan Enhancements</strong><br/>
		<ul>
		<li><a href="javascript:void(0)" onclick="window.open(''http://images.firstlook.biz/quickadsannouncement.pdf'')">Quick Advertising:</a>Quickly and easily create advertising plans for any and all inventory.</li>
		<li><a href="javascript:void(0)" onclick="window.open(''http://images.firstlook.biz/inventoryphotosannouncement.pdf'')">Inventory Photos:</a>Upload inventory photos from your photo vendor or dealership camera and send photos and inventory to Internet sites like AutoTrader and Cars.com.  Photos also display in the First Look Search Engine and Aged Inventory Exchange.</li>
		</ul>
	<br>
	<strong>eStock Card Enhancements</strong><br />
	<ul>
		<li>The eStock Card now provides one-click access to a vehicle''s inventory planning history</li>
		<li>Access PING directly from the eStock Card</li>
	</ul>
</div>',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.MessageCenter WHERE 1=1)	INSERT IMT.dbo.MessageCenter ([Ordering],[Title],[Body],[HasPriority])	VALUES (4,'<h3 style="display:block;color: #cd1f26;border-bottom:1px dashed #CCC;margin-right:0px;">
<strong><span style="font-size:14px; color:#FDD017;">NEW!</span> FIRST LOOK Customer Referral Program</strong></h3>','<div class="releaseNotes">
<font color="#000"><strong>Earn $1,000.00 OFF</strong> your bill when you refer us to a client that signs up and is launched on the First Look program. <a href="javascript:void(0)" onclick="document.getElementById(''Referral'').style.display = ''block'';">Click here</a> for more details.</font><br />
<div id="Referral" style="width: 270px; height: 260px; display: none; background: #eee; border:solid 1px black; padding:5px;" >
<div>
<a href="#"  class="toggle" onclick="document.getElementById(''Referral'').style.display = ''none'';">Close</a><br />
<div class="releaseNotes" style="font-size:11px;color:#000;">
<p style="margin-top:5px;padding-top:3px;border-top:1px solid #999;">If you have benefited from using the First Look program, wouldn''t you like to tell a friend? Our Referral Program lets you help your friends while helping yourself. Every dealer you refer to First Look that signs up for and is launched on the full First Look program yields you <strong>a $1,000.00 credit off your monthly bill</strong>&mdash;<em>or a $500.00 credit off your monthly bill if they purchase PING II only.</em></p>
<p style="padding:5px 0;margin:0;">It''s easy&mdash; just email your referral to <a href="mailto:sales@firstlook.biz?subject=Referral">sales@firstlook.biz</a>.<br/>
<p style="color:#666;padding-top:0px;font-size:10px;line-height:11px;">This offer only applies to new business and does not apply to dealerships that a First Look representative has already presented to. To qualify the dealership must sign a contract with us within 60 days of the initial presentation by First Look. This offer applies to initlal presentations scheduled and completed before May 31, 2008.</p>
</div>
</div>
</div>
</div>',0)
