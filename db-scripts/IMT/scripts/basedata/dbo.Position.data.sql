SET IDENTITY_INSERT IMT.dbo.Position ON
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Position WHERE 1=1 AND [PositionID] = 1 )	INSERT IMT.dbo.Position ([PositionID],[Description])	VALUES (1,'Salesperson')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Position WHERE 1=1 AND [PositionID] = 2 )	INSERT IMT.dbo.Position ([PositionID],[Description])	VALUES (2,'Appraiser')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Position WHERE 1=1 AND [PositionID] = 3 )	INSERT IMT.dbo.Position ([PositionID],[Description])	VALUES (3,'Buyer')
SET IDENTITY_INSERT IMT.dbo.Position OFF
