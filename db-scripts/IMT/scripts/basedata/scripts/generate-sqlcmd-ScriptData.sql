
DECLARE @SqlCmd VARCHAR(MAX) = 'SQLCMD -E -S PRODDB02SQL -h-1 -y 0 -Q "EXEC Utility.dbo.ScriptData @schema = ''<A>'', @table = ''<B>'', @Where = '''', @DB = ''<C>'', @IfNotExists = 1, @Update = 0, @IDInsert = 1, @UseTransaction = 0" -o <D>';

WITH Parms AS (
SELECT	A	= OBJECT_SCHEMA_NAME(o.object_id),
	B	= OBJECT_NAME(o.object_id),
	C	= DB_NAME(),
	D	= 'E:\Workspace\17.1-dbo\db-scripts\IMT\scripts\basedata\' +OBJECT_SCHEMA_NAME(o.object_id) + '.' + OBJECT_NAME(o.object_id) + '.data.sql',
	rows	= rows
FROM	sys.objects o
	INNER JOIN (	SELECT	o.object_id, rows = AVG(rows)
				FROM    sys.indexes i
					INNER JOIN sys.objects o ON i.[object_id] = o.[object_id]
					INNER JOIN sys.filegroups f ON i.data_space_id = f.data_space_id
					INNER JOIN sys.partitions P ON o.object_id = P.object_id AND I.index_id = P.index_id AND P.partition_number = P.partition_number
				WHERE	o.type = 'U' -- User Created Tables
				GROUP
				BY	o.object_id
			) rc ON o.object_id = rc.object_id
WHERE	type = 'U'
	AND rows BETWEEN 1 AND 100
)
SELECT	REPLACE(REPLACE(REPLACE(REPLACE(@SqlCmd, '<A>', A), '<B>', B), '<C>', C), '<D>', D), rows, Utility.String.GetFileName(D)
FROM	Parms
ORDER
BY	rows asc


