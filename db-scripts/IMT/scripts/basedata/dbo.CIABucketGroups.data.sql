SET IDENTITY_INSERT IMT.dbo.CIABucketGroups ON
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.CIABucketGroups WHERE 1=1 AND [CIABucketGroupID] = 1 )	INSERT IMT.dbo.CIABucketGroups ([CIABucketGroupID],[Description])	VALUES (1,'Year')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.CIABucketGroups WHERE 1=1 AND [CIABucketGroupID] = 2 )	INSERT IMT.dbo.CIABucketGroups ([CIABucketGroupID],[Description])	VALUES (2,'UnitCost')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.CIABucketGroups WHERE 1=1 AND [CIABucketGroupID] = 3 )	INSERT IMT.dbo.CIABucketGroups ([CIABucketGroupID],[Description])	VALUES (3,'Age in Years')
SET IDENTITY_INSERT IMT.dbo.CIABucketGroups OFF
