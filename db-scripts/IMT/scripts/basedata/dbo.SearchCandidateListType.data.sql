SET IDENTITY_INSERT IMT.dbo.SearchCandidateListType ON
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.SearchCandidateListType WHERE 1=1 AND [SearchCandidateListTypeID] = 1 )	INSERT IMT.dbo.SearchCandidateListType ([SearchCandidateListTypeID],[Description])	VALUES (1,'Permanent Hot List')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.SearchCandidateListType WHERE 1=1 AND [SearchCandidateListTypeID] = 2 )	INSERT IMT.dbo.SearchCandidateListType ([SearchCandidateListTypeID],[Description])	VALUES (2,'Current Hot List')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.SearchCandidateListType WHERE 1=1 AND [SearchCandidateListTypeID] = 3 )	INSERT IMT.dbo.SearchCandidateListType ([SearchCandidateListTypeID],[Description])	VALUES (3,'Temporary List')
SET IDENTITY_INSERT IMT.dbo.SearchCandidateListType OFF
