SET IDENTITY_INSERT IMT.Distribution.VehicleEntityType ON

--IF NOT EXISTS (SELECT 1 FROM IMT.Distribution.VehicleEntityType WHERE 1=1 AND [VehicleEntityTypeID] = 0 )--	INSERT IMT.Distribution.VehicleEntityType ([VehicleEntityTypeID],[Name])--	VALUES (0,'Not Defined')
IF NOT EXISTS (SELECT 1 FROM IMT.Distribution.VehicleEntityType WHERE 1=1 AND [VehicleEntityTypeID] = 1 )	INSERT IMT.Distribution.VehicleEntityType ([VehicleEntityTypeID],[Name])	VALUES (1,'Inventory')
IF NOT EXISTS (SELECT 1 FROM IMT.Distribution.VehicleEntityType WHERE 1=1 AND [VehicleEntityTypeID] = 2 )	INSERT IMT.Distribution.VehicleEntityType ([VehicleEntityTypeID],[Name])	VALUES (2,'Appraisal')
SET IDENTITY_INSERT IMT.Distribution.VehicleEntityType OFF
