SET IDENTITY_INSERT IMT.dbo.Franchise ON
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 1 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (1,'ACURA',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 2 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (2,'ALFA ROMEO',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 3 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (3,'AM GENERAL',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 4 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (4,'ASTON MARTIN',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 5 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (5,'AUDI',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 6 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (6,'BENTLEY',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 7 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (7,'BMW',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 8 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (8,'BUICK',1)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 9 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (9,'CADILLAC',1)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 10 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (10,'CHEVROLET',1)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 11 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (11,'CHRYSLER',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 12 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (12,'DAEWOO',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 13 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (13,'DAIHATSU',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 14 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (14,'DODGE',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 15 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (15,'EAGLE',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 16 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (16,'ELDORADO',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 17 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (17,'FERRARI',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 18 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (18,'FORD',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 19 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (19,'GEO',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 20 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (20,'GMC',1)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 21 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (21,'HONDA',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 22 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (22,'HUMMER',1)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 23 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (23,'HYUNDAI',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 24 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (24,'INFINITI',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 25 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (25,'ISUZU',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 26 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (26,'JAGUAR',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 27 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (27,'JEEP',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 28 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (28,'KIA',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 29 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (29,'LAMBORGHINI',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 30 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (30,'LAND ROVER',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 31 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (31,'LEXUS',2)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 32 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (32,'LINCOLN',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 33 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (33,'LOTUS',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 34 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (34,'MASERATI',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 35 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (35,'MAZDA',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 36 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (36,'MERCEDES-BENZ',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 37 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (37,'MERCURY',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 38 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (38,'MINI',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 39 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (39,'MITSUBISHI',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 40 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (40,'NISSAN',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 41 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (41,'OLDSMOBILE',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 42 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (42,'PANOZ',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 43 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (43,'PEUGEOT',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 44 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (44,'PLYMOUTH',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 45 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (45,'PONTIAC',1)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 46 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (46,'PORSCHE',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 47 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (47,'ROLLS ROYCE',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 48 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (48,'SAAB',1)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 49 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (49,'SATURN',1)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 50 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (50,'SCION',2)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 51 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (51,'SUBARU',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 52 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (52,'SUZUKI',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 53 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (53,'TOYOTA',2)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 54 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (54,'VOLKSWAGEN',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Franchise WHERE 1=1 AND [FranchiseID] = 55 )	INSERT IMT.dbo.Franchise ([FranchiseID],[FranchiseDescription],[ManufacturerID])	VALUES (55,'VOLVO',0)
SET IDENTITY_INSERT IMT.dbo.Franchise OFF
