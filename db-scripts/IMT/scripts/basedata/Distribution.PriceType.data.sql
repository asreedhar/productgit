SET IDENTITY_INSERT IMT.Distribution.PriceType ON
IF NOT EXISTS (SELECT 1 FROM IMT.Distribution.PriceType WHERE 1=1 AND [PriceTypeID] = 0 )	INSERT IMT.Distribution.PriceType ([PriceTypeID],[Name])	VALUES (0,'NotDefined')
IF NOT EXISTS (SELECT 1 FROM IMT.Distribution.PriceType WHERE 1=1 AND [PriceTypeID] = 1 )	INSERT IMT.Distribution.PriceType ([PriceTypeID],[Name])	VALUES (1,'Lot')
IF NOT EXISTS (SELECT 1 FROM IMT.Distribution.PriceType WHERE 1=1 AND [PriceTypeID] = 2 )	INSERT IMT.Distribution.PriceType ([PriceTypeID],[Name])	VALUES (2,'Internet')
IF NOT EXISTS (SELECT 1 FROM IMT.Distribution.PriceType WHERE 1=1 AND [PriceTypeID] = 3 )	INSERT IMT.Distribution.PriceType ([PriceTypeID],[Name])	VALUES (3,'Appraisal')
SET IDENTITY_INSERT IMT.Distribution.PriceType OFF
