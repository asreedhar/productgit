SET IDENTITY_INSERT IMT.dbo.BookoutSources ON
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.BookoutSources WHERE 1=1 AND [BookoutSourceID] = 1 )	INSERT IMT.dbo.BookoutSources ([BookoutSourceID],[Description])	VALUES (1,'Appraisal')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.BookoutSources WHERE 1=1 AND [BookoutSourceID] = 2 )	INSERT IMT.dbo.BookoutSources ([BookoutSourceID],[Description])	VALUES (2,'Bookout Processor')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.BookoutSources WHERE 1=1 AND [BookoutSourceID] = 3 )	INSERT IMT.dbo.BookoutSources ([BookoutSourceID],[Description])	VALUES (3,'Vehicle Detail Page')
SET IDENTITY_INSERT IMT.dbo.BookoutSources OFF
