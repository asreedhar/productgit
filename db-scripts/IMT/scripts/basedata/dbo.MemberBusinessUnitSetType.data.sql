SET IDENTITY_INSERT IMT.dbo.MemberBusinessUnitSetType ON
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.MemberBusinessUnitSetType WHERE 1=1 AND [MemberBusinessUnitSetTypeID] = 1 )	INSERT IMT.dbo.MemberBusinessUnitSetType ([MemberBusinessUnitSetTypeID],[Name])	VALUES (1,'Command Center Dealer Group Selection')
SET IDENTITY_INSERT IMT.dbo.MemberBusinessUnitSetType OFF
