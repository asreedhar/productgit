SET IDENTITY_INSERT IMT.dbo.CIABucketRuleTypes ON
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.CIABucketRuleTypes WHERE 1=1 AND [CIABucketRuleTypeID] = 1 )	INSERT IMT.dbo.CIABucketRuleTypes ([CIABucketRuleTypeID],[Description],[ColumnName])	VALUES (1,'Year','VehicleYear')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.CIABucketRuleTypes WHERE 1=1 AND [CIABucketRuleTypeID] = 2 )	INSERT IMT.dbo.CIABucketRuleTypes ([CIABucketRuleTypeID],[Description],[ColumnName])	VALUES (2,'Unit Cost Range','UnitCost')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.CIABucketRuleTypes WHERE 1=1 AND [CIABucketRuleTypeID] = 3 )	INSERT IMT.dbo.CIABucketRuleTypes ([CIABucketRuleTypeID],[Description],[ColumnName])	VALUES (3,'Age in Years','n/a')
SET IDENTITY_INSERT IMT.dbo.CIABucketRuleTypes OFF
