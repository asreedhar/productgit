IF NOT EXISTS (SELECT 1 FROM IMT.Carfax.ResponseCode WHERE 1=1 AND [ResponseCode] = '500' )	INSERT IMT.Carfax.ResponseCode ([ResponseCode],[Description],[IsError],[IsTryAgain])	VALUES ('500','A CARFAX Report is available in the Dealer''s Inventory Manager.  
			CARFAX found at least one record for the specified VIN and the vehicle 
			is eligible to be listed in CARFAX Hot Listings.  The vehicle does not
			have a title brand or odometer rollback.',0,0)
IF NOT EXISTS (SELECT 1 FROM IMT.Carfax.ResponseCode WHERE 1=1 AND [ResponseCode] = '501' )	INSERT IMT.Carfax.ResponseCode ([ResponseCode],[Description],[IsError],[IsTryAgain])	VALUES ('501','A CARFAX Report is available in the Dealer''s Inventory Manager.  
			CARFAX found at least one record for the specified VIN and the vehicle 
			is not eligible to be listed in CARFAX Hot Listings because the vehicle 
			has a title brand or odometer rollback.',0,0)
IF NOT EXISTS (SELECT 1 FROM IMT.Carfax.ResponseCode WHERE 1=1 AND [ResponseCode] = '600' )	INSERT IMT.Carfax.ResponseCode ([ResponseCode],[Description],[IsError],[IsTryAgain])	VALUES ('600','VIN Does not exist in Inventory Manager OR No data available: The VIN
			does not exist in the Dealer''s Inventory Manager OR CARFAX did
			not find any records from the specified VIN: however, the VIN otherwise
			appears valid.',1,0)
IF NOT EXISTS (SELECT 1 FROM IMT.Carfax.ResponseCode WHERE 1=1 AND [ResponseCode] = '800' )	INSERT IMT.Carfax.ResponseCode ([ResponseCode],[Description],[IsError],[IsTryAgain])	VALUES ('800','Invalid VIN: The Server sends this message to indicate that the VIN
			supplied by the user was invalid for some reason.',1,0)
IF NOT EXISTS (SELECT 1 FROM IMT.Carfax.ResponseCode WHERE 1=1 AND [ResponseCode] = '900' )	INSERT IMT.Carfax.ResponseCode ([ResponseCode],[Description],[IsError],[IsTryAgain])	VALUES ('900','Service Unavailable: The Server sends this message to indicate that the
			CARFAX Vehicle History Service is not available to process vehicle
			queries for some reason.',1,1)
IF NOT EXISTS (SELECT 1 FROM IMT.Carfax.ResponseCode WHERE 1=1 AND [ResponseCode] = '901' )	INSERT IMT.Carfax.ResponseCode ([ResponseCode],[Description],[IsError],[IsTryAgain])	VALUES ('901','Transaction Error: The Server sends this message to indicate there was
			an error in the last message it received.  The Client can choose to resend
			the message packet.',1,1)
IF NOT EXISTS (SELECT 1 FROM IMT.Carfax.ResponseCode WHERE 1=1 AND [ResponseCode] = '903' )	INSERT IMT.Carfax.ResponseCode ([ResponseCode],[Description],[IsError],[IsTryAgain])	VALUES ('903','Account Status:  The Server sends this message to indicate that the )
			CARFAX Vehicle History Service has denied acces to the supplied User
			ID for non-technical reasons.  For example, the user may have cancelled
			the account or there may be billing issues requiring CARFAX to put the
			account on hold.',1,0)
IF NOT EXISTS (SELECT 1 FROM IMT.Carfax.ResponseCode WHERE 1=1 AND [ResponseCode] = '904' )	INSERT IMT.Carfax.ResponseCode ([ResponseCode],[Description],[IsError],[IsTryAgain])	VALUES ('904','Security Violation:  Used when the user name cannot be authenticated.',1,0)
IF NOT EXISTS (SELECT 1 FROM IMT.Carfax.ResponseCode WHERE 1=1 AND [ResponseCode] = '905' )	INSERT IMT.Carfax.ResponseCode ([ResponseCode],[Description],[IsError],[IsTryAgain])	VALUES ('905','Security Violation:  Used when the password cannot be authenticated.',1,0)
IF NOT EXISTS (SELECT 1 FROM IMT.Carfax.ResponseCode WHERE 1=1 AND [ResponseCode] = '906' )	INSERT IMT.Carfax.ResponseCode ([ResponseCode],[Description],[IsError],[IsTryAgain])	VALUES ('906','No Code Supplied:  Used when a code is not included as a part of the
			socket request to CARFAX or whn the CODE supplied is not valid.',1,0)
