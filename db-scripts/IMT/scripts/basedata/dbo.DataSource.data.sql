SET IDENTITY_INSERT IMT.dbo.DataSource ON
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.DataSource WHERE 1=1 AND [DataSourceID] = 1 )	INSERT IMT.dbo.DataSource ([DataSourceID],[DataSource])	VALUES (1,'MakeModelGrouping')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.DataSource WHERE 1=1 AND [DataSourceID] = 2 )	INSERT IMT.dbo.DataSource ([DataSourceID],[DataSource])	VALUES (2,'OldVehicle')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.DataSource WHERE 1=1 AND [DataSourceID] = 3 )	INSERT IMT.dbo.DataSource ([DataSourceID],[DataSource])	VALUES (3,'VehicleOptions')
SET IDENTITY_INSERT IMT.dbo.DataSource OFF
