SET IDENTITY_INSERT IMT.dbo.CIASummaryStatus ON
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.CIASummaryStatus WHERE 1=1 AND [CIASummaryStatusID] = 1 )	INSERT IMT.dbo.CIASummaryStatus ([CIASummaryStatusID],[Description])	VALUES (1,'Pending')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.CIASummaryStatus WHERE 1=1 AND [CIASummaryStatusID] = 2 )	INSERT IMT.dbo.CIASummaryStatus ([CIASummaryStatusID],[Description])	VALUES (2,'Current')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.CIASummaryStatus WHERE 1=1 AND [CIASummaryStatusID] = 3 )	INSERT IMT.dbo.CIASummaryStatus ([CIASummaryStatusID],[Description])	VALUES (3,'Prior')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.CIASummaryStatus WHERE 1=1 AND [CIASummaryStatusID] = 4 )	INSERT IMT.dbo.CIASummaryStatus ([CIASummaryStatusID],[Description])	VALUES (4,'Inactive')
SET IDENTITY_INSERT IMT.dbo.CIASummaryStatus OFF
