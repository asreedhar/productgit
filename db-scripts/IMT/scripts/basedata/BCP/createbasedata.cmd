for /F "eol=# tokens= 1,2 delims=: " %%i in (manifest.txt) do bcp IMT.%%i out %%j -n -SPRODDB01SQL -T
echo --- Queries ---
bcp "SELECT * FROM IMT.dbo.Member M WHERE Login IN ('admin', 'CarfaxSystemUserAdmin')" queryout dbo.Member.data -n -SPRODDB01SQL -T
bcp "SELECT * FROM IMT.dbo.BusinessUnit M WHERE BusinessUnit = 'Firstlook' AND BusinessUnitTypeID = 5" queryout dbo.BusinessUnit.data -n -SPRODDB01SQL -T