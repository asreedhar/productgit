SET IDENTITY_INSERT IMT.dbo.InternetAdvertisementBuilderOptionProvider ON
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.InternetAdvertisementBuilderOptionProvider WHERE 1=1 AND [Id] = 1 )	INSERT IMT.dbo.InternetAdvertisementBuilderOptionProvider ([Id],[Description])	VALUES (1,'Primary GuideBook Options')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.InternetAdvertisementBuilderOptionProvider WHERE 1=1 AND [Id] = 2 )	INSERT IMT.dbo.InternetAdvertisementBuilderOptionProvider ([Id],[Description])	VALUES (2,'Secondary GuideBook Options')
SET IDENTITY_INSERT IMT.dbo.InternetAdvertisementBuilderOptionProvider OFF
