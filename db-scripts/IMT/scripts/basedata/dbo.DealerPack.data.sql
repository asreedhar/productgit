SET IDENTITY_INSERT IMT.dbo.DealerPack ON
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.DealerPack WHERE 1=1 AND [DealerPackID] = 1 )	INSERT IMT.dbo.DealerPack ([DealerPackID],[BusinessUnitID],[PackTypeCD],[PackAmount],[PackJavaClass])	VALUES (1,100148,1,400.00,NULL)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.DealerPack WHERE 1=1 AND [DealerPackID] = 5 )	INSERT IMT.dbo.DealerPack ([DealerPackID],[BusinessUnitID],[PackTypeCD],[PackAmount],[PackJavaClass])	VALUES (5,100239,1,350.00,NULL)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.DealerPack WHERE 1=1 AND [DealerPackID] = 6 )	INSERT IMT.dbo.DealerPack ([DealerPackID],[BusinessUnitID],[PackTypeCD],[PackAmount],[PackJavaClass])	VALUES (6,100246,1,450.00,NULL)
SET IDENTITY_INSERT IMT.dbo.DealerPack OFF
