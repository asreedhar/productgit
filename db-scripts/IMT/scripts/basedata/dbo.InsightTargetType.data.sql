IF NOT EXISTS (SELECT 1 FROM IMT.dbo.InsightTargetType WHERE 1=1 AND [InsightTargetTypeID] = 1 )	INSERT IMT.dbo.InsightTargetType ([InsightTargetTypeID],[Name])	VALUES (1,'N/A')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.InsightTargetType WHERE 1=1 AND [InsightTargetTypeID] = 2 )	INSERT IMT.dbo.InsightTargetType ([InsightTargetTypeID],[Name])	VALUES (2,'Range')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.InsightTargetType WHERE 1=1 AND [InsightTargetTypeID] = 3 )	INSERT IMT.dbo.InsightTargetType ([InsightTargetTypeID],[Name])	VALUES (3,'Value')
