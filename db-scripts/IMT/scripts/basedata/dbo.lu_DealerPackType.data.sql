SET IDENTITY_INSERT IMT.dbo.lu_DealerPackType ON
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.lu_DealerPackType WHERE 1=1 AND [PackTypeCD] = 1 )	INSERT IMT.dbo.lu_DealerPackType ([PackTypeCD],[PackTypeDESC])	VALUES (1,'Dynamic')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.lu_DealerPackType WHERE 1=1 AND [PackTypeCD] = 2 )	INSERT IMT.dbo.lu_DealerPackType ([PackTypeCD],[PackTypeDESC])	VALUES (2,'Flat/Fixed')
SET IDENTITY_INSERT IMT.dbo.lu_DealerPackType OFF
