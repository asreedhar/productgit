SET IDENTITY_INSERT IMT.Distribution.MessageType ON
IF NOT EXISTS (SELECT 1 FROM IMT.Distribution.MessageType WHERE 1=1 AND [MessageTypeID] = 0 )	INSERT IMT.Distribution.MessageType ([MessageTypeID],[Name])	VALUES (0,'Price')
IF NOT EXISTS (SELECT 1 FROM IMT.Distribution.MessageType WHERE 1=1 AND [MessageTypeID] = 1 )	INSERT IMT.Distribution.MessageType ([MessageTypeID],[Name])	VALUES (1,'Description')
IF NOT EXISTS (SELECT 1 FROM IMT.Distribution.MessageType WHERE 1=1 AND [MessageTypeID] = 2 )	INSERT IMT.Distribution.MessageType ([MessageTypeID],[Name])	VALUES (2,'Mileage')
SET IDENTITY_INSERT IMT.Distribution.MessageType OFF
