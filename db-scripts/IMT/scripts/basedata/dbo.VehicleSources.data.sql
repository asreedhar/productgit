SET IDENTITY_INSERT IMT.dbo.VehicleSources ON
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.VehicleSources WHERE 1=1 AND [VehicleSourceID] = 1 )	INSERT IMT.dbo.VehicleSources ([VehicleSourceID],[Description])	VALUES (1,'DMS')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.VehicleSources WHERE 1=1 AND [VehicleSourceID] = 2 )	INSERT IMT.dbo.VehicleSources ([VehicleSourceID],[Description])	VALUES (2,'Appraisal')
--IF NOT EXISTS (SELECT 1 FROM IMT.dbo.VehicleSources WHERE 1=1 AND [VehicleSourceID] = 3 )--	INSERT IMT.dbo.VehicleSources ([VehicleSourceID],[Description])--	VALUES (3,'In-Transit Inventory')
SET IDENTITY_INSERT IMT.dbo.VehicleSources OFF
