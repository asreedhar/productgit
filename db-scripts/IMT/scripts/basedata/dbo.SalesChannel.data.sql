SET IDENTITY_INSERT IMT.dbo.SalesChannel ON
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.SalesChannel WHERE 1=1 AND [SalesChannelID] = 1 )	INSERT IMT.dbo.SalesChannel ([SalesChannelID],[Name])	VALUES (1,'FirstLook Sales Team')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.SalesChannel WHERE 1=1 AND [SalesChannelID] = 2 )	INSERT IMT.dbo.SalesChannel ([SalesChannelID],[Name])	VALUES (2,'GMAC Sales Team')
SET IDENTITY_INSERT IMT.dbo.SalesChannel OFF
