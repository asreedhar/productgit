SET IDENTITY_INSERT IMT.dbo.HomePage ON
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.HomePage WHERE 1=1 AND [HomePageID] = 1 )	INSERT IMT.dbo.HomePage ([HomePageID],[Name])	VALUES (1,'Multi-Dealer Access Page (bubble page)')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.HomePage WHERE 1=1 AND [HomePageID] = 2 )	INSERT IMT.dbo.HomePage ([HomePageID],[Name])	VALUES (2,'Performance Management Center (PMC)')
SET IDENTITY_INSERT IMT.dbo.HomePage OFF
