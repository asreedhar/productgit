SET IDENTITY_INSERT IMT.dbo.PurchasingCenterChannels ON
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.PurchasingCenterChannels WHERE 1=1 AND [ChannelID] = 1 )	INSERT IMT.dbo.PurchasingCenterChannels ([ChannelID],[ChannelName],[HasAccessGroups])	VALUES (1,'On-Line Auction',1)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.PurchasingCenterChannels WHERE 1=1 AND [ChannelID] = 2 )	INSERT IMT.dbo.PurchasingCenterChannels ([ChannelID],[ChannelName],[HasAccessGroups])	VALUES (2,'In-Group',0)
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.PurchasingCenterChannels WHERE 1=1 AND [ChannelID] = 3 )	INSERT IMT.dbo.PurchasingCenterChannels ([ChannelID],[ChannelName],[HasAccessGroups])	VALUES (3,'Live Auction',0)
SET IDENTITY_INSERT IMT.dbo.PurchasingCenterChannels OFF
