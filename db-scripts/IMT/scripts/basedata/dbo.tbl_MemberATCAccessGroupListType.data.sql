SET IDENTITY_INSERT IMT.dbo.tbl_MemberATCAccessGroupListType ON
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.tbl_MemberATCAccessGroupListType WHERE 1=1 AND [MemberATCAccessGroupListTypeID] = 1 )	INSERT IMT.dbo.tbl_MemberATCAccessGroupListType ([MemberATCAccessGroupListTypeID],[Description])	VALUES (1,'Permanent Market List')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.tbl_MemberATCAccessGroupListType WHERE 1=1 AND [MemberATCAccessGroupListTypeID] = 2 )	INSERT IMT.dbo.tbl_MemberATCAccessGroupListType ([MemberATCAccessGroupListTypeID],[Description])	VALUES (2,'Current Market List')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.tbl_MemberATCAccessGroupListType WHERE 1=1 AND [MemberATCAccessGroupListTypeID] = 3 )	INSERT IMT.dbo.tbl_MemberATCAccessGroupListType ([MemberATCAccessGroupListTypeID],[Description])	VALUES (3,'Temporary Market List')
SET IDENTITY_INSERT IMT.dbo.tbl_MemberATCAccessGroupListType OFF
