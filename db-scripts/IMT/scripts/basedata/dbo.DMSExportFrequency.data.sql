SET IDENTITY_INSERT IMT.dbo.DMSExportFrequency ON
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.DMSExportFrequency WHERE 1=1 AND [DMSExportFrequencyID] = 1 )	Insert IMT.dbo.DMSExportFrequency ([DMSExportFrequencyID],[Description])	Values (1,'Daily')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.DMSExportFrequency WHERE 1=1 AND [DMSExportFrequencyID] = 2 )	Insert IMT.dbo.DMSExportFrequency ([DMSExportFrequencyID],[Description])	Values (2,'Incremental')
SET IDENTITY_INSERT IMT.dbo.DMSExportFrequency OFF
