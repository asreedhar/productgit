IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Role WHERE 1=1 AND [RoleID] = 1 )	INSERT IMT.dbo.Role ([RoleID],[Type],[Code],[Name])	VALUES (1,'U','NOACCESS  ','No Access')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Role WHERE 1=1 AND [RoleID] = 2 )	INSERT IMT.dbo.Role ([RoleID],[Type],[Code],[Name])	VALUES (2,'U','APPRAISE  ','Appraiser')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Role WHERE 1=1 AND [RoleID] = 3 )	INSERT IMT.dbo.Role ([RoleID],[Type],[Code],[Name])	VALUES (3,'U','STANDARD  ','Standard')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Role WHERE 1=1 AND [RoleID] = 4 )	INSERT IMT.dbo.Role ([RoleID],[Type],[Code],[Name])	VALUES (4,'U','MANAGER   ','Manager')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Role WHERE 1=1 AND [RoleID] = 5 )	INSERT IMT.dbo.Role ([RoleID],[Type],[Code],[Name])	VALUES (5,'N','NOACCESS  ','No Access')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Role WHERE 1=1 AND [RoleID] = 6 )	INSERT IMT.dbo.Role ([RoleID],[Type],[Code],[Name])	VALUES (6,'N','STANDARD  ','Standard')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Role WHERE 1=1 AND [RoleID] = 7 )	INSERT IMT.dbo.Role ([RoleID],[Type],[Code],[Name])	VALUES (7,'A','NONE      ','None')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Role WHERE 1=1 AND [RoleID] = 8 )	INSERT IMT.dbo.Role ([RoleID],[Type],[Code],[Name])	VALUES (8,'A','FULL      ','Full')
