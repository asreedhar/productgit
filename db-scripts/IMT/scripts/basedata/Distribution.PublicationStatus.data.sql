SET IDENTITY_INSERT IMT.Distribution.PublicationStatus ON
IF NOT EXISTS (SELECT 1 FROM IMT.Distribution.PublicationStatus WHERE 1=1 AND [PublicationStatusID] = 0 )	INSERT IMT.Distribution.PublicationStatus ([PublicationStatusID],[Name])	VALUES (0,'Unknown')
IF NOT EXISTS (SELECT 1 FROM IMT.Distribution.PublicationStatus WHERE 1=1 AND [PublicationStatusID] = 1 )	INSERT IMT.Distribution.PublicationStatus ([PublicationStatusID],[Name])	VALUES (1,'Online')
IF NOT EXISTS (SELECT 1 FROM IMT.Distribution.PublicationStatus WHERE 1=1 AND [PublicationStatusID] = 2 )	INSERT IMT.Distribution.PublicationStatus ([PublicationStatusID],[Name])	VALUES (2,'Offline')
IF NOT EXISTS (SELECT 1 FROM IMT.Distribution.PublicationStatus WHERE 1=1 AND [PublicationStatusID] = 3 )	INSERT IMT.Distribution.PublicationStatus ([PublicationStatusID],[Name])	VALUES (3,'NotApplicable')
SET IDENTITY_INSERT IMT.Distribution.PublicationStatus OFF
