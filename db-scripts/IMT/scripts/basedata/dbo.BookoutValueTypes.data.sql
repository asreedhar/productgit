SET IDENTITY_INSERT IMT.dbo.BookoutValueTypes ON
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.BookoutValueTypes WHERE 1=1 AND [BookoutValueTypeID] = 1 )	INSERT IMT.dbo.BookoutValueTypes ([BookoutValueTypeID],[Description])	VALUES (1,'Base Value')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.BookoutValueTypes WHERE 1=1 AND [BookoutValueTypeID] = 2 )	INSERT IMT.dbo.BookoutValueTypes ([BookoutValueTypeID],[Description])	VALUES (2,'Final Value')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.BookoutValueTypes WHERE 1=1 AND [BookoutValueTypeID] = 3 )	INSERT IMT.dbo.BookoutValueTypes ([BookoutValueTypeID],[Description])	VALUES (3,'Mileage Independent')
IF NOT EXISTS (SELECT 1 FROM IMT.dbo.BookoutValueTypes WHERE 1=1 AND [BookoutValueTypeID] = 4 )	INSERT IMT.dbo.BookoutValueTypes ([BookoutValueTypeID],[Description])	VALUES (4,'Options Value')
SET IDENTITY_INSERT IMT.dbo.BookoutValueTypes OFF
