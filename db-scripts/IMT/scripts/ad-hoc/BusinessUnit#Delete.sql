

DECLARE @BusinessUnitID INT

SET @BusinessUnitID = 100215


EXEC sp_map_exec 'ALTER TABLE ? NOCHECK CONSTRAINT ALL',

		'SELECT	OBJECT_SCHEMA_NAME(OBJECT_ID) + "." + NAME FROM	sys.tables',
		0
		
CREATE TABLE #BusinessUnitIDs (BusinessUnitID INT)
	INSERT INTO #BusinessUnitIDs (BusinessUnitID)
	SELECT	BU.BusinessUnitID
	FROM	BusinessUnit BU

	WHERE 	BU.Active = 0
		OR 		BU.BusinessUnitID NOT IN (
		100150						-- Firstlook
		,100149						-- United Auto Group
		,100067						-- Goodson Auto Group
		,100136						-- Goodson Honda North
		,100138						-- Goodson Honda West
		,100068						-- Hendrick Automotive Group 
		,100147						-- Hendrick City Chevrolet
		,100148						-- Hendrick BMW
		,100246						-- Hendrick Honda of Charleston
		,100590						-- Rick Hendrick Imports
		,100650						-- Rick Hendrick Chevrolet 
		,100239						-- Rick Hendrick Toyota 
		,100845						-- Hendrick Pontiac Buick GMC - Cary Auto M
		,100854						-- Hendrick Chevrolet - Cary Auto Mall
		,100855						-- Hendrick Dodge - Cary Auto Mall
		,100862						-- Hendrick Automotive Group

		,100214, 100215,100216 				-- Longo
		,100832, 100833, 100834, 100835, 100836, 100837	-- Gary McGrath
		,100805						-- Gwinnett Place Honda
		,100926, 100927					-- Ken Garff
		,100205, 100206, 100235, 100349			-- Phil Long
		,101250, 101255, 101251, 101011, 101240		-- Lithia/Boise

		---- HAL ----
	
		,100350						-- Phil Long Hyundai - Chapel Hills
		,101486, 101897					-- Landers Dodge Southhaven 
		
		---- S&A Navigator (VIP,DR) ----

		,101376, 101375					-- Jennings Chevrolet	
		)
	
				

DELETE	T0
FROM	AutoCheck.Account_Dealer T0
WHERE	T0.DealerID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	AutoCheck.Vehicle_Dealer T0
WHERE	T0.DealerID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	Carfax.Account_Dealer T0
WHERE	T0.DealerID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	Carfax.ReportPreference T0
WHERE	T0.DealerID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	Carfax.ReportProcessorCommand T0 INNER JOIN Carfax.ReportProcessorCommand_DataMigration T1 ON T0.ReportProcessorCommandID = T1.ReportProcessorCommandID
WHERE	T0.DealerID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	Carfax.ReportProcessorCommand T0 INNER JOIN Carfax.ReportProcessorCommand_Exception T1 ON T0.ReportProcessorCommandID = T1.ReportProcessorCommandID
WHERE	T0.DealerID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	Carfax.ReportProcessorCommand T0 INNER JOIN Carfax.ReportProcessorCommand_Request T1 ON T0.ReportProcessorCommandID = T1.ReportProcessorCommandID
WHERE	T0.DealerID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	Carfax.ReportProcessorCommand T0
WHERE	T0.DealerID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	Carfax.Vehicle_Dealer T0
WHERE	T0.DealerID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.HalSessions T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.AppraisalBusinessUnitGroupDemand T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Appraisals T0 INNER JOIN dbo.AppraisalActions T1 ON T0.AppraisalID = T1.AppraisalID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Appraisals T0 INNER JOIN dbo.AppraisalBookouts T1 ON T0.AppraisalID = T1.AppraisalID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Appraisals T0 INNER JOIN dbo.AppraisalBusinessUnitGroupDemand T1 ON T0.AppraisalID = T1.AppraisalID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Appraisals T0 INNER JOIN dbo.AppraisalCustomer T1 ON T0.AppraisalID = T1.AppraisalID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Appraisals T0 INNER JOIN dbo.AppraisalFormOptions T1 ON T0.AppraisalID = T1.AppraisalID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Appraisals T0 INNER JOIN dbo.AppraisalWindowStickers T1 ON T0.AppraisalID = T1.AppraisalID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Appraisals T0 INNER JOIN dbo.AppraisalPhotos T1 ON T0.AppraisalID = T1.AppraisalID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Appraisals T0 INNER JOIN dbo.AppraisalValues T1 ON T0.AppraisalID = T1.AppraisalID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.Appraisals T0 INNER JOIN dbo.Staging_Bookouts T1 ON T0.AppraisalID = T1.AppraisalID INNER JOIN dbo.Staging_BookoutOptions T2 ON T1.Staging_BookoutID = T2.Staging_BookoutID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.Appraisals T0 INNER JOIN dbo.Staging_Bookouts T1 ON T0.AppraisalID = T1.AppraisalID INNER JOIN dbo.Staging_BookoutValues T2 ON T1.Staging_BookoutID = T2.Staging_BookoutID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Appraisals T0 INNER JOIN dbo.Staging_Bookouts T1 ON T0.AppraisalID = T1.AppraisalID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Appraisals T0 INNER JOIN dbo.Staging_VehicleHistoryReport T1 ON T0.AppraisalID = T1.AppraisalID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.Appraisals T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.AuditBookOuts T0
WHERE	T0.BusinessUnitId in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.BookoutProcessorRunLog T0
WHERE	T0.BusinessUnitId in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.BusinessUnitCredential T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.BusinessUnitPhotos T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.BusinessUnitPreference_Bookout T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.BusinessUnitRelationship T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.BusinessUnitRelationship T0
WHERE	T0.ParentID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.BusinessUnitStatus T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T3
FROM	dbo.CIASummary T0 INNER JOIN dbo.CIABodyTypeDetails T1 ON T0.CIASummaryID = T1.CIASummaryID INNER JOIN dbo.CIAGroupingItems T2 ON T1.CIABodyTypeDetailID = T2.CIABodyTypeDetailID INNER JOIN dbo.CIAGroupingItemDetails T3 ON T2.CIAGroupingItemID = T3.CIAGroupingItemID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.CIASummary T0 INNER JOIN dbo.CIABodyTypeDetails T1 ON T0.CIASummaryID = T1.CIASummaryID INNER JOIN dbo.CIAGroupingItems T2 ON T1.CIABodyTypeDetailID = T2.CIABodyTypeDetailID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.CIASummary T0 INNER JOIN dbo.CIABodyTypeDetails T1 ON T0.CIASummaryID = T1.CIASummaryID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.CIASummary T0 INNER JOIN dbo.CIAInventoryOverstocking T1 ON T0.CIASummaryID = T1.CIASummaryID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.CIASummary T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.DealerAuctionPreference T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.DealerCIAPreferences T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.DealerFacts T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.DealerFinancialStatement T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.DealerGridThresholds T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.DealerGridValues T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.DealerGroupPreference T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.DealerInsightTargetPreference T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.DealerPack T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.DealerPreference T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.DealerSalesChannel T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.DealerTransferAllowRetailOnlyRule T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.DealerTransferPriceUnitCostRule T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.tbl_DealerValuation T0
WHERE	T0.DealerId in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.DMSExportBusinessUnitPreference T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.DealerPreference_InternetAdvertisementBuilder T0 INNER JOIN dbo.DealerInternetAdvertisementBuilderOptionProvider T1 ON T0.BusinessUnitID = T1.BusinessUnitID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.DealerPreference_InternetAdvertisementBuilder T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.DealerPreference_JDPowerSettings T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.DealerPreference_Pricing T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.DealerPreference_WindowSticker T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.EquityAnalyzerSearches T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.FLUSAN_EventLog T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.GDLight T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.tbl_GroupingPromotionPlan T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.InternetAdvertiserDealership T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.Inventory T0 INNER JOIN dbo.AIP_Event T1 ON T0.InventoryID = T1.InventoryID INNER JOIN dbo.RepriceExport T2 ON T1.AIP_EventID = T2.RepriceEventID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.ThirdPartyEntity T0 INNER JOIN dbo.AIP_Event T1 ON T0.ThirdPartyEntityID = T1.ThirdPartyEntityID INNER JOIN dbo.RepriceExport T2 ON T1.AIP_EventID = T2.RepriceEventID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Inventory T0 INNER JOIN dbo.AIP_Event T1 ON T0.InventoryID = T1.InventoryID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Inventory T0 INNER JOIN dbo.CIAInventoryOverstocking T1 ON T0.InventoryID = T1.InventoryID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Inventory T0 INNER JOIN dbo.DealFinance T1 ON T0.InventoryID = T1.InventoryId
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Inventory T0 INNER JOIN dbo.DealLease T1 ON T0.InventoryID = T1.InventoryId
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Inventory T0 INNER JOIN dbo.DealTradeIn T1 ON T0.InventoryID = T1.InventoryId
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Inventory T0 INNER JOIN dbo.DemoDealerBaseline_Inventory T1 ON T0.InventoryID = T1.InventoryID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Inventory T0 INNER JOIN dbo.InternetAdvertisement T1 ON T0.InventoryID = T1.InventoryID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Inventory T0 INNER JOIN dbo.InternetAdvertiserBuildList T1 ON T0.InventoryID = T1.InventoryID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Inventory T0 INNER JOIN dbo.Inventory_Advertising T1 ON T0.InventoryID = T1.InventoryID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Inventory T0 INNER JOIN dbo.Inventory_Insight T1 ON T0.InventoryID = T1.InventoryID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Inventory T0 INNER JOIN dbo.Inventory_Insight T1 ON T0.InventoryID = T1.InventoryID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Inventory T0 INNER JOIN dbo.Inventory_Tracking T1 ON T0.InventoryID = T1.InventoryID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Inventory T0 INNER JOIN dbo.InventoryBookouts T1 ON T0.InventoryID = T1.InventoryID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Inventory T0 INNER JOIN dbo.InventoryPhotos T1 ON T0.InventoryID = T1.InventoryID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Inventory T0 INNER JOIN dbo.MarketplaceSubmission T1 ON T0.InventoryID = T1.InventoryID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Inventory T0 INNER JOIN dbo.tbl_VehicleSale T1 ON T0.InventoryID = T1.InventoryID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.Inventory T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.InventoryBucketRanges T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.KBBArchiveDatasetOverride T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.MarketShare T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.MarketToZip T0
WHERE	T0.BusinessUnitId in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.MemberAccess T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.MemberAppraisalReviewPreferences T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.tbl_MemberATCAccessGroupList T0 INNER JOIN dbo.tbl_MemberATCAccessGroup T1 ON T0.MemberATCAccessGroupListID = T1.MemberATCAccessGroupListID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.tbl_MemberATCAccessGroupList T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.MemberBusinessUnitSet T0 INNER JOIN dbo.MemberBusinessUnitSetItem T1 ON T0.MemberBusinessUnitSetID = T1.MemberBusinessUnitSetID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.MemberBusinessUnitSet T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.MemberBusinessUnitSetItem T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.MemberInsightTypePreference T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.tbl_NewCarScoreCard T0
WHERE	T0.BusinessUnitId in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.PackAdjustRules T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.Person T0 INNER JOIN dbo.Appraisals T1 ON T0.PersonID = T1.DealTrackSalespersonID INNER JOIN dbo.AppraisalActions T2 ON T1.AppraisalID = T2.AppraisalID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.Person T0 INNER JOIN dbo.Appraisals T1 ON T0.PersonID = T1.DealTrackSalespersonID INNER JOIN dbo.AppraisalBookouts T2 ON T1.AppraisalID = T2.AppraisalID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.Person T0 INNER JOIN dbo.Appraisals T1 ON T0.PersonID = T1.DealTrackSalespersonID INNER JOIN dbo.AppraisalBusinessUnitGroupDemand T2 ON T1.AppraisalID = T2.AppraisalID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.Person T0 INNER JOIN dbo.Appraisals T1 ON T0.PersonID = T1.DealTrackSalespersonID INNER JOIN dbo.AppraisalCustomer T2 ON T1.AppraisalID = T2.AppraisalID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.Person T0 INNER JOIN dbo.Appraisals T1 ON T0.PersonID = T1.DealTrackSalespersonID INNER JOIN dbo.AppraisalFormOptions T2 ON T1.AppraisalID = T2.AppraisalID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.Person T0 INNER JOIN dbo.Appraisals T1 ON T0.PersonID = T1.DealTrackSalespersonID INNER JOIN dbo.AppraisalWindowStickers T2 ON T1.AppraisalID = T2.AppraisalID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.Person T0 INNER JOIN dbo.Appraisals T1 ON T0.PersonID = T1.DealTrackSalespersonID INNER JOIN dbo.AppraisalPhotos T2 ON T1.AppraisalID = T2.AppraisalID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.Person T0 INNER JOIN dbo.Appraisals T1 ON T0.PersonID = T1.DealTrackSalespersonID INNER JOIN dbo.AppraisalValues T2 ON T1.AppraisalID = T2.AppraisalID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T3
FROM	dbo.Person T0 INNER JOIN dbo.Appraisals T1 ON T0.PersonID = T1.DealTrackSalespersonID INNER JOIN dbo.Staging_Bookouts T2 ON T1.AppraisalID = T2.AppraisalID INNER JOIN dbo.Staging_BookoutOptions T3 ON T2.Staging_BookoutID = T3.Staging_BookoutID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T3
FROM	dbo.Person T0 INNER JOIN dbo.Appraisals T1 ON T0.PersonID = T1.DealTrackSalespersonID INNER JOIN dbo.Staging_Bookouts T2 ON T1.AppraisalID = T2.AppraisalID INNER JOIN dbo.Staging_BookoutValues T3 ON T2.Staging_BookoutID = T3.Staging_BookoutID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.Person T0 INNER JOIN dbo.Appraisals T1 ON T0.PersonID = T1.DealTrackSalespersonID INNER JOIN dbo.Staging_Bookouts T2 ON T1.AppraisalID = T2.AppraisalID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.Person T0 INNER JOIN dbo.Appraisals T1 ON T0.PersonID = T1.DealTrackSalespersonID INNER JOIN dbo.Staging_VehicleHistoryReport T2 ON T1.AppraisalID = T2.AppraisalID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Person T0 INNER JOIN dbo.Appraisals T1 ON T0.PersonID = T1.DealTrackSalespersonID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.Person T0 INNER JOIN dbo.PersonPosition T1 ON T0.PersonID = T1.PersonID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.Person T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.PhotoProviderDealership T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.ReportCenterSessions T0
WHERE	T0.BusinessUnitId in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.SearchCandidateList T0 INNER JOIN dbo.SearchCandidate T1 ON T0.SearchCandidateListID = T1.SearchCandidateListID INNER JOIN dbo.SearchCandidateCIACategoryAnnotation T2 ON T1.SearchCandidateID = T2.SearchCandidateID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T3
FROM	dbo.SearchCandidateList T0 INNER JOIN dbo.SearchCandidate T1 ON T0.SearchCandidateListID = T1.SearchCandidateListID INNER JOIN dbo.SearchCandidateOption T2 ON T1.SearchCandidateID = T2.SearchCandidateID INNER JOIN dbo.SearchCandidateOptionAnnotation T3 ON T2.SearchCandidateOptionID = T3.SearchCandidateOptionID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.SearchCandidateList T0 INNER JOIN dbo.SearchCandidate T1 ON T0.SearchCandidateListID = T1.SearchCandidateListID INNER JOIN dbo.SearchCandidateOption T2 ON T1.SearchCandidateID = T2.SearchCandidateID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.SearchCandidateList T0 INNER JOIN dbo.SearchCandidate T1 ON T0.SearchCandidateListID = T1.SearchCandidateListID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.SearchCandidateList T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.SoftwareSystemComponentState T0
WHERE	T0.DealerID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.SoftwareSystemComponentState T0
WHERE	T0.DealerGroupID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.SystemErrors T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.tbl_CIAPreferences T0
WHERE	T0.BusinessUnitId in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.tbl_RedistributionLog T0
WHERE	T0.OriginatingDealerId in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.ThirdPartyEntity T0 INNER JOIN dbo.AIP_Event T1 ON T0.ThirdPartyEntityID = T1.ThirdPartyEntityID INNER JOIN dbo.RepriceExport T2 ON T1.AIP_EventID = T2.RepriceEventID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.Inventory T0 INNER JOIN dbo.AIP_Event T1 ON T0.InventoryID = T1.InventoryID INNER JOIN dbo.RepriceExport T2 ON T1.AIP_EventID = T2.RepriceEventID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.ThirdPartyEntity T0 INNER JOIN dbo.AIP_Event T1 ON T0.ThirdPartyEntityID = T1.ThirdPartyEntityID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.ThirdPartyEntity T0 INNER JOIN dbo.DMSExport_ThirdPartyEntity T1 ON T0.ThirdPartyEntityID = T1.DMSExportID INNER JOIN dbo.DMSExportBusinessUnitPreference T2 ON T1.DMSExportID = T2.DMSExportID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.ThirdPartyEntity T0 INNER JOIN dbo.DMSExport_ThirdPartyEntity T1 ON T0.ThirdPartyEntityID = T1.DMSExportID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.ThirdPartyEntity T0 INNER JOIN dbo.InternetAdvertiser_ThirdPartyEntity T1 ON T0.ThirdPartyEntityID = T1.InternetAdvertiserID INNER JOIN dbo.InternetAdvertiserBuildList T2 ON T1.InternetAdvertiserID = T2.InternetAdvertiserID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T3
FROM	dbo.ThirdPartyEntity T0 INNER JOIN dbo.InternetAdvertiser_ThirdPartyEntity T1 ON T0.ThirdPartyEntityID = T1.InternetAdvertiserID INNER JOIN dbo.InternetAdvertiserBuildLog T2 ON T1.InternetAdvertiserID = T2.InternetAdvertiserID INNER JOIN dbo.InternetAdvertiserBuildLogDetail T3 ON T2.InternetAdvertiserBuildLogID = T3.InternetAdvertiserBuildLogID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.ThirdPartyEntity T0 INNER JOIN dbo.InternetAdvertiser_ThirdPartyEntity T1 ON T0.ThirdPartyEntityID = T1.InternetAdvertiserID INNER JOIN dbo.InternetAdvertiserBuildLog T2 ON T1.InternetAdvertiserID = T2.InternetAdvertiserID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.ThirdPartyEntity T0 INNER JOIN dbo.InternetAdvertiser_ThirdPartyEntity T1 ON T0.ThirdPartyEntityID = T1.InternetAdvertiserID INNER JOIN dbo.InternetAdvertiserDealership T2 ON T1.InternetAdvertiserID = T2.InternetAdvertiserID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.ThirdPartyEntity T0 INNER JOIN dbo.InternetAdvertiser_ThirdPartyEntity T1 ON T0.ThirdPartyEntityID = T1.InternetAdvertiserID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.ThirdPartyEntity T0 INNER JOIN dbo.PhotoProvider_ThirdPartyEntity T1 ON T0.ThirdPartyEntityID = T1.PhotoProviderID INNER JOIN dbo.PhotoProviderDealership T2 ON T1.PhotoProviderID = T2.PhotoProviderID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T3
FROM	dbo.ThirdPartyEntity T0 INNER JOIN dbo.PhotoProvider_ThirdPartyEntity T1 ON T0.ThirdPartyEntityID = T1.PhotoProviderID INNER JOIN dbo.Photos T2 ON T1.PhotoProviderID = T2.PhotoProviderID INNER JOIN dbo.AppraisalPhotos T3 ON T2.PhotoID = T3.PhotoID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T3
FROM	dbo.ThirdPartyEntity T0 INNER JOIN dbo.PhotoProvider_ThirdPartyEntity T1 ON T0.ThirdPartyEntityID = T1.PhotoProviderID INNER JOIN dbo.Photos T2 ON T1.PhotoProviderID = T2.PhotoProviderID INNER JOIN dbo.BusinessUnitPhotos T3 ON T2.PhotoID = T3.PhotoID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T3
FROM	dbo.ThirdPartyEntity T0 INNER JOIN dbo.PhotoProvider_ThirdPartyEntity T1 ON T0.ThirdPartyEntityID = T1.PhotoProviderID INNER JOIN dbo.Photos T2 ON T1.PhotoProviderID = T2.PhotoProviderID INNER JOIN dbo.InventoryPhotos T3 ON T2.PhotoID = T3.PhotoID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.ThirdPartyEntity T0 INNER JOIN dbo.PhotoProvider_ThirdPartyEntity T1 ON T0.ThirdPartyEntityID = T1.PhotoProviderID INNER JOIN dbo.Photos T2 ON T1.PhotoProviderID = T2.PhotoProviderID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.ThirdPartyEntity T0 INNER JOIN dbo.PhotoProvider_ThirdPartyEntity T1 ON T0.ThirdPartyEntityID = T1.PhotoProviderID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T2
FROM	dbo.ThirdPartyEntity T0 INNER JOIN dbo.PrintAdvertiser_ThirdPartyEntity T1 ON T0.ThirdPartyEntityID = T1.PrintAdvertiserID INNER JOIN dbo.PrintAdvertiserTextOptionDefault T2 ON T1.PrintAdvertiserID = T2.PrintAdvertiserID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.ThirdPartyEntity T0 INNER JOIN dbo.PrintAdvertiser_ThirdPartyEntity T1 ON T0.ThirdPartyEntityID = T1.PrintAdvertiserID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.ThirdPartyEntity T0 INNER JOIN dbo.RepriceExport T1 ON T0.ThirdPartyEntityID = T1.ThirdPartyEntityID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.ThirdPartyEntity T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.tbl_UsedCarScoreCard T0
WHERE	T0.BusinessUnitId in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.VehicleBookoutState T0 INNER JOIN dbo.VehicleBookoutAdditionalState T1 ON T0.VehicleBookoutStateID = T1.VehicleBookoutStateID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T1
FROM	dbo.VehicleBookoutState T0 INNER JOIN dbo.VehicleBookoutStateThirdPartyVehicles T1 ON T0.VehicleBookoutStateID = T1.VehicleBookoutStateID
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.VehicleBookoutState T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.DealerPreference_KBBConsumerTool T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------

DELETE	T0
FROM	Extract.DealerConfiguration T0
WHERE	T0.BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)
--------------------------------------------------------------------------------
DELETE
FROM	dbo.BusinessUnit
WHERE	BusinessUnitID in (SELECT BusinessUnitID FROM #BusinessUnitIDs)

--------------------------------------------------------------------------------
--	Orphaned Bookouts
--------------------------------------------------------------------------------

SELECT	B.BookoutID
INTO	#BookoutOrphans
FROM	dbo.Bookouts B WITH (NOLOCK)
	LEFT JOIN dbo.InventoryBookouts IB WITH (NOLOCK) ON B.BookoutID = IB.BookoutID
	LEFT JOIN dbo.AppraisalBookouts AB WITH (NOLOCK) ON B.BookoutID = AB.BookoutID
WHERE	IB.BookoutID IS NULL
	AND AB.BookoutID IS NULL


DELETE	T1
FROM	dbo.BookoutThirdPartyCategories T0 INNER JOIN dbo.BookoutValues T1 ON T0.BookoutThirdPartyCategoryID = T1.BookoutThirdPartyCategoryID
WHERE	T0.BookoutID IN (SELECT	BookoutID FROM #BookoutOrphans)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.BookoutThirdPartyCategories T0
WHERE	T0.BookoutID IN (SELECT	BookoutID FROM #BookoutOrphans)
--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.BookoutThirdPartyVehicles T0
WHERE	T0.BookoutID IN (SELECT	BookoutID FROM #BookoutOrphans)

--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.Bookouts T0
WHERE	T0.BookoutID IN (SELECT	BookoutID FROM #BookoutOrphans)

--------------------------------------------------------------------------------
--	Orphaned TPVs
--------------------------------------------------------------------------------

SELECT	TPV.ThirdPartyVehicleID
INTO	#ThirdPartyVehicleOrphans
FROM	dbo.ThirdPartyVehicles TPV WITH (NOLOCK)
	LEFT JOIN dbo.BookoutThirdPartyVehicles BTPV WITH (NOLOCK) ON TPV.ThirdPartyVehicleID = BTPV.ThirdPartyVehicleID
WHERE	BTPV.ThirdPartyVehicleID IS NULL
	
--------------------------------------------------------------------------------
	
DELETE	T0
FROM	dbo.VehicleBookoutStateThirdPartyVehicles T0
WHERE	T0.ThirdPartyVehicleID IN (SELECT ThirdPartyVehicleID FROM #ThirdPartyVehicleOrphans)
--------------------------------------------------------------------------------

SELECT	T1.*
INTO	#T1
FROM	dbo.ThirdPartyVehicleOptions T0 INNER JOIN dbo.ThirdPartyVehicleOptionValues T1 ON T0.ThirdPartyVehicleOptionID = T1.ThirdPartyVehicleOptionID
WHERE	T0.ThirdPartyVehicleID NOT IN (SELECT ThirdPartyVehicleID FROM #ThirdPartyVehicleOrphans)

TRUNCATE TABLE dbo.ThirdPartyVehicleOptionValues

SET IDENTITY_INSERT ThirdPartyVehicleOptionValues ON

INSERT
INTO	dbo.ThirdPartyVehicleOptionValues (ThirdPartyVehicleOptionValueID, ThirdPartyVehicleOptionID, ThirdPartyOptionValueTypeID, Value, DateCreated)
SELECT	ThirdPartyVehicleOptionValueID, ThirdPartyVehicleOptionID, ThirdPartyOptionValueTypeID, Value, DateCreated 
FROM	#T1

SET IDENTITY_INSERT ThirdPartyVehicleOptionValues OFF
GO

--------------------------------------------------------------------------------

ALTER TABLE [dbo].[ThirdPartyVehicleOptionValues] DROP CONSTRAINT [FK_VehicleThirdPartyVehicleOptionValues_VehicleThirdPartyVehicleOptions] 
GO


SELECT	T0.*
INTO	#T0
FROM	dbo.ThirdPartyVehicleOptions T0
WHERE	T0.ThirdPartyVehicleID NOT IN (SELECT ThirdPartyVehicleID FROM #ThirdPartyVehicleOrphans)

TRUNCATE TABLE dbo.ThirdPartyVehicleOptions

SET IDENTITY_INSERT dbo.ThirdPartyVehicleOptions ON

INSERT
INTO	dbo.ThirdPartyVehicleOptions (ThirdPartyVehicleOptionID, ThirdPartyVehicleID, ThirdPartyOptionID, IsStandardOption, Status, SortOrder, DateCreated)
SELECT	ThirdPartyVehicleOptionID, ThirdPartyVehicleID, ThirdPartyOptionID, IsStandardOption, Status, SortOrder, DateCreated
FROM	#T0

SET IDENTITY_INSERT dbo.ThirdPartyVehicleOptions OFF

ALTER TABLE [dbo].[ThirdPartyVehicleOptionValues] WITH NOCHECK ADD CONSTRAINT [FK_VehicleThirdPartyVehicleOptionValues_VehicleThirdPartyVehicleOptions] FOREIGN KEY ([ThirdPartyVehicleOptionID]) REFERENCES [dbo].[ThirdPartyVehicleOptions] ([ThirdPartyVehicleOptionID])
GO

--------------------------------------------------------------------------------

DELETE	T0
FROM	dbo.ThirdPartyVehicles T0
WHERE	T0.ThirdPartyVehicleID IN (SELECT ThirdPartyVehicleID FROM #ThirdPartyVehicleOrphans)

--------------------------------------------------------------------------------
--	Orphaned Photos
--------------------------------------------------------------------------------
SELECT	P.PhotoID
INTO	#PhotoOrphans
FROM	dbo.Photos P
	LEFT JOIN dbo.AppraisalPhotos AP ON P.PhotoID = AP.PhotoID
	LEFT JOIN dbo.InventoryPhotos IP ON P.PhotoID = IP.PhotoID
WHERE	AP.PhotoID IS NULL
	AND IP.PhotoID IS NULL	
--------------------------------------------------------------------------------
DELETE	T0
FROM	dbo.Photos T0
WHERE	T0.PhotoID IN (SELECT PhotoID FROM #PhotoOrphans)

EXEC sp_map_exec 'ALTER TABLE ? CHECK CONSTRAINT ALL',

		'SELECT	OBJECT_SCHEMA_NAME(OBJECT_ID) + "." + NAME FROM	sys.tables',
		0	
		
