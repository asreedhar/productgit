
DELETE	BV
FROM	Inventory I
	JOIN dbo.InventoryBookouts IB ON I.InventoryID = IB.InventoryID
	JOIN dbo.Bookouts B ON IB.BookoutID = B.BookoutID
	JOIN dbo.BookoutThirdPartyCategories BTPC ON B.BookoutID = BTPC.BookoutID
	JOIN dbo.BookoutValues BV ON BTPC.BookoutThirdPartyCategoryID = BV.BookoutThirdPartyCategoryID
WHERE	B.BookoutID IN (20251849,20211539,20251850)


DELETE	BTPC
FROM	Inventory I
	JOIN dbo.InventoryBookouts IB ON I.InventoryID = IB.InventoryID
	JOIN dbo.Bookouts B ON IB.BookoutID = B.BookoutID
	JOIN dbo.BookoutThirdPartyCategories BTPC ON B.BookoutID = BTPC.BookoutID
	
WHERE	B.BookoutID IN (20251849,20211539,20251850)



SELECT	ThirdPartyVehicleOptionID
INTO	#TPVO
FROM	Inventory I
	JOIN dbo.InventoryBookouts IB ON I.InventoryID = IB.InventoryID
	JOIN dbo.Bookouts B ON IB.BookoutID = B.BookoutID
	JOIN dbo.BookoutThirdPartyVehicles BTPV ON B.BookoutID = BTPV.BookoutID
	JOIN dbo.ThirdPartyVehicles TPV ON BTPV.ThirdPartyVehicleID = TPV.ThirdPartyVehicleID
	JOIN dbo.ThirdPartyVehicleOptions TPVO WITH (NOLOCK) ON TPV.ThirdPartyVehicleID = TPVO.ThirdPartyVehicleID

WHERE	B.BookoutID IN (20251849,20211539,20251850)


DELETE 
FROM	dbo.ThirdPartyVehicleOptionValues
WHERE	ThirdPartyVehicleOptionID IN (SELECT ThirdPartyVehicleOptionID FROM #TPVO)

DELETE 
FROM	dbo.ThirdPartyVehicleOptions
WHERE	ThirdPartyVehicleOptionID IN (SELECT ThirdPartyVehicleOptionID FROM #TPVO)


SELECT	TPV.ThirdPartyVehicleID
INTO	#TPV
FROM	Inventory I
	JOIN dbo.InventoryBookouts IB ON I.InventoryID = IB.InventoryID
	JOIN dbo.Bookouts B ON IB.BookoutID = B.BookoutID
	JOIN dbo.BookoutThirdPartyVehicles BTPV ON B.BookoutID = BTPV.BookoutID
	JOIN dbo.ThirdPartyVehicles TPV ON BTPV.ThirdPartyVehicleID = TPV.ThirdPartyVehicleID
WHERE	B.BookoutID IN (20251849,20211539,20251850)


DELETE	BookoutThirdPartyVehicles
WHERE	ThirdPartyVehicleID IN ( SELECT ThirdPartyVehicleID FROM #TPV)

DELETE	ThirdPartyVehicles
WHERE	ThirdPartyVehicleID IN ( SELECT ThirdPartyVehicleID FROM #TPV)


SELECT	B.BookoutID 
INTO	#Bookouts
FROM	Inventory I
	JOIN dbo.InventoryBookouts IB ON I.InventoryID = IB.InventoryID
	JOIN dbo.Bookouts B ON IB.BookoutID = B.BookoutID

WHERE	B.BookoutID IN (20251849,20211539,20251850)


DELETE	InventoryBookouts
WHERE	BookoutID IN ( SELECT BookoutID FROM #Bookouts)

DELETE	AuditBookOuts
WHERE	BookoutID IN ( SELECT BookoutID FROM #Bookouts)

DELETE	Bookouts
WHERE	BookoutID IN ( SELECT BookoutID FROM #Bookouts)