
IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'DATABASE_ROLE'
			AND name = 'MerchandisingUser'
		)

	CREATE ROLE [MerchandisingUser] AUTHORIZATION [dbo]

GO


IF NOT EXISTS (	SELECT	1
		FROM	sys.database_role_members RM
			JOIN sys.database_principals P1 ON RM.member_principal_id = P1.principal_id
			JOIN sys.database_principals P2 ON RM.role_principal_id = P2.principal_id
		WHERE	P1.NAME = 'MerchandisingInterface'
			AND P1.type_desc = 'SQL_USER'
			AND P2.NAME = 'MerchandisingUser'
			AND P2.type_desc = 'DATABASE_ROLE'
			)

	EXEC sp_addrolemember N'MerchandisingUser', N'MerchandisingInterface'
	
	
GRANT SELECT, INSERT, UPDATE ON dbo.InternetAdvertisement TO MerchandisingUser

GO