
IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'DATABASE_ROLE'
			AND name = 'CarfaxUser'
		)

	CREATE ROLE [CarfaxUser] AUTHORIZATION [dbo]

GO