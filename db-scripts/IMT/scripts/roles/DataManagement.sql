IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'DATABASE_ROLE'
			AND NAME = 'DataManagement'
		)

	CREATE ROLE [DataManagement] AUTHORIZATION [dbo]

GO


/* 	

USE [IMT]
--GO
CREATE ROLE [DataManagement] AUTHORIZATION [dbo]
--GO
USE [IMT]
--GO
EXEC sp_addrolemember N'DataManagement', N'Firstlook\DataManagement'
--GO

USE HAL
--GO
CREATE ROLE [DataManagement] AUTHORIZATION [dbo]
--GO
USE HAL
--GO
EXEC sp_addrolemember N'DataManagement', N'Firstlook\DataManagement'

--GO
USE HAL
GRANT EXEC ON dbo.ConfigureDealershipForMAX TO DataManagement
--GO                
USE IMT

GRANT EXEC ON dbo.SetTradeOrPurchase TO DataManagement
GRANT EXEC ON dbo.reQueue_RepriceExportEvents_AfterProcessorFailure TO DataManagement
GRANT EXEC ON dbo.reQueue_RepriceFailures TO DataManagement
GRANT EXEC ON dbo.Set_RepriceExportEvents_ConfirmedFailure TO DataManagement
--GO


*/