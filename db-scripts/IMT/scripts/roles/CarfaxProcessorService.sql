
IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'DATABASE_ROLE'
			AND name = 'CarfaxProcessorService'
		)

	CREATE ROLE [CarfaxProcessorService] AUTHORIZATION [dbo]

GO