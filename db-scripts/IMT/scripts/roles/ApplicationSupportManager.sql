
IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'DATABASE_ROLE'
			AND name = 'ApplicationSupportManager'
		)

	CREATE ROLE [ApplicationSupportManager] AUTHORIZATION [dbo]

GO



/*
USE [IMT]
--GO
EXEC sp_addrolemember N'ApplicationSupportManager', N'Firstlook\Dmaniar'
--GO
use [IMT]
--GO

GRANT UPDATE ON dbo.DMSExportBusinessUnitPreference TO [ApplicationSupportManager]
GRANT UPDATE ON dbo.DMSExportFrequency TO [ApplicationSupportManager]
GRANT UPDATE ON dbo.RepriceExport TO [ApplicationSupportManager]
GRANT UPDATE ON dbo.RepriceExportStatus TO [ApplicationSupportManager]

GRANT EXEC ON dbo.reQueue_RepriceFailures TO [ApplicationSupportManager]
GRANT EXEC ON dbo.Set_RepriceExportEvents_ConfirmedFailure TO [ApplicationSupportManager]
GRANT EXEC ON dbo.reQueue_RepriceExportEvents_AfterProcessorFailure TO [ApplicationSupportManager]

GRANT EXEC ON dbo.CopyInventoryListPriceToDMS TO [ApplicationSupportManager]
GRANT EXEC ON dbo.GetInventoryAdvertisementText TO ApplicationSupportManager
USE [Utility]
--GO
CREATE ROLE [ApplicationSupportManager] AUTHORIZATION [dbo]
--GO

EXEC sp_addrolemember N'ApplicationSupportManager', N'Firstlook\Dmaniar'
--GO

GRANT EXEC ON Utility.dbo.ITOperations_KPIs TO [ApplicationSupportManager]
--GO


USE [master]
--GO
CREATE LOGIN [FIRSTLOOK\fsantos] FROM WINDOWS WITH DEFAULT_DATABASE=[IMT]
--GO
USE [IMT]
--GO
CREATE USER [FIRSTLOOK\fsantos] FOR LOGIN [FIRSTLOOK\fsantos]
--GO
USE [IMT]
--GO
EXEC sp_addrolemember N'ApplicationSupportManager', N'FIRSTLOOK\fsantos'
--GO
USE [Utility]
--GO
CREATE USER [FIRSTLOOK\fsantos] FOR LOGIN [FIRSTLOOK\fsantos]
--GO
USE [Utility]
--GO
EXEC sp_addrolemember N'ApplicationSupportManager', N'FIRSTLOOK\fsantos'
--GO
*/