
CREATE PARTITION FUNCTION PF_VehicleStatus (TINYINT)
AS RANGE RIGHT FOR VALUES (1)
GO
CREATE PARTITION SCHEME PS_VehicleStatus
AS PARTITION PF_VehicleStatus TO (DATA, DATA)
GO

CREATE TABLE dbo.VehiclePartition(
	[VehicleID] [int] IDENTITY(1,1) NOT NULL,
	[VehicleStatusID] [TINYINT] NOT NULL CONSTRAINT DF_VehiclePartition__VehicleStatusID DEFAULT (1),
	[Vin] [varchar](17) NOT NULL,
	[VehicleYear] [int] NOT NULL,
	[Series] [varchar](50) NULL,
	[Engine] [varchar](35) NOT NULL,
	[DriveTrain] [varchar](10) NOT NULL,
	[Transmission] [varchar](20) NOT NULL,
	[CylinderCount] [int] NULL,
	[DoorCount] [int] NULL,
	[BaseColor] [varchar](50) NOT NULL,
	[InteriorColor] [varchar](50) NULL,
	[FuelType] [varchar](3) NOT NULL,
	[InteriorDescription] [varchar](50) NULL,
	[CreateDt] [smalldatetime] NOT NULL CONSTRAINT [DF_VehiclePartition__CreateDt]  DEFAULT (convert(smalldatetime,getdate())),
	[OriginalMake] [varchar](20) NULL,
	[OriginalModel] [varchar](25) NULL,
	[OriginalYear] [smallint] NULL,
	[OriginalTrim] [varchar](50) NULL,
	[OriginalBodyStyle] [varchar](50) NULL,
	[Audit_ID] [int] NOT NULL CONSTRAINT [DF_VehiclePartition__Audit_ID]  DEFAULT (0),
	[VehicleSourceID] [int] NOT NULL CONSTRAINT [DF_VehiclePartition__VehicleSourceID]  DEFAULT (1),
	[VIC] [char](10) NULL,
	[VehicleCatalogID] [int] NULL,
	[DateCreated]	SMALLDATETIME NOT NULL CONSTRAINT DF_Vehicle__DateCreated DEFAULT(GETDATE())
) ON PS_VehicleStatus([VehicleStatusID])
GO

ALTER TABLE [dbo].[VehiclePartition] ADD  CONSTRAINT [PK_VehiclePartition] PRIMARY KEY CLUSTERED 
(	[VehicleStatusID],
	[VehicleID] ASC
	
)

ALTER TABLE [dbo].[VehiclePartition] ADD  CONSTRAINT [UQ_VehiclePartition__VIN] UNIQUE NONCLUSTERED 
(	[VehicleStatusID],
	[Vin] ASC
) 

GO
ALTER TABLE [dbo].[VehiclePartition]  WITH CHECK ADD  CONSTRAINT [FK_VehiclePartition__VehicleSources] FOREIGN KEY([VehicleSourceID])
REFERENCES [dbo].[VehicleSources] ([VehicleSourceID])
GO
ALTER TABLE [dbo].[VehiclePartition] CHECK CONSTRAINT [FK_VehiclePartition__VehicleSources]
GO


SET IDENTITY_INSERT VehiclePartition ON

INSERT
INTO	VehiclePartition (VehicleID, VehicleStatusID, Vin, VehicleYear, VehicleTrim, VehicleEngine, VehicleDriveTrain, VehicleTransmission, CylinderCount, DoorCount, BaseColor, InteriorColor, FuelType, InteriorDescription, CreateDt, OriginalMake, OriginalModel, OriginalYear, OriginalTrim, OriginalBodyStyle, Audit_ID, VehicleSourceID, VIC, VehicleCatalogID)
SELECT	DISTINCT V.VehicleID, CASE WHEN I.InventoryActive = 1 OR A.AppraisalStatusID = 1 THEN 1 ELSE 0 end, Vin, VehicleYear, VehicleTrim, VehicleEngine, VehicleDriveTrain, VehicleTransmission, CylinderCount, DoorCount, BaseColor, InteriorColor, FuelType, InteriorDescription, CreateDt, OriginalMake, OriginalModel, OriginalYear, OriginalTrim, OriginalBodyStyle, V.Audit_ID, VehicleSourceID, VIC, VehicleCatalogID
FROM	tbl_Vehicle V
	LEFT JOIN Inventory I ON I.InventoryActive = 1 AND V.VehicleID = I.VehicleID
	LEFT JOIN Appraisals A ON A.AppraisalStatusID = 1 AND V.VehicleID = A.VehicleID

SET IDENTITY_INSERT VehiclePartition OFF


--	DROP tbl_Vehicle, rename VehiclePartition to tbl_Vehicle
