
SET NOCOUNT ON;

DECLARE @DateA DATETIME, @End DATETIME

SET @DateA = '2011-04-25'

SET @End = '2011-05-17'

WHILE (@DateA < @End)
BEGIN
    
    EXEC Transfers.Transfer#Record @Date = @DateA
    
    SET @DateA = DATEADD(DD, 1, @DateA)
    
END
