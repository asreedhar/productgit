
IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'SQL_USER'
			AND name = 'firstlook'
		)

	CREATE USER [firstlook] FOR LOGIN [firstlook] WITH DEFAULT_SCHEMA=[dbo]

GO

EXEC sp_addrolemember N'ApplicationUser', N'firstlook'
EXEC sp_addrolemember N'db_datareader', N'firstlook'
EXEC sp_addrolemember N'db_datawriter', N'firstlook'
GO