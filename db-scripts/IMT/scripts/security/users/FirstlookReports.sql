
IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'SQL_USER'
			AND name = 'FirstlookReports'
		)

	CREATE USER [FirstlookReports] FOR LOGIN [FirstlookReports] WITH DEFAULT_SCHEMA=[dbo]

GO

GRANT UPDATE on ReportCenterSessions to FirstlookReports
GO