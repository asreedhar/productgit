IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'FIRSTLOOK\CustomerOperations'
		)
		
	CREATE USER [FIRSTLOOK\CustomerOperations] FOR LOGIN [FIRSTLOOK\CustomerOperations]
GO


EXEC sp_addrolemember N'CustomerOperations', N'FIRSTLOOK\CustomerOperations'
GO
