IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'WINDOWS_USER'
			AND name = 'FIRSTLOOK\InsightReports'
		)
		
	CREATE USER [FIRSTLOOK\InsightReports] FOR LOGIN [FIRSTLOOK\InsightReports]
GO


EXEC sp_addrolemember 'db_datareader', 'FIRSTLOOK\InsightReports'
