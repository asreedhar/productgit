IF EXISTS(SELECT 1 FROM sys.server_principals SP WHERE type_desc = 'SQL_USER' AND name = 'merchandisingWebsite')
   AND NOT EXISTS (	SELECT  1
			FROM    sys.database_principals
			WHERE   type_desc = 'SQL_USER'
				AND name = 'merchandisingWebsite' ) 
	CREATE USER merchandisingWebsite FOR LOGIN merchandisingWebsite

IF EXISTS (	SELECT  1
			FROM    sys.database_principals
			WHERE   type_desc = 'SQL_USER'
				AND name = 'merchandisingWebsite' 
		) BEGIN
						
	EXEC sp_addrolemember 'db_datareader', 'merchandisingWebsite'
	EXEC sp_addrolemember 'MerchandisingUser', 'merchandisingWebsite'
	
END