
------------------------------------------------------------------------------------------------
--	ApplicationUser
--
--	Grant EXEC rights to all procedures, scalar functions, and synonyms.
--	Note that this script may give EXEC rights on table synonyms -- which is technically
--	incorrect but harmless.  Rev 2 of the script will parse the underlying object types
--	of the synonyms.
--
------------------------------------------------------------------------------------------------

IF NOT EXISTS (	SELECT	1
		FROM	sys.database_role_members RM
			JOIN sys.database_principals P1 ON RM.member_principal_id = P1.principal_id
			JOIN sys.database_principals P2 ON RM.role_principal_id = P2.principal_id
		WHERE	P1.NAME = 'firstlook'
			AND P1.type_desc = 'SQL_USER'
			AND P2.NAME = 'ApplicationUser'
			AND P2.type_desc = 'DATABASE_ROLE'
			)

	EXEC sp_addrolemember 'ApplicationUser', 'firstlook'

GO

GRANT EXECUTE TO ApplicationUser

GO

/*
EXEC sp_map_exec 
'GRANT EXEC ON ? TO ApplicationUser',
'SELECT	sc.NAME + "." + o.NAME from sys.sysobjects o JOIN sys.schemas sc ON o.uid = sc.schema_id where o.name not like "dt%" and type IN ( "P", "FN", "SN")'
--GO

*/
