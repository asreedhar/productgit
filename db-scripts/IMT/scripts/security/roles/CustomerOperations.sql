IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'CustomerOperations' AND type = 'R')
	CREATE ROLE [CustomerOperations]
GO

sp_addrolemember @rolename='db_datareader', @membername='CustomerOperations'
GO