GRANT UPDATE ON dbo.DMSExportBusinessUnitPreference TO [ApplicationSupportManager]
GRANT UPDATE ON dbo.DMSExportFrequency TO [ApplicationSupportManager]
GRANT UPDATE ON dbo.RepriceExport TO [ApplicationSupportManager]
GRANT UPDATE ON dbo.RepriceExportStatus TO [ApplicationSupportManager]

GRANT EXEC ON dbo.reQueue_RepriceFailures TO [ApplicationSupportManager]
GRANT EXEC ON dbo.Set_RepriceExportEvents_ConfirmedFailure TO [ApplicationSupportManager]
GRANT EXEC ON dbo.reQueue_RepriceExportEvents_AfterProcessorFailure TO [ApplicationSupportManager]

GRANT EXEC ON dbo.CopyInventoryListPriceToDMS TO [ApplicationSupportManager]

GRANT EXEC ON dbo.RepriceExport#SetStatusInProgress TO [ApplicationSupportManager]
GO