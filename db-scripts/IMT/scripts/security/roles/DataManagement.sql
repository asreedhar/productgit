IF NOT EXISTS (	SELECT	1
		FROM	sys.database_role_members RM
			JOIN sys.database_principals P1 ON RM.member_principal_id = P1.principal_id
			JOIN sys.database_principals P2 ON RM.role_principal_id = P2.principal_id
		WHERE	P1.NAME = 'Firstlook\DataManagement'
			AND P1.type_desc = 'WINDOWS_GROUP'
			AND P2.NAME = 'DataManagement'
			AND P2.type_desc = 'DATABASE_ROLE'
			)

	EXEC sp_addrolemember 'DataManagement', 'Firstlook\DataManagement'

GO
/*
GRANT EXEC ON dbo.SetTradeOrPurchase TO DataManagement
GRANT EXEC ON dbo.reQueue_RepriceExportEvents_AfterProcessorFailure TO DataManagement
GRANT EXEC ON dbo.reQueue_RepriceFailures TO DataManagement
GRANT EXEC ON dbo.Set_RepriceExportEvents_ConfirmedFailure TO DataManagement
GRANT EXEC ON dbo.ReactivateInventory TO DataManagement
*/
