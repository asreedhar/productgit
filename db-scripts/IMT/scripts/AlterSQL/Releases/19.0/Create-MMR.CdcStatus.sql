IF ( OBJECT_ID('[MMR].[CdcStatus]', 'U') IS NOT NULL ) 
    DROP TABLE [MMR].[CdcStatus]
GO
CREATE TABLE [MMR].[CdcStatus]
    (
      [LastRun] DATETIME2(0) NOT NULL
    )

INSERT  INTO [MMR].[CdcStatus]
        ( [LastRun] )
VALUES  ( DATEADD(DAY, -7, GETDATE()) )
