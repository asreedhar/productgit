USE [IMT]
GO

/****** Object:  Table [Distribution].[ADP_FieldTypes]    Script Date: 03/19/2014 16:17:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Distribution].[ADP_FieldTypes]') AND type in (N'U'))
DROP TABLE [Distribution].[ADP_FieldTypes]
GO

USE [IMT]
GO

/****** Object:  Table [Distribution].[ADP_FieldTypes]    Script Date: 03/19/2014 16:17:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Distribution].[ADP_FieldTypes](
	[FieldId] [int] NOT NULL,
	[Description] [varchar](500) NOT NULL,
	
 CONSTRAINT [PK_Distribution.ADP_FieldTypes] PRIMARY KEY CLUSTERED 
(
	[FieldId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]

GO

SET ANSI_PADDING OFF
GO

