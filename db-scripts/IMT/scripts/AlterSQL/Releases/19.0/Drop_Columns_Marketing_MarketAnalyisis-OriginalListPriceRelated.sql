
declare  @sql nvarchar(max)

select top 1 @sql = N'alter table Marketing.MarketAnalysisPreference drop constraint ['+dc.NAME+N']'
from sys.default_constraints dc
join  sys.columns c
    on  c.default_object_id = dc.object_id
where  
    dc.parent_object_id = OBJECT_ID('Marketing.MarketAnalysisPreference')
AND c.name = N'OriginalListPriceCalculationOverride'

if @@ROWCOUNT = 1
begin
	exec (@sql)
end

alter table marketing.MarketAnalysisPreference drop column OriginalListPriceCalculationOverride;
alter table marketing.MarketAnalysisVehiclePreference drop column OriginalListPriceOverride;
