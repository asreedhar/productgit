alter table Distribution.ADP_DealerPriceMapping Add FieldTypeId int
GO
Update Distribution.ADP_DealerPriceMapping set FieldTypeId=1
GO 

alter table Distribution.ADP_DealerPriceMapping
add constraint ADPDealerPriceMapping_ADPFieldType_FK FOREIGN KEY ( FieldTypeId ) references Distribution.ADP_FieldTypes([FieldId])
GO

alter table Distribution.ADP_DealerPriceMapping drop constraint [FK_Distribution.ADP_DealerPriceMapping_Distribution.ADP_PriceType]
GO