/****** Object:  Table [MMR].[MMR_ErrorType]    Script Date: 04/02/2014 19:44:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MMR].[MMR_ErrorType]') AND type in (N'U'))
DROP TABLE [MMR].[MMR_ErrorType]
GO

USE [IMT]
GO

/****** Object:  Table [MMR].[MMR_ErrorType]    Script Date: 04/02/2014 19:44:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [MMR].[MMR_ErrorType](
	[ErrorTypeId] [int] NOT NULL,
	[Description] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ErrorTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]

GO

SET ANSI_PADDING OFF
GO

