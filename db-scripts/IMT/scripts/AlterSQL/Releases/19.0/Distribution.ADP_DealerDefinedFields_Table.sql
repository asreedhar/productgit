USE [IMT]
GO

/****** Object:  Table [Distribution].[ADP_DealerDefinedFields]    Script Date: 03/19/2014 16:16:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Distribution].[ADP_DealerDefinedFields]') AND type in (N'U'))
DROP TABLE [Distribution].[ADP_DealerDefinedFields]
GO

USE [IMT]
GO

/****** Object:  Table [Distribution].[ADP_DealerDefinedFields]    Script Date: 03/19/2014 16:16:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Distribution].[ADP_DealerDefinedFields](
	[DealerDefinedFieldId] [int] NOT NULL,
	[Description] [varchar](500) NOT NULL,
 CONSTRAINT [PK_Distribution.ADP_DealerDefinedFields] PRIMARY KEY CLUSTERED 
(
	[DealerDefinedFieldId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]

GO

SET ANSI_PADDING OFF
GO

