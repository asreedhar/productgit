INSERT INTO [IMT].[MMR].[MMR_ErrorType]
           ([ErrorTypeId]
           ,[Description])
     VALUES
           (1
           ,'No MMR available for this MID.')
GO

insert into IMT.MMR.MMR_ErrorType(ErrorTypeId,Description)
values(2,'Invalid VIN. VIN may contain only letters and numbers')
GO

insert into IMT.MMR.MMR_ErrorType(ErrorTypeId,Description)
values(3,'Requested VIN was either not found or invalid')
GO