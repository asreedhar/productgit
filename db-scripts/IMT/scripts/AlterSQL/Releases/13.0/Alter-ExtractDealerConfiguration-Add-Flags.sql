ALTER TABLE Extract.DealerConfiguration ADD Flags BINARY(2) NOT NULL CONSTRAINT DF_ExtractDealerConfiguration__Flags DEFAULT (0x0000)

EXEC sp_SetColumnDescription 'Extract.DealerConfiguration.Flags', 'Collection of boolean flags as defined in Extract.DealerConfigurationFlag.  Currently limited to 32 (two-bytes) per extract destination'
GO

CREATE TABLE Extract.DealerConfigurationFlag (
	DestinationName		VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	Name			VARCHAR(50) NOT NULL,
	BitPosition		TINYINT NOT NULL,
	Description		VARCHAR(100) NULL,
	CONSTRAINT PK_ExtractDealerConfigurationFlagReference PRIMARY KEY CLUSTERED (DestinationName, Name)
	)
GO

ALTER TABLE Extract.DealerConfigurationFlag ADD CONSTRAINT FK_ExtractDealerConfigurationFlag__DestinationName FOREIGN KEY (DestinationName) REFERENCES Extract.Destination (Name)
GO

EXEC sp_SetTableDescription 'Extract.DealerConfigurationFlag', 'Reference table containing flag definitions for the flags stored in IMT.Extract.DealerConfiguration.Flags'
EXEC sp_SetColumnDescription 'Extract.DealerConfigurationFlag.DestinationName', 'Name of the extract destination associated with the flag'
EXEC sp_SetColumnDescription 'Extract.DealerConfigurationFlag.Name', 'Name of the flag (unique per extract destination)'
EXEC sp_SetColumnDescription 'Extract.DealerConfigurationFlag.BitPosition', 'Zero-based ordinal position within the Flags column'
EXEC sp_SetColumnDescription 'Extract.DealerConfigurationFlag.Description', 'Long description of the flag'
GO

INSERT
INTO	Extract.DealerConfigurationFlag (DestinationName, Name, BitPosition, Description)
SELECT	'OVE-Hendrick', 'HighlineDealer', 0, 'Flags if the dealer is a highline store; affects standard price adjustment'
UNION 
SELECT	'AULTec', 'UseMaxAdTransmission', 0, 'Specifies whether the AULtec GID Merchandising source passes through the MaxAd transmission'
	
GO

UPDATE	DC
SET	Flags = CAST(Flags | POWER(2, 0) AS BINARY(2))	-- both at bit position zero
FROM	Extract.DealerConfiguration DC
WHERE	DestinationName = 'OVE-Hendrick' AND ExternalIdentifier IN ('5326136','5005989','5000406','5320998','5006105','5320895','5316202','5259973','5006121','5006124','5320843','5000308','5000308','5288657','5003121','5150956')
	OR DestinationName = 'AULtec' AND BusinessUnitID IN (105437,105890,105891,105894,105893,105575,105761,105803,105767)


	