if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetColumnList]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetColumnList]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetSysObjects]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetSysObjects]
GO