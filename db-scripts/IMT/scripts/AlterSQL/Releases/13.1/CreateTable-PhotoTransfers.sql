USE [IMT]
GO
/****** Object:  Table [dbo].[PhotoTransfers]    Script Date: 10/30/2012 15:58:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhotoTransfers](
	[SrcInventoryID] [int] NOT NULL,
	[DstInventoryID] [int] NOT NULL,
	[QueuedDt] [datetime] NOT NULL,
	[StatusDt] [datetime] NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_PhotoTransfers] PRIMARY KEY CLUSTERED 
(
	[DstInventoryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When the transfer detected and queued' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PhotoTransfers', @level2type=N'COLUMN',@level2name=N'QueuedDt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When the transfer was processed' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PhotoTransfers', @level2type=N'COLUMN',@level2name=N'StatusDt'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Status code indicating result of transfer. See PhotoTranferStatus table for meaning.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PhotoTransfers', @level2type=N'COLUMN',@level2name=N'Status'
GO
ALTER TABLE [dbo].[PhotoTransfers]  WITH CHECK ADD  CONSTRAINT [FK_PhotoTransfers_DstInventoryID] FOREIGN KEY([DstInventoryID])
REFERENCES [dbo].[Inventory] ([InventoryID])
GO
ALTER TABLE [dbo].[PhotoTransfers] CHECK CONSTRAINT [FK_PhotoTransfers_DstInventoryID]
GO
ALTER TABLE [dbo].[PhotoTransfers]  WITH CHECK ADD  CONSTRAINT [FK_PhotoTransfers_PhotoTransfers] FOREIGN KEY([DstInventoryID])
REFERENCES [dbo].[PhotoTransfers] ([DstInventoryID])
GO
ALTER TABLE [dbo].[PhotoTransfers] CHECK CONSTRAINT [FK_PhotoTransfers_PhotoTransfers]
GO
ALTER TABLE [dbo].[PhotoTransfers]  WITH CHECK ADD  CONSTRAINT [FK_PhotoTransfers_PhotoTransfers1] FOREIGN KEY([DstInventoryID])
REFERENCES [dbo].[PhotoTransfers] ([DstInventoryID])
GO
ALTER TABLE [dbo].[PhotoTransfers] CHECK CONSTRAINT [FK_PhotoTransfers_PhotoTransfers1]
GO
ALTER TABLE [dbo].[PhotoTransfers]  WITH CHECK ADD  CONSTRAINT [FK_PhotoTransfers_Status] FOREIGN KEY([Status])
REFERENCES [dbo].[PhotoTransferStatus] ([Code])
GO
ALTER TABLE [dbo].[PhotoTransfers] CHECK CONSTRAINT [FK_PhotoTransfers_Status]