USE [IMT]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhotoTransferGroups](
	[GroupBusinessUnitId] [int] NOT NULL,
	[Enabled] [bit] NULL,
 CONSTRAINT [PK_PhotoTransferGroups] PRIMARY KEY CLUSTERED 
(
	[GroupBusinessUnitId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]

GO
ALTER TABLE [dbo].[PhotoTransferGroups]  WITH CHECK ADD  CONSTRAINT [FK_PhotoTransferGroups_GroupBusinessUnitId] FOREIGN KEY([GroupBusinessUnitId])
REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
ALTER TABLE [dbo].[PhotoTransferGroups] CHECK CONSTRAINT [FK_PhotoTransferGroups_GroupBusinessUnitId]