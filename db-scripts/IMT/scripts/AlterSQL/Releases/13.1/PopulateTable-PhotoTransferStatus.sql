insert into imt.dbo.photoTransferStatus (code, description)
	select 0, 'Queued'
	union
	select 1, 'Pending'
	union 
	select 2, 'Success'
	union
	select 3, 'No Source Photos'
	union
	select 4, 'Destination Has Photos'
	union select 5, 'Failed'
	select 6, 'Unknown Business Unit ID';
