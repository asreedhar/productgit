
CREATE TABLE Extract.DealerConfiguration#OVEHendrick#SellerGroup (
	SellerGroupID	TINYINT NOT NULL CONSTRAINT PK_ExtractDealerConfiguration#OVEHendrick#SellerGroup PRIMARY KEY CLUSTERED,
	SellerGroup	VARCHAR(100) NOT NULL,
	CONSTRAINT UQ_ExtractDealerConfiguration#OVEHendrick#SellerGroup UNIQUE NONCLUSTERED (SellerGroup)
	)

EXEC dbo.sp_SetTableDescription 'Extract.DealerConfiguration#OVEHendrick#SellerGroup',
	'OVE SellerGroup reference table'


CREATE TABLE Extract.DealerConfiguration#OVEHendrick#ExtendedProperties (
	BusinessUnitID	INT NOT NULL CONSTRAINT PK_ExtractDealerConfiguration#OVEHendrick#ExtendedProperties PRIMARY KEY CLUSTERED ,
	SellerGroupID	TINYINT NOT NULL,
	FranchiseMake	VARCHAR(50) NULL,
	CONSTRAINT FK_ExtractDealerConfiguration#OVEHendrick#ExtendedProperties__BusinessUnitID FOREIGN KEY (BusinessUnitID) REFERENCES dbo.BusinessUnit(BusinessUnitID),
	CONSTRAINT FK_ExtractDealerConfiguration#OVEHendrick#ExtendedProperties__SellerGroup FOREIGN KEY (SellerGroupID) REFERENCES Extract.DealerConfiguration#OVEHendrick#SellerGroup(SellerGroupID)
	)
GO

EXEC dbo.sp_SetTableDescription 'Extract.DealerConfiguration#OVEHendrick#ExtendedProperties',
	'OVE-Hendrick export-specific dealer configuration properties'

EXEC dbo.sp_SetColumnDescription 'Extract.DealerConfiguration#OVEHendrick#ExtendedProperties.SellerGroupID',
	'Specifies the OVE SellerGroup (via FK to Extract.DealerConfiguration#OVEHendrick#ExtendedProperties#SellerGroup).'

EXEC dbo.sp_SetColumnDescription 'Extract.DealerConfiguration#OVEHendrick#ExtendedProperties.FranchiseMake',
	'If set, this property adjusts the inventory age filter to eighty days (from fifty) for vehicles of the specified make.  These vehicles are the dealer''s top franchise/make that they want to retain for a bit longer before sending to auction.'
GO
			
INSERT
INTO	Extract.DealerConfiguration#OVEHendrick#SellerGroup
SELECT	1, 'HENDRICKTRADECLOSED'
UNION 
SELECT	2, 'HENDRICKKANSASTRAINING'
UNION 
SELECT	3, 'HENDRICKTHURSDAYTRAINING'
UNION 
SELECT	4, 'HENDRICKFRIDAYTRAINING'
GO

------------------------------------------------------------------------------------------------------------------------
--	Update the existing dealers
------------------------------------------------------------------------------------------------------------------------

INSERT
INTO	Extract.DealerConfiguration#OVEHendrick#ExtendedProperties (BusinessUnitID, SellerGroupID, FranchiseMake)
SELECT	DC.BusinessUnitID, SellerGroupID, FM.FranchiseMake
FROM	Extract.DealerConfiguration DC
	INNER JOIN Extract.DealerConfiguration#OVEHendrick#SellerGroup SG ON SG.SellerGroup = 'HENDRICKTRADECLOSED'
	LEFT JOIN (	SELECT	101735 AS BusinessUnitID, 'Porsche' AS FranchiseMake	-- this will be a export preference post 13.0.  can't use IMT.dbo.DealerFranchise since a dealer can have more than one
			UNION 
			SELECT	102007, 'Land Rover'
			) FM ON DC.BusinessUnitID = FM.BusinessUnitID
WHERE	DestinationName = 'OVE-Hendrick'
	AND Active = 1

------------------------------------------------------------------------------------------------------------------------
--	Define training dataset
------------------------------------------------------------------------------------------------------------------------


CREATE TABLE #TrainingSet (DealerName VARCHAR(100), DealershipIdentifier varchar(30), HighlineDealer BIT, SellerGroup VARCHAR(50))

INSERT INTO #TrainingSet
SELECT	'Hendrick Chevrolet Shawnee Mission' AS DealerName,'5004948' AS DealershipIdentifier, 0 AS HighlineDealer, 'HENDRICKKANSASTRAINING' AS SellerGroup
UNION SELECT 'Superior Buick Cadillac','5013198',1, 'HENDRICKKANSASTRAINING'
UNION SELECT 'Superior Acura','5002720',1, 'HENDRICKKANSASTRAINING'
UNION SELECT 'Superior Lexus North','5152049',1, 'HENDRICKKANSASTRAINING'
UNION SELECT 'Superior Lexus South','5003112',1, 'HENDRICKKANSASTRAINING'
UNION SELECT 'Hendrick Volvo of Kansas City ','5035873',0, 'HENDRICKKANSASTRAINING'
UNION SELECT 'Hendrick Toyota','5006237',0, 'HENDRICKKANSASTRAINING'
UNION SELECT 'Hendrick Nissan','5316149',0, 'HENDRICKKANSASTRAINING'

UNION SELECT 'Hendrick Buick AL','5300810', 0, 'HENDRICKTHURSDAYTRAINING'
UNION SELECT 'Hendrick CDJ AL','5304617', 0, 'HENDRICKTHURSDAYTRAINING'
UNION SELECT 'Dale Earnhardt Jr Chevrolet','5327239', 0, 'HENDRICKTHURSDAYTRAINING'
UNION SELECT 'Dale Earnhardt Jr Buick GMC Cadillac','5327237', 0, 'HENDRICKTHURSDAYTRAINING'
UNION SELECT 'Honda Cars of McKinney','5103084', 0, 'HENDRICKTHURSDAYTRAINING'

UNION SELECT 'Colonial Cadillac','5035398', 0, 'HENDRICKFRIDAYTRAINING'
UNION SELECT 'Colonial Chevrolet','5017307', 0, 'HENDRICKFRIDAYTRAINING'
UNION SELECT 'Darrell Waltrip Honda','5061992',1, 'HENDRICKFRIDAYTRAINING'
UNION SELECT 'Hendrick Honda-VA','5071946', 0, 'HENDRICKFRIDAYTRAINING'


------------------------------------------------------------------------------------------------------------------------
--	Insert training dealers.  
------------------------------------------------------------------------------------------------------------------------

INSERT
INTO	Extract.DealerConfiguration (BusinessUnitID, DestinationName, ExternalIdentifier, StartDate, Active, Flags)

SELECT	BU.BusinessUnitID, 'OVE-Hendrick', DealershipIdentifier, '11/6/2012', 0,
	Flags = CASE WHEN HighlineDealer = 1 THEN CAST(POWER(2, BitPosition) AS BINARY(2))
		     ELSE 0
		END 
FROM	#TrainingSet TS
	INNER JOIN IMT.dbo.BusinessUnit BU ON TS.DealerName = BU.BusinessUnit AND BU.BusinessUnitID <> 102282
	INNER JOIN Extract.DealerConfigurationFlag DCF ON 'OVE-Hendrick' = DCF.DestinationName AND DCF.Name = 'HighlineDealer'


INSERT
INTO	Extract.DealerConfiguration#OVEHendrick#ExtendedProperties (BusinessUnitID, SellerGroupID, FranchiseMake)

SELECT	BU.BusinessUnitID, SellerGroupID, NULL
FROM	#TrainingSet TS
	INNER JOIN IMT.dbo.BusinessUnit BU ON TS.DealerName = BU.BusinessUnit AND BU.BusinessUnitID <> 102282	-- remove the dup dealer
	INNER JOIN Extract.DealerConfiguration#OVEHendrick#SellerGroup SG ON TS.SellerGroup = SG.SellerGroup
	
GO

------------------------------------------------------------------------------------------------------------------------
--	Insert new flag
------------------------------------------------------------------------------------------------------------------------

INSERT 
INTO	Extract.DealerConfigurationFlag (DestinationName, Name, BitPosition, Description)
SELECT	'OVE-Hendrick', 'SuppressLoanerFilter', 1, 'Flags whether to supress the default loaner filter (by stock number pattern match)'				

GO

------------------------------------------------------------------------------------------------------------------------
--	Create multi-valued properties table.  Omitting the "Extended", as it's implied AND the name already is a bit 
--	long
------------------------------------------------------------------------------------------------------------------------

CREATE TABLE Extract.DealerConfiguration#MultiValueProperties (
	BusinessUnitID	INT NOT NULL,
	DestinationName	VARCHAR(100) NOT NULL, 
	Property	VARCHAR (50) NOT NULL,	
	Value		SQL_VARIANT NOT NULL,
	CONSTRAINT FK_ExtractDealerConfiguration#ExtendedProperties__DealerConfiguration FOREIGN KEY (BusinessUnitID, DestinationName) REFERENCES Extract.DealerConfiguration(BusinessUnitID,DestinationName),
	)
GO

CREATE CLUSTERED INDEX IXC_ExtractDealerConfiguration#MultiValueProperties ON Extract.DealerConfiguration#MultiValueProperties(BusinessUnitID, DestinationName)

EXEC dbo.sp_SetTableDescription 'Extract.DealerConfiguration#MultiValueProperties',
	'Multi-valued dealer configuration properties (omitting the #Extended, as that is implied.)  No PK until we add an ordinal, if ever required.'

EXEC dbo.sp_SetColumnDescription 'Extract.DealerConfiguration#MultiValueProperties.Property',
	'Property Name.  At some point, we may wish to create a property lookup table.  Not today'
GO

EXEC dbo.sp_SetColumnDescription 'Extract.DealerConfiguration#MultiValueProperties.Value',
	'Property Value.  It''s a SQL variant, so don''t forget to CAST it.'
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Extract.DealerConfiguration#SetFlag') AND type in (N'P', N'PC'))
DROP PROCEDURE Extract.DealerConfiguration#SetFlag
GO

CREATE PROC Extract.DealerConfiguration#SetFlag
@BusinessUnitID 	INT, 
@DestinationName 	VARCHAR(100), 
@FlagName 		VARCHAR(50), 
@Value			BIT
AS

SET NOCOUNT ON 

UPDATE	DC
SET	Flags = CASE WHEN @Value = 1 THEN CAST(Flags | POWER(2, BitPosition) AS BINARY(2))
		     WHEN @Value = 0 THEN CAST(Flags & ~POWER(2, BitPosition) AS BINARY(2))
		END     
FROM	Extract.DealerConfiguration DC
	INNER JOIN Extract.DealerConfigurationFlag DCF ON DC.DestinationName = DCF.DestinationName AND DCF.Name = @FlagName
WHERE	DC.BusinessUnitID = @BusinessUnitID

IF @@ROWCOUNT = 0
	RAISERROR('Extract.DealerConfiguration#SetFlag Failed for BusinessUnitID=%i, DestinationName=%s, Flag=%s', 16, 1, @BusinessUnitID, @DestinationName, @FlagName)

GO

------------------------------------------------------------------------------------------------------------------------
--	Update export configuration and add new properties
------------------------------------------------------------------------------------------------------------------------
BEGIN TRY
EXEC IMT.Extract.DealerConfiguration#SetFlag @BusinessUnitID = 101119, -- int
        @DestinationName = 'OVE-Hendrick', -- varchar(100)
        @FlagName = 'HighlineDealer', -- varchar(50)
        @Value = 1 -- bit


EXEC IMT.Extract.DealerConfiguration#SetFlag @BusinessUnitID = 100941, -- int
        @DestinationName = 'OVE-Hendrick', -- varchar(100)
        @FlagName = 'HighlineDealer', -- varchar(50)
        @Value = 0 -- bit
END TRY
BEGIN CATCH
	PRINT 'IGNORE ERROR FOR SCRATCH BUILDS'
END CATCH	        
        
INSERT
INTO	Extract.DealerConfiguration#MultiValueProperties (BusinessUnitID, DestinationName, Property, Value)

SELECT	BU.BusinessUnitID, 'OVE-Hendrick', 'HighlineMake', CAST(HighlineMake AS VARCHAR(50))
FROM	IMT.dbo.BusinessUnit BU
	INNER JOIN IMT.Extract.DealerConfiguration DC ON BU.BusinessUnitID = DC.BusinessUnitID AND DestinationName = 'OVE-Hendrick' AND DC.Flags & POWER(2, 0) = POWER(2, 0)
	LEFT  JOIN (	SELECT	'Performance Automall - Hendrick' AS BusinessUnit, 'Porsche' AS HighlineMake
			UNION 
			SELECT	'Superior Acura','Acura'	
			UNION 
			SELECT	'Hendrick Acura', 'Acura'
			UNION 
			SELECT	'Hendrick BMW-MINI', 'BMW'
			UNION 
			SELECT	'Hendrick BMW-MINI', 'MINI'
			UNION 
			SELECT	'Rick Hendrick BMW Charleston', 'BMW'	
			UNION 
			SELECT	'Rick Hendrick BMW Charleston', 'MINI'
			UNION 
			SELECT	'Hendrick BMW Northlake', 'BMW'	
			UNION 
			SELECT	'Performance BMW', 'BMW'	
			UNION 
			SELECT	'Land Rover Charlotte', 'Land Rover'	
			UNION 
			SELECT	'Superior Lexus South', 'Lexus'	
			UNION 
			SELECT	'Hendrick Lexus', 'Lexus'	
			UNION 
			SELECT	'Superior Lexus North', 'Lexus'	
			UNION 
			SELECT	'Lexus of Charleston', 'Lexus'	
			UNION 
			SELECT	'Lexus NorthLake', 'Lexus'	
			UNION 
			SELECT	'Hendrick Motors of Charlotte', 'Mercedes-Benz'	
			UNION 
			SELECT	'Mercedes-Benz of Northlake', 'Mercedes-Benz'	
			UNION 
			SELECT	'Hendrick Porsche', 'Porsche'	
			UNION 
			SELECT	'Darrell Waltrip Honda', 'Volvo'	
			UNION 
			SELECT	'Volvo of Charleston', 'Volvo'	
			UNION 
			SELECT	'Hendrick Volvo of Kansas City', 'Volvo'	
			) HM ON BU.BusinessUnit = HM.BusinessUnit
			
WHERE	BU.BusinessUnitID <> 102282	-- de-dup
