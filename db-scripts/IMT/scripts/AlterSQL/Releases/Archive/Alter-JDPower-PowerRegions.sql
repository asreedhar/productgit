IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_JDPowerDealerMarketArea_JDPowerPowerRegionID]') AND parent_object_id = OBJECT_ID(N'[dbo].[JDPowerDealerMarketArea]'))
ALTER TABLE [dbo].[JDPowerDealerMarketArea] DROP CONSTRAINT [FK_JDPowerDealerMarketArea_JDPowerPowerRegionID]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[JDPowerDealerMarketArea]') AND type in (N'U'))
DROP TABLE [dbo].[JDPowerDealerMarketArea]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[JDPowerPowerRegion]') AND type in (N'U'))
DROP TABLE [dbo].[JDPowerPowerRegion]

CREATE TABLE [dbo].[JDPower_PowerRegion] (
	PowerRegionID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT [PK_JDPower_PowerRegion_PowerRegionID] PRIMARY KEY CLUSTERED
	, [Name] VARCHAR(240) NOT NULL
)
GO
CREATE TABLE [dbo].[JDPower_DealerMarketArea] (
	DealerMarketAreaID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT [PK_JDPower_DealerMarketArea_DealerMarketAreaID] PRIMARY KEY CLUSTERED
	, PowerRegionID INT NOT NULL
		CONSTRAINT [FK_JDPower_DealerMarketArea_PowerRegionID]
		FOREIGN KEY REFERENCES JDPower_PowerRegion(PowerRegionID)
	, [Name] VARCHAR(200) NOT NULL
)
GO