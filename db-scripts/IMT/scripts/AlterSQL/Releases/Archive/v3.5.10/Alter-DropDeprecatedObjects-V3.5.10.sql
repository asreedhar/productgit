if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleClass]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[VehicleClass]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleCombination]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[VehicleCombination]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleOrigin]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[VehicleOrigin]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleSegment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[VehicleSegment]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_VehicleToSegmentSub1]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_VehicleToSegmentSub1]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleLocatorContactDealerEvent]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[VehicleLocatorContactDealerEvent]
GO
