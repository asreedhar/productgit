INSERT
INTO	PrintAdvertiserTextOptionDefault (PrintAdvertiserID, PrintAdvertiserTextOptionID, DisplayOrder, Status)
SELECT	PrintAdvertiserId, TEMP.*
FROM	PrintAdvertiser_ThirdPartyEntity  ptp
	CROSS JOIN (	SELECT  1 PrintAdvertiserTextOptionID, 0 DisplayOrder, 1 Status
			UNION
			SELECT  2, 1, 1
			UNION
			SELECT  3, 2, 1
			UNION
			SELECT  4, 3, 1
			UNION
			SELECT  5, 4, 1
			UNION
			SELECT  6, 5, 1
			UNION
			SELECT  7, 6, 1
			) TEMP

WHERE	ptp.PrintAdvertiserID NOT IN (	SELECT PrintAdvertiserID FROM PrintAdvertiserTextOptionDefault)

GO
