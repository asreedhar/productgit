SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[WindowStickerTemplate] (
	[WindowStickerTemplateId] [int] IDENTITY(1,1) NOT NULL ,
	[WindowStickerTemplateName] [varchar](100) NOT NULL ,
)
GO

INSERT INTO dbo.WindowStickerTemplate VALUES ('Lithia')
INSERT INTO dbo.WindowStickerTemplate VALUES ('Tuttle')
INSERT INTO dbo.WindowStickerTemplate VALUES ('Kbb')

-- Add WindowStickerTemplate to dealerPref 
if not exists (select * from syscolumns
	where id=object_id('dbo.DealerPreference') and name='WindowStickerTemplate')

ALTER TABLE dbo.DealerPreference ADD WindowStickerTemplate [int] NOT NULL DEFAULT (0)
GO

-- set up current guys with new window sticker info
Update dbo.DealerPreference 
set WindowStickerTemplate = 1
where businessUnitId in 
(
select du.businessUnitId
from dbo.DealerUpgrade du
where DealerUpgradeCD=6
and du.active=1
and (endDate IS NULL 
OR (
endDate IS NOT NULL
and endDate > GetDate()
))
)

GO

delete from 
dbo.DealerUpgrade
where dealerUpgradeCD=6

delete from dbo.lu_DealerUpgrade
where dealerUpgradeCD=6

GO
