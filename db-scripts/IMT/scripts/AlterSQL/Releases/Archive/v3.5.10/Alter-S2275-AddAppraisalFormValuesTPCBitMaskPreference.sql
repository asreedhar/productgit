ALTER TABLE dbo.DealerPreference
ADD AppraisalFormValuesTPCBitMask INT NOT NULL
	CONSTRAINT DF_DealerPreference_AppraisalFormValuesTPCBitMask DEFAULT(674) 
-- 674 = 001010100010 = nada Trade-in, BB avg, KBB wholesale, Galves wholesale
-- the bit correlating to the specific books is the ThirdPartyCategoryID from ThirdPartyCategories
GO

-- If a dealer has the kbb trade-in upgrade turned on - then the default kbb value is trade in 
-- so instead of 674, it should be 1570 = 011000100010 = nada Trade-in, BB avg, KBB Trade-In, Galves wholesale
UPDATE	dbo.DealerPreference 
SET		AppraisalFormValuesTPCBitMask = 1570
WHERE   businessUnitID IN ( SELECT 	BusinessUnitID 
				FROM 	DealerUpgrade 
				WHERE	DealerUpgradeCD = 17
					AND Active = 1  ) 
GO

-- need to add the include mileage adjustment so the user can save defaults
ALTER TABLE dbo.DealerPreference
ADD AppraisalFormIncludeMileageAdjustment TINYINT NOT NULL
	CONSTRAINT DF_DealerPreference_AppraisalFormIncludeMileageAdjustment DEFAULT(1) 
GO

ALTER TABLE dbo.AppraisalFormOptions
ADD CategoriesToDisplayBitMask INT NULL
GO


UPDATE	AFO
SET	CategoriesToDisplayBitMask = U.CategoriesToDisplayBitMask
FROM	AppraisalFormOptions AFO
	JOIN (	SELECT	U.AppraisalFormID,
			CategoriesToDisplayBitMask = SUM(POWER(2, CAST(U.Value AS INT) -1))
		FROM	(	SELECT	AppraisalFormID,
					S.Value
				FROM	AppraisalFormOptions AFO
					JOIN Appraisals A ON AFO.AppraisalID = A.AppraisalID
					CROSS APPLY dbo.Split(AFO.PrimaryBooksToDisplay,',') S
				WHERE	A.AppraisalID > 0
					AND RIGHT(PrimaryBooksToDisplay,1) = ',' 
					AND S.Value <> ''
				UNION 

				SELECT	AppraisalFormID,
					S.Value
				FROM	AppraisalFormOptions AFO
					JOIN Appraisals A ON AFO.AppraisalID = A.AppraisalID
					CROSS APPLY dbo.Split(AFO.SecondaryBooksToDisplay,',') S
				WHERE	A.AppraisalID > 0
					AND RIGHT(SecondaryBooksToDisplay,1) = ',' 
					AND S.Value <> ''
			) U 
		GROUP
		BY	U.AppraisalFormID
		) U ON AFO.AppraisalFormID = U.AppraisalFormID
GO

--migration from primaryBooksToDisplay and SEcondaryBooksToDisplay to CategoriesToDisplayBitMask
ALTER TABLE dbo.AppraisalFormOptions
DROP COLUMN PrimaryBooksToDisplay
GO

ALTER TABLE dbo.appraisalFormOptions
DROP COLUMN SecondaryBooksToDisplay
GO

