if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_SquishVin]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view dbo.vw_SquishVin
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.vw_Style') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view dbo.vw_Style
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.vw_Colors') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view dbo.vw_Colors
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.SingleVINDecode') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure dbo.SingleVINDecode
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.usp_CreateEdmundsIndexes') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure dbo.usp_CreateEdmundsIndexes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.CreateEdmundsPrimaryKeys') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure dbo.CreateEdmundsPrimaryKeys
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.fn_squishvin') and xtype in (N'FN', N'IF', N'TF'))
drop function dbo.fn_squishvin
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.lu_Edmunds_Colors_New') AND type in (N'U'))
DROP TABLE dbo.lu_Edmunds_Colors_New
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.lu_Edmunds_Colors_Used') AND type in (N'U'))
DROP TABLE dbo.lu_Edmunds_Colors_Used
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.lu_Edmunds_Squishvin') AND type in (N'U'))
DROP TABLE dbo.lu_Edmunds_Squishvin
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.lu_Edmunds_Style_New') AND type in (N'U'))
DROP TABLE dbo.lu_Edmunds_Style_New
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.lu_Edmunds_Style_Used') AND type in (N'U'))
DROP TABLE dbo.lu_Edmunds_Style_Used
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.lu_Edmunds_Style_Used') AND type in (N'U'))
DROP TABLE dbo.lu_Edmunds_Style_Used
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.VehicleSaleCommissions') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.VehicleSaleCommissions
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.fn_VehicleSegmentSubSegment') and xtype in (N'FN', N'IF', N'TF'))
drop function dbo.fn_VehicleSegmentSubSegment
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.vw_VehicleToSegmentSub1') and xtype in (N'FN', N'IF', N'TF'))
drop function dbo.vw_VehicleToSegmentSub1
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.FK_map_VehicleTypeToSegment_lu_VehicleSegment') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.map_VehicleTypeToSegment DROP CONSTRAINT FK_map_VehicleTypeToSegment_lu_VehicleSegment
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.FK_map_VehicleSubTypeToSubSegment_lu_vehicleSubSegment') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.map_VehicleSubTypeToSubSegment DROP CONSTRAINT FK_map_VehicleSubTypeToSubSegment_lu_vehicleSubSegment
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.FK_map_VehicleSubTypeToSubSegment_lu_VehicleSubType') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.map_VehicleSubTypeToSubSegment DROP CONSTRAINT FK_map_VehicleSubTypeToSubSegment_lu_VehicleSubType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.FK_map_VehicleTypeToSegment_lu_VehicleType') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.map_VehicleTypeToSegment DROP CONSTRAINT FK_map_VehicleTypeToSegment_lu_VehicleType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.lu_VehicleSubType') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.lu_VehicleSubType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.lu_VehicleType') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.lu_VehicleType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.map_VehicleSubTypeToSubSegment') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.map_VehicleSubTypeToSubSegment
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.map_VehicleTypeToSegment') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.map_VehicleTypeToSegment
GO


if exists (select * from dbo.sysobjects where id = object_id(N'dbo.lu_VehicleSegment') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.lu_VehicleSegment
GO


if exists (select * from dbo.sysobjects where id = object_id(N'dbo.map_FLTypeClassToSegments') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.map_FLTypeClassToSegments
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetVehicleBodyStyle]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetVehicleBodyStyle]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetVehicleBodyStyleResults]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetVehicleBodyStyleResults]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_CreateEdmundsIndexes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_CreateEdmundsIndexes]
GO

-- ModelBodyStyleOverrides


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetDisplayBodyType]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetDisplayBodyType]
GO