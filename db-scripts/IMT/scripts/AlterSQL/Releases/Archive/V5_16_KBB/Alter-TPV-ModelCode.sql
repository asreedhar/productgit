ALTER TABLE dbo.ThirdPartyVehicles ALTER COLUMN ModelCode VARCHAR(5) NULL
GO
CREATE INDEX IX_ThirdPartyOptions__OptionKeyOptionName ON dbo.ThirdPartyOptions(OptionKey,OptionName) WITH (online=ON)
GO 