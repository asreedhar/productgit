print '126 Alter Script'
go

ALTER TABLE [dbo].[tbl_CIAPreferences]
	ADD PowerzoneGroupingThreshold INT NULL
GO

UPDATE [dbo].[tbl_CIAPreferences]
	SET PowerzoneGroupingThreshold = 10
GO

ALTER TABLE [dbo].[tbl_CIAPreferences]
	ALTER COLUMN PowerzoneGroupingThreshold INT NOT NULL
GO

CREATE TABLE [dbo].[tbl_CIAPowerZoneItem] (
	[CIASummaryId] [int] NOT NULL ,
	[CIAClassTypeId] [int] NOT NULL ,
	[GroupingDescriptionId] [int] NOT NULL ,
) ON [DATA]
GO

ALTER TABLE [dbo].[tbl_CIAPowerZoneItem] ADD 
	CONSTRAINT [FK_tbl_CIAPOwerZoneItem_tbl_CIASummary] FOREIGN KEY 
	(
		[CIASummaryId]
	) REFERENCES [dbo].[tbl_CIASummary] (
		[CIASummaryId]
	)
GO

ALTER TABLE [dbo].[tbl_CIAPowerZoneItem] ADD 
	CONSTRAINT [FK_tbl_CIAPowerZoneItem_GroupingDescription] FOREIGN KEY 
	(
		[GroupingDescriptionId]
	) REFERENCES [dbo].[GroupingDescription] (
		[GroupingDescriptionId]
	)
GO

CREATE TABLE [dbo].[lu_CIASectionType]
(
	CIASectionTypeId INT NOT NULL,
	CIASectionTypeDescription VARCHAR(20) NOT NULL
)
GO

ALTER TABLE [dbo].[lu_CIASectionType] WITH NOCHECK ADD 
	CONSTRAINT [PK_lu_CIASectionType] PRIMARY KEY  NONCLUSTERED 
	(
		[CIASectionTypeId]
	)  ON [IDX] 
GO

INSERT INTO [dbo].[lu_CIASectionType] VALUES( 1, 'Power Zone' )
INSERT INTO [dbo].[lu_CIASectionType] VALUES( 2, 'Winners' )
INSERT INTO [dbo].[lu_CIASectionType] VALUES( 3, 'Good Bets' )
INSERT INTO [dbo].[lu_CIASectionType] VALUES( 4, 'Managers Choice' )
GO

sp_rename 'tbl_CIAPowerZoneItem', 'tbl_CIAGroupingItem'
GO

ALTER TABLE [dbo].[tbl_CIAGroupingItem]
	ADD CIASectionTypeId INT NULL
GO

UPDATE [dbo].[tbl_CIAGroupingItem]
	SET CIASectionTypeId = 0
GO

ALTER TABLE [dbo].[tbl_CIAGroupingItem]
	ALTER COLUMN CIASectionTypeId INT NOT NULL
GO

CREATE TABLE [dbo].[map_FLTypeClassToSegments] (
	[FLTypeClassID] [smallint] IDENTITY (1, 1) NOT NULL ,
	[VehicleSegmentID] [tinyint] NOT NULL ,
	[VehicleSubSegmentID] [tinyint] NOT NULL 
) ON [DATA]
GO

ALTER TABLE [dbo].[map_FLTypeClassToSegments] ADD 
	CONSTRAINT [PK_map_FLTypeClassToSegments] PRIMARY KEY  CLUSTERED 
	(
		[FLTypeClassID]
	)  ON [DATA] 
GO

insert
into	map_FLTypeClassToSegments (VehicleSegmentID, VehicleSubSegmentID)
select 	vehiclesegmentid,VehicleSubSegmentID
from 	lu_FLTypeClass TC
	join (	select	vehiclesegmentid, VehicleSubSegmentID, vehiclesegmentdesc + '-' + VehicleSubSegmentDESC TypeClass
		from	lu_VehicleSegment S
			cross join lu_VehicleSubSegment
		) S on upper(TC.FLTypeClassDesc) = S.TypeClass
order
by	vehiclesegmentid, VehicleSubSegmentID
GO

create table dbo.tbl_VehicleSegmentOverrides
(	VehicleSegmentID		tinyint not null,
	VehicleSubSegmentID		tinyint not null,
	VehicleSegmentIDOverride	tinyint not null,
	VehicleSubSegmentIDOverride	tinyint not null
)
go
ALTER TABLE [dbo].[tbl_VehicleSegmentOverrides] ADD 
	CONSTRAINT [PK_tbl_VehicleSegmentOverrides] PRIMARY KEY  CLUSTERED 
	(
		[VehicleSegmentID],
		[VehicleSubSegmentID]
	)  ON [DATA] 
GO

insert
into	tbl_VehicleSegmentOverrides
select	6, 15, 6, 17	-- WAGON, SPORTY -> 6, 17 WAGON,STANDARD 
union	
select	6, 6, 6, 17	-- WAGON, SMALL	-> 6, 17 WAGON,STANDARD 
union
select	6, 4, 6, 17	-- WAGON, MEDIUM -> 6, 17 WAGON,STANDARD 
union
select	1, 2, 6, 17 	-- COUPE, FULLSIZE -> 1, 4  COUPE, FULLSIZE
union
select	2, 15, 2, 6	-- SEDAN, SPORTY -> 2, 6  SEDAN, SMALL
union
select	6, 2, 6, 17	-- WAGON, FULLSIZE -> 6, 17 WAGON,STANDARD 

GO

ALTER TABLE [dbo].[tbl_CIAClassTypeDetail] drop
	CONSTRAINT [FK_tbl_CIAClassTypeDetail_lu_FLTypeClass] 
GO
	
ALTER TABLE [dbo].[tbl_CIAClassTypeDetail] ADD 
	CONSTRAINT [FK_tbl_CIAClassTypeDetail_map_FLTypeClassToSegments] FOREIGN KEY 
	(
		[CIAClassTypeId]
	) REFERENCES [dbo].[map_FLTypeClassToSegments] (
		[FLTypeClassID]
	)
GO
drop table lu_FLTypeClass
GO
create function dbo.ToMixedCase(@string varchar(100))
returns varchar(100)
as
begin
	return upper(left(@string,1)) + lower(substring(@string,2,99))
end
go


create view dbo.vw_lu_FLTypeClass
--------------------------------------------------------------------------------
--
--	Lookup for Type Classification defined in map_FLTypeClassToSegments
--	Deprecates lu_FLTypeClass
--	
---History----------------------------------------------------------------------
--	
--	WGH	4/16/2004	Initial design/development
--
--------------------------------------------------------------------------------	
as

select	FLTypeClassID, dbo.ToMixedCase(VehicleSegmentDesc)  + '-' + dbo.ToMixedCase(VehicleSubSegmentDesc) FLTypeClassDesc

from	map_FLTypeClassToSegments CXS
	join lu_VehicleSegment S on CXS.VehicleSegmentID = S.VehicleSegmentID
	join lu_VehicleSubSegment SS on CXS.VehicleSubSegmentID = SS.VehicleSubSegmentID

GO


create view dbo.vw_GroupingToFLTypeClassID
------------------------------------------------------------------------------------------------
--
--	INS-1522
--	1.	List all the groupings from edmunds with model years within the last 
--		6 years. [ | (current year - model year) | < 6 ]
--	..
--	3.	The groupings should only be for the class-type.
--	
---History--------------------------------------------------------------------------------------
--	
--	WGH	4/16/2004	Initial design/development
--		4/19/2004	Added segment override table and missing ABS on where clause	
--	
------------------------------------------------------------------------------------------------	
as

select	distinct MMG.GroupingDescriptionID, CXS.FLTypeClassID
from 	vw_Style S
	join lu_VehicleType VT on S.vehicle_type = VT.VehicleTypeDesc
	join lu_VehicleSubType VST on S.vehicle_subtype = VST.VehicleSubTypeDesc
	join map_VehicleTypeToSegment TS on VT.VehicleTypeID = TS.VehicleTypeID
	join map_VehicleSubTypeToSubSegment TSS on VST.VehicleSubTypeID = TSS.VehicleSubTypeID
	left join tbl_VehicleSegmentOverrides VSO on TS.VehicleSegmentID = VSO.VehicleSegmentID and TSS.VehicleSubSegmentID = VSO.VehicleSubSegmentID
	join map_FLTypeClassToSegments CXS on isnull(VSO.VehicleSegmentIDOverride, TS.VehicleSegmentID) = CXS.VehicleSegmentID 
						and isnull(VSO.VehicleSubSegmentIDOverride, TSS.VehicleSubSegmentID) = CXS.VehicleSubSegmentID
	join MakeModelGrouping MMG on S.make = MMG.Make and S.model = MMG.model
where	abs(year(current_timestamp)- S.year) < 6 

GO

ALTER TABLE [dbo].[tbl_CIAGroupingItem]
	ADD CIAGroupingItemId INT IDENTITY(1,1) NOT NULL
GO

ALTER TABLE [dbo].[tbl_CIAGroupingItem] WITH NOCHECK ADD 
	CONSTRAINT [PK_tbl_CIAGroupingItem] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAGroupingItemId]
	)  ON [IDX] 
GO

ALTER VIEW dbo.VehicleAttributes 
as
select	VA.VehicleId,
	VA.VehicleSegmentID,
	VA.VehicleSubSegmentID,
	CXS.FLTypeClassID as VehicleTypeClassID
from 	tbl_VehicleAttributes VA
	left join tbl_VehicleSegmentOverrides VSO on VA.VehicleSegmentID = VSO.VehicleSegmentID and VA.VehicleSubSegmentID = VSO.VehicleSubSegmentID
	join map_FLTypeClassToSegments CXS on isnull(VSO.VehicleSegmentIDOverride, VA.VehicleSegmentID) = CXS.VehicleSegmentID 
						and isnull(VSO.VehicleSubSegmentIDOverride, VA.VehicleSubSegmentID) = CXS.VehicleSubSegmentID
GO

ALTER TABLE [dbo].[DealerPreference]
	ADD BookOutPreferenceSecondId INT NULL
GO
	
ALTER TABLE [dbo].[tbl_CIAPreferences]
	ADD GoodBetsGroupingThreshold INT NULL
GO

UPDATE 	tbl_CIAPreferences
SET 	GoodBetsGroupingThreshold = 5
GO

ALTER TABLE [dbo].[tbl_CIAPreferences] ADD 
	CONSTRAINT [DF_tbl_CIAPreferences_GoodBetsGrouingThreshold] DEFAULT (5) FOR [GoodBetsGroupingThreshold]
GO

ALTER TABLE [dbo].[tbl_CIAPreferences]
	ALTER COLUMN GoodBetsGroupingThreshold INT NOT NULL
GO
							

CREATE TABLE [dbo].[map_BusinessUnitToMarketDealer] (
	[BusinessUnitID] [int] NOT NULL ,
	[DealerNumber] [int] NOT NULL 
) ON [DATA]
GO

ALTER TABLE [dbo].[map_BusinessUnitToMarketDealer] ADD 
	CONSTRAINT [PK_map_BusinessUnitToMarketDealer] PRIMARY KEY  CLUSTERED 
	(
		[BusinessUnitID],
		[DealerNumber]
	)  ON [DATA] 
GO

insert into map_BusinessUnitToMarketDealer
select	100135,70354
union
select	100136,70357
union 
select	100138,72238
union 
select	100139,70356
union 
select	100141,70355
union 
select	100147,18078
union 
select	100148,13589
GO

CREATE function dbo.ToDate
--------------------------------------------------------------------------------
--
--	Removes the time from a datetime -- usefull when using gt and lt 
--	operators to compare date ranges
--
---Parmeters--------------------------------------------------------------------
(
@datetime datetime
)
---History----------------------------------------------------------------------
--	
--	WGH	4/13/2004	Initial design/development
--
--------------------------------------------------------------------------------	
returns datetime
as
begin
	return(cast(convert(varchar(10),@datetime,101) as datetime))
end
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE function dbo.IsInBasisPeriod 
--------------------------------------------------------------------------------
--
--	Returns a 1 if the passed date is in the given basis period (calc'd from
--	the passed end date.)
--
---Parmeters--------------------------------------------------------------------
(
	@date 		as datetime, 
	@end_date	as datetime,
	@basis_period 	as int		-- # of weeks in the basis period

)
---History----------------------------------------------------------------------
--	
--	WGH	4/13/2004	Initial design/development
--
--------------------------------------------------------------------------------	
returns bit

as
begin
return 
       case when dbo.ToDate(@date) > dateadd(wk, - @basis_period, dbo.ToDate(@end_date)) 
		 and dbo.ToDate(@date) <= dbo.ToDate(@end_date) 
	    then 1 
	    else 0 
       end
end


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


CREATE function dbo.GetBusinessUnitsOverVehicleSaleThreshold
--------------------------------------------------------------------------------------------------------
--
--	Returns the set pf dealers where there are enough sales data to determine the "market" via 
--	customer zip 
--
---History----------------------------------------------------------------------------------------------
--	
--	WGH	4/15/2004	Initial design/development
--		4/20/2004	Comment: may work better as an inline instead of a multi-statement 
--				table-valued function; seems to work fine so will leave in
--				
--------------------------------------------------------------------------------------------------------
(
@vehicle_sale_threshold	float = 0.80,
@EndBasisDate	datetime
)
returns @table TABLE (BusinessUnitID int)
as
begin
	insert @table
	select 	I.BusinessUnitID 

	from 	Inventory I
		join tbl_CIABasisPeriod BP on I.BusinessUnitID = BP.BusinessUnitID
		join tbl_VehicleSale VS on I.InventoryID = VS.InventoryID
		left join VehicleSaleCustomerDetail SCD on VS.InventoryID = SCD.InventoryID
	where	dbo.IsInBasisPeriod(VS.DealDate, @EndBasisDate, BP.Days/7.0) = 1
	group
	by	I.BusinessUnitID
	having	cast(sum(case when not CustomerZip is null then BP.Weight/100.0 else 0 end) as float) / cast(count(*) as float) >= @vehicle_sale_threshold
	return
end

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE view dbo.vw_WeightedBusinessUnitSalesAggregate
as
select 	0 Source, I.BusinessUnitID, sum(BP.Weight/100.0) BusinessUnitCount
from 	Inventory I
	join tbl_CIABasisPeriod BP on I.BusinessUnitID = BP.BusinessUnitID
	join tbl_VehicleSale VS on I.InventoryID = VS.InventoryID
where	dbo.IsInBasisPeriod(VS.DealDate, getdate(), BP.Days/7.0) = 1
group
by	I.BusinessUnitID

/*	Restore this once the MARKET data is re-integrated

union all
select	1 Source, MBD.BusinessUnitID, sum(Cnt * BP.Weight/100.0) 
from	MARKET..Market_Summary MS 
	join map_BusinessUnitToMarketDealer MBD on MS.DealerNumber = MBD.DealerNumber
	join tbl_CIABasisPeriod BP on MBD.BusinessUnitID = BP.BusinessUnitID
where	dbo.IsInBasisPeriod(MS.ReportDT, getdate(), BP.Days/7.0) = 1		-- MS is rolled up
group
by	MBD.BusinessUnitID

*/

GO

alter table dbo.VehicleSaleCommissions
alter column salespersonid varchar(25) null
go

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_VehicleSegmentSubSegment2]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_VehicleSegmentSubSegment]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE function dbo.fn_VehicleSegmentSubSegment (@VIN varchar(17), @intQueryType int)
returns int
as
begin
	declare @ReturnValue int

	if len(@VIN) <> 17
		begin
			set @ReturnValue = 99
			return (@ReturnValue)
		end
 
	if @intQueryType = 1
		select	@ReturnValue = CASE
				when sq.make = 'Mazda' and sq.model = 'MPV' then 5 
				when sq.make = 'ISUZU' and sq.model = 'AMIGO' then 5
				else seg.vehiclesegmentid
			END 

		from	vw_squishvin sq 
			join vw_style vw on sq.ed_style_id = vw.ed_style_id
			join lu_vehicleType ty on vw.Vehicle_Type = ty.VehicleTypeDesc
			join map_vehicletypetosegment map1 on  ty.vehicletypeid = map1.vehicletypeid
			join lu_vehiclesegment seg on map1.vehiclesegmentid = seg.vehiclesegmentid
		where	substring (@VIN,1,8) + substring (@VIN,10,2) = sq.squish_vin
	else
		select @ReturnValue = CASE
				when sq.make = 'Mazda' and sq.model = 'MPV' then 5 
				when sq.make = 'ISUZU' and sq.model = 'AMIGO' then 5	
				else seg.VehicleSubSegmentid
				END

		from 	vw_squishvin sq
			join vw_style vw on sq.ed_style_id = vw.ed_style_id 
			join lu_vehicleSubType ty on vw.Vehicle_SubType = ty.VehicleSubTypeDesc
			join map_VehicleSubTypeToSubSegment map1 on  ty.VehicleSubTypeid = map1.vehiclesubtypeid
			join lu_VehicleSubSegment seg on map1.vehiclesubsegmentid = seg.vehiclesubsegmentid
		where	substring (@VIN,1,8) + substring (@VIN,10,2) = sq.squish_vin
	
	return (isnull(@ReturnValue,99))
end
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
CREATE TABLE [dbo].[lu_InventoryVehicleLight] (
	[InventoryVehicleLightCD] [tinyint] NOT NULL ,
	[InventoryVehicleLightDesc] [varchar] (30) NOT NULL,
 	CONSTRAINT [PK_lu_InventoryVehicleLight] PRIMARY KEY  NONCLUSTERED 
	(
		[InventoryVehicleLightCD]
	) WITH  FILLFACTOR = 100  ON [IDX] 
) ON [DATA]
GO

insert into lu_InventoryVehicleLight
(InventoryVehicleLightCD, InventoryVehicleLightDesc)
select
0, 'NOT PROCESSED'
union
select
1,'RED'
union
select
2,'YELLOW'
union
select
3,'GREEN'



