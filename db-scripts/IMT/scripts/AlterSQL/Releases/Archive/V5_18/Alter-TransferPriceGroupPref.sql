ALTER TABLE dbo.DealerGroupPreference
ADD IncludeTransferPricing BIT NOT NULL
	CONSTRAINT [DF_DealerGroupPreference_IncludeTransferPricing] DEFAULT(0)
GO