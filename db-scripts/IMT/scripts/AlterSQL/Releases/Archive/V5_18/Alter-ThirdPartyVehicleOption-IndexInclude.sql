IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ThirdPartyVehicleOptionValues]') AND name = N'IX_ThirdPartyVehicleOptionValues__ThirdPartyVehicleOptionID_Value')
DROP INDEX [IX_ThirdPartyVehicleOptionValues__ThirdPartyVehicleOptionID_Value] ON [dbo].[ThirdPartyVehicleOptionValues] WITH ( ONLINE = OFF )
GO

CREATE NONCLUSTERED INDEX [IX_ThirdPartyVehicleOptionValues__ThirdPartyVehicleOptionID] ON [dbo].[ThirdPartyVehicleOptionValues] 
(
	[ThirdPartyVehicleOptionID] ASC
)INCLUDE ([Value]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
