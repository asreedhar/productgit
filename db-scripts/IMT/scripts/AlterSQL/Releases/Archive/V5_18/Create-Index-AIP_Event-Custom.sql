------------------------------------------------------------------------------------------------
-- 	Custom index to prevent full scans of AIP_Event
--	Addresses a query seen to run a couple times per minute during normal operational hours
------------------------------------------------------------------------------------------------

CREATE INDEX IX_AIP_Event__ThirdPartyEntityIDEndDate ON AIP_Event(ThirdPartyEntityID,EndDate)
GO