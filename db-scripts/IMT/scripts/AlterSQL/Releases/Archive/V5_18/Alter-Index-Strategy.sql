ALTER TABLE dbo.tbl_DealerValuation DROP CONSTRAINT PK_tbl_DealerValuation
GO


CREATE CLUSTERED INDEX IX_tbl_DealerValuation__DealerIdDate ON tbl_DealerValuation
(	DealerId,
	Date DESC
	) 
		
GO

ALTER TABLE dbo.tbl_DealerValuation ADD CONSTRAINT PK_tbl_DealerValuation PRIMARY KEY NONCLUSTERED (DealerValuationId)
GO


DROP INDEX Inventory.IX_Inventory__BusinessUnitID_EtAl

CREATE INDEX IX_Inventory__BusinessUnitID_EtAl ON Inventory(BusinessUnitID, InventoryActive, InventoryType, StockNumber)

GO

/*

Do MEMBER NEXT

*/