--- nk FB: 2176
--- Smart Cars have a 3 digit make code. ThirdPartyVehicles used to restrict the column size to 2. 
--- I made it 5 now - same as ModelCode

ALTER TABLE dbo.ThirdPartyVehicles
ALTER COLUMN MakeCode [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL

GO