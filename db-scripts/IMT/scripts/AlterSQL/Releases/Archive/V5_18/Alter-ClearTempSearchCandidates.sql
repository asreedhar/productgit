DECLARE @SearchCandidateListIDs TABLE (SearchCandidateListID INT NOT NULL)

INSERT INTO @SearchCandidateListIDs(SearchCandidateListID)
SELECT SearchCandidateListID
FROM dbo.SearchCandidateList
WHERE SearchCandidateListTypeID = 3

DECLARE @SearchCandidateIDs TABLE (SearchCandidateID INT NOT NULL)

INSERT INTO @SearchCandidateIDs (SearchCandidateID)
SELECT SearchCandidateID
FROM dbo.SearchCandidate C
JOIN @SearchCandidateListIDs L
	ON C.SearchCandidateListID = L.SearchCandidateListID

DECLARE @SearchCandidateOptionIDs TABLE (SearchCandidateOptionID INT NOT NULL)

INSERT INTO @SearchCandidateOptionIDs(SearchCandidateOptionID)
SELECT SearchCandidateOptionID
FROM dbo.SearchCandidateOption O
JOIN @SearchCandidateIDs C
	ON O.SearchCandidateID = C.SearchCandidateID

DELETE A
FROM dbo.SearchCandidateOptionAnnotation A
JOIN @SearchCandidateOptionIDs O 
	ON A.SearchCandidateOptionID = O.SearchCandidateOptionID

DELETE O1 
FROM dbo.SearchCandidateOption O1
JOIN @SearchCandidateOptionIDs O2
	ON O1.SearchCandidateOptionID = O2.SearchCandidateOptionID

DELETE A
FROM dbo.SearchCandidateCIACategoryAnnotation A
JOIN @SearchCandidateIDs C
	ON A.SearchCandidateID = C.SearchCandidateID

DELETE C1
FROM dbo.SearchCandidate C1
JOIN @SearchCandidateIDs C2
	ON C1.SearchCandidateID = C2.SearchCandidateID

DELETE L1
FROM dbo.SearchCandidateList L1
JOIN @SearchCandidateListIDs L2
	ON L1.SearchCandidateListID = L2.SearchCandidateListID