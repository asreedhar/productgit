CREATE TABLE TransferPriceUnitCostValue (
	TransferPriceUnitCostValueID INT NOT NULL
		CONSTRAINT [PK_TransferPriceUnitCostValue] PRIMARY KEY
	, [Description] VARCHAR(100) NOT NULL
)
GO

CREATE TABLE dbo.TransferPriceInventoryAgeRange (
	TransferPriceInventoryAgeRangeID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT [PK_TransferPriceInventoryAgeRange] PRIMARY KEY
	, Low INT NOT NULL
	, High INT NOT NULL
	, Description AS (
			CAST(Low AS VARCHAR) + ' to ' + 
			CASE WHEN HIGH = 2147483647 THEN 'Unlimited' ELSE CAST(High AS VARCHAR) END +
			' days'
		)
	, CONSTRAINT [CK_TransferPriceInventoryAgeRange_LowHigh]
		CHECK (Low > -1 AND Low < 2147483647 AND Low <= High)
	, CONSTRAINT [UK_TransferPriceInventoryAgeRange]
		UNIQUE NONCLUSTERED (Low, High)
)
GO

CREATE TABLE dbo.DealerTransferPriceUnitCostRule (
	DealerTransferPriceUnitCostRuleID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT [PK_DealerTransferPriceUnitCostRule] PRIMARY KEY NONCLUSTERED
	, BusinessUnitID INT NOT NULL 
		CONSTRAINT [FK_DealerTransferPriceUnitCostRule_BusinessUnitID] FOREIGN KEY 
		REFERENCES dbo.BusinessUnit(BusinessUnitID)
	, TransferPriceUnitCostValueID INT NOT NULL
		CONSTRAINT [FK_DealerTransferPriceUnitCostRule_TransferPriceUnitCostValueID] FOREIGN KEY
		REFERENCES dbo.TransferPriceUnitCostValue(TransferPriceUnitCostValueID)
	, TransferPriceInventoryAgeRangeID INT NULL
		CONSTRAINT [FK_DealerTransferPriceUnitCostRule_TransferPriceInventoryAgeRangeID] FOREIGN KEY
		REFERENCES dbo.TransferPriceInventoryAgeRange(TransferPriceInventoryAgeRangeID)
	, CONSTRAINT [UK_DealerTransferPriceUnitCostRule]
		UNIQUE NONCLUSTERED (BusinessUnitID, TransferPriceUnitCostValueID, TransferPriceInventoryAgeRangeID)
)
GO

CREATE CLUSTERED INDEX [IX_DealerTransferPriceUnitCostRule_BusinessUnitID]
ON dbo.DealerTransferPriceUnitCostRule(BusinessUnitID)
GO

CREATE NONCLUSTERED INDEX [IX_DealerTransferPriceUnitCostRule_TransferPriceUnitCostValueID]
ON dbo.DealerTransferPriceUnitCostRule(TransferPriceUnitCostValueID)
GO

CREATE NONCLUSTERED INDEX [IX_DealerTransferPriceUnitCostRule_TransferPriceInventoryAgeRangeID]
ON dbo.DealerTransferPriceUnitCostRule(TransferPriceInventoryAgeRangeID)
GO

CREATE TABLE dbo.DealerTransferAllowRetailOnlyRule (
	DealerTransferAllowRetailOnlyRuleID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT [PK_DealerTransferAllowRetailOnlyRule] PRIMARY KEY NONCLUSTERED
	, BusinessUnitID INT NOT NULL 
		CONSTRAINT [FK_DealerTransferAllowRetailOnlyRule_BusinessUnitID] FOREIGN KEY 
		REFERENCES dbo.BusinessUnit(BusinessUnitID)
	, TransferPriceInventoryAgeRangeID INT NULL
		CONSTRAINT [FK_DealerTransferAllowRetailOnlyRule_TransferPriceInventoryAgeRangeID] FOREIGN KEY
		REFERENCES dbo.TransferPriceInventoryAgeRange(TransferPriceInventoryAgeRangeID)
	, CONSTRAINT [UK_DealerTransferAllowRetailOnlyRule]
		UNIQUE NONCLUSTERED (BusinessUnitID, TransferPriceInventoryAgeRangeID)
)
GO

CREATE CLUSTERED INDEX [IX_DealerTransferAllowRetailOnlyRule_BusinessUnitID]
ON dbo.DealerTransferAllowRetailOnlyRule(BusinessUnitID)
GO

INSERT INTO dbo.TransferPriceUnitCostValue (TransferPriceUnitCostValueID, Description)
SELECT 1, 'Any'
UNION ALL SELECT 2, 'Not Null'
UNION ALL SELECT 3, 'Less than or Equal to Unit Cost'
GO

INSERT INTO dbo.TransferPriceInventoryAgeRange(Low, High)
SELECT 0, 6
UNION ALL SELECT 7, 29
UNION ALL SELECT 30, 2147483647  --high is largest int value
UNION ALL SELECT 0, 29 --the default range ForRetailOnly flag 
GO

ALTER TABLE dbo.Inventory
ADD TransferPrice DECIMAL(9,2) NULL
GO

ALTER TABLE dbo.Inventory
ADD TransferForRetailOnly BIT NOT NULL
	CONSTRAINT [DF_Inventory_TransferForRetailOnly] DEFAULT(1)
GO

ALTER TABLE dbo.Inventory
ADD TransferForRetailOnlyEnabled BIT NOT NULL
	CONSTRAINT [DF_Inventory_TransferForRetailOnlyEnabled] DEFAULT(1)
GO

ALTER TABLE dbo.Inventory
ADD AgeInDays AS DATEDIFF(DD, InventoryReceivedDate, getdate())
GO

ALTER TABLE dbo.AppraisalActions
ADD TransferPrice DECIMAL(9,2) NULL
GO

ALTER TABLE dbo.AppraisalActions
ADD TransferForRetailOnly BIT NOT NULL DEFAULT(0)
GO

CREATE TABLE dbo.InventoryTransferPriceHistory (
	InventoryTransferHistoryID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT [PK_InventoryTransferHistory] PRIMARY KEY
	, InventoryID INT NOT NULL
	, TransferPrice_Old DECIMAL(9,2) NULL
	, TransferPrice_New DECIMAL(9,2) NULL
	, Created DATETIME NOT NULL 
		CONSTRAINT [DF_InventoryTransferHistory_Created] DEFAULT getdate()
	, CreatedBy VARCHAR(100) NOT NULL
)
GO

--The FK prevents MAX from logging out correctly!
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SystemErrors_HalSessions]') AND parent_object_id = OBJECT_ID(N'[dbo].[SystemErrors]'))
ALTER TABLE [dbo].[SystemErrors] DROP CONSTRAINT [FK_SystemErrors_HalSessions]
GO