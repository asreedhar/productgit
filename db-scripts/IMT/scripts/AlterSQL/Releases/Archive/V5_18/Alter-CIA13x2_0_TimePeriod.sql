UPDATE dbo.tbl_CIAPreferences 
SET CiaStoreTargetInventoryBasisPeriodId = 8
	, CiaCoreModelDeterminationBasisPeriodId = 8
	, CiaPowerzoneModelTargetInventoryBasisPeriodId = 8
	, CiaCoreModelYearAllocationBasisPeriodId = 8
WHERE BusinessUnitID NOT IN (
	SELECT D.BusinessUnitID
	FROM dbo.BusinessUnit D
	JOIN dbo.CIASummary S
		ON D.BusinessUnitID = S.BusinessUnitID
	JOIN dbo.CIABodyTypeDetails B
		ON S.CIASummaryID = B.CIASummaryID
	JOIN dbo.CIAGroupingItems G
		ON B.CIABodyTypeDetailID = G.CIABodyTypeDetailID
	JOIN dbo.CIAGroupingItemDetails I
		ON G.CIAGroupingItemID = I.CIAGroupingItemID
	WHERE I.CIAGroupingItemDetailTypeID BETWEEN 1 AND 3
	AND I.DateCreated >= DATEADD(month, -2, getdate())
	GROUP BY D.BusinessUnitID
)

ALTER TABLE [dbo].[tbl_CIAPreferences] DROP CONSTRAINT [DF_CiaCoreModelDeterminationBasisPeriodId]
ALTER TABLE [dbo].[tbl_CIAPreferences] ADD  CONSTRAINT [DF_CiaCoreModelDeterminationBasisPeriodId]  DEFAULT (8) FOR [CiaCoreModelDeterminationBasisPeriodId]

ALTER TABLE [dbo].[tbl_CIAPreferences] DROP CONSTRAINT [DF_CiaCoreModelYearAllocationBasisPeriodId]
ALTER TABLE [dbo].[tbl_CIAPreferences] ADD  CONSTRAINT [DF_CiaCoreModelYearAllocationBasisPeriodId]  DEFAULT (8) FOR [CiaCoreModelYearAllocationBasisPeriodId]

ALTER TABLE [dbo].[tbl_CIAPreferences] DROP CONSTRAINT [DF_CiaPowerzoneModelTargetInventoryBasisPeriodId]
ALTER TABLE [dbo].[tbl_CIAPreferences] ADD  CONSTRAINT [DF_CiaPowerzoneModelTargetInventoryBasisPeriodId]  DEFAULT (8) FOR [CiaPowerzoneModelTargetInventoryBasisPeriodId]

ALTER TABLE [dbo].[tbl_CIAPreferences] DROP CONSTRAINT [DF_CiaStoreTargetInventoryBasisPeriodId]
ALTER TABLE [dbo].[tbl_CIAPreferences] ADD  CONSTRAINT [DF_CiaStoreTargetInventoryBasisPeriodId]  DEFAULT (8) FOR [CiaStoreTargetInventoryBasisPeriodId]