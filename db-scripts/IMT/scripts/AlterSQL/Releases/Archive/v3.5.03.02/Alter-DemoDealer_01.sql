/*

Used to persist demo dealer data

*/

CREATE TABLE DemoDealerBaseline_Inventory
 (
   InventoryID       int            not null
    constraint PK_DemoDealerBaseline_Inventory
     primary key clustered
    constraint FK_DemoDealerBaseline_Inventory__Inventory
     foreign key references Inventory (InventoryID)
  ,PlanReminderDate  smalldatetime  null
 )

 GO
 