CREATE TABLE dbo.AppraisalStatus (
	[AppraisalStatusID] [tinyint] CONSTRAINT [PK_AppraisalStatus] PRIMARY KEY NOT NULL,
	[Description] [varchar](50) CONSTRAINT [UK_AppraisalStatus_Description] UNIQUE NOT NULL
)

INSERT INTO dbo.AppraisalStatus ([AppraisalStatusID] ,[Description])
VALUES (1 ,'Active')

INSERT INTO dbo.AppraisalStatus ([AppraisalStatusID] ,[Description])
VALUES (2 ,'Inactive')

-- 1. Add the column, 
-- 2. populate defaults to active,
-- 3. Update records that should be inactive
ALTER TABLE dbo.Appraisals
ADD AppraisalStatusID tinyint NOT NULL
CONSTRAINT [DF_Appraisal_AppraisalStatus]
DEFAULT (1)
GO
ALTER TABLE dbo.Appraisals
ADD CONSTRAINT [FK_Appraisal_AppraisalStatus] 
FOREIGN KEY (AppraisalStatusID) REFERENCES dbo.AppraisalStatus(AppraisalStatusID)

-- An Appraisal is Inactive if it has a corresponding Inventory record
UPDATE Appraisals
SET AppraisalStatusID = 2
WHERE AppraisalID in (
	SELECT DISTINCT A.AppraisalID
	FROM	Appraisals A 
		JOIN tbl_Vehicle V ON A.VehicleID = V.VehicleID
		JOIN Inventory I ON I.BusinessUnitID = A.BusinessUnitID AND V.VehicleID = I.VehicleID
	WHERE I.InventoryType = 2
)
