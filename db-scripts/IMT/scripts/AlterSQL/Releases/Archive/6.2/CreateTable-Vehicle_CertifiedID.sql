IF (OBJECT_ID('dbo.VehicleCertifiedID') IS NULL)
CREATE TABLE dbo.VehicleCertifiedID(
	InventoryID int NOT NULL,
	CertifiedID varchar(50) NOT NULL,
	ModifiedByUser varchar(100) NOT NULL,
	ModifiedDate datetime NOT NULL,
 CONSTRAINT PK_VehicleCertifiedID PRIMARY KEY CLUSTERED 
(
	InventoryID ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON DATA
) ON DATA
GO


IF (OBJECT_ID('dbo.FK_VehicleCertifiedID_Inventory') IS NULL)
ALTER TABLE dbo.VehicleCertifiedID  WITH CHECK ADD  CONSTRAINT FK_VehicleCertifiedID_Inventory FOREIGN KEY(InventoryID)
REFERENCES dbo.Inventory (InventoryID)
GO


IF (OBJECT_ID('dbo.FK_VehicleCertifiedID_VehicleCertifiedID') IS NULL)
ALTER TABLE dbo.VehicleCertifiedID  WITH CHECK ADD  CONSTRAINT FK_VehicleCertifiedID_VehicleCertifiedID FOREIGN KEY(InventoryID)
REFERENCES dbo.VehicleCertifiedID (InventoryID)
GO

