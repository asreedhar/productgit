/****** Object:  Table [dbo].[RepriceInventory]    Script Date: 02/09/2007 13:56:12 ******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RepriceHistory](
	[RepriceHistoryID] [int] IDENTITY (1,1) NOT NULL,
	[InventoryID] [int] NOT NUll,
	[RepricedBy] varchar(128) NOT NULL,
	[OldListPrice] decimal(8,2) CONSTRAINT [DF_RepriceHistory_OldListPrice] DEFAULT (0) NOT NULL,       
	[NewListPrice] decimal(8,2) CONSTRAINT [DF_RepriceHistory_NewListPrice] DEFAULT (0) NOT NULL,       
	[Date] [smalldatetime] NOT NULL,
	CONSTRAINT [PK_RepriceHistory] PRIMARY KEY
	(
		[RepriceHistoryID]
	),
	CONSTRAINT [FK_RepriceHistory_Inventory] FOREIGN KEY
	(
		[InventoryID]
	) REFERENCES [dbo].[Inventory]
	(
		[InventoryID]
	)         
)
GO
 
CREATE TABLE [dbo].[RepriceExportStatus](
	[RepriceExportStatusID] [int] IDENTITY (1,1) NOT NULL,
	[Description] varchar(20),
	CONSTRAINT [PK_RepriceExportStatus] PRIMARY KEY
	(
		[RepriceExportStatusID]
	),
)
GO

INSERT INTO [dbo].[RepriceExportStatus]
           ([Description])
     VALUES
           ('InQueue')
GO
INSERT INTO [dbo].[RepriceExportStatus]
           ([Description])
     VALUES
           ('InProgress')
GO
INSERT INTO [dbo].[RepriceExportStatus]
           ([Description])
     VALUES
           ('Sent')
GO
INSERT INTO [dbo].[RepriceExportStatus]
           ([Description])
     VALUES
           ('Failed')
GO


CREATE TABLE [dbo].[RepriceExport](
	[RepriceExportID] [int] IDENTITY (1,1) NOT NULL,            
	[RepriceHistoryID] [int] NOT NULL,
	[RepriceExportStatusID] [int] NOT NULL,
	[InternetAdvertiserID] [int] NOT NULL,
	[FailureReason] varchar(256),
	[FailureCount] [int] CONSTRAINT [DF_RepriceExport_FailureCount] DEFAULT (0) NOT NULL,
	[DateSent] [smalldatetime] NULL,
	[StatusModifiedDate] [smalldatetime] CONSTRAINT [DF_RepriceExport_StatusModifiedDate] DEFAULT getdate() NOT NULL,
	CONSTRAINT [PK_RepriceExport] PRIMARY KEY
	(
		[RepriceExportID]
	),  
	CONSTRAINT [FK_RepriceExport_RepriceHistory] FOREIGN KEY
	(
		[RepriceHistoryID]
	) REFERENCES [dbo].[RepriceHistory]
	(
		[RepriceHistoryID]
	),
	CONSTRAINT [FK_RepriceExport_RepriceExportStatus] FOREIGN KEY
	(
		[RepriceExportStatusID]
	) REFERENCES [dbo].[RepriceExportStatus]
	(
		[RepriceExportStatusID]
	),
	CONSTRAINT [FK_RepriceExport_InternetAdvertiserID] FOREIGN KEY
	(
		[InternetAdvertiserID]
	) REFERENCES [dbo].[InternetAdvertiser_ThirdPartyEntity]
	(
		[InternetAdvertiserID]
	)
)
GO

CREATE TRIGGER RepriceExportStatusModifiedUpdate
ON RepriceExport
FOR UPDATE
AS IF UPDATE(RepriceExportStatusID)
	UPDATE RE
	SET StatusModifiedDate = getdate()
	FROM inserted i
	JOIN RepriceExport RE on i.RepriceExportID = RE.RepriceExportID 
GO
 
SET ANSI_PADDING OFF
