


--------------------------------------------------------------------------------
--	DELETE THE OLD ATC ACCESS GROUPS
--	CAN'T RELY ON A JOIN TO ATC..AccessGroups 
--------------------------------------------------------------------------------

/*
DELETE	MAG
FROM	tbl_MemberATCAccessGroup MAG
	JOIN ATC..AccessGroup AG ON MAG.AccessGroupID = AG.AccessGroupID
WHERE	AG.DatasourceID = 12
*/

DELETE	tbl_MemberATCAccessGroup 
WHERE	AccessGroupID <= 126



/*
DELETE	DAG
FROM	tbl_DealerATCAccessGroups DAG
	JOIN ATC..AccessGroup AG ON DAG.AccessGroupID = AG.AccessGroupID
WHERE	AG.DatasourceID = 12
*/


DELETE	tbl_DealerATCAccessGroups 
WHERE	AccessGroupID <= 126

GO


INSERT
INTO	tbl_DealerATCAccessGroups (BusinessUnitID, AccessGroupId, AccessGroupPriceId, AccessGroupActorId, AccessGroupValue)
SELECT 	DF.BusinessUnitID, AG.AccessGroupID, 1, 3, 1
FROM	dbo.DealerPreference DP 			
	JOIN dbo.DealerFranchise DF ON DP.BusinessUnitID = DF.BusinessUnitID
	JOIN dbo.Franchise F ON DF.FranchiseID = F.FranchiseID
	JOIN [ATC].dbo.AccessGroup AG ON F.FranchiseDescription = AG.FranchiseDescription	-- FRANCHISE-ONLY
	JOIN [ATC].dbo.Datafeed DFD ON AG.DatasourceID = DFD.DatafeedID				-- RECTIFY THE COLUMN NAMES
	JOIN [ATC].dbo.OnlineAuction OA ON DFD.OnlineAuctionID = OA.OnlineAuctionID
	
WHERE	DP.ATCEnabled = 1
	AND OA.OnlineAuctionID = 1
UNION ALL
SELECT 	DP.BusinessUnitID, AG.AccessGroupID, 1, 3, 1
FROM	dbo.DealerPreference DP 			
	JOIN [ATC].dbo.AccessGroup AG ON AG.Public_Group = 1				-- PUBLIC GROUP(S)
	JOIN [ATC].dbo.Datafeed DFD ON AG.DatasourceID = DFD.DatafeedID			-- RECTIFY THE COLUMN NAMES
	JOIN [ATC].dbo.OnlineAuction OA ON DFD.OnlineAuctionID = OA.OnlineAuctionID
	
WHERE	DP.ATCEnabled = 1
	AND AG.Public_Group = 1
	AND OA.OnlineAuctionID =  1
