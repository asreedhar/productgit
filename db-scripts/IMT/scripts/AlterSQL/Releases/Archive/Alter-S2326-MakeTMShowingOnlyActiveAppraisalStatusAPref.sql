ALTER TABLE dbo.DealerPreference
ADD ShowInactiveAppraisals TINYINT NOT NULL
	CONSTRAINT DF_DealerPreference_ShowInactiveAppraisals DEFAULT(0)
GO