/****** Object:  Table [Marketing].[MarketListingPreference]    Script Date: 12/04/2009 09:33:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[MarketListingPreference]') AND type in (N'U'))
BEGIN
CREATE TABLE [Marketing].[MarketListingPreference](
	[MarketListingPreferenceId] [int] IDENTITY(1,1) NOT NULL,
	[OwnerId] [int] NULL,
	[IsDisplayed] [bit] NOT NULL,
	[ShowDealerName] [bit] NOT NULL,
	[ShowCertifiedIndicator] [bit] NOT NULL,
	[MileageDeltaBelow] [int] NOT NULL,
	[MileageDeltaAbove] [int] NOT NULL,
	[PriceDeltaAbove] [int] NOT NULL,
	[PriceDeltaBelow] [int] NOT NULL,
	[InsertUserId] [int] NULL,
	[InsertDate] [datetime] NULL,
	[UpdateUserId] [int] NULL,
	[UpdateDate] [datetime] NULL,
	[Version] [timestamp] NOT NULL,
 CONSTRAINT [PK_MarketListingPreference] PRIMARY KEY CLUSTERED 
(
	[MarketListingPreferenceId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA],
 CONSTRAINT [UQ_MarketListingPreference_OwnerId] UNIQUE NONCLUSTERED 
(
	[OwnerId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
END
GO
SET IDENTITY_INSERT [Marketing].[MarketListingPreference] ON
INSERT [Marketing].[MarketListingPreference] ([MarketListingPreferenceId], [OwnerId], [IsDisplayed], [ShowDealerName], [ShowCertifiedIndicator], [MileageDeltaBelow], [MileageDeltaAbove], [PriceDeltaAbove], [PriceDeltaBelow], [InsertUserId], [InsertDate], [UpdateUserId], [UpdateDate]) VALUES (1, NULL, 1, 1, 1, 1000, 1000, 1000, 1000, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [Marketing].[MarketListingPreference] OFF
/****** Object:  ForeignKey [FK_Marketing_MarketListingPreference_Member_InsertUser]    Script Date: 12/04/2009 09:33:34 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Marketing].[FK_Marketing_MarketListingPreference_Member_InsertUser]') AND parent_object_id = OBJECT_ID(N'[Marketing].[MarketListingPreference]'))
ALTER TABLE [Marketing].[MarketListingPreference]  WITH CHECK ADD  CONSTRAINT [FK_Marketing_MarketListingPreference_Member_InsertUser] FOREIGN KEY([InsertUserId])
REFERENCES [dbo].[Member] ([MemberID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Marketing].[FK_Marketing_MarketListingPreference_Member_InsertUser]') AND parent_object_id = OBJECT_ID(N'[Marketing].[MarketListingPreference]'))
ALTER TABLE [Marketing].[MarketListingPreference] CHECK CONSTRAINT [FK_Marketing_MarketListingPreference_Member_InsertUser]
GO
/****** Object:  ForeignKey [FK_Marketing_MarketListingPreference_Member_UpdateUser]    Script Date: 12/04/2009 09:33:34 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Marketing].[FK_Marketing_MarketListingPreference_Member_UpdateUser]') AND parent_object_id = OBJECT_ID(N'[Marketing].[MarketListingPreference]'))
ALTER TABLE [Marketing].[MarketListingPreference]  WITH CHECK ADD  CONSTRAINT [FK_Marketing_MarketListingPreference_Member_UpdateUser] FOREIGN KEY([UpdateUserId])
REFERENCES [dbo].[Member] ([MemberID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Marketing].[FK_Marketing_MarketListingPreference_Member_UpdateUser]') AND parent_object_id = OBJECT_ID(N'[Marketing].[MarketListingPreference]'))
ALTER TABLE [Marketing].[MarketListingPreference] CHECK CONSTRAINT [FK_Marketing_MarketListingPreference_Member_UpdateUser]
GO
