----------------------------------
--  Create the tables
----------------------------------

/*

USE [IMT]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Marketing].[FK_Marketing_ConsumerHighlight__ConsumerHighlightVehiclePreference]') AND parent_object_id = OBJECT_ID(N'[Marketing].[ConsumerHighlight]'))
ALTER TABLE [Marketing].[ConsumerHighlight] DROP CONSTRAINT [FK_Marketing_ConsumerHighlight__ConsumerHighlightVehiclePreference]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Marketing].[FK_Marketing_ConsumerHighlight__HighlightProvider]') AND parent_object_id = OBJECT_ID(N'[Marketing].[ConsumerHighlight]'))
ALTER TABLE [Marketing].[ConsumerHighlight] DROP CONSTRAINT [FK_Marketing_ConsumerHighlight__HighlightProvider]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Marketing].[CK_Marketing_HighlightProvider__Name]') AND parent_object_id = OBJECT_ID(N'[Marketing].[HighlightProvider]'))
ALTER TABLE [Marketing].[HighlightProvider] DROP CONSTRAINT [CK_Marketing_HighlightProvider__Name]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[ConsumerHighlight]') AND type in (N'U'))
DROP TABLE [Marketing].[ConsumerHighlight]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[ConsumerHighlightVehiclePreference]') AND type in (N'U'))
DROP TABLE [Marketing].[ConsumerHighlightVehiclePreference]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[HighlightProvider]') AND type in (N'U'))
DROP TABLE [Marketing].[HighlightProvider]

*/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO


CREATE TABLE Marketing.HighlightProvider(
	HighlightProviderId	    TINYINT	    NOT NULL,
	Name			    VARCHAR(50)	    NOT NULL,
	CONSTRAINT PK_Marketing_HighlightProvider 
	    PRIMARY KEY CLUSTERED (HighlightProviderId),
	CONSTRAINT UK_Marketing_HighlightProvider__Name
	    UNIQUE NONCLUSTERED (Name ASC),
        CONSTRAINT CK_Marketing_HighlightProvider__Name
                CHECK (LEN(Name) > 0)
)

GO


CREATE TABLE Marketing.ConsumerHighlightVehiclePreference(
	ConsumerHighlightVehiclePreferenceId	INT IDENTITY(1,1) NOT NULL,
	OwnerId					INT NOT NULL,
	VehicleEntityId				INT NOT NULL,
	VehicleEntityTypeID			INT NOT NULL,
	IsDisplayed				BIT NOT NULL,
	IncludeCarfaxOneOwnerIcon		BIT NOT NULL,
	InsertUser				INT NOT NULL,
	InsertDate				DATETIME NOT NULL,
	UpdateUser				INT NULL,
	UpdateDate				DATETIME NULL,
	CONSTRAINT PK_Marketing_ConsumerHighlightVehiclePreference 
	    PRIMARY KEY CLUSTERED (ConsumerHighlightVehiclePreferenceId ASC),
	CONSTRAINT UK_Marketing_ConsumerHighlightVehiclePreference__OwnerId_VehicleEntityId_VehicleEntityTypeID
	    UNIQUE NONCLUSTERED (OwnerId,VehicleEntityId,VehicleEntityTypeID)
)

GO

CREATE TABLE Marketing.ConsumerHighlight(
	ConsumerHighlightId			INT IDENTITY(1,1),
	ConsumerHighlightVehiclePreferenceId	INT NOT NULL,
	HighlightProviderId			TINYINT NOT NULL,
	Text					VARCHAR(200) NOT NULL,
	Rank					INT NOT NULL,
	IsDisplayed				BIT NOT NULL,
	ExpirationDate				DATETIME NULL,
	InsertUser				INT NOT NULL,
	InsertDate				DATETIME NOT NULL,
	UpdateUser				INT NULL,
	UpdateDate				DATETIME NULL,
	CONSTRAINT PK_Marketing_ConsumerHighlight
	    PRIMARY KEY CLUSTERED (ConsumerHighlightId),
        CONSTRAINT FK_Marketing_ConsumerHighlight__HighlightProvider
                FOREIGN KEY (HighlightProviderId)
                REFERENCES Marketing.HighlightProvider (HighlightProviderId),
        CONSTRAINT FK_Marketing_ConsumerHighlight__ConsumerHighlightVehiclePreference
                FOREIGN KEY (ConsumerHighlightVehiclePreferenceId)
                REFERENCES Marketing.ConsumerHighlightVehiclePreference (ConsumerHighlightVehiclePreferenceId) 
		ON DELETE CASCADE,
        CONSTRAINT CK_Marketing_ConsumerHighlight__Text
                CHECK (LEN(Text) > 0)

)
GO
----------------------------------
-- Insert the highlight type rows
----------------------------------

insert into Marketing.HighlightProvider(HighlightProviderId, Name)values(1,'FreeText')
insert into Marketing.HighlightProvider(HighlightProviderId, Name)values(2,'AutoCheck')
insert into Marketing.HighlightProvider(HighlightProviderId, Name)values(3,'Carfax')


