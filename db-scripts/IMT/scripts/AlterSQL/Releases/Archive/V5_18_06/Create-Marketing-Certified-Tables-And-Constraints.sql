/* ZB 12/11/2009	This script creates the tables that belong in the Marketing.Certified schema

1.  CertifiedVehicleBenefitPreference
2.  CertifiedVehicleBenefitLineItem

*/

/*

1.  CertfiedProgram

*/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[CertifiedVehicleBenefitPreference]') AND type in (N'U'))
BEGIN
	CREATE 
	TABLE	Marketing.CertifiedVehicleBenefitPreference (
		CertifiedVehicleBenefitPreferenceID  INT IDENTITY(1,1) NOT NULL,
		OwnerID INT NOT NULL,					-- FK TODO: to Market.Pricing.Owner 
		VehicleEntityTypeID TINYINT NOT NULL,	-- FK TODO: to Market.Pricing.VehicleEntityType 
		VehicleEntityID INT NOT NULL,			-- FK TODO: to IMT.dbo.Inventory or Market.Listing.Vehicle
		IsDisplayed BIT NOT NULL,
		OwnerCertifiedProgramID INT NOT NULL,	-- FK to IMT.Certified.OwnerCertifiedProgram
		InsertUser  INT NOT NULL,				-- FK TODO: to Market.Pricing.Owner
		InsertDate  DATETIME NOT NULL,
		UpdateUser  INT NULL,					-- FK TODO: to Market.Pricing.Owner
		UpdateDate  DATETIME NULL
	CONSTRAINT PK_Marketing_CertifiedVehicleBenefitPreference
	PRIMARY KEY CLUSTERED (CertifiedVehicleBenefitPreferenceID ASC))
END
GO

/* Unique Constraints */

-- OwnerID/VehicleEntityTypeID/VehicleEntityID
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Marketing].[CertifiedVehicleBenefitPreference]') 
	AND name = N'UQ_Marketing_CertifiedVehicleBenefitPreference__OwnerID_VehicleEntityTypeID_VehicleEntityID')
	ALTER TABLE [Marketing].[CertifiedVehicleBenefitPreference]  
	ADD  CONSTRAINT [UQ_Marketing_CertifiedVehicleBenefitPreference__OwnerID_VehicleEntityTypeID_VehicleEntityID] 
	UNIQUE NONCLUSTERED ( [OwnerID],[VehicleEntityTypeID],[VehicleEntityID] )
GO

/* Check Constraints */
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Marketing].[CK_Marketing_CertifiedVehicleBenefitPreference__UpdateDateGreaterThanInsertDate]') 
	AND parent_object_id = OBJECT_ID(N'[Marketing].[CertifiedVehicleBenefitPreference]'))
	ALTER TABLE [Marketing].[CertifiedVehicleBenefitPreference]  
	WITH CHECK ADD CONSTRAINT [CK_Marketing_CertifiedVehicleBenefitPreference__UpdateDateGreaterThanInsertDate]
	CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO

-- We only support Inventory and Listings
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Marketing].[CK_Marketing_CertifiedVehicleBenefitPreference__VehicleEntityTypeID]') 
	AND parent_object_id = OBJECT_ID(N'[Marketing].[CertifiedVehicleBenefitPreference]'))
	ALTER TABLE [Marketing].[CertifiedVehicleBenefitPreference]  
	WITH CHECK ADD CONSTRAINT [CK_Marketing_CertifiedVehicleBenefitPreference__VehicleEntityTypeID]
	CHECK ([VehicleEntityTypeID] = 1 OR [VehicleEntityTypeID] = 5)
GO


/** Foreign Key Constraints **/


--
-- NOTE: We delete the preference with a cascading delete when the owner is no longer affiliated with the program.
--

-- IMT.Marketing.CertifiedVehicleBenefitPreference -> IMT.Certified.OwnerCertifiedProgram
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Marketing].[FK_Marketing_CertifiedVehicleBenefitPreference__OwnerCertifiedProgramID]') 
AND parent_object_id = OBJECT_ID(N'[Marketing].[CertifiedVehicleBenefitPreference]'))
ALTER TABLE [Marketing].[CertifiedVehicleBenefitPreference]  
WITH CHECK ADD  CONSTRAINT [FK_Marketing_CertifiedVehicleBenefitPreference__OwnerCertifiedProgramID] 
FOREIGN KEY([OwnerCertifiedProgramID])
REFERENCES [Certified].[OwnerCertifiedProgram] ([OwnerCertifiedProgramID])
ON DELETE CASCADE	
GO

/*

2.  CertifiedVehicleBenefitLineItem

*/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[CertifiedVehicleBenefitLineItem]') AND type in (N'U'))
BEGIN
	CREATE 
	TABLE	Marketing.CertifiedVehicleBenefitLineItem (
		CertifiedVehicleBenefitPreferenceID  INT NOT NULL,					-- FK to IMT.Marketing.CertifiedVehicleBenefitPreference
		OwnerCertifiedProgramBenefitID INT NOT NULL,						-- FK to IMT.Certified.OwnerCertifiedProgram
		IsHighlight BIT NOT NULL,
		InsertUser  INT NOT NULL,											-- FK TODO: to Market.Pricing.Owner
		InsertDate  DATETIME NOT NULL,
		UpdateUser  INT NULL,												-- FK TODO: to Market.Pricing.Owner
		UpdateDate  DATETIME NULL
	CONSTRAINT PK_Marketing_CertifiedVehicleBenefitLineItem
	PRIMARY KEY CLUSTERED (CertifiedVehicleBenefitPreferenceID, OwnerCertifiedProgramBenefitID ASC))
END
GO


/* Check Constraints */
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Marketing].[CK_Marketing_CertifiedVehicleBenefitLineItem__UpdateDateGreaterThanInsertDate]') 
	AND parent_object_id = OBJECT_ID(N'[Marketing].[CertifiedVehicleBenefitLineItem]'))
	ALTER TABLE [Marketing].[CertifiedVehicleBenefitLineItem]  
	WITH CHECK ADD CONSTRAINT [CK_Marketing_CertifiedVehicleBenefitLineItem__UpdateDateGreaterThanInsertDate]
	CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO

/** Foreign Key Constraints **/

--
-- NOTE: We delete the line item with a cascading delete when the preference is deleted and/or when an owner benefit is deleted.
--

-- IMT.Marketing.CertifiedVehicleBenefitLineItem -> IMT.Marketing.CertifiedVehicleBenefitPreference  
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Marketing].[FK_Marketing_CertifiedVehicleBenefitLineItem__CertifiedVehicleBenefitPreferenceID]') 
AND parent_object_id = OBJECT_ID(N'[Marketing].[CertifiedVehicleBenefitLineItem]'))
ALTER TABLE [Marketing].[CertifiedVehicleBenefitLineItem]  
WITH CHECK ADD  CONSTRAINT [FK_Marketing_CertifiedVehicleBenefitLineItem__CertifiedVehicleBenefitPreferenceID] 
FOREIGN KEY([CertifiedVehicleBenefitPreferenceID])
REFERENCES [Marketing].[CertifiedVehicleBenefitPreference] ([CertifiedVehicleBenefitPreferenceID])
ON DELETE CASCADE	
GO

-- IMT.Marketing.CertifiedVehicleBenefitLineItem -> IMT.Certified.OwnerCertifiedProgramBenefit
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Marketing].[FK_Marketing_CertifiedVehicleBenefitLineItem__OwnerCertifiedProgramBenefitID]') 
AND parent_object_id = OBJECT_ID(N'[Marketing].[CertifiedVehicleBenefitLineItem]'))
ALTER TABLE [Marketing].[CertifiedVehicleBenefitLineItem]  
WITH CHECK ADD  CONSTRAINT [FK_Marketing_CertifiedVehicleBenefitLineItem__OwnerCertifiedProgramBenefitID] 
FOREIGN KEY([OwnerCertifiedProgramBenefitID])
REFERENCES [Certified].[OwnerCertifiedProgramBenefit] ([OwnerCertifiedProgramBenefitID])
ON DELETE CASCADE	
GO
