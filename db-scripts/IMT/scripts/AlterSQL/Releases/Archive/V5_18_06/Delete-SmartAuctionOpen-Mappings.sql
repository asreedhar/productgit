----------------------------------------------------------------
--	FBz 9099: Remove SmartAuctionOpen
----------------------------------------------------------------

DELETE	
FROM	dbo.tbl_MemberATCAccessGroup
WHERE	AccessGroupID = 148

DELETE
FROM	dbo.tbl_DealerATCAccessGroups
WHERE	AccessGroupID = 148

GO