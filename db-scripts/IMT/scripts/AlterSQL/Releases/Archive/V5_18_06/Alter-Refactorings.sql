------------------------------------------------------------------------------------------------
--
--	PK is on the surrogate.  Access pattern most definitely by BusinessUnitID.  In fact,
--	the relationship from BU is 1-1 optional as this is really a vertical partition of the
--	BU table for level 4 (Dealers).
--	
--	So change the PK to BusinessUnitID.  We could drop the surrogate if we refactored the
--	mappings in the legacy app.
--
------------------------------------------------------------------------------------------------


ALTER TABLE [dbo].[DealerPreference_Dataload] DROP CONSTRAINT [FK_DealerPreference_Dataload__DealerPreference]
ALTER TABLE [dbo].[DealerRisk] DROP CONSTRAINT [FK_DealerRisk__DealerPreference] 

ALTER TABLE dbo.DealerPreference DROP CONSTRAINT IX_DealerPreference

ALTER TABLE dbo.DealerPreference ADD CONSTRAINT PK_DealerPreference PRIMARY KEY CLUSTERED (BusinessUnitID)
                                                                                

CREATE INDEX IX_DealerPreference ON dbo.DealerPreference(DealerPreferenceID)


ALTER TABLE [dbo].[DealerRisk] ADD CONSTRAINT [FK_DealerRisk__DealerPreference] FOREIGN KEY ([BusinessUnitId]) REFERENCES [dbo].[DealerPreference] ([BusinessUnitID])
GO


ALTER TABLE [dbo].[DealerPreference_Dataload] ADD CONSTRAINT [FK_DealerPreference_Dataload__DealerPreference] FOREIGN KEY ([BusinessUnitID]) REFERENCES [dbo].[DealerPreference] ([BusinessUnitID])
GO

ALTER TABLE [dbo].ThirdPartyVehicles DROP COLUMN DateModified

ALTER TABLE [dbo].ThirdPartyVehicles DROP CONSTRAINT DF_ThirdPartyVehicles_DateCreated_1
ALTER TABLE [dbo].ThirdPartyVehicles ALTER COLUMN DateCreated SMALLDATETIME NOT NULL
ALTER TABLE [dbo].ThirdPartyVehicles ADD CONSTRAINT [DF_ThirdPartyVehicles__DateCreated] DEFAULT (GETDATE())
GO