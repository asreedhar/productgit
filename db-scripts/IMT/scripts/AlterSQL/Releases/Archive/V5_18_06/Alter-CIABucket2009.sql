
--------------------------------------------------------------------------------
--	ORIGINALLY PLANNED TO BE DONE IN AN ADMIN TOOL, THIS NEEDS TO BE DONE
--	EVERY YEAR AS LONG AS THE CIA BUCKETRON IS DRIVEN COMPLETELY FROM DATA
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- 	ADD 2009
--------------------------------------------------------------------------------

UPDATE	CIABuckets
SET	RANK = RANK + 1
WHERE	CIABucketGroupID = 1

INSERT
INTO	CIABuckets (CIABucketGroupID, Rank, Description)
SELECT	1, 1, '2009'

INSERT
INTO	CIABucketRules (CIABucketID, CIABucketRuleTypeID, Value, Value2)
SELECT	22, 1, 2009, NULL

--------------------------------------------------------------------------------
-- 	DROP 2001
--------------------------------------------------------------------------------

DELETE 
FROM	CIABucketRules
WHERE	CIABucketID = 6

DELETE 
FROM	CIABuckets
WHERE	CIABucketID = 6
