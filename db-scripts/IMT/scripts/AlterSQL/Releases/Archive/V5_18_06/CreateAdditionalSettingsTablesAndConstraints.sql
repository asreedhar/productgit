/* 

	ZB 12/11/2009	This script creates the tables that belong in the Marketing.AdditionalSettings schema.
	NOTE:	No "system preferences" may be stored in this table as there are really no reasonable defaults.
			The data stored in this table is truly dealer specific and I can't think of any system default
			values that would make sense.


1.  DealerGeneralPreference

*/

/*

1.  DealerGeneralPreference

*/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[DealerGeneralPreference]') AND type in (N'U'))
BEGIN
	CREATE 
	TABLE	Marketing.DealerGeneralPreference (		
		OwnerID INT NOT NULL,					-- FK TODO: to Market.Pricing.Owner 
		AddressLine1 VARCHAR(500) NOT NULL,
		AddressLine2 VARCHAR(500) NOT NULL,
		City VARCHAR(500) NOT NULL,
		StateCode VARCHAR(2) NOT NULL,
		ZipCode VARCHAR(5) NOT NULL,
		SalesEmailAddress VARCHAR(500) NOT NULL,
		SalesPhoneNumber VARCHAR(14) NOT NULL,
		ExtendedTagLine VARCHAR(250) NOT NULL,
		DaysValidFor INT NOT NULL,
		Disclaimer VARCHAR(500),
		InsertUser  INT NOT NULL,				-- FK TODO: to Market.Pricing.Owner
		InsertDate  DATETIME NOT NULL,
		UpdateUser  INT NULL,					-- FK TODO: to Market.Pricing.Owner
		UpdateDate  DATETIME NULL
	CONSTRAINT PK_Marketing_DealerGeneralPreference
	PRIMARY KEY CLUSTERED (OwnerID ASC))
END
GO

/** Foreign Key Constraints **/

