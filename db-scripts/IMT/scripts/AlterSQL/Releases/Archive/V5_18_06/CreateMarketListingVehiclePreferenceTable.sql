USE [IMT]
GO
/****** Object:  Table [Marketing].[MarketListingVehiclePreference]    Script Date: 12/11/2009 15:00:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Marketing].[MarketListingVehiclePreference](
	[MarketListingVehiclePreferenceId] [int] IDENTITY(1,1) NOT NULL,
	[VehicleEntityId] [int] NOT NULL,
	[VehicleEntityTypeId] [tinyint] NOT NULL,
	[Ownerid] [int] NOT NULL,
	[IsDisplayed] [bit] NOT NULL,
	[ShowDealerName] [bit] NOT NULL,
	[ShowCertifiedIndicator] [bit] NOT NULL,
	[MileageLow] [int] NOT NULL,
	[MileageHigh] [int] NOT NULL,
	[PriceLow] [int] NOT NULL,
	[PriceHigh] [int] NOT NULL,
	[InsertUserID] [int] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[UpdateUserId] [int] NOT NULL,
	[UpdateDate] [datetime] NOT NULL,
	[Version] [timestamp] NOT NULL,
 CONSTRAINT [PK_MarketListingVehiclePreference] PRIMARY KEY CLUSTERED 
(
	[MarketListingVehiclePreferenceId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]

GO
ALTER TABLE [Marketing].[MarketListingVehiclePreference]  WITH CHECK ADD  CONSTRAINT [FK_Marketing_MarketListingVehiclePreference_Member_InsertUser] FOREIGN KEY([InsertUserID])
REFERENCES [dbo].[Member] ([MemberID])
GO
ALTER TABLE [Marketing].[MarketListingVehiclePreference] CHECK CONSTRAINT [FK_Marketing_MarketListingVehiclePreference_Member_InsertUser]
GO
ALTER TABLE [Marketing].[MarketListingVehiclePreference]  WITH CHECK ADD  CONSTRAINT [FK_Marketing_MarketListingVehiclePreference_Member_UpdateUser] FOREIGN KEY([UpdateUserId])
REFERENCES [dbo].[Member] ([MemberID])
GO
ALTER TABLE [Marketing].[MarketListingVehiclePreference] CHECK CONSTRAINT [FK_Marketing_MarketListingVehiclePreference_Member_UpdateUser]