
--DROP TABLE Marketing.EquipmentProviderAssignment
--DROP TABLE Marketing.EquipmentProviderAssignmentList
--DROP TABLE Marketing.VehicleEquipmentProvider
--DROP TABLE Marketing.Equipment
--DROP TABLE Marketing.EquipmentList
--DROP TABLE Marketing.EquipmentProvider

CREATE TABLE Marketing.EquipmentProvider (
        EquipmentProviderID     TINYINT         NOT NULL,
        Name                    VARCHAR(200)    NOT NULL,
        ThirdPartyID            TINYINT         NULL,
        CONSTRAINT PK_Marketing_EquipmentProvider
                PRIMARY KEY CLUSTERED (EquipmentProviderID),
        CONSTRAINT UK_Marketing_EquipmentProvider__Name
                UNIQUE NONCLUSTERED (Name),
        CONSTRAINT FK_Marketing_EquipmentProvider__ThirdParty
                FOREIGN KEY (ThirdPartyID)
                REFERENCES dbo.ThirdParties (ThirdPartyID),
        CONSTRAINT CK_Marketing_EquipmentProvider__Name
                CHECK (LEN(Name) > 0)
)
GO

INSERT INTO Marketing.EquipmentProvider (EquipmentProviderID, Name, ThirdPartyID)
SELECT  ThirdPartyID, Name, ThirdPartyID
FROM    dbo.ThirdParties
GO

INSERT INTO Marketing.EquipmentProvider (EquipmentProviderID, Name, ThirdPartyID)
VALUES (6, 'AutoTrader', NULL)
GO

INSERT INTO Marketing.EquipmentProvider (EquipmentProviderID, Name, ThirdPartyID)
VALUES (7, 'CarsDotCom', NULL)
GO

INSERT INTO Marketing.EquipmentProvider (EquipmentProviderID, Name, ThirdPartyID)
VALUES (8, 'GetAuto', NULL)
GO

CREATE TABLE Marketing.EquipmentList (
        EquipmentListID         INT IDENTITY(1,1)       NOT NULL,
        EquipmentProviderID     TINYINT                 NOT NULL,
        OwnerID                 INT                     NOT NULL,
        VehicleEntityTypeID     TINYINT                 NOT NULL,
        VehicleEntityID         INT                     NOT NULL,
        IdentityValue           BIGINT                  NOT NULL,
        InsertUser	        INT                     NOT NULL,
        InsertDate	        DATETIME                NOT NULL,
        CONSTRAINT PK_Marketing_EquipmentList
                PRIMARY KEY CLUSTERED (EquipmentListID),
        CONSTRAINT FK_Marketing_EquipmentList__EquipmentProvider
                FOREIGN KEY (EquipmentProviderID)
                REFERENCES Marketing.EquipmentProvider (EquipmentProviderID),
        CONSTRAINT FK_Marketing_EquipmentList__InsertUser
                FOREIGN KEY (InsertUser)
                REFERENCES dbo.Member (MemberID)
)
GO

CREATE NONCLUSTERED INDEX IX_Marketing_EquipmentList__Owner_VehicleEntityType_VehicleEntity
        ON Marketing.EquipmentList (
                OwnerID,
                VehicleEntityTypeID,
                VehicleEntityID,
                EquipmentProviderID)
        INCLUDE (
                EquipmentListID)
GO

CREATE TABLE Marketing.Equipment (
        EquipmentListID INT                     NOT NULL,
        EquipmentID     INT IDENTITY(1,1)       NOT NULL,
        Name            VARCHAR(200)            NOT NULL,
        Value           INT                     NULL,
        Selected        BIT                     NOT NULL,
        Position        TINYINT                 NOT NULL,
        InsertUser	INT                     NOT NULL,
        InsertDate	DATETIME                NOT NULL,
        UpdateUser	INT                     NULL,
        UpdateDate	DATETIME                NULL,
        Version		ROWVERSION              NOT NULL,
        CONSTRAINT PK_Marketing_Equipment
                PRIMARY KEY NONCLUSTERED (EquipmentID),
        CONSTRAINT UK_Marketing_Equipment__EquipmentList_Name
                UNIQUE NONCLUSTERED (EquipmentListID, Name),
        CONSTRAINT FK_Marketing_Equipment__EquipmentList
                FOREIGN KEY (EquipmentListID)
                REFERENCES Marketing.EquipmentList (EquipmentListID),
        CONSTRAINT FK_Marketing_Equipment__InsertUser
                FOREIGN KEY (InsertUser)
                REFERENCES dbo.Member (MemberID),
        CONSTRAINT FK_Marketing_Equipment__UpdateUser
                FOREIGN KEY (UpdateUser)
                REFERENCES dbo.Member (MemberID),
        CONSTRAINT CK_Marketing_Equipment__Name
                CHECK (LEN(Name) > 0),
        CONSTRAINT CK_Marketing_Equipment__Position
                CHECK (Position >= 0),
        CONSTRAINT CK_Marketing_Equipment__UpdateUserDate
                CHECK ( (UpdateDate IS NULL AND UpdateUser IS NULL) OR
                        (UpdateDate IS NOT NULL AND UpdateUser IS NOT NULL)),
        CONSTRAINT CK_Marketing_Equipment__UpdateDateGreaterThanInsertDate
                CHECK  (UpdateDate IS NULL OR UpdateDate>=InsertDate)
)
GO

CREATE CLUSTERED INDEX IX_Marketing_Equipment__EquipmentList
        ON Marketing.Equipment (
                EquipmentListID,
                EquipmentID)
GO

CREATE TABLE Marketing.VehicleEquipmentProvider (
        OwnerID                 INT             NOT NULL,
        VehicleEntityTypeID     TINYINT         NOT NULL,
        VehicleEntityID         INT             NOT NULL,
        EquipmentProviderID     TINYINT         NOT NULL,
        Visible                 BIT             NOT NULL,
        InsertUser	        INT             NOT NULL,
        InsertDate	        DATETIME        NOT NULL,
        UpdateUser	        INT             NULL,
        UpdateDate	        DATETIME        NULL,
        Version		        ROWVERSION      NOT NULL,
        CONSTRAINT PK_Marketing_VehicleEquipmentProvider
                PRIMARY KEY CLUSTERED (OwnerID, VehicleEntityTypeID, VehicleEntityID),
        CONSTRAINT FK_Marketing_VehicleEquipmentProvider__EquipmentProvider
                FOREIGN KEY (EquipmentProviderID)
                REFERENCES Marketing.EquipmentProvider (EquipmentProviderID),
        CONSTRAINT FK_Marketing_VehicleEquipmentProvider__InsertUser
                FOREIGN KEY (InsertUser)
                REFERENCES dbo.Member (MemberID),
        CONSTRAINT FK_Marketing_VehicleEquipmentProvider__UpdateUser
                FOREIGN KEY (UpdateUser)
                REFERENCES dbo.Member (MemberID),
        CONSTRAINT CK_Marketing_VehicleEquipmentProvider__UpdateUserDate
                CHECK ( (UpdateDate IS NULL AND UpdateUser IS NULL) OR
                        (UpdateDate IS NOT NULL AND UpdateUser IS NOT NULL)),
        CONSTRAINT CK_Marketing_VehicleEquipmentProvider__UpdateDateGreaterThanInsertDate
                CHECK  (UpdateDate IS NULL OR UpdateDate >= InsertDate)
)
GO

CREATE TABLE Marketing.EquipmentProviderAssignmentList (
        EquipmentProviderAssignmentListID       INT IDENTITY(1,1)       NOT NULL,
        OwnerID                                 INT                     NOT NULL,
        InsertUser	                        INT                     NOT NULL,
        InsertDate	                        DATETIME                NOT NULL,
        CONSTRAINT PK_Marketing_EquipmentProviderAssignmentList
                PRIMARY KEY CLUSTERED (EquipmentProviderAssignmentListID),
        CONSTRAINT UK_Marketing_EquipmentProviderAssignmentList__Owner
                UNIQUE NONCLUSTERED (OwnerID),
        CONSTRAINT FK_Marketing_EquipmentProviderAssignmentList__InsertUser
                FOREIGN KEY (InsertUser)
                REFERENCES dbo.Member (MemberID)
)
GO

CREATE TABLE Marketing.EquipmentProviderAssignment (
        EquipmentProviderAssignmentListID       INT             NOT NULL,
        EquipmentProviderID                     TINYINT         NOT NULL,
        IsDefault                               BIT             NOT NULL,
        Position                                TINYINT         NOT NULL,
        InsertUser	                        INT             NOT NULL,
        InsertDate	                        DATETIME        NOT NULL,
        UpdateUser	                        INT             NULL,
        UpdateDate	                        DATETIME        NULL,
        Version		                        ROWVERSION      NOT NULL,
        CONSTRAINT PK_Marketing_EquipmentProviderAssignment
                PRIMARY KEY CLUSTERED (EquipmentProviderAssignmentListID, EquipmentProviderID),
        CONSTRAINT FK_Marketing_EquipmentProviderAssignment__EquipmentProvider
                FOREIGN KEY (EquipmentProviderID)
                REFERENCES Marketing.EquipmentProvider (EquipmentProviderID),
        CONSTRAINT FK_Marketing_EquipmentProviderAssignment__EquipmentProviderAssignmentList
                FOREIGN KEY (EquipmentProviderAssignmentListID)
                REFERENCES Marketing.EquipmentProviderAssignmentList (EquipmentProviderAssignmentListID),
        CONSTRAINT FK_Marketing_EquipmentProviderAssignment__InsertUser
                FOREIGN KEY (InsertUser)
                REFERENCES dbo.Member (MemberID),
        CONSTRAINT FK_Marketing_EquipmentProviderAssignment__UpdateUser
                FOREIGN KEY (UpdateUser)
                REFERENCES dbo.Member (MemberID),
        CONSTRAINT CK_Marketing_EquipmentProviderAssignment__UpdateUserDate
                CHECK ( (UpdateDate IS NULL AND UpdateUser IS NULL) OR
                        (UpdateDate IS NOT NULL AND UpdateUser IS NOT NULL)),
        CONSTRAINT CK_Marketing_EquipmentProviderAssignment__UpdateDateGreaterThanInsertDate
                CHECK  (UpdateDate IS NULL OR UpdateDate >= InsertDate),
        CONSTRAINT CK_Marketing_EquipmentProviderAssignment__Position
                CHECK (Position >= 0)
)
GO
