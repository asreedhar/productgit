
/* DGH 10/30/2009	This script creates the tables that belong in the Certified schema

1.  CertfiedProgram
2.  CertifiedProgramPhoto
3.  CertifiedProgram_Make
4.  CertifiedProgramBenefit
5.  OwnerCertifiedProgram
6.  OwnerCertifiedProgramBenefit

*/

/*

1.  CertfiedProgram

*/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[CertifiedProgram]') AND type in (N'U'))
BEGIN
	CREATE 
	TABLE	Certified.CertifiedProgram(
		CertifiedProgramID  INT IDENTITY(1,1) NOT NULL,
		Name	VARCHAR(50) NOT NULL,
		Text	VARCHAR(100) NOT NULL,
		Active	BIT NOT NULL,
		InsertUser  INT NOT NULL,
		InsertDate  DATETIME NOT NULL,
		UpdateUser  INT NULL,
		UpdateDate  DATETIME NULL
	CONSTRAINT PK_Certified_CertifiedProgram 
	PRIMARY KEY CLUSTERED (CertifiedProgramID ASC))
END
GO

/* Unique Constraints */
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Certified].[CertifiedProgram]') AND name = N'UQ_Certified_CertifiedProgram__Name')
	ALTER TABLE [Certified].[CertifiedProgram]  
	ADD  CONSTRAINT [UQ_Certified_CertifiedProgram__Name] 
	UNIQUE NONCLUSTERED ( [Name] )
GO

/* Check Constraints */
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Certified].[CK_Certified_CertifiedProgram__Name]') AND parent_object_id = OBJECT_ID(N'[Certified].[CertifiedProgram]'))
	ALTER TABLE [Certified].[CertifiedProgram]  
	WITH CHECK ADD  CONSTRAINT [CK_Certified_CertifiedProgram__Name] 
	CHECK ( LEN(RTRIM(LTRIM([Name]))) > 0 )
GO

/* Check Constraints */
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Certified].[CK_Certified_CertifiedProgram__UpdateDateGreaterThanInsertDate]') AND parent_object_id = OBJECT_ID(N'[Certified].[CertifiedProgram]'))
	ALTER TABLE [Certified].[CertifiedProgram]  
	WITH CHECK ADD CONSTRAINT [CK_Certified_CertifiedProgram__UpdateDateGreaterThanInsertDate]
	CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO


/*

2.  CertifiedProgramPhoto

*/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[CertifiedProgramPhoto]') AND type in (N'U'))
BEGIN
	CREATE 
	TABLE	Certified.CertifiedProgramPhoto(
		CertifiedProgramID INT NOT NULL,
		PhotoID	INT NOT NULL,
		InsertUser  INT NOT NULL,
		InsertDate  DATETIME NOT NULL
	CONSTRAINT PK_Certified_CertifiedProgramPhoto 
	PRIMARY KEY CLUSTERED (CertifiedProgramID ASC))
END
GO

/*

3.  CertifiedProgram_Make

*/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[CertifiedProgram_Make]') AND type in (N'U'))
BEGIN
	CREATE 
	TABLE	Certified.CertifiedProgram_Make(
		MakeID	INT NOT NULL,
		CertifiedProgramID  INT NOT NULL
	CONSTRAINT PK_Certified_CertifiedProgram_Make 
	PRIMARY KEY CLUSTERED (MakeID ASC))
END
GO

/*

4.  CertifiedProgramBenefit

*/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[CertifiedProgramBenefit]') AND type in (N'U'))
BEGIN
	CREATE 
	TABLE	Certified.CertifiedProgramBenefit(
		CertifiedProgramBenefitID   INT IDENTITY(1,1)	NOT NULL,
		CertifiedProgramID INT NOT NULL,
		Name	VARCHAR(50) NOT NULL,
		Text	VARCHAR(100) NOT NULL,
		Rank	SMALLINT NOT NULL,
		IsProgramHighlight BIT NOT NULL,
		InsertUser  INT NOT NULL,
		InsertDate  DATETIME NOT NULL,
		UpdateUser  INT NULL,
		UpdateDate  DATETIME NULL
	CONSTRAINT PK_Certified_CertifiedProgramBenefit 
	PRIMARY KEY CLUSTERED (CertifiedProgramBenefitID ASC))
END
GO

/* Check Constraints */
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Certified].[CK_Certified_CertifiedProgramBenefit__UpdateDateGreaterThanInsertDate]') AND parent_object_id = OBJECT_ID(N'[Certified].[CertifiedProgramBenefit]'))
	ALTER TABLE [Certified].[CertifiedProgramBenefit]  
	WITH CHECK ADD CONSTRAINT [CK_Certified_CertifiedProgramBenefit__UpdateDateGreaterThanInsertDate]
	CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO

IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Certified].[CK_Certified_CertifiedProgramBenefit__Name]') AND parent_object_id = OBJECT_ID(N'[Certified].[CertifiedProgramBenefit]'))
	ALTER TABLE [Certified].[CertifiedProgramBenefit]  
	WITH CHECK ADD CONSTRAINT [CK_Certified_CertifiedProgramBenefit__Name]
	CHECK ( LEN(RTRIM(LTRIM([Name]))) > 0 )
GO

IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Certified].[CK_Certified_CertifiedProgramBenefit__Text]') AND parent_object_id = OBJECT_ID(N'[Certified].[CertifiedProgramBenefit]'))
	ALTER TABLE [Certified].[CertifiedProgramBenefit]  
	WITH CHECK ADD CONSTRAINT [CK_Certified_CertifiedProgramBenefit__Text]
	CHECK ( LEN(RTRIM(LTRIM([Text]))) > 0 )
GO



/* Unique Constraints */
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Certified].[CertifiedProgramBenefit]') AND name = N'UQ_Certified_CertifiedProgramBenefit__CertifiedProgramIDName')
	ALTER TABLE [Certified].[CertifiedProgramBenefit]  
	ADD  CONSTRAINT [UQ_Certified_CertifiedProgramBenefit__CertifiedProgramIDName] 
	UNIQUE NONCLUSTERED ( [CertifiedProgramID], [Name] )  
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Certified].[CertifiedProgramBenefit]') AND name = N'UQ_Certified_CertifiedProgramBenefit__CertifiedProgramIDRank')
	ALTER TABLE [Certified].[CertifiedProgramBenefit]  
	ADD  CONSTRAINT [UQ_Certified_CertifiedProgramBenefit__CertifiedProgramIDRank] 
	UNIQUE NONCLUSTERED ( [CertifiedProgramID], [Rank] )  
GO

/*

5.  OwnerCertifiedProgram

*/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[OwnerCertifiedProgram]') AND type in (N'U'))
BEGIN
	CREATE 
	TABLE	Certified.OwnerCertifiedProgram(
		OwnerCertifiedProgramID	INT IDENTITY(1,1) NOT NULL,
		OwnerId	INT	NOT NULL,
		CertifiedProgramID  INT NOT NULL,
		InsertUser  INT NOT NULL,
		InsertDate  DATETIME NOT NULL
	CONSTRAINT PK_Certified_OwnerCertifiedProgram 
	PRIMARY KEY CLUSTERED (OwnerCertifiedProgramID ASC))
END
GO

/* Unique Constraints */
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Certified].[OwnerCertifiedProgram]') AND name = N'UQ_Certified_OwnerCertifiedProgram__OwnerIdCertifiedProgramID')
	ALTER TABLE [Certified].[OwnerCertifiedProgram]  
	ADD  CONSTRAINT [UQ_Certified_OwnerCertifiedProgram__OwnerIdCertifiedProgramID] 
	UNIQUE NONCLUSTERED ( [OwnerId], [CertifiedProgramID] )  
GO

/*

6.  OwnerCertifiedProgramBenefit

*/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[OwnerCertifiedProgramBenefit]') AND type in (N'U'))
BEGIN
	CREATE 
	TABLE	Certified.OwnerCertifiedProgramBenefit(
		OwnerCertifiedProgramBenefitID	INT IDENTITY(1,1) NOT NULL,
		OwnerCertifiedProgramID	INT NOT NULL,
		CertifiedProgramBenefitID   INT NOT NULL,
		Rank	SMALLINT NOT NULL,
		IsProgramHighlight BIT NOT NULL,
		InsertUser  INT NOT NULL,
		InsertDate  DATETIME NOT NULL,
		UpdateUser  INT NULL,
		UpdateDate  DATETIME NULL
	CONSTRAINT PK_Certified_OwnerCertifiedProgramBenefit
	PRIMARY KEY CLUSTERED (OwnerCertifiedProgramBenefitID ASC))
END
GO

/* Check Constraints */
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Certified].[CK_Certified_OwnerCertifiedProgramBenefit__UpdateDateGreaterThanInsertDate]') AND parent_object_id = OBJECT_ID(N'[Certified].[OwnerCertifiedProgramBenefit]'))
	ALTER TABLE [Certified].[OwnerCertifiedProgramBenefit]
	WITH CHECK ADD  CONSTRAINT [CK_Certified_OwnerCertifiedProgramBenefit__UpdateDateGreaterThanInsertDate] 
	CHECK (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
GO

/* Unique Constraints */
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Certified].[OwnerCertifiedProgramBenefit]') AND name = N'UQ_Certified_OwnerCertifiedProgramBenefit__OwnerCertifiedProgramIDCertifiedProgramBenefitID')
	ALTER TABLE [Certified].[OwnerCertifiedProgramBenefit]  
	ADD  CONSTRAINT [UQ_Certified_OwnerCertifiedProgramBenefit__OwnerCertifiedProgramIDCertifiedProgramBenefitID] 
	UNIQUE NONCLUSTERED ( [OwnerCertifiedProgramID], [CertifiedProgramBenefitID] )  
GO

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Certified].[OwnerCertifiedProgramBenefit]') AND name = N'UQ_Certified_OwnerCertifiedProgramBenefit__OwnerCertifiedProgramIDRank')
	ALTER TABLE [Certified].[OwnerCertifiedProgramBenefit]  
	ADD  CONSTRAINT [UQ_Certified_OwnerCertifiedProgramBenefit__OwnerCertifiedProgramIDRank] 
	UNIQUE NONCLUSTERED ( [OwnerCertifiedProgramID], [Rank] )  
GO


/** Foreign Key Constraints **/

-- CertifiedProgramBenefit -> CertifiedProgram
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Certified].[FK_Certified_CertifiedProgramBenefit__CertifiedProgramID]') AND parent_object_id = OBJECT_ID(N'[Certified].[CertifiedProgramBenefit]'))
ALTER TABLE [Certified].[CertifiedProgramBenefit]  
WITH CHECK ADD  CONSTRAINT [FK_Certified_CertifiedProgramBenefit__CertifiedProgramID] 
FOREIGN KEY([CertifiedProgramID])
REFERENCES [Certified].[CertifiedProgram] ([CertifiedProgramID])
GO

-- OwnerCertifiedProgram -> CertifiedProgram
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Certified].[FK_Certified_OwnerCertifiedProgram__CertifiedProgramID]') AND parent_object_id = OBJECT_ID(N'[Certified].[OwnerCertifiedProgram]'))
ALTER TABLE [Certified].[OwnerCertifiedProgram]  
WITH CHECK ADD  CONSTRAINT [FK_Certified_OwnerCertifiedProgram__CertifiedProgramID] 
FOREIGN KEY([CertifiedProgramID])
REFERENCES [Certified].[CertifiedProgram] ([CertifiedProgramID])
GO

-- OwnerCertifiedProgramBenefit -> CertifiedProgramBenefit
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Certified].[FK_Certified_OwnerCertifiedProgramBenefit__CertifiedProgramBenefitID]') AND parent_object_id = OBJECT_ID(N'[Certified].[OwnerCertifiedProgramBenefit]'))
ALTER TABLE [Certified].[OwnerCertifiedProgramBenefit]  
WITH CHECK ADD  CONSTRAINT [FK_Certified_OwnerCertifiedProgramBenefit__CertifiedProgramBenefitID] 
FOREIGN KEY([CertifiedProgramBenefitID])
REFERENCES [Certified].[CertifiedProgramBenefit] ([CertifiedProgramBenefitID])
GO

-- OwnerCertifiedProgramBenefit -> OwnerCertifiedProgram
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Certified].[FK_Certified_OwnerCertifiedProgramBenefit__OwnerCertifiedProgramID]') AND parent_object_id = OBJECT_ID(N'[Certified].[OwnerCertifiedProgramBenefit]'))
ALTER TABLE [Certified].[OwnerCertifiedProgramBenefit]  
WITH CHECK ADD  CONSTRAINT [FK_Certified_OwnerCertifiedProgramBenefit__OwnerCertifiedProgramID] 
FOREIGN KEY([OwnerCertifiedProgramID])
REFERENCES [Certified].[OwnerCertifiedProgram] ([OwnerCertifiedProgramID])
GO

-- CertifiedProgramPhoto -> CertifiedProgram
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Certified].[FK_Certified_CertifiedProgramPhoto__CertifiedProgramID]') AND parent_object_id = OBJECT_ID(N'[Certified].[CertifiedProgramPhoto]'))
ALTER TABLE [Certified].[CertifiedProgramPhoto]  
WITH CHECK ADD  CONSTRAINT [FK_Certified_CertifiedProgramPhoto__CertifiedProgramID] 
FOREIGN KEY([CertifiedProgramID])
REFERENCES [Certified].[CertifiedProgram] ([CertifiedProgramID])
GO

-- CertifiedProgramPhoto -> Photo
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Certified].[FK_Certified_CertifiedProgramPhoto__PhotoID]') AND parent_object_id = OBJECT_ID(N'[Certified].[CertifiedProgramPhoto]'))
ALTER TABLE [Certified].[CertifiedProgramPhoto]  
WITH CHECK ADD  CONSTRAINT [FK_Certified_CertifiedProgramPhoto__PhotoID] 
FOREIGN KEY([PhotoID])
REFERENCES [dbo].[Photos] ([PhotoID])
GO


-- CertifiedProgram_Make -> CertifiedProgram
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Certified].[FK_Certified_CertifiedProgram_Make__CertifiedProgramID]') AND parent_object_id = OBJECT_ID(N'[Certified].[CertifiedProgram_Make]'))
ALTER TABLE [Certified].[CertifiedProgram_Make]  
WITH CHECK ADD  CONSTRAINT [FK_Certified_CertifiedProgram_Make__CertifiedProgramID] 
FOREIGN KEY([CertifiedProgramID])
REFERENCES [Certified].[CertifiedProgram] ([CertifiedProgramID])
GO


