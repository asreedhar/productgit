/*
"Offer Price" is no longer deemed necessary in this table.
- dhillis
*/

DECLARE @OfferPriceId TINYINT
SET @OfferPriceId = (SELECT PriceProviderId FROM Marketing.MarketAnalysisPriceProvider WHERE Description = 'Offer Price')
IF (@OfferPriceId IS NOT NULL)
BEGIN
    DELETE FROM Marketing.MarketAnalysisOwnerPriceProvider WHERE PriceProviderId = @OfferPriceId
    DELETE FROM Marketing.MarketAnalysisPriceProvider WHERE PriceProviderId = @OfferPriceId
END