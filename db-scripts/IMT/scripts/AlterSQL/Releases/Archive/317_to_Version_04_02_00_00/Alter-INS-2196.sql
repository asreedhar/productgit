print 'INS-2196 Alter Script'
go

ALTER TABLE [dbo].[tbl_CIAPreferences]
ADD PowerZoneDisplayThreshold INT NULL
GO

UPDATE [dbo].[tbl_CIAPreferences] SET PowerZoneDisplayThreshold = 5
GO

ALTER TABLE [dbo].[tbl_CIAPreferences]
ALTER COLUMN PowerZoneDisplayThreshold INT NOT NULL
GO