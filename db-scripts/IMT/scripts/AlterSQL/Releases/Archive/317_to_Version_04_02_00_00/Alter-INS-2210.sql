print 'INS-2210 Alter Script'
GO

ALTER TABLE dbo.dealerrisk 
ADD HighMileageRedLight INT NULL

GO

UPDATE dbo.dealerrisk set highmileageredlight = 100000

GO

ALTER TABLE dbo.dealerrisk ALTER COLUMN highmileageredlight INT NOT NULL

GO

ALTER TABLE [dbo].[DealerRisk] ADD CONSTRAINT [DF_DealerRisk_HighMileageRedLight] DEFAULT (100000) FOR [HighMileageRedLight]


GO