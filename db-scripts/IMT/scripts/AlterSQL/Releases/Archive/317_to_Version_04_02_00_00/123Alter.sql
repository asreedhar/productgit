/******** Iteration 123 Alter Statements ***********/
print '123 Alter Script'
go

alter table Audit_Inventory_DMI
alter column cylinder_ct varchar(1) null
go

CREATE TABLE dbo.[tbl_CIABasisPeriod] ( 
[BusinessUnitId] [int] NOT NULL , 
[Days] [int] NOT NULL , 
[Weight] [int] NOT NULL
CONSTRAINT [PK_tbl_CIABasisPeriod] PRIMARY KEY CLUSTERED 
(
	[BusinessUnitID],
	[Days]
) WITH  FILLFACTOR = 90,
CONSTRAINT [FK_tbl_CIABasisPeriod_BusinessUnit] FOREIGN KEY 
( 
[BusinessUnitID] 
) REFERENCES [BusinessUnit] ( 
[BusinessUnitID] 
) ON DELETE CASCADE ON UPDATE CASCADE 
) ON [DATA] 
GO 

insert into tbl_ciabasisperiod
select bu.businessunitid, 84, 100
from businessunit bu, dealerupgrade du
where businessunittypeid = 4 
and bu.businessunitid = du.businessunitid 
and dealerupgradecd = 3
GO

CREATE TABLE dbo.[lu_CIATimePeriod] ( 
[Days] [int] NOT NULL , 
[Description] [varchar] (10) NOT NULL

CONSTRAINT [PK_lu_CIATimePeriod] PRIMARY KEY NONCLUSTERED 
(
	[Days]
) WITH  FILLFACTOR = 90
) ON [DATA] 
GO

insert into lu_CIATimePeriod
( days, description )
select
56, '8 Weeks'
union select 84, '12 Weeks'
union select 182, '26 Weeks'
union select 364, '52 Weeks'
GO 

CREATE TABLE dbo.[tbl_CIAPreferences] ( 
[BusinessUnitId] [int] NOT NULL , 
[CoreThreshold] [int] NOT NULL , 
[DaysSupplyThreshold] [int] NOT NULL,
[RunDayOfWeek] [int] NOT NULL
CONSTRAINT [PK_tbl_CIAPreferences] PRIMARY KEY NONCLUSTERED 
(
	[BusinessUnitID]
) WITH  FILLFACTOR = 90,
CONSTRAINT [FK_tbl_CIAPreferences_BusinessUnit] FOREIGN KEY 
( 
[BusinessUnitID] 
) REFERENCES [BusinessUnit] ( 
[BusinessUnitID] 
) ON DELETE CASCADE ON UPDATE CASCADE 
) ON [DATA] 
GO

ALTER TABLE [dbo].[tbl_CIAPreferences] ADD 
	CONSTRAINT [DF_tbl_CIAPreferences_CoreThreshold] DEFAULT (10) FOR [CoreThreshold],
	CONSTRAINT [DF_tbl_CIAPreferences_DaysSupplyThreshold] DEFAULT (45) FOR [DaysSupplyThreshold],
	CONSTRAINT [DF_tbl_CIAPreferences_RunDayOfWeek] DEFAULT (1) FOR [RunDayOfWeek]
GO

insert into tbl_CIAPreferences (BusinessUnitID, RunDayOFWeek)
select dcp.businessunitid, dcp.RunDayOfWeek
from DealerCIAPreferences dcp
GO




