print '124 Alter Script'
go
CREATE TABLE [dbo].[lu_Status] (
	[StatusId] [tinyint] NOT NULL ,
	[StatusName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

CREATE TABLE [dbo].[tbl_CIAClassTypeDetail] (
	[CIAClassTypeDetailId] [int] IDENTITY (1,1) NOT NULL,
	[CIASummaryId] [int] NOT NULL ,
	[CIAClassTypeId] [smallint] NOT NULL ,
	[Understock] [int] NOT NULL ,
	[Buy] [int] NULL ,
	[IsCore] [tinyint] NOT NULL
) ON [DATA]
GO

CREATE TABLE [dbo].[tbl_CIASummary] (
	[CIASummaryId] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitId] [int] NOT NULL ,
	[StatusId] [tinyint] NOT NULL ,
	[DT] [smalldatetime] NOT NULL ,
	[NumVehicles] [int] NOT NULL ,
	[DaysSupply] [int] NOT NULL ,
	[TargetDaysSupply] [int] NOT NULL 
) ON [DATA]
GO

ALTER TABLE [dbo].[lu_Status] WITH NOCHECK ADD 
	CONSTRAINT [PK_lu_Status] PRIMARY KEY  NONCLUSTERED 
	(
		[StatusId]
	)  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_CIAClassTypeDetail] WITH NOCHECK ADD 
	CONSTRAINT [PK_tbl_CIAClassTypeDetail] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAClassTypeDetailId]
	)  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_CIASummary] WITH NOCHECK ADD 
	CONSTRAINT [PK_tbl_CIASummary] PRIMARY KEY  NONCLUSTERED 
	(
		[CIASummaryId]
	)  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_CIASummary] ADD 
	CONSTRAINT [FK_tbl_CIASummary_lu_Status] FOREIGN KEY 
	(
		[StatusId]
	) REFERENCES [dbo].[lu_Status] (
		[StatusId]
	)
GO


INSERT INTO lu_Status ( StatusId, StatusName ) VALUES ( 1, 'Pending' )
INSERT INTO lu_Status ( StatusId, StatusName ) VALUES ( 2, 'Current' )
INSERT INTO lu_Status ( StatusId, StatusName ) VALUES ( 3, 'Prior' )
INSERT INTO lu_Status ( StatusId, StatusName ) VALUES ( 4, 'Inactive' )
GO

ALTER TABLE dbo.tbl_CIASummary ADD CONSTRAINT
	FK_tbl_CIASummary_BusinessUnit FOREIGN KEY
	(
	BusinessUnitId
	) REFERENCES dbo.BusinessUnit
	(
	BusinessUnitID
	)
GO

ALTER TABLE [dbo].[BusinessUnit] ADD 
	CONSTRAINT [FK_BusinessUnit_BusinessUnitType] FOREIGN KEY 
	(
		[BusinessUnitTypeId]
	) REFERENCES [dbo].[BusinessUnitType] (
		[BusinessUnitTypeId]
	)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Inventory_VehicleID]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Inventory] DROP CONSTRAINT FK_Inventory_VehicleID
go


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_VehicleAttributes_Vehicle]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[VehicleAttributes] DROP CONSTRAINT FK_VehicleAttributes_Vehicle
go


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleLocatorContactDealerEvent_VehicleID]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[VehicleLocatorContactDealerEvent] DROP CONSTRAINT VehicleLocatorContactDealerEvent_VehicleID


go
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_VehicleOption_VehicleID]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[VehicleOption] DROP CONSTRAINT FK_VehicleOption_VehicleID
go


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_VehiclePhoto_VehicleID]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[VehiclePhoto] DROP CONSTRAINT FK_VehiclePhoto_VehicleID
go

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_VehicleAttributes_Vehicle]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[VehicleAttributes] DROP CONSTRAINT FK_VehicleAttributes_Vehicle
go

ALTER TABLE [dbo].[Vehicle] DROP CONSTRAINT [FK_DMIVehicleClassificationCD]
go
ALTER TABLE [dbo].[Vehicle] DROP CONSTRAINT [FK_DMIVehicleTypeCD]
GO
ALTER TABLE dbo.Vehicle DROP COLUMN DMIVehicleClassificationCD
GO
ALTER TABLE dbo.Vehicle DROP COLUMN DMIVehicleTypeCD
GO
exec sp_rename 'FK_Vehicle_MakeModelGroupingID', 'FK_tbl_Vehicle_MakeModelGrouping'
exec sp_rename 'FK_Vehicle_VehicleBodyStyleID', 'FK_tbl_Vehicle_VehicleBodyStyleMapping'
exec sp_rename 'DF_Vehicle_CreateDt', 'DF_tbl_Vehicle_CreateDt'
exec sp_rename 'UQ_Vehicle_Vin', 'UQ_tbl_Vehicle_Vin'
exec sp_rename 'PK_Vehicle', 'PK_tbl_Vehicle'
exec sp_rename 'Vehicle', 'tbl_Vehicle'
exec sp_rename 'VehicleAttributes', 'tbl_VehicleAttributes'

GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[lu_DMIVehicleType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[lu_DMIVehicleType]


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[lu_DMIVehicleClassification]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[lu_DMIVehicleClassification]
go


ALTER TABLE [dbo].[Inventory] ADD CONSTRAINT [FK_Inventory_tbl_Vehicle_VehicleID] FOREIGN KEY 
	(
		[VehicleID]
	) REFERENCES [tbl_Vehicle] (
		[VehicleID]
	)


delete from VehicleLocatorContactDealerEvent where vehicleid not in (Select vehicleid from tbl_vehicle)
go


ALTER TABLE [dbo].[VehicleLocatorContactDealerEvent] ADD CONSTRAINT [FK_VehicleLocatorContactDealerEvent_tbl_Vehicle_VehicleID] FOREIGN KEY 
	(
		[VehicleID]
	) REFERENCES [tbl_Vehicle] (
		[VehicleID]
	)ON DELETE CASCADE ON UPDATE CASCADE 

ALTER TABLE [dbo].[VehicleOption] ADD CONSTRAINT [FK_VehicleOption_tbl_Vehicle_VehicleID] FOREIGN KEY 
	(
		[VehicleID]
	) REFERENCES [tbl_Vehicle] (
		[VehicleID]
	)ON DELETE  CASCADE ON UPDATE CASCADE 

ALTER TABLE [dbo].[VehiclePhoto] ADD CONSTRAINT [FK_VehiclePhoto_tbl_Vehicle_VehicleID] FOREIGN KEY 
	(
		[VehicleID]
	) REFERENCES [tbl_Vehicle] (
		[VehicleID]
	)ON DELETE  CASCADE ON UPDATE CASCADE 

ALTER TABLE [dbo].[tbl_VehicleAttributes] ADD CONSTRAINT [FK_tbl_VehicleAttributes_tbl_Vehicle] FOREIGN KEY 
	(
		[VehicleID]
	) REFERENCES [tbl_Vehicle] (
		[VehicleID]
	) ON DELETE CASCADE ON UPDATE CASCADE 
GO






exec sp_rename 'dbo.lu_VehicleSubSegment.SubSegmentID', 'VehicleSubSegmentID','COLUMN'
exec sp_rename 'dbo.lu_VehicleSubSegment.SubSegmentDESC', 'VehicleSubSegmentDESC','COLUMN'
exec sp_rename 'dbo.map_VehicleSubTypeToSubSegment.SubSegmentID', 'VehicleSubSegmentID','COLUMN'
go


CREATE TABLE dbo.tbl_VehicleGuideBookIdentifier
	(
	VehicleID int NOT NULL,
	GuideBookID tinyint NOT NULL,
	Identifier varchar(50) NOT NULL
	)  ON DATA
GO
ALTER TABLE dbo.tbl_VehicleGuideBookIdentifier ADD CONSTRAINT
	PK_tbl_VehicleGuideBookIdentifier PRIMARY KEY NONCLUSTERED 
	(
	VehicleID,
	GuideBookID
	) ON IDX
create table dbo.lu_FLTypeClass
(
FLTypeClassID smallint not null,
FLTypeClassDesc varchar (25) not null
CONSTRAINT PK_lu_FLTypeClass PRIMARY KEY NONCLUSTERED
(FLTypeClassID),
CONSTRAINT UQ_lu_FLTypeClass_FLTypeClassDesc UNIQUE
(FLTypeClassDesc)
)
GO

GO
ALTER TABLE dbo.tbl_VehicleGuideBookIdentifier ADD CONSTRAINT
	FK_tbl_VehicleGuideBookIdentifier_lu_GuideBook FOREIGN KEY
	(
	GuideBookID
	) REFERENCES dbo.lu_GuideBook
	(
	GuideBookID
	)
GO
ALTER TABLE dbo.tbl_VehicleGuideBookIdentifier ADD CONSTRAINT
	FK_tbl_VehicleGuideBookIdentifier_tbl_Vehicle FOREIGN KEY
	(
	VehicleID
	) REFERENCES dbo.tbl_Vehicle
	(
	VehicleID
	)
GO
SET IDENTITY_INSERT lu_VehicleSubSegment ON
insert into lu_VehicleSubSegment
(VehicleSubSegmentID, VehicleSubSegmentDesc)
select
15,'SPORTY'
union
select
17,'STANDARD'
SET IDENTITY_INSERT lu_VehicleSubSegment OFF
go
update map_VehicleSubTypeToSubSegment
set VehicleSubSegmentID = 15
where VehicleSubTypeId = 15
go
insert into lu_FLTypeClass
(FLTypeClassID, FLTypeClassDesc)
select
16, 'Coupe-Small'
union
select 
14, 'Coupe-Medium'
union
select
13, 'Coupe-Luxury'
union
select
115, 'Coupe-Sporty'
union
select
26, 'Sedan-Small'
union
select
24, 'Sedan-Medium'
union
select
22, 'Sedan-Fullsize'
union
select
23, 'Sedan-Luxury'
union
select
36, 'SUV-Small'
union
select
34, 'SUV-Medium'
union
select
32, 'SUV-FullSize'
union
select
33, 'SUV-Luxury'
union
select
42, 'Truck-Fullsize'
union
select
41,  'Truck-Compact'
union
select
55, 'Van-Mini'
union
select
52, 'Van-FullSize'
union
select
63, 'Wagon-Luxury'
union
select
617, 'Wagon-Standard'
go

ALTER TABLE [dbo].[tbl_CIAClassTypeDetail] ADD 
	CONSTRAINT [FK_tbl_CIAClassTypeDetail_lu_FLTypeClass] FOREIGN KEY 
	(
		[CIAClassTypeId]
	) REFERENCES [dbo].[lu_FLTypeClass] (
		[FLTypeClassId]
	),
	CONSTRAINT [FK_tbl_CIAClassTypeDetail_tbl_CIASummary] FOREIGN KEY 
	(
		[CIASummaryId]
	) REFERENCES [dbo].[tbl_CIASummary] (
		[CIASummaryId]
	)
GO

/* Begin Aging Plan fixes */
ALTER TABLE [dbo].[VehiclePlanTrackingHeader] ADD [Status] tinyint
GO

ALTER TABLE [dbo].[VehiclePlanTrackingHeader] ADD [Date] smalldatetime
GO

--Current
UPDATE [dbo].[VehiclePlanTrackingHeader] SET Status = 2 
WHERE weeknumber = datepart(ww, getDate() )
GO
UPDATE [dbo].[VehiclePlanTrackingHeader] SET [Date] = '04/04/2004'
WHERE weeknumber = datepart(ww, getDate() )
GO

--Prior
UPDATE [dbo].[VehiclePlanTrackingHeader] SET Status = 3
WHERE weeknumber = ( datepart(ww, getDate() ) - 1 )
GO
UPDATE [dbo].[VehiclePlanTrackingHeader] SET [Date] = '03/28/2004'
WHERE weeknumber = ( datepart(ww, getDate() ) - 1 )
GO

-- Inactive
UPDATE [dbo].[VehiclePlanTrackingHeader] 
SET [Date] = vpt.Created
FROM VehiclePlanTrackingHeader vpth, VehiclePlanTracking vpt
WHERE vpth.status IS NULL
  AND vpth.VehiclePlanTrackingHeaderId = vpt.VehiclePlanTrackingHeaderId
  AND vpt.Created IS NOT NULL
  AND vpt.Created = (SELECT MIN(vpt2.Created) 
		     FROM VehiclePlanTracking vpt2
		     WHERE vpt2.VehiclePlanTrackingHeaderId = vpt.VehiclePlanTrackingHeaderId)
            
GO

UPDATE [dbo].[VehiclePlanTrackingHeader] SET Status = 4 
WHERE status IS NULL
GO

ALTER TABLE [dbo].[VehiclePlanTrackingHeader] ALTER COLUMN [Status] tinyint NOT NULL
GO

ALTER TABLE [dbo].[VehiclePlanTrackingHeader] DROP COLUMN [WeekNumber] 
GO

ALTER TABLE [dbo].[VehiclePlanTrackingHeader] DROP COLUMN [YearNumber] 
GO

ALTER TABLE [dbo].[VehiclePlanTracking] DROP CONSTRAINT [UQ_VehiclePlanTrackingUnique_InventoryID_BU]
GO

ALTER TABLE [dbo].[VehiclePlanTracking] DROP COLUMN [WeekId] 

GO

ALTER TABLE [dbo].[VehiclePlanTracking] ADD CONSTRAINT [UQ_VehiclePlanTrackingUnique_InventoryID_BU] UNIQUE  NONCLUSTERED 
	(
		[InventoryID],
		[BusinessUnitID],
		[VehiclePlanTrackingHeaderID]
	) WITH  FILLFACTOR = 80  ON [DATA] 
GO

/* End Aging Plan Fixes */

ALTER TABLE [dbo].[tbl_VehicleAttributes] DROP CONSTRAINT [FK_VehicleAttributes_lu_VehicleSegment]

GO

ALTER TABLE [dbo].[tbl_VehicleAttributes] ADD CONSTRAINT [FK_VehicleAttributes_lu_VehicleSegment] FOREIGN KEY 
	(
		[VehicleSegmentID]
	) REFERENCES [lu_VehicleSegment] (
		[vehiclesegmentid]
	)
GO