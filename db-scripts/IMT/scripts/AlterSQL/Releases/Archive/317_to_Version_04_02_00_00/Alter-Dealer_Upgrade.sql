
insert 
into lu_DealerUpgrade (DealerUpgradeCd, DealerUpgradeDESC) 
select	1,'Trade Analyzer'
where 	not exists	(select 1 from lu_DealerUpgrade where DealerUpgradeCd = 1)
GO

insert into lu_DealerUpgrade (DealerUpgradeCd, DealerUpgradeDESC) 
select 2,'Aging Inventory Plan'
where  not exists   (select 2 from lu_DealerUpgrade where DealerUpgradeCd = 2)
GO

insert into lu_DealerUpgrade (DealerUpgradeCd, DealerUpgradeDESC)
select 3,'Custom Inventory Analysis'
where  not exists   (select 3 from lu_DealerUpgrade where DealerUpgradeCd = 3)
GO

insert into lu_DealerUpgrade (DealerUpgradeCd, DealerUpgradeDESC)
select 4,'Redistribution'
where  not exists   (select 4 from lu_DealerUpgrade where DealerUpgradeCd = 4)
GO

insert into lu_DealerUpgrade (DealerUpgradeCd, DealerUpgradeDESC)
select 5,'Auction Data'
where  not exists   (select 5 from lu_DealerUpgrade where DealerUpgradeCd = 5)
GO

insert into lu_DealerUpgrade (DealerUpgradeCd, DealerUpgradeDESC)
select 6,'Window Sticker'
where  not exists   (select 6 from lu_DealerUpgrade where DealerUpgradeCd = 6)
GO

insert into lu_DealerUpgrade (DealerUpgradeCd, DealerUpgradeDESC)
select 7,'Performance Dashboard'
where  not exists   (select 7 from lu_DealerUpgrade where DealerUpgradeCd = 7)
GO

insert into lu_DealerUpgrade (DealerUpgradeCd, DealerUpgradeDESC)
select 8,'Market Data'
where  not exists   (select 8 from lu_DealerUpgrade where DealerUpgradeCd = 8)
GO