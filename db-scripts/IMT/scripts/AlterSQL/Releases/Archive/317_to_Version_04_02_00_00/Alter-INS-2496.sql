ALTER TABLE [dbo].[DealerPreference] 
ADD SellThroughRate INT NOT NULL DEFAULT 90

ALTER TABLE [dbo].[DealerPreference] 
ADD IncludeBackEndInValuation TINYINT NOT NULL DEFAULT 1