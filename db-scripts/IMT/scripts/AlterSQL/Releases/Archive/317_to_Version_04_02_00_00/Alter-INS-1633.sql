if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_TargetDaysSupply]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_TargetDaysSupply]
GO

CREATE TABLE [dbo].[tbl_TargetDaysSupply] (
	[TargetDaysSupplyId] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[FLTypeClassID] [smallint] NULL ,
	[TargetDaysSupply] [int] NOT NULL 
) ON [DATA]
GO

INSERT INTO tbl_TargetDaysSupply (BusinessUnitID, TargetDaysSupply) select businessunitid, 45 from businessunit
GO

-- This line must be executed so that FL level business unit has a default value for the target days supply
INSERT INTO tbl_TargetDaysSupply (BusinessUnitID, TargetDaysSupply) values (100150, 45)
GO


