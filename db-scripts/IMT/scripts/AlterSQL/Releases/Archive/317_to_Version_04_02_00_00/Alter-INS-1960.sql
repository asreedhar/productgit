print 'INS-1960 Alter Script'
go

ALTER TABLE [dbo].DealerPreference
ADD MarginPercentile SMALLINT NULL
GO

ALTER TABLE [dbo].DealerPreference
ADD DaysToSalePercentile SMALLINT NULL
GO

UPDATE DealerPreference
SET MarginPercentile=35, DaysToSalePercentile=90
GO