print 'INS-1934 Alter Script'
go

ALTER TABLE [dbo].DealerPreference
ADD FEGrossProfitThreshold INT NULL DEFAULT (-1000000)
GO

UPDATE DealerPreference
SET FEGrossProfitThreshold=0
GO