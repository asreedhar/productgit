print 'INS-1665 Alter Script'
go
CREATE TABLE [dbo].[DecodedSquishVINs] (
	[squish_vin] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[edstyleid] [int] NULL ,
	[year] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[make] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MakeModelGroupingID] [int] NULL ,
	[trim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[bodyType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[class] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[doors] [tinyint] NULL ,
	[fuel_type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[engine] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[drive_type_code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[trans] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[cylinder_qty] [tinyint] NULL 
) ON [DATA]
GO

ALTER TABLE [dbo].[DecodedSquishVINs] WITH NOCHECK ADD 
	CONSTRAINT [PK_DecodedSquishVINs] PRIMARY KEY  CLUSTERED 
	(
		[squish_vin]
	)  ON [DATA] 
GO

CREATE TABLE [dbo].[ModelBodyStyleOverrides] (
	[Make] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Model] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[BodyType] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ModelAppend] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

ALTER TABLE [dbo].[ModelBodyStyleOverrides] WITH NOCHECK ADD 
	CONSTRAINT [PK_ModelNameOverrides] PRIMARY KEY  CLUSTERED 
	(
		[Make],
		[Model],
		[BodyType]
	)  ON [DATA] 
GO


insert
into	ModelBodyStyleOverrides

select	'HONDA','CIVIC','COUP','COUPE' 
union
select	'HONDA','CIVIC','HBK', 'COUPE'
union
select	'HONDA','CIVIC','SDN', 'SEDAN'
union
select	'HONDA','CIVIC','WAGN','SEDAN'
union
select	'HONDA','ACCORD','COUP','COUPE'
union
select	'HONDA','ACCORD','HBK', 'COUPE'
union
select	'HONDA','ACCORD','SDN', 'SEDAN'
union
select	'HONDA','ACCORD','WAGN','SEDAN'

GO


