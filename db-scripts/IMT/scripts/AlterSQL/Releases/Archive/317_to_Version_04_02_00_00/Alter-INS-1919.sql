print 'INS-1919 Alter Script'
go
sp_rename 'ModelBodyStyleOverrides.ModelAppend', 'EnhancedModel','COLUMN'
go
alter table ModelBodyStyleOverrides alter column EnhancedModel varchar(25)
go
update	ModelBodyStyleOverrides
set	EnhancedModel = Model + ' ' + EnhancedModel
go