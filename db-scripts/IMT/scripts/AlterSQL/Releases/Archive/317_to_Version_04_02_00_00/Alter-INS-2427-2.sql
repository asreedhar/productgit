create table dbo.InventoryStatusCodes (InventoryStatusCD smallint identity(1,1), ShortDescription char(1), Description varchar(50))
 

alter table dbo.InventoryStatusCodes with nocheck add 
 constraint PK_InventoryStatusCodes primary key  clustered 
 (
  InventoryStatusCD
 )  ON DATA
GO
 

set identity_insert InventoryStatusCodes on
insert 
into InventoryStatusCodes (InventoryStatusCD, ShortDescription, Description)
select 0, '-','N/A'
set identity_insert InventoryStatusCodes off
GO
 
insert
into InventoryStatusCodes (ShortDescription, Description)
select  distinct StatusCode, StatusDescription  
from DBASTAT..Longo_BusinessUnit_Inventory_Status BIS
 join BusinessUnit BU on BIS.BusinessUnitID = BU.BusinessUnitID
 join DBASTAT..Longo_Inventory_Status_Codes ISC on BIS.InventoryStatusID = ISC.InventoryStatusID
 join DBASTAT..Longo_Inventory_Status_Descriptions ISD on ISC.StatusDescriptionID = ISD.StatusDescriptionID
 
GO
 
alter table Inventory add InventoryStatusCD smallint
go
update Inventory set InventoryStatusCD = 0
go
 
alter table Inventory alter column InventoryStatusCD smallint not null
go
 
alter table dbo.Inventory ADD 
 constraint DF_Inventory_InventoryStatusCD DEFAULT (0) FOR InventoryStatusCD