/******** Iteration 118 Alter Statements ***********/
print '118 Alter Script'
go
create index IDX_Squishvin on dbo.lu_edmunds_squishvin (squish_vin) 
WITH FILLFACTOR = 100 ON IDX
go
update lu_edmunds_squishvin
set used_ed_style_id = 0
where used_ed_style_id is null

update lu_edmunds_style_new
set trim_line = ''
where trim_line is null

update lu_edmunds_style_used
set trim_line = ''
where trim_line is null
go



if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleAttributes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[VehicleAttributes]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[lu_VehicleSegment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[lu_VehicleSegment]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[lu_VehicleSubType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[lu_VehicleSubType]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[lu_VehicleType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[lu_VehicleType]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[lu_vehicleSubSegment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[lu_vehicleSubSegment]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[map_VehicleSubTypeToSubSegment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[map_VehicleSubTypeToSubSegment]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[map_VehicleTypeToSegment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[map_VehicleTypeToSegment]
GO

CREATE TABLE [dbo].[VehicleAttributes] (
	[VehicleID] [int] NOT NULL ,
	[VehicleSegmentID] [tinyint] NOT NULL ,
	[VehicleSubSegmentID] [tinyint] NOT NULL 
) ON [DATA]
GO

CREATE TABLE [dbo].[lu_VehicleSegment] (
	[vehiclesegmentid] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[vehiclesegmentdesc] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

CREATE TABLE [dbo].[lu_VehicleSubType] (
	[VehicleSubTypeID] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[VehicleSubTypeDESC] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

CREATE TABLE [dbo].[lu_VehicleType] (
	[VehicleTypeID] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[VehicleTypeDesc] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

CREATE TABLE [dbo].[lu_VehicleSubSegment] (
	[SubSegmentID] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[SubSegmentDesc] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

CREATE TABLE [dbo].[map_VehicleSubTypeToSubSegment] (
	[VehicleSubTypeID] [tinyint] NOT NULL ,
	[SubSegmentID] [tinyint] NOT NULL 
) ON [DATA]
GO

CREATE TABLE [dbo].[map_VehicleTypeToSegment] (
	[VehicleTypeID] [tinyint] NOT NULL ,
	[VehicleSegmentID] [tinyint] NOT NULL 
) ON [DATA]
GO

ALTER TABLE [dbo].[VehicleAttributes] ADD 
	CONSTRAINT [PK_VehicleAttributes] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleID]
	) WITH  FILLFACTOR = 90  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_VehicleSegment] ADD 
	CONSTRAINT [PK_lu_VehicleSegment] PRIMARY KEY  NONCLUSTERED 
	(
		[vehiclesegmentid]
	) WITH  FILLFACTOR = 100  ON [IDX] ,
	CONSTRAINT [UQ_lu_VehicleSegment] UNIQUE  NONCLUSTERED 
	(
		[vehiclesegmentdesc]
	) WITH  FILLFACTOR = 100  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_VehicleSubType] ADD 
	CONSTRAINT [PK_lu_VehicleSubType] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleSubTypeID]
	) WITH  FILLFACTOR = 100  ON [IDX] ,
	CONSTRAINT [UQ_lu_VehicleSubType] UNIQUE  NONCLUSTERED 
	(
		[VehicleSubTypeDESC]
	) WITH  FILLFACTOR = 100  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_VehicleType] ADD 
	CONSTRAINT [PK_lu_VehicleType] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleTypeID]
	) WITH  FILLFACTOR = 100  ON [IDX] ,
	CONSTRAINT [UQ_lu_VehicleType] UNIQUE  NONCLUSTERED 
	(
		[VehicleTypeDesc]
	) WITH  FILLFACTOR = 100  ON [IDX] 
GO

ALTER TABLE [dbo].[lu_vehicleSubSegment] ADD 
	CONSTRAINT [PK_lu_vehicleSubSegment] PRIMARY KEY  NONCLUSTERED 
	(
		[SubSegmentID]
	) WITH  FILLFACTOR = 100  ON [IDX] ,
	CONSTRAINT [UQ_lu_vehicleSubSegment] UNIQUE  NONCLUSTERED 
	(
		[SubSegmentDesc]
	) WITH  FILLFACTOR = 100  ON [IDX] 
GO

ALTER TABLE [dbo].[map_VehicleSubTypeToSubSegment] ADD 
	CONSTRAINT [PK_map_VehicleSubTypeToSubSegment] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleSubTypeID]
	) WITH  FILLFACTOR = 100  ON [IDX] 
GO

ALTER TABLE [dbo].[map_VehicleTypeToSegment] ADD 
	CONSTRAINT [PK_map_VehicleTypeToSegment] PRIMARY KEY  NONCLUSTERED 
	(
		[VehicleTypeID]
	) WITH  FILLFACTOR = 100  ON [IDX] 
GO

ALTER TABLE [dbo].[VehicleAttributes] ADD 
	CONSTRAINT [FK_VehicleAttributes_lu_VehicleSegment] FOREIGN KEY 
	(
		[VehicleSubSegmentID]
	) REFERENCES [dbo].[lu_VehicleSegment] (
		[vehiclesegmentid]
	),
	CONSTRAINT [FK_VehicleAttributes_lu_vehicleSubSegment] FOREIGN KEY 
	(
		[VehicleSubSegmentID]
	) REFERENCES [dbo].[lu_vehicleSubSegment] (
		[SubSegmentID]
	),
	CONSTRAINT [FK_VehicleAttributes_Vehicle] FOREIGN KEY 
	(
		[VehicleID]
	) REFERENCES [dbo].[Vehicle] (
		[VehicleID]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

ALTER TABLE [dbo].[map_VehicleSubTypeToSubSegment] ADD 
	CONSTRAINT [FK_map_VehicleSubTypeToSubSegment_lu_vehicleSubSegment] FOREIGN KEY 
	(
		[SubSegmentID]
	) REFERENCES [dbo].[lu_vehicleSubSegment] (
		[SubSegmentID]
	),
	CONSTRAINT [FK_map_VehicleSubTypeToSubSegment_lu_VehicleSubType] FOREIGN KEY 
	(
		[VehicleSubTypeID]
	) REFERENCES [dbo].[lu_VehicleSubType] (
		[VehicleSubTypeID]
	)
GO

ALTER TABLE [dbo].[map_VehicleTypeToSegment] ADD 
	CONSTRAINT [FK_map_VehicleTypeToSegment_lu_VehicleSegment] FOREIGN KEY 
	(
		[VehicleSegmentID]
	) REFERENCES [dbo].[lu_VehicleSegment] (
		[vehiclesegmentid]
	),
	CONSTRAINT [FK_map_VehicleTypeToSegment_lu_VehicleType] FOREIGN KEY 
	(
		[VehicleTypeID]
	) REFERENCES [dbo].[lu_VehicleType] (
		[VehicleTypeID]
	)
GO

/*segment*/
set identity_insert lu_vehiclesegment on
insert lu_vehiclesegment
( vehiclesegmentid, vehiclesegmentdesc)
select
1,	'COUPE'
union
select
2	,'SEDAN'
union
select
3	,'SUV'
union
select
4	,'TRUCK'
union
select
5	,'VAN'
union
select
6	,'WAGON'
union
select
99	,'UNKNOWN'
set identity_insert lu_vehiclesegment off
go
/*subsegment*/
set identity_insert lu_vehiclesubsegment on
insert lu_vehiclesubsegment
(subsegmentid, subsegmentdesc)
select
1	,'COMPACT'
union
select
2	,'FULLSIZE'
union
select
3	,'LUXURY'
union
select
4	,'MEDIUM'
union
select
5	,'MINI'
union
select
6	,'SMALL'
union
select
99	,'UNKNOWN'
set identity_insert lu_vehiclesubsegment off
go
/*type*/
set identity_insert lu_vehicletype on
insert lu_vehicletype
(vehicletypeid, vehicletypedesc)
select
1	,'Convertible'
union
select
2	,'Coupe'
union
select
3,	'Hatchback'
union
select
4	,'Sedan'
union
select
5	,'SUV'
union
select
6	,'Truck'
union
select
7	,'Van'
union
select
8	,'Wagon'

set identity_insert lu_vehicletype off
go
/*subtype*/
set identity_insert lu_vehiclesubtype on
insert lu_vehiclesubtype
(vehiclesubtypeid, vehiclesubtypedesc)
select
1	,'Cargo'
union
select
2	,'Compact'
union
select
3	,'Compact Crew Cab'
union
select
4	,'Compact Extended Cab'
union
select
5	,'Compact Regular Cab'
union
select
6	,'Exotic'
union
select
7	,'Fullsize'
union
select
8	,'Fullsize Crew Cab'
union
select
9	,'Fullsize Extended Cab'
union
select
10	,'Fullsize Regular Cab'
union
select
11	,'Luxury'
union
select
12,	'Midsize'
union
select
13	,'Mini'
union
select
14	,'Minivan'
union
select
15	,'Sport'
union
select
16	,'Subcompact'
set identity_insert lu_vehiclesubtype off
go
/*map1*/
insert map_VehicleTypeToSegment
(VehicleTypeID, VehicleSegmentID)
select
1,	1
union
select
2,	1
union
select
3,	1
union
select
4,	2
union
select
5,	3
union
select
6,	4
union
select
7,	5
union
select
8,	6
go
/*map2*/
insert map_VehicleSubTypeToSubSegment
(VehicleSubTypeID, SubSegmentID)
select
1,	2
union
select
2,	6
union
select
3,	1
union
select
4,	1
union
select
5,	1
union
select
6,	3
union
select
7,	2
union
select
8,	2
union
select
9,	2
union
select
10,	2
union
select
11,	3
union
select
12,	4
union
select
13,6
union
select
14, 5
union
select
15, 6
union
select
16, 6
go





ALTER TABLE member
	ALTER COLUMN Password VARCHAR(80)
GO