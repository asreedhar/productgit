print 'INS-2390 Alter Script'
go

CREATE TABLE [dbo].[lu_AppraisalGuideBookValueTypes]
(
	[Id] tinyint NOT NULL PRIMARY KEY,
	[Description] VARCHAR(25) NOT NULL
)

INSERT INTO [dbo].[lu_AppraisalGuideBookValueTypes] ( [Id], [Description] ) VALUES ( 1, 'Base Value' )
INSERT INTO [dbo].[lu_AppraisalGuideBookValueTypes] ( [Id], [Description] ) VALUES ( 2, 'Final Value' )
INSERT INTO [dbo].[lu_AppraisalGuideBookValueTypes] ( [Id], [Description] ) VALUES ( 3, 'Mileage Independent' )

GO

ALTER TABLE dbo.AppraisalGuideBookValues
ADD ValueType tinyint NULL
GO

UPDATE AppraisalGuideBookValues
SET ValueType = 2
GO

ALTER TABLE AppraisalGuideBookValues
ALTER COLUMN ValueType tinyint NOT NULL
GO

ALTER TABLE AppraisalGuideBookValues
ADD CONSTRAINT [FK_Type_lu_AppraisalValueType] FOREIGN KEY 
	(
		ValueType
	) 
		REFERENCES lu_AppraisalGuideBookValueTypes
	(
		[Id]
	)
GO
