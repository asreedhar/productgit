CREATE TABLE dbo.Audit_Exceptions_DMI (
	Audit_ID int NOT NULL ,
	SourceTableID char (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	ExceptionRuleID int NOT NULL ,
	TimeStamp datetime NOT NULL 
) ON DATA
GO

ALTER TABLE dbo.Audit_Exceptions_DMI ADD 
	CONSTRAINT DF_Audit_Exceptions_DMI_TimeStamp DEFAULT (getdate()) FOR TimeStamp
GO
ALTER TABLE dbo.Audit_Inventory_DMI ADD AUDIT_ID int NULL
GO

ALTER TABLE dbo.Audit_Sales_DMI ADD AUDIT_ID int NULL
GO
