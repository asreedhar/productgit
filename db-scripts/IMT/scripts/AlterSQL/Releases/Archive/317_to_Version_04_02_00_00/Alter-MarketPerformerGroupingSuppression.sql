
alter table tbl_GroupingDescription add SuppressMarketPerformer bit default (0)
go
update	tbl_GroupingDescription
set	SuppressMarketPerformer = 0
go
update tbl_GroupingDescription
set	SuppressMarketPerformer = 1
where	GroupingDescription like 'FORD TAURUS%'