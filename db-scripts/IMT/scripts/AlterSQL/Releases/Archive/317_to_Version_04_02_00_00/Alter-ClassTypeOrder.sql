
ALTER TABLE dbo.map_FLTypeClassToSegments ADD FLTypeClassOrder INT NULL
GO

UPDATE dbo.map_FLTypeClassToSegments SET FLTypeClassOrder = 1 WHERE FLTypeClassId = 3
UPDATE dbo.map_FLTypeClassToSegments SET FLTypeClassOrder = 2 WHERE FLTypeClassId = 2
UPDATE dbo.map_FLTypeClassToSegments SET FLTypeClassOrder = 3 WHERE FLTypeClassId = 1
UPDATE dbo.map_FLTypeClassToSegments SET FLTypeClassOrder = 4 WHERE FLTypeClassId = 4
UPDATE dbo.map_FLTypeClassToSegments SET FLTypeClassOrder = 5 WHERE FLTypeClassId = 8
UPDATE dbo.map_FLTypeClassToSegments SET FLTypeClassOrder = 6 WHERE FLTypeClassId = 7
UPDATE dbo.map_FLTypeClassToSegments SET FLTypeClassOrder = 7 WHERE FLTypeClassId = 5
UPDATE dbo.map_FLTypeClassToSegments SET FLTypeClassOrder = 8 WHERE FLTypeClassId = 6
UPDATE dbo.map_FLTypeClassToSegments SET FLTypeClassOrder = 9 WHERE FLTypeClassId = 12
UPDATE dbo.map_FLTypeClassToSegments SET FLTypeClassOrder = 10 WHERE FLTypeClassId = 11
UPDATE dbo.map_FLTypeClassToSegments SET FLTypeClassOrder = 11 WHERE FLTypeClassId = 9
UPDATE dbo.map_FLTypeClassToSegments SET FLTypeClassOrder = 12 WHERE FLTypeClassId = 10
UPDATE dbo.map_FLTypeClassToSegments SET FLTypeClassOrder = 13 WHERE FLTypeClassId = 13
UPDATE dbo.map_FLTypeClassToSegments SET FLTypeClassOrder = 14 WHERE FLTypeClassId = 14
UPDATE dbo.map_FLTypeClassToSegments SET FLTypeClassOrder = 15 WHERE FLTypeClassId = 16
UPDATE dbo.map_FLTypeClassToSegments SET FLTypeClassOrder = 16 WHERE FLTypeClassId = 15
UPDATE dbo.map_FLTypeClassToSegments SET FLTypeClassOrder = 17 WHERE FLTypeClassId = 18
UPDATE dbo.map_FLTypeClassToSegments SET FLTypeClassOrder = 18 WHERE FLTypeClassId = 17
GO

ALTER TABLE dbo.map_FLTypeClassToSegments ALTER COLUMN FLTypeClassOrder INT NOT NULL
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_lu_FLTypeClass]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_lu_FLTypeClass]
GO


