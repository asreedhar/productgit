
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_RedistributionLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_RedistributionLog]
GO 


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[lu_RedistributionSource]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[lu_RedistributionSource]
GO 

CREATE TABLE [dbo].[lu_RedistributionSource] (
	[RedistributionSourceId] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[Description] VARCHAR(25) NOT NULL ,
	CONSTRAINT [PK_lu_RedistributionSource] PRIMARY KEY  NONCLUSTERED 
	(
		[RedistributionSourceId]
	)  ON [IDX] 
) ON [DATA]
GO

SET IDENTITY_INSERT lu_RedistributionSource ON
INSERT INTO [dbo].[lu_RedistributionSource] ( RedistributionSourceId, Description ) VALUES ( 1, 'Appraisal' )
INSERT INTO [dbo].[lu_RedistributionSource] ( RedistributionSourceId, Description ) VALUES ( 2, 'Showroom' )
INSERT INTO [dbo].[lu_RedistributionSource] ( RedistributionSourceId, Description ) VALUES ( 3, 'Flash Locator' )
INSERT INTO [dbo].[lu_RedistributionSource] ( RedistributionSourceId, Description ) VALUES ( 4, 'Custom Inventory Exchange' )
INSERT INTO [dbo].[lu_RedistributionSource] ( RedistributionSourceId, Description ) VALUES ( 5, 'External Redistribution Candidate' )
SET IDENTITY_INSERT lu_RedistributionSource OFF
GO

CREATE TABLE [dbo].[tbl_RedistributionLog] (
	[RedistributionLogId] [int] IDENTITY (1, 1) NOT NULL ,
	[VIN] VARCHAR(17) NOT NULL ,
	[OriginatingDealerId] [int] NOT NULL ,
	[DestinationDealerId] [int] NULL ,
	[RedistributionSourceId] [tinyint] NOT NULL,
	[ReceivedDate] [smalldatetime] NULL,
	[OriginatingDealerStockNumber] VARCHAR(15) NULL ,
	[DestinationDealerStockNumber] VARCHAR(15) NULL ,
	[MemberId] [int] NOT NULL ,
	[TransactionDate] [smalldatetime] NOT NULL,
	CONSTRAINT [PK_tbl_RedistributionLog] PRIMARY KEY  NONCLUSTERED 
	(
		[RedistributionLogId]
	)  ON [IDX] ,
	CONSTRAINT [FK_tbl_RedistributionLog_Dealer] FOREIGN KEY 
	(
		[OriginatingDealerId]
	) REFERENCES [BusinessUnit] (
		[BusinessUnitId]
	),
	CONSTRAINT [FK_tbl_RedistributionLog_Member] FOREIGN KEY 
	(
		[MemberId]
	) REFERENCES [Member] (
		[MemberId]
	),
	CONSTRAINT [FK_tbl_RedistributionLog_lu_RedistributionSource] FOREIGN KEY 
	(
		[RedistributionSourceId]
	) REFERENCES [lu_RedistributionSource] (
		[RedistributionSourceId]
	)
) ON [DATA]
GO

insert
into	Member (Login, FirstName, LastName, OfficePhoneNumber, CreateDate, UserRoleCD)
select	'Redistribution' Login, 'Redistribution' FirstName, 'Candidates' LastName, 'xxx-xxxx' OfficePhoneNumber, getdate() CreateDate, 3 UserRoleCD 
GO