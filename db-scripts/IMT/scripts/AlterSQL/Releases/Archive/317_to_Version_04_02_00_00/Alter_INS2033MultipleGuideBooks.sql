print 'Alter_2033MultipleGuideBooks'
go

ALTER TABLE dbo.Appraisal
	ADD GuideBookId tinyint NULL
GO

UPDATE Appraisal
	SET Appraisal.GuideBookId = ( Select d.GuideBookId 
				from DealerPreference d
				where d.BusinessUnitId = Appraisal.BusinessUnitId )
GO

ALTER TABLE dbo.Appraisal
	ALTER COLUMN GuideBookId tinyint NOT NULL
GO
 
ALTER TABLE [dbo].[Appraisal] ADD CONSTRAINT [FK_Appraisal_lu_GuideBook] FOREIGN KEY 
	(
		[GuideBookId]
	) REFERENCES [lu_GuideBook] (
		[GuideBookId]
	)
GO
