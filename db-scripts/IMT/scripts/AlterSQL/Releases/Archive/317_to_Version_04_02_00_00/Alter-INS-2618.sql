ALTER TABLE [dbo].[tbl_CIAGroupingItem]
	DROP column CIAClassTypeId 
GO

ALTER TABLE [dbo].[tbl_CIAGroupingItem]
	DROP column TargetUnits 
GO

ALTER TABLE [dbo].[tbl_CIAGroupingItem]
	ADD DisplayBodyTypeId INT DEFAULT(1) NOT NULL
GO

ALTER TABLE [dbo].[tbl_CIAGroupingItem] ADD 
	CONSTRAINT [FK_tbl_CIAGroupingItem_lu_DisplayBodyType] FOREIGN KEY 
	(
		[DisplayBodyTypeId]
	) REFERENCES [dbo].[lu_DisplayBodyType] (
		[DisplayBodyTypeId]
	)
GO