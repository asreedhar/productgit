print 'Alter-INS-2067'
go

ALTER TABLE [dbo].[DealerPreference]
ADD ProgramType_CD TINYINT NULL
GO

ALTER TABLE [dbo].[DealerPreference] ADD CONSTRAINT [FK_DealerPreference_lu_ProgramType] FOREIGN KEY 
	(
		[ProgramType_CD]
	) REFERENCES [lu_ProgramType] (
		[ProgramType_CD]
	)
GO

--Update DealerPreference.ProgramType_CD with the values from DealerGroupPreference.ProgramType_CD
UPDATE 	DealerPreference 
SET 	DealerPreference.ProgramType_CD = ( 
SELECT 	dg.ProgramType_CD 
FROM DealerGroupPreference dg, 
	BusinessUnit dealer, 
	BusinessUnit dealerGroup, 
	BusinessUnitRelationship bur
WHERE DealerPreference.BusinessUnitId = dealer.BusinessUnitId
  AND dealer.BusinessUnitId = bur.BusinessUnitId
  AND bur.ParentId = dealerGroup.BusinessUnitId
  AND dealerGroup.BusinessUnitId = dg.BusinessUnitId
)
GO

ALTER TABLE [dbo].[DealerGroupPreference] DROP CONSTRAINT [FK_DealerGroupPreference_lu_ProgramType]
GO

ALTER TABLE [dbo].[DealerGroupPreference]
DROP COLUMN ProgramType_CD
GO