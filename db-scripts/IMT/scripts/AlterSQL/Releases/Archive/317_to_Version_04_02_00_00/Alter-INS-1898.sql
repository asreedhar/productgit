ALTER TABLE dbo.Appraisal 
ADD Weight INT NULL,
	GuideBookFooter VARCHAR(255) NULL,
	MileageCostAdjustment INT NULL,
	MSRP INT NULL,
	GuideBookDescription VARCHAR(50) NULL,
	GuideBookRegion VARCHAR(50) NULL
GO
