print 'Alter-INS-2248.sql'
go

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Audit_Inventory_lu_AuditStatus]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Audit_Inventory] DROP CONSTRAINT [FK_Audit_Inventory_lu_AuditStatus]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Audit_Sales_lu_AuditStatus]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Audit_Sales] DROP CONSTRAINT [FK_Audit_Sales_lu_AuditStatus]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[lu_AuditStatus]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[lu_AuditStatus]
GO



CREATE TABLE [dbo].[lu_AuditStatus] (
	[AuditStatusCD] [tinyint] NOT NULL ,
	[AuditStatusDESC] [varchar] (25) NOT NULL ,
	[ShortDesc] [char] (1) NOT NULL ,
	CONSTRAINT [PK_lu_AuditStatus] PRIMARY KEY  NONCLUSTERED 
	(
		[AuditStatusCD]
	)  ON [IDX] 
) ON [DATA]
GO

INSERT INTO [dbo].[lu_AuditStatus]
(AuditStatusCD, AuditStatusDESC,ShortDesc)
SELECT
1,'Add','A'
union
select
2,'Update','U'
union
select
3,'Delete','D'
union
select
4,'Unwind','W'
go

-- ALTER TABLE [dbo].[Audit_Inventory] ADD CONSTRAINT [FK_Audit_Inventory_lu_AuditStatus] FOREIGN KEY 
-- 	(
-- 		[STATUS]
-- 	) REFERENCES [lu_AuditStatus] (
-- 		[AuditStatusCD]
-- 	) ON UPDATE CASCADE 
-- GO
-- 
-- 
-- ALTER TABLE [dbo].[Audit_Sales] ADD CONSTRAINT [FK_Audit_Sales_lu_AuditStatus] FOREIGN KEY 
-- 	(
-- 		[STATUS]
-- 	) REFERENCES [lu_AuditStatus] (
-- 		[AuditStatusCD]
-- 	) ON UPDATE CASCADE 
-- GO



