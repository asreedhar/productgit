CREATE TABLE [dbo].[AppraisalWindowSticker]
(
	[AppraisalWindowStickerId] [int] IDENTITY (1, 1) NOT NULL ,
	[AppraisalId] INT NOT NULL,
	[StockNumber] VARCHAR(25) NULL,
	[SellingPrice] INT NULL
)

GO


ALTER TABLE [dbo].[AppraisalWindowSticker] ADD CONSTRAINT [PK_AppraisalWindowSticker] PRIMARY KEY  NONCLUSTERED 
	(
		[AppraisalWindowStickerID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

ALTER TABLE [dbo].[AppraisalWindowSticker]
ADD CONSTRAINT [FK_AppraisalId_Appraisal] FOREIGN KEY 
	(
		AppraisalId
	) 
		REFERENCES Appraisal
	(
		[VehicleGuideBookId]
	)
GO