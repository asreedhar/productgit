print 'Alter-INS-2146.sql'
go
alter table DealerPreference add UnitCostThreshold decimal(10,2) null
go

ALTER TABLE [dbo].[DealerPreference] ADD 
	CONSTRAINT [DF_DealerPreference_UnitCostThreshold] DEFAULT (1000.0) FOR [UnitCostThreshold]
GO

update	DealerPreference set UnitCostThreshold = 1000
go

alter table DealerPreference alter column UnitCostThreshold decimal(10,2) not null
go
