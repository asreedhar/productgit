/******** Iteration 121 Alter Statements ***********/
print '121 Alter Script'
go
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_vehicleidsegment]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_vehicleidsegment]
GO

update inventory 
set Certified = 0 where 
Certified is null 
GO
alter table inventory 
alter column Certified tinyint not null 
GO

UPDATE DealerGroupPreference
SET IncludeRoundTable = 0;
GO

update vehicle
set basecolor = 'UNKNOWN'
where basecolor  is null
go

alter table vehicle
alter column basecolor varchar(50) not null
go

update vehicle
set vehicletrim = 'N/A'
from vehicle
where vehicletrim is null
go

alter table vehicle
alter column vehicletrim varchar(50) not null
go