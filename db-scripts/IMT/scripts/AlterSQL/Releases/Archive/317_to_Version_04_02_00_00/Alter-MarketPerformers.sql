print 'alter-MarketPerformers Alter Script'
go


insert 
into 	dbo.map_BusinessUnitToMarketDealer
select	100135,100142
union	
select	100135,100143

go

alter table tbl_CIAPreferences add MarketPerformersUnitsThreshold int not null default (6)
go

alter table tbl_CIAPreferences add MarketPerformersZipCodeThreshold decimal(9,2) not null default (0.8)
go