/******** Iteration 122 Alter Statements ***********/
print '122 Alter Script'
go

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.[VehicleSaleCommissions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) 
drop table dbo.[VehicleSaleCommissions] 
GO 

if exists (select * from dbo.sysindexes where name = N'UQ_VehicleSale_InventoryID' and id = object_id(N'[dbo].[VehicleSale]')) 
drop index [dbo].[VehicleSale].[UQ_VehicleSale_InventoryID] 
GO 


ALTER TABLE [dbo].[VehicleSale] DROP CONSTRAINT [FK_VehicleSale_InventoryID]
GO
ALTER TABLE [dbo].[VehicleSaleCustomerDetail] DROP CONSTRAINT [PK_VehicleSaleCustomerDetail]
GO

ALTER TABLE [dbo].[VehicleSaleCustomerDetail] DROP CONSTRAINT [FK_VehicleSaleCustomerDetail_VehicleSale]
GO

ALTER TABLE [dbo].[VehicleSale] DROP CONSTRAINT [PK_VehicleSale]
GO

update VehicleSaleCustomerDetail
set VehicleSaleID = InventoryID
from VehicleSaleCustomerDetail vscd
join VehicleSale vs on 
vscd.VehicleSaleID = vs.VehicleSaleId

exec sp_rename 'VehicleSaleCustomerDetail.VehicleSaleID', 'InventoryID', 'COLUMN'

GO
ALTER TABLE [dbo].[VehicleSale] DROP COLUMN [VehicleSaleID]

GO

exec sp_rename 'VehicleSale','tbl_VehicleSale'
GO

ALTER TABLE [dbo].[tbl_VehicleSale] ADD CONSTRAINT [PK_tbl_VehicleSale] PRIMARY KEY  NONCLUSTERED 
	(
		[InventoryID]
	) WITH  FILLFACTOR = 90  ON [IDX] 
GO

ALTER TABLE [dbo].[tbl_VehicleSale] ADD CONSTRAINT [FK_tbl_VehicleSale_Inventory] FOREIGN KEY
(
		[InventoryID]
	) REFERENCES [Inventory] 
(
	[InventoryID]
)
GO
ALTER TABLE [dbo].[VehicleSaleCustomerDetail] ADD CONSTRAINT [FK_VehicleSaleCustomerDetail_tbl_VehicleSale] FOREIGN KEY 
	(
		[InventoryID]
	) REFERENCES [tbl_VehicleSale]
(
	[InventoryID]
)ON DELETE CASCADE  ON UPDATE CASCADE
GO


ALTER TABLE [dbo].[VehicleSaleCustomerDetail] ADD CONSTRAINT [FK_VehicleSaleCustomerDetail_lu_CustomerType] FOREIGN KEY 
	(
		[CustomerTypeCD]
	) REFERENCES [lu_CustomerType]
(
	[CustomerTypeCD]
)
GO

alter table dbo.tbl_VehicleSale
add  DealStatusCD tinyint CONSTRAINT DF_DealStatusCD DEFAULT 2 
GO

update tbl_VehicleSale
set DealStatusCD = 2
go

alter table dbo.tbl_VehicleSale
alter column DealStatusCD tinyint NOT NULL 
go

create table dbo.lu_DealStatus
(
DealStatusCD tinyint not null,
DealStatusDesc	varchar(15) not null
CONSTRAINT [PK_lu_DealStatus] PRIMARY KEY  NONCLUSTERED 
	(
		[DealStatusCD]
	) WITH  FILLFACTOR = 100  ON [IDX] 
) ON [DATA]
GO

insert into dbo.lu_DealStatus
(DealStatusCD, DealStatusDesc)
select
1,'Booked'
union
select
2,'Finalized'
go


CREATE TABLE dbo.[VehicleSaleCommissions] ( 
[InventoryID] [int] NOT NULL , 
[SalesCommission] [decimal](8, 2) NULL , 
[OtherCommission] [decimal](8, 2) NULL , 
[SalespersonLastName] [varchar] (50) NULL , 
[SalesPersonFirstName] [varchar] (50) NULL , 
[SalesPersonID] [int] NULL
CONSTRAINT [FK_VehicleSaleCommissions_tbl_VehicleSale] FOREIGN KEY 
( 
[InventoryID] 
) REFERENCES [tbl_VehicleSale] ( 
[InventoryID] 
) ON DELETE CASCADE ON UPDATE CASCADE 
) ON [DATA] 
GO 


CREATE TABLE [dbo].[lu_DealerUpgrade] 
(
	[DealerUpgradeCD] tinyint not null,
	[DealerUpgradeDESC] varchar(25) not null,
	CONSTRAINT [PK_lu_DealerUpgrade] PRIMARY KEY  NONCLUSTERED 
	(
		[DealerUpgradeCD]
	) WITH  FILLFACTOR = 90  ON [IDX] 
) ON [DATA]
GO

CREATE TABLE [dbo].[DealerUpgrade]
(
	[BusinessUnitID] int not null,
	[DealerUpgradeCD] tinyint not null,
	[StartDate] smalldatetime null,
	[EndDate] smalldatetime null,
	[Active] tinyint not null
	CONSTRAINT [PK_DealerUpgrade] PRIMARY KEY  NONCLUSTERED 
	(
		[BusinessUnitID],
		[DealerUpgradeCD]
	) WITH  FILLFACTOR = 90  ON [IDX],
	CONSTRAINT [FK_DealerUpgrade_lu_DealerUpgrade] FOREIGN KEY 
	(
		[DealerUpgradeCD]
	) REFERENCES [lu_DealerUpgrade] (
		[DealerUpgradeCD]
	)	
)
GO

INSERT INTO lu_DealerUpgrade VALUES ( 1, "Trade Analyzer" )
INSERT INTO lu_DealerUpgrade VALUES ( 2, "Aging Inventory Plan" )
INSERT INTO lu_DealerUpgrade VALUES ( 3, "Custom Inventory Analysis" )
INSERT INTO lu_DealerUpgrade VALUES ( 4, "Redistribution" )
GO

insert into DealerUpgrade 
select dealer.businessunitid, 1, getDate(), getDate(), 1 
from BusinessUnit dealer, BusinessUnitRelationship bur, BusinessUnit dealerGroup, DealerGroupPreference dgp
where dealer.BusinessUnitTypeid = 4 
and dealer.businessunitid = bur.businessunitid
and dealer.active = 1
and bur.parentid = dealerGroup.businessunitid
and dealerGroup.businessunittypeid = 3
and dealergroup.businessunitid = dgp.businessunitid
and dgp.programtype_cd = 2 

insert into DealerUpgrade 
select dealer.businessunitid, 2, getDate(), getDate(), 1 
from BusinessUnit dealer, BusinessUnitRelationship bur, BusinessUnit dealerGroup, DealerGroupPreference dgp
where dealer.BusinessUnitTypeid = 4 
and dealer.businessunitid = bur.businessunitid
and dealer.active = 1
and bur.parentid = dealerGroup.businessunitid
and dealerGroup.businessunittypeid = 3
and dealergroup.businessunitid = dgp.businessunitid
and dgp.programtype_cd = 2 

insert into DealerUpgrade 
select dealer.businessunitid, 3, getDate(), getDate(), 1 
from BusinessUnit dealer, BusinessUnitRelationship bur, BusinessUnit dealerGroup, DealerGroupPreference dgp
where dealer.BusinessUnitTypeid = 4 
and dealer.businessunitid = bur.businessunitid
and dealer.active = 1
and bur.parentid = dealerGroup.businessunitid
and dealerGroup.businessunittypeid = 3
and dealergroup.businessunitid = dgp.businessunitid
and dgp.programtype_cd = 2 

insert into DealerUpgrade 
select dealer.businessunitid, 4, getDate(), getDate(), 1 
from BusinessUnit dealer, BusinessUnitRelationship bur, BusinessUnit dealerGroup, DealerGroupPreference dgp
where dealer.BusinessUnitTypeid = 4 
and dealer.businessunitid = bur.businessunitid
and dealer.active = 1
and bur.parentid = dealerGroup.businessunitid
and dealerGroup.businessunittypeid = 3
and dealergroup.businessunitid = dgp.businessunitid
and dgp.programtype_cd = 2 
GO

ALTER TABLE [dbo].[DealerCIAPreferences] ADD [RunDayOfWeek] int

GO

UPDATE [dbo].[DealerCIAPreferences] set RunDayOfWeek=1

GO

ALTER TABLE [dbo].[DealerCIAPreferences] ALTER COLUMN [RunDayOfWeek] int NOT NULL

GO

ALTER TABLE [dbo].[CIAReport] ADD [Status] tinyint

GO

ALTER TABLE [dbo].[CIAReport] ADD [Date] datetime

GO

UPDATE [dbo].[CIAReport] SET Date = getDate() -- this will need to be set based on week and year
GO

UPDATE [dbo].[CIAReport] SET Status = 2 -- current
WHERE Active = 1
GO

UPDATE [dbo].[CIAReport] SET Status = 4 -- need to set based on date 
WHERE Active <> 1
GO

ALTER TABLE [dbo].[CIAReport] ALTER COLUMN [Status] tinyint NOT NULL

GO

ALTER TABLE [dbo].[CIAReport] ALTER COLUMN [Date] datetime NOT NULL

GO

ALTER TABLE [dbo].[CIAReport] DROP COLUMN [Active] 

GO

ALTER TABLE [dbo].[CIAReport] DROP COLUMN [WeekNumber] 

GO

ALTER TABLE [dbo].[CIAReport] DROP COLUMN [YearNumber] 

GO
ALTER TABLE [dbo].[CIAReportAverages] ADD [Status] tinyint

GO

ALTER TABLE [dbo].[CIAReportAverages] ADD [Date] datetime

GO

UPDATE [dbo].[CIAReportAverages] SET Date = getDate() -- this will need to be set based on week and year

GO

UPDATE [dbo].[CIAReportAverages] SET Status = 4 -- make all inactive for now
GO

ALTER TABLE [dbo].[CIAReportAverages] ALTER COLUMN [Status] tinyint NOT NULL

GO

ALTER TABLE [dbo].[CIAReportAverages] ALTER COLUMN [Date] datetime NOT NULL

GO

ALTER TABLE [dbo].[CIAReportAverages] DROP COLUMN [WeekNumber] 

GO

ALTER TABLE [dbo].[CIAReportAverages] DROP COLUMN [YearNumber] 

GO

ALTER TABLE [dbo].[DealerCIAPreferences] ADD [Modifiable] tinyint

GO

UPDATE DealerCIAPreferences SET Modifiable = 1
WHERE CIAWeekLastModified != datepart( ww, getDate() )
GO

UPDATE DealerCIAPreferences SET Modifiable = 0
WHERE CIAWeekLastModified = datepart( ww, getDate() )
GO

ALTER TABLE [dbo].[DealerCIAPreferences] ALTER COLUMN [Modifiable] tinyint NOT NULL
GO

ALTER TABLE [dbo].[DealerCIAPreferences] DROP COLUMN [CIAWeekLastModified] 

GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Audit_Sales_DMI]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.[Audit_Sales_DMI]
GO


CREATE TABLE dbo.[Audit_Sales_DMI] (
	[DEALER_ID] [varchar] (10) NULL ,
	[DEALER_STATUS] [varchar] (25) NULL ,
	[EXCEPTION_CODE] [varchar] (25) NULL ,
	[SALE_STATUS] varchar (1) NULL,
	[SALES_REF_NUM] [varchar] (16) NULL ,
	[DEAL_NUM_FI] [varchar] (10) NULL ,
	[DEAL_DT] [varchar] (25) NULL ,
	[VIN] [varchar] (50) NULL ,
	[STOCK_NUM] [varchar] (15) NULL ,
	[VEH_YEAR] [varchar] (25) NULL ,
	[MAKE] [varchar] (20) NULL ,
	[MODEL] [varchar] (25) NULL ,
	[MODEL_PKG] [varchar] (50) NULL ,
	[BODY_TYPE] [varchar] (50) NULL ,
	[DOOR_CT] [varchar] (25) NULL ,
	[VEH_TYPE] [varchar] (1) NULL ,
	[VEH_CLASS] [varchar] (50) NULL ,
	[BASE_COLOR] [varchar] (50) NULL ,
	[EXT_COLOR] [varchar] (50) NULL ,
	[INT_COLOR] [varchar] (50) NULL ,
	[INT_DESC] [varchar] (50) NULL ,
	[ENGINE_DESC] [varchar] (35) NULL ,
	[CYLINDER_CT] [varchar] (25) NULL ,
	[FUEL_CODE] [varchar] (2) NULL ,
	[TRANS_DESC] [varchar] (50) NULL ,
	[DRIVE_TRAIN_DESC] [varchar] (10) NULL ,
	[MILEAGE] [varchar] (50) NULL ,
	[CERTIFIED] [varchar] (1) NULL ,
	[VEH_SOURCE] [varchar] (1) NULL ,
	[VEH_SOURCE_DTL] [varchar] (50) NULL ,
	[ACQUISITION_PRICE] [varchar] (25) NULL ,
	[RECON_COST] [varchar] (25) NULL ,
	[PACK_AMOUNT] [varchar] (25) NULL ,
	[UNIT_COST] [varchar] (25) NULL ,
	[SALE_PRICE] [varchar] (25) NULL ,
	[FRONT_END_GROSS] [varchar] (25) NULL ,
	[BACKEND_GROSS] [varchar] (25) NULL ,
	[AFTERMARKET_GROSS] [varchar] (25) NULL ,
	[TOTAL_GROSS] [varchar] (25) NULL ,
	[SALESPERSON_ID] [varchar] (25) NULL ,
	[SALESPERSON_LASTNAME] [varchar] (50) NULL ,
	[SALESPERSON_FIRSTNAME] [varchar] (50) NULL ,
	[SALES_COMMISSION] [varchar] (25) NULL ,
	[OTHER_COMMISSIONS] [varchar] (25) NULL ,
	[VEH_PROMOTION] [varchar] (25) NULL ,
	[LEASE_FLAG] [varchar] (1) NULL ,
	[SALE_DESC] [varchar] (20) NULL ,
	[REF_DT] [varchar] (25) NULL ,
	[RECEIVED_DT] [varchar] (25) NULL ,
	[AGE_IN_DAYS] [varchar] (25) NULL ,
	[CUSTOMER_TYPE] [varchar] (1) NULL ,
	[CUSTOMER_ADDRESS1] [varchar] (50) NULL ,
	[CUSTOMER_ADDRESS2] [varchar] (50) NULL ,
	[CUSTOMER_CITY] [varchar] (25) NULL ,
	[CUSTOMER_STATE] [varchar] (2) NULL ,
	[CUSTOMER_ZIP] [varchar] (5) NULL ,
	[CUSTOMER_ZIP_EXT] [varchar] (4) NULL ,
	[FILLER1] [varchar] (1) NULL ,
	[FILLER2] [varchar] (1) NULL ,
	[FILLER3] [varchar] (1) NULL ,
	[FILLER4] [varchar] (1) NULL ,
	[FILLER5] [varchar] (1) NULL ,
	[FILLER6] [varchar] (1) NULL ,
	[FILLER7] [varchar] (1) NULL ,
	[FILLER8] [varchar] (1) NULL ,
	[FILLER9] [varchar] (1) NULL ,
	[FILLER10] [varchar] (1) NULL ,
	[FILLER11] [varchar] (1) NULL ,
	[FILLER12] [varchar] (1) NULL ,
	[FILLER13] [varchar] (1) NULL ,
	[FILLER14] [varchar] (1) NULL ,
	[FILLER15] [varchar] (1) NULL ,
	[FILLER16] [varchar] (1) NULL ,
	[FILLER17] [varchar] (1) NULL ,
	[FILLER18] [varchar] (1) NULL ,
	[FILLER19] [varchar] (1) NULL ,
	[FILLER20] [varchar] (1) NULL ,
	[SOURCE] [varchar] (35) NOT NULL ,
	[STATUS] [varchar] (1) NOT NULL ,
	[PROCESSEDDATE] [datetime] NOT NULL CONSTRAINT [DF_AUDIT_SALES_DMI_PROCESSEDDATE] DEFAULT (getdate())
) ON [DATA]
GO



