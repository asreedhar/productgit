if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_CIAClassTypeDetail]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_CIAClassTypeDetail]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_CIAClassTypeDetail_Old]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_CIAClassTypeDetail_Old]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_CIAClassTypePricePoint]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_CIAClassTypePricePoint]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_CIAPowerzonePricePoint]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_CIAPowerzonePricePoint]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_ColorPreference]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_ColorPreference]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_GroupingPreference]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_GroupingPreference]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_Plan]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_Plan]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_InventoryStocking]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_InventoryStocking]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_InventoryStockingModel]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_InventoryStockingModel]
GO 

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[lu_CIASectionType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[lu_CIASectionType]
GO 

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UnitCostPointRange]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[UnitCostPointRange]
GO


ALTER TABLE dbo.tbl_CIASummary
DROP COLUMN TargetUnits
GO

ALTER TABLE dbo.tbl_CIASummary
DROP COLUMN RateOfSale
GO

ALTER TABLE dbo.tbl_CIASummary
DROP COLUMN TargetDaysSupply
GO

ALTER TABLE dbo.tbl_CIASummary
DROP COLUMN Weeks
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_CIABodyTypeDetail]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_CIABodyTypeDetail]
GO
CREATE TABLE [dbo].[tbl_CIABodyTypeDetail] (
	[CIABodyTypeDetailId] [int] IDENTITY (1, 1) NOT NULL ,
	[DisplayBodyTypeId] [int] NOT NULL ,
	[CIASummaryId] [int] NOT NULL ,
	[DisplayOrder] [int] NULL ,
	[NonCoreTargetUnits] [int] NOT NULL ,
	[TotalTargetUnits] [int] NOT NULL ,
	CONSTRAINT [PK_tbl_CIABodyTypeDetail] PRIMARY KEY  NONCLUSTERED 
	(
		[CIABodyTypeDetailId]
	)  ON [IDX] ,
	CONSTRAINT [FK_tbl_CIABodyTypeDetail_tbl_CIASummary] FOREIGN KEY 
	(
		[CIASummaryId]
	) REFERENCES [tbl_CIASummary] (
		[CIASummaryId]
	),
	CONSTRAINT [FK_tbl_CIABodyTypeDetail_lu_DisplayBodyType] FOREIGN KEY 
	(
		[DisplayBodyTypeId]
	) REFERENCES [lu_DisplayBodyType] (
		[DisplayBodyTypeId]
	)
) ON [DATA]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[lu_CIACategory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[lu_CIACategory]
GO
CREATE TABLE [dbo].[lu_CIACategory] (
	[CIACategoryId] [int] IDENTITY (1, 1) NOT NULL ,
	[CIACategoryDescription] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	CONSTRAINT [PK_tbl_CIACategory] PRIMARY KEY  NONCLUSTERED 
	(
		[CIACategoryId]
	)  ON [IDX] 
) ON [DATA]
GO

INSERT INTO [dbo].[lu_CIACategory] VALUES ( 'PowerZone' ) 
INSERT INTO [dbo].[lu_CIACategory] VALUES ( 'Winners' ) 
INSERT INTO [dbo].[lu_CIACategory] VALUES ( 'GoodBets' )
INSERT INTO [dbo].[lu_CIACategory] VALUES ( 'MarketPerformers' ) 
INSERT INTO [dbo].[lu_CIACategory] VALUES ( 'Managers Choice' ) 

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_CIAInventoryStocking]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_CIAInventoryStocking]
GO
CREATE TABLE [dbo].[tbl_CIAInventoryStocking] (
	[CIAInventoryStockingId] [int] IDENTITY (1, 1) NOT NULL ,
	[CIACategoryId] [int] NOT NULL ,
	[CIABodyTypeDetailId] [int] NOT NULL ,
	[TargetUnits] [int] NOT NULL ,
	CONSTRAINT [PK_tbl_CIAInventoryStocking] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAInventoryStockingId]
	)  ON [IDX] ,
	CONSTRAINT [FK_tbl_CIAInventoryStocking_tbl_CIABodyTypeDetail] FOREIGN KEY 
	(
		[CIABodyTypeDetailId]
	) REFERENCES [tbl_CIABodyTypeDetail] (
		[CIABodyTypeDetailId]
	),
	CONSTRAINT [FK_tbl_CIAInventoryStocking_lu_CIACategory] FOREIGN KEY 
	(
		[CIACategoryId]
	) REFERENCES [lu_CIACategory] (
		[CIACategoryId]
	)
) ON [DATA]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_CIAGroupingItem]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_CIAGroupingItem]
GO
CREATE TABLE [dbo].[tbl_CIAGroupingItem] (
	[CIAGroupingItemId] [int] IDENTITY (1, 1) NOT NULL ,
	[CIAInventoryStockingId] [int] NOT NULL ,
	[GroupingDescriptionId] [int] NOT NULL ,
	[CIACategoryId] [int] NOT NULL ,
	[MarketShare] [decimal] (4,4) NULL, 
	[Valuation] [decimal] (10,2) NULL, 
	[MarketPenetration] [decimal] (10,4) NULL, 
	[Notes] [varchar] (255) NULL, 
	CONSTRAINT [PK_tbl_CIAGroupingItem] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAGroupingItemId]
	)  ON [IDX] ,
	CONSTRAINT [FK_tbl_CIAGroupingItem_tbl_CIAInventoryStocking] FOREIGN KEY 
	(
		[CIAInventoryStockingId]
	) REFERENCES [tbl_CIAInventoryStocking] (
		[CIAInventoryStockingId]
	),
	CONSTRAINT [FK_tbl_CIAGroupingItem_GroupingDescription] FOREIGN KEY 
	(
		[GroupingDescriptionId]
	) REFERENCES [tbl_GroupingDescription] (
		[GroupingDescriptionId]
	),
	CONSTRAINT [FK_tbl_CIAGroupingItem_CIACategory] FOREIGN KEY 
	(
		[CIACategoryId]
	) REFERENCES [lu_CIACategory] (
		[CIACategoryId]
	)
) ON [DATA]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_CIAUnitCostPointRange]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_CIAUnitCostPointRange]
GO
CREATE TABLE [dbo].[tbl_CIAUnitCostPointRange] (
	[CIAUnitCostPointRangeId] [int] IDENTITY (1, 1) NOT NULL ,
	[CIAUnitCostPointRangeIndex] [int] NOT NULL,
	[CIAGroupingItemId] [int] NOT NULL ,
	[RangeLevelTargetUnits] [int] NOT NULL ,
	[UpperRange] [int] NOT NULL ,	
	[LowerRange] [int] NOT NULL ,		
	CONSTRAINT [PK_tbl_CIAUnitCostPointRange] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAUnitCostPointRangeId]
	)  ON [IDX] ,
	CONSTRAINT [FK_tbl_CIAUnitCostPointRange_tbl_CIAGroupingItem] FOREIGN KEY 
	(
		[CIAGroupingItemId]
	) REFERENCES [tbl_CIAGroupingItem] (
		[CIAGroupingItemId]
	)
) ON [DATA]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tbl_TargetDaysSupply]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tbl_TargetDaysSupply]
GO

ALTER TABLE [dbo].[DealerPreference]
ADD CIATargetDaysSupply int NULL
GO

ALTER TABLE [dbo].[DealerPreference]
ADD CIAMarketPerformerInStockThreshold int NULL
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_CIAPreferences]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_CIAPreferences]
GO
CREATE TABLE [dbo].[tbl_CIAPreferences] (
 [BusinessUnitId] [int] NOT NULL ,
 [CoreThreshold] [int] NOT NULL CONSTRAINT [DF_tbl_CIAPreferences_CoreThreshold] DEFAULT (10),
 [GoodBetsDealThreshold] [float] NOT NULL CONSTRAINT [DF_tbl_CIAPreferences_GoodBetsDealThreshold] DEFAULT (0.50),
 [PowerZoneGroupingThreshold] [int] NOT NULL ,
 [GoodBetsGroupingThreshold] [int] NOT NULL CONSTRAINT [DF_tbl_CIAPreferences_GoodBetsGrouingThreshold] DEFAULT (5),
 [BucketAllocationMinimumThreshold] [int] NOT NULL ,
 [TargetDaysSupply] [int] NOT NULL DEFAULT (45) , 
 [MarketPerformersDisplayThreshold] [int] NOT NULL CONSTRAINT [DF_tbl_CIAPreferences_MarketPerformersDisplayThreshold] DEFAULT (3), 
 [MarketPerformersInStockThreshold] [int] NOT NULL CONSTRAINT [DF_tbl_CIAPreferences_MarketPerformersInStockThreshold] DEFAULT (3),  
 CONSTRAINT [PK_tbl_CIAPreferences] PRIMARY KEY  NONCLUSTERED 
 (
  [BusinessUnitId]
 ) WITH  FILLFACTOR = 90  ON [DATA] ,
 CONSTRAINT [FK_tbl_CIAPreferences_BusinessUnit] FOREIGN KEY 
 (
  [BusinessUnitId]
 ) REFERENCES [BusinessUnit] (
  [BusinessUnitID]
 ) ON DELETE CASCADE  ON UPDATE CASCADE 
) ON [DATA]
GO

declare @matrix table (core int, goodbetsdeal float, unitcostbucketcreationthreshold int, goodbetgrouping int, bucketallocationminimumthreshold int, targetdayssupply int, marketperformerdisplay int)

insert 
into    @matrix 
select  10, 0.50, 10, 5, 5, 45, 3

insert into tbl_CIAPreferences
select  BU.BusinessUnitID, M.core, M.goodbetsdeal, M.unitcostbucketcreationthreshold, M.goodbetgrouping, M.bucketallocationminimumthreshold, M.targetdayssupply, M.marketperformerdisplay , 3 
from    @matrix M
    cross join BusinessUnit BU
GO

CREATE TABLE [dbo].[lu_CIAPlanType] (
 [CIAPlanTypeId] [int] IDENTITY (1, 1) NOT NULL ,
 [Description] VARCHAR(45) NOT NULL,
  CONSTRAINT [PK_lu_CIAPlanType] PRIMARY KEY  NONCLUSTERED 
  (
	[CIAPlanTypeId]
  )  ON [IDX] 
) ON [DATA]
GO

SET IDENTITY_INSERT lu_CIAPlanType ON
INSERT INTO [dbo].[lu_CIAPlanType] ( CIAPlanTypeId, Description ) VALUES ( 1, 'Color' )
INSERT INTO [dbo].[lu_CIAPlanType] ( CIAPlanTypeId, Description ) VALUES ( 2, 'Year' )
INSERT INTO [dbo].[lu_CIAPlanType] ( CIAPlanTypeId, Description ) VALUES ( 3, 'Trim' )
SET IDENTITY_INSERT lu_CIAPlanType OFF
GO
  
CREATE TABLE [dbo].[tbl_CIAPlan] (
 [CIAPlanId] [int] IDENTITY (1, 1) NOT NULL ,
 [CIAGroupingItemId] [int] NOT NULL,
 [Avoid] [bit] NULL,
 [Prefer] [bit] NULL ,
 [CIAPlanTypeId] [int] NOT NULL,
 [Value] VARCHAR(45) NOT NULL ,
 CONSTRAINT [FK_tbl_CIAPlan_tbl_CIAGroupingItem] FOREIGN KEY 
 (
  [CIAGroupingItemId]
 ) REFERENCES [tbl_CIAGroupingItem] (
  [CIAGroupingItemId]
 ), 
 CONSTRAINT [FK_tbl_CIAPlan_lu_CIAPlanType] FOREIGN KEY 
 (
  [CIAPlanTypeId]
 ) REFERENCES [lu_CIAPlanType] (
  [CIAPlanTypeId]
 ) 
) ON [DATA]
GO

CREATE TABLE [dbo].[tbl_CIAUnitCostPointPlan] (
 [CIAUnitCostPointRangeId] [int] NOT NULL,
 [BuyAmount] [int] NULL,
 [Avoid] [bit] NULL,
 [MinValue] [int] NULL ,
 [MaxValue] [int] NULL,
 [UserSelected] [tinyint] NULL,
 CONSTRAINT [PK_tbl_CIAUnitCostPointPlan] PRIMARY KEY  NONCLUSTERED 
 (
   [CIAUnitCostPointRangeId]
 )  ON [IDX], 
 CONSTRAINT [FK_tbl_CIAUnitCostPointPlan_tbl_CIAUnitCostPointRange] FOREIGN KEY 
 (
  [CIAUnitCostPointRangeId]
 ) REFERENCES [tbl_CIAUnitCostPointRange] (
  [CIAUnitCostPointRangeId]
 ) 
) ON [DATA]
GO
