print '125 Alter Script'
go
alter table dbo.tbl_CIAPreferences
add GoodBetsDealThreshold float not null CONSTRAINT DF_tbl_CIAPreferences_GoodBetsDealThreshold DEFAULT (.50)
go

CREATE TABLE [dbo].[tbl_CIAClassTypePricePoint] (
	[CIAClassTypePricePointId] [int] IDENTITY (1,1) NOT NULL,
	[CIAClassTypeDetailId] [int] NOT NULL ,
	[orderNum] [int] NOT NULL ,
	[start] [int] NOT NULL ,
	[stop] [int] NULL,
	[understock] [float] NULL,
) ON [DATA]
GO

ALTER TABLE [dbo].[tbl_CIAClassTypePricePoint] WITH NOCHECK ADD 
	CONSTRAINT [PK_tbl_CIAClassTypePricePoint] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAClassTypePricePointId]
	)  ON [IDX] 
GO

UPDATE [dbo].[BusinessUnit] SET BusinessUnitCode = 'UAGCODE1' WHERE BusinessUnitId = 100149
GO

UPDATE [dbo].[BusinessUnit] SET BusinessUnitCode = 'FLCODE1' WHERE BusinessUnitId = 100150
GO

ALTER TABLE [dbo].[BusinessUnit] ADD 
CONSTRAINT [UQ_BusinessUnit_BusinessUnitCode] UNIQUE NONCLUSTERED 
( 
[BusinessUnitCode] 
) ON [DATA] 
GO 
declare @colName nvarchar(50), @sql nvarchar(4000)
DECLARE cols CURSOR
READ_ONLY
FOR select name from syscolumns where object_name(id) = 'Audit_Inventory_DMI' and xtype <> 167 and name not in
('SOURCE','STATUS','PROCESSEDDATE')
OPEN cols
FETCH NEXT FROM cols INTO @colName
WHILE (@@fetch_status <> -1)
BEGIN
IF (@@fetch_status <> -2)
	BEGIN
		set @sql = '	alter table audit_inventory_dmi
				alter column ' + @colName + ' varchar(50) null'
		exec (@sql)	
	END 
	FETCH NEXT FROM cols INTO @colName
END
CLOSE cols
DEALLOCATE cols
GO
alter table VehicleSaleCommissions
alter column SalesPersonID varchar(50) null
go
