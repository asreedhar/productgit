CREATE TABLE [dbo].[DecodedFirstLookSquishVINs] (
	[squish_vin] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[edstyleid] [int] NULL ,
	[year] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[make] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MakeModelGroupingID] [int] NULL ,
	[trim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[bodyType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[class] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[doors] [tinyint] NULL ,
	[fuel_type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[engine] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[drive_type_code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[trans] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[cylinder_qty] [tinyint] NULL 
) ON [DATA]
GO

alter table dbo.DecodedSquishVINs add dsvSourceID tinyint not null default 1 
go

