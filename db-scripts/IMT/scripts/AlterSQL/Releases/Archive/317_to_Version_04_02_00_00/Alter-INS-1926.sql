

ALTER TABLE [dbo].tbl_CIAClassTypePricePoint
DROP COLUMN percentage
go

ALTER TABLE [dbo].tbl_CIAClassTypePricePoint
ADD PercentageOfValuation DECIMAL(6,5) NULL
go

if exists (select * from dbo.sysobjects where id = object_id(N'[tbl_DealerValuation]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tbl_DealerValuation]
GO





CREATE TABLE [dbo].[tbl_DealerValuation] (
	[DealerValuationId] [int] IDENTITY (1, 1) NOT NULL ,
	[DealerId] [int] NOT NULL ,
	[DiscountRate] [decimal](6, 4) NULL ,
	[TimeHorizon] [decimal](8, 4) NULL ,
	[Date] [datetime] NULL ,
	CONSTRAINT [PK_tbl_DealerValuation] PRIMARY KEY  NONCLUSTERED 
	(
		[DealerValuationId]
	) WITH  FILLFACTOR = 90  ON [IDX] ,
	CONSTRAINT [FK_DealerValuation_BusinessUnit] FOREIGN KEY 
	(
		[DealerId]
	) REFERENCES [BusinessUnit] (
		[BusinessUnitID]
	)
) ON [DATA]
GO