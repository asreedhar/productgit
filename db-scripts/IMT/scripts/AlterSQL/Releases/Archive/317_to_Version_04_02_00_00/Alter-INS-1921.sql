print 'INS-1921 Alter Script'
go

ALTER TABLE dbo.DealerPreference
ADD RunDayOfWeek INT NULL
GO

ALTER TABLE [dbo].[tbl_CIAPreferences] DROP CONSTRAINT [DF_tbl_CIAPreferences_RunDayOfWeek]
GO

ALTER TABLE dbo.tbl_CIAPreferences
DROP COLUMN RunDayOfWeek
GO