print 'CIATimePeriod'
go

CREATE TABLE [dbo].[tbl_CIACompositeTimePeriod] (
    [CIACompositeTimePeriodId] [int] IDENTITY (1, 1) NOT NULL ,
    [PriorTimePeriodId] [int] NOT NULL ,
    [ForecastTimePeriodId] [int] NOT NULL ,
    [Description] [varchar] (30) NOT NULL , 
    CONSTRAINT [PK_tbl_CIACompositeTimePeriod] PRIMARY KEY  NONCLUSTERED 
    (
        [CIACompositeTimePeriodId]
    ) WITH  FILLFACTOR = 90  ON [IDX],
        CONSTRAINT [FK_tbl_CIACompositeTimePeriod_lu_CIATimePeriod_Prior] FOREIGN KEY 
    (
        [PriorTimePeriodId]
    ) REFERENCES [lu_CIATimePeriod] (
        [CIATimePeriodID]
    ),
        CONSTRAINT [FK_tbl_CIACompositeTimePeriod_BusinessUnit] FOREIGN KEY 
    (
        [ForecastTimePeriodId]
    ) REFERENCES [lu_CIATimePeriod] (
        [CIATimePeriodID]
    )
) ON [DATA]
GO

ALTER TABLE lu_CIATimePeriod
DROP COLUMN Description
GO

INSERT INTO lu_CIATimePeriod ( Days, Forecast) VALUES (91, 0 ) 
GO

INSERT INTO lu_CIATimePeriod ( Days, Forecast) VALUES (91, 1 ) 
GO

INSERT INTO lu_CIATimePeriod ( Days, Forecast) VALUES (182, 1 ) 
GO

INSERT INTO tbl_CIACompositeTimePeriod ( PriorTimePeriodId, ForecastTimePeriodId, Description) 
VALUES ( 6, 7, '13/13' )
GO 

INSERT INTO tbl_CIACompositeTimePeriod ( PriorTimePeriodId, ForecastTimePeriodId, Description) 
VALUES ( 3, 8, '26/26' )
GO 

INSERT INTO tbl_CIACompositeTimePeriod ( PriorTimePeriodId, ForecastTimePeriodId, Description) 
VALUES ( 3, 7, '26/13' )
GO 

INSERT INTO tbl_CIACompositeTimePeriod ( PriorTimePeriodId, ForecastTimePeriodId, Description) 
VALUES ( 6, 8, '13/26' )
GO 

DROP TABLE [dbo].[tbl_CIABasisPeriod]
GO


CREATE TABLE [dbo].[tbl_CIABasisPeriod] (
    [BusinessUnitId] [int] NOT NULL ,
    [CIACompositeTimePeriodId] [int] NOT NULL ,
    CONSTRAINT [PK_tbl_CIABasisPeriod] PRIMARY KEY  NONCLUSTERED 
    (
        [BusinessUnitId],
        [CIACompositeTimePeriodId]
    ) WITH  FILLFACTOR = 90  ON [IDX] ,
    CONSTRAINT [FK_tbl_CIABasisPeriod_BusinessUnit] FOREIGN KEY 
    (
        [BusinessUnitId]
    ) REFERENCES [BusinessUnit] (
        [BusinessUnitID]
    ),
    CONSTRAINT [FK_tbl_CIABasisCompositePeriod_CIACompositeTimePeriodId] FOREIGN KEY 
    (
        [CIACompositeTimePeriodId]
    ) REFERENCES [tbl_CIACompositeTimePeriod] (
        [CIACompositeTimePeriodId]
    )
) ON [DATA]
GO

-- This is done to populate the tbl_CIABasisPeriod with the default 13/13 basis period for all dealers - MH 10/13/2004
declare @matrix table (ciaCompositeTimePeriodId int)

insert 
into    @matrix 
select CIACompositeTimePeriodId from tbl_CIACompositeTimePeriod where [Description] = '13/13'

insert into tbl_CIABasisPeriod
select  BU.BusinessUnitID, M.ciaCompositeTimePeriodId  
from    @matrix M
    cross join BusinessUnit BU