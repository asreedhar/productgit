print '127 Alter Script'
go

ALTER TABLE [dbo].[tbl_CIASummary]
	ADD Weeks INT NULL
GO

UPDATE [dbo].[tbl_CIASummary]
	SET Weeks = 0
GO

ALTER TABLE [dbo].[tbl_CIASummary]
	ALTER COLUMN Weeks INT NOT NULL
GO

alter table dbo.tbl_VehicleSale
alter column FinanceInsuranceDealNumber varchar(16) null
go

ALTER TABLE [dbo].[tbl_CIAClassTypeDetail]
	ADD DaysSupply FLOAT NULL
GO

UPDATE [dbo].[tbl_CIAClassTypeDetail]
	SET DaysSupply = 0
GO

ALTER TABLE [dbo].[tbl_CIAClassTypeDetail]
	ALTER COLUMN DaysSupply FLOAT NOT NULL
GO

ALTER TABLE [dbo].[tbl_CIAClassTypeDetail]
	ALTER COLUMN Understock FLOAT NOT NULL
GO

ALTER TABLE [dbo].[tbl_CIAClassTypeDetail]
	ALTER COLUMN Buy FLOAT NOT NULL
GO


ALTER TABLE [dbo].[tbl_CIAClassTypeDetail]
	DROP COLUMN IsCore
GO

ALTER TABLE [dbo].[tbl_CIAGroupingItem]
	ADD orderNum tinyint
GO

update [dbo].[tbl_CIAGroupingItem]
	set orderNum = 0

GO

ALTER TABLE [dbo].[tbl_CIAGroupingItem]
	ALTER COLUMN orderNum tinyint not null
GO

CREATE TABLE [dbo].[tbl_CIAPowerZonePricePoint] (
	[CIAPowerZonePricePointId] [int] IDENTITY (1,1) NOT NULL ,
	[CIAPowerZoneItemId] [int] NOT NULL ,
	[Start] [int] NOT NULL ,
	[Stop] [int] NOT NULL ,
	[OrderNum] [int] NOT NULL ,
) ON [DATA]
GO

ALTER TABLE [dbo].[tbl_CIAPowerZonePricePoint] ADD 
	CONSTRAINT [FK_tbl_CIAPowerZonePricePoint_tbl_CIAGroupingItem] FOREIGN KEY 
	(
		[CIAPowerZoneItemId]
	) REFERENCES [dbo].[tbl_CIAGroupingItem] (
		[CIAGroupingItemId]
	)
GO


ALTER TABLE [dbo].[tbl_CIAPowerZonePricePoint] WITH NOCHECK ADD 
	CONSTRAINT [PK_tbl_CIAPowerZonePricePoint] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAPowerZonePricePointId]
	)  ON [IDX] 
GO

ALTER TABLE [dbo].[CIAPricePoint] DROP CONSTRAINT [FK_CIAPricePoint_GroupingDescription]

GO

ALTER TABLE [dbo].[CIAMakeModelSuppression] DROP CONSTRAINT [FK_CIAMakeModelSuppression_MakeModelGroupingID]

GO

ALTER TABLE [dbo].[CIAMakeModelMinMax] DROP CONSTRAINT [FK_CIAMakeModelMinMax_MakeModelGrouping]

GO

--------------------------------------------------------------------------------------------------------
--	Dropping old CIA tables -  "You drop the bomb on me...baby."

-- DROP TABLE CIAMakeModelMinMax
-- DROP TABLE CIAMakeModelSuppression
-- DROP TABLE CIAPricePoint
-- DROP TABLE CIAReportAverages
-- DROP TABLE CIAReportMakeModelAttributes
-- DROP TABLE CIAReportPerformance
-- DROP TABLE CIAReport
-- DROP TABLE DealerCIAPreferences

-----------------------------------------------------------------------------------------------------




