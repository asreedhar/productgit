/*delete from  ste
from QA..SourceTableExceptionRules ste
join QA..ExceptionRules er
on ste.ExceptionRuleID = er.ExceptionRuleID
where er.ColumnName in
(
'Disposition',
'veh_Source_DTL',
'Age_In_Days',
'Entry_DT',
'VEH_STATUS',
'VEH_STATUS_CODE'
)

delete from QA..ExceptionRules
where ColumnName in
(
'Disposition',
'veh_Source_DTL',
'Age_In_Days',
'Entry_DT',
'VEH_STATUS',
'VEH_STATUS_CODE'
)

delete from QA..TableLookUps
where ColumnName in 
(
'Disposition',
'veh_Source_DTL',
'Age_In_Days',
'Entry_DT',
'VEH_STATUS',
'VEH_STATUS_CODE'
)

delete from QA..StandardizationRules
where ColumnName in 
(
'Disposition',
'veh_Source_DTL',
'Age_In_Days',
'Entry_DT',
'VEH_STATUS',
'VEH_STATUS_CODE'
)
alter table DBASTAT..DMI_inv_raw
drop column Disposition

alter table DBASTAT..DMI_inv_raw
drop column veh_Source_DTL

alter table DBASTAT..DMI_inv_raw
drop column Age_In_Days

alter table DBASTAT..DMI_inv_raw
drop column Entry_DT

alter table DBASTAT..DMI_inv_raw
drop column VEH_STATUS

alter table DBASTAT..DMI_inv_raw
drop column VEH_STATUS_CODE


alter table DBASTAT..DMI_inv_staging_1
drop column Disposition

alter table DBASTAT..DMI_inv_staging_1
drop column veh_Source_DTL

alter table DBASTAT..DMI_inv_staging_1
drop column Age_In_Days

alter table DBASTAT..DMI_inv_staging_1
drop column Entry_DT

alter table DBASTAT..DMI_inv_staging_1
drop column VEH_STATUS

alter table DBASTAT..DMI_inv_staging_1
drop column VEH_STATUS_CODE



alter table DBASTAT..DMI_inv_staging_adds
drop column Disposition

alter table DBASTAT..DMI_inv_staging_adds
drop column veh_Source_DTL

alter table DBASTAT..DMI_inv_staging_adds
drop column Age_In_Days

alter table DBASTAT..DMI_inv_staging_adds
drop column Entry_DT

alter table DBASTAT..DMI_inv_staging_adds
drop column VEH_STATUS

alter table DBASTAT..DMI_inv_staging_adds
drop column VEH_STATUS_CODE



alter table DBASTAT..DMI_inv_staging_updates
drop column Disposition


alter table DBASTAT..DMI_inv_staging_updates
drop column veh_Source_DTL

alter table DBASTAT..DMI_inv_staging_updates
drop column Age_In_Days

alter table DBASTAT..DMI_inv_staging_updates
drop column Entry_DT

alter table DBASTAT..DMI_inv_staging_updates
drop column VEH_STATUS

alter table DBASTAT..DMI_inv_staging_updates
drop column VEH_STATUS_CODE




alter table DBASTAT..DMI_inv_staging_deletes
drop column Disposition

alter table DBASTAT..DMI_inv_staging_deletes
drop column veh_Source_DTL

alter table DBASTAT..DMI_inv_staging_deletes
drop column Age_In_Days

alter table DBASTAT..DMI_inv_staging_deletes
drop column Entry_DT

alter table DBASTAT..DMI_inv_staging_deletes
drop column VEH_STATUS

alter table DBASTAT..DMI_inv_staging_deletes
drop column VEH_STATUS_CODE

alter table DBASTAT..DMI_SALES_RAW
drop column veh_Source_DTL

alter table DBASTAT..DMI_SALES_RAW
drop column Age_In_Days

alter table DBASTAT..DMI_SALES_RAW
drop column VEH_STATUS

alter table DBASTAT..DMI_SALES_RAW
drop column VEH_STATUS_CODE

alter table DBASTAT..DMI_SALES_staging_1
drop column veh_Source_DTL

alter table DBASTAT..DMI_SALES_staging_1
drop column Age_In_Days



alter table DBASTAT..DMI_STAGING_INVENTORY
drop column Disposition

alter table DBASTAT..DMI_STAGING_INVENTORY
drop column Source

alter table DBASTAT..DMI_STAGING_INVENTORY
drop column SourceDetail

alter table DBASTAT..DMI_STAGING_INVENTORY
drop column AgeInDays

alter table DBASTAT..DMI_STAGING_INVENTORY
drop column DMSEntryDT

alter table DBASTAT..DMI_STAGING_INVENTORY
drop column DMSVehicleStatusCD

alter table DBASTAT..DMI_STAGING_INVENTORY
drop column DMIVehicleStatusCD
*/

alter table DMI_STAGING_INVENTORY
drop column Disposition

alter table DMI_STAGING_INVENTORY
drop column Source

alter table DMI_STAGING_INVENTORY
drop column SourceDetail

alter table DMI_STAGING_INVENTORY
drop column AgeInDays

alter table DMI_STAGING_INVENTORY
drop column DMSEntryDT

alter table DMI_STAGING_INVENTORY
drop column DMSVehicleStatusCD

alter table DMI_STAGING_INVENTORY
drop column DMIVehicleStatusCD
