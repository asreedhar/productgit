print 'INS-1936 Alter Script'
go

ALTER TABLE [dbo].DealerPreference
ADD UnitCostThresholdLower int null,
	UnitCostThresholdUpper int null
GO

UPDATE DealerPreference
SET UnitCostThresholdLower=4000, UnitCostThresholdUpper=1000000
GO