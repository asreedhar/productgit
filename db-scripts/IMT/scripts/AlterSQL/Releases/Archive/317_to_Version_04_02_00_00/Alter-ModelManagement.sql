if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Create_PricePointsForPricePointTable]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Create_PricePointsForPricePointTable]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ModelManagement]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ModelManagement]
GO