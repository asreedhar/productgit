if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MMGLight]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[MMGLight]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[luVehicleLight]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[lu_VehicleLight]
GO


create table dbo.lu_VehicleLight
(
InventoryVehicleLightID tinyint not null,
InventoryVehicleLightDESC varchar (10) not null,
CONSTRAINT [PK_lu_VehicleLight] PRIMARY KEY  NONCLUSTERED 
	(
		[InventoryVehicleLightID]
	) WITH  FILLFACTOR = 90  ON [IDX],
CONSTRAINT [UQ_lu_VehicleLight_InventoryVehicleLightDESC] UNIQUE
	(
		[InventoryVehicleLightDESC]
	)
) ON [DATA]


insert into lu_VehicleLight
(InventoryVehicleLightID, InventoryVehicleLightDESC)
select
0,'NOT SET'
union
select
1,'RED'
union
select
2,'YELLOW'
union
select
3,'GREEN'


CREATE TABLE dbo.MMGLight
	(
	MakeModelGroupingLightId int Identity(1,1) NOT NULL,
	MakeModelGroupingId int NOT NULL,
	BusinessUnitID int NOT NULL,
	InventoryVehicleLightID tinyint NOT NULL,
	CONSTRAINT PK_MMGLight PRIMARY KEY  NONCLUSTERED 
		(
		MakeModelGroupingLightId
		) 
	WITH  FILLFACTOR = 90  ON IDX,
	CONSTRAINT UQ_MMGLight UNIQUE NONCLUSTERED
		(
		MakeModelGroupingId,
		BusinessUnitID,
		InventoryVehicleLightID
		),
	CONSTRAINT FK_MMGLight_MakeModelGrouping FOREIGN KEY 
		(
		MakeModelGroupingId
		) 
		REFERENCES MakeModelGrouping 
		(
		MakeModelGroupingId
		),
	CONSTRAINT FK_MMGLight_BusinessUnit FOREIGN KEY
		(
		BusinessUnitID
		)
		REFERENCES BusinessUnit
		(
		BusinessUnitID
		),
	CONSTRAINT FK_MMGLight_lu_VehicleLight FOREIGN KEY
		(
		InventoryVehicleLightID
		)
		REFERENCES lu_VehicleLight
		(
		InventoryVehicleLightID
		)
	) ON DATA

