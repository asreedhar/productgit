print 'INS-1771 Alter Script'
go

ALTER TABLE dbo.AppraisalGuideBookOptions
ADD OptionValue INT NULL
GO

UPDATE dbo.AppraisalGuideBookOptions
SET OptionValue = 0
GO

ALTER TABLE dbo.AppraisalGuideBookOptions
ALTER COLUMN OptionValue INT NOT NULL
GO