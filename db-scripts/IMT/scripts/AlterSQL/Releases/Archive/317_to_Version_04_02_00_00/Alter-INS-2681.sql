if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_InventoryOverstocking]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_InventoryOverstocking]
GO 

CREATE TABLE [dbo].[tbl_InventoryOverstocking] (
	[InventoryOverstockingId] [int] IDENTITY (1, 1) NOT NULL ,
	[CIAUnitCostPointRangeId] [int] NULL ,
	[InventoryId] [int] NOT NULL ,
	[StatusId] [tinyint] NOT NULL,
	[OverstockingDate] [smalldatetime] NOT NULL,
	CONSTRAINT [PK_tbl_InventoryOverstocking] PRIMARY KEY  NONCLUSTERED 
	(
		[InventoryOverstockingId]
	)  ON [IDX] ,
	CONSTRAINT [FK_tbl_InventoryOverstocking__Inventory] FOREIGN KEY 
	(
		[InventoryId]
	) REFERENCES [Inventory] (
		[InventoryId]
	)
) ON [DATA]
GO