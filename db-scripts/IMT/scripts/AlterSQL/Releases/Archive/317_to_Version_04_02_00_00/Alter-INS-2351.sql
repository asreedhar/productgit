ALTER TABLE dbo.AppraisalGuideBookOptions
ADD Type tinyint NULL
GO

UPDATE AppraisalGuideBookOptions
SET Type = 1
GO

ALTER TABLE dbo.AppraisalGuideBookOptions
ALTER COLUMN Type tinyint NOT NULL
GO

CREATE TABLE [dbo].[lu_OptionType]
(
	[Id] tinyint NOT NULL PRIMARY KEY,
	[Description] VARCHAR(25) NOT NULL
)

INSERT INTO [dbo].[lu_OptionType] ( [Id], [Description] ) VALUES ( 1, 'Option' )
INSERT INTO [dbo].[lu_OptionType] ( [Id], [Description] ) VALUES ( 2, 'Engine' )
INSERT INTO [dbo].[lu_OptionType] ( [Id], [Description] ) VALUES ( 3, 'Transmission' )
INSERT INTO [dbo].[lu_OptionType] ( [Id], [Description] ) VALUES ( 4, 'Drive Train' )
GO

ALTER TABLE dbo.AppraisalGuideBookOptions
ADD
CONSTRAINT [FK_Type_lu_OptionType] FOREIGN KEY 
	(
		Type
	) 
		REFERENCES lu_OptionType
	(
		[Id]
	)
GO
