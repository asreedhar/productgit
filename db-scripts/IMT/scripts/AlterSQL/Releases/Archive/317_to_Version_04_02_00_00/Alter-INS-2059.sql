if exists (select * from dbo.sysobjects where id = object_id(N'[DMI_STAGING_INVENTORY]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [DMI_STAGING_INVENTORY]
GO

CREATE TABLE dbo.[DMI_STAGING_INVENTORY] (
	[StockNumber] [varchar] (15) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[VehicleID] [int] NOT NULL ,
	[InventoryActive] [tinyint] NOT NULL ,
	[InventoryReceivedDate] [smalldatetime] NOT NULL ,
	[DeleteDt] [smalldatetime] NULL ,
	[ModifiedDT] [smalldatetime] NULL ,
	[DMSReferenceDt] [smalldatetime] NULL ,
	[UnitCost] [decimal](8, 2) NULL ,
	[AcquisitionPrice] [decimal](8, 2) NULL ,
	[Pack] [decimal](8, 2) NULL ,
	[MileageReceived] [int] NULL ,
	[Disposition] [int] NULL ,
	[Source] [tinyint] NULL ,
	[SourceDetail] [varchar] (20) NULL ,
	[TradeOrPurchase] [tinyint] NOT NULL ,
	[ListPrice] [decimal](8, 2) NULL ,
	[InitialVehicleLight] [int] NULL ,
	[Level4Analysis] [varchar] (150) NULL ,
	[InventoryType] [tinyint] NULL ,
	[ReconditionCost] [decimal](8, 2) NULL ,
	[UsedSellingPrice] [decimal](8, 2) NULL ,
	[Certified] [int] NULL ,
	[AgeInDays] [int] NULL ,
	[DMSEntryDT] [smalldatetime] NULL ,
	[DMSVehicleStatusCD] [tinyint] NULL ,
	[VehicleLocation] [varchar] (50) NULL ,
	[DMIVehicleStatusCD] [tinyint] NOT NULL ,
	[CurrentVehicleLight] [tinyint] NULL ,
	[FLRecFollowed] [tinyint] NOT NULL ,
	[Audit_ID] [int] NOT NULL 
) ON [DATA]

GO

alter table dbo.tbl_Vehicle
add Audit_ID int not null CONSTRAINT [DF_tbl_Vehicle] DEFAULT (0)

alter table dbo.Inventory
add Audit_ID int not null CONSTRAINT [DF_tbl_Inventory] DEFAULT (0)

alter table dbo.tbl_VehicleSale
add Audit_ID int not null CONSTRAINT [DF_tbl_VehicleSale] DEFAULT (0)


alter table dbo.tbl_VehicleSaleReversals
add Audit_ID int null 
go
update tbl_VehicleSaleReversals
set Audit_ID = 0
where Audit_ID is null
go
alter table dbo.tbl_VehicleSaleReversals
alter column Audit_ID int not null
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[VehicleSaleReversals]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [VehicleSaleReversals]
GO