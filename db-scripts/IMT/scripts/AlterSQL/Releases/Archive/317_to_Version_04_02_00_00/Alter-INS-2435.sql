print 'INS-2435 Alter Script'
go

ALTER TABLE [dbo].[DealerPreference]
	ADD PopulateClassName VARCHAR(255)
GO

UPDATE DealerPreference
	SET PopulateClassName = 'com.firstlook.service.agingplan.LongoAgingPlanStrategy'
	WHERE BusinessUnitId IN ( 100215, 100216 )
GO