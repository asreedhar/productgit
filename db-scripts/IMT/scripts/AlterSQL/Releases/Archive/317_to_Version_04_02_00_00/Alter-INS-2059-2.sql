if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Audit_Exceptions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Audit_Exceptions]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Audit_Inventory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Audit_Inventory]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Audit_Sales]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Audit_Sales]
GO

CREATE TABLE [dbo].[Audit_Exceptions] (
	[Audit_ID] [int] NOT NULL ,
	[SourceTableID] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ExceptionRuleID] [int] NOT NULL ,
	[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_Audit_Exceptions_TimeStamp] DEFAULT (getdate())
) ON [DATA]
GO

CREATE TABLE [dbo].[Audit_Inventory] (
	[AUDIT_ID] [int] NOT NULL ,
	[STATUS] [tinyint] NOT NULL 
) ON [DATA]
GO

CREATE TABLE [dbo].[Audit_Sales] (
	[AUDIT_ID] [int] NOT NULL ,
	[STATUS] [tinyint] NOT NULL 
) ON [DATA]
GO
