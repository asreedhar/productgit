/******** Iteration 119 Alter Statements ***********/
print '119 Alter Script'
go

ALTER TABLE [dbo].[DealerPreference]
   ADD BookOut [tinyint] null
GO

UPDATE [dbo].[DealerPreference] SET Bookout = 1
GO

ALTER TABLE [dbo].[DealerPreference]
    ALTER COLUMN BookOut [tinyint] NOT NULL
GO
alter table dbo.Audit_Inventory_DMI
alter column mileage varchar(50) null
go
alter table dbo.Audit_Sales_DMI
alter column mileage varchar(50) null
go