print 'INS-1987 Alter Script'
go

ALTER TABLE [dbo].DealerPreference
ADD GuideBook2Id INT NULL
GO

ALTER TABLE [dbo].DealerPreference
ADD GuideBook2BookOutPreferenceId INT NULL
GO

ALTER TABLE [dbo].DealerPreference
ADD GuideBook2SecondBookOutPreferenceId INT NULL
GO
