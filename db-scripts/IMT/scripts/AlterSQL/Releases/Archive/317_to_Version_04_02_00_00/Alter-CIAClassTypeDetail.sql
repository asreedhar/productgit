if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_CIAClassTypeDetail]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.tbl_CIAClassTypeDetail
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_CIAClassTypeDetail_Old]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.tbl_CIAClassTypeDetail_Old
GO

CREATE TABLE dbo.tbl_CIAClassTypeDetail (
	[CIAClassTypeDetailId] [int] IDENTITY (1, 1) NOT NULL ,
	[IsSaved] [tinyint] NOT NULL ,
        [HasPowerZone] [tinyint] NOT NULL,
        [RateOfSale] [float] NOT NULL,
	[CIAClassTypeId] [smallint] NOT NULL ,
	[CIASummaryId] [int] NOT NULL,
        [Valuation] [float] NOT NULL DEFAULT 0,
	CONSTRAINT [PK_tbl_CIAClassTypeDetail] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAClassTypeDetailId]
	) WITH  FILLFACTOR = 90  ON [IDX] ,
	CONSTRAINT [FK_tbl_CIAClassTypeDetail_map_FLTypeClassToSegments] FOREIGN KEY 
	(
		[CIAClassTypeId]
	) REFERENCES [map_FLTypeClassToSegments] (
		[FLTypeClassID]
	),
        CONSTRAINT [FK_tbl_CIAClassTypeDetail_tbl_CIASummary] FOREIGN KEY 
	(
		[CIASummaryId]
	) REFERENCES [tbl_CIASummary] (
		[CIASummaryId]
	)
) ON [DATA]
GO

CREATE TABLE dbo.tbl_CIAClassTypeDetail_Old (
	[CIAClassTypeDetailId] [int] IDENTITY (1, 1) NOT NULL ,
	[CIASummaryId] [int] NOT NULL ,
	[RateOfSale] [float] NOT NULL ,
	[TargetDaysSupply] [int] NOT NULL ,
	[TargetUnits] [float] NOT NULL ,
	[ClassTypeOrder] [decimal](9, 2) NULL ,
	[CIAClassTypeId] [smallint] NOT NULL ,
	[UnitsSold] [int] NOT NULL CONSTRAINT [DF__tbl_CIACl__Units__7D2E8C24] DEFAULT (0),
	CONSTRAINT [PK_tbl_CIAClassTypeDetail_Old] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAClassTypeDetailId]
	) WITH  FILLFACTOR = 90  ON [IDX] ,
	CONSTRAINT [FK_tbl_CIAClassTypeDetail_Old_map_FLTypeClassToSegments] FOREIGN KEY 
	(
		[CIAClassTypeId]
	) REFERENCES [map_FLTypeClassToSegments] (
		[FLTypeClassID]
	),
	CONSTRAINT [FK_tbl_CIAClassTypeDetail_Old_tbl_CIASummary] FOREIGN KEY 
	(
		[CIASummaryId]
	) REFERENCES [tbl_CIASummary] (
		[CIASummaryId]
	)
) ON [DATA]
GO