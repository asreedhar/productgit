print '127 Alter Market Script'
go

USE MARKET
GO

exec sp_addrolemember N'appuser', N'firstlook'
GO

GRANT  SELECT  ON [dbo].[Market_Ref_Dealers]  TO [appuser]
GO

GRANT  SELECT  ON [dbo].[Market_Ref_Dealers_Source]  TO [appuser]
GO

GRANT  SELECT  ON [dbo].[Market_Summary]  TO [appuser]
GO

GRANT  INSERT  ON [dbo].[Market_Summary]  TO [appuser]
GO

