print 'INS-2225 Alter Script'
go

CREATE TABLE [dbo].[lu_DealerPackType] (
	[PackTypeCD] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[PackTypeDESC] [varchar] (50) NOT NULL ,
	CONSTRAINT [PK_lu_DealerPackType] PRIMARY KEY  CLUSTERED 
	(
		[PackTypeCD]
	)  ON [DATA] ,
	CONSTRAINT [IX_lu_DealerPackType] UNIQUE  NONCLUSTERED 
	(
		[PackTypeDESC]
	)  ON [DATA] 
) ON [DATA]
GO

INSERT into dbo.lu_DealerPackType (PackTypeDESC)
SELECT
'Flat/Fixed'
union
select
'Dynamic'
go

CREATE TABLE [dbo].[DealerPack] (
	[DealerPackID] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[PackTypeCD] [tinyint] NOT NULL ,
	[PackAmount] [decimal](8, 2) NULL ,
	[PackJavaClass] [varchar] (100) NULL ,
	CONSTRAINT [PK_DealerPack] PRIMARY KEY  CLUSTERED 
	(
		[DealerPackID]
	)  ON [DATA] ,
	CONSTRAINT [FK_DealerPack_BusinessUnit] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [BusinessUnit] (
		[BusinessUnitID]
	) ON DELETE CASCADE  ON UPDATE CASCADE ,
	CONSTRAINT [FK_DealerPack_lu_DealerPackType] FOREIGN KEY 
	(
		[PackTypeCD]
	) REFERENCES [lu_DealerPackType] (
		[PackTypeCD]
	) ON UPDATE CASCADE 
) ON [DATA]
GO
