
ALTER TABLE dbo.tbl_CIASummary
	DROP CONSTRAINT FK_tbl_CIASummary_BusinessUnit
GO
ALTER TABLE dbo.tbl_CIASummary
	DROP CONSTRAINT FK_tbl_CIASummary_lu_Status
GO
CREATE TABLE dbo.Tmp_tbl_CIASummary
	(
	CIASummaryId int NOT NULL IDENTITY (1, 1),
	BusinessUnitId int NOT NULL,
	StatusId tinyint NOT NULL,
	DT smalldatetime NOT NULL,
	TargetUnits float(53) NOT NULL,
	RateOfSale float(53) NOT NULL,
	TargetDaysSupply int NOT NULL,
	Weeks int NOT NULL
	)  ON DATA
GO
SET IDENTITY_INSERT dbo.Tmp_tbl_CIASummary ON
GO
IF EXISTS(SELECT * FROM dbo.tbl_CIASummary)
	 EXEC('INSERT INTO dbo.Tmp_tbl_CIASummary (CIASummaryId, BusinessUnitId, StatusId, DT, TargetDaysSupply, Weeks)
		SELECT CIASummaryId, BusinessUnitId, StatusId, DT, TargetDaysSupply, Weeks FROM dbo.tbl_CIASummary TABLOCKX')
GO
SET IDENTITY_INSERT dbo.Tmp_tbl_CIASummary OFF
GO
ALTER TABLE dbo.tbl_CIAClassTypeDetail
	DROP CONSTRAINT FK_tbl_CIAClassTypeDetail_tbl_CIASummary
GO
ALTER TABLE dbo.tbl_CIAGroupingItem
	DROP CONSTRAINT FK_tbl_CIAPOwerZoneItem_tbl_CIASummary
GO
DROP TABLE dbo.tbl_CIASummary
GO
EXECUTE sp_rename N'dbo.Tmp_tbl_CIASummary', N'tbl_CIASummary', 'OBJECT'
GO
ALTER TABLE dbo.tbl_CIASummary ADD CONSTRAINT
	PK_tbl_CIASummary PRIMARY KEY NONCLUSTERED 
	(
	CIASummaryId
	) ON IDX

GO
ALTER TABLE dbo.tbl_CIASummary WITH NOCHECK ADD CONSTRAINT
	FK_tbl_CIASummary_lu_Status FOREIGN KEY
	(
	StatusId
	) REFERENCES dbo.lu_Status
	(
	StatusId
	)
GO
ALTER TABLE dbo.tbl_CIASummary WITH NOCHECK ADD CONSTRAINT
	FK_tbl_CIASummary_BusinessUnit FOREIGN KEY
	(
	BusinessUnitId
	) REFERENCES dbo.BusinessUnit
	(
	BusinessUnitID
	)
GO
ALTER TABLE dbo.tbl_CIAGroupingItem WITH NOCHECK ADD CONSTRAINT
	FK_tbl_CIAPOwerZoneItem_tbl_CIASummary FOREIGN KEY
	(
	CIASummaryId
	) REFERENCES dbo.tbl_CIASummary
	(
	CIASummaryId
	)
GO
ALTER TABLE dbo.tbl_CIAClassTypeDetail WITH NOCHECK ADD CONSTRAINT
	FK_tbl_CIAClassTypeDetail_tbl_CIASummary FOREIGN KEY
	(
	CIASummaryId
	) REFERENCES dbo.tbl_CIASummary
	(
	CIASummaryId
	)
GO

