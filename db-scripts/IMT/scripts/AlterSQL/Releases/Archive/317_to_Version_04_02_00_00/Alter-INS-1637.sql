print 'INS-1637 Alter Script'
go

CREATE TABLE [dbo].[tbl_Plan] (
	[PlanId] [int] IDENTITY(1,1) NOT NULL ,
	[CIAGroupingItemId] [int] NOT NULL ,
	[Type] [char] NOT NULL ,
	[Buy] [varchar](20) NULL ,
	[PriceRange] [varchar](20) NULL ,
	[Notes] [varchar](50) NULL ,
	[Trim] [varchar](50) NULL
) ON [DATA]
GO

ALTER TABLE [dbo].[tbl_Plan] ADD 
	CONSTRAINT [FK_tbl_Plan_tbl_CIAGroupingItem] FOREIGN KEY 
	(
		[CIAGroupingItemId]
	) REFERENCES [dbo].[tbl_CIAGroupingItem] (
		[CIAGroupingItemId]
	)
GO

CREATE TABLE [dbo].[tbl_ColorPreference] (
	[ColorPreferenceId] [int] IDENTITY(1,1) NOT NULL ,
	[CIASummaryId] [int] NOT NULL ,
	[ClassTypeId] [smallint] NOT NULL ,
	[ColorKey] [varchar](50) NOT NULL ,
	[Prefer] [tinyint] NULL ,
	[Avoid] [tinyint] NULL
) ON [DATA]
GO

ALTER TABLE [dbo].[tbl_ColorPreference] ADD 
	CONSTRAINT [FK_tbl_ColorPreference_tbl_CIASummary] FOREIGN KEY 
	(
		[CIASummaryId]
	) REFERENCES [dbo].[tbl_CIASummary] (
		[CIASummaryId]
	)
GO

ALTER TABLE [dbo].[tbl_ColorPreference] ADD 
	CONSTRAINT [FK_tbl_ColorPreference_map_FLTypeClassToSegments] FOREIGN KEY 
	(
		[ClassTypeId]
	) REFERENCES [dbo].[map_FLTypeClassToSegments] (
		[FLTypeClassId]
	)
GO


CREATE TABLE [dbo].[tbl_GroupingPreference] (
	[GroupingPreferenceId] [int] IDENTITY(1,1) NOT NULL ,
	[CIAGroupingItemId] [int] NOT NULL ,
	[PreferenceKey] [varchar](50) NOT NULL ,
	[Type] [varchar](50) NOT NULL ,
	[Prefer] [tinyint] NULL ,
	[Avoid] [tinyint] NULL
) ON [DATA]
GO

ALTER TABLE [dbo].[tbl_GroupingPreference] ADD 
	CONSTRAINT [FK_tbl_GroupingPreference_tbl_CIAGroupingItem] FOREIGN KEY 
	(
		[CIAGroupingItemId]
	) REFERENCES [dbo].[tbl_CIAGroupingItem] (
		[CIAGroupingItemId]
	)
GO
