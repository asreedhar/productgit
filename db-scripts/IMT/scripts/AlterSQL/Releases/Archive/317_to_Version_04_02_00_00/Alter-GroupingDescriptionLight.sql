if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MMGLight]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[MMGLight]
GO

CREATE TABLE dbo.GDLight
	(
	GroupingDescriptionLightId int Identity(1,1) NOT NULL,
	GroupingDescriptionId int NOT NULL,
	BusinessUnitID int NOT NULL,
	InventoryVehicleLightID tinyint NOT NULL,
	CONSTRAINT PK_MMGLight PRIMARY KEY  NONCLUSTERED 
		(
		GroupingDescriptionLightId
		) 
	WITH  FILLFACTOR = 90  ON IDX,
	CONSTRAINT UQ_GDLight UNIQUE NONCLUSTERED
		(
		GroupingDescriptionId,
		BusinessUnitID,
		InventoryVehicleLightID
		),
	CONSTRAINT FK_GDLight_BusinessUnit FOREIGN KEY
		(
		BusinessUnitID
		)
		REFERENCES BusinessUnit
		(
		BusinessUnitID
		),
	CONSTRAINT FK_GDLight_lu_VehicleLight FOREIGN KEY
		(
		InventoryVehicleLightID
		)
		REFERENCES lu_VehicleLight
		(
		InventoryVehicleLightID
		)
	) ON DATA