if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UnitCostPointRange]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[UnitCostPointRange]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[UnitCostPoint]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[UnitCostPoint]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PowerZoneUnitCostPointData]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[PowerZoneUnitCostPointData]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventoryStockingModel]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[InventoryStockingModel]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PowerZoneInventoryStockingData]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[PowerZoneInventoryStockingData]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventoryStocking]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[InventoryStocking]
GO


create table dbo.InventoryStocking
(
InventoryStockingID int IDENTITY not null,
BusinessUnitID int not null,
DisplayBodyTypeID int not null,
CIASummaryId int not null,
TargetInventory int null,
CONSTRAINT [PK_InventoryStocking] PRIMARY KEY  NONCLUSTERED 
	(
		[InventoryStockingID]
	) WITH  FILLFACTOR = 90  ON [IDX],
CONSTRAINT [FK_CIASummaryId] FOREIGN KEY
	(
		[CIASummaryId]
	) REFERENCES [tbl_CIASummary] (
		[CIASummaryId]
	),
CONSTRAINT [FK_BusinessUnitId] FOREIGN KEY
	(
		[BusinessUnitID]
	) REFERENCES [BusinessUnit] (
		[BusinessUnitID]
	),
CONSTRAINT [FK_DisplayBodyTypeID] FOREIGN KEY
	(
		[DisplayBodyTypeID]
	) REFERENCES [lu_DisplayBodyType] (
		[DisplayBodyTypeID]
	)
) ON [DATA]
GO

CREATE TABLE dbo.InventoryStockingModel (
	[InventoryStockingModelID] [int] IDENTITY NOT NULL ,
	[InventoryStockingID] [int] NOT NULL ,
	[GroupingDescriptionID] [int] NOT NULL,
	CONSTRAINT [PK_InventoryStockingModel] PRIMARY KEY  NONCLUSTERED 
	(
		[InventoryStockingModelID]
	) WITH  FILLFACTOR = 90  ON [IDX] ,
	CONSTRAINT [FK_InventoryStockingModel_InventoryStockingID] FOREIGN KEY 
	(
		[InventoryStockingID]
	) REFERENCES [InventoryStocking] (
		[InventoryStockingID]
	),
	CONSTRAINT [FK_InventoryStockingModel_GroupingDescriptionID] FOREIGN KEY 
	(
		[GroupingDescriptionID]
	) REFERENCES [tbl_GroupingDescription] (
		[GroupingDescriptionID]
	)
) ON [DATA]
GO

create table dbo.UnitCostPointRange
(
UnitCostPointRangeID int IDENTITY not null,
InventoryStockingModelID int not null,
LowerRange int not null,
UpperRange int not null,
TargetUnits int null,
CONSTRAINT [PK_UnitCostPointRange] PRIMARY KEY  NONCLUSTERED 
	(
		[UnitCostPointRangeID]
	) WITH  FILLFACTOR = 90  ON [IDX],
CONSTRAINT [FK_UnitCostPointRange] FOREIGN KEY 
	(
		[InventoryStockingModelID]
	) REFERENCES [InventoryStockingModel] (
		[InventoryStockingModelID]
	)
) ON [DATA]

GO
