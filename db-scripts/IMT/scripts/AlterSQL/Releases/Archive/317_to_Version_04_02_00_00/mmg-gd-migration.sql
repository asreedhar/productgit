-- Save the old MakeModelGrouping and GroupingDescription tables, just in case

select	*
into	MakeModelGrouping317
from	MakeModelGrouping
go

select	*
into	GroupingDescription317
from	GroupingDescription
go

-- Migration script for grouping descrition

/*  allow NULL GroupingDescriptionIDs in the MakeModelGrouping table   */
Alter Table MakeModelGrouping
alter column GroupingDescriptionID  int NULL
GO

/*  If a make model does not need to be grouped set it GroupingDescriptionID to null   */
update  mmg
set     GroupingDescriptionID = NULL
from    MakeModelGrouping mmg
where   mmg.GroupingDescriptionID in
            (select mmg1.GroupingDescriptionID
            from MakeModelGrouping mmg1
            group by mmg1.GroupingDescriptionID
            having count(*) = 1)
GO

/*  do not check the constraint betweem marketshare and grouping description   */
ALTER TABLE MarketShare NOCHECK CONSTRAINT FK_MarketShare_GroupingDescriptionID
GO

/*  remove groupings that are no longer needed   */
delete 
from    GroupingDescription
where   GroupingDescriptionID not in (select distinct GroupingDescriptionID
                    from MakeModelGrouping where GroupingDescriptionID is not NULL)
go

/*  rename the groupingdescription table to allow a view with the old table name   */
EXEC sp_rename 'GroupingDescription', 'tbl_GroupingDescription'
GO

DBCC DBREINDEX ('tbl_GroupingDescription')
go

/*  rename the MakeModelGrouping table to allow a view with the old table name   */
EXEC sp_rename 'MakeModelGrouping', 'tbl_MakeModelGrouping'
GO

DBCC DBREINDEX ('tbl_MakeModelGrouping')
go

/*  do not check the constraint betweem vehicle and tbl_MakeModelGrouping description   */
ALTER TABLE tbl_Vehicle NOCHECK CONSTRAINT FK_tbl_Vehicle_MakeModelGrouping
GO

/*  do not check the constraint betweem Appraisal and tbl_MakeModelGrouping description   */
ALTER TABLE Appraisal NOCHECK CONSTRAINT FK_Appraisal_MakeModelGrouping
GO

/*  do not check the constraint on tbl_MakeModelGrouping to have a unigue make, model   */
ALTER TABLE tbl_MakeModelGrouping DROP CONSTRAINT UNK_MakeModelGrouping
GO

/*  add a column to store the edmunds body type  */
ALTER TABLE tbl_MakeModelGrouping
ADD EdmundsBodyType varchar(50) NULL
GO

/*  add a column to store the body type first look wants to display  */
ALTER TABLE [dbo].[tbl_MakeModelGrouping]
    ADD DisplayBodyTypeID INT DEFAULT(1) NOT NULL
GO

ALTER TABLE [dbo].[DecodedSquishVINs]
    ADD DisplayBodyTypeID INT DEFAULT(1) NOT NULL
GO

alter table dbo.DecodedFirstLookSquishVINs 
	add DisplayBodyTypeID int DEFAULT(1) NOT NULL
go
