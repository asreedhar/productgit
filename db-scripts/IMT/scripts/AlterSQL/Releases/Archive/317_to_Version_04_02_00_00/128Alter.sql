print '128 Alter Script'
go

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_dbVersion]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
create table dbo.tbl_dbVersion
(
dbVersionID int not null,
dbVersionTS datetime not null CONSTRAINT [DF_tbl_dbVersion_dbVersionTS] DEFAULT (getdate())
)
GO




if exists (select * from dbo.sysindexes where name = N'IX_lu_Edmunds_Style_Used' and id = object_id(N'[dbo].[lu_Edmunds_Style_Used]'))
drop index [dbo].[lu_Edmunds_Style_Used].[IX_lu_Edmunds_Style_Used]
GO

if exists (select * from dbo.sysindexes where name = N'IX_lu_Edmunds_Style_New' and id = object_id(N'[dbo].[lu_Edmunds_Style_New]'))
drop index [dbo].[lu_Edmunds_Style_New].[IX_lu_Edmunds_Style_New]
GO

ALTER TABLE lu_Edmunds_Style_New ALTER COLUMN ed_style_id int not null
GO
ALTER TABLE lu_Edmunds_Style_Used ALTER COLUMN ed_style_id int not null
GO
IF NOT EXISTS (SELECT 1 FROM sysindexes WHERE object_name(id) = 'lu_Edmunds_Style_New' and name = 'PK_lu_Edmunds_Style_New')

	ALTER TABLE [dbo].[lu_Edmunds_Style_New] WITH NOCHECK ADD 
		CONSTRAINT [PK_lu_Edmunds_Style_New] PRIMARY KEY  CLUSTERED 
		(
			[ed_style_id]
		)  ON [DATA] 
GO
UPDATE STATISTICS dbo.lu_Edmunds_Style_New
GO
IF NOT EXISTS (SELECT 1 FROM sysindexes WHERE object_name(id) = 'lu_Edmunds_Style_Used' and name = 'PK_lu_Edmunds_Style_Used')

	ALTER TABLE [dbo].[lu_Edmunds_Style_Used] WITH NOCHECK ADD 
		CONSTRAINT [PK_lu_Edmunds_Style_Used] PRIMARY KEY  CLUSTERED 
		(
			[ed_style_id]
		)  ON [DATA] 
GO
UPDATE STATISTICS dbo.lu_Edmunds_Style_Used  
GO