print 'INS-1521 Alter Script'
go


CREATE TABLE dbo.Tmp_map_BusinessUnitToMarketDealer
	(
	ID int Identity(1,1) NOT NULL,
	BusinessUnitID int NOT NULL,
	DealerNumber int NOT NULL
	)  ON DATA
GO
IF EXISTS(SELECT * FROM dbo.map_BusinessUnitToMarketDealer)
	 EXEC('INSERT INTO dbo.Tmp_map_BusinessUnitToMarketDealer (BusinessUnitID, DealerNumber)
		SELECT BusinessUnitID, DealerNumber FROM dbo.map_BusinessUnitToMarketDealer TABLOCKX')
GO
DROP TABLE dbo.map_BusinessUnitToMarketDealer
GO
EXECUTE sp_rename N'dbo.Tmp_map_BusinessUnitToMarketDealer', N'map_BusinessUnitToMarketDealer', 'OBJECT'
GO
ALTER TABLE dbo.map_BusinessUnitToMarketDealer ADD CONSTRAINT
	PK_map_BusinessUnitToMarketDealer PRIMARY KEY NONCLUSTERED 
	(
	ID
	) ON IDX

GO

