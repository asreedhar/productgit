if exists (select * from dbo.sysobjects where id = object_id(N'lu_DisplayBodyType') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [lu_DisplayBodyType]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[lu_DisplayBodyType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[lu_DisplayBodyType]
GO

CREATE TABLE [dbo].[lu_DisplayBodyType] (
    [DisplayBodyTypeID] [int] IDENTITY NOT NULL ,
    [DisplayBodyType] [varchar] (50) NOT NULL ,
    CONSTRAINT [PK_DisplayBodyType] PRIMARY KEY  NONCLUSTERED 
    (
        [DisplayBodyTypeID]
    ) WITH  FILLFACTOR = 80  ON [IDX] 
) ON [DATA]
GO

INSERT INTO lu_DisplayBodyType VALUES ('Unknown')
INSERT INTO lu_DisplayBodyType VALUES ('Truck')
INSERT INTO lu_DisplayBodyType VALUES ('Sedan')
INSERT INTO lu_DisplayBodyType VALUES ('Coupe')
INSERT INTO lu_DisplayBodyType VALUES ('Van')
INSERT INTO lu_DisplayBodyType VALUES ('SUV')
INSERT INTO lu_DisplayBodyType VALUES ('Convertible')
INSERT INTO lu_DisplayBodyType VALUES ('Wagon')

ALTER TABLE [dbo].[tbl_MakeModelGrouping] ADD 
    CONSTRAINT [FK_tbl_MakeModelGrouping_lu_DisplayBodyType] FOREIGN KEY 
    (
        [DisplayBodyTypeID]
    ) REFERENCES [dbo].[lu_DisplayBodyType] (
        [DisplayBodyTypeID]
    )
GO

ALTER TABLE [dbo].[DecodedSquishVINs] ADD 
    CONSTRAINT [FK_DecodedSquishVINs_lu_DisplayBodyType] FOREIGN KEY 
    (
        [DisplayBodyTypeID]
    ) REFERENCES [dbo].[lu_DisplayBodyType] (
        [DisplayBodyTypeID]
    )
GO
