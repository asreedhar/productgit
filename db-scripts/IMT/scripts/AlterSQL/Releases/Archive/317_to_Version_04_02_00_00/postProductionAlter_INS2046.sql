print 'postProductionAlter_INS2046'
GO

ALTER TABLE [dbo].[Appraisal] DROP CONSTRAINT [UNK_VehicleGuideBook_Vin_BU]
GO

ALTER TABLE [dbo].[Appraisal] ADD CONSTRAINT [UNK_VehicleGuideBook_Vin_BU_GuideBook] UNIQUE  NONCLUSTERED 
	(
		[Vin],
		[BusinessUnitID],
		[GuideBookID]
	) WITH  FILLFACTOR = 90  ON [IDX] 
GO

ALTER TABLE [dbo].[AppraisalGuideBookOptions]
ADD VehicleGuideBookID [int] NULL
GO

UPDATE AppraisalGuideBookOptions 
SET VehicleGuideBookId = a.VehicleGuideBookId
from AppraisalGuideBookOptions ag join Appraisal a
on a.Vin = ag.Vin
GO

ALTER TABLE [dbo].[AppraisalGuideBookOptions]
ALTER COLUMN VehicleGuideBookID [int] NOT NULL
GO

ALTER TABLE [dbo].[AppraisalGuideBookOptions] ADD CONSTRAINT [FK_AppraisalGuideBookOptions_VehicleGuideBookId] FOREIGN KEY 
	(
		[VehicleGuideBookID]
	) REFERENCES [Appraisal] (
		[VehicleGuideBookID]
	)
GO