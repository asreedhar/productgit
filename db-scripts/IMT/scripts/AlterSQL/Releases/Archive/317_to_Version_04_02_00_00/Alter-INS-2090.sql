
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DealerLightThresholds]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[DealerLightThresholds]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DealerLightGridValues]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[DealerLightGridValues]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DealerGridThresholds]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[DealerGridThresholds]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DealerGridValues]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[DealerGridValues]
GO


CREATE TABLE dbo.DealerGridThresholds
    (
    DealerGridThresholdsId int Identity(1,1) NOT NULL,
    BusinessUnitID int NOT NULL,
    firstValuationThreshold int NOT NULL,
    secondValuationThreshold int NOT NULL,
    thirdValuationThreshold int NOT NULL,
    fourthValuationThreshold int NOT NULL,
    firstDealThreshold int NOT NULL,
    secondDealThreshold int NOT NULL,
    thirdDealThreshold int NOT NULL,
    fourthDealThreshold int NOT NULL
    CONSTRAINT PK_DealerGridThresholds PRIMARY KEY  NONCLUSTERED 
        (
        DealerGridThresholdsId
        ) 
    WITH  FILLFACTOR = 90  ON IDX,
    CONSTRAINT UQ_DealerGridThresholdsBusinessUnit UNIQUE NONCLUSTERED
        (
        BusinessUnitID
        ),
    CONSTRAINT FK_DealerGridThresholds_BusinessUnit FOREIGN KEY
        (
        BusinessUnitID
        )
        REFERENCES BusinessUnit
        (
        BusinessUnitID
        )
    ) ON DATA
    
    GO
    
    CREATE TABLE dbo.DealerGridValues
        (
        DealerGridValuesId int Identity(1,1) NOT NULL,
        BusinessUnitID int NOT NULL,
        LightValue int NOT NULL,
        TypeValue int NOT NULL,
        indexKey int NOT NULL
        CONSTRAINT PK_DealerGridValues PRIMARY KEY  NONCLUSTERED 
            (
            DealerGridValuesId
            ) 
        WITH  FILLFACTOR = 90  ON IDX,
        CONSTRAINT FK_DealerGridValues_BusinessUnit FOREIGN KEY
            (
            BusinessUnitID
            )
            REFERENCES BusinessUnit
            (
            BusinessUnitID
            )
    ) ON DATA
    GO

declare @matrix table (idx int, lightValue int, typeValue int)

insert 
into    @matrix 
select  1,2,5
union   
select  2,2,5
union
select  3,2,5
union
select  4,2,5
union
select  5,2,5
union
select  6,1,6
union
select  7,2,5
union
select  8,3,4
union
select  9,3,4
union
select  10,3,3
union
select  11,1,6
union
select  12,2,5
union
select  13,3,3
union
select  14,3,3
union
select  15,3,3
union
select  16,1,6
union
select  17,2,5
union
select  18,3,2
union
select  19,3,2
union
select  20,3,2
union
select  21,1,6
union
select  22,3,1
union
select  23,3,1
union
select  24,3,1
union
select  25,3,1

insert into DealerGridValues
select  BU.BusinessUnitID, M.lightValue, M.typeValue, M.idx  
from    @matrix M
    cross join BusinessUnit BU
GO

declare @matrix table (firstValThresh int, secondValThres int, thirdValThres int, fourthValThres int, firstDThresh int, secondDThresh int, thirdDThresh int, fourthDThresh int)

insert 
into    @matrix 
select 0,500,1000,1500,2,4,7,12

insert into DealerGridThresholds
select  BU.BusinessUnitID, M.*  
from    @matrix M
    cross join BusinessUnit BU
GO