print 'INS-1932 Alter Script'
go

DROP TABLE [dbo].lu_CIATimePeriod
GO

CREATE TABLE [dbo].lu_CIATimePeriod 
(
	[CIATimePeriodId] [int] IDENTITY NOT NULL ,
	[Days] [int] NOT NULL ,
	[Description] [VARCHAR] (20) NOT NULL ,	
	[Forecast] [TINYINT] NOT NULL ,	
	CONSTRAINT [PK_lu_CIATimePeriod] PRIMARY KEY  NONCLUSTERED 
	(
		[CIATimePeriodId]
	) WITH  FILLFACTOR = 90  ON [IDX] 
) ON [DATA]
GO

INSERT INTO [dbo].lu_CIATimePeriod( Days, Description, Forecast ) VALUES ( 56, '8 Weeks', 0 )
GO
INSERT INTO [dbo].lu_CIATimePeriod( Days, Description, Forecast ) VALUES ( 84, '12 Weeks', 0 )
GO
INSERT INTO [dbo].lu_CIATimePeriod( Days, Description, Forecast ) VALUES ( 182, '26 Weeks', 0 )
GO
INSERT INTO [dbo].lu_CIATimePeriod( Days, Description, Forecast ) VALUES ( 364, '52 Weeks', 0 )
GO
INSERT INTO [dbo].lu_CIATimePeriod( Days, Description, Forecast ) VALUES ( 84, '12 Week Forecast', 1 )
GO

ALTER TABLE [dbo].[tbl_CIABasisPeriod] DROP CONSTRAINT [FK_tbl_CIABasisPeriod_BusinessUnit]
GO

DROP TABLE [dbo].tbl_CIABasisPeriod
GO

CREATE TABLE [dbo].tbl_CIABasisPeriod 
(
	[BusinessUnitId] [int] NOT NULL ,
	[CIATimePeriodId] [int] NOT NULL ,	
	[Weight] [INT] NOT NULL ,	
	CONSTRAINT [PK_tbl_CIABasisPeriod] PRIMARY KEY  NONCLUSTERED 
	(
		[BusinessUnitId],
		[CIATimePeriodId]		
	) WITH  FILLFACTOR = 90  ON [IDX], 
	CONSTRAINT [FK_tbl_CIABasisPeriod_BusinessUnit] FOREIGN KEY 
	(
		[BusinessUnitId]
	) REFERENCES [BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_tbl_CIABasisPeriod_CIATimePeriod] FOREIGN KEY 
	(
		[CIATimePeriodId]
	) REFERENCES [lu_CIATimePeriod] (
		[CIATimePeriodId]
	)		
) ON [DATA]
GO



