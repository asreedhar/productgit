print 'INS-1939 Alter Script'
GO	

DROP TABLE [dbo].tbl_CIABasisPeriod
GO

CREATE TABLE [dbo].tbl_CIABasisPeriod 
(
	[BusinessUnitId] [int] NOT NULL ,
	[CIATimePeriodId] [int] NOT NULL ,	
	[Weight] [INT] NOT NULL ,
	[ClassTypePeriod] [TINYINT] NOT NULL,	
	CONSTRAINT [PK_tbl_CIABasisPeriod] PRIMARY KEY  NONCLUSTERED 
	(
		[BusinessUnitId],
		[CIATimePeriodId],
		[ClassTypePeriod]		
	) WITH  FILLFACTOR = 90  ON [IDX], 
	CONSTRAINT [FK_tbl_CIABasisPeriod_BusinessUnit] FOREIGN KEY 
	(
		[BusinessUnitId]
	) REFERENCES [BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_tbl_CIABasisPeriod_CIATimePeriod] FOREIGN KEY 
	(
		[CIATimePeriodId]
	) REFERENCES [lu_CIATimePeriod] (
		[CIATimePeriodId]
	)		
) ON [DATA]
GO