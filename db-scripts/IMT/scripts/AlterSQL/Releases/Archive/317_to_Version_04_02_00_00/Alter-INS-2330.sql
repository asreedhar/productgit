alter table dbo.lu_GuideBook add GuideBookName varchar(20)
go

update dbo.lu_GuideBook
	set GuideBookName = 'BLACKBOOK'
	where guideBookId = 1
GO

update dbo.lu_GuideBook
	set GuideBookName = 'NADA'
	where guideBookId = 2
GO

update dbo.lu_GuideBook
	set GuideBookName = 'KELLEY'
	where guideBookId = 3
GO
