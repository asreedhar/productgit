print '126 Alter Script'
go

if exists (select * from dbo.sysobjects where id = object_id(N'[tbl_CIAGroupingItem]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tbl_CIAGroupingItem]
GO

ALTER TABLE [dbo].[tbl_CIAPreferences]
	ADD PowerzoneGroupingThreshold INT NULL
GO

UPDATE [dbo].[tbl_CIAPreferences]
	SET PowerzoneGroupingThreshold = 10
GO

ALTER TABLE [dbo].[tbl_CIAPreferences]
	ALTER COLUMN PowerzoneGroupingThreshold INT NOT NULL
GO

CREATE TABLE [dbo].[tbl_CIAPowerZoneItem] (
	[CIASummaryId] [int] NOT NULL ,
	[CIAClassTypeId] [int] NOT NULL ,
	[GroupingDescriptionId] [int] NOT NULL ,
) ON [DATA]
GO

ALTER TABLE [dbo].[tbl_CIAPowerZoneItem] ADD 
	CONSTRAINT [FK_tbl_CIAPOwerZoneItem_tbl_CIASummary] FOREIGN KEY 
	(
		[CIASummaryId]
	) REFERENCES [dbo].[tbl_CIASummary] (
		[CIASummaryId]
	)
GO

ALTER TABLE [dbo].[tbl_CIAPowerZoneItem] ADD 
	CONSTRAINT [FK_tbl_CIAPowerZoneItem_GroupingDescription] FOREIGN KEY 
	(
		[GroupingDescriptionId]
	) REFERENCES [dbo].[GroupingDescription] (
		[GroupingDescriptionId]
	)
GO

CREATE TABLE [dbo].[lu_CIASectionType]
(
	CIASectionTypeId INT NOT NULL,
	CIASectionTypeDescription VARCHAR(20) NOT NULL
)
GO

ALTER TABLE [dbo].[lu_CIASectionType] WITH NOCHECK ADD 
	CONSTRAINT [PK_lu_CIASectionType] PRIMARY KEY  NONCLUSTERED 
	(
		[CIASectionTypeId]
	)  ON [IDX] 
GO

INSERT INTO [dbo].[lu_CIASectionType] VALUES( 1, 'Power Zone' )
INSERT INTO [dbo].[lu_CIASectionType] VALUES( 2, 'Winners' )
INSERT INTO [dbo].[lu_CIASectionType] VALUES( 3, 'Good Bets' )
INSERT INTO [dbo].[lu_CIASectionType] VALUES( 4, 'Managers Choice' )
GO

sp_rename 'tbl_CIAPowerZoneItem', 'tbl_CIAGroupingItem'
GO

ALTER TABLE [dbo].[tbl_CIAGroupingItem]
	ADD CIASectionTypeId INT NULL
GO

ALTER TABLE [dbo].[tbl_CIAGroupingItem]
	ADD Valuation Decimal(10,2) NULL
GO


UPDATE [dbo].[tbl_CIAGroupingItem]
	SET CIASectionTypeId = 0
GO

ALTER TABLE [dbo].[tbl_CIAGroupingItem]
	ALTER COLUMN CIASectionTypeId INT NOT NULL
GO

CREATE TABLE [dbo].[map_FLTypeClassToSegments] (
	[FLTypeClassID] [smallint] IDENTITY (1, 1) NOT NULL ,
	[VehicleSegmentID] [tinyint] NOT NULL ,
	[VehicleSubSegmentID] [tinyint] NOT NULL 
) ON [DATA]
GO

ALTER TABLE [dbo].[map_FLTypeClassToSegments] ADD 
	CONSTRAINT [PK_map_FLTypeClassToSegments] PRIMARY KEY  CLUSTERED 
	(
		[FLTypeClassID]
	)  ON [DATA] 
GO

insert
into	map_FLTypeClassToSegments (VehicleSegmentID, VehicleSubSegmentID)
select 	vehiclesegmentid,VehicleSubSegmentID
from 	lu_FLTypeClass TC
	join (	select	vehiclesegmentid, VehicleSubSegmentID, vehiclesegmentdesc + '-' + VehicleSubSegmentDESC TypeClass
		from	lu_VehicleSegment S
			cross join lu_VehicleSubSegment
		) S on upper(TC.FLTypeClassDesc) = S.TypeClass
order
by	vehiclesegmentid, VehicleSubSegmentID
GO

create table dbo.tbl_VehicleSegmentOverrides
(	VehicleSegmentID		tinyint not null,
	VehicleSubSegmentID		tinyint not null,
	VehicleSegmentIDOverride	tinyint not null,
	VehicleSubSegmentIDOverride	tinyint not null
)
go
ALTER TABLE [dbo].[tbl_VehicleSegmentOverrides] ADD 
	CONSTRAINT [PK_tbl_VehicleSegmentOverrides] PRIMARY KEY  CLUSTERED 
	(
		[VehicleSegmentID],
		[VehicleSubSegmentID]
	)  ON [DATA] 
GO

insert
into	tbl_VehicleSegmentOverrides
select	6, 15, 6, 17	-- WAGON, SPORTY -> 6, 17 WAGON,STANDARD 
union	
select	6, 6, 6, 17	-- WAGON, SMALL	-> 6, 17 WAGON,STANDARD 
union
select	6, 4, 6, 17	-- WAGON, MEDIUM -> 6, 17 WAGON,STANDARD 
union
select	1, 2, 6, 17 	-- COUPE, FULLSIZE -> 1, 4  COUPE, FULLSIZE
union
select	2, 15, 2, 6	-- SEDAN, SPORTY -> 2, 6  SEDAN, SMALL
union
select	6, 2, 6, 17	-- WAGON, FULLSIZE -> 6, 17 WAGON,STANDARD 

GO

ALTER TABLE [dbo].[tbl_CIAClassTypeDetail] drop
	CONSTRAINT [FK_tbl_CIAClassTypeDetail_lu_FLTypeClass] 
GO
	
ALTER TABLE [dbo].[tbl_CIAClassTypeDetail] ADD 
	CONSTRAINT [FK_tbl_CIAClassTypeDetail_map_FLTypeClassToSegments] FOREIGN KEY 
	(
		[CIAClassTypeId]
	) REFERENCES [dbo].[map_FLTypeClassToSegments] (
		[FLTypeClassID]
	)
GO
drop table lu_FLTypeClass
GO




ALTER TABLE [dbo].[tbl_CIAGroupingItem]
	ADD CIAGroupingItemId INT IDENTITY(1,1) NOT NULL
GO

ALTER TABLE [dbo].[tbl_CIAGroupingItem] WITH NOCHECK ADD 
	CONSTRAINT [PK_tbl_CIAGroupingItem] PRIMARY KEY  NONCLUSTERED 
	(
		[CIAGroupingItemId]
	)  ON [IDX] 
GO



ALTER TABLE [dbo].[DealerPreference]
	ADD BookOutPreferenceSecondId INT NULL
GO
	
ALTER TABLE [dbo].[tbl_CIAPreferences]
	ADD GoodBetsGroupingThreshold INT NULL
GO

UPDATE 	tbl_CIAPreferences
SET 	GoodBetsGroupingThreshold = 5
GO

ALTER TABLE [dbo].[tbl_CIAPreferences] ADD 
	CONSTRAINT [DF_tbl_CIAPreferences_GoodBetsGrouingThreshold] DEFAULT (5) FOR [GoodBetsGroupingThreshold]
GO

ALTER TABLE [dbo].[tbl_CIAPreferences]
	ALTER COLUMN GoodBetsGroupingThreshold INT NOT NULL
GO
							

CREATE TABLE [dbo].[map_BusinessUnitToMarketDealer] (
	[BusinessUnitID] [int] NOT NULL ,
	[DealerNumber] [int] NOT NULL 
) ON [DATA]
GO

ALTER TABLE [dbo].[map_BusinessUnitToMarketDealer] ADD 
	CONSTRAINT [PK_map_BusinessUnitToMarketDealer] PRIMARY KEY  CLUSTERED 
	(
		[BusinessUnitID],
		[DealerNumber]
	)  ON [DATA] 
GO

insert into map_BusinessUnitToMarketDealer
select	100135,70354
union
select	100136,70357
union 
select	100138,72238
union 
select	100139,70356
union 
select	100141,70355
union 
select	100147,18078
union 
select	100148,13589
GO


alter table dbo.VehicleSaleCommissions
alter column salespersonid varchar(25) null
go

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
CREATE TABLE [dbo].[lu_InventoryVehicleLight] (
	[InventoryVehicleLightCD] [tinyint] NOT NULL ,
	[InventoryVehicleLightDesc] [varchar] (30) NOT NULL,
 	CONSTRAINT [PK_lu_InventoryVehicleLight] PRIMARY KEY  NONCLUSTERED 
	(
		[InventoryVehicleLightCD]
	) WITH  FILLFACTOR = 100  ON [IDX] 
) ON [DATA]
GO

insert into lu_InventoryVehicleLight
(InventoryVehicleLightCD, InventoryVehicleLightDesc)
select
0, 'NOT PROCESSED'
union
select
1,'RED'
union
select
2,'YELLOW'
union
select
3,'GREEN'



