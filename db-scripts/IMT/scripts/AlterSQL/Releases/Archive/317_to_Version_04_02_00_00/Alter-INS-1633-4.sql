
CREATE TABLE dbo.Tmp_tbl_CIAClassTypePricePoint
	(
	CIAClassTypePricePointId int NOT NULL IDENTITY (1, 1),
	CIAClassTypeDetailId int NOT NULL,
	orderNum int NOT NULL,
	start int NOT NULL,
	stop int NULL,
	percentage float(53) NOT NULL
	)  ON DATA
GO
SET IDENTITY_INSERT dbo.Tmp_tbl_CIAClassTypePricePoint ON
GO
IF EXISTS(SELECT * FROM dbo.tbl_CIAClassTypePricePoint)
	 EXEC('INSERT INTO dbo.Tmp_tbl_CIAClassTypePricePoint (CIAClassTypePricePointId, CIAClassTypeDetailId, orderNum, start, stop, percentage)
		SELECT CIAClassTypePricePointId, CIAClassTypeDetailId, orderNum, start, stop, understock FROM dbo.tbl_CIAClassTypePricePoint TABLOCKX')
GO
SET IDENTITY_INSERT dbo.Tmp_tbl_CIAClassTypePricePoint OFF
GO
DROP TABLE dbo.tbl_CIAClassTypePricePoint
GO
EXECUTE sp_rename N'dbo.Tmp_tbl_CIAClassTypePricePoint', N'tbl_CIAClassTypePricePoint', 'OBJECT'
GO
ALTER TABLE dbo.tbl_CIAClassTypePricePoint ADD CONSTRAINT
	PK_tbl_CIAClassTypePricePoint PRIMARY KEY NONCLUSTERED 
	(
	CIAClassTypePricePointId
	) ON IDX

GO

