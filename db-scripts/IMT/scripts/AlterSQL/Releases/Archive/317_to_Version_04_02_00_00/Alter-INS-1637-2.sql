ALTER TABLE dbo.tbl_CIAClassTypeDetail
	DROP CONSTRAINT FK_tbl_CIAClassTypeDetail_map_FLTypeClassToSegments
GO
ALTER TABLE dbo.tbl_CIAClassTypeDetail
	DROP CONSTRAINT FK_tbl_CIAClassTypeDetail_tbl_CIASummary
GO
CREATE TABLE dbo.Tmp_tbl_CIAClassTypeDetail
	(
	CIAClassTypeDetailId int NOT NULL IDENTITY (1, 1),
	CIASummaryId int NOT NULL,
	RateOfSale float(53) NOT NULL,
	TargetDaysSupply int NOT NULL,
	TargetUnits float(53) NOT NULL,
	ClassTypeOrder int NOT NULL,
	CIAClassTypeId smallint NOT NULL
	)  ON DATA
GO
SET IDENTITY_INSERT dbo.Tmp_tbl_CIAClassTypeDetail ON
GO
IF EXISTS(SELECT * FROM dbo.tbl_CIAClassTypeDetail)
	 EXEC('INSERT INTO dbo.Tmp_tbl_CIAClassTypeDetail (CIAClassTypeDetailId, CIASummaryId, RateOfSale, TargetDaysSupply, TargetUnits, CIAClassTypeId)
		SELECT CIAClassTypeDetailId, CIASummaryId, RateOfSale, TargetDaysSupply, TargetUnits, CIAClassTypeId FROM dbo.tbl_CIAClassTypeDetail TABLOCKX')
GO
SET IDENTITY_INSERT dbo.Tmp_tbl_CIAClassTypeDetail OFF
GO
DROP TABLE dbo.tbl_CIAClassTypeDetail
GO
EXECUTE sp_rename N'dbo.Tmp_tbl_CIAClassTypeDetail', N'tbl_CIAClassTypeDetail', 'OBJECT'
GO
ALTER TABLE dbo.tbl_CIAClassTypeDetail ADD CONSTRAINT
	PK_tbl_CIAClassTypeDetail PRIMARY KEY NONCLUSTERED 
	(
	CIAClassTypeDetailId
	) WITH FILLFACTOR = 90 ON IDX

GO
ALTER TABLE dbo.tbl_CIAClassTypeDetail WITH NOCHECK ADD CONSTRAINT
	FK_tbl_CIAClassTypeDetail_tbl_CIASummary FOREIGN KEY
	(
	CIASummaryId
	) REFERENCES dbo.tbl_CIASummary
	(
	CIASummaryId
	)
GO
ALTER TABLE dbo.tbl_CIAClassTypeDetail WITH NOCHECK ADD CONSTRAINT
	FK_tbl_CIAClassTypeDetail_map_FLTypeClassToSegments FOREIGN KEY
	(
	CIAClassTypeId
	) REFERENCES dbo.map_FLTypeClassToSegments
	(
	FLTypeClassID
	)
GO

