/******** Iteration 120 Alter Statements ***********/
print '120 Alter Script'
go
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO
ALTER TABLE DealerPreference
	ADD BookOutPreferenceId INT
GO

UPDATE DealerPreference
	SET BookOutPreferenceId = 2
	WHERE GuideBookId = 2
GO


UPDATE DealerPreference
	SET BookOutPreferenceId = 6
	WHERE GuideBookId = 1
GO

ALTER TABLE DealerPreference
	ALTER COLUMN BookOutPreferenceId INT NOT NULL
GO

ALTER TABLE DealerPreference ADD 
	CONSTRAINT [FK_DealerPreference_GuideBookId] FOREIGN KEY 
	(
		[BookOutPreferenceId]
	) REFERENCES [dbo].[GuideBook] (
		[GuideBookId]
	)
GO

ALTER TABLE DealerPreference 
	ADD RiskLevelYearInitialTimePeriod int null,
		RiskLevelYearSecondaryTimePeriod int null,
		RiskLevelYearRollOverMonth int null
GO

UPDATE DealerPreference
SET RiskLevelYearInitialTimePeriod = 5, RiskLevelYearSecondaryTimePeriod = 5, RiskLevelYearRollOverMonth = 9
GO

ALTER TABLE DealerPreference
	ALTER COLUMN RiskLevelYearInitialTimePeriod int not null
GO

ALTER TABLE DealerPreference
	ALTER COLUMN RiskLevelYearSecondaryTimePeriod int not null
GO

ALTER TABLE DealerPreference
	ALTER COLUMN RiskLevelYearRollOverMonth int not null
GO


if exists (select * from dbo.sysobjects where id = object_id(N'dbo.[VehicleSaleCommissions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.[VehicleSaleCommissions]
GO

if exists (select * from dbo.sysindexes where name = N'UQ_VehicleSale_InventoryID' and id = object_id(N'[dbo].[VehicleSale]'))
drop index [dbo].[VehicleSale].[UQ_VehicleSale_InventoryID]
GO

create unique index UQ_VehicleSale_InventoryID on
dbo.VehicleSale (InventoryID) WITH FILLFACTOR = 70 ON IDX
go



CREATE TABLE dbo.[VehicleSaleCommissions] (
	[InventoryID] [int] NOT NULL ,
	[SalesCommission] [decimal](8, 2) NULL ,
	[OtherCommission] [decimal](8, 2) NULL ,
	[SalespersonLastName] [varchar] (50) NULL ,
	[SalesPersonFirstName] [varchar] (50) NULL ,
	[SalesPersonID] [int] NULL ---,
	CONSTRAINT [FK_VehicleSaleCommissions_VehicleSale] FOREIGN KEY 
	(
		[InventoryID]
	) REFERENCES [VehicleSale] (
		[InventoryID]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
) ON [DATA]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.[DMI_STAGING_INVENTORY]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.[DMI_STAGING_INVENTORY]
GO

CREATE TABLE dbo.[DMI_STAGING_INVENTORY] (
	[StockNumber] [varchar] (15) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[VehicleID] [int] NOT NULL ,
	[InventoryActive] [tinyint] NOT NULL ,
	[InventoryReceivedDate] [smalldatetime] NOT NULL ,
	[DeleteDt] [smalldatetime] NULL ,
	[ModifiedDT] [smalldatetime] NULL ,
	[DMSReferenceDt] [smalldatetime] NULL ,
	[UnitCost] [decimal](8, 2) NULL ,
	[AcquisitionPrice] [decimal](8, 2) NULL ,
	[Pack] [decimal](8, 2) NULL ,
	[MileageReceived] [int] NULL ,
	[Disposition] [int] NULL ,
	[Source] [tinyint] NULL ,
	[SourceDetail] [varchar] (20) NULL ,
	[TradeOrPurchase] [tinyint] NOT NULL ,
	[ListPrice] [decimal](8, 2) NULL ,
	[InitialVehicleLight] [int] NULL ,
	[Level4Analysis] [varchar] (150) NULL ,
	[InventoryType] [tinyint] NULL ,
	[ReconditionCost] [decimal](8, 2) NULL ,
	[UsedSellingPrice] [decimal](8, 2) NULL ,
	[Certified] [int] NULL ,
	[AgeInDays] [int] NULL ,
	[DMSEntryDT] [smalldatetime] NULL ,
	[DMSVehicleStatusCD] [tinyint] NULL ,
	[VehicleLocation] [varchar] (50) NULL ,
	[DMIVehicleStatusCD] [tinyint] NOT NULL ,
	[CurrentVehicleLight] [tinyint] NULL ,
	[FLRecFollowed] [tinyint] NOT NULL 
) ON [DATA]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Audit_Bookouts_TradeAnalyzer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [Audit_Bookouts_TradeAnalyzer]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.[Audit_Bookouts_Inventory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.[Audit_Bookouts_Inventory]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.[lu_BookOutSource]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.[lu_BookOutSource]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.[lu_GuideBook]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.[lu_GuideBook]
GO

CREATE TABLE dbo.[lu_GuideBook] (
	[GuideBookId] [tinyint] NOT NULL,
	[GuideBookDescription] [varchar] (20) NOT NULL,
	CONSTRAINT [PK_lu_GuideBook] PRIMARY KEY  NONCLUSTERED 
	(
		[GuideBookId]
	)  ON [IDX]
) ON [DATA]
GO

Insert lu_GuideBook
(GuideBookId, GuideBookDescription)
select
1, 'BlackBook'
union
select
2, 'NADA'
union
select
3, 'KBB'
union
select
4, 'Galves'

go


CREATE TABLE dbo.[Audit_Bookouts_Inventory] (
	[BookOutReferenceID] [int] NOT NULL ,
	[BookOutSourceID] tinyint NOT NULL,
	[BookOutTimeStamp] [datetime] NOT NULL ,
	CONSTRAINT [FK_Audit_Bookouts_Inventory_lu_GuideBook] FOREIGN KEY 
	(
		[BookOutSourceID]
	) REFERENCES [lu_GuideBook] (
		[GuideBookID]
	),
	CONSTRAINT [FK_Audit_BookOuts_Inventory_Inventory] FOREIGN KEY
	(
		[BookOutReferenceID]
	)REFERENCES [Inventory] (
		INVENTORYID
	)
) ON [DATA]
GO


CREATE TABLE dbo.[Audit_Bookouts_TradeAnalyzer] (
	[BookOutReferenceID] [int] NOT NULL ,
	[BookOutSourceID] tinyint NOT NULL,
	[BookOutTimeStamp] [datetime] NOT NULL ,
	CONSTRAINT [FK_Audit_Bookouts_TradeAnalyzer_lu_GuideBook] FOREIGN KEY 
	(
		[BookOutSourceID]
	) REFERENCES [lu_GuideBook] (
		[GuideBookID]
	),
	CONSTRAINT [FK_Audit_BookOuts_TradeAnalyzer_Appraisal] FOREIGN KEY
	(
		[BookOutReferenceID]
	)REFERENCES [Appraisal] (
		VehicleGuideBookId
	)
) ON [DATA]
GO

ALTER TABLE [dbo].[Appraisal] ADD 
	CONSTRAINT [FK_Appraisal_MakeModelGrouping] FOREIGN KEY 
	(
		[MakeModelGroupingId]
	) REFERENCES [dbo].[MakeModelGrouping] (
		[MakeModelGroupingId]
	),
	CONSTRAINT [FK_Appraisal_BusinessUnit] FOREIGN KEY 
	(
		[BusinessUnitId]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitId]
	)
GO

CREATE TABLE [dbo].[DealerRisk] (
	[BusinessUnitId] [int] NOT NULL ,
	[RiskLevelNumberOfWeeks] [int] NOT NULL ,
	[RiskLevelDealsThreshold] [int] NOT NULL ,
	[RiskLevelNumberOfContributors] [int] NOT NULL ,
	[RedLightNoSaleThreshold] [int] NOT NULL ,
	[RedLightGrossProfitThreshold] [int] NOT NULL ,
	[GreenLightNoSaleThreshold] [int] NOT NULL ,
	[GreenLightGrossProfitThreshold] [decimal](8, 2) NOT NULL ,
	[GreenLightMarginThreshold] [decimal](8, 2) NOT NULL ,
	[GreenLightDaysPercentage] [int] NOT NULL ,
	[HighMileageYear] [int] NOT NULL ,
	[HighMileageOverall] [int] NOT NULL ,
	[RiskLevelYearInitialTimePeriod] [int] NOT NULL ,
	[RiskLevelYearSecondaryTimePeriod] [int] NOT NULL ,
	[RiskLevelYearRollOverMonth] [int] NOT NULL ,
	[RedLightTarget] [int] NOT NULL ,
	[YellowLightTarget] [int] NOT NULL ,
	[GreenLightTarget] [int] NOT NULL 
) ON [DATA]
GO

ALTER TABLE [dbo].[DealerRisk] ADD 
	CONSTRAINT [DF_DealerRisk_RiskLevelNumberOfWeeks] DEFAULT (26) FOR [RiskLevelNumberOfWeeks],
	CONSTRAINT [DF_DealerRisk_RiskLevelDealsThreshold] DEFAULT (3) FOR [RiskLevelDealsThreshold],
	CONSTRAINT [DF_DealerRisk_RiskLevelNumberOfContributors] DEFAULT (4) FOR [RiskLevelNumberOfContributors],
	CONSTRAINT [DF_DealerRisk_RedLightNoSaleThreshold] DEFAULT (60) FOR [RedLightNoSaleThreshold],
	CONSTRAINT [DF_DealerRisk_RedLightGrossProfitThreshold] DEFAULT (50) FOR [RedLightGrossProfitThreshold],
	CONSTRAINT [DF_DealerRisk_GreenLightNoSaleThreshold] DEFAULT (40) FOR [GreenLightNoSaleThreshold],
	CONSTRAINT [DF_DealerRisk_GreenLightGrossProfitThreshold] DEFAULT (75) FOR [GreenLightGrossProfitThreshold],
	CONSTRAINT [DF_DealerRisk_GreenLightMarginThreshold] DEFAULT (1.0) FOR [GreenLightMarginThreshold],
	CONSTRAINT [DF_DealerRisk_GreenLightDaysPercentage] DEFAULT (25) FOR [GreenLightDaysPercentage],
	CONSTRAINT [DF_DealerRisk_HighMileageYear] DEFAULT (70000) FOR [HighMileageYear],
	CONSTRAINT [DF_DealerRisk_HighMileageOverall] DEFAULT (70000) FOR [HighMileageOverall],
	CONSTRAINT [DF_DealerRisk_RiskLevelYearInitialTimePeriod] DEFAULT (6) FOR [RiskLevelYearInitialTimePeriod],
	CONSTRAINT [DF_DealerRisk_RiskLevelYearSecondaryTimePeriod] DEFAULT (5) FOR [RiskLevelYearSecondaryTimePeriod],
	CONSTRAINT [DF_DealerRisk_RiskLevelYearRollOverMonth] DEFAULT (9) FOR [RiskLevelYearRollOverMonth],
	CONSTRAINT [DF_DealerRisk_RedLightTarget] DEFAULT (10) FOR [RedLightTarget],
	CONSTRAINT [DF_DealerRisk_YellowLightTarget] DEFAULT (35) FOR [YellowLightTarget],
	CONSTRAINT [DF_DealerRisk_GreenLightTarget] DEFAULT (55) FOR [GreenLightTarget],
	CONSTRAINT [PK_DealerRisk] PRIMARY KEY  CLUSTERED 
	(
		[BusinessUnitId]
	)  ON [IDX] 
GO

ALTER TABLE [dbo].[DealerRisk] ADD 
	CONSTRAINT [FK_DealerRisk_DealerPreference] FOREIGN KEY 
	(
		[BusinessUnitId]
	) REFERENCES [dbo].[DealerPreference] (
		[BusinessUnitID]
	) ON DELETE CASCADE 
GO

CREATE TRIGGER [TR_NewDealerRisk] ON [dbo].[DealerPreference] 
FOR INSERT
AS
INSERT INTO [dbo].[DealerRisk] (BusinessUnitId) SELECT i.BusinessUnitId FROM inserted i
GO

update [dbo].[DealerPreference] set RedLightTarget = 10 where RedLightTarget is null
update [dbo].[DealerPreference] set YellowLightTarget = 35 where YellowLightTarget is null
update [dbo].[DealerPreference] set GreenLightTarget = 55 where GreenLightTarget is null

insert into [dbo].[DealerRisk] 
	(BusinessUnitId, 
 	RiskLevelYearInitialTimePeriod, RiskLevelYearSecondaryTimePeriod, 
 	RiskLevelYearRollOverMonth,
	RedLightTarget, YellowLightTarget, GreenLightTarget)
select 
	dp.BusinessUnitId, 
	dp.RiskLevelYearInitialTimePeriod, dp.RiskLevelYearSecondaryTimePeriod,
	dp.RiskLevelYearRollOverMonth, 
	dp.RedLightTarget, dp.YellowLightTarget, dp.GreenLightTarget
from [dbo].[DealerPreference] dp
GO

update [dbo].[DealerRisk]
set 	
	RiskLevelNumberOfWeeks = dgp.RiskLevelNumberOfWeeks, 
	RiskLevelDealsThreshold = dgp.RiskLevelDealsThreshold, 
	RiskLevelNumberOfContributors = dgp.RiskLevelNumberOfContributors, 
	RedLightNoSaleThreshold = dgp.NoSaleRedLightThreshold, 
	RedLightGrossProfitThreshold = dgp.RedLightGrossProfitThreshold, 
	GreenLightNoSaleThreshold = dgp.GreenLightNoSaleThreshold,
	GreenLightGrossProfitThreshold = dgp.GreenLightGrossProfitThreshold, 
	GreenLightMarginThreshold = dgp.GreenLightMarginThreshold, 
	GreenLightDaysPercentage = dgp.GreenLightDaysPercent,
	HighMileageYear = dgp.HighMileageYear, 
	HighMileageOverall = dgp.HighMileageOverall
from [dbo].[DealerRisk] dr, [dbo].[DealerGroupPreference] dgp 
where dr.BusinessUnitId in
	(select
		bur.businessunitid
	from
		businessunitrelationship bur, businessunit bu
	where
		bur.businessunitid  = bu.businessunitid
		and bur.parentid = dgp.businessUnitId)
GO		
		
alter table DealerPreference
drop column
	RiskLevelYearInitialTimePeriod, 
	RiskLevelYearSecondaryTimePeriod,
	RiskLevelYearRollOverMonth, 
	RedLightTarget, 
	YellowLightTarget, 
	GreenLightTarget
GO

alter table DealerGroupPreference
drop column 	
	RiskLevelNumberOfWeeks, 
	RiskLevelDealsThreshold, 
	RiskLevelNumberOfContributors, 
	NoSaleRedLightThreshold, 
	RedLightGrossProfitThreshold, 
	GreenLightNoSaleThreshold,
	GreenLightGrossProfitThreshold, 
	GreenLightMarginThreshold, 
	GreenLightDaysPercent,
	HighMileageYear, 
	HighMileageOverall
GO