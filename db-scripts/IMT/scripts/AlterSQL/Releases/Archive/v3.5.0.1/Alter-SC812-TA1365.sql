CREATE TABLE Manufacturer (ManufacturerID TINYINT, Name VARCHAR(50) NOT NULL, ShortName VARCHAR(20) NULL)

INSERT
INTO	Manufacturer
SELECT	0, 'UNKNOWN/UNCLASSIFIED', NULL
UNION
SELECT	1, 'General Motors','GM'

GO

ALTER TABLE Franchise ADD ManufacturerID TINYINT NOT NULL CONSTRAINT DF_Franchise_Manufacturer DEFAULT (0)
GO

UPDATE	Franchise
SET	ManufacturerID = 1
WHERE	FranchiseDescription IN ('BUICK','CADILLAC','CHEVROLET','GMC','HUMMER','PONTIAC','SAAB','SATURN')
GO

/*
INSERT
INTO	tbl_DealerATCAccessGroups (BusinessUnitID, AccessGroupId, AccessGroupPriceId, AccessGroupActorId, AccessGroupValue)
SELECT	BU.BusinessUnitID, AG.AccessGroupID, 1, 3, 1
FROM	BusinessUnit BU
	JOIN DealerFranchise DF ON BU.BusinessUnitID = DF.BusinessUnitID
	JOIN Franchise F ON DF.FranchiseID = F.FranchiseID 
	JOIN [ATC]..AccessGroup AG ON AG.DatasourceID = 30 AND AG.Public_Group = 0		-- IMPLICIT CROSS JOIN 
WHERE	F.ManufacturerID = 1
*/
