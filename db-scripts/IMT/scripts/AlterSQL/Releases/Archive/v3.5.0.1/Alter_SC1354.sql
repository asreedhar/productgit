if not exists (select * from syscolumns where id=object_id('dbo.DealerGroupPreference') and name='LithiaStore')
ALTER TABLE dbo.DealerGroupPreference
ADD LithiaStore [tinyint] NOT NULL CONSTRAINT [DF_DealerGroupPreference_LithiaStore] DEFAULT (0)
GO

update dbo.DealerGroupPreference set LithiaStore = 1 where businessUnitId IN (100150, 101240)
GO