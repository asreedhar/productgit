if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DealerAuctionPreference]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.DealerAuctionPreference
GO

/*

Create dealer auction preferences table.  Configure it for every dealer, defaulting
to 250 miles and 14 days.

*/

CREATE TABLE dbo.DealerAuctionPreference
 (
   BusinessUnitID        int       not null
    constraint PK_DealerAuctionPreference
     primary key clustered
    constraint FK_DealerAuctionPreference__BusinessUnit
     foreign key references BusinessUnit (BusinessUnitID)
  ,AuctionSearchEnabled  tinyint   not null
  ,MaxMilesAway          smallint  not null
  ,MaxDaysAhead          smallint  not null
 )
GO

INSERT DealerAuctionPreference (BusinessUnitID, AuctionSearchEnabled, MaxMilesAway, MaxDaysAhead)
 select BusinessUnitID, 1, 250, 14
  from BusinessUnit
   where BusinessUnitTypeID = 4  --  Dealers
GO


