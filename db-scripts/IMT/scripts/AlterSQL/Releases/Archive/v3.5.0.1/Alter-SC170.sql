print 'Alter-SC170'
if not exists (select * from dbo.syscolumns where id = object_id(N'AppraisalGuideBookOptions') and name = 'standardOption')
alter table appraisalguidebookoptions add standardOption tinyint not null default 0
go