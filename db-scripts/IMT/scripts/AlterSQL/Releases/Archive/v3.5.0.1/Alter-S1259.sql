
alter table Appraisals add DealTrackStockNumber varchar(15) null
go

alter table Appraisals add DealTrackSalesperson varchar(40) null
go

alter table Appraisals add DealTrackNewOrUsed tinyint not null default (0)
go

alter table Appraisals add
  constraint FK_Appraisals_lu_InventoryVehicleType foreign key(DealTrackNewOrUsed) references lu_InventoryVehicleType(InventoryVehicleTypeCD)
go
