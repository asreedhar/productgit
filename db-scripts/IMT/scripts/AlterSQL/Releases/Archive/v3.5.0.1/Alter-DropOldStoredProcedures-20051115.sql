/*

Procedure(s) to be dropped as of Nov 15, 2005
 - usp_SetDeltaProcessing (actually, revised and renamed to SetDeltaProcessing)

*/


IF objectproperty(object_id('usp_SetDeltaProcessing'), 'isProcedure') = 1
    DROP PROCEDURE usp_SetDeltaProcessing
GO
