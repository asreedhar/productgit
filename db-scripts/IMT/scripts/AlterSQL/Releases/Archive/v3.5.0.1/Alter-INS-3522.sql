CREATE TABLE [dbo].[CIAEngineFailures] (
	[BusinessUnitID] [int] NOT NULL ,
	[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_ExpertSystemFailures_TimeStamp] DEFAULT (getdate()),
	[Message] varchar(100) NULL
) ON [DATA]
GO


