
ALTER TABLE MarketplaceSubmissions ALTER COLUMN [URL] [varchar] (1000) NOT NULL
GO

DELETE
FROM	SubscriptionFrequencyDetail
WHERE	SubscriptionFrequencyDetailID = 0
GO

UPDATE	SubscriptionFrequencyDetail
SET	Description = 'On New Data w/ CIA'
WHERE	SubscriptionFrequencyDetailID = 2

GO
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('dbo.SubscriptionTypes') and name='DealerUpgradeCD')
	ALTER TABLE SubscriptionTypes ADD DealerUpgradeCD TINYINT NOT NULL DEFAULT (1)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('dbo.SubscriptionTypes') and name='UserSelectable')
	ALTER TABLE SubscriptionTypes ADD UserSelectable TINYINT NOT NULL DEFAULT (1)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('dbo.SubscriptionTypes') and name='DefaultSubscriptionFrequencyDetailID')
	ALTER TABLE SubscriptionTypes ADD DefaultSubscriptionFrequencyDetailID TINYINT NOT NULL DEFAULT (0)
GO

UPDATE	ST
SET	Description = U.Description, 
	Notes = U.Notes, 
	DealerUpgradeCD = U.DealerUpgradeCD, 
	UserSelectable = U.UserSelectable, 
	DefaultSubscriptionFrequencyDetailID = U.DefaultSubscriptionFrequencyDetailID
FROM	SubscriptionTypes ST
	JOIN (	SELECT	5 SubscriptionTypeID, 'Overstocking / Understocking Report' Description, 
			'Overstocking / Understocking Report gives an indication of potentially understocked and overstocked models, the years that are under/overstocked, the target inventory per model and year, and days supply by segment.' Notes, 
			3 DealerUpgradeCD, 1 UserSelectable, 5 DefaultSubscriptionFrequencyDetailID
		UNION
		SELECT	6, 'Policy Implementation Progress Report',
				'Policy Implementation Progress Report update provides an indication of system usage by store personnel.',
				1, 1, 6
		UNION
		SELECT	7, 'Redistribution Hot Sheets', '', 1, 0, 2
		) U ON ST.SubscriptionTypeID = U.SubscriptionTypeID
		
GO

UPDATE SubscriptionTypes

SET 	Notes='Buying alerts provide notification that new data has arrived in the Search and Acquisition Network, searches that data and provides numbers of new vehicles available that match those models that have been selected in the CIA.',
  	DealerUpgradeCD = 3, 
  	UserSelectable = 1, 
  	DefaultSubscriptionFrequencyDetailID = 2
  	
WHERE 	SubscriptionTypeID = 1
GO

UPDATE SubscriptionTypes
SET description='Weekly Trade-In Appraisal Update', 
  notes='The Weekly Trade-In Appraisal Update shows the number of appraisals run through the Trade Analyzer in the latest week, the percent of Trade-In inventory that was analyzed over the last 4 weeks, the appraisal closing rate over the last 4 and 8 weeks and the Average Immediate Wholesale Gross Profit over the last 4 and 8 weeks.',
  DealerUpgradeCD = 1, UserSelectable = 1, DefaultSubscriptionFrequencyDetailID = 5
WHERE SubscriptionTypeID = 2
GO

UPDATE SubscriptionTypes
SET notes='Storms on the Horizon gives an indication of the number of units in current inventory in each age bucket, the water associated with each bucket, potentially overstocked models, higher risk inventory aging in inventory and the number of Red Light vehicles purchased in the last 2 weeks.',
  DealerUpgradeCD = 2, UserSelectable = 1, DefaultSubscriptionFrequencyDetailID = 9
WHERE SubscriptionTypeID = 4
GO

UPDATE SubscriptionTypes
SET description='Aging Inventory Plan', 
  notes='The Aging Inventory Plan Update lists all of the cars in inventory and the retail or wholesale plans associated with each.',
  DealerUpgradeCD = 2, UserSelectable = 1, DefaultSubscriptionFrequencyDetailID = 9
WHERE SubscriptionTypeID = 3
GO

UPDATE Subscriptions
SET SubscriptionFrequencyDetailID = 2
WHERE SubscriptionFrequencyDetailID = 3
GO