if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MemberPreference]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[MemberPreference]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[map_MemberToInvStatusFilter]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[map_MemberToInvStatusFilter]
GO

CREATE TABLE dbo.map_MemberToInvStatusFilter
(
	MemberToInvStatusFilterId INT IDENTITY (1, 1) NOT NULL ,
	MemberID INT NOT NULL,
	InventoryStatusCD SMALLINT NOT NULL
	CONSTRAINT FK_MemberToInvStatusFilters_Member FOREIGN KEY 
	(
		MemberID
	) REFERENCES Member (
		MemberID
	),
	CONSTRAINT FK_MemberToInvStatusFilters_InventoryStatusCodes FOREIGN KEY
	(
		InventoryStatusCD
	) REFERENCES InventoryStatusCodes (
		InventoryStatusCD
	)
)

GO