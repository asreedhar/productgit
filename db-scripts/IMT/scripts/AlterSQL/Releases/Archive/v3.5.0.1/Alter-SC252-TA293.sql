
CREATE TABLE dbo.FLUSAN_EventType (
	FLUSAN_EventTypeCode 	TINYINT NOT NULL, 
	Description 		VARCHAR(100) NOT NULL,
	CONSTRAINT [PK_FLUSAN_EventTypes] PRIMARY KEY CLUSTERED 
	(
	[FLUSAN_EventTypeCode]
	)
)
GO

CREATE TABLE dbo.FLUSAN_EventLog (	
	FLUSAN_EventLogID	INT IDENTITY(1,1) NOT NULL,
	FLUSAN_EventTypeCode	TINYINT NOT NULL,
	BusinessUnitID 		INT NULL, 
	MemberID 		INT NULL, 
	ChannelID 		INT NULL, 
	VIN 			VARCHAR(17) NULL,
	URL 			VARCHAR(250) NULL,
	DateCreated 		DATETIME NOT NULL CONSTRAINT [DF_FLUSAN_EventLog_DateCreated] DEFAULT (getdate()), 
	CONSTRAINT [PK_FLUSAN_FLUSAN_EventLog] PRIMARY KEY CLUSTERED 
	(
	[FLUSAN_EventLogID]
	),
	CONSTRAINT [FK_FLUSAN_EventLog_FLUSAN_EventType] FOREIGN KEY 
	(
		[FLUSAN_EventTypeCode]
	) REFERENCES [FLUSAN_EventType] (
		[FLUSAN_EventTypeCode]
	),
	CONSTRAINT [FK_FLUSAN_EventLog_BusinessUnit] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_FLUSAN_EventLog_Member] FOREIGN KEY 
	(
		[MemberID]
	) REFERENCES [Member] (
		[MemberID]
	),
	CONSTRAINT [FK_FLUSAN_EventLog_PurchasingCenterChannels] FOREIGN KEY 
	(
		[ChannelID]
	) REFERENCES [PurchasingCenterChannels] (
		[ChannelID]
	)
)

GO

INSERT
INTO	FLUSAN_EventType
SELECT	1, 'FLUSAN vehicle view action'
GO