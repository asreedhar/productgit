if exists (select * from dbo.sysobjects where id = object_id(N'[tbl_CIABasisPeriod]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tbl_CIABasisPeriod]
GO

--recreating the columns
--This will break when applying alters on a db that's already have these columns. BF

if not exists(select * from dbo.syscolumns where name = 'CIAAllocationTimePeriodId' )
ALTER TABLE dbo.tbl_CIAPreferences
    ADD CIAAllocationTimePeriodId INT NOT NULL CONSTRAINT DF_CIAAllocationTimePeriodId DEFAULT (5)
GO

ALTER TABLE dbo.tbl_CIAPreferences
    ADD CONSTRAINT [FK_tbl_CIAPreferences_CIABucketAllocationTimePeriod] FOREIGN KEY 
    (
        [CIAAllocationTimePeriodId]
    ) REFERENCES [tbl_CIACompositeTimePeriod] (
        [CIACompositeTimePeriodId]
    )
GO

if not exists(select * from dbo.syscolumns where name = 'CIATargetUnitsTimePeriodId' )
ALTER TABLE dbo.tbl_CIAPreferences
    ADD CIATargetUnitsTimePeriodId INT NOT NULL CONSTRAINT DF_CIATargetUnitsTimePeriodId DEFAULT (1)
GO
ALTER TABLE dbo.tbl_CIAPreferences
    ADD CONSTRAINT [FK_tbl_CIAPreferences_CIATargetUnitsTimePeriod] FOREIGN KEY 
    (
        [CIATargetUnitsTimePeriodId]
    ) REFERENCES [tbl_CIACompositeTimePeriod] (
        [CIACompositeTimePeriodId]
    )
GO

if not exists(select * from dbo.syscolumns where name = 'GDLightProcessorTimePeriodId' )
ALTER TABLE dbo.tbl_CIAPreferences
    ADD GDLightProcessorTimePeriodId INT NOT NULL CONSTRAINT DF_GDLightProcessorTimePeriodId DEFAULT (1)
GO
ALTER TABLE dbo.tbl_CIAPreferences
    ADD CONSTRAINT [FK_tbl_CIAPreferences_CIAgdLightsProcessorTimePeriod] FOREIGN KEY 
    (
        [GDLightProcessorTimePeriodId]
    ) REFERENCES [tbl_CIACompositeTimePeriod] (
        [CIACompositeTimePeriodId]
    )
GO