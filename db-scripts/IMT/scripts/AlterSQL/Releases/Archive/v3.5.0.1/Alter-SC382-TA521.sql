
CREATE TABLE dbo.CIABodyTypeDetails (
	CIABodyTypeDetailID 	int IDENTITY (1, 1) NOT NULL ,
	DisplayBodyTypeID 	int NOT NULL ,
	CIASummaryID	 	int NOT NULL ,
	NonCoreTargetUnits 	int NOT NULL ,
	DisplaySortOrder 	int NULL,
	LegacyID		int NULL
) ON DATA
GO

CREATE TABLE dbo.CIASummaryStatus (
	CIASummaryStatusID 	tinyint IDENTITY (1, 1) NOT NULL ,
	Description 		varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON DATA
GO

CREATE TABLE dbo.CIASummary (
	CIASummaryID	 	int IDENTITY (1, 1) NOT NULL ,
	BusinessUnitID 		int NOT NULL ,
	CIASummaryStatusID 	tinyint NOT NULL ,
	DateCreated 		smalldatetime NOT NULL ,
	LegacyID		int NULL
) ON DATA
GO

CREATE TABLE dbo.CIACategories (
	CIACategoryID tinyint IDENTITY (1, 1) NOT NULL ,
	Description varchar (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON DATA
GO

CREATE TABLE dbo.CIAGroupingItems (
	CIAGroupingItemID 	int IDENTITY (1, 1) NOT NULL ,
	CIABodyTypeDetailID 	int NOT NULL ,
	GroupingDescriptionID 	int NOT NULL ,
	CIACategoryID 		tinyint NOT NULL ,
	MarketShare 		decimal(4, 4) NULL ,
	Valuation 		decimal(10, 2) NULL ,
	MarketPenetration 	decimal(10, 4) NULL ,
	Notes 			varchar (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	LegacyID		int NULL
) ON DATA
GO

CREATE TABLE dbo.CIAGroupingItemDetailTypes (
	CIAGroupingItemDetailTypeID tinyint IDENTITY (1, 1) NOT NULL ,
	Description varchar (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON DATA
GO

CREATE TABLE dbo.CIAGroupingItemDetailLevels (
	CIAGroupingItemDetailLevelID tinyint IDENTITY (1, 1) NOT NULL ,
	Description varchar (45) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON DATA
GO

CREATE TABLE dbo.CIAGroupingItemDetails (
	CIAGroupingItemDetailID 	int IDENTITY (1, 1) NOT NULL ,
	CIAGroupingItemID 		int NOT NULL ,
	CIAGroupingItemDetailLevelID 	tinyint NOT NULL ,
	CIAGroupingItemDetailTypeID 	tinyint NOT NULL ,
	Value 				varchar (45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	LowRange 			int NULL ,
	HighRange 			int NULL ,
	Units 				int NULL ,
	DateCreated 			smalldatetime NOT NULL ,
	
) ON DATA
GO

ALTER TABLE dbo.CIASummaryStatus WITH NOCHECK ADD 
	CONSTRAINT PK_CIASummaryStatus PRIMARY KEY  CLUSTERED (CIASummaryStatusID)  ON DATA 
GO

ALTER TABLE dbo.CIAGroupingItems WITH NOCHECK ADD 
	CONSTRAINT PK_CIAGroupingItems PRIMARY KEY  CLUSTERED (CIAGroupingItemID)  ON DATA 
GO

ALTER TABLE dbo.CIAGroupingItemDetailTypes WITH NOCHECK ADD 
	CONSTRAINT PK_CIAGroupingItemDetailTypes PRIMARY KEY  CLUSTERED (CIAGroupingItemDetailTypeID)  ON DATA 
GO

ALTER TABLE dbo.CIAGroupingItemDetails WITH NOCHECK ADD 
	CONSTRAINT PK_CIAGroupingItemDetails PRIMARY KEY  CLUSTERED (CIAGroupingItemDetailID)  ON DATA 
GO

ALTER TABLE dbo.CIABodyTypeDetails ADD 
	CONSTRAINT PK_CIABodyTypeDetail PRIMARY KEY  NONCLUSTERED (CIABodyTypeDetailID) WITH  FILLFACTOR = 80  ON IDX 
GO

ALTER TABLE dbo.CIASummary ADD 
	CONSTRAINT PK_CIASummary PRIMARY KEY  NONCLUSTERED (CIASummaryID) WITH  FILLFACTOR = 80  ON IDX 
GO

 CREATE  INDEX IX_CIASummary_BusinessUnitID ON dbo.CIASummary(BusinessUnitID) WITH  FILLFACTOR = 80 ON DATA
GO

ALTER TABLE dbo.CIACategories ADD 
	CONSTRAINT PK_CIACategory PRIMARY KEY  NONCLUSTERED (CIACategoryID) WITH  FILLFACTOR = 80  ON IDX 
GO

ALTER TABLE dbo.CIAGroupingItemDetailLevels ADD 
	CONSTRAINT PK_CIAGroupingItemDetailLevel PRIMARY KEY  NONCLUSTERED (CIAGroupingItemDetailLevelID) WITH  FILLFACTOR = 80  ON IDX 
GO

ALTER TABLE dbo.CIAGroupingItemDetails ADD 
	CONSTRAINT DF_CIAGroupingItemDetails_DateCreated DEFAULT (getdate()) FOR DateCreated,
	CONSTRAINT DF_CIAGroupingItemDetails_CIAGroupingItemDetailTypeID DEFAULT (0) FOR CIAGroupingItemDetailTypeID,
	CONSTRAINT DF_CIAGroupingItemDetails_Units DEFAULT (0) FOR Units
GO

ALTER TABLE dbo.CIABodyTypeDetails ADD 
	CONSTRAINT FK_CIABodyTypeDetail_CIASummary FOREIGN KEY (CIASummaryID) REFERENCES dbo.CIASummary (CIASummaryID),
	CONSTRAINT FK_CIABodyTypeDetail_DisplayBodyType FOREIGN KEY (DisplayBodyTypeID) REFERENCES dbo.lu_DisplayBodyType (DisplayBodyTypeID)
GO

ALTER TABLE dbo.CIASummary ADD 
	CONSTRAINT FK_CIASummary_CIASummaryStatus FOREIGN KEY (CIASummaryStatusID) REFERENCES dbo.CIASummaryStatus (CIASummaryStatusID),
	CONSTRAINT FK_CIASummary_BusinessUnit FOREIGN KEY (BusinessUnitID) REFERENCES dbo.BusinessUnit (BusinessUnitID)
GO

ALTER TABLE dbo.CIAGroupingItems ADD 
	CONSTRAINT FK_CIAGroupingItems_CIABodyTypeDetails FOREIGN KEY (CIABodyTypeDetailID) REFERENCES dbo.CIABodyTypeDetails (CIABodyTypeDetailID),
	CONSTRAINT FK_CIAGroupingItems_CIACategories FOREIGN KEY (CIACategoryID) REFERENCES dbo.CIACategories (CIACategoryID)
GO

ALTER TABLE dbo.CIAGroupingItemDetails ADD 
	CONSTRAINT FK_CIAGroupingItemDetails_CIAGroupingItemID FOREIGN KEY (CIAGroupingItemID) REFERENCES dbo.CIAGroupingItems (CIAGroupingItemID),
	CONSTRAINT FK_CIAGroupingItemDetails_CIAGroupingItemDetailTypes FOREIGN KEY (CIAGroupingItemDetailTypeID) REFERENCES dbo.CIAGroupingItemDetailTypes (CIAGroupingItemDetailTypeID),
	CONSTRAINT FK_CIAGroupingItemDetails_CIAGroupingItemDetailLevels FOREIGN KEY (CIAGroupingItemDetailLevelID) REFERENCES dbo.CIAGroupingItemDetailLevels (CIAGroupingItemDetailLevelID)
GO

-----------------------------------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_CIABuckets_CIABucketGroups]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[CIABuckets] DROP CONSTRAINT FK_CIABuckets_CIABucketGroups
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_CIABucketRules_CIABucketRuleTypes]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[CIABucketRules] DROP CONSTRAINT FK_CIABucketRules_CIABucketRuleTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_CIABucketRules_CIABuckets]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[CIABucketRules] DROP CONSTRAINT FK_CIABucketRules_CIABuckets
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIABucketGroups]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[CIABucketGroups]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIABucketRuleTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[CIABucketRuleTypes]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIABucketRules]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[CIABucketRules]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIABuckets]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[CIABuckets]
GO

CREATE TABLE [dbo].[CIABucketGroups] (
	[CIABucketGroupID] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

CREATE TABLE [dbo].[CIABucketRuleTypes] (
	[CIABucketRuleTypeID] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ColumnName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

CREATE TABLE [dbo].[CIABucketRules] (
	[CIABucketRuleID] [int] IDENTITY (1, 1) NOT NULL ,
	[CIABucketID] [int] NOT NULL ,
	[CIABucketRuleTypeID] [tinyint] NOT NULL ,
	[Value] [int] NOT NULL ,
	[Value2] [int] NULL 
) ON [DATA]
GO

CREATE TABLE [dbo].[CIABuckets] (
	[CIABucketID] [int] IDENTITY (1, 1) NOT NULL ,
	[CIABucketGroupID] [tinyint] NOT NULL ,
	[Rank] [tinyint] NOT NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [DATA]
GO

ALTER TABLE [dbo].[CIABucketGroups] WITH NOCHECK ADD 
	CONSTRAINT [PK_CIABucketGroups] PRIMARY KEY  CLUSTERED 
	(
		[CIABucketGroupID]
	)  ON [DATA] 
GO

ALTER TABLE [dbo].[CIABucketRuleTypes] WITH NOCHECK ADD 
	CONSTRAINT [PK_CIABucketRuleTypes] PRIMARY KEY  CLUSTERED 
	(
		[CIABucketRuleTypeID]
	)  ON [DATA] 
GO

ALTER TABLE [dbo].[CIABucketRules] WITH NOCHECK ADD 
	CONSTRAINT [PK_CIABucketRules] PRIMARY KEY  CLUSTERED 
	(
		[CIABucketRuleID]
	)  ON [DATA] 
GO

ALTER TABLE [dbo].[CIABuckets] WITH NOCHECK ADD 
	CONSTRAINT [PK_CIABuckets] PRIMARY KEY  CLUSTERED 
	(
		[CIABucketID]
	)  ON [DATA] 
GO

ALTER TABLE [dbo].[CIABucketRules] ADD 
	CONSTRAINT [FK_CIABucketRules_CIABucketRuleTypes] FOREIGN KEY 
	(
		[CIABucketRuleTypeID]
	) REFERENCES [dbo].[CIABucketRuleTypes] (
		[CIABucketRuleTypeID]
	),
	CONSTRAINT [FK_CIABucketRules_CIABuckets] FOREIGN KEY 
	(
		[CIABucketID]
	) REFERENCES [dbo].[CIABuckets] (
		[CIABucketID]
	)
GO

ALTER TABLE [dbo].[CIABuckets] ADD 
	CONSTRAINT [FK_CIABuckets_CIABucketGroups] FOREIGN KEY 
	(
		[CIABucketGroupID]
	) REFERENCES [dbo].[CIABucketGroups] (
		[CIABucketGroupID]
	)
GO

CREATE TABLE dbo.CIAInventoryOverstocking
	(
	CIAInventoryOverstockingID int IDENTITY (1, 1) NOT NULL,
	CIASummaryID int NOT NULL,
	InventoryID int NOT NULL
	)  ON DATA
GO
ALTER TABLE dbo.CIAInventoryOverstocking ADD CONSTRAINT
	PK_CIAInventoryOverstocking PRIMARY KEY CLUSTERED 
	(
	CIAInventoryOverstockingID
	) ON DATA

GO
ALTER TABLE dbo.CIAInventoryOverstocking ADD CONSTRAINT
	FK_CIAInventoryOverstocking_CIASummary FOREIGN KEY
	(
	CIASummaryID
	) REFERENCES dbo.CIASummary
	(
	CIASummaryID
	)
GO
ALTER TABLE dbo.CIAInventoryOverstocking ADD CONSTRAINT
	FK_CIAInventoryOverstocking_Inventory FOREIGN KEY
	(
	InventoryID
	) REFERENCES dbo.Inventory
	(
	InventoryID
	)
GO
