------------------------------------------------------------------------------------------------
--
--	Clean up deprecated objects
--
------------------------------------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM dbo.syscolumns WHERE id = object_id(N'[dbo].[PurchasingCenterChannels]') and OBJECTPROPERTY(id, N'IsUserTable') = 1 and name = 'SearchFunctionName' )
	ALTER TABLE PurchasingCenterChannels DROP COLUMN SearchFunctionName
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[dbo].[GetATCVehiclesUsingCIACriteria]') and xtype in (N'FN', N'IF', N'TF'))
	DROP FUNCTION GetATCVehiclesUsingCIACriteria
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[dbo].[GetCIACostPointSearchCriteria]') and xtype in (N'FN', N'IF', N'TF'))
	DROP FUNCTION GetCIACostPointSearchCriteria
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[dbo].[GetCIAPlanTypeSearchCriteria]') and xtype in (N'FN', N'IF', N'TF'))
	DROP FUNCTION GetCIAPlanTypeSearchCriteria
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[dbo].[usp_SeeBuyingPlans]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE usp_SeeBuyingPlans
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[dbo].[InventoryStockingModel]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE InventoryStockingModel
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[dbo].[InventoryStocking]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE InventoryStocking
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[dbo].[CIAPricePoint]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE CIAPricePoint
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[dbo].[CIAReportAverages]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE CIAReportAverages
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[dbo].[FK_CIAReport_BusinessUnitID]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
	ALTER TABLE CIAReport DROP CONSTRAINT FK_CIAReport_BusinessUnitID
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[dbo].[FK_CIAReportMakeModelAttributes_CIAReportID]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
	ALTER TABLE CIAReportMakeModelAttributes DROP CONSTRAINT FK_CIAReportMakeModelAttributes_CIAReportID
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[dbo].[CIAReport]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE CIAReport
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[dbo].[CIAReportMakeModelAttributes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE CIAReportMakeModelAttributes
GO

IF EXISTS (SELECT * FROM dbo.sysobjects where id = object_id(N'[dbo].[CIAReportPerformance]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE CIAReportPerformance
GO