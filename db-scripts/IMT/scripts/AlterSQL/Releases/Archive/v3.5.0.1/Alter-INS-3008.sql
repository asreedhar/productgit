insert into lu_ciatimeperiod (days,forecast)
values (0,1)
GO

INSERT INTO 
	dbo.tbl_CIACompositeTimePeriod 
	(
		[priortimeperiodid], 
		[forecasttimeperiodid], 
		[description]
	)
(
	SELECT 	ctp_p.CIATimePeriodId priorTimePeriod,
		ctp_f.CIATimePeriodId forecastTimePeriod,
		'26/0'
	FROM 	dbo.lu_CIATimePeriod ctp_p,
		dbo.lu_CIATimePeriod ctp_f
	WHERE 	ctp_p.days = 26*7
	and	ctp_p.forecast = 0
	and 	ctp_f.days = 0
	and	ctp_f.forecast = 1
)
GO