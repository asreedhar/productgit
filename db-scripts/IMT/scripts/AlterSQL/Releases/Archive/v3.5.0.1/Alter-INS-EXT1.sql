CREATE TABLE [dbo].[CredentialType] (
	[CredentialTypeID] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[Name] [varchar] (50) NOT NULL,
	CONSTRAINT [PK_CredentialType] PRIMARY KEY NONCLUSTERED
	(
		[CredentialTypeID]
	) WITH FILLFACTOR = 80 ON [IDX]
) ON [DATA]
GO

CREATE TABLE [dbo].[MemberCredential] (
	[MemberID] [int] NOT NULL,
	[CredentialTypeID] [tinyint] NOT NULL,
	[Username] [varchar] (250) NULL,
	[Password] [varchar] (250) NULL,
	CONSTRAINT [PK_MemberCredential] PRIMARY KEY NONCLUSTERED
	(
		[MemberID],
		[CredentialTypeID]
	) WITH FILLFACTOR = 80 ON [IDX],
	CONSTRAINT [FK_MemberCredential_MemberID] FOREIGN KEY 
	(
		[MemberID]
	) REFERENCES [Member] (
		[MemberID]
	) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT [FK_MemberCredential_CredentialTypeID] FOREIGN KEY 
	(
		[CredentialTypeID]
	) REFERENCES [CredentialType] (
		[CredentialTypeID]
	) ON DELETE CASCADE ON UPDATE CASCADE
) ON [DATA]
GO


Insert into [dbo].[CredentialType] ([Name]) values ('CARFAX');
GO