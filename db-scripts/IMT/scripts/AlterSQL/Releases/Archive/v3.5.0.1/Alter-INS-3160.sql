UPDATE dbo.DealerPreference
SET RunDayOfWeek = 1
WHERE RunDayOfWeek is NULL
GO

alter table DealerPreference 
	alter column RunDayOfWeek INT NOT NULL
GO