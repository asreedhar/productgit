ALTER TABLE [dbo].[tbl_Vehicle] ADD
	[VehicleSourceID] [int] NOT NULL CONSTRAINT [DF_tbl_Vehicle_VehicleSourceID] DEFAULT (1)
GO	

if exists (select * from dbo.sysobjects where id = object_id(N'[VehicleSources]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[VehicleSources]
GO

CREATE TABLE [dbo].[VehicleSources] (
	[VehicleSourceID] [int] IDENTITY (1, 1) NOT NULL ,
	[Description] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	CONSTRAINT [PK_VehicleSources] PRIMARY KEY  CLUSTERED 
	(
		[VehicleSourceID]
	)  ON [DATA] 
) ON [DATA]	
GO

SET IDENTITY_INSERT VehicleSources ON

INSERT
INTO	VehicleSources (VehicleSourceID, Description)
SELECT	1, 'DMS'
UNION
SELECT	2, 'Appraisal'

SET IDENTITY_INSERT VehicleSources OFF
GO

ALTER TABLE [dbo].[tbl_Vehicle] ADD 
	CONSTRAINT [FK_tbl_Vehicle_VehicleSources] FOREIGN KEY 
	(
		[VehicleSourceID]
	) REFERENCES [dbo].[VehicleSources] (
		[VehicleSourceID]
	)
GO
