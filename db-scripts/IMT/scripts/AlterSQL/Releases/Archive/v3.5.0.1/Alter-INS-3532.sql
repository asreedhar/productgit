CREATE TABLE [dbo].[JobTitle] (
	[id] [int] NOT NULL ,
	[title] [varchar] (64) NOT NULL 
) ON [DATA]
GO

insert into [dbo].[JobTitle] values (1, 'Buyer');
insert into [dbo].[JobTitle] values (2, 'Controller');
insert into [dbo].[JobTitle] values (3, 'Data Entry');
insert into [dbo].[JobTitle] values (4, 'Dealer Principal');
insert into [dbo].[JobTitle] values (5, 'DMS Manager');
insert into [dbo].[JobTitle] values (6, 'First Look Admin');
insert into [dbo].[JobTitle] values (7, 'General Manager');
insert into [dbo].[JobTitle] values (8, 'General Sales Manager');
insert into [dbo].[JobTitle] values (9, 'Office Manager');
insert into [dbo].[JobTitle] values (10, 'Owner');
insert into [dbo].[JobTitle] values (11, 'Service Manager');
insert into [dbo].[JobTitle] values (12, 'Tag and Title');
insert into [dbo].[JobTitle] values (13, 'Used Car Manager');
insert into [dbo].[JobTitle] values (14, 'Wholesaler');
GO