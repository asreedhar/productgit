if not exists (select * from dbo.syscolumns where id = object_id(N'DealerPreference') and name = 'DisplayUnitCostToDealerGroup')
ALTER TABLE [dbo].DealerPreference
ADD DisplayUnitCostToDealerGroup tinyint NULL
GO

Update dbo.DealerPreference set DisplayUnitCostToDealerGroup = 1
GO

ALTER TABLE [dbo].DealerPreference
ALTER COLUMN DisplayUnitCostToDealerGroup tinyint NOT NULL
GO