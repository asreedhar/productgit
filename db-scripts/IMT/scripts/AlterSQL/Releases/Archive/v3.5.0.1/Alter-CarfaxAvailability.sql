CREATE TABLE dbo.Tmp_CarfaxAvailability
	(
	UserName varchar(250) NOT NULL,
	Vin varchar(250) NOT NULL,
	ReportType char(3) NOT NULL,
	Expires smalldatetime NOT NULL
	)  ON [PRIMARY]
GO
IF EXISTS(SELECT * FROM dbo.CarfaxAvailability)
	 EXEC('INSERT INTO dbo.Tmp_CarfaxAvailability (UserName, Vin, ReportType, Expires)
		SELECT UserName, Vin, ''VHR'', Expires FROM dbo.CarfaxAvailability TABLOCKX')
GO
DROP TABLE dbo.CarfaxAvailability
GO
EXECUTE sp_rename N'dbo.Tmp_CarfaxAvailability', N'CarfaxAvailability', 'OBJECT'
GO