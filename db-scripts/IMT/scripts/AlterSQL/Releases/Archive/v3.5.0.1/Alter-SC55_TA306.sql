if not exists (select * from syscolumns where id=object_id('dbo.DealerPreference') and name='SearchAppraisalDaysBackThreshold')
ALTER TABLE dbo.DealerPreference
ADD SearchAppraisalDaysBackThreshold [int] NOT NULL CONSTRAINT [DF_DealerPreference_SearchAppraisalDaysBackThreshold] DEFAULT (45)
GO

if not exists (select * from syscolumns where id=object_id('dbo.DealerPreference') and name='SearchInactiveInventoryDaysBackThreshold')
ALTER TABLE dbo.DealerPreference
ADD SearchInactiveInventoryDaysBackThreshold [int] NOT NULL CONSTRAINT [DF_DealerPreference_SearchInactiveInventoryDaysBackThreshold] DEFAULT (90)
GO