CREATE TABLE [BookoutProcessorMode] (
	[BookoutProcessorModeId] tinyint not null,
	[Description] varchar(20) not null,
	CONSTRAINT [PK_BookoutProcessorMode] PRIMARY KEY
	(
		[BookoutProcessorModeId]
	)
);
GO

INSERT INTO [BookoutProcessorMode]
SELECT 0, 'Full Bookout'
UNION SELECT 1, 'Incremental'
GO

CREATE TABLE [BookoutProcessorStatus] (
	[BookoutProcessorStatusId] tinyint not null,
	[Description] varchar(20) not null,
	CONSTRAINT [PK_BookoutProcessorStatus] PRIMARY KEY
	(
		[BookoutProcessorStatusId]
	)
);
GO

INSERT INTO [BookoutProcessorStatus]
SELECT 0, 'Pending'
UNION SELECT 1, 'Loaded'
UNION SELECT 2, 'In Progress'
UNION SELECT 3, 'Successful'
UNION SELECT 4, 'Failure'
UNION SELECT 5, 'Process Died'
GO

CREATE TABLE [BookoutProcessorRunLog] (
	[BookoutProcessorRunId] int IDENTITY(1, 1) not null,
	[BusinessUnitId] int not null,
	[ThirdPartyId] tinyint not null,
	[BookoutProcessorModeId] tinyint not null,
	[BookoutProcessorStatusId] tinyint not null,
	[LoadTime] smalldatetime null
     constraint DF_BookoutProcessorRunLog__LoadTime
      default CURRENT_TIMESTAMP,
	[StartTime] smalldatetime null,
	[EndTime] smalldatetime null,
	[ServerName] varchar(50) null,
	CONSTRAINT [PK_BookoutProcessorRun] PRIMARY KEY
	(
		[BookoutProcessorRunId]
	),
	CONSTRAINT [FK_BookoutProcessorRun_BusinessUnit] FOREIGN KEY 
	(
		[BusinessUnitId]
	) REFERENCES [BusinessUnit] (
		[BusinessUnitId]
	),
	CONSTRAINT [FK_BookoutProcessorRun_ThirdParty] FOREIGN KEY 
	(
		[ThirdPartyId]
	) REFERENCES [ThirdParties] (
		[ThirdPartyId]
	),
	CONSTRAINT [FK_BookoutProcessorRun_BookoutProcessorMode] FOREIGN KEY 
	(
		[BookoutProcessorModeId]
	) REFERENCES [BookoutProcessorMode] (
		[BookoutProcessorModeId]
	),
	CONSTRAINT [FK_BookoutProcessorRun_BookoutProcessorStatus] FOREIGN KEY 
	(
		[BookoutProcessorStatusId]
	) REFERENCES [BookoutProcessorStatus] (
		[BookoutProcessorStatusId]
	)
);
GO