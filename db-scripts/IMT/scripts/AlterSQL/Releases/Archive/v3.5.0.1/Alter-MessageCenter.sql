/*

Create and populate the (revised) MessageCenter table

*/

SET NOCOUNT on

IF objectproperty(object_id('MessageCenter'), 'isTable') = 1
    DROP TABLE MessageCenter

CREATE TABLE MessageCenter
 (
   Ordering     tinyint        not null
  ,Title        varchar(300)   not null
  ,Body         varchar(4000)  not null
  ,HasPriority  tinyint        not null
 )

GO

--  Load three rows, Benson style
INSERT MessageCenter (Ordering, Title, Body, HasPriority)
       select 0, '<strong>Introducing the Customer Center</strong>', '- A quick and easy place to answer your questions and introduce you to new functionality.', 0
 union select 1, '<strong>Need For Speed - Welcome to v4.08</strong>', 'In this release we focused on improving and maximizing system speed. The performance improvements were concentrated in the following areas:
		<ul>
			<li>Aging Inventory Plan</li>
			<li>eStock Card</li>
			<li>Inventory Overview</li>
			<li>Edge Home Page</li>
			<li>Total Inventory Report</li>
			<li>Trade Analyzer and Trade Manager</li>
		</ul>', 1
 union select 2, '<strong>Help Desk Enhancements</strong>', 'We have upgraded our Help Desk support. Full time automotive technology professionals are available to our customers from 7am to 11pm CST (7 days a week) to answer questions and help with issues. Please call <strong>1-800-730-5665</strong> or <strong><a href="mailto:helpdesk@firstlook.biz?subject=Help Request">click here to email us</a></strong> for help.', 0
GO

