ALTER TABLE dbo.DealerPreference
	DROP DF_DealerPreference_UnitsSoldThresholdInvOverview
GO

ALTER TABLE dbo.DealerPreference
	ADD CONSTRAINT DF_DealerPreference_UnitsSoldThresholdInvOverview
	DEFAULT (6) FOR UnitsSoldThresholdInvOverview
GO

UPDATE dbo.DealerPreference
	SET UnitsSoldThresholdInvOverview = 6
GO
