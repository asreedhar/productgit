UPDATE dbo.tbl_CIAPreferences
    SET CIAAllocationTimePeriodId = 5
GO

UPDATE dbo.tbl_CIAPreferences
    SET CIATargetUnitsTimePeriodId = 1
GO

UPDATE dbo.tbl_CIAPreferences
    SET GDLightProcessorTimePeriodId = 1
GO

ALTER TABLE DealerRisk
    DROP CONSTRAINT DF_DealerRisk_RiskLevelNumberOfWeeks

ALTER TABLE DealerRisk
    ADD CONSTRAINT DF_DealerRisk_RiskLevelNumberOfWeeks
    DEFAULT (13) FOR RiskLevelNumberOfWeeks
GO

UPDATE dbo.DealerRisk
    SET RiskLevelNumberOfWeeks = 13
GO
