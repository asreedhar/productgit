ALTER TABLE DealerPreference
  ADD ATCEnabled tinyint NULL
GO

CREATE TABLE [dbo].[tbl_DealerATCAccessGroups] (
	[DealerATCAccessGroupsId] [int] IDENTITY (10000, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[AccessGroupId] [int] NOT NULL ,
	CONSTRAINT [PK_DealerATCAccessGroups] PRIMARY KEY  NONCLUSTERED 
	(
		[DealerATCAccessGroupsId]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [UNK_DealerATCAccessGroupsUnique_BU_AccessGroupsID] UNIQUE  NONCLUSTERED 
	(
		[BusinessUnitID],
		[AccessGroupId]
	) WITH  FILLFACTOR = 80  ON [IDX] 
) ON [DATA]
GO
