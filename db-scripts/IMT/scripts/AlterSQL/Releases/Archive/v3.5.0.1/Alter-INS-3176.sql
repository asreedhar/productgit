if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DealerFacts]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[DealerFacts]
GO

CREATE TABLE [dbo].[DealerFacts] (
	[DealerFactID] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[DateCreated] [datetime] NOT NULL ,
	[LastPolledDate] [datetime] NULL,
	[LastDMSReferenceDateNew] [datetime] NULL,
	[LastDMSReferenceDateUsed] [datetime] NULL
	CONSTRAINT [PK_DealerFacts] PRIMARY KEY  NONCLUSTERED 
	(
		[DealerFactID]
	) WITH  FILLFACTOR = 80  ON [DATA] ,
	CONSTRAINT [FK_DealerFacts_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	)
) ON [DATA]
GO