CREATE TABLE [dbo].[Role]
	(
	[RoleID] int NOT NULL,
	[Type] char(1) NOT NULL,
	[Code] char(10) NOT NULL,
	[Name] varchar(50) NOT NULL
	)  ON DATA
GO

ALTER TABLE [dbo].[Role] ADD CONSTRAINT
	[PK_Role] PRIMARY KEY CLUSTERED 
	(
	[RoleID]
	) ON DATA

GO

CREATE TABLE [dbo].[MemberRole]
	(
	[MemberRoleID] int NOT NULL IDENTITY (1, 1),
	[MemberID] int NOT NULL,
	[RoleID] int NOT NULL
	)  ON DATA
GO

ALTER TABLE [dbo].[MemberRole] ADD CONSTRAINT
	[PK_MemberRole] PRIMARY KEY CLUSTERED 
	(
	[MemberRoleID]
	) ON DATA
GO

ALTER TABLE [dbo].[MemberRole] ADD CONSTRAINT
	[UQ_MemberRole] UNIQUE NONCLUSTERED 
	(
	[MemberID],
	[RoleID]
	) ON DATA

GO

ALTER TABLE [dbo].[MemberRole] ADD CONSTRAINT
	[FK_MemberRole_Member] FOREIGN KEY
	(
	[MemberID]
	) REFERENCES [dbo].[Member]
	(
	[MemberID]
	)
GO

ALTER TABLE [dbo].[MemberRole] ADD CONSTRAINT
	[FK_MemberRole_Role] FOREIGN KEY
	(
	[RoleID]
	) REFERENCES [dbo].[Role]
	(
	[RoleID]
	)
GO

INSERT INTO [dbo].[Role] values (1, 'U', 'NOACCESS', 'No Access');
INSERT INTO [dbo].[Role] values (2, 'U', 'APPRAISE', 'Appraiser');
INSERT INTO [dbo].[Role] values (3, 'U', 'STANDARD', 'Standard');
INSERT INTO [dbo].[Role] values (4, 'U', 'MANAGER', 'Manager');
INSERT INTO [dbo].[Role] values (5, 'N', 'NOTAPPL', 'N/A');
INSERT INTO [dbo].[Role] values (6, 'N', 'STANDARD', 'Standard');
GO

INSERT INTO [dbo].[MemberRole] (MemberID, RoleID)
select m.MemberID, 4
from Member m, lu_UserRole ur 
where m.UserRoleCD = ur.UserRoleCD
  and ( ur.UserRoleDesc = 'Used'
   or ur.UserRoleDesc = 'All' );
GO

INSERT INTO [dbo].[MemberRole] (MemberID, RoleID)
select m.MemberID, 6
from Member m, lu_UserRole ur 
where m.UserRoleCD = ur.UserRoleCD
  and ( ur.UserRoleDesc = 'New'
   or ur.UserRoleDesc = 'All' );
GO