
CREATE TABLE [dbo].[lu_AccessGroupPrice] (
    [AccessGroupPriceId] [int] NOT NULL ,
    [Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
    CONSTRAINT [PK_lu_AccessGroupPrice] PRIMARY KEY  NONCLUSTERED 
    (
        [AccessGroupPriceId]
    ) WITH  FILLFACTOR = 80  ON [IDX] 
) ON [DATA]
GO

INSERT INTO lu_AccessGroupPrice (AccessGroupPriceId, Description) VALUES(1,'Buy it Now')
INSERT INTO lu_AccessGroupPrice (AccessGroupPriceId, Description) VALUES(2,'Current Bid')
GO

CREATE TABLE [dbo].[lu_AccessGroupActor] (
    [AccessGroupActorId] [int] NOT NULL ,
    [Description] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
    CONSTRAINT [PK_lu_AccessGroupActor] PRIMARY KEY  NONCLUSTERED 
    (
        [AccessGroupActorId]
    ) WITH  FILLFACTOR = 80  ON [IDX] 
) ON [DATA]
GO

INSERT INTO lu_AccessGroupActor (AccessGroupActorId, Description) VALUES(1,'+')
INSERT INTO lu_AccessGroupActor (AccessGroupActorId, Description) VALUES(2,'-')
INSERT INTO lu_AccessGroupActor (AccessGroupActorId, Description) VALUES(3,'*')
GO






