/*

Drop unnecessary/redundant unique constraing (index), and add a useeful index
(Only do this if the old index is found on the table)

*/

IF indexProperty(object_id('AppraisalBookouts'), 'UK_AppraisalBookouts', 'IndexID') is not null
 BEGIN
    ALTER TABLE AppraisalBookouts
     drop constraint UK_AppraisalBookouts


    CREATE nonclustered INDEX IX_AppraisalBookouts__AppraisalID
     on AppraisalBookouts (AppraisalID)
 END
GO
