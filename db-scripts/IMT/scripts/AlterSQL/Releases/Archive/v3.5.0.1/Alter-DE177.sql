if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_InventoryBucketRanges_InventoryBuckets]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[InventoryBucketRanges] DROP CONSTRAINT FK_InventoryBucketRanges_InventoryBuckets
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventoryBucketRanges]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[InventoryBucketRanges]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventoryBuckets]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[InventoryBuckets]
GO

CREATE TABLE [dbo].[InventoryBucketRanges] (
	[InventoryBucketRangeID] [int] IDENTITY (1, 1) NOT NULL ,
	[InventoryBucketID] [int] NOT NULL ,
	[RangeID] [tinyint] NOT NULL ,
	[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Low] [int] NOT NULL ,
	[High] [int] NULL ,
	[Lights] [tinyint] NOT NULL 
) ON [DATA]
GO

CREATE TABLE [dbo].[InventoryBuckets] (
	[InventoryBucketID] [int] IDENTITY (1, 1) NOT NULL ,
	[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[InventoryType] [tinyint] NOT NULL 
) ON [DATA]
GO

ALTER TABLE [dbo].[InventoryBucketRanges] WITH NOCHECK ADD 
	CONSTRAINT [PK_InventoryBucketRanges] PRIMARY KEY  CLUSTERED 
	(
		[InventoryBucketRangeID]
	)  ON [DATA] 
GO

ALTER TABLE [dbo].[InventoryBuckets] WITH NOCHECK ADD 
	CONSTRAINT [PK_InventoryBuckets] PRIMARY KEY  CLUSTERED 
	(
		[InventoryBucketID]
	)  ON [DATA] 
GO

ALTER TABLE [dbo].[InventoryBucketRanges] ADD 
	CONSTRAINT [DF_InventoryBucketRanges_Lights] DEFAULT (0) FOR [Lights]
GO

ALTER TABLE [dbo].[InventoryBuckets] ADD 
	CONSTRAINT [DF_InventoryBuckets_InventoryType] DEFAULT (2) FOR [InventoryType]
GO

ALTER TABLE [dbo].[InventoryBucketRanges] ADD 
	CONSTRAINT [FK_InventoryBucketRanges_InventoryBuckets] FOREIGN KEY 
	(
		[InventoryBucketID]
	) REFERENCES [dbo].[InventoryBuckets] (
		[InventoryBucketID]
	)
GO

set identity_insert InventoryBuckets on 
insert
into	InventoryBuckets (InventoryBucketID, Description, InventoryType)
select	1, 'Aging Inventory Plan (Used)', 2
union 
select	2, 'Total Inventory Report (New)', 1
union
select	3, 'Total Inventory Report (Used)', 2

set identity_insert InventoryBuckets off

insert
into	InventoryBucketRanges (InventoryBucketID, RangeID, Description, Low, High, Lights)
select 1, 1, 'OFF THE CLIFF - EFFICIENTLY LIQUIDATE INVENTORY', 60, null, 7  
union    
select 1, 2, 'AGGRESSIVELY PURSUE WHOLESALE OPTIONS', 50, 59, 7  
union    
select 1, 3, 'FINAL PUSH - AGGRESSIVE RETAIL STRATEGY<br> BEGIN PURSUING WHOLESALE OPTIONS', 40, 49, 7     
union      
select 1, 4, 'PURSUE AGGRESSIVE RETAIL STRATEGIES', 30, 39, 7  
union      
select 1, 5, 'LOW RISK VEHICLES (0-29 DAYS OLD)', 0, 29, 4  
union  
select 1, 6, 'HIGH RISK VEHICLES (0-15 DAYS)', 0, 15, 3  
union  
select 1, 7, 'HIGH RISK VEHICLES (16-29 DAYS OLD)', 16, 29, 3  
union	
select 2, 1, '91+', 91, null, 7
union    
select 2, 2, '76 - 90', 76, 90, 7  
union    
select 2, 3, '61 - 75', 61, 75, 7     
union      
select 2, 4, '31 - 60', 31, 60, 7  
union      
select 2, 5, '0 - 30', 0, 30, 7  
union
select 3, 1, '60+', 60, null, 7  
union    
select 3, 2, '50 - 59', 50, 59, 7  
union    
select 3, 3, '40 - 49', 40, 49, 7     
union      
select 3, 4, '30 - 39', 30, 39, 7  
union      
select 3, 5, '16 - 29', 16, 29, 7
union      
select 3, 6, '0 - 15', 0, 15, 7
go

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAgingInventoryPlanDetail]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetAgingInventoryPlanDetail]
GO
