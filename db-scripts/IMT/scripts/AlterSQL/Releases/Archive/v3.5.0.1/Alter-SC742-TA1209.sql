UPDATE 	V
SET	V.VehicleBodyStyleID = ISNULL(VBS.VehicleBodyStyleID,1)
FROM	Vehicle V 
	JOIN MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID
	LEFT JOIN EdmundsBodyTypes EBT ON MMG.EdmundsBodyType = EBT.EdmundsBodyType
	LEFT JOIN VehicleBodyStyle VBS ON ISNULL(CAST(NULLIF(DoorCount,0) AS VARCHAR) + 'DR' + CASE WHEN EBT.ExtendsVehicleBodyStyleDescription = 1 THEN ' ' + EBT.Description ELSE '' END, '') = VBS.Description
WHERE	V.VehicleBodyStyleID = 99
GO


DELETE
FROM	VehicleBodyStyle
WHERE	VehicleBodyStyleID = 99
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_VehicleSegmentSubSegment]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_VehicleSegmentSubSegment]
GO


--------------------------------------------------------------------------------
--	THROW IN SOME PROJECT CB AS WELL
--------------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetColorsForCIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetColorsForCIA]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_VehicleToSegment]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_VehicleToSegment]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_VehicleToSegmentSub1]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_VehicleToSegmentSub1]
GO
