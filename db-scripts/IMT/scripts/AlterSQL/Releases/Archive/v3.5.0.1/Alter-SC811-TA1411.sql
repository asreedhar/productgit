

INSERT
INTO	SubscriptionTypes (SubscriptionTypeID, Description)
SELECT	5, 'Over Under Stocking'
UNION
SELECT	6, 'PIPR'
UNION
SELECT	7, 'Redistribution Hot Sheets'
GO

ALTER TABLE Subscriptions DROP CONSTRAINT FK_Subscriptions_SubscriptionFrequencyTypes
GO

IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('dbo.Subscriptions') and name='SubscriptionFrequencySubTypeID')
	ALTER TABLE SubscriptionFrequencySubTypes DROP CONSTRAINT FK_SubscriptionFrequencySubTypes_SubscriptionFrequencyTypes
GO

DELETE
FROM	SubscriptionFrequencyTypes
GO

INSERT
INTO	SubscriptionFrequencyTypes (SubscriptionFrequencyID, Description)
SELECT	0, 'N/A'
UNION
SELECT	1, 'Weekly'
UNION
SELECT	2, 'Monthly'
UNION
SELECT	3, 'Quarterly'
GO


sp_rename 'SubscriptionFrequencyTypes', 'SubscriptionFrequency'
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('dbo.Subscriptions') and name='BusinessUnitID')
	ALTER TABLE Subscriptions ADD BusinessUnitID INT NOT NULL DEFAULT (0)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('dbo.Subscriptions') and name='SubscriptionFrequencyDetailID')
	ALTER TABLE Subscriptions ADD SubscriptionFrequencyDetailID TINYINT NOT NULL DEFAULT (0)
GO

UPDATE	Subscriptions
SET	SubscriptionFrequencyDetailID = SubscriptionFrequencyID
GO

IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('dbo.Subscriptions') and name='SubscriptionFrequencySubTypeID')
	ALTER TABLE Subscriptions DROP COLUMN SubscriptionFrequencySubTypeID
GO

IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('dbo.Subscriptions') and name='SubscriptionFrequencyID')
	ALTER TABLE Subscriptions DROP COLUMN SubscriptionFrequencyID
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SubscriptionFrequencySubTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SubscriptionFrequencySubTypes]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SubscriptionFrequencyDetail]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SubscriptionFrequencyDetail]
GO

CREATE TABLE [dbo].[SubscriptionFrequencyDetail] (
	[SubscriptionFrequencyDetailID] [tinyint] NOT NULL ,
	[SubscriptionFrequencyID] [tinyint] NOT NULL ,
	[Rank] [tinyint] NOT NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

ALTER TABLE [dbo].[SubscriptionFrequencyDetail] ADD 
	CONSTRAINT [PK_SubscriptionFrequencyDetail] PRIMARY KEY  CLUSTERED 
	(
		[SubscriptionFrequencyDetailID]
	)  ON [DATA] 
GO

ALTER TABLE [dbo].[SubscriptionFrequencyDetail] ADD 
	CONSTRAINT [FK_SubscriptionFrequencyDetail_SubscriptionFrequency] FOREIGN KEY
	(
	SubscriptionFrequencyID
	)
	REFERENCES SubscriptionFrequency
	(
		[SubscriptionFrequencyID]
	)  
GO

INSERT
INTO	SubscriptionFrequencyDetail (SubscriptionFrequencyDetailID, SubscriptionFrequencyID, Rank, Description)
SELECT	0, 0, 0, 'N/A'
UNION
SELECT	1, 0, 0, 'No Alerts'
UNION
SELECT	2, 0, 0, 'Only from Dealer Plan'
UNION
SELECT	3, 0, 0, 'Always'
GO

INSERT
INTO	SubscriptionFrequencyDetail (SubscriptionFrequencyDetailID, SubscriptionFrequencyID, Rank, Description)
SELECT	3 + DATEPART(dw,BeginDate), 1, DATEPART(dw,BeginDate), DATENAME(dw,BeginDate)
FROM	FLDW.dbo.Time_D
WHERE	TimeID between 20051212 and 20051218

GO

ALTER TABLE dbo.Subscriptions ADD CONSTRAINT
	FK_Subscriptions_SubscriptionFrequencyDetail FOREIGN KEY
	(
	SubscriptionFrequencyDetailID
	) REFERENCES dbo.SubscriptionFrequencyDetail
	(
	SubscriptionFrequencyDetailID
	)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('dbo.SubscriptionTypes') and name='Notes')
	ALTER TABLE SubscriptionTypes ADD Notes VARCHAR(500) NULL
GO

