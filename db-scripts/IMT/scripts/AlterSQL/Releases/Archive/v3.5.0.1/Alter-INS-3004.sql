print 'INS-3004 Alter Script'

go

alter table appraisalguidebookvalues alter column value int null

go

alter table appraisalguidebookvalues add valid tinyint not null  DEFAULT (1)

go

update appraisalguidebookvalues set valid = 0, value = null where value = -2147483648

go

--now update the guidebookvalue table



alter table guidebookvalue alter column initialvalue int null

go

alter table guidebookvalue alter column currentvalue int null

go

alter table guidebookvalue add valid tinyint not null  DEFAULT (1)

go

update guidebookvalue set valid = 0, currentvalue = null where currentvalue = -2147483648

go

update guidebookvalue set initialvalue = null where initialvalue = -2147483648 and valid = 0

go

update guidebookvalue set initialvalue = 0 where initialvalue = -2147483648 and valid = 1

go
