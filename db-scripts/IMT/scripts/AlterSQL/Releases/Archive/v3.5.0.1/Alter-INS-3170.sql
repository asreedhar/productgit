------------------------------------------------------------------------------------------------
--	INS-3170
------------------------------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PurchasingCenterChannels]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[PurchasingCenterChannels]
GO

CREATE TABLE [dbo].[PurchasingCenterChannels] (
	[ChannelID] [int] IDENTITY (1, 1) NOT NULL ,
	[ChannelName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SearchFunctionName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HasAccessGroups] [bit] NOT NULL  
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[PurchasingCenterChannels] WITH NOCHECK ADD 
	CONSTRAINT [DF_PurchasingCenterChannels_HasAccessGroups] DEFAULT (0) FOR [HasAccessGroups],
	CONSTRAINT [PK_PurchasingCenterChannels] PRIMARY KEY  CLUSTERED 
	(
		[ChannelID]
	)  ON [PRIMARY] 
GO


insert
into	PurchasingCenterChannels (ChannelName,SearchFunctionName,HasAccessGroups)
select	'ATC', 'GetATCVehiclesUsingCIACriteria', 1
union
select	'In-Group','GetInGroupVehiclesUsingCIACriteria', 0
