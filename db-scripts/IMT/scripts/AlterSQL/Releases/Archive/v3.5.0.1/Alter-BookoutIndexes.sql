/*

Add indexes (if not already present) to optimize bookout-related queries

*/

IF indexProperty(object_id('ThirdPartyVehicleOptions'), 'IX_ThirdPartyVehicleOptions__ThirdPartyVehicleID', 'IndexID') is null
    CREATE nonclustered INDEX IX_ThirdPartyVehicleOptions__ThirdPartyVehicleID
     on ThirdPartyVehicleOptions (ThirdPartyVehicleID)
     with sort_in_tempdb

IF indexProperty(object_id('ThirdPartyVehicleOptionValues'), 'IX_ThirdPartyVehicleOptionValues__ThirdPartyVehicleOptionID', 'IndexID') is null
    CREATE nonclustered INDEX IX_ThirdPartyVehicleOptionValues__ThirdPartyVehicleOptionID
     on ThirdPartyVehicleOptionValues (ThirdPartyVehicleOptionID)
     with sort_in_tempdb


IF indexProperty(object_id('InventoryThirdPartyVehicles'), 'IX_InventoryThirdPartyVehicles_InventoryID', 'IndexID') is null
    CREATE nonclustered INDEX IX_InventoryThirdPartyVehicles_InventoryID
     on InventoryThirdPartyVehicles (InventoryID)
     with sort_in_tempdb

IF indexProperty(object_id('AppraisalThirdPartyVehicles'), 'IX_AppraisalThirdPartyVehicles_AppraisalID', 'IndexID') is null
    CREATE nonclustered INDEX IX_AppraisalThirdPartyVehicles_AppraisalID
     on AppraisalThirdPartyVehicles (AppraisalID)
     with sort_in_tempdb
GO
