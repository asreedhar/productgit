CREATE TABLE dbo.GlobalParam
	(
	id int IDENTITY(1,1) NOT NULL,
	[key] char(20) NOT NULL,
	[value] varchar(256) NULL
	)  ON DATA
GO
ALTER TABLE dbo.GlobalParam ADD CONSTRAINT
	PK_GlobalParam PRIMARY KEY CLUSTERED 
	(
	id
	) ON DATA
GO
INSERT INTO GlobalParam ([key], [value]) VALUES ('AutoCheckCID', '5010084' );
INSERT INTO GlobalParam ([key], [value]) VALUES ('AutoCheckPWD', 'First501' );
GO