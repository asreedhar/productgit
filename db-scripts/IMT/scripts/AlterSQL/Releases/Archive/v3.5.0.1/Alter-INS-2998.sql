if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DealerPreferenceDataload]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[DealerPreferenceDataload]
GO

CREATE TABLE [dbo].[DealerPreferenceDataload] (
	[DealerPreferenceDataloadID] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[TradeorPurchaseFromSales] [tinyint] NOT NULL ,
	[TradeorPurchaseFromSalesDaysThreshold] [smallint] NOT NULL ,
	[GenerateInventoryDeltas] [tinyint] NOT NULL 
) ON [DATA]
GO

ALTER TABLE [dbo].[DealerPreferenceDataload] ADD 
	CONSTRAINT [DF_DealerPreferenceDataload_TradeorPurchaseFromSales] DEFAULT (0) FOR [TradeorPurchaseFromSales],
	CONSTRAINT [DF_DealerPreferenceDataload_TradeorPurchaseFromSalesDaysThreshold] DEFAULT (0) FOR [TradeorPurchaseFromSalesDaysThreshold],
	CONSTRAINT [DF_DealerPreferenceDataload_GenerateInventoryDeltas] DEFAULT (0) FOR [GenerateInventoryDeltas],
	CONSTRAINT [PK_DealerPreferenceDataload] PRIMARY KEY  NONCLUSTERED 
	(
		[DealerPreferenceDataloadID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
GO

 CREATE  UNIQUE  INDEX [IX_DealerPreferenceDataload] ON [dbo].[DealerPreferenceDataload]([BusinessUnitID]) WITH  FILLFACTOR = 80 ON [IDX]
GO

ALTER TABLE [dbo].[DealerPreferenceDataload] ADD 
	CONSTRAINT [FK_DealerPreferenceDataload_DealerPreference] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[DealerPreference] (
		[BusinessUnitID]
	) ON DELETE CASCADE  ON UPDATE CASCADE 
GO

