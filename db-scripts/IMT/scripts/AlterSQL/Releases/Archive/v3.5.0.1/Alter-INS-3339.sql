ALTER TABLE dbo.DealerPreference
	ADD averageInventoryAgeRedThreshold int NULL
GO

ALTER TABLE dbo.DealerPreference
	ADD CONSTRAINT [DF_DealerPreference_AverageInventoryAgeRedThreshold]
	DEFAULT (30) FOR [averageInventoryAgeRedThreshold]
GO

UPDATE dbo.DealerPreference
	SET averageInventoryAgeRedThreshold = 30

ALTER TABLE dbo.DealerPreference
	ADD averageDaysSupplyRedThreshold int NULL
GO

ALTER TABLE dbo.DealerPreference
	ADD CONSTRAINT [DF_DealerPreference_AverageDaysSupplyRedThreshold]
	DEFAULT (50) FOR [averageDaysSupplyRedThreshold]
GO

UPDATE dbo.DealerPreference
	SET averageDaysSupplyRedThreshold = 50