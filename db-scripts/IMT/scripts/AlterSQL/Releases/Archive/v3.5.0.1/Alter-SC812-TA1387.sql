if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_MarketplaceSubmissions_MarketplaceSubmissionStatusCodes]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[MarketplaceSubmissions] DROP CONSTRAINT FK_MarketplaceSubmissions_MarketplaceSubmissionStatusCodes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Marketplace_MarketplaceTypes]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Marketplaces] DROP CONSTRAINT FK_Marketplace_MarketplaceTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_MarketplaceSubmissions_Marketplaces]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[MarketplaceSubmissions] DROP CONSTRAINT FK_MarketplaceSubmissions_Marketplaces
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MarketplaceSubmissionStatusCodes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[MarketplaceSubmissionStatusCodes]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MarketplaceSubmissions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[MarketplaceSubmissions]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MarketplaceTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[MarketplaceTypes]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Marketplaces]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Marketplaces]
GO

CREATE TABLE [dbo].[MarketplaceSubmissionStatusCodes] (
	[MarketplaceSubmissionStatusCD] [tinyint] NOT NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[MarketplaceSubmissions] (
	[MarketplaceSubmissionID] [int] IDENTITY (1, 1) NOT NULL ,
	[MarketplaceID] [tinyint] NOT NULL ,
	[InventoryID] [int] NOT NULL ,
	[MemberID] [int] NOT NULL ,
	[MarketplaceSubmissionStatusCD] [tinyint] NOT NULL ,
	[URL] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[BeginEffectiveDate] [datetime] NOT NULL ,
	[EndEffectiveDate] [datetime] NOT NULL ,
	[DateCreated] [datetime] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[MarketplaceTypes] (
	[MarketplaceTypeID] [tinyint] NOT NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Marketplaces] (
	[MarketplaceID] [tinyint] NOT NULL ,
	[MarketplaceTypeID] [tinyint] NOT NULL ,
	[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[MarketplaceSubmissionStatusCodes] WITH NOCHECK ADD 
	CONSTRAINT [PK_MarketplaceSubmissionStatusCodes] PRIMARY KEY  CLUSTERED 
	(
		[MarketplaceSubmissionStatusCD]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[MarketplaceSubmissions] WITH NOCHECK ADD 
	CONSTRAINT [PK_MarketplaceSubmissions] PRIMARY KEY  CLUSTERED 
	(
		[MarketplaceSubmissionID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[MarketplaceTypes] WITH NOCHECK ADD 
	CONSTRAINT [PK_MarketplaceTypes] PRIMARY KEY  CLUSTERED 
	(
		[MarketplaceTypeID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Marketplaces] WITH NOCHECK ADD 
	CONSTRAINT [PK_Marketplace] PRIMARY KEY  CLUSTERED 
	(
		[MarketplaceID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[MarketplaceSubmissions] ADD 
	CONSTRAINT [DF_MarketplaceSubmissions_DateCreated] DEFAULT (getdate()) FOR [DateCreated]
GO

ALTER TABLE [dbo].[MarketplaceSubmissions] ADD 
	CONSTRAINT [FK_MarketplaceSubmissions_Inventory] FOREIGN KEY 
	(
		[InventoryID]
	) REFERENCES [dbo].[Inventory] (
		[InventoryID]
	),
	CONSTRAINT [FK_MarketplaceSubmissions_Marketplaces] FOREIGN KEY 
	(
		[MarketplaceID]
	) REFERENCES [dbo].[Marketplaces] (
		[MarketplaceID]
	),
	CONSTRAINT [FK_MarketplaceSubmissions_MarketplaceSubmissionStatusCodes] FOREIGN KEY 
	(
		[MarketplaceSubmissionStatusCD]
	) REFERENCES [dbo].[MarketplaceSubmissionStatusCodes] (
		[MarketplaceSubmissionStatusCD]
	),
	CONSTRAINT [FK_MarketplaceSubmissions_Member] FOREIGN KEY 
	(
		[MemberID]
	) REFERENCES [dbo].[Member] (
		[MemberID]
	)
GO

ALTER TABLE [dbo].[Marketplaces] ADD 
	CONSTRAINT [FK_Marketplace_MarketplaceTypes] FOREIGN KEY 
	(
		[MarketplaceTypeID]
	) REFERENCES [dbo].[MarketplaceTypes] (
		[MarketplaceTypeID]
	)
GO



INSERT
INTO	MarketplaceTypes
SELECT	1, 'Listing Service'
UNION	
SELECT	2, 'Auction'

GO

INSERT
INTO	Marketplaces
SELECT	1, 1, 'GMAC Smart Auction'

GO

INSERT
INTO	MarketplaceSubmissionStatusCodes
SELECT	1, 'Submitted'
UNION	
SELECT	2, 'Posted'

GO

INSERT
INTO	CredentialType (Name)
SELECT	'GMAC Smart Auction'
GO

