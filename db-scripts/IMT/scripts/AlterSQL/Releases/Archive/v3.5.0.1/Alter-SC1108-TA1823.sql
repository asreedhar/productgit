

ALTER TABLE dbo.MemberCredential ADD MemberCredentialID INT IDENTITY(1,1)
GO
ALTER TABLE dbo.MemberCredential DROP CONSTRAINT PK_MemberCredential
GO
ALTER TABLE dbo.MemberCredential ADD CONSTRAINT PK_MemberCredential PRIMARY KEY CLUSTERED ([MemberCredentialID])
GO
ALTER TABLE dbo.MemberCredential ALTER COLUMN Username VARCHAR(25)
GO
ALTER TABLE dbo.MemberCredential ALTER COLUMN Password VARCHAR(25)
GO

DELETE 
FROM 	MemberCredential
WHERE 	ISNULL(Username,'') = ''
	OR ISNULL(password,'') = ''

GO