--Best Guess to restore aging plan

------------------------------
-- Update Retail Approaches --
------------------------------
update
	dbo.VehiclePlanTracking
set 
	Approach = 'R'
--select vpt.*
from
	dbo.VehiclePlanTracking as vpt
	join dbo.VehiclePlanTrackingHeader as vpth on vpt.VehiclePlanTrackingHeaderID = vpth.VehiclePlanTrackingHeaderID
where
(
	-- Exclude Wholesale
	vpt.wholesaler = 0
	and vpt.auction = 0
	-- Exclude Sold
	and vpt.Retail = 0
	and vpt.wholesale = 0
	-- Must have RetailDate if it was ever retailed
	and RetailDate is not null
	-- if reprice was null and non of the above, best guess is that it was a retail and not an other
	and (vpt.spiffs = 1 or vpt.Promos = 1 or vpt.Advertise = 1 or vpt.Other = 1 or vpt.Reprice is not null )
)
and vpt.Approach not in ( 'R', 'W', 'S', 'O' )
and vpth.Status = 2
GO

---------------------------------
-- Update Wholesale Approaches --
---------------------------------
update
	dbo.VehiclePlanTracking
set 
	Approach = 'W'
--select vpt.*
from
	dbo.VehiclePlanTracking as vpt
	join dbo.VehiclePlanTrackingHeader as vpth on vpt.VehiclePlanTrackingHeaderID = vpth.VehiclePlanTrackingHeaderID
where
(
	-- Exclude Retail
	vpt.spiffs = 0
	and vpt.promos = 0
	and vpt.advertise = 0
	and vpt.other = 0
	-- Exclude Sold
	--and vpt.Retail = 0
	--and vpt.wholesale = 0
	and vpt.WholesaleDate is not null
	and (vpt.Wholesaler = 1 or vpt.Auction = 1)
)
and vpt.Approach not in ( 'R', 'W', 'S', 'O' )
and vpth.Status = 2
GO

---------------------------------
-- Update Sold Approaches --
---------------------------------
update
	dbo.VehiclePlanTracking
set 
	Approach = 'S'
--select vpt.*
from
	dbo.VehiclePlanTracking as vpt
	join dbo.VehiclePlanTrackingHeader as vpth on vpt.VehiclePlanTrackingHeaderID = vpth.VehiclePlanTrackingHeaderID
where
(
	vpt.RETAIL = 1
	or vpt.WHOLESALE = 1
	and (vpt.Auction = 0 and vpt.Wholesale = 0)
)
and vpt.Approach not in ( 'R', 'W', 'S', 'O' )
and vpth.Status = 2
GO

---------------------------------
-- Update Other Approaches --
---------------------------------
update
	dbo.VehiclePlanTracking
set 
	Approach = 'O'
--select vpt.*
from
	dbo.VehiclePlanTracking as vpt
	join dbo.VehiclePlanTrackingHeader as vpth on vpt.VehiclePlanTrackingHeaderID = vpth.VehiclePlanTrackingHeaderID
where
(
	vpt.spiffs = 0
	and vpt.promos = 0
	and vpt.advertise = 0
	and vpt.other = 0
	and vpt.wholesaler = 0
	and vpt.Auction = 0
	and vpt.RETAIL = 0
	and vpt.WHOLESALE = 0
	and vpt.RetailDate is null
	and vpt.WholesaleDate is null
	and vpt.Notes is not null
)
and vpt.Approach not in ( 'R', 'W', 'S', 'O' )
and vpth.Status = 2
GO

/**

Fix Range IDs

*/
update
	dbo.VehiclePlanTracking
set 
	RangeId =  
--select 
	CASE
		WHEN( vpt.currentPlanVehicleAge >= 60 ) then 1
		WHEN( vpt.currentPlanVehicleAge >= 50 and vpt.currentPlanVehicleAge < 60 ) then 2
		WHEN( vpt.currentPlanVehicleAge >= 40 and vpt.currentPlanVehicleAge < 50 ) then 3
		WHEN( vpt.currentPlanVehicleAge >= 30 and vpt.currentPlanVehicleAge < 40 ) then 4
		WHEN( vpt.currentPlanVehicleAge < 30 and inv.currentVehicleLight = 3) then 5
		WHEN( vpt.currentPlanVehicleAge < 30 and inv.currentVehicleLight != 3 ) then 6
	END
from dbo.VehiclePlanTracking vpt
join dbo.Inventory inv on vpt.InventoryId = inv.InventoryId
join dbo.VehiclePlanTrackingHeader as vpth on vpt.VehiclePlanTrackingHeaderID = vpth.VehiclePlanTrackingHeaderID
where vpth.Status = 2
GO

--
--Use this query to see how many plans are empty
--
/*
select vpt.* --distinct vpt.businessunitid, count(vpt.vehiclePlanTrackingId) as NoPlansOrMessedUp
from dbo.VehiclePlanTracking vpt
join dbo.VehiclePlanTrackingHeader as vpth on vpt.VehiclePlanTrackingHeaderID = vpth.VehiclePlanTrackingHeaderID
where approach not in ( 'R', 'W', 'S', 'O' )
and retailDate is not null
and wholesaledate is not null
and vpth.Status = 2
and vpt.created > '2005-06-01'
--group by vpt.BusinessUnitId
*/