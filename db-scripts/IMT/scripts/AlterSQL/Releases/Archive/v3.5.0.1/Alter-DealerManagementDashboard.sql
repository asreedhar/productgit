--  Now add the login as a database user  -----------------------------------------------
IF exists (select 0
            from sysUsers su
             inner join master..sysLogins sl
              on sl.sid = su.sid
            where sl.name = 'FirstlookReports')
    PRINT 'Login "FirstLookReports" has already been configured as a user in database ' + db_name()
ELSE
 BEGIN
    --  Not configured in this database, so set up
    EXECUTE sp_addUser 'FirstlookReports'
 END

GO