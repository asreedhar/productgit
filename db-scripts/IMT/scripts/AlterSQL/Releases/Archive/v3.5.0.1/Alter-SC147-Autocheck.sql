if not exists (select * from dbo.syscolumns where id = object_id(N'DealerPreference') and name = 'AutocheckSID')
ALTER TABLE [dbo].DealerPreference
ADD AutocheckSID varchar(25) NULL
GO