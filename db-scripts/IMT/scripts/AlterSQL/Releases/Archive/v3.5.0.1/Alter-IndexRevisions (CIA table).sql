/*

This will optimize an apparently frequent Hibernate query

*/

CREATE nonclustered INDEX IX_CIAGroupingItemDetails__CIAGroupingItemId
 on CIAGroupingItemDetails (ciaGroupingItemId)
 with fillfactor = 95
GO
