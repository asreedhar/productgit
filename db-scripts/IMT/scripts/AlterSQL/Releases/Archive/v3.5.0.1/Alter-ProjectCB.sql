if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Create_NEWPricePointBucket]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sp_Create_NEWPricePointBucket]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sp_Create_USEDPricePointBucket]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sp_Create_USEDPricePointBucket]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[getDealerRedistributionOpportunities]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[getDealerRedistributionOpportunities]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetTradeAnalyzerReportBodyStyle]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetTradeAnalyzerReportBodyStyle]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetTradeAnalyzerReportBodyStyle]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetTradeAnalyzerReportBodyStyle]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_Create_PricePointsForPricePointTable_Hendrick]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_Create_PricePointsForPricePointTable_Hendrick]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehiclePhoto]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[VehiclePhoto]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAMakeModelMinMax]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[CIAMakeModelMinMax]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAMakeModelSuppression]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[CIAMakeModelSuppression]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MakeModelGroupingMigration]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[MakeModelGroupingMigration]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleOption]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[VehicleOption]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vwCIAContributers]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vwCIAContributers]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vwCIAGroupingSuppression]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vwCIAGroupingSuppression]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vwCIAWeighting]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vwCIAWeighting]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_CIAMakeModelMinMax]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_CIAMakeModelMinMax]
GO

if exists (select * from syscolumns where id=object_id('dbo.tbl_MakeModelGrouping') and name='OriginID')
alter table [dbo].[tbl_MakeModelGrouping] DROP COLUMN [OriginID]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[FK_MakeModelGrouping_VehicleCategoryID]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
alter table [dbo].[tbl_MakeModelGrouping] DROP CONSTRAINT [FK_MakeModelGrouping_VehicleCategoryID]
GO

if exists (select * from syscolumns where id=object_id('dbo.tbl_MakeModelGrouping') and name='CategoryID')
alter table [dbo].[tbl_MakeModelGrouping] DROP COLUMN [CategoryID]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleCategory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[VehicleCategory]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleAttributes]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[VehicleAttributes]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_VehicleAttributes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_VehicleAttributes]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleAttributes]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[VehicleAttributes]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fnCIAStockingGuide]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fnCIAStockingGuide]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_Test]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_Test]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_VehicleSegmentSubSegment]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_VehicleSegmentSubSegment]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_VehicleToSegment]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_VehicleToSegment]
GO



