ALTER TABLE [dbo].[tbl_CIAPreferences] 
DROP CONSTRAINT [DF_tbl_CIAPreferences_GoodBetsDealThreshold]

ALTER TABLE [dbo].[tbl_CIAPreferences] 
DROP CONSTRAINT [DF_tbl_CIAPreferences_GoodBetsGrouingThreshold]

ALTER TABLE [dbo].[tbl_CIAPreferences] 
DROP CONSTRAINT [DF_tbl_CIAPreferences_CoreThreshold]

ALTER TABLE tbl_CIAPreferences
DROP COLUMN GoodBetsDealThreshold
GO

ALTER TABLE tbl_CIAPreferences
DROP COLUMN GoodBetsGroupingThreshold
GO

ALTER TABLE tbl_CIAPreferences
DROP COLUMN CoreThreshold
GO