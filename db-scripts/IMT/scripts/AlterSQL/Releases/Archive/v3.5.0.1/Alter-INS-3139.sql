------------------------------------------------------------------------
--	NOTE: THIS ALTER SCRIPT TARGETS THE AuctionNet DATABASE
------------------------------------------------------------------------

USE AuctionNet
GO

ALTER TABLE [dbo].[AuctionTransaction_A1] ADD [ModelYear] [int] not null default (0)
GO

ALTER TABLE [dbo].[AuctionTransaction_A2] ADD [ModelYear] [int] not null default (0)
GO

ALTER TABLE [dbo].[AuctionTransaction_A1] DROP 
	CONSTRAINT [PK_AuctionTransaction_A1]
GO

ALTER TABLE [dbo].[AuctionTransaction_A1] ADD 
	CONSTRAINT [PK_AuctionTransaction_A1] PRIMARY KEY  CLUSTERED 
	(
		[PeriodID],
		[SeriesBodyStyleID],
		[RegionID],
		[ModelYear]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[AuctionTransaction_A2] DROP 
	CONSTRAINT [PK_AuctionTransaction_A2]
GO
ALTER TABLE [dbo].[AuctionTransaction_A2] ADD 
	CONSTRAINT [PK_AuctionTransaction_A2] PRIMARY KEY  CLUSTERED 
	(
		[PeriodID],
		[SeriesBodyStyleID],
		[RegionID],
		[ModelYear],
		[LowMileageRange],
		[HighMileageRange]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO


alter proc dbo.Rebuild_Aggregates
--------------------------------------------------------------------------------
--
--	Loads the AuctionNet data and associated dimensions in the AuctionNet
--	star schema.
--	
---History----------------------------------------------------------------------
--	
--	WGH	09/02/2004	Initial design/development
--		01/11/2005	Added ModelYear to the aggregates
--
--------------------------------------------------------------------------------	
as

--------------------------------------------------------------------------------
--	INITIALIZATION
--------------------------------------------------------------------------------

set nocount on 

declare @sql 	varchar(1000),
	@rc		int,
	@err 		int,
	@msg		varchar(255),
	@proc_name	varchar(128),
	@path		varchar(100)

set @proc_name = 'AuctionNet_Rebuild_Aggregates'
exec DBASTAT..Log_Event 'I', @proc_name, 'Started'

--------------------------------------------------------------------------------
--	REBUILD AGGREGATE TABLES'
--	SHOULD DROP KEYS AND RESTORE AFTER COMPLETELY POP'D
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
set @msg = 'TRUNCATE AuctionTransaction_A1'
--------------------------------------------------------------------------------

truncate table AuctionTransaction_A1

select @err = @@error, @rc = @@error
if @err <> 0 goto Failed

--------------------------------------------------------------------------------
set @msg = 'Populate AuctionTransaction_A1'
--------------------------------------------------------------------------------

insert
into	AuctionTransaction_A1 (PeriodID, SeriesBodyStyleID, RegionID, ModelYear, HighSalePrice, AveSalePrice, LowSalePrice, HighMileage, AveMileage, LowMileage, TransactionCount)
select 	WP.PeriodID, SBS.SeriesBodyStyleID, A.RegionID, ModelYear, max(SalePrice), avg(SalePrice), min(SalePrice), 0, avg(cast(Mileage as decimal(10,2))), 0, count(*)
from 	AuctionTransaction_F A
	join Time_D T on A.TimeID = T.TimeID
	join WeeklyPeriods WP on T.BeginDate between WP.BeginDate and WP.EndDate
	join SeriesBodyStyle_D SBS on A.SeriesID = SBS.SeriesID and A.BodyStyleID = SBS.BodyStyleID

group
by	WP.PeriodID, SBS.SeriesBodyStyleID, A.RegionID, SBS.SeriesBodyStyleID, ModelYear

select @err = @@error, @rc = @@rowcount
if @err <> 0 goto Failed

set @msg = cast(@rc as varchar) + ' rows inserted into AuctionTransaction_A1'
exec DBASTAT..Log_Event 'I', @proc_name, @msg

--------------------------------------------------------------------------------
set @msg = 'Update AuctionTransaction_A1 (HighMileage)'
--------------------------------------------------------------------------------

update	A
set	HighMileage = Mileage
from	AuctionTransaction_A1 A
	join SeriesBodyStyle_D SBS on A.SeriesBodyStyleID = SBS.SeriesBodyStyleID
	join AuctionTransaction_F F on SBS.SeriesID = F.SeriesID and SBS.BodyStyleID = F.BodyStyleID and A.RegionID = F.RegionID and A.ModelYear = F.ModelYear and F.SalePrice = A.HighSalePrice
	join Time_D T on F.TimeID = T.TimeID
	join WeeklyPeriods WP on T.BeginDate between WP.BeginDate and WP.EndDate and A.PeriodID = WP.PeriodID

select @err = @@error, @rc = @@rowcount
if @err <> 0 goto Failed

set @msg = cast(@rc as varchar) + ' rows updated in AuctionTransaction_A1 (HighMileage)'
exec DBASTAT..Log_Event 'I', @proc_name, @msg

--------------------------------------------------------------------------------
set @msg = 'Update AuctionTransaction_A1 (LowMileage)'
--------------------------------------------------------------------------------

update	A
set	LowMileage = Mileage
from	AuctionTransaction_A1 A
	join SeriesBodyStyle_D SBS on A.SeriesBodyStyleID = SBS.SeriesBodyStyleID
	join AuctionTransaction_F F on SBS.SeriesID = F.SeriesID and SBS.BodyStyleID = F.BodyStyleID and A.RegionID = F.RegionID  and A.ModelYear = F.ModelYear and F.SalePrice = A.LowSalePrice
	join Time_D T on F.TimeID = T.TimeID
	join WeeklyPeriods WP on T.BeginDate between WP.BeginDate and WP.EndDate and A.PeriodID = WP.PeriodID


select @err = @@error, @rc = @@rowcount
if @err <> 0 goto Failed

set @msg = cast(@rc as varchar) + ' rows updated in AuctionTransaction_A1 (LowMileage)'
exec DBASTAT..Log_Event 'I', @proc_name, @msg

--------------------------------------------------------------------------------
set @msg = 'TRUNCATE AuctionTransaction_A2'
--------------------------------------------------------------------------------

truncate table AuctionTransaction_A2

select @err = @@error, @rc = @@error
if @err <> 0 goto Failed

--------------------------------------------------------------------------------
set @msg = 'Populate AuctionTransaction_A2'
--------------------------------------------------------------------------------

insert
into	AuctionTransaction_A2 (PeriodID, SeriesBodyStyleID, RegionID, ModelYear, LowMileageRange, HighMileageRange, AveSalePrice)
select	PeriodID, SeriesBodyStyleID, RegionID, ModelYear, R.Low, R.High, avg(SalePrice)
from	AuctionTransaction_F A
	join Time_D T on A.TimeID = T.TimeID
	join WeeklyPeriods WP on T.BeginDate between WP.BeginDate and WP.EndDate
	join SeriesBodyStyle_D SBS on A.SeriesID = SBS.SeriesID and A.BodyStyleID = SBS.BodyStyleID
	join (	select 	distinct cast(Mileage / 10000 as int) * 10000 Low, cast(Mileage / 10000 as int) * 10000 + 9999 High
		from 	AuctionTransaction_F
		) R on A.Mileage between R.Low and R.High
group
by	PeriodID, SeriesBodyStyleID, RegionID, ModelYear, R.Low, R.High

select @err = @@error, @rc = @@rowcount
if @err <> 0 goto Failed

set @msg = cast(@rc as varchar) + ' rows updated in AuctionTransaction_A2'
exec DBASTAT..Log_Event 'I', @proc_name, @msg


Finished:

exec DBASTAT..Log_Event 'I', @proc_name, 'Completed without error'
return 0

Failed:

set @msg = 'Failed at step "' + @msg + '"'
exec DBASTAT..Log_Event 'I', @proc_name, @msg
return @err

GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AuctionTransaction_AV1]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[AuctionTransaction_AV1]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AuctionTransaction_AV2]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[AuctionTransaction_AV2]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE view dbo.AuctionTransaction_AV1
as
select 	PeriodID, SeriesBodyStyleID, RegionID, ModelYear, HighSalePrice, AveSalePrice, LowSalePrice, HighMileage, AveMileage, LowMileage, TransactionCount
from	AuctionTransaction_A1

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE view dbo.AuctionTransaction_AV2
as
select 	PeriodID, SeriesBodyStyleID, RegionID, ModelYear, LowMileageRange, HighMileageRange, AveSalePrice 
from	AuctionTransaction_A2

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AuctionDetailReport]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[AuctionDetailReport]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE view dbo.AuctionDetailReport
as
select 	A.AuctionTransactionID, WP.PeriodID, SBS.SeriesBodyStyleID, RegionID, ModelYear, cast(T.BeginDate as smalldatetime) SaleDate, SaleTypeName, SalePrice, Mileage, E.Engine, TR.Transmission, SBSV.SeriesBodyStyle, A.VIN
from 	AuctionTransaction_F A
	join Time_D T on A.TimeID = T.TimeID
	join WeeklyPeriods WP on T.BeginDate between WP.BeginDate and WP.EndDate
	join SaleType_D ST on A.SaleTypeID = ST.SaleTypeID
	join SeriesBodyStyle_D SBS on A.SeriesID = SBS.SeriesID and A.BodyStyleID = SBS.BodyStyleID
	join SeriesBodyStyle SBSV on SBS.SeriesBodyStyleID = SBSV.SeriesBodyStyleID
	join Engine_D E on A.EngineID = E.EngineID
	join Transmission_D TR on A.TransmissionID = TR.TransmissionID



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO