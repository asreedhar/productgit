ALTER TABLE dbo.TradeAnalyzerEvent
	ALTER COLUMN Model varchar(50)
GO

ALTER TABLE dbo.Appraisal
	ALTER COLUMN Model varchar(50)
GO

ALTER TABLE dbo.DecodedFirstLookSquishVINs
	ALTER COLUMN Model varchar(50)
GO

ALTER TABLE dbo.DecodedSquishVINs
	ALTER COLUMN Model varchar(50)
GO

ALTER TABLE dbo.lu_Edmunds_Squishvin
	ALTER COLUMN Model varchar(50)
GO

ALTER TABLE dbo.lu_Edmunds_Style_New
	ALTER COLUMN Model varchar(50)
GO

ALTER TABLE dbo.lu_Edmunds_Style_Used
	ALTER COLUMN Model varchar(50)
GO

if exists( select * from sysobjects where id = object_id( N'dbo.MakeModelGroupingMigration'))
ALTER TABLE dbo.MakeModelGroupingMigration
	ALTER COLUMN Model varchar(50)
GO

--Don't need to change this one...yet
--ALTER TABLE dbo.ModelBodyStyleOverrides
--	ALTER COLUMN Model varchar(50)
--GO

ALTER TABLE dbo.SingleVINDecodeTarget
	ALTER COLUMN Model varchar(50)
GO

ALTER TABLE dbo.tbl_MakeModelGrouping
	ALTER COLUMN Model varchar(50)
GO

ALTER TABLE dbo.TradeAnalyzerEvent
	ALTER COLUMN Model varchar(50)
GO

ALTER TABLE dbo.VehicleSalesHistoryAudit
	ALTER COLUMN Model varchar(50)
GO
