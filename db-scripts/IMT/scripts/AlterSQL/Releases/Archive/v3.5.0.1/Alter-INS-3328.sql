/*
INS-3328
alter-plan-dmm.sql use to add text into the DealerPreference table that is no longer needed.
alter-plan-dmm-drop.sql will drop the column.

*/
if exists (select * from dbo.syscolumns where id = object_id(N'DealerPreference') and name = 'AgingInvPlan')
ALTER TABLE dbo.DealerPreference DROP COLUMN AgingInvPlan
GO