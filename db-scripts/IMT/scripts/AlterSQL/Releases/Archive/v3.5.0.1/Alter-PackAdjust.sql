if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PackAdjustRules]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[PackAdjustRules]
GO

CREATE TABLE [dbo].[PackAdjustRules] (
	[PackAdjustRuleID] 	[int] identity(1,1) NOT NULL ,
	[BusinessUnitID] 	[int] NOT NULL ,
	[SaleDescription] 	[CHAR](1) NULL ,
	[VehicleSource] 	[CHAR](1) NULL ,
	[UnitCostThresholdLow] 	[int] NULL ,
	[UnitCostThresholdHigh]	[int] NULL ,
	[PackAmount] 		[decimal](9, 2) NULL 
) ON [DATA]
GO

ALTER TABLE [dbo].[PackAdjustRules] WITH NOCHECK ADD 
	CONSTRAINT [PK_PackAdjustRules] PRIMARY KEY  CLUSTERED 
	(
		[PackAdjustRuleID]
	)  ON [DATA] 
GO

INSERT
INTO	PackAdjustRules (BusinessUnitID, SaleDescription, VehicleSource, UnitCostThresholdLow, UnitCostThresholdHigh, PackAmount)
SELECT	100136, 'R', NULL, NULL, NULL, 500
UNION
SELECT	100138, 'R', NULL, NULL, NULL, 500
UNION
SELECT	100141, 'R', NULL, NULL, NULL, 500
UNION
SELECT	100148, 'R', NULL, NULL, NULL, 1000
UNION
SELECT	100239, 'R', NULL, NULL, NULL, 350
UNION
SELECT	100246, 'R', NULL, NULL, NULL, 450
UNION
SELECT	100262, 'R', NULL, NULL, NULL, 300
UNION
SELECT	100262, 'W', NULL, NULL, NULL, 300
UNION
SELECT	100347, 'R', NULL, NULL, NULL, 110

-- UNION
-- SELECT	100147, NULL, 'P', NULL, NULL, 1000
-- UNION
-- SELECT	100147, NULL, 'T', NULL, 2000, 0
-- UNION
-- SELECT	100147, NULL, 'T', 2000, NULL, 500


----------------------------------------------------------------------------------------
--
--	THE COMMENTED-OUT RULES FOR 100147 ARE FOR THE "DYNAMIC" PACK
--	IT SEEMS THAT THEY ARE NOT ACTIVE, SO DON'T INSERT INTO THE TABLE
--	BUT LEAVE HERE AS A GUIDE FOR IMPLEMENTATION, IF NECESSARY
--
----------------------------------------------------------------------------------------

GO

----------------------------------------------------------------------------------------
--
--	THIS IS HERE TO AVOID BREAKING THE BUILD ON BASE DATA SETS W/O ALL BU'S 
--
----------------------------------------------------------------------------------------

DELETE	R
FROM	PackAdjustRules R
WHERE	NOT EXISTS (SELECT 1 FROM BusinessUnit BU WHERE R.BusinessUnitID = BU.BusinessUnitID)
GO

----------------------------------------------------------------------------------------
--
--	NOW IT'S OK TO CREATE THE PK
--
----------------------------------------------------------------------------------------

ALTER TABLE [dbo].[PackAdjustRules] ADD 
	CONSTRAINT [FK_PackAdjustRules_BusinessUnit] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	)
GO

/*

--	USE THIS FOR ALPHA

UPDATE	R
SET	BusinessUnitID = BU.BusinessUnitID
FROM	PackAdjustRules R
	JOIN (
		SELECT 100136 BusinessUnitID, 'GOODSONH01' BusinessUnitCode UNION
		SELECT 100138 BusinessUnitID, 'GOODSONH02' BusinessUnitCode UNION
		SELECT 100141 BusinessUnitID, 'SPRINGCJ01' BusinessUnitCode UNION
		SELECT 100148 BusinessUnitID, 'HENDRICK02' BusinessUnitCode UNION
		SELECT 100239 BusinessUnitID, 'RICKHEND01' BusinessUnitCode UNION
		SELECT 100246 BusinessUnitID, 'HENDRICK03' BusinessUnitCode UNION
		SELECT 100262 BusinessUnitID, 'GWINNETT02' BusinessUnitCode UNION
		SELECT 100347 BusinessUnitID, 'FORESTLA01' BusinessUnitCode 

		) P ON R.BusinessUnitID = P.BusinessUnitID

	JOIN BusinessUnit BU ON BU.BusinessUnitCode = P.BusinessUnitCode

*/
