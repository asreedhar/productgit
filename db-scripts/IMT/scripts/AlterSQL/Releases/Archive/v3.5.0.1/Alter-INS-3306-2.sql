UPDATE dbo.DealerPreference
SET ApplyPriorAgingNotes = 1
GO

ALTER TABLE dbo.DealerPreference
	ALTER COLUMN ApplyPriorAgingNotes bit NOT NULL
GO

ALTER TABLE dbo.DealerPreference
	ADD CONSTRAINT DF_DealerPreference_ApplyPriorAgingNotes
		DEFAULT (1) FOR ApplyPriorAgingNotes
GO