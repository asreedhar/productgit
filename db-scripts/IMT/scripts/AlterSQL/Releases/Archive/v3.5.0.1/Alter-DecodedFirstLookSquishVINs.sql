
IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns WHERE id = object_id(N'[dbo].[DecodedFirstlookSquishVINs]') and OBJECTPROPERTY(id, N'IsUserTable') = 1 and name = 'EdmundsBodyTypeCode' )
	ALTER TABLE DecodedFirstlookSquishVINs ADD EdmundsBodyTypeCode VARCHAR(6) NULL
GO
