IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('dbo.Inventory') and name='eStockCardLock')
ALTER TABLE dbo.Inventory add eStockCardLock tinyint not null  DEFAULT (0)
GO

UPDATE	Inventory
SET	eStockCardLock = 1
WHERE	BusinessUnitID = 100900		-- BOB MOORE INFINITY
	AND InventoryActive = 1
GO
	