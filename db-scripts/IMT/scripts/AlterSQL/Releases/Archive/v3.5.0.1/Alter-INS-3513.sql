CREATE TABLE [dbo].[BookOutTypes] (
	[BookOutTypeId] int NOT NULL,
	[BookOutDescription] varchar(100) NOT NULL
	CONSTRAINT [PK_BookOutTypes] PRIMARY KEY
	(
		[BookOutTypeId]
	)
)
GO

INSERT INTO [dbo].[BookOutTypes]
SELECT 1, 'Appraisal Bookout'
UNION SELECT 2, 'Manual Bookout'
UNION SELECT 3, 'FLUSAN Bookout'
UNION SELECT 4, 'Inventory Bookout'
GO

CREATE TABLE [dbo].[AuditBookOuts] (
	[AuditId] int IDENTITY(1,1) NOT NULL,
	[BusinessUnitId] int NOT NULL,
	[MemberId] int NOT NULL,
	[ProductId] int NOT NULL,
	[ReferenceId] varchar(25) NULL,
	[BookId] tinyint NOT NULL,
	[BookOutTimeStamp] smalldatetime NOT NULL,
	CONSTRAINT [PK_AuditBookOuts] PRIMARY KEY
	(
		[AuditId]
	),
	CONSTRAINT [FK_AuditBookOuts_BusinessUnitId] FOREIGN KEY
	(
		[BusinessUnitId]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitId]
	),
	CONSTRAINT [FK_AuditBookOuts_MemberId] FOREIGN KEY
	(
		[MemberId]
	) REFERENCES [dbo].[Member] (
		[MemberId]
	),
	CONSTRAINT [FK_AuditBookOuts_ProductId] FOREIGN KEY
	(
		[ProductId]
	) REFERENCES [dbo].[BookOutTypes] (
		[BookOutTypeId]
	),
	CONSTRAINT [FK_AuditBookOuts_BookId] FOREIGN KEY
	(
		[BookId]
	) REFERENCES [dbo].[lu_GuideBook] (
		[GuideBookId]
	)
)

ALTER TABLE [AuditBookOuts]
	ADD CONSTRAINT [DF_BookOutTimeStamp] DEFAULT getdate()
	FOR BookOutTimeStamp
GO