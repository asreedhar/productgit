
--------------------------------------------------------------------------------------------------------
-- ONLY ADD IF NOT THERE
-- THIS IS NECESSARY SINCE THE ALTER WAS ORIGINALLY IN AN "INSERT" SCRIPT RATHER THAN IN AN ALTER
--------------------------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns WHERE id = object_id(N'[dbo].[AuditBookOuts]') and OBJECTPROPERTY(id, N'IsUserTable') = 1 and name = 'BookoutID' )
	ALTER TABLE dbo.AuditBookOuts ADD BookoutID INT NOT NULL CONSTRAINT DF_DefaultBookoutID DEFAULT (-1)
GO
