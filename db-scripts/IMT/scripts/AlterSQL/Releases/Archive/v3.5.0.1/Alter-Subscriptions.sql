
CREATE TABLE dbo.SubscriptionFrequencyTypes
	(
	SubscriptionFrequencyID tinyint NOT NULL,
	Description varchar(50) NOT NULL
	)  ON DATA
GO
ALTER TABLE dbo.SubscriptionFrequencyTypes ADD CONSTRAINT
	PK_SubscriptionFrequencyTypes PRIMARY KEY CLUSTERED 
	(
	SubscriptionFrequencyID
	) ON DATA

GO


CREATE TABLE dbo.SubscriberTypes
	(
	SubscriberTypeID tinyint NOT NULL,
	Description varchar(50) NOT NULL
	)  ON DATA
GO
ALTER TABLE dbo.SubscriberTypes ADD CONSTRAINT
	PK_SubscriberTypes PRIMARY KEY CLUSTERED 
	(
	SubscriberTypeID
	) ON DATA

GO


CREATE TABLE dbo.SubscriptionDeliveryTypes
	(
	SubscriptionDeliveryTypeID tinyint NOT NULL,
	Description varchar(50) NOT NULL
	)  ON DATA
GO
ALTER TABLE dbo.SubscriptionDeliveryTypes ADD CONSTRAINT
	PK_SubscriptionDeliveryTypes PRIMARY KEY CLUSTERED 
	(
	SubscriptionDeliveryTypeID
	) ON DATA

GO


CREATE TABLE dbo.SubscriptionTypes
	(
	SubscriptionTypeID tinyint NOT NULL,
	Description varchar(50) NOT NULL
	)  ON DATA
GO
ALTER TABLE dbo.SubscriptionTypes ADD CONSTRAINT
	PK_SubscriptionTypes PRIMARY KEY CLUSTERED 
	(
	SubscriptionTypeID
	) ON DATA

GO


CREATE TABLE dbo.Subscriptions
	(
	SubscriptionID int IDENTITY(1,1) NOT NULL,
	SubscriberID int NOT NULL,
	SubscriberTypeID tinyint NOT NULL,
	SubscriptionFrequencyID tinyint NOT NULL,
	SubscriptionTypeID tinyint NOT NULL,
	SubscriptionDeliveryTypeID tinyint NOT NULL
	)  ON DATA
GO
ALTER TABLE dbo.Subscriptions ADD CONSTRAINT
	PK_Subscriptions PRIMARY KEY CLUSTERED 
	(
	SubscriptionID
	) ON DATA

GO
ALTER TABLE dbo.Subscriptions ADD CONSTRAINT
	FK_Subscriptions_SubscriberTypes FOREIGN KEY
	(
	SubscriberTypeID
	) REFERENCES dbo.SubscriberTypes
	(
	SubscriberTypeID
	)
GO
ALTER TABLE dbo.Subscriptions ADD CONSTRAINT
	FK_Subscriptions_SubscriptionFrequencyTypes FOREIGN KEY
	(
	SubscriptionFrequencyID
	) REFERENCES dbo.SubscriptionFrequencyTypes
	(
	SubscriptionFrequencyID
	)
GO
ALTER TABLE dbo.Subscriptions ADD CONSTRAINT
	FK_Subscriptions_SubscriptionTypes FOREIGN KEY
	(
	SubscriptionTypeID
	) REFERENCES dbo.SubscriptionTypes
	(
	SubscriptionTypeID
	)
GO
ALTER TABLE dbo.Subscriptions ADD CONSTRAINT
	FK_Subscriptions_SubscriptionDeliveryTypes FOREIGN KEY
	(
	SubscriptionDeliveryTypeID
	) REFERENCES dbo.SubscriptionDeliveryTypes
	(
	SubscriptionDeliveryTypeID
	)
GO

INSERT
INTO	SubscriptionFrequencyTypes
SELECT	1, 'On-Demand'
UNION
SELECT	2, 'Daily'
UNION
SELECT	3, 'Business Day'
UNION
SELECT	4, 'Monthly'
UNION
SELECT	5, 'Quarterly'

GO

INSERT
INTO	SubscriberTypes
SELECT	1, 'Member'
UNION
SELECT	2, 'Dealer'
UNION
SELECT	3, 'Dealer Group'

GO

INSERT
INTO	SubscriptionTypes
SELECT	1, 'Buying Alert'
UNION
SELECT	2, 'Trade-In and Appraisal Update'
UNION
SELECT	3, 'Aging Plan Update'
UNION
SELECT	4, 'Storms on the Horizon'

GO

INSERT
INTO	SubscriptionDeliveryTypes
SELECT	1, 'HTML E-Mail'
UNION
SELECT	2, 'Text E-Mail'
UNION
SELECT	3, 'SMS'
UNION
SELECT	4, 'Fax'

GO

--------------------------------------------------------------------------------
--	TEST MEMBER - ON-DEMAND BUYING ALERT VIA HTML MAIL
--------------------------------------------------------------------------------

INSERT
INTO	Subscriptions (SubscriberID, SubscriberTypeID, SubscriptionFrequencyID, SubscriptionTypeID, SubscriptionDeliveryTypeID)
SELECT	100685, 1, 1, 1, 1

--------------------------------------------------------------------------------
--	TEST DEALER - DAILY AGING PLAN UPDATE VIA FAX
--------------------------------------------------------------------------------

INSERT
INTO	Subscriptions (SubscriberID, SubscriberTypeID, SubscriptionFrequencyID, SubscriptionTypeID, SubscriptionDeliveryTypeID)
SELECT	100147, 2, 2, 3, 4

