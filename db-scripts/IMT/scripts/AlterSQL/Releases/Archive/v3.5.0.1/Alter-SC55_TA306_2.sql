
if exists (select * from dbo.sysobjects where id = object_id(N'[DF_DealerPreference_SearchAppraisalDaysBackThreshold]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
ALTER TABLE [dbo].[DealerPreference] DROP CONSTRAINT [DF_DealerPreference_SearchAppraisalDaysBackThreshold]
GO

if exists (select * from syscolumns where id=object_id('dbo.DealerPreference') and name='SearchAppraisalDaysBackThreshold')
ALTER TABLE dbo.DealerPreference
Drop column SearchAppraisalDaysBackThreshold
GO