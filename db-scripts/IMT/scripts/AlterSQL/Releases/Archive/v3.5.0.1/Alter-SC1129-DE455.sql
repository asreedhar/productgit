UPDATE 	MMG
SET 	DisplayBodyTypeID = 3,
	EdmundsBodyType = 'SDN'
FROM 	tbl_makemodelgrouping MMG
WHERE	MakeModelGroupingID = 101813
GO

UPDATE 	GD
SET 	GroupingDescription = 'CHEVROLET MALIBU MAXX SEDAN'
FROM 	tbl_GroupingDescription GD
WHERE 	GroupingDescriptionid = 101863
GO

UPDATE 	DSV
SET 	DisplayBodyTypeID = 3,
	EdmundsBodyTypeCode = 'SDN'
FROM 	dbo.DecodedSquishVINs DSV
WHERE 	model = 'Malibu Maxx' 
	and make = 'Chevrolet'
GO
