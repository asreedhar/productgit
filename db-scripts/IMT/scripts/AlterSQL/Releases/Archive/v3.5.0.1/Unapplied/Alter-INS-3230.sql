if exists (select * from dbo.sysobjects where id = object_id(N'[lu_CIAEngineStageBasisPeriod]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [lu_CIAEngineStageBasisPeriod]
GO

CREATE TABLE [dbo].[lu_CIAEngineStageBasisPeriod] (
	[CIAEngineStageBasisPeriodDescription] [varchar] (20) NOT NULL 
	CONSTRAINT [PK_lu_CIAEngineStageBasisPeriodId] PRIMARY KEY  CLUSTERED 
	(
		[CIAEngineStageBasisPeriodDescription]
	)
) ON [DATA]
GO

INSERT INTO dbo.lu_CIAEngineStageBasisPeriod
VALUES( 'TargetUnits' )
GO

INSERT INTO dbo.lu_CIAEngineStageBasisPeriod
VALUES( 'BucketAllocation' )
GO

INSERT INTO dbo.lu_CIAEngineStageBasisPeriod
VALUES( 'GDLightProc' )
GO