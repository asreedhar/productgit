CREATE TABLE [ReportCenterSessions] (
	[ReportCenterSessionId] int IDENTITY(1, 1) not null,
	[SessionId] varchar(42) not null,
	[rcSessionId] varchar(42) null,
	[BusinessUnitId] int not null,
	[MemberId] int not null,
	[AccessTime] smalldatetime not null CONSTRAINT [DF_ReportCenterSessions_RequestTime] DEFAULT (getdate()),
	[DoAuthentication] tinyint not null CONSTRAINT [DF_ReportCenterSessions_DoAuthenication] DEFAULT (1),
	CONSTRAINT [PK_ReportCenterSession] PRIMARY KEY
	(
		[ReportCenterSessionId]
	),
	CONSTRAINT [FK_ReportCenterSession_BusinessUnit] FOREIGN KEY 
	(
		[BusinessUnitId]
	) REFERENCES [BusinessUnit] (
		[BusinessUnitId]
	),
	CONSTRAINT [FK_ReportCenterSession_Member] FOREIGN KEY 
	(
		[MemberId]
	) REFERENCES [Member] (
		[MemberId]
	)
);
GO