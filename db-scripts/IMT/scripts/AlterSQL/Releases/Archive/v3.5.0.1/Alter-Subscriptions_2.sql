DELETE FROM Subscriptions
GO

DELETE FROM SubscriptionFrequencyTypes
GO

INSERT
INTO	SubscriptionFrequencyTypes
SELECT	1, 'No Alerts'
UNION
SELECT	2, 'Only from Dealer Plan'
UNION
SELECT	3, 'Always'

GO

--------------------------------------------------------------------------------
--	TEST MEMBER - ON-DEMAND BUYING ALERT VIA HTML MAIL
--------------------------------------------------------------------------------

INSERT
INTO	Subscriptions (SubscriberID, SubscriberTypeID, SubscriptionFrequencyID, SubscriptionTypeID, SubscriptionDeliveryTypeID)
SELECT	100685, 1, 1, 1, 1

--------------------------------------------------------------------------------
--	TEST DEALER - DAILY AGING PLAN UPDATE VIA FAX
--------------------------------------------------------------------------------

INSERT
INTO	Subscriptions (SubscriberID, SubscriberTypeID, SubscriptionFrequencyID, SubscriptionTypeID, SubscriptionDeliveryTypeID)
SELECT	100147, 2, 2, 3, 4

GO

IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns WHERE id = object_id(N'[dbo].[Member]') and OBJECTPROPERTY(id, N'IsUserTable') = 1 and name = 'sms_address' )
	ALTER TABLE Member ADD [sms_address] [varchar] (129) NULL
GO