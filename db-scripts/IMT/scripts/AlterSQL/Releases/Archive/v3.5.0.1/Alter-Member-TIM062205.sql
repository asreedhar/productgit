ALTER TABLE dbo.Member ADD
	DefaultDealerGroupID int NULL
GO
Update
            m
Set
            DefaultDealerGroupID = bur.parentID
from
            member m
            join MemberAccess ma
            on m.memberid = ma.memberid
            join BusinessUnitRelationship bur
            on ma.BusinessUnitID = bur.BusinessUnitID
GO