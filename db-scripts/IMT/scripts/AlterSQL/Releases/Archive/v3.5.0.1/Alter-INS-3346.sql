if not exists(select * from dbo.syscolumns where name = 'SalesHistoryDisplayTimePeriodId' )
ALTER TABLE dbo.tbl_CIAPreferences
    ADD SalesHistoryDisplayTimePeriodId INT NOT NULL CONSTRAINT DF_SalesHistoryDisplayTimePeriodId DEFAULT (5)
GO
ALTER TABLE dbo.tbl_CIAPreferences
    ADD CONSTRAINT [FK_tbl_CIAPreferences_CIASalesHistoryDisplayTimePeriodId] FOREIGN KEY 
    (
        [SalesHistoryDisplayTimePeriodId]
    ) REFERENCES [tbl_CIACompositeTimePeriod] (
        [CIACompositeTimePeriodId]
    )
GO