IF NOT EXISTS (SELECT * FROM syscolumns WHERE id=object_id('dbo.VehiclePlanTrackingHeader') and name='LastModifiedDate')
	ALTER TABLE dbo.VehiclePlanTrackingHeader ADD LastModifiedDate smalldatetime NULL
GO

IF EXISTS (SELECT * FROM syscolumns WHERE id=object_id('dbo.VehiclePlanTrackingHeader') and name='LastModifiedDate')
        UPDATE dbo.VehiclePlanTrackingHeader SET LastModifiedDate = [Date]
GO