CREATE TABLE [dbo].[tbl_GroupingPromotionPlan] (
    [GroupingPromotionPlanId] [int] IDENTITY (1, 1) NOT NULL,
    [VehiclePlanTrackingHeaderId] [int] NOT NULL,
    [GroupingDescriptionId] [int] NOT NULL,
    [PromotionStartDate] [smalldatetime] NOT NULL,
    [PromotionEndDate] [smalldatetime] NOT NULL,
    [Notes] [varchar] (500) NULL,
    CONSTRAINT [PK_GroupingPromotionPlan] PRIMARY KEY
    (
        [GroupingPromotionPlanId]
    ),
    CONSTRAINT [FK_GroupingPromotionPlan_VehiclePlanTrackingHeader] FOREIGN KEY
    (
        [VehiclePlanTrackingHeaderId]
    ) REFERENCES [dbo].[VehiclePlanTrackingHeader] (
    	[VehiclePlanTrackingHeaderID]
    ),
    CONSTRAINT [FK_GroupingPromotionPlan_tbl_GroupingDescription] FOREIGN KEY
    (
        [GroupingDescriptionId]
    ) REFERENCES [dbo].[tbl_GroupingDescription] (
    	[GroupingDescriptionId]
    ),
)
GO