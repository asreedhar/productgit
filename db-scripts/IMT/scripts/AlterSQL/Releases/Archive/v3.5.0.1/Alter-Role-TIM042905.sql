UPDATE [dbo].[Role] SET [Code] = 'NOACCESS', [Name] = 'No Access' WHERE [RoleID] = 5;
GO

INSERT INTO [dbo].[Role] VALUES (7, 'A', 'NONE', 'None');
INSERT INTO [dbo].[Role] VALUES (8, 'A', 'FULL', 'Full');
GO