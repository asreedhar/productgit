
INSERT
INTO	 InventoryStatusCodes (ShortDescription,Description)
SELECT	'D', 'SOLD AT AUCTION PAPERWORK PENDING' 

UPDATE	map_MemberToInvStatusFilter
SET	InventoryStatusCD = 16	-- CERTIFIED VEHICLES
WHERE	InventoryStatusCD = 15	-- CPO

UPDATE	Inventory
SET	InventoryStatusCD = 16	-- CERTIFIED VEHICLES
WHERE	InventoryStatusCD = 15	-- CPO

DELETE
FROM	InventoryStatusCodes
WHERE	InventoryStatusCD = 15

UPDATE	ISC
SET	ISC.ShortDescription = U.ShortDescription,
	ISC.Description = U.Description

FROM	InventoryStatusCodes ISC
	JOIN (	SELECT	17 InventoryStatusCD, 'B' ShortDescription, 'AT AN AUCTION' Description UNION
		SELECT	12, 'F', 'IN FINANCE' UNION
		SELECT	13, 'G', 'SOLD' UNION
		SELECT	14, 'H', 'RTS OR OFFICE HOLD' UNION
		SELECT	16, 'M', 'CERTIFIED VEHICLES' UNION
		SELECT	20, 'R', 'READY FOR SALE - ON FRONT LINE' UNION
		SELECT	22, 'S', 'MANAGER REVIEW AND TRADES NOT CLEAR' UNION
		SELECT	21, 'T', 'IN SERVICE' UNION
		SELECT	23, 'W', 'WHOLESALE ON SITE' 
		) U ON ISC.InventoryStatusCD = U.InventoryStatusCD

GO