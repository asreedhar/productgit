if not exists (select * from dbo.syscolumns where id = object_id(N'DealerPreference') and name = 'PurchasingDistanceFromDealer')
ALTER TABLE [dbo].DealerPreference
ADD PurchasingDistanceFromDealer int NULL
GO

ALTER TABLE [dbo].DealerPreference
ADD CONSTRAINT
[DF_DealerPreference_PurchasingDistanceFromDealer]
DEFAULT (-1) FOR [PurchasingDistanceFromDealer]
GO

Update dbo.DealerPreference set PurchasingDistanceFromDealer = -1
GO

ALTER TABLE [dbo].DealerPreference
ALTER COLUMN PurchasingDistanceFromDealer int NOT NULL
GO




