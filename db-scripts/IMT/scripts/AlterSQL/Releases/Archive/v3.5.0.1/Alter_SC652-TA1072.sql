SET IDENTITY_INSERT ThirdPartyVehicleOptionValueTypes ON

--NADA options have different values, same as KBB.
INSERT INTO [dbo].[ThirdPartyVehicleOptionValueTypes]([ThirdPartyOptionValueTypeID], [OptionValueTypeName])
VALUES( 3, 'Loan')
GO

INSERT INTO [dbo].[ThirdPartyVehicleOptionValueTypes]([ThirdPartyOptionValueTypeID], [OptionValueTypeName])
VALUES( 4, 'Trade-In')
GO

SET IDENTITY_INSERT ThirdPartyVehicleOptionValueTypes OFF