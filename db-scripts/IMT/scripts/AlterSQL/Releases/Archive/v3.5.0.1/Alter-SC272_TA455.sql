/*

This script will:
 - Drop index Member.IX_Member_Login, if it exists
 - Add unique constraint Member.UQ_Member__Login, if it does not exist

This script will fail if the contents of Member.Login is not unique. Any duplicates will have
to be fixed manually before this script can run succesfully.


--  Use this to identify duplicate logins on a given IMT instance
SELECT me.login, me.memberid
 from member me
  inner join (
        select login, count(*) howMany
         from member
         group by login
         having count(*) > 1
    ) foo
   on foo.login = me.login
 order by me.login, me.memberid

*/


IF indexproperty(object_id('member'), 'ix_member__login', 'indexid') is not null
    --  Index exists, drop it
    DROP INDEX Member.IX_Member__Login


IF object_id('UQ_Member__Login') is null
    --  Constraint does not exist, create it
    ALTER TABLE Member
     add constraint UQ_Member__Login
      unique nonclustered (Login)
       with fillfactor = 95

GO
