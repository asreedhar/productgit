if not exists (select * from dbo.syscolumns where id = object_id(N'DealerPreference') and name = 'CalculateAverageBookValue')
alter table dbo.DealerPreference add CalculateAverageBookValue tinyint not null default(1)
go