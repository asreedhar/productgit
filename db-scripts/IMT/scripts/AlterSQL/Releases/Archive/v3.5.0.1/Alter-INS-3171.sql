-------------------------------------------------
-- tbl_GroupingDescription.SuppressMarketPerformer 
-------------------------------------------------
if exists (select * from dbo.sysobjects where id = object_id(N'[DF__tbl_Group__Suppr__2215F810]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
ALTER TABLE [dbo].[tbl_GroupingDescription] DROP CONSTRAINT [DF__tbl_Group__Suppr__2215F810]
GO
alter table tbl_GroupingDescription
alter column SuppressMarketPerformer int not null
go
alter table tbl_GroupingDescription
add constraint DF_tbl_GroupingDescription_SuppressMarketPerformer default (0) for SuppressMarketPerformer
go
-------------------------------------------------
-- dealerpreference.UnitCostUpdateOnSale
-------------------------------------------------
if exists (select * from dbo.sysobjects where id = object_id(N'[DF_DealerPreference_UnitCostUpdateOnSale]') and OBJECTPROPERTY(id, N'IsConstraint') = 1)
ALTER TABLE [dbo].[DealerPreference] DROP CONSTRAINT [DF_DealerPreference_UnitCostUpdateOnSale]
GO
alter table dealerpreference
alter column UnitCostUpdateOnSale int not null
go
alter table dealerpreference
add constraint DF_DealerPreference_UnitCostUpdateOnSale default (0) for UnitCostUpdateOnSale
go
-------------------------------------------------
-- dealertarget.active
-------------------------------------------------
alter table DealerTarget
alter column active int not null
go
-------------------------------------------------
-- tbl_CIAPlan.avoid
-------------------------------------------------
alter table tbl_CIAPlan
alter column Avoid int not null
go
-------------------------------------------------
-- tbl_CIAPlan.prefer
-------------------------------------------------
alter table tbl_CIAPlan
alter column Prefer int not null
go
-------------------------------------------------
-- tbl_CIAUnitCostPointPlan.avoid
-------------------------------------------------
alter table tbl_CIAUnitCostPointPlan
alter column Avoid int not null
go
