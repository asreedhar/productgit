/*

Routine to drop table VehicleBodyStyleMapping, while retaining relational integrity between
tbl_Vehicle and VehicleBodyStyle.  The two columns in VehicleBodyStyle will be renamed.

 - Assumes that no data in any of the underlying tables might be modified while
    this routine is running

There are three procedures referencing this table for which there is no evidence
that they are actually being used.  These procedures will be dropped:
 - sp_Create_NEWPricePointBucket
 - getDealerRedistributionOpportunities
 - GetTradeAnalyzerReportBodyStyle

And we tossed this one in too, for good measure
 - PopulateMakeModelGroupingDescriptionTablesFromFirstLookSquishVINs


Nov 1, 2005   PKelley

*/


--  Drop the suspect stored procedures
IF objectproperty(object_id('sp_Create_NEWPricePointBucket'), 'isProcedure') = 1
    DROP PROCEDURE sp_Create_NEWPricePointBucket
IF objectproperty(object_id('getDealerRedistributionOpportunities'), 'isProcedure') = 1
    DROP PROCEDURE getDealerRedistributionOpportunities
IF objectproperty(object_id('GetTradeAnalyzerReportBodyStyle'), 'isProcedure') = 1
    DROP PROCEDURE GetTradeAnalyzerReportBodyStyle

IF objectproperty(object_id('PopulateMakeModelGroupingDescriptionTablesFromFirstLookSquishVINs'), 'isProcedure') = 1
    DROP PROCEDURE PopulateMakeModelGroupingDescriptionTablesFromFirstLookSquishVINs


--  Drop old constraints
ALTER TABLE tbl_Vehicle
 drop constraint FK_tbl_Vehicle_VehicleBodyStyleMapping

ALTER TABLE VehicleBodyStyleMapping
 drop constraint FK_VehicleBodyStyleMapping_VehicleBodyStyle_StandardBodyStyleID


-- SELECT vbs.StandardBodyStyleID, count(*)
--  from tbl_Vehicle ve
--   inner join VehicleBodyStyleMapping vbsm
--    on vbsm.VehicleBodyStyleMappingID = ve.VehicleBodyStyleID
--   inner join VehicleBodyStyle vbs
--    on vbs.StandardBodyStyleID = vbsm.StandardBodyStyleID
--  group by vbs.StandardBodyStyleID
--  order by vbs.StandardBodyStyleID
-- 
-- begin transaction


--  Update column with proper data
UPDATE tbl_Vehicle
 set VehicleBodyStyleID = vbs.StandardBodyStyleID
 from tbl_Vehicle ve
  inner join VehicleBodyStyleMapping vbsm
   on vbsm.VehicleBodyStyleMappingID = ve.VehicleBodyStyleID
  inner join VehicleBodyStyle vbs
   on vbs.StandardBodyStyleID = vbsm.StandardBodyStyleID

-- SELECT VehicleBodyStyleID, count(*)
--  from tbl_Vehicle
--  group by VehicleBodyStyleID
--  order by VehicleBodyStyleID
-- 
-- rollback

GO
--  Reset the column data type in tbl_Vehicle
ALTER TABLE tbl_Vehicle
 alter column VehicleBodyStyleID  tinyint  not null


--  Revise names in VehicleBodyStyle
ALTER TABLE VehicleBodyStyle
 drop constraint UQ_VehicleBodyStyle_VehicleBodyStyle

ALTER TABLE VehicleBodyStyle
 drop constraint PK_VehicleBodyStyle

EXECUTE sp_rename 'VehicleBodyStyle.StandardBodyStyleID', 'VehicleBodyStyleID', 'column'
EXECUTE sp_rename 'VehicleBodyStyle.VehicleBodyStyle', 'Description', 'column'

ALTER TABLE VehicleBodyStyle
 add constraint PK_VehicleBodyStyle
  primary key clustered (VehicleBodyStyleID)

ALTER TABLE VehicleBodyStyle
 with check add constraint UQ_VehicleBodyStyle__Description
  unique nonclustered (Description)


--  Create new constraint
ALTER TABLE tbl_Vehicle
 with check add constraint FK_tbl_Vehicle__VehicleBodyStyle
  foreign key (VehicleBodyStyleID) references VehicleBodyStyle (VehicleBodyStyleID)


--  Drop old table
DROP TABLE  VehicleBodyStyleMapping
GO
