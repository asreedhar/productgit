INSERT
INTO	Manufacturer (ManufacturerID, Name, ShortName)
SELECT	2, 'TOYOTA','TOYOTA'
GO

UPDATE	Franchise
SET	ManufacturerID = 2
WHERE	FranchiseDescription IN ('TOYOTA','LEXUS','SCION')
GO