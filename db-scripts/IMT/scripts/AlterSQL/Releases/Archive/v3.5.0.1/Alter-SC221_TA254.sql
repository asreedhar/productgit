update dbo.Appraisal
set KelleyPublishInfo = SUBSTRING( GuideBookFooter, 0, 5 ) + KelleyPublishInfo
where guidebookid = 3
and SUBSTRING( GuideBookFooter, 0, 5 ) != 'All '

update dbo.Appraisal
set GuideBookFooter = SUBSTRING( GuideBookFooter, 5, 78 )
where guidebookid = 3
and SUBSTRING( GuideBookFooter, 0, 5 ) != 'All '