SET ANSI_NULLS ON
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Appraisals]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Appraisals]
GO

CREATE TABLE [dbo].[Appraisals] (
	[AppraisalID] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[VehicleID] [int] NOT NULL ,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_Appraisals_DateCreated] DEFAULT (getdate()),
	[MemberID] [int] NOT NULL ,
	[DealCompleted] [tinyint] NULL ,
	[Sold] [tinyint] NULL ,
	[Color] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VehicleGuideBookID] [int] NULL ,
	CONSTRAINT [PK_Appraisals] PRIMARY KEY  CLUSTERED 
	(
		[AppraisalID]
	)  ON [DATA] ,
	CONSTRAINT [UK_Appraisals] UNIQUE  NONCLUSTERED 
	(
		[BusinessUnitID],
		[VehicleID],
		[DateCreated]
	)  ON [DATA] ,
	CONSTRAINT [FK_Appraisals_BusinessUnit] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_Appraisals_tbl_Vehicle] FOREIGN KEY 
	(
		[VehicleID]
	) REFERENCES [tbl_Vehicle] (
		[VehicleID]
	)
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalActionTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AppraisalActionTypes]
GO

CREATE TABLE [dbo].[AppraisalActionTypes] (
	[AppraisalActionTypeID] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [PK_AppraisalActionTypes] PRIMARY KEY  CLUSTERED 
	(
		[AppraisalActionTypeID]
	)  ON [DATA] 
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalActions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AppraisalActions]
GO

CREATE TABLE [dbo].[AppraisalActions] (
	[AppraisalActionID] [int] IDENTITY (1, 1) NOT NULL ,
	[AppraisalID] [int] NOT NULL ,
	[AppraisalActionTypeID] [tinyint] NOT NULL ,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_AppraisalActions_DateCreated] DEFAULT (getdate()),
	[WholesalePrice] [decimal](12, 2) NULL ,
	[EstimatedReconditioningCost] [decimal](12, 2) NULL ,
	[ConditionDescription] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	CONSTRAINT [PK_AppraisalActions] PRIMARY KEY  CLUSTERED 
	(
		[AppraisalActionID]
	)  ON [DATA] ,
	CONSTRAINT [FK_AppraisalActions_AppraisalActionTypes] FOREIGN KEY 
	(
		[AppraisalActionTypeID]
	) REFERENCES [AppraisalActionTypes] (
		[AppraisalActionTypeID]
	),
	CONSTRAINT [FK_AppraisalActions_Appraisals] FOREIGN KEY 
	(
		[AppraisalID]
	) REFERENCES [Appraisals] (
		[AppraisalID]
	)
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalValues]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AppraisalValues]
GO

CREATE TABLE [dbo].[AppraisalValues] (
	[AppraisalValueID] [int] IDENTITY (1, 1) NOT NULL ,
	[AppraisalID] [int] NULL ,
	[SequenceNumber] [int] NOT NULL CONSTRAINT [DF_AppraisalValues_SequenceNumber] DEFAULT (1) ,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_AppraisalValues_DateCreated] DEFAULT (getdate()) ,
	[Value] [decimal](12, 2) NOT NULL CONSTRAINT [DF_AppraisalValues_Value] DEFAULT (0.0) ,
	[Initials] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	CONSTRAINT [PK_AppraisalValues] PRIMARY KEY  CLUSTERED 
	(
		[AppraisalValueID]
	)  ON [DATA] ,
	CONSTRAINT [UK_AppraisalValues] UNIQUE  NONCLUSTERED 
	(
		[AppraisalID],
		[SequenceNumber]
	)  ON [DATA] ,
	CONSTRAINT [FK_AppraisalValues_Appraisals] FOREIGN KEY 
	(
		[AppraisalID]
	) REFERENCES [Appraisals] (
		[AppraisalID]
	)
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalBusinessUnitGroupDemand]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AppraisalBusinessUnitGroupDemand]
GO

CREATE TABLE [dbo].[AppraisalBusinessUnitGroupDemand] (
	[AppraisalBusinessUnitGroupDemandID] [int] IDENTITY (1, 1) NOT NULL ,
	[AppraisalID] [int] NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	CONSTRAINT [PK_AppraisalBusinessUnitGroupDemand] PRIMARY KEY  NONCLUSTERED 
	(
		[AppraisalBusinessUnitGroupDemandID]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [FK_AppraisalBusinessUnitGroupDemand_BusinessUnit] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_AppraisalBusinessUnitGroupDemand_Appraisals] FOREIGN KEY 
	(
		[AppraisalID]
	) REFERENCES [Appraisals] (
		[AppraisalID]
	)
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ThirdParties]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ThirdParties]
GO

CREATE TABLE [dbo].[ThirdParties] (
	[ThirdPartyID] [tinyint] NOT NULL ,
	[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Name] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DefaultFooter] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	CONSTRAINT [PK_ThirdParties] PRIMARY KEY  NONCLUSTERED 
	(
		[ThirdPartyID]
	) WITH  FILLFACTOR = 80  ON [IDX] 
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ThirdPartyCategories]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ThirdPartyCategories]
GO

CREATE TABLE [dbo].[ThirdPartyCategories] (
	[ThirdPartyCategoryID] [int] IDENTITY (1, 1) NOT NULL ,
	[ThirdPartyID] [tinyint] NOT NULL ,
	[Category] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [PK_ThirdPartyCategories] PRIMARY KEY  CLUSTERED 
	(
		[ThirdPartyCategoryID]
	)  ON [DATA] ,
	CONSTRAINT [UC_ThirdPartyCategories] UNIQUE  NONCLUSTERED 
	(
		[ThirdPartyID],
		[Category]
	)  ON [DATA] ,
	CONSTRAINT [FK_ThirdPartyCategories_ThirdParties] FOREIGN KEY 
	(
		[ThirdPartyID]
	) REFERENCES [ThirdParties] (
		[ThirdPartyID]
	)
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ThirdPartyOptionTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ThirdPartyOptionTypes]
GO

CREATE TABLE [dbo].[ThirdPartyOptionTypes] (
	[ThirdPartyOptionTypeID] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[OptionTypeName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [PK_ThirdPartyVehicleOptionTypes_1] PRIMARY KEY  CLUSTERED 
	(
		[ThirdPartyOptionTypeID]
	)  ON [DATA] 
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ThirdPartyOptions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ThirdPartyOptions]
GO

CREATE TABLE [dbo].[ThirdPartyOptions] (
	[ThirdPartyOptionID] [int] IDENTITY (1, 1) NOT NULL ,
	[OptionName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[OptionKey] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ThirdPartyOptionTypeID] [tinyint] NOT NULL ,
	CONSTRAINT [PK_ThirdPartyOptions] PRIMARY KEY  CLUSTERED 
	(
		[ThirdPartyOptionID]
	)  ON [DATA] ,
	CONSTRAINT [FK_ThirdPartyOptions_ThirdPartyOptionTypes] FOREIGN KEY 
	(
		[ThirdPartyOptionTypeID]
	) REFERENCES [ThirdPartyOptionTypes] (
		[ThirdPartyOptionTypeID]
	)
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ThirdPartyVehicles]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ThirdPartyVehicles]
GO

CREATE TABLE [dbo].[ThirdPartyVehicles] (
	[ThirdPartyVehicleID] [int] IDENTITY (1, 1) NOT NULL ,
	[ThirdPartyID] [tinyint] NOT NULL ,
	[ThirdPartyVehicleCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Description] [varchar] (65) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MakeCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ModelCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Status] [tinyint] NOT NULL CONSTRAINT [DF_ThirdPartyVehicles_Status] DEFAULT (0),
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_ThirdPartyVehicles_DateCreated_1] DEFAULT (getdate()),
	[DateModified] [datetime] NULL ,
	[VehicleGuideBookID] [int] NULL ,
	CONSTRAINT [PK_ThirdPartyVehicles] PRIMARY KEY  CLUSTERED 
	(
		[ThirdPartyVehicleID]
	)  ON [DATA] ,
	CONSTRAINT [FK_VehicleThirdPartyVehicles_ThirdParties] FOREIGN KEY 
	(
		[ThirdPartyID]
	) REFERENCES [ThirdParties] (
		[ThirdPartyID]
	)
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ThirdPartyVehicleOptions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ThirdPartyVehicleOptions]
GO

CREATE TABLE [dbo].[ThirdPartyVehicleOptions] (
	[ThirdPartyVehicleOptionID] [int] IDENTITY (1, 1) NOT NULL ,
	[ThirdPartyVehicleID] [int] NOT NULL ,
	[ThirdPartyOptionID] [int] NOT NULL ,
	[IsStandardOption] [tinyint] NOT NULL CONSTRAINT [DF_ThirdPartyOptions_IsStandardOption] DEFAULT (0),
	[Status] [tinyint] NULL CONSTRAINT [DF_ThirdPartyVehicleOptions_Status] DEFAULT (0),
	[SortOrder] [tinyint] NULL CONSTRAINT [DF_ThirdPartyVehicleOptions_SortOrder] DEFAULT (0),
	[DateCreated] [datetime] NULL CONSTRAINT [DF_ThirdPartyVehicleOptions_DateCreated2] DEFAULT (getdate()),
	[DateModified] [datetime] NULL ,
	CONSTRAINT [PK_VehicleThirdPartyVehicleOptions] PRIMARY KEY  CLUSTERED 
	(
		[ThirdPartyVehicleOptionID]
	)  ON [DATA] ,
	CONSTRAINT [FK_ThirdPartyVehicleOptions_ThirdPartyOptions] FOREIGN KEY 
	(
		[ThirdPartyOptionID]
	) REFERENCES [ThirdPartyOptions] (
		[ThirdPartyOptionID]
	),
	CONSTRAINT [FK_VehicleThirdPartyVehicleOptions_VehicleThirdPartyVehicles] FOREIGN KEY 
	(
		[ThirdPartyVehicleID]
	) REFERENCES [ThirdPartyVehicles] (
		[ThirdPartyVehicleID]
	)
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ThirdPartyVehicleOptionValueTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ThirdPartyVehicleOptionValueTypes]
GO

CREATE TABLE [dbo].[ThirdPartyVehicleOptionValueTypes] (
	[ThirdPartyOptionValueTypeID] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[OptionValueTypeName] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	CONSTRAINT [PK_ThirdPartyVehicleOptionTypes] PRIMARY KEY  CLUSTERED 
	(
		[ThirdPartyOptionValueTypeID]
	)  ON [DATA] 
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ThirdPartyVehicleOptionValues]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ThirdPartyVehicleOptionValues]
GO

CREATE TABLE [dbo].[ThirdPartyVehicleOptionValues] (
	[ThirdPartyVehicleOptionValueID] [int] IDENTITY (1, 1) NOT NULL ,
	[ThirdPartyVehicleOptionID] [int] NOT NULL ,
	[ThirdPartyOptionValueTypeID] [tinyint] NULL CONSTRAINT [DF_ThirdPartyVehicleOptionValues_ThirdPartyOptionValueTypeID] DEFAULT (0),
	[Value] [int] NULL ,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_ThirdPartyVehicleOptionValues_DateCreated] DEFAULT (getdate()),
	[DateModified] [datetime] NULL ,
	CONSTRAINT [PK_VehicleThirdPartyVehicleOptionValues] PRIMARY KEY  CLUSTERED 
	(
		[ThirdPartyVehicleOptionValueID]
	)  ON [DATA] ,
	CONSTRAINT [FK_VehicleThirdPartyVehicleOptionValues_ThirdPartyVehicleOptionTypes] FOREIGN KEY 
	(
		[ThirdPartyOptionValueTypeID]
	) REFERENCES [ThirdPartyVehicleOptionValueTypes] (
		[ThirdPartyOptionValueTypeID]
	),
	CONSTRAINT [FK_VehicleThirdPartyVehicleOptionValues_VehicleThirdPartyVehicleOptions] FOREIGN KEY 
	(
		[ThirdPartyVehicleOptionID]
	) REFERENCES [ThirdPartyVehicleOptions] (
		[ThirdPartyVehicleOptionID]
	)
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BookoutSources]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[BookoutSources]
GO

CREATE TABLE [dbo].[BookoutSources] (
	[BookoutSourceID] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [PK_BookoutSources] PRIMARY KEY  CLUSTERED 
	(
		[BookoutSourceID]
	)  ON [DATA] 
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Bookouts]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Bookouts]
GO

CREATE TABLE [dbo].[Bookouts] (
	[BookoutID] [int] IDENTITY (1, 1) NOT NULL ,
	[BookoutSourceID] [tinyint] NOT NULL CONSTRAINT [DF_Bookouts_BookoutSourceID] DEFAULT (1),
	[ThirdPartyID] [tinyint] NOT NULL ,
	[DateCreated] [datetime] NULL CONSTRAINT [DF_Bookouts_DateCreated] DEFAULT (getdate()),
	[Region] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DatePublished] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BookPrice] [int] NULL ,
	[Weight] [int] NULL ,
	[MileageCostAdjustment] [int] NULL ,
	[MSRP] [int] NULL ,
	[Mileage] [int] NULL ,
	[IsAccurate] [tinyint] NOT NULL CONSTRAINT [DF_Bookouts_IsAccurate] DEFAULT (0),	
	[LegacyID] [int] NOT NULL ,
	CONSTRAINT [PK_Bookouts] PRIMARY KEY  CLUSTERED 
	(
		[BookoutID]
	)  ON [DATA] ,
	CONSTRAINT [FK_Bookouts_BookoutSources] FOREIGN KEY 
	(
		[BookoutSourceID]
	) REFERENCES [BookoutSources] (
		[BookoutSourceID]
	),
	CONSTRAINT [FK_Bookouts_ThirdParties] FOREIGN KEY 
	(
		[ThirdPartyID]
	) REFERENCES [ThirdParties] (
		[ThirdPartyID]
	)
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BookoutThirdPartyCategories]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[BookoutThirdPartyCategories]
GO

CREATE TABLE [dbo].[BookoutThirdPartyCategories] (
	[BookoutThirdPartyCategoryID] [int] IDENTITY (1, 1) NOT NULL ,
	[BookoutID] [int] NOT NULL ,
	[ThirdPartyCategoryID] [int] NOT NULL ,
	[IsValid] [tinyint] NOT NULL CONSTRAINT [DF_Bookouts_Valid] DEFAULT (1),	
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_BookoutThirdPartyCategories_DateCreated] DEFAULT (getdate()),
	[DateModified] [datetime] NULL ,
	[LegacyID]	[int] NULL ,
	CONSTRAINT [PK_BookoutThirdPartyCategories] PRIMARY KEY  CLUSTERED 
	(
		[BookoutThirdPartyCategoryID]
	)  ON [DATA] ,
	CONSTRAINT [FK_BookoutThirdPartyCategories_Bookouts] FOREIGN KEY 
	(
		[BookoutID]
	) REFERENCES [Bookouts] (
		[BookoutID]
	),
	CONSTRAINT [FK_BookoutValues_ThirdPartyCategories] FOREIGN KEY 
	(
		[ThirdPartyCategoryID]
	) REFERENCES [ThirdPartyCategories] (
		[ThirdPartyCategoryID]
	)
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BookoutValueTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[BookoutValueTypes]
GO

CREATE TABLE [dbo].[BookoutValueTypes] (
	[BookoutValueTypeID] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	CONSTRAINT [PK_BookoutValueTypes] PRIMARY KEY  CLUSTERED 
	(
		[BookoutValueTypeID]
	)  ON [DATA] 
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BookoutValues]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[BookoutValues]
GO

CREATE TABLE [dbo].[BookoutValues] (
	[BookoutValueID] [int] IDENTITY (1, 1) NOT NULL ,
	[BookoutThirdPartyCategoryID] [int] NOT NULL ,
	[BookoutValueTypeID] [tinyint] NOT NULL ,
	[Value] [int] NULL ,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_BookoutValues_DateCreated] DEFAULT (getdate()),
	[DateModified] [datetime] NULL ,
	CONSTRAINT [PK_BookoutValues] PRIMARY KEY  CLUSTERED 
	(
		[BookoutValueID]
	)  ON [DATA] ,
	CONSTRAINT [FK_BookoutValues_BookoutValueTypes] FOREIGN KEY 
	(
		[BookoutValueTypeID]
	) REFERENCES [BookoutValueTypes] (
		[BookoutValueTypeID]
	),
	CONSTRAINT [FK_BookoutValues_BookoutThirdPartyCategories] FOREIGN KEY 
	(
		[BookoutThirdPartyCategoryID]
	) REFERENCES [BookoutThirdPartyCategories] (
		[BookoutThirdPartyCategoryID]
	)
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalBookouts]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AppraisalBookouts]
GO

CREATE TABLE [dbo].[AppraisalBookouts] (
	[BookoutID] [int] NOT NULL ,
	[AppraisalID] [int] NOT NULL ,
	CONSTRAINT [PK_AppraisalBookouts] PRIMARY KEY  CLUSTERED 
	(
		[BookoutID]
	)  ON [DATA] ,
	CONSTRAINT [UK_AppraisalBookouts] UNIQUE  NONCLUSTERED 
	(
		[BookoutID],
		[AppraisalID]
	)  ON [DATA] ,
	CONSTRAINT [FK_AppraisalBookouts_Appraisals] FOREIGN KEY 
	(
		[AppraisalID]
	) REFERENCES [Appraisals] (
		[AppraisalID]
	),
	CONSTRAINT [FK_AppraisalBookouts_Bookouts] FOREIGN KEY 
	(
		[BookoutID]
	) REFERENCES [Bookouts] (
		[BookoutID]
	)
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalThirdPartyVehicles]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AppraisalThirdPartyVehicles]
GO

CREATE TABLE [dbo].[AppraisalThirdPartyVehicles] (
	[ThirdPartyVehicleID] [int] NOT NULL ,
	[AppraisalID] [int] NOT NULL ,
	CONSTRAINT [PK_AppraisalThirdPartyVehicles] PRIMARY KEY  CLUSTERED 
	(
		[ThirdPartyVehicleID]
	)  ON [DATA] ,
	CONSTRAINT [FK_AppraisalThirdPartyVehicles_Appraisals] FOREIGN KEY 
	(
		[AppraisalID]
	) REFERENCES [Appraisals] (
		[AppraisalID]
	),
	CONSTRAINT [FK_AppraisalThirdPartyVehicles_ThirdPartyVehicles] FOREIGN KEY 
	(
		[ThirdPartyVehicleID]
	) REFERENCES [ThirdPartyVehicles] (
		[ThirdPartyVehicleID]
	)
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventoryBookouts]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[InventoryBookouts]
GO

CREATE TABLE [dbo].[InventoryBookouts] (
	[BookoutID] [int] NOT NULL ,
	[InventoryID] [int] NOT NULL ,
	CONSTRAINT [PK_InventoryBookouts] PRIMARY KEY  CLUSTERED 
	(
		[BookoutID]
	)  ON [DATA] ,
	CONSTRAINT [FK_InventoryBookouts_Bookouts] FOREIGN KEY 
	(
		[BookoutID]
	) REFERENCES [Bookouts] (
		[BookoutID]
	),
	CONSTRAINT [FK_InventoryBookouts_Inventory] FOREIGN KEY 
	(
		[InventoryID]
	) REFERENCES [Inventory] (
		[InventoryID]
	)
) ON [DATA]
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventoryThirdPartyVehicles]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[InventoryThirdPartyVehicles]
GO

CREATE TABLE [dbo].[InventoryThirdPartyVehicles] (
	[ThirdPartyVehicleID] [int] NOT NULL ,
	[InventoryID] [int] NOT NULL ,
	CONSTRAINT [PK_InventoryThirdPartyVehicles] PRIMARY KEY  CLUSTERED 
	(
		[ThirdPartyVehicleID]
	)  ON [DATA] ,
	CONSTRAINT [FK_InventoryThirdPartyVehicles_Inventory1] FOREIGN KEY 
	(
		[InventoryID]
	) REFERENCES [Inventory] (
		[InventoryID]
	),
	CONSTRAINT [FK_InventoryThirdPartyVehicles_ThirdPartyVehicles] FOREIGN KEY 
	(
		[ThirdPartyVehicleID]
	) REFERENCES [ThirdPartyVehicles] (
		[ThirdPartyVehicleID]
	)
) ON [DATA]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalWindowStickers]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AppraisalWindowStickers]
GO

CREATE TABLE [dbo].[AppraisalWindowStickers] (
	[AppraisalWindowStickerID] [int] IDENTITY (1, 1) NOT NULL ,
	[AppraisalID] [int] NOT NULL ,
	[StockNumber] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SellingPrice] [int] NULL ,
	CONSTRAINT [PK_AppraisalWindowStickers] PRIMARY KEY  NONCLUSTERED 
	(
		[AppraisalWindowStickerID]
	) WITH  FILLFACTOR = 80  ON [IDX] ,
	CONSTRAINT [FK_AppraisalID_Appraisals] FOREIGN KEY 
	(
		[AppraisalID]
	) REFERENCES [Appraisals] (
		[AppraisalID]
	)
) ON [DATA]
GO

