/****** Object:  Table [dbo].[Photos]    Script Date: 03/27/2006 15:46:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PhotosVehicle](
	[PhotoID] [int] NOT NULL PRIMARY KEY,
	[VehicleID] [int] NOT NULL
)
GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[PhotosVehicle]  WITH CHECK ADD  CONSTRAINT [FK_PhotosVehicle_Photo] FOREIGN KEY([PhotoID])
REFERENCES [dbo].[Photos] ([PhotoID])
GO

ALTER TABLE [dbo].[PhotosVehicle]  WITH CHECK ADD  CONSTRAINT [FK_PhotosVehicle_Vehicle] FOREIGN KEY([VehicleID])
REFERENCES [dbo].[tbl_Vehicle] ([VehicleID])
GO

ALTER TABLE [dbo].[PhotosVehicle]
ADD [isPrimaryPhoto] [tinyint] NOT NULL CONSTRAINT [DF_PhotosVehicle_IsPrimary] DEFAULT (0)
GO