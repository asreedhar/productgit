SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Moves the wholesaleDays preference from DealerGroup to DealerPreference
-- Renames the column to 'tradeManagerDaysFilter'
-- Author: bfung
-- Rally card/task C1365/TA2302

--DECLARE @TranName VARCHAR(20)
--SELECT @TranName = 'moveWholesaleDaysPreference'
--
--BEGIN TRANSACTION @TranName
--	WITH MARK 'Moving WholesaleDays From DealerGroup to Dealer'
--GO
--add new column
ALTER TABLE dbo.DealerPreference
ADD TradeManagerDaysFilter int NOT NULL 
	CONSTRAINT DF_DealerGroupPreference_TradeManagerDaysFilter DEFAULT (0)
GO

-- copy preference to DealerPreference
-- Get set dealerPreferences for dealers for dealergroup
UPDATE dbo.DealerPreference
Set TradeManagerDaysFilter = 
dgp.WholesaleDays
from dbo.DealerGroupPreference dgp
join dbo.BusinessUnitRelationship bur on dgp.businessunitid = bur.parentid
join dbo.DealerPreference dp on bur.businessunitid = dp.businessunitid
GO

-- remove preference from DealerGroupPreference
ALTER TABLE dbo.DealerGroupPreference
DROP CONSTRAINT DF_DealerGroupPreference_WholesaleDays
GO

ALTER TABLE dbo.DealerGroupPreference
DROP COLUMN WholesaleDays
GO

--IF @@ERROR <> 0
--BEGIN
--	COMMIT TRANSACTION @TranName
--END
--ELSE
--BEGIN
--	ROLLBACK TRANSACTION @TranName
--	PRINT "An Error occured while moving WholesaleDays from DealerGroupPreference to TradeManagerDaysFilter in DealerPreference"
--END
--GO