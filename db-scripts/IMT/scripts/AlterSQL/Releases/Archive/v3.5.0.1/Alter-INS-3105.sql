print 'INS-3105 Alter Script'
go

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CarfaxAvailability]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.CarfaxAvailability
GO


CREATE TABLE [dbo].[CarfaxAvailability] (
	[UserName]  [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL ,
	[Vin]  [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL ,
	[Expires] [smalldatetime] NOT NULL,
	CONSTRAINT [PK_CarfaxAvailability] PRIMARY KEY  NONCLUSTERED 
	(
		[UserName],
		[Vin]
	) WITH  FILLFACTOR = 80  ON [IDX] 
) ON [DATA]
GO

