CREATE TABLE dbo.DealerPreference_JDPowerSettings (
	BusinessUnitID INT NOT NULL 
		CONSTRAINT [PK_DP_JDPowerSettings_BusinessUnitID] PRIMARY KEY
	, PowerRegionID INT NOT NULL
	--, DealerMarketAreaID INT NULL
	, CONSTRAINT [FK_DP_JDPowerSettings_BusinessUnitID] 
		FOREIGN KEY (BusinessUnitID)
		REFERENCES dbo.BusinessUnit(BusinessUnitID)
	, CONSTRAINT [FK_DP_JDPowerSettings_PowerRegionID]
		FOREIGN KEY (PowerRegionID)
		REFERENCES dbo.JDPower_PowerRegion(PowerRegionID)
--	, CONSTRAINT [FK_DP_JDPowerSettings_DealerMarketAreaID]
--		FOREIGN KEY (DealerMarketAreaID)
--		REFERENCES dbo.JDPower_DealerMarketArea(DealerMarketAreaID)
)