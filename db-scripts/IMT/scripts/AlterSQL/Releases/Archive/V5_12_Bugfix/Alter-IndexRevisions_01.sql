/*

Some defrag reviewing suggested making these index changes

*/

ALTER INDEX IX_Inventory_ListPriceHistory_InventoryID
 on Inventory_ListPriceHistory
 rebuild with (fillfactor = 95)

DROP INDEX tbl_VehicleSale.IX_tbl_VehicleSale__LastPolledDate

ALTER INDEX IX_tbl_VehicleSale__DealDate_EtAl
 on tbl_VehicleSale
 rebuild with (fillfactor = 95)
