/*

Add what looks like a darn useful index to CIAGroupingItemDetails

*/


CREATE nonclustered INDEX IX_CIAGroupingItemDetails__CIAGroupingItemID
 on CIAGroupingItemDetails (CIAGroupingItemID)
 with sort_in_tempdb
