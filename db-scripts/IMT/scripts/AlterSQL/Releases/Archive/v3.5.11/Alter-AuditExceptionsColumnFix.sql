/*

Change Audit_Exceptions.SourceTableID from char(10) to tinyint

*/

--  Drop primary key
ALTER TABLE Audit_Exceptions
 drop constraint PK_Audit_Exceptions
GO

--  Alter column
ALTER TABLE Audit_Exceptions
 alter column SourceTableID tinyint not null
GO

--  Rebuild primary key
ALTER TABLE Audit_Exceptions
 add constraint PK_Audit_Exceptions
  primary key clustered (Audit_ID, SourceTableID, ExceptionRuleID)
GO
