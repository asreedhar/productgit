--------------------------------------------------------------------------------------------
--
--	NEED TO AUGMENT THE BUILD SYSTEM WITH A WAY TO LOAD TABLES WHEN IN "ALTER" MODE.
--	CURRENTLY, WE CAN ONLY DO A BCP TO LOAD A TABLE IN A SCRATCH MODE.
--
--	SO WE WILL NEED TO LOAD THE MAPPING AND CODE TABLES MANUALLY, WITH SOMETHING LIKE:
--
--
--	C:\Projects-Branch\IMT\database\management\scripts\basedata>bcp IMT..PING_Code IN PING_Code.data -b10000 -SPRODDB01SQL -T -E
--	C:\Projects-Branch\IMT\database\management\scripts\basedata>bcp IMT..PING_GroupingCodeMapping IN PING_GroupingCodeMapping.data -b10000 -SPRODDB01SQL -T -E
--
--
--------------------------------------------------------------------------------------------


CREATE TABLE dbo.PING_GroupingCodeMapping (	
			CodeID INT, 
			GroupingDescription VARCHAR(50),
			Make		 VARCHAR(20),
			Model		 VARCHAR(50),
			SegmentID	 TINYINT,
			Line		 VARCHAR(40),
			FirstlookModel	VARCHAR(50),
			FirstlookLine	VARCHAR(50),
			FirstlookMake	VARCHAR(50)
			)