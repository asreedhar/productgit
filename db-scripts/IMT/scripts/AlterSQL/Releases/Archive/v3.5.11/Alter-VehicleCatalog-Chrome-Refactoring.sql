
--------------------------------------------------------------------------------------------
--	REPLACE THE WRETCHEDLY NAMED TABLE lu_DisplayBodyType WITH Segment
--	IT MIGHT PROVE TO BE TOO MUCH; SO, FIRST BUILD PARALLEL TO lu_DisplayBodyType...
--------------------------------------------------------------------------------------------


SET ANSI_NULLS ON
GO

CREATE TABLE [dbo].[Segment](
	[SegmentID] [int]  NOT NULL,
	[Segment] [varchar](50) NOT NULL,
	CONSTRAINT [PK_Segment] PRIMARY KEY NONCLUSTERED ([SegmentID] ASC)
) 

GO

INSERT
INTO	Segment (SegmentID, Segment)
SELECT	DisplayBodyTypeID, DisplayBodyType
FROM	lu_DisplayBodyType

GO

------------------------------------------------------------------------------------------------------------
--	FOR COMPATIBILITY WITH IE, RETAIN lu_VehicleSubSegment BUT RENAME IT TO CONFORM WITH CONVENTIONS
------------------------------------------------------------------------------------------------------------

EXEC SP_RENAME 'lu_VehicleSubSegment','VehicleSubSegment', 'OBJECT'
GO

ALTER TABLE MakeModelGrouping DROP CONSTRAINT FK_tbl_MakeModelGrouping_lu_DisplayBodyType

ALTER TABLE MakeModelGrouping ADD CONSTRAINT FK_MakeModelGrouping__Segment FOREIGN KEY (SegmentID) REFERENCES Segment(SegmentID)
GO

ALTER TABLE CIABodyTypeDetails DROP CONSTRAINT FK_CIABodyTypeDetail_DisplayBodyType

EXEC SP_RENAME 'CIABodyTypeDetails.DisplayBodyTypeID','SegmentID', 'COLUMN'
GO

ALTER TABLE CIABodyTypeDetails ADD CONSTRAINT FK_CIABodyTypeDetails__Segment FOREIGN KEY (SegmentID) REFERENCES Segment(SegmentID)
GO

----------------------------------------------------------------------------------------------------------------
--
--	VehicleBodyStyleID  
--
--	This column was a lookup into the VehicleBodyStyle table, which was solely derived from Edmunds.
--	We are replacing it with the BodyStyle attribute from the VehicleCatalog which will be exposed
--	through the Vehicle view.  Obviously, all code referring to the VehicleBodyStyleID will need to 
--	be updated.
--
----------------------------------------------------------------------------------------------------------------


ALTER TABLE tbl_Vehicle DROP CONSTRAINT FK_tbl_Vehicle__VehicleBodyStyle
DROP TABLE VehicleBodyStyle

ALTER TABLE tbl_Vehicle DROP CONSTRAINT DF_tbl_Vehicle_VehicleBodyStyleID
ALTER TABLE tbl_Vehicle DROP COLUMN VehicleBodyStyleID

ALTER TABLE tbl_Vehicle ADD VehicleCatalogID INT NULL
GO

/*	
----------------------------------------------------------------------------------------------------------------
--	KEEP ON TRUCKIN'
--	MMG CAN BE DERIVED FROM THE VC, AND WILL BE EXPOSED UP THROUGH THE VIEW.  WHAT ABOUT PERFORMANCE
--	LOSS DUE TO THE DROP OF IX_MakeModelGroupingID? HIT THE OLAP VEHICLE FACT TABLE/MATERIALIZED VIEW
--	FLDW..VEHICLE OR FLDW..VEHICLE_F (SOMEDAY)
--
--	REVISED ONE WEEK BEFORE RELEASE:
--	DEPRECATE THESE ANOTHER TIME, TOO MUCH OF A PERFORMANCE HIT
--
----------------------------------------------------------------------------------------------------------------

DROP INDEX tbl_Vehicle.IX_MakeModelGroupingID

ALTER TABLE tbl_Vehicle DROP CONSTRAINT FK_tbl_Vehicle_MakeModelGrouping

EXEC SP_RENAME 'tbl_Vehicle.CatalogKey','__CatalogKey', 'COLUMN'
GO
EXEC SP_RENAME 'tbl_Vehicle.MakeModelGroupingID','__MakeModelGroupingID', 'COLUMN'
GO

ALTER TABLE tbl_Vehicle ALTER COLUMN __MakeModelGroupingID INT NULL

go
-----------------------------------------------------------------------------
--	NOT JUST YET, MIGHT NEED FOR MAPPING
-----------------------------------------------------------------------------

-- ALTER TABLE tbl_Vehicle DROP COLUMN MakeModelGroupingID
-- ALTER TABLE tbl_Vehicle DROP COLUMN CatalogKey  
GO
*/
-----------------------------------------------------------------------------
--	ADD SOME AUDITING 
-----------------------------------------------------------------------------

ALTER TABLE MakeModelGrouping ADD DateCreated SMALLDATETIME CONSTRAINT DF_MakeModelGrouping__DateCreated DEFAULT(GETDATE())
GO
ALTER TABLE tbl_GroupingDescription ADD DateCreated SMALLDATETIME CONSTRAINT DF_GroupingDescription__DateCreated DEFAULT(GETDATE())
GO

-----------------------------------------------------------------------------
--	MISCELLANEOUS
-----------------------------------------------------------------------------

ALTER TABLE dbo.SearchCandidate DROP CONSTRAINT UQ_SearchCandidate

ALTER TABLE dbo.SearchCandidate ALTER COLUMN Make VARCHAR(20) NOT NULL
ALTER TABLE dbo.SearchCandidate ALTER COLUMN Line VARCHAR(40) NOT NULL

ALTER TABLE dbo.SearchCandidate ADD CONSTRAINT UQ_SearchCandidate UNIQUE NONCLUSTERED (SearchCandidateListID, Make, Line, SegmentID)

ALTER TABLE tbl_Vehicle ALTER COLUMN OriginalModel VARCHAR(50) NULL		-- Line gets stored here for Appraisals (???)

ALTER TABLE tbl_Vehicle ADD BodyTypeID INT NOT NULL CONSTRAINT DF_tbl_Vehicle__BodyTypeID DEFAULT (0)
GO


DROP TABLE dbo.lu_DisplayBodyType
GO
/*
------------------------------------------------------------------------------------------------
--	CREATE A VERTICAL PARTITION FOR tbl_Vehicle, STORING THE "ORIGINAL" DMS ATTRIBUTES
--	THESE ARE NOT REQ'D IN DAY-TO-DAY USAGE OF THE TABLE
------------------------------------------------------------------------------------------------

CREATE TABLE dbo.Vehicle_DMSAttributes (
		VehicleID		INT NOT NULL PRIMARY KEY CLUSTERED,
		OriginalMake		VARCHAR(20) NULL,
		OriginalModel		VARCHAR(25) NULL,
		OriginalYear		SMALLINT NULL,
		OriginalTrim		VARCHAR(50) NULL,
		OriginalBodyStyle	VARCHAR(50) NULL
		)
INSERT
INTO	dbo.Vehicle_DMSAttributes
SELECT	VehicleID, OriginalMake, OriginalModel, OriginalYear, OriginalTrim, OriginalBodyStyle
FROM	tbl_Vehicle

*/





/*

Should drop MMG, VehicleBodyStyleID and replace with FK to VehicleCatalog
Expose, if necessary, MMG and VehicleBodyStyleID in the view Vehicle

ALTER TABLE tbl_Vehicle DROP CatalogKey
ALTER TABLE tbl_Vehicle DROP MakeModelGroupingID

*/
/*
--------------------------------------------------------------------------------------------
--	REPLACE THE WRETCHEDLY NAMED TABLE Segment WITH Segment
--	IT MIGHT PROVE TO BE TOO MUCH; IF SO, BUILD PARALLEL TO Segment
--------------------------------------------------------------------------------------------

SP_RENAME 'dbo.Segment','Segment','OBJECT'
GO

SP_RENAME 'PK_DisplayBodyType','PK_Segment', 'OBJECT'
GO

SP_RENAME 'Segment.DisplayBodyTypeID','SegmentID', 'COLUMN'
GO
SP_RENAME 'Segment.DisplayBodyType','Segment', 'COLUMN'
GO

SP_RENAME 'FK_CIABodyTypeDetail_DisplayBodyType','FK_CIABodyTypeDetail_Segment', 'OBJECT'
GO
SP_RENAME 'FK_tbl_MakeModelGrouping_Segment','FK_MakeModelGrouping_Segment', 'OBJECT'
GO

*/



