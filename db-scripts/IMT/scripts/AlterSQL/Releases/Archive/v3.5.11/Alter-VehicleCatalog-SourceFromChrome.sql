
-- should rename this to VehicleCatalogSource


INSERT
INTO	VehicleAttributeSource
SELECT	3, 'Chrome'
GO

ALTER TABLE VehicleAttributeCatalog_11 DROP CONSTRAINT UQ_VehicleAttributeCatalog_11
GO

ALTER TABLE VehicleAttributeCatalog_11 ADD CONSTRAINT UQ_VehicleAttributeCatalog_11 UNIQUE NONCLUSTERED (SquishVIN, VINPattern)
GO 

ALTER TABLE VehicleAttributeCatalog_11 DROP COLUMN EdmundsBodyTypeCode
GO
ALTER TABLE VehicleAttributeCatalog_1M DROP COLUMN EdmundsBodyTypeCode
GO

DELETE FROM VehicleAttributeCatalog_11 WHERE VehicleAttributeSourceID = 1
DELETE FROM VehicleAttributeCatalog_1M WHERE VehicleAttributeSourceID = 1
GO

--------------------------------------------------------------------------------------------------------
--	PREVENT THE TRIGGER FROM MUCKIN' WITH THE BUILD; PROBABLY ONLY REQ'D FOR TEST BUILDS
--	IT WILL BE RECREATED LATER...
--------------------------------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TR_U_Vehicle]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].[TR_U_Vehicle]
GO

--------------------------------------------------------------------------------
--
--	REMOVE THE LEGACY MakeModelGrouping VIEW / tbl_MakeModelGrouping DUO BY
--	UPDATING THE TABLE (AS IT SHOULD HAVE BEEN DONE YEARS AGO) AND DROPPING
--	THE VIEW.  THIS WORKS AS WE ARE IN COMPLETE CONTROL OF THE LOADING
--	OF MMG AND CAN SIMPLY DO WHATEVER TRANSFORMATIONS THAT WERE DONE IN THE
--	VIEW WHEN LOADING THE TABLE.
--
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--	FIRST, GET RID OF THE EDMUNDS HANGOVER
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
--	DODGE RAM VAN
--------------------------------------------------------------------------------

DECLARE @Mapping TABLE (OldMakeModelGroupingID INT, NewMakeModelGroupingID INT)

INSERT
INTO	@Mapping
SELECT	102717, 101924		-- DODGE RAM VAN
UNION
SELECT	102718, 101925		-- DODGE RAM VAN
UNION
SELECT	102719, 102241		-- MAZDA B-SERIES TRUCK
UNION
SELECT	102720, 102242		-- MAZDA B-SERIES TRUCK

UPDATE	T
SET	T.MakeModelGroupingID = M.NewMakeModelGroupingID
FROM	tbl_Vehicle T
	JOIN @Mapping M ON T.MakeModelGroupingID = M.OldMakeModelGroupingID

UPDATE	T
SET	T.MakeModelGroupingID = M.NewMakeModelGroupingID
FROM	VehicleAttributeCatalog_11 T
	JOIN @Mapping M ON T.MakeModelGroupingID = M.OldMakeModelGroupingID
	
UPDATE	T
SET	T.MakeModelGroupingID = M.NewMakeModelGroupingID
FROM	VehicleAttributeCatalog_1M T
	JOIN @Mapping M ON T.MakeModelGroupingID = M.OldMakeModelGroupingID
	
UPDATE	T
SET	T.MakeModelGroupingID = M.NewMakeModelGroupingID
FROM	PING_GroupingCodes T
	JOIN @Mapping M ON T.MakeModelGroupingID = M.OldMakeModelGroupingID
	
DELETE
FROM	tbl_MakeModelGrouping
WHERE	MakeModelGroupingID IN (SELECT OldMakeModelGroupingID FROM @Mapping)

DELETE FROM @Mapping

INSERT
INTO	@Mapping
SELECT	MMG.MakeModelGroupingID	OldMakeModelGroupingID, 
	MID.MakeModelGroupingID NewMakeModelGroupingID 
FROM	tbl_MakeModelGrouping MMG
	JOIN (	SELECT	Make, Model, DisplayBodyTypeID, MIN(MakeModelGroupingID) MakeModelGroupingID
		FROM	tbl_MakeModelGrouping MMG
		GROUP
		BY	Make, Model, DisplayBodyTypeID
		HAVING	COUNT(*) > 1	
		) MID ON MMG.Make = MID.Make AND MMG.Model = MID.Model AND MMG.DisplayBodyTypeID = MID.DisplayBodyTypeID AND MMG.MakeModelGroupingID <> MID.MakeModelGroupingID

UPDATE	T
SET	T.MakeModelGroupingID = M.NewMakeModelGroupingID
FROM	tbl_Vehicle T
	JOIN @Mapping M ON T.MakeModelGroupingID = M.OldMakeModelGroupingID

UPDATE	T
SET	T.MakeModelGroupingID = M.NewMakeModelGroupingID
FROM	VehicleAttributeCatalog_11 T
	JOIN @Mapping M ON T.MakeModelGroupingID = M.OldMakeModelGroupingID
	
UPDATE	T
SET	T.MakeModelGroupingID = M.NewMakeModelGroupingID
FROM	VehicleAttributeCatalog_1M T
	JOIN @Mapping M ON T.MakeModelGroupingID = M.OldMakeModelGroupingID
	
UPDATE	T
SET	T.MakeModelGroupingID = M.NewMakeModelGroupingID
FROM	PING_GroupingCodes T
	JOIN @Mapping M ON T.MakeModelGroupingID = M.OldMakeModelGroupingID
	
DELETE
FROM	tbl_MakeModelGrouping
WHERE	MakeModelGroupingID IN (SELECT OldMakeModelGroupingID FROM @Mapping)

--------------------------------------------------------------------------------
--	NOW, ADD THE VIEW COLUMNS, DEFAULTS, INDEXES, ETC. TO THE TABLE
--------------------------------------------------------------------------------

IF EXISTS(SELECT 1 FROM sys.indexes WHERE NAME = 'IDX_c_MakeModelGrouping_GroupingMMG')
	DROP INDEX tbl_MakeModelGrouping.IDX_c_MakeModelGrouping_GroupingMMG
GO
IF EXISTS(SELECT 1 FROM sys.indexes WHERE NAME = 'IX_MakeModelGrouping__GroupingDescriptionID_MakeModelGroupingID')
	DROP INDEX tbl_MakeModelGrouping.IX_MakeModelGrouping__GroupingDescriptionID_MakeModelGroupingID
GO

ALTER TABLE dbo.tbl_MakeModelGrouping ADD Line VARCHAR (40) NULL
GO

ALTER TABLE dbo.tbl_MakeModelGrouping ALTER COLUMN Model VARCHAR (50) NULL
GO
UPDATE	tbl_MakeModelGrouping
SET	GroupingDescriptionID = 0
WHERE	GroupingDescriptionID IS NULL
GO
ALTER TABLE dbo.tbl_MakeModelGrouping ALTER COLUMN GroupingDescriptionID INT NOT NULL 
GO
ALTER TABLE dbo.tbl_MakeModelGrouping ADD CONSTRAINT DF_GroupingDescription DEFAULT(0) FOR GroupingDescriptionID
GO

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE NAME = '[IX_MakeModelGrouping__GroupingDescriptionID_MakeModelGroupingID]')
	CREATE CLUSTERED INDEX [IX_MakeModelGrouping__GroupingDescriptionID_MakeModelGroupingID] ON [dbo].[tbl_MakeModelGrouping] 
	(
		[GroupingDescriptionID] ASC,
		[MakeModelGroupingID] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
GO

--------------------------------------------------------------------------------
--	UPDATE THE COLUMNS
--------------------------------------------------------------------------------

UPDATE	tbl_MakeModelGrouping
SET	GroupingDescriptionID	= ISNULL(GroupingDescriptionID, 0),
	Make			= UPPER(Make),
	Model			= UPPER(Model) + CASE WHEN Model LIKE '% ' + DBT.DisplayBodyType OR MMG.Model = DBT.DisplayBodyType 
							THEN '' 
							ELSE ' ' + UPPER(DBT.DisplayBodyType)
						END,
--	LegacyBodyType		= UPPER(MMG.LegacyBodyType),
	Line 			= UPPER(Model)
FROM	tbl_MakeModelGrouping MMG
	JOIN lu_DisplayBodyType DBT ON DBT.DisplayBodyTypeID = MMG.DisplayBodyTypeID

GO

--------------------------------------------------------------------------------
--	DROP AND RENAME.  FINITO.
--------------------------------------------------------------------------------

IF EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS
      WHERE TABLE_NAME = 'MakeModelGrouping')
   DROP VIEW MakeModelGrouping
GO

SP_RENAME 'dbo.tbl_MakeModelGrouping','MakeModelGrouping','OBJECT'
GO

--------------------------------------------------------------------------------
--	FINAL STEPS, NOT SURE IF WE WILL GO THIS FAR YET...
--------------------------------------------------------------------------------

ALTER TABLE MakeModelGrouping DROP CONSTRAINT UQ_MakeModelBodyType
GO

ALTER TABLE dbo.MakeModelGrouping DROP COLUMN EdmundsBodyType
GO

SP_RENAME 'MakeModelGrouping.DisplayBodyTypeID','SegmentID','COLUMN'
GO

ALTER TABLE MakeModelGrouping ADD CONSTRAINT UQ_MakeModelGrouping UNIQUE (Make, Model, SegmentID)
GO

SP_RENAME 'VehicleAttributeCatalog_11.DisplayBodyTypeID','SegmentID', 'COLUMN'
GO

SP_RENAME 'VehicleAttributeCatalog_1M.DisplayBodyTypeID','SegmentID', 'COLUMN'
GO

SP_RENAME 'VehicleAttributeCatalog_11.Type','ExtendedBodyStyle', 'COLUMN'
GO

SP_RENAME 'VehicleAttributeCatalog_1M.Type','ExtendedBodyStyle', 'COLUMN'
GO

ALTER TABLE tbl_GroupingDescription ALTER COLUMN GroupingDescription VARCHAR(50) NOT NULL
GO
--------------------------------------------------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DecodedFirstLookSquishVINs]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[DecodedFirstLookSquishVINs]
GO

ALTER TABLE tbl_GroupingDescription ALTER COLUMN GroupingDescription VARCHAR(50) NOT NULL
go

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PopulateVehicleAttributeCatalog]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PopulateVehicleAttributeCatalog]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DecodedSquishVINs]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[DecodedSquishVINs]
GO

--------------------------+------------------------------------------------------------------------------------------

ALTER TABLE MakeModelGrouping ADD ModelID INT NULL
GO

UPDATE	MMG
SET	MMG.ModelID = COALESCE(MD.ModelID,0)
FROM	MakeModelGrouping MMG
	LEFT JOIN [VehicleCatalog].Firstlook.Make MK ON MMG.Make = MK.Make
	LEFT JOIN [VehicleCatalog].Firstlook.Line L ON MK.MakeID = L.MakeID AND MMG.Line = L.Line
	LEFT JOIN [VehicleCatalog].Firstlook.Segment S ON MMG.SegmentID = S.SegmentID 
	LEFT JOIN [VehicleCatalog].Firstlook.Model MD ON L.LineID = MD.LineID AND S.SegmentID = MD.SegmentID
							AND MMG.Model = UPPER(MD.Model + CASE WHEN ' ' + MD.Model + ' ' LIKE '% ' + S.Segment + ' %' THEN '' ELSE ' ' + S.Segment END)

WHERE	COALESCE(MMG.ModelID,0) <> COALESCE(MD.ModelID,0)
GO

UPDATE	MakeModelGrouping 
SET	ModelID = 0
WHERE	MakeModelGroupingID = 0
GO

CREATE INDEX IX_MakeModelGrouping__ModelID ON MakeModelGrouping(ModelID)
GO

--------------------------------------------------------------------------------------------------------------------
--	ADD VIN DECODE PREFERENCES
--	NOTE THAT FOR ALL DEALERS, THE PRIMARY WILL BE #1 (US) AND SECONDARY #2 (CA) 
--------------------------------------------------------------------------------------------------------------------

ALTER TABLE dbo.DealerPreferenceDataload ADD VINDecodeCountryCodePrimary TINYINT NOT NULL CONSTRAINT DF_DealerPreferenceDataload__VINDecodeCountryCodePrimary DEFAULT(1)
GO
ALTER TABLE dbo.DealerPreferenceDataload ADD VINDecodeCountryCodeSecondary TINYINT NOT NULL CONSTRAINT DF_DealerPreferenceDataload__VINDecodeCountryCodeSecondary DEFAULT(2)
GO

  
