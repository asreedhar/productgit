-- DashboardRowDisplay Default value should be 10
-- Null value causes problems in legacy secions of the app

UPDATE dbo.Member
SET DashboardRowDisplay = 10
WHERE DashboardRowDisplay IS NULL

ALTER TABLE dbo.Member ALTER COLUMN DashboardRowDisplay INT NOT NULL
ALTER TABLE dbo.Member ADD CONSTRAINT DF_Member_DashboardRowDisplay DEFAULT(10) FOR DashboardRowDisplay