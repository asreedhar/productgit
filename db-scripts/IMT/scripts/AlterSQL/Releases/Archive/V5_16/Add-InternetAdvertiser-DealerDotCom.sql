SET NOCOUNT on

DECLARE
  @NewID            int
 ,@NewDatafeedCode  varchar(20)
 ,@NewName          varchar(50)

SET @NewDatafeedCode = 'DealerDotCom'
  --  The datafeed code for the new advertiser

SET @NewName = 'DealerDotCom'
  --  The (display) name of the new advertiser

-------------------------------------------------------------------------------

INSERT ThirdPartyEntity (BusinessUnitID, ThirdPartyEntityTypeID, Name)
 values (100150, 4, @NewName)  --  "Firstlook" BusinessUnit entity; "Internet Advertiser" type

SET @NewID = scope_identity()

INSERT InternetAdvertiser_ThirdPartyEntity (InternetAdvertiserID, DatafeedCode)
 values (@NewID, @NewDatafeedCode)

GO
