/*

Template to configure a new Internet Advertiser within an IMT database


--  List current Internet Advertsiers
SELECT * from ThirdPartyEntityType

SELECT *
 from ThirdPartyEntity
 where ThirdPartyEntityTypeID = 4
 order by ThirdPartyEntityID

SELECT *
 from InternetAdvertiser_ThirdPartyEntity
 order by InternetAdvertiserID

*/

SET NOCOUNT on

DECLARE
  @NewID            int
 ,@NewDatafeedCode  varchar(20)
 ,@NewName          varchar(50)

SET @NewDatafeedCode = 'devVenture'
  --  The datafeed code for the new advertiser

SET @NewName = 'devVenture'
  --  The (display) name of the new advertiser

-------------------------------------------------------------------------------

INSERT ThirdPartyEntity (BusinessUnitID, ThirdPartyEntityTypeID, Name)
 values (100150, 4, @NewName)  --  "Firstlook" BusinessUnit entity; "Internet Advertiser" type

SET @NewID = scope_identity()

INSERT InternetAdvertiser_ThirdPartyEntity (InternetAdvertiserID, DatafeedCode)
 values (@NewID, @NewDatafeedCode)

GO
