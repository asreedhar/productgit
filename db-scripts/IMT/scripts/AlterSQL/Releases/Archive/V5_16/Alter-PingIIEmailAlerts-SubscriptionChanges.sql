--- nk 03/10/08
--- Major alter for PingII Subscription changes. 


-- add to 2 PingII alerts to subscription types - defaults to sending Sunday's
INSERT INTO dbo.SubscriptionTypes (SubscriptionTypeID, Description, Notes, UserSelectable, DefaultSubscriptionFrequencyDetailID, Active) VALUES (13, 'PingII Overpriced Alert', 'PingII Overpriced Alert', 1, 4,0);
INSERT INTO dbo.SubscriptionTypes (SubscriptionTypeID, Description, Notes, UserSelectable, DefaultSubscriptionFrequencyDetailID, Active) VALUES (14, 'PingII Underpriced Alert', 'PingII Underpriced Alert', 1, 4,0);

-- create new table: SubscriptionTypeSubscriptionFrequencyDetail to control which frequencies are possible for each subscriptionType
CREATE TABLE [dbo].[SubscriptionTypeSubscriptionFrequencyDetail](
	[SubscriptionTypeSubscriptionFrequencyDetailID] [tinyint] IDENTITY(1,1) NOT NULL,
	[SubscriptionTypeID] [tinyint] NOT NULL,
	[SubscriptionFrequencyDetailID] [tinyint] NOT NULL,
	CONSTRAINT [PK_SubscriptionTypeSubscriptionFrequencyDetail] PRIMARY KEY NONCLUSTERED 
	(
		[SubscriptionTypeSubscriptionFrequencyDetailID] ASC
	)
) ON [DATA] 
GO

ALTER TABLE [dbo].[SubscriptionTypeSubscriptionFrequencyDetail]  WITH CHECK ADD CONSTRAINT [FK_SubscriptionTypeSubscriptionFrequencyDetail__SubscriptionTypeID] FOREIGN KEY([SubscriptionTypeID])
REFERENCES [dbo].[SubscriptionTypes] ([SubscriptionTypeID])
GO

ALTER TABLE [dbo].[SubscriptionTypeSubscriptionFrequencyDetail]  WITH CHECK ADD CONSTRAINT [FK_SubscriptionTypeSubscriptionFrequencyDetail__SubscriptionFrequencyDetailID] FOREIGN KEY([SubscriptionFrequencyDetailID])
REFERENCES [dbo].[SubscriptionFrequencyDetail] ([SubscriptionFrequencyDetailID])
GO

---- Populate SubscriptionTypeSubscriptionFrequencyDetail with default values
INSERT INTO SubscriptionTypeSubscriptionFrequencyDetail (SubscriptionTypeID, SubscriptionFrequencyDetailID)
(
	select SubscriptionTypeID as SubscriptionTypeID, SubscriptionFrequencyDetailID as SubscriptionFrequencyDetailID
	FROM 
	(
		SELECT st.subscriptionTypeID as SubscriptionTypeID, del.SubscriptionFrequencyDetailID as SubscriptionFrequencyDetailID 
		FROM SubscriptionTypes st
		CROSS JOIN SubscriptionFrequencyDetail del
	) a1
	EXCEPT 
		(
		-- nk - JUST place holders till we can get an answer from PM (03/10/08)
		SELECT 1, 2 --- buying alerts cant be text email
		union SELECT 1, 1 --- buying alerts cant be HTML email
		)
)
GO


-- create new table: SubscriptionTypeSubscriptionDeliveryType to control which delivery types (i.e. html email, fax) are possible for each subscriptionType
CREATE TABLE [dbo].[SubscriptionTypeSubscriptionDeliveryType](
	[SubscriptionTypeSubscriptionDeliveryTypeID] [tinyint] IDENTITY(1,1) NOT NULL,
	[SubscriptionTypeID] [tinyint] NOT NULL,
	[SubscriptionDeliveryTypeID] [tinyint] NOT NULL,
	CONSTRAINT [PK_SubscriptionTypeSubscriptionDeliveryType] PRIMARY KEY NONCLUSTERED 
	(
		[SubscriptionTypeSubscriptionDeliveryTypeID] ASC
	)
) ON [DATA] 
GO

ALTER TABLE [dbo].[SubscriptionTypeSubscriptionDeliveryType]  WITH CHECK ADD CONSTRAINT [FK_SubscriptionTypeSubscriptionDeliveryType__SubscriptionTypeID] FOREIGN KEY([SubscriptionTypeID])
REFERENCES [dbo].[SubscriptionTypes] ([SubscriptionTypeID])
GO

ALTER TABLE [dbo].[SubscriptionTypeSubscriptionDeliveryType]  WITH CHECK ADD CONSTRAINT [FK_SubscriptionTypeSubscriptionDeliveryType__SubscriptionDeliveryTypeID] FOREIGN KEY([SubscriptionDeliveryTypeID])
REFERENCES [dbo].[SubscriptionDeliveryTypes] ([SubscriptionDeliveryTypeID])
GO

---- Populate SubscriptionTypeSubscriptionFrequencyDetail with default values
INSERT INTO SubscriptionTypeSubscriptionDeliveryType (SubscriptionTypeID, SubscriptionDeliveryTypeID)
(
	select SubscriptionTypeID as SubscriptionTypeID, SubscriptionDeliveryTypeID as SubscriptionDeliveryTypeID
	FROM 
	(
		SELECT st.subscriptionTypeID as SubscriptionTypeID, del.SubscriptionDeliveryTypeID as SubscriptionDeliveryTypeID 
		FROM SubscriptionTypes st
		CROSS JOIN SubscriptionDeliveryTypes del
	) a1
	EXCEPT 
		(
		-- nk - JUST place holders till we can get an answer from PM (03/10/08)
		SELECT 1, 2 --- buying alerts cant be text email
		union SELECT 1, 1 --- buying alerts cant be HTML email
		)
)
GO


-- create new table: PricingDealerPreferences. We are creating this new table 
-- and then populating it with the existing PingII preferences currently in DealerPreferences 
CREATE TABLE [dbo].[DealerPreference_Pricing](
		[BusinessUnitID] [int] NOT NULL,
		[PingII_DefaultSearchRadius] [int] NULL DEFAULT (NULL),
		[PingII_MaxSearchRadius] [int] NULL DEFAULT (NULL),
		[PingII_SupressSellerName] [tinyint] NULL DEFAULT (NULL),
		[PingII_ExcludeNoPriceFromCalc] [tinyint] NULL DEFAULT (NULL),
		[PingII_ExcludeLowPriceOutliersMultiplier] [decimal](4, 2) NULL DEFAULT (NULL),
		[PingII_ExcludeHighPriceOutliersMultiplier] [decimal](4, 2) NULL DEFAULT (NULL),
		CONSTRAINT [PK_DP_Pricing_BusinessUnitId] PRIMARY KEY CLUSTERED 
		(
			[BusinessUnitId] ASC
		)WITH (IGNORE_DUP_KEY = OFF) ON [DATA]
) ON [DATA]
GO
ALTER TABLE [dbo].[DealerPreference_Pricing]  WITH CHECK ADD  CONSTRAINT [FK_DP_Pricing_BusinessUnitID] FOREIGN KEY([BusinessUnitID])
REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO

-- populate new table dbo.DealerPreference_Pricing with current valid data in dbo.DealerPreference
INSERT INTO DealerPreference_Pricing (businessUnitId, PingII_DefaultSearchRadius, PingII_MaxSearchRadius, PingII_SupressSellerName, PingII_ExcludeNoPriceFromCalc, PingII_ExcludeLowPriceOutliersMultiplier, PingII_ExcludeHighPriceOutliersMultiplier)
(
	SELECT businessUnitId, PingII_SearchRadius, 250, PingII_SupressSellerName, PingII_ExcludeNoPriceFromCalc, PingII_ExcludeLowPriceOutliersMultiplier, PingII_ExcludeHighPriceOutliersMultiplier
	FROM dbo.DealerPreference 
	WHERE PingII_SearchRadius IS NOT NULL
)	
GO

ALTER TABLE DealerPreference DROP CONSTRAINT DF__DealerPre__PingI__60E9B536
ALTER TABLE DealerPreference DROP CONSTRAINT DF__DealerPre__PingI__61DDD96F
ALTER TABLE DealerPreference DROP CONSTRAINT DF__DealerPre__PingI__62D1FDA8
ALTER TABLE DealerPreference DROP CONSTRAINT DF__DealerPre__PingI__63C621E1
ALTER TABLE DealerPreference DROP CONSTRAINT DF__DealerPre__PingI__64BA461A

GO
-- now clean out the old dealerPreference columns


ALTER TABLE dbo.DealerPreference 
DROP COLUMN PingII_SearchRadius
GO


ALTER TABLE dbo.DealerPreference 
DROP COLUMN PingII_SupressSellerName
GO

ALTER TABLE dbo.DealerPreference 
DROP COLUMN PingII_ExcludeNoPriceFromCalc
GO

ALTER TABLE dbo.DealerPreference 
DROP COLUMN PingII_ExcludeLowPriceOutliersMultiplier
GO

ALTER TABLE dbo.DealerPreference 
DROP COLUMN PingII_ExcludeHighPriceOutliersMultiplier
GO


--- populate SubscriptionTypeDealerUpgrade with enties for new subscriptionTypes 13, 14 (ping under/over)
INSERT INTO SubscriptionTypeDealerUpgrade (SubscriptionTypeID, DealerUpgradeCD) 
( 
		SELECT 13, DealerUpgradeCD 
		FROM dbo.lu_DealerUpgrade
		WHERE DealerUpgradeDESC = 'PING II'
)
GO
INSERT INTO SubscriptionTypeDealerUpgrade (SubscriptionTypeID, DealerUpgradeCD) 
( 
		SELECT 14, DealerUpgradeCD 
		FROM dbo.lu_DealerUpgrade
		WHERE DealerUpgradeDESC = 'PING II'
)
GO

--- add two new pos. distances to the dist bucket table for standard search radius checks
INSERT INTO [Market].Pricing.DistanceBucket VALUES (9, 750)
INSERT INTO [Market].Pricing.DistanceBucket VALUES (10, 1000)
GO