if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetTargetDaysSupply]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetTargetDaysSupply]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_InventoryOverstocking]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_InventoryOverstocking]
GO
/*
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehiclePlanTracking]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[VehiclePlanTracking]
GO
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehiclePlanTrackingHeader]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[VehiclePlanTrackingHeader]
GO
*/