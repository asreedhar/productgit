-- add new column for FB14247 - note: we're keeping the other one for now even though the approach to storing the background image has changed.
-- 2011-02-22 - dh
ALTER TABLE WindowSticker.Template ADD
	BackgroundImage varchar(50) NULL
