/*

This is the clean up script associated with FB 14407.

The following comment was taken from an email, regarding a change to this proc:
db-scripts\IMT\scripts\storedProcs\Marketing\Marketing.EquipmentProviderList#Fetch.PRC

It could be difficult to verify this proc change without updating the identity values of 
the existing equipment lists that MaxSE has collected already.
Unless the car has been (re) booked out since the last time it was loaded in MaxSE, 
the modified proc will not be executed to get the standard equipment.  This is because 
the MaxSE Equipment code thinks it has the current list (because the identity value stored 
matches the latest and greatest identity value reported by the provider).  So, this clean-up
script will simply update the identity values of guidebook equipment lists 
(identityvalue = identityvalue * -1) that have been stored already, thereby forcing the MaxSE 
equipment code to re-pull the list from the equipment provider (using the modified proc).  
This will cause the newly found standard equipment to be merged into the old list.

2011-02-18 - dh

*/



UPDATE el
SET IdentityValue = IdentityValue * -1
FROM imt.Marketing.EquipmentList el
JOIN imt.Marketing.EquipmentProvider ep ON el.EquipmentProviderID = ep.EquipmentProviderID
WHERE ep.Name IN 
('NADA',
'GALVES',
'Kelley Blue Book')
AND IdentityValue > 0

