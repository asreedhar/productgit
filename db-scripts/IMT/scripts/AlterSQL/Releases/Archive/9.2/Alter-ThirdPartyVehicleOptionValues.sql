-- BUGZID: 18155

----------------------------------------------------------------------------
-- Copy over rest of the data into ThirdPartyVehicleOptionValues_Temp
----------------------------------------------------------------------------

DECLARE @maxTPVOVId INT
SELECT @maxTPVOVId=MaxTPVOVId FROM [dbo].[ThirdPartyVehicleOptionValues_Temp_CurrentTPVOId]

INSERT 
INTO [dbo].[ThirdPartyVehicleOptionValues_Temp] ([ThirdPartyVehicleOptionID],[ThirdPartyOptionValueTypeID],[Value])
SELECT   [ThirdPartyVehicleOptionID]
		,COALESCE([ThirdPartyOptionValueTypeID], 0)
		,CASE WHEN ABS(MAX([Value])) > ABS(MIN([Value])) THEN MAX([Value]) ELSE MIN([Value]) END AS [Value]
FROM [dbo].[ThirdPartyVehicleOptionValues]
WHERE [ThirdPartyVehicleOptionValueID] > @maxTPVOVId AND [ThirdPartyVehicleOptionValueID] < IDENT_CURRENT( 'dbo.ThirdPartyVehicleOptionValues' ) 
GROUP BY  [ThirdPartyVehicleOptionID],[ThirdPartyOptionValueTypeID]
	
GO

----------------------------------------------------------------------------
-- Retreived all the data now swap over tables Temp-->[Live]...[Live]-->Orig
----------------------------------------------------------------------------

GO
EXEC sp_rename @objname='dbo.FK_VehicleThirdPartyVehicleOptionValues_ThirdPartyVehicleOptionTypes', @newname='FK_VehicleThirdPartyVehicleOptionValues_Orig_ThirdPartyVehicleOptionTypes', @objtype='OBJECT'
GO
EXEC sp_rename @objname='dbo.FK_VehicleThirdPartyVehicleOptionValues_VehicleThirdPartyVehicleOptions', @newname='FK_VehicleThirdPartyVehicleOptionValues_Orig_VehicleThirdPartyVehicleOptions', @objtype='OBJECT'
GO
EXEC sp_rename @objname='dbo.DF_ThirdPartyVehicleOptionValues_ThirdPartyOptionValueTypeID', @newname='DF_ThirdPartyVehicleOptionValues_Orig_ThirdPartyOptionValueTypeID', @objtype='OBJECT'
GO
EXEC sp_rename @objname='dbo.PK_ThirdPartyVehicleOptionValues', @newname='PK_ThirdPartyVehicleOptionValues_Orig', @objtype='OBJECT'
GO
EXEC sp_rename @objname='dbo.ThirdPartyVehicleOptionValues.IX_ThirdPartyVehicleOptionValues__ThirdPartyVehicleOptionID', @newname='IX_ThirdPartyVehicleOptionValues_Orig__ThirdPartyVehicleOptionID', @objtype='INDEX'
GO
EXEC sp_rename @objname='dbo.ThirdPartyVehicleOptionValues', @newname='ThirdPartyVehicleOptionValues_Orig'
GO
EXEC sp_rename @objname='dbo.ThirdPartyVehicleOptionValues_Temp.IX_ThirdPartyVehicleOptionValues_Temp__ThirdPartyVehicleOptionID', @newname='IX_ThirdPartyVehicleOptionValues__ThirdPartyVehicleOptionID', @objtype='INDEX'
GO
EXEC sp_rename @objname='dbo.PK_ThirdPartyVehicleOptionValues_Temp', @newname='PK_ThirdPartyVehicleOptionValues', @objtype='OBJECT'
GO
EXEC sp_rename @objname='dbo.DF_ThirdPartyVehicleOptionValues_Temp_ThirdPartyOptionValueTypeID', @newname='DF_ThirdPartyVehicleOptionValues_ThirdPartyOptionValueTypeID', @objtype='OBJECT'
GO
EXEC sp_rename @objname='dbo.FK_VehicleThirdPartyVehicleOptionValues_Temp_VehicleThirdPartyVehicleOptions', @newname='FK_VehicleThirdPartyVehicleOptionValues_VehicleThirdPartyVehicleOptions', @objtype='OBJECT'
GO
EXEC sp_rename @objname='dbo.FK_VehicleThirdPartyVehicleOptionValues_Temp_ThirdPartyVehicleOptionTypes', @newname='FK_VehicleThirdPartyVehicleOptionValues_ThirdPartyVehicleOptionTypes', @objtype='OBJECT'
GO
EXEC sp_rename @objname='dbo.ThirdPartyVehicleOptionValues_Temp', @newname='ThirdPartyVehicleOptionValues'
GO

----------------------------------------------------------------------------
-- For rollback
----------------------------------------------------------------------------
--
--
--
--Reverse Temp ([Live]-->Temp):
--
--GO
--EXEC sp_rename @objname='dbo.ThirdPartyVehicleOptionValues.IX_ThirdPartyVehicleOptionValues__ThirdPartyVehicleOptionID', @newname='IX_ThirdPartyVehicleOptionValues_Temp__ThirdPartyVehicleOptionID', @objtype='INDEX'
--GO
--EXEC sp_rename @objname='dbo.PK_ThirdPartyVehicleOptionValues', @newname='PK_ThirdPartyVehicleOptionValues_Temp', @objtype='OBJECT'
--GO
--EXEC sp_rename @objname='dbo.DF_ThirdPartyVehicleOptionValues_ThirdPartyOptionValueTypeID', @newname='DF_ThirdPartyVehicleOptionValues_Temp_ThirdPartyOptionValueTypeID', @objtype='OBJECT'
--GO
--EXEC sp_rename @objname='dbo.FK_VehicleThirdPartyVehicleOptionValues_VehicleThirdPartyVehicleOptions', @newname='FK_VehicleThirdPartyVehicleOptionValues_Temp_VehicleThirdPartyVehicleOptions', @objtype='OBJECT'
--GO
--EXEC sp_rename @objname='dbo.FK_VehicleThirdPartyVehicleOptionValues_ThirdPartyVehicleOptionTypes', @newname='FK_VehicleThirdPartyVehicleOptionValues_Temp_ThirdPartyVehicleOptionTypes', @objtype='OBJECT'
--GO
--EXEC sp_rename @objname='dbo.ThirdPartyVehicleOptionValues', @newname='ThirdPartyVehicleOptionValues_Temp'
--GO
--
--
--Reverse Orig (Orig-->[Live]):
--
--GO
--EXEC sp_rename @objname='dbo.FK_VehicleThirdPartyVehicleOptionValues_Orig_ThirdPartyVehicleOptionTypes', @newname='FK_VehicleThirdPartyVehicleOptionValues_ThirdPartyVehicleOptionTypes', @objtype='OBJECT'
--GO
--EXEC sp_rename @objname='dbo.FK_VehicleThirdPartyVehicleOptionValues_Orig_VehicleThirdPartyVehicleOptions', @newname='FK_VehicleThirdPartyVehicleOptionValues_VehicleThirdPartyVehicleOptions', @objtype='OBJECT'
--GO
--EXEC sp_rename @objname='dbo.DF_ThirdPartyVehicleOptionValues_Orig_ThirdPartyOptionValueTypeID', @newname='DF_ThirdPartyVehicleOptionValues_ThirdPartyOptionValueTypeID', @objtype='OBJECT'
--GO
--EXEC sp_rename @objname='dbo.PK_ThirdPartyVehicleOptionValues_Orig', @newname='PK_ThirdPartyVehicleOptionValues', @objtype='OBJECT'
--GO
--EXEC sp_rename @objname='dbo.ThirdPartyVehicleOptionValues_Orig.IX_ThirdPartyVehicleOptionValues_Orig__ThirdPartyVehicleOptionID', @newname='IX_ThirdPartyVehicleOptionValues__ThirdPartyVehicleOptionID', @objtype='INDEX'
--GO
--EXEC sp_rename @objname='dbo.ThirdPartyVehicleOptionValues_Orig', @newname='ThirdPartyVehicleOptionValues'
--GO