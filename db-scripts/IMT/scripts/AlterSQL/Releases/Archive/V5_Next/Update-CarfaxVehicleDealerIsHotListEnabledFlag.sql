	/* MAK  02/16/2010  This script corrects the values of Vehicle_dealer.  The IsHotListEnabled flag should match the value
	from the last successful request.*/	
		
		
		
		
		UPDATE VD1
		SET	IsHotListEnabled =CASE WHEN RQQ.DisplayInHotList =1 AND V.IsHotListEligible =1 THEN 1
							ELSE 0 END
		FROM	
				(
				SELECT	MAX(RQ.RequestID) AS RequestID,
						RQ.VehicleID,
						AD.DealerID
				FROM	Carfax.Request RQ
				JOIN	Carfax.Report RPT ON RQ.RequestID =RPT.RequestID
				JOIN	Carfax.Account_Dealer AD ON RQ.AccountID =AD.AccountID
				JOIN	Carfax.Response RS ON RPT.RequestID =RS.RequestID
				WHERE	RS.ResponseCode IN ('500','501')
				GROUP BY RQ.VehicleID,
						 AD.DealerID) MX
		JOIN	Carfax.Request RQQ ON MX.RequestID =RQQ.RequestID
		JOIN	Carfax.Vehicle_Dealer VD1 ON MX.VehicleID =VD1.VehicleID AND MX.DealerID =VD1.DealerID
		JOIN	Carfax.Vehicle V ON VD1.VehicleID=V.VehicleID
		WHERE	(RQQ.DisplayInHotList<> VD1.IsHotListEnabled)
		
 