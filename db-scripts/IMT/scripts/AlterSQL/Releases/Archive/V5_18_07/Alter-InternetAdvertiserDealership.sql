/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *  Fogbugz ID: 11077 
 *	Adding DistributionActive column to InternetAdvertiserDealership to signal whether the
 *  distribution system is active for a dealer / provider.
 * 
 * History
 * ----------
 *  
 * CGC	06/18/2010	Wrote alter script.
 * 						
 * ------------------------------------------------------------------------------------------- */

alter table [dbo].[InternetAdvertiserDealership]
add [DistributionActive] bit null
go
