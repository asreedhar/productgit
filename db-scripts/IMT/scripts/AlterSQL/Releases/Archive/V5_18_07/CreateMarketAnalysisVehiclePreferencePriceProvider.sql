----------------------------------
--  Create the table
----------------------------------

/*
USE [IMT]
--GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Marketing].[FK_Marketing_MarketAnalysisVehiclePreferencePriceProvider__MarketAnalysisPriceProvider]') AND parent_object_id = OBJECT_ID(N'[Marketing].[MarketAnalysisVehiclePreferencePriceProvider]'))
ALTER TABLE [Marketing].[MarketAnalysisVehiclePreferencePriceProvider] DROP CONSTRAINT [FK_Marketing_MarketAnalysisVehiclePreferencePriceProvider__MarketAnalysisPriceProvider]
--GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Marketing].[FK_Marketing_MarketAnalysisVehiclePreferencePriceProvider__MarketAnalysisVehiclePreference]') AND parent_object_id = OBJECT_ID(N'[Marketing].[MarketAnalysisVehiclePreferencePriceProvider]'))
ALTER TABLE [Marketing].[MarketAnalysisVehiclePreferencePriceProvider] DROP CONSTRAINT [FK_Marketing_MarketAnalysisVehiclePreferencePriceProvider__MarketAnalysisVehiclePreference]
--GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[MarketAnalysisVehiclePreferencePriceProvider]') AND type in (N'U'))
DROP TABLE [Marketing].[MarketAnalysisVehiclePreferencePriceProvider]

*/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO


CREATE TABLE Marketing.MarketAnalysisVehiclePreferencePriceProvider(
	MarketAnalysisVehiclePreferenceId	INT NOT NULL,
	MarketAnalysisPriceProviderId		TINYINT NOT NULL,
	InsertUser				INT NOT NULL,
	InsertDate				DATETIME NOT NULL
	CONSTRAINT PK_Marketing_MarketAnalysisVehiclePreferencePriceProvider
	    PRIMARY KEY CLUSTERED (MarketAnalysisVehiclePreferenceId, MarketAnalysisPriceProviderId),
        CONSTRAINT FK_Marketing_MarketAnalysisVehiclePreferencePriceProvider__MarketAnalysisVehiclePreference
                FOREIGN KEY (MarketAnalysisVehiclePreferenceId)
                REFERENCES Marketing.MarketAnalysisVehiclePreference(MarketAnalysisVehiclePreferenceId)
                ON DELETE CASCADE,
        CONSTRAINT FK_Marketing_MarketAnalysisVehiclePreferencePriceProvider__MarketAnalysisPriceProvider
                FOREIGN KEY (MarketAnalysisPriceProviderId)
                REFERENCES Marketing.MarketAnalysisPriceProvider(PriceProviderId) 
		ON DELETE CASCADE
)
GO



