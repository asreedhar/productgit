
/*
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Marketing].[FK_Marketing_ValueAnalyzerVehiclePreference__InsertUser]') AND parent_object_id = OBJECT_ID(N'[Marketing].[ValueAnalyzerVehiclePreference]'))
ALTER TABLE [Marketing].[ValueAnalyzerVehiclePreference] DROP CONSTRAINT [FK_Marketing_ValueAnalyzerVehiclePreference__InsertUser]
--GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Marketing].[FK_Marketing_ValueAnalyzerVehiclePreference__UpdateUser]') AND parent_object_id = OBJECT_ID(N'[Marketing].[ValueAnalyzerVehiclePreference]'))
ALTER TABLE [Marketing].[ValueAnalyzerVehiclePreference] DROP CONSTRAINT [FK_Marketing_ValueAnalyzerVehiclePreference__UpdateUser]
--GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Marketing].[CK_Marketing_ValueAnalyzerVehiclePreference__UpdateDateGreaterThanInsertDate]') AND parent_object_id = OBJECT_ID(N'[Marketing].[ValueAnalyzerVehiclePreference]'))
ALTER TABLE [Marketing].[ValueAnalyzerVehiclePreference] DROP CONSTRAINT [CK_Marketing_ValueAnalyzerVehiclePreference__UpdateDateGreaterThanInsertDate]
--GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Marketing].[CK_Marketing_ValueAnalyzerVehiclePreference__UpdateUserDate]') AND parent_object_id = OBJECT_ID(N'[Marketing].[ValueAnalyzerVehiclePreference]'))
ALTER TABLE [Marketing].[ValueAnalyzerVehiclePreference] DROP CONSTRAINT [CK_Marketing_ValueAnalyzerVehiclePreference__UpdateUserDate]
--GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[ValueAnalyzerVehiclePreference]') AND type in (N'U'))
DROP TABLE [Marketing].[ValueAnalyzerVehiclePreference]
*/

CREATE TABLE Marketing.ValueAnalyzerVehiclePreference (
        OwnerID				INT             NOT NULL,
        VehicleEntityTypeID		TINYINT         NOT NULL,
        VehicleEntityID			INT             NOT NULL,
        DisplayVehicleDescription	BIT             NOT NULL,
        UseLongMarketingListingLayout	BIT             NOT NULL,
        InsertUser			INT             NOT NULL,
        InsertDate			DATETIME        NOT NULL,
        UpdateUser			INT             NULL,
        UpdateDate			DATETIME        NULL
        CONSTRAINT PK_Marketing_ValueAnalyzerVehiclePreference
                PRIMARY KEY CLUSTERED (OwnerID, VehicleEntityTypeID, VehicleEntityID),
        CONSTRAINT FK_Marketing_ValueAnalyzerVehiclePreference__InsertUser
                FOREIGN KEY (InsertUser)
                REFERENCES dbo.Member (MemberID),
        CONSTRAINT FK_Marketing_ValueAnalyzerVehiclePreference__UpdateUser
                FOREIGN KEY (UpdateUser)
                REFERENCES dbo.Member (MemberID),
        CONSTRAINT CK_Marketing_ValueAnalyzerVehiclePreference__UpdateUserDate
                CHECK ( (UpdateDate IS NULL AND UpdateUser IS NULL) OR
                        (UpdateDate IS NOT NULL AND UpdateUser IS NOT NULL)),
        CONSTRAINT CK_Marketing_ValueAnalyzerVehiclePreference__UpdateDateGreaterThanInsertDate
                CHECK  (UpdateDate IS NULL OR UpdateDate >= InsertDate)
)
GO