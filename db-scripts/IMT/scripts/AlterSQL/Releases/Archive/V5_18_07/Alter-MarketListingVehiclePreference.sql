USE [IMT]
GO

ALTER TABLE [Marketing].[MarketListingVehiclePreference] ADD SortCode int NOT NULL default 1
ALTER TABLE [Marketing].[MarketListingVehiclePreference] ADD SortAscending bit NOT NULL default 0