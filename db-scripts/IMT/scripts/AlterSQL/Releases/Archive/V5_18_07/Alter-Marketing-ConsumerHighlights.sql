-----------------------------------------------------------------------------------------------------------------------
--	Alters needed for SA 1.1 for Consumer Highlights.
--  Must run after Create-Highlights-Preferences-Tables.sql.
--
--      - CGC 03/30/2010
-----------------------------------------------------------------------------------------------------------------------

ALTER TABLE [Marketing].[ConsumerHighlight] 
ADD ProviderSourceRowId int null
GO

ALTER TABLE [Marketing].[HighlightProvider]
ADD ConsumerHighlightSectionId int null
GO

UPDATE [Marketing].[HighlightProvider] SET ConsumerHighlightSectionId = 3 WHERE HighlightProviderId = 2 OR HighlightProviderId = 3
UPDATE [Marketing].[HighlightProvider] SET ConsumerHighlightSectionId = 4 WHERE HighlightProviderId = 1
GO

INSERT INTO [Marketing].[HighlightProvider] (HighlightProviderId, Name, ConsumerHighlightSectionId) VALUES (4, 'Expert Reviews & Awards', 2)
INSERT INTO [Marketing].[HighlightProvider] (HighlightProviderId, Name, ConsumerHighlightSectionId) VALUES (5, 'JDPower.com Ratings', 1)
GO

ALTER TABLE [Marketing].[HighlightProvider] 
ADD CONSTRAINT FK_Marketing_HighlightProvider__ConsumerHighlight FOREIGN KEY ([ConsumerHighlightSectionId]) 
REFERENCES [Marketing].[ConsumerHighlightSection] ([ConsumerHighlightSectionId])
GO

ALTER TABLE [Marketing].[ConsumerHighlight]
DROP CONSTRAINT CK_Marketing_ConsumerHighlight__Text
GO

ALTER TABLE [Marketing].[ConsumerHighlight]
ALTER COLUMN Text varchar(200) null
GO
