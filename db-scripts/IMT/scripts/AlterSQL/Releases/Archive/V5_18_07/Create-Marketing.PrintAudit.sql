/* 	CGC 03/19/2010  
	
	This table is for the auditing of printed pdfs from Sales Accelerator 1.1 
	There are no constraints or keys on this table.	
*/

create table [Marketing].[PrintAudit](	
	[PrintPDF]             [varbinary](max) null,
	[PrintXML]             [text] null,
	[OwnerID]              [int] null,
	[VehicleEntityID]      [int] null,
	[VehicleEntityTypeID]  [int] null,
	[InsertUserID]         [int] null,
	[InsertDate]           [DateTime] null
)  
go
