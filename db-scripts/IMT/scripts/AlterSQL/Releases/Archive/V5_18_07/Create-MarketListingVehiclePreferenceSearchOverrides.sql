-- Create table MarketListingVehiclePreferenceSearchOverrides

USE [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Marketing].[MarketListingVehiclePreferenceSearchOverrides](
	[MarketListingVehiclePreferenceId] [int] NOT NULL,
	[VehicleId] [int] NOT NULL,
	[InsertUserId] [int] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[UpdateUserId] [int] NULL,
	[UpdateDate] [datetime] NULL
 CONSTRAINT [PK_MarketListingVehiclePreferenceSearchOverrides] PRIMARY KEY CLUSTERED 
(
	[MarketListingVehiclePreferenceId] ASC,
	[VehicleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]
GO

ALTER TABLE [Marketing].[MarketListingVehiclePreferenceSearchOverrides]  
WITH CHECK ADD CONSTRAINT [FK_Marketing_MarketListingVehiclePreferenceSearchOverrides_MarketListingVehiclePreference_MarketListingVehiclePreferenceId] FOREIGN KEY([MarketListingVehiclePreferenceId])
REFERENCES [Marketing].[MarketListingVehiclePreference] ([MarketListingVehiclePreferenceId])
ON DELETE CASCADE
GO

ALTER TABLE [Marketing].[MarketListingVehiclePreferenceSearchOverrides]  
WITH CHECK ADD CONSTRAINT [FK_Marketing_MarketListingVehiclePreferenceSearchOverrides_Member_InsertUserId] FOREIGN KEY([InsertUserID])
REFERENCES [dbo].[Member] ([MemberID])
GO

ALTER TABLE [Marketing].[MarketListingVehiclePreferenceSearchOverrides] 
CHECK CONSTRAINT [FK_Marketing_MarketListingVehiclePreferenceSearchOverrides_Member_InsertUserId]
GO

ALTER TABLE [Marketing].[MarketListingVehiclePreferenceSearchOverrides]  
WITH CHECK ADD CONSTRAINT [FK_Marketing_MarketListingVehiclePreferenceSearchOverrides_Member_UpdateUserId] FOREIGN KEY([UpdateUserId])
REFERENCES [dbo].[Member] ([MemberID])
GO

ALTER TABLE [Marketing].[MarketListingVehiclePreferenceSearchOverrides] 
CHECK CONSTRAINT [FK_Marketing_MarketListingVehiclePreferenceSearchOverrides_Member_UpdateUserId]
GO