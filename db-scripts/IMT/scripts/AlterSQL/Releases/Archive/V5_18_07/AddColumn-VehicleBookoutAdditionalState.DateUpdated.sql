-- Add a datetime column to IMT.dbo.VehicleBookoutAdditionalState in order to implement KBB Consumer values as a CVA equipment provider.
-- dhillis, 03/2010

ALTER TABLE dbo.VehicleBookoutAdditionalState ADD [DateUpdated] DATETIME NULL
GO

UPDATE dbo.VehicleBookoutAdditionalState SET DateUpdated = GETDATE()
GO

ALTER TABLE dbo.VehicleBookoutAdditionalState ALTER COLUMN DateUpdated DATETIME NOT NULL
GO
