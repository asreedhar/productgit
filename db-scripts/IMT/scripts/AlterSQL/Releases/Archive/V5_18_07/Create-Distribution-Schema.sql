set ansi_nulls on
go

set quoted_identifier on
go

set ansi_padding on
go

set nocount on
go

-----------------------------------------------------------------------------------------------------------------------
--
--	Creates the IMT.Distribution schema and its associated tables.
--	CGC	01/25/2010	Initial design/development
--
-----------------------------------------------------------------------------------------------------------------------
--	1.  Create Distribution schema
-----------------------------------------------------------------------------------------------------------------------
create schema Distribution
go


-----------------------------------------------------------------------------------------------------------------------
--	2.  Create Distribution.VehicleEntityType
--      a. Check Constraints    
--              - CK_Distribution_VehicleEntityType__Name
--      b. Foreign Keys
--				- None
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[VehicleEntityType]
(
    [VehicleEntityTypeID]   [int] identity(1,1) not null,
    [Name]                  [varchar](50) not null,

    constraint [PK_Distribution_VehicleEntityType]
    primary key ([VehicleEntityTypeID]),
    
    constraint [CK_Distribution_VehicleEntityType__Name]
    check (len([Name]) > 0)
)
go

insert into [Distribution].[VehicleEntityType] (name) values ('Inventory')
go


-----------------------------------------------------------------------------------------------------------------------
--	3.  Create Distribution.Vehicle
--      a. Check Constraints    
--              - None
--      b. Foreign Keys
--				- FK_Distribution_Vehicle__VehicleEntityType
--              - FK_Distribution_Vehicle__Member
--      c. Unique Constraints
--              - UQ_Distribution_Vehicle__VehicleEntityID
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[Vehicle]
(
    [VehicleEntityID]       [int] not null,
    [VehicleEntityTypeID]   [int] not null,    
    [InsertUserID]          [int] not null,
    [InsertDate]            [datetime] not null,
    
    constraint [PK_Distribution_Vehicle]
    primary key ([VehicleEntityID], [VehicleEntityTypeID]),
    
    constraint [UQ_Distribution_Vehicle__VehicleEntityID]
    unique ([VehicleEntityID]),
    
    constraint [FK_Distribution_Vehicle__VehicleEntityType]
    foreign key ([VehicleEntityTypeID])
    references [Distribution].[VehicleEntityType] ([VehicleEntityTypeID]),
    
    constraint [FK_Distribution_Vehicle__Member]
    foreign key ([InsertUserID])
    references [dbo].[Member] ([MemberID])
)
go


-----------------------------------------------------------------------------------------------------------------------
--	4.  Create Distribution.Message
--      a. Check Constraints    
--              - None
--      b. Foreign Keys
--				- FK_Distribution_Message__BusinessUnit
--              - FK_Distribution_Message__Vehicle
--              - FK_Distribution_Message__Member
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[Message]
(
    [MessageID]             [int] identity(1,1) not null,
    [DealerID]              [int] not null,
    [VehicleEntityID]       [int] not null,
    [VehicleEntityTypeID]   [int] not null,
    [InsertUserID]          [int] not null,
    [InsertDate]            [datetime] not null,
    
    constraint [PK_Distribution_Message]
    primary key ([MessageID]),
    
    constraint [FK_Distribution_Message__BusinessUnit]
    foreign key ([DealerID]) 
    references [dbo].[BusinessUnit] ([BusinessUnitID]),
    
    constraint [FK_Distribution_Message__Vehicle]
    foreign key ([VehicleEntityID], [VehicleEntityTypeID])
    references [Distribution].[Vehicle] ([VehicleEntityID], [VehicleEntityTypeID]),   
    
    constraint [FK_Distribution_Message__Member]
    foreign key ([InsertUserID])
    references [dbo].[Member] ([MemberID])     
)
go


-----------------------------------------------------------------------------------------------------------------------
--	5.  Create Distribution.VehicleInformation
--      a. Check Constraints    
--              - CK_Distribution_VehicleInformation__Mileage
--      b. Foreign Keys
--              - FK_Distribution_VehicleInformation__Message
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[VehicleInformation]
(
    [MessageID]     [int] not null,
    [Mileage]       [int] not null,
    
    constraint [PK_Distribution_VehicleInformation]
    primary key ([MessageID]),
    
    constraint [FK_Distribution_VehicleInformation__Message]
    foreign key ([MessageID])
    references [Distribution].[Message] ([MessageID]),
    
    constraint [CK_Distribution_VehicleInformation__Mileage]
    check ([Mileage] >= 0  and [Mileage] <= 999999)
) 
go


-----------------------------------------------------------------------------------------------------------------------
--	6.  Create Distribution.PriceType
--      a. Check Constraints    
--              - CK_Distribution_PriceType__Name
--      b. Foreign Keys
--				- None
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[PriceType]
(
    [PriceTypeID]   [int] identity(0,1) not null,
    [Name]          [varchar](50) not null,
    
    constraint [PK_Distribution_PriceType]
    primary key ([PriceTypeID]),

    constraint [CK_Distribution_PriceType__Name]
    check (len([Name]) > 0)
)
go

insert into [Distribution].[PriceType] (name) values ('NotDefined')
insert into [Distribution].[PriceType] (name) values ('Lot')
insert into [Distribution].[PriceType] (name) values ('Internet')
go


-----------------------------------------------------------------------------------------------------------------------
--	7.  Create Distribution.Price
--      a. Check Constraints    
--              - CK_Distribution_Price__Value
--      b. Foreign Keys
--              - FK_Distribution_Price__Message    
--				- FK_Distribution_Price__PriceType
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[Price]
(
    [MessageID]     [int] not null,
    [PriceTypeID]   [int] not null,
    [Value]         [int] not null,
    
    constraint [PK_Distribution_Price]
    primary key ([MessageID]),
    
    constraint [FK_Distribution_Price__Message]
    foreign key ([MessageID])
    references [Distribution].[Message] ([MessageID]),
    
    constraint [FK_Distribution_Price__PriceType]
    foreign key ([PriceTypeID])
    references [Distribution].[PriceType] ([PriceTypeID]),
    
    constraint [CK_Distribution_Price__Value]
    check ([Value] >= 0 and [Value] <= 999999)
)
go


-----------------------------------------------------------------------------------------------------------------------
--	8.  Create Distribution.ContentType
--      a. Check Constraints    
--              - CK_Distribution_ContentType__Description
--      b. Foreign Keys
--				- None
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[ContentType]
(
    [ContentTypeID]   [int] identity(1,1) not null,
    [Description]     [varchar](50) not null,
    
    constraint [PK_Distribution_ContentType] 
    primary key ([ContentTypeID]),
    
    constraint [CK_Distribution_ContentType__Description]
    check (len([Description]) > 0)
)
go

insert into [Distribution].[ContentType] (Description) values ('Text')
go


-----------------------------------------------------------------------------------------------------------------------
--	9.  Create Distribution.Content
--      a. Check Constraints    
--              - None
--      b. Foreign Keys
--				- FK_Distribution_Content__ContentType
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[Content]
(
    [ContentID]       [int] identity(1,1) not null,
    [ContentTypeID]   [int] not null,
    
    constraint [PK_Distribution_Content] 
    primary key ([ContentID]),
    
    constraint [FK_Distribution_Content__ContentType] 
    foreign key ([ContentTypeID])
    references [Distribution].[ContentType] ([ContentTypeID])
)
go


-----------------------------------------------------------------------------------------------------------------------
--	10.  Create Distribution.ContentText
--      a. Check Constraints    
--              - CK_Distribution_ContentText__Text
--      b. Foreign Keys
--				- FK_Distribution_ContextText__Content
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[ContentText]
(
    [ContentID]   [int] not null,
    [Text]        [varchar](2000) not null,
    
    constraint [PK_Distribution_ContentText] 
    primary key ([ContentID]),
    
    constraint [FK_Distribution_ContextText__Content] 
    foreign key ([ContentID])
    references [Distribution].[Content] ([ContentID]),
    
    constraint [CK_Distribution_ContentText__Text] 
    check (len([Text]) > 0)
)
go


-----------------------------------------------------------------------------------------------------------------------
--	11.  Create Distribution.Advertisement_Content
--      a. Check Constraints    
--              - None
--      b. Foreign Keys
--				- FK_Distribution_Advertisement_Content__Message
--              - FK_Distribution_Advertisement_Content__Content
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[Advertisement_Content]
(
    [MessageID]   [int] not null,
    [ContentID]   [int] not null,
    
    constraint [PK_Distribution_Advertisement_Content] 
    primary key ([MessageID], [ContentID]),
    
    constraint [FK_Distribution_Advertisement_Content__Message] 
    foreign key ([MessageId])
    references [Distribution].[Message] ([MessageId]),
    
    constraint [FK_Distribution_Advertisement_Content__Content] 
    foreign key ([ContentID])
    references [Distribution].[Content] ([ContentID])
)
go


-----------------------------------------------------------------------------------------------------------------------
--	12.  Create Distribution.Provider
--      a. Check Constraints    
--              - CK_Distribution_Provider__Name
--      b. Foreign Keys
--				- None
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[Provider]
(
    [ProviderID]   [int] identity(1,1) not null,
    [Name]         [varchar](50) not null,  

    constraint [PK_Distribution_Provider] 
    primary key ([ProviderID]),
    
    constraint [CK_Distribution_Provider__Name]
    check (len([Name]) > 0)
)
go

insert into [Distribution].[Provider] (name) values ('AutoTrader')
insert into [Distribution].[Provider] (name) values ('AutoUplink')
insert into [Distribution].[Provider] (name) values ('Aultec')
go


-----------------------------------------------------------------------------------------------------------------------
--	13.  Create Distribution.Account
--      a. Check Constraints    
--              - CK_Distribution_Account__UserName
--              - CK_Distribution_Account__Password
--              - CK_Distribution_Account__Dates
--      b. Foreign Keys
--				- FK_Distribution_Account__BusinessUnit
--              - FK_Distribution_Account__Provider
--              - FK_Distribution_Account__InsertMember
--              - FK_Distribution_Account__UpdateMember
--      c. Triggers
--              - TR_I_Account
--              - TR_U_Account
--              - TR_D_Account
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[Account]
(    
    [DealerID]       [int] not null,
    [ProviderID]     [int] not null,
    [UserName]       [varchar](50) null,
    [Password]       [varchar](50) null,
    [DealershipCode] [varchar](50) null,
	[Active]         [bit] not null,    
    [InsertUserID]   [int] not null,
    [InsertDate]     [datetime] not null,
    [UpdateUserID]   [int] null,
    [UpdateDate]     [datetime] null,
		        
    constraint [PK_Distribution_Account] 
    primary key ([DealerID], [ProviderID]),   
          
    constraint [FK_Distribution_Account__BusinessUnit] 
    foreign key ([DealerID]) 
    references [dbo].[BusinessUnit] ([BusinessUnitID]),
	
    constraint [FK_Distribution_Account__Provider] 
    foreign key ([ProviderID]) 
    references [Distribution].[Provider] ([ProviderID]),
    
    constraint [FK_Distribution_Account__InsertMember] 
    foreign key ([InsertUserID]) 
    references [dbo].[Member] ([MemberID]),
    
    constraint [FK_Distribution_Account__UpdateMember] 
    foreign key ([UpdateUserID]) 
    references [dbo].[Member] ([MemberID]),      
    
    constraint [CK_Distribution_Account__Dates] 
    check ([UpdateDate] >= [InsertDate])
)
go


-----------------------------------------------------------------------------------------------------------------------
--	14.  Create Distribution.Account_Audit
--      a. Check Constraints    
--              - CK_Distribution_AccountAudit__ValidDates
--              - CK_Distribution_AccountAudit__WasInsertUpdateDelete
--              - CK_Distribution_AccountAudit__UserName
--              - CK_Distribution_AccountAudit__Password
--      b. Foreign Keys
--				- FK_Distribution_AccountAudit__BusinessUnit
--              - FK_Distribution_AccountAudit__Provider
--              - FK_Distribution_AccountAudit__Member
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[Account_Audit]
(
    [AccountAuditID] [int] identity(1,1) not null,    
    [DealerID]       [int] not null,
    [ProviderID]     [int] not null,
    [UserName]       [varchar](50) null,
    [Password]       [varchar](50) null,
    [DealershipCode] [varchar](50) null,
	[Active]         [bit] not null,    
    [ChangeUserID]   [int] not null,
    [ChangeDate]     [datetime] not null,
    [ValidFrom]      [datetime] not null,
    [ValidUntil]     [datetime] not null,
    [WasInsert]      [bit] not null,
    [WasUpdate]      [bit] not null,
    [WasDelete]      [bit] not null,    
		    
    constraint [PK_Distribution_AccountAudit] 
    primary key ([AccountAuditID]),              
            
    constraint [FK_Distribution_AccountAudit__BusinessUnit] 
    foreign key ([DealerID]) 
    references [dbo].[BusinessUnit] ([BusinessUnitID]),
	
    constraint [FK_Distribution_AccountAudit__Provider] 
    foreign key ([ProviderID]) 
    references [Distribution].[Provider] ([ProviderID]),
    
    constraint [FK_Distribution_AccountAudit__Member] 
    foreign key ([ChangeUserID]) 
    references [dbo].[Member] ([MemberID]),     
   
    constraint [CK_Distribution_AccountAudit__ValidDates] 
    check ([ValidFrom] <= [ValidUntil]),
    
    constraint [CK_Distribution_AccountAudit__WasInsertUpdateDelete] 
    check 
    (([WasInsert] = 1 and [WasUpdate] = 0 and [WasDelete] = 0) or
     ([WasInsert] = 0 and [WasUpdate] = 1 and [WasDelete] = 0) or
     ([WasInsert] = 0 and [WasUpdate] = 0 and [WasDelete] = 1))
)
go

-- Trigger on inserts.
create trigger [TR_I_Account] on [Distribution].[Account]
for insert
as
begin
    set nocount on;
    
    declare @DealerID int, @ProviderID int, @ValidFrom datetime
    
    select @DealerID = i.DealerID, @ProviderID = i.ProviderID
    from inserted i
    
    set @ValidFrom = getdate()
    
    -- Change the ValidUntil date of the last entry for this dealer and provider.
    update [Distribution].[Account_Audit]
    set [ValidUntil] = @ValidFrom
    where [DealerID] = @DealerID
    and [ProviderID] = @ProviderID
    and [ValidUntil] = '01/01/2040'
    
    -- Insert into the audit table.
    insert into [Distribution].[Account_Audit] 
    (DealerID, ProviderID, UserName, Password, DealershipCode, Active, ChangeUserID, ChangeDate, ValidFrom, ValidUntil, WasInsert, WasUpdate, WasDelete)
    select DealerID, ProviderID, UserName, Password, DealershipCode, Active, InsertUserID, InsertDate, @ValidFrom, '01/01/2040', 1, 0, 0
    from inserted
end
go

-- Trigger on updates.
create trigger [TR_U_Account] on [Distribution].[Account]
after update
as
begin
    set nocount on;
    
    declare @DealerID int, @ProviderID int, @ValidFrom datetime
    
    select @DealerID = i.DealerID, @ProviderID = i.ProviderID
    from inserted i
    
    set @ValidFrom = getdate()
    
    -- Change the ValidUntil date of the last entry for this dealer and provider.
    update [Distribution].[Account_Audit]
    set [ValidUntil] = @ValidFrom
    where [DealerID] = @DealerID
    and [ProviderID] = @ProviderID
    and [ValidUntil] = '01/01/2040'
    
    -- Insert into the audit table.
    insert into [Distribution].[Account_Audit]  
    (DealerID, ProviderID, UserName, Password, DealershipCode, Active, ChangeUserID, ChangeDate, ValidFrom, ValidUntil, WasInsert, WasUpdate, WasDelete)    
    select DealerID, ProviderID, UserName, Password, DealershipCode, Active, UpdateUserID, UpdateDate, @ValidFrom, '01/01/2040', 0, 1, 0
    from inserted  
end
go

-- Trigger on deletes.
create trigger [TR_D_Account] on [Distribution].[Account]
after delete
as
begin
    set nocount on;
    
    declare @DealerID int, @ProviderID int, @ValidFrom datetime
    
    select @DealerID = d.DealerID, @ProviderID = d.ProviderID
    from deleted d
    
    set @ValidFrom = getdate()
    
    -- Change the ValidUntil date of the last entry for this dealer and provider.
    update [Distribution].[Account_Audit]
    set [ValidUntil] = @ValidFrom
    where [DealerID] = @DealerID
    and [ProviderID] = @ProviderID
    and [ValidUntil] = '01/01/2040'
    
    -- Insert into the audit table.
    insert into [Distribution].[Account_Audit]  
	(DealerID, ProviderID, UserName, Password, DealershipCode, Active, ChangeUserID, ChangeDate, ValidFrom, ValidUntil, WasInsert, WasUpdate, WasDelete)    
    select DealerID, ProviderID, UserName, Password, DealershipCode, Active, UpdateUserID, UpdateDate, @ValidFrom, '01/01/2040', 0, 0, 1
    from deleted  
end
go


-----------------------------------------------------------------------------------------------------------------------
--	15.  Create Distribution.PublicationStatus
--      a. Check Constraints    
--              - CK_Distribution_PublicationStatus__Name
--      b. Foreign Keys
--				- None
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[PublicationStatus]
(
    [PublicationStatusID]   [int] identity(0,1) not null,
    [Name]                  [varchar](50) not null,
    
    constraint [PK_Distribution_PublicationStatus] 
    primary key ([PublicationStatusID]),
    
    constraint [CK_Distribution_PublicationStatus__Name]
    check (len([Name]) > 0)
)
go

insert into [Distribution].[PublicationStatus] (name) values ('Unknown')
insert into [Distribution].[PublicationStatus] (name) values ('Online')
insert into [Distribution].[PublicationStatus] (name) values ('Offline')
insert into [Distribution].[PublicationStatus] (name) values ('NotApplicable')
go


-----------------------------------------------------------------------------------------------------------------------
--  16.  Create Distribution.Publication
--      a. Check Constraints    
--              - CK_Distribution_Publication__InsertDates
--      b. Foreign Keys
--				- FK_Distribution_Publication__Message
--              - FK_Distribution_Publication__PublicationStatus
--              - FK_Distribution_Publication__InsertMember
--              - FK_Distribution_Publication__UpdateMember
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[Publication]
(
    [PublicationID]         [int] identity(1,1) not null,
    [MessageID]             [int] not null,    
    [PublicationStatusID]   [int] not null,
    [InsertUserID]          [int] not null,
    [InsertDate]            [datetime] not null,
    [UpdateUserID]          [int] null,
    [UpdateDate]            [datetime] null,
    
    constraint [PK_Distribution_Publication] 
    primary key ([PublicationID]),
    
    constraint [FK_Distribution_Publication__Message] 
    foreign key ([MessageID])
    references [Distribution].[Message] ([MessageID]),
    
    constraint [FK_Distribution_Publication__PublicationStatus] 
    foreign key ([PublicationStatusID])
    references [Distribution].[PublicationStatus] ([PublicationStatusID]),
    
    constraint [FK_Distribution_Publication__InsertMember]
    foreign key ([InsertUserID])
    references [dbo].[Member] ([MemberID]),
    
    constraint [FK_Distribution_Publication__UpdateMember]
    foreign key ([UpdateUserID])
    references [dbo].[Member] ([MemberID]),     
    
    constraint [CK_Distribution_Publication__InsertDates]
    check ([UpdateDate] >= [InsertDate])
)
go


-----------------------------------------------------------------------------------------------------------------------		 
--	17.  Create Distribution.SubmissionStatus
--      a. Check Constraints    
--              - CK_Distibution_SubmissionStatus__Name
--      b. Foreign Keys
--				- None
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[SubmissionStatus]
(
    [SubmissionStatusID]   [int] identity(0,1) not null,
    [Name]                 [varchar](50) not null,
    
    constraint [PK_Distribution_SubmissionStatus] 
    primary key ([SubmissionStatusID]),
    
    constraint [CK_Distibution_SubmissionStatus__Name]
    check (len([Name]) > 0)
)
go

insert into [Distribution].[SubmissionStatus] (name) values ('NotDefined')
insert into [Distribution].[SubmissionStatus] (name) values ('Waiting')
insert into [Distribution].[SubmissionStatus] (name) values ('Processing')
insert into [Distribution].[SubmissionStatus] (name) values ('Submitted') 
insert into [Distribution].[SubmissionStatus] (name) values ('Accepted') 
insert into [Distribution].[SubmissionStatus] (name) values ('Rejected')
insert into [Distribution].[SubmissionStatus] (name) values ('Retracted')
insert into [Distribution].[SubmissionStatus] (name) values ('Superseded')
insert into [Distribution].[SubmissionStatus] (name) values ('NotSupported')
go


-----------------------------------------------------------------------------------------------------------------------
--	18.  Create Distribution.Submission
--      a. Check Constraints    
--              - CK_Distribution_Submission__Dates
--      b. Foreign Keys
--              - FK_Distribution_Submission__Publication
--              - FK_Distribution_Submission__Provider
--				- FK_Distribution_Submission__SubmissionStatus
--      c. Unique Constraint
--              - UQ_Distribution_Submission__Publication_Provider
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[Submission]
(
    [SubmissionID]         [int] identity(1,1) not null,
    [PublicationID]        [int] not null,
    [ProviderID]           [int] not null,
    [SubmissionStatusID]   [int] not null,
    [PushedOnQueue]        [datetime] not null,
    [PoppedOffQueue]       [datetime] null,
    [SubmissionSent]       [datetime] null,
    [ResponseReceived]     [datetime] null,
        
    constraint [PK_Distribution_Submission] 
    primary key ([SubmissionID]),
    
    constraint [UQ_Distribution_Submission__Publication_Provider]
    unique ([PublicationID], [ProviderID]),
    
    constraint [FK_Distribution_Submission__Publication]
    foreign key ([PublicationID])
    references [Distribution].[Publication] ([PublicationID]),      
    
    constraint [FK_Distribution_Submission__Provider] 
    foreign key ([ProviderID])
    references [Distribution].[Provider] ([ProviderID]),
    
    constraint [FK_Distribution_Submission__SubmissionStatus] 
    foreign key ([SubmissionStatusID])
    references [Distribution].[SubmissionStatus] ([SubmissionStatusID]),
    
    constraint [CK_Distribution_Submission__Dates] 
    check ([ResponseReceived] >= [SubmissionSent] and [SubmissionSent] >= [PoppedOffQueue] and [PoppedOffQueue] >= [PushedOnQueue])
)
go


-----------------------------------------------------------------------------------------------------------------------
--	19.  Create Distribution.SubmissionSuccess
--      a. Check Constraints    
--              - None
--      b. Foreign Keys
--				- FK_Distribution_SubmissionSuccess__Submission
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[SubmissionSuccess]
(
    [SubmissionID]      [int] not null,
    [MessageTypeFlag]   [int] not null,
    [OccuredOn]         [datetime] not null,
    
    constraint [FK_Distribution_SubmissionSuccess__Submission]
    foreign key ([SubmissionID])
    references [Distribution].[Submission] ([SubmissionID])
)
go


-----------------------------------------------------------------------------------------------------------------------
--	20.  Create Distribution.SubmissionErrorType
--      a. Check Constraints    
--              - CK_Distribution_SubmissionErrorType__Description
--      b. Foreign Keys
--				- None
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[SubmissionErrorType]
(
    [ErrorTypeID]    [int] identity(0,1) not null,
    [Description]    [varchar](2000) not null,
    [ConcernsUser]   [bit] not null,
    
    constraint [PK_Distribution_SubmissionErrorType] 
    primary key ([ErrorTypeID]),
    
    constraint [CK_Distribution_SubmissionErrorType__Description]
    check (len([Description]) > 0)
)
go

insert into [Distribution].[SubmissionErrorType] (description, concernsuser) values ('Unrecognized', 1)
insert into [Distribution].[SubmissionErrorType] (description, concernsuser) values ('Generic Fatal Error', 1)
insert into [Distribution].[SubmissionErrorType] (description, concernsuser) values ('System Error', 1)
insert into [Distribution].[SubmissionErrorType] (description, concernsuser) values ('Field Locked', 1)
insert into [Distribution].[SubmissionErrorType] (description, concernsuser) values ('Field Has Same Value', 1)
insert into [Distribution].[SubmissionErrorType] (description, concernsuser) values ('Authentication Error', 1)
insert into [Distribution].[SubmissionErrorType] (description, concernsuser) values ('Vehicle Not Found', 1)
insert into [Distribution].[SubmissionErrorType] (description, concernsuser) values ('Vehicle Deleted', 1)
insert into [Distribution].[SubmissionErrorType] (description, concernsuser) values ('Not Authorized', 1)
insert into [Distribution].[SubmissionErrorType] (description, concernsuser) values ('Dealer Not Found', 1)
insert into [Distribution].[SubmissionErrorType] (description, concernsuser) values ('Dealer VIN Mismatch', 1)
insert into [Distribution].[SubmissionErrorType] (description, concernsuser) values ('Missing Field', 1)
insert into [Distribution].[SubmissionErrorType] (description, concernsuser) values ('Invalid Field', 1)
go


-----------------------------------------------------------------------------------------------------------------------		 
--	21.  Create Distribution.SubmissionError
--      a. Check Constraints    
--              - None
--      b. Foreign Keys
--				- FK_Distribution_SubmissionError__SubmissionErrorType
--              - FK_Distribution_SubmissionError__Submission
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[SubmissionError]
(
    [ErrorID]        [int] identity(1,1) not null,
    [ErrorTypeID]    [int] not null,
    [SubmissionID]   [int] not null,    
    [OccuredOn]      [datetime] not null,
    
    constraint [PK_Distribution_Error] 
    primary key ([ErrorID]),
          
    constraint [FK_Distribution_SubmissionError__SubmissionErrorType] 
    foreign key ([ErrorTypeID])
    references [Distribution].[SubmissionErrorType] ([ErrorTypeID]),
    
    constraint [FK_Distribution_SubmissionError__Submission] 
    foreign key ([SubmissionID])
    references [Distribution].[Submission] ([SubmissionID])
)
go


-----------------------------------------------------------------------------------------------------------------------
--	22.  Create Distribution.AutoTrader_ErrorType
--      a. Check Constraints    
--              - CK_Distribution_AutoTrader_ErrorType__Description
--      b. Foreign Keys
--				- None
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[AutoTrader_ErrorType]
(
    [ErrorTypeID]   [int] identity(0,1) not null,
    [Description]   [varchar](2000) not null,
    
    constraint [PK_Distribution_AutoTrader_ErrorType] 
    primary key ([ErrorTypeID]),
    
    constraint [CK_Distribution_AutoTrader_ErrorType__Description]
    check (len([Description]) > 0)
)
go

insert into [Distribution].[AutoTrader_ErrorType] ([Description]) values ('Unrecognized')
insert into [Distribution].[AutoTrader_ErrorType] ([Description]) values ('Not Authorizd')
insert into [Distribution].[AutoTrader_ErrorType] ([Description]) values ('VIN Required')
insert into [Distribution].[AutoTrader_ErrorType] ([Description]) values ('Price Required')
insert into [Distribution].[AutoTrader_ErrorType] ([Description]) values ('VIN Not Found')
insert into [Distribution].[AutoTrader_ErrorType] ([Description]) values ('Dealer Invalid')
insert into [Distribution].[AutoTrader_ErrorType] ([Description]) values ('VIN Invalid')
insert into [Distribution].[AutoTrader_ErrorType] ([Description]) values ('Price Invalid')
insert into [Distribution].[AutoTrader_ErrorType] ([Description]) values ('System Error')
go


-----------------------------------------------------------------------------------------------------------------------
--	23.  Create Distribution.AutoTrader_Error
--      a. Check Constraints    
--              - None
--      b. Foreign Keys
--				- FK_Distribution_AutoTraderError__SubmissionError
--              - FK_Distribution_AutoTraderError__AutoTrader_ErrorType
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[AutoTrader_Error]
(
    [ErrorID]       [int] not null,
    [ErrorTypeID]   [int] not null,
    [ErrorMessage]  [varchar](2000) null,
    
    constraint [PK_Distribution_AutoTraderError] 
    primary key ([ErrorID]),
    
    constraint [FK_Distribution_AutoTraderError__SubmissionError] 
    foreign key ([ErrorID])
    references [Distribution].[SubmissionError] ([ErrorID]),
    
    constraint [FK_Distribution_AutoTraderError__AutoTrader_ErrorType] 
    foreign key ([ErrorTypeID])
    references [Distribution].[AutoTrader_ErrorType] ([ErrorTypeID])    
)
go


-----------------------------------------------------------------------------------------------------------------------
--	24.  Create Distribution.AutoUplink_ErrorType
--      a. Check Constraints    
--              - CK_Distribution_AutoUplink_ErrorType__Description
--      b. Foreign Keys
--				- None
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[AutoUplink_ErrorType]
(
    [ErrorTypeID]   [int] identity(0,1) not null,
    [Description]   [varchar](2000) not null,
    
    constraint [PK_Distribution_AutoUplink_ErrorType] 
    primary key ([ErrorTypeID]),
    
    constraint [CK_Distribution_AutoUplink_ErrorType__Description]
    check (len([Description]) > 0)
)
go

insert into [Distribution].[AutoUplink_ErrorType] ([Description]) values ('Unrecognized')
insert into [Distribution].[AutoUplink_ErrorType] ([Description]) values ('Vehicle Not Found')
insert into [Distribution].[AutoUplink_ErrorType] ([Description]) values ('Vehicle Deleted')
insert into [Distribution].[AutoUplink_ErrorType] ([Description]) values ('Field Has Same Value')
insert into [Distribution].[AutoUplink_ErrorType] ([Description]) values ('Field Locked')
insert into [Distribution].[AutoUplink_ErrorType] ([Description]) values ('Authentication Error')
insert into [Distribution].[AutoUplink_ErrorType] ([Description]) values ('System Error')
go


-----------------------------------------------------------------------------------------------------------------------
--	25.  Create Distribution.AutoUplink_Error
--      a. Check Constraints    
--              - None
--      b. Foreign Keys
--				- FK_Distribution_AutoUplink_Error__SubmissionError
--              - FK_Distribution_AutoUplink_Error__AutoTrader_ErrorType
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[AutoUplink_Error]
(
    [ErrorID]       [int] not null,
    [ErrorTypeID]   [int] not null,
    [ErrorCode]     [varchar](50) not null,
    
    constraint [PK_Distribution_AutoUplink_Error] 
    primary key ([ErrorID]),
    
    constraint [FK_Distribution_AutoUplink_Error__SubmissionError] 
    foreign key ([ErrorID])
    references [Distribution].[SubmissionError] ([ErrorID]),
    
    constraint [FK_Distribution_AutoUplink_Error__AutoUplink_ErrorType] 
    foreign key ([ErrorTypeID])
    references [Distribution].[AutoUplink_ErrorType] ([ErrorTypeID])
)
go


-----------------------------------------------------------------------------------------------------------------------
--	26.  Create Distribution.Exception
--      a. Check Constraints    
--              - None
--      b. Foreign Keys
--				- FK_Distribution_Exception__Publication.
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[Exception]
(
    [ExceptionID]       [int] identity(1,1) not null,
    [ExceptionTime]     [datetime] not null,     
    [MachineName]       [nvarchar](256) collate SQL_Latin1_General_CP1_CI_AS not null,
    [Message]           [nvarchar](1024) collate SQL_Latin1_General_CP1_CI_AS not null, 
    [ExceptionType]     [nvarchar](256) collate SQL_Latin1_General_CP1_CI_AS null,
    [Details]           [ntext] collate SQL_Latin1_General_CP1_CI_AS null,
    [PublicationID]     [int] null,
        
    constraint [PK_Distribution_Exception] 
    primary key ([ExceptionID]),
    
    constraint [FK_Distribution_Exception__Publication]
    foreign key ([PublicationID])
    references [Distribution].[Publication] ([PublicationID])    
)
go


-----------------------------------------------------------------------------------------------------------------------
--	27.  Create Distribution.MessageType
--      a. Check Constraints    
--              - CK_Distribution_MessageType__Name
--				- CK_Distribution_MessageType__MessageTypeID
--      b. Foreign Keys
--				- None
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[MessageType]
(
    [MessageTypeID]     [int] identity(0,1) not null,
    [Name]              [varchar](50) not null,
    [FlagValue]         as case when [MessageTypeID] < 31 then power(2, [MessageTypeID]) else 0 end persisted,
    
    constraint [PK_Distribution_MessageType]
    primary key ([MessageTypeID]),
    
    constraint [CK_Distribution_MessageType__Name]
    check (len([Name]) > 0),
    
    constraint [CK_Distribution_MessageType__MessageTypeID]
    check ([MessageTypeID] < 31)
)
go

insert into [Distribution].[MessageType] (Name) values ('Price')
insert into [Distribution].[MessageType] (Name) values ('Description')
insert into [Distribution].[MessageType] (Name) values ('Mileage')
go


-----------------------------------------------------------------------------------------------------------------------
--	28.  Create Distribution.ProviderMessageType
--      a. Check Constraints    
--              - None
--      b. Foreign Keys
--				- FK_Distribution_ProviderMessageType__Provider
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[ProviderMessageType]
(
    [ProviderID]        [int] not null,
    [MessageTypeFlag]  [int] not null,
    
    constraint [PK_Distribution_ProviderMessageType]
    primary key ([ProviderID], [MessageTypeFlag]),
    
    constraint [FK_Distribution_ProviderMessageType__Provider]
    foreign key ([ProviderID])
    references [Distribution].[Provider] ([ProviderID])
)
go

-- AutoTrader supports Price.
insert into [Distribution].[ProviderMessageType] (ProviderID, MessageTypeFlag) values (1, 1)
-- AutoTrader supports Price + Advertisement.
insert into [Distribution].[ProviderMessageType] (ProviderID, MessageTypeFlag) values (1, 3)
-- AutoUplink supports Price.
insert into [Distribution].[ProviderMessageType] (ProviderID, MessageTypeFlag) values (2, 1)
-- AutoUplink supports Advertisement.
insert into [Distribution].[ProviderMessageType] (ProviderID, MessageTypeFlag) values (2, 2)
-- Temporary?
insert into [Distribution].[ProviderMessageType] (ProviderID, MessageTypeFlag) values (2, 3)
-- Aultec supports Price.
insert into [Distribution].[ProviderMessageType] (ProviderID, MessageTypeFlag) values (3, 1)
-- Aultec supports Advertisement.
insert into [Distribution].[ProviderMessageType] (ProviderID, MessageTypeFlag) values (3, 2)
-- Temporary?
insert into [Distribution].[ProviderMessageType] (ProviderID, MessageTypeFlag) values (3, 3)
go


-----------------------------------------------------------------------------------------------------------------------
--	29.  Create Distribution.AccountPreferences
--      a. Check Constraints    
--              - None
--      b. Foreign Keys
--				- FK_Distribution_AccountPreferences__BusinessUnit
--              - FK_Distribution_AccountPreferences__ProviderMessageType
--      c. Unique Constraints
--              - UQ_Distribution_AccountPreferences__Dealer_Provider_MessageType
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[AccountPreferences]
(
    [DealerID]          [int] not null,
    [ProviderID]        [int] not null,
    [MessageTypeFlag]  [int] not null,
    
    constraint [UQ_Distribution_AccountPreferences__Dealer_Provider_MessageType]
    unique ([DealerID], [ProviderID], [MessageTypeFlag]),
    
    constraint [FK_Distribution_AccountPreferences__BusinessUnit]
    foreign key ([DealerID])
    references [dbo].[BusinessUnit] ([BusinessUnitID]),
    
    constraint [FK_Distribution_AccountPreferences__ProviderMessageType]
    foreign key ([ProviderID], [MessageTypeFlag])
    references [Distribution].[ProviderMessageType] ([ProviderID], [MessageTypeFlag])
)
go
