/*
These equipment providers are not used.  Since we now have valid equipment providers with
a null thirdpartyid, we need to only have valid/active equipment providers in this table.
- dgh 05/12/2010
*/


DELETE FROM Marketing.EquipmentProvider
WHERE Name IN 
(
    'AutoTrader',
    'CarsDotCom'
)
GO



