/*
Add some Url columns that will be used by publishing.
*/
ALTER TABLE Marketing.ValueAnalyzerVehiclePreference
ADD ShortUrl VARCHAR(100) NULL, 
    LongUrl VARCHAR(2083) NULL	-- IE7,8 url limit - had to pick a number.


