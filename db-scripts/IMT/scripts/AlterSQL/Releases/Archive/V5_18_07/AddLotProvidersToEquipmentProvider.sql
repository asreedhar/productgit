/*
Add the Lot Providers to the EquipmentProvider table.
GetAuto was already there.
- dgh 05/12/2010
*/


INSERT INTO Marketing.EquipmentProvider( EquipmentProviderID , Name )
VALUES  ( 9, 'DealerPeak')
GO

INSERT INTO Marketing.EquipmentProvider( EquipmentProviderID , Name )
VALUES  ( 10, 'eBizAutos')
GO

INSERT INTO Marketing.EquipmentProvider( EquipmentProviderID , Name )
VALUES  ( 11, 'HomeNet')
GO

INSERT INTO Marketing.EquipmentProvider( EquipmentProviderID , Name )
VALUES  ( 12, 'CDMData')
GO

INSERT INTO Marketing.EquipmentProvider( EquipmentProviderID , Name )
VALUES  ( 13, 'AULTec')
GO