
EXEC sp_rename 'Marketing.MarketAnalysisVehiclePreference.VehicleID', 'VehicleEntityId', 'COLUMN';
GO

ALTER TABLE Marketing.MarketAnalysisVehiclePreference
ADD VehicleEntityTypeId TINYINT
GO

UPDATE mavp
SET VehicleEntityTypeId = 1
FROM Marketing.MarketAnalysisVehiclePreference mavp
JOIN market.pricing.owner o ON o.OwnerID = mavp.OwnerId
JOIN imt.dbo.Inventory i ON i.InventoryID = mavp.VehicleEntityId
GO

DELETE FROM marketing.MarketAnalysisVehiclePreference WHERE VehicleEntityTypeId IS NULL
GO

ALTER TABLE Marketing.MarketAnalysisVehiclePreference 
    ALTER COLUMN VehicleEntityTypeId TINYINT NOT NULL
GO


 