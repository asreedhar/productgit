ALTER TABLE [Marketing].[CertifiedVehicleBenefitLineItem] 
Add Rank int not null default(0)

GO

-- Update the line items to have the same rank as the underlying owner certified benefits
UPDATE cvbl 
	SET Rank = ocb.Rank 
	FROM certified.OwnerCertifiedProgramBenefit ocb 
	JOIN marketing.CertifiedVehicleBenefitLineItem cvbl 
	ON cvbl.OwnerCertifiedProgramBenefitID = ocb.OwnerCertifiedProgramBenefitID
