-- Add this column to store whether or not the user wants to override the price providers for this vehicle.
-- dh, 2010-04-27
ALTER TABLE Marketing.MarketAnalysisVehiclePreference
ADD HasOverriddenPriceProviders BIT NOT NULL 
CONSTRAINT DF_Marketing_MarketAnalysisVehiclePreference__HasOverriddenPriceProviders DEFAULT(0)
