-----------------------------------------------------------------------------------------------------------------------
--	Create the tables needed for the Consumer Highlights Dealer Preference Page for Sales Accelerator 1.1.
--
--      - CGC 04/07/2010
-----------------------------------------------------------------------------------------------------------------------

set nocount on
go


-----------------------------------------------------------------------------------------------------------------------
--	1.  Create Marketing.ConsumerHighlightPreference
--      a. Check Constraints 
--              - CK_Marketing_ConsumerHighlightPreference__Dates   
--              - CK_Marketing_ConsumerHighlightPreference__CircleRating
--      b. Foreign Keys
--              - FK_Marketing_ConsumerHighlightPreference__InsertMember
--              - FK_Marketing_ConsumerHighlightPreference__UpdateMember
-----------------------------------------------------------------------------------------------------------------------
create table [Marketing].[ConsumerHighlightPreference]
(        
    [OwnerId]                           [int] not null,
    [MinimumJDPowerCircleRating]        [float] not null,
    [InsertUserId]                      [int] not null,
    [InsertDate]                        [datetime] not null,
    [UpdateUserId]                      [int] null,
    [UpdateDate]                        [datetime] null,

    constraint [PK_Marketing_ConsumerHighlightPreference]
    primary key ([OwnerId]),
    
    constraint [FK_Marketing_ConsumerHighlightPreference__InsertMember]
    foreign key ([InsertUserID])
    references [dbo].[Member] ([MemberID]),
    
    constraint [FK_Marketing_ConsumerHighlightPreference__UpdateMember]
    foreign key ([UpdateUserID])
    references [dbo].[Member] ([MemberID]),
    
    constraint [CK_Marketing_ConsumerHighlightPreference__Dates]
    check ([UpdateDate] >= [InsertDate]),
    
    constraint [CK_Marketing_ConsumerHighlightPreference__CircleRating]
    check ([MinimumJDPowerCircleRating] = 1.0 OR   
           [MinimumJDPowerCircleRating] = 1.5 OR
           [MinimumJDPowerCircleRating] = 2.0 OR
           [MinimumJDPowerCircleRating] = 2.5 OR
           [MinimumJDPowerCircleRating] = 3.0 OR
           [MinimumJDPowerCircleRating] = 3.5 OR
           [MinimumJDPowerCircleRating] = 4.0 OR
           [MinimumJDPowerCircleRating] = 4.5 OR
           [MinimumJDPowerCircleRating] = 5.0)
)   
go


-----------------------------------------------------------------------------------------------------------------------
--	2.  Create Marketing.ConsumerHighlightSection
--      a. Check Constraints 
--              - CK_Marketing_ConsumerHighlightSection__Name
--              - CK_Marketing_ConsumerHighlightSection__Rank
--      b. Foreign Keys
--              - None
-----------------------------------------------------------------------------------------------------------------------
create table [Marketing].[ConsumerHighlightSection]
(    
    [ConsumerHighlightSectionId]    [int] identity(1,1) not null,
    [Name]                          [varchar](50) not null,   
    [DefaultRank]                   [int] not null,

    constraint [PK_Marketing_ConsumerHighlightSection]
    primary key ([ConsumerHighlightSectionId]),
    
    constraint [UQ_Marketing_ConsumerHighlightSection__Rank]
    unique ([DefaultRank]),
          
    constraint [CK_Marketing_ConsumerHighlightSection__Name]
    check (len([Name]) > 0),

    constraint [CK_Marketing_ConsumerHighlightSection__Rank]
    check ([DefaultRank] > 0)
)
go

insert into [Marketing].[ConsumerHighlightSection] (Name, DefaultRank) values ('JDPower.com Ratings', 1)
insert into [Marketing].[ConsumerHighlightSection] (Name, DefaultRank) values ('Experts & Awards', 2)
insert into [Marketing].[ConsumerHighlightSection] (Name, DefaultRank) values ('Vehicle History Report Highlights', 3)
insert into [Marketing].[ConsumerHighlightSection] (Name, DefaultRank) values ('Consumer Highlights', 4)
go


-----------------------------------------------------------------------------------------------------------------------
--	3.  Create Marketing.ConsumerHighlightSectionPreference
--      a. Check Constraints   
--              - CK_Marketing_ConsumerHighlightSectionPreference__Rank 
--              - CK_Marketing_ConsumerHighlightSectionPreference__Dates
--      b. Foreign Keys
--              - FK_Marketing_ConsumerHighlightSectionPreference__ConsumerHighlightSection
--              - FK_Marketing_ConsumerHighlightSectionPreference__InsertMember
--              - FK_Marketing_ConsumerHighlightSectionPreference__UpdateMember
--      c. Unique Constraints
--              - UQ_Marketing_ConsumerHighlightSectionPreference__OwnerRank
-----------------------------------------------------------------------------------------------------------------------
create table [Marketing].[ConsumerHighlightSectionPreference]
(
    [OwnerId]                     [int] not null,
    [ConsumerHighlightSectionId]  [int] not null,    
    [Rank]                        [int] not null,
    [InsertUserId]                [int] not null,
    [InsertDate]                  [datetime] not null,
    [UpdateUserId]                [int] null,
    [UpdateDate]                  [datetime] null,

    constraint [PK_Marketing_ConsumerHighlightSectionPreference]
    primary key ([OwnerId], [ConsumerHighlightSectionID]),
    
    constraint [FK_Marketing_ConsumerHighlightSectionPreference__ConsumerHighlightSection]
    foreign key ([ConsumerHighlightSectionId])
    references [Marketing].[ConsumerHighlightSection] ([ConsumerHighlightSectionId]),
    
    constraint [FK_Marketing_ConsumerHighlightSectionPreference__InsertMember]
    foreign key ([InsertUserID])
    references [dbo].[Member] ([MemberID]),
    
    constraint [FK_Marketing_ConsumerHighlightSectionPreference__UpdateMember]
    foreign key ([UpdateUserID])
    references [dbo].[Member] ([MemberID]),
    
    constraint [UQ_Marketing_ConsumerHighlightSectionPreference__OwnerRank]
    unique ([OwnerId], [Rank]),
    
    constraint [CK_Marketing_ConsumerHighlightSectionPreference__Rank]
    check ([Rank] >= 0),
    
    constraint [CK_Marketing_ConsumerHighlightSectionPreference__Dates]
    check ([UpdateDate] >= [InsertDate])
)
go


-----------------------------------------------------------------------------------------------------------------------
--	4.  Create Marketing.SnippetSourcePreference
--      a. Check Constraints    
--              - CK_Marketing_SnippetSourcePreference__Rank
--              - CK_Marketing_SnippetSourcePreference__Dates
--      b. Foreign Keys
--              - FK_Marketing_SnippetSourcePreference__InsertMember
--              - FK_Marketing_SnippetSourcePreference__UpdateMember
--      c. Unique Constraints
--              - UQ_Marketing_SnippetSourcePreference__OwnerRank
-----------------------------------------------------------------------------------------------------------------------
create table [Marketing].[SnippetSourcePreference]
(
    [OwnerId]          [int] not null,
    [SnippetSourceId]  [int] not null,
    [Rank]             [int] not null,    
    [IsDisplayed]      [bit] not null,    
    [InsertUserId]     [int] not null,
    [InsertDate]       [datetime] not null,
    [UpdateUserId]     [int] null,
    [UpdateDate]       [datetime] null,

    constraint [PK_Marketing_SnippetSourcePreference]
    primary key ([SnippetSourceID], [OwnerId]),      
    
    constraint [FK_Marketing_SnippetSourcePreference__InsertMember]
    foreign key ([InsertUserID])
    references [dbo].[Member] ([MemberID]),
    
    constraint [FK_Marketing_SnippetSourcePreference__UpdateMember]
    foreign key ([UpdateUserID])
    references [dbo].[Member] ([MemberID]),
    
    constraint [UQ_Marketing_SnippetSourcePreference__OwnerRank]
    unique ([OwnerId], [Rank], [IsDisplayed]),
    
    constraint [CK_Marketing_SnippetSourcePreference__Rank]
    check ([Rank] >= 0),
    
    constraint [CK_Marketing_SnippetSourcePreference__Dates]
    check ([UpdateDate] >= [InsertDate])
)
go

