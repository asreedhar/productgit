/*
The name has officially changed.  From this point forward, she shall be known as 'Max Marketing'.
*/

UPDATE	dbo.lu_DealerUpgrade
SET	DealerUpgradeDESC = 'Max Marketing'
WHERE	DealerUpgradeCD = 23
