ALTER TABLE dbo.tbl_Vehicle DROP CONSTRAINT DF_tbl_Vehicle__BodyTypeID

ALTER TABLE dbo.tbl_Vehicle ALTER COLUMN BodyTypeID TINYINT NOT NULL 
ALTER TABLE dbo.tbl_Vehicle ADD CONSTRAINT DF_tbl_Vehicle__BodyTypeID DEFAULT(0) FOR BodyTypeID
GO


ALTER TABLE dbo.tbl_Vehicle ALTER COLUMN DoorCount TINYINT NULL 
ALTER TABLE dbo.tbl_Vehicle ALTER COLUMN CylinderCount TINYINT NULL 
GO

ALTER TABLE dbo.tbl_Vehicle DROP CONSTRAINT DF_tbl_Vehicle_VehicleSourceID
ALTER TABLE dbo.tbl_Vehicle DROP CONSTRAINT FK_tbl_Vehicle_VehicleSources
GO

ALTER TABLE dbo.VehicleSources DROP CONSTRAINT PK_VehicleSources
ALTER TABLE dbo.VehicleSources ALTER COLUMN VehicleSourceID TINYINT NOT NULL

ALTER TABLE dbo.VehicleSources ADD CONSTRAINT [PK_VehicleSources] PRIMARY KEY CLUSTERED ([VehicleSourceID])

ALTER TABLE dbo.tbl_Vehicle ALTER COLUMN VehicleSourceID TINYINT NOT NULL 
ALTER TABLE dbo.tbl_Vehicle ADD CONSTRAINT DF_tbl_Vehicle__VehicleSourceID DEFAULT(1) FOR VehicleSourceID
GO

ALTER TABLE dbo.tbl_Vehicle ADD CONSTRAINT FK_tbl_Vehicle__VehicleSources FOREIGN KEY (VehicleSourceID) REFERENCES dbo.VehicleSources (VehicleSourceID)
GO

ALTER INDEX PK_tbl_Vehicle ON tbl_Vehicle REBUILD 
GO