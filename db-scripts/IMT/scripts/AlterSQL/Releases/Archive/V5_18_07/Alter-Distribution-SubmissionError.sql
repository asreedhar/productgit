/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *  Fogbugz ID: 10999
 *	Adding MessageTypeFlag column to Distribution.SubmissionError table for logging of which
 *  kind of messages have failed.
 * 
 * History
 * ----------
 *  
 * CGC	06/17/2010	Added message type.
 * 						
 * ------------------------------------------------------------------------------------------- */

alter table [Distribution].[SubmissionError]
add [MessageTypeFlag] int null
go

update [Distribution].[SubmissionError]
set [MessageTypeFlag] = 0
go

alter table [Distribution].[SubmissionError]
alter column [MessageTypeFlag] int not null
go

update [Distribution].[SubmissionErrorType]
set [ConcernsUser] = 0 
where [ErrorTypeID] = 4
go
