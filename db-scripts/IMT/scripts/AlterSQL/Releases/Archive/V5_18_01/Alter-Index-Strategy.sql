
ALTER TABLE dbo.InventoryBucketRanges DROP CONSTRAINT PK_InventoryBucketRanges
GO


CREATE CLUSTERED INDEX IX_InventoryBucketRanges__BusinessUnitIDInventoryBucketIDRangeID ON dbo.InventoryBucketRanges
(	BusinessUnitID,
	InventoryBucketID, 
	RangeID
	
	) 
		
GO

ALTER TABLE dbo.InventoryBucketRanges ADD CONSTRAINT PK_InventoryBucketRanges PRIMARY KEY NONCLUSTERED (InventoryBucketRangeID)
GO

