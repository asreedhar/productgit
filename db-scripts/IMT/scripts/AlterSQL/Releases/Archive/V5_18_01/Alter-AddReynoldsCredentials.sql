if not exists (select * from syscolumns where id=object_id('dbo.BusinessUnitCredential') and name='ReynoldsAreaNumber')
ALTER TABLE dbo.BusinessUnitCredential ADD ReynoldsAreaNumber INT NULL

if not exists (select * from syscolumns where id=object_id('dbo.BusinessUnitCredential') and name='ReynoldsDealerNumber')
ALTER TABLE dbo.BusinessUnitCredential ADD ReynoldsDealerNumber varchar(20) NULL

if not exists (select * from syscolumns where id=object_id('dbo.BusinessUnitCredential') and name='ReynoldsStoreNumber')
ALTER TABLE dbo.BusinessUnitCredential ADD ReynoldsStoreNumber INT NULL
