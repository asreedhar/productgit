INSERT INTO dbo.PING_GroupingCodes VALUES ( 85 , 103730 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 85 , 104056 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 85 , 103286 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 82 , 101706 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 103 , 102665 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 84 , 103859 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 110 , 101709 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 104 , 101712 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 101 , 103865 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 82 , 101707 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 84 , 104000 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 120 , 101710 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 85 , 101701 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 120 , 103728 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 9194 , 104120 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 9231 , 104134 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 82 , 104057 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 85 , 101704 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 9194 , 104129 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 85 , 101702 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 110 , 103989 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 96 , 101705 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 113 , 103969 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 101 , 101711 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 82 , 103729 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 85 , 103858 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 97 , 103984 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 82 , 103274 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 9231 , 104176 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 110 , 103878 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 85 , 104001 ) ;
INSERT INTO dbo.PING_GroupingCodes VALUES ( 110 , 101708 ) ;

GO

DECLARE @newId INT

INSERT INTO PING_Code VALUES ('X6', '9231', 2, 80, 'D');

SELECT @newId = SCOPE_IDENTITY()

INSERT INTO dbo.PING_GroupingCodes VALUES ( @newId , 104134 ) ;

INSERT INTO dbo.PING_GroupingCodes VALUES ( @newId , 104176 ) ;

GO

DECLARE @newId INT

INSERT INTO PING_Code VALUES ('128', '615', 2, 80, 'D');

SELECT @newId = SCOPE_IDENTITY()

INSERT INTO dbo.PING_GroupingCodes VALUES ( @newId , 104120 ) ;
GO

DECLARE @newId INT

INSERT INTO PING_Code VALUES ('135', '9194', 2, 80, 'D');

SELECT @newId = SCOPE_IDENTITY()

INSERT INTO dbo.PING_GroupingCodes VALUES ( @newId , 104129 ) ;
GO