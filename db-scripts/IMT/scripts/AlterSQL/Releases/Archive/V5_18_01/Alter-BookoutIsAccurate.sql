-- 
-- 10/16/08
-- nk - reimplementing isAccurate as a computed column

ALTER TABLE dbo.Bookouts
DROP CONSTRAINT DF_Bookouts_IsAccurate
GO
ALTER TABLE dbo.Bookouts
DROP COLUMN IsAccurate 
GO 
ALTER TABLE dbo.Bookouts
ADD IsAccurate AS CAST( CASE WHEN BookoutStatusID = 1 THEN 1 ELSE 0 END AS TinyInt)
GO
