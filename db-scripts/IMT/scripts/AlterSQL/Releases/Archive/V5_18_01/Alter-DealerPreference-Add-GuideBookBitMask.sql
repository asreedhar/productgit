ALTER TABLE dbo.DealerPreference ADD GuideBookBitMask AS (CAST(POWER(2,GuideBookID-1) | COALESCE(POWER(2, GuideBook2ID-1),0) AS TINYINT))	-- SECOND BOOK CAN BE NULL
GO

UPDATE	I
SET	I.BookoutRequired = DP.GuideBookBitMask
FROM	Inventory I
	JOIN DealerPreference DP ON I.BusinessUnitID = DP.BusinessUnitID
WHERE	I.BookoutRequired = 1	
	AND I.InventoryActive = 1
	
GO