
-----------------------------------------------------------------------------------------------------------------------------
--	BookoutThirdPartyVehicles
-----------------------------------------------------------------------------------------------------------------------------

ALTER TABLE dbo.BookoutThirdPartyVehicles DROP CONSTRAINT PK_BookoutThirdPartyVehicles
GO
ALTER TABLE dbo.BookoutThirdPartyVehicles ADD CONSTRAINT PK_BookoutThirdPartyVehicles PRIMARY KEY CLUSTERED (BookoutID, ThirdPartyVehicleID)
GO

DROP INDEX dbo.BookoutThirdPartyVehicles.IX_BookoutThirdPartyVehicles__BookoutID
GO

-----------------------------------------------------------------------------------------------------------------------------
--	BookoutValues
-----------------------------------------------------------------------------------------------------------------------------

ALTER TABLE dbo.BookoutValues DROP CONSTRAINT PK_BookoutValues
GO
ALTER TABLE dbo.BookoutValues ADD CONSTRAINT PK_BookoutValues PRIMARY KEY NONCLUSTERED (BookoutValueID) ON DATA
GO
CREATE CLUSTERED INDEX IXC_BookoutValues_BookoutThirdPartyCategoryID ON dbo.BookoutValues (BookoutThirdPartyCategoryID, BookoutValueTypeID)
GO
DROP INDEX dbo.BookoutValues.IX_BookoutValues_ValueTypeCategoryID
GO


DROP INDEX dbo.MakeModelGrouping.IX_MakeModelGrouping__GroupingDescriptionID_MakeModelGroupingID
GO

ALTER TABLE dbo.tbl_Vehicle DROP CONSTRAINT FK_tbl_Vehicle_MakeModelGrouping

ALTER TABLE dbo.MakeModelGrouping DROP CONSTRAINT PK_MakeModelGrouping
ALTER TABLE dbo.MakeModelGrouping ADD CONSTRAINT PK_MakeModelGrouping PRIMARY KEY CLUSTERED (MakeModelGroupingID)

ALTER TABLE dbo.tbl_Vehicle ADD CONSTRAINT FK_tbl_Vehicle_MakeModelGrouping FOREIGN KEY (MakeModelGroupingID) REFERENCES dbo.MakeModelGrouping(MakeModelGroupingID)

GO
/*

UQ__HalSessions__6DC3C210
PK_SearchCandidateCIACategoryAnnotation
PK_CIAGroupingItemDetailLevel
PK_SearchCandidateList
IX_CIASummary_BusinessUnitID
IX_CIABodyTypeDetails__CIASummaryID
IX_CIAGroupingItems_CIAGBodyTypeDetailIdCIAGroupingItemIdGroupingDescriptionId
IX_CIAGroupingItemDetailsGroupingItemIdGroupingItemDetailLevelIdGroupingItemDetailTypeId
IX_BookoutThirdPartyVehicles__BookoutID

PK_InventoryBucketRanges
UQ_InventoryBucketRanges

*/