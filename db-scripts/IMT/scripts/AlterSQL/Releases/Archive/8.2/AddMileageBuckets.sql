-- *-------------------------------------------------------* --
-- | Add Mileage Adjustment Table for Market Pricing Power | --
-- *-------------------------------------------------------* --
INSERT INTO dbo.InventoryBuckets VALUES('PING II Mileage Adjustment', 2)
GO

-- *--------------------------* --
-- | Fetch the Incremented ID | --
-- *--------------------------* --
DECLARE @MileageBucketID INT
SELECT @MileageBucketID = InventoryBucketID
FROM dbo.InventoryBuckets
WHERE Description = 'PING II Mileage Adjustment'

INSERT INTO dbo.InventoryBucketRanges (InventoryBucketID, RangeID, Description, LOW, HIGH, Lights, BusinessUnitID, LongDescription, SortOrder, Value1, Value2)
 SELECT @MileageBucketID, 1, '0 - 10387'    ,     0, 10387, 7, 100150, NULL, 1,  6,  9 
 UNION
 SELECT @MileageBucketID, 2, '10388 - 12386', 10388, 12386, 7, 100150, NULL, 1,  3,  9
 UNION
 SELECT @MileageBucketID, 3, '12387 - 14386', 12387, 14386, 7, 100150, NULL, 1, -2, -1
 UNION
 SELECT @MileageBucketID, 4, '14387 - 16386', 14387, 16386, 7, 100150, NULL, 1, -3, -2
 UNION
 SELECT @MileageBucketID, 5, '16387 - 18386', 16387, 18386, 7, 100150, NULL, 1, -6, -5
 UNION
 SELECT @MileageBucketID, 6, '18387 - 20386', 18387, 20386, 7, 100150, NULL, 1, -9, -6
 UNION
 SELECT @MileageBucketID, 7, '20387 +'      , 20387,  null, 7, 100150, NULL, 1, -11, -8
GO

