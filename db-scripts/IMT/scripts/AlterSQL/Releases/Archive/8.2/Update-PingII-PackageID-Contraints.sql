--------------------------------------------------------------
-- Update Constraint to allow Market Pricing Power, PM
--------------------------------------------------------------
ALTER TABLE [dbo].[DealerPreference_Pricing] DROP CONSTRAINT [CK_DealerPreference_Pricing_PackageID]

ALTER TABLE [dbo].[DealerPreference_Pricing]  WITH CHECK ADD  CONSTRAINT [CK_DealerPreference_Pricing_PackageID] CHECK  (([PackageID]>=(1) AND [PackageID]<=(8)))
GO

