ALTER TABLE dbo.InternetAdvertiserDealership
ADD IncrementalExport bit CONSTRAINT [DF_InternetAdvertiserDealership_IncExport] Default 0 NOT NULL;

ALTER TABLE dbo.InternetAdvertiserDealership
ADD ExportClientID int;

ALTER TABLE dbo.InternetAdvertiserDealership
ADD ExportGuid varchar(36);

INSERT INTO InternetAdvertiserExportStatus
SELECT 20, 'Incremental Export, set By User'
UNION
SELECT 21, 'Incremental and Full Export, set by User'