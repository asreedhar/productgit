----------------------------------------------------------------------------------------
--
--	SOME LONG-NEEDED TABLE MAINTENANCE.
--
--	THE DateCreated COLUMN ISN'T REALLY USED, AND THE Value COLUMN DOESN'T NEED TO 
--	BE AN INT -- IF OPTIONS ON YOUR CAR COST MORE THAN 32767, YA DON'T NEED OUR
--	PRODUCT.
--
--	ALSO, CHANGE THE INDEXING STRAT TO MATCH THE TRUE ACCESS PATTERN.  LEAVE THE 
--	SURROGATE KEY IN FOR HIBERNATE.
--
----------------------------------------------------------------------------------------

DROP INDEX ThirdPartyVehicleOptionValues.IX_ThirdPartyVehicleOptionValues__OptionID_ValueTypeID_Value	

ALTER TABLE dbo.ThirdPartyVehicleOptionValues ALTER COLUMN Value SMALLINT NULL
ALTER TABLE dbo.ThirdPartyVehicleOptionValues DROP CONSTRAINT [DF_ThirdPartyVehicleOptionValues_DateCreated]
ALTER TABLE dbo.ThirdPartyVehicleOptionValues DROP COLUMN DateCreated

ALTER TABLE dbo.ThirdPartyVehicleOptionValues DROP CONSTRAINT PK_VehicleThirdPartyVehicleOptionValues
GO
CREATE CLUSTERED INDEX IX_ThirdPartyVehicleOptionValues__ThirdPartyVehicleOptionID ON ThirdPartyVehicleOptionValues
(	ThirdPartyVehicleOptionID,
	ThirdPartyOptionValueTypeID
	)	

ALTER TABLE dbo.ThirdPartyVehicleOptionValues ADD CONSTRAINT PK_ThirdPartyVehicleOptionValues PRIMARY KEY NONCLUSTERED (ThirdPartyVehicleOptionValueID)
GO
