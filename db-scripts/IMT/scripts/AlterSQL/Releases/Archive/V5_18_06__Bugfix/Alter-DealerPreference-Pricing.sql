
ALTER TABLE dbo.DealerPreference_Pricing ADD SuppressTrimMatchStatus BIT NULL
GO

UPDATE dbo.DealerPreference_Pricing SET SuppressTrimMatchStatus = 1
GO
