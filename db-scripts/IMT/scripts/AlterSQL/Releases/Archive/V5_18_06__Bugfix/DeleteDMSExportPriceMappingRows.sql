
-- Create a table for the list of mapping ids that we will keep.
-- All others get axed.
DECLARE @KeepMappingIds TABLE(MappingID INT)

-- Load the table with the safe mapping ids.
INSERT INTO @KeepMappingIds( MappingID )
SELECT DMSExportPriceMappingID
FROM dbo.DMSExportPriceMapping
WHERE Description IN
(
'LIST_PRICE',    
'INVOICE_AMT',    
'SLS_PRICE',    
'SLS_COST',    
'STICKER_PRICE',
'DRAFT_AMOUNT',    
'LOCATION',
'MEMO1',
'MEMO2',
'CODED_COST'
)

IF @@ROWCOUNT <> 10
BEGIN
    RAISERROR('An unexpected number of rows were found in the DMSExportPriceMapping table.', 16, 1);
    GOTO Failed
END

-- Set the DMSExportInternetPriceMappingIDs and DMSExportLotPriceMappingIDs of those business unit preference 
-- rows that reference a soon-to-be-deleted (STBD) mapping id to NULL to satisfy the FK Constraint.

BEGIN TRANSACTION DeleteMappings

-- update DMSExportPriceMapping.DMSExportInternetPriceMappingID
UPDATE bup
SET DMSExportInternetPriceMappingID = NULL
FROM dbo.DMSExportPriceMapping pm
JOIN dbo.DMSExportBusinessUnitPreference bup
    ON bup.DMSExportInternetPriceMappingID = pm.DMSExportPriceMappingID
LEFT JOIN @KeepMappingIds k
    ON k.MappingID = bup.DMSExportInternetPriceMappingID
WHERE k.MappingID IS NULL

IF @@ERROR <> 0 GOTO Failed


-- update DMSExportPriceMapping.DMSExportLotPriceMappingID
UPDATE bup
SET DMSExportLotPriceMappingID = NULL
FROM dbo.DMSExportPriceMapping pm
JOIN dbo.DMSExportBusinessUnitPreference bup
    ON bup.DMSExportLotPriceMappingID = pm.DMSExportPriceMappingID
LEFT JOIN @KeepMappingIds k
    ON k.MappingID = bup.DMSExportLotPriceMappingID
WHERE k.MappingID IS NULL

IF @@ERROR <> 0 GOTO Failed

-- Now, safely delete the reference mapping ids.
DELETE pm
FROM dbo.DMSExportPriceMapping pm
LEFT JOIN @KeepMappingIds k
ON k.MappingID = pm.DMSExportPriceMappingID
WHERE k.MappingID IS NULL

IF @@ERROR <> 0 GOTO Failed

GOTO Succeeded

Failed:
PRINT 'Failed, transaction (if exists) is being rolled back.'
IF @@TRANCOUNT = 1 ROLLBACK TRANSACTION DeleteMappings
RETURN

Succeeded:
PRINT 'Succeeded, transaction is being committed.'
COMMIT TRANSACTION DeleteMappings
RETURN

