---Description--------------------------------------------------------------------------------------
--
-- 	dbo.CarfaxProcessorStatus: Valid status states the CarFaxProcessor can be in. 
--
--	dbo.CarfaxProcessorRunLog: Each row corresponds to a business unit and a vehicleEntityType
--  (Inventory or appraisal) that corresponds to a unit of work for the carFaxProcessor.
--
--  dbo.CarfaxProcessorErrorDetail: This has an FK to dbo.CarfaxProcessorRunLog. Each row describes
--  the details of any errors encountered while trying to retreieve and save the results of a 
--	carFaxReport request and save.
-- 
--  Also, this is hillarious: http://i44.tinypic.com/oh1mh0.gif
---History------------------------------------------------------------------------------------------
--	
--	NK	01/30/09	Initial design/developemt
--
----------------------------------------------------------------------------------------------------	

CREATE TABLE dbo.CarfaxProcessorStatus
	(
		CarfaxProcessorStatusId	int Identity(1,1) NOT NULL,
		Description				varchar(64) NOT NULL
	)  
GO
ALTER TABLE dbo.CarfaxProcessorStatus ADD CONSTRAINT PK_CarfaxProcessorStatus PRIMARY KEY NONCLUSTERED (CarfaxProcessorStatusId)
GO
INSERT INTO dbo.CarfaxProcessorStatus VALUES ('Loaded');
INSERT INTO dbo.CarfaxProcessorStatus VALUES ('InProgress');
INSERT INTO dbo.CarfaxProcessorStatus VALUES ('Successful');
INSERT INTO dbo.CarfaxProcessorStatus VALUES ('Failed');
GO


CREATE TABLE dbo.CarfaxProcessorRunLog
(
	CarfaxProcessorRunLogId	int Identity(1,1) NOT NULL,
	BusinessUnitId			int NOT NULL,
	VehicleEntityTypeID		int NOT NULL,
	StatusId				int NOT NULL,
	LoadTime				smalldatetime NULL,
	StartTime				smalldatetime NULL,
	EndTime					smalldatetime NULL
)  
GO
ALTER TABLE dbo.CarfaxProcessorRunLog ADD CONSTRAINT PK_CarfaxProcessorRunLog PRIMARY KEY NONCLUSTERED (CarfaxProcessorRunLogId)
GO
ALTER TABLE dbo.CarfaxProcessorRunLog ADD CONSTRAINT FK_CarfaxProcessorRunLog__CarfaxProcessorStatus FOREIGN KEY  (StatusId) REFERENCES dbo.CarfaxProcessorStatus(CarfaxProcessorStatusId)
GO

CREATE TABLE dbo.CarfaxProcessorErrorDetail
	(
		CarfaxProcessorErrorDetailId	int Identity(1,1) NOT NULL,
		CarfaxProcessorRunLogId			int NOT NULL,
		BusinessUnitId					int NOT NULL,
		Vin								varchar(17) NOT NULL,
		Description						varchar(2000),
		FailureTime						smalldatetime NULL
	)  
GO
ALTER TABLE dbo.CarfaxProcessorErrorDetail ADD CONSTRAINT PK_CarfaxProcessorErrorDetail PRIMARY KEY NONCLUSTERED (CarfaxProcessorErrorDetailId)
GO
ALTER TABLE dbo.CarfaxProcessorErrorDetail ADD CONSTRAINT FK_CarfaxProcessorErrorDetail_CarfaxProcessorRunLog FOREIGN KEY  (CarfaxProcessorRunLogId) REFERENCES dbo.CarfaxProcessorRunLog(CarfaxProcessorRunLogId)
GO