
ALTER TABLE dbo.DealerPreference_InternetAdvertisementBuilder
ADD SpacerText VARCHAR(5) NULL;

ALTER TABLE dbo.DealerPreference_InternetAdvertisementBuilder
ADD HighValueEquipmentThreshold SMALLINT NULL;

ALTER TABLE dbo.DealerPreference_InternetAdvertisementBuilder
ADD HighValueEquipmentPrefix VARCHAR(150) NULL;

ALTER TABLE dbo.DealerPreference_InternetAdvertisementBuilder
ADD StandardEquipmentPrefix VARCHAR(150) NULL;
