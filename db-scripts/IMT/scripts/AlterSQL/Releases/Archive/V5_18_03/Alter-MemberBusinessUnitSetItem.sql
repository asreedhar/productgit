
-- SAME SQL AS V5_15

DELETE	I
FROM	dbo.MemberBusinessUnitSetItem I
JOIN	dbo.MemberBusinessUnitSet S ON S.MemberBusinessUnitSetID = I.MemberBusinessUnitSetID
JOIN	dbo.Member M ON S.MemberID = M.MemberID
WHERE	NOT EXISTS (
			SELECT	1
			FROM	dbo.MemberAccess MA
			WHERE	MA.MemberID = S.MemberID AND MA.BusinessUnitID = I.BusinessUnitID
		)
AND	M.MemberType = 2 -- user

-- NEW SQL FOR ADMIN USERS

DELETE	I
FROM	dbo.MemberBusinessUnitSetItem I
JOIN	dbo.MemberBusinessUnitSet S ON S.MemberBusinessUnitSetID = I.MemberBusinessUnitSetID
JOIN	dbo.Member M ON S.MemberID = M.MemberID
WHERE	NOT EXISTS (
			SELECT	1
			FROM	dbo.BusinessUnit B
			JOIN	dbo.BusinessUnitRelationship R ON R.BusinessUnitID = B.BusinessUnitID
			WHERE	B.BusinessUnitID = I.BusinessUnitID
			AND	S.BusinessUnitID = R.ParentID
			AND	B.BusinessUnitTypeID = 4
			AND	B.Active = 1
		)
AND	M.MemberType = 1 -- admin
