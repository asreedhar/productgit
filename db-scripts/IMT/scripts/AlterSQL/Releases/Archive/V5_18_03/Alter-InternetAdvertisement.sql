SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[InternetAdvertisement](
	[InventoryID] [int] NOT NULL,
	[AdvertisementText] [varchar](2000) NULL,
	[UpdateUser] [varchar](255) NOT NULL CONSTRAINT [DF_InternetAdvertisement__LastUpdatedBy] DEFAULT (suser_sname()),
	[UpdateDate] [datetime] NOT NULL CONSTRAINT [DF_InternetAdvertisement__UpdateDate]  DEFAULT (getdate()),
	CONSTRAINT [PK_InternetAdvertisement] PRIMARY KEY CLUSTERED 
	(
		[InventoryID] ASC
	) 
)
GO

ALTER TABLE [dbo].[InternetAdvertisement]  WITH CHECK ADD  CONSTRAINT [FK_InternetAdvertisement_InventoryID] FOREIGN KEY([InventoryID])
REFERENCES [dbo].[Inventory] ([InventoryID])
GO
ALTER TABLE [dbo].[InternetAdvertisement] CHECK CONSTRAINT [FK_InternetAdvertisement_InventoryID]
GO

CREATE TABLE dbo.InternetAdvertisementBuilderOptionProvider (
	Id INT IDENTITY(1,1) NOT NULL,
	Description VARCHAR(50) NOT NULL,
	CONSTRAINT [PK_InternetAdvertisementBuilderOptionProvider] PRIMARY KEY (
		Id ASC
	)
)
GO

EXEC sys.sp_addextendedproperty 
@name = N'MS_Description', 
@value = N'Metadata describing available data to include in a system generated advertisement', 
@level0type = N'SCHEMA', @level0name = dbo, 
@level1type = N'TABLE',  @level1name = InternetAdvertisementBuilderOptionProvider;
 GO

INSERT INTO dbo.InternetAdvertisementBuilderOptionProvider ([Description])
VALUES('Primary GuideBook Options')
GO

INSERT INTO dbo.InternetAdvertisementBuilderOptionProvider ([Description])
VALUES('Secondary GuideBook Options')
GO

CREATE TABLE dbo.DealerPreference_InternetAdvertisementBuilder (
	BusinessUnitID INT NOT NULL,
	EnableBuilder BIT NOT NULL CONSTRAINT [DF_DP_IAB_EnableBuilder] DEFAULT(1),
	EnableTagLine BIT NOT NULL CONSTRAINT [DF_DP_IAB_EnableTagLine] DEFAULT(0),
	TagLine VARCHAR(500) NULL,
	CONSTRAINT [PK_DP_InternetAdvertisementBuilder_BusinessUnitID] PRIMARY KEY (
		BusinessUnitID ASC
	)
)
GO

ALTER TABLE dbo.DealerPreference_InternetAdvertisementBuilder
WITH CHECK ADD CONSTRAINT [FK_DP_InternetAdvertisementBuilder_BusinessUnitID] FOREIGN KEY([BusinessUnitID])
REFERENCES dbo.BusinessUnit (BusinessUnitID)
GO

EXEC sys.sp_addextendedproperty 
@name = N'MS_Description', 
@value = N'Internet Advertisement preferences at a Dealer level', 
@level0type = N'SCHEMA', @level0name = dbo, 
@level1type = N'TABLE',  @level1name = DealerPreference_InternetAdvertisementBuilder;
GO

EXEC sys.sp_addextendedproperty 
@name = N'MS_Description', 
@value = N'Specifies if automatic generation of an advertisement is on or off', 
@level0type = N'SCHEMA', @level0name = dbo, 
@level1type = N'TABLE',  @level1name = DealerPreference_InternetAdvertisementBuilder,
@level2type = N'COLUMN', @level2name = EnableBuilder;
GO

CREATE TABLE dbo.DealerInternetAdvertisementBuilderOptionProvider (
	BusinessUnitID INT NOT NULL,
	InternetAdvertisementBuilderOptionProviderID INT NOT NULL,
	CONSTRAINT [PK_DealerInternetAdvertisementBuilderOptionProvider] PRIMARY KEY (
		[BusinessUnitID] ASC
	)
)
GO

ALTER TABLE dbo.DealerInternetAdvertisementBuilderOptionProvider
WITH CHECK ADD CONSTRAINT [FK_DealerInternetAdvertisementBuilderOptionProvider_BusinessUnitID] FOREIGN KEY([BusinessUnitID])
REFERENCES dbo.DealerPreference_InternetAdvertisementBuilder (BusinessUnitID)
GO

ALTER TABLE dbo.DealerInternetAdvertisementBuilderOptionProvider
WITH CHECK ADD CONSTRAINT [FK_DealerInternetAdvertisementBuilderOptionProvider_InternetAdvertisementBuilderOptionProviderID] FOREIGN KEY([InternetAdvertisementBuilderOptionProviderID])
REFERENCES dbo.InternetAdvertisementBuilderOptionProvider (Id)
GO

EXEC sys.sp_addextendedproperty 
@name = N'MS_Description',
@value = N'The provider specified to retrieve vehicle options to use in auto advertisment generation', 
@level0type = N'SCHEMA', @level0name = dbo, 
@level1type = N'TABLE',  @level1name = DealerInternetAdvertisementBuilderOptionProvider;
GO
