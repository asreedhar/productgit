
-- dodge ram
INSERT INTO dbo.PING_GroupingCodes
	SELECT	id, 102663 
	FROM	Ping_code
	WHERE	providerId=2
	AND		name='ram'
	AND		parentId = 
			(Select id 
			from Ping_code
			where providerId=2
			and type='K'
			and name='Dodge')

-- sierra
INSERT INTO dbo.PING_GroupingCodes
	SELECT	id, 103964 
	FROM	Ping_code
	WHERE	providerId=2
	AND		name='sierra'
	AND		parentId = 
			(Select id 
			from Ping_code
			where providerId=2
			and type='K'
			and name='GMC')

-- yukon
INSERT INTO dbo.PING_GroupingCodes
	SELECT	id, 102068 
	FROM	Ping_code
	WHERE	providerId=2
	AND		name='yukon'
	AND		parentId = 
			(Select id 
			from Ping_code
			where providerId=2
			and type='K'
			and name='GMC')
			
-- delete erroneous mapping			
delete from dbo.PING_GroupingCodes
where codeId=361 and makeModelGroupingId=102663			
