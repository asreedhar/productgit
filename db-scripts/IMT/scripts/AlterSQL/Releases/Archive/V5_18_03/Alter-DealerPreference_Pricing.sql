
ALTER TABLE dbo.DealerPreference_Pricing ADD PackageID TINYINT NULL
GO

UPDATE dbo.DealerPreference_Pricing SET PackageID = 4
GO

ALTER TABLE dbo.DealerPreference_Pricing ALTER COLUMN PackageID TINYINT NOT NULL
GO

ALTER TABLE dbo.DealerPreference_Pricing ADD CONSTRAINT CK_DealerPreference_Pricing_PackageID CHECK (PackageID BETWEEN 1 AND 6)
GO
