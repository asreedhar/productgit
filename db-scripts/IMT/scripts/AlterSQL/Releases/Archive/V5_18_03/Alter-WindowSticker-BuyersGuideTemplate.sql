ALTER TABLE [dbo].[WindowStickerTemplate] WITH CHECK ADD 
CONSTRAINT [PK_WindowStickerTemplate] PRIMARY KEY CLUSTERED ([WindowStickerTemplateId] ASC)
GO

CREATE TABLE [dbo].[WindowStickerBuyersGuideTemplate] (
	WindowStickerBuyersGuideTemplateId INT NOT NULL,
	Name VARCHAR(100) NOT NULL,
	CONSTRAINT [PK_WindowStickerBuyersGuideTemplate] PRIMARY KEY CLUSTERED (
		WindowStickerBuyersGuideTemplateId ASC
	)
)
GO

INSERT INTO WindowStickerBuyersGuideTemplate(WindowStickerBuyersGuideTemplateId, Name)
SELECT 1, 'As-Is No Warranty'
UNION SELECT 2, 'Implied Warranties'
GO

CREATE TABLE [dbo].[DealerPreference_WindowSticker] (
	BusinessUnitID INT NOT NULL,
	WindowStickerTemplateId INT NOT NULL,
	WindowStickerBuyersGuideTemplateId INT NOT NULL CONSTRAINT [DF_DP_WindowStickerBuyingGuideTemplateId] DEFAULT(1),
	CONSTRAINT [PK_DP_WindowSticker_BusinessUnitID] PRIMARY KEY CLUSTERED (
		BusinessUnitID ASC
	)
)
GO

ALTER TABLE [dbo].[DealerPreference_WindowSticker]  WITH CHECK ADD 
CONSTRAINT [FK_DP_WindowSticker_BusinessUnitID] FOREIGN KEY([BusinessUnitID])
REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO

ALTER TABLE [dbo].[DealerPreference_WindowSticker]  WITH CHECK ADD 
CONSTRAINT [FK_DP_WindowSticker_WindowStickerTemplate] FOREIGN KEY([WindowStickerTemplateId])
REFERENCES [dbo].[WindowStickerTemplate] ([WindowStickerTemplateId])
GO

ALTER TABLE [dbo].[DealerPreference_WindowSticker]  WITH CHECK ADD 
CONSTRAINT [FK_DP_WindowSticker_WindowStickerBuyersGuideTemplate] FOREIGN KEY([WindowStickerBuyersGuideTemplateId])
REFERENCES [dbo].[WindowStickerBuyersGuideTemplate] ([WindowStickerBuyersGuideTemplateId])
GO

--Migrate data over from DealerPreference main table
--filter out non real values in dealer preference
--BuyingGuide has a default
INSERT INTO dbo.DealerPreference_WindowSticker (
	BusinessUnitID, WindowStickerTemplateId
)
SELECT BusinessUnitID, WindowStickerTemplate
FROM dbo.DealerPreference P
JOIN dbo.WindowStickerTemplate T
	ON P.WindowStickerTemplate = T.WindowStickerTemplateId
GO

--This column had an unnamed default constraint!
DECLARE @ConstraintName VARCHAR(50)

SELECT @ConstraintName = obj.name
FROM sys.columns col
LEFT JOIN sys.objects obj
	ON obj.object_id = col.default_object_id and obj.type = 'D'
WHERE col.object_id = object_id(N'dbo.DealerPreference')
AND obj.name IS NOT NULL
AND col.name = 'WindowStickerTemplate'

IF @ConstraintName IS NOT NULL
BEGIN
	DECLARE @SQL NVARCHAR(100)
	SET @SQL = N'ALTER TABLE dbo.DealerPreference DROP CONSTRAINT [' + @ConstraintName + ']'
	EXECUTE sp_executesql @sql
END
GO

--Remove the column from DealerPreference
ALTER TABLE dbo.DealerPreference
DROP COLUMN WindowStickerTemplate
GO