
ALTER TABLE dbo.PING_Code ADD CONSTRAINT CK_PING_Code__Type CHECK (
	type = 'K' -- maKe
OR	type = 'D' -- moDel
)
GO

ALTER TABLE dbo.PING_Code ADD CONSTRAINT FK_PING_Code__ProviderID FOREIGN KEY (
	providerId
)
REFERENCES dbo.PING_Provider (
	id
)
GO

ALTER TABLE dbo.PING_Code ADD CONSTRAINT FK_PING_Code__ParentID FOREIGN KEY (
	parentId
)
REFERENCES dbo.PING_Code (
	id
)
GO

CREATE UNIQUE INDEX IX_PING_Code__ProviderId_ParentId_Id ON dbo.PING_Code (
	providerId,
	parentId,
	id)
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PING_GroupingCodes]') AND name = N'PK_PING_GroupingCodes')
ALTER TABLE dbo.PING_GroupingCodes DROP CONSTRAINT PK_PING_GroupingCodes
GO

ALTER TABLE dbo.PING_GroupingCodes ADD CONSTRAINT PK_PING_GroupingCodes PRIMARY KEY NONCLUSTERED (id)
GO

CREATE UNIQUE CLUSTERED INDEX IX_PING_GroupingCodes ON dbo.PING_GroupingCodes (
	CodeId,
	MakeModelGroupingId
)
GO
