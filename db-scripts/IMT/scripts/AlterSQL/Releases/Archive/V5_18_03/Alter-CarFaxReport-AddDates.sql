ALTER TABLE CarFaxReport
ADD DateCreated SmallDateTime NOT NULL DEFAULT GetDate()
GO

ALTER TABLE CarFaxReport
ADD DateModified SmallDateTime NOT Null DEFAULT GetDate()
GO