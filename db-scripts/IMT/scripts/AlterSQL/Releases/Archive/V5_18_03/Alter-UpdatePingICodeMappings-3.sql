DELETE pgc FROM [dbo].[PING_GroupingCodes] pgc JOIN [dbo].[PING_Code] pc on pc.id = pgc.CodeId JOIN [dbo].[PING_Provider] pp on pp.id = pc.providerid WHERE pgc.MakeModelGroupingId = 102731 and pp.Name = 'Cars.com'

INSERT INTO [dbo].[PING_GroupingCodes]([CodeId],[MakeModelGroupingId]) select id, 102731 from PING_CODE WHERE (providerId = (select id from Ping_Provider where name = 'Cars.com')) AND (parentId = (SELECT id FROM PING_Code WHERE (providerId = (select id from Ping_Provider where name = 'Cars.com')) AND (parentId IS NULL) and (type='K') AND (name = 'Toyota'))) AND (name = 'Camry Hybrid')


DELETE pgc FROM [dbo].[PING_GroupingCodes] pgc JOIN [dbo].[PING_Code] pc on pc.id = pgc.CodeId JOIN [dbo].[PING_Provider] pp on pp.id = pc.providerid WHERE pgc.MakeModelGroupingId = 102731 and pp.Name = 'Autotrader.com'

INSERT INTO [dbo].[PING_GroupingCodes]([CodeId],[MakeModelGroupingId]) select id, 102731 from PING_CODE WHERE (providerId = (select id from Ping_Provider where name = 'Autotrader.com')) AND (parentId = (SELECT id FROM PING_Code WHERE (providerId = (select id from Ping_Provider where name = 'Autotrader.com')) AND (parentId IS NULL) and (type='K') AND (name = 'Toyota'))) AND (name = 'Camry')