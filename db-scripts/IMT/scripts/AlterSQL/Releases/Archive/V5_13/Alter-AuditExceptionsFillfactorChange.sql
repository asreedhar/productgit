/*

Attempt to reduce fragmentation on one of the larger tables

*/

ALTER INDEX PK_Audit_Exceptions
 on Audit_Exceptions
  rebuild with (fillfactor = 80, sort_in_tempdb = on)

