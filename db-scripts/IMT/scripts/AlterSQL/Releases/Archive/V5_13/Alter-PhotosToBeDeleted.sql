/*

Add table to record photos files to be dropped

*/

CREATE TABLE dbo.PhotosToBeDeleted
 (
   PhotoID   int           not null
    constraint PK_PhotosToBeDeleted
     primary key clustered
  ,LoggedOn  datetime      not null
    constraint DF_PhotosToBeDeleted__LoggedOn
     default CURRENT_TIMESTAMP
  ,Reason    varchar(200)  not null
 )

GO
