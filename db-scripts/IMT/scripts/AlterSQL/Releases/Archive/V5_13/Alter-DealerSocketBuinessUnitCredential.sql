SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

insert into dbo.CredentialType (name) values ('DealerSocket')
insert into appraisalSource (AppraisalSourceID, description) values (5, 'DealerSocket')
GO