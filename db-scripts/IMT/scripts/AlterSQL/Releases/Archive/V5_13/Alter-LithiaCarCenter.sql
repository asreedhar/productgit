

GO
SET IDENTITY_INSERT JobTitle ON 

INSERT INTO JobTitle (JobTitleID, NAME)
VALUES (23, 'Lithia Merchandise Manager')

INSERT INTO JobTitle (JobTitleID, NAME)
VALUES (24, 'Lithia Car Center')

INSERT INTO JobTitle (JobTitleID, NAME)
VALUES (25, 'Lithia Transportation Manager')

SET IDENTITY_INSERT JobTitle OFF
GO

ALTER TABLE dbo.DealerPreference
ADD DefaultLithiaCarCenterMemberID INT NULL 
GO

ALTER TABLE dbo.DealerPreference
ADD CONSTRAINT [FK_DealerPreferenceLithiaCarCenterMember_Member] 
FOREIGN KEY (DefaultLithiaCarCenterMemberID) REFERENCES dbo.Member(MemberID)
GO