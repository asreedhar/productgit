-- Creates the AppraisalRequirementLevel column to the DealerPreference table
-- Author: DMincemoyer
-- Rally card/task DE703/TA3214


if not exists (select * from syscolumns
	where id=object_id('dbo.DealerPreference') and name='AppraisalRequirementLevel')
--add new column
ALTER TABLE dbo.DealerPreference
	ADD AppraisalRequirementLevel TINYINT NOT NULL DEFAULT (0)
GO

UPDATE DealerPreference SET AppraisalRequirementLevel = 2 WHERE BusinessUnitId IN (
	SELECT
		BusinessUnit.BusinessUnitId
	FROM
		DealerGroupPreference
		JOIN BusinessUnitRelationship on BusinessUnitRelationship.ParentId = DealerGroupPreference.BusinessUnitId
		JOIN BusinessUnit on BusinessUnit.BusinessUnitId = BusinessUnitRelationship.BusinessUnitId
	WHERE
		DealerGroupPreference.LithiaStore = 1
)
GO

