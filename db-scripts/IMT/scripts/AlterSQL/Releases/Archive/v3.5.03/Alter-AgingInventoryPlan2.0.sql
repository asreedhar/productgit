
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AIP_ThirdParty]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AIP_ThirdParty]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AIP_Event]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AIP_Event]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AIP_EventType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AIP_EventType]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AIP_EventCategory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AIP_EventCategory]
GO

CREATE TABLE dbo.AIP_EventCategory (
	AIP_EventCategoryID	TINYINT NOT NULL,
	Description		VARCHAR(50) NOT NULL,
	CONSTRAINT PK_AIP_EventCategory PRIMARY KEY CLUSTERED 
	(
		AIP_EventCategoryID
	)
)
GO
	
CREATE TABLE dbo.AIP_EventType (
	AIP_EventTypeID		TINYINT,
	Description		VARCHAR(100) NOT NULL,
	AIP_EventCategoryID	TINYINT NOT NULL,
	CONSTRAINT PK_AIP_EventType PRIMARY KEY CLUSTERED 
	(
		AIP_EventTypeID
	),
	CONSTRAINT FK_AIP_EventType_AIP_EventCategoryID FOREIGN KEY 
	(
		AIP_EventCategoryID
	) REFERENCES dbo.AIP_EventCategory (
		AIP_EventCategoryID
	)	
)
GO

CREATE TABLE dbo.AIP_ThirdParty (
	AIP_ThirdPartyID	SMALLINT IDENTITY(1,1), 
	BusinessUnitID		INT NOT NULL,
	AIP_EventTypeID		TINYINT NOT NULL,
	Description		VARCHAR(50) NOT NULL,
	Comments		VARCHAR(500) NULL,
	CONSTRAINT PK_AIP_ThirdParty PRIMARY KEY CLUSTERED 
	(
		AIP_ThirdPartyID
	),
	CONSTRAINT FK_AIP_ThirdParty_BusinessUnitID FOREIGN KEY 
	(
		BusinessUnitID
	) REFERENCES dbo.BusinessUnit (
		BusinessUnitID
	),	
	CONSTRAINT FK_AIP_ThirdParty_AIP_EventTypeID FOREIGN KEY 
	(
		AIP_EventTypeID
	) REFERENCES dbo.AIP_EventType (
		AIP_EventTypeID
	)	
)
GO	

CREATE TABLE dbo.AIP_Event (
	AIP_EventID		INT IDENTITY(1,1),
	InventoryID		INT NOT NULL,
	AIP_EventTypeID		TINYINT NOT NULL,
	BeginDate		SMALLDATETIME NOT NULL,
	EndDate			SMALLDATETIME NULL,
	Notes			VARCHAR(1000) NULL,
	Notes2			VARCHAR(1000) NULL,	-- OPTIONAL
	AIP_ThirdPartyID	INT NULL,		-- OPTIONAL
	ThirdParty      	VARCHAR(50) NULL,    	-- OPTIONAL; hopefully won't be around forever
	Value			DECIMAL(9,2) NULL,	-- OPTIONAL	
	Created			SMALLDATETIME NOT NULL CONSTRAINT [DF_AIP_Event_Created] DEFAULT (GETDATE()),
	CreatedBy		VARCHAR(128) NOT NULL CONSTRAINT DF_AIP_Event_CreatedBy DEFAULT (SUSER_SNAME()),
	LastModified		SMALLDATETIME NOT NULL CONSTRAINT [DF_AIP_Event_LastModified] DEFAULT (GETDATE()),
	LastModifiedBy		VARCHAR(128) NOT NULL CONSTRAINT DF_AIP_Event_LastModifiedBy DEFAULT (SUSER_SNAME()),	
	VersionNumber		INT NOT NULL CONSTRAINT [DF_AIP_Event_VersionNumber] DEFAULT (0)
	CONSTRAINT PK_AIP_Event PRIMARY KEY CLUSTERED 
	(
		AIP_EventID
	),
	CONSTRAINT FK_AIP_Event_InventoryID FOREIGN KEY 
	(
		InventoryID
	) REFERENCES dbo.Inventory (
		InventoryID
	),
	CONSTRAINT FK_AIP_Event_AIP_EventTypeID FOREIGN KEY 
	(
		AIP_EventTypeID
	) REFERENCES dbo.AIP_EventType (
		AIP_EventTypeID
	),
	CONSTRAINT CK_AIP_Event__BeginDate CHECK (SUBSTRING(CAST(BeginDate AS BINARY(4)),3,2) & -1 = 0),
	CONSTRAINT CK_AIP_Event__EndDate CHECK (SUBSTRING(CAST(EndDate AS BINARY(4)),3,2) & -1 = 0)
)

GO

CREATE TABLE dbo.AIP_InventoryEventTypeDefault (
	InventoryID		INT NOT NULL,
	AIP_EventTypeID		TINYINT NOT NULL,
	Notes			VARCHAR(1000) NULL,
	CONSTRAINT PK_AIP_InventoryEventTypeDefault PRIMARY KEY CLUSTERED 
	(
		InventoryID,
		AIP_EventTypeID
	),
	CONSTRAINT FK_AIP_InventoryEventTypeDefault_InventoryID FOREIGN KEY 
	(
		InventoryID
	) REFERENCES dbo.Inventory (
		InventoryID
	),	
	CONSTRAINT FK_AIP_InventoryEventTypeDefault_AIP_EventTypeID FOREIGN KEY 
	(
		AIP_EventTypeID
	) REFERENCES dbo.AIP_EventType (
		AIP_EventTypeID
	)	
)
GO

ALTER TABLE dbo.Inventory ADD PlanReminderDate SMALLDATETIME NULL CONSTRAINT CK_Inventory__PlanReminderDate CHECK (SUBSTRING(CAST(PlanReminderDate AS BINARY(4)),3,2) & -1 = 0)
GO

INSERT
INTO	AIP_EventCategory (AIP_EventCategoryID, Description)
SELECT	1, 'Retail'
UNION 
SELECT	2, 'Wholesale'
UNION 
SELECT	3, 'Sold'
UNION 
SELECT	4, 'Other'
UNION 
SELECT	5, 'Dealer'
UNION 
SELECT	6, 'Admin'

GO

INSERT
INTO	AIP_EventType (AIP_EventTypeID, Description, AIP_EventCategoryID)
SELECT	0, 'No Plan', 4
UNION
SELECT	1, 'SPIFF', 1
UNION
SELECT	2, 'Promotion', 1
UNION
SELECT	3, 'Advertisement', 1
UNION
SELECT	4, 'Other', 1
UNION
SELECT	5, 'Reprice', 1
UNION
SELECT	6, 'Auction', 2
UNION
SELECT	7, 'Wholesaler', 2
UNION
SELECT	8, 'Retail', 3
UNION
SELECT	9, 'Wholesale', 3
UNION
SELECT	10, 'Other', 4
UNION
SELECT	11, 'Service Department', 5
UNION
SELECT	12, 'Detailer', 5
UNION
SELECT	13, 'Other', 2
UNION
SELECT	14, 'Set Reminder Date', 6

GO


/*
INSERT	
INTO	AIP_ThirdParty (AIP_EventTypeID, Description, Comments)
SELECT	3, 'Chicago Tribune', NULL
UNION
SELECT	3, 'Chicago Sun-Times', NULL
*/

SET IDENTITY_INSERT InventoryBuckets ON 
INSERT
INTO	InventoryBuckets (InventoryBucketID, Description, InventoryType)
SELECT	4, 'New Aging Inventory Plan', 2

SET IDENTITY_INSERT InventoryBuckets OFF
GO

---------------------------------------------------------------------------------------------------

ALTER TABLE InventoryBucketRanges ADD BusinessUnitID INT NOT NULL CONSTRAINT [DF_InventoryBucketRanges_BusinessUnitID] DEFAULT (100150)
GO

ALTER TABLE InventoryBucketRanges ADD CONSTRAINT FK_InventoryBucketRanges_BusinessUnitID FOREIGN KEY 
	(BusinessUnitID) REFERENCES dbo.BusinessUnit(BusinessUnitID)
GO

CREATE UNIQUE INDEX UQ_InventoryBucketRanges ON InventoryBucketRanges(InventoryBucketID, RangeID, BusinessUnitID) 
GO

ALTER TABLE InventoryBuckets ALTER COLUMN Description VARCHAR(50) NULL
GO

ALTER TABLE InventoryBucketRanges ADD LongDescription VARCHAR(100) NULL
GO

INSERT
INTO	InventoryBucketRanges (InventoryBucketID, RangeID, Description, Low, High, Lights, BusinessUnitID)
SELECT 	4, 1, 'WATCH LIST', 0, 29, 3, 100150
UNION    
SELECT 	4, 2, '0-29 Days', 0, 29, 4, 100150  
UNION    
SELECT 	4, 3, '30-39 Days', 30, 39, 7, 100150  
UNION    
SELECT 	4, 4, '40-49 Days', 40, 49, 7, 100150     
UNION      
SELECT	4, 5, '50-59 Days', 50, 59, 7, 100150  
UNION      
SELECT 	4, 6, '60+ Days', 60, NULL, 7, 100150

GO

------------------------------------------------------------------------------------------------
--	SR257: Add Version column for Hibernate
------------------------------------------------------------------------------------------------

ALTER TABLE Inventory ADD VersionNumber INT NOT NULL CONSTRAINT [DF_Inventory_VersionNumber] DEFAULT (0)
GO
