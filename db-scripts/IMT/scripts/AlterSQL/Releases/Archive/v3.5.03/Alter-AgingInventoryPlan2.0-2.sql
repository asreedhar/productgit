

ALTER TABLE AIP_EventType ADD CurrentWhenSingleDayPlan BIT CONSTRAINT DF_AIP_Event__CurrentWhenSingleDayPlan DEFAULT (0)
GO

UPDATE	AIP_EventType
SET	CurrentWhenSingleDayPlan = CASE AIP_EventTypeID WHEN 5 THEN 1 ELSE 0 END
GO

ALTER TABLE AIP_EventType ALTER COLUMN CurrentWhenSingleDayPlan BIT NOT NULL
GO


ALTER TABLE AIP_Event ADD CONSTRAINT CK_AIP_Event__BeginEndConsistency CHECK (CASE WHEN EndDate IS NULL THEN 1 WHEN EndDate >= BeginDate THEN 1 ELSE 0 END = 1)
GO

INSERT
INTO	AIP_EventCategory (AIP_EventCategoryID, Description)
SELECT	7, 'Reprice'
GO

UPDATE	AIP_EventType
SET	AIP_EventCategoryID = 7
WHERE	AIP_EventTypeID = 5
GO

INSERT
INTO	AIP_EventType (AIP_EventTypeID, Description, AIP_EventCategoryID, CurrentWhenSingleDayPlan)
SELECT	15, 'No Strategy', 1, 0
UNION
SELECT	16, 'No Strategy', 2, 0
UNION
SELECT	17, 'No Strategy', 3, 0

GO