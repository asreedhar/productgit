IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns WHERE id = object_id(N'[dbo].[DealerPreference]') and OBJECTPROPERTY(id, N'IsUserTable') = 1 and name = 'bucketJumperAIPApproach' )
	ALTER TABLE DealerPreference ADD [bucketJumperAIPApproach] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_BucketJumperAIPApproach] DEFAULT (0)
GO

IF NOT EXISTS (SELECT 1 FROM dbo.syscolumns WHERE id = object_id(N'[dbo].[DealerPreference]') and OBJECTPROPERTY(id, N'IsUserTable') = 1 and name = 'aipRunDayOfWeek' )
	ALTER TABLE DealerPreference ADD [aipRunDayOfWeek] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_aipRunDayOfWeek] DEFAULT (1)

GO
