if exists (select * from dbo.sysobjects where id = object_id(N'dbo.FK_Appraisals_Person') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.Appraisals DROP CONSTRAINT FK_Appraisals_Person
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.FK_PersonPosition_Person') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.PersonPosition DROP CONSTRAINT FK_PersonPosition_Person
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.FK_PersonPosition_Position') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.PersonPosition DROP CONSTRAINT FK_PersonPosition_Position
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.FK_Person_BusinessUnit') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.Person DROP CONSTRAINT FK_Person_BusinessUnit
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.Person') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.Person
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.PersonPosition') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.PersonPosition
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.Position') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.Position
GO

CREATE TABLE dbo.Position (
	PositionID	INT IDENTITY(1,1) NOT NULL,
	Description	VARCHAR(50)  COLLATE SQL_Latin1_General_CP1_CI_AS NULL
	CONSTRAINT PK_Position PRIMARY KEY CLUSTERED (PositionID) 
)
GO

---------------------------------------------------------------------------------------------------------------------
--	FOR NOW, FK TO BusinessUnit AND optionally to Member
--	WE MAY WANT AN INTERSECTION TABLE TO ALLOW A PERSON TO BE IN MORE THAN ONE
--	OR MAKE IT AN OPTIONAL RELATIONSHIP...SLIGHTLY REDUNDANT BECAUSE WE COULD GET BU FROM MEMBER
---------------------------------------------------------------------------------------------------------------------

CREATE TABLE dbo.Person (
	PersonID		INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID		INT NOT NULL,								-- Should move to intersection table?
	MemberID		INT NULL,								-- 
	LastName		VARCHAR(30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	MiddleInitial 		CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	FirstName 		VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	NickName		VARCHAR(20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	Salutation 		VARCHAR(6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	FreeFormTextName	VARCHAR(200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	CONSTRAINT PK_Person PRIMARY KEY CLUSTERED (PersonID),
	CONSTRAINT FK_Person_BusinessUnit FOREIGN KEY (BusinessUnitID) REFERENCES dbo.BusinessUnit(BusinessUnitID),
	CONSTRAINT FK_Person_Member FOREIGN KEY (MemberID) REFERENCES dbo.Member(MemberID)
)

GO

CREATE TABLE dbo.PersonPosition (
	PersonID	INT NOT NULL,
	PositionID	INT NOT NULL,
	CONSTRAINT PK_PersonPosition PRIMARY KEY CLUSTERED (PersonID,PositionID),
	CONSTRAINT FK_PersonPosition_Person FOREIGN KEY (PersonID) REFERENCES dbo.Person (PersonID),
	CONSTRAINT FK_PersonPosition_Position FOREIGN KEY (PositionID) REFERENCES dbo.Position (PositionID)
)

GO

SET IDENTITY_INSERT Position ON 
INSERT
INTO	Position (PositionID, Description)
SELECT	1, 'Salesperson'

SET IDENTITY_INSERT Position OFF 

GO

---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------


INSERT
INTO	Person (BusinessUnitID, MemberID, LastName, MiddleInitial, FirstName, NickName, Salutation, FreeFormTextName)
SELECT	DISTINCT MA.BusinessUnitID, M.MemberID, LastName, MiddleInitial, FirstName, NULLIF(PreferredFirstName,'') NickName, NULLIF(Salutation,''),  NULL FreeFormTextName
FROM	Member M
	JOIN MemberAccess MA ON M.MemberID = MA.BusinessUnitID
GO

INSERT
INTO	Person (BusinessUnitID, LastName, FreeFormTextName)
SELECT	DISTINCT BusinessUnitID, 'DUMMY' LastName, DealTrackSalesperson
FROM 	Appraisals
WHERE	NULLIF(DealTrackSalesperson,'') IS NOT NULL

UPDATE	Person
SET	FreeFormTextName = REPLACE(FreeFormTextName,'/',' ')
WHERE	FreeFormTextName IS NOT NULL
GO

UPDATE	Person
SET	FreeFormTextName = REPLACE(FreeFormTextName,'.',' ')
WHERE	FreeFormTextName IS NOT NULL
GO

UPDATE	P
SET	FirstName = CASE WHEN Pos>1 THEN LEFT(FreeFormTextName, Pos-1) ELSE NULL END,
	LastName = LTRIM(CASE WHEN Pos>1 THEN SUBSTRING(FreeFormTextName, Pos+1,40)  ELSE FreeFormTextName END) 
FROM	(	SELECT DISTINCT PersonID, CHARINDEX(' ',FreeFormTextName) Pos 	
		FROM	Person
		WHERE	FreeFormTextName IS NOT NULL
		) S
	JOIN Person P ON S.PersonID = P.PersonID
GO

ALTER TABLE Appraisals ADD DealTrackSalespersonID INT 
GO
ALTER TABLE Appraisals ADD CONSTRAINT FK_Appraisals_Person FOREIGN KEY (DealTrackSalespersonID) REFERENCES dbo.Person(PersonID)
GO

UPDATE	A
SET	DealTrackSalespersonID = P.PersonID
FROM	Appraisals A
	JOIN Person P ON REPLACE(REPLACE(A.DealTrackSalesperson,'/',' '), '.',' ') = P.FreeFormTextName
GO

---------------------------------------------------------------------------------------------------------------------
--	DEFAULT ALL PERSONS TO THE SALES PERSON POSITION; THE END-USERS WILL REMOVE THEM AS NECESSARY
---------------------------------------------------------------------------------------------------------------------

INSERT
INTO	PersonPosition (PersonID, PositionID)
SELECT	PersonID, 1
FROM	Person

GO