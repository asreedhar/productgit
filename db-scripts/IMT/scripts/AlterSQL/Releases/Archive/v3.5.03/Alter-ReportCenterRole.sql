/*

Create and initially configure the IMT database ReportCenter role

*/

EXECUTE sp_addRole 'ReportCenter'

GRANT EXECUTE on GetSalesHistoryReport to ReportCenter
GRANT EXECUTE on rf_AuthenticateCredentials to ReportCenter

EXECUTE sp_addRoleMember 'ReportCenter', 'FirstlookReports'
GO
