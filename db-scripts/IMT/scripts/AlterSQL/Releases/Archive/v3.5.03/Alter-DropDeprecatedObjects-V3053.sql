if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_invage]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_invage]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_getDataTypeSizeNumeric]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_getDataTypeSizeNumeric]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_InGroupUnitCostValid]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_InGroupUnitCostValid]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_saleage]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_saleage]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_QA_NegativeDaysToSale]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_QA_NegativeDaysToSale]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vwCountOfActiveInventory]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vwCountOfActiveInventory]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OJB_DLIST]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[OJB_DLIST]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OJB_DLIST_ENTRIES]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[OJB_DLIST_ENTRIES]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OJB_DMAP]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[OJB_DMAP]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OJB_DMAP_ENTRIES]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[OJB_DMAP_ENTRIES]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OJB_DSET]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[OJB_DSET]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OJB_DSET_ENTRIES]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[OJB_DSET_ENTRIES]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OJB_HL_SEQ]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[OJB_HL_SEQ]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OJB_LOCKENTRY]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[OJB_LOCKENTRY]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[OJB_NRM]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[OJB_NRM]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tbl_CIAGroupingItem_CIACategory]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tbl_CIAGroupingItem] DROP CONSTRAINT FK_tbl_CIAGroupingItem_CIACategory
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tbl_CIAInventoryStocking_lu_CIACategory]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tbl_CIAInventoryStocking] DROP CONSTRAINT FK_tbl_CIAInventoryStocking_lu_CIACategory
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tbl_CIAPlan_lu_CIAPlanType]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tbl_CIAPlan] DROP CONSTRAINT FK_tbl_CIAPlan_lu_CIAPlanType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tbl_CIAInventoryStocking_tbl_CIABodyTypeDetail]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tbl_CIAInventoryStocking] DROP CONSTRAINT FK_tbl_CIAInventoryStocking_tbl_CIABodyTypeDetail
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tbl_CIAPlan_tbl_CIAGroupingItem]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tbl_CIAPlan] DROP CONSTRAINT FK_tbl_CIAPlan_tbl_CIAGroupingItem
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tbl_CIAUnitCostPointRange_tbl_CIAGroupingItem]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tbl_CIAUnitCostPointRange] DROP CONSTRAINT FK_tbl_CIAUnitCostPointRange_tbl_CIAGroupingItem
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tbl_CIAGroupingItem_tbl_CIAInventoryStocking]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tbl_CIAGroupingItem] DROP CONSTRAINT FK_tbl_CIAGroupingItem_tbl_CIAInventoryStocking
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tbl_CIABodyTypeDetail_tbl_CIASummary]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tbl_CIABodyTypeDetail] DROP CONSTRAINT FK_tbl_CIABodyTypeDetail_tbl_CIASummary
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_tbl_CIAUnitCostPointPlan_tbl_CIAUnitCostPointRange]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[tbl_CIAUnitCostPointPlan] DROP CONSTRAINT FK_tbl_CIAUnitCostPointPlan_tbl_CIAUnitCostPointRange
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_CIABodyTypeDetail]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_CIABodyTypeDetail]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_CIAGroupingItem]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_CIAGroupingItem]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_CIAInventoryStocking]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_CIAInventoryStocking]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_CIASummary]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_CIASummary]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_CIAUnitCostPointPlan]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_CIAUnitCostPointPlan]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_CIAUnitCostPointRange]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_CIAUnitCostPointRange]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[lu_CIACategory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[lu_CIACategory]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[lu_CIAPlanType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[lu_CIAPlanType]
GO

