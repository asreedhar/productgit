ALTER TABLE DealerPreference ALTER COLUMN GuideBookID TINYINT NOT NULL
GO
ALTER TABLE DealerPreference ALTER COLUMN GuideBook2ID TINYINT NULL
GO
UPDATE	DealerPreference
SET	GuideBook2ID = NULL
WHERE	GuideBook2ID = 0
GO
ALTER TABLE DealerPreference ADD CONSTRAINT FK_DealerPreference_GuideBookID FOREIGN KEY (GuideBookID) REFERENCES ThirdParties (ThirdPartyID)
GO
ALTER TABLE DealerPreference ADD CONSTRAINT FK_DealerPreference_GuideBook2ID FOREIGN KEY (GuideBook2ID) REFERENCES ThirdParties (ThirdPartyID)
GO

UPDATE	DealerPreference
SET	BookoutPreferenceId = NULLIF(BookoutPreferenceId,0),
	BookoutPreferenceSecondId = NULLIF(BookoutPreferenceSecondId,0),
	GuideBook2BookoutPreferenceId = NULLIF(GuideBook2BookoutPreferenceId,0),
	GuideBook2SecondBookoutPreferenceId = NULLIF(GuideBook2SecondBookoutPreferenceId,0)

GO

ALTER TABLE DealerPreference ADD CONSTRAINT FK_DealerPreference_BookoutPreferenceSecondId FOREIGN KEY (BookoutPreferenceSecondId) REFERENCES ThirdPartyCategories (ThirdPartyCategoryID)
GO
ALTER TABLE DealerPreference ADD CONSTRAINT FK_DealerPreference_GuideBook2BookoutPreferenceId FOREIGN KEY (GuideBook2BookoutPreferenceId) REFERENCES ThirdPartyCategories (ThirdPartyCategoryID)
GO
ALTER TABLE DealerPreference ADD CONSTRAINT FK_DealerPreference_GuideBook2SecondBookoutPreferenceId FOREIGN KEY (GuideBook2SecondBookoutPreferenceId) REFERENCES ThirdPartyCategories (ThirdPartyCategoryID)
GO

ALTER TABLE Member ALTER COLUMN OfficePhoneNumber VARCHAR(20) NULL
GO