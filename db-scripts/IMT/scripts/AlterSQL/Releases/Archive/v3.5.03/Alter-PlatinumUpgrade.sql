INSERT
INTO SubscriptionTypes(SubscriptionTypeID, Description, Notes, DealerUpgradeCD, UserSelectable, DefaultSubscriptionFrequencyDetailID, Active) 
SELECT 9, 'Used Scorecard','Used Scorecard', 7, 1, 2, 1
UNION
SELECT 10, 'New Scorecard','New Scorecard', 7, 1, 2, 1
UNION
SELECT 11, 'Used Inventory Manager','Used Inventory Manager', 7, 1, 2, 1
UNION
SELECT 12, 'New Inventory Manager','New Inventory Manager', 7, 1, 2, 1
GO

--------------------------------------------------------------------------------------------------------
--	ADD AN INTERSECTION TABLE ENUMERATING VISIBILITY OF SUBSCRIPTION TYPE BASE ON SELECTED UPGRADES
--------------------------------------------------------------------------------------------------------

CREATE TABLE dbo.SubscriptionTypeDealerUpgrade (
		SubscriptionTypeID TINYINT NOT NULL,
		DealerUpgradeCD TINYINT NOT NULL,
		CONSTRAINT PK_SubscriptionTypeDealerUpgrade PRIMARY KEY
		(
			SubscriptionTypeID,
			DealerUpgradeCD
		),
		CONSTRAINT FK_SubscriptionTypeDealerUpgrade_SubscriptionTypeID FOREIGN KEY 
		(
			SubscriptionTypeID
		) REFERENCES dbo.SubscriptionTypes (
			SubscriptionTypeID
		),
		CONSTRAINT FK_SubscriptionTypeDealerUpgrade_DealerUpgradeCD FOREIGN KEY 
		(
			DealerUpgradeCD
		) REFERENCES dbo.lu_DealerUpgrade (
			DealerUpgradeCD
		)
)

GO
--------------------------------------------------------------------------------------------------------
-- 	MIGRATE THE EXISTING VISIBILITY RULES TO THE NEW ONE-TO-MANY TABLE
--------------------------------------------------------------------------------------------------------

INSERT
INTO	SubscriptionTypeDealerUpgrade (SubscriptionTypeID,DealerUpgradeCD)
SELECT	SubscriptionTypeID, DealerUpgradeCD
FROM	SubscriptionTypes
		
GO
--------------------------------------------------------------------------------------------------------
-- 	ADD THE PLATINUM PACKAGE UPGRADE
--------------------------------------------------------------------------------------------------------

INSERT
INTO	lu_DealerUpgrade
SELECT	12, 'Platinum Package'

GO
--------------------------------------------------------------------------------------------------------
-- 	SET VISIBILITY FOR THE PLATINUM PACKAGE
--------------------------------------------------------------------------------------------------------

INSERT
INTO	SubscriptionTypeDealerUpgrade (SubscriptionTypeID,DealerUpgradeCD)
SELECT	SubscriptionTypeID, 12
FROM	SubscriptionTypes
WHERE	SubscriptionTypeID IN (3,8,6,4,5,2)

GO
--------------------------------------------------------------------------------------------------------
-- 	NOW, MAKE SURE THAT "REDISTRIBUTION HOT SHEETS' IS PART OF THE REDISTRIBUTION UPGRADE
--------------------------------------------------------------------------------------------------------

UPDATE	SubscriptionTypeDealerUpgrade
SET	DealerUpgradeCD = 4
WHERE	SubscriptionTypeID = 7
GO

--------------------------------------------------------------------------------------------------------
-- 	DROP THE UPGRADE COLUMN
--------------------------------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[DF__Subscript__Deale__5FC9DD1D]') AND OBJECTPROPERTY(id, 'IsDefaultCnst') = 1)
	ALTER TABLE SubscriptionTypes DROP CONSTRAINT DF__Subscript__Deale__5FC9DD1D
GO
	
ALTER TABLE SubscriptionTypes DROP COLUMN DealerUpgradeCD
GO

--------------------------------------------------------------------------------------------------------
-- 	ADD PLATINUM PACKAGE UPGRADE FOR ALL NON-LITHIA STORES THAT HAVE AN ACTIVE EDGE UPGRADE
--------------------------------------------------------------------------------------------------------

INSERT
INTO	DealerUpgrade (BusinessUnitID, DealerUpgradeCD, StartDate, EndDate, Active)

SELECT	DISTINCT DU.BusinessUnitID, 12, NULL, NULL, 1

FROM	DealerUpgrade DU
	JOIN BusinessUnit BU ON DU.BusinessUnitID = BU.BusinessUnitID
	JOIN BusinessUnitRelationship BUR ON BU.BusinessUnitID = BUR.BusinessUnitID
	JOIN DealerGroupPreference DGP ON BUR.ParentID = DGP.BusinessUnitID

WHERE	DU.Active = 1	
	AND DGP.LithiaStore = 0

--------------------------------------------------------------------------------------------------------
-- 	DELETE ANY SUBSCRIPTIONS TO THE ALERTS (SUBSCRIPTION TYPES) THAT ARE PART OF THE PLATINUM 
--	PACKAGE FOR ALL LITHIA STORES
--------------------------------------------------------------------------------------------------------

DELETE 	S
FROM	Subscriptions S
	JOIN MemberAccess MA ON S.SubscriberID = MA.MemberID
	JOIN BusinessUnitRelationship BUR ON MA.BusinessUnitID = BUR.BusinessUnitID
	JOIN DealerGroupPreference DGP ON BUR.ParentID = DGP.BusinessUnitID
WHERE	SubscriptionTypeID IN (3,8,6,4,5,2)
	AND DGP.LithiaStore = 1