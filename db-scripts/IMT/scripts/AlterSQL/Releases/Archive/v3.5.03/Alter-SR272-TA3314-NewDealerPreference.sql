-- SR272/TA3314:
-- New dealership preference controlling the appearance of unit cost in the FlashLocator.
-- More specifically how long (in days) before the unit cost can be shown to dealer group.
-- The Lithia stores default to 15 days and every one else defaults to 0.

ALTER TABLE DealerPreference ADD FlashLocatorHideUnitCostDays TINYINT NOT NULL DEFAULT (0)
GO

UPDATE DealerPreference SET FlashLocatorHideUnitCostDays = 15 WHERE BusinessUnitId IN (
	SELECT
		BusinessUnit.BusinessUnitId
	FROM
		DealerGroupPreference
		JOIN BusinessUnitRelationship on BusinessUnitRelationship.ParentId = DealerGroupPreference.BusinessUnitId
		JOIN BusinessUnit on BusinessUnit.BusinessUnitId = BusinessUnitRelationship.BusinessUnitId
	WHERE
		DealerGroupPreference.LithiaStore = 1
)
GO
