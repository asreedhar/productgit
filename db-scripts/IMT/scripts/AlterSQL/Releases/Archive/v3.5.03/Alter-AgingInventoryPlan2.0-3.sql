

ALTER TABLE InventoryBucketRanges ADD SortOrder TINYINT NOT NULL CONSTRAINT DF_InventoryBucketRanges_SortOrder DEFAULT(0)
GO

UPDATE	InventoryBucketRanges
SET	SortOrder = RangeID
GO

UPDATE	InventoryBucketRanges
SET	SortOrder = 8 - RangeID
WHERE	InventoryBucketID = 4
	AND RangeID > 1

GO

ALTER TABLE AIP_EventType ADD CurrentAtCreateDate BIT CONSTRAINT DF_AIP_Event__CurrentAtCreateDate DEFAULT (0)
GO

UPDATE	AIP_EventType
SET	CurrentAtCreateDate = CASE WHEN AIP_EventTypeID IN (2,3) THEN 1 ELSE 0 END	-- Promotion, Advert
-- sold: retail and sold: wholesale need this flag to be 1 as well.  Going in a seperate alter. -bf.
GO

ALTER TABLE AIP_EventType ALTER COLUMN CurrentAtCreateDate BIT NOT NULL
GO
