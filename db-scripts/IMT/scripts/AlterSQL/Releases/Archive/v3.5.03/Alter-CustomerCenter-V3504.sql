UPDATE	MessageCenter

SET 
Title = 
'<h3 style="display:block;color:#333;border-bottom:1px dashed #CCC;margin-right:0px;"><strong>The EDGE v3.5.04 has been released!</strong></h3> <span style="font-size:13px;">',
Body = 
'<div class="releaseNotes"><p><font color="#2B3856"> <b><a href="javascript:void(0)" onclick="window.open(''http://images.firstlook.biz/ReleaseNotes-InventoryPlanning-explained.pdf'')">Next Generation Inventory Planning</a>
<br>A Quick and Easy way to Plan Inventory</b></font>
<ul><li>An easier and more intuitive interface</li>
<li>Only plan vehicles that need replanning instead of every vehicle every week</li>
<li>Quick and Easy repricing of aging vehicles</li>
<li>New Service Department action plan</li>
</ul>
<br><strong>For questions regarding the new release, please contact out help desk at 800-730-5665.</strong></p></div>'
 WHERE Ordering = 0
