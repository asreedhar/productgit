if object_id('dbo.PhotoProcessorLog') is null

	create table dbo.PhotoProcessorLog
	(
		MachineName varchar(256) not null,
		Mode varchar(20) not null,
		LogCyclePointer timestamp not null,
		LogDate datetime not null,
		PhotosInStatus1 int not null,
		PhotosInStatus3 int not null,
		PhotosInStatus7 int not null
	)
	
go

if object_id('dbo.PK_PhotoProcessorLog') is null
	alter table dbo.PhotoProcessorLog add constraint PK_PhotoProcessorLog primary key clustered (MachineName, Mode, LogCyclePointer)
	
go

	





