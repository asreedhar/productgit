-- Distribution needs to mark some incoming vehicles as being undefined.

SET IDENTITY_INSERT IMT.Distribution.VehicleEntityType ON

INSERT INTO IMT.Distribution.VehicleEntityType
        ( VehicleEntityTypeID, Name )
VALUES  ( 0, 'Not Defined')

SET IDENTITY_INSERT IMT.Distribution.VehicleEntityType OFF

GO
