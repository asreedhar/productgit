/*

Revised a sloppy index

*/

ALTER TABLE tbl_InventoryOverstocking
 drop constraint PK_tbl_InventoryOverstocking

ALTER TABLE tbl_InventoryOverstocking
 add constraint PK_tbl_InventoryOverstocking
  primary key clustered (InventoryOverstockingID)

GO
