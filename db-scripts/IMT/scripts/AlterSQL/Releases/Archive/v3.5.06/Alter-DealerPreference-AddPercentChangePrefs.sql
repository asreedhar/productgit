ALTER TABLE dbo.DealerPreference
ADD RepricePercentChangeThreshold INT NOT NULL DEFAULT 10
GO
ALTER TABLE dbo.DealerPreference
ADD RepriceConfirmation tinyint NOT NULL DEFAULT 0
GO
