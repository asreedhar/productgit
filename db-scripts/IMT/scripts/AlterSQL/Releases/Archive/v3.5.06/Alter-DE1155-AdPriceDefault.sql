--Puts the use adprice column on internetAdvertiserBuildListTable

if not exists (select * from syscolumns
	where id=object_id('dbo.InternetAdvertiserBuildList') and name='UseAdPrice')
ALTER TABLE dbo.InternetAdvertiserBuildList
	ADD UseAdPrice BIT NOT NULL DEFAULT(0)
GO

if exists (select * from syscolumns
	where id=object_id('dbo.InternetAdvertiserBuildList') and name='AdPrice')
ALTER TABLE dbo.InternetAdvertiserBuildList
	DROP COLUMN AdPrice
GO