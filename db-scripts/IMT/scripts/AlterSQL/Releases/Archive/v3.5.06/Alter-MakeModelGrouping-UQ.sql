----------------------------------------------------------------------------------------
--
--	WE NEED TO ALTER THE UQ SINCE THERE ARE INSTANCES WHERE AN EDMUNDS BODY TYPE
--	DOESN'T DIRECTLY MAP TO A DBT.  FOR EXAMPLE, THE FORD FOCUS HBK CAN HAVE TWO OR
--	FOUR DOORS; FIRSTLOOK'S DBT FOR THE TWO DOOR IS COUPE AND FOR THE FOUR DOOR IS
--	SEDAN.  
--
----------------------------------------------------------------------------------------


ALTER TABLE [dbo].[tbl_MakeModelGrouping] DROP CONSTRAINT UNK_MakeModelBodyType
GO
ALTER TABLE [dbo].[tbl_MakeModelGrouping] ADD 
	CONSTRAINT [UQ_MakeModelBodyType] UNIQUE NONCLUSTERED 
	(
		[Make],
		[Model],
		[EdmundsBodyType],
		[DisplayBodyTypeID]
	) 
GO