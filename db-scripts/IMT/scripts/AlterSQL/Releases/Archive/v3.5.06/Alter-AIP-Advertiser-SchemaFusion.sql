----------------------------------------------------------------------------------------------------
--
--	The introduction of the internet and print advertiser sub-schemas provided the opportunity
--	to revise the AIP_ThirdParty into a generalized table to store data for the external 
--	required by the IMP and advertisement-tracking subsystems.  Related to this new table, 
--	called "ThirdPartyEntity", will be the sub-type tables storing the specifics for each
--	sub-type, as necessary.  
--
----------------------------------------------------------------------------------------------------

EXEC sp_rename @objname = 'FK_AIP_Event__AIP_ThirdPartyID',  @newname  = 'FK_AIP_Event__ThirdPartyEntityID', @objtype = 'OBJECT'
GO 

ALTER TABLE AIP_ThirdParty DROP CONSTRAINT FK_AIP_ThirdParty_AIP_EventTypeID
GO


EXEC sp_rename @objname = 'AIP_ThirdParty',  @newname  = 'ThirdPartyEntity', @objtype = 'OBJECT'
GO 

CREATE TABLE ThirdPartyEntityType (
		ThirdPartyEntityTypeID 	TINYINT NOT NULL,
		Description		VARCHAR(50)
		)
GO
	

ALTER TABLE dbo.ThirdPartyEntityType ADD CONSTRAINT PK_ThirdPartyEntityType PRIMARY KEY CLUSTERED (ThirdPartyEntityTypeID)  
GO

INSERT
INTO	ThirdPartyEntityType
SELECT	1, 'Print Advertiser'
UNION
SELECT	2, 'Auction'
UNION	
SELECT	3, 'Wholesaler'
UNION
SELECT	4, 'Internet Advertiser'
UNION
SELECT	5, 'Internet Auction'
GO
----------------------------------------------------------------------------------------------------
--	For existing IMP event types, map to an analogous Third Party entity type.
--	This can/should be used to ensure that the ThirdPartyEntityID column on AIP_Event is
--	populated with the an entity of the correct type.
----------------------------------------------------------------------------------------------------
ALTER TABLE AIP_EventType ADD ThirdPartyEntityTypeID TINYINT NULL 
GO

sp_rename @objname = 'AIP_Event.AIP_ThirdPartyID',  @newname  = 'ThirdPartyEntityID', @objtype = 'COLUMN'
GO 

DECLARE @Map TABLE (ThirdPartyEntityTypeID TINYINT, AIP_EventTypeID TINYINT)

INSERT
INTO	@Map
SELECT	1, 3 
UNION
SELECT	2, 6
UNION
SELECT	3, 7

UPDATE	ET
SET	ThirdPartyEntityTypeID = M.ThirdPartyEntityTypeID
FROM	AIP_EventType ET
	JOIN @Map M ON ET.AIP_EventTypeID = M.AIP_EventTypeID

--------------------------------------------------------------------------------
--	UPDATE THE OLD AIP EVENT TYPE TO A THE NEW ENTITY TYPE
--------------------------------------------------------------------------------

UPDATE	TPE
SET	AIP_EventTypeID = M.ThirdPartyEntityTypeID
FROM	ThirdPartyEntity TPE
	JOIN @Map M ON TPE.AIP_EventTypeID = M.AIP_EventTypeID 


EXEC sp_rename @objname = 'ThirdPartyEntity.AIP_EventTypeID',  @newname  = 'ThirdPartyEntityTypeID', @objtype = 'COLUMN'
GO 

ALTER TABLE dbo.AIP_EventType ADD CONSTRAINT FK_AIP_EventType_ThirdPartyEntityTypeID FOREIGN KEY (ThirdPartyEntityTypeID) REFERENCES dbo.ThirdPartyEntityType(ThirdPartyEntityTypeID)
GO


----------------------------------------------------------------
-- 	NOW, UPDATE SOME STUFFS
----------------------------------------------------------------

EXEC sp_rename @objname = 'PK_AIP_ThirdParty',  @newname  = 'PK_ThirdPartyEntity', @objtype = 'OBJECT'
GO 

EXEC sp_rename @objname = 'FK_AIP_ThirdParty_BusinessUnitID',  @newname  = 'FK_ThirdPartyEntity_BusinessUnitID', @objtype = 'OBJECT'
GO 

EXEC sp_rename @objname = 'ThirdPartyEntity.AIP_ThirdPartyID',  @newname  = 'ThirdPartyEntityID', @objtype = 'COLUMN'
GO 

EXEC sp_rename @objname = 'ThirdPartyEntity.Description',  @newname  = 'Name', @objtype = 'COLUMN'
GO 

ALTER TABLE dbo.ThirdPartyEntity DROP COLUMN Comments
GO

ALTER TABLE dbo.ThirdPartyEntity ADD CONSTRAINT FK_ThirdPartyEntity_ThirdPartyEntityTypeID FOREIGN KEY (ThirdPartyEntityTypeID) REFERENCES dbo.ThirdPartyEntityType(ThirdPartyEntityTypeID)
GO

--------------------------------------------------------------------------------
--	A "Print Advertiser" entity will be owned at the Business unit, so
-- 	the TPE will have the BU of the owner
--------------------------------------------------------------------------------

CREATE TABLE dbo.PrintAdvertiser_ThirdPartyEntity (
		PrintAdvertiserID			INT NOT NULL,
		ContactEmail				VARCHAR(100) NULL,
		FaxNumber				VARCHAR(20) NULL,
		DisplayPrice				TINYINT NOT NULL CONSTRAINT DF_PrintAdvertiser_ThirdPartyEntity_DisplayPrice DEFAULT (1),
		VehicleOptionThirdPartyID		TINYINT NOT NULL
			
		)
GO

ALTER TABLE dbo.PrintAdvertiser_ThirdPartyEntity ADD CONSTRAINT PK_PrintAdvertiser_ThirdPartyEntity PRIMARY KEY CLUSTERED (PrintAdvertiserID)  
GO
ALTER TABLE dbo.PrintAdvertiser_ThirdPartyEntity ADD CONSTRAINT FK_PrintAdvertiser_ThirdPartyEntity_PrintAdvertiserID FOREIGN KEY (PrintAdvertiserID) REFERENCES dbo.ThirdPartyEntity(ThirdPartyEntityID)
GO
ALTER TABLE dbo.PrintAdvertiser_ThirdPartyEntity ADD CONSTRAINT FK_PrintAdvertiser_ThirdPartyEntity_VehicleOptionThirdPartyID FOREIGN KEY (VehicleOptionThirdPartyID) REFERENCES dbo.ThirdParties(ThirdPartyID)
GO

CREATE TABLE dbo.PrintAdvertiserTextOption (
		PrintAdvertiserTextOptionID 	TINYINT NOT NULL,
		Description			VARCHAR(50)
		)
GO

INSERT
INTO	PrintAdvertiserTextOption
SELECT	1, 'Vehicle Description'
UNION
SELECT	2, 'VIN'
UNION
SELECT	3, 'Stock #'
UNION
SELECT	4, 'Color'
UNION
SELECT	5, 'Mileage'
UNION
SELECT	6, 'Options'
UNION
SELECT	7, 'Ad Description'
GO
ALTER TABLE dbo.PrintAdvertiserTextOption ADD CONSTRAINT PK_PrintAdvertiserTextOption PRIMARY KEY CLUSTERED (PrintAdvertiserTextOptionID)  
GO

CREATE TABLE dbo.PrintAdvertiserTextOptionDefault (
		PrintAdvertiserTextOptionDefaultID	INT IDENTITY(1,1) NOT NULL,
		PrintAdvertiserID			INT NOT NULL,			-- Maps to a PrintAdvertiser TPE
		PrintAdvertiserTextOptionID		TINYINT NOT NULL,
		DisplayOrder				TINYINT NOT NULL,
		Status					TINYINT NOT NULL CONSTRAINT DF_PrintAdvertiserTextOptionDefault_Status DEFAULT (0)
		)
GO

ALTER TABLE dbo.PrintAdvertiserTextOptionDefault ADD CONSTRAINT PK_PrintAdvertiserTextOptionDefault PRIMARY KEY CLUSTERED (PrintAdvertiserTextOptionDefaultID)  
GO
ALTER TABLE dbo.PrintAdvertiserTextOptionDefault ADD CONSTRAINT FK_PrintAdvertiserTextOptionDefault_ThirdPartyEntityID FOREIGN KEY (PrintAdvertiserID) REFERENCES dbo.PrintAdvertiser_ThirdPartyEntity(PrintAdvertiserID)
GO
ALTER TABLE dbo.PrintAdvertiserTextOptionDefault ADD CONSTRAINT FK_PrintAdvertiserTextOptionDefault_PrintAdvertiserTextOptionID FOREIGN KEY (PrintAdvertiserTextOptionID) REFERENCES dbo.PrintAdvertiserTextOption(PrintAdvertiserTextOptionID)
GO

/*


--  Undo script
DROP TABLE InternetAdvertiserBuildLogDetail
DROP TABLE InternetAdvertiserBuildLog
DROP TABLE InternetAdvertiserBuildList
DROP TABLE InternetAdvertiserDealership
DROP TABLE InternetAdvertiser

ALTER TABLE DealerPreference
 drop column InternetAdvertiser_AdPriceEnabled, InternetAdvertiser_SendzeroesAsNull

*/

SET NOCOUNT on

--  Add two new columns to DealerPreference
ALTER TABLE DealerPreference
 add
   InternetAdvertiser_AdPriceEnabled    bit  not null
    constraint DF_DealerPreference__InternetAdvertiser_AdPriceEnabled
     default 0
  ,InternetAdvertiser_SendZeroesAsNull  bit  not null
    constraint DF_DealerPreference__InternetAdvertiser_SendZeroesAsNull
     default 0
GO


--  Create table InternetAdvertiser
CREATE TABLE InternetAdvertiser_ThirdPartyEntity
 (
   InternetAdvertiserID    int           not null
    constraint PK_InternetAdvertiser_ThirdPartyEntity
     primary key clustered
--     constraint FK_InternetAdvertiser_ThirdPartyEntity__ThirdPartyEntity
--      foreign key references ThirdPartyEntity (ThirdPartyEntityID)
  ,DatafeedCode    varchar(20)   not null
    constraint UQ_InternetAdvertiser__DatafeedCode
     unique nonclustered
 )


ALTER TABLE dbo.InternetAdvertiser_ThirdPartyEntity ADD CONSTRAINT FK_InternetAdvertiser_ThirdPartyEntity_InternetAdvertiserID FOREIGN KEY (InternetAdvertiserID) REFERENCES dbo.ThirdPartyEntity(ThirdPartyEntityID)
GO

--  Create table InternetAdvertiserDealership
CREATE TABLE InternetAdvertiserDealership
 (
   InternetAdvertiserID               int          not null
    constraint FK_InternetAdvertiserDealership__InternetAdvertiser
     foreign key references InternetAdvertiser_ThirdPartyEntity (InternetAdvertiserID)
  ,BusinessUnitID             		int          not null    constraint FK_InternetAdvertiserDealership__BusinessUnit  foreign key references BusinessUnit (BusinessUnitID)
  ,InternetAdvertisersDealershipCode  	varchar(50)  null
  ,IsLive				bit	     not null
  ,constraint PK_InternetAdvertiserDealership
    primary key clustered (BusinessUnitID, InternetAdvertiserID)
 )


CREATE TABLE InternetAdvertiserExportStatus
 (
  ExportStatusCD	TINYINT NOT NULL,
  Description  		VARCHAR(50),
  CONSTRAINT PK_ExportStatus PRIMARY KEY CLUSTERED (ExportStatusCD)
  )
 
GO

INSERT
INTO	InternetAdvertiserExportStatus	
SELECT	1, 'Export, set by Rule Engine'
UNION
SELECT	10, 'Do NOT export, set by User'
UNION
SELECT	11, 'Export, set by User' 
GO

--  Create table InternetAdvertiserBuildList
CREATE TABLE InternetAdvertiserBuildList
 (
  InternetAdvertiserID  int           not null
    constraint FK_InternetAdvertiserBuildList__InternetAdvertiser
     foreign key references InternetAdvertiser_ThirdPartyEntity (InternetAdvertiserID)
  ,InventoryID   int           not null
    constraint FK_InternetAdvertiserBuildList__Inventory
     foreign key references Inventory (InventoryID)
  ,ExportStatusCD tinyint not null 
    constraint FK_InternetAdvertiserBuildList_InternetAdvertiserExportStatus 
     foreign key references InternetAdvertiserExportStatus(ExportStatusCD)   
  ,AdPrice       int           not null
  ,Highlights    varchar(500)  null
  ,constraint PK_InternetAdvertiserBuildList
    primary key clustered (InternetAdvertiserID, InventoryID)
 )
CREATE nonclustered INDEX IX_InternetAdvertiserBuildList__InventoryID
 on InternetAdvertiserBuildList (InventoryID)



--  Create table InternetAdvertiserBuildLog
CREATE TABLE InternetAdvertiserBuildLog
 (
   InternetAdvertiserBuildLogID  int       not null  identity(1,1)
    constraint PK_InternetAdvertiserBuildLog
     primary key clustered
  ,InternetAdvertiserID          int       not null
    constraint FK_InternetAdvertiserBuildLog__InternetAdvertiser
     foreign key references InternetAdvertiser_ThirdPartyEntity (InternetAdvertiserID)
  ,Created             		datetime  not null
    constraint DF_InternetAdvertiserBuildLog__Created
     default CURRENT_TIMESTAMP
  ,TestForBusinessUnitID    int       null
  ,ExceptionReportsSent		datetime  null
 )

--  Create table InternetAdvertiserBuildLogDetail
CREATE TABLE InternetAdvertiserBuildLogDetail
 (
   InternetAdvertiserBuildLogID  int           not null
    constraint FK_InternetAdvertiserBuildLogDetail__InternetAdvertiserBuildLog
     foreign key references InternetAdvertiserBuildLog (InternetAdvertiserBuildLogID)
  ,InventoryID           int           not null
  ,ListedPrice           int           not null
  ,ListedPriceSource     char(1)       not null
    constraint CK_InternetAdvertiserBuildLogDetail__ListedPriceSource
     check (ListedPriceSource in ('A','L'))
  ,Highlights  			varchar(500) null     
  ,SendZeroesAsNull  		bit not null
  ,constraint PK_InternetAdvertiserBuildLogDetail
    primary key clustered (InternetAdvertiserBuildLogID, InventoryID)
 )

GO

INSERT
INTO	ThirdPartyEntity (BusinessUnitID, ThirdPartyEntityTypeID, Name)
SELECT	100150, 4, 'eBizAutos'
UNION
SELECT	100150, 4, 'CarsDotCom'
UNION
SELECT	100150, 4, 'AutoTrader'
GO
INSERT
INTO	InternetAdvertiser_ThirdPartyEntity (InternetAdvertiserID, DatafeedCode)
SELECT	ThirdPartyEntityID, Name
FROM	ThirdPartyEntity
WHERE	ThirdPartyEntityTypeID = 4

GO
------------------------------------------------------------------------------------------------
--	NOW MIGRATE EXISTING PRINT ADVERTS
------------------------------------------------------------------------------------------------

INSERT
INTO	PrintAdvertiser_ThirdPartyEntity (PrintAdvertiserID, DisplayPrice, VehicleOptionThirdPartyID)
SELECT	ThirdPartyEntityID, 1, GuideBookID 
FROM	ThirdPartyEntity TPE
	JOIN DealerPreference DP ON TPE.BusinessUnitID = DP.BusinessUnitID
WHERE	ThirdPartyEntityTypeID = 1

GO
INSERT
INTO	PrintAdvertiserTextOptionDefault (PrintAdvertiserID, PrintAdvertiserTextOptionID, DisplayOrder, Status)
SELECT	PA.PrintAdvertiserID, PrintAdvertiserTextOptionID, PrintAdvertiserTextOptionID, 1
FROM	PrintAdvertiser_ThirdPartyEntity PA
	CROSS JOIN PrintAdvertiserTextOption PATO
GO

UPDATE	AIP_Event
SET	Notes2	= Notes3,
	Notes3	= 'True',
	Value 	= 0.0
WHERE	AIP_EventTypeID = 3
	
GO

------------------------------------------------------------------------------------------------
--	CREATE NEW "VERTICAL PARTIION" FOR INVENTORY
--	STORE THE ADVERTISER NOTES (ONCE PART OF AIP) HERE SINCE THEY CAN APPLY TO ANY KIND
--	OF ADVERTISING.  ALSO, THE AdPrice GOES HERE.
------------------------------------------------------------------------------------------------

CREATE TABLE dbo.Inventory_Advertising (
		InventoryID	INT NOT NULL CONSTRAINT PK_Inventory_Advertising PRIMARY KEY CLUSTERED,
		Description	VARCHAR(500) NULL,
		AdPrice		DECIMAL(9,2) NULL
		)
GO
ALTER TABLE dbo.Inventory_Advertising ADD CONSTRAINT FK_Inventory_Advertising_InventoryID FOREIGN KEY (InventoryID) REFERENCES dbo.Inventory(InventoryID)
GO

INSERT
INTO	Inventory_Advertising (InventoryID, Description)
SELECT	InventoryID, Notes
FROM	AIP_InventoryEventTypeDefault
GO

DROP TABLE AIP_InventoryEventTypeDefault
GO

------------------------------------------------------------------------------------------------
--	Dependent objects:
--
--	AIP_UserEvent
--	GetAgingInventoryPlanMultiSet
------------------------------------------------------------------------------------------------
