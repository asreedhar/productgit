-- Add the columns to the internetAdvertiserDealership table that are used
-- on the admin side of things.

if not exists (select * from syscolumns
	where id=object_id('dbo.InternetAdvertiserDealership') and name='Notes')
ALTER TABLE dbo.InternetAdvertiserDealership
	ADD Notes VARCHAR(1000)
GO

if not exists (select * from syscolumns
	where id=object_id('dbo.InternetAdvertiserDealership') and name='ContactFirstName')
ALTER TABLE dbo.InternetAdvertiserDealership
	ADD ContactFirstName VARCHAR(15)
GO

if not exists (select * from syscolumns
	where id=object_id('dbo.InternetAdvertiserDealership') and name='ContactLastName')
ALTER TABLE dbo.InternetAdvertiserDealership
	ADD ContactLastName VARCHAR(25)
GO

if not exists (select * from syscolumns
	where id=object_id('dbo.InternetAdvertiserDealership') and name='ContactPhoneNumber')
ALTER TABLE dbo.InternetAdvertiserDealership
	ADD ContactPhoneNumber VARCHAR(15)
GO

if not exists (select * from syscolumns
	where id=object_id('dbo.InternetAdvertiserDealership') and name='ContactEmail')
ALTER TABLE dbo.InternetAdvertiserDealership
	ADD ContactEmail VARCHAR(35)
GO

if not exists (select * from syscolumns
	where id=object_id('dbo.InternetAdvertiserDealership') and name='Active')
ALTER TABLE dbo.InternetAdvertiserDealership
	ADD Active TINYINT
GO

if not exists (select * from syscolumns
	where id=object_id('dbo.InternetAdvertiserDealership') and name='IsLive')
ALTER TABLE dbo.InternetAdvertiserDealership
	ADD IsLive BIT NOT NULL DEFAULT(0)
GO

if not exists (select * from syscolumns
	where id=object_id('dbo.InternetAdvertiserDealership') and name='status')
ALTER TABLE dbo.InternetAdvertiserDealership
	ADD status TINYINT
GO

if not exists (select * from syscolumns
	where id=object_id('dbo.InternetAdvertiserDealership') and name='VerifiedBy')
ALTER TABLE dbo.InternetAdvertiserDealership
	ADD VerifiedBy VARCHAR(35)
GO

if not exists (select * from syscolumns
	where id=object_id('dbo.InternetAdvertiserDealership') and name='VerifiedDate')
ALTER TABLE dbo.InternetAdvertiserDealership
	ADD VerifiedDate SMALLDATETIME 
GO