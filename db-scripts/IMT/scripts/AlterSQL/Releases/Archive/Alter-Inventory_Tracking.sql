------------------------------------------------------------------------------------------------
--	INVENTORY "TRACKING"  (COULDN'T THINK OF A BETTER NAME)
--
--	FOR NOW, JUST STORE THE RAW SOURCE DESCRIPTION FROM THE DMS.
--	EVENTUALLY, WILL NEED TO STANDARDIZE THIS INFORMATION.
--	FURTHERMORE, WE CAN ADD A "DESTINATION" COLUMN AS WELL FOR IN-GROUP SALES OR WHOLESALES
------------------------------------------------------------------------------------------------


CREATE TABLE dbo.Inventory_Tracking (
		InventoryID	INT NOT NULL CONSTRAINT PK_Inventory_Source PRIMARY KEY CLUSTERED,
		Source		VARCHAR(50) NOT NULL,
		AuditID		INT NOT NULL
		)
GO
ALTER TABLE dbo.Inventory_Tracking ADD CONSTRAINT FK_Inventory_Tracking__InventoryID FOREIGN KEY (InventoryID) REFERENCES dbo.Inventory(InventoryID)
GO
