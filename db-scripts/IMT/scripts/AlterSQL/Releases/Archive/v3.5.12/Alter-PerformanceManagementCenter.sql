
ALTER TABLE DealerGroupPreference ADD IncludePerformanceManagementCenter BIT
GO

UPDATE DealerGroupPreference SET IncludePerformanceManagementCenter = 0
GO

ALTER TABLE DealerGroupPreference ALTER COLUMN IncludePerformanceManagementCenter BIT NOT NULL
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SoftwareSystemComponentState]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.SoftwareSystemComponentState
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SoftwareSystemComponent]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.SoftwareSystemComponent
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MemberContext]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.MemberContext
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MemberBusinessUnitSetItem]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.MemberBusinessUnitSetItem
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MemberBusinessUnitSet]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.MemberBusinessUnitSet
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MemberBusinessUnitSetType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.MemberBusinessUnitSetType
GO

CREATE TABLE dbo.MemberBusinessUnitSetType (
	[MemberBusinessUnitSetTypeID] INT IDENTITY(1,1) NOT NULL,
	[Name] VARCHAR(255) NOT NULL,
	CONSTRAINT PK_MemberBusinessUnitSetType PRIMARY KEY (
		MemberBusinessUnitSetTypeID
	)
)
GO

INSERT INTO MemberBusinessUnitSetType ([Name]) VALUES ('Command Center Dealer Group Selection')
GO

CREATE TABLE dbo.MemberBusinessUnitSet (
	MemberBusinessUnitSetID     INT IDENTITY(1,1) NOT NULL,
	MemberBusinessUnitSetTypeID INT NOT NULL,
	BusinessUnitID              INT NOT NULL,
	MemberID                    INT NOT NULL,
	CONSTRAINT PK_MemberBusinessUnitSet PRIMARY KEY (
		MemberBusinessUnitSetID
	),
	CONSTRAINT FK_MemberBusinessUnitSet_Type FOREIGN KEY (
		MemberBusinessUnitSetTypeID
	)
	REFERENCES MemberBusinessUnitSetType (
		MemberBusinessUnitSetTypeID
	),
	CONSTRAINT FK_MemberBusinessUnitSet_BusinessUnit FOREIGN KEY (
		BusinessUnitID
	)
	REFERENCES BusinessUnit (
		BusinessUnitID
	),
	CONSTRAINT FK_MemberBusinessUnitSet_Member FOREIGN KEY (
		MemberID
	)
	REFERENCES Member (
		MemberID
	),
	CONSTRAINT UK_MemberBusinessUnitList UNIQUE (
		MemberBusinessUnitSetTypeID,
		BusinessUnitID,
		MemberID
	)
)
GO

CREATE TABLE dbo.MemberBusinessUnitSetItem (
	[MemberBusinessUnitSetID] INT NOT NULL,
	[BusinessUnitID] INT NOT NULL,
	CONSTRAINT PK_MemberBusinessUnitSetItem PRIMARY KEY (
		[MemberBusinessUnitSetID],
		[BusinessUnitID]
	),
	CONSTRAINT FK_MemberBusinessUnitSetItem_Set FOREIGN KEY (
		MemberBusinessUnitSetID
	)
	REFERENCES MemberBusinessUnitSet (
		MemberBusinessUnitSetID
	),
	CONSTRAINT FK_MemberBusinessUnitSetItem_BusinessUnit FOREIGN KEY (
		BusinessUnitID
	)
	REFERENCES BusinessUnit (
		BusinessUnitID
	)
)
GO

CREATE TABLE dbo.SoftwareSystemComponent (
	[SoftwareSystemComponentID] INT NOT NULL,
	[ParentID]                  INT NULL,
	[Name]                      VARCHAR(64) NOT NULL,
	[Token]                     VARCHAR(64) NOT NULL,
	CONSTRAINT PK_SoftwareSystemComponent PRIMARY KEY (
		[SoftwareSystemComponentID]
	),
	CONSTRAINT FK_SoftwareSystemComponent_SoftwareSystemComponent FOREIGN KEY (
		[ParentID]
	)
	REFERENCES SoftwareSystemComponent (
		[SoftwareSystemComponentID]
	),
	CONSTRAINT UK_SoftwareSystemComponent_Name UNIQUE (
		[Name]
	),
	CONSTRAINT UK_SoftwareSystemComponent_Token UNIQUE (
		[Token]
	),
	CONSTRAINT CK_SoftwareSystemComponent_Name CHECK (
		LEN(LTRIM(RTRIM([Name]))) > 0
	),
	CONSTRAINT CK_SoftwareSystemComponent_Token CHECK (
		LEN(LTRIM(RTRIM([Token]))) > 0
	)
)
GO

INSERT INTO dbo.SoftwareSystemComponent ([SoftwareSystemComponentID], [ParentID], [Name], [Token]) VALUES (1, NULL, 'Dealer Group System Component', 'DEALER_GROUP_SYSTEM_COMPONENT')
INSERT INTO dbo.SoftwareSystemComponent ([SoftwareSystemComponentID], [ParentID], [Name], [Token]) VALUES (2, NULL, 'Dealer System Component', 'DEALER_SYSTEM_COMPONENT')
GO

CREATE TABLE dbo.SoftwareSystemComponentState (
	[SoftwareSystemComponentStateID] INT IDENTITY(1,1) NOT NULL,
	[SoftwareSystemComponentID]      INT NOT NULL,
	[AuthorizedMemberID]             INT NOT NULL,
	[DealerGroupID]                  INT NOT NULL,
	[DealerID]                       INT NULL,
	[MemberID]                       INT NULL,
	CONSTRAINT PK_SoftwareSystemComponentState PRIMARY KEY (
		[SoftwareSystemComponentStateID]
	),
	CONSTRAINT UK_SoftwareSystemComponentState UNIQUE (
		[SoftwareSystemComponentID],
		[AuthorizedMemberID]
	),
	CONSTRAINT FK_SoftwareSystemComponentState_Member_Authorized FOREIGN KEY (
		[AuthorizedMemberID]
	)
	REFERENCES Member (
		[MemberID]
	),
	CONSTRAINT FK_SoftwareSystemComponentState_Member_Impersonating FOREIGN KEY (
		[MemberID]
	)
	REFERENCES Member (
		[MemberID]
	),
	CONSTRAINT FK_SoftwareSystemComponentState_BusinessUnit_DealerGroup FOREIGN KEY (
		[DealerGroupID]
	)
	REFERENCES BusinessUnit (
		[BusinessUnitID]
	),
	CONSTRAINT FK_SoftwareSystemComponentState_BusinessUnit_Dealer FOREIGN KEY (
		[DealerID]
	)
	REFERENCES BusinessUnit (
		[BusinessUnitID]
	),
	CONSTRAINT FK_SoftwareSystemComponentState_SoftwareSystemComponent FOREIGN KEY (
		[SoftwareSystemComponentID]
	)
	REFERENCES SoftwareSystemComponent (
		[SoftwareSystemComponentID]
	)
)
GO
