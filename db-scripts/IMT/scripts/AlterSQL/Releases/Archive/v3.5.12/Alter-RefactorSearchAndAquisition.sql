ALTER TABLE dbo.SearchCandidate ADD ModelID INT
GO

UPDATE	SC
SET		ModelID = MM.ModelID
FROM	dbo.SearchCandidate SC
JOIN	dbo.MakeModelGrouping MM ON
			SC.Make = MM.Make
		AND	SC.Line = MM.Model
		AND	SC.SegmentID = MM.SegmentID
GO

UPDATE	SC
SET		ModelID = MM.ModelID
FROM	dbo.SearchCandidate SC
JOIN	dbo.MakeModelGrouping MM ON
		SC.Make = MM.Make
		AND	SC.SegmentID = MM.SegmentID
JOIN	dbo.Segment S ON MM.SegmentID = S.SegmentID
WHERE	SC.ModelID IS NULL
AND		SC.Line = MM.Model + ' ' + S.Segment
GO

DELETE FROM dbo.SearchCandidateCIACategoryAnnotation
WHERE SearchCandidateID IN (
	SELECT SearchCandidateID FROM dbo.SearchCandidate WHERE ModelID IN (0,186,1326)
)
GO

DELETE FROM dbo.SearchCandidateOptionAnnotation
WHERE SearchCandidateOptionID IN (
	SELECT	SearchCandidateOptionID
	FROM	dbo.SearchCandidateOption
	WHERE	SearchCandidateID IN (
		SELECT SearchCandidateID FROM dbo.SearchCandidate WHERE ModelID IN (0,186,1326)
	)
)
GO

DELETE FROM dbo.SearchCandidateOption
WHERE SearchCandidateID IN (
	SELECT SearchCandidateID FROM dbo.SearchCandidate WHERE ModelID IN (0,186,1326)
)
GO

DELETE FROM dbo.SearchCandidate
WHERE ModelID IN (0,186,1326)
GO

ALTER TABLE [dbo].[SearchCandidate] DROP CONSTRAINT [UQ_SearchCandidate]
GO

ALTER TABLE [dbo].[SearchCandidate] DROP COLUMN [Make]
GO

ALTER TABLE [dbo].[SearchCandidate] DROP COLUMN [Line]
GO

ALTER TABLE [dbo].[SearchCandidate] DROP COLUMN [SegmentID]
GO

ALTER TABLE [dbo].[SearchCandidate] ALTER COLUMN [ModelID] INT NOT NULL
GO

ALTER TABLE [dbo].[SearchCandidate] ADD CONSTRAINT [UQ_SearchCandidate] UNIQUE (SearchCandidateListID,ModelID)
GO
