
ALTER TABLE dbo.ThirdPartyVehicleOptionValues DROP CONSTRAINT PK_VehicleThirdPartyVehicleOptionValues

DROP INDEX ThirdPartyVehicleOptionValues.IX_ThirdPartyVehicleOptionValues__ThirdPartyVehicleOptionID_Value

CREATE CLUSTERED INDEX IX_ThirdPartyVehicleOptionValues__ThirdPartyVehicleOptionID ON ThirdPartyVehicleOptionValues
(	ThirdPartyVehicleOptionID
	)	

ALTER TABLE dbo.ThirdPartyVehicleOptionValues ADD CONSTRAINT PK_ThirdPartyVehicleOptionValues PRIMARY KEY NONCLUSTERED (ThirdPartyVehicleOptionValueID)
GO

-----------------------------------------------------------------------------------------------------------------------------
ALTER TABLE dbo.ThirdPartyVehicleOptionValues DROP CONSTRAINT FK_VehicleThirdPartyVehicleOptionValues_VehicleThirdPartyVehicleOptions
GO

ALTER TABLE ThirdPartyVehicleOptions DROP CONSTRAINT PK_VehicleThirdPartyVehicleOptions

DROP INDEX ThirdPartyVehicleOptions.IX_ThirdPartyVehicleOptions__ThirdPartyVehicleID

CREATE CLUSTERED INDEX IX_ThirdPartyVehicleOptions__ThirdPartyVehicleID ON ThirdPartyVehicleOptions(ThirdPartyVehicleID)
GO

ALTER TABLE dbo.ThirdPartyVehicleOptions ADD CONSTRAINT PK_ThirdPartyVehicleOptions PRIMARY KEY NONCLUSTERED (ThirdPartyVehicleOptionID)
GO


ALTER TABLE dbo.ThirdPartyVehicleOptionValues  WITH CHECK ADD  CONSTRAINT FK_VehicleThirdPartyVehicleOptionValues_VehicleThirdPartyVehicleOptions FOREIGN KEY(ThirdPartyVehicleOptionID)
REFERENCES dbo.ThirdPartyVehicleOptions (ThirdPartyVehicleOptionID)
GO
ALTER TABLE dbo.ThirdPartyVehicleOptionValues CHECK CONSTRAINT FK_VehicleThirdPartyVehicleOptionValues_VehicleThirdPartyVehicleOptions
GO
