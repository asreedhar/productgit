/*
select	co.NAME, po.NAME, *
FROM	sys.foreign_key_columns fkc
	JOIN sys.objects o ON fkc.referenced_object_id = o.OBJECT_ID
	JOIN sys.objects co ON fkc.constraint_object_id = co.OBJECT_ID
	JOIN sys.objects po ON fkc.parent_object_id = po.OBJECT_ID
WHERE	o.name = 'Inventory'
ORDER BY po.NAME
*/

ALTER TABLE [dbo].[AIP_Event] DROP CONSTRAINT [FK_AIP_Event_InventoryID]
GO
ALTER TABLE [dbo].[CIAInventoryOverstocking] DROP CONSTRAINT [FK_CIAInventoryOverstocking__Inventory]
GO
ALTER TABLE [dbo].[DemoDealerBaseline_Inventory] DROP CONSTRAINT [FK_DemoDealerBaseline_Inventory__Inventory]
GO
ALTER TABLE [dbo].[InternetAdvertiserBuildList] DROP CONSTRAINT [FK_InternetAdvertiserBuildList__Inventory]
GO
ALTER TABLE [dbo].[Inventory_Advertising] DROP CONSTRAINT [FK_Inventory_Advertising__InventoryID]
GO
ALTER TABLE [dbo].[Inventory_Insight] DROP CONSTRAINT [FK_Inventory_Insight__InventoryID]
GO
ALTER TABLE [dbo].[Inventory_Tracking] DROP CONSTRAINT [FK_Inventory_Tracking__InventoryID]
GO
ALTER TABLE [dbo].[InventoryBookouts] DROP CONSTRAINT [FK_InventoryBookouts__Inventory]
GO
ALTER TABLE [dbo].[InventoryPhotos] DROP CONSTRAINT [FK_InventoryPhotos__Inventory]
GO
ALTER TABLE [dbo].[InventoryThirdPartyVehicles] DROP CONSTRAINT [FK_InventoryThirdPartyVehicles__Inventory]
GO
ALTER TABLE [dbo].[MarketplaceSubmission] DROP CONSTRAINT [FK_MarketplaceSubmission_Inventory]
GO
ALTER TABLE [dbo].[RepriceHistory] DROP CONSTRAINT [FK_RepriceHistory_Inventory]
GO
ALTER TABLE [dbo].[tbl_InventoryOverstocking] DROP CONSTRAINT [FK_tbl_InventoryOverstocking__Inventory]
GO
ALTER TABLE [dbo].[tbl_VehicleSale] DROP CONSTRAINT [FK_tbl_VehicleSale__Inventory]
GO
ALTER TABLE [dbo].[VehiclePlanTracking] DROP CONSTRAINT [FK_VehiclePlanTracking__Inventory]
GO


ALTER TABLE dbo.Inventory DROP CONSTRAINT PK_Inventory

CREATE CLUSTERED INDEX [IX_Inventory_BusinessUnitID_Active_Type] ON [dbo].[Inventory] 
(
	[BusinessUnitID] ASC,
	[InventoryActive] ASC,
	[InventoryType] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON,  FILLFACTOR = 90) ON [PRIMARY]
GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Inventory]') AND name = N'IX_Inventory__BusinessUnitID_EtAl')
DROP INDEX [IX_Inventory__BusinessUnitID_EtAl] ON [dbo].[Inventory] WITH ( ONLINE = OFF )

ALTER TABLE dbo.Inventory ADD CONSTRAINT PK_Inventory PRIMARY KEY NONCLUSTERED (InventoryID)
GO

------------------------------------------------------------------------------------------------------------

ALTER TABLE [dbo].[AIP_Event]  WITH CHECK ADD  CONSTRAINT [FK_AIP_Event_InventoryID] FOREIGN KEY([InventoryID])
REFERENCES [dbo].[Inventory] ([InventoryID])
GO
ALTER TABLE [dbo].[AIP_Event] CHECK CONSTRAINT [FK_AIP_Event_InventoryID]
GO

ALTER TABLE [dbo].[CIAInventoryOverstocking]  WITH CHECK ADD  CONSTRAINT [FK_CIAInventoryOverstocking__Inventory] FOREIGN KEY([InventoryID])
REFERENCES [dbo].[Inventory] ([InventoryID])
GO
ALTER TABLE [dbo].[CIAInventoryOverstocking] CHECK CONSTRAINT [FK_CIAInventoryOverstocking__Inventory]
GO

ALTER TABLE [dbo].[DemoDealerBaseline_Inventory]  WITH CHECK ADD  CONSTRAINT [FK_DemoDealerBaseline_Inventory__Inventory] FOREIGN KEY([InventoryID])
REFERENCES [dbo].[Inventory] ([InventoryID])
GO
ALTER TABLE [dbo].[DemoDealerBaseline_Inventory] CHECK CONSTRAINT [FK_DemoDealerBaseline_Inventory__Inventory]
GO

ALTER TABLE [dbo].[InternetAdvertiserBuildList]  WITH CHECK ADD  CONSTRAINT [FK_InternetAdvertiserBuildList__Inventory] FOREIGN KEY([InventoryID])
REFERENCES [dbo].[Inventory] ([InventoryID])
GO
ALTER TABLE [dbo].[InternetAdvertiserBuildList] CHECK CONSTRAINT [FK_InternetAdvertiserBuildList__Inventory]
GO

ALTER TABLE [dbo].[Inventory_Advertising]  WITH CHECK ADD  CONSTRAINT [FK_Inventory_Advertising__InventoryID] FOREIGN KEY([InventoryID])
REFERENCES [dbo].[Inventory] ([InventoryID])
GO
ALTER TABLE [dbo].[Inventory_Advertising] CHECK CONSTRAINT [FK_Inventory_Advertising__InventoryID]
GO

ALTER TABLE [dbo].[Inventory_Insight]  WITH CHECK ADD  CONSTRAINT [FK_Inventory_Insight__InventoryID] FOREIGN KEY([InventoryID])
REFERENCES [dbo].[Inventory] ([InventoryID])
GO
ALTER TABLE [dbo].[Inventory_Insight] CHECK CONSTRAINT [FK_Inventory_Insight__InventoryID]
GO

ALTER TABLE [dbo].[Inventory_Tracking]  WITH CHECK ADD  CONSTRAINT [FK_Inventory_Tracking__InventoryID] FOREIGN KEY([InventoryID])
REFERENCES [dbo].[Inventory] ([InventoryID])
GO
ALTER TABLE [dbo].[Inventory_Tracking] CHECK CONSTRAINT [FK_Inventory_Tracking__InventoryID]
GO

ALTER TABLE [dbo].[InventoryBookouts]  WITH CHECK ADD  CONSTRAINT [FK_InventoryBookouts__Inventory] FOREIGN KEY([InventoryID])
REFERENCES [dbo].[Inventory] ([InventoryID])
GO
ALTER TABLE [dbo].[InventoryBookouts] CHECK CONSTRAINT [FK_InventoryBookouts__Inventory]
GO

ALTER TABLE [dbo].[InventoryPhotos]  WITH CHECK ADD  CONSTRAINT [FK_InventoryPhotos__Inventory] FOREIGN KEY([InventoryID])
REFERENCES [dbo].[Inventory] ([InventoryID])
GO
ALTER TABLE [dbo].[InventoryPhotos] CHECK CONSTRAINT [FK_InventoryPhotos__Inventory]
GO

ALTER TABLE [dbo].[InventoryThirdPartyVehicles]  WITH NOCHECK ADD  CONSTRAINT [FK_InventoryThirdPartyVehicles__Inventory] FOREIGN KEY([InventoryID])
REFERENCES [dbo].[Inventory] ([InventoryID])
GO
ALTER TABLE [dbo].[InventoryThirdPartyVehicles] NOCHECK CONSTRAINT [FK_InventoryThirdPartyVehicles__Inventory]
GO

ALTER TABLE [dbo].[MarketplaceSubmission]  WITH CHECK ADD  CONSTRAINT [FK_MarketplaceSubmission_Inventory] FOREIGN KEY([InventoryID])
REFERENCES [dbo].[Inventory] ([InventoryID])
GO
ALTER TABLE [dbo].[MarketplaceSubmission] CHECK CONSTRAINT [FK_MarketplaceSubmission_Inventory]
GO

ALTER TABLE [dbo].[RepriceHistory]  WITH CHECK ADD  CONSTRAINT [FK_RepriceHistory_Inventory] FOREIGN KEY([InventoryID])
REFERENCES [dbo].[Inventory] ([InventoryID])
GO
ALTER TABLE [dbo].[RepriceHistory] CHECK CONSTRAINT [FK_RepriceHistory_Inventory]
GO

ALTER TABLE [dbo].[tbl_InventoryOverstocking]  WITH CHECK ADD  CONSTRAINT [FK_tbl_InventoryOverstocking__Inventory] FOREIGN KEY([InventoryId])
REFERENCES [dbo].[Inventory] ([InventoryID])
GO
ALTER TABLE [dbo].[tbl_InventoryOverstocking] CHECK CONSTRAINT [FK_tbl_InventoryOverstocking__Inventory]
GO

ALTER TABLE [dbo].[tbl_VehicleSale]  WITH CHECK ADD  CONSTRAINT [FK_tbl_VehicleSale__Inventory] FOREIGN KEY([InventoryID])
REFERENCES [dbo].[Inventory] ([InventoryID])
GO
ALTER TABLE [dbo].[tbl_VehicleSale] CHECK CONSTRAINT [FK_tbl_VehicleSale__Inventory]
GO

ALTER TABLE [dbo].[VehiclePlanTracking]  WITH CHECK ADD  CONSTRAINT [FK_VehiclePlanTracking__Inventory] FOREIGN KEY([InventoryID])
REFERENCES [dbo].[Inventory] ([InventoryID])
GO
ALTER TABLE [dbo].[VehiclePlanTracking] CHECK CONSTRAINT [FK_VehiclePlanTracking__Inventory]
GO
