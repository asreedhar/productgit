DROP TABLE InventoryThirdPartyVehicles
DROP TABLE AppraisalThirdPartyVehicles

ALTER TABLE BookoutThirdPartyVehicles DROP COLUMN Legacy_object_id
ALTER TABLE BookoutThirdPartyVehicles DROP COLUMN Legacy_primary_key
GO