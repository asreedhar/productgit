
INSERT INTO dbo.InventoryBuckets VALUES ('Dealer Group Command Center Inventory Breakdown', 2)
GO

INSERT INTO dbo.InventoryBucketRanges (InventoryBucketID, RangeID, Description, LOW, HIGH, Lights, BusinessUnitID, LongDescription, SortOrder)
SELECT	InventoryBucketID, 1, '0-29', 0, 29, 3, 100150, NULL, 1
FROM	dbo.InventoryBuckets
WHERE	description = 'Dealer Group Command Center Inventory Breakdown'
GO

INSERT INTO dbo.InventoryBucketRanges (InventoryBucketID, RangeID, Description, LOW, HIGH, Lights, BusinessUnitID, LongDescription, SortOrder)
SELECT	InventoryBucketID, 2, '0-29', 0, 29, 4, 100150, NULL, 2
FROM	dbo.InventoryBuckets
WHERE	description = 'Dealer Group Command Center Inventory Breakdown'
GO

INSERT INTO dbo.InventoryBucketRanges (InventoryBucketID, RangeID, Description, LOW, HIGH, Lights, BusinessUnitID, LongDescription, SortOrder)
SELECT	InventoryBucketID, 3, '30-39', 30, 39, 7, 100150, NULL, 3
FROM	dbo.InventoryBuckets
WHERE	description = 'Dealer Group Command Center Inventory Breakdown'
GO

INSERT INTO dbo.InventoryBucketRanges (InventoryBucketID, RangeID, Description, LOW, HIGH, Lights, BusinessUnitID, LongDescription, SortOrder)
SELECT	InventoryBucketID, 4, '40-49', 40, 49, 7, 100150, NULL, 4
FROM	dbo.InventoryBuckets
WHERE	description = 'Dealer Group Command Center Inventory Breakdown'
GO

INSERT INTO dbo.InventoryBucketRanges (InventoryBucketID, RangeID, Description, LOW, HIGH, Lights, BusinessUnitID, LongDescription, SortOrder)
SELECT	InventoryBucketID, 5, '50-59', 50, 59, 7, 100150, NULL, 5
FROM	dbo.InventoryBuckets
WHERE	description = 'Dealer Group Command Center Inventory Breakdown'
GO

INSERT INTO dbo.InventoryBucketRanges (InventoryBucketID, RangeID, Description, LOW, HIGH, Lights, BusinessUnitID, LongDescription, SortOrder)
SELECT	InventoryBucketID, 6, '60+', 60, NULL, 7, 100150, NULL, 6
FROM	dbo.InventoryBuckets
WHERE	description = 'Dealer Group Command Center Inventory Breakdown'
GO