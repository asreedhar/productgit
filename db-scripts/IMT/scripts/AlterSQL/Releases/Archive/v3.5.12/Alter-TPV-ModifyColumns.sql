
ALTER TABLE ThirdPartyVehicleOptionValues DROP COLUMN DateModified
ALTER TABLE ThirdPartyVehicleOptionValues DROP CONSTRAINT DF_ThirdPartyVehicleOptionValues_DateCreated
ALTER TABLE ThirdPartyVehicleOptionValues ALTER COLUMN DateCreated SMALLDATETIME 
ALTER TABLE ThirdPartyVehicleOptionValues ADD CONSTRAINT DF_ThirdPartyVehicleOptionValues_DateCreated DEFAULT (GETDATE()) FOR DateCreated

ALTER TABLE ThirdPartyVehicleOptions DROP COLUMN DateModified
ALTER TABLE ThirdPartyVehicleOptions DROP CONSTRAINT DF_ThirdPartyVehicleOptions_DateCreated2
ALTER TABLE ThirdPartyVehicleOptions ALTER COLUMN DateCreated SMALLDATETIME
ALTER TABLE ThirdPartyVehicleOptions ADD CONSTRAINT DF_ThirdPartyVehicleOptions_DateCreated DEFAULT (GETDATE()) FOR DateCreated




/*
--------------------------------------------------------------------------------------------------------------------
--	WE CAN'T DO THIS AS THE TIMESTAMP IS PART OF THE UNIQUE KEY...NOT SURE THAT IS CORRECT BUT CAN'T DROP JUST 
--	YET AS CHANGING CAUSES A UK VIOLATION!
--------------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------
--	CHANGE DateCreated TO SMALLDATETIME
--------------------------------------------------------------------------------------------------------------------

ALTER TABLE Appraisals DROP CONSTRAINT UK_Appraisals
ALTER TABLE Appraisals DROP CONSTRAINT DF_Appraisals_DateCreated
ALTER TABLE Appraisals ALTER COLUMN DateCreated SMALLDATETIME NOT NULL 
ALTER TABLE Appraisals ADD CONSTRAINT DF_Appraisals_DateCreated DEFAULT (GETDATE()) FOR DateCreated
ALTER TABLE Appraisals ADD CONSTRAINT UK_Appraisals UNIQUE NONCLUSTERED (BusinessUnitID ASC,VehicleID ASC, DateCreated ASC) 
*/ 