/*

Drop and recreate all foreign keys where:
 - Was created with NOCHECK, or
 - Was created with cascading deletes, or
 - Was created with cascading updates
 - But only where the key can be created WITH CHECK (which excludes the MarketShare and 
    tbl_VehicleSale tables)

Based on all such FKs found on both Hyperion.IMT and proddb01sql.IMT
All FKs below exist on both these database instances


--  Use this to preview for WITH CHECK violations
select STATUS, count(*) from Audit_Inventory where STATUS not in (select AuditStatusCD from lu_AuditStatus) group by STATUS
select STATUS, count(*) from Audit_Sales where STATUS not in (select AuditStatusCD from lu_AuditStatus) group by STATUS
select InventoryID, count(*) from CIAInventoryOverstocking where InventoryID not in (select InventoryID from Inventory) group by InventoryID
select BusinessUnitID, count(*) from DealerPack where BusinessUnitID not in (select BusinessUnitID from BusinessUnit) group by BusinessUnitID
select PackTypeCD, count(*) from DealerPack where PackTypeCD not in (select PackTypeCD from lu_DealerPackType) group by PackTypeCD
select BusinessUnitID, count(*) from DealerPreference where BusinessUnitID not in (select BusinessUnitID from BusinessUnit) group by BusinessUnitID
select ProgramType_CD, count(*) from DealerPreference where ProgramType_CD not in (select ProgramType_CD from lu_ProgramType) group by ProgramType_CD
select BusinessUnitID, count(*) from DealerPreferenceDataload where BusinessUnitID not in (select BusinessUnitID from DealerPreference) group by BusinessUnitID
select BusinessUnitId, count(*) from DealerRisk where BusinessUnitId not in (select BusinessUnitID from DealerPreference) group by BusinessUnitId
select FranchiseID, count(*) from FranchiseAlias where FranchiseID not in (select FranchiseID from Franchise) group by FranchiseID
select GroupingDescriptionId, count(*) from GDLight where GroupingDescriptionId not in (select GroupingDescriptionID from tbl_GroupingDescription) group by GroupingDescriptionId
select BusinessUnitID, count(*) from Inventory where BusinessUnitID not in (select BusinessUnitID from BusinessUnit) group by BusinessUnitID
select InventoryType, count(*) from Inventory where InventoryType not in (select InventoryVehicleTypeCD from lu_InventoryVehicleType) group by InventoryType
select VehicleID, count(*) from Inventory where VehicleID not in (select VehicleID from tbl_Vehicle) group by VehicleID
select InventoryID, count(*) from InventoryBookouts where InventoryID not in (select InventoryID from Inventory) group by InventoryID
select InventoryID, count(*) from InventoryThirdPartyVehicles where InventoryID not in (select InventoryID from Inventory) group by InventoryID
select CredentialTypeID, count(*) from MemberCredential where CredentialTypeID not in (select CredentialTypeID from CredentialType) group by CredentialTypeID
select MemberID, count(*) from MemberCredential where MemberID not in (select MemberID from Member) group by MemberID
select TargetTypeCD, count(*) from Target where TargetTypeCD not in (select TargetTypeCD from TargetType) group by TargetTypeCD
select BusinessUnitId, count(*) from tbl_CIAPreferences where BusinessUnitId not in (select BusinessUnitID from BusinessUnit) group by BusinessUnitId
select GroupingDescriptionId, count(*) from tbl_GroupingPromotionPlan where GroupingDescriptionId not in (select GroupingDescriptionID from tbl_GroupingDescription) group by GroupingDescriptionId
select InventoryId, count(*) from tbl_InventoryOverstocking where InventoryId not in (select InventoryID from Inventory) group by InventoryId
select GroupingDescriptionID, count(*) from tbl_MakeModelGrouping where GroupingDescriptionID not in (select GroupingDescriptionID from tbl_GroupingDescription) group by GroupingDescriptionID
select InventoryID, count(*) from tbl_VehicleSale where InventoryID not in (select InventoryID from Inventory) group by InventoryID
select BusinessUnitID, count(*) from VehicleLocatorContactDealerEvent where BusinessUnitID not in (select BusinessUnitID from BusinessUnit) group by BusinessUnitID
select SiteAuditEventID, count(*) from VehicleLocatorContactDealerEvent where SiteAuditEventID not in (select SiteAuditEventID from SiteAuditEvent) group by SiteAuditEventID
select InventoryID, count(*) from VehiclePlanTracking where InventoryID not in (select InventoryID from Inventory) group by InventoryID
select BusinessUnitID, count(*) from VehiclePlanTrackingHeader where BusinessUnitID not in (select BusinessUnitID from BusinessUnit) group by BusinessUnitID

*/

ALTER TABLE Audit_Inventory drop constraint FK_Audit_Inventory_lu_AuditStatus  
ALTER TABLE Audit_Inventory with check add constraint FK_Audit_Inventory__lu_AuditStatus foreign key (STATUS) references lu_AuditStatus (AuditStatusCD)

ALTER TABLE Audit_Sales drop constraint FK_Audit_Sales_lu_AuditStatus  
ALTER TABLE Audit_Sales with check add constraint FK_Audit_Sales__lu_AuditStatus foreign key (STATUS) references lu_AuditStatus (AuditStatusCD)

ALTER TABLE CIAInventoryOverstocking drop constraint FK_CIAInventoryOverstocking_Inventory  
ALTER TABLE CIAInventoryOverstocking with check add constraint FK_CIAInventoryOverstocking__Inventory foreign key (InventoryID) references Inventory (InventoryID)

ALTER TABLE DealerPack drop constraint FK_DealerPack_BusinessUnit  
ALTER TABLE DealerPack with check add constraint FK_DealerPack__BusinessUnit foreign key (BusinessUnitID) references BusinessUnit (BusinessUnitID)

ALTER TABLE DealerPack drop constraint FK_DealerPack_lu_DealerPackType  
ALTER TABLE DealerPack with check add constraint FK_DealerPack__lu_DealerPackType foreign key (PackTypeCD) references lu_DealerPackType (PackTypeCD)

ALTER TABLE DealerPreference drop constraint FK_DealerPreference_BusinessUnitID  
ALTER TABLE DealerPreference with check add constraint FK_DealerPreference__BusinessUnit foreign key (BusinessUnitID) references BusinessUnit (BusinessUnitID)

ALTER TABLE DealerPreference drop constraint FK_DealerPreference_lu_ProgramType  
ALTER TABLE DealerPreference with check add constraint FK_DealerPreference__lu_ProgramType foreign key (ProgramType_CD) references lu_ProgramType (ProgramType_CD)

ALTER TABLE DealerPreferenceDataload drop constraint FK_DealerPreferenceDataload_DealerPreference  
ALTER TABLE DealerPreferenceDataload with check add constraint FK_DealerPreferenceDataload__DealerPreference foreign key (BusinessUnitID) references DealerPreference (BusinessUnitID)

ALTER TABLE DealerRisk drop constraint FK_DealerRisk_DealerPreference  
ALTER TABLE DealerRisk with check add constraint FK_DealerRisk__DealerPreference foreign key (BusinessUnitId) references DealerPreference (BusinessUnitID)

ALTER TABLE FranchiseAlias drop constraint FK_FranchiseAlias_FranchiseID  
ALTER TABLE FranchiseAlias with check add constraint FK_FranchiseAlias__Franchise foreign key (FranchiseID) references Franchise (FranchiseID)

ALTER TABLE GDLight drop constraint FK_GDLight_GroupingDescription  
ALTER TABLE GDLight with check add constraint FK_GDLight__tbl_GroupingDescription foreign key (GroupingDescriptionId) references tbl_GroupingDescription (GroupingDescriptionID)

ALTER TABLE Inventory drop constraint FK_Inventory_BusinessUnitID  
ALTER TABLE Inventory with check add constraint FK_Inventory__BusinessUnit foreign key (BusinessUnitID) references BusinessUnit (BusinessUnitID)

ALTER TABLE Inventory drop constraint FK_Inventory_InventoryType  
ALTER TABLE Inventory with check add constraint FK_Inventory__lu_InventoryVehicleType foreign key (InventoryType) references lu_InventoryVehicleType (InventoryVehicleTypeCD)

ALTER TABLE Inventory drop constraint FK_Inventory_tbl_Vehicle_VehicleID  
ALTER TABLE Inventory with check add constraint FK_Inventory__tbl_Vehicle foreign key (VehicleID) references tbl_Vehicle (VehicleID)

ALTER TABLE InventoryBookouts drop constraint FK_InventoryBookouts_Inventory  
ALTER TABLE InventoryBookouts with check add constraint FK_InventoryBookouts__Inventory foreign key (InventoryID) references Inventory (InventoryID)

ALTER TABLE InventoryThirdPartyVehicles drop constraint FK_InventoryThirdPartyVehicles_Inventory1  
ALTER TABLE InventoryThirdPartyVehicles with check add constraint FK_InventoryThirdPartyVehicles__Inventory foreign key (InventoryID) references Inventory (InventoryID)

ALTER TABLE MemberCredential drop constraint FK_MemberCredential_CredentialTypeID  
ALTER TABLE MemberCredential with check add constraint FK_MemberCredential__CredentialType foreign key (CredentialTypeID) references CredentialType (CredentialTypeID)

ALTER TABLE MemberCredential drop constraint FK_MemberCredential_MemberID  
ALTER TABLE MemberCredential with check add constraint FK_MemberCredential__Member foreign key (MemberID) references Member (MemberID)

ALTER TABLE Target drop constraint FK_Target_TargetType  
ALTER TABLE Target with check add constraint FK_Target__TargetType foreign key (TargetTypeCD) references TargetType (TargetTypeCD)

ALTER TABLE tbl_CIAPreferences drop constraint FK_tbl_CIAPreferences_BusinessUnit  
ALTER TABLE tbl_CIAPreferences with check add constraint FK_tbl_CIAPreferences__BusinessUnit foreign key (BusinessUnitId) references BusinessUnit (BusinessUnitID)

ALTER TABLE tbl_GroupingPromotionPlan drop constraint FK_GroupingPromotionPlan_tbl_GroupingDescription  
ALTER TABLE tbl_GroupingPromotionPlan with check add constraint FK_tbl_GroupingPromotionPlan__tbl_GroupingDescription foreign key (GroupingDescriptionId) references tbl_GroupingDescription (GroupingDescriptionID)

ALTER TABLE tbl_InventoryOverstocking drop constraint FK_tbl_InventoryOverstocking__Inventory  
ALTER TABLE tbl_InventoryOverstocking with check add constraint FK_tbl_InventoryOverstocking__Inventory foreign key (InventoryId) references Inventory (InventoryID)

ALTER TABLE tbl_MakeModelGrouping drop constraint FK_MakeModelGrouping_GroupingDescriptionID  
ALTER TABLE tbl_MakeModelGrouping with check add constraint FK_tbl_MakeModelGrouping__tbl_GroupingDescription foreign key (GroupingDescriptionID) references tbl_GroupingDescription (GroupingDescriptionID)

ALTER TABLE tbl_VehicleSale drop constraint FK_tbl_VehicleSale_Inventory  
ALTER TABLE tbl_VehicleSale with check add constraint FK_tbl_VehicleSale__Inventory foreign key (InventoryID) references Inventory (InventoryID)

ALTER TABLE VehicleLocatorContactDealerEvent drop constraint VehicleLocatorContactDealerEvent_BusinessUnitID  
ALTER TABLE VehicleLocatorContactDealerEvent with check add constraint FK_VehicleLocatorContactDealerEvent__BusinessUnit foreign key (BusinessUnitID) references BusinessUnit (BusinessUnitID)

ALTER TABLE VehicleLocatorContactDealerEvent drop constraint VehicleLocatorContactDealerEvent_SiteAuditEventID  
ALTER TABLE VehicleLocatorContactDealerEvent with check add constraint FK_VehicleLocatorContactDealerEvent__SiteAuditEvent foreign key (SiteAuditEventID) references SiteAuditEvent (SiteAuditEventID)

ALTER TABLE VehiclePlanTracking drop constraint FK_VehiclePlanTracking_InventoryID  
ALTER TABLE VehiclePlanTracking with check add constraint FK_VehiclePlanTracking__Inventory foreign key (InventoryID) references Inventory (InventoryID)

ALTER TABLE VehiclePlanTrackingHeader drop constraint FK_VehiclePlanTrackingHeader_BusinessUnitID  
ALTER TABLE VehiclePlanTrackingHeader with check add constraint FK_VehiclePlanTrackingHeader__BusinessUnit foreign key (BusinessUnitID) references BusinessUnit (BusinessUnitID)
