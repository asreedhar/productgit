----------------------------------------------------------------------------------------------------
--	Add sort preference flag for inventory overview page
--	0 = "All Alphabetically"
--	1 = "Segment"
----------------------------------------------------------------------------------------------------

ALTER TABLE dbo.Member ADD InventoryOverviewSortOrderType TINYINT NOT NULL CONSTRAINT DF_Member__InventoryOverviewSortOrderType DEFAULT (0)
GO

ALTER TABLE dbo.Member ADD CONSTRAINT CK_Member__InventoryOverviewSortOrderType CHECK (InventoryOverviewSortOrderType IN (0,1))
GO
