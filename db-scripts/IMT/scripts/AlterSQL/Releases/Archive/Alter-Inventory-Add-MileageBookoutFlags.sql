ALTER TABLE dbo.Inventory ADD BookoutRequired TINYINT NOT NULL CONSTRAINT DF_Inventory__BookoutRequired DEFAULT (0)
GO
ALTER TABLE dbo.Inventory ADD MileageReceivedLock TINYINT NOT NULL CONSTRAINT DF_Inventory__MileageReceivedLock DEFAULT (0)
GO
