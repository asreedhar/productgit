UPDATE A
SET A.High = B.High
FROM InventoryBucketRanges A
JOIN InventoryBucketRanges B
	ON A.InventoryBucketID = B.InventoryBucketID
	AND A.BusinessUnitID = B.BusinessUnitID
WHERE A.InventoryBucketID = 4
AND A.RangeID = 1 -- WatchList
AND B.RangeID = 2 -- 1st bucket
AND A.High <> B.High
