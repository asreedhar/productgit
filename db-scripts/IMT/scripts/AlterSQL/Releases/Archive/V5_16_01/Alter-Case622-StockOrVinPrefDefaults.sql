UPDATE dbo.DealerPreference
SET AgingInventoryTrackingDisplayPref = 0
WHERE AgingInventoryTrackingDisplayPref IS NULL

UPDATE dbo.DealerPreference
SET StockOrVinPreference = 0
WHERE StockOrVinPreference IS NULL


ALTER TABLE dbo.DealerPreference ALTER COLUMN StockOrVinPreference TINYINT NOT NULL

ALTER TABLE dbo.DealerPreference ALTER COLUMN AgingInventoryTrackingDisplayPref TINYINT NOT NULL


ALTER TABLE dbo.DealerPreference ADD CONSTRAINT DF_DealerPreference_StockOrVinPreference DEFAULT(1) FOR [StockOrVinPreference]

ALTER TABLE dbo.DealerPreference ADD CONSTRAINT DF_DealerPreference_AgingInventoryTrackingDisplayPref DEFAULT(1) FOR [AgingInventoryTrackingDisplayPref]