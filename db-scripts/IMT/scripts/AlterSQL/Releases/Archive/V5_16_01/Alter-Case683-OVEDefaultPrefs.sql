ALTER TABLE dbo.DealerPreference DROP CONSTRAINT DF_DealerPreference_OveEnabled
ALTER TABLE dbo.DealerPreference WITH NOCHECK ADD CONSTRAINT [DF_DealerPreference_OveEnabled] DEFAULT (0) FOR [OVEEnabled]

UPDATE dbo.DealerPreference
SET OVEEnabled = 0