--------------------------------------------------------------------------------
--
--	DELETE UNNECESSARY TPV COLUMNS AND ADD COLUMNS AS REQ'D FOR GALVES
--
--------------------------------------------------------------------------------

ALTER TABLE DealerPreference ADD UseWindowStickerPrice tinyint NOT NULL default(0)
GO

ALTER TABLE dbo.Inventory ADD WindowStickerPrice INT NOT NULL CONSTRAINT DF_Inventory__WindowStickerPrice DEFAULT (0) 
GO

UPDATE dbo.Inventory
SET WindowStickerPrice=COALESCE(ROUND(ListPrice,0),0)
GO