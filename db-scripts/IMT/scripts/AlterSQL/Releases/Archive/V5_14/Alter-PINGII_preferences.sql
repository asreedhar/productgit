
-- Add Ping II Upgrade ID
INSERT INTO dbo.lu_DealerUpgrade
SELECT 19, 'PING II'
GO

-- Add dealer level preferences for Ping II
ALTER TABLE DealerPreference ADD PingII_SearchRadius INT NULL DEFAULT null
GO 

ALTER TABLE DealerPreference ADD PingII_SupressSellerName TINYINT NULL DEFAULT null
GO 

ALTER TABLE DealerPreference ADD PingII_ExcludeNoPriceFromCalc TINYINT NULL DEFAULT null
GO

ALTER TABLE DealerPreference ADD PingII_ExcludeLowPriceOutliersMultiplier DECIMAL(4,2) NULL DEFAULT null
GO 

ALTER TABLE DealerPreference ADD PingII_ExcludeHighPriceOutliersMultiplier DECIMAL(4,2) NULL DEFAULT null
GO 

-- Add 100150 (firstlook) row to dealer prefence to act as system level prefernces
-- so if a business unit has is null for a pingII pref, it rolls up to the firstlook rows preference
INSERT 
INTO 	DealerPreference (
		BusinessUnitID,
		CustomHomePageMessage, DashboardColumnDisplayPreference, 
		UnitsSoldThreshold4Wks, UnitsSoldThreshold8Wks, UnitsSoldThreshold13Wks, 
		UnitsSoldThreshold26Wks, UnitsSoldThreshold52Wks, DefaultTrendingView, 
		DefaultTrendingWeeks, DefaultForecastingWeeks, GuideBookID, NADARegionCode, 
		InceptionDate, AgingInventoryTrackingDisplayPref, PackAmount, StockOrVinPreference,
		MaxSalesHistoryDate, UnitCostOrListPricePreference, ListPricePreference, AgeBandTarget1,
		AgeBandTarget2, AgeBandTarget3, AgeBandTarget4, AgeBandTarget5, AgeBandTarget6, DaysSupply12WeekWeight,
		DaysSupply26WeekWeight, BookOut, BookOutPreferenceId, UnitCostThreshold,
		BookOutSecondPreferenceId, GuideBook2Id, GuideBook2BookOutPreferenceId, 
		GuideBook2SecondBookOutPreferenceId, UnitCostUpdateOnSale, UnwindDaysThreshold, 
		AuctionAreaId, showLotLocationStatus, PopulateClassName,BookOutPreferenceSecondId,
		RunDayOfWeek, UnitCostThresholdLower, UnitCostThresholdUpper, FEGrossProfitThreshold,
		MarginPercentile, DaysToSalePercentile, ProgramType_CD, SellThroughRate, IncludeBackEndInValuation,
		UnitsSoldThresholdInvOverview, CIATargetDaysSupply, CIAMarketPerformerInStockThreshold, 
		GoLiveDate, VehicleSaleThresholdForCoreMarketPenetration, ATCEnabled, ApplyPriorAgingNotes, 
		averageInventoryAgeRedThreshold, averageDaysSupplyRedThreshold, PurchasingDistanceFromDealer, 
		DisplayUnitCostToDealerGroup, UnitsSoldThreshold12Wks, SearchInactiveInventoryDaysBackThreshold, 
		CalculateAverageBookValue, CarfaxListVINsOnWebsite, TFSEnabled, GMACEnabled, TradeManagerDaysFilter, 
		ShowroomDaysFilter, bucketJumperAIPApproach, aipRunDayOfWeek, FlashLocatorHideUnitCostDays, 
		AppraisalRequirementLevel, CheckAppraisalHistoryForIMPPlanning, ApplyDefaultPlanToGreenLightsInIMP, 
		AppraisalFormShowOptions, AppraisalFormShowPhotos, AppraisalFormValidDate, AppraisalFormValidMileage,
		AppraisalFormDisclaimer, AppraisalFormMemo, ShowAppraisalForm, LiveAuctionDistanceFromDealer, 
		RepricePercentChangeThreshold, RepriceConfirmation, InternetAdvertiser_AdPriceEnabled, 
		InternetAdvertiser_SendZeroesAsNull, KBBInventoryBookoutDatasetPreference, KBBAppraisalBookoutDatasetPreference, 
		KBBInventoryDefaultCondition, DisplayTMV, GroupAppraisalSearchWeeks, ShowAppraisalValueGroup, 
		ShowAppraisalFormOfferGroup, RedistributionNumTopDealers, RedistributionDealerDistance, RedistributionROI, 
		RedistributionUnderstock, AuctionTimePeriodID, OVEEnabled, ShowCheckOnAppraisalForm, ShowInactiveAppraisals, 
		AppraisalFormValuesTPCBitMask, AppraisalFormIncludeMileageAdjustment, WindowStickerTemplate, DefaultLithiaCarCenterMemberID, twixURL,
		
		PingII_SearchRadius, PingII_SupressSellerName, PingII_ExcludeLowPriceOutliersMultiplier, PingII_ExcludeHighPriceOutliersMultiplier
		)		



SELECT	100150, CustomHomePageMessage, DashboardColumnDisplayPreference, 
		UnitsSoldThreshold4Wks, UnitsSoldThreshold8Wks, UnitsSoldThreshold13Wks, 
		UnitsSoldThreshold26Wks, UnitsSoldThreshold52Wks, DefaultTrendingView, 
		DefaultTrendingWeeks, DefaultForecastingWeeks, GuideBookID, NADARegionCode, 
		InceptionDate, AgingInventoryTrackingDisplayPref, PackAmount, StockOrVinPreference,
		MaxSalesHistoryDate, UnitCostOrListPricePreference, ListPricePreference, AgeBandTarget1,
		AgeBandTarget2, AgeBandTarget3, AgeBandTarget4, AgeBandTarget5, AgeBandTarget6, DaysSupply12WeekWeight,
		DaysSupply26WeekWeight, BookOut, BookOutPreferenceId, UnitCostThreshold,
		BookOutSecondPreferenceId, GuideBook2Id, GuideBook2BookOutPreferenceId, 
		GuideBook2SecondBookOutPreferenceId, UnitCostUpdateOnSale, UnwindDaysThreshold, 
		AuctionAreaId, showLotLocationStatus, PopulateClassName,BookOutPreferenceSecondId,
		RunDayOfWeek, UnitCostThresholdLower, UnitCostThresholdUpper, FEGrossProfitThreshold,
		MarginPercentile, DaysToSalePercentile, ProgramType_CD, SellThroughRate, IncludeBackEndInValuation,
		UnitsSoldThresholdInvOverview, CIATargetDaysSupply, CIAMarketPerformerInStockThreshold, 
		GoLiveDate, VehicleSaleThresholdForCoreMarketPenetration, ATCEnabled, ApplyPriorAgingNotes, 
		averageInventoryAgeRedThreshold, averageDaysSupplyRedThreshold, PurchasingDistanceFromDealer, 
		DisplayUnitCostToDealerGroup, UnitsSoldThreshold12Wks, SearchInactiveInventoryDaysBackThreshold, 
		CalculateAverageBookValue, CarfaxListVINsOnWebsite, TFSEnabled, GMACEnabled, TradeManagerDaysFilter, 
		ShowroomDaysFilter, bucketJumperAIPApproach, aipRunDayOfWeek, FlashLocatorHideUnitCostDays, 
		AppraisalRequirementLevel, CheckAppraisalHistoryForIMPPlanning, ApplyDefaultPlanToGreenLightsInIMP, 
		AppraisalFormShowOptions, AppraisalFormShowPhotos, AppraisalFormValidDate, AppraisalFormValidMileage,
		AppraisalFormDisclaimer, AppraisalFormMemo, ShowAppraisalForm, LiveAuctionDistanceFromDealer, 
		RepricePercentChangeThreshold, RepriceConfirmation, InternetAdvertiser_AdPriceEnabled, 
		InternetAdvertiser_SendZeroesAsNull, KBBInventoryBookoutDatasetPreference, KBBAppraisalBookoutDatasetPreference, 
		KBBInventoryDefaultCondition, DisplayTMV, GroupAppraisalSearchWeeks, ShowAppraisalValueGroup, 
		ShowAppraisalFormOfferGroup, RedistributionNumTopDealers, RedistributionDealerDistance, RedistributionROI, 
		RedistributionUnderstock, AuctionTimePeriodID, OVEEnabled, ShowCheckOnAppraisalForm, ShowInactiveAppraisals, 
		AppraisalFormValuesTPCBitMask, AppraisalFormIncludeMileageAdjustment, WindowStickerTemplate, DefaultLithiaCarCenterMemberID, twixURL,
		25, 1, 1, .25, 5.0
FROM	dealerpreference 
WHERE	businessUnitId = 100002
GO

-- Add new Inventory Bucket Ranges for Ping II
ALTER TABLE InventoryBucketRanges ADD Value1 SMALLINT NULL DEFAULT NULL
GO

ALTER TABLE InventoryBucketRanges ADD Value2 SMALLINT NULL DEFAULT NULL
GO

ALTER TABLE InventoryBucketRanges ADD Value3 SMALLINT NULL DEFAULT NULL
GO

INSERT INTO dbo.InventoryBuckets
SELECT 'PING II Pricing Age', 2
GO

DECLARE @PingBucketID INT
SELECT	@PingBucketID = InventoryBucketID
FROM	InventoryBuckets
WHERE	description = 'PING II Pricing Age'

INSERT INTO dbo.InventoryBucketRanges
SELECT	@PingBucketID, 1, '0-20', 0, 20, 7, 100150, NULL, 1, 96, 104, 0
UNION
SELECT	@PingBucketID, 2, '21-35', 21, 35, 7, 100150, NULL, 2, 96, 104, 0
UNION
SELECT	@PingBucketID, 3, '36-45', 36, 45, 7, 100150, NULL, 3, 94, 98, 0
UNION
SELECT	@PingBucketID, 4, '46-55', 46, 55, 7, 100150, NULL, 4, 94, 96, 0
UNION
SELECT	@PingBucketID, 5, '56-65', 56, 65, 7, 100150, NULL, 5, 88, 94, 0
UNION
SELECT	@PingBucketID, 6, '66+', 66, null, 7, 100150, NULL, 6, 88, 94, 0

-- add custom bucktes for the ping II stuff that match a dealers current unique IMP buckets
INSERT INTO dbo.InventoryBucketRanges
SELECT	@PingBucketID, RangeID - 1, 
		CASE WHEN HIGH IS NOT NULL THEN CAST(LOW AS VARCHAR(2)) + ' - ' + CAST(HIGH AS VARCHAR(2)) ELSE CAST(LOW AS VARCHAR(2)) + '+' END ,
		Low, High, 7, BusinessUnitID, LongDescription, RangeID - 1, Value1, Value2, Value3
FROM	inventorybucketranges
WHERE	inventoryBucketID = 4
		AND businessUnitid <> 100150
		AND Description <> 'WATCH LIST'

UPDATE	ibr1
SET		Value1 = ibr2.value1, Value2 = ibr2.Value2, Value3 = ibr2.Value3
FROM	InventoryBucketRanges ibr1 
		JOIN InventoryBucketRanges ibr2 ON ibr2.rangeid = ibr1.rangeID
WHERE	ibr2.businessUnitID = 100150
		AND ibr2.inventoryBucketID = @PingBucketID
		AND	ibr1.inventorybucketID = @PingBucketID
		AND ibr1.value1 IS NULL
GO


