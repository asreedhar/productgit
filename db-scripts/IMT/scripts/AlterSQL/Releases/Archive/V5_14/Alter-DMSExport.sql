
-- insert new DMS Export Third Party Entity Type

INSERT INTO [dbo].ThirdPartyEntityType(ThirdPartyEntityTypeID, Description)  values (7, 'DMS Export')

GO

-- insert default DMS Export Third Parties

INSERT INTO [dbo].ThirdPartyEntity(BusinessUnitID, ThirdPartyEntityTypeID, Name) values (100150, 7, 'SIS - Rey & Rey')
GO

INSERT INTO [dbo].ThirdPartyEntity(BusinessUnitID, ThirdPartyEntityTypeID, Name) values (100150, 7, 'SIS - ADP')
GO

INSERT INTO [dbo].ThirdPartyEntity(BusinessUnitID, ThirdPartyEntityTypeID, Name) values (100150, 7, 'Reynolds Direct')
GO

-- create new DMSExport 'Child' table which 'inherits' from ThirdPartyEntity

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DMSExport_ThirdPartyEntity]')) 
DROP TABLE [dbo].DMSExport_ThirdPartyEntity
GO
 
CREATE TABLE [dbo].DMSExport_ThirdPartyEntity (
	DMSExportID INT UNIQUE NOT NULL,
	Description VarChar(25)
)

GO

ALTER TABLE DMSExport_ThirdPartyEntity ADD CONSTRAINT
PK_DMSExport_ThirdPartyEntity PRIMARY KEY CLUSTERED 
(
DMSExportID
)

GO

ALTER TABLE [dbo].DMSExport_ThirdPartyEntity 
ADD CONSTRAINT FK_DMSExport_ThirdPartyEntity_ThirdPartyEntity FOREIGN KEY
(
DMSExportID
) REFERENCES [dbo].ThirdPartyEntity
(
ThirdPartyEntityID
)
GO


-- insert SIS DMSExport_ThirdPartyEntity

INSERT INTO [dbo].DMSExport_ThirdPartyEntity(DMSExportID, Description) 
	select 
		ThirdPartyEntityID, 'SIS - Rey & Rey'
	from 
		ThirdPartyEntity 
	where 
		ThirdPartyEntityTypeID=7 
		AND name='SIS - Rey & Rey'
		
INSERT INTO [dbo].DMSExport_ThirdPartyEntity(DMSExportID, Description) 
	select 
		ThirdPartyEntityID, 'SIS - ADP'
	from 
		ThirdPartyEntity 
	where 
		ThirdPartyEntityTypeID=7 
		AND name='SIS - ADP'
		
INSERT INTO [dbo].DMSExport_ThirdPartyEntity(DMSExportID, Description) 
	select 
		ThirdPartyEntityID, 'Reynolds Direct'
	from 
		ThirdPartyEntity 
	where 
		ThirdPartyEntityTypeID=7 
		AND name='Reynolds Direct'

-- create DMSExportBusinessUnitPreference

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DMSExportBusinessUnitPreference]')) 
DROP TABLE [dbo].DMSExportBusinessUnitPreference
GO
 
CREATE TABLE [dbo].DMSExportBusinessUnitPreference (
	BusinessUnitID INT PRIMARY KEY NOT NULL,
	DMSExportID INT NOT NULL,
	isActive TINYINT,
	ClientSystemID VARCHAR(11) NOT NULL,
	DMSIPAddress VARCHAR(16) NOT NULL,
	DMSDialUpPhone1 VARCHAR(16) NOT NULL,
	DMSDialUpPhone2 VARCHAR(16),
	DMSLogin VARCHAR(20) NOT NULL,
	DMSPassword VARCHAR(20) NOT NULL,
	AccountLogon VARCHAR(20) NULL,
	FinanceLogon VARCHAR(20) NULL,
	StoreNumber INT NOT NULL,
	BranchNumber INT,
	DMSExportFrequencyID TINYINT NOT NULL,
	DMSExportDailyHourOfDay INT,
	DealerName VARCHAR(40) NOT NULL,
	DealerPhoneOffice VARCHAR(16) NOT NULL,
	DealerPhoneCell VARCHAR(16) NOT NULL,
	DealerEmail VARCHAR(25) NOT NULL,
	DMSExportInternetPriceMappingID INT,
	DMSExportWindowStickerPriceMappingID INT
)
GO

-- Create and populate DMSExportFrequency look up table
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DMSExportFrequency]')) 
DROP TABLE [dbo].DMSExportFrequency
GO
 
CREATE TABLE [dbo].DMSExportFrequency (
	DMSExportFrequencyID TINYINT PRIMARY KEY  IDENTITY(1,1) NOT NULL,
	Description VARCHAR(25) 
)
GO

INSERT INTO DMSExportFrequency
SELECT	'Daily'

GO
INSERT INTO DMSExportFrequency
SELECT	'Incremental'

GO

-- Create DMS Price Mapping Table
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DMSExportPriceMapping]')) 
DROP TABLE [dbo].DMSExportPriceMapping
GO
 
CREATE TABLE [dbo].DMSExportPriceMapping (
	DMSExportPriceMappingID INT PRIMARY KEY IDENTITY(1,1) NOT NULL ,
	Description VARCHAR(25) UNIQUE
)
GO

INSERT INTO DMSExportPriceMapping
SELECT	'DMSField-1'
GO
INSERT INTO DMSExportPriceMapping
SELECT	'DMSField-2'
GO


-- Add FK contraints to DMSExportBusinessUnitPreference

ALTER TABLE [dbo].[DMSExportBusinessUnitPreference] ADD 
	CONSTRAINT [FK_DMSExportBusinessUnitPreference_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT FK_DMSExportBusinessUnitPreference_DMSExport_ThirdPartyEntity FOREIGN KEY
	(
		DMSExportID
	) REFERENCES [dbo].DMSExport_ThirdPartyEntity (
		DMSExportID
	),
	CONSTRAINT FK_DMSExportBusinessUnitPreference_DMSExportFrequency FOREIGN KEY
	(
		DMSExportFrequencyID
	) REFERENCES [dbo].DMSExportFrequency (
		DMSExportFrequencyID
	),
	CONSTRAINT FK_DMSExportBusinessUnitPreference_DMSExportInternetPriceMapping FOREIGN KEY
	(
		DMSExportInternetPriceMappingID
	) REFERENCES [dbo].DMSExportPriceMapping (
		DMSExportPriceMappingID
	),
	CONSTRAINT FK_DMSExportBusinessUnitPreference_DMSExportWindowStickerPriceMapping FOREIGN KEY
	(
		DMSExportWindowStickerPriceMappingID
	) REFERENCES [dbo].DMSExportPriceMapping (
		DMSExportPriceMappingID
	)
GO

-- Add DMS Export System Preference
INSERT INTO dbo.GlobalParam
SELECT 'SIS_URL', ''
GO

INSERT INTO dbo.GlobalParam
SELECT 'Reynolds_Direct_URL', ''
GO

-- disable ad price for the dealers who have it enabled
UPDATE	dbo.DealerPreference
SET		InternetAdvertiser_AdPriceEnabled = 0
WHERE	InternetAdvertiser_AdPriceEnabled = 1
GO


-- temp table used to map VINs and Stock between FirstLook and SIS
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[dbo.SISVinAndStockMapping]')) 
DROP TABLE [dbo].SISVinAndStockMapping
GO
 
CREATE TABLE [dbo].SISVinAndStockMapping (
	SISVin	VarChar(17) NOT NULL,
	SISStockNumber VarChar(15) NOT NULL,
	Vin	VarChar(17) NOT NULL, -- FL 
	StockNumber VarChar(15) NOT NULL, -- FL	
	BusinessUnitID INT NOT NULL
)

GO






  