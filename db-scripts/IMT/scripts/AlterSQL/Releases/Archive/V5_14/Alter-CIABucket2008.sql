
--------------------------------------------------------------------------------
--	ORIGINALLY PLANNED TO BE DONE IN AN ADMIN TOOL, THIS NEEDS TO BE DONE
--	EVERY YEAR AS LONG AS THE CIA BUCKETRON IS DRIVEN COMPLETELY FROM DATA
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- 	ADD 2008
--------------------------------------------------------------------------------

UPDATE	CIABuckets
SET	RANK = RANK + 1
WHERE	CIABucketGroupID = 1

INSERT
INTO	CIABuckets (CIABucketGroupID, Rank, Description)
SELECT	1, 1, '2008'

INSERT
INTO	CIABucketRules (CIABucketID, CIABucketRuleTypeID, Value, Value2)
SELECT	21, 1, 2008, NULL

--------------------------------------------------------------------------------
-- 	DROP 2000
--------------------------------------------------------------------------------

DELETE 
FROM	CIABucketRules
WHERE	CIABucketID = 7

DELETE 
FROM	CIABuckets
WHERE	CIABucketID = 7
