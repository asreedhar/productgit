
-- ADD REPRICE SOURCE TABLE AND ADD COLUMN TO AIP_EVENT TABLE W/ FOREIGN KEY TO REPRICE SOURCE LOOKUP TABLE
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RepriceSource]')) 
DROP TABLE dbo.RepriceSource
GO

CREATE TABLE dbo.RepriceSource (
	RepriceSourceID INT IDENTITY(1,1) NOT NULL,
	Description VarChar(25)
)
ALTER TABLE dbo.RepriceSource ADD CONSTRAINT
PK_RepriceSource PRIMARY KEY CLUSTERED 
(
RepriceSourceID
)
GO

SET IDENTITY_INSERT dbo.RepriceSource ON 

INSERT INTO dbo.RepriceSource(RepriceSourceID, Description)  values (1, 'IMP')
INSERT INTO dbo.RepriceSource(RepriceSourceID, Description) values (2, 'eStock')
INSERT INTO dbo.RepriceSource(RepriceSourceID, Description) values (3, 'PingII')

SET IDENTITY_INSERT dbo.RepriceSource OFF
GO 

if not exists (select * from dbo.syscolumns where id = object_id(N'AIP_Event') and name = 'RepriceSourceID')
ALTER TABLE [dbo].AIP_Event 
ADD RepriceSourceID int DEFAULT null
GO 

ALTER TABLE [dbo].AIP_Event 
ADD CONSTRAINT FK_AIP_Event_RepriceSource FOREIGN KEY
(
RepriceSourceID
) REFERENCES RepriceSource
(
RepriceSourceID
)
GO


-- Drop and rebuild RepriceExport
-- Currently, RepriceExport contains old garbage data as the export has
-- not been running. This is us starting fresh.	

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RepriceExport]')) 
DROP TABLE RepriceExport
GO

CREATE TABLE [dbo].[RepriceExport](
		[RepriceExportID] [int] IDENTITY (1,1) NOT NULL,            
		[RepriceEventID] [int] NOT NULL,
		[RepriceExportStatusID] [int] NOT NULL,
		[ThirdPartyEntityID] [int] NOT NULL,
		[FailureReason] varchar(256),
		[FailureCount] [int] CONSTRAINT [DF_RepriceExport_FailureCount] DEFAULT (0) NOT NULL,
		[DateSent] [smalldatetime] NULL,
		[StatusModifiedDate] [smalldatetime] CONSTRAINT [DF_RepriceExport_StatusModifiedDate] DEFAULT getdate() NOT NULL,
		CONSTRAINT [PK_RepriceExport] PRIMARY KEY
		(
			[RepriceExportID]
		),  
		CONSTRAINT [FK_RepriceExport_AIP_Event] FOREIGN KEY
		(
			[RepriceEventID]
		) REFERENCES [dbo].[AIP_Event]
		(
			[AIP_EventID]
		),
		CONSTRAINT [FK_RepriceExport_RepriceExportStatus] FOREIGN KEY
		(
			[RepriceExportStatusID]
		) REFERENCES [dbo].[RepriceExportStatus]
		(
			[RepriceExportStatusID]
		),
		CONSTRAINT [FK_RepriceExport_ThirdPartyEntityID] FOREIGN KEY
		(
			[ThirdPartyEntityID]
		) REFERENCES [dbo].[ThirdPartyEntity]
		(
			[ThirdPartyEntityID]
		)
	)
	GO

	CREATE TRIGGER RepriceExportStatusModifiedUpdate
	ON RepriceExport
	FOR UPDATE
	AS IF UPDATE(RepriceExportStatusID)
		UPDATE RE
		SET StatusModifiedDate = getdate()
		FROM inserted i
		JOIN RepriceExport RE on i.RepriceExportID = RE.RepriceExportID 
	GO

	
-- Drop Reprice History. Now covered by view RepriceEvent on AIP_Event	
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RepriceHistory]')) 
DROP TABLE RepriceHistory
GO






	