-- nk : rebuild DMSExportBusinessUnitPreference with a proper primary key so hibernate gets its panties out of a twist

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DMSExportBusinessUnitPreference]')) 
DROP TABLE [dbo].DMSExportBusinessUnitPreference
GO
 
CREATE TABLE [dbo].DMSExportBusinessUnitPreference (
	DMSExportBusinessUnitPreferenceID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID INT NOT NULL,
	DMSExportID INT NOT NULL,
	isActive TINYINT,
	ClientSystemID VARCHAR(11) NOT NULL,
	DMSIPAddress VARCHAR(16) NOT NULL,
	DMSDialUpPhone1 VARCHAR(16) NOT NULL,
	DMSDialUpPhone2 VARCHAR(16),
	DMSLogin VARCHAR(20) NOT NULL,
	DMSPassword VARCHAR(20) NOT NULL,
	AccountLogon VARCHAR(20) NULL,
	FinanceLogon VARCHAR(20) NULL,
	StoreNumber INT NOT NULL,
	BranchNumber INT,
	DMSExportFrequencyID TINYINT NOT NULL,
	DMSExportDailyHourOfDay INT,
	DealerName VARCHAR(40) NOT NULL,
	DealerPhoneOffice VARCHAR(16) NOT NULL,
	DealerPhoneCell VARCHAR(16) NOT NULL,
	DealerEmail VARCHAR(25) NOT NULL,
	DMSExportInternetPriceMappingID INT,
	DMSExportWindowStickerPriceMappingID INT
)
GO

-- Add FK contraints to DMSExportBusinessUnitPreference

ALTER TABLE [dbo].[DMSExportBusinessUnitPreference] ADD 
	CONSTRAINT [PK_DMSExportBusinessUnitPreference] PRIMARY KEY 
	(
			[DMSExportBusinessUnitPreferenceID] ASC
	),
	CONSTRAINT [FK_DMSExportBusinessUnitPreference_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT FK_DMSExportBusinessUnitPreference_DMSExport_ThirdPartyEntity FOREIGN KEY
	(
		DMSExportID
	) REFERENCES [dbo].DMSExport_ThirdPartyEntity (
		DMSExportID
	),
	CONSTRAINT FK_DMSExportBusinessUnitPreference_DMSExportFrequency FOREIGN KEY
	(
		DMSExportFrequencyID
	) REFERENCES [dbo].DMSExportFrequency (
		DMSExportFrequencyID
	),
	CONSTRAINT FK_DMSExportBusinessUnitPreference_DMSExportInternetPriceMapping FOREIGN KEY
	(
		DMSExportInternetPriceMappingID
	) REFERENCES [dbo].DMSExportPriceMapping (
		DMSExportPriceMappingID
	),
	CONSTRAINT FK_DMSExportBusinessUnitPreference_DMSExportWindowStickerPriceMapping FOREIGN KEY
	(
		DMSExportWindowStickerPriceMappingID
	) REFERENCES [dbo].DMSExportPriceMapping (
		DMSExportPriceMappingID
	)
GO