if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RepriceExport]')) 
DROP TABLE RepriceExport
GO

CREATE TABLE [dbo].[RepriceExport](
		[RepriceExportID] [int] IDENTITY (1,1) NOT NULL,            
		[RepriceEventID] [int] NOT NULL,
		[RepriceExportStatusID] [int] NOT NULL,
		[ThirdPartyEntityID] [int] NOT NULL,
		[FailureReason] varchar(2000),
		[FailureCount] [int] CONSTRAINT [DF_RepriceExport_FailureCount] DEFAULT (0) NOT NULL,
		[DateCreated] [datetime] NULL,
		[DateSent] [datetime] NULL,
		[StatusModifiedDate] [datetime] CONSTRAINT [DF_RepriceExport_StatusModifiedDate] DEFAULT getdate() NOT NULL,
		CONSTRAINT [PK_RepriceExport] PRIMARY KEY
		(
			[RepriceExportID]
		),  
		CONSTRAINT [FK_RepriceExport_AIP_Event] FOREIGN KEY
		(
			[RepriceEventID]
		) REFERENCES [dbo].[AIP_Event]
		(
			[AIP_EventID]
		),
		CONSTRAINT [FK_RepriceExport_RepriceExportStatus] FOREIGN KEY
		(
			[RepriceExportStatusID]
		) REFERENCES [dbo].[RepriceExportStatus]
		(
			[RepriceExportStatusID]
		),
		CONSTRAINT [FK_RepriceExport_ThirdPartyEntityID] FOREIGN KEY
		(
			[ThirdPartyEntityID]
		) REFERENCES [dbo].[ThirdPartyEntity]
		(
			[ThirdPartyEntityID]
		)
	)
	GO

	CREATE TRIGGER RepriceExportStatusModifiedUpdate
	ON RepriceExport
	FOR UPDATE
	AS IF UPDATE(RepriceExportStatusID)
		UPDATE RE
		SET StatusModifiedDate = getdate()
		FROM inserted i
		JOIN RepriceExport RE on i.RepriceExportID = RE.RepriceExportID 
	GO

	-- add new status to RepriceExportStatus to flag for DM/archiving
	INSERT INTO RepriceExportStatus
	VALUES ('ConfirmedFailure')
	
	