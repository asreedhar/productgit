
ALTER TABLE Carfax.Account ADD Active BIT NULL
GO

UPDATE Carfax.Account SET Active = 1
GO

ALTER TABLE Carfax.Account ALTER COLUMN Active BIT NOT NULL
GO
