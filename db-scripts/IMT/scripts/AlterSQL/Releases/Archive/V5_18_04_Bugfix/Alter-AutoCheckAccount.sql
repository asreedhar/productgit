
ALTER TABLE AutoCheck.Account ADD Active BIT NULL
GO

UPDATE AutoCheck.Account SET Active = 1
GO

ALTER TABLE AutoCheck.Account ALTER COLUMN Active BIT NOT NULL
GO
