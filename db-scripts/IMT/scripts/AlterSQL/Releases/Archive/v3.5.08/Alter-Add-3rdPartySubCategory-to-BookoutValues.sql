
ALTER TABLE dbo.BookoutValues
	DROP CONSTRAINT FK_BookoutValues_BookoutThirdPartyCategories
GO
ALTER TABLE dbo.BookoutValues
	DROP CONSTRAINT FK_BookoutValues_BookoutValueTypes
GO
ALTER TABLE dbo.BookoutValues
	DROP CONSTRAINT DF_BookoutValues_DateCreated	
GO
CREATE TABLE dbo.Tmp_BookoutValues
	(
	BookoutValueID int NOT NULL IDENTITY (1, 1),
	BookoutThirdPartyCategoryID int NOT NULL,
	BookoutValueTypeID tinyint NOT NULL,
	ThirdPartySubCategoryID int NULL,
	[Value] int NULL,
	DateCreated datetime NOT NULL,
	DateModified datetime NULL
	)  ON DATA
GO

ALTER TABLE dbo.Tmp_BookoutValues ADD CONSTRAINT
	DF_BookoutValues_DateCreated DEFAULT (getdate()) FOR DateCreated
GO
SET IDENTITY_INSERT dbo.Tmp_BookoutValues ON
GO

INSERT 
INTO 	dbo.Tmp_BookoutValues (BookoutValueID, BookoutThirdPartyCategoryID, BookoutValueTypeID, [Value], DateCreated, DateModified)
SELECT 	BookoutValueID, BookoutThirdPartyCategoryID, BookoutValueTypeID, [Value], DateCreated, DateModified 
FROM 	dbo.BookoutValues TABLOCKX
GO
SET IDENTITY_INSERT dbo.Tmp_BookoutValues OFF
GO
DROP TABLE dbo.BookoutValues
GO
EXECUTE sp_rename N'dbo.Tmp_BookoutValues', N'BookoutValues', 'OBJECT'
GO
ALTER TABLE dbo.BookoutValues ADD CONSTRAINT
	PK_BookoutValues PRIMARY KEY CLUSTERED 
	(
	BookoutValueID
	)

GO
CREATE NONCLUSTERED INDEX IX_BookoutValues_ValueTypeCategoryID ON dbo.BookoutValues
	(
	BookoutThirdPartyCategoryID,
	BookoutValueTypeID
	)
GO
ALTER TABLE dbo.BookoutValues WITH NOCHECK ADD CONSTRAINT
	FK_BookoutValues_BookoutValueTypes FOREIGN KEY
	(
	BookoutValueTypeID
	) REFERENCES dbo.BookoutValueTypes
	(
	BookoutValueTypeID
	)
GO
ALTER TABLE dbo.BookoutValues WITH NOCHECK ADD CONSTRAINT
	FK_BookoutValues_BookoutThirdPartyCategories FOREIGN KEY
	(
	BookoutThirdPartyCategoryID
	) REFERENCES dbo.BookoutThirdPartyCategories
	(
	BookoutThirdPartyCategoryID
	)
GO
ALTER TABLE dbo.BookoutValues ADD CONSTRAINT
	FK_BookoutValues_ThirdPartySubCategory FOREIGN KEY
	(
	ThirdPartySubCategoryID
	) REFERENCES dbo.ThirdPartySubCategory
	(
	ThirdPartySubCategoryID
	)
GO
