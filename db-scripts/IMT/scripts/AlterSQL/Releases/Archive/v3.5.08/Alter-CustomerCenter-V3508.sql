UPDATE	MessageCenter
SET	Ordering = Ordering + 1, 
	HasPriority = 0

INSERT
INTO	MessageCenter (Ordering, Title, Body, HasPriority)
SELECT	1, 'PlaceHolder','PlaceHolder',0

UPDATE	MessageCenter
SET 
Title = 
'<h3 style="display:block;color:#333;border-bottom:1px dashed #CCC;margin-right:0px;"> <strong>On April 18, 2007, the newest version of The EDGE was released!</strong> </h3>',
Body = 
'<span style="font-size:12">
The enhancements included are:
<ul><li>Inventory Management Plan
    <ul>
	<li><a href="javascript:void(0)" onclick="window.open(''http://images.firstlook.biz/ReleaseNotes-QP-Overview.pdf'')">Quick Planning</a>
		<ul type="i">
		<li><a href="javascript:void(0)" onclick="window.open(''http://images.firstlook.biz/ReleaseNotes-QP-Repricing.pdf'')">Quick Repricing</a> makes it easy to accurately evaluate and reprice your inventory with precision</li>
		<li><a href="javascript:void(0)" onclick="window.open(''http://images.firstlook.biz/ReleaseNotes-QP-SPIFF.pdf'')">Quick SPIFF</a>  makes it easy to create SPIFFs for any and all inventory</li>
		<li><a href="javascript:void(0)" onclick="window.open(''http://images.firstlook.biz/ReleaseNotes-QP-Wholesale.pdf'')">Quick Auction</a> makes it easy to allocate inventory targeted for wholesale to the appropriate live auctions</li>
		<li><a href="javascript:void(0)" onclick="window.open(''http://images.firstlook.biz/ReleaseNotes-QP-Wholesale.pdf'')">Quick Wholesaler</a> makes it easy to indicate the vehicles available for wholesalers</li>
		</ul>

	</li>
        <li>Search By Stock Number:  In the Plan, find any vehicle in your inventory from the Inventory Management Plan by entering the stock number</li>
        <li>Enhanced Action Plans:  You can now send all action plans from The EDGE (in either PDF or HTML) 
	</li>
    </ul>
    </li>
    <li><i><b>Auction Pipeline</b></i> Line Auctions Now Available in the First Look Search Engine</li> 
</ul>
</span>',
HasPriority = 1
WHERE	Ordering = 1
GO
