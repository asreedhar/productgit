-- first make bookoutID nullable
ALTER TABLE dbo.AuditBookouts ALTER COLUMN BookoutID int NULL
GO

-- second set all bookoutid = -1 to be null

UPDATE AuditBookOuts
SET Bookoutid = NULL
WHERE bookoutid = -1
GO

-- third remove the -1 bookoutID from the bookouts table
DELETE
FROM	Bookouts
WHERE	bookoutid = -1
GO
