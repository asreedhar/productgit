
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_BookoutValues_ThirdPartySubCategory]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1) ALTER TABLE [dbo].[BookoutValues] 
	DROP CONSTRAINT FK_BookoutValues_ThirdPartySubCategory
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[KBBArchiveDatasetOverride]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) 
	drop table [dbo].[KBBArchiveDatasetOverride] 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ThirdPartySubCategory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1) 
	drop table [dbo].[ThirdPartySubCategory] 
GO

CREATE TABLE [dbo].[KBBArchiveDatasetOverride] (
	[KBBArchiveDatasetOverrideID] [int] IDENTITY (1, 1) NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[MemberID] [int] NOT NULL ,
	[StartDate] [datetime] NOT NULL ,
	[EndDate] [datetime] NOT NULL
) ON [DATA]
GO

CREATE TABLE [dbo].[ThirdPartySubCategory] (
	[ThirdPartySubCategoryID] [int] IDENTITY (1, 1) NOT NULL ,
	[ThirdPartyCategoryID] [int] NOT NULL ,
	[Description] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [DATA]
GO

ALTER TABLE [dbo].[ThirdPartySubCategory] WITH NOCHECK ADD 
	CONSTRAINT [PK_ThirdPartySubCategory] PRIMARY KEY  CLUSTERED 
	(
		[ThirdPartySubCategoryID]
	)  ON [DATA]
GO

ALTER TABLE [dbo].[KBBArchiveDatasetOverride] ADD 
	CONSTRAINT [FK_KBBArchiveDatasetOverride_BusinessUnit] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [dbo].[BusinessUnit] (
		[BusinessUnitID]
	),
	CONSTRAINT [FK_KBBArchiveDatasetOverride_Member] FOREIGN KEY 
	(
		[MemberID]
	) REFERENCES [dbo].[Member] (
		[MemberID]
	)
GO

ALTER TABLE [dbo].[ThirdPartySubCategory] ADD 
	CONSTRAINT [FK_ThirdPartySubCategory_ThirdPartyCategories] FOREIGN KEY 
	(
		[ThirdPartyCategoryID]
	) REFERENCES [dbo].[ThirdPartyCategories] (
		[ThirdPartyCategoryID]
	)

GO
SET IDENTITY_INSERT ThirdPartyCategories ON 
INSERT
INTO	ThirdPartyCategories (ThirdPartyCategoryID, ThirdPartyID, Category)
SELECT	11, 3, 'Trade-In'
INSERT
INTO	ThirdPartyCategories (ThirdPartyCategoryID, ThirdPartyID, Category)
SELECT	12, 3, 'Private Party'

SET IDENTITY_INSERT ThirdPartyCategories OFF 
GO

ALTER TABLE DealerPreference ADD [KBBInventoryBookoutDatasetPreference] TINYINT NOT NULL DEFAULT (1)
GO

ALTER TABLE DealerPreference ADD [KBBAppraisalBookoutDatasetPreference] TINYINT NOT NULL DEFAULT (0)
GO

ALTER TABLE DealerPreference ADD [KBBInventoryDefaultCondition] TINYINT NOT NULL DEFAULT (1)
GO

ALTER TABLE AppraisalFormOptions ADD [includeMileageAdjustment] TINYINT NOT NULL DEFAULT (1)
GO

ALTER TABLE ThirdPartyVehicleOptionValueTypes ALTER COLUMN [OptionValueTypeName] VARCHAR(30) NOT NULL
GO

SET IDENTITY_INSERT ThirdPartyVehicleOptionValueTypes ON 
INSERT
INTO	ThirdPartyVehicleOptionValueTypes (ThirdPartyOptionValueTypeID, OptionValueTypeName)
SELECT	5, 'Trade-In: Excellent'
INSERT
INTO	ThirdPartyVehicleOptionValueTypes (ThirdPartyOptionValueTypeID, OptionValueTypeName)
SELECT	6, 'Trade-In: Good'
INSERT
INTO	ThirdPartyVehicleOptionValueTypes (ThirdPartyOptionValueTypeID, OptionValueTypeName)
SELECT	7, 'Trade-In: Fair'

INSERT
INTO	ThirdPartyVehicleOptionValueTypes (ThirdPartyOptionValueTypeID, OptionValueTypeName)
SELECT	8, 'Private Party: Excellent'
INSERT
INTO	ThirdPartyVehicleOptionValueTypes (ThirdPartyOptionValueTypeID, OptionValueTypeName)
SELECT	9, 'Private Party: Good'
INSERT
INTO	ThirdPartyVehicleOptionValueTypes (ThirdPartyOptionValueTypeID, OptionValueTypeName)
SELECT	10, 'Private Party: Fair'

SET IDENTITY_INSERT ThirdPartyVehicleOptionValueTypes OFF 
GO

SET IDENTITY_INSERT ThirdPartySubCategory ON 
INSERT
INTO	ThirdPartySubCategory (ThirdPartySubCategoryID, ThirdPartyCategoryID, Description)
SELECT	ThirdPartyCategoryID, ThirdPartyCategoryID, Category
FROM	ThirdPartyCategories
WHERE	ThirdPartyCategoryID <= 10

INSERT
INTO	ThirdPartySubCategory (ThirdPartySubCategoryID, ThirdPartyCategoryID, Description)
SELECT	11, 11, 'Excellent'
INSERT
INTO	ThirdPartySubCategory (ThirdPartySubCategoryID, ThirdPartyCategoryID, Description)
SELECT	12, 11, 'Good'
INSERT
INTO	ThirdPartySubCategory (ThirdPartySubCategoryID, ThirdPartyCategoryID, Description)
SELECT	13, 11, 'Fair'

INSERT
INTO	ThirdPartySubCategory (ThirdPartySubCategoryID, ThirdPartyCategoryID, Description)
SELECT	14, 12, 'Excellent'
INSERT
INTO	ThirdPartySubCategory (ThirdPartySubCategoryID, ThirdPartyCategoryID, Description)
SELECT	15, 12, 'Good'
INSERT
INTO	ThirdPartySubCategory (ThirdPartySubCategoryID, ThirdPartyCategoryID, Description)
SELECT	16, 12, 'Fair'

SET IDENTITY_INSERT ThirdPartySubCategory OFF 

GO

SET IDENTITY_INSERT BookoutValueTypes ON 
INSERT
INTO	BookoutValueTypes (BookoutValueTypeID, Description)
SELECT	4, 'Options Value'
SET IDENTITY_INSERT BookoutValueTypes OFF 
GO
