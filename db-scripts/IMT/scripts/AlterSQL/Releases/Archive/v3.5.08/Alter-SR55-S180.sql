-- Adding is quick advertiser flag for print advertisers

if not exists (select * from syscolumns
	where id=object_id('PrintAdvertiser_ThirdPartyEntity') and name='QuickAdvertiser')
ALTER TABLE dbo.PrintAdvertiser_ThirdPartyEntity
	ADD QuickAdvertiser TINYINT NULL
GO

UPDATE dbo.PrintAdvertiser_ThirdPartyEntity SET QuickAdvertiser = 0
