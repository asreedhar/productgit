
--
-- New Inventory Bucket
--

INSERT INTO dbo.InventoryBuckets ([Description], [InventoryType]) VALUES ('Aging Inventory - Short Leash', 2)
GO

-- Lights == 1 (red)
-- 1 & 2^(1-1) = 1 & 1 = 001 & 001 = 1
-- 1 & 2^(2-1) = 1 & 2 = 001 & 010 = 0
-- 1 & 2^(3-1) = 1 & 4 = 001 & 100 = 0

INSERT INTO [dbo].[InventoryBucketRanges](
	[InventoryBucketID],
	[RangeID],
	[Description],
	[Low],
	[High],
	[Lights],
	[BusinessUnitID],
	[LongDescription],
	[SortOrder]
)
VALUES (5, 1, 'Red Light Vehicles', 14, 21, 1, 100150, 'Days in inventory thresholds for red light vehicles for warning- and error-operational insights', 1)
GO

-- Lights == 2 (yellow)
-- 2 & 2^(1-1) = 2 & 1 = 010 & 001 = 0
-- 2 & 2^(2-1) = 2 & 2 = 010 & 010 = 1
-- 2 & 2^(3-1) = 2 & 4 = 010 & 100 = 0

INSERT INTO [dbo].[InventoryBucketRanges](
	[InventoryBucketID],
	[RangeID],
	[Description],
	[Low],
	[High],
	[Lights],
	[BusinessUnitID],
	[LongDescription],
	[SortOrder]
)
VALUES (5, 2, 'Yellow Light Vehicles', 21, 30, 2, 100150, 'Days in inventory thresholds for yellow light vehicles for warning- and error-operational insights', 2)
GO

-- Lights == 4 (green)
-- 4 & 2^(1-1) = 4 & 1 = 100 & 001 = 0
-- 4 & 2^(2-1) = 4 & 2 = 100 & 010 = 0
-- 4 & 2^(3-1) = 4 & 4 = 100 & 100 = 1

INSERT INTO [dbo].[InventoryBucketRanges](
	[InventoryBucketID],
	[RangeID],
	[Description],
	[Low],
	[High],
	[Lights],
	[BusinessUnitID],
	[LongDescription],
	[SortOrder]
)
VALUES (5, 3, 'Green Light Vehicles', 45, 60, 4, 100150, 'Days in inventory thresholds for green light vehicles for warning- and error-operational insights', 3)
GO

--
-- DROP TABLE COMMANDS
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MemberInsightTypePreference]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.MemberInsightTypePreference
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DealerInsightTargetPreference]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.DealerInsightTargetPreference
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InsightTarget]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.InsightTarget
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InsightTargetValueType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.InsightTargetValueType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InsightTargetType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.InsightTargetType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InsightReflection]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.InsightReflection
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InsightAdjectival]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.InsightAdjectival
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InsightType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.InsightType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InsightCategory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.InsightCategory
GO

--
-- Reference Table: Insight Category
--

CREATE TABLE dbo.InsightCategory (
	[InsightCategoryID] INT NOT NULL,
	[Name]              VARCHAR(255) NOT NULL,
	CONSTRAINT PK_InsightCategory PRIMARY KEY (
		InsightCategoryID
	)
)
GO

INSERT INTO dbo.InsightCategory ([InsightCategoryID], [Name]) VALUES (1, 'Appraisal Insights')
INSERT INTO dbo.InsightCategory ([InsightCategoryID], [Name]) VALUES (2, 'Operation Insights')
GO

--
-- Reference Table: Insight Type
--

CREATE TABLE dbo.InsightType (
	[InsightTypeID]     INT NOT NULL,
	[InsightCategoryID] INT NOT NULL,
	[Name]              VARCHAR(255) NOT NULL,
	CONSTRAINT PK_InsightType PRIMARY KEY (
		InsightTypeID
	),
	CONSTRAINT FK_InsightType_InsightCategory FOREIGN KEY (
		InsightCategoryID
	)
	REFERENCES dbo.InsightCategory (
		InsightCategoryID
	)
)
GO

INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (1, 1, 'Insufficient Sales History')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (2, 1, 'Average Gross Profit')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (3, 1, 'Frequent No Sales')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (4, 1, 'Average Margin')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (5, 1, 'Contribution Rank')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (6, 1, 'Average Days to Sale')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (7, 1, 'Traffic Light')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (8, 1, 'Stocking Levels')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (9, 1, 'High Mileage')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (10, 1, 'Older Vehicle')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (11, 1, 'Annualized ROI')
GO

INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (12, 2, 'Appraisal Closing Rate')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (13, 2, 'Make a Deal')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (14, 2, 'Trade-In Inventory Analyzed')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (15, 2, 'Average Immediate Wholesale Profit')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (16, 2, 'Current Inventory - Short Leash')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (17, 2, 'Current Inventory - Unplanned Vehicles')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (18, 2, 'Current Inventory - Days Supply')
GO

--
-- Reference Table: Insight Adjectival
--

CREATE TABLE dbo.InsightAdjectival (
	[InsightAdjectivalID] INT NOT NULL,
	[Name]                VARCHAR(255) NOT NULL,
	CONSTRAINT PK_InsightAdjectival PRIMARY KEY (
		InsightAdjectivalID
	)
)
GO

INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (1, 'None')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (2, 'Insufficent')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (3, 'Faster')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (4, 'Slower')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (5, 'More')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (6, 'Less')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (7, 'Higher')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (8, 'Lower')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (9, 'Under')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (10, 'Over')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (11, 'High')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (12, 'Older')
GO

INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (13, 'Profit')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (14, 'Loss')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (15, 'Below')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (16, 'Above')
INSERT INTO dbo.InsightAdjectival ([InsightAdjectivalID], [Name]) VALUES (17, 'On')
GO

--
-- Reference Table: Insight Reflection (Suggestions and Warnings)
--

CREATE TABLE dbo.InsightReflection (
	[InsightReflectionID] INT NOT NULL,
	[Name]                VARCHAR(255) NOT NULL,
	CONSTRAINT PK_InsightReflection PRIMARY KEY (
		InsightReflectionID
	)
)
GO

INSERT INTO dbo.InsightReflection ([InsightReflectionID], [Name]) VALUES (1, 'Low Trade Analyzer Usage')
INSERT INTO dbo.InsightReflection ([InsightReflectionID], [Name]) VALUES (2, 'Inadequate Trade Analyzer Usage')
INSERT INTO dbo.InsightReflection ([InsightReflectionID], [Name]) VALUES (3, 'Wholesale Profit Hurting Closing Rate?')
INSERT INTO dbo.InsightReflection ([InsightReflectionID], [Name]) VALUES (4, 'Losing Retail Deals?')
INSERT INTO dbo.InsightReflection ([InsightReflectionID], [Name]) VALUES (5, 'Over-paying for trade-ins but it is not helping your closing rate')
INSERT INTO dbo.InsightReflection ([InsightReflectionID], [Name]) VALUES (6, 'Consider for wholesale?')
GO

CREATE TABLE dbo.InsightTargetType (
	[InsightTargetTypeID] INT         NOT NULL,
	[Name]                VARCHAR(50) NOT NULL,
	CONSTRAINT PK_InsightTargetType PRIMARY KEY (
		InsightTargetTypeID
	)
)
GO

INSERT INTO dbo.InsightTargetType ([InsightTargetTypeID], [Name]) VALUES (1, 'N/A')
INSERT INTO dbo.InsightTargetType ([InsightTargetTypeID], [Name]) VALUES (2, 'Range')
INSERT INTO dbo.InsightTargetType ([InsightTargetTypeID], [Name]) VALUES (3, 'Value')
GO

CREATE TABLE dbo.InsightTargetValueType (
	[InsightTargetValueTypeID] INT         NOT NULL,
	[Name]                     VARCHAR(50) NOT NULL,
	CONSTRAINT PK_InsightTargetValueType PRIMARY KEY (
		InsightTargetValueTypeID
	)
)
GO

INSERT INTO dbo.InsightTargetValueType ([InsightTargetValueTypeID], [Name]) VALUES (1, 'Currency')
INSERT INTO dbo.InsightTargetValueType ([InsightTargetValueTypeID], [Name]) VALUES (2, 'Percentage')
INSERT INTO dbo.InsightTargetValueType ([InsightTargetValueTypeID], [Name]) VALUES (3, 'Number')
GO

CREATE TABLE dbo.InsightTarget (
	[InsightTargetID]          INT IDENTITY(1,1) NOT NULL,
	[InsightTargetTypeID]      INT NOT NULL,
	[InsightTargetValueTypeID] INT NOT NULL,
	[InsightTypeID]            INT NOT NULL,
	[InsightContext]           INT NULL,
	[Name]                     VARCHAR(255) NOT NULL,
	[Value]                    INT NULL,
	[Min]                      INT NULL,
	[Max]                      INT NULL,
	[Threshold]                INT NULL,
	CONSTRAINT PK_InsightTarget PRIMARY KEY (
		InsightTargetID
	),
	CONSTRAINT FK_InsightTarget_InsightTargetType FOREIGN KEY (
		InsightTargetTypeID
	)
	REFERENCES dbo.InsightTargetType (
		InsightTargetTypeID
	),
	CONSTRAINT FK_InsightTarget_InsightTargetValueType FOREIGN KEY (
		InsightTargetValueTypeID
	)
	REFERENCES dbo.InsightTargetValueType (
		InsightTargetValueTypeID
	),
	CONSTRAINT FK_InsightTarget_InsightType FOREIGN KEY (
		InsightTypeID
	)
	REFERENCES dbo.InsightType (
		InsightTypeID
	)
)
GO

INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (3, 3, 12, NULL, 'Appraisal Closing Rate', 60, NULL, NULL, 20)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (1, 3, 13, NULL, 'Make a Deal', NULL, NULL, NULL, 10)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (3, 2, 14, NULL, 'Trade-In Inventory Analyzed', 100, NULL, NULL, 20)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (2, 1, 15, NULL, 'Average Immediate Wholesale Profit', NULL, -250, 250, 500)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (1, 3, 16, 1, 'Current Inventory - Short Leash - Red', NULL, NULL, NULL, 2)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (1, 3, 16, 2, 'Current Inventory - Short Leash - Yellow', NULL, NULL, NULL, 2)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (1, 3, 16, 3, 'Current Inventory - Short Leash - Green', NULL, NULL, NULL, 2)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (1, 3, 17, NULL, 'Current Inventory - Unplanned Vehicles', NULL, NULL, NULL, 5)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (3, 3, 18, NULL, 'Current Inventory - Days Supply', 45, NULL, NULL, 10)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (3, 3, 18, 2, 'Current Inventory - Days Supply - Truck', 45, NULL, NULL, 10)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (3, 3, 18, 3, 'Current Inventory - Days Supply - Sedan', 45, NULL, NULL, 10)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (3, 3, 18, 4, 'Current Inventory - Days Supply - Coupe', 45, NULL, NULL, 10)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (3, 3, 18, 5, 'Current Inventory - Days Supply - Van', 45, NULL, NULL, 10)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (3, 3, 18, 6, 'Current Inventory - Days Supply - SUV', 45, NULL, NULL, 10)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (3, 3, 18, 7, 'Current Inventory - Days Supply - Convertible', 45, NULL, NULL, 10)
INSERT INTO dbo.InsightTarget ([InsightTargetTypeID], [InsightTargetValueTypeID], [InsightTypeID], [InsightContext], [Name], [Value], [Min], [Max], [Threshold]) VALUES (3, 3, 18, 8, 'Current Inventory - Days Supply - Wagon', 45, NULL, NULL, 10)
GO

--
-- Instance Table: Dealer (BusinessUnit type 4) override of the Insight Target defaults.
--

CREATE TABLE dbo.DealerInsightTargetPreference (
	[DealerInsightTargetPreferenceID] INT IDENTITY(1,1) NOT NULL,
	[BusinessUnitID]                  INT NOT NULL,
	[InsightTargetID]                 INT NOT NULL,
	[Value]                           INT NULL,
	[Min]                             INT NULL,
	[Max]                             INT NULL,
	[Threshold]                       INT NULL,
	CONSTRAINT PK_DealerInsightTargetPreference PRIMARY KEY (
		DealerInsightTargetPreferenceID
	),
	CONSTRAINT FK_DealerInsightTargetPreference_BusinessUnit FOREIGN KEY (
		BusinessUnitID
	)
	REFERENCES dbo.BusinessUnit (
		BusinessUnitID
	),
	CONSTRAINT FK_DealerInsightTargetPreference_InsightTarget FOREIGN KEY (
		InsightTargetID
	)
	REFERENCES dbo.InsightTarget (
		InsightTargetID
	)
)
GO

--
-- View: Targets and Thresholds for the BusinessUnit falling back to the defaults.
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[DealerInsightTarget]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[DealerInsightTarget]
GO

CREATE VIEW dbo.DealerInsightTarget (
	[BusinessUnitID],
	[InsightTargetID],
	[InsightTargetTypeID],
	[InsightTargetValueTypeID],
	[InsightTypeID],
	[InsightContext],
	[Name],
	[Value],
	[Min],
	[Max],
	[Threshold]
)
AS
SELECT
	B.[BusinessUnitID],
	T.[InsightTargetID],
	T.[InsightTargetTypeID],
	T.[InsightTargetValueTypeID],
	T.[InsightTypeID],
	T.[InsightContext],
	T.[Name],
	COALESCE(P.[Value], T.[Value]) Value,
	COALESCE(P.[Min], T.[Min]) [Min],
	COALESCE(P.[Max], T.[Max]) [Max],
	COALESCE(P.[Threshold],T.[Threshold]) [Threshold]
FROM
                dbo.InsightTarget T
LEFT JOIN       dbo.DealerInsightTargetPreference P ON P.InsightTargetID = T.InsightTargetID
CROSS JOIN      dbo.BusinessUnit B
GO

--
-- Instance Table: Insight Permission / Visibility
--

CREATE TABLE dbo.MemberInsightTypePreference (
	[MemberInsightTypePreference] INT IDENTITY(1,1) NOT NULL,
	[BusinessUnitID]              INT NOT NULL,
	[MemberID]                    INT NOT NULL,
	[InsightTypeID]               INT NOT NULL,
	[Active]                      BIT NOT NULL,
	CONSTRAINT PK_MemberInsightType PRIMARY KEY (
		MemberInsightTypePreference
	),
	CONSTRAINT FK_MemberInsightType_InsightType FOREIGN KEY (
		InsightTypeID
	)
	REFERENCES dbo.InsightType (
		InsightTypeID
	),
	CONSTRAINT FK_MemberInsightType_BusinessUnit FOREIGN KEY (
		BusinessUnitID
	)
	REFERENCES dbo.BusinessUnit (
		BusinessUnitID
	),
	CONSTRAINT FK_MemberInsightType_Member FOREIGN KEY (
		MemberID
	)
	REFERENCES dbo.Member (
		MemberID
	),
	CONSTRAINT UK_MemberInsightType UNIQUE (
		BusinessUnitID,
		MemberID,
		InsightTypeID
	)
)
GO

--
-- Instance View: Members Insight Visibility
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MemberInsightType]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[MemberInsightType]
GO

CREATE VIEW dbo.MemberInsightType (
	[BusinessUnitID],
	[MemberID],
	[InsightTypeID]
)
AS
(
	-- standard users: 118K rows
	SELECT	MA.BusinessUnitID,
		M.MemberID,
		T.InsightTypeID
	FROM	Member M
	JOIN	MemberAccess MA ON MA.MemberID = M.MemberID
	CROSS JOIN dbo.InsightType T
	LEFT  JOIN dbo.MemberInsightTypePreference P ON (
			P.InsightTypeID   = T.InsightTypeID
		AND	MA.BusinessUnitID = P.BusinessUnitID
		AND	MA.MemberID       = P.MemberID
		)
	WHERE	M.MemberType = 2
	AND	COALESCE(P.Active,1) = 1
UNION ALL
	-- admin users: 1.3M rows
	SELECT	B.BusinessUnitID,
		M.MemberID,
		T.InsightTypeID
	FROM	Member M
	CROSS JOIN dbo.BusinessUnit B
	CROSS JOIN dbo.InsightType T
	LEFT  JOIN dbo.MemberInsightTypePreference P ON (
			T.InsightTypeID  = P.InsightTypeID
		AND	B.BusinessUnitID = P.BusinessUnitID
		AND	M.MemberID       = P.MemberID
		)
	WHERE	M.MemberType = 1
	AND	B.BusinessUnitTypeID = 4
	AND	B.Active = 1
	AND	COALESCE(P.Active,1) = 1
)
GO
