-- Add EdmundsTMV (Edmunds True Market Value)
if not exists (select * from syscolumns
	where id=object_id('dbo.Inventory') and name='EdmundsTMV')

ALTER TABLE dbo.Inventory ADD EdmundsTMV DECIMAL(8,2) NULL
GO
