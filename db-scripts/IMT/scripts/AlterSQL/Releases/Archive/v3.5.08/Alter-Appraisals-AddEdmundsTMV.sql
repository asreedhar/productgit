-- Add EdmundsTMV (Edmunds True Market Value)
if not exists (select * from syscolumns
	where id=object_id('dbo.Appraisals') and name='EdmundsTMV')
ALTER TABLE dbo.Appraisals
ADD EdmundsTMV decimal(8,2)
GO
