-- Add DisplayTMV column to DealerGroupPreferences and DealerPreferences
if not exists (select * from syscolumns
	where id=object_id('dbo.DealerGroupPreference') and name='DisplayTMV')
ALTER TABLE dbo.DealerGroupPreference
ADD DisplayTMV tinyint Not Null Default(0)
GO

if not exists (select * from syscolumns
	where id=object_id('dbo.DealerPreference') and name='DisplayTMV')
ALTER TABLE dbo.DealerPreference
ADD DisplayTMV tinyint Not Null Default(0)
GO
