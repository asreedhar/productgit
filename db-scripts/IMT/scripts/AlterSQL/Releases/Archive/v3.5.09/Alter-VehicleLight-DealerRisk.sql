ALTER TABLE DealerRisk DROP CONSTRAINT DF_DealerRisk_HighMileageRedLight
ALTER TABLE DealerRisk DROP CONSTRAINT DF_DealerRisk_HighMileageOverall
ALTER TABLE DealerRisk DROP CONSTRAINT DF_DealerRisk_HighMileageYear

EXEC sp_rename 'DealerRisk.HighMileageRedLight', 'ExcessiveMileageThreshold','COLUMN'
EXEC sp_rename 'DealerRisk.HighMileageOverall', 'HighMileageThreshold','COLUMN'
EXEC sp_rename 'DealerRisk.HighMileageYear', 'HighMileagePerYearThreshold','COLUMN'

ALTER TABLE DealerRisk ADD CONSTRAINT DF_DealerRisk__ExcessiveMileageThreshold DEFAULT (100000) FOR ExcessiveMileageThreshold
ALTER TABLE DealerRisk ADD CONSTRAINT DF_DealerRisk__HighMileageThreshold DEFAULT (70000) FOR HighMileageThreshold
ALTER TABLE DealerRisk ADD CONSTRAINT DF_DealerRisk__HighMileagePerYearThreshold DEFAULT (70000) FOR HighMileagePerYearThreshold


ALTER TABLE DealerRisk ADD HighAgeThreshold TINYINT NOT NULL CONSTRAINT DF_Dealer_Risk__HighAgeThreshold DEFAULT (6)
ALTER TABLE DealerRisk ADD ExcessiveAgeThreshold TINYINT NOT NULL CONSTRAINT DF_Dealer_Risk__ExcessiveAgeThreshold DEFAULT (7)
ALTER TABLE DealerRisk ADD ExcessiveMileagePerYearThreshold INT NOT NULL CONSTRAINT DF_Dealer_Risk__ExcessiveMileagePerYearThreshold DEFAULT (100000)
GO

