ALTER TABLE dbo.VehicleAttributeCatalog_11 ADD VINPattern CHAR(17) NULL
GO
ALTER TABLE dbo.VehicleAttributeCatalog_1M ADD VINPattern CHAR(17) NULL
GO

UPDATE	dbo.VehicleAttributeCatalog_11
SET	VINPattern = LEFT(SquishVIN,8) + '_' + SUBSTRING(SquishVIN,9,2) + '______'

GO

UPDATE	dbo.VehicleAttributeCatalog_1M
SET	VINPattern = LEFT(SquishVIN,8) + '_' + SUBSTRING(SquishVIN,9,2) + '______'
GO

/*
NEEDS TO BE NULLABLE UNTIL PopulateVehicleAttributeCatalog HAS BEEN UPDATED

ALTER TABLE dbo.VehicleAttributeCatalog_11 ALTER COLUMN VINPattern CHAR(17) NOT NULL
GO
ALTER TABLE dbo.VehicleAttributeCatalog_1M ALTER COLUMN VINPattern CHAR(17) NOT NULL
GO
*/
