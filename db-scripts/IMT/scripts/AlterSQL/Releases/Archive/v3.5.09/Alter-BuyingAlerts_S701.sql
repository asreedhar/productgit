-- Creates the tables used for sending the buying alerts.  
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BuyingAlertsStatus](
	[BuyingAlertsStatusId] [tinyint] NOT NULL,
	[Description] [varchar](20) NOT NULL,
 CONSTRAINT [PK_BuyingAlertsStatus] PRIMARY KEY CLUSTERED 
(
	[BuyingAlertsStatusId] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [DATA]
) ON [DATA]

GO
SET ANSI_PADDING OFF

INSERT INTO BuyingAlertsStatus (BuyingAlertsStatusId, Description) VALUES (1, 'unsent')
INSERT INTO BuyingAlertsStatus (BuyingAlertsStatusId, Description) VALUES (2, 'sent')
INSERT INTO BuyingAlertsStatus (BuyingAlertsStatusId, Description) VALUES (3, 'failed')
GO

CREATE TABLE [dbo].[BuyingAlertsRunList](
	[BuyingAlertsRunListId] [int] IDENTITY(1,1) NOT NULL,
	[SubscriptionId] [int] NOT NULL,
	[BuyingAlertsStatusId] [tinyint] NOT NULL,
	[SendDate] [smalldatetime] NULL,
 CONSTRAINT [PK_BuyingAlertsRunList] PRIMARY KEY NONCLUSTERED 
(
	[BuyingAlertsRunListId] ASC
) WITH (IGNORE_DUP_KEY = OFF) ON [DATA]
) ON [DATA]

ALTER TABLE [dbo].[BuyingAlertsRunList]  WITH NOCHECK ADD  CONSTRAINT [FK_BuyingAlertsRunList_Subscriptions] FOREIGN KEY([SubscriptionId])
REFERENCES [dbo].[Subscriptions] ([SubscriptionId])
GO
ALTER TABLE [dbo].[BuyingAlertsRunList]  WITH NOCHECK ADD  CONSTRAINT [FK_BuyingAlertsRunList_BuyingAlertsStatus] FOREIGN KEY([BuyingAlertsStatusId])
REFERENCES [dbo].[BuyingAlertsStatus] ([BuyingAlertsStatusId])
GO
ALTER TABLE [dbo].[BuyingAlertsRunList] WITH NOCHECK ADD CONSTRAINT [UK_BuyingAlertsRunList] UNIQUE (SubscriptionId)
GO
