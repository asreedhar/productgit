
EXEC sp_rename @objname = 'lu_DealStatus',  @newname  = 'DealStatus', @objtype = 'OBJECT'
GO 

ALTER TABLE dbo.tbl_VehicleSale ADD CONSTRAINT FK_tbl_VehicleSale_DealStatus FOREIGN KEY (DealStatusCD) REFERENCES dbo.DealStatus(DealStatusCD)
GO

