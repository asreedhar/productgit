------------------------------------------------------------------------------------------------
--	INVENTORY "INSIGHT"  
--
--	ESSENTIALLY, THESE COLUMNS SERVE AS A REPLACEMENT FOR THE Level4Analysis COLUMN AND
--	STORE THE CONSTITUENT "INSIGHTS" AS DERIVED BY THE LIGHTS PROCESSOR.
------------------------------------------------------------------------------------------------

CREATE TABLE dbo.Inventory_Insight (
		InventoryID			INT NOT NULL CONSTRAINT PK_Inventory_Insight PRIMARY KEY CLUSTERED,
		MileageInsightID		TINYINT NULL, 
		AgeInsightID			TINYINT NULL, 
		MileagePerYearInsightID		TINYINT NULL, 
		WeakPerformingVehicleInsightID	TINYINT NULL, 
		SalesHistoryInsightID		TINYINT NULL
		)
GO
ALTER TABLE dbo.Inventory_Insight ADD CONSTRAINT FK_Inventory_Insight__InventoryID FOREIGN KEY (InventoryID) REFERENCES dbo.Inventory(InventoryID)
GO


------------------------------------------------------------------------------------------------
--	SAVE OFF THE CURRENT LEVEL4'S AS A FAILSAFE AND FOR TEST/COMPARISON.
--	DELETE AT NEXT RELEASE.
------------------------------------------------------------------------------------------------

CREATE TABLE dbo.Inventory_Level4Analysis (
		InventoryID	INT NOT NULL CONSTRAINT PK_Inventory_Level4Analysis PRIMARY KEY CLUSTERED,
		Level4Analysis	VARCHAR(250) NOT NULL	
		)
GO
ALTER TABLE dbo.Inventory_Insight ADD CONSTRAINT FK_Inventory_Level4Analysis__InventoryID FOREIGN KEY (InventoryID) REFERENCES dbo.Inventory(InventoryID)
GO

INSERT
INTO	Inventory_Level4Analysis (InventoryID, Level4Analysis)
SELECT	InventoryID, Level4Analysis
FROM	Inventory
WHERE	NULLIF(Level4Analysis,'') IS NOT NULL

GO

ALTER TABLE dbo.Inventory DROP COLUMN Level4Analysis
GO

