-- DM Mar 20 2007 S473
-- Alter to add email and phone fields to person table.

if not exists (select * from syscolumns where id=object_id('dbo.DealerPreference') and name='GroupAppraisalSearchWeeks')
ALTER TABLE dbo.DealerPreference
	ADD GroupAppraisalSearchWeeks INT not null DEFAULT(6)  
GO

if not exists (select * from syscolumns where id=object_id('dbo.DealerPreference') and name='ShowAppraisalValueGroup')
ALTER TABLE dbo.DealerPreference
	ADD ShowAppraisalValueGroup TINYINT not null DEFAULT(1)
GO

if not exists (select * from syscolumns where id=object_id('dbo.DealerPreference') and name='ShowAppraisalFormOfferGroup')
ALTER TABLE dbo.DealerPreference
	ADD ShowAppraisalFormOfferGroup TINYINT not null DEFAULT(1)
GO


if not exists (select * from syscolumns where id=object_id('dbo.DealerGroupPreference') and name='showAppraisalFormOffer')
ALTER TABLE dbo.DealerGroupPreference
	ADD ShowAppraisalFormOffer TINYINT null
GO

if not exists (select * from syscolumns where id=object_id('dbo.DealerGroupPreference') and name='ShowAppraisalValue')
ALTER TABLE dbo.DealerGroupPreference
	ADD ShowAppraisalValue TINYINT null
GO
