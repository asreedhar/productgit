-- Add preferences for Redistribution sorting
if not exists (select * from syscolumns where id=object_id('dbo.DealerPreference') and name='RedistributionNumTopDealers')
ALTER TABLE dbo.DealerPreference
	ADD RedistributionNumTopDealers INT not null DEFAULT(10)  
GO

if not exists (select * from syscolumns where id=object_id('dbo.DealerPreference') and name='RedistributionDealerDistance')
ALTER TABLE dbo.DealerPreference
	ADD RedistributionDealerDistance INT not null DEFAULT(500)  
GO

-- 1. Add the columns for sorting, 
-- 2. populate defaults to sort first on understock
-- 3. Update records that have ROI turned on for the BusinessUnit
if not exists (select * from syscolumns where id=object_id('dbo.DealerPreference') and name='RedistributionROI')
ALTER TABLE dbo.DealerPreference
	ADD RedistributionROI TINYINT not null DEFAULT(0)
GO

if not exists (select * from syscolumns where id=object_id('dbo.DealerPreference') and name='RedistributionUnderstock')
ALTER TABLE dbo.DealerPreference
	ADD RedistributionUnderstock TINYINT not null DEFAULT(1) 
GO

--IF annual ROI is turned on for the BusinessUnit
UPDATE DealerPreference
SET RedistributionROI = 1, RedistributionUnderstock = 2
WHERE BusinessUnitID in
(
		SELECT DU.BusinessUnitID
		FROM DealerUpgrade DU
		JOIN DealerPreference DP ON DU.BusinessUnitID = DP.BusinessUnitID
		WHERE DU.DealerUpgradeCD = 9 AND DU.Active = 1
)
