ALTER TABLE dbo.lu_DealerUpgrade
ALTER COLUMN DealerUpgradeDESC varchar(50) NOT NULL
GO

ALTER TABLE dbo.lu_DealerUpgrade
ADD CONSTRAINT [UK_DealerUpgrade_DealerUpgradeDESC] UNIQUE (DealerUpgradeDESC)
GO

INSERT INTO dbo.lu_DealerUpgrade
SELECT 18, 'JDPower Used Car Market Data'
GO
