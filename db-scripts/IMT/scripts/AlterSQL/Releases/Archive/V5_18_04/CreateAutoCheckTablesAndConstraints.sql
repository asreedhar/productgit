

/* JRC 06/05/2009	This script creates the AutoCheck schema and the tables in it.
			This script is organized in the following way:
			1.	Create schema.
			2.	Reference tables with CHAR(3) primary keys
			3.	Reference tables with TINYINT primary keys.
			4.	Data tables with simple, self identified INT primary keys.
			5.	Data tables with mixed and foreign key primary keys.
*/
/********************************************************************************************
*
* 1. Create Schema.
*
**********************************************************************************************/

	--- IF  NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'AutoCheck')
	 ---	CREATE SCHEMA AutoCheck AUTHORIZATION [firstlook]
	
	--- GO
/********************************************************************************************
*
* 2. Create Reference table with CHAR(3) primary keys.
*	Each table should have 2 constraints. 1. Ensure Len(Key)= 3 char, 2. Ensure Len(Description)>0.
*	and an index to ensure that the Description is unique.	
*	a. ReportType
*	b. ResponseCode
*
**********************************************************************************************/

/* 2a. ReportType */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[AutoCheck].[ReportType]') AND type in (N'U'))
	BEGIN
		CREATE 
		TABLE	AutoCheck.ReportType(
			ReportTypeID	TINYINT	NOT NULL,
			Name			VARCHAR(50)	NOT NULL,
		CONSTRAINT [PK_AutoCheck_ReportType] PRIMARY KEY CLUSTERED 
				(ReportTypeID ASC))
	END
	GO
	
	/*** Unique Index ***/

	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[AutoCheck].[ReportType]') AND name = N'UK_AutoCheck_ReportType__Name')
	BEGIN
		CREATE 
		UNIQUE NONCLUSTERED INDEX UK_AutoCheck_ReportType__Name ON AutoCheck.ReportType
		(Name ASC)
	END
	GO

	/*** Check Constraints ***/	
	
	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[AutoCheck].[CK_AutoCheck_ReportType__LenName]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[ReportType]'))
		ALTER TABLE [AutoCheck].[ReportType]  WITH CHECK ADD  CONSTRAINT 
		[CK_AutoCheck_ReportType__LenName] 
		CHECK  ((len([Name])>(0)))
	GO



/* 2b. ResponseCode */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[AutoCheck].[ResponseCode]') AND type in (N'U'))
	BEGIN
		CREATE 
		TABLE	AutoCheck.ResponseCode(
			ResponseCode	CHAR(3)		NOT NULL,
			Description	VARCHAR(500)	NOT NULL,
			IsError		BIT		NOT NULL,
			IsTryAgain	BIT		NOT NULL,
		CONSTRAINT [PK_AutoCheck_ResponseCode] PRIMARY KEY CLUSTERED 
		([ResponseCode] ASC))
	END
	GO

	/*** Unique Index ***/

	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[AutoCheck].[ResponseCode]') AND name = N'UK_AutoCheck_ResponseCode__Description')
	BEGIN
		CREATE 
		UNIQUE NONCLUSTERED INDEX [UK_AutoCheck_ResponseCode__Description] ON [AutoCheck].[ResponseCode] 
		([Description] ASC)
	END
	GO
	
	
	/*** Check Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[AutoCheck].[CK_AutoCheck_ResponseCode__LenDescription]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[ResponseCode]'))
		ALTER TABLE [AutoCheck].[ResponseCode]  WITH CHECK ADD  CONSTRAINT 
			[CK_AutoCheck_ResponseCode__LenDescription] 
		CHECK  ((LEN([Description])>(0)))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[AutoCheck].[CK_AutoCheck_ResponseCode__LenResponseCode]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[ResponseCode]'))
		ALTER TABLE [AutoCheck].[ResponseCode]  WITH CHECK ADD  CONSTRAINT 
			[CK_AutoCheck_ResponseCode__LenResponseCode] 
		CHECK  ((LEN([ResponseCode])=(3)))
	GO



/********************************************************************************************
*
* 3. Create Reference tables with TINYINT primary keys.
*	Each table should have 1 constraint. 1. Ensure Len(Name)>0.
*	and an index to ensure that the Name is unique.	
*	a. AccountStatus
*	b. AccountType
*	c. ReportInspection ** Description instead of Name.
*   d. ServiceType
*
**********************************************************************************************/

/* 3a.  AccountStatus */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[AutoCheck].[AccountStatus]') AND type in (N'U'))
	BEGIN
		CREATE 
		TABLE	AutoCheck.AccountStatus(
			AccountStatusID TINYINT		NOT NULL,
			Name		VARCHAR(50)	NOT NULL,
		CONSTRAINT PK_AutoCheck_AccountStatus PRIMARY KEY CLUSTERED 
		(AccountStatusID ASC))
	END
	GO

	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[AutoCheck].[AccountStatus]') AND name = N'UK_AutoCheck_AccountStatus__Name')
	BEGIN
		CREATE 
		UNIQUE NONCLUSTERED INDEX UK_AutoCheck_AccountStatus__Name ON AutoCheck.AccountStatus
		(Name ASC	)
	END
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[AutoCheck].[CK_AutoCheck_AccountStatus__LenName]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[AccountStatus]'))
		ALTER TABLE [AutoCheck].[AccountStatus]  WITH CHECK ADD  CONSTRAINT [CK_AutoCheck_AccountStatus__LenName] CHECK  ((len([Name])>(0)))
	GO

/* 3b.  AccountType*/

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[AutoCheck].[AccountType]') AND type in (N'U'))
	BEGIN
		CREATE 
		TABLE	AutoCheck.AccountType(
			AccountTypeID	TINYINT		NOT NULL,
			Name		VARCHAR(50)	NOT NULL,
		CONSTRAINT [PK_AutoCheck_AccountType] PRIMARY KEY CLUSTERED 
			(AccountTypeID ASC))
	END
	GO

	/*** Unique Index ***/

	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[AutoCheck].[AccountType]') AND name = N'UK_AutoCheck_AccountType__Name')
	BEGIN
		CREATE 
		UNIQUE NONCLUSTERED INDEX UK_AutoCheck_AccountType__Name ON AutoCheck.AccountType
		(Name ASC)
	END
	GO

	/*** Check Constraints ***/	

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[AutoCheck].[CK_AutoCheck_AccountType__LenName]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[AccountType]'))
		ALTER TABLE [AutoCheck].[AccountType]  WITH CHECK ADD  CONSTRAINT 
		[CK_AutoCheck_AccountType__LenName] 
		CHECK  ((LEN([Name])>(0)))
	GO

/* 3c. ReportInspection */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[AutoCheck].[ReportInspection]') AND type in (N'U'))
	BEGIN
		CREATE 
		TABLE	AutoCheck.ReportInspection(
			ReportInspectionID	TINYINT		NOT NULL,
			Description		VARCHAR(100)	NOT NULL,
			XPath			VARCHAR(500)		NOT NULL,
		CONSTRAINT [PK_AutoCheck_ReportInspectionID] PRIMARY KEY CLUSTERED 
			(ReportInspectionID ASC))
	END
	GO

	/*** Unique Index ***/

	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[AutoCheck].[ReportInspection]') AND name = N'UK_AutoCheck_ReportInspection__Description')
	BEGIN
		CREATE 
		UNIQUE NONCLUSTERED INDEX UK_AutoCheck_ReportInspection__Description ON AutoCheck.ReportInspection
		(Description ASC)
	END
	GO
	
	/*** Check Constraints ***/	

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[AutoCheck].[CK_AutoCheck_AutoCheck_ReportInspection__LenDescription]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[ReportInspection]'))
		ALTER TABLE AutoCheck.ReportInspection  WITH CHECK ADD  CONSTRAINT 
			[CK_AutoCheck_AutoCheck_ReportInspection__LenDescription] 
		CHECK  ((LEN([Description])>(0)))
	GO

/* 3d. ServiceType */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[AutoCheck].[ServiceType]') AND type in (N'U'))
	BEGIN
		CREATE 
		TABLE	AutoCheck.ServiceType(
			ServiceTypeID	TINYINT		NOT NULL,
			Name		VARCHAR(50)	NOT NULL,
		CONSTRAINT [PK_AutoCheck_ServiceType] PRIMARY KEY CLUSTERED 
			(ServiceTypeID ASC))
	END
	GO

	/*** Unique Index ***/

	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[AutoCheck].[ServiceType]') AND name = N'UK_AutoCheck_ServiceType__Name')
	BEGIN
		CREATE 
		UNIQUE NONCLUSTERED INDEX UK_AutoCheck_ServiceType__Name ON AutoCheck.ServiceType
		(Name ASC)
	END
	GO

	/*** Check Constraints ***/	

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[AutoCheck].[CK_AutoCheck_ServiceType__LenName]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[ServiceType]'))
		ALTER TABLE [AutoCheck].[ServiceType]  WITH CHECK ADD  CONSTRAINT 
		[CK_AutoCheck_ServiceType__LenName] 
		CHECK  ((LEN([Name])>(0)))
	GO


/********************************************************************************************
*
* 4. Create Data tables with simple INT primary keys.  These are identities.
*	a. Account -	Insert,Update and Delete User
*	b. Vehicle -	Insert and Update User -** FK to Request added after Request.
*	c. Request -	Insert User
*	d. Exception	
*	Tables with Update user have 2 additional constraints:
*		1). (UpdateUser is NULL AND UpdateDate is NULL) OR (UpdateUser is not null AND UpdateDate is not NULL)
*		2). (UpdateDate is NULL OR UpdateDate >=InsertDate.
*	Tables with Delete user have 2 additional constraints:
*		1). (DeleteUser is NULL AND DeleteDate is NULL) OR (DeleteUser is not null AND DeleteDate is not NULL)
*		2). (DeleteDate is NULL OR DeleteDate >=COALESCE(UpdateDate,InsertDate).
*	All User types have an FK relationship to the member table.
*
**********************************************************************************************/
/* 4a. Account 
	Account has additional CheckConstraints:
		1). Length UserName >0
		2). Length Password >0 AND <=5
*/

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[AutoCheck].[Account]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	AutoCheck.Account(
		AccountID	INT IDENTITY(1,1) NOT NULL,
		AccountStatusID TINYINT		NOT NULL,
		AccountTypeID	TINYINT		NOT NULL,
		UserName	VARCHAR(20)	NOT NULL,
		Password	VARCHAR(20)	NULL,
		InsertUser	INT		NOT NULL,
		InsertDate	DATETIME	NOT NULL,
		UpdateUser	INT		NULL,
		UpdateDate	DATETIME	NULL,
		DeleteUser	INT		NULL,
		DeleteDate	DATETIME	NULL,
		Version		ROWVERSION NOT NULL,
	CONSTRAINT
		PK_AutoCheck_Account PRIMARY KEY CLUSTERED 
		(AccountID ASC))
	END
	GO
	
	/*** Check Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[AutoCheck].[CK_AutoCheck_Account__UpdateUserDate]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account]'))
		ALTER TABLE [AutoCheck].[Account]  WITH CHECK ADD  CONSTRAINT 
		[CK_AutoCheck_Account__UpdateUserDate] CHECK  
		(([UpdateDate] IS NULL AND [UpdateUser] IS NULL) 
		OR ([UpdateDate] IS NOT NULL AND [UpdateUser] IS NOT NULL))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[AutoCheck].[CK_AutoCheck_Account__UpdateDateGreaterThanInsertDate]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account]'))
		ALTER TABLE [AutoCheck].[Account]  WITH CHECK ADD  CONSTRAINT 
		[CK_AutoCheck_Account__UpdateDateGreaterThanInsertDate] CHECK  
		(([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[AutoCheck].[CK_AutoCheck_Account__DeleteUserDate]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account]'))
		ALTER TABLE [AutoCheck].[Account]  WITH CHECK ADD  CONSTRAINT 
		[CK_AutoCheck_Account__DeleteUserDate] CHECK  
		(([DeleteDate] IS NULL AND [DeleteUser] IS NULL) 
		OR ([DeleteDate] IS NOT NULL AND [DeleteUser] IS NOT NULL))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[AutoCheck].[CK_AutoCheck_Account__DeleteDateGreaterThanInsertDate]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account]'))
	ALTER TABLE [AutoCheck].[Account]  WITH CHECK ADD  CONSTRAINT 
		[CK_AutoCheck_Account__DeleteDateGreaterThanInsertDate] CHECK  
		([DeleteDate] IS NULL OR [DeleteDate]>=COALESCE(UpdateDate,InsertDate))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[AutoCheck].[CK_AutoCheck_Account__LenUserName]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account]'))
	ALTER TABLE [AutoCheck].[Account]  WITH CHECK ADD  CONSTRAINT 
		[CK_AutoCheck_Account__LenUserName] 
		CHECK  ((LEN([UserName])>(0)))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[AutoCheck].[CK_AutoCheck_Account__LenPassword]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account]'))
	ALTER TABLE [AutoCheck].[Account]  WITH CHECK ADD  CONSTRAINT 
		[CK_AutoCheck_Account__LenPassword] CHECK  (( [Password] IS NULL OR (len([Password])>(0) AND len([Password])<=(20)) ))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[AutoCheck].[CK_AutoCheck_Account__NullPassword]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account]'))
	ALTER TABLE [AutoCheck].[Account]  WITH CHECK ADD  CONSTRAINT 
		[CK_AutoCheck_Account__NullPassword] CHECK  ((
	([AccountTypeID] IN (1,2) AND [Password] IS NULL) OR
	([AccountTypeID]=(3) AND [Password] IS NOT NULL)
	))
	GO



	/*** Foreign Key Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Account__AccountStatus]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account]'))
	ALTER TABLE [AutoCheck].[Account]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Account__AccountStatus] FOREIGN KEY([AccountStatusID])
	REFERENCES [AutoCheck].[AccountStatus] ([AccountStatusID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Account__AccountType]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account]'))
	ALTER TABLE [AutoCheck].[Account]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Account__AccountType] FOREIGN KEY([AccountTypeID])
	REFERENCES [AutoCheck].[AccountType] ([AccountTypeID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Account__InsertMember]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account]'))
	ALTER TABLE [AutoCheck].[Account]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Account__InsertMember] FOREIGN KEY([InsertUser])
	REFERENCES [dbo].[Member] ([MemberID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Account__UpdateMember]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account]'))
	ALTER TABLE [AutoCheck].[Account]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Account__UpdateMember] FOREIGN KEY([UpdateUser])
	REFERENCES [dbo].[Member] ([MemberID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Account__DeleteMember]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account]'))
	ALTER TABLE [AutoCheck].[Account]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Account__DeleteMember] FOREIGN KEY([DeleteUser])
	REFERENCES [dbo].[Member] ([MemberID])
	GO

/* 4b. Vehicle 
	Vehicle has an additional CheckConstraints:
		1). Length VIN =17
*/

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'AutoCheck.Vehicle') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	AutoCheck.Vehicle(
		VehicleID	INT IDENTITY(1,1) NOT NULL,
		VIN			VARCHAR(17)		NOT NULL,
		RequestID		INT		NULL,
		InsertDate		DATETIME	NOT NULL,
		InsertUser		INT		NOT NULL,
		UpdateDate		DATETIME	NULL,
		UpdateUser		INT		NULL,
	 CONSTRAINT PK_AutoCheck_Vehicle PRIMARY KEY CLUSTERED 
		(VehicleID ASC))
	END
	GO
	
	/*** Check Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[AutoCheck].[CK_AutoCheck_Vehicle__UpdateUserAndDateExists]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Vehicle]'))
	ALTER TABLE [AutoCheck].[Vehicle]  WITH CHECK ADD  CONSTRAINT 
		[CK_AutoCheck_Vehicle__UpdateUserAndDateExists] CHECK  
	(([UpdateUser] IS NULL AND [UpdateDate] IS NULL) OR 
	 ([UpdateUser] IS NOT NULL AND [UpdateDate] IS NOT NULL))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[AutoCheck].[CK_AutoCheck_Vehicle__UpdateDateGreaterThanInsertDate]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Vehicle]'))
	ALTER TABLE [AutoCheck].[Vehicle]  WITH CHECK ADD  CONSTRAINT 
		[CK_AutoCheck_Vehicle__UpdateDateGreaterThanInsertDate] CHECK  
		([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate])
	GO
	
	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[AutoCheck].[CK_AutoCheck_Vehicle__LenVin]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Vehicle]'))
	ALTER TABLE [AutoCheck].[Vehicle]  WITH CHECK ADD  CONSTRAINT 
		[CK_AutoCheck_Vehicle__LenVin] CHECK  ((LEN([VIN])=(17)))
	GO

	/*** Foreign Key Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Vehicle__Member_UpdateUser]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Vehicle]'))
	ALTER TABLE [AutoCheck].[Vehicle]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Vehicle__Member_UpdateUser] FOREIGN KEY([UpdateUser])
		REFERENCES [dbo].[Member] ([MemberID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Vehicle__Member_InsertUser]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Vehicle]'))
	ALTER TABLE [AutoCheck].[Vehicle]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Vehicle__Member_InsertUser] FOREIGN KEY([InsertUser])
	REFERENCES [dbo].[Member] ([MemberID])
	GO

/* 4c. Request */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[AutoCheck].[Request]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	AutoCheck.Request(
		RequestID		INT IDENTITY(1,1) 	NOT NULL,
		VehicleID		INT			NOT NULL,
		AccountID		INT			NOT NULL,
		ReportTypeID	TINYINT 	NOT NULL,
		InsertUser		INT			NOT NULL,
		InsertDate		DATETIME 	NOT NULL,
	CONSTRAINT PK_AutoCheck_Request PRIMARY KEY CLUSTERED 
	(RequestID ASC)
	)
	END
	GO
	
	/*** No Check Constraints ***/
	/*** Foreign Key Constraints ***/
	

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Request__Account]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Request]'))
	ALTER TABLE [AutoCheck].[Request]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Request__Account] FOREIGN KEY([AccountID])
	REFERENCES [AutoCheck].[Account] ([AccountID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Request__Member_InsertUser]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Request]'))
	ALTER TABLE [AutoCheck].[Request]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Request__Member_InsertUser] FOREIGN KEY([InsertUser])
	REFERENCES [dbo].[Member] ([MemberID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Request__ReportType]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Request]'))
	ALTER TABLE [AutoCheck].[Request]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Request__ReportType] FOREIGN KEY([ReportTypeID])
	REFERENCES [AutoCheck].[ReportType] ([ReportTypeID])
	GO


	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Request__VehicleID]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Request]'))
	ALTER TABLE [AutoCheck].[Request]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Request__VehicleID] FOREIGN KEY([VehicleID])
	REFERENCES [AutoCheck].[Vehicle] ([VehicleID])
	GO


/* 4d. Exception */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[AutoCheck].[Exception]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	AutoCheck.Exception(
		ExceptionID		INT IDENTITY(1,1) NOT NULL,
		ExceptionTime		DATETIME	NOT NULL,
		MachineName		NVARCHAR(256)	NOT NULL,
		Message			NVARCHAR(1024)	NOT NULL,
		ExceptionType		NVARCHAR(256)	NULL,
		Details			NTEXT	NULL,
		RequestID		INT		NULL,
	 CONSTRAINT [PK_AutoCheck_Exception] PRIMARY KEY CLUSTERED 
		([ExceptionID] ASC	))
	END
	GO

	/*** No Check Constraints **/


	/*** Foreign Key Contraints ***/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Exception__RequestID]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Exception]'))
	ALTER TABLE	AutoCheck.Exception  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Exception__RequestID] FOREIGN KEY(RequestID)
	REFERENCES [AutoCheck].[Request] (RequestID)
	GO

/********************************************************************************************
*
* 5. Create Data tables with mixed and foreign key primary keys.
*
*	a. Account_Member -	Insert 
*	b. Account_Dealer -	Insert   
*	c. Report 
*	d. ReportInspectionResult
*	e. Response
*	f. Vehicle_Dealer	
*   g. Account_ServiceType
*	Tables with Update user have 2 additional constraints:
*		1). (UpdateUser is NULL AND UpdateDate is NULL) OR (UpdateUser is not null AND UpdateDate is not NULL)
*		2). (UpdateDate is NULL OR UpdateDate >=InsertDate.
*	Tables with Delete user have 2 additional constraints:
*		1). (DeleteUser is NULL AND DeleteDate is NULL) OR (DeleteUser is not null AND DeleteDate is not NULL)
*		2). (DeleteDate is NULL OR DeleteDate >=COALESCE(UpdateDate,InsertDate).
*	All User types have an FK relationship to the member table.
*
**********************************************************************************************/

/* 5a.  Account_Member */	

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[AutoCheck].[Account_Member]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	AutoCheck.Account_Member(
		AccountID	INT		NOT NULL,
		MemberID	INT		NOT NULL,
		InsertUser	INT		NOT NULL,
		InsertDate	DATETIME	NOT NULL
	CONSTRAINT PK_AutoCheck_Account_Member PRIMARY KEY CLUSTERED 
	(AccountID ASC,
	MemberID ASC))
	END
	GO

	/** Check Constraints **/

	/** Foreign Key Constraints **/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Account_Member__MemberID_AccountID]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account_Member]'))
	ALTER TABLE [AutoCheck].[Account_Member]  WITH CHECK ADD  	CONSTRAINT 
		[FK_AutoCheck_Account_Member__MemberID_AccountID] FOREIGN KEY([AccountID])
	REFERENCES [AutoCheck].[Account] ([AccountID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Account_Member__MemberID]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account_Member]'))
	ALTER TABLE [AutoCheck].[Account_Member]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Account_Member__MemberID] FOREIGN KEY([MemberID])
	REFERENCES [dbo].[Member] ([MemberID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Account_Member__MemberID_InsertUser]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account_Member]'))
	ALTER TABLE [AutoCheck].[Account_Member]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Account_Member__MemberID_InsertUser] FOREIGN KEY([InsertUser])
	REFERENCES [dbo].[Member] ([MemberID])
	GO

/* 5b.  Account_Dealer */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[AutoCheck].[Account_Dealer]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	AutoCheck.Account_Dealer(
		DealerID	INT		NOT NULL,
		AccountID	INT		NOT NULL,
		InsertUser	INT		NOT NULL,
		InsertDate	DATETIME	NOT NULL,
		CONSTRAINT PK_AutoCheck_Account_Dealer PRIMARY KEY CLUSTERED 
	(DealerID ASC
	))
	END 
	GO
	
	/** Check Constraints **/

	

	/** Foreign Key Constraints **/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Account_Dealer__AccountID]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account_Dealer]'))
	ALTER TABLE [AutoCheck].[Account_Dealer]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Account_Dealer__AccountID] FOREIGN KEY([AccountID])
	REFERENCES [AutoCheck].[Account] ([AccountID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Account_Dealer__BusinessUnitID]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account_Dealer]'))
	ALTER TABLE [AutoCheck].[Account_Dealer]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Account_Dealer__BusinessUnitID] FOREIGN KEY([DealerID])
	REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
	GO

	
	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Account_Dealer__InsertMember]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account_Dealer]'))
	ALTER TABLE [AutoCheck].[Account_Dealer]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Account_Dealer__InsertMember] FOREIGN KEY([InsertUser])
	REFERENCES [dbo].[Member] ([MemberID])
	GO

/* 5c. Report */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[AutoCheck].[Report]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	AutoCheck.Report(
		RequestID		INT		NOT NULL,
		Report			XML		NOT NULL,
		Score			INT		NULL,
		CompareScoreRangeLow	INT		NULL,
		CompareScoreRangeHigh	INT		NULL,
		Assured			BIT		NULL,
		OwnerCount		INT		NOT NULL,
	CONSTRAINT [PK_AutoCheck_Report] PRIMARY KEY CLUSTERED 
	([RequestID] ASC))
	END
	GO

	/*** No Check Constraints ***/
	
	/*** Foreign Key Constraints ***/
	
	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Report__RequestID]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Report]'))
	ALTER TABLE [AutoCheck].[Report]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Report__RequestID] FOREIGN KEY([RequestID])
	REFERENCES [AutoCheck].[Request] ([RequestID])
	GO

/* 5d. ReportInspectionResult	 */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[AutoCheck].[ReportInspectionResult]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	AutoCheck.ReportInspectionResult(
		RequestID		INT		NOT NULL,
		ReportInspectionID 	TINYINT NOT NULL,
		Selected		BIT		NOT NULL ,
	CONSTRAINT [PK_AutoCheck_ReportInspectionResult] PRIMARY KEY CLUSTERED 
	([RequestID] ASC,
	[ReportInspectionID] ASC))
	END
	GO

	/*** No Check Constraints ***/

	/*** Foreign Key Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_ReportInspectionResult__Report]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[ReportInspectionResult]'))
	ALTER TABLE [AutoCheck].[ReportInspectionResult]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_ReportInspectionResult__Report] FOREIGN KEY([RequestID])
	REFERENCES [AutoCheck].[Report] ([RequestID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_ReportInspectionResult__ReportInspectionID]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[ReportInspectionResult]'))
	ALTER TABLE [AutoCheck].[ReportInspectionResult]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_ReportInspectionResult__ReportInspectionID] FOREIGN KEY([ReportInspectionID])
	REFERENCES [AutoCheck].[ReportInspection] ([ReportInspectionID])
	GO


	
/*	5e. Response */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[AutoCheck].[Response]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	AutoCheck.Response (
		RequestID		INT		NOT NULL,
		ResponseCode	CHAR(3) NOT NULL,
		ReceivedDate	DATETIME NOT NULL,
	CONSTRAINT [PK_AutoCheck_Response] PRIMARY KEY CLUSTERED 
	([RequestID] ASC))
	END
	GO


	/*** No Check Constraints ***/
	
	/***  Foreign Key Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Response__RequestID]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Response]'))
	ALTER TABLE [AutoCheck].[Response]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Response__RequestID] FOREIGN KEY([RequestID])
	REFERENCES [AutoCheck].[Request] ([RequestID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Response__ResponseCode]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Response]'))
	ALTER TABLE [AutoCheck].[Response]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Response__ResponseCode] FOREIGN KEY([ResponseCode])
	REFERENCES [AutoCheck].[ResponseCode] ([ResponseCode])
	GO

/*	5f. Vehicle_Dealer*/

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[AutoCheck].[Vehicle_Dealer]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	AutoCheck.Vehicle_Dealer(
		VehicleID	INT NOT NULL,
		DealerID	INT NOT NULL,
		RequestID	INT NOT NULL,
	CONSTRAINT [PK_AutoCheck_Vehicle_Dealer] PRIMARY KEY CLUSTERED 
	(	[VehicleID] ASC,
		[DealerID] ASC
	))
	END
	GO

	/*** No Check Constraints ***/
	
	/***  Foreign Key Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Vehicle_Dealer__BusinessUnitID]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Vehicle_Dealer]'))
	ALTER TABLE [AutoCheck].[Vehicle_Dealer]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Vehicle_Dealer__BusinessUnitID] FOREIGN KEY([DealerID])
	REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Vehicle_Dealer__VehicleID]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Vehicle_Dealer]'))
	ALTER TABLE [AutoCheck].[Vehicle_Dealer]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Vehicle_Dealer__VehicleID] FOREIGN KEY([VehicleID])
	REFERENCES [AutoCheck].[Vehicle] ([VehicleID])
	GO
	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Vehicle_Dealer__RequestID]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Vehicle_Dealer]'))
	ALTER TABLE [AutoCheck].[Vehicle_Dealer]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Vehicle_Dealer__RequestID] FOREIGN KEY([RequestID])
	REFERENCES [AutoCheck].[Request] ([RequestID])
	GO
	
	
	/* 5g.  Account_ServiceType */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[AutoCheck].[Account_ServiceType]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	AutoCheck.Account_ServiceType(
		ServiceTypeID	TINYINT		NOT NULL,
		AccountID	INT		NOT NULL,
		InsertUser	INT		NOT NULL,
		InsertDate	DATETIME	NOT NULL,
		CONSTRAINT PK_AutoCheck_Account_ServiceType PRIMARY KEY CLUSTERED 
	(ServiceTypeID ASC
	))
	END 
	GO
	
	/** Check Constraints **/

	

	/** Foreign Key Constraints **/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Account_ServiceType__AccountID]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account_ServiceType]'))
	ALTER TABLE [AutoCheck].[Account_ServiceType]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Account_ServiceType__AccountID] FOREIGN KEY([AccountID])
	REFERENCES [AutoCheck].[Account] ([AccountID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Account_ServiceType__BusinessUnitID]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account_ServiceType]'))
	ALTER TABLE [AutoCheck].[Account_ServiceType]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Account_ServiceType__ServiceTypeID] FOREIGN KEY([ServiceTypeID])
	REFERENCES [AutoCheck].[ServiceType] ([ServiceTypeID])
	GO

	
	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[AutoCheck].[FK_AutoCheck_Account_ServiceType__InsertMember]') AND parent_object_id = OBJECT_ID(N'[AutoCheck].[Account_ServiceType]'))
	ALTER TABLE [AutoCheck].[Account_ServiceType]  WITH CHECK ADD  CONSTRAINT 
		[FK_AutoCheck_Account_ServiceType__InsertMember] FOREIGN KEY([InsertUser])
	REFERENCES [dbo].[Member] ([MemberID])
	GO

