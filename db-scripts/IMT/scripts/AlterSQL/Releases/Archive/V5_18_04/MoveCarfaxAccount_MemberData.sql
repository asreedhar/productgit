/* Insert all Account members not on restricted list.*/

DECLARE @CarFaxAdmin INT

SELECT @CarFaxAdmin =MemberID
FROM	dbo.Member 
WHERE	Login  ='CarfaxSystemUserAdmin'

INSERT
INTO	Carfax.Account_Member
	(AccountID,
	MemberID,
	InsertUser,
	InsertDate)
SELECT	AD.AccountID,
	M.MemberID,
	 @CarFaxAdmin,
	GETDATE() 
FROM	dbo.DealerPreference_Carfax DP
JOIN	dbo.MemberAccess MA
on	DP.BusinessUnitID =MA.BusinessUnitID
JOIN	dbo.Member M
ON	M.MemberID =MA.MemberID
JOIN	Carfax.Account_Dealer AD
ON	AD.DealerID =DP.BusinessUnitID
LEFT
JOIN	dbo.CarfaxRestrictedMembers CRM
ON	DP.BusinessUnitID =CRM.BusinessUnitID
AND	M.MemberID =CRM.MemberID
WHERE	CRM.BusinessUnitID is null
AND	CRM.MemberID is null
