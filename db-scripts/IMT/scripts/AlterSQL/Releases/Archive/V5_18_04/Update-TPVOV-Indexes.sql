------------------------------------------------------------------------------------
--
--	THE OLD INDEX ADDED VALUE TO THE INDEX TO CREATE A PRE-2K5 COVERING INDEX
--	HOWEVER, THE TRUE ACCESS IS VIA OptionID + ValueTypeID SO INDEX BY THAT
--	AND INCLUDE THE VALUE.  EVENTUALLY, THIS SHOULD BE THE PK/CLUSTER KEY
--
--	THE FILLFACTOR IS SET TO DEFAULT (100%) AS THE THIS IS AN INSERT ONCE, READ
--	MANY TABLE AND THE KEY IS ThirdPartyOptionID + ValueTypeID.  HOWEVER, THIS 
--	INDEX WILL BE PRONE TO FRAGMENTATION AS TPVOV VALUES ARE DELETED.
--
------------------------------------------------------------------------------------


DROP INDEX ThirdPartyVehicleOptionValues.IX_ThirdPartyVehicleOptionValues__ThirdPartyVehicleOptionID_Value

CREATE INDEX IX_ThirdPartyVehicleOptionValues__OptionID_ValueTypeID_Value ON ThirdPartyVehicleOptionValues(ThirdPartyVehicleOptionID, ThirdPartyOptionValueTypeID) INCLUDE (Value) WITH (ONLINE = ON)

GO