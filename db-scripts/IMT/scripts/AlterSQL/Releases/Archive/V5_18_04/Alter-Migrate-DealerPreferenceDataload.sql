UPDATE	DPD2
SET	TradeorPurchaseFromSales		= DPD.TradeorPurchaseFromSales, 
	TradeorPurchaseFromSalesDaysThreshold	= DPD.TradeorPurchaseFromSalesDaysThreshold, 
	GenerateInventoryDeltas			= DPD.GenerateInventoryDeltas, 
	CalculateUnitCost			= DPD.CalculateUnitCost, 
	VINDecodeCountryCodePrimary		= DPD.VINDecodeCountryCodePrimary, 
	VINDecodeCountryCodeSecondary		= DPD.VINDecodeCountryCodeSecondary
	
FROM	dbo.DealerPreference_Dataload DPD2
	JOIN dbo.DealerPreferenceDataload DPD ON DPD2.BusinessUnitID = DPD.BusinessUnitID
	
	
GO

INSERT
INTO	dbo.DealerPreference_Dataload (BusinessUnitID, TradeorPurchaseFromSales, TradeorPurchaseFromSalesDaysThreshold, GenerateInventoryDeltas, CalculateUnitCost, VINDecodeCountryCodePrimary, VINDecodeCountryCodeSecondary)
SELECT	BusinessUnitID, TradeorPurchaseFromSales, TradeorPurchaseFromSalesDaysThreshold, GenerateInventoryDeltas, CalculateUnitCost, VINDecodeCountryCodePrimary, VINDecodeCountryCodeSecondary
FROM	dbo.DealerPreferenceDataload 
WHERE	NOT EXISTS (	SELECT	1
			FROM	dbo.DealerPreference_Dataload 
			WHERE	DealerPreferenceDataload.BusinessUnitID = DealerPreference_Dataload.BusinessUnitID
			)	
			
GO


DROP TABLE dbo.DealerPreferenceDataload
GO

--------------------------------------------------------------------------------
--	DELETE THIS NEXT RELEASE.
--------------------------------------------------------------------------------

CREATE VIEW dbo.DealerPreferenceDataload
AS
SELECT	0 AS DealerPreferenceDataloadID, BusinessUnitID, TradeorPurchaseFromSales, TradeorPurchaseFromSalesDaysThreshold, GenerateInventoryDeltas, CalculateUnitCost, VINDecodeCountryCodePrimary, VINDecodeCountryCodeSecondary
FROM	DealerPreference_Dataload

GO