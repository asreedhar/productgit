
/* MAK 05/21/2009	This script creates the Carfax schema and the tables in it.
			This script is organized in the following way:
			1.	Create schema.
			2.	Reference tables with CHAR(3) primary keys
			3.	Reference tables with TINYINT primary keys.
			4.	Data tables with simple, self identified INT primary keys.
			5.	Data tables with mixed and foreign key primary keys.
			6.	Temporary Migration Table UG!
*/
/********************************************************************************************
*
* 1. Create Schema.
*
**********************************************************************************************/

	--- IF  NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'Carfax')
	 	CREATE SCHEMA Carfax AUTHORIZATION [firstlook]
	
	--- GO
/********************************************************************************************
*
* 2. Create Reference tables with CHAR(3) primary keys.
*	Each table should have 2 constraints. 1. Ensure Len(Key)= 3 char, 2. Ensure Len(Description)>0.
*	and an index to ensure that the Description is unique.	
*	a. RequestType
*	b. ResponseCode
*	c. TrackingCode
*	d. WindowSticker
*	e. ReportType
*
**********************************************************************************************/

/* 2a. RequestType */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[RequestType]') AND type in (N'U'))
	BEGIN
		CREATE 
		TABLE	Carfax.RequestType(
			RequestType	CHAR(3)		NOT NULL,
			Description	VARCHAR(100)	NOT NULL,
	 	CONSTRAINT [PK_Carfax_RequestType] PRIMARY KEY CLUSTERED 
			([RequestType] ASC)	)
	END
	GO
	
	/*** Unique Index ***/

	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Carfax].[RequestType]') AND name = N'UK_Carfax_RequestType__Description')
	BEGIN
		CREATE 
		UNIQUE NONCLUSTERED INDEX [UK_Carfax_RequestType__Description] ON [Carfax].[RequestType] 
		([Description] ASC	)
	END
	GO

	/*** Check Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_RequestType__LenRequestType]') AND parent_object_id = OBJECT_ID(N'[Carfax].[RequestType]'))
		ALTER TABLE [Carfax].[RequestType]  WITH CHECK ADD  CONSTRAINT 
			[CK_Carfax_RequestType__LenRequestType] 
		CHECK  ((LEN([RequestType])=(3)))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_RequestType__LenDescription]') AND parent_object_id = OBJECT_ID(N'[Carfax].[RequestType]'))
		ALTER TABLE [Carfax].[RequestType]  WITH CHECK ADD  CONSTRAINT 
			[CK_Carfax_RequestType__LenDescription] 
		CHECK  ((LEN([Description])>(0)))
	GO

/* 2b. ResponseCode */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[ResponseCode]') AND type in (N'U'))
	BEGIN
		CREATE 
		TABLE	Carfax.ResponseCode(
			ResponseCode	CHAR(3)		NOT NULL,
			Description	VARCHAR(500)	NOT NULL,
			IsError		BIT		NOT NULL,
			IsTryAgain	BIT		NOT NULL,
		CONSTRAINT [PK_Carfax_ResponseCode] PRIMARY KEY CLUSTERED 
		([ResponseCode] ASC))
	END
	GO

	/*** Unique Index ***/

	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Carfax].[ResponseCode]') AND name = N'UK_Carfax_ResponseCode__Description')
	BEGIN
		CREATE 
		UNIQUE NONCLUSTERED INDEX [UK_Carfax_ResponseCode__Description] ON [Carfax].[ResponseCode] 
		([Description] ASC)
	END
	GO
	
	
	/*** Check Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_ResponseCode__LenDescription]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ResponseCode]'))
		ALTER TABLE [Carfax].[ResponseCode]  WITH CHECK ADD  CONSTRAINT 
			[CK_Carfax_ResponseCode__LenDescription] 
		CHECK  ((LEN([Description])>(0)))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_ResponseCode__LenResponseCode]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ResponseCode]'))
		ALTER TABLE [Carfax].[ResponseCode]  WITH CHECK ADD  CONSTRAINT 
			[CK_Carfax_ResponseCode__LenResponseCode] 
		CHECK  ((LEN([ResponseCode])=(3)))
	GO

/* 2c. TrackingCode*/

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[TrackingCode]') AND type in (N'U'))
	BEGIN
		CREATE 
		TABLE	Carfax.TrackingCode(
			TrackingCode	CHAR(3)		NOT NULL,
			Description	VARCHAR(100)	NOT NULL,
		CONSTRAINT [PK_Carfax_TrackingCode] PRIMARY KEY CLUSTERED 
				([TrackingCode] ASC))
	END
	GO

	/*** Unique Index ***/

	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Carfax].[TrackingCode]') AND name = N'UK_Carfax_TrackingCode_Description')
	BEGIN
		CREATE UNIQUE NONCLUSTERED INDEX [UK_Carfax_TrackingCode_Description] ON [Carfax].[TrackingCode] 
		([Description] ASC)
	END
	GO

	/*** Check Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_TrackingCode__LenDescription]') AND parent_object_id = OBJECT_ID(N'[Carfax].[TrackingCode]'))
		ALTER TABLE [Carfax].[TrackingCode]  WITH CHECK ADD  CONSTRAINT 
			[CK_Carfax_TrackingCode__LenDescription] 
		CHECK  ((LEN([Description])>(0)))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_TrackingCode__LenTrackingCode]') AND parent_object_id = OBJECT_ID(N'[Carfax].[TrackingCode]'))
		ALTER TABLE [Carfax].[TrackingCode]  WITH CHECK ADD  CONSTRAINT 
			[CK_Carfax_TrackingCode__LenTrackingCode] 
		CHECK  ((LEN([TrackingCode])=(3)))
	GO

/* 2d. WindowSticker */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[WindowSticker]') AND type in (N'U'))
	BEGIN
		CREATE 
		TABLE	Carfax.WindowSticker(
			WindowSticker	CHAR(3)		NOT NULL,
			Description	VARCHAR(100)	NOT NULL,
		CONSTRAINT [PK_Carfax_WindowSticker] PRIMARY KEY CLUSTERED 
		([WindowSticker] ASC))
	END
	GO

	/*** Unique Index ***/

	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Carfax].[WindowSticker]') AND name = N'UK_Carfax_WindowSticker__Description')
	CREATE UNIQUE NONCLUSTERED INDEX [UK_Carfax_WindowSticker__Description] ON [Carfax].[WindowSticker] 
	(	[Description] ASC
	)
	GO

	/*** Check Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_WindowSticker__LenDescription]') AND parent_object_id = OBJECT_ID(N'[Carfax].[WindowSticker]'))
	ALTER TABLE [Carfax].[WindowSticker]  WITH CHECK ADD  CONSTRAINT 
		[CK_Carfax_WindowSticker__LenDescription] 
	CHECK  ((LEN([Description])>(0)))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_WindowSticker__LenWindowSticker]') AND parent_object_id = OBJECT_ID(N'[Carfax].[WindowSticker]'))
	ALTER TABLE [Carfax].[WindowSticker]  WITH CHECK ADD  CONSTRAINT 
		[CK_Carfax_WindowSticker__LenWindowSticker] 
	CHECK  ((LEN([WindowSticker])=(3)))
	GO

/* 2e. ReportType */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[ReportType]') AND type in (N'U'))
	BEGIN
		CREATE 
		TABLE	Carfax.ReportType(
			ReportType	CHAR(3)		NOT NULL,
			Name		VARCHAR(50)	NOT NULL,
		CONSTRAINT [PK_Carfax_ReportType] PRIMARY KEY CLUSTERED 
				(ReportType ASC))
	END
	GO
	
	/*** Unique Index ***/

	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Carfax].[ReportType]') AND name = N'UK_Carfax_ReportType__Name')
	BEGIN
		CREATE 
		UNIQUE NONCLUSTERED INDEX UK_Carfax_ReportType__Name ON Carfax.ReportType
		(Name ASC)
	END
	GO

	/*** Check Constraints ***/	
	
	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_ReportType__LenReportType]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportType]'))
		ALTER TABLE [Carfax].[ReportType]  WITH CHECK ADD  CONSTRAINT 
			[CK_Carfax_ReportType__LenReportType] 
			CHECK  ((len([ReportType])=(3)))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_ReportType__LenName]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportType]'))
		ALTER TABLE [Carfax].[ReportType]  WITH CHECK ADD  CONSTRAINT 
		[CK_Carfax_ReportType__LenName] 
		CHECK  ((len([Name])>(0)))
	GO


/********************************************************************************************
*
* 3. Create Reference tables with TINYINT primary keys.
*	Each table should have 1 constraint. 1. Ensure Len(Name)>0.
*	and an index to ensure that the Name is unique.	
*	a. AccountStatus
*	b. AccountType
*	c. ReportInspection ** Description instead of Name.
*	d. VehicleEntityType
*	e. ReportProcessorCommandStatus
*	f. ReportProcessorCommandType
*
**********************************************************************************************/

/* 3a.  AccountStatus */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[AccountStatus]') AND type in (N'U'))
	BEGIN
		CREATE 
		TABLE	Carfax.AccountStatus(
			AccountStatusID TINYINT		NOT NULL,
			Name		VARCHAR(50)	NOT NULL,
		CONSTRAINT PK_Carfax_AccountStatus PRIMARY KEY CLUSTERED 
		(AccountStatusID ASC))
	END
	GO

	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Carfax].[AccountStatus]') AND name = N'UK_Carfax_AccountStatus__Name')
	BEGIN
		CREATE 
		UNIQUE NONCLUSTERED INDEX UK_Carfax_AccountStatus__Name ON Carfax.AccountStatus
		(Name ASC	)
	END
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_AccountStatus__LenName]') AND parent_object_id = OBJECT_ID(N'[Carfax].[AccountStatus]'))
		ALTER TABLE [Carfax].[AccountStatus]  WITH CHECK ADD  CONSTRAINT [CK_Carfax_AccountStatus__LenName] CHECK  ((len([Name])>(0)))
	GO

/* 3b.  AccountType*/

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[AccountType]') AND type in (N'U'))
	BEGIN
		CREATE 
		TABLE	Carfax.AccountType(
			AccountTypeID	TINYINT		NOT NULL,
			Name		VARCHAR(50)	NOT NULL,
		CONSTRAINT [PK_Carfax_AccountType] PRIMARY KEY CLUSTERED 
			(AccountTypeID ASC))
	END
	GO

	/*** Unique Index ***/

	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Carfax].[AccountType]') AND name = N'UK_Carfax_AccountType__Name')
	BEGIN
		CREATE 
		UNIQUE NONCLUSTERED INDEX UK_Carfax_AccountType__Name ON Carfax.AccountType
		(Name ASC)
	END
	GO

	/*** Check Constraints ***/	

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_AccountType__LenName]') AND parent_object_id = OBJECT_ID(N'[Carfax].[AccountType]'))
		ALTER TABLE [Carfax].[AccountType]  WITH CHECK ADD  CONSTRAINT 
		[CK_Carfax_AccountType__LenName] 
		CHECK  ((LEN([Name])>(0)))
	GO

/* 3c. ReportInspection */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[ReportInspection]') AND type in (N'U'))
	BEGIN
		CREATE 
		TABLE	Carfax.ReportInspection(
			ReportInspectionID	TINYINT		NOT NULL,
			Description		VARCHAR(100)	NOT NULL,
			XPath			VARCHAR(500)		NOT NULL,
		CONSTRAINT [PK_Carfax_ReportInspectionID] PRIMARY KEY CLUSTERED 
			(ReportInspectionID ASC))
	END
	GO

	/*** Unique Index ***/

	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Carfax].[ReportInspection]') AND name = N'UK_Carfax_ReportInspection__Description')
	BEGIN
		CREATE 
		UNIQUE NONCLUSTERED INDEX UK_Carfax_ReportInspection__Description ON Carfax.ReportInspection
		(Description ASC)
	END
	GO
	
	/*** Check Constraints ***/	

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_ReportInspection__LenDescription]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportInspection]'))
		ALTER TABLE Carfax.ReportInspection  WITH CHECK ADD  CONSTRAINT 
			[CK_Carfax_ReportInspection__LenDescription] 
		CHECK  ((LEN([Description])>(0)))
	GO


/* 3d.  VehicleEntityType*/

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[VehicleEntityType]') AND type in (N'U'))
	BEGIN
		CREATE 
		TABLE	Carfax.VehicleEntityType(
			VehicleEntityTypeID	TINYINT NOT NULL,
			Name			VARCHAR(50) NOT NULL,
		CONSTRAINT 
			PK_Carfax_VehicleEntityType PRIMARY KEY CLUSTERED 
			(VehicleEntityTypeID ASC))
	END
	GO

	/*** Unique Index ***/

	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Carfax].[VehicleEntityType]') AND name = N'UK_Carfax_VehicleEntityType__Name')
	BEGIN
		CREATE 
		UNIQUE NONCLUSTERED INDEX UK_Carfax_VehicleEntityType__Name ON Carfax.VehicleEntityType
		(Name ASC)
	END
	GO

	/*** Check Constraints ***/	

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_VehicleEntityType__LenName]') AND parent_object_id = OBJECT_ID(N'[Carfax].[VehicleEntityType]'))
		ALTER TABLE [Carfax].[VehicleEntityType]  WITH CHECK ADD  CONSTRAINT 
			[CK_Carfax_VehicleEntityType__LenName] 
		CHECK  ((LEN([Name])>(0)))
	GO

/* 3e.  ReportProcessorCommandStatus */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommandStatus]') AND type in (N'U'))
	BEGIN
		CREATE 
		TABLE	Carfax.ReportProcessorCommandStatus(
			ReportProcessorCommandStatusID	TINYINT		NOT NULL,
			Name			VARCHAR(50)	NOT NULL,
		CONSTRAINT	PK_Carfax_ReportProcessorCommandStatus	PRIMARY KEY CLUSTERED 
			(ReportProcessorCommandStatusID	ASC))
	END
	GO

	/*** Unique Index ***/

	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommandStatus]') AND name = N'UK_Carfax_ReportProcessorCommandStatus__Name')
	BEGIN
		CREATE 
		UNIQUE NONCLUSTERED INDEX UK_Carfax_ReportProcessorCommandStatus__Name ON Carfax.ReportProcessorCommandStatus
		(Name ASC	)
	END
	GO

	/*** Check Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_ReportProcessorCommandStatus__LenName]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommandStatus]'))
	ALTER TABLE [Carfax].[ReportProcessorCommandStatus]  WITH CHECK ADD  CONSTRAINT 
		[CK_Carfax_ReportProcessorCommandStatus__LenName] 
	CHECK  ((len([Name])>(0)))
	GO

/* 3f.  ReportProcessorCommandType*/

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommandType]') AND type in (N'U'))
	BEGIN
		CREATE 
		TABLE	Carfax.ReportProcessorCommandType(
			ReportProcessorCommandTypeID	TINYINT		NOT NULL,
			Name		VARCHAR(50)	NOT NULL,
		CONSTRAINT 
			PK_Carfax_ReportProcessorCommandType PRIMARY KEY CLUSTERED 
			(ReportProcessorCommandTypeID ASC))
	END
	GO

	/*** Unique Index ***/
	
	IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommandType]') AND name = N'UK_Carfax_ReportProcessorCommandType__Name')
	BEGIN
		CREATE 
		UNIQUE NONCLUSTERED INDEX UK_Carfax_ReportProcessorCommandType__Name ON Carfax.ReportProcessorCommandType
		(Name ASC)
	END
	GO

	/*** Check Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_ReportProcessorCommandType__LenName]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommandType]'))
		ALTER TABLE [Carfax].[ReportProcessorCommandType]  WITH CHECK ADD  CONSTRAINT 
			[CK_Carfax_ReportProcessorCommandType__LenName] 
		CHECK  ((LEN([Name])>(0)))
	GO


/********************************************************************************************
*
* 4. Create Data tables with simple INT primary keys.  These are identities.
*	a. Account -	Insert,Update and Delete User
*	b. Vehicle -	Insert and Update User -** FK to Request added after Request.
*	c. Request -	Insert User
*	d. ReportProcessorCommand	   -	Insert and Update User
*	e. Exception	
*	Tables with Update user have 2 additional constraints:
*		1). (UpdateUser is NULL AND UpdateDate is NULL) OR (UpdateUser is not null AND UpdateDate is not NULL)
*		2). (UpdateDate is NULL OR UpdateDate >=InsertDate.
*	Tables with Delete user have 2 additional constraints:
*		1). (DeleteUser is NULL AND DeleteDate is NULL) OR (DeleteUser is not null AND DeleteDate is not NULL)
*		2). (DeleteDate is NULL OR DeleteDate >=COALESCE(UpdateDate,InsertDate).
*	All User types have an FK relationship to the member table.
*
**********************************************************************************************/
/* 4a. Account 
	Account has additional CheckConstraints:
		1). Length UserName >0
		2). Length Password >0 AND <=5
*/

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[Account]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	Carfax.Account(
		AccountID	INT IDENTITY(1,1) NOT NULL,
		AccountStatusID TINYINT		NOT NULL,
		AccountTypeID	TINYINT		NOT NULL,
		UserName	VARCHAR(20)	NOT NULL,
		Password	VARCHAR(5)	NOT NULL,
		InsertUser	INT		NOT NULL,
		InsertDate	DATETIME	NOT NULL,
		UpdateUser	INT		NULL,
		UpdateDate	DATETIME	NULL,
		DeleteUser	INT		NULL,
		DeleteDate	DATETIME	NULL,
		Version		ROWVERSION NOT NULL,
	CONSTRAINT
		PK_Carfax_Account PRIMARY KEY CLUSTERED 
		(AccountID ASC))
	END
	GO
	
	/*** Check Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_Account__UpdateUserDate]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Account]'))
		ALTER TABLE [Carfax].[Account]  WITH CHECK ADD  CONSTRAINT 
		[CK_Carfax_Account__UpdateUserDate] CHECK  
		(([UpdateDate] IS NULL AND [UpdateUser] IS NULL) 
		OR ([UpdateDate] IS NOT NULL AND [UpdateUser] IS NOT NULL))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_Account__UpdateDateGreaterThanInsertDate]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Account]'))
		ALTER TABLE [Carfax].[Account]  WITH CHECK ADD  CONSTRAINT 
		[CK_Carfax_Account__UpdateDateGreaterThanInsertDate] CHECK  
		(([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_Account__DeleteUserDate]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Account]'))
		ALTER TABLE [Carfax].[Account]  WITH CHECK ADD  CONSTRAINT 
		[CK_Carfax_Account__DeleteUserDate] CHECK  
		(([DeleteDate] IS NULL AND [DeleteUser] IS NULL) 
		OR ([DeleteDate] IS NOT NULL AND [DeleteUser] IS NOT NULL))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_Account__DeleteDateGreaterThanInsertDate]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Account]'))
	ALTER TABLE [Carfax].[Account]  WITH CHECK ADD  CONSTRAINT 
		[CK_Carfax_Account__DeleteDateGreaterThanInsertDate] CHECK  
		([DeleteDate] IS NULL OR [DeleteDate]>=COALESCE(UpdateDate,InsertDate))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_Account__LenUserName]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Account]'))
	ALTER TABLE [Carfax].[Account]  WITH CHECK ADD  CONSTRAINT 
		[CK_Carfax_Account__LenUserName] 
		CHECK  ((LEN([UserName])>(0)))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_Account__LenPassword]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Account]'))
	ALTER TABLE [Carfax].[Account]  WITH CHECK ADD  CONSTRAINT 
		[CK_Carfax_Account__LenPassword] CHECK  ((len([Password])>(0)) AND (LEN(Password)<=5))
	GO

	/*** Foreign Key Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'Carfax.FK_Carfax_Account__AccountStatus') AND parent_object_id = OBJECT_ID(N'[Carfax].[Account]'))
	ALTER TABLE [Carfax].[Account]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_Account__AccountStatus] FOREIGN KEY([AccountStatusID])
	REFERENCES [Carfax].[AccountStatus] ([AccountStatusID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'Carfax.FK_Carfax_Account__AccountType') AND parent_object_id = OBJECT_ID(N'[Carfax].[Account]'))
	ALTER TABLE [Carfax].[Account]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_Account__AccountType] FOREIGN KEY([AccountTypeID])
	REFERENCES [Carfax].[AccountType] ([AccountTypeID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'Carfax.FK_Carfax_Account__InsertMember') AND parent_object_id = OBJECT_ID(N'[Carfax].[Account]'))
	ALTER TABLE [Carfax].[Account]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_Account__InsertMember] FOREIGN KEY([InsertUser])
	REFERENCES [dbo].[Member] ([MemberID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_Account__UpdateMember]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Account]'))
	ALTER TABLE [Carfax].[Account]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_Account__UpdateMember] FOREIGN KEY([UpdateUser])
	REFERENCES [dbo].[Member] ([MemberID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_Account__DeleteMember]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Account]'))
	ALTER TABLE [Carfax].[Account]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_Account__DeleteMember] FOREIGN KEY([DeleteUser])
	REFERENCES [dbo].[Member] ([MemberID])
	GO

/* 4b. Vehicle 
	Vehicle has an additional CheckConstraints:
		1). Length VIN =17
*/

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.Vehicle') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	Carfax.Vehicle(
		VehicleID	INT IDENTITY(1,1) NOT NULL,
		VIN			VARCHAR(17)		NOT NULL,
		IsHotListEligible 	BIT		NOT NULL,
		RequestID		INT		NULL,
		InsertDate		DATETIME	NOT NULL,
		InsertUser		INT		NOT NULL,
		UpdateDate		DATETIME	NULL,
		UpdateUser		INT		NULL,
	 CONSTRAINT PK_Carfax_Vehicle PRIMARY KEY CLUSTERED 
		(VehicleID ASC))
	END
	GO
	
	/*** Check Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_Vehicle__UpdateUserAndDateExists]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Vehicle]'))
	ALTER TABLE [Carfax].[Vehicle]  WITH CHECK ADD  CONSTRAINT 
		[CK_Carfax_Vehicle__UpdateUserAndDateExists] CHECK  
	(([UpdateUser] IS NULL AND [UpdateDate] IS NULL) OR 
	 ([UpdateUser] IS NOT NULL AND [UpdateDate] IS NOT NULL))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_Vehicle__UpdateDateGreaterThanInsertDate]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Vehicle]'))
	ALTER TABLE [Carfax].[Vehicle]  WITH CHECK ADD  CONSTRAINT 
		[CK_Carfax_Vehicle__UpdateDateGreaterThanInsertDate] CHECK  
		([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate])
	GO
	
	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_Vehicle__LenVin]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Vehicle]'))
	ALTER TABLE [Carfax].[Vehicle]  WITH CHECK ADD  CONSTRAINT 
		[CK_Carfax_Vehicle__LenVin] CHECK  ((LEN([VIN])=(17)))
	GO

	/*** Foreign Key Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE name ='FK_Carfax_Vehicle_Member__UpdateUser' AND parent_object_id = OBJECT_ID(N'[Carfax].[Vehicle]'))
	ALTER TABLE [Carfax].[Vehicle]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_Vehicle_Member__UpdateUser] FOREIGN KEY([UpdateUser])
		REFERENCES [dbo].[Member] ([MemberID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE name ='FK_Carfax_Vehicle_Member__InsertUser'AND parent_object_id = OBJECT_ID(N'[Carfax].[Vehicle]'))
	ALTER TABLE [Carfax].[Vehicle]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_Vehicle_Member__InsertUser] FOREIGN KEY([InsertUser])
	REFERENCES [dbo].[Member] ([MemberID])
	GO

/* 4c. Request */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[Request]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	Carfax.Request(
		RequestID	INT IDENTITY(1,1) 	NOT NULL,
		VehicleID	INT			NOT NULL,
		AccountID	INT			NOT NULL,
		TrackingCode	CHAR(3) 		NOT NULL,
		RequestType	CHAR(3) 		NOT NULL,
		ReportType	CHAR(3) 		NOT NULL,
		WindowSticker	CHAR(3) 		NOT NULL,
		DisplayInHotList BIT			NOT NULL,
		InsertUser	INT			NOT NULL,
		InsertDate	DATETIME 		NOT NULL,
	CONSTRAINT PK_Carfax_Request PRIMARY KEY CLUSTERED 
	(RequestID ASC)
	)
	END
	GO
	
	/*** No Check Constraints ***/
	/*** Foreign Key Constraints ***/
	

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Request_Account]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Request]'))
	ALTER TABLE [Carfax].[Request]  WITH CHECK ADD  CONSTRAINT 
		[FK_Request_Account] FOREIGN KEY([AccountID])
	REFERENCES [Carfax].[Account] ([AccountID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Request__InsertUser]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Request]'))
	ALTER TABLE [Carfax].[Request]  WITH CHECK ADD  CONSTRAINT 
		[FK_Request__InsertUser] FOREIGN KEY([InsertUser])
	REFERENCES [dbo].[Member] ([MemberID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Request__ReportType]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Request]'))
	ALTER TABLE [Carfax].[Request]  WITH CHECK ADD  CONSTRAINT 
		[FK_Request__ReportType] FOREIGN KEY([ReportType])
	REFERENCES [Carfax].[ReportType] ([ReportType])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_Request__RequestType]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Request]'))
	ALTER TABLE [Carfax].[Request]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_Request__RequestType] FOREIGN KEY([RequestType])
	REFERENCES [Carfax].[RequestType] ([RequestType])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_Request__TrackingCode]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Request]'))
	ALTER TABLE [Carfax].[Request]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_Request__TrackingCode] FOREIGN KEY([TrackingCode])
	REFERENCES [Carfax].[TrackingCode] ([TrackingCode])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_Request__VehicleID]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Request]'))
	ALTER TABLE [Carfax].[Request]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_Request__VehicleID] FOREIGN KEY([VehicleID])
	REFERENCES [Carfax].[Vehicle] ([VehicleID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_Request__WindowSticker]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Request]'))
	ALTER TABLE [Carfax].[Request]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_Request__WindowSticker] FOREIGN KEY([WindowSticker])
	REFERENCES [Carfax].[WindowSticker] ([WindowSticker])
	GO

/* 4d. ReportProcessorCommand */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommand]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	Carfax.ReportProcessorCommand(
		ReportProcessorCommandID			INT IDENTITY(1,1) NOT NULL,
		DealerID		INT			NOT NULL,
		VehicleID		INT			NOT NULL,
		VehicleEntityTypeID TINYINT	NOT NULL,
		ReportProcessorCommandTypeID		TINYINT		NOT NULL,
		ReportProcessorCommandStatusID	TINYINT		NOT NULL,
		ProcessAfterDate DATETIME	NOT NULL,
		InsertUser		INT			NOT NULL,
		InsertDate		DATETIME	NOT NULL,
		UpdateUser		INT			NULL,
		UpdateDate		DATETIME	NULL,
	 CONSTRAINT PK_Carfax_ReportProcessorCommand PRIMARY KEY CLUSTERED 
	(	ReportProcessorCommandID ASC))
	END
	GO
	
	/*** Check Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_ReportProcessorCommand__UpdateUserAndDate]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommand]'))
		ALTER TABLE [Carfax].[ReportProcessorCommand]  WITH CHECK ADD  CONSTRAINT 
		[CK_Carfax_ReportProcessorCommand__UpdateUserAndDate] 
	CHECK  (([UpdateUser] IS NULL AND [UpdateDate] IS NULL) 
		OR ([UpdateUser] IS NOT NULL AND [UpdateDate] IS NOT NULL))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_ReportProcessorCommand__UpdateDateGreaterThanInsertDate]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommand]'))
	ALTER TABLE [Carfax].[ReportProcessorCommand]  WITH CHECK ADD 	CONSTRAINT 
		[CK_Carfax_ReportProcessorCommand__UpdateDateGreaterThanInsertDate] 
	CHECK  (([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
	GO

	/** Foreign Key Constraints **/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportProcessorCommand__BusinessUnitID]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommand]'))
	ALTER TABLE [Carfax].[ReportProcessorCommand]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_ReportProcessorCommand__BusinessUnitID] FOREIGN KEY([DealerID])
	REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportProcessorCommand_Member__InsertUser]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommand]'))
	ALTER TABLE [Carfax].[ReportProcessorCommand]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_ReportProcessorCommand_Member__InsertUser] FOREIGN KEY([InsertUser])
	REFERENCES [dbo].[Member] ([MemberID])
	GO
	
	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportProcessorCommand_Member__UpdateUser]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommand]'))
	ALTER TABLE [Carfax].[ReportProcessorCommand]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_ReportProcessorCommand_Member__UpdateUser] FOREIGN KEY([UpdateUser])
	REFERENCES [dbo].[Member] ([MemberID])
	GO
	
	IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportProcessorCommand__VehicleID]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommand]'))
ALTER TABLE [Carfax].[ReportProcessorCommand]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_ReportProcessorCommand__VehicleID] FOREIGN KEY([VehicleID])
	REFERENCES [Carfax].[Vehicle] ([VehicleID])
	GO
	
	IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportProcessorCommand__VehicleEntityTypeID]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommand]'))
ALTER TABLE [Carfax].[ReportProcessorCommand]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_ReportProcessorCommand__VehicleEntityTypeID] FOREIGN KEY([VehicleEntityTypeID])
	REFERENCES [Carfax].[VehicleEntityType] ([VehicleEntityTypeID])
	GO

	IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportProcessorCommand__ReportProcessorCommandStatus]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommand]'))
ALTER TABLE [Carfax].[ReportProcessorCommand]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_ReportProcessorCommand__ReportProcessorCommandStatus] FOREIGN KEY([ReportProcessorCommandStatusID])
	REFERENCES [Carfax].[ReportProcessorCommandStatus] ([ReportProcessorCommandStatusID])
	GO

		IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportProcessorCommand__ReportProcessorCommandType]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommand]'))
ALTER TABLE [Carfax].[ReportProcessorCommand]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_ReportProcessorCommand__ReportProcessorCommandType] FOREIGN KEY([ReportProcessorCommandTypeID])
	REFERENCES [Carfax].[ReportProcessorCommandType] ([ReportProcessorCommandTypeID])
	GO

/* 4e. Exception */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[Exception]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	Carfax.Exception(
		ExceptionID		INT IDENTITY(1,1) NOT NULL,
		ExceptionTime		DATETIME	NOT NULL,
		MachineName		NVARCHAR(256)	NOT NULL,
		Message			NVARCHAR(1024)	NOT NULL,
		ExceptionType		NVARCHAR(256)	NULL,
		Details			NTEXT	NULL,
		RequestID		INT		NULL,
	 CONSTRAINT [PK_Carfax_Exception] PRIMARY KEY CLUSTERED 
		([ExceptionID] ASC	))
	END
	GO

	/*** No Check Constraints **/


	/*** Foreign Key Contraints ***/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Exception__RequestID]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Exception]'))
	ALTER TABLE	Carfax.Exception  WITH CHECK ADD  CONSTRAINT 
		[FK_Exception__RequestID] FOREIGN KEY(RequestID)
	REFERENCES [Carfax].[Request] (RequestID)
	GO

/********************************************************************************************
*
* 5. Create Data tables with mixed and foreign key primary keys.
*
*	a. Account_Member -	Insert 
*	b. Account_Dealer -	Insert   
*	c. Report 
*	d. ReportInspectionResult	 
*	e. ReportPreference -	Insert,Update and Delete User
*	f. Response
*	g. Vehicle_Dealer	
*	h. ReportProcessorCommand_Exception
*	i. ReportProcessorCommand_Request	
*	Tables with Update user have 2 additional constraints:
*		1). (UpdateUser is NULL AND UpdateDate is NULL) OR (UpdateUser is not null AND UpdateDate is not NULL)
*		2). (UpdateDate is NULL OR UpdateDate >=InsertDate.
*	Tables with Delete user have 2 additional constraints:
*		1). (DeleteUser is NULL AND DeleteDate is NULL) OR (DeleteUser is not null AND DeleteDate is not NULL)
*		2). (DeleteDate is NULL OR DeleteDate >=COALESCE(UpdateDate,InsertDate).
*	All User types have an FK relationship to the member table.
*
**********************************************************************************************/

/* 5a.  Account_Member */	

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[Account_Member]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	Carfax.Account_Member(
		AccountID	INT		NOT NULL,
		MemberID	INT		NOT NULL,
		InsertUser	INT		NOT NULL,
		InsertDate	DATETIME	NOT NULL
	CONSTRAINT PK_Carfax_Account_Member PRIMARY KEY CLUSTERED 
	(AccountID ASC,
	MemberID ASC))
	END
	GO

	/** Check Constraints **/

	/** Foreign Key Constraints **/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Account_Member__Account]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Account_Member]'))
	ALTER TABLE [Carfax].[Account_Member]  WITH CHECK ADD  	CONSTRAINT 
		[FK_Account_Member__Account] FOREIGN KEY([AccountID])
	REFERENCES [Carfax].[Account] ([AccountID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Account_Member__Member]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Account_Member]'))
	ALTER TABLE [Carfax].[Account_Member]  WITH CHECK ADD  CONSTRAINT 
		[FK_Account_Member__Member] FOREIGN KEY([MemberID])
	REFERENCES [dbo].[Member] ([MemberID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Account_Member__InsertUser]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Account_Member]'))
	ALTER TABLE [Carfax].[Account_Member]  WITH CHECK ADD  CONSTRAINT 
		[FK_Account_Member__InsertUser] FOREIGN KEY([InsertUser])
	REFERENCES [dbo].[Member] ([MemberID])
	GO

/* 5b.  Account_Dealer */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[Account_Dealer]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	Carfax.Account_Dealer(
		DealerID	INT		NOT NULL,
		AccountID	INT		NOT NULL,
		InsertUser	INT		NOT NULL,
		InsertDate	DATETIME	NOT NULL,
		CONSTRAINT PK_Carfax_Account_Dealer PRIMARY KEY CLUSTERED 
	(DealerID ASC
	))
	END 
	GO
	
	/** Check Constraints **/

	

	/** Foreign Key Constraints **/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Account_Dealer__Account]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Account_Dealer]'))
	ALTER TABLE [Carfax].[Account_Dealer]  WITH CHECK ADD  CONSTRAINT 
		[FK_Account_Dealer__Account] FOREIGN KEY([AccountID])
	REFERENCES [Carfax].[Account] ([AccountID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Account_Dealer__BusinessUnit]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Account_Dealer]'))
	ALTER TABLE [Carfax].[Account_Dealer]  WITH CHECK ADD  CONSTRAINT 
		[FK_Account_Dealer__BusinessUnit] FOREIGN KEY([DealerID])
	REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
	GO

	
	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Account_Dealer__InsertMember]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Account_Dealer]'))
	ALTER TABLE [Carfax].[Account_Dealer]  WITH CHECK ADD  CONSTRAINT 
		[FK_Account_Dealer__InsertMember] FOREIGN KEY([InsertUser])
	REFERENCES [dbo].[Member] ([MemberID])
	GO

/* 5c. Report */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[Report]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	Carfax.Report(
		RequestID	INT		NOT NULL,
		Report		XML		NOT NULL,
		ExpirationDate	DATETIME 	NOT NULL,
		OwnerCount	TINYINT 	NULL,
		HasProblem	BIT		NOT NULL,
	CONSTRAINT [PK_Carfax_Report] PRIMARY KEY CLUSTERED 
	([RequestID] ASC))
	END
	GO

	/*** No Check Constraints ***/
	
	/*** Foreign Key Constraints ***/
	
	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_Report__Request]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Report]'))
	ALTER TABLE [Carfax].[Report]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_Report__Request] FOREIGN KEY([RequestID])
	REFERENCES [Carfax].[Request] ([RequestID])
	GO

/* 5d. ReportInspectionResult	 */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[ReportInspectionResult]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	Carfax.ReportInspectionResult(
		RequestID		INT		NOT NULL,
		ReportInspectionID 	TINYINT NOT NULL,
		Selected		BIT		NOT NULL ,
	CONSTRAINT [PK_Carfax_ReportInspectionResult] PRIMARY KEY CLUSTERED 
	([RequestID] ASC,
	[ReportInspectionID] ASC))
	END
	GO

	/*** No Check Constraints ***/

	/*** Foreign Key Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportInspectionResult__Report]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportInspectionResult]'))
	ALTER TABLE [Carfax].[ReportInspectionResult]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_ReportInspectionResult__Report] FOREIGN KEY([RequestID])
	REFERENCES [Carfax].[Report] ([RequestID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportInspectionResult__ReportInspection]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportInspectionResult]'))
	ALTER TABLE [Carfax].[ReportInspectionResult]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_ReportInspectionResult__ReportInspection] FOREIGN KEY([ReportInspectionID])
	REFERENCES [Carfax].[ReportInspection] ([ReportInspectionID])
	GO

/*	5e. ReportPreference -	Insert,Update User */
	
	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[ReportPreference]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	Carfax.ReportPreference(
		DealerID		INT	NOT NULL,
		VehicleEntityTypeID	TINYINT		NOT NULL,
		AutoPurchase		BIT		NOT NULL,
		DisplayInHotList	BIT		NOT NULL,
		ReportType		CHAR(3) 	NOT NULL,
		InsertUser		INT		NOT NULL,
		InsertDate		DATETIME 	NOT NULL,
		UpdateUser		INT		NULL,
		UpdateDate		DATETIME 	NULL,
		Version			TIMESTAMP 	NOT NULL,
	 CONSTRAINT PK_Carfax_ReportPreferences PRIMARY KEY CLUSTERED 
	(		DealerID ASC,
			VehicleEntityTypeID))
	END
	GO

	/***  Check Constraints ***/
	
	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_ReportPreference__UpdateDate]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportPreference]'))
	ALTER TABLE [Carfax].[ReportPreference]  WITH CHECK ADD  CONSTRAINT 
		[CK_Carfax_ReportPreference__UpdateDate] CHECK  
		(([UpdateDate] IS NULL AND [UpdateUser] IS NULL) OR 
		([UpdateDate] IS NOT NULL AND [UpdateUser] IS NOT NULL))
	GO

	IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Carfax].[CK_Carfax_ReportPreference__UpdateInsertDate]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportPreference]'))
	ALTER TABLE [Carfax].[ReportPreference]  WITH CHECK ADD  CONSTRAINT 
		[CK_Carfax_ReportPreference__UpdateInsertDate] CHECK  
		(([UpdateDate] IS NULL OR [UpdateDate]>=[InsertDate]))
	GO


	
	/*** Foreign Key Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportPreference__VehicleEntityTypeID]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportPreference]'))
	ALTER TABLE [Carfax].[ReportPreference]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_ReportPreference__VehicleEntityTypeID] FOREIGN KEY([VehicleEntityTypeID])
	REFERENCES [Carfax].[VehicleEntityType] ([VehicleEntityTypeID])
	GO	

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportPreference__ReportType]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportPreference]'))
	ALTER TABLE [Carfax].[ReportPreference]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_ReportPreference__ReportType] FOREIGN KEY([ReportType])
	REFERENCES [Carfax].[ReportType] ([ReportType])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportPreference__BusinessUnit]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportPreference]'))
	ALTER TABLE [Carfax].[ReportPreference]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_ReportPreference__BusinessUnit] FOREIGN KEY([DealerID])
	REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportPreference__InsertUser]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportPreference]'))
	ALTER TABLE [Carfax].[ReportPreference]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_ReportPreference__InsertUser] FOREIGN KEY([InsertUser])
	REFERENCES [dbo].[Member] ([MemberID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportPreference__UpdateUser]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportPreference]'))
	ALTER TABLE [Carfax].[ReportPreference]  WITH CHECK ADD  CONSTRAINT [FK_Carfax_ReportPreference__UpdateUser] FOREIGN KEY([UpdateUser])
	REFERENCES [dbo].[Member] ([MemberID])
	GO
	
	
/*	5f. Response */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[Response]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	Carfax.Response (
		RequestID		INT		NOT NULL,
		ResponseCode	CHAR(3) NOT NULL,
		ReceivedDate	DATETIME NOT NULL,
	CONSTRAINT [PK_Carfax_Response] PRIMARY KEY CLUSTERED 
	([RequestID] ASC))
	END
	GO


	/*** No Check Constraints ***/
	
	/***  Foreign Key Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_Response__RequestID]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Response]'))
	ALTER TABLE [Carfax].[Response]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_Response__RequestID] FOREIGN KEY([RequestID])
	REFERENCES [Carfax].[Request] ([RequestID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_Response__ResponseCode]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Response]'))
	ALTER TABLE [Carfax].[Response]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_Response__ResponseCode] FOREIGN KEY([ResponseCode])
	REFERENCES [Carfax].[ResponseCode] ([ResponseCode])
	GO

/*	5g. Vehicle_Dealer*/

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[Vehicle_Dealer]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	Carfax.Vehicle_Dealer(
		VehicleID	INT NOT NULL,
		DealerID	INT NOT NULL,
		RequestID	INT NOT NULL,
		IsHotListEnabled 	BIT		NOT NULL,
	CONSTRAINT [PK_Carfax_Vehicle_Dealer] PRIMARY KEY CLUSTERED 
	(	[VehicleID] ASC,
		[DealerID] ASC
	))
	END
	GO

	/*** No Check Constraints ***/
	
	/***  Foreign Key Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_Vehicle_Dealer__BusinessUnit]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Vehicle_Dealer]'))
	ALTER TABLE [Carfax].[Vehicle_Dealer]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_Vehicle_Dealer__BusinessUnit] FOREIGN KEY([DealerID])
	REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_Vehicle_Dealer__Vehicle]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Vehicle_Dealer]'))
	ALTER TABLE [Carfax].[Vehicle_Dealer]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_Vehicle_Dealer__Vehicle] FOREIGN KEY([VehicleID])
	REFERENCES [Carfax].[Vehicle] ([VehicleID])
	GO
	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_Vehicle_Dealer__Request]') AND parent_object_id = OBJECT_ID(N'[Carfax].[Vehicle_Dealer]'))
	ALTER TABLE [Carfax].[Vehicle_Dealer]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_Vehicle_Dealer__Request] FOREIGN KEY([RequestID])
	REFERENCES [Carfax].[Request] ([RequestID])
	GO
	
	
/* 5h.	ReportProcessorCommand_Exception */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommand_Exception]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	Carfax.ReportProcessorCommand_Exception(
		ReportProcessorCommandID INT NOT NULL,
		ExceptionID INT NOT NULL
		CONSTRAINT [PK_Carfax_ReportProcessorCommand_Exception] PRIMARY KEY CLUSTERED 
	(	[ReportProcessorCommandID] ASC,
		[ExceptionID] ASC
	))
	END
	GO

	/*** No Check Constraints ***/
	
	/***  Foreign Key Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportProcessorCommand_Exception__ReportProcessorCommandID]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommand_Exception]'))
	ALTER TABLE [Carfax].[ReportProcessorCommand_Exception]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_ReportProcessorCommand_Exception__ReportProcessorCommandID] FOREIGN KEY([ReportProcessorCommandID])
	REFERENCES[Carfax].[ReportProcessorCommand]([ReportProcessorCommandID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportProcessorCommand_Exception__ExceptionID]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommand_Exception]'))
	ALTER TABLE [Carfax].[ReportProcessorCommand_Exception]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_ReportProcessorCommand_Exception__ExceptionID] FOREIGN KEY([ExceptionID])
	REFERENCES[Carfax].[Exception]([ExceptionID])
	GO


/* 5i.	ReportProcessorCommand_Request */

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommand_Request]') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	Carfax.ReportProcessorCommand_Request(
		ReportProcessorCommandID INT NOT NULL,
		RequestID INT NOT NULL
		CONSTRAINT [PK_Carfax_ReportProcessorCommand_Request] PRIMARY KEY CLUSTERED 
	(	[ReportProcessorCommandID] ASC,
		[RequestID] ASC
	))
	END
	GO

	/*** No Check Constraints ***/
	
	/***  Foreign Key Constraints ***/

	 
	IF  NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportProcessorCommand_Request__ReportProcessorCommandID]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommand_Request]'))
	ALTER TABLE [Carfax].[ReportProcessorCommand_Request]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_ReportProcessorCommand_Request__ReportProcessorCommandID] FOREIGN KEY([ReportProcessorCommandID])
	REFERENCES[Carfax].[ReportProcessorCommand]([ReportProcessorCommandID])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportProcessorCommand_Request__RequestID]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommand_Request]'))
	ALTER TABLE [Carfax].[ReportProcessorCommand_Request]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_ReportProcessorCommand_Request__RequestID] FOREIGN KEY([RequestId])
	REFERENCES[Carfax].[Request]([RequestID])
	GO

/********************************************************************************************
*
* 6. Create Data tables with mixed and foreign key primary keys.
*
*	a. Carfax.ReportProcessorCommand_DataMigration 
*	 
**********************************************************************************************/

/* 6.a	Carfax.ReportProcessorCommand_DataMigration */	

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.ReportProcessorCommand_DataMigration') AND type in (N'U'))
	BEGIN
	CREATE 
	TABLE	Carfax.ReportProcessorCommand_DataMigration (
		ReportProcessorCommandID INT NOT NULL,
		ReportType	CHAR(3) NOT NULL,
		IsHotListed	BIT	NOT NULL
		CONSTRAINT [PK_Carfax_ReportProcessorCommand_DataMigration] PRIMARY KEY CLUSTERED 
	(	[ReportProcessorCommandID] ASC
	))
	END

	/*** No Check Constraints ***/
	
	/***  Foreign Key Constraints ***/

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportProcessorCommand_DataMigration__ReportType]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommand_DataMigration]'))
	ALTER TABLE [Carfax].[ReportProcessorCommand_DataMigration]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_ReportProcessorCommand_DataMigration__ReportType] FOREIGN KEY([ReportType])
	REFERENCES[Carfax].[ReportType]([ReportType])
	GO

	IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportProcessorCommand_DataMigration__ReportProcessorCommandID]') AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommand_DataMigration]'))
	ALTER TABLE [Carfax].[ReportProcessorCommand_DataMigration]  WITH CHECK ADD  CONSTRAINT 
		[FK_Carfax_ReportProcessorCommand_DataMigration__ReportProcessorCommandID] FOREIGN KEY([ReportProcessorCommandID])
	REFERENCES[Carfax].[ReportProcessorCommand]([ReportProcessorCommandID])
	GO
	 
	 
 