--********************************************************
--IMT.dbo.DealTradeIn
--********************************************************
CREATE TABLE dbo.DealTradeIn (
InventoryId INT NOT NULL
	CONSTRAINT FK_DealTradeIn__InventoryID 
	FOREIGN KEY REFERENCES dbo.Inventory (InventoryId),
VIN char(17) NOT NULL,
VehicleCatalogId INT NULL,
Make VARCHAR(50) NOT NULL,
Model VARCHAR(50) NOT NULL,
ModelYear INT NOT NULL,
BodyType VARCHAR(50) NULL,
DoorCount TINYINT NULL,
ExteriorColor VARCHAR(50) NULL,
DriveType VARCHAR(20) NULL,
EngineDisplacement DECIMAL(4,1) NULL,
FuelType VARCHAR(10) NULL,
Transmission VARCHAR(10) NULL,
Trim VARCHAR(50) NULL,
Mileage INT NULL,
CreditAmount INT NULL,
ActualCashValue INT NULL,
LienAmount INT NULL,
InsertDate SMALLDATETIME NOT NULL 
	CONSTRAINT DF_DealTradeIn__InsertDate 
	DEFAULT GETDATE(),
InsertUser VARCHAR(50) NOT NULL 
	CONSTRAINT DF_DealTradeIn__InsertUser 
	DEFAULT SYSTEM_USER,
UpdateDate SMALLDATETIME NOT NULL
	CONSTRAINT DF_DealTradeIn__UpdateDate
	DEFAULT GETDATE(),
UpdateUser VARCHAR(50) NOT NULL
	CONSTRAINT DF_DealTradeIn__UpdateUser
	DEFAULT SYSTEM_USER
)
GO
CREATE UNIQUE CLUSTERED INDEX PK_DealTradeIn_InventoryIdVIN on dbo.DealTradeIn (InventoryId, VIN)
CREATE INDEX IX_DealTradeIn__VIN on dbo.DealTradeIn (VIN)
GO

--********************************************************
--IMT.dbo.DealFinance
--********************************************************
CREATE TABLE dbo.DealFinance (
InventoryId INT NOT NULL
	CONSTRAINT FK_DealFinance__InventoryID 
	FOREIGN KEY REFERENCES dbo.Inventory (InventoryId),
FinancedAmount MONEY NOT NULL,
TermInMonths TINYINT NULL,
AnnualPercentageRate DECIMAL(4,2) NULL,
MonthlyPaymentAmount MONEY NULL,
LenderName VARCHAR(100) NULL,
ManufacturerSuggestedRetailPrice MONEY NULL,
BalloonPaymentAmount MONEY NULL,
BuyRate DECIMAL(4,2) NULL,
CashDownAmount MONEY NULL,
DeferredDownPaymentAmount MONEY NULL,
RebateAmount MONEY NULL,
InsertDate SMALLDATETIME NOT NULL 
	CONSTRAINT DF_DealFinance__InsertDate 
	DEFAULT GETDATE(),
InsertUser VARCHAR(50) NOT NULL 
	CONSTRAINT DF_DealFinance__InsertUser 
	DEFAULT SYSTEM_USER,
UpdateDate SMALLDATETIME NOT NULL
	CONSTRAINT DF_DealFinance__UpdateDate
	DEFAULT GETDATE(),
UpdateUser VARCHAR(50) NOT NULL
	CONSTRAINT DF_DealFinance__UpdateUser
	DEFAULT SYSTEM_USER
)
GO
CREATE UNIQUE CLUSTERED INDEX PK_DealFinance on dbo.DealFinance (InventoryId)
GO

--********************************************************
--IMT.dbo.DealLease
--********************************************************
CREATE TABLE dbo.DealLease (
InventoryId INT NOT NULL 
	CONSTRAINT FK_DealLease__InventoryID 
	FOREIGN KEY REFERENCES dbo.Inventory (InventoryId),
AllowedAnnualMileage INT NULL,
ResidualAmount MONEY NULL,
MoneyFactor MONEY NULL,
CapitalReductionAmount MONEY NULL,
DriveOffAmount MONEY NULL,
InsertDate SMALLDATETIME NOT NULL 
	CONSTRAINT DF_DealLease__InsertDate 
	DEFAULT GETDATE(),
InsertUser VARCHAR(50) NOT NULL 
	CONSTRAINT DF_DealLease__InsertUser 
	DEFAULT SYSTEM_USER,
UpdateDate SMALLDATETIME NOT NULL
	CONSTRAINT DF_DealLease__UpdateDate
	DEFAULT GETDATE(),
UpdateUser VARCHAR(50) NOT NULL
	CONSTRAINT DF_DealLease__UpdateUser
	DEFAULT SYSTEM_USER
)
GO
CREATE UNIQUE CLUSTERED INDEX PK_DealLease on dbo.DealLease (InventoryId)
GO