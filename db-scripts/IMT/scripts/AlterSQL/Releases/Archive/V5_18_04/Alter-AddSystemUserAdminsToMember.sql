/** MAK New Member as Carfax Admin **/
/** MAK New Member as AutoCheck Admin **/


INSERT
INTO	[IMT].dbo.Member
	(Login,
	Password,
	FirstName, 
	LastName, 
	DashboardRowDisplay,
	CreateDate,
	UserRoleCD,
	JobTitleID,
	InventoryOverViewSortOrderType,
	GroupHomePageID,
	ActiveInventoryLaunchTool,
	MemberType,
	Active)
SELECT	'CarfaxSystemUserAdmin',
	'7288edd0fc3ffcbe93a0cf06e3568e28521111bc',
	'Carfax', 
	'Admin', 
	DashboardRowDisplay,
	GETDATE(),
	UserRoleCD,	
	JobTitleID,
	InventoryOverViewSortOrderType,
	GroupHomePageID,
	ActiveInventoryLaunchTool,
	1,
	1
FROM [IMT].dbo.Member where Login ='admin'
GO

INSERT
INTO	[IMT].dbo.Member
	(Login,
	Password,
	FirstName, 
	LastName, 
	DashboardRowDisplay,
	CreateDate,
	UserRoleCD,
	JobTitleID,
	InventoryOverViewSortOrderType,
	GroupHomePageID,
	ActiveInventoryLaunchTool,
	MemberType,
	Active)
SELECT	'AutoCheckSystemUserAdmin',
	'7288edd0fc3ffcbe93a0cf06e3568e28522222bc',
	'AutoCheck', 
	'Admin', 
	DashboardRowDisplay,
	GETDATE(),
	UserRoleCD,	
	JobTitleID,
	InventoryOverViewSortOrderType,
	GroupHomePageID,
	ActiveInventoryLaunchTool,
	1,
	1
FROM [IMT].dbo.Member where Login ='admin'

GO