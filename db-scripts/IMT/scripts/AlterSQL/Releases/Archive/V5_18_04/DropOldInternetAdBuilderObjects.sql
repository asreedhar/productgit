
/* MAK 07/01/2009 	Drop of DP_IAB moved into Market.  Data has to be imported first.*/

--DROP TABLE [IMT].[dbo].[DealerPreference_InternetAdvertisementBuilder]
DROP TABLE [IMT].[dbo].[InternetAdvertisementBuilderOptionProvider]
GO
DROP TABLE [IMT].[dbo].[DealerInternetAdvertisementBuilderOptionProvider]
GO

