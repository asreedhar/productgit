/* Insert all Account members not on restricted list.*/

DECLARE @AutoCheckAdmin INT

SELECT @AutoCheckAdmin =MemberID
FROM	[IMT].dbo.Member 
WHERE	Login  ='AutoCheckSystemUserAdmin'

INSERT
INTO	[IMT].AutoCheck.Account_Member
	(AccountID,
	MemberID,
	InsertUser,
	InsertDate)
SELECT	AD.AccountID,
	mc.MemberID,
	 @AutoCheckAdmin,
	GETDATE() 
FROM	[IMT].dbo.MemberCredential mc
JOIN	[IMT].dbo.MemberAccess ma ON ma.MemberID = mc.MemberID
JOIN	[IMT].dbo.Member M ON	M.MemberID =MA.MemberID
JOIN	[IMT].AutoCheck.Account_Dealer AD ON	AD.DealerID =ma.BusinessUnitID

WHERE     (mc.CredentialTypeID = 2) AND (LEN(mc.Username) >0)
