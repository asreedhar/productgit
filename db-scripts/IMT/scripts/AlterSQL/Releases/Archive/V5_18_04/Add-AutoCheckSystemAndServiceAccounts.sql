/***********************************************************************
*
*  MAK 	06/25/2009	Add Service and System Accounts for AutoCheck.
*
************************************************************************/

DECLARE @MemberID INT
SELECT	@MemberID = MemberID
FROM	[IMT].dbo.Member
WHERE	Login ='AutoCheckSystemUserAdmin'


/* Add System AutoCheck System Account*/

IF NOT EXISTS (	SELECT 1
		FROM	AutoCheck.Account
		WHERE	AccountTypeID =2)

BEGIN

	INSERT
	INTO	AutoCheck.Account
		(AccountStatusID,
		AccountTypeID,
		UserName,
		Password,
		InsertUser,
		InsertDate)
	VALUES	(3,
		2,
		'5010085',
		NULL,
		@MemberID,
		GETDATE())
END

/**************************************************************
*
* Add AutoCheck Service Accounts    - XML ServiceTypeID =1
*
***************************************************************/
 
IF NOT EXISTS (SELECT 1
		FROM 	AutoCheck.Account A
		JOIN	AutoCheck.Account_ServiceType ST
		ON	A.AccountID =ST.AccountID
		WHERE 	A.AccountTypeID =3
		AND	ST.ServiceTypeid=1) 
BEGIN	
	INSERT
	INTO 	AutoCheck.Account 
		(AccountStatusID,
		AccountTypeID,
		UserName,
		password,
		InsertUser,
		InsertDate)
	VALUES	(1,
		3,
		'5051732',
		'mBNCjEWaSg4',
		@MemberID,
		GETDATE())
	
	INSERT
	INTO	AutoCheck.Account_ServiceType 
		(ServiceTypeID,
		AccountID,
		InsertUser,
		InsertDate)
	SELECT	1,
		A.AccountID,
		@MemberID,
		GETDATE()
	FROM	IMT.AutoCheck.Account A
	WHERE	UserName ='5051732'  

END	

/**************************************************************
*
* Add AutoCheck Service Accounts    - HTML ServiceTypeID =2
*
***************************************************************/
 
IF NOT EXISTS (SELECT 1
		FROM 	AutoCheck.Account A
		JOIN	AutoCheck.Account_ServiceType ST
		ON	A.AccountID =ST.AccountID
		WHERE 	A.AccountTypeID =3
		AND	ST.ServiceTypeid=2) 
BEGIN		
	INSERT
	INTO 	AutoCheck.Account 
		(AccountStatusID,
		AccountTypeID,
		UserName,
		password,
		InsertUser,
		InsertDate)
	VALUES	(1,
		3,
		'5050306',
		'FLC2005',
		@MemberID,
		GETDATE())
	
	INSERT
	INTO	AutoCheck.Account_ServiceType 
		(ServiceTypeID,
		AccountID,
		InsertUser,
		InsertDate)
	SELECT	2,
		A.AccountID,
		@MemberID,
		GETDATE()
	FROM	IMT.AutoCheck.Account A
	WHERE	UserName ='5050306'  

END	

GO