--modify column width
ALTER TABLE imt.dbo.lu_states ALTER COLUMN statelong VARCHAR(20)
--new record
INSERT INTO imt.dbo.lu_states (stateid, stateshort, statelong)
SELECT 103, 'DC', 'District of Columbia'
