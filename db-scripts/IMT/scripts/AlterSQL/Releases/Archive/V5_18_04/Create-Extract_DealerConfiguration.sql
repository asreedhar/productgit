CREATE SCHEMA [Extract]
GO

CREATE TABLE [Extract].[Destination] (
	[Name] VARCHAR(100) NOT NULL,
	CONSTRAINT [PK_Extract_Destination] PRIMARY KEY NONCLUSTERED ([Name])
)
GO

INSERT INTO [IMT].[Extract].[Destination]([Name])
VALUES('GMAC')
GO

INSERT INTO [IMT].[Extract].[Destination]([Name])
VALUES('Edmunds')
GO

CREATE TABLE [Extract].[DealerConfiguration] (
	BusinessUnitID INT NOT NULL,
	DestinationName VARCHAR(100) NOT NULL,
	ExternalIdentifier VARCHAR(30) NOT NULL,
	StartDate SMALLDATETIME NOT NULL,
	EndDate SMALLDATETIME NULL,
	Active BIT NOT NULL CONSTRAINT [DF_Extract_DealerConfiguration__IsActive] DEFAULT 0,
	EffectiveDateActive AS CONVERT([tinyint],
		CASE WHEN getdate() BETWEEN [StartDate] AND [EndDate] THEN 1
			 WHEN getdate() > [StartDate] AND [EndDate] IS NULL THEN 1
			 ELSE 0 
		END & [Active],0),
	IncludeHistorical BIT NOT NULL CONSTRAINT DF_Extract_DealerConfiguration__IncludeHistorical DEFAULT 0,
	InsertDate DATETIME NOT NULL CONSTRAINT DF_Extract_DealerConfiguration__InsertDate DEFAULT getdate(),
	InsertUser VARCHAR(100) NOT NULL CONSTRAINT DF_Extract_DealerConfiguration__InsertUser DEFAULT system_user,
	UpdateDate DATETIME NOT NULL CONSTRAINT DF_Extract_DealerConfiguration__UpdateDate DEFAULT getdate(),
	UpdateUser VARCHAR(100) NOT NULL CONSTRAINT DF_Extract_DealerConfiguration__UpdateUser DEFAULT system_user,
	CONSTRAINT [PK_Extract_DealerConfiguration__BusinessUnitIDDestinationName] PRIMARY KEY (
		BusinessUnitID ASC, DestinationName ASC
	), 
	CONSTRAINT [FK_Extract_DealerConfiguration__BusinessUnitId] FOREIGN KEY (BusinessUnitID)
		REFERENCES [dbo].[BusinessUnit] (BusinessUnitID),
	CONSTRAINT [FK_Extract_DealerConfiguration__DestinationName] FOREIGN KEY (DestinationName)
		REFERENCES [Extract].[Destination] ([Name])
)
GO

CREATE UNIQUE INDEX [UQ_Extract_DealerConfiguration__DestinationNameExternalIdentifier] 
ON [Extract].[DealerConfiguration] (DestinationName, ExternalIdentifier)
GO