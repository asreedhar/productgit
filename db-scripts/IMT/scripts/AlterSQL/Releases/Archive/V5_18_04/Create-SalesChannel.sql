CREATE TABLE dbo.SalesChannel (
	SalesChannelID int IDENTITY(1,1) NOT NULL,
	[Name] varchar(50) NOT NULL,
	CONSTRAINT [PK_SalesChannel] PRIMARY KEY CLUSTERED (
		SalesChannelID ASC
	),
	CONSTRAINT [UK_SalesChannel_Name] UNIQUE NONCLUSTERED (
		[Name]
	)
)
GO

EXEC sp_addextendedproperty 
@name = N'MS_Description',
@value = N'Channels that sell the FirstLook product.',
@level0type = N'Schema', @level0name = dbo,
@level1type = N'Table', @level1name = SalesChannel
GO

INSERT INTO [IMT].[dbo].[SalesChannel] ([Name])
VALUES ('FirstLook Sales Team')
GO

INSERT INTO [IMT].[dbo].[SalesChannel] ([Name])
VALUES ('GMAC Sales Team')
GO

CREATE TABLE dbo.DealerSalesChannel (
	BusinessUnitID int NOT NULL,
	SalesChannelID int NOT NULL,
	CONSTRAINT [PK_DealerSalesChannel] PRIMARY KEY (
		BusinessUnitID ASC
	),
	CONSTRAINT [FK_DealerSalesChannel_BusinessUnitID] FOREIGN KEY (
		BusinessUnitID
	) REFERENCES dbo.BusinessUnit(BusinessUnitID),
	CONSTRAINT [FK_DealerSalesChannel_SalesChannelID] FOREIGN KEY (
		SalesChannelID
	) REFERENCES dbo.SalesChannel(SalesChannelID)
)
GO

CREATE NONCLUSTERED INDEX [IX_DealerSalesChannel_SalesChannelID]
ON [dbo].[DealerSalesChannel] (SalesChannelID)
GO

EXEC sp_addextendedproperty 
@name = N'MS_Description',
@value = N'Indicates which SalesChannel lead to the creation of the Dealership.',
@level0type = N'Schema', @level0name = dbo,
@level1type = N'Table', @level1name = DealerSalesChannel
GO