/** JRC 05/ 21/2009 Initial load of AutoCheck Schema Reference tables. **/

/********************************************************************************************
* 1. Populate Reference tables with CHAR(3) primary keys.
*	a. ResponseCode
*	b. ReportType
**********************************************************************************************/

/* 1a. ResponseCode */

	DELETE FROM  AutoCheck.ResponseCode
	GO

	INSERT
	INTO	AutoCheck.ResponseCode
			(ResponseCode,
			Description,
			IsError,
			IsTryAgain)
	VALUES
			('200',
			'Successful HTTP request.',
			0,
			0)
	GO
	
	INSERT
	INTO	AutoCheck.ResponseCode
			(ResponseCode,
			Description,
			IsError,
			IsTryAgain)
	VALUES
			('403',
			'The request was a legal request, but the server is refusing to respond to it.',
			1,
			0)
	GO

	INSERT
	INTO	AutoCheck.ResponseCode
			(ResponseCode,
			Description,
			IsError,
			IsTryAgain)
	VALUES
			('500',
			'Internal Server error.',
			1,
			0)
	GO



/* 1b. ReportType */

	DELETE FROM  AutoCheck.ReportType
	GO

	INSERT
	INTO	AutoCheck.ReportType
			(ReportTypeID,
			Name)
	VALUES
			(5, 'Full')
	GO

	INSERT
	INTO	AutoCheck.ReportType
			(ReportTypeID,
			Name)
	VALUES
			(3, 'Summary')
	GO

	INSERT
	INTO	AutoCheck.ReportType
			(ReportTypeID,
			Name)
	VALUES
			(2, 'Lemon')
	GO



	INSERT
	INTO	AutoCheck.ReportType
			(ReportTypeID,
			Name)
	VALUES
			(1,'Check')
	GO



	INSERT
	INTO	AutoCheck.ReportType
			(ReportTypeID,
			Name)
	VALUES
			(4, 'Indicators')
	GO

/********************************************************************************************
* 2. Populate Reference tables with TINYINT primary keys.
*	a. AccountStatus
*	b. AccountType
*	c. ReportInspection ** Description instead of Name.
*   d. ServiceType
**********************************************************************************************/

/* 2a.  AccountStatus */

	DELETE FROM  AutoCheck.AccountStatus
	GO
	
	INSERT
	INTO	AutoCheck.AccountStatus
			(AccountStatusID,
			Name)
	VALUES
			(1,
			'New')
	GO

	INSERT
	INTO	AutoCheck.AccountStatus
			(AccountStatusID,
			Name)
	VALUES
			(2,
			'Okay')
	GO

	INSERT
	INTO	AutoCheck.AccountStatus
			(AccountStatusID,
			Name)
	VALUES
			(3,
			'Bad User Name/Password')
	GO

/* 2b.  AccountType */

	DELETE FROM  AutoCheck.AccountType
	GO

	INSERT
	INTO	AutoCheck.AccountType
			(AccountTypeID,
			Name)
	VALUES
			(1,
			'Dealer')	
	GO

	INSERT
	INTO	AutoCheck.AccountType
			(AccountTypeID,
			Name)
	VALUES
			(2,
			'System')
	GO


	INSERT
	INTO	AutoCheck.AccountType
			(AccountTypeID,
			Name)
	VALUES
			(3,
			'Service')
	GO

/* 2c. ReportInspection */

   /* NOTE: Unlike Carfax a ReportInspectionResult is Selected when the
    * XPath returns no nodes.  As a result, the XPath queries find bad
    * things and the result is selected in their absence. */

	INSERT
	INTO	[IMT].AutoCheck.ReportInspection
		(ReportInspectionId, Description, XPath)
	VALUES	(1, '1-Owner', '/VHR/VEHICLE/SCORE[@OWNER_COUNT!=''1'']')
	
	INSERT
	INTO	[IMT].AutoCheck.ReportInspection
		(ReportInspectionId, Description, XPath)
	VALUES	(2, 'Assured', '/VHR/VEHICLE/HISTORY[@ASSURED=''N'' or not(@ASSURED)]')

	INSERT
	INTO	[IMT].AutoCheck.ReportInspection
		(ReportInspectionId, Description, XPath)
	VALUES	(3, 'No Total Loss Reported', '/VHR/VEHICLE/HISTORY/HDATA[@CATEGORY=''Insurance Loss'']')

	INSERT
	INTO	[IMT].AutoCheck.ReportInspection
		(ReportInspectionId, Description, XPath)
	VALUES	(4, 'No Frame Damage Reported', '/VHR/VEHICLE/HISTORY/HDATA[@CATEGORY=''Frame Damage'']')

	INSERT
	INTO	[IMT].AutoCheck.ReportInspection
		(ReportInspectionId, Description, XPath)
	VALUES	(5, 'No Airbag Deployment Reported', '/VHR/VEHICLE/HISTORY/HDATA[@ACTCODE=''9510'' or @ACTCODE=''9520'']')

	INSERT
	INTO	[IMT].AutoCheck.ReportInspection
		(ReportInspectionId, Description, XPath)
	VALUES	(6, 'No Odometer Rollback Reported', '/VHR/VEHICLE/HISTORY[@ROLLBACK=''Y'']')

	INSERT
	INTO	[IMT].AutoCheck.ReportInspection
		(ReportInspectionId, Description, XPath)
	VALUES	(7, 'No Accidents / Damage Reported', '/VHR/VEHICLE/HISTORY/HDATA[@CATEGORY=''Accident Data'' or @CATEGORY=''Title Damage'' or @CATEGORY=''Fire Damage'' or @CATEGORY=''Hail Damage'' or @CATEGORY=''Water Damage'' or @CATEGORY=''Frame Damage'' or @CATEGORY=''Major Damage Incident'']')
	
	INSERT
	INTO	[IMT].AutoCheck.ReportInspection
		(ReportInspectionId, Description, XPath)
	VALUES	(8, 'No Manufacturer Recalls Reported', '/VHR/VEHICLE/HISTORY/HDATA[@CATEGORY=''Lemon'']')

/* 2d. ServiceType */

	INSERT INTO [IMT].AutoCheck.ServiceType (ServiceTypeID, Name) VALUES (1, 'XML')
	INSERT INTO [IMT].AutoCheck.ServiceType (ServiceTypeID, Name) VALUES (2, 'HTML')
