--  Configure the ADPImage provider
DECLARE
  @PhotoProviderID  int

INSERT ThirdPartyEntity
  (
    BusinessUnitID
   ,ThirdPartyEntityTypeID
   ,Name
  )
 values
  (
    100150  --  Firstlook "default" business unit id
   ,6       --  Photo Provider ID
   ,'ADP'
  )


--  Get ID just generated
SET @PhotoProviderID = scope_identity()

INSERT PhotoProvider_ThirdPartyEntity
  (
    PhotoProviderID
   ,DatafeedCode
   ,PhotoStorageCode
 )
 values
  (
    @PhotoProviderID
   ,'ADPImage'
   ,2  --  Retrieve images and store locally
 )

INSERT
INTO	dbo.PhotoProviderDealership (
	PhotoProviderID,
	BusinessUnitID,
	PhotoProvidersDealershipCode,
	PhotoStorageCode_Override
) VALUES ( 
	/* PhotoProviderID - int */ @PhotoProviderID,
	/* BusinessUnitID - int */ 101445,
	/* PhotoProvidersDealershipCode - varchar(50) */ 'akancbmw',
	/* PhotoStorageCode_Override - tinyint */ 1 ) 
	
GO	