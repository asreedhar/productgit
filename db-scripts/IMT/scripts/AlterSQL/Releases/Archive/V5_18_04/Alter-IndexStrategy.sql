ALTER TABLE tbl_MemberATCAccessGroup DROP CONSTRAINT FK_MemberATCAccessGroup_MemberATCAccessGroupList

ALTER TABLE tbl_MemberATCAccessGroupList DROP CONSTRAINT PK_MemberATCAccessGroupList

ALTER TABLE tbl_MemberATCAccessGroupList ADD CONSTRAINT PK_MemberATCAccessGroupList PRIMARY KEY CLUSTERED (MemberATCAccessGroupListID)


ALTER TABLE tbl_MemberATCAccessGroup ADD CONSTRAINT FK_MemberATCAccessGroup_MemberATCAccessGroupList FOREIGN KEY (
		MemberATCAccessGroupListID
	)
	REFERENCES tbl_MemberATCAccessGroupList (
		MemberATCAccessGroupListID
	)
GO	