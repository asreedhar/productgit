
/* MAK Updatd so that migration is  Inventory only per 5985. */


DECLARE @CarFaxAdmin INT

	SELECT @CarFaxAdmin =MemberID
	FROM	dbo.Member 
	WHERE	Login  ='CarfaxSystemUserAdmin'

INSERT
INTO	Carfax.Vehicle
	(VIN,
	IsHotListEligible,
	InsertUser,
	InsertDate)
SELECT	CR.VIN,
	0,
	@CarFaxAdmin,
	GETDATE()
FROM	dbo.CarfaxReport CR
LEFT 
JOIN	Carfax.Vehicle V
ON	CR.VIN =V.VIN
WHERE	CR.Expires >=GETDATE()
AND	V.VIN IS NULL

GO

DECLARE @ReportProcessorCommandTypeID TINYINT
DECLARE @ReportProcessorCommandStatusID TINYINT
 

SET	@ReportProcessorCommandTypeID =5 --- Migrate Data 
SET	@ReportProcessorCommandStatusID =1 -- Ready
 	
 	
 DECLARE @CarFaxAdmin INT

SELECT @CarFaxAdmin =MemberID
FROM	dbo.Member 
WHERE	Login  ='CarfaxSystemUserAdmin'
 	 
INSERT
INTO	Carfax.ReportProcessorCommand
	(DealerId,
	VehicleID,
	VehicleEntityTypeID,
	ReportProcessorCommandTypeID,
	ReportProcessorCommandStatusID,
	ProcessAfterDate,
	InsertUser,
	InsertDate)
SELECT	CR.BusinessUnitID,
	V.VehicleID,
	1, --PER 5985, this is always Inventory 1
	@ReportProcessorCommandTypeID,
	@ReportProcessorCommandStatusID,
	GETDATE(),
 	@CarFaxAdmin  ,
	GETDATE()

FROM	dbo.CarfaxReport CR
JOIN	Carfax.Vehicle V
ON	CR.VIN =V.VIN
LEFT
JOIN	dbo.Vehicle VV
on	V.VIN =VV.VIN
LEFT
JOIN	dbo.Inventory I
ON	VV.VehicleID =I.VehicleID
AND	CR.BusinessUnitID =I.BusinessUnitID
WHERE	CR.Expires >=GETDATE()
AND	 I.InventoryID is not Null
 
GO

INSERT
INTO	Carfax.ReportProcessorCommand_DataMigration
	(ReportProcessorCommandID,
	ReportType,
	IsHotListed)
SELECT	RPC.ReportProcessorCommandID,
	CR.ReportType,
	1
FROM	Carfax.ReportProcessorCommand RPC
JOIN	dbo.CarfaxReport CR
ON	RPC.DealerID =CR.BusinessUnitID
JOIN	Carfax.Vehicle V
ON	V.VehicleID =RPC.VehicleID
AND	V.VIN =CR.VIN
WHERE	RPC.ReportProcessorCommandTypeID =5

GO