
DROP TABLE IMT.dbo.CarfaxProcessorErrorDetail
GO
DROP TABLE IMT.dbo.CarfaxProcessorRunLog
GO
DROP TABLE IMT.dbo.CarfaxReport
GO
DROP TABLE IMT.dbo.CarfaxRestrictedMembers
GO
DROP TABLE IMT.dbo.DealerPreference_Carfax
GO
DROP TABLE IMT.dbo.CarfaxProcessorStatus
GO
DROP TABLE IMT.dbo.CarfaxReportType
GO

DROP PROCEDURE Carfax.LoadCarFaxProcessorRunLog
GO
DROP PROCEDURE Carfax.Dealer#UpdateRunLog
GO
DROP PROCEDURE Carfax.Report#LogError
GO
DROP PROCEDURE Carfax.Report#SaveOrUpdate
GO
DROP PROCEDURE dbo.MemberCanUseDealerLevelCarFaxCredentials
GO
DROP PROCEDURE dbo.RestrictMemberFromUsingCarFaxCredentials
GO

/*DELETE FROM IMT.dbo.CredentialType WHERE CredentialTypeID = 1*/
GO