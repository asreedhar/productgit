SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DealerPreference_KBBConsumerTool](
      [BusinessUnitID] [int] NOT NULL,
      [ShowRetail] [bit] NOT NULL CONSTRAINT DF_DealerPreference_KBBConsumerTool__ShowRetail DEFAULT ((0)),
      [ShowTradeIn] [bit] NOT NULL CONSTRAINT DF_DealerPreference_KBBConsumerTool__ShowTradeIn DEFAULT ((1))
 CONSTRAINT [PK_DealerPreference_KBBConsumerTool] PRIMARY KEY CLUSTERED 
(
      [BusinessUnitID] ASC
)WITH (IGNORE_DUP_KEY = OFF)
) 

GO

ALTER TABLE [dbo].[DealerPreference_KBBConsumerTool]  WITH CHECK ADD  CONSTRAINT [PK_DealerPreference_KBBConsumerTool__BusinessUnitId] FOREIGN KEY([BusinessUnitID])
REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])

GO

