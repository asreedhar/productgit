/* Migrate Account Data from DealerPreference_Carfax*/



--TRUNCATE TABLE Carfax.ReportPreference
--GO

DECLARE @CarFaxAdmin INT

SELECT @CarFaxAdmin =MemberID
FROM	dbo.Member 
WHERE	Login  ='CarfaxSystemUserAdmin'

INSERT
INTO	 Carfax.ReportPreference
	(DealerID,
	VehicleEntityTypeID,
	AutoPurchase,
	DisplayInHotList,
	ReportType,
	InsertUser,
	InsertDate)
SELECT BusinessUnitID,
	1, --Inventory First,
	AutoRunInventory,
	1,
	DefaultInventoryreportType,
	@CarFaxAdmin,
	GETDATE() 
FROM   dbo.DealerPreference_Carfax
WHERE	LEN(CarfaxUserName) >0
AND	LEN(CarfaxPassword) >0

GO

DECLARE @CarFaxAdmin INT

SELECT @CarFaxAdmin =MemberID
FROM	dbo.Member 
WHERE	Login  ='CarfaxSystemUserAdmin'
INSERT
INTO	 Carfax.ReportPreference
	(DealerID,
	VehicleEntityTypeID,
	AutoPurchase,
	DisplayInHotList,
	ReportType,
	InsertUser,	
	InsertDate)
SELECT	BusinessUnitID,
	2, --Appraisal First,
	AutoRunAppraisal,
	0,
	DefaultAppraisalReportType,
	@CarFaxAdmin,
	GETDATE() 
FROM	dbo.DealerPreference_Carfax
WHERE	LEN(CarfaxUserName) >0
AND	LEN(CarfaxPassword) >0

GO