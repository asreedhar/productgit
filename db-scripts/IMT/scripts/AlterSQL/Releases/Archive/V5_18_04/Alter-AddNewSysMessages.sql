IF EXISTS (SELECT 1
	FROM	Sys.messages 
	WHERE	message_id =50111)
BEGIN
	EXEC sp_addmessage @msgnum = 50111, @severity = 16, 
	   @msgtext = N'Data Error!  %s must be between %d and %d characters', 
	   @lang = 'us_english', @replace =  'replace' 
END
ELSE
BEGIN
	EXEC sp_addmessage @msgnum = 50111, @severity = 16, 
	   @msgtext = N'Data Error!  %s must be between %d and %d characters', 
	   @lang = 'us_english' 
END
GO

IF EXISTS (SELECT 1
	FROM	Sys.messages 
	WHERE	message_id =50112)
BEGIN
	EXEC sp_addmessage @msgnum = 50112, @severity = 16, 
	   @msgtext = N'That account is already associated with a dealer.', 
	   @lang = 'us_english', @replace =  'replace' 
END
ELSE
BEGIN
	EXEC sp_addmessage @msgnum = 50112, @severity = 16, 
	   @msgtext = N'That account is already associated with a dealer.', 
	   @lang = 'us_english' 
END
GO

IF EXISTS (SELECT 1
	FROM	Sys.messages 
	WHERE	message_id =50113)
BEGIN
	EXEC sp_addmessage @msgnum = 50113, @severity = 16, 
	   @msgtext = N'The record exists in the %s table.', 
	   @lang = 'us_english', @replace =  'replace' 
END
ELSE
BEGIN
	EXEC sp_addmessage @msgnum = 50113, @severity = 16, 
	   @msgtext = N'The record exists in the %s table.', 
	   @lang = 'us_english' 
END
GO

IF EXISTS (SELECT 1
	FROM	Sys.messages 
	WHERE	message_id =50114)
BEGIN
	EXEC sp_addmessage @msgnum = 50114, @severity = 16, 
	   @msgtext = N'The VIN %s is not 17 characters long.', 
	   @lang = 'us_english', @replace =  'replace' 
END
ELSE
BEGIN
	EXEC sp_addmessage @msgnum = 50114, @severity = 16, 
	   @msgtext = N'The VIN %s is not 17 characters long.', 
	   @lang = 'us_english' 
END
GO

IF EXISTS (SELECT 1
	FROM	Sys.messages 
	WHERE	message_id =50115)
BEGIN
	EXEC sp_addmessage @msgnum = 50115, @severity = 16, 
	   @msgtext = N'Row has wrong version in %s table.', 
	   @lang = 'us_english', @replace =  'replace' 
END
ELSE
BEGIN
	EXEC sp_addmessage @msgnum = 50115, @severity = 16, 
	   @msgtext = N'Row has wrong version in %s table.', 
	   @lang = 'us_english' 
END
GO

IF EXISTS (SELECT 1
	FROM	Sys.messages 
	WHERE	message_id =50116)
BEGIN
	EXEC sp_addmessage @msgnum = 50116, @severity = 16, 
	   @msgtext = N'The Response for Request does not have a success code (500 or 501).', 
	   @lang = 'us_english', @replace =  'replace' 
END
ELSE
BEGIN
	EXEC sp_addmessage @msgnum = 50116, @severity = 16, 
	   @msgtext = N'The Response for Request does not have a success code (500 or 501).', 
	   @lang = 'us_english' 
END
GO

IF EXISTS (SELECT 1
	FROM	Sys.messages 
	WHERE	message_id =50117)
BEGIN
	EXEC sp_addmessage @msgnum = 50117, @severity = 16, 
	   @msgtext = N'OwnerCount cannot be < 0.', 
	   @lang = 'us_english', @replace =  'replace' 
END
ELSE
BEGIN
	EXEC sp_addmessage @msgnum = 50117, @severity = 16, 
	   @msgtext = N'OwnerCount cannot be < 0.', 
	   @lang = 'us_english' 
END
GO

IF EXISTS (SELECT 1
	FROM	Sys.messages 
	WHERE	message_id =50118)
BEGIN
	EXEC sp_addmessage @msgnum = 50118, @severity = 16, 
	   @msgtext = N'ExpirationDate cannot be in the past.', 
	   @lang = 'us_english', @replace =  'replace' 
END
ELSE
BEGIN
	EXEC sp_addmessage @msgnum = 50118, @severity = 16, 
	   @msgtext = N'ExpirationDate cannot be in the past.',  
	   @lang = 'us_english' 
END
GO