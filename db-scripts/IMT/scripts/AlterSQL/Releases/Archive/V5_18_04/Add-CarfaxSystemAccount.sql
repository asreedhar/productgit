/***********************************************************************
*
*  MAK 	06/30/2009	Add  System Account  for AutoCheck.
*
************************************************************************/

DECLARE @MemberID INT
SELECT	@MemberID = MemberID
FROM	[IMT].dbo.Member
WHERE	Login ='CarfaxSystemUserAdmin'


/* Add System Carfax System Account*/

IF NOT EXISTS (	SELECT 1
		FROM	Carfax.Account
		WHERE	AccountTypeID =2)

BEGIN

	INSERT
	INTO	Carfax.Account
		(AccountStatusID,
		AccountTypeID,
		UserName,
		Password,
		InsertUser,
		InsertDate)
	VALUES	(1,
		2,
		'C413833',
		'13651',
		@MemberID,
		GETDATE())
END

GO