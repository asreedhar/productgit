/** MAK 05/ 21/2009 Initial load of Carfax Schema Reference tables. **/

/********************************************************************************************
* 1. Populate Reference tables with CHAR(3) primary keys.
*	a. RequestType
*	b. ResponseCode
*	c. TrackingCode
*	d. WindowSticker
*	e. ReportType
**********************************************************************************************/

/* 1a. RequestType */

	DELETE FROM  [IMT].Carfax.RequestType
	GO

	INSERT
	INTO	[IMT].Carfax.RequestType
			(RequestType,
			Description)
	VALUES
			('IM2',
			'CARFAX Inventory Manager')
	GO

/* 1b. ResponseCode */

	DELETE FROM  Carfax.ResponseCode
	GO

	INSERT
	INTO	[IMT].Carfax.ResponseCode
			(ResponseCode,
			Description,
			IsError,
			IsTryAgain)
	VALUES
			('500',
			'A CARFAX Report is available in the Dealer''s Inventory Manager.  
			CARFAX found at least one record for the specified VIN and the vehicle 
			is eligible to be listed in CARFAX Hot Listings.  The vehicle does not
			have a title brand or odometer rollback.',
			0,
			0)
	GO
	
	INSERT
	INTO	[IMT].Carfax.ResponseCode
			(ResponseCode,
			Description,
			IsError,
			IsTryAgain)
	VALUES
			('501',
			'A CARFAX Report is available in the Dealer''s Inventory Manager.  
			CARFAX found at least one record for the specified VIN and the vehicle 
			is not eligible to be listed in CARFAX Hot Listings because the vehicle 
			has a title brand or odometer rollback.',
			0,
			0)
	GO

	INSERT
	INTO	[IMT].Carfax.ResponseCode
			(ResponseCode,
			Description,
			IsError,
			IsTryAgain)
	VALUES
			('600',
			'VIN Does not exist in Inventory Manager OR No data available: The VIN
			does not exist in the Dealer''s Inventory Manager OR CARFAX did
			not find any records from the specified VIN: however, the VIN otherwise
			appears valid.',
			1,
			0)
	GO
	
	INSERT
	INTO	[IMT].Carfax.ResponseCode
			(ResponseCode,
			Description,
			IsError,
			IsTryAgain)
	VALUES
			('800',
			'Invalid VIN: The Server sends this message to indicate that the VIN
			supplied by the user was invalid for some reason.',
			1,
			0)
	GO	
		
	INSERT
	INTO	[IMT].Carfax.ResponseCode
			(ResponseCode,
			Description,
			IsError,
			IsTryAgain)
	VALUES
			('900',
			'Service Unavailable: The Server sends this message to indicate that the
			CARFAX Vehicle History Service is not available to process vehicle
			queries for some reason.',
			1,
			1	)
	GO

	INSERT
	INTO	[IMT].Carfax.ResponseCode
			(ResponseCode,
			Description,
			IsError,
			IsTryAgain)
	VALUES
			('901',
			'Transaction Error: The Server sends this message to indicate there was
			an error in the last message it received.  The Client can choose to resend
			the message packet.',
			1,
			1)
	GO

	INSERT
	INTO	[IMT].Carfax.ResponseCode
			(ResponseCode,
			Description,
			IsError,
			IsTryAgain)
	VALUES
			('903',
			'Account Status:  The Server sends this message to indicate that the )
			CARFAX Vehicle History Service has denied acces to the supplied User
			ID for non-technical reasons.  For example, the user may have cancelled
			the account or there may be billing issues requiring CARFAX to put the
			account on hold.',
			1,
			0)
	GO

	INSERT
	INTO	[IMT].Carfax.ResponseCode
			(ResponseCode,
			Description,
			IsError,
			IsTryAgain)
	VALUES
			('904',
			'Security Violation:  Used when the user name cannot be authenticated.',
			1,
			0)
	GO

	INSERT
	INTO	[IMT].Carfax.ResponseCode
			(ResponseCode,
			Description,
			IsError,
			IsTryAgain)
	VALUES
			('905',
			'Security Violation:  Used when the password cannot be authenticated.',
			1,
			0)
	GO

	INSERT
	INTO	[IMT].Carfax.ResponseCode
			(ResponseCode,
			Description,
			IsError,
			IsTryAgain)
	VALUES
			('906',
			'No Code Supplied:  Used when a code is not included as a part of the
			socket request to CARFAX or whn the CODE supplied is not valid.',
			1,
			0)
	GO

/* 1c.  TrackingCode */

	DELETE FROM  [IMT].Carfax.TrackingCode
	GO

	INSERT
	INTO	[IMT].Carfax.TrackingCode
			(TrackingCode,
			Description)
	VALUES	('FLA',
			'Auto Run Request.')
	GO

	INSERT
	INTO	[IMT].Carfax.TrackingCode
			(TrackingCode,
			Description)
	VALUES	('FLN',
			'User Driven Application Request.')
	GO
	

/* 1d. WindowSticker */

	DELETE FROM  [IMT].Carfax.WindowSticker
	GO

	INSERT
	INTO	[IMT].Carfax.WindowSticker
			(WindowSticker,
			Description)
	VALUES
			('WS1',
			'Default Window Sticker Type.')
	GO

/* 1e. ReportType */

	DELETE FROM  [IMT].Carfax.ReportType
	GO

	INSERT
	INTO	[IMT].Carfax.ReportType
			(ReportType,
			Name)
	VALUES
			('CIP',
			'Consumer Information Packet.')
	GO

	INSERT
	INTO	[IMT].Carfax.ReportType
			(ReportType,
			Name)
	VALUES
			('VHR',
			'Vehicle History Report.')
	GO

	INSERT
	INTO	[IMT].Carfax.ReportType
			(ReportType,
			Name)
	VALUES
			('BTC',
			'Branded Title Check.')
	GO

/********************************************************************************************
* 2. Populate Reference tables with TINYINT primary keys.
*	a. AccountStatus
*	b. AccountType
*	c. ReportInspection ** Description instead of Name.
*	d. VehicleEntityType
*	e. ReportProcesssorCommandStatus
*	f. ReportProcesssorCommandType
*	g. Cancelled
**********************************************************************************************/

/* 2a.  AccountStatus */

	DELETE FROM  [IMT].Carfax.AccountStatus
	GO
	
	INSERT
	INTO	[IMT].Carfax.AccountStatus
			(AccountStatusID,
			Name)
	VALUES
			(1,
			'New')
	GO

	INSERT
	INTO	[IMT].Carfax.AccountStatus
			(AccountStatusID,
			Name)
	VALUES
			(2,
			'Okay')
	GO

	INSERT
	INTO	[IMT].Carfax.AccountStatus
			(AccountStatusID,
			Name)
	VALUES
			(3,
			'Bad User Name')
	GO

	INSERT
	INTO	[IMT].Carfax.AccountStatus
			(AccountStatusID,
			Name)
	VALUES
			(4,
			'Bad User Password')
	GO

	INSERT
	INTO	[IMT].Carfax.AccountStatus
			(AccountStatusID,
			Name)
	VALUES
			(5,
			'Account Disabled')
	GO

/* 2b.  AccountType */

	DELETE FROM  [IMT].Carfax.AccountType
	GO

	INSERT
	INTO	[IMT].Carfax.AccountType
			(AccountTypeID,
			Name)
	VALUES
			(1,
			'Dealership')	
	GO

	INSERT
	INTO	[IMT].Carfax.AccountType
			(AccountTypeID,
			Name)
	VALUES
			(2,
			'Member')
	GO

/* 2c. ReportInspection */

	DELETE FROM  [IMT].Carfax.VehicleEntityType
	GO
	
	INSERT
	INTO	[IMT].Carfax.ReportInspection
		(ReportInspectionId, Description, XPath)
	VALUES	(1, '1-Owner', '/carfaxInventoryManager/WindowSticker/Ownership/OwnershipText[text()=''CARFAX 1-Owner'']')

	INSERT
	INTO	[IMT].Carfax.ReportInspection
		(ReportInspectionId, Description, XPath)
	VALUES(2, 'Buy Back Guarantee', '/carfaxInventoryManager/WindowSticker/BBG/BBGText[text()=''Vehicle Qualifies for the CARFAX Buyback Guarantee'']')

	INSERT
	INTO	[IMT].Carfax.ReportInspection
		(ReportInspectionId, Description, XPath)
	VALUES (3, 'No Total Loss Reported', '/carfaxInventoryManager/WindowSticker/TotalLoss/TotalLossText[text()=''No Total Loss Reported to CARFAX'']')

	INSERT
	INTO	[IMT].Carfax.ReportInspection
		(ReportInspectionId, Description, XPath)
	VALUES (4, 'No Frame Damage Reported', '/carfaxInventoryManager/WindowSticker/FrameDamage/FrameDamageText[text()=''No Structural/Frame Damage Reported to CARFAX'']')

	INSERT
	INTO	[IMT].Carfax.ReportInspection
		(ReportInspectionId, Description, XPath)
	VALUES  (5, 'No Airbag Deployment Reported', '/carfaxInventoryManager/WindowSticker/AirbagDeployment/AirbagDeploymentText[text()=''No Airbag Deployment Reported to CARFAX'']')

	INSERT
	INTO	[IMT].Carfax.ReportInspection
		(ReportInspectionId, Description, XPath)
	VALUES (6, 'No Odometer Rollback Reported', '/carfaxInventoryManager/WindowSticker/OdometerRollback/OdometerRollbackText[text()=''No Indication of an Odometer Rollback'']')

	INSERT
	INTO	[IMT].Carfax.ReportInspection
		(ReportInspectionId, Description, XPath)
	VALUES (7, 'No Accidents / Damage Reported', '/carfaxInventoryManager/WindowSticker/AccidentIndicators/AccidentIndicatorsText[text()=''No Accidents / Damage Reported to CARFAX'']')

	INSERT
	INTO	[IMT].Carfax.ReportInspection
		(ReportInspectionId, Description, XPath)
	VALUES  (8, 'No Manufacturer Recalls Reported', '/carfaxInventoryManager/WindowSticker/ManufacturerRecall/ManufacturerRecallText[text()=''No Manufacturer Recalls Reported to CARFAX'']')


/* 2d. VehicleEntityType */
	
	DELETE FROM  [IMT].Carfax.VehicleEntityType
	GO

	INSERT
	INTO	[IMT].Carfax.VehicleEntityType
			(VehicleEntityTypeID,
			Name)
	VALUES
			(1,
			'Inventory')
	GO

	INSERT
	INTO	[IMT].Carfax.VehicleEntityType
			(VehicleEntityTypeID,
			Name)
	VALUES
			(2,
			'Appraisal')
	GO
	
/* 2e. ReportProcessorCommandStatus */

	DELETE FROM  [IMT].Carfax.ReportProcessorCommandStatus
	GO

	INSERT 
	INTO	[IMT].Carfax.ReportProcessorCommandStatus
			(ReportProcessorCommandStatusID,
			Name)
	VALUES	
			(1,
			'Ready')
	GO

	INSERT 
	INTO	[IMT].Carfax.ReportProcessorCommandStatus
			(ReportProcessorCommandStatusID,
			Name)
	VALUES	
			(2,
			'In Progress')
	GO

	INSERT 
	INTO	[IMT].Carfax.ReportProcessorCommandStatus
			(ReportProcessorCommandStatusID,
			Name)
	VALUES	
			(3,
			'Completed')
	GO

	INSERT 
	INTO	[IMT].Carfax.ReportProcessorCommandStatus
			(ReportProcessorCommandStatusID,
			Name)
	VALUES	
			(4,
			'Error')
	GO

	INSERT 
	INTO	[IMT].Carfax.ReportProcessorCommandStatus
			(ReportProcessorCommandStatusID,
			Name)
	VALUES	
			(5,
			'Account Problem')
	GO
	
	INSERT 
	INTO	[IMT].Carfax.ReportProcessorCommandStatus
			(ReportProcessorCommandStatusID,
			Name)
	VALUES	
			(6,
			'Cancelled')
	GO

/* 2f. ReportProcessorCommandType */

	DELETE FROM  [IMT].Carfax.ReportProcessorCommandType
	GO

	INSERT
	INTO	[IMT].Carfax.ReportProcessorCommandType
			(ReportProcessorCommandTypeID,
			Name)
	VALUES	(1,
			'Purchase Default Report')
	GO

	INSERT
	INTO	[IMT].Carfax.ReportProcessorCommandType
			(ReportProcessorCommandTypeID,
			Name)
	VALUES	(2,
			'Renew Report')

	GO
		INSERT
	INTO	[IMT].Carfax.ReportProcessorCommandType
			(ReportProcessorCommandTypeID,
			Name)
	VALUES	(3,
			'Upgrade Report')
	GO

	INSERT
	INTO	[IMT].Carfax.ReportProcessorCommandType
			(ReportProcessorCommandTypeID,
			Name)
	VALUES	(4,
			'Change HotList Status')

	GO
	
	
	INSERT
	INTO	[IMT].Carfax.ReportProcessorCommandType
			(ReportProcessorCommandTypeID,
			Name)
	VALUES	(5,
			'Migrate Data')

	GO

	


