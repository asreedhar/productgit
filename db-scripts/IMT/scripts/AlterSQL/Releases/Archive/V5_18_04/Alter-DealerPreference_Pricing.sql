
ALTER TABLE dbo.DealerPreference_Pricing ADD MatchCertifiedByDefault BIT
GO

UPDATE dbo.DealerPreference_Pricing SET MatchCertifiedByDefault = 0
GO

ALTER TABLE dbo.DealerPreference_Pricing ALTER COLUMN MatchCertifiedByDefault BIT NOT NULL
GO
