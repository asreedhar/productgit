DECLARE @AutoCheckAdmin INT

SELECT @AutoCheckAdmin =MemberID
FROM	[IMT].dbo.Member 
WHERE	Login  ='AutoCheckSystemUserAdmin'



DECLARE @Table TABLE 
	(idx		INT	IDENTITY(1,1), 
	DealerID	INT	NOT NULL,
	UserName	 VARCHAR(20) NOT NULL,
	Password	VARCHAR(20) NULL
)

INSERT
INTO	@Table 
	(DealerID,
	 UserName,
	Password)

SELECT DISTINCT	ma.BusinessUnitID, mc.Username, case when len(mc.Password) = 0 then null else mc.Password end Password
FROM			MemberCredential AS mc 
INNER JOIN		MemberAccess AS ma ON ma.MemberID = mc.MemberID and (mc.CredentialTypeID = 2) AND (LEN(mc.Username) >0)
INNER JOIN		(SELECT		ma.BusinessUnitID, count(distinct mc.Username) CntUsername
				 FROM		MemberCredential AS mc 
				INNER JOIN	MemberAccess AS ma ON ma.MemberID = mc.MemberID and (mc.CredentialTypeID = 2) AND (LEN(mc.Username) >0)
				GROUP BY	ma.BusinessUnitID
				HAVING		count(distinct mc.Username) = 1) UserCnt on UserCnt.BusinessUnitId = ma.BusinessUnitId
ORDER BY		ma.BusinessUnitID



DECLARE @i		INT,
	@c		INT,
	@t		DATETIME,
	@DealerID	INT,
	@UserName	VARCHAR(20),
	@Password	VARCHAR(20),
	 @AccountID	INT,
	@NewAccountStatusID TINYINT,
	@NewAccountTypeID TINYINT

SET	@NewAccountStatusID =1 
SET	@NewAccountTypeID =1
 
SELECT @i = 1, @c = COUNT(*) FROM @Table

WHILE(@i <= @c) BEGIN

	SELECT	@DealerID = DealerID,
		@UserName =UserName,
		@Password =Password
	FROM	@Table
	WHERE	idx = @i

	INSERT
	INTO	[IMT].AutoCheck.Account
		(AccountStatusID,
		AccountTypeID,
		UserName,
		Password,
		InsertUser,
		InsertDate)
	VALUES(	 @NewAccountStatusID,
		@NewAccountTypeID,
		@UserName,
		@Password,
		@AutoCheckAdmin ,
		GETDATE()
		)

	SET @AccountID =SCOPE_IDENTITY()	

	INSERT
	INTO	[IMT].AutoCheck.Account_Dealer
		(DealerID,
		AccountId,
		InsertUser,
		InsertDate)
	VALUES(	@DealerID,
		@AccountID,
		@AutoCheckAdmin ,
		GETDATE())
	
SET @i = @i + 1
	
END

GO
