DECLARE @CarFaxAdmin INT

SELECT @CarFaxAdmin =MemberID
FROM	[IMT].dbo.Member 
WHERE	Login  ='CarfaxSystemUserAdmin'



DECLARE @Table TABLE 
	(idx		INT	IDENTITY(1,1), 
	DealerID	INT	NOT NULL,
	UserName	 VARCHAR(20) NOT NULL,
	Password	VARCHAR(20) NOT NULL
)

INSERT
INTO	@Table 
	(DealerID,
	 UserName,
	Password)
SELECT	BusinessUnitID,
	CarfaxUserName,
	LTRIM(RTRIM(CarfaxPassword))
FROM	dbo.DealerPreference_Carfax
WHERE	LEN(CarfaxUserName) >0
AND	LEN(CarfaxPassword) >0

DECLARE @i		INT,
	@c		INT,
	@t		DATETIME,
	@DealerID	INT,
	@UserName	VARCHAR(20),
	@Password	VARCHAR(20),
	 @AccountID	INT,
	@NewAccountStatusID TINYINT,
	@NewAccountTypeID TINYINT

SET	@NewAccountStatusID =1 
SET	@NewAccountTypeID =1
 
SELECT @i = 1, @c = COUNT(*) FROM @Table

WHILE(@i <= @c) BEGIN

	SELECT	@DealerID = DealerID,
		@UserName =UserName,
		@Password =Password
	FROM	@Table
	WHERE	idx = @i

	INSERT
	INTO	Carfax.Account
		(AccountStatusID,
		AccountTypeID,
		UserName,
		Password,
		InsertUser,
		InsertDate)
	VALUES(	 @NewAccountStatusID,
		@NewAccountTypeID,
		@UserName,
		@Password,
		@CarFaxAdmin ,
		GETDATE()
		)

	SET @AccountID =SCOPE_IDENTITY()	

	INSERT
	INTO	Carfax.Account_Dealer
		(DealerID,
		AccountId,
		InsertUser,
		InsertDate)
	VALUES(	@DealerID,
		@AccountID,
		@CarFaxAdmin ,
		GETDATE())
	
SET @i = @i + 1
	
END

GO
