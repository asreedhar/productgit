INSERT
INTO	VehicleAttributeCatalog_11 (SquishVIN, CatalogKey, ModelYear, Make, Line, BodyType, Series, MakeModelGroupingID, Type, Class, Doors, FuelType, Engine, DriveTypeCode, Transmission, CylinderQty, VehicleAttributeSourceID, DisplayBodyTypeID, EdmundsBodyTypeCode, BodyStyleID)
SELECT	DISTINCT dbo.fn_squishvin('1FAFP56U67A106041'), CatalogKey, 2007, Make, Line, BodyType, Series, MakeModelGroupingID, Type, Class, Doors, FuelType, Engine, DriveTypeCode, Transmission, CylinderQty, 2, DisplayBodyTypeID, EdmundsBodyTypeCode, BodyStyleID
FROM	VehicleAttributeCatalog_11
WHERE	ModelYear = 2006
	AND Make = 'Ford'
	AND Line = 'Taurus'
	AND CatalogKey = 'SEL FWD 4DR Sedan (3.0L V6 4A)'
UNION
SELECT	DISTINCT dbo.fn_squishvin('1FAFP53UX7A108864'), CatalogKey, 2007, Make, Line, BodyType, Series, MakeModelGroupingID, Type, Class, Doors, FuelType, Engine, DriveTypeCode, Transmission, CylinderQty, 2, DisplayBodyTypeID, EdmundsBodyTypeCode, BodyStyleID
FROM	VehicleAttributeCatalog_11
WHERE	ModelYear = 2006
	AND Make = 'Ford'
	AND Line = 'Taurus'
	AND CatalogKey = 'SE FWD 4DR Sedan (3.0L V6 4A)'
GO