/****** Object:  Table [dbo].[HalSessions]    Script Date: 01/19/2007 10:46:03 ******/
/* Benson Fung */
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[HalSessions](
	[ID] [int] IDENTITY(1,1) CONSTRAINT [PK_HalSession_Count] NOT NULL PRIMARY KEY,
	[HalSessionID] varchar(32) CONSTRAINT [UK_HalSession] NOT NULL UNIQUE([HalSessionID]), --place index hint here, this is our lookup!
	[BusinessUnitID] [int]
		CONSTRAINT [FK_HalSession_BusinsesUnit] NOT NULL FOREIGN KEY REFERENCES dbo.BusinessUnit(BusinessUnitID),
	[DateModified] [smalldatetime] NOT NULL CONSTRAINT [DF_HalSession_Modified] DEFAULT (getdate())
)
GO