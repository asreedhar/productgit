
SET IDENTITY_INSERT PurchasingCenterChannels ON 

INSERT
INTO	PurchasingCenterChannels (ChannelID, ChannelName, HasAccessGroups)
SELECT	3, 'Live Auction', 0

SET IDENTITY_INSERT PurchasingCenterChannels OFF


UPDATE	PurchasingCenterChannels
SET	ChannelName = 'On-Line Auction'
WHERE	ChannelID = 1
GO

--MAX is an upgrade for now
INSERT INTO [dbo].[lu_DealerUpgrade]
           ([DealerUpgradeCD]
           ,[DealerUpgradeDESC])
     VALUES
           (15
           ,'MAX')
GO

INSERT INTO [dbo].[lu_DealerUpgrade]
           ([DealerUpgradeCD]
           ,[DealerUpgradeDESC])
     VALUES
           (16
           ,'MAX-Test')
GO