--------------------------------------------------------------------------------
--
--	If an inventory item comes into inventory where there already exists a
--	an active inventory item for the same vehicle (VIN) and Business Unit,
--	we will consider the new inventory to be an "override" that will 
--	overwrite the existing inventory row.  When this occurs, the dataload
--	process will store the overwritten inventory row in the table
--	InventoryOverrideHistory.
--
--------------------------------------------------------------------------------

INSERT
INTO	lu_AuditStatus
SELECT	5, 'Override', 'O'
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventoryOverrideHistory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[InventoryOverrideHistory]
GO

CREATE TABLE [dbo].[InventoryOverrideHistory] (
	[InventoryID] 		[int] NOT NULL ,
	[StockNumber] 		[varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[BusinessUnitID] 	[int] NOT NULL ,
	[VehicleID] 		[int] NOT NULL ,
	[InventoryActive] 	[tinyint] NOT NULL ,
	[InventoryReceivedDate] [smalldatetime] NOT NULL ,
	[DeleteDt] 		[smalldatetime] NULL ,
	[ModifiedDT] 		[smalldatetime] NOT NULL ,
	[DMSReferenceDt] 	[smalldatetime] NULL ,
	[UnitCost] 		[decimal](9, 2) NOT NULL ,
	[AcquisitionPrice] 	[decimal](8, 2) NULL ,
	[Pack] 			[decimal](8, 2) NULL ,
	[MileageReceived] 	[int] NULL ,
	[TradeOrPurchase] 	[tinyint] NOT NULL ,
	[ListPrice] 		[decimal](8, 2) NULL ,
	[InitialVehicleLight] 	[int] NULL ,
	[Level4Analysis] 	[varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[InventoryType] 	[tinyint] NOT NULL ,
	[ReconditionCost] 	[decimal](8, 2) NULL ,
	[UsedSellingPrice] 	[decimal](8, 2) NULL ,
	[Certified] 		[tinyint] NOT NULL ,
	[VehicleLocation] 	[varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CurrentVehicleLight] 	[tinyint] NULL ,
	[FLRecFollowed] 	[tinyint] NOT NULL ,
	[Audit_ID] 		[int] NOT NULL ,
	[InventoryStatusCD] 	[smallint] NOT NULL ,
	[ListPriceLock] 	[tinyint] NOT NULL ,
	[SpecialFinance] 	[tinyint] NULL ,
	[eStockCardLock] 	[tinyint] NOT NULL ,
	[DateBookoutLocked] 	[smalldatetime] NULL ,
	[PlanReminderDate] 	[smalldatetime] NULL,
	[NewStockNumber] 	[varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DateCreated]		[smalldatetime] NOT NULL
) 
GO

