CREATE TABLE dbo.Inventory_ListPriceHistory (	ListPriceHistoryID	INT IDENTITY(1,1) NOT NULL CONSTRAINT PK_Inventory_ListPriceHistory PRIMARY KEY NONCLUSTERED,
						InventoryID 		INT NOT NULL,
						DMSReferenceDT		SMALLDATETIME NOT NULL,
						ListPrice		DECIMAL(8,2) NULL,
						UpdateLocked		BIT NOT NULL CONSTRAINT DF_Inventory_ListPriceHistory_UpdateLocked DEFAULT(0))

GO						
CREATE CLUSTERED INDEX IX_Inventory_ListPriceHistory_InventoryID ON Inventory_ListPriceHistory(InventoryID) 
GO