/*

Revise the indexes on GDLights

*/

ALTER TABLE GDLight
 drop constraint UQ_GDLight

ALTER TABLE GDLight
 drop constraint PK_MMGLight

ALTER TABLE GDLight
 add constraint PK_GDLight
  primary key clustered (BusinessUnitID, GroupingDescriptionID, InventoryVehicleLightID)
  on data

ALTER TABLE GDLight
 add constraint UQ_GDLight__GroupingDescriptionLightId
  unique nonclustered (GroupingDescriptionLightId)
  on data
