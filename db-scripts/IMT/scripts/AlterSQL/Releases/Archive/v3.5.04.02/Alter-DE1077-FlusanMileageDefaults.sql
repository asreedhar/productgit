-- new preference for LiveAuctions
ALTER TABLE dbo.DealerPreference
ADD LiveAuctionDistanceFromDealer INT NOT NULL
CONSTRAINT [DF_DealerPreference_LiveAuctionDistanceFromDealer] DEFAULT (1000)
GO

-- delete old constraint for purchasing (online and in-group)
ALTER TABLE [dbo].[DealerPreference] DROP CONSTRAINT [DF_DealerPreference_PurchasingDistanceFromDealer]
GO
-- update existing records for FLUSAN
UPDATE dbo.DealerPreference
SET PurchasingDistanceFromDealer = 1000
WHERE PurchasingDistanceFromDealer = -1
GO

-- add constraint for purchasing (online and in-group) back in
ALTER TABLE [dbo].[DealerPreference] 
ADD CONSTRAINT [DF_DealerPreference_PurchasingDistanceFromDealer] DEFAULT (1000)
FOR [PurchasingDistanceFromDealer]
GO
