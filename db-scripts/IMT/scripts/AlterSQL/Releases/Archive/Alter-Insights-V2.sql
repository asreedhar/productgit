
--
-- Update the "Short Leash" InventoryBucketRange low/high values to match the specification.
--

UPDATE  IBR
SET     IBR.Low = 21,
        IBR.High = 30
FROM    [dbo].[InventoryBucketRanges] IBR
WHERE   IBR.InventoryBucketID = 5
AND     IBR.RangeID = 1
GO

UPDATE  IBR
SET     IBR.Low = 30,
        IBR.High = 45
FROM    [dbo].[InventoryBucketRanges] IBR
WHERE   IBR.InventoryBucketID = 5
AND     IBR.RangeID = 2
GO

UPDATE  IBR
SET     IBR.Low = 45,
        IBR.High = 60
FROM    [dbo].[InventoryBucketRanges] IBR
WHERE   IBR.InventoryBucketID = 5
AND     IBR.RangeID = 3
GO

--
-- Add Market Insights with correct IDs
--

UPDATE  [dbo].[InsightType]
SET     [Name] = 'Make/Model Market Share Percentage (in Segment)',
        [InsightCategoryID] = 1
WHERE   InsightTypeID = 12
GO

UPDATE  [dbo].[InsightType]
SET     [Name] = 'Make/Model Market Share Rank (in Segment)',
        [InsightCategoryID] = 1
WHERE   InsightTypeID = 13
GO

UPDATE  [dbo].[InsightType]
SET     [Name] = 'Model Year Market Share Percentage',
        [InsightCategoryID] = 1
WHERE   InsightTypeID = 14
GO

UPDATE  [dbo].[InsightType]
SET     [Name] = 'Model Year Market Share Rank',
        [InsightCategoryID] = 1
WHERE   InsightTypeID = 15
GO

--
-- Bump the operational insights
--

UPDATE  [dbo].[InsightType]
SET     [Name] = 'Appraisal Closing Rate'
WHERE   InsightTypeID = 16
GO

UPDATE  [dbo].[InsightType]
SET     [Name] = 'Make a Deal'
WHERE   InsightTypeID = 17
GO

UPDATE  [dbo].[InsightType]
SET     [Name] = 'Trade-In Inventory Analyzed'
WHERE   InsightTypeID = 18
GO

--
-- Insert the (now) "missing" operational insights
--

INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (19, 2, 'Average Immediate Wholesale Profit')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (20, 2, 'Current Inventory - Short Leash')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (21, 2, 'Current Inventory - Unplanned Vehicles')
INSERT INTO dbo.InsightType ([InsightTypeID], [InsightCategoryID], [Name]) VALUES (22, 2, 'Current Inventory - Days Supply')
GO

--
-- Update foreign keys referencing InsightType
--

UPDATE  InsightTarget
SET     InsightTypeID = InsightTypeID+4
GO

