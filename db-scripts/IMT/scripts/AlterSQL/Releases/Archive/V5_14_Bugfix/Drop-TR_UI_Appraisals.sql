
-- DROP MISNAMED TRIGGER (ONLY AN INSERT TRIGGER)

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TR_UI_Appraisals]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].[TR_UI_Appraisals]
GO