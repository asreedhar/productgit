----------------------------------------------------------------------------------------------------
--	Add Group Homepage preference on the Member table, References new HomePage table in case
--  more homepage preferences are added in the future
----------------------------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HomePage]') AND type in (N'U'))
DROP TABLE [dbo].[HomePage]

CREATE TABLE [dbo].[HomePage] (
	HomePageID TINYINT IDENTITY(1,1) NOT NULL
		CONSTRAINT [PK_HomePage_HomePageRegionID] PRIMARY KEY CLUSTERED
	, [Name] VARCHAR(255) NOT NULL
)
GO

INSERT INTO [dbo].[HomePage] VALUES ('Multi-Dealer Access Page (bubble page)')
INSERT INTO [dbo].[HomePage] VALUES ('Performance Management Center (PMC)')

ALTER TABLE dbo.Member ADD GroupHomePageID TINYINT NOT NULL CONSTRAINT DF_Member__GroupHomePageID DEFAULT (1)
GO

ALTER TABLE [dbo].[Member]  WITH CHECK ADD CONSTRAINT [FK_Member_GroupHomepageID] FOREIGN KEY([GroupHomepageID])
REFERENCES [dbo].[HomePage] ([HomePageID])
GO

--For users who currently have acccess to PMC, keep this as their HomePage
UPDATE M
SET GroupHomePageID = 2
FROM Member M
JOIN (   SELECT MA.MemberID 
    
		FROM BusinessUnit BU
		JOIN dbo.DealerGroupPreference DGP ON BU.BusinessUnitID = DGP.BusinessUnitID
		JOIN dbo.BusinessUnitRelationship BUR ON BU.BusinessUnitID = BUR.ParentID
		JOIN dbo.MemberAccess MA ON BUR.BusinessUnitID = MA.BusinessUnitID

		WHERE
	   	 	BU.BusinessUnitTypeID = 3
	    	AND DGP.IncludePerformanceManagementCenter = 1

		GROUP 
		BY  MA.MemberID

		HAVING COUNT(MA.MemberID) > 1

      ) MA ON M.MemberID = MA.MemberID
GO
