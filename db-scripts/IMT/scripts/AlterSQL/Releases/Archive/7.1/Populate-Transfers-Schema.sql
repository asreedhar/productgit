
DECLARE @sql VARCHAR(500)
DECLARE @Result INT,
	@FileName VARCHAR(1000) = '\\' + @@SERVERNAME + '\Datafeeds\IMT-Transfers\Transfers#Purchase.csv'
	
EXEC master.dbo.xp_FileExist @FileName, @Result  OUTPUT

IF @Result = 1 BEGIN

	
	SET @sql =
	'
	BULK INSERT Transfers.Purchase
	FROM ''\\' + @@SERVERNAME + '\Datafeeds\IMT-Transfers\Transfers#Purchase.csv''
	WITH
	(
	        DATAFILETYPE = ''char'',
	        FIELDTERMINATOR = '','',
	        FIRSTROW = 2
	)
	'
	EXEC(@sql)

	
	
	SET @sql =
	'
	BULK INSERT Transfers.Sale
	FROM ''\\' + @@SERVERNAME + '\Datafeeds\IMT-Transfers\Transfers#Sale.csv''
	WITH
	(
	        DATAFILETYPE = ''char'',
	        FIELDTERMINATOR = '','',
	        FIRSTROW = 2
	)
	'
	EXEC(@sql)
	
	SET @sql =
	'
	BULK INSERT Transfers.Transfer
	FROM ''\\' + @@SERVERNAME + '\Datafeeds\IMT-Transfers\Transfers#Transfer.csv''
	WITH
	(
	        DATAFILETYPE = ''char'',
	        FIELDTERMINATOR = '','',
	        FIRSTROW = 2
	)
	'
	EXEC(@sql)
END