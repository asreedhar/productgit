
-- Creates an index on the date of Distribution exceptions, since this will improve performance of an OPs monitoring query.

CREATE NONCLUSTERED INDEX  IX_Distribution_Exception__ExceptionTime 
ON IMT.Distribution.Exception (ExceptionTime DESC)