ALTER TABLE dbo.InventoryStatusCodes ALTER COLUMN ShortDescription VARCHAR(2) NOT NULL
GO

----------------------------------------------------------------------------------------
--
--	Add placeholder mappings as the DMS inventory load process uses this table  
--	to map the original vehicle status codes to an internal code. 
--
--	Note that InventoryStatusCD values 2-24 are specific to one DMS or dealer 
--	(origin lost, been in the DB since 2004) and probably need to be set to 
--	*PLACEHOLDER* as well.  
--	
--	Ultimately, it probably makes more sense to store the raw vehcicle status code
--	rather than map to an internal table as the mappings could be different for
--	each dealer.
--  
----------------------------------------------------------------------------------------


INSERT
INTO	dbo.InventoryStatusCodes (ShortDescription, Description)
SELECT	'10', '*PLACEHOLDER*'
UNION 
SELECT	'11', '*PLACEHOLDER*'
UNION 
SELECT	'12', '*PLACEHOLDER*'
UNION 
SELECT	'13', '*PLACEHOLDER*'
UNION 
SELECT	'14', '*PLACEHOLDER*'
UNION 
SELECT	'15', '*PLACEHOLDER*'
UNION 
SELECT	'16', '*PLACEHOLDER*'
UNION 
SELECT	'17', '*PLACEHOLDER*'
UNION 
SELECT	'18', '*PLACEHOLDER*'
UNION 
SELECT	'19', '*PLACEHOLDER*'
UNION 
SELECT	'20', '*PLACEHOLDER*'
UNION 
SELECT	'21', '*PLACEHOLDER*'
UNION 
SELECT	'22', '*PLACEHOLDER*'
UNION 
SELECT	'23', '*PLACEHOLDER*'
UNION 
SELECT	'24', '*PLACEHOLDER*'
UNION 
SELECT	'25', '*PLACEHOLDER*'
UNION 
SELECT	'26', '*PLACEHOLDER*'
UNION 
SELECT	'27', '*PLACEHOLDER*'
UNION 
SELECT	'28', '*PLACEHOLDER*'

GO

EXEC dbo.sp_SetTableDescription 'dbo.InventoryStatusCodes',
'This table maps raw vehicle status codes from various DMS''s to a numeric status code in the IMT database. 

Note that InventoryStatusCD descriptions 2-24 are specific to one DMS or dealer (origins lost, been in the DB 
since 2004) and probably need to be set to *PLACEHOLDER* as well.  The Description column really has no 
meaning now.
  	
Ultimately, it probably makes more sense to store the raw vehcicle status code
rather than map to an internal table as the mappings could be different for
each dealer.'
