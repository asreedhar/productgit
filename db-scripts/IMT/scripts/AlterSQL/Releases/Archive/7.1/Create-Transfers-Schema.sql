
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'Transfers')
EXEC('CREATE SCHEMA Transfers AUTHORIZATION dbo')
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Transfers].[Purchase]') AND type in (N'U'))
DROP TABLE [Transfers].[Purchase]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Transfers].[Sale]') AND type in (N'U'))
DROP TABLE [Transfers].[Sale]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Transfers].[Transfer]') AND type in (N'U'))
DROP TABLE [Transfers].[Transfer]
GO

CREATE TABLE Transfers.Purchase (
    PurchaseID      INT IDENTITY(1,1)   NOT NULL,
    PurchaseDate    SMALLDATETIME       NOT NULL,
    DealerID        INT                 NOT NULL,
    VehicleID       INT                 NOT NULL,
    CONSTRAINT PK_Transfers_Purchase
        PRIMARY KEY NONCLUSTERED (PurchaseID),
    CONSTRAINT UK_Transfers_Purchase
        UNIQUE CLUSTERED (VehicleID, DealerID, PurchaseDate)
)
GO

CREATE NONCLUSTERED INDEX IX_Transfers_Purchase__Vehicle_Date
    ON Transfers.Purchase (
        PurchaseDate,
        VehicleID
    )
    INCLUDE (
        DealerID
    )
GO

CREATE TABLE Transfers.Sale (
    SaleID          INT IDENTITY(1,1)   NOT NULL,
    SaleDate        SMALLDATETIME       NOT NULL,
    DealerID        INT                 NOT NULL,
    VehicleID       INT                 NOT NULL,
    CONSTRAINT PK_Transfers_Sale
        PRIMARY KEY NONCLUSTERED (SaleID),
    CONSTRAINT UK_Transfers_Sale
        UNIQUE CLUSTERED (VehicleID, DealerID, SaleDate)
)
GO

CREATE TABLE Transfers.Transfer (
    TransferID      INT IDENTITY(1,1)   NOT NULL,
    SrcDealerID     INT                 NOT NULL,
    SrcDate         SMALLDATETIME       NOT NULL,
    DstDealerID     INT                 NOT NULL,
    DstDate         SMALLDATETIME       NOT NULL,
    VehicleID       INT                 NOT NULL,
    CONSTRAINT PK_Transfers_Transfer
        PRIMARY KEY NONCLUSTERED (TransferID),
    CONSTRAINT UK_Transfers_Transfer
        UNIQUE CLUSTERED (              -- query by:
            DstDealerID, SrcDealerID,   -- who
            DstDate, SrcDate,           -- when
            VehicleID                   -- what
        )
)
GO
