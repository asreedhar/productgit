
---Description--------------------------------------------------------------------------------------
--
-- 	New SystemError table create. Drops old ErrorHandler table.
--
---Parameters---------------------------------------------------------------------------------------

/****** Object:  Table [dbo].[SystemErrors]    Script Date: 03/27/2007 13:54:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'dbo.ErrorHandler') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.ErrorHandler
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'dbo.SystemErrors') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.SystemErrors
GO

CREATE TABLE [dbo].[SystemErrors] (
	[SystemErrorID] [int] IDENTITY(1,1) NOT NULL,
	[Trace] [varchar] (8000) NOT NULL ,
	[DateCreated] [smalldatetime] NOT NULL, 
CONSTRAINT [PK_SystemErrors] PRIMARY KEY CLUSTERED 
(
	[SystemErrorID] ASC
) 
)
GO
