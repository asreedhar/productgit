/********
 * bfung - august 21, 2006
 */

--Add column
ALTER TABLE dbo.tbl_GroupingPromotionPlan
	ADD BusinessUnitID int 
GO

--Insert data
UPDATE gp
SET gp.BusinessUnitID = vpt.BusinessUnitID
FROM dbo.tbl_GroupingPromotionPlan gp
join dbo.VehiclePlanTrackingHeader vpt on gp.VehiclePlanTrackingHeaderId = vpt.VehiclePlanTrackingHeaderId
GO

--Add not null
ALTER TABLE dbo.tbl_GroupingPromotionPlan
	ALTER COLUMN BusinessUnitID int not null
GO

--Add constraint
ALTER TABLE dbo.tbl_GroupingPromotionPlan
	ADD CONSTRAINT [FK_GroupingPromotionPlan_BusinessUnit] FOREIGN KEY
	(
		[BusinessUnitID]
	) REFERENCES [BusinessUnit] (
		[BusinessUnitID]
	)
GO

--Drop old contraints and column
ALTER TABLE dbo.tbl_GroupingPromotionPlan
	DROP FK_GroupingPromotionPlan_VehiclePlanTrackingHeader
GO

ALTER TABLE dbo.tbl_GroupingPromotionPlan
	DROP COLUMN VehiclePlanTrackingHeaderId
GO

--ADD CREATED DATE column for report compatibility (maybe?)
--This column will not be used for now, but will be of use when a migration to AIP_EVENT needs to happen.
ALTER TABLE dbo.tbl_GroupingPromotionPlan
	ADD Created smalldatetime NOT NULL CONSTRAINT [DF_GroupingPromotionPlan_Created] DEFAULT (getdate())
GO	
