/*************************************************************************************************************
**													    
**	MAK 10/23/2009	This script creates the Purchasing schema and the PurchasingUser Role
**
*************************************************************************************************************/						


/*************************************************
**						 
**	1.      Create Purchasing Schema.
**	2.		Create PurchasingUser Role.		 
**						 
**************************************************/

CREATE SCHEMA Purchasing AUTHORIZATION dbo

GO
 
CREATE ROLE PurchasingUser AUTHORIZATION  dbo

GO