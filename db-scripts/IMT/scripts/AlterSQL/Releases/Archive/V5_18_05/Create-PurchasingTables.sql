/*************************************************************************************************************
**													    
**	MAK 10/23/2009	Create the tables in the IMT.Purchasing
**
*************************************************************************************************************/						


/*************************************************
**						 
**	1.      Create IMT.Purchasing.List
**		a.	Create Check Constraints
**		b.  Create ForeignKeys
**
**	2.		Create IMT.Purchasing.ListItem	 
**		a.	Create Check Constraints
**		b.  Create ForeignKeys
**
**						 
**************************************************/

/***************************************************	
**					 
**	1.      Create IMT.Purchasing.List
**		a.	Create Check Constraint- CK_Purchasing_ListItem__UpdateUser
**		b.  Create ForeignKeys
**			i.  FK_Purchasing_report_Member(InsertUser)
**			i.  FK_Purchasing_report_Member(UpdateUser)
**
**************************************************/


CREATE TABLE Purchasing.List
	(ListID INT IDENTITY(1,1) NOT NULL,
	OwnerID	INT NOT NULL,
	InsertDate DATETIME  NOT NULL,
	InsertUser	INT	NOT NULL,
	UpdateDate DATETIME NULL,
	UpdateUser	INT NULL,
	Version	TIMESTAMP NOT NULL,	
 	 CONSTRAINT PK_Purchasing_List__ListID PRIMARY KEY CLUSTERED 
(
	ListID ASC
))

GO

-- a.	Create Check Constraint- CK_Purchasing_List__UpdateUser.
	 
	ALTER TABLE Purchasing.List WITH CHECK ADD  CONSTRAINT 
		 CK_Purchasing_List__UpdateUser
		CHECK  ((UpdateUser IS NULL AND UpdateDate IS NULL)
				OR (UpdateUser IS NOT NULL AND UpdateDate IS NOT NULL))
	GO

--	
--			b.	Create Foreign Keys
--			i. InsertUser

			ALTER TABLE Purchasing.List  WITH CHECK ADD  CONSTRAINT 
			FK_Purchasing_List__InsertUser FOREIGN KEY(InsertUser)
			REFERENCES dbo.Member(MemberID)  

GO

--			ii. UpdateUser
			ALTER TABLE Purchasing.List  WITH CHECK ADD  CONSTRAINT 
			FK_Purchasing_List__UpdateUser FOREIGN KEY(UpdateUser)
			REFERENCES dbo.Member(MemberID)  

/***************************************************	
**					 
**	2.		Create IMT.Purchasing.ListItem table		 
**		a.	Create Check Constraint- CK_Purchasing_ListItem__UpdateUser
**		b.  Create ForeignKeys
**			i.  FK_Purchasing_report_Member(InsertUser)
**			i.  FK_Purchasing_report_Member(UpdateUser)
**
**************************************************/

CREATE TABLE Purchasing.ListItem
	(ListItemID INT IDENTITY(1,1) NOT NULL,
	ListID	INT NOT NULL,
	ModelYear SMALLINT NOT NULL,
	ModelFamilyID SMALLINT NOT NULL,
	SegmentID TINYINT NOT NULL,
	ModelConfigurationID INT NULL,
	Quantity TINYINT NOT NULL,
	Notes VARCHAR(2000) NULL,
	InsertDate DATETIME  NOT NULL,
	InsertUser	INT	NOT NULL,
	UpdateDate DATETIME NULL,
	UpdateUser	INT NULL,
	Version	TIMESTAMP NOT NULL,	
 	 CONSTRAINT PK_Purchasing_ListItem__ListItemID PRIMARY KEY CLUSTERED 
(
	ListItemID ASC
))

GO

--			a.	Create Foreign Keys
--			i. ListID

			ALTER TABLE Purchasing.ListItem  WITH CHECK ADD  CONSTRAINT 
			FK_Purchasing_ListItem__ListID  FOREIGN KEY(ListID)
			REFERENCES Purchasing.List(ListID)  
 


--		C.	Create Unique Constraints -


	CREATE 	UNIQUE NONCLUSTERED INDEX UK_Purchasing_ListItem__ListModelYearModelFamilyIDSegmentIDModelConfigurationID
 ON  Purchasing.ListItem
		(ListID,ModelYear,ModelFamilyID,SegmentID,ModelConfigurationID)
	
GO 