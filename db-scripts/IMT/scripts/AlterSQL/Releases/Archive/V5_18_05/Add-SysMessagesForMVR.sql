/* --------------------------------------------------------------------
 * 
 *	MAK  10/30/2009	Add MVR Errors.
 *
 *	50312 -  Concurrent Change Detected
 * -------------------------------------------------------------------- */

DECLARE @SysNum INT

SET @SysNum = 50312

IF EXISTS (select * from sys.messages where message_id = @SysNum)
	EXEC sp_dropmessage @msgnum = @SysNum
 

EXEC sp_addmessage @SysNum, 16,
  'Concurrent change detected!' ;
GO
 