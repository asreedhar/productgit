SET IDENTITY_INSERT JobTitle ON 

INSERT
INTO	JobTitle (JobTitleID, Name)
SELECT	18, 'Keynote Monitor'

SET IDENTITY_INSERT JobTitle OFF
GO

UPDATE	Member
SET	JobTitleID = 18
WHERE	Login LIKE 'keynote%'

GO