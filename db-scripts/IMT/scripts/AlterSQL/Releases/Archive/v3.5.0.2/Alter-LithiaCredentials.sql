
INSERT	
INTO	BusinessUnitCredential (BusinessUnitID, CredentialTypeID, Username, Password)
SELECT	BU.BusinessUnitID, 4, NULL, 'pyramid'
FROM	BusinessUnit BU 
	JOIN DealerGroupPreference DGP on BU.BusinessUnitID = DGP .BusinessUnitID
WHERE	BusinessUnitTypeID = 3
	AND LithiaStore = 1
