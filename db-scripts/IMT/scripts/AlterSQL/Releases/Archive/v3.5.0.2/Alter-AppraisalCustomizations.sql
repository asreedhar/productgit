------------------------------------------------------------------------------------------------
--	SR251 & SR252: Appraisal Customizations
--
--	Lithia customizations regarding locking of appraisals and storage of customer info
--	for an appraisal.
--
--	To implement, extend the existing "Upgrade" scheme to allow dealer-group level upgrades.
--	Also, create a new credential table to store the business unit-level credentials.
--	Eventually, the credential scheme (business unit/member) could/should be merged as 
--	the relationship is hierarchical.
--
------------------------------------------------------------------------------------------------

ALTER TABLE dbo.Appraisals ADD DateLocked SMALLDATETIME NULL
GO
ALTER TABLE dbo.Appraisals ADD Locked AS CAST(CASE WHEN DateLocked <= GETDATE() THEN 1 ELSE 0 END AS TINYINT)
GO

ALTER TABLE dbo.Inventory ADD DateBookoutLocked SMALLDATETIME NULL
GO
ALTER TABLE dbo.Inventory ADD BookoutLocked AS CAST(CASE WHEN DateBookoutLocked <= GETDATE() THEN 1 ELSE 0 END AS TINYINT)
GO

INSERT
INTO	lu_DealerUpgrade (DealerUpgradeCD, DealerUpgradeDESC)
SELECT	10, 'Appraisal Lockout'
GO

SET IDENTITY_INSERT CredentialType ON 
GO
INSERT
INTO	CredentialType (CredentialTypeID, Name)
SELECT	4, 'Appraisal Lockout'
GO
SET IDENTITY_INSERT CredentialType OFF 
GO

CREATE TABLE [dbo].[BusinessUnitCredential] (
	[BusinessUnitID] [int] NOT NULL,
	[CredentialTypeID] [tinyint] NOT NULL,
	[Username] [varchar] (50) NULL,
	[Password] [varchar] (50) NULL,
	CONSTRAINT [PK_BusinessUnitCredential] PRIMARY KEY NONCLUSTERED
	(
		[BusinessUnitID],
		[CredentialTypeID]
	),
	CONSTRAINT [FK_BusinessUnitCredential_BusinessUnitID] FOREIGN KEY 
	(
		[BusinessUnitID]
	) REFERENCES [BusinessUnit] (
		[BusinessUnitID]
	), 
	CONSTRAINT [FK_BusinessUnitCredential_CredentialTypeID] FOREIGN KEY 
	(
		[CredentialTypeID]
	) REFERENCES [CredentialType] (
		[CredentialTypeID]
	)
) 
GO

ALTER TABLE Appraisals ADD DateModified SMALLDATETIME NOT NULL DEFAULT (GETDATE())
GO

UPDATE	Appraisals 
SET	DateModified = DateCreated
GO

CREATE TRIGGER dbo.TR_IU_Appraisals ON Appraisals
FOR INSERT, UPDATE NOT FOR REPLICATION
AS
UPDATE	A
SET	DateModified = GETDATE()
FROM	Appraisals A
	JOIN inserted I ON A.AppraisalID = I.AppraisalID
GO