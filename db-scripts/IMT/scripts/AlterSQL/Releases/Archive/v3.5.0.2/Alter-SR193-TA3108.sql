UPDATE	ThirdParties
SET	Name = 'GALVES',
	DefaultFooter = 'All Galves values are reprinted with permission of Galves.',
	ImageName = 'images/tools/galves_logo.gif'
WHERE	ThirdPartyID = 4
GO

SET IDENTITY_INSERT ThirdPartyCategories ON 

INSERT
INTO	ThirdPartyCategories (ThirdPartyCategoryID, ThirdPartyID, Category)
SELECT	10, 4, 'Wholesale'

SET IDENTITY_INSERT ThirdPartyCategories OFF
GO