UPDATE	MC1
SET	MC1.Body = MC2.Body,
	MC1.Title = MC2.Title
FROM	MessageCenter MC1
	JOIN MessageCenter MC2 ON MC2.Ordering = 0
WHERE	MC1.Ordering = 1

GO

UPDATE	MessageCenter
SET	Title = '<h3 style="display:block;color:#333;border-bottom:1px dashed #CCC;margin-right:0px;">  <strong>We have upgraded The EDGE to v3.5.02</strong>  </h3> <span style="font-size:100%;">',
	Body = 
'<div class="releaseNotes"><p><font color="F0000"> <b>What''s new?</b></font><ul><li>
              <strong><a href="javascript:void(0)" onclick="window.open(''http://images.firstlook.biz/ReleaseNotes-EquityReport.pdf'')">Loan Value - Book Value Calculator</a></strong><br>Calculate your potential profit based on loan value or book value verses unit cost.
              </li><li>
              <strong><a href="javascript:void(0)" onclick="window.open(''http://images.firstlook.biz/ReleaseNotes-CustomerInfo.pdf'')">Customer Information Attached to the Appraisal</a></strong><br>Save Deals Using The Edge Trade Manager!
              </li><li>
              <strong><a href="javascript:void(0)" onclick="window.open(''http://images.firstlook.biz/ReleaseNotes-PricingAnalyzer.pdf'')">Pricing Analyzer</a></strong><br>Know the ''right'' retail or wholesale price for vehicles on your lot at the click of a button!
 (available as of May 23, 2006)
              </li></ul>
            </p>

</div>'

WHERE	Ordering = 0 