/*

More indexes, this time to help the Lithia testing dealership reset

*/

CREATE INDEX IX_AuditBookouts__BookoutID
 on AuditBookouts (BookoutID)
 with sort_in_tempdb

CREATE INDEX IX_AppraisalActions__AppraisalID
 on AppraisalActions (AppraisalID)
 with sort_in_tempdb
GO
