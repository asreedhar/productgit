--------------------------------------------------------------------------------
--
--	DELETE UNNECESSARY TPV COLUMNS AND ADD COLUMNS AS REQ'D FOR GALVES
--
--------------------------------------------------------------------------------

ALTER TABLE ThirdPartyVehicles DROP COLUMN VehicleGuideBookID
GO
ALTER TABLE ThirdPartyVehicles ADD EngineType VARCHAR(20) NULL
GO
ALTER TABLE ThirdPartyVehicles ADD Make VARCHAR(25) NULL
GO
ALTER TABLE ThirdPartyVehicles ADD Model VARCHAR(25) NULL
GO
ALTER TABLE ThirdPartyVehicles ADD Body VARCHAR(25) NULL
GO