INSERT
INTO	DealerUpgrade (BusinessUnitID, DealerUpgradeCD, StartDate, EndDate, Active)

SELECT	du.BusinessUnitID
    	,11 --equity analysis
    	,'6/28/2006 0:00:00AM'
    	,'6/28/2007 0:00:00AM'
    	,1
FROM 	DealerUpgrade du 
	JOIN businessunit bu ON du.businessunitid = bu.businessunitid
WHERE 	du.DealerUpgradeCD = 2 AND du.Active = 1 and bu.active = 1 