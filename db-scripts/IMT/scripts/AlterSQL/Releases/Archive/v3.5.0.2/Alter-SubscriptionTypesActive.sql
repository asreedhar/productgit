ALTER TABLE SubscriptionTypes ADD Active TINYINT NOT NULL DEFAULT (1)
GO

UPDATE	SubscriptionTypes 
SET	Active = 0
WHERE	SubscriptionTypeID IN (6,7)	-- Policy, Redistribution
GO


UPDATE 	SubscriptionTypes 
SET 	Description = 'Understocking / Overstocking Update'
WHERE 	SubscriptionTypeID = 5
GO

DELETE 
FROM 	SubscriptionFrequencyDetail 
WHERE 	SubscriptionFrequencyDetailID = 100
GO

