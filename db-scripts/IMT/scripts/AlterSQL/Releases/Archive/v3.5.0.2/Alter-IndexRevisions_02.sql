/*

Optimizing index work:
 - Add a clustered index to VehiclePlanTrackingHeader..BusinessUnitID
 - Reset the standard primary key to a fillfactor of 0 (100)
 - Add a clustered index to VehiclePlanTracking (on the ...header foreign key)

--  DBCC SHOWCONTIG (vehicleplantrackingheader) with tableresults, all_indexes, all_levels

*/

--  Drop existing FK references
ALTER TABLE tbl_GroupingPromotionPlan
 drop constraint FK_GroupingPromotionPlan_VehiclePlanTrackingHeader

ALTER TABLE VehiclePlanTracking
 drop constraint FK_VehiclePlanTracking_VehiclePlanTrackingHeaderID

--  Drop the existing PK (only way to change the fillfactor)
ALTER TABLE VehiclePlanTrackingHeader
 drop constraint PK_VehiclePlanTrackingHeader

--  Create the new clustered index
CREATE clustered INDEX IX_VehiclePlanTrackingHeader__BusinessUnitID
 on VehiclePlanTrackingHeader (BusinessUnitID)
 with fillfactor = 90
  ,sort_in_tempdb

--  Restore the primery key
ALTER TABLE VehiclePlanTrackingHeader
 add constraint PK_VehiclePlanTrackingHeader
  primary key (VehiclePlanTrackingHeaderID)

--  Restore the FKs
ALTER TABLE tbl_GroupingPromotionPlan
 add constraint FK_GroupingPromotionPlan_VehiclePlanTrackingHeader
  foreign key (VehiclePlanTrackingHeaderID) references VehiclePlanTrackingHeader (VehiclePlanTrackingHeaderID)

ALTER TABLE VehiclePlanTracking
 add constraint FK_VehiclePlanTracking_VehiclePlanTrackingHeaderID
  foreign key (VehiclePlanTrackingHeaderID) references VehiclePlanTrackingHeader (VehiclePlanTrackingHeaderID)

  
  --  Add the matching (clustered) index to VehiclePlanTracking
CREATE clustered INDEX IX_VehiclePlanTracking__VehiclePlanTrackingHeaderID
 on VehiclePlanTracking (VehiclePlanTrackingHeaderID)
  with fillfactor = 95
  ,sort_in_tempdb
  