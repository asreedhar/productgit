
--------------------------------------------------------------------
--	CYA
--------------------------------------------------------------------

UPDATE	DealerPreference
SET 	BookOutPreferenceSecondId = BookOutSecondPreferenceId
WHERE	BookOutPreferenceSecondId IS NULL
	AND BookOutSecondPreferenceId IS NOT NULL
GO	