/*

Rework indexes on a badly out-of-whack table

*/


ALTER TABLE tbl_DealerValuation
 drop constraint PK_tbl_DealerValuation
GO

ALTER TABLE tbl_DealerValuation
 add constraint PK_tbl_DealerValuation
  primary key clustered (DealerValuationID)
GO
