--------------------------------------------------------------------------------------------------------
--
--	FE61: Equity Analyzer Report
--
--	The UCM should be able to save the search criteria to rerun again for the dealership.
--	The UCM should be able to run a saved search.
--
--	Note: We don't have to save the ThirdPartyID as we can determine that from the 
--	ThirdPartyCategoryID
--
--------------------------------------------------------------------------------------------------------


CREATE TABLE dbo.EquityAnalyzerSearches (
	EquityAnalyzerSearchID	INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID 		INT NOT NULL,
	Name			VARCHAR(50) NOT NULL,
	ThirdPartyCategoryID	INT NOT NULL,			-- SHOULD BE A TINYINT
	PercentMultiplier	TINYINT NOT NULL CONSTRAINT DF_EquityAnalyzerSearches_Multiplier DEFAULT (100),			
	CONSTRAINT PK_EquityAnalyzerSearches PRIMARY KEY NONCLUSTERED
	(
		EquityAnalyzerSearchID
	),
	CONSTRAINT FK_EquityAnalyzerSearches_BusinessUnitID FOREIGN KEY 
	(
		BusinessUnitID
	) REFERENCES BusinessUnit (
		BusinessUnitID
	), 
	CONSTRAINT FK_EquityAnalyzerSearchesThirdPartyCategoryID FOREIGN KEY 
	(
		ThirdPartyCategoryID
	) REFERENCES ThirdPartyCategories (
		ThirdPartyCategoryID
	)
) 
GO


INSERT
INTO	lu_DealerUpgrade (DealerUpgradeCD, DealerUpgradeDESC)
SELECT	11, 'Equity Analyzer'
GO
