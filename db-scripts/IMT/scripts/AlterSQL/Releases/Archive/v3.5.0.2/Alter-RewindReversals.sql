ALTER TABLE tbl_VehicleSaleReversals ALTER COLUMN FrontEndGross DECIMAL(18,0)
GO

ALTER TABLE tbl_VehicleSaleReversals ALTER COLUMN SalePrice DECIMAL(9,2)
GO

ALTER TABLE tbl_VehicleSaleReversals ALTER COLUMN TotalGross DECIMAL(8,2)
GO
