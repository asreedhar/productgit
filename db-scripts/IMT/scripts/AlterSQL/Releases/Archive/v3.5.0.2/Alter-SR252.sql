CREATE TABLE [AppraisalCustomer] (
	[AppraisalCustomerID] [int] IDENTITY (1, 1) NOT NULL ,
	[AppraisalID] [int] NOT NULL,
	[Email] [varchar] (50) NULL,
	[FirstName] [varchar] (40) NOT NULL ,
	[LastName] [varchar] (40) NOT NULL ,
	[Gender] [varchar] (1) NULL ,
	[PhoneNumber] [varchar] (40) NOT NULL ,
	CONSTRAINT [PK_AppraisalCustomerID] PRIMARY KEY  NONCLUSTERED 
	(
		[AppraisalCustomerID]
	),
	CONSTRAINT [FK_AppraisalCustomer_Appraisals] FOREIGN KEY
	(
		[AppraisalID]
	)  REFERENCES [Appraisals]  (
		[AppraisalID]
	)
) 




