ALTER TABLE DealerGroupPotentialEvent DROP CONSTRAINT FK_DealerGroupPotentialEvent_TradeAnalyzerEvent
GO

DROP TABLE DealerGroupPotentialEvent
GO

DROP TABLE TradeAnalyzerEvent
GO