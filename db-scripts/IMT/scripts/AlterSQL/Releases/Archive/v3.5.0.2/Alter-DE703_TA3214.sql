-- Creates the ShowroomDaysFilter column to the DealerPreference table
-- Author: DMincemoyer
-- Rally card/task DE703/TA3214


if not exists (select * from syscolumns
	where id=object_id('dbo.DealerPreference') and name='ShowroomDaysFilter')
--add new column
ALTER TABLE dbo.DealerPreference
ADD ShowroomDaysFilter int NOT NULL 
	CONSTRAINT DF_DealerGroupPreference_ShowroomDaysFilter DEFAULT (3)
GO