-- Author: bfung
-- Enable hotsheets subscription in Member Profile

update dbo.SubscriptionTypes
set UserSelectable = 1
where Description = 'Redistribution Hot Sheets'