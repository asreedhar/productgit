/*

Add additional bookout logging columns

*/
if not exists (select * from syscolumns
	where id=object_id('dbo.BookoutProcessorRunLog') and name='SuccesfulBookouts')
ALTER TABLE BookoutProcessorRunLog
 add SuccesfulBookouts  int  null
GO

if not exists (select * from syscolumns
	where id=object_id('dbo.BookoutProcessorRunLog') and name='FailedBookouts') 
ALTER TABLE BookoutProcessorRunLog
 add FailedBookouts     int  null
  
GO