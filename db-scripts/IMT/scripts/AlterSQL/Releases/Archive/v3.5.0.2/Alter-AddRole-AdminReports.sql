/*

Create a role used to grant execute rights on selected procedures -- generaly for administrative
or simple reporting purposes.

As of May 11 2006, execute privileges should be grated on:
 - GetBillingAppraisalsByDate
 - GetBillingInventoryBookoutsByDate


*/

EXECUTE sp_addGroup 'AdminReports'
