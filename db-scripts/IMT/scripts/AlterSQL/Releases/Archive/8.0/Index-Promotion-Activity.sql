
CREATE NONCLUSTERED INDEX IX_Promotion_Activity__Promotion_InsertUser
    ON Promotion.Activity (
        PromotionID,
        InsertUser
    )
GO

CREATE NONCLUSTERED INDEX IX_Promotion_Activity__Promotion_InsertUser_Type
    ON Promotion.Activity (
        PromotionID,
        InsertUser,
        ActivityTypeID
    )
GO
