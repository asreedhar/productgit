

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetInternetAdvertisingActiveInventoryBookoutValues]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[GetInternetAdvertisingActiveInventoryBookoutValues]
GO

IF objectproperty(object_id('dbo.InternetAdvertiserGenerateExport_Beaverton'), 'isProcedure') = 1
    DROP PROCEDURE dbo.InternetAdvertiserGenerateExport_Beaverton

GO
IF objectproperty(object_id('dbo.InternetAdvertiserPrepareForExport_Beaverton'), 'isProcedure') = 1
    DROP PROCEDURE dbo.InternetAdvertiserPrepareForExport_Beaverton

GO

IF objectproperty(object_id('Extract.InventoryExtract#AULTec'), 'isProcedure') = 1
	DROP PROC Extract.InventoryExtract#AULTec


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.InventoryBookoutFailures') AND type in (N'U'))	
	DROP TABLE dbo.InventoryBookoutFailures	