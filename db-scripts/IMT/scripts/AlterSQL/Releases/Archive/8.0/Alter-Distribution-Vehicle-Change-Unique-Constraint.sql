-- Drops unique constraint on vehicle ids and adds one for vehicle ids + vehicle type ids.

ALTER TABLE Distribution.Vehicle DROP CONSTRAINT UQ_Distribution_Vehicle__VehicleEntityID

ALTER TABLE Distribution.Vehicle ADD CONSTRAINT UQ_Distribution_Vehicle__VehicleEntity
UNIQUE (VehicleEntityID, VehicleEntityTypeID)
GO
