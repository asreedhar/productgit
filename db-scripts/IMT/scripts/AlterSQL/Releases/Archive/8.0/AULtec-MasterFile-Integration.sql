

------------------------------------------------------------------------------------------------
--	Delete all photos wired to inactive inventory older than the unwind threshold
------------------------------------------------------------------------------------------------

SELECT	P.PhotoID
INTO	#Photos
FROM	dbo.Photos P
	INNER JOIN dbo.InventoryPhotos IP ON P.PhotoID = IP.PhotoID
	INNER JOIN dbo.Inventory I ON IP.InventoryID = I.InventoryID
	INNER JOIN dbo.DealerPreference DP ON I.BusinessUnitID = DP.BusinessUnitID
WHERE	I.InventoryActive = 0 AND I.DeleteDt < DATEADD(dd,- UnwindDaysThreshold, DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0))

DELETE	IP
FROM	dbo.InventoryPhotos IP
	INNER JOIN #Photos P ON IP.PhotoID = P.PhotoID

DELETE	P
FROM	dbo.Photos P
	INNER JOIN #Photos D ON P.PhotoID = d.PhotoID
	
------------------------------------------------------------------------------------------------
--	We only care about the OriginalPhotoURL now, even for PhotoProviderID 11999 and NULL 
--	(IMS). So set PhotoURL = OriginalPhotoURL and status to "Stored Remotely" for all photos
--
--	We will create a keeper list from PhotoURL for post-clean up (AppraisalPhotos, etc)
------------------------------------------------------------------------------------------------

UPDATE	P
SET	PhotoURL 		= OriginalPhotoURL,
	PhotoStatusCode 	= 2
FROM	dbo.Photos P
	INNER JOIN dbo.InventoryPhotos IP ON P.PhotoID = IP.PhotoID
	INNER JOIN dbo.Inventory I ON IP.InventoryID = I.InventoryID
	INNER JOIN dbo.DealerPreference DP ON I.BusinessUnitID = DP.BusinessUnitID
WHERE	(I.InventoryActive = 1 OR I.DeleteDt >= DATEADD(dd,- UnwindDaysThreshold, DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0)))

------------------------------------------------------------------------------------------------
--	Add a pointer back to the original load ID in DBASTAT.dbo.Dataload_History
--	NULLABLE since we won't have data for photos loaded pre-8.0
------------------------------------------------------------------------------------------------

ALTER TABLE dbo.Photos ADD LoadID INT NULL

GO

------------------------------------------------------------------------------------------------
--	Drop original test version of the extract view
------------------------------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'Extract.InventoryImageURLs#AULtec') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view Extract.InventoryImageURLs#AULtec
GO