if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleSaleCommissions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[VehicleSaleCommissions]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[fn_VehicleSegmentSubSegment]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[fn_VehicleSegmentSubSegment]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_VehicleToSegmentSub1]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[vw_VehicleToSegmentSub1]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_map_VehicleTypeToSegment_lu_VehicleSegment]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[map_VehicleTypeToSegment] DROP CONSTRAINT FK_map_VehicleTypeToSegment_lu_VehicleSegment
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_map_VehicleSubTypeToSubSegment_lu_vehicleSubSegment]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[map_VehicleSubTypeToSubSegment] DROP CONSTRAINT FK_map_VehicleSubTypeToSubSegment_lu_vehicleSubSegment
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_map_VehicleSubTypeToSubSegment_lu_VehicleSubType]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[map_VehicleSubTypeToSubSegment] DROP CONSTRAINT FK_map_VehicleSubTypeToSubSegment_lu_VehicleSubType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_map_VehicleTypeToSegment_lu_VehicleType]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[map_VehicleTypeToSegment] DROP CONSTRAINT FK_map_VehicleTypeToSegment_lu_VehicleType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[lu_VehicleSegment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[lu_VehicleSegment]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[lu_VehicleSubSegment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[lu_VehicleSubSegment]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[lu_VehicleSubType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[lu_VehicleSubType]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[lu_VehicleType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[lu_VehicleType]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[map_FLTypeClassToSegments]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[map_FLTypeClassToSegments]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[map_VehicleSubTypeToSubSegment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[map_VehicleSubTypeToSubSegment]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[map_VehicleTypeToSegment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[map_VehicleTypeToSegment]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_Colors]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_Colors]
GO

------------------------------------------------------------------------------------------------
--	Deny access to the Edmunds tables and views for the VehicleCatalog switch-over
--	Eventually, the Edmunds tables and views will be dropped when we have removed from all
--	sub-systems (e.g. Market)
------------------------------------------------------------------------------------------------

DENY SELECT ON vw_SquishVin TO firstlook
DENY SELECT ON vw_Style TO firstlook
DENY SELECT ON DecodedSquishVINs TO firstlook

DENY SELECT ON lu_Edmunds_Colors_New TO firstlook
DENY SELECT ON lu_Edmunds_Colors_Used TO firstlook
DENY SELECT ON lu_Edmunds_Squishvin TO firstlook
DENY SELECT ON lu_Edmunds_Style_New TO firstlook
DENY SELECT ON lu_Edmunds_Style_Used TO firstlook

GO

------------------------------------------------------------------------------------------------
--	LEGACY 
------------------------------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetTradeAnalyzerReport]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetTradeAnalyzerReport]
GO
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetTradeAnalyzerReportSpecific]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetTradeAnalyzerReportSpecific]
GO
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetTradeAnalyzerReportTrim]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetTradeAnalyzerReportTrim]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetDashBoardReportAverages]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetDashBoardReportAverages]
GO
