IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MP_JDPowerSettings_MemberID]') AND parent_object_id = OBJECT_ID(N'[dbo].[MemberPreference_JDPowerSettings]'))
ALTER TABLE [dbo].[MemberPreference_JDPowerSettings] DROP CONSTRAINT [FK_MP_JDPowerSettings_MemberID]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MemberPreference_JDPowerSettings]') AND type in (N'U'))
DROP TABLE [dbo].[MemberPreference_JDPowerSettings]
GO

CREATE TABLE dbo.License (
	LicenseID INT IDENTITY(1,1) NOT NULL PRIMARY KEY
	, Name varchar(255) NOT NULL
		CONSTRAINT [UK_License_Name] UNIQUE
)
GO

CREATE TABLE dbo.MemberLicenseAcceptance (
	MemberID INT NOT NULL
		CONSTRAINT [FK_MemberLicenseAcceptance_MemberID] FOREIGN KEY
		REFERENCES dbo.Member(MemberID)
	, LicenseID INT NOT NULL
		CONSTRAINT [FK_MemberLicenseAcceptance_LicenseID] FOREIGN KEY
		REFERENCES dbo.License(LicenseID)
	, DateCreated smalldatetime NOT NULL
		CONSTRAINT [FK_MemberLicenseAcceptance_DateCreated] DEFAULT (getdate())
	, CONSTRAINT [PK_MemberLicenseAcceptance] PRIMARY KEY (
		MemberID
		, LicenseID
	)
)
GO

INSERT INTO [dbo].[License]
	([Name])
VALUES('JD Power Used Car Market License')
GO
