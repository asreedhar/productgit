USE [IMT]
GO
/****** Object:  StoredProcedure [Distribution].[Account#FetchDealershipCode]    Script Date: 03/24/2011 11:26:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Distribution].[Account#FetchDealershipCode]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Distribution].[Account#FetchDealershipCode]

/****** Object:  StoredProcedure [Distribution].[Submission#IsStale]    Script Date: 03/24/2011 11:32:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Distribution].[Submission#IsStale]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Distribution].[Submission#IsStale]

/****** Object:  StoredProcedure [Distribution].[Inventory#FetchVin]    Script Date: 03/24/2011 16:29:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Distribution].[Inventory#FetchVin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Distribution].[Inventory#FetchVin]