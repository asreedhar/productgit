
-- Disable the reprice processor for all dealers who have accounts with AutoUplink, Aultec, eBiz and AutoTrader.

UPDATE dbo.InternetAdvertiserDealership
SET IncrementalExport = 0, Active = 0
WHERE InternetAdvertiserId IN (3684, 3686, 5034, 11284)