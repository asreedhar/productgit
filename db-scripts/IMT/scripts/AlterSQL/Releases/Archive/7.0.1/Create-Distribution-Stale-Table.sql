-----------------------------------------------------------------------------------------------------------------------
--	Create Distribution.SubmissionStale
--      a. Check Constraints    
--              - None
--      b. Foreign Keys
--				- FK_Distribution_SubmissionStale__Submission
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[SubmissionStale]
(
    SubmissionID   int not null,
    MessageTypes   int not null,
    OccuredOn      datetime not null
    
    constraint FK_Distribution_SubmissionStale__Submission
    foreign key (SubmissionID)
    references Distribution.Submission (SubmissionID)
)
go


