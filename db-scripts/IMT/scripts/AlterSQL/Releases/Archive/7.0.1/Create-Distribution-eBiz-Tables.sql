-----------------------------------------------------------------------------------------------------------------------
--	Create Distribution.eBiz_ErrorType
--      a. Check Constraints    
--              - CK_Distribution_eBiz_ErrorType__Description
--      b. Foreign Keys
--				- None
-----------------------------------------------------------------------------------------------------------------------
create table Distribution.eBiz_ErrorType
(
    ErrorTypeID   int identity(0,1) not null,
    Description   varchar(2000) not null,
    
    constraint PK_Distribution_eBiz_ErrorType 
    primary key (ErrorTypeID),
    
    constraint CK_Distribution_eBiz_ErrorType__Description
    check (len(Description) > 0)
)
go

insert into Distribution.eBiz_ErrorType (Description) values ('Unrecognized')
insert into Distribution.eBiz_ErrorType (Description) values ('Vehicle Does Not Exist')
insert into Distribution.eBiz_ErrorType (Description) values ('Invalid Vendor Authorization')
insert into Distribution.eBiz_ErrorType (Description) values ('Invalid Account Authorization')
insert into Distribution.eBiz_ErrorType (Description) values ('Only One Item Allowed')
insert into Distribution.eBiz_ErrorType (Description) values ('Internal Error')
insert into Distribution.eBiz_ErrorType (Description) values ('Bad ID Parameter')
go


-----------------------------------------------------------------------------------------------------------------------
--	Create Distribution.eBiz_Error
--      a. Check Constraints    
--              - None
--      b. Foreign Keys
--				- FK_Distribution_eBizError__SubmissionError
--              - FK_Distribution_eBizError__eBiz_ErrorType
-----------------------------------------------------------------------------------------------------------------------
create table Distribution.eBiz_Error
(
    ErrorID       int not null,
    ErrorTypeID   int not null,
    ErrorMessage  varchar(2000) null,
    
    constraint PK_Distribution_eBizError 
    primary key (ErrorID),
    
    constraint FK_Distribution_eBizError__SubmissionError 
    foreign key (ErrorID)
    references Distribution.SubmissionError (ErrorID),
    
    constraint FK_Distribution_eBizError__eBiz_ErrorType 
    foreign key (ErrorTypeID)
    references Distribution.eBiz_ErrorType (ErrorTypeID)    
)
go
