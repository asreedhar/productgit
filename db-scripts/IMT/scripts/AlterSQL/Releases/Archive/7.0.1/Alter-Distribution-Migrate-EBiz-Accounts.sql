-- Move everyone with an account with eBiz into the Distribution.Account table.
-- CGC 04/11/2011

DECLARE @ProviderID INT
SET @ProviderID = 5 -- eBiz in the Distribution.Provider table.

DECLARE @Accounts TABLE 
(	
	BusinessUnitID int not null,  -- Maps to DealerID in Distribution.Account
	InternetAdvertisersDealershipCode varchar(50) null,  -- Will map to DealerCode in Distribution.Account
	ExportGuid varchar(36) null,  -- Maps to Password in Distribution.Account
	Active bit not null -- Maps to Active in Distribution.Account
)
	
-- Get the eBiz dealers.	
INSERT INTO @Accounts
SELECT
	BusinessUnitID,	
	InternetAdvertisersDealershipCode,
	ExportGuid,
	CASE WHEN Active = 1 AND IncrementalExport = 1 THEN 1 ELSE 0 END AS Active
FROM 
	dbo.InternetAdvertiserDealership
WHERE
	InternetAdvertiserID = 3686 -- eBiz in dbo.ThirdPartyEntity

-- Disable triggers.	
ALTER TABLE Distribution.Account
DISABLE TRIGGER TR_I_Account

ALTER TABLE Distribution.Account
DISABLE TRIGGER TR_D_Account

ALTER TABLE Distribution.Account
DISABLE TRIGGER TR_U_Account

-- Clear out any existing eBiz account data, though there shouldn't be any.
DELETE FROM Distribution.Account
WHERE ProviderID = @ProviderID

-- Insert account info.
INSERT INTO Distribution.Account (DealerID, ProviderID, DealershipCode, Password, Active, InsertUserID, InsertDate)
SELECT
	BusinessUnitID,
	@ProviderID,
	InternetAdvertisersDealershipCode,
	ExportGuid,
	Active,
	100000,
	getdate()
FROM
	@Accounts
	
-- Clear out any eBiz account preferences, though there shouldn't be any.
DELETE FROM Distribution.AccountPreferences
WHERE ProviderID = @ProviderID	
	
-- Insert Price preference.	
INSERT INTO Distribution.AccountPreferences (DealerID, ProviderID, MessageTypes)
SELECT
	BusinessUnitID,
	@ProviderID,
	1 -- Price
FROM
	@Accounts
	
-- Insert Description preference.
--INSERT INTO Distribution.AccountPreferences (DealerID, ProviderID, MessageTypes)
--SELECT
--	BusinessUnitID,
--	@ProviderID,
--	2 -- Description
--FROM
--	@Accounts
	
-- Insert  Price + Description preference.	
--INSERT INTO Distribution.AccountPreferences (DealerID, ProviderID, MessageTypes)
--SELECT
--	BusinessUnitID,
--	@ProviderID,
--	3 -- Price + Description
--FROM
--	@Accounts		

-- Reenable triggers.
ALTER TABLE Distribution.Account
ENABLE TRIGGER TR_U_Account		
	
ALTER TABLE Distribution.Account
ENABLE TRIGGER TR_D_Account	
	
ALTER TABLE Distribution.Account
ENABLE TRIGGER TR_I_Account	

GO
