
/* remove entries for which the user does not have access */

DELETE	I
FROM	dbo.MemberBusinessUnitSetItem I
JOIN	dbo.MemberBusinessUnitSet S ON S.MemberBusinessUnitSetID = I.MemberBusinessUnitSetID
JOIN	dbo.Member M ON S.MemberID = M.MemberID
WHERE	NOT EXISTS (
			SELECT	1
			FROM	dbo.MemberAccess MA
			WHERE	MA.MemberID = S.MemberID AND MA.BusinessUnitID = I.BusinessUnitID
		)
AND		M.MemberType = 2
