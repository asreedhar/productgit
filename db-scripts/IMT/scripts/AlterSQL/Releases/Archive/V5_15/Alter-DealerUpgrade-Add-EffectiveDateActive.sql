------------------------------------------------------------------------------------
--	ADD A COMPUTED COLUMN THAT CHECKS THE UPGRADE TIME PERIOD AND THE ACTIVE
--	FLAG TO DETERMINE IF AN UPGRADE IS "ACTIVE".  DOWN THE ROAD, REPLACE THE
--	CURRENT Active COLUMN WITH THIS COMPUTED COLUMN.
------------------------------------------------------------------------------------


ALTER TABLE DealerUpgrade ADD EffectiveDateActive AS (CAST(CASE WHEN DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0)
						                     BETWEEN DATEADD(DD, DATEDIFF(DD, 0, COALESCE(StartDate,GETDATE())), 0)
						                        AND DATEADD(DD, DATEDIFF(DD, 0, COALESCE(EndDate,GETDATE())), 0)
			                                        THEN 1
                                                                ELSE 0
                                                            END & Active AS TINYINT)
							)
GO