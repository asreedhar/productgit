------------------------------------------------------------------------------------------------
--	FROM THE MAPPING TABLE, FIND THE COMBINATIONS THAT HAVE ONE DISTINCT MAPPING
--	FROM OLD TO NEW.  THOSE ARE THE UPDATE CANDIDATES.  
------------------------------------------------------------------------------------------------

UPDATE	TPV
SET     Description		= CAST(NewModelDisplayName + ' ' + NewTrimTrimName AS VARCHAR(65)),
        ThirdPartyVehicleCode	= CAST(NewVehicleID AS VARCHAR),
        ModelCode		= CAST(NewModelId AS VARCHAR),
        Model			= CAST(NewModelDisplayName AS VARCHAR(25)),
        Body			= CAST(NewTrimTrimName AS VARCHAR(50))
          
FROM	dbo.Bookouts B
	INNER JOIN dbo.BookoutThirdPartyVehicles BTPV ON B.BookoutID = BTPV.BookoutID
	INNER JOIN dbo.ThirdPartyVehicles TPV ON BTPV.ThirdPartyVehicleID = TPV.ThirdPartyVehicleID
	INNER JOIN dbo.InventoryBookouts IB ON B.BookoutID = IB.BookoutID
	INNER JOIN dbo.Inventory I ON IB.InventoryID = I.InventoryID
	
	INNER JOIN (	SELECT	OldModelId, OldVehicleID, OldTrimDisplayName, 
				NewModelId		= MAX(NewModelId), 
				NewVehicleID		= MAX(NewVehicleId), 
				NewModelDisplayName	= MAX(NewModelDisplayName),
				NewTrimTrimName		= MAX(NewTrimTrimName)
			FROM	KBB.Migration.ModelTrimMapping
			WHERE	NewVehicleId IS NOT null
			GROUP
			BY	OldModelId, OldVehicleID, OldTrimDisplayName
			HAVING	COUNT(*) = 1
			) M ON TPV.ModelCode = CAST(M.OldModelId AS VARCHAR) AND TPV.ThirdPartyVehicleCode = CAST(M.OldVehicleId  AS VARCHAR) 
				AND TPV.Body = M.OldTrimDisplayName
			
WHERE	B.ThirdPartyID = 3
	AND I.InventoryActive = 1

------------------------------------------------------------------------------------------------
--	FIRST, GET THE SET OF DISTINCT TPO'S FROM KELLY.  UPDATE W/MAPPINGS AFTERWARDS.
------------------------------------------------------------------------------------------------

SELECT	DISTINCT TPO.*
INTO	#TPO
FROM	dbo.Bookouts B
	INNER JOIN dbo.BookoutThirdPartyVehicles BTPV ON B.BookoutID = BTPV.BookoutID
	INNER JOIN dbo.InventoryBookouts IB ON B.BookoutID = IB.BookoutID
	INNER JOIN dbo.Inventory I ON IB.InventoryID = I.InventoryID
	
	INNER JOIN dbo.ThirdPartyVehicles TPV ON BTPV.ThirdPartyVehicleID = TPV.ThirdPartyVehicleID
	INNER JOIN dbo.ThirdPartyVehicleOptions TPVO ON TPV.ThirdPartyVehicleID = TPVO.ThirdPartyVehicleID
	INNER JOIN dbo.ThirdPartyOptions TPO ON TPVO.ThirdPartyOptionID = TPO.ThirdPartyOptionID

WHERE	B.ThirdPartyID = 3
	AND I.InventoryActive = 1

UPDATE	TPO
SET	OptionName	= OM.NewVehicleOptionDisplayName
FROM	dbo.ThirdPartyOptions TPO
	INNER JOIN #TPO U ON TPO.ThirdPartyOptionID = U.ThirdPartyOptionID
	INNER JOIN KBB.Migration.OptionMapping OM ON TPO.OptionKey = CAST(OM.OldVehicleOptionVehicleOptionID AS VARCHAR) 
	
