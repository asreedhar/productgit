ALTER TABLE dbo.ThirdPartyVehicleOptions DROP CONSTRAINT DF_ThirdPartyVehicleOptions_SortOrder

ALTER TABLE dbo.ThirdPartyVehicleOptions ALTER COLUMN SortOrder INT NULL

ALTER TABLE dbo.ThirdPartyVehicleOptions ADD CONSTRAINT [DF_ThirdPartyVehicleOptions_SortOrder] DEFAULT (0) FOR SortOrder

GO