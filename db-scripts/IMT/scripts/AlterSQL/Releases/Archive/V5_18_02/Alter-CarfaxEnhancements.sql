--*****************************************************************	
--  Migrates old carFax report data to a new table with a new schema
--*****************************************************************	

--- create a tmp table to store data during migration
CREATE TABLE dbo.Tmp_CarfaxAvailability
	(
	CarfaxReportId INT IDENTITY(1,1) NOT NULL,
	BusinessUnitId INT NOT NULL,
 	Vin VARCHAR(250) NOT NULL,
	UserName VARCHAR(250) NOT NULL,
	ReportType CHAR(3) NOT NULL,
	Expires SMALLDATETIME NOT NULL
	)  ON [PRIMARY]
GO

--- migrate old data in CarfaxAvailability to the tmp table
INSERT INTO dbo.Tmp_CarfaxAvailability (BusinessUnitId, UserName, Vin, ReportType, Expires)
(
SELECT    distinct i.businessUnitId, mc.userName,v.vin, ca.ReportType, ca.Expires
FROM     Inventory i 
                JOIN Vehicle v on v.vehicleId=i.vehicleId
                JOIN dbo.CarfaxAvailability ca on ca.vin=v.vin
                JOIN memberCredential mc ON ca.userName = mc.userName
                JOIN memberAccess ma ON mc.memberId = ma.memberid AND i.businessUnitId = ma.businessUnitId

WHERE   i.inventoryType = 2
                AND i.InventoryActive = 1
                AND mc.userName != '' 
                AND mc.userName is NOT NULL
                AND mc.credentialTypeId=1
)
GO

--- DROP the old CarfaxAvailability including its index
DROP TABLE dbo.CarfaxAvailability
GO

--- Renaming CarfaxAvailability to comething sensible: CarfaxReport

EXECUTE sp_rename N'dbo.Tmp_CarfaxAvailability', N'CarfaxReport', 'OBJECT'
GO

--- Add new columns
ALTER TABLE dbo.CarfaxReport ADD Problem [varchar](1) NULL
ALTER TABLE dbo.CarfaxReport ADD OneOwner TINYINT NULL
GO

--- Build a new index on BuId and Vin
CREATE INDEX IX_CarfaxReport_UserNameVIN 
	on dbo.CarfaxReport
	(BusinessUnitId, VIN) 
	on IDX --specify filegroup
GO

