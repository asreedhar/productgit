----------------------------------------------------------------------------
--	CREATE A UTILITY SCHEMA FOR NON-APPLICATION PROCS SUCH AS
--	DeleteBusinessUnit.
----------------------------------------------------------------------------

CREATE SCHEMA Utility
GO

use [IMT]
GO
DENY CONTROL ON SCHEMA::[Utility] TO [firstlook]
GO
use [IMT]
GO
DENY EXECUTE ON SCHEMA::[Utility] TO [firstlook]
GO
use [IMT]
GO
DENY REFERENCES ON SCHEMA::[Utility] TO [firstlook]
GO
use [IMT]
GO
DENY TAKE OWNERSHIP ON SCHEMA::[Utility] TO [firstlook]
GO
