
/****** Object:  Table [dbo].[CarfaxRestrictedMembers]    Script Date: 10/28/2008 16:25:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CarfaxRestrictedMembers](
	[BusinessUnitID] [int] NOT NULL,
	[MemberID] [int] NOT NULL,
 CONSTRAINT [PK_CarfaxRestrictedMembers] PRIMARY KEY CLUSTERED 
(
	[BusinessUnitID] ASC,
	[MemberID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [DATA]
) ON [DATA]

GO
USE [IMT]
GO
ALTER TABLE [dbo].[CarfaxRestrictedMembers]  WITH CHECK ADD  CONSTRAINT [FK_CarfaxRestrictedMembers__BusinessUnit] FOREIGN KEY([BusinessUnitID])
REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO
ALTER TABLE [dbo].[CarfaxRestrictedMembers]  WITH CHECK ADD  CONSTRAINT [FK_CarfaxRestrictedMembers__Member] FOREIGN KEY([MemberID])
REFERENCES [dbo].[Member] ([MemberID])

GO
CREATE TABLE [dbo].[DealerPreference_Carfax](
	[BusinessUnitID] [int] NOT NULL,
	[CarfaxUsername] [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CarfaxPassword] [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AutoRunInventory] [tinyint] NOT NULL DEFAULT ((1)),
	[AutoRunAppraisal] [tinyint] NOT NULL DEFAULT ((0)),
	[ListCarfaxInventoryVins] [tinyint] NOT NULL DEFAULT ((1)),
	[ListCarfaxAppraisalVins] [tinyint] NOT NULL DEFAULT ((0)),
	[DefaultInventoryReportType] [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL DEFAULT ('VHR'),
	[DefaultAppraisalReportType] [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL DEFAULT ('VHR'),
 CONSTRAINT [PK_DP_Carfax_BusinessUnitId] PRIMARY KEY CLUSTERED 
(
	[BusinessUnitID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [DATA]
) ON [DATA]

GO
SET ANSI_PADDING OFF
GO
USE [IMT]
GO
ALTER TABLE [dbo].[DealerPreference_Carfax]  WITH CHECK ADD  CONSTRAINT [FK_DP_Carfax_BusinessUnitId] FOREIGN KEY([BusinessUnitID])
REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
