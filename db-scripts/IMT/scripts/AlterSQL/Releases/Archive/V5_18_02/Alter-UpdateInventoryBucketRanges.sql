/*  This script updates any Watch List Bucket Ranges (High) with the Bucket #1 value */

UPDATE	IBR1
SET	IBR1.High = IBR2.High
FROM	InventoryBucketRanges IBR1
	JOIN InventoryBucketRanges IBR2 ON IBR1.BusinessUnitID = IBR2.BusinessUnitID AND IBR1.InventoryBucketID = IBR2.InventoryBucketID
WHERE	IBR1.InventoryBucketID = 4
        AND IBR1.RangeID = 1
        AND IBR2.RangeID = 2
        AND IBR1.High <> IBR2.High
        