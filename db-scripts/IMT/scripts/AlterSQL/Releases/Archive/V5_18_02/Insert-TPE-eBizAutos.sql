DECLARE @NewID INT

INSERT ThirdPartyEntity
(
	BusinessUnitID,
	ThirdPartyEntityTypeID,
	Name
)
VALUES
(
	100150,			-- Firstlook default BusinessUnitID
	6,				-- Photo Provider ID
	'eBizAutos'
)

SET @NewID = SCOPE_IDENTITY()

INSERT PhotoProvider_ThirdPartyEntity
(
	PhotoProviderID,
	DatafeedCode,
	PhotoStorageCode
)
VALUES
(
	@NewID,
	'eBizAutosImage',
	2
)
GO