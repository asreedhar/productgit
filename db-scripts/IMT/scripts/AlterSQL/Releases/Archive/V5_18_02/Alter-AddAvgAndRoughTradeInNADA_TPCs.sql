---
--- nk 11/18/08 : New Average and Rough trade-in values for NADA.
---
---

INSERT INTO dbo.ThirdPartyCategories
VALUES (2, 'Average Trade-In')

INSERT INTO dbo.ThirdPartyCategories
VALUES (2, 'Rough Trade-In')

-- change display names
UPDATE	dbo.ThirdPartyCategories
SET		Category='Clean Retail'
WHERE	ThirdPartyCategoryId=1

UPDATE	dbo.ThirdPartyCategories
SET		Category='Clean Trade-In'
WHERE	ThirdPartyCategoryId=2

UPDATE	dbo.ThirdPartyCategories
SET		Category='Clean Loan'
WHERE	ThirdPartyCategoryId=3
GO