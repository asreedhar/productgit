UPDATE dbo.DMSExportPriceMapping 
SET Description = 'CODED_COST'
WHERE DMSExportPriceMappingID = 10

UPDATE dbo.DMSExportPriceMapping 
SET Description = 'LIST_PRICE'
WHERE DMSExportPriceMappingID = 5

UPDATE dbo.DMSExportPriceMapping 
SET Description = 'DRAFT_AMOUNT'
WHERE DMSExportPriceMappingID = 13

UPDATE dbo.DMSExportPriceMapping 
SET Description = 'STICKER_PRICE'
WHERE DMSExportPriceMappingID = 7