-- insert new subscriptiontype
INSERT INTO [IMT].[dbo].[SubscriptionTypes]
           ([SubscriptionTypeID]
           ,[Description]
           ,[Notes]
           ,[UserSelectable]
           ,[DefaultSubscriptionFrequencyDetailID]
           ,[Active])
     VALUES
           (15
           ,'Group Make-A-Deal Alert'
           ,'Group Make-A-Deal Alert - not supported by admin'
           ,0
           ,4
           ,0)


-- link to MAX upgrade
INSERT INTO [IMT].[dbo].[SubscriptionTypeDealerUpgrade]
           ([SubscriptionTypeID]
           ,[DealerUpgradeCD])
     VALUES
           (15
           ,15)

-- link to MAX-text upgrade
INSERT INTO [IMT].[dbo].[SubscriptionTypeDealerUpgrade]
           ([SubscriptionTypeID]
           ,[DealerUpgradeCD])
     VALUES
           (15
           ,16)

-- subscription type frequency
DECLARE @CurrentSubscriptionFrequencyDetailID INT
SET @CurrentSubscriptionFrequencyDetailID = 1

WHILE @CurrentSubscriptionFrequencyDetailID < 11
BEGIN
	INSERT INTO [IMT].[dbo].[SubscriptionTypeSubscriptionFrequencyDetail]
			   ([SubscriptionTypeID]
			   ,[SubscriptionFrequencyDetailID])
		 VALUES
			   (15
			   ,@CurrentSubscriptionFrequencyDetailID)

	SET @CurrentSubscriptionFrequencyDetailID = 
		@CurrentSubscriptionFrequencyDetailID + 1
END

-- subscription type format
INSERT INTO [IMT].[dbo].[SubscriptionTypeSubscriptionDeliveryType]
           ([SubscriptionTypeID]
           ,[SubscriptionDeliveryTypeID])
     VALUES
           (15
           ,1) --only support email


