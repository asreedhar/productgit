if not exists (select * from syscolumns where id=object_id('dbo.DMSExportBusinessUnitPreference') and name='AreaNumber')
ALTER TABLE dbo.DMSExportBusinessUnitPreference ADD AreaNumber INT NULL

if not exists (select * from syscolumns where id=object_id('dbo.DMSExportBusinessUnitPreference') and name='ReynoldsDealerNo')
ALTER TABLE dbo.DMSExportBusinessUnitPreference ADD ReynoldsDealerNo INT NULL


-- Custom Mapping Codes for Reynolds Direct

INSERT INTO dbo.DMSExportPriceMapping
VALUES('LOCATION')