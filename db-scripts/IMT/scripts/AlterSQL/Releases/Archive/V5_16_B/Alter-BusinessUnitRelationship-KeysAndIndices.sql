/****************************/
/* Get rid of bogus entries */
/****************************/
WITH InvalidRelationships AS (
	SELECT BusinessUnitID
	FROM BusinessUnitRelationship
	GROUP BY BusinessUnitID
	HAVING COUNT(*) > 1
)
DELETE FROM BusinessUnitRelationship
WHERE BusinessUnitID IN (
	SELECT *
	FROM InvalidRelationships
)
AND ParentID = 100150 --FIRSTLOOK

-- Are there other BU's with multiple parentIDs?

/****************************/
/* Drop Key and Indices     */
/****************************/

-- drop primary key
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[BusinessUnitRelationship]') AND name = N'PK_BusinessUnitRelationship')
ALTER TABLE [dbo].[BusinessUnitRelationship] DROP CONSTRAINT [PK_BusinessUnitRelationship]
GO

-- add primary key with clustered index on BusinessUnitID
ALTER TABLE [dbo].[BusinessUnitRelationship] 
ADD CONSTRAINT [PK_BusinessUnitRelationship] PRIMARY KEY CLUSTERED (
	[BusinessUnitID] ASC
)
GO

-- Add index on ParentID
CREATE NONCLUSTERED INDEX [IX_BusinessUnitRelationship_ParentID] ON [dbo].[BusinessUnitRelationship] 
(
	[ParentID] ASC
)
GO
