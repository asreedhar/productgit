---
--- nk : Rename WindowSticerPrice to LotPrice
---

EXEC sp_rename 
    @objname = 'Inventory.WindowStickerPrice', 
    @newname = 'LotPrice', 
    @objtype = 'COLUMN'
GO  	
ALTER TABLE dbo.Inventory DROP CONSTRAINT DF_Inventory__WindowStickerPrice    	
GO
ALTER TABLE dbo.Inventory ADD CONSTRAINT DF_Inventory__LotPrice DEFAULT (0) FOR LotPrice
GO


EXEC sp_rename 
	@objname = 'DMSExportBusinessUnitPreference.DMSExportWindowStickerPriceMappingID', 
	@newname = 'DMSExportLotPriceMappingID', 
	@objtype = 'COLUMN'
GO
ALTER TABLE dbo.DMSExportBusinessUnitPreference DROP CONSTRAINT FK_DMSExportBusinessUnitPreference_DMSExportWindowStickerPriceMapping   	
GO
ALTER TABLE [dbo].[DMSExportBusinessUnitPreference]  WITH CHECK ADD CONSTRAINT [FK_DMSExportBusinessUnitPreference_DMSExportLotPriceMapping] FOREIGN KEY([DMSExportLotPriceMappingID])
REFERENCES [dbo].DMSExportPriceMapping (DMSExportPriceMappingID)
GO

EXEC sp_rename 
	@objname = 'DealerPreference.UseWindowStickerPrice', 
	@newname = 'UseLotPrice', 
	@objtype = 'COLUMN'
GO
ALTER TABLE dbo.DealerPreference DROP CONSTRAINT DF__DealerPre__UseWi__0432F173   	
GO
ALTER TABLE dbo.DealerPreference ADD CONSTRAINT DF_DealerPreference__UseLotPrice DEFAULT (0) FOR UseLotPrice
GO

    	
