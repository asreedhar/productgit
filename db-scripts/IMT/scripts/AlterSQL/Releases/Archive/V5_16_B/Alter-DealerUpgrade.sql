INSERT INTO lu_DealerUpgrade VALUES (20, 'EDMUNDS_TMV')
GO

INSERT INTO DealerUpgrade (BusinessUnitId, DealerUpgradeCD, StartDate, EndDate, Active)
(SELECT businessUnitId, 20, GetDate(), NULL, 1 
FROM DealerPreference
WHERE DisplayTMV=1)
GO 

INSERT INTO DealerUpgrade (BusinessUnitId, DealerUpgradeCD, StartDate, EndDate, Active)
(SELECT businessUnitId, 20, GetDate(), NULL, 1 
FROM dbo.DealerGroupPreference
WHERE DisplayTMV=1)
GO 

INSERT INTO DealerUpgrade (BusinessUnitId, DealerUpgradeCD, StartDate, EndDate, Active)
(SELECT businessUnitId, 20, GetDate(), NULL, 1 
FROM dbo.DealerGroupPreference
WHERE lithiaStore=1)
GO

ALTER TABLE DealerPreference DROP CONSTRAINT DF__DealerPre__Displ__310FB98B
ALTER TABLE DealerPreference DROP COLUMN DisplayTMV
GO

ALTER TABLE DealerGroupPreference DROP CONSTRAINT DF__DealerGro__Displ__301B9552
ALTER TABLE DealerGroupPreference DROP COLUMN DisplayTMV
GO
