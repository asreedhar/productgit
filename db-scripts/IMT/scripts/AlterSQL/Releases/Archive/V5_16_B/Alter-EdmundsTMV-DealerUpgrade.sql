----------------------------------------------------------------------------
--	THIS ALTER FIXES PROBLEMS IN Alter-DealerUpgrade.sql
----------------------------------------------------------------------------

INSERT INTO DealerUpgrade (BusinessUnitId, DealerUpgradeCD, StartDate, EndDate, Active)
SELECT	DISTINCT BUR.BusinessUnitID, DealerUpgradeCD, StartDate, EndDate, Active
FROM	DealerUpgrade DU
	JOIN dbo.BusinessUnitRelationship BUR ON DU.BusinessUnitID = BUR.ParentID
	
WHERE	DealerUpgradeCD = 20 AND DU.BusinessUnitID IN (	SELECT	BusinessUnitId
							FROM	dbo.DealerGroupPreference
							WHERE	lithiaStore=1)
							
	AND NOT EXISTS (SELECT 1 FROM DealerUpgrade DU2 WHERE BUR.BusinessUnitID = DU2.BusinessUnitID AND DU2.DealerUpgradeCD = 20)


DELETE FROM DealerUpgrade 
WHERE	DealerUpgradeCD = 20 AND BusinessUnitID IN (	SELECT	BusinessUnitId
							FROM	dbo.DealerGroupPreference
							WHERE	lithiaStore=1)

							
GO