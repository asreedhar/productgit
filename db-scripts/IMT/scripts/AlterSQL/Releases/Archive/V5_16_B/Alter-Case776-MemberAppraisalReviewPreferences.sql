CREATE TABLE dbo.MemberAppraisalReviewPreferences (
	MemberAppraisalReviewPreferenceID INT IDENTITY(1,1) NOT NULL 
		CONSTRAINT [PK_MemberAppraisalReviewPreferences] PRIMARY KEY NONCLUSTERED
	, MemberID INT NOT NULL
		CONSTRAINT [FK_MemberAppraisalReviewPreferences_MemberID] FOREIGN KEY
		REFERENCES dbo.Member(MemberID)
	, BusinessUnitID INT NOT NULL -- Can be DealerGroup or Dealer!  PMC usage is dealergoupid
		CONSTRAINT [FK_MemberAppraisalReviewPreferences_BusinessUnitID] FOREIGN KEY  
		REFERENCES dbo.BusinessUnit(BusinessUnitID)
	, DaysBack INT NOT NULL 
		CONSTRAINT [DF_MemberAppraisalReviewPreferences_DaysBack] DEFAULT(10)
	, CONSTRAINT [UK_MemberappraisalReviewPreferenceList] UNIQUE CLUSTERED (
		MemberID ASC, BusinessUnitID ASC
	)
)
