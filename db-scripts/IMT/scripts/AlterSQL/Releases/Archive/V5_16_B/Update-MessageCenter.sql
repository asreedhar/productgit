UPDATE     dbo.MessageCenter
SET     HasPriority = 0,
     Ordering = Ordering + 1
GO

INSERT
INTO     dbo.MessageCenter (Ordering, Title, Body, HasPriority)
SELECT     1,     
'<div class="title">
<h3 style="display:block;color: #cd1f26;border-bottom:1px dashed #CCC;margin-right:0px;">
<strong><span style="font-size:14px; color:#FDD017;">JUST RELEASED!</span><br />PINGII Enhancements with DMS Writeback </strong></h3>
</div>',
'<div class="releaseNotes"><font color="#000">
<ul style="border-top:0px;">
<li>Improved Appraisal Calculator in PINGII (access from Trade Analyzer) </li>
<li>Improved 360&deg; Pricing with QUICK access to potential profit </li>
<li>One-click access to Auction Data including NAAA and MMR </li>
<li>Access more details on Market Listings (VIN, Stock#, Notes) </li>
<li>Automatically matches equipment when the market has sufficient listings</li> 
<li>Add Pricing Notes to your vehicle and print reports with notes </li>
<li>Adjust your Internet Price by Rank </li>
<li>Easier access to PINGII from the Inventory Pricing Analyzer: The entire vehicle is clickable </li>
<li>Writeback your price changes to ADP & Reynolds</li> 
</ul>
<p style="margin-top:3px;"><strong>Sign Up for Over and Under Pricing Alerts <a href="javascript:openMemberProfileWindow();">Here</a></strong><br />
Override pricing alerts if vehicle is priced right.</p>
</div>',
1
GO

UPDATE	MessageCenter
SET Title = 
'<div class="title" style="border:0;margin-bottom:0;padding-bottom:0;"><p><h3 style="display:block;color: #cd1f26;border-bottom:1px dashed #CCC;margin-right:0px;"><strong><span style="font-size:14px; color:#FDD017;">JUST RELEASED!</span><br/>PINGII Enhancements with DMS Writeback</strong></h3></div>',
Body = 
'<div class="title" style="border:0;margin-top:0;padding-top:0;">
	<span style="font-size:12px;">
	<ul style="border-top:0;">
	<li style="list-style-image:url(..img);list-style:disc;">Improved Appraisal Calculator in PINGII (access from Trade Analyzer)</li>
	<li style="list-style-image:url(..img);list-style:disc;">Improved 360&deg; Pricing with QUICK access to potential profit</li>
	<li style="list-style-image:url(..img);list-style:disc;">One-click access to Auction Data including NAAA and MMR</li>
	<li style="list-style-image:url(..img);list-style:disc;">Access more details on Market Listings (VIN, Stock#, Notes)</li>
	<li style="list-style-image:url(..img);list-style:disc;">Automatically matches equipment when the market has sufficient listings</li>
	<li style="list-style-image:url(..img);list-style:disc;">Add Pricing Notes to your vehicle and print reports with notes</li>
	<li style="list-style-image:url(..img);list-style:disc;">Adjust your Internet Price by Rank</li>
	<li style="list-style-image:url(..img);list-style:disc;">Easier access to PINGII from the Inventory Pricing Analyzer: The entire vehicle is clickable</li>
	<li style="list-style-image:url(..img);list-style:disc;">Writeback your price changes to ADP & Reynolds</li>
	</ul></span>
	</p>
	<p style="margin-top:4px;"><strong>Sign Up for Over and Under Pricing Alerts <a href="javascript:openMemberProfileWindow();">Here</a></strong>
	- Override pricing alerts if vehicle is priced right.</p>
</div>'
WHERE	Ordering = 1
GO