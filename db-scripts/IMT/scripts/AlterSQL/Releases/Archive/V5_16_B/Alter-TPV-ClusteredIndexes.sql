
-----------------------------------------------------------------------------------------------------------------------------
--	ThirdPartyVehicleOptionValues
-----------------------------------------------------------------------------------------------------------------------------

ALTER TABLE dbo.ThirdPartyVehicleOptionValues DROP CONSTRAINT PK_VehicleThirdPartyVehicleOptionValues

DROP INDEX ThirdPartyVehicleOptionValues.IX_ThirdPartyVehicleOptionValues__ThirdPartyVehicleOptionID_Value

CREATE CLUSTERED INDEX IX_ThirdPartyVehicleOptionValues__ThirdPartyVehicleOptionID ON ThirdPartyVehicleOptionValues
(	ThirdPartyVehicleOptionID,
	ThirdPartyOptionValueTypeID
	)	

ALTER TABLE dbo.ThirdPartyVehicleOptionValues ADD CONSTRAINT PK_ThirdPartyVehicleOptionValues PRIMARY KEY NONCLUSTERED (ThirdPartyVehicleOptionValueID)
GO
-----------------------------------------------------------------------------------------------------------------------------
--	ThirdPartyVehicleOptions
-----------------------------------------------------------------------------------------------------------------------------
ALTER TABLE dbo.ThirdPartyVehicleOptionValues DROP CONSTRAINT FK_VehicleThirdPartyVehicleOptionValues_VehicleThirdPartyVehicleOptions
GO

ALTER TABLE ThirdPartyVehicleOptions DROP CONSTRAINT PK_VehicleThirdPartyVehicleOptions

DROP INDEX ThirdPartyVehicleOptions.IX_ThirdPartyVehicleOptions__ThirdPartyVehicleID

CREATE CLUSTERED INDEX IX_ThirdPartyVehicleOptions__ThirdPartyVehicleID ON ThirdPartyVehicleOptions(ThirdPartyVehicleID)
GO

ALTER TABLE dbo.ThirdPartyVehicleOptions ADD CONSTRAINT PK_ThirdPartyVehicleOptions PRIMARY KEY NONCLUSTERED (ThirdPartyVehicleOptionID)
GO


ALTER TABLE dbo.ThirdPartyVehicleOptionValues  WITH CHECK ADD  CONSTRAINT FK_VehicleThirdPartyVehicleOptionValues_VehicleThirdPartyVehicleOptions FOREIGN KEY(ThirdPartyVehicleOptionID)
REFERENCES dbo.ThirdPartyVehicleOptions (ThirdPartyVehicleOptionID)
GO
ALTER TABLE dbo.ThirdPartyVehicleOptionValues CHECK CONSTRAINT FK_VehicleThirdPartyVehicleOptionValues_VehicleThirdPartyVehicleOptions
GO

-----------------------------------------------------------------------------------------------------------------------------
--	BookoutThirdPartyVehicles
-----------------------------------------------------------------------------------------------------------------------------

ALTER TABLE dbo.BookoutThirdPartyVehicles DROP CONSTRAINT PK_BookoutThirdPartyVehicles

ALTER TABLE dbo.BookoutThirdPartyVehicles ADD CONSTRAINT PK_BookoutThirdPartyVehicles PRIMARY KEY CLUSTERED (BookoutID, ThirdPartyVehicleID)
GO

ALTER TABLE dbo.BookoutThirdPartyVehicles ADD CONSTRAINT UK_BookoutThirdPartyVehicles UNIQUE (ThirdPartyVehicleID)

DROP INDEX dbo.BookoutThirdPartyVehicles.IX_BookoutThirdPartyVehicles__BookoutID


-----------------------------------------------------------------------------------------------------------------------------
--	BookoutValues
-----------------------------------------------------------------------------------------------------------------------------
ALTER TABLE dbo.BookoutValues DROP CONSTRAINT PK_BookoutValues

DROP INDEX dbo.BookoutValues.IX_BookoutValues_ValueTypeCategoryID

ALTER TABLE dbo.BookoutValues ADD CONSTRAINT PK_BookoutValues PRIMARY KEY CLUSTERED (BookoutThirdPartyCategoryID, BookoutValueTypeID)
GO

ALTER TABLE dbo.BookoutValues ADD CONSTRAINT UK_BookoutValues UNIQUE (BookoutValueID)
CREATE INDEX IX_BookoutValues__BookoutValueID ON BookoutValues(BookoutValueID)
GO

-----------------------------------------------------------------------------------------------------------------------------
--	BookoutThirdPartyCategories
-----------------------------------------------------------------------------------------------------------------------------

ALTER TABLE dbo.BookoutValues DROP CONSTRAINT FK_BookoutValues_BookoutThirdPartyCategories

ALTER TABLE dbo.BookoutThirdPartyCategories DROP CONSTRAINT PK_BookoutThirdPartyCategories
DROP INDEX dbo.BookoutThirdPartyCategories. IX_BookoutThirdPartyCategories_BookoutID

CREATE CLUSTERED INDEX IX_BookoutThirdPartyCategories__BookoutIDThirdPartyCategoryID ON BookoutThirdPartyCategories(BookoutID, ThirdPartyCategoryID)
GO

ALTER TABLE dbo.BookoutThirdPartyCategories ADD CONSTRAINT PK_BookoutThirdPartyCategories PRIMARY KEY NONCLUSTERED (BookoutThirdPartyCategoryID)

ALTER TABLE dbo.BookoutValues WITH CHECK ADD CONSTRAINT FK_BookoutValues__BookoutThirdPartyCategories FOREIGN KEY(BookoutThirdPartyCategoryID) REFERENCES BookoutThirdPartyCategories (BookoutThirdPartyCategoryID)

GO

-----------------------------------------------------------------------------------------------------------------------------
--	InventoryBookouts
-----------------------------------------------------------------------------------------------------------------------------

ALTER TABLE dbo.InventoryBookouts DROP CONSTRAINT PK_InventoryBookouts

DROP INDEX dbo.InventoryBookouts.IX_InventoryBookouts_InventoryID

CREATE CLUSTERED INDEX IX_InventoryBookouts__InventoryIDBookoutID ON InventoryBookouts(InventoryID, BookoutID)

ALTER TABLE dbo.InventoryBookouts ADD CONSTRAINT PK_InventoryBookouts PRIMARY KEY NONCLUSTERED (BookoutID)

GO

-----------------------------------------------------------------------------------------------------------------------------
--	AppraisalBookouts
-----------------------------------------------------------------------------------------------------------------------------

ALTER TABLE dbo.AppraisalBookouts DROP CONSTRAINT PK_AppraisalBookouts

DROP INDEX dbo.AppraisalBookouts.IX_AppraisalBookouts__AppraisalID

CREATE CLUSTERED INDEX IX_AppraisalBookouts__AppraisalIDBookoutID ON AppraisalBookouts(AppraisalID, BookoutID)

ALTER TABLE dbo.AppraisalBookouts ADD CONSTRAINT PK_AppraisalBookouts PRIMARY KEY NONCLUSTERED (BookoutID)

GO