-- update the autotrader mapping for the specific F-F250 MMG

UPDATE 	[IMT]..PING_GroupingCodes
SET 	CodeId=1399
WHERE 	MakeModelGroupingId=103433
AND 	id=1793

-- insert a CDC mapping for the specific F-F250 MMG
INSERT INTO [IMT]..PING_GroupingCodes
VALUES (360, 103433)
