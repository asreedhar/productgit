---
--- nk : Add a new INT column to track which guide book to use when displaying options
---

ALTER TABLE dbo.DealerPreference_Pricing ADD [PingII_GuideBookId] [tinyint] NULL
GO

---  Will let default be NULL for right now, which will auto default to the GuideBookId on DP.

--- UPDATE P
--- SET PingII_GuideBookId = dp.GuideBookId
--- FROM	DealerPreference_Pricing P
---	JOIN DealerPreference dp ON dp.businessUnitID=P.businessUnitId
--- GO


    	
