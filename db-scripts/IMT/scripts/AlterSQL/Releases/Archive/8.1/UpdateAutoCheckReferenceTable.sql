/** PWM 07/25/2011 Update AutoCheck No Accidents Report Item **/

/* ReportInspection */

---------------------------------------------------------------
-- Add "Accident Repair" as condition for "HasAccident", PWM --
---------------------------------------------------------------
UPDATE [IMT].AutoCheck.ReportInspection
SET XPath = '/VHR/VEHICLE/HISTORY/HDATA[@CATEGORY=''Accident Data'' or @CATEGORY=''Title Damage'' or @CATEGORY=''Fire Damage'' or @CATEGORY=''Hail Damage'' or @CATEGORY=''Water Damage'' or @CATEGORY=''Frame Damage'' or @CATEGORY=''Major Damage Incident'' or @CATEGORY=''Accident Repair'']'
WHERE ReportInspectionID = 7
