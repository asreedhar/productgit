ALTER TABLE dbo.DealerPreference
ADD OVEEnabled TINYINT NOT NULL
	CONSTRAINT DF_DealerPreference_OveEnabled DEFAULT(1)
GO