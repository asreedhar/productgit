ALTER TABLE Bookouts DISABLE TRIGGER ALL
ALTER TABLE BookoutValues DISABLE TRIGGER ALL
GO

/********************************************************************
** S973, TA1551
** Bookouts 1 --> * ThirdPartyVehicles
** A Bookout can reference several ThirdPartyVehicles,
** one of which has the 'status' flag set as true
********************************************************************/
CREATE TABLE BookoutThirdPartyVehicles (
	ThirdPartyVehicleID INT NOT NULL 
		CONSTRAINT [PK_BookoutThirdPartyVehicles] 
			PRIMARY KEY ([ThirdPartyVehicleID])
		CONSTRAINT [FK_BookoutThirdPartyVehicles_ThirdPartyVehicles] 
			FOREIGN KEY (ThirdPartyVehicleID)
				REFERENCES ThirdPartyVehicles(ThirdPartyVehicleID)
	, BookoutID INT NOT NULL 
		CONSTRAINT [FK_BookoutThirdPartyVehicles_BookoutID] 
			FOREIGN KEY (BookoutID)
				REFERENCES Bookouts(BookoutID)
	, Legacy_object_id INT NULL
	, Legacy_primary_key INT NULL
--	, CONSTRAINT [UK_BookoutThirdPartyVehicles_LegacyCheck] 
--		UNIQUE( Legacy_object_id, Legacy_primary_key )
)
GO
/********************************************************************
** Put Inventory Records into BookoutThirdPartyVehicles
** S973, TA1554
********************************************************************/
DECLARE @InventoryTableID INT
SELECT @InventoryTableID = object_id 
FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[Inventory]') 
AND type in (N'U')

INSERT INTO BookoutThirdPartyVehicles
SELECT 
	ITPV.ThirdPartyVehicleID
	, B.BookoutID AS InventoryBookoutID
	, @InventoryTableID
	, ITPV.InventoryID
FROM InventoryThirdPartyVehicles ITPV
JOIN ThirdPartyVehicles TPV 
	ON ITPV.ThirdPartyVehicleID = TPV.ThirdPartyVehicleID
JOIN ( --Get the latest bookout for the inventory
	SELECT IB.InventoryID
		, B.ThirdPartyID
		, MAX(B.BookoutID) AS BookoutID
		, MAX(B.DateCreated) AS DateCreated
	FROM InventoryBookouts IB
	JOIN Bookouts B
		ON IB.BookoutID = B.BookoutID
	GROUP BY IB.InventoryID
		, B.ThirdPartyID
	) B ON ITPV.InventoryID = B.InventoryID
		AND TPV.ThirdPartyID = B.ThirdPartyID
GO

/********************************************************************
** Create copy of AppraisalBookouts
********************************************************************/
CREATE TABLE AppraisalBookouts_Migration (
	BookoutID int NOT NULL
		CONSTRAINT [PK_AppraisalBookouts_Migration] 
			PRIMARY KEY CLUSTERED ([BookoutID] ASC) 
				WITH (IGNORE_DUP_KEY = OFF) ON [DATA]
		CONSTRAINT [FK_AppraisalBookouts_Migration_Bookouts]
			FOREIGN KEY([BookoutID])
				REFERENCES Bookouts([BookoutID])
	, AppraisalID int NOT NULL
		CONSTRAINT [FK_AppraisalBookouts_Migration_Appraisals]
			FOREIGN KEY([AppraisalID]) 
				REFERENCES Appraisals([AppraisalID])
) ON [DATA]
GO

INSERT INTO AppraisalBookouts_Migration
SELECT *
FROM AppraisalBookouts
GO

/********************************************************************
** Find the duplicate references from InventoryBookouts
********************************************************************/
DECLARE @Dupes_AppraisalBookouts TABLE(	BookoutID int NOT NULL PRIMARY KEY
											,AppraisalID int NOT NULL )

INSERT INTO @Dupes_AppraisalBookouts
SELECT A.BookoutID, A.AppraisalID
FROM AppraisalBookouts_Migration A
JOIN BookoutThirdPartyVehicles I 
	ON A.BookoutID = I.BookoutID
GROUP BY A.BookoutID, A.AppraisalID 
--BookoutThirdPartyVehicles is 1 bookout, many thirdpartyvehicles
--so we need to do group by!
--------------------------------------------------------------------
-- Do row-by-row copy of Bookouts with cascades
--------------------------------------------------------------------
DECLARE @ProcessBookouts TINYINT,
	--@RowCounter INT,
	@CurrentBookoutID INT,
	@NewBookoutID INT,
	@NewBookoutID_Audit INT, --will help you verify results
	@CurrentBookoutTPCategoryID INT,
	@NewBookoutTPCategoryID INT,
	@CurrentBookoutValueID INT

SET @ProcessBookouts = 1
--SET @RowCounter = 0
SET @NewBookoutID_Audit = 0

-- Retrieve the first row from AppraisalBookouts_Migration
SELECT @CurrentBookoutID = MIN(BookoutID)
FROM  @Dupes_AppraisalBookouts

WHILE @ProcessBookouts = 1
BEGIN
	INSERT INTO Bookouts
		([BookoutSourceID]
		,[ThirdPartyID]
		,[DateCreated]
		,[Region]
		,[DatePublished]
		,[BookPrice]
		,[Weight]
		,[MileageCostAdjustment]
		,[MSRP]
		,[IsAccurate])
	SELECT	BookoutSourceID
			,ThirdPartyID
			,DateCreated
			,Region
			,DatePublished
			,BookPrice
			,Weight
			,MileageCostAdjustment
			,MSRP
			,IsAccurate
	FROM Bookouts
	WHERE BookoutID = @CurrentBookoutID

	SET @NewBookoutID = @@IDENTITY

	-- This ID is the MIN of the first copy
	-- so just select IDs > than this to find all new entries
	IF @NewBookoutID_Audit = 0
		SET @NewBookoutID_Audit = @NewBookoutID

	-- Get the first BookoutThirdPartyCategory for the current Bookout.
	SELECT  @CurrentBookoutTPCategoryID = MIN(BookoutThirdPartyCategoryID)
	FROM    BookoutThirdPartyCategories
	WHERE   BookoutID = @CurrentBookoutID

	WHILE ISNULL(@CurrentBookoutTPCategoryID,0) <> 0
	BEGIN
		INSERT INTO BookoutThirdPartyCategories
			([BookoutID]
			,[ThirdPartyCategoryID]
			,[IsValid]
			,[DateCreated]
			,[DateModified])
		SELECT	@NewBookoutID
				, ThirdPartyCategoryID
				, IsValid
				, DateCreated
				, getdate()
		FROM BookoutThirdPartyCategories
		WHERE BookoutThirdPartyCategoryID = @CurrentBookoutTPCategoryID

		SET @NewBookoutTPCategoryID = @@IDENTITY

		-- Get the first BookoutValue for BookoutThirdPartyCategory
		SELECT @CurrentBookoutValueID = MIN(BookoutValueID)
		FROM BookoutValues
		WHERE BookoutThirdPartyCategoryID = @CurrentBookoutTPCategoryID
		
		WHILE ISNULL(@CurrentBookoutValueID,0) <> 0
		BEGIN			
			INSERT INTO BookoutValues
				([BookoutThirdPartyCategoryID]
				,[BookoutValueTypeID]
				,[ThirdPartySubCategoryID]
				,[Value]
				,[DateCreated]
				,[DateModified])
			SELECT	@NewBookoutTPCategoryID
					, BookoutValueTypeID
					, ThirdPartySubCategoryID
					, Value
					, DateCreated
					, getdate()
			FROM BookoutValues
			WHERE BookoutValueID = @CurrentBookoutValueID

			PRINT 'FROM BookoutID: ' + CAST(@CurrentBookoutID AS VARCHAR(20)) 
					+ ', BookoutTPCategoryID: ' 
					+ CAST(@CurrentBookoutTPCategoryID AS VARCHAR(20))
					+ ', BookoutValueID: ' 
					+ CAST(@CurrentBookoutValueID AS VARCHAR(20))
			PRINT 'TO BookoutID:' 
					+ CAST(@NewBookoutID AS VARCHAR(20))
					+ ', BookoutTPCategoryID: ' 
					+ CAST(@NewBookoutTPCategoryID AS VARCHAR(20))
					+ ', BookoutValueID: ' 
					+ CAST(@@IDENTITY AS VARCHAR(20))

			SELECT @CurrentBookoutValueID = MIN(BookoutValueID)
			FROM BookoutValues
			WHERE BookoutThirdPartyCategoryID = @CurrentBookoutTPCategoryID
			AND BookoutValueID > @CurrentBookoutValueID
		END

		SELECT @CurrentBookoutTPCategoryID = MIN(BookoutThirdPartyCategoryID)
		FROM BookoutThirdPartyCategories
		WHERE BookoutID = @CurrentBookoutID
		AND BookoutThirdPartyCategoryID > @CurrentBookoutTPCategoryID
	END

	UPDATE AppraisalBookouts_Migration 
	SET BookoutID = @NewBookoutID
	WHERE BookoutID = @CurrentBookoutID

	SELECT @CurrentBookoutID = MIN(BookoutID)
	FROM  @Dupes_AppraisalBookouts
	WHERE BookoutID > @CurrentBookoutID

	--SET @RowCounter = @RowCounter + 1
	IF ISNULL(@CurrentBookoutID,0) = 0
	BEGIN
		BREAK
	END
END
GO
--SELECT COUNT(*) AS [dupes], @RowCounter AS [RowsUpdated]
--FROM @Dupes_AppraisalBookouts

/********************************************************************
*********************************************************************
**  Process AppraisalThirdPartyVehicles - do same as Bookout side  **
*********************************************************************
********************************************************************/

/********************************************************************
** Create copy of AppraisalThirdPartyVehicles
********************************************************************/
CREATE TABLE AppraisalThirdPartyVehicles_Migration (
	ThirdPartyVehicleID int NOT NULL
		CONSTRAINT [PK_AppraisalThirdPartyVehicles_Migration] 
			PRIMARY KEY CLUSTERED ([ThirdPartyVehicleID] ASC) 
				WITH (IGNORE_DUP_KEY = OFF) ON [DATA]
		CONSTRAINT [FK_AppraisalThirdPartyVehicles_Migration_TPV]
			FOREIGN KEY([ThirdPartyVehicleID])
				REFERENCES ThirdPartyVehicles([ThirdPartyVehicleID])
	, AppraisalID int NOT NULL
		CONSTRAINT [FK_AppraisalThirdPartyVehicles_Migration_Appraisals]
			FOREIGN KEY([AppraisalID]) 
				REFERENCES Appraisals([AppraisalID])
) ON [DATA]
GO

INSERT INTO AppraisalThirdPartyVehicles_Migration
SELECT *
FROM AppraisalThirdPartyVehicles
GO

/********************************************************************
** Find duplicate ThirdPartyVehicles
********************************************************************/
DECLARE @Dupes_AppraisalThirdPartyVehicles 
TABLE(ThirdPartyVehicleID int NOT NULL PRIMARY KEY, AppraisalID int NOT NULL)

INSERT INTO @Dupes_AppraisalThirdPartyVehicles
SELECT A.ThirdPartyVehicleID, A.AppraisalID
FROM AppraisalThirdPartyVehicles_Migration A
JOIN BookoutThirdPartyVehicles B
	ON A.ThirdPartyVehicleID = B.ThirdPartyVehicleID
--------------------------------------------------------------------
-- Do row-by-row copy of ThirdPartyVehicles with cascades
--------------------------------------------------------------------
DECLARE @ProcessTPV TINYINT,
	@CurrentTpvID INT,
	@NewTpvID INT,
	@CurrentTpvOptionsID INT,
	@NewTpvOptionsID INT,
	@CurrentTpvOptionValuesID INT

SET @ProcessTPV = 1
--SET @RowCounter = 0

-- Retrieve the first row from @Dupes_AppraisalThirdPartyVehicles
SELECT @CurrentTpvID = MIN(ThirdPartyVehicleID)
FROM  @Dupes_AppraisalThirdPartyVehicles

WHILE @ProcessTPV = 1
BEGIN
	INSERT INTO ThirdPartyVehicles
           ([ThirdPartyID],[ThirdPartyVehicleCode]
           ,[Description],[MakeCode]
           ,[ModelCode],[Status]
           ,[DateCreated],[DateModified]
           ,[EngineType],[Make]
           ,[Model],[Body])
	SELECT ThirdPartyID,ThirdPartyVehicleCode
           ,Description,MakeCode
           ,ModelCode,Status
           ,DateCreated,getdate()
           ,EngineType,Make
           ,Model,Body
	FROM ThirdPartyVehicles
	WHERE ThirdPartyVehicleID = @CurrentTpvID

	SET @NewTpvID = @@IDENTITY

	-- Get the first ThirdPartyVehicleOption for the current TPV.
	SELECT  @CurrentTpvOptionsID = MIN(ThirdPartyVehicleOptionID)
	FROM    ThirdPartyVehicleOptions
	WHERE   ThirdPartyVehicleID = @CurrentTpvID

	WHILE ISNULL(@CurrentTpvOptionsID,0) <> 0
	BEGIN
		INSERT INTO ThirdPartyVehicleOptions
           ([ThirdPartyVehicleID]
           ,[ThirdPartyOptionID]
           ,[IsStandardOption]
           ,[Status]
           ,[SortOrder]
           ,[DateCreated]
           ,[DateModified])
		SELECT	@NewTpvID
				, ThirdPartyOptionID
				, IsStandardOption
				, Status
				, SortOrder
				, DateCreated
				, getdate()
		FROM ThirdPartyVehicleOptions
		WHERE ThirdPartyVehicleOptionID = @CurrentTpvOptionsID

		SET @NewTpvOptionsID = @@IDENTITY

		-- Get the first OptionValue for Option
		SELECT @CurrentTpvOptionValuesID = MIN(ThirdPartyVehicleOptionValueID)
		FROM ThirdPartyVehicleOptionValues
		WHERE ThirdPartyVehicleOptionID = @CurrentTpvOptionsID
		
		WHILE ISNULL(@CurrentTpvOptionValuesID,0) <> 0
		BEGIN
			INSERT INTO ThirdPartyVehicleOptionValues
				([ThirdPartyVehicleOptionID]
				,[ThirdPartyOptionValueTypeID]
				,[Value]
				,[DateCreated]
				,[DateModified])
			SELECT	@NewTpvOptionsID
					, ThirdPartyOptionValueTypeID
					, Value
					, DateCreated
					, getdate()
			FROM ThirdPartyVehicleOptionValues
			WHERE ThirdPartyVehicleOptionValueID = @CurrentTpvOptionValuesID

			PRINT 'FROM TPV: ' + CAST(@CurrentTpvID AS VARCHAR(20)) 
					+ ', TPV_Option: ' 
					+ CAST(@CurrentTpvOptionsID AS VARCHAR(20))
					+ ', TPV_OptionValueID: ' 
					+ CAST(@CurrentTpvOptionValuesID AS VARCHAR(20))
			PRINT 'TO TPV_ID:' 
					+ CAST(@NewTpvID AS VARCHAR(20))
					+ ', TPV_Option: ' 
					+ CAST(@NewTpvOptionsID AS VARCHAR(20))
					+ ', TPV_OptionValueID: ' 
					+ CAST(@@IDENTITY AS VARCHAR(20))

			SELECT @CurrentTpvOptionValuesID 
					= MIN(ThirdPartyVehicleOptionValueID)
			FROM ThirdPartyVehicleOptionValues
			WHERE ThirdPartyVehicleOptionID = @CurrentTpvOptionsID
			AND ThirdPartyVehicleOptionValueID > @CurrentTpvOptionValuesID
		END

		SELECT @CurrentTpvOptionsID = MIN(ThirdPartyVehicleOptionID)
		FROM ThirdPartyVehicleOptions
		WHERE ThirdPartyVehicleID = @CurrentTpvID
		AND ThirdPartyVehicleOptionID > @CurrentTpvOptionsID
	END

	UPDATE AppraisalThirdPartyVehicles_Migration 
	SET ThirdPartyVehicleID = @NewTpvID
	WHERE ThirdPartyVehicleID = @CurrentTpvID

	SELECT @CurrentTpvID = MIN(ThirdPartyVehicleID)
	FROM  @Dupes_AppraisalThirdPartyVehicles
	WHERE ThirdPartyVehicleID > @CurrentTpvID

	IF ISNULL(@CurrentTpvID,0) = 0
	BEGIN
		BREAK
	END
END
GO

/*****************************************************************************
** Now Migrate from Appraisal migration tables into BookoutThirdPartyVehicles
** There should be no conflicts/duplicates due to migration tables
*****************************************************************************/
DECLARE @AppraisalTableID INT
SELECT @AppraisalTableID = object_id 
FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[Appraisals]') 
AND type in (N'U')

INSERT INTO BookoutThirdPartyVehicles
SELECT 
	ATPV.ThirdPartyVehicleID
	, B.BookoutID as AppraisalBookoutID
	, @AppraisalTableID
	, ATPV.AppraisalID
FROM AppraisalThirdPartyVehicles_Migration ATPV
JOIN ThirdPartyVehicles TPV
	ON ATPV.ThirdPartyVehicleID = TPV.ThirdPartyVehicleID
JOIN (
	SELECT AB.AppraisalID
		, B.ThirdPartyID
		, MAX(B.BookoutID) AS BookoutID
		, MAX(B.DateCreated) AS DateCreated
	FROM AppraisalBookouts_Migration AB
	JOIN Bookouts B
		ON AB.BookoutID = B.BookoutID
	GROUP BY AB.AppraisalID
		, B.ThirdPartyID
	) B ON ATPV.AppraisalID = B.AppraisalID
		AND TPV.ThirdPartyID = B.ThirdPartyID
GO
/********************************************************************
** Clean up
********************************************************************/
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[AppraisalBookouts_Migration]')
AND type in (N'U'))
DROP TABLE AppraisalBookouts_Migration
GO
IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[AppraisalThirdPartyVehicles_Migration]')
AND type in (N'U'))
DROP TABLE AppraisalThirdPartyVehicles_Migration
GO

/********************************************************************
** DENY Permissions, drop tables later
********************************************************************/
DENY SELECT, INSERT, UPDATE, DELETE
ON AppraisalThirdPartyVehicles
TO firstlook

DENY SELECT, INSERT, UPDATE, DELETE
ON InventoryThirdPartyVehicles
TO firstlook
GO

ALTER TABLE Bookouts ENABLE TRIGGER ALL
ALTER TABLE BookoutValues ENABLE TRIGGER ALL
GO
-- WILL HAVE TO REBUILD THE FLDW TABLES

--------------------------------------------------------------------------------------------
-- POST-DEPLOYMENT ADDITIONS.  SINCE THE TABLES ARE DEAD, DON'T WORRY 'BOUT THE FK RELAT'NS
--------------------------------------------------------------------------------------------
ALTER TABLE AppraisalThirdPartyVehicles NOCHECK CONSTRAINT ALL
ALTER TABLE InventoryThirdPartyVehicles NOCHECK CONSTRAINT ALL
GO