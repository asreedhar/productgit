
CREATE TABLE dbo.VehicleBookoutState(	
	VehicleBookoutStateID INT IDENTITY(1,1) NOT NULL CONSTRAINT [PK_VehicleBookoutState] PRIMARY KEY,
	VIN VARCHAR(17) NOT NULL,
	BusinessUnitID INT NOT NULL CONSTRAINT FK_VehicleBookoutState_BusinessUnit REFERENCES dbo.BusinessUnit(BusinessUnitID),
	DateCreated DATETIME NOT NULL DEFAULT (getdate())
)

GO

CREATE TABLE dbo.VehicleBookoutAdditionalState(	
	VehicleBookoutStateID int NOT NULL CONSTRAINT FK_VehicleBookoutAdditionalState_VehicleBookoutState REFERENCES dbo.VehicleBookoutState(VehicleBookoutStateID) PRIMARY key,
	ThirdPartySubCategoryID INT NOT NULL CONSTRAINT FK_VehicleBookoutAdditionalState_ThirdPartySubCategory REFERENCES dbo.ThirdPartySubCategory(ThirdPartySubCategoryID)
)

GO

CREATE TABLE dbo.VehicleBookoutStateThirdPartyVehicles(	
	ThirdPartyVehicleID INT NOT NULL CONSTRAINT FK_VehicleBookoutStateThirdPartyVehicles_ThirdPartyVehicles REFERENCES dbo.ThirdPartyVehicles(ThirdPartyVehicleID) PRIMARY KEY,
	VehicleBookoutStateID int NOT NULL CONSTRAINT FK_VehicleBookoutStateThirdPartyVehicles_VehicleBookoutState REFERENCES dbo.VehicleBookoutState(VehicleBookoutStateID)
)

GO 