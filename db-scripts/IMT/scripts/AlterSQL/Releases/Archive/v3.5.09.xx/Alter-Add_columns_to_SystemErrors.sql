

if not exists (select * from syscolumns where id=object_id('dbo.SystemErrors') and name='BusinessUnitID')
ALTER TABLE dbo.SystemErrors
	ADD BusinessUnitID INT NULL CONSTRAINT FK_SystemErrors_BusinessUnit FOREIGN KEY (BusinessUnitID) REFERENCES BusinessUnit (BusinessUnitID)
GO

if not exists (select * from syscolumns where id=object_id('dbo.SystemErrors') and name='MemberID')
ALTER TABLE dbo.SystemErrors
	ADD MemberID INT NULL CONSTRAINT FK_SystemErrors_Memeber FOREIGN KEY (MemberID) REFERENCES Member (MemberID)
GO
if not exists (select * from syscolumns where id=object_id('dbo.SystemErrors') and name='HTTPSessionID')
ALTER TABLE dbo.SystemErrors
	ADD HTTPSessionID VARCHAR(127) NULL
GO
if not exists (select * from syscolumns where id=object_id('dbo.SystemErrors') and name='Product')
ALTER TABLE dbo.SystemErrors
	ADD Product VARCHAR(20) NULL
GO
if not exists (select * from syscolumns where id=object_id('dbo.SystemErrors') and name='Source')
ALTER TABLE dbo.SystemErrors
	ADD Source VARCHAR(100) NULL
GO
if not exists (select * from syscolumns where id=object_id('dbo.SystemErrors') and name='HalSessionID')
ALTER TABLE dbo.SystemErrors
	ADD HalSessionID VARCHAR(32) NULL CONSTRAINT FK_SystemErrors_HalSessions FOREIGN KEY (HalSessionID) REFERENCES HalSessions (HalSessionID)
GO