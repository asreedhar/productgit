if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[BookoutStatus]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[BookoutStatus]
GO
 
CREATE TABLE dbo.BookoutStatus (
    BookoutStatusID 	int not null ,
    Description 	varchar(45) not null ,
    IsAccurate		tinyint null,
	CONSTRAINT [PK_BookoutStatus] PRIMARY KEY  CLUSTERED 
	(
		[BookoutStatusID]
	) 
)

GO

INSERT INTO dbo.BookoutStatus VALUES (0, 'inactive', NULL)
INSERT INTO dbo.BookoutStatus VALUES (1, 'clean', 1)
INSERT INTO dbo.BookoutStatus VALUES (2, 'notReviewed', 0)
INSERT INTO dbo.BookoutStatus VALUES (3, 'dmsMileageChange', 0)
INSERT INTO dbo.BookoutStatus VALUES (4, 'userMileageChange', 0)

ALTER TABLE dbo.Bookouts ADD
      BookoutStatusID tinyint NOT NULL CONSTRAINT DF_Bookouts_BookoutStatusID DEFAULT (1) 
GO

--- set all current bookouts which have inaccurate inventory items to status=not reviewed

UPDATE	BO
SET	BO.BookoutStatusID = CASE WHEN IsAccurate = 1 THEN 1 ELSE 2 END
FROM	dbo.Bookouts BO

GO