/*

Implement photo additions and revisions in IMT:
 - Create and populate PhotoStorage
 - Create PhotoProvider_ThirdPartyEntity
 - Add row to ThirdPartyEntityType
 - Create PhotoProviderDealership
 - Create and populate PhotoStatus
 - Create new version of Photos, rename old to zzzPhotos, load data from old to new
 - Create InventoryPhotos, load data from zzzPhotos
 - Create AppraisalPhotos, load data from zzzPhotos
 - Create BusinessUnitPhotos, load data from zzzPhotos
 - Configure HomeNetImage provider


--  Work the Alter_Script table
IF not exists (select 1 from Alter_Script where AlterScriptName = 'Alter-S846-PhotoProviderRevisions.sql')
    INSERT Alter_Script (AlterScriptName)
     values ('Alter-S846-PhotoProviderRevisions.sql')
ELSE
    PRINT 'Already Loaded'

SELECT *
 from Alter_Script
 where AlterScriptName = 'Alter-S846-PhotoProviderRevisions.sql'
 order by ApplicationDate

*/

SET NOCOUNT on

--  CREATE TABLE PhotoStorage
CREATE TABLE dbo.PhotoStorage
 (
   PhotoStorageCode  tinyint       not null
    constraint PK_PhotoStorage
     primary key clustered
  ,Description       varchar(200)  not null
 )

--  Load static data
INSERT PhotoStorage (PhotoStorageCode, Description)
       select 1, 'Photo image files are hosted remotely'
 union select 2, 'Photo image files are retrieved and stored locally'


--  Create table PhotoProvider_ThirdPartyEntity
CREATE TABLE dbo.PhotoProvider_ThirdPartyEntity
 (
   PhotoProviderID   int          not null
    constraint PK_PhotoProvider_ThirdPartyEntity
     primary key clustered
    constraint FK_PhotoProvider_ThirdPartyEntity__ThirdPartyEntity
     foreign key references ThirdPartyEntity (ThirdPartyEntityID)
  ,DatafeedCode      varchar(20)  not null
    constraint UQ_PhotoProvider_ThirdPartyEntity__DatafeedCode
     unique nonclustered
  ,PhotoStorageCode  tinyint      not null
    constraint FK_PhotoProvider_ThirdPartyEntity__PhotoStorage
     foreign key references PhotoStorage (PhotoStorageCode)
 )


--  Load static data
INSERT ThirdPartyEntityType (ThirdPartyEntityTypeID, Description)
 values (6, 'Photo Provider')


--  Create table PhotoProviderDealership
CREATE TABLE dbo.PhotoProviderDealership
 (
   PhotoProviderID               int          not null
    constraint FK_PhotoProviderDealership__PhotoProvider_ThirdPartyEntity
     foreign key references PhotoProvider_ThirdPartyEntity (PhotoProviderID)
  ,BusinessUnitID                int          not null
    constraint FK_PhotoProviderDealership__BusinessUnit
     foreign key references BusinessUnit (BusinessUnitID)
  ,PhotoProvidersDealershipCode  varchar(50)  not null
  ,PhotoStorageCode_Override     tinyint      null    
    constraint FK_PhotoProviderDealership__PhotoStorage
     foreign key references PhotoStorage (PhotoStorageCode)
  ,constraint PK_PhotoProviderDealership
    primary key clustered (PhotoProviderID, BusinessUnitID)
 )


--  Create table PhotoStatus
CREATE TABLE dbo.PhotoStatus
 (
   PhotoStatusCode  tinyint       not null
    constraint PK_PhotoStatus
     primary key clustered
  ,Description      varchar(200)  not null
 )

--  Load static data
INSERT PhotoStatus (PhotoStatusCode, Description)
       select 0, 'Loading...'
 union select 1, 'Stored locally'
 union select 2, 'Stored remotely'
 union select 3, 'To be retrieved for local storage'
 union select 4, 'Failed to retrieve for local storage'
 union select 5, 'Intentionally removed and marked unavailable'
 union select 6, 'To be deleted'
 union select 7, 'Check, copy in local storage may need to be updated'

GO

--  Create the revised Photos table
CREATE TABLE dbo.temp_Photos
 (
   PhotoID           int           not null  identity(1,1)
  ,PhotoTypeID       tinyint       not null
  ,PhotoProviderID   int           null    
    constraint FK_Photos__PhotoProvider_ThirdPartyEntity
     foreign key references PhotoProvider_ThirdPartyEntity (PhotoProviderID)
  ,PhotoStatusCode   tinyint       not null
    constraint FK_Photos__PhotoStatus
     foreign key references PhotoStatus (PhotoStatusCode)
  ,PhotoURL          varchar(200)  not null
  ,OriginalPhotoURL  varchar(200)  not null
  ,DateCreated       datetime      not null
  ,PhotoUpdated      datetime      not null
  ,IsPrimaryPhoto    bit           not null
 )

GO

--  Copy old data over to it
SET IDENTITY_INSERT temp_Photos ON
INSERT temp_Photos
  (
    PhotoID
   ,PhotoTypeID
   ,PhotoProviderID
   ,PhotoStatusCode
   ,PhotoURL
   ,OriginalPhotoURL
   ,DateCreated
   ,PhotoUpdated
   ,IsPrimaryPhoto
  )
 select
   PhotoID
  ,PhotoTypeID
  ,null
  ,1  --  Stored locally
  ,PhotoURL
  ,PhotoURL
  ,DateCreated
  ,DateCreated
  ,0  --  Default to no primary photo
 from Photos
SET IDENTITY_INSERT temp_Photos OFF

GO

--  Flip names
EXECUTE sp_rename 'Photos', 'zzzPhotos'
EXECUTE sp_rename 'temp_Photos', 'Photos'


--  Drop old primary key and foreign key
ALTER TABLE zzzPhotos
 drop constraint PK_Photos

ALTER TABLE zzzPhotos
 drop constraint FK_PhotoTypeID


--  Revise PhotoTypes table: Rename column
EXECUTE sp_rename 'PhotoTypes.description', 'Description', 'Column'

--  Revise PhotoTypes table: Revise type of other column
ALTER TABLE PhotoTypes
 drop constraint PK_PhotoTypes

--  Revise PhotoTypes table: Add "unknown" type
INSERT PhotoTypes (PhotoTypeID, Description)
 values (0, 'Unknown')


ALTER TABLE PhotoTypes
 alter column PhotoTypeID  tinyint  not null

ALTER TABLE PhotoTypes
 add constraint PK_PhotoTypes
  primary key clustered (PhotoTypeID)

GO

--  With that done, we can add constraints to the new table
ALTER TABLE Photos
 add constraint PK_Photos
  primary key clustered (PhotoID)

--  Add in foreign key, now that data type is properly set
ALTER TABLE Photos
 with check add constraint FK_Photos__PhotoTypes
  foreign key (PhotoTypeID) references PhotoTypes (PhotoTypeID)

GO


--  InventoryPhotos
CREATE TABLE dbo.InventoryPhotos
 (
   InventoryID  int  not null
    constraint FK_InventoryPhotos__Inventory
     foreign key references Inventory (InventoryID) 
  ,PhotoID      int  not null
    constraint FK_InventoryPhotos__Photos
     foreign key references Photos (PhotoID)
  ,constraint PK_InventoryPhotos
    primary key clustered (InventoryID, PhotoID)
    with fillfactor = 95
 )

--  Load existing photos
INSERT InventoryPhotos (InventoryID, PhotoID)
 select ParentEntityID, PhotoID
  from zzzPhotos
  where PhotoTypeID = 2  --  Inventory
   and ParentEntityID in (select InventoryID from Inventory)

--  Identify invalid photos
UPDATE Photos
 set PhotoTypeID = 0  --  Unknown
 from Photos ph
  inner join zzzPhotos zzz
   on zzz.PhotoID = ph.PhotoID
  where zzz.PhotoTypeID = 2  --  Inventory
   and zzz.ParentEntityID not in (select InventoryID from Inventory)



--  AppraisalPhotos
CREATE TABLE dbo.AppraisalPhotos
 (
   AppraisalID  int  not null
    constraint FK_AppraisalPhotos__Appraisal
     foreign key references Appraisals (AppraisalID) 
  ,PhotoID      int  not null
    constraint FK_AppraisalPhotos__Photos
     foreign key references Photos (PhotoID)
  ,constraint PK_AppraisalPhotos
    primary key clustered (AppraisalID, PhotoID)
    with fillfactor = 95
 )

--  Load existing photos
INSERT AppraisalPhotos (AppraisalID, PhotoID)
 select ParentEntityID, PhotoID
  from zzzPhotos
  where PhotoTypeID = 1  --  Appraisal
   and ParentEntityID in (select AppraisalID from Appraisals)

--  Identify invalid photos
UPDATE Photos
 set PhotoTypeID = 0  --  Unknown
 from Photos ph
  inner join zzzPhotos zzz
   on zzz.PhotoID = ph.PhotoID
  where zzz.PhotoTypeID = 1  --  Appraisal
   and zzz.ParentEntityID not in (select AppraisalID from Appraisals)



-- Logo (BusinessUnit) photos
CREATE TABLE dbo.BusinessUnitPhotos
 (
   BusinessUnitID  int  not null
    constraint FK_BusinessUnitPhotos__BusinessUnit
     foreign key references BusinessUnit (BusinessUnitID) 
  ,PhotoID      int  not null
    constraint FK_BusinessUnitPhotos__Photos
     foreign key references Photos (PhotoID)
  ,constraint PK_BusinessUnitPhotos
    primary key clustered (BusinessUnitID, PhotoID)
    with fillfactor = 95
 )

--  Load existing photos
INSERT BusinessUnitPhotos (BusinessUnitID, PhotoID)
 select ParentEntityID, PhotoID
  from zzzPhotos
  where PhotoTypeID = 3  --  Logo
   and ParentEntityID in (select BusinessUnitID from BusinessUnit)

--  Identify invalid photos
UPDATE Photos
 set PhotoTypeID = 0  --  Unknown
 from Photos ph
  inner join zzzPhotos zzz
   on zzz.PhotoID = ph.PhotoID
  where zzz.PhotoTypeID = 3  --  Logo
   and zzz.ParentEntityID not in (select BusinessUnitID from BusinessUnit)

GO

SELECT PhotoTypeID [Post-Modification PhototypeID], count(*) HowMany
 from Photos
 group by Phototypeid
 order by Phototypeid

GO

--  Configure the HomeNetImage provider
DECLARE
  @NewID  int

INSERT ThirdPartyEntity
  (
    BusinessUnitID
   ,ThirdPartyEntityTypeID
   ,Name
  )
 values
  (
    100150  --  Firstlook "default" business unit id
   ,6       --  Photo Provider ID
   ,'Home Net'
  )


--  Get ID just generated
SET @NewID = scope_identity()

INSERT PhotoProvider_ThirdPartyEntity
  (
    PhotoProviderID
   ,DatafeedCode
   ,PhotoStorageCode
 )
 values
  (
    @NewID
   ,'HomeNetImage'
   ,2  --  Retrieve images and store locally
 )

GO
