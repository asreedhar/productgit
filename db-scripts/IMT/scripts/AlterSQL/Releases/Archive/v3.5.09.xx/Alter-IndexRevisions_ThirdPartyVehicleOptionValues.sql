/*

Drop superfluous index

--  List indexes for this store
select ta.name, ind.*
 from sys.tables ta
  inner join sys.indexes ind
   on ind.object_id = ta.object_id
 where ta.name = 'ThirdPartyVehicleOptionValues'



--  Work the Alter_Script table
IF not exists (select 1 from Alter_Script where AlterScriptName = 'Alter-IndexRevisions_ThirdPartyVehicleOptionValues.sql')
    INSERT Alter_Script (AlterScriptName)
     values ('Alter-IndexRevisions_ThirdPartyVehicleOptionValues.sql')
ELSE
    PRINT 'Already Loaded'

SELECT *
 from Alter_Script
 where AlterScriptName = 'Alter-IndexRevisions_ThirdPartyVehicleOptionValues.sql'
 order by ApplicationDate

*/

IF exists(select 1
           from sys.indexes
           where name = 'IX_ThirdPartyVehicleOptionValues__ThirdPartyVehicleOptionID')
    DROP INDEX ThirdPartyVehicleOptionValues.IX_ThirdPartyVehicleOptionValues__ThirdPartyVehicleOptionID
