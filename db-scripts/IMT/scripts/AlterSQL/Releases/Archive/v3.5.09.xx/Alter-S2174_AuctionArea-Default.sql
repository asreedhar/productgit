UPDATE dbo.DealerPreference
SET AuctionAreaID = 1
WHERE AuctionAreaID = 0 
   OR AuctionAreaID is NULL
GO

ALTER TABLE dbo.DealerPreference
ALTER COLUMN AuctionAreaID INT NOT NULL
GO

ALTER TABLE dbo.DealerPreference
ADD CONSTRAINT DF_DealerPreference_AuctionAreaID
DEFAULT 1 FOR AuctionAreaID
GO