----------------------------------------------------------------------------------------------------
--	Add flag to determine to send photo url information
----------------------------------------------------------------------------------------------------

ALTER TABLE dbo.InternetAdvertiserBuildList ADD IncludePhotos BIT NOT NULL DEFAULT (1)
GO