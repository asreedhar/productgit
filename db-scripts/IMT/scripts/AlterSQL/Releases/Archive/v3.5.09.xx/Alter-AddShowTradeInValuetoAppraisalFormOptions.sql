
-- new appraisal form option to display the kbb trade-in Value

ALTER TABLE AppraisalFormOptions ADD [showKbbConsumerValue] TINYINT NOT NULL CONSTRAINT DF_AppraisalFormOptions__showKbbConsumerValue DEFAULT (0)
GO