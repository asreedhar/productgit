-- Changed default from a request after a UAT session from DEFAULT(12) -- 4 weeks
ALTER TABLE dbo.DealerPreference
ADD AuctionTimePeriodID INT NOT NULL 
	CONSTRAINT [DF_DealerPreference_AuctionTimePeriod] DEFAULT(11) -- 2 weeks
GO

update 
dbo.DealerPreference
set AuctionTimePeriodID=11
where AuctionTimePeriodID=12

GO