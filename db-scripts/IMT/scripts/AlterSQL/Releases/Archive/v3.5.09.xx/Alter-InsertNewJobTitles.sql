/*

S2252

	FLAdmin Enhnacement - Add new job tittles in the member profile

	Add new Job Profiles to associate a member with:

    * Sales Manager

    * Internet Manager

    * IT Contact

    * Receptionist
*/    


SET IDENTITY_INSERT JobTitle ON 

INSERT
INTO	JobTitle (JobTitleID, Name)
SELECT	19,'Sales Manager'
UNION 
SELECT	20,'Internet Manager'
UNION 
SELECT	21,'IT Contact'
UNION 
SELECT	22,'Receptionist'

SET IDENTITY_INSERT JobTitle OFF

GO
