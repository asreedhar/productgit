ALTER TABLE dbo.DealerPreference
ADD ShowCheckOnAppraisalForm TINYINT NOT NULL
	CONSTRAINT DF_DealerPreference_ShowCheckOnAppraisalForm DEFAULT(1)
GO