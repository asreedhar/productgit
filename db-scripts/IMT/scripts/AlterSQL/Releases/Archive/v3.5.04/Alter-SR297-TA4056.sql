-- Creates the CheckAppraisalHistoryForIMPPlanning field on dealerPrerence
-- Author: DW
-- Rally card/task SR297/TA4056


if not exists (select * from syscolumns
	where id=object_id('dbo.DealerPreference') and name='CheckAppraisalHistoryForIMPPlanning')
--add new column
ALTER TABLE dbo.DealerPreference
	ADD CheckAppraisalHistoryForIMPPlanning TINYINT NOT NULL DEFAULT (1)
GO