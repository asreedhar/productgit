
----------------------------------------------------------------------------------------------------------------
--	ALL REFERENCES TO THESE SHOULD BE TO FLDW VERSIONS
--	SEEMS THAT THE *Specific AND *Trim ARE STILL ALIVE
----------------------------------------------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetDashBoardReportAverages]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetDashBoardReportAverages]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetTradeAnalyzerReport]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetTradeAnalyzerReport]
GO

--if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetTradeAnalyzerReportSpecific]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
--drop procedure [dbo].[GetTradeAnalyzerReportSpecific]
--GO

--if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetTradeAnalyzerReportTrim]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
--drop procedure [dbo].[GetTradeAnalyzerReportTrim]
--GO

----------------------------------------------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_DeactivateInventoryItem]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_DeactivateInventoryItem]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_UpdateCIAEngineFailures]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_UpdateCIAEngineFailures]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[CIAEngineFailures]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[CIAEngineFailures]
GO





