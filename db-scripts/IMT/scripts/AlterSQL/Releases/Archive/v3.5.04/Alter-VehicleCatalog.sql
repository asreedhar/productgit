if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleAttributeSource]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[VehicleAttributeSource]
GO

CREATE TABLE [dbo].[VehicleAttributeSource] (
	[VehicleAttributeSourceID] [tinyint] NOT NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [PK_VehicleAttributeSource] PRIMARY KEY  CLUSTERED 
	(
		[VehicleAttributeSourceID]
	)  ON [DATA] 
) ON [DATA]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleAttributeCatalog_11]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[VehicleAttributeCatalog_11]
GO

CREATE TABLE [dbo].[VehicleAttributeCatalog_11] (
	[VehicleCatalogID] [int] IDENTITY (1, 1) NOT NULL ,
	[SquishVIN] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CatalogKey] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ModelYear] [int] NULL ,
	[Make] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Line] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BodyType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Series] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MakeModelGroupingID] [int] NULL ,
	[Type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Class] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Doors] [tinyint] NULL ,
	[FuelType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Engine] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DriveTypeCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Transmission] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CylinderQty] [tinyint] NULL ,
	[VehicleAttributeSourceID] [tinyint] NOT NULL ,
	[DisplayBodyTypeID] [tinyint] NULL ,
	[EdmundsBodyTypeCode] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BodyStyleID] [tinyint] NULL ,
	CONSTRAINT [PK_VehicleAttributeCatalog_11] PRIMARY KEY  CLUSTERED 
	(
		[VehicleCatalogID]
	)  ON [DATA] ,
	CONSTRAINT [UQ_VehicleAttributeCatalog_11] UNIQUE  NONCLUSTERED 
	(
		[SquishVIN]
	)  ON [DATA] ,
	CONSTRAINT [FK_VehicleAttributeCatalog_11_tbl_MakeModelGrouping] FOREIGN KEY 
	(
		[MakeModelGroupingID]
	) REFERENCES [dbo].[tbl_MakeModelGrouping] (
		[MakeModelGroupingID]
	),
	CONSTRAINT [FK_VehicleAttributeCatalog_11_VehicleAttributeSource] FOREIGN KEY 
	(
		[VehicleAttributeSourceID]
	) REFERENCES [dbo].[VehicleAttributeSource] (
		[VehicleAttributeSourceID]
	)
) ON [DATA]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VehicleAttributeCatalog_1M]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[VehicleAttributeCatalog_1M]
GO

CREATE TABLE [dbo].[VehicleAttributeCatalog_1M] (
	[VehicleCatalogID] [int] NOT NULL CONSTRAINT [DF_VehicleAttributeCatalog_1M_VehicleCatalogID] DEFAULT (0),
	[StatusCode] [tinyint] NOT NULL CONSTRAINT [DF_VehicleAttributeCatalog_1M_StatusCode] DEFAULT (1),
	[SquishVIN] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CatalogKey] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ModelYear] [int] NULL ,
	[Make] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Line] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[BodyType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Series] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MakeModelGroupingID] [int] NULL ,
	[Type] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Class] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Doors] [tinyint] NULL ,
	[FuelType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Engine] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DriveTypeCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Transmission] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CylinderQty] [tinyint] NULL ,
	[VehicleAttributeSourceID] [tinyint] NOT NULL ,
	[DisplayBodyTypeID] [tinyint] NULL ,
	[EdmundsBodyTypeCode] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BodyStyleID] [tinyint] NULL ,
	CONSTRAINT [FK_VehicleAttributeCatalog_1M_tbl_MakeModelGrouping] FOREIGN KEY 
	(
		[MakeModelGroupingID]
	) REFERENCES [dbo].[tbl_MakeModelGrouping] (
		[MakeModelGroupingID]
	),
	CONSTRAINT [FK_VehicleAttributeCatalog_1M_VehicleAttributeSource] FOREIGN KEY 
	(
		[VehicleAttributeSourceID]
	) REFERENCES [dbo].[VehicleAttributeSource] (
		[VehicleAttributeSourceID]
	)
) ON [DATA]
GO

INSERT
INTO	VehicleAttributeSource
SELECT	1, 'Edmunds'
GO

----------------------------------------------------------------------------------------------------------------
-- 	EVENTUALLY, THIS DESCRIPTION (VEHICLE CATALOG KEY) WILL BE REPLACED WITH A UNIQUE NUMERIC KEY MAPPING 
--	TO THE VEHICLE CATALOG
----------------------------------------------------------------------------------------------------------------

ALTER TABLE dbo.tbl_Vehicle ADD CatalogKey VARCHAR(100) NULL
GO

