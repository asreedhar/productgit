if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PK_AIP_ThirdParty]') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[AIP_ThirdParty] DROP CONSTRAINT PK_AIP_ThirdParty
GO

ALTER TABLE AIP_ThirdParty ALTER COLUMN AIP_ThirdPartyID INT NOT NULL
GO

ALTER TABLE AIP_ThirdParty ADD CONSTRAINT PK_AIP_ThirdParty PRIMARY KEY CLUSTERED (AIP_ThirdPartyID)
GO

ALTER TABLE AIP_Event ADD CONSTRAINT FK_AIP_Event__AIP_ThirdPartyID FOREIGN KEY (AIP_ThirdPartyID) REFERENCES AIP_ThirdParty(AIP_ThirdPartyID)
GO


INSERT
INTO	AIP_ThirdParty (BusinessUnitID, AIP_EventTypeID, Description)

SELECT	DISTINCT I.BusinessUnitID, AIP_EventTypeID, UPPER(E.ThirdParty)
FROM	AIP_Event E
	JOIN Inventory I ON E.InventoryID = I.InventoryID

WHERE	NULLIF(ThirdParty,'') IS NOT NULL
	AND AIP_EventTypeID NOT IN (8,9)

GO

UPDATE	E
SET	AIP_ThirdPartyID = TP.AIP_ThirdPartyID
FROM	AIP_Event E
	JOIN Inventory I ON E.InventoryID = I.InventoryID
	JOIN AIP_ThirdParty TP ON I.BusinessUnitID = TP.BusinessUnitID AND E.AIP_EventTypeID = TP.AIP_EventTypeID AND NULLIF(UPPER(E.ThirdParty),'') = TP.Description 
GO

UPDATE	AIP_Event 
SET	Notes2 = ThirdParty
WHERE	AIP_EventTypeID IN (8,9)

ALTER TABLE AIP_Event DROP COLUMN ThirdParty
GO

ALTER TABLE AIP_Event ALTER COLUMN Notes VARCHAR(500) NULL
GO
ALTER TABLE AIP_Event ALTER COLUMN Notes2 VARCHAR(250) NULL
GO
ALTER TABLE AIP_Event ADD Notes3 VARCHAR(100) NULL
GO