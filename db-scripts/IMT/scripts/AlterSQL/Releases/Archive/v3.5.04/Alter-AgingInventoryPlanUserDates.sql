
ALTER TABLE AIP_Event ADD UserBeginDate SMALLDATETIME NULL
GO
ALTER TABLE AIP_Event ADD UserEndDate SMALLDATETIME NULL
GO
ALTER TABLE AIP_Event ADD CONSTRAINT CK_AIP_Event__UserBeginDate CHECK (SUBSTRING(CAST(UserBeginDate AS BINARY(4)),3,2) & -1 = 0)
GO
ALTER TABLE AIP_Event ADD CONSTRAINT CK_AIP_Event__UserEndDate CHECK (SUBSTRING(CAST(UserEndDate AS BINARY(4)),3,2) & -1 = 0)
GO
ALTER TABLE AIP_Event ADD CONSTRAINT CK_AIP_Event__UserBeginEndConsistency CHECK (CASE WHEN UserEndDate IS NULL THEN 1 WHEN UserEndDate >= UserBeginDate THEN 1 ELSE 0 END = 1)
GO
