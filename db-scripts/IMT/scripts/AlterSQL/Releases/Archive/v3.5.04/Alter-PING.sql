SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PING_Provider](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL, 
	[code] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[available] [bit] NOT NULL,
	[sortOrder] [int] NOT NULL,
	CONSTRAINT [PK_PING_Provider] PRIMARY KEY CLUSTERED 
	(
	[id] ASC 
	) 
) 
GO

CREATE TABLE [dbo].[PING_System](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[active] [bit] NOT NULL,
	CONSTRAINT [PK_PING_System] PRIMARY KEY CLUSTERED 
	(
	[id] ASC
	) 
) 

GO

CREATE TABLE [dbo].[PING_GroupingCodes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[CodeId] [int] NOT NULL,
	[MakeModelGroupingId] [int] NOT NULL,
	CONSTRAINT [PK_PING_GroupingCodes] PRIMARY KEY CLUSTERED 
	(
	[id] ASC
	) 
) 
GO



CREATE TABLE [dbo].[PING_Code] (
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	[name] [nvarchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[value] [nvarchar] (64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[providerId] [int] NOT NULL ,
	[parentId] [int] NULL ,
	[type] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [DATA]
GO

ALTER TABLE [dbo].[PING_Code] WITH NOCHECK ADD 
	CONSTRAINT [PK_PING_Code] PRIMARY KEY  CLUSTERED 
	(
		[id]
	)  ON [DATA] 
GO



SET IDENTITY_INSERT PING_Provider ON

INSERT
INTO	PING_Provider (id, name, code, available, sortOrder)
SELECT	2,  'Cars.com','carsDotCom', 1, 2
UNION
SELECT	3,  'Cars Direct','carsDirect', 1, 3
UNION
SELECT	4,  'Autotrader.com','autotrader', 1, 1

SET IDENTITY_INSERT PING_Provider OFF

SET IDENTITY_INSERT PING_System ON

INSERT
INTO	PING_System (id, active)
SELECT 	1,1

SET IDENTITY_INSERT PING_System OFF
GO