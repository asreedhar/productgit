--For saving the appraisal form preferences to each appraisal.
--PrimaryBooksToDisplay and SecondaryBooksToDisplay are an odd
--way to save the books used on the form.  Sorry Bill don't hit me.
if exists (select * from dbo.sysobjects where id = object_id(N'dbo.AppraisalFormOptions') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.AppraisalFormOptions
GO

CREATE TABLE dbo.[AppraisalFormOptions] (
	[AppraisalFormID] [int] IDENTITY (1, 1) NOT NULL ,
	[AppraisalID] [int] NULL,
	[AppraisalFormOffer] [int] NULL,
	[AppraisalFormHistory] [int] NULL,
	[PrimaryBooksToDisplay] [varchar] (10) NULL,
	[SecondaryBooksToDisplay] [varchar] (10) NULL ,
	[showEquipmentOptions] [tinyint] NULL ,
	[showPhotos] [tinyint] NULL ,
	CONSTRAINT [PK_AppraisalFormID] PRIMARY KEY  NONCLUSTERED 
	(
		[AppraisalFormID]
	),
	CONSTRAINT [FK_AppraisalFormOptions_Appraisals] FOREIGN KEY
	(
		[AppraisalID]
	)  REFERENCES [Appraisals]  (
		[AppraisalID]
	)
) 

GO

ALTER TABLE dbo.Appraisals DROP COLUMN AppraisalFormOffer
GO

ALTER TABLE dbo.Appraisals DROP COLUMN AppraisalFormOfferHistory
GO
