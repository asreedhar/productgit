
if exists (select * from dbo.sysobjects where id = object_id(N'dbo.SearchCandidateOptionAnnotation') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.SearchCandidateOptionAnnotation
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.SearchCandidateOptionAnnotationType') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.SearchCandidateOptionAnnotationType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.SearchCandidateCIACategoryAnnotation') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.SearchCandidateCIACategoryAnnotation
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.SearchCandidateOption') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.SearchCandidateOption
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.SearchCandidate') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.SearchCandidate
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.SearchCandidateList') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.SearchCandidateList
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.SearchCandidateListType') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.SearchCandidateListType
GO

CREATE TABLE dbo.SearchCandidateListType (
	SearchCandidateListTypeID INT IDENTITY (1, 1) NOT NULL,
	Description VARCHAR(255) NOT NULL,
	CONSTRAINT PK_SearchCandidateListType PRIMARY KEY NONCLUSTERED (
		SearchCandidateListTypeID
	)
)
GO

INSERT INTO SearchCandidateListType (Description) VALUES ('Permanent Hot List')
INSERT INTO SearchCandidateListType (Description) VALUES ('Current Hot List')
INSERT INTO SearchCandidateListType (Description) VALUES ('Temporary List')
GO

CREATE TABLE dbo.SearchCandidateList (
	SearchCandidateListID INT IDENTITY (1, 1) NOT NULL,
	SearchCandidateListTypeID INT NOT NULL,
	BusinessUnitID INT NOT NULL,
	MemberID INT NOT NULL,
	Created DATETIME NOT NULL
	CONSTRAINT PK_SearchCandidateList PRIMARY KEY NONCLUSTERED (
		SearchCandidateListID
	),
	CONSTRAINT FK_SearchCandidateList_Member FOREIGN KEY (
		MemberID
	)
	REFERENCES Member (
		MemberID
	),
	CONSTRAINT FK_SearchCandidateList_BusinessUnit FOREIGN KEY (
		BusinessUnitID
	)
	REFERENCES BusinessUnit (
		BusinessUnitID
	),
	CONSTRAINT FK_SearchCandidateList_SearchCandidateListType FOREIGN KEY (
		SearchCandidateListTypeID
	)
	REFERENCES SearchCandidateListType (
		SearchCandidateListTypeID
	),
	CONSTRAINT UQ_SearchCandidateList UNIQUE NONCLUSTERED (
		SearchCandidateListTypeID,
		BusinessUnitID,
		MemberID
	)
)
GO

CREATE TABLE dbo.SearchCandidate (
	SearchCandidateID INT IDENTITY (1, 1) NOT NULL,
	SearchCandidateListID INT NOT NULL,
	Make VARCHAR(255) NOT NULL,
	Line VARCHAR(255) NOT NULL,
	SegmentID INT NOT NULL,
	Suppress BIT NOT NULL,
	CONSTRAINT PK_SearchCandidate PRIMARY KEY NONCLUSTERED (
		SearchCandidateID
	),
	CONSTRAINT FK_SearchCandidate_SearchCandidateList FOREIGN KEY (
		SearchCandidateListID
	)
	REFERENCES SearchCandidateList (
		SearchCandidateListID
	),
	CONSTRAINT UQ_SearchCandidate UNIQUE NONCLUSTERED	(
		SearchCandidateListID,
		Make,
		Line,
		SegmentID
	)
)
GO

CREATE TABLE dbo.SearchCandidateCIACategoryAnnotation (
	SearchCandidateID INT NOT NULL,
	CIACategoryID TINYINT NOT NULL,
	CONSTRAINT PK_SearchCandidateCIACategoryAnnotation PRIMARY KEY NONCLUSTERED (
		SearchCandidateID,
		CIACategoryID
	),
	CONSTRAINT FK_SearchCandidateCIACategoryAnnotation_SearchCandidate FOREIGN KEY (
		SearchCandidateID
	)
	REFERENCES SearchCandidate (
		SearchCandidateID
	),
	CONSTRAINT FK_SearchCandidateCIACategoryAnnotation_CIACategories FOREIGN KEY (
		CIACategoryID
	)
	REFERENCES CIACategories (
		CIACategoryID
	)
)
GO

CREATE TABLE dbo.SearchCandidateOption (
	SearchCandidateOptionID INT IDENTITY (1, 1) NOT NULL,
	SearchCandidateID INT NOT NULL,
	ModelYear INT NULL,
	Expires DATETIME NULL,
	Suppress BIT NOT NULL,
	CONSTRAINT PK_SearchCandidateOption PRIMARY KEY NONCLUSTERED (
		SearchCandidateOptionID
	),
	CONSTRAINT FK_SearchCandidateOption_SearchCandidate FOREIGN KEY (
		SearchCandidateID
	)
	REFERENCES SearchCandidate (
		SearchCandidateID
	),
	CONSTRAINT UQ_SearchCandidateOption UNIQUE NONCLUSTERED	(
		SearchCandidateID,
		ModelYear
	)
)
GO

CREATE TABLE dbo.SearchCandidateOptionAnnotationType (
	SearchCandidateOptionAnnotationTypeID INT IDENTITY (1, 1) NOT NULL,
	Description VARCHAR(255) NOT NULL,
	CONSTRAINT PK_SearchCandidateOptionAnnotationType PRIMARY KEY NONCLUSTERED (
		SearchCandidateOptionAnnotationTypeID
	)
)
GO


INSERT INTO SearchCandidateOptionAnnotationType (Description) VALUES ('Optimal Plan')
INSERT INTO SearchCandidateOptionAnnotationType (Description) VALUES ('Buying Plan')
INSERT INTO SearchCandidateOptionAnnotationType (Description) VALUES ('Hot List')
INSERT INTO SearchCandidateOptionAnnotationType (Description) VALUES ('One Time')
GO

CREATE TABLE dbo.SearchCandidateOptionAnnotation (
	SearchCandidateOptionID INT NOT NULL,
	SearchCandidateOptionAnnotationTypeID INT NOT NULL,
	CONSTRAINT PK_SearchCandidateOptionAnnotation PRIMARY KEY NONCLUSTERED (
		SearchCandidateOptionID,
		SearchCandidateOptionAnnotationTypeID
	),
	CONSTRAINT FK_SearchCandidateOptionAnnotation_SearchCandidateOption FOREIGN KEY (
		SearchCandidateOptionID
	)
	REFERENCES SearchCandidateOption (
		SearchCandidateOptionID
	),
	CONSTRAINT FK_SearchCandidateOptionAnnotation_SearchCandidateOptionAnnotationType FOREIGN KEY (
		SearchCandidateOptionAnnotationTypeID
	)
	REFERENCES SearchCandidateOptionAnnotationType (
		SearchCandidateOptionAnnotationTypeID
	)
)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.tbl_MemberATCAccessGroup') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.tbl_MemberATCAccessGroup
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.tbl_MemberATCAccessGroupList') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.tbl_MemberATCAccessGroupList
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.tbl_MemberATCAccessGroupListType') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.tbl_MemberATCAccessGroupListType
GO


CREATE TABLE dbo.tbl_MemberATCAccessGroupListType (
	MemberATCAccessGroupListTypeID INT IDENTITY (1, 1) NOT NULL,
	Description VARCHAR(255) NOT NULL,
	CONSTRAINT PK_MemberATCAccessGroupListType PRIMARY KEY NONCLUSTERED (
		MemberATCAccessGroupListTypeID
	)
)
GO

INSERT INTO tbl_MemberATCAccessGroupListType (Description) VALUES ('Permanent Market List')
INSERT INTO tbl_MemberATCAccessGroupListType (Description) VALUES ('Current Market List')
INSERT INTO tbl_MemberATCAccessGroupListType (Description) VALUES ('Temporary Market List')
GO

CREATE TABLE dbo.tbl_MemberATCAccessGroupList (
	MemberATCAccessGroupListID INT IDENTITY(1,1) NOT NULL,
	MemberATCAccessGroupListTypeID INT NOT NULL,
	MemberID INT NOT NULL,
	BusinessUnitID INT NOT NULL,
	MaxDistanceFromDealer INT NOT NULL,
	CONSTRAINT PK_MemberATCAccessGroupList PRIMARY KEY NONCLUSTERED (
		MemberATCAccessGroupListID
	),
	CONSTRAINT FK_MemberATCAccessGroupList_Member FOREIGN KEY (
		MemberID
	)
	REFERENCES Member (
		MemberID
	),
	CONSTRAINT FK_MemberATCAccessGroupList_BusinessUnit FOREIGN KEY (
		BusinessUnitID
	)
	REFERENCES BusinessUnit (
		BusinessUnitID
	),
	CONSTRAINT FK_MemberATCAccessGroupList_MemberATCAccessGroupListType FOREIGN KEY (
		MemberATCAccessGroupListTypeID
	)
	REFERENCES tbl_MemberATCAccessGroupListType (
		MemberATCAccessGroupListTypeID
	),
	CONSTRAINT UQ_MemberATCAccessGroupList UNIQUE NONCLUSTERED	(
		MemberID,
		BusinessUnitID,
		MemberATCAccessGroupListTypeID
	)
)
GO

CREATE TABLE dbo.tbl_MemberATCAccessGroup (
	MemberATCAccessGroupID     INT IDENTITY(1,1) NOT NULL,
	MemberATCAccessGroupListID INT NOT NULL,
	MaxDistanceFromDealer      INT NOT NULL,
	Position                   INT NOT NULL,
	Suppress                   BIT NOT NULL,
	AccessGroupID              INT NOT NULL,
	CONSTRAINT PK_MemberATCAccessGroup PRIMARY KEY NONCLUSTERED (
		MemberATCAccessGroupID
	),
	CONSTRAINT FK_MemberATCAccessGroup_MemberATCAccessGroupList FOREIGN KEY (
		MemberATCAccessGroupListID
	)
	REFERENCES tbl_MemberATCAccessGroupList (
		MemberATCAccessGroupListID
	),
	CONSTRAINT UQ_MemberATCAccessGroup_AccessGroup UNIQUE NONCLUSTERED (
		MemberATCAccessGroupListID,
		AccessGroupID
	)
)
GO

ALTER TABLE dbo.Member ADD SearchHomePage VARCHAR(255)
GO

INSERT
INTO	lu_DealerUpgrade (DealerUpgradeCD, DealerUpgradeDESC)
SELECT	13, 'S&A Navigator (VIP,DR)'
GO

