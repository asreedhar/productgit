-- Creates and updates database for the appraisal form.
-- Author: Dave M
-- FE 68

if not exists (select * from syscolumns
	where id=object_id('dbo.DealerPreference') and name='AppraisalFormShowOptions')
--add new dealer preference column
ALTER TABLE dbo.DealerPreference
	ADD AppraisalFormShowOptions TINYINT NOT NULL DEFAULT (1)
GO

if not exists (select * from syscolumns
	where id=object_id('dbo.DealerPreference') and name='AppraisalFormShowPhotos')
--add new dealer preference column
ALTER TABLE dbo.DealerPreference
	ADD AppraisalFormShowPhotos TINYINT NOT NULL DEFAULT (1)
GO

if not exists (select * from syscolumns
	where id=object_id('dbo.DealerPreference') and name='AppraisalFormValidDate')
--add new dealer preference column
ALTER TABLE dbo.DealerPreference
	ADD AppraisalFormValidDate INTEGER NOT NULL DEFAULT (14)
GO

if not exists (select * from syscolumns
	where id=object_id('dbo.DealerPreference') and name='AppraisalFormValidMileage')
--add new dealer preference column
ALTER TABLE dbo.DealerPreference
	ADD AppraisalFormValidMileage INTEGER NOT NULL DEFAULT (150)	
GO

if not exists (select * from syscolumns
	where id=object_id('dbo.DealerPreference') and name='AppraisalFormDisclaimer')
--add new dealer preference column
ALTER TABLE dbo.DealerPreference
	ADD AppraisalFormDisclaimer VARCHAR (180) NOT NULL DEFAULT 'The owner of this vehicle herby affirms that it has not been damaged by flood or had frame damage.'	
GO

if not exists (select * from syscolumns
	where id=object_id('dbo.DealerPreference') and name='AppraisalFormMemo')
--add new dealer preference column
ALTER TABLE dbo.DealerPreference
	ADD AppraisalFormMemo VARCHAR (45) NOT NULL DEFAULT 'Trade in value for purchase of a vehicle.'	
GO

SET IDENTITY_INSERT Position ON 
INSERT
INTO	Position (PositionID, Description)
SELECT	2, 'Appraiser'

SET IDENTITY_INSERT Position ON 
INSERT
INTO	PhotoTypes (PhotoTypeID, description)
SELECT	3, 'Logo'

SET IDENTITY_INSERT Position OFF 
GO

if not exists (select * from syscolumns
	where id=object_id('dbo.AppraisalValues') and name='AppraiserName')
ALTER TABLE AppraisalValues ADD AppraiserName VARCHAR(35)

GO 

UPDATE AppraisalValues
SET appraiserName = initials
GO

if not exists (select * from syscolumns
	where id=object_id('dbo.Appraisals') and name='AppraisalFormOffer')
ALTER TABLE Appraisals ADD AppraisalFormOffer INTEGER NULL
GO

if not exists (select * from syscolumns
	where id=object_id('dbo.Appraisals') and name='AppraisalFormOfferHistory')
ALTER TABLE Appraisals ADD AppraisalFormOfferHistory INTEGER NULL
GO