------------------------------------------------------------------------------------------------
--
--	LOOKS LIKE THE RELEASE TAGGING HAS CHANGED; THIS FOLLOWS ..V3055
--
------------------------------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAgingInventoryPlanXML]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetAgingInventoryPlanXML]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetPurchasingCenterAuctionsXML]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetPurchasingCenterAuctionsXML]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetPurchasingCenterVehiclesXML]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetPurchasingCenterVehiclesXML]
GO

