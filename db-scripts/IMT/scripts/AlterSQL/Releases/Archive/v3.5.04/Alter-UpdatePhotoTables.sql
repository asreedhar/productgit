create table dbo.PhotoTypes
(
PhotoTypeID int not null,
description varchar(30) not null,
CONSTRAINT [PK_PhotoTypes] PRIMARY KEY  NONCLUSTERED 
	(
		[PhotoTypeID]
	) WITH  FILLFACTOR = 90  ON [IDX]
) ON [DATA]
GO

insert into PhotoTypes values (1, 'Appraisal') 
insert into PhotoTypes values (2, 'Inventory') 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PhotosVehicle]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[PhotosVehicle]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Photos]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Photos]
GO


create table dbo.Photos
(
PhotoID int IDENTITY not null,
PhotoURL varchar(100) not null,
PhotoTypeID int not null,
ParentEntityID int not null,
DateCreated datetime not null
CONSTRAINT [PK_Photos] PRIMARY KEY  NONCLUSTERED 
	(
		[PhotoID]
	) WITH  FILLFACTOR = 90  ON [IDX],
CONSTRAINT [FK_PhotoTypeID] FOREIGN KEY
	(
		[PhotoTypeID]
	) REFERENCES [PhotoTypes] (
		[PhotoTypeID]
	)
) ON [DATA]
GO