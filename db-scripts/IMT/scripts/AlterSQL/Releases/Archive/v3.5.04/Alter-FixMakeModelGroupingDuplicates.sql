CREATE TABLE #Map (OldMakeModelGroupingID INT, NewMakeModelGroupingID INT, OldGroupingDescriptionID INT, NewGroupingDescriptionID INT NULL)

INSERT
INTO	#Map
SELECT	102712, 101772, 102583, NULL	-- Chevrolet Blazer (WGN->SUV)
UNION
SELECT	102701, 100718, 102573, NULL	-- CHEVROLET K1500 (SDN->REGL)
UNION
SELECT	100473, 102702, 102574, 101509	-- GMC K1500 (NULL->CRWC)
UNION
SELECT	100212, 102704, 102576, 101509	-- GMC SIERRA (NULL->REGC)
UNION
SELECT	102495, 102812, 102392, NULL	-- Scion xA HBK->SDN
UNION
SELECT	102558, 102764, 102450, NULL	-- Toyota Prius HBK->SDN

------------------------------------------------------------------------------------------------

-- SELECT	MakeModelGroupingID, count(*) Units
-- FROM	Inventory I
-- 
-- 	JOIN Vehicle V ON I.VehicleID = V.VehicleID
-- 	JOIN #Map M ON V.MakeModelGroupingID in (M.OldMakeModelGroupingID, M.NewMakeModelGroupingID)
-- GROUP
-- BY	MakeModelGroupingID


------------------------------------------------------------------------------------------------

UPDATE	V
SET	V.MakeModelGroupingID = M.NewMakeModelGroupingID
FROM	Vehicle V
	JOIN #Map M ON V.MakeModelGroupingID = M.OldMakeModelGroupingID

UPDATE	V
SET	V.MakeModelGroupingID = M.NewMakeModelGroupingID
FROM	VehicleAttributeCatalog_11 V
	JOIN #Map M ON V.MakeModelGroupingID = M.OldMakeModelGroupingID

UPDATE	V
SET	V.MakeModelGroupingID = M.NewMakeModelGroupingID
FROM	VehicleAttributeCatalog_1M V
	JOIN #Map M ON V.MakeModelGroupingID = M.OldMakeModelGroupingID

------------------------------------------------------------------------------------------------

UPDATE	MMG
SET	MMG.GroupingDescriptionID = M.NewGroupingDescriptionID
FROM	tbl_MakeModelGrouping MMG
	JOIN #Map M ON MMG.GroupingDescriptionID = M.OldGroupingDescriptionID
WHERE	M.NewGroupingDescriptionID IS NOT NULL

------------------------------------------------------------------------------------------------

DELETE	MMG
FROM	tbl_MakeModelGrouping MMG
	JOIN #Map M ON MMG.MakeModelGroupingID = M.OldMakeModelGroupingID

------------------------------------------------------------------------------------------------

DELETE	GD
FROM	GDLight GD
	JOIN #Map M ON GD.GroupingDescriptionID = OldGroupingDescriptionID

DELETE	GD
FROM	GroupingDescription GD
	JOIN #Map M ON GD.GroupingDescriptionID = OldGroupingDescriptionID

------------------------------------------------------------------------------------------------

-- SELECT	MakeModelGroupingID, count(*) Units
-- FROM	Inventory I
-- 
-- 	JOIN Vehicle V ON I.VehicleID = V.VehicleID
-- 	JOIN #Map M ON V.MakeModelGroupingID in (M.OldMakeModelGroupingID, M.NewMakeModelGroupingID)
-- GROUP
-- BY	MakeModelGroupingID

------------------------------------------------------------------------------------------------

UPDATE	C
SET	GroupingDescriptionID = M.New
FROM	CIAGroupingItems C
	JOIN (	SELECT	102450 Old, 102451 New
		UNION
		SELECT	102392, 102635
		) M ON C.GroupingDescriptionID = M.Old
GO

ALTER TABLE CIAGroupingItems ADD CONSTRAINT [FK_CIAGroupingItems_GroupingDescriptionID] FOREIGN KEY (GroupingDescriptionID)
	REFERENCES tbl_GroupingDescription (GroupingDescriptionID)
GO