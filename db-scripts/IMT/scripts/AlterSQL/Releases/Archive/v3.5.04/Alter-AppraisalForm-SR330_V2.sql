-- Creates and updates database for the appraisal form.
-- Author: Dave M
-- FE 68


if not exists (select * from syscolumns
	where id=object_id('dbo.DealerPreference') and name='ShowAppraisalForm')
--add new dealer preference column
ALTER TABLE dbo.DealerPreference
	ADD ShowAppraisalForm TINYINT NOT NULL DEFAULT (0)
GO


if not exists (select * from syscolumns
	where id=object_id('dbo.Appraisals') and name='AppraisalFormOffer')
ALTER TABLE Appraisals ADD AppraisalFormOffer INTEGER NULL
GO