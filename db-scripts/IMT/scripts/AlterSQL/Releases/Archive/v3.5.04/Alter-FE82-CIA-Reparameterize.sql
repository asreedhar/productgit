--Add new preferences

--CIA Store Target Inventory
ALTER TABLE dbo.tbl_CIAPreferences
ADD CiaStoreTargetInventoryBasisPeriodId INT NOT NULL 
CONSTRAINT [DF_CiaStoreTargetInventoryBasisPeriodId] DEFAULT (1),
CONSTRAINT [FK_tbl_CIAPreferences_CiaStoreTargetInventoryBasisPeriodId] FOREIGN KEY
( [CiaStoreTargetInventoryBasisPeriodId] ) 
REFERENCES [tbl_CIACompositeTimePeriod]
( [CIACompositeTimePeriodId] )
GO

--CIA Core Model Determination (the basis period used to retrieve vehicle sales for a model)
ALTER TABLE dbo.tbl_CIAPreferences
ADD CiaCoreModelDeterminationBasisPeriodId INT NOT NULL 
CONSTRAINT [DF_CiaCoreModelDeterminationBasisPeriodId] DEFAULT (5),
CONSTRAINT [FK_tbl_CIAPreferences_CiaCoreModelDeterminationBasisPeriodId] FOREIGN KEY
( [CiaCoreModelDeterminationBasisPeriodId] ) 
REFERENCES [tbl_CIACompositeTimePeriod]
( [CIACompositeTimePeriodId] )
GO

--CIA Powerzone Model Target Inventory
ALTER TABLE dbo.tbl_CIAPreferences
ADD CiaPowerzoneModelTargetInventoryBasisPeriodId INT NOT NULL 
CONSTRAINT [DF_CiaPowerzoneModelTargetInventoryBasisPeriodId] DEFAULT (1),
CONSTRAINT [FK_tbl_CIAPreferences_CiaPowerzoneModelTargetInventoryBasisPeriodId] FOREIGN KEY
( [CiaPowerzoneModelTargetInventoryBasisPeriodId] ) 
REFERENCES [tbl_CIACompositeTimePeriod]
( [CIACompositeTimePeriodId] )
GO

--CIA Core Model Year Allocation Basis Period
ALTER TABLE dbo.tbl_CIAPreferences
ADD CiaCoreModelYearAllocationBasisPeriodId INT NOT NULL 
CONSTRAINT [DF_CiaCoreModelYearAllocationBasisPeriodId] DEFAULT (5),
CONSTRAINT [FK_tbl_CIAPreferences_CiaCoreModelYearAllocationBasisPeriodId] FOREIGN KEY
( [CiaCoreModelYearAllocationBasisPeriodId] ) 
REFERENCES [tbl_CIACompositeTimePeriod]
( [CIACompositeTimePeriodId] )
GO

--Drop the old table definitions
ALTER TABLE dbo.tbl_CIAPreferences
DROP DF_CIATargetUnitsTimePeriodId

ALTER TABLE dbo.tbl_CIAPreferences
DROP FK_tbl_CIAPreferences_CIATargetUnitsTimePeriod

ALTER TABLE dbo.tbl_CIAPreferences
DROP COLUMN CIATargetUnitsTimePeriodId
GO

ALTER TABLE dbo.tbl_CIAPreferences
DROP DF_CIAAllocationTimePeriodId

ALTER TABLE dbo.tbl_CIAPreferences
DROP FK_tbl_CIAPreferences_CIABucketAllocationTimePeriod

ALTER TABLE dbo.tbl_CIAPreferences
DROP COLUMN CIAAllocationTimePeriodId
GO