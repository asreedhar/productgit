--drop FK constraint on subscriptions table, not needed in this case since app
--checks existence of subscription before executing on results in buying alerts run list

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_BuyingAlertsRunList_Subscriptions]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[BuyingAlertsRunList] DROP CONSTRAINT [FK_BuyingAlertsRunList_Subscriptions]
GO