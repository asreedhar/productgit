ALTER TABLE Marketing.ConsumerHighlightPreference
ADD MaxVHRItemsInitialDisplay TINYINT NOT NULL
CONSTRAINT DF_Marketing_ConsumerHighlightPreference__MaxVHRItemsInitialDisplay DEFAULT (6)
GO
