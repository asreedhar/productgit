/*
add third party id
*/

ALTER TABLE Marketing.MarketAnalysisPriceProvider
ADD ThirdPartyId TINYINT NULL;

ALTER TABLE Marketing.MarketAnalysisPriceProvider  
WITH CHECK ADD CONSTRAINT FK_Marketing_MarketAnalysisPriceProvider__ThirdParty 
FOREIGN KEY(ThirdPartyID)
REFERENCES dbo.ThirdParties (ThirdPartyID)
GO
ALTER TABLE Marketing.MarketAnalysisPriceProvider CHECK CONSTRAINT FK_Marketing_MarketAnalysisPriceProvider__ThirdParty
GO

UPDATE Marketing.MarketAnalysisPriceProvider
SET ThirdPartyId = 
CASE 
WHEN PriceProviderId = 6 THEN 5	    -- Edmunds
WHEN PriceProviderId = 7 THEN 2	    -- NADA
WHEN PriceProviderId = 8 THEN 3	    -- KBB
WHEN PriceProviderId = 9 THEN 1	    -- BlackBook
WHEN PriceProviderId = 10 THEN 4    -- Galves
ELSE NULL
END