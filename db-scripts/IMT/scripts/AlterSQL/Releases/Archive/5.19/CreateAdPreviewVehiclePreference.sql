CREATE TABLE Marketing.AdPreviewVehiclePreference
(
	AdPreviewVehiclePreferenceId INT IDENTITY(1,1) NOT NULL,
	VehicleEntityId INT NOT NULL,
	VehicleEntityTypeId INT NOT NULL,
	CharacterLimit INT NOT NULL,
	OwnerID INT NOT NULL,
	InsertUserID INT NOT NULL,
	InsertDate DATETIME NOT NULL,
	UpdateUserId INT NOT NULL,
	UpdateDate DATETIME NOT NULL,
	CONSTRAINT PK_AdPreviewVehiclePreference PRIMARY KEY CLUSTERED
(
	AdPreviewVehiclePreferenceId ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]

GO
ALTER TABLE [Marketing].[AdPreviewVehiclePreference]  WITH CHECK ADD  CONSTRAINT [FK_Marketing_AdPreviewVehiclePreference_Member_InsertUser] FOREIGN KEY([InsertUserID])
REFERENCES [dbo].[Member] ([MemberID])
GO
ALTER TABLE [Marketing].[AdPreviewVehiclePreference] CHECK CONSTRAINT [FK_Marketing_AdPreviewVehiclePreference_Member_InsertUser]
GO
ALTER TABLE [Marketing].[AdPreviewVehiclePreference]  WITH CHECK ADD  CONSTRAINT [FK_Marketing_AdPreviewVehiclePreference_Member_UpdateUser] FOREIGN KEY([UpdateUserId])
REFERENCES [dbo].[Member] ([MemberID])
GO
ALTER TABLE [Marketing].[AdPreviewVehiclePreference] CHECK CONSTRAINT [FK_Marketing_AdPreviewVehiclePreference_Member_UpdateUser]
