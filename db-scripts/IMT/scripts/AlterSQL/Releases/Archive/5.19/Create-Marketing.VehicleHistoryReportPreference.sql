
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Marketing].[VehicleHistoryReportPreference](
    [VehicleHistoryReportPreferenceId] INT IDENTITY(1,1) NOT NULL,
    [OwnerId] INT NULL,
	[InsertUserId] [int] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
CONSTRAINT [PK_Marketing_VehicleHistoryReportPreference] PRIMARY KEY CLUSTERED 
(
	[VehicleHistoryReportPreferenceId]

),
CONSTRAINT [UK_Marketing_VehicleHistoryReportPreference_OwnerId] UNIQUE NONCLUSTERED 
(
	[OwnerId] ASC
)
)
GO

ALTER TABLE [Marketing].[VehicleHistoryReportPreference]  WITH CHECK 
ADD  CONSTRAINT [FK_Marketing_VehicleHistoryReportPreference__InsertMember] FOREIGN KEY([InsertUserId])
REFERENCES [dbo].[Member] ([MemberID])
GO
ALTER TABLE [Marketing].[VehicleHistoryReportPreference] CHECK CONSTRAINT [FK_Marketing_VehicleHistoryReportPreference__InsertMember]
GO


