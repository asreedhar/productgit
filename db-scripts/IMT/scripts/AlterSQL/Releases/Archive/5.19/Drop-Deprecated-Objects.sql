
IF OBJECT_ID('dbo.EdmundsPostLoadProcessing') IS NOT NULL DROP PROC dbo.EdmundsPostLoadProcessing
IF OBJECT_ID('dbo.vwSales') IS NOT NULL DROP VIEW dbo.vwSales
IF OBJECT_ID('dbo.DealerFinancialStatement') IS NOT NULL DROP TABLE dbo.DealerFinancialStatement
IF OBJECT_ID('dbo.LatestDealerFinancialStatement') IS NOT NULL DROP VIEW dbo.LatestDealerFinancialStatement

IF OBJECT_ID('dbo.DeleteBusinessUnit') IS NOT NULL DROP PROC dbo.DeleteBusinessUnit
IF OBJECT_ID('dbo.vw_QA_NegativeDaysToSale') IS NOT NULL DROP VIEW dbo.vw_QA_NegativeDaysToSale
IF OBJECT_ID('dbo.DealerPreferenceDataload') IS NOT NULL DROP VIEW dbo.DealerPreferenceDataload
IF OBJECT_ID('dbo.vwDealersWithoutRandTRecords') IS NOT NULL DROP VIEW dbo.vwDealersWithoutRandTRecords

IF OBJECT_ID('dbo.usp_load_used_scorecardtargets') IS NOT NULL DROP PROC dbo.usp_load_used_scorecardtargets
IF OBJECT_ID('dbo.usp_load_new_scorecardtargets') IS NOT NULL DROP PROC dbo.usp_load_new_scorecardtargets
IF OBJECT_ID('dbo.usp_LoadScoreCardTargets') IS NOT NULL DROP PROC dbo.usp_LoadScoreCardTargets

IF OBJECT_ID('dbo.IndexDefragUtility') IS NOT NULL DROP PROC dbo.IndexDefragUtility
IF OBJECT_ID('dbo.LoadCarFaxProcessorRunLog') IS NOT NULL DROP PROC dbo.LoadCarFaxProcessorRunLog

IF OBJECT_ID('dbo.IsValidMakeModel') IS NOT NULL DROP FUNCTION dbo.IsValidMakeModel
IF OBJECT_ID('dbo.IsValidSquishVIN') IS NOT NULL DROP FUNCTION dbo.IsValidSquishVIN
IF OBJECT_ID('dbo.GetDisplayBodyTypeID') IS NOT NULL DROP FUNCTION dbo.GetDisplayBodyTypeID
