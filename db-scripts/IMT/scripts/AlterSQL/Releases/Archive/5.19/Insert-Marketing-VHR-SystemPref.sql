DECLARE @systemPreferenceId INT          

INSERT INTO marketing.VehicleHistoryReportPreference
        ( OwnerId, InsertUserId, InsertDate )
VALUES  ( null, -- OwnerId - int
          100000, -- InsertUserId - int
          GETDATE()
          )
          
SELECT @systemPreferenceId = SCOPE_IDENTITY()

-- add the system preference for the vhr report inspection items
INSERT INTO marketing.VehicleHistoryReportSourcePreference
        ( VehicleHistoryReportPreferenceId ,
          VehicleHistoryReportInspectionId ,
          Rank ,
          IsDisplayed ,
          InsertUserId ,
          InsertDate
        )
SELECT	@systemPreferenceId,  -- ownerid
	ri.VehicleHistoryReportInspectionId,
	ri.VehicleHistoryReportInspectionId, -- Rank: default rank to be the inspectionid
	1,  -- Is Displayed
	100000, -- admin userid 
	GETDATE() -- insert date
FROM 	marketing.VehicleHistoryReportInspection ri
	LEFT JOIN Carfax.ReportInspection cfi ON cfi.ReportInspectionID = ri.CarfaxReportInspectionId
	LEFT JOIN AutoCheck.ReportInspection afi ON afi.ReportInspectionID = ri.AutocheckReportInspectionId

