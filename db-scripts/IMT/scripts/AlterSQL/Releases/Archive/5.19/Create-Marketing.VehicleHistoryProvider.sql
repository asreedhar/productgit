
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE Marketing.VehicleHistoryProvider(
	VehicleHistoryProviderId tinyint NOT NULL,
	Name varchar(50) NOT NULL,
CONSTRAINT PK_Marketing_VehicleHistoryProvider PRIMARY KEY CLUSTERED (	VehicleHistoryProviderId ASC),
CONSTRAINT UK_Marketing_VehicleHistoryProvider__Name UNIQUE NONCLUSTERED ( Name ASC ))
GO

SET ANSI_PADDING OFF
GO

ALTER TABLE Marketing.VehicleHistoryProvider  WITH CHECK ADD  CONSTRAINT CK_Marketing_VehicleHistoryProvider__Name CHECK (len([Name])>(0))
GO

ALTER TABLE Marketing.VehicleHistoryProvider CHECK CONSTRAINT CK_Marketing_VehicleHistoryProvider__Name
GO



INSERT INTO Marketing.VehicleHistoryProvider
        ( VehicleHistoryProviderId, Name )
VALUES  ( 1, -- VehicleHistoryProviderId - tinyint
          'Carfax'  -- Name - varchar(50)
          )

INSERT INTO Marketing.VehicleHistoryProvider
        ( VehicleHistoryProviderId, Name )
VALUES  ( 2, -- VehicleHistoryProviderId - tinyint
          'Autocheck'  -- Name - varchar(50)
          )
          
     


