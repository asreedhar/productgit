
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'Promotion')
EXEC('CREATE SCHEMA Promotion AUTHORIZATION [dbo]')
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Promotion].[Activity]') AND type in (N'U'))
DROP TABLE [Promotion].[Activity]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Promotion].[ActivityType]') AND type in (N'U'))
DROP TABLE [Promotion].[ActivityType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Promotion].[Promotion]') AND type in (N'U'))
DROP TABLE [Promotion].[Promotion]
GO

CREATE TABLE Promotion.Promotion ( 
	PromotionID             int identity(1,1)  NOT NULL,
	Name                    varchar(40) NOT NULL,
	Description             varchar(500) NOT NULL,
	StartDate               datetime NOT NULL,
	EndDate                 datetime NOT NULL,
	Enabled                 bit NOT NULL,
	MaxViews                tinyint NOT NULL,
	Priority                tinyint NOT NULL,
	PromotionUrl            varchar(2000) NOT NULL,
	PromotionSummaryUrl     varchar(2000) NOT NULL,
	InsertUser              int NOT NULL,
	InsertDate              datetime NOT NULL,
	UpdateUser              int NULL,
	UpdateDate              datetime NULL,
	CONSTRAINT PK_Promotion_Promotion
	        PRIMARY KEY CLUSTERED (PromotionID),
        CONSTRAINT UK_Promotion_Promotion__Description
                UNIQUE (Description),
        CONSTRAINT FK_Promotion_Promotion__InsertUser
	        FOREIGN KEY (InsertUser)
	        REFERENCES dbo.Member (MemberID),
        CONSTRAINT FK_Promotion_Promotion__UpdateUser
	        FOREIGN KEY (UpdateUser)
	        REFERENCES dbo.Member (MemberID),
        CONSTRAINT CK_Promotion_Promotion__UpdateDateGreaterThanInsertDate
                CHECK  (UpdateDate IS NULL OR UpdateDate>=InsertDate),
        CONSTRAINT CK_Promotion_Promotion__UpdateUserDate
                CHECK  (UpdateDate IS NULL AND UpdateUser IS NULL OR UpdateDate IS NOT NULL AND UpdateUser IS NOT NULL),
        CONSTRAINT CK_Promotion_Promotion__EndDateGreaterThanStartDate
                CHECK  (EndDate >= StartDate),
        CONSTRAINT CK_Promotion_Promotion__Name
                CHECK  (LEN(Name) > 0),
        CONSTRAINT CK_Promotion_Promotion__Description
                CHECK  (LEN(Description) > 0),
        CONSTRAINT CK_Promotion_Promotion__PromotionUrl
                CHECK  (LEN(PromotionUrl) > 0),
        CONSTRAINT CK_Promotion_Promotion__PromotionSummaryUrl
                CHECK  (LEN(PromotionSummaryUrl) > 0)
)
GO

CREATE TABLE Promotion.ActivityType ( 
	ActivityTypeID  tinyint NOT NULL,
	Name            varchar(50) NOT NULL,
        CONSTRAINT PK_Promotion_ActivityType
	        PRIMARY KEY CLUSTERED (ActivityTypeID),
        CONSTRAINT UK_Promotion_ActivityType__Name
	        UNIQUE (Name),
        CONSTRAINT CK_Promotion_ActivityType__Name
	        CHECK (LEN(Name) > 0)
)
GO

CREATE TABLE Promotion.Activity ( 
	ActivityID      int identity(1,1) NOT NULL,
	ActivityTypeID  tinyint NOT NULL,
	PromotionID     int NOT NULL,
	InsertDate      datetime NOT NULL,
	InsertUser      int NOT NULL,
        CONSTRAINT PK_Promotion_Activity
                PRIMARY KEY CLUSTERED (ActivityID),
        CONSTRAINT FK_Promotion_Activity__ActivityType 
	        FOREIGN KEY (ActivityTypeID)
	        REFERENCES Promotion.ActivityType (ActivityTypeID),
        CONSTRAINT FK_Promotion_Activity__InsertUser
                FOREIGN KEY (InsertUser)
                REFERENCES dbo.Member (MemberID),
        CONSTRAINT FK_Promotion_Activity__Promotion
	        FOREIGN KEY (PromotionID)
	        REFERENCES Promotion.Promotion (PromotionID)
)
GO

INSERT INTO [Promotion].[ActivityType]
           ([ActivityTypeID]
           ,[Name])
	SELECT 1, 'VIEW'
	UNION
	SELECT 2, 'SKIP'
	UNION
	SELECT 3, 'NEVER'
GO
