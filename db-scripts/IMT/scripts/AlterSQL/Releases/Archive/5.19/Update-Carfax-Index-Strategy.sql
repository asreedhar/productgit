------------------------------------Drop FKs-----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Exception__RequestID]') 
AND parent_object_id = OBJECT_ID(N'[Carfax].[Exception]'))
ALTER TABLE [Carfax].[Exception] DROP CONSTRAINT [FK_Exception__RequestID]

GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_Report__Request]') 
AND parent_object_id = OBJECT_ID(N'[Carfax].[Report]'))
ALTER TABLE [Carfax].[Report] DROP CONSTRAINT [FK_Carfax_Report__Request]

GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_Response__RequestID]') 
AND parent_object_id = OBJECT_ID(N'[Carfax].[Response]'))
ALTER TABLE [Carfax].[Response] DROP CONSTRAINT [FK_Carfax_Response__RequestID]

GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_Vehicle_Dealer__Request]') 
AND parent_object_id = OBJECT_ID(N'[Carfax].[Vehicle_Dealer]'))
ALTER TABLE [Carfax].[Vehicle_Dealer] DROP CONSTRAINT [FK_Carfax_Vehicle_Dealer__Request]

GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Carfax].[FK_Carfax_ReportProcessorCommand_Request__RequestID]') 
AND parent_object_id = OBJECT_ID(N'[Carfax].[ReportProcessorCommand_Request]'))
ALTER TABLE [Carfax].[ReportProcessorCommand_Request] DROP CONSTRAINT [FK_Carfax_ReportProcessorCommand_Request__RequestID]


--------------- Make Cafax.Request table index changes--------------------------------------------------

-- Drop PK with Clustered index
IF  EXISTS (SELECT * FROM sys.indexes WHERE name = N'PK_Carfax_Request')
ALTER TABLE [Carfax].[Request] DROP CONSTRAINT [PK_Carfax_Request]
GO

--Create NonClustered PK 
ALTER TABLE [Carfax].[Request] ADD  CONSTRAINT [PK_Carfax_Request] PRIMARY KEY  NONCLUSTERED
(
	[RequestID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
GO

-- Create clustered index on IDX filegroup. 

IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = N'IX_Request_Vehicle_Account')
CREATE CLUSTERED INDEX [IX_Request_Vehicle_Account] ON [Carfax].[Request] 
(
	[VehicleID] ASC,
	[AccountID] ASC
)WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, 
ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [IDX]
GO

-- Create non clustered index on AccountID and add RequestID as included colum on Filegroup IDX
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE name = N'IX_Account_Request')
CREATE NONCLUSTERED INDEX [IX_Account_Request] ON [Carfax].[Request] 
(
	[AccountID] ASC
)
INCLUDE ( [RequestID]) WITH (STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, 
DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [IDX]
GO

------------------------------------------ Create FKs----------------------------------------------------------------

ALTER TABLE [Carfax].[Exception]  WITH CHECK ADD  CONSTRAINT [FK_Exception__RequestID] FOREIGN KEY([RequestID])
REFERENCES [Carfax].[Request] ([RequestID])
GO
ALTER TABLE [Carfax].[Exception] CHECK CONSTRAINT [FK_Exception__RequestID]

GO
ALTER TABLE [Carfax].[Report]  WITH CHECK ADD  CONSTRAINT [FK_Carfax_Report__Request] FOREIGN KEY([RequestID])
REFERENCES [Carfax].[Request] ([RequestID])
GO
ALTER TABLE [Carfax].[Report] CHECK CONSTRAINT [FK_Carfax_Report__Request]

GO
ALTER TABLE [Carfax].[Response]  WITH CHECK ADD  CONSTRAINT [FK_Carfax_Response__RequestID] FOREIGN KEY([RequestID])
REFERENCES [Carfax].[Request] ([RequestID])
GO
ALTER TABLE [Carfax].[Response] CHECK CONSTRAINT [FK_Carfax_Response__RequestID]

GO
ALTER TABLE [Carfax].[Vehicle_Dealer]  WITH CHECK ADD  CONSTRAINT [FK_Carfax_Vehicle_Dealer__Request] FOREIGN KEY([RequestID])
REFERENCES [Carfax].[Request] ([RequestID])
GO
ALTER TABLE [Carfax].[Vehicle_Dealer] CHECK CONSTRAINT [FK_Carfax_Vehicle_Dealer__Request]

GO
ALTER TABLE [Carfax].[ReportProcessorCommand_Request]  WITH CHECK ADD  CONSTRAINT [FK_Carfax_ReportProcessorCommand_Request__RequestID] FOREIGN KEY([RequestID])
REFERENCES [Carfax].[Request] ([RequestID])
GO
ALTER TABLE [Carfax].[ReportProcessorCommand_Request] CHECK CONSTRAINT [FK_Carfax_ReportProcessorCommand_Request__RequestID]