create nonclustered index IX_VehicleEntityID_VehicleEntityTypeID on dbo.Inventory
	(
		InventoryActive,
		DeleteDt
	) on IDX

create nonclustered index IX_VehicleEntityID_VehicleEntityTypeID on Marketing.CertifiedVehicleBenefitPreference
	(
		VehicleEntityID,
		VehicleEntityTypeID
	) on IDX
	

create nonclustered index IX_ConsumerHighlightVehiclePreferenceId on Marketing.ConsumerHighlight
	(ConsumerHighlightVehiclePreferenceId) on IDX
	
create nonclustered index IX_VehicleEntityID_VehicleEntityTypeID on Marketing.ConsumerHighlightVehiclePreference
	(
		VehicleEntityID,
		VehicleEntityTypeID
	) on IDX



create nonclustered index IX_VehicleEntityID_VehicleEntityTypeID on Marketing.EquipmentList
	(
		VehicleEntityID,
		VehicleEntityTypeID
	) on IDX


create nonclustered index IX_VehicleEntityID_VehicleEntityTypeID on Marketing.MarketAnalysisVehiclePreference
	(
		VehicleEntityID,
		VehicleEntityTypeID
	) on IDX


create nonclustered index IX_VehicleEntityID_VehicleEntityTypeID on Marketing.MarketListingVehiclePreference
	(
		VehicleEntityID,
		VehicleEntityTypeID
	) on IDX
	
	
create nonclustered index IX_VehicleEntityID_VehicleEntityTypeID on Marketing.PhotoVehiclePreference
	(
		VehicleEntityID,
		VehicleEntityTypeID
	) on IDX
		
		
		 
create nonclustered index IX_VehicleEntityID_VehicleEntityTypeID on Marketing.ValueAnalyzerVehiclePreference
	(
		VehicleEntityID,
		VehicleEntityTypeID
	) on IDX
	
	
	
create nonclustered index IX_VehicleEntityID_VehicleEntityTypeID on Marketing.VehicleEquipmentProvider
	(
		VehicleEntityID,
		VehicleEntityTypeID
	) on IDX
	
	