-- bugzid: 12620 - change description
UPDATE Marketing.MarketAnalysisPriceProvider
SET Description = 'PING Market Average Selling Price'
WHERE PriceProviderId = 4 
AND Description = 'PING Market Average Internet Price'