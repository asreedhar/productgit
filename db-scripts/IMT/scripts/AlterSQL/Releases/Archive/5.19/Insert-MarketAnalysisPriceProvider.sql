
INSERT INTO Marketing.MarketAnalysisPriceProvider
        ( PriceProviderId, Description )
VALUES  ( 9, -- PriceProviderId - tinyint
          'Black Book'  -- Description - varchar(50)
          );
INSERT INTO Marketing.MarketAnalysisPriceProvider
        ( PriceProviderId, Description )
VALUES  ( 10, -- PriceProviderId - tinyint
          'Galves'  -- Description - varchar(50)
          );


INSERT INTO marketing.MarketAnalysisOwnerPriceProvider
        ( OwnerId ,
          PriceProviderId ,
          Rank ,
          InsertUser ,
          InsertDate ,
          UpdateUser ,
          UpdateDate
        )
VALUES  ( NULL, -- System Default
          9 , -- PriceProviderId - tinyint
          8 , -- Rank - int
          100000 , -- InsertUser - int
          GETDATE() , -- InsertDate - datetime
          NULL, -- UpdateUser - int
          NULL  -- UpdateDate - datetime
        )
        
INSERT INTO marketing.MarketAnalysisOwnerPriceProvider
        ( OwnerId ,
          PriceProviderId ,
          Rank ,
          InsertUser ,
          InsertDate ,
          UpdateUser ,
          UpdateDate
        )
VALUES  ( NULL, -- System Default
          10 , -- PriceProviderId - tinyint
          9 , -- Rank - int
          100000 , -- InsertUser - int
          GETDATE() , -- InsertDate - datetime
          NULL, -- UpdateUser - int
          NULL  -- UpdateDate - datetime
        )        






