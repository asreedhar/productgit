IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Marketing].[FK_Marketing_VehicleHistoryReportInspection__VehicleHistoryProvider]') 
	AND parent_object_id = OBJECT_ID(N'[Marketing].[VehicleHistoryReportInspection]'))
ALTER TABLE [Marketing].[VehicleHistoryReportInspection] 
DROP CONSTRAINT [FK_Marketing_VehicleHistoryReportInspection__VehicleHistoryProvider]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[VehicleHistoryProvider]') AND type in (N'U'))
UPDATE [Marketing].[VehicleHistoryProvider]
SET VehicleHistoryProviderId = 3
WHERE VehicleHistoryProviderId = 1

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[VehicleHistoryReportInspection]') AND type in (N'U'))
BEGIN

UPDATE [Marketing].[VehicleHistoryReportInspection]
SET VehicleHistoryProviderId = 3
WHERE VehicleHistoryProviderId = 1

ALTER TABLE [Marketing].[VehicleHistoryReportInspection]  WITH CHECK 
ADD CONSTRAINT [FK_Marketing_VehicleHistoryReportInspection__VehicleHistoryProvider]
FOREIGN KEY([VehicleHistoryProviderId])
REFERENCES [Marketing].[VehicleHistoryProvider]([VehicleHistoryProviderId])

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Marketing].[CK_Marketing_VehicleHistoryReportInspection_CarfaxReportInspectionId]') 
	AND parent_object_id = OBJECT_ID(N'[Marketing].[VehicleHistoryReportInspection]'))
ALTER TABLE [Marketing].[VehicleHistoryReportInspection] 
DROP CONSTRAINT [CK_Marketing_VehicleHistoryReportInspection_CarfaxReportInspectionId]

ALTER TABLE [Marketing].[VehicleHistoryReportInspection]  WITH CHECK 
ADD CONSTRAINT [CK_Marketing_VehicleHistoryReportInspection_CarfaxReportInspectionId] 
CHECK(VehicleHistoryProviderId <> 3 OR (CarfaxReportInspectionId IS NOT NULL AND AutocheckReportInspectionId IS NULL) )

END
