
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Marketing].[VehicleHistoryReportSourcePreference](
    [VehicleHistoryReportPreferenceId] INT NOT NULL,
	[VehicleHistoryReportInspectionId] [tinyint] NOT NULL,
	[Rank] [int] NOT NULL,
	[IsDisplayed] [bit] NOT NULL,
	[InsertUserId] [int] NOT NULL,
	[InsertDate] [datetime] NOT NULL,
	[UpdateUserId] [int] NULL,
	[UpdateDate] [datetime] NULL,
CONSTRAINT [PK_Marketing_VehicleHistoryReportSourcePreference] PRIMARY KEY CLUSTERED 
(
	[VehicleHistoryReportPreferenceId] ASC,
	[VehicleHistoryReportInspectionId] ASC
),
CONSTRAINT [UQ_Marketing_VehicleHistoryReportSourcePreference__PreferenceRank] UNIQUE NONCLUSTERED 
(
	[VehicleHistoryReportPreferenceId] ASC,
	[Rank] ASC
))
GO

ALTER TABLE [Marketing].[VehicleHistoryReportSourcePreference]  WITH CHECK 
ADD CONSTRAINT [FK_Marketing_VehicleHistoryReportSourcePreference__VehicleHistoryReportPreference]
FOREIGN KEY( VehicleHistoryReportPreferenceId)
REFERENCES [Marketing].VehicleHistoryReportPreference(VehicleHistoryReportPreferenceId)
GO

ALTER TABLE [Marketing].[VehicleHistoryReportSourcePreference]  WITH CHECK 
ADD CONSTRAINT [FK_Marketing_VehicleHistoryReportSourcePreference__VehicleHistoryReportInspection]
FOREIGN KEY(VehicleHistoryReportInspectionId) 
REFERENCES [Marketing].VehicleHistoryReportInspection(VehicleHistoryReportInspectionId)
GO

ALTER TABLE [Marketing].[VehicleHistoryReportSourcePreference]  WITH CHECK ADD  CONSTRAINT [FK_Marketing_VehicleHistoryReportSourcePreference__InsertMember] FOREIGN KEY([InsertUserId])
REFERENCES [dbo].[Member] ([MemberID])
GO
ALTER TABLE [Marketing].[VehicleHistoryReportSourcePreference] CHECK CONSTRAINT [FK_Marketing_VehicleHistoryReportSourcePreference__InsertMember]
GO
ALTER TABLE [Marketing].[VehicleHistoryReportSourcePreference]  WITH CHECK ADD  CONSTRAINT [FK_Marketing_VehicleHistoryReportSourcePreference__UpdateMember] FOREIGN KEY([UpdateUserId])
REFERENCES [dbo].[Member] ([MemberID])
GO
ALTER TABLE [Marketing].[VehicleHistoryReportSourcePreference] CHECK CONSTRAINT [FK_Marketing_VehicleHistoryReportSourcePreference__UpdateMember]
GO
ALTER TABLE [Marketing].[VehicleHistoryReportSourcePreference]  WITH CHECK ADD  CONSTRAINT [CK_Marketing_VehicleHistoryReportSourcePreference__Dates] CHECK  (([UpdateDate]>=[InsertDate]))
GO
ALTER TABLE [Marketing].[VehicleHistoryReportSourcePreference] CHECK CONSTRAINT [CK_Marketing_VehicleHistoryReportSourcePreference__Dates]
GO
ALTER TABLE [Marketing].[VehicleHistoryReportSourcePreference]  WITH CHECK ADD  CONSTRAINT [CK_Marketing_VehicleHistoryReportSourcePreference__Rank] CHECK  (([Rank]>=(0)))
GO
ALTER TABLE [Marketing].[VehicleHistoryReportSourcePreference] CHECK CONSTRAINT [CK_Marketing_VehicleHistoryReportSourcePreference__Rank]
GO
