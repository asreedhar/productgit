
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE Marketing.VehicleHistoryReportInspection(
	VehicleHistoryReportInspectionId TINYINT NOT NULL,
	VehicleHistoryProviderId TINYINT NOT NULL,
	CarfaxReportInspectionId TINYINT NULL,
	AutocheckReportInspectionId TINYINT NULL,
CONSTRAINT PK_Marketing_VehicleHistoryReportInspection PRIMARY KEY CLUSTERED (	VehicleHistoryReportInspectionId ASC))
GO

SET ANSI_PADDING OFF
GO

ALTER TABLE Marketing.VehicleHistoryReportInspection  WITH CHECK 
ADD  CONSTRAINT CK_Marketing_VehicleHistoryReportInspection_CarfaxReportInspectionId 
CHECK(VehicleHistoryProviderId <> 1 OR (CarfaxReportInspectionId IS NOT NULL AND AutocheckReportInspectionId IS NULL) )
GO

ALTER TABLE Marketing.VehicleHistoryReportInspection  WITH CHECK 
ADD  CONSTRAINT CK_Marketing_VehicleHistoryReportInspection_AutoCheckReportInspectionId 
CHECK(VehicleHistoryProviderId <> 2 OR (AutocheckReportInspectionId IS NOT NULL AND CarfaxReportInspectionId IS NULL) )
GO

ALTER TABLE Marketing.VehicleHistoryReportInspection  WITH CHECK 
ADD CONSTRAINT [FK_Marketing_VehicleHistoryReportInspection__VehicleHistoryProvider]
FOREIGN KEY( VehicleHistoryProviderId ) 
REFERENCES Marketing.VehicleHistoryProvider(VehicleHistoryProviderId)
GO

ALTER TABLE Marketing.VehicleHistoryReportInspection  WITH CHECK 
ADD CONSTRAINT [FK_Marketing_VehicleHistoryReportInspection__CarfaxReportInspection]
FOREIGN KEY( CarfaxReportInspectionId ) 
REFERENCES Carfax.ReportInspection(ReportInspectionID)
GO

ALTER TABLE Marketing.VehicleHistoryReportInspection  WITH CHECK 
ADD CONSTRAINT [FK_Marketing_VehicleHistoryReportInspection__AutocheckReportInspection]
FOREIGN KEY( AutocheckReportInspectionId ) 
REFERENCES AutoCheck.ReportInspection(ReportInspectionID)
GO

-- Load the initial values into this table

DECLARE @t TABLE (idx TINYINT IDENTITY(1,1), ProviderId TINYINT NOT NULL, ReportInspectionId TINYINT NOT NULL)

INSERT INTO @t ( ProviderId , ReportInspectionId )
SELECT 1, ReportInspectionID FROM Carfax.ReportInspection
UNION 
SELECT 2, ReportInspectionID FROM AutoCheck.ReportInspection
ORDER BY 1, 2

-- load 
INSERT INTO marketing.VehicleHistoryReportInspection
	( VehicleHistoryReportInspectionId , 
	  VehicleHistoryProviderId ,
          CarfaxReportInspectionId,
          AutocheckReportInspectionId
        )
SELECT 
idx,
ProviderId,
CASE WHEN ProviderId = 1 THEN ReportInspectionId ELSE NULL END, -- CarfaxReportInspectionId
CASE WHEN ProviderId = 2 THEN ReportInspectionId ELSE NULL END -- AutocheckReportInspectionId
FROM @t

