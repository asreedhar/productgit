-- Move everyone with an account with AutoTrader, AutoUplink or Aultec into the Distribution.Account table.

DECLARE @Accounts TABLE 
(	
	BusinessUnitID int,
	InternetAdvertiserID int,
	InternetAdvertisersDealershipCode varchar(50) null,
	DistributionActive bit null
)
	
INSERT INTO @Accounts
SELECT
	BusinessUnitID,
	InternetAdvertiserID,
	InternetAdvertisersDealershipCode,
	DistributionActive
FROM 
	dbo.InternetAdvertiserDealership
WHERE
	InternetAdvertiserID in (3684, 5034, 11284)

ALTER TABLE Distribution.Account
DISABLE TRIGGER TR_I_Account

ALTER TABLE Distribution.Account
DISABLE TRIGGER TR_U_Account
	
ALTER TABLE Distribution.Account
DISABLE TRIGGER TR_D_Account

DELETE FROM Distribution.Account

INSERT INTO Distribution.Account (DealerID, ProviderID, DealershipCode, Active, InsertUserID, InsertDate)
SELECT
	BusinessUnitID,
	CASE WHEN InternetAdvertiserID = 3684 THEN 1 WHEN InternetAdvertiserID = 5034 THEN 2 WHEN InternetAdvertiserID = 11284 THEN 3 END,
	InternetAdvertisersDealershipCode,
	COALESCE(DistributionActive, 0),
	100000,
	getdate()
FROM
	@Accounts
	
ALTER TABLE Distribution.Account
ENABLE TRIGGER TR_D_Account	

ALTER TABLE Distribution.Account
ENABLE TRIGGER TR_I_Account	

ALTER TABLE Distribution.Account
ENABLE TRIGGER TR_U_Account	

GO
