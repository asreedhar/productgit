-----------------------------------------------------------------------------------------------------------------------
--
--	CGC 12/05/2010
--
--	Remove the 'PublicationID' column from Distribution.Exception, and replace with 'SubmissionID'.
--
-----------------------------------------------------------------------------------------------------------------------

SET NOCOUNT ON
GO

ALTER TABLE Distribution.Exception
DROP CONSTRAINT FK_Distribution_Exception__Publication

ALTER TABLE Distribution.Exception
DROP COLUMN PublicationID

ALTER TABLE Distribution.Exception
ADD SubmissionID int null

ALTER TABLE Distribution.Exception
ADD CONSTRAINT FK_Distribution_Exception__Submission
FOREIGN KEY (SubmissionID)
REFERENCES Distribution.Submission (SubmissionID)

GO
