
CREATE TABLE Marketing.InventoryDocuments(
	DocumentID INT IDENTITY(1,1) NOT NULL,
	OwnerID INT NOT NULL,
	VehicleEntityID INT NOT NULL,
	VehicleEntityTypeID INT NOT NULL,
	DocumentXml VARCHAR(MAX) NOT NULL,
	InsertUserID INT NOT NULL,
	InsertDate DATETIME NOT NULL,
	UpdateUserID INT NOT NULL,
	UpdateDate DATETIME NOT NULL
	CONSTRAINT [PK_MarketingInventoryDocuments]
	PRIMARY KEY CLUSTERED(DocumentID ASC))
	
