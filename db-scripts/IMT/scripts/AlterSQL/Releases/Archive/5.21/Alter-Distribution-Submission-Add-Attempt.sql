-----------------------------------------------------------------------------------------------------------------------
--
--	CGC 11/15/2010
--
--	Adds a new column 'Attempt' to Distribution.Submission, as well as updating a constraint and setting an Attempt 
--  value for existing submissions.
--
-----------------------------------------------------------------------------------------------------------------------

SET NOCOUNT ON
GO

ALTER TABLE Distribution.Submission
ADD Attempt int null
GO

UPDATE Distribution.Submission
SET Attempt = 1
GO

ALTER TABLE Distribution.Submission
ALTER COLUMN Attempt int not null
GO

IF EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Distribution].[Submission]') AND name = N'UQ_Distribution_Submission__Publication_Provider')
ALTER TABLE [Distribution].[Submission] DROP CONSTRAINT [UQ_Distribution_Submission__Publication_Provider]
GO

ALTER TABLE [Distribution].[Submission]  WITH CHECK ADD  
CONSTRAINT [UQ_Distribution_Submission__Publication_Provider_Attempt]
UNIQUE ([PublicationID], [ProviderID], [Attempt])
GO
ALTER TABLE [Distribution].[Submission] CHECK CONSTRAINT [UQ_Distribution_Submission__Publication_Provider_Attempt]
GO

ALTER TABLE [Distribution].[Submission] WITH CHECK ADD
CONSTRAINT [CK_Distribution_Submission__Attempt]
CHECK ([Attempt] <= 5 AND [Attempt] >= 1)
GO
