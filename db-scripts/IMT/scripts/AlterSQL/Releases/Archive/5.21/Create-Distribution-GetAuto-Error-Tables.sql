-----------------------------------------------------------------------------------------------------------------------
--	Create Distribution.GetAuto_ErrorType
--      a. Check Constraints    
--              - CK_Distribution_GetAuto_ErrorType__Description
--      b. Foreign Keys
--				- None
-----------------------------------------------------------------------------------------------------------------------
create table Distribution.GetAuto_ErrorType
(
    ErrorTypeID   [int] identity(0,1) not null,
    Description   [varchar](2000) not null,
    
    constraint PK_Distribution_GetAuto_ErrorType 
    primary key (ErrorTypeID),
    
    constraint CK_Distribution_GetAuto_ErrorType__Description
    check (len(Description) > 0)
)

go

insert into Distribution.GetAuto_ErrorType (Description) values ('Unrecognized')
insert into Distribution.GetAuto_ErrorType (Description) values ('Method Failed')
insert into Distribution.GetAuto_ErrorType (Description) values ('Security Failed')
insert into Distribution.GetAuto_ErrorType (Description) values ('Object Is Null')
insert into Distribution.GetAuto_ErrorType (Description) values ('Validation Failed')
insert into Distribution.GetAuto_ErrorType (Description) values ('Record Not Found')
insert into Distribution.GetAuto_ErrorType (Description) values ('Invalid Dealer Lot Key')
insert into Distribution.GetAuto_ErrorType (Description) values ('Vehicle Already Exists')
insert into Distribution.GetAuto_ErrorType (Description) values ('Missing Required Field')
insert into Distribution.GetAuto_ErrorType (Description) values ('Vehicle Type Not Supported')
insert into Distribution.GetAuto_ErrorType (Description) values ('Invalid Authentication Token')
insert into Distribution.GetAuto_ErrorType (Description) values ('Expired Authentication Token')
insert into Distribution.GetAuto_ErrorType (Description) values ('Dealer Does Not Have Feature')
insert into Distribution.GetAuto_ErrorType (Description) values ('Vehicle Does Not Have Feature')

go		  

-----------------------------------------------------------------------------------------------------------------------
--	Create Distribution.GetAuto_Error
--      a. Check Constraints    
--              - None
--      b. Foreign Keys
--				- FK_Distribution_GetAutoError__SubmissionError
--              - FK_Distribution_GetAutoError__GetAuto_ErrorType
-----------------------------------------------------------------------------------------------------------------------
create table [Distribution].[GetAuto_Error]
(
    [ErrorID]       [int] not null,
    [ErrorTypeID]   [int] not null,
    
    constraint [PK_Distribution_GetAutoError] 
    primary key ([ErrorID]),
    
    constraint [FK_Distribution_GetAutoError__SubmissionError] 
    foreign key ([ErrorID])
    references [Distribution].[SubmissionError] ([ErrorID]),
    
    constraint [FK_Distribution_GetAutoError__GetAuto_ErrorType] 
    foreign key ([ErrorTypeID])
    references [Distribution].[GetAuto_ErrorType] ([ErrorTypeID])    
)

go

-----------------------------------------------------------------------------------------------------------------------
--	Add GetAuto to Distribution.ProviderMessageType
-----------------------------------------------------------------------------------------------------------------------

insert into Distribution.ProviderMessageType (ProviderID, MessageTypes) values (4,1)
insert into Distribution.ProviderMessageType (ProviderID, MessageTypes) values (4,2)
insert into Distribution.ProviderMessageType (ProviderID, MessageTypes) values (4,3)

go

-----------------------------------------------------------------------------------------------------------------------
--	Add eBiz to Distribution.ProviderMessageType
-----------------------------------------------------------------------------------------------------------------------

insert into Distribution.ProviderMessageType (ProviderID, MessageTypes) values (5,1)
insert into Distribution.ProviderMessageType (ProviderID, MessageTypes) values (5,2)
insert into Distribution.ProviderMessageType (ProviderID, MessageTypes) values (5,3)

go
