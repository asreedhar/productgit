/* MAK 11/16/2010

This deletes all reports that were mistakenly ordered more than once by the processor.  This is either due to an error in the
processor or to an error populating the queue. */
IF OBJECT_ID('tempdb..#GoodReports') IS NOT NULL 
    DROP TABLE #GoodReports

CREATE TABLE #GoodReports
    (
      VehicleID INT,
      DealerID INT,
      RequestID INT,
      HasProblem BIT,
      OwnerCount SMALLINT,
      ExpirationDate DATETIME,
      DisplayInHotList BIT,
      ReportType VARCHAR(5),
      ReportProcessorCommandID INT
    )

INSERT  INTO #GoodReports
        (
          VehicleID,
          DealerID,
          RequestID,
          HasProblem,
          OwnerCount,
          ExpirationDate,
          DisplayInHotList,
          ReportType 
        )
        SELECT  RPC.VehicleID,
                RPC.DealerID,
                COALESCE(MAX(VD.RequestID), MAX(RQ.RequestID)),
                RP.HasProblem,
                RP.OwnerCount,
                RP.ExpirationDate,
                RQ.DisplayInHotList,
                RQ.ReportType
        FROM    Carfax.Report RP
                JOIN Carfax.Request RQ ON RP.RequestID = RQ.RequestID
                JOIN Carfax.ReportProcessorCommand_Request RPR ON RQ.RequestID = RPR.RequestID
                JOIN Carfax.ReportProcessorCommand RPC ON RPR.ReportProcessorCommandID = RPC.ReportProcessorCommandID
                LEFT JOIN Carfax.Vehicle_Dealer VD ON RPC.DealerID = VD.DealerID
                                                      AND RPC.VehicleID = VD.DealerID
                                                      AND RQ.RequestID = VD.RequestID
        GROUP BY RPC.VehicleID,
                RPC.DealerID,
                RP.ExpirationDate,
                RP.HasProblem,
                RP.OwnerCount,
                RQ.DisplayInHotList,
                RQ.ReportType
        HAVING  COUNT(*) > 1


UPDATE  DR
SET     ReportProcessorCommandID = RPC.ReportProcessorCommandID
FROM    #GoodReports DR
        JOIN Carfax.ReportProcessorCommand RPC ON DR.DealerID = RPC.DealerID
                                                  AND DR.VehicleID = RPC.VehicleID
        JOIN Carfax.ReportProcessorCommand_Request RPR ON RPC.ReportProcessorCommandID = RPR.ReportProcessorCommandID
                                                          AND DR.RequestID = RPR.RequestID


IF OBJECT_ID('tempdb..#BadReports') IS NOT NULL 
    DROP TABLE #BadReports

CREATE TABLE #BadReports
    (
      VehicleID INT,
      DealerID INT,
      RequestID INT,
      HasProblem BIT,
      OwnerCount SMALLINT,
      ExpirationDate DATETIME,
      DisplayInHotList BIT,
      ReportType VARCHAR(5),
      ReportProcessorCommandID INT
    )
	    
INSERT  INTO #BadReports
        (
          VehicleID,
          DealerID,
          RequestID,
          HasProblem,
          OwnerCount,
          ExpirationDate,
          DisplayInHotList,
          ReportType,
          ReportProcessorCommandID
        )
        SELECT  RPC.VehicleID,
                RPC.DealerID,
                RQ.RequestID,
                RP.HasProblem,
                RP.OwnerCount,
                RP.ExpirationDate,
                RQ.DisplayInHotList,
                RQ.ReportType,
                RPC.ReportProcessorCommandID
        FROM    Carfax.Report RP
                JOIN Carfax.Request RQ ON RP.RequestID = RQ.RequestID
                JOIN Carfax.ReportProcessorCommand_Request RPR ON RQ.RequestID = RPR.RequestID
                JOIN Carfax.ReportProcessorCommand RPC ON RPR.ReportProcessorCommandID = RPC.ReportProcessorCommandID
                JOIN #GoodReports GR ON RPC.DealerID = GR.DealerID
                                        AND RPC.VehicleID = GR.VehicleID
                                        AND RP.ExpirationDate = GR.ExpirationDate
                                        AND RP.HasProblem = GR.HasProblem
                                        AND RQ.DisplayInHotList = GR.DisplayInHotList
                                        AND RQ.ReportType = GR.ReportType
        WHERE   NOT EXISTS ( SELECT 1
                             FROM   #GoodReports GG
                             WHERE  RQ.VehicleID = GG.VehicleID
                                    AND RPC.DealerID = GG.DealerID
                                    AND RP.ExpirationDate = GR.ExpirationDate
                                    AND RP.RequestID = GG.RequestID
                                    AND RPC.ReportProcessorCommandID = GG.ReportProcessorCommandID
                                    AND RP.HasProblem = GR.HasProblem
                                    AND RQ.DisplayInHotList = GR.DisplayInHotList
                                    AND RQ.ReportType = GR.ReportType )
                AND NOT EXISTS ( SELECT 1
                                 FROM   Carfax.Vehicle_Dealer VD
                                 WHERE  RQ.RequestID = VD.RequestID )
-- Delete all references to the bad requests in the ReportProcessorCommand_Request table. 

DELETE  FROM Carfax.ReportPRocessorCommand_Request
WHERE   RequestID IN ( SELECT   RequestID
                       FROM     #BadReports )
 



-- Delete all references to the bad requests in the Carfax.ReportInspectionResult, Carfax.Report, Carfax.Response and
-- finally the Carfax.Request tables. 
DELETE  FROM Carfax.ReportInspectionResult
WHERE   RequestID IN ( SELECT   RequestID
                       FROM     #BadReports )

DELETE  FROM Carfax.Report
WHERE   RequestID IN ( SELECT   RequestID
                       FROM     #BadReports )

DELETE  FROM Carfax.Response
WHERE   RequestID IN ( SELECT   RequestID
                       FROM     #BadReports )

DELETE  FROM Carfax.Request
WHERE   RequestID IN ( SELECT   RequestID
                       FROM     #BadReports )

        
IF OBJECT_ID('tempdb..#BadRPC') IS NOT NULL 
    DROP TABLE #BadRPC

CREATE TABLE #BadRPC
    (
      ReportProcessorCommandID INT
    )

INSERT  INTO #BadRPC
        (
          ReportProcessorCommandID
        )
        SELECT  BR.ReportProcessorCommandID
        FROM    #BadReports BR
                LEFT JOIN #GoodReports GR ON BR.ReportProcessorCommandID = GR.ReportProcessorCommandID
        WHERE   GR.ReportProcessorCommandID IS NULL
                AND NOT EXISTS ( SELECT 1
                                 FROM   Carfax.ReportProcessorCommand_Request RPR
                                 WHERE  BR.ReportProcessorCommandID = RPR.ReportProcessorCommandID )
 

DELETE  FROM Carfax.ReportPRocessorCommand_Exception
WHERE   ReportProcessorCommandID IN ( SELECT    ReportProcessorCommandID
                                      FROM      #BadRPC )


DELETE  FROM Carfax.ReportPRocessorCommand_DataMigration
WHERE   ReportProcessorCommandID IN ( SELECT    ReportProcessorCommandID
                                      FROM      #BadRPC ) 

DELETE  FROM Carfax.ReportPRocessorCommand
WHERE   ReportProcessorCommandID IN ( SELECT    ReportProcessorCommandID
                                      FROM      #BadRPC ) 

 
GO 
 
 