-- Add column to dbo.DealerPreference that controls whether the In-Transit Inventory form is shown.

ALTER TABLE dbo.DealerPreference
ADD ShowInTransitInventoryForm BIT NULL
GO

UPDATE dbo.DealerPreference SET ShowInTransitInventoryForm = 0
GO

ALTER TABLE dbo.DealerPreference
ALTER COLUMN ShowInTransitInventoryForm BIT NOT NULL
GO

ALTER TABLE dbo.DealerPreference
ADD CONSTRAINT DF_DealerPreference_ShowInTransitInventoryForm DEFAULT(0) FOR ShowInTransitInventoryForm
GO
