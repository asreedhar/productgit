
DECLARE @msgnum INT
DECLARE @text VARCHAR(500)

SET @msgnum = 50119
SET @text = 'VehicleEntityTypeID = %d.  This procedure is only valid for inventory.'

IF EXISTS ( SELECT  1
            FROM    sys.messages
            WHERE   Message_ID = @msgnum ) 
    EXEC sp_addmessage @msgnum = @msgnum, @severity = 16, @msgtext = @text,
        @lang = 'us_english', @replace = 'REPLACE'
ELSE 
    EXEC sp_addmessage @msgnum = @msgnum, @severity = 16, @msgtext = @text,
        @lang = 'us_english'
   
   GO