/*
The name has officially changed.  From this point forward, she shall be known as 'MAX for Selling & Email'.
*/

UPDATE	dbo.lu_DealerUpgrade
SET	DealerUpgradeDESC = 'MAX for Selling & Email'
WHERE	DealerUpgradeCD = 23
