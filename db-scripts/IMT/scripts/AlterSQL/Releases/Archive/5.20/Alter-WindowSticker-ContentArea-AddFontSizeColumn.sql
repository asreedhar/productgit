-- add a column, initially with a default 
ALTER TABLE WindowSticker.ContentArea
ADD FontSize DECIMAL(9,3) NOT NULL CONSTRAINT [DF_ContentArea_FontSize] DEFAULT(1.0)
go

-- drop the contraint, leave it as not null w/out default
ALTER TABLE WindowSticker.ContentArea
DROP CONSTRAINT [DF_ContentArea_FontSize]
go

ALTER TABLE WindowSticker.ContentArea
ADD CONSTRAINT [CK_WindowSticker_ContentArea_FontSize_GreaterThanZero] CHECK(FontSize > 0)
go
