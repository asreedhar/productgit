-- Moving the columnns from paper size to the template table.  Paper size turned out to be unnecessary.

-- add columns to window sticker template
ALTER TABLE WindowSticker.Template
ADD Height DECIMAL(9,3) NOT NULL CONSTRAINT [DF_TEMP_WindowSticker_Template_Height] DEFAULT(1.0),
Width DECIMAL(9,3) NOT NULL CONSTRAINT [DF_TEMP_WindowSticker_Template_Width] DEFAULT(1.0)
GO


-- set the values of the columns from the soon-to-be-deleted table.
UPDATE  WindowSticker.Template
SET     Height = ps.Height ,
        Width = ps.Width
FROM    WindowSticker.Template t
        JOIN WindowSticker.PaperSize ps ON ps.PaperSizeId = t.PaperSizeId
GO

-- drop the foreign key constraint and our two temp default constraints
ALTER TABLE [WindowSticker].[Template] DROP CONSTRAINT [DF_TEMP_WindowSticker_Template_Height]
ALTER TABLE [WindowSticker].[Template] DROP CONSTRAINT [DF_TEMP_WindowSticker_Template_Width]
ALTER TABLE [WindowSticker].[Template] DROP CONSTRAINT [FK_Template_PaperSize]
GO

-- drop the papersizeid column
ALTER TABLE WindowSticker.Template
DROP COLUMN PaperSizeId
GO

-- drop the PaperSize table
DROP TABLE WindowSticker.PaperSize
GO



