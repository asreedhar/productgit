
UPDATE Marketing.MarketAnalysisPriceProvider
SET Description = 'Original Internet Price' WHERE Description = 'Original List Price'

UPDATE Marketing.MarketAnalysisPriceProvider
SET Description = 'Market Average Internet Price' WHERE Description = 'PING Market Average Selling Price'

UPDATE Marketing.MarketAnalysisPriceProvider
SET Description = 'JD Power PIN Average Selling Price' WHERE Description = 'JD Power PIN Average Internet Price'

