
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE WindowSticker.PaperSize(
    PaperSizeId TINYINT NOT NULL,
    [Name] VARCHAR(50) NOT NULL,
    Height DECIMAL(9, 3) NOT NULL,
    Width DECIMAL(9, 3) NOT NULL,
    --Units CHAR(2) NOT NULL,
    CONSTRAINT PK_PaperSize PRIMARY KEY CLUSTERED (PaperSizeId ASC),
    CONSTRAINT UK_PaperSize_Name UNIQUE NONCLUSTERED([Name])
)
GO

CREATE TABLE WindowSticker.TemplateType(
    TemplateTypeId TINYINT NOT NULL,
    [Name] VARCHAR(50) NOT NULL,
    CONSTRAINT PK_TemplateType PRIMARY KEY CLUSTERED (TemplateTypeId ASC),
    CONSTRAINT UK_TemplateType_Name UNIQUE NONCLUSTERED([Name])
)
GO


CREATE TABLE WindowSticker.FontFamily(
    FontFamilyId TINYINT NOT NULL,
    [Name] VARCHAR(50) NOT NULL,
    CONSTRAINT PK_FontFamily PRIMARY KEY CLUSTERED (FontFamilyId ASC),
    CONSTRAINT UK_FontFamily_Name UNIQUE NONCLUSTERED([Name])
)
GO

CREATE TABLE WindowSticker.FontAlign(
    FontAlignId TINYINT NOT NULL,
    [Name] VARCHAR(50) NOT NULL,
    CONSTRAINT PK_FontAlign PRIMARY KEY CLUSTERED (FontAlignId ASC),
    CONSTRAINT UK_FontAlign_Name UNIQUE NONCLUSTERED([Name])
)
GO

CREATE TABLE WindowSticker.ContentArea(
    ContentAreaId INT NOT NULL IDENTITY(1,1),
    TopLeftX DECIMAL(9,3) NOT NULL,
    TopLeftY DECIMAL(9,3) NOT NULL,
    BottomRightX DECIMAL(9,3) NOT NULL,
    BottomRightY DECIMAL(9,3) NOT NULL,
    FontFamilyId TINYINT NOT NULL,
    FontAlignId TINYINT NOT NULL,
    LineHeight DECIMAL(9,3) NOT NULL,
    CalculatedCharacterAverage SMALLINT NOT NULL,
    CalculatedEmSize DECIMAL(9,3) NOT NULL,
    CalculatedEnSize DECIMAL(9,3) NOT NULL,
    CalculatedLineHeight DECIMAL(9,3) NOT NULL,
    CalculatedBulletSize DECIMAL(9,3) NOT NULL,
    Italics BIT NOT NULL CONSTRAINT [DF_ContentArea_Italics] DEFAULT(0),
    Underline BIT NOT NULL CONSTRAINT [DF_ContentArea_Underline] DEFAULT(0),
    Bold BIT NOT NULL CONSTRAINT [DF_ContentArea_Bold] DEFAULT(0),
    CONSTRAINT PK_ContentArea PRIMARY KEY CLUSTERED (ContentAreaId ASC),
    CONSTRAINT CK_ContentArea_Height CHECK (TopLeftX < BottomRightX),
    CONSTRAINT CK_ContentArea_Width CHECK (TopLeftY < BottomRightY)
)
GO

CREATE TABLE WindowSticker.DataPointKey(
    DataPointKeyId TINYINT NOT NULL,
    [Key] VARCHAR(50) NOT NULL,
    CONSTRAINT PK_DataPointKey PRIMARY KEY CLUSTERED(DataPointKeyId),
    CONSTRAINT UK_DataPointKey UNIQUE NONCLUSTERED([Key]),
    CONSTRAINT CK_DataPointKey_KeyLength CHECK (LEN([Key]) > 0)
)

CREATE TABLE WindowSticker.DataPoint(
    DataPointId INT NOT NULL IDENTITY(1,1),
    DataPointKeyId TINYINT NULL,
    [Text] VARCHAR(1000) NULL,
    Prompt VARCHAR(1000) NULL,
    CONSTRAINT PK_DataPoint PRIMARY KEY CLUSTERED(DataPointId),
    CONSTRAINT CK_DataPoint_KeyOrText CHECK ( (DataPointKeyId IS NULL AND [Text] IS NOT NULL) OR (DataPointKeyId IS NOT NULL AND [Text] IS NULL) ),
    CONSTRAINT CK_DataPoint_TextLength CHECK ( [Text] IS NULL OR LEN([Text]) > 0),
    CONSTRAINT CK_DataPoint_PromptLength CHECK ( [Prompt] IS NULL OR LEN([Prompt]) > 0)
)

CREATE TABLE WindowSticker.ContentAreaDataPoint(
    ContentAreaId INT NOT NULL,
    DataPointId INT NOT NULL,
    CONSTRAINT PK_ContentAreaDataPoint PRIMARY KEY CLUSTERED(ContentAreaId ASC, DataPointId ASC)
)

CREATE TABLE WindowSticker.Template(
    TemplateId INT NOT NULL IDENTITY(1,1),
    TemplateTypeId TINYINT NOT NULL,
    PaperSizeId TINYINT NOT NULL,
    Units CHAR(2) NOT NULL,	-- do we need this if it is on the paper size row??
    [Name] VARCHAR(100) NOT NULL,
    StockPdf VARBINARY(MAX) NULL,
    InsertUser INT NOT NULL,
    InsertDate DATETIME NOT NULL,
    UpdateUser INT NULL,
    UpdateDate DATETIME NULL,
    CONSTRAINT PK_Template PRIMARY KEY CLUSTERED (TemplateId ASC),
    CONSTRAINT CK_Template_UpdateDateGreaterThanInsertDate CHECK  (UpdateDate IS NULL OR UpdateDate>=InsertDate),
    CONSTRAINT CK_Template_NameLength CHECK (LEN(Name) > 0),
    CONSTRAINT CK_Template_Units CHECK (Units IN ('in','cm','mm','pt','pc'))
)
GO

CREATE TABLE WindowSticker.TemplateContentArea(
    TemplateId INT NOT NULL,
    ContentAreaId INT NOT NULL,
    CONSTRAINT PK_TemplateContentArea PRIMARY KEY CLUSTERED (TemplateId ASC, ContentAreaId ASC)
)
   

CREATE TABLE WindowSticker.OwnerTemplate(
    OwnerId INT NOT NULL,
    TemplateId INT NOT NULL,
    InsertUser INT NOT NULL,
    InsertDate DATETIME NOT NULL,
    CONSTRAINT PK_OwnerTemplate PRIMARY KEY CLUSTERED (OwnerId ASC, TemplateId ASC)
)
GO


SET ANSI_PADDING OFF
GO

ALTER TABLE WindowSticker.Template  WITH CHECK ADD  CONSTRAINT FK_Template_PaperSize FOREIGN KEY(PaperSizeId)
REFERENCES WindowSticker.PaperSize (PaperSizeId)
GO

ALTER TABLE WindowSticker.Template  WITH CHECK ADD  CONSTRAINT FK_Template_TemplateType FOREIGN KEY(TemplateTypeId)
REFERENCES WindowSticker.TemplateType (TemplateTypeId)
GO

ALTER TABLE WindowSticker.ContentArea  WITH CHECK ADD  CONSTRAINT FK_ContentArea_FontFamily FOREIGN KEY(FontFamilyId)
REFERENCES WindowSticker.FontFamily (FontFamilyId)
GO

ALTER TABLE WindowSticker.ContentArea  WITH CHECK ADD  CONSTRAINT FK_ContentArea_FontAlign FOREIGN KEY(FontAlignId)
REFERENCES WindowSticker.FontAlign (FontAlignId)
GO

ALTER TABLE WindowSticker.DataPoint  WITH CHECK ADD  CONSTRAINT FK_DataPoint_DataPointKey FOREIGN KEY(DataPointKeyId)
REFERENCES WindowSticker.DataPointKey (DataPointKeyId)
GO

ALTER TABLE WindowSticker.ContentAreaDataPoint  WITH CHECK ADD  CONSTRAINT FK_ContentAreaDataPoint_DataPoint FOREIGN KEY(DataPointId)
REFERENCES WindowSticker.DataPoint (DataPointId)
GO

ALTER TABLE WindowSticker.ContentAreaDataPoint  WITH CHECK ADD  CONSTRAINT FK_ContentAreaDataPoint_ContentArea FOREIGN KEY(ContentAreaId)
REFERENCES WindowSticker.ContentArea (ContentAreaId)
GO

ALTER TABLE WindowSticker.TemplateContentArea WITH CHECK ADD CONSTRAINT FK_TemplateContentArea_Template FOREIGN KEY (TemplateId)
REFERENCES WindowSticker.Template (TemplateId)
GO

ALTER TABLE WindowSticker.TemplateContentArea WITH CHECK ADD CONSTRAINT FK_TemplateContentArea_ContentArea FOREIGN KEY (ContentAreaId)
REFERENCES WindowSticker.ContentArea (ContentAreaId)
GO

ALTER TABLE WindowSticker.OwnerTemplate WITH CHECK ADD CONSTRAINT FK_OwnerTemplate_Template FOREIGN KEY (TemplateId)
REFERENCES WindowSticker.Template (TemplateId)
GO

-- Load WindowSticker.FontAlign
INSERT INTO WindowSticker.FontAlign( FontAlignId, Name ) VALUES (1,'center')
INSERT INTO WindowSticker.FontAlign( FontAlignId, Name ) VALUES (2,'left')
INSERT INTO WindowSticker.FontAlign( FontAlignId, Name ) VALUES (3,'right')

-- Load WindowSticker.TemplateType
INSERT INTO WindowSticker.TemplateType( TemplateTypeId, Name )VALUES  ( 1, 'Window Sticker')
INSERT INTO WindowSticker.TemplateType( TemplateTypeId, Name )VALUES  ( 2, 'Buyers Guide')

-- Load WindowSticker.DataPointKey
INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] )VALUES  ( 1,'Year')
INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] )VALUES  ( 2,'Make')
INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] )VALUES  ( 3,'Model')
INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] )VALUES  ( 4,'Trim')
INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] )VALUES  ( 5,'VehicleDescription')
INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] )VALUES  ( 6,'ExteriorColor')
INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] )VALUES  ( 7,'InteriorColor')
INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] )VALUES  ( 8,'Mileage')
INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] )VALUES  ( 9,'StockNumber')
INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] )VALUES  ( 10,'Transmission')
INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] )VALUES  ( 11,'Engine')
INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] )VALUES  ( 12,'Drivetrain')
INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] )VALUES  ( 13,'Certified')
INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] )VALUES  ( 14,'BodyStyle')

/* Load WindowSticker.PaperSize
INSERT INTO WindowSticker.PaperSize(PaperSizeId,Name,Height,Width,Units)
VALUES  (1,'Letter' ,8.5,11,'in')
*/

-- Load WindowSticker.FontFamily
INSERT INTO WindowSticker.FontFamily( FontFamilyId, Name )VALUES  ( 1,'Arial')
INSERT INTO WindowSticker.FontFamily( FontFamilyId, Name )VALUES  ( 2,'Arial Black')
INSERT INTO WindowSticker.FontFamily( FontFamilyId, Name )VALUES  ( 3,'Comic Sans MS')
INSERT INTO WindowSticker.FontFamily( FontFamilyId, Name )VALUES  ( 4,'Courier New')
INSERT INTO WindowSticker.FontFamily( FontFamilyId, Name )VALUES  ( 5,'Georgia')
INSERT INTO WindowSticker.FontFamily( FontFamilyId, Name )VALUES  ( 6,'Impact')
INSERT INTO WindowSticker.FontFamily( FontFamilyId, Name )VALUES  ( 7,'Lucida Console')
INSERT INTO WindowSticker.FontFamily( FontFamilyId, Name )VALUES  ( 8,'Lucida Grande')
INSERT INTO WindowSticker.FontFamily( FontFamilyId, Name )VALUES  ( 9,'Tahoma')
INSERT INTO WindowSticker.FontFamily( FontFamilyId, Name )VALUES  ( 10,'Time New Roman')
INSERT INTO WindowSticker.FontFamily( FontFamilyId, Name )VALUES  ( 11,'Trebuchet')
INSERT INTO WindowSticker.FontFamily( FontFamilyId, Name )VALUES  ( 12,'Verdana')





