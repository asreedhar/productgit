-- add missing keys
INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] ) VALUES  ( 0, 'Undefined')
INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] ) VALUES  ( 15, 'Vin')
INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] ) VALUES  ( 16, 'Series')
INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] ) VALUES  ( 17, 'Price')
INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] ) VALUES  ( 18, 'Equipment')
INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] ) VALUES  ( 19, 'AgeInDays')