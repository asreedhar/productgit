
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AppraisalType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AppraisalType]
GO
 
CREATE TABLE dbo.AppraisalType (
    AppraisalTypeID		INT NOT NULL,
    Description			VARCHAR(45) NOT NULL,
	CONSTRAINT [PK_AppraisalType] PRIMARY KEY  CLUSTERED 
	(
		[AppraisalTypeID]
	) 
)

GO

INSERT INTO dbo.AppraisalType VALUES (1, 'Trade-In');
INSERT INTO dbo.AppraisalType VALUES (2, 'Purchase');

GO

if not exists (select * from dbo.syscolumns where id = object_id(N'Appraisals') and name = 'AppraisalTypeID')
ALTER TABLE [dbo].Appraisals
ADD AppraisalTypeID INT NOT NULL DEFAULT 1
GO 

-- At the time of this alter all appraisals in the system are of type Trade-In
ALTER TABLE [dbo].Appraisals 
ADD CONSTRAINT FK_Appraisals_AppraisalType FOREIGN KEY
(
AppraisalTypeID
) REFERENCES [dbo].AppraisalType
(
AppraisalTypeID
)
GO

