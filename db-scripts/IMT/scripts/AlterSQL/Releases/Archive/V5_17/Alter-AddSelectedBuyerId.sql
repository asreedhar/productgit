if not exists (select * from dbo.syscolumns where id = object_id(N'Appraisals') and name = 'SelectedBuyerId')
ALTER TABLE [dbo].Appraisals
ADD SelectedBuyerId INT DEFAULT NULL
GO 
