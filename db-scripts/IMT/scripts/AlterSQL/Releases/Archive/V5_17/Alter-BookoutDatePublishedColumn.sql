
---
--- NK - 07/30/08
---  	Remove the version string from all bookouts that have it. Column should only be date.
---

UPDATE BOOKOUTS
SET DatePublished = SUBSTRING(DatePublished,0, (CHARINDEX('v0', DatePublished) - 1))
WHERE	DatePublished like '% v0%'

GO
