--------------------------------------------------------------------------------
--	
--	Use the "Business Unit Preference" concept to store attributes at any
--	level of the BusinessUnit hierarchy.  For this particular case, we
--	are assigning the bookout processor priorities at the dealer group
--	level.  Note that this pattern requires a companion table, view, or
--	function to access the attributes and properly propagate then to 
--	the multiple levels of the business unit hierarchy.  In this case,
--	we'll use a simple view called "DealerPreference_Bookout" where the
--	attributes in BusinessUnitPreference_Bookout may be at the dealer
--	or dealer group level and are accessed ONLY at the business unit level.
--
--	Note that attributes are prioritized from dealer on up, so we can set
--	a priority at the dealer level that would override the parent.  Ex: Hendrick
--	City Chevrolet.
-- 
--------------------------------------------------------------------------------

CREATE TABLE dbo.BusinessUnitPreference_Bookout (	
			BusinessUnitID		INT NOT NULL CONSTRAINT PK_BusinessUnitPreference_Bookout PRIMARY KEY CLUSTERED,
			ProcessorPriority	TINYINT NOT NULL CONSTRAINT DF_BusinessUnitPreference_Bookout__ProcessorPriority DEFAULT (0),
			CONSTRAINT FK_BusinessUnitPreference_Bookout__BusinessUnit FOREIGN KEY (BusinessUnitID) REFERENCES dbo.BusinessUnit (BusinessUnitID)
			)
			
GO

INSERT
INTO	dbo.BusinessUnitPreference_Bookout (BusinessUnitID, ProcessorPriority)

SELECT	100068, 7		-- Hendrick Automotive Group
UNION SELECT 100914, 6		-- Penske Auto Group NC
UNION SELECT 100933, 6		-- Penske Auto Group Atlanta
UNION SELECT 101033, 6		-- Penske Auto Group Florida
UNION SELECT 101054, 6		-- Penske Auto Group California
UNION SELECT 101897, 6		-- Penske Auto Group Central
UNION SELECT 102378, 6		-- Penske Auto Group Turnersville
UNION SELECT 102387, 6		-- Penske Auto Group Little Rock
UNION SELECT 102391, 6		-- Penske Auto Group Fayetteville
UNION SELECT 102569, 6		-- Penske Auto Group Texas
UNION SELECT 101363, 6		-- Penske Auto Group Arizona
UNION SELECT 100214, 5		-- Penske Motor Group
UNION SELECT 101676, 4		-- Luther Auto Group
UNION SELECT 101397, 3		-- Ron Tonkin Group
UNION SELECT 101726, 2		-- Herb Chambers Group
UNION SELECT 101327, 1		-- Lorensen Auto Group

GO	

INSERT
INTO	dbo.BusinessUnitPreference_Bookout (BusinessUnitID, ProcessorPriority)
SELECT	100147, 8		-- Hendrick City Chevrolet
GO
		