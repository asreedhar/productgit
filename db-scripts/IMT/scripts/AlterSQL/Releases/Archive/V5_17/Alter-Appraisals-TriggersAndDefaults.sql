--------------------------------------------------------------------------------------------
--	PER PM WE DON'T WANT THE TRIGGER ANYMORE AS THE APPLICATION WILL HANDLE THE UPDATING
--	OF THE DateModified COLUMN...
--------------------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[TR_IU_Appraisals]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
DROP TRIGGER [dbo].[TR_IU_Appraisals]
GO

--------------------------------------------------------------------------------------------
--	NOW, IN ORDER TO AVOID UPSETTING EXISTING METRICS, WE NEED TO ASSIGN THE DateCreated
--	TO DateModified. 
--------------------------------------------------------------------------------------------

UPDATE	Appraisals
SET	DateModified = DateCreated

--------------------------------------------------------------------------------------------
--	THE DEFAULT WAS CREATED W/O A PROPER NAME, SO SQL SERVER ASSIGNED ONE.
--	RETREIVE THE GENERATED NAME AND DROP THE CONSTRAINT, THEN ADD THE REPLACEMENT
--------------------------------------------------------------------------------------------

DECLARE @name VARCHAR(128)

SELECT	@name = do.name
FROM	sys.objects o
	JOIN sys.columns c ON o.OBJECT_ID = c.OBJECT_ID
	JOIN sys.objects do ON c.default_object_id = do.object_id
WHERE	o.NAME = 'Appraisals'
	AND c.NAME = 'DateModified'
	
EXEC ('ALTER TABLE dbo.Appraisals DROP CONSTRAINT ' + @name)

ALTER TABLE dbo.Appraisals ADD CONSTRAINT DF_Appraisals__DateModified DEFAULT (GETDATE()) FOR DateModified

GO

