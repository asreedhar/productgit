----------------------------------------------------------------------------------------------------
--	Depending on a members preference, when search a VIN/Stk number from the homepage or search widget
--  a member wants to be taken either to the VPA or the eStock card
--	0 = eStock (DEFAULT)
--	1 = VPA
--
--  AND yes, this ridiculous preference does not belong on the member table OR encodes as a simple
--  Integer value - BUT - PM says this is temporary until we can actually include this as a role
--  based preference. I am sure we're going to be doing the work any day now...
----------------------------------------------------------------------------------------------------

ALTER TABLE dbo.Member ADD ActiveInventoryLaunchTool TINYINT NOT NULL CONSTRAINT DF_Member__ActiveInventoryLaunchTool DEFAULT (0)
GO

ALTER TABLE dbo.Member ADD CONSTRAINT CK_Member__ActiveInventoryLaunchTool CHECK (ActiveInventoryLaunchTool IN (0,1))
GO
