
if not exists (select * from dbo.syscolumns where id = object_id(N'DealerPreference') and name = 'SearchAppraisalDaysBackThreshold')
ALTER TABLE [dbo].DealerPreference
ADD SearchAppraisalDaysBackThreshold INT NOT NULL DEFAULT 45
GO 
