
---
---  NK - 07/28/08
---
---  If a Galves Bookout vehhicle option is Standard, it should NOT also be selected.
---  If it is standard the UI should grey it out (and autoselects it).
---  This alter removes the 'selected' status from the option if it is standard.
---  This fixes a display problem that occured as a result of the bug in FB:1538
---

UPDATE 	dbo.ThirdPartyVehicleOptions 
SET 	status=0
WHERE	ThirdPartyVehicleOptionID IN  
(
	SELECT 	tpvo.ThirdPartyVehicleOptionID
	FROM	dbo.Bookouts b
	JOIN 	dbo.InventoryBookOuts ib ON ib.bookoutId=b.bookoutId
	JOIN 	dbo.Inventory i ON i.inventoryId=ib.inventoryId
	JOIN 	dbo.BookoutThirdPartyVehicles btpv ON btpv.bookoutId=b.bookOutid
	JOIN 	dbo.ThirdPartyVehicles tpv ON tpv.thirdPartyVehicleId = btpv.thirdPartyVehicleId
	JOIN 	dbo.ThirdPartyVehicleOptions tpvo ON tpvo.ThirdPartyVehicleID=btpv.ThirdPartyVehicleID
	WHERE 	tpv.thirdPartyId=4
	AND 	i.inventoryActive = 1
	AND 	tpvo.IsStandardOption=1 
	AND 	tpvo.status=1
)