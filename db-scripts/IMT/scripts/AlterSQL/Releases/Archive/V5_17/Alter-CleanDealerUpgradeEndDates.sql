
---
---  nk - 08/03/08
---   	FB : 1734.  The max product respects the End date that is set for the standard appraisal, purchasing and AIP upgrades.
---					DM, however, has not always set the endDates for these upgrades as they should. They will commonly set
---					the startDate=endDate to indicate : keep upgrade on indefinitely. This is wrong. 
---					To fix this we are setting upgrades in this state to have a NULL end date, which IS a way to indicate
---					the upgrade should be on indefinitely. We have also communicated with DM to update their procedures.
---   
---


UPDATE 	dealerUpgrade
SET 	endDate = null
WHERE 	dealerUpgradeCD in (1,2,3)
AND 	active = 1
AND 	endDate = startDate