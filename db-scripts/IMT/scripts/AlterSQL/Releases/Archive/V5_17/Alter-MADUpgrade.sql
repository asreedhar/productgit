----------------------------------------------------------------------------
--	THIS ALTER CREATES THE MAKE-A-DEAL UPGRADE
----------------------------------------------------------------------------

INSERT INTO lu_DealerUpgrade VALUES (21, 'Make-A-Deal')
GO


-- Give every dealership that currently has MAX the new MAD upgrade

INSERT INTO DealerUpgrade (BusinessUnitId, DealerUpgradeCD, StartDate, EndDate, Active)
(SELECT DISTINCT(du.businessUnitId), 21, GetDate(), NULL, 1 
FROM DealerUpgrade du 
WHERE DealerUpgradeCD IN (15,16))
GO 
