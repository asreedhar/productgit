Alter TABLE DealerPreference_Pricing Add  MarketDaysSupplyBasePeriod int NULL
Go
Alter TABLE DealerPreference_Pricing Add  VehicleSalesBasePeriod int NULL
Go
Alter TABLE DealerPreference_Pricing Add  VehicleUnwindPeriod int NULL 
Go
Update dbo.DealerPreference_Pricing set MarketDaysSupplyBasePeriod =90,
 VehicleSalesBasePeriod=90,
 VehicleUnwindPeriod=14
where BusinessUnitID=100150 and MarketDaysSupplyBasePeriod is null

