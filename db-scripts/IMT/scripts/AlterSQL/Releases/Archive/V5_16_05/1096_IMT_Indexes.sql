
declare @BuildIndex bit
select @BuildIndex = 1

--********************************************
--IMT.dbo.tbl_Vehicle
IF (EXISTS(	SELECT * FROM SYS.Indexes 
		WHERE	NAME = 'IX_tbl_VehicleVehicleIdVehicleCatalogId' 
			AND OBJECT_ID = OBJECT_ID('dbo.tbl_Vehicle')))
	DROP INDEX dbo.tbl_Vehicle.IX_tbl_VehicleVehicleIdVehicleCatalogId
IF (@BuildIndex = 1)
	CREATE INDEX IX_tbl_VehicleVehicleIdVehicleCatalogId 
	on dbo.tbl_Vehicle
	(VehicleId, VehicleCatalogId) 
	--INCLUDE (Included non-indexed columns)
	
	on IDX --specify filegroup
--********************************************

--********************************************
--IMT.dbo.Inventory
IF (EXISTS(	SELECT * FROM SYS.Indexes 
		WHERE	NAME = 'IX_InventoryVehicleIdBusinessUnitIdInventoryReceivedDate' 
			AND OBJECT_ID = OBJECT_ID('dbo.Inventory')))
	DROP INDEX dbo.Inventory.IX_InventoryVehicleIdBusinessUnitIdInventoryReceivedDate
IF (@BuildIndex = 1)
	CREATE INDEX IX_InventoryVehicleIdBusinessUnitIdInventoryReceivedDate 
	on dbo.Inventory
	(VehicleId, BusinessUnitId)--, InventoryReceivedDate) 
	INCLUDE --(Included non-indexed columns)
	(InventoryReceiveddate)
	on IDX --specify filegroup
--********************************************
