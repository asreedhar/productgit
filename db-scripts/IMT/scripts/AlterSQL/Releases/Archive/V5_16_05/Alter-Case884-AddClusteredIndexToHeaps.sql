DECLARE @BuildIndex bit
SET @BuildIndex = 1


--***********************************************************************
--dbo.AppraisalBusinessUnitGroupDemand
--***********************************************************************
--remove nonclustered pk (AppraisalId)
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_AppraisalBusinessUnitGroupDemand' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.AppraisalBusinessUnitGroupDemand'))
	)
		ALTER TABLE dbo.AppraisalBusinessUnitGroupDemand
		DROP CONSTRAINT PK_AppraisalBusinessUnitGroupDemand
--add clustered pk (AppraisalId)
IF (@BuildIndex = 1)
	ALTER TABLE dbo.AppraisalBusinessUnitGroupDemand
	ADD CONSTRAINT PK_AppraisalBusinessUnitGroupDemand 
	PRIMARY KEY CLUSTERED
	(AppraisalID ASC, BusinessUnitID)
	WITH  FILLFACTOR = 90 ON DATA


--***********************************************************************
--dbo.AppraisalFormOptions
--***********************************************************************
--remove non-standard named PK
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_AppraisalFormId' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.AppraisalFormOptions'))
	)
		ALTER TABLE dbo.AppraisalFormOptions
		DROP CONSTRAINT PK_AppraisalFormID

--remove non-clustered pk (AppraisalFormId)
--add clustered pk (AppraisalId)
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_AppraisalFormOptions' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.AppraisalFormOptions'))
	)
		ALTER TABLE dbo.AppraisalFormOptions
		DROP CONSTRAINT PK_AppraisalFormOptions
IF (@BuildIndex = 1)
	ALTER TABLE dbo.AppraisalFormOptions
	ADD CONSTRAINT PK_AppraisalFormOptions
	PRIMARY KEY CLUSTERED
	(AppraisalFormId ASC)
	WITH FILLFACTOR = 90 ON DATA


--***********************************************************************
--dbo.BusinessUnit
--***********************************************************************
--Because there are so many existing foreign keys to the Business Unit table,
--simply create a new clustered index on BusinessUnitId

--remove clustered index (BusinessUnitId)
--add clustered index (BusinessUnitId)
IF	(EXISTS(SELECT * FROM SYS.INDEXES 
		WHERE NAME = 'IX_BusinessUnitClustered' 
			AND OBJECT_ID = OBJECT_ID('IMT.dbo.BusinessUnit'))
	)
	DROP INDEX dbo.BusinessUnit.IX_BusinessUnitClustered
IF (@BuildIndex = 1)
	CREATE CLUSTERED INDEX IX_BusinessUnitClustered
	ON dbo.BusinessUnit
	(BusinessUnitID ASC)
	WITH FILLFACTOR = 90 ON DATA


--***********************************************************************
--dbo.CIABodyTypeDetails
--***********************************************************************
--Drop foreign keys
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CIAGroupingItems_CIABodyTypeDetails]') AND parent_object_id = OBJECT_ID(N'[dbo].[CIAGroupingItems]'))
ALTER TABLE [dbo].[CIAGroupingItems] DROP CONSTRAINT [FK_CIAGroupingItems_CIABodyTypeDetails]

--remove non-standard named primary key constraint
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[CIABodyTypeDetails]') AND name = N'PK_CIABodyTypeDetail')
ALTER TABLE [dbo].[CIABodyTypeDetails] DROP CONSTRAINT [PK_CIABodyTypeDetail]

--removed non-clustered pk (CIABodyTypeDetailId) 
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_CIABodyTypeDetails' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.CIABodyTypeDetails'))
	)
		ALTER TABLE dbo.CIABodyTypeDetails
		DROP CONSTRAINT PK_CIABodyTypeDetails
--add clustered pk (CIABodyTypeDetailId)
IF (@BuildIndex = 1)
	ALTER TABLE dbo.CIABodyTypeDetails
	ADD CONSTRAINT PK_CIABodyTypeDetails
	PRIMARY KEY CLUSTERED
	(CIABodyTypeDetailID ASC)
	WITH FILLFACTOR = 90 ON DATA

--recreate Foreign Keys
ALTER TABLE [dbo].[CIAGroupingItems]  WITH CHECK ADD  CONSTRAINT [FK_CIAGroupingItems_CIABodyTypeDetails] FOREIGN KEY([CIABodyTypeDetailID])
REFERENCES [dbo].[CIABodyTypeDetails] ([CIABodyTypeDetailID])


--***********************************************************************
--dbo.CIASummary
--***********************************************************************
--remove Foreign Keys
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CIAInventoryOverstocking_CIASummary]') AND parent_object_id = OBJECT_ID(N'[dbo].[CIAInventoryOverstocking]'))
ALTER TABLE [dbo].[CIAInventoryOverstocking] DROP CONSTRAINT [FK_CIAInventoryOverstocking_CIASummary]
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CIABodyTypeDetail_CIASummary]') AND parent_object_id = OBJECT_ID(N'[dbo].[CIABodyTypeDetails]'))
ALTER TABLE [dbo].[CIABodyTypeDetails] DROP CONSTRAINT [FK_CIABodyTypeDetail_CIASummary]

--remove non-clustered pk (CIASummaryId)
--add clustered pk (CIASummaryId)
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_CIASummary' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.CIASummary'))
	)
		ALTER TABLE dbo.CIASummary 
		DROP CONSTRAINT PK_CIASummary
IF (@BuildIndex = 1)
	ALTER TABLE dbo.CIASummary
	ADD CONSTRAINT PK_CIASummary
	PRIMARY KEY CLUSTERED
	(CIASummaryId ASC)
	WITH FILLFACTOR = 90 ON DATA
--recreate foreign keys
ALTER TABLE [dbo].[CIAInventoryOverstocking]  WITH CHECK ADD  CONSTRAINT [FK_CIAInventoryOverstocking_CIASummary] FOREIGN KEY([CIASummaryID])
REFERENCES [dbo].[CIASummary] ([CIASummaryID])
ALTER TABLE [dbo].[CIABodyTypeDetails]  WITH CHECK ADD  CONSTRAINT [FK_CIABodyTypeDetail_CIASummary] FOREIGN KEY([CIASummaryID])
REFERENCES [dbo].[CIASummary] ([CIASummaryID])


--***********************************************************************
--dbo.DealerFacts
--***********************************************************************
--remove non-clustered pk (DealerFactId)
--add clustered pk (DealerFactId)
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_DealerFacts' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.DealerFacts'))
	)
		ALTER TABLE dbo.DealerFacts
		DROP CONSTRAINT PK_DealerFacts
IF (@BuildIndex = 1)
	ALTER TABLE dbo.DealerFacts
	ADD CONSTRAINT PK_DealerFacts
	PRIMARY KEY CLUSTERED
	(DealerFactId ASC)
	WITH FILLFACTOR = 90 ON DATA


--***********************************************************************
--dbo.DealerGridThresholds
--***********************************************************************
--remove non-clustered pk (DealerGridThresholdsId)
--add clustered pk (DealerGridThresholdsId)
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_DealerGridThresholds' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.DealerGridThresholds'))
	)
		ALTER TABLE dbo.DealerGridThresholds
		DROP CONSTRAINT PK_DealerGridThresholds
IF (@BuildIndex = 1)
	ALTER TABLE dbo.DealerGridThresholds
	ADD CONSTRAINT PK_DealerGridThresholds
	PRIMARY KEY CLUSTERED
	(DealerGridThresholdsId ASC)
	WITH FILLFACTOR = 90 ON DATA


--***********************************************************************
--dbo.DealerGroupPreference
--***********************************************************************
--remove non-clustered pk (DealerGroupPreference)
--add clustered pk (DealerGridThresholdsId)
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_DealerGroupPreference' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.DealerGroupPreference'))
	)
		ALTER TABLE dbo.DealerGroupPreference
		DROP CONSTRAINT PK_DealerGroupPreference
IF (@BuildIndex = 1)
	ALTER TABLE dbo.DealerGroupPreference
	ADD CONSTRAINT PK_DealerGroupPreference
	PRIMARY KEY CLUSTERED
	(DealerGroupPreferenceId ASC)
	WITH FILLFACTOR = 90 ON DATA


--***********************************************************************
--dbo.DealerPreference
--***********************************************************************
--remove non-clustered pk (DealerPreference)
--add clustered pk (DealerPreferenceId)
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_DealerPreference' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.DealerPreference'))
	)
		ALTER TABLE dbo.DealerPreference
		DROP CONSTRAINT PK_DealerPreference
IF (@BuildIndex = 1)
	ALTER TABLE dbo.DealerPreference
	ADD CONSTRAINT PK_DealerPreference
	PRIMARY KEY CLUSTERED
	(DealerPreferenceId ASC)
	WITH FILLFACTOR = 90 ON DATA


--***********************************************************************
--dbo.DealerPreferenceDataload
--***********************************************************************
--remove non-clustered pk (DealerPreferenceDataloadId)
--add clustered pk (DealerPreferenceDataloadId)
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_DealerPreferenceDataload' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.DealerPreferenceDataload'))
	)
		ALTER TABLE dbo.DealerPreferenceDataload
		DROP CONSTRAINT PK_DealerPreferenceDataload
IF (@BuildIndex = 1)
	ALTER TABLE dbo.DealerPreferenceDataload
	ADD CONSTRAINT PK_DealerPreferenceDataload
	PRIMARY KEY CLUSTERED
	(DealerPreferenceDataloadId ASC)
	WITH FILLFACTOR = 90 ON DATA


--***********************************************************************
--dbo.DealerTarget
--***********************************************************************
--remove non-clustered pk (DealerTargetId)
--add clustered pk (DealerTargetIdId)
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_DealerTarget' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.DealerTarget'))
	)
		ALTER TABLE dbo.DealerTarget
		DROP CONSTRAINT PK_DealerTarget
IF (@BuildIndex = 1)
	ALTER TABLE dbo.DealerTarget
	ADD CONSTRAINT PK_DealerTarget
	PRIMARY KEY CLUSTERED
	(DealerTargetId ASC)
	WITH FILLFACTOR = 90 ON DATA


--***********************************************************************
--dbo.MarketShare
--***********************************************************************
--remove non-clusterd pk (MarketShareId)
--add clustered pk (MarketShareId)
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_MarketShare' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.MarketShare'))
	)
		ALTER TABLE dbo.MarketShare
		DROP CONSTRAINT PK_MarketShare
IF (@BuildIndex = 1)
	ALTER TABLE dbo.MarketShare
	ADD CONSTRAINT PK_MarketShare
	PRIMARY KEY CLUSTERED
	(MarketShareId ASC)
	WITH FILLFACTOR = 90 ON DATA


--***********************************************************************
--dbo.MemberAccess
--***********************************************************************
--remove non-clusterd pk (MemberId, BusinessUnitId)
--add clustered pk (MemberId, BusinessUnitId)
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_MemberAccess' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.MemberAccess'))
	)
		ALTER TABLE dbo.MemberAccess
		DROP CONSTRAINT PK_MemberAccess
IF (@BuildIndex = 1)
	ALTER TABLE dbo.MemberAccess
	ADD CONSTRAINT PK_MemberAccess
	PRIMARY KEY CLUSTERED
	(MemberId ASC, BusinessUnitId)
	WITH FILLFACTOR = 90 ON DATA


--***********************************************************************
--dbo.SubscriptionTypeSubscriptionDeliveryType
--***********************************************************************
--remove non-clusterd pk (SubscriptionTypeSubscriptionDeliveryTypeId)
--add clustered pk (SubscriptionTypeSubscriptionDeliveryTypeId)
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_SubscriptionTypeSubscriptionDeliveryType' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.SubscriptionTypeSubscriptionDeliveryType'))
	)
		ALTER TABLE dbo.SubscriptionTypeSubscriptionDeliveryType
		DROP CONSTRAINT PK_SubscriptionTypeSubscriptionDeliveryType
IF (@BuildIndex = 1)
	ALTER TABLE dbo.SubscriptionTypeSubscriptionDeliveryType
	ADD CONSTRAINT PK_SubscriptionTypeSubscriptionDeliveryType
	PRIMARY KEY CLUSTERED
	(SubscriptionTypeSubscriptionDeliveryTypeId ASC)
	WITH FILLFACTOR = 90 ON DATA


--***********************************************************************
--dbo.SubscriptionTypeSubscriptionFrequencyDetail
--***********************************************************************
--remove non-clusterd pk (SubscriptionTypeSubscriptionFrequencyDetailId)
--add clustered pk (SubscriptionTypeSubscriptionFrequencyDetailId)
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_SubscriptionTypeSubscriptionFrequencyDetail' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.SubscriptionTypeSubscriptionFrequencyDetail'))
	)
		ALTER TABLE dbo.SubscriptionTypeSubscriptionFrequencyDetail
		DROP CONSTRAINT PK_SubscriptionTypeSubscriptionFrequencyDetail
IF (@BuildIndex = 1)
	ALTER TABLE dbo.SubscriptionTypeSubscriptionFrequencyDetail
	ADD CONSTRAINT PK_SubscriptionTypeSubscriptionFrequencyDetail
	PRIMARY KEY CLUSTERED
	(SubscriptionTypeSubscriptionFrequencyDetailId ASC)
	WITH FILLFACTOR = 90 ON DATA


--***********************************************************************
--dbo.tbl_CIAPreferences
--***********************************************************************
--remove non-clusterd pk (BusinessUnitID)
--add clustered pk (BusinessUnitID)
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_tbl_CIAPreferences' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.tbl_CIAPreferences'))
	)
		ALTER TABLE dbo.tbl_CIAPreferences
		DROP CONSTRAINT PK_tbl_CIAPreferences
IF (@BuildIndex = 1)
	ALTER TABLE dbo.tbl_CIAPreferences
	ADD CONSTRAINT PK_tbl_CIAPreferences
	PRIMARY KEY CLUSTERED
	(BusinessUnitID ASC)
	WITH FILLFACTOR = 90 ON DATA


--***********************************************************************
--dbo.tbl_DealerATCAccessGroups
--***********************************************************************

--remove non-standard named Primary Key
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_DealerATCAccessGroups' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.tbl_DealerATCAccessGroups'))
	)
		ALTER TABLE dbo.tbl_DealerATCAccessGroups
		DROP CONSTRAINT PK_DealerATCAccessGroups

--remove non-clusterd pk (DealerATCAccessGroupsId)
--add clustered pk (DealerATCAccessGroupsId)
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_tbl_DealerATCAccessGroups' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.tbl_DealerATCAccessGroups'))
	)
		ALTER TABLE dbo.tbl_DealerATCAccessGroups
		DROP CONSTRAINT PK_tbl_DealerATCAccessGroups
IF (@BuildIndex = 1)
	ALTER TABLE dbo.tbl_DealerATCAccessGroups
	ADD CONSTRAINT PK_tbl_DealerATCAccessGroups
	PRIMARY KEY CLUSTERED
	(DealerATCAccessGroupsId ASC)
	WITH FILLFACTOR = 90 ON DATA


--***********************************************************************
--dbo.tbl_GroupingDescription
--***********************************************************************

--remove foreign keys
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CIAGroupingItems_GroupingDescriptionID]') AND parent_object_id = OBJECT_ID(N'[dbo].[CIAGroupingItems]'))
ALTER TABLE [dbo].[CIAGroupingItems] DROP CONSTRAINT [FK_CIAGroupingItems_GroupingDescriptionID]
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbl_MakeModelGrouping__tbl_GroupingDescription]') AND parent_object_id = OBJECT_ID(N'[dbo].[MakeModelGrouping]'))
ALTER TABLE [dbo].[MakeModelGrouping] DROP CONSTRAINT [FK_tbl_MakeModelGrouping__tbl_GroupingDescription]
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_tbl_GroupingPromotionPlan__tbl_GroupingDescription]') AND parent_object_id = OBJECT_ID(N'[dbo].[tbl_GroupingPromotionPlan]'))
ALTER TABLE [dbo].[tbl_GroupingPromotionPlan] DROP CONSTRAINT [FK_tbl_GroupingPromotionPlan__tbl_GroupingDescription]
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GDLight__tbl_GroupingDescription]') AND parent_object_id = OBJECT_ID(N'[dbo].[GDLight]'))
ALTER TABLE [dbo].[GDLight] DROP CONSTRAINT [FK_GDLight__tbl_GroupingDescription]
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MarketShare_GroupingDescriptionID]') AND parent_object_id = OBJECT_ID(N'[dbo].[MarketShare]'))
ALTER TABLE [dbo].[MarketShare] DROP CONSTRAINT [FK_MarketShare_GroupingDescriptionID]

--remove non-standard named pk
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_GroupingDescription' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.tbl_GroupingDescription'))
	)
		ALTER TABLE dbo.tbl_GroupingDescription
		DROP CONSTRAINT PK_GroupingDescription
--remove non-clusterd pk (GroupingDescriptionId)
--add clustered pk (GroupingDescriptionId)
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_GroupingDescription' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.tbl_GroupingDescription'))
	)
		ALTER TABLE dbo.tbl_GroupingDescription
		DROP CONSTRAINT PK_tbl_GroupingDescription
IF (@BuildIndex = 1)
	ALTER TABLE dbo.tbl_GroupingDescription
	ADD CONSTRAINT PK_tbl_GroupingDescription
	PRIMARY KEY CLUSTERED
	(GroupingDescriptionId ASC)
	WITH FILLFACTOR = 90 ON DATA

--recreate foreign keys
ALTER TABLE [dbo].[CIAGroupingItems]  WITH CHECK ADD  CONSTRAINT [FK_CIAGroupingItems_GroupingDescriptionID] FOREIGN KEY([GroupingDescriptionID])
REFERENCES [dbo].[tbl_GroupingDescription] ([GroupingDescriptionID])
ALTER TABLE [dbo].[MakeModelGrouping]  WITH CHECK ADD  CONSTRAINT [FK_tbl_MakeModelGrouping__tbl_GroupingDescription] FOREIGN KEY([GroupingDescriptionID])
REFERENCES [dbo].[tbl_GroupingDescription] ([GroupingDescriptionID])
ALTER TABLE [dbo].[tbl_GroupingPromotionPlan]  WITH CHECK ADD  CONSTRAINT [FK_tbl_GroupingPromotionPlan__tbl_GroupingDescription] FOREIGN KEY([GroupingDescriptionId])
REFERENCES [dbo].[tbl_GroupingDescription] ([GroupingDescriptionID])
ALTER TABLE [dbo].[GDLight]  WITH CHECK ADD  CONSTRAINT [FK_GDLight__tbl_GroupingDescription] FOREIGN KEY([GroupingDescriptionId])
REFERENCES [dbo].[tbl_GroupingDescription] ([GroupingDescriptionID])
ALTER TABLE [dbo].[MarketShare]  WITH NOCHECK ADD  CONSTRAINT [FK_MarketShare_GroupingDescriptionID] FOREIGN KEY([GroupingDescriptionID])
REFERENCES [dbo].[tbl_GroupingDescription] ([GroupingDescriptionID])


--***********************************************************************
--dbo.tbl_MemberATCAccessGroup
--***********************************************************************

--remove non-standard named primary key
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_MemberATCAccessGroup' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.tbl_MemberATCAccessGroup'))
	)
		ALTER TABLE dbo.tbl_MemberATCAccessGroup
		DROP CONSTRAINT PK_MemberATCAccessGroup

--remove non-clusterd pk (MemberATCAccessGroupId)
--add clustered pk (MemberATCAccessGroupId)
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_tbl_MemberATCAccessGroup' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.tbl_MemberATCAccessGroup'))
	)
		ALTER TABLE dbo.tbl_MemberATCAccessGroup
		DROP CONSTRAINT PK_tbl_MemberATCAccessGroup
IF (@BuildIndex = 1)
	ALTER TABLE dbo.tbl_MemberATCAccessGroup
	ADD CONSTRAINT PK_tbl_MemberATCAccessGroup
	PRIMARY KEY CLUSTERED
	(MemberATCAccessGroupId ASC)
	WITH FILLFACTOR = 90 ON DATA


--***********************************************************************
--dbo.tbl_NewCarScorecard
--***********************************************************************
--remove non-standard named primary key
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_NewCarScorecard' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.tbl_NewCarScorecard'))
	)
		ALTER TABLE dbo.tbl_NewCarScorecard
		DROP CONSTRAINT PK_NewCarScorecard

--remove non-clusterd pk (NewCarScorecardId)
--add clustered pk (NewCarScorecardId)
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_tbl_NewCarScorecard' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.tbl_NewCarScorecard'))
	)
		ALTER TABLE dbo.tbl_NewCarScorecard
		DROP CONSTRAINT PK_tbl_NewCarScorecard
IF (@BuildIndex = 1)
	ALTER TABLE dbo.tbl_NewCarScorecard
	ADD CONSTRAINT PK_tbl_NewCarScorecard
	PRIMARY KEY CLUSTERED
	(NewCarScoreCardId ASC)
	WITH FILLFACTOR = 90 ON DATA


--***********************************************************************
--dbo.tbl_RedistributionLog
--***********************************************************************
--remove non-clusterd pk (RedistributionLogId)
--add clustered pk (RedistributionLogId)
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_tbl_RedistributionLog' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.tbl_RedistributionLog'))
	)
		ALTER TABLE dbo.tbl_RedistributionLog
		DROP CONSTRAINT PK_tbl_RedistributionLog
IF (@BuildIndex = 1)
	ALTER TABLE dbo.tbl_RedistributionLog
	ADD CONSTRAINT PK_tbl_RedistributionLog
	PRIMARY KEY CLUSTERED
	(RedistributionLogId ASC)
	WITH FILLFACTOR = 90 ON DATA


--***********************************************************************
--dbo.tbl_UsedCarScorecard
--***********************************************************************

--remove non-standard named primary key
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_UsedCarScorecard' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.tbl_UsedCarScorecard'))
	)
		ALTER TABLE dbo.tbl_UsedCarScorecard
		DROP CONSTRAINT PK_UsedCarScorecard

--remove non-clusterd pk (UsedCarScorecardId)
--add clustered pk (UsedCarScorecardId)
IF	(EXISTS(SELECT * FROM SYS.OBJECTS 
		WHERE NAME = 'PK_tbl_UsedCarScorecard' 
			AND TYPE = 'PK' 
			AND PARENT_OBJECT_ID = OBJECT_ID('IMT.dbo.tbl_UsedCarScorecard'))
	)
		ALTER TABLE dbo.tbl_UsedCarScorecard
		DROP CONSTRAINT PK_tbl_UsedCarScorecard
IF (@BuildIndex = 1)
	ALTER TABLE dbo.tbl_UsedCarScorecard
	ADD CONSTRAINT PK_tbl_UsedCarScorecard
	PRIMARY KEY CLUSTERED
	(UsedCarScoreCardId ASC)
	WITH FILLFACTOR = 90 ON DATA
--***********************************************************************
