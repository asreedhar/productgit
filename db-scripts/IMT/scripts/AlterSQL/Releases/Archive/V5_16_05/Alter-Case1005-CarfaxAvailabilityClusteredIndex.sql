DECLARE @BuildIndex BIT
SET @BuildIndex = 1
--********************************************
--CarfaxAvailability
IF (EXISTS(	SELECT * FROM SYS.Indexes 
		WHERE	NAME = 'IX_CarfaxAvailability_UserNameVIN' 
			AND OBJECT_ID = OBJECT_ID('IMT.dbo.CarfaxAvailability')))
	DROP INDEX dbo.CarfaxAvailability.IX_CarfaxAvailability_UserNameVIN
IF (@BuildIndex = 1)
	CREATE INDEX IX_CarfaxAvailability_UserNameVIN 
	on dbo.CarfaxAvailability
	(Username, VIN) 
	--INCLUDE (Included non-indexed columns)
	on IDX --specify filegroup
--********************************************
