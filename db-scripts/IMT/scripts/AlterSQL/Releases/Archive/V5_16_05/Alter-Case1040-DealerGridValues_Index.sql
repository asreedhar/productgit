--CreatedBy	Date		Purpose
--ffoiii	20080604	Covering index for function dbo.GetGroupingDescriptionLightGrid

DECLARE @BuildIndex bit
SELECT @BuildIndex = 1

--*********************************************
--dbo.DealerGridValues
--*********************************************

--drop index by same name if it exists
IF (EXISTS(	SELECT * FROM SYS.Indexes 
		WHERE	NAME = 'IX_DealerGridValuesBusinessUnitIdIndexKey' 
			AND OBJECT_ID = OBJECT_ID('IMT.dbo.DealerGridValues')))
	DROP INDEX dbo.DealerGridValues.IX_DealerGridValuesBusinessUnitIdIndexKey
--create index
IF (@BuildIndex = 1)
	CREATE INDEX IX_DealerGridValuesBusinessUnitIdIndexKey 
	on dbo.DealerGridValues
	(BusinessUnitId, IndexKey) 
	INCLUDE (TypeValue)
	on IDX --specify filegroup

--********************************************
