
--------------------------------------------------------------------------------
--	ORIGINALLY PLANNED TO BE DONE IN AN ADMIN TOOL, THIS NEEDS TO BE DONE
--	EVERY YEAR AS LONG AS THE CIA BUCKETRON IS DRIVEN COMPLETELY FROM DATA
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- 	ADD 2010
--------------------------------------------------------------------------------

UPDATE	CIABuckets
SET	RANK = RANK + 1
WHERE	CIABucketGroupID = 1

INSERT
INTO	CIABuckets (CIABucketGroupID, Rank, Description)
SELECT	1, 1, '2010'

INSERT
INTO	CIABucketRules (CIABucketID, CIABucketRuleTypeID, Value, Value2)
SELECT	23, 1, 2010, NULL

--------------------------------------------------------------------------------
-- 	DROP 2002
--------------------------------------------------------------------------------

DELETE 
FROM	CIABucketRules
WHERE	CIABucketID = 5

DELETE 
FROM	CIABuckets
WHERE	CIABucketID = 5
