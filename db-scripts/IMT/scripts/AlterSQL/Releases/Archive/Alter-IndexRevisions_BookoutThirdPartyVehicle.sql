--  This will help bookout processors

IF left(cast(serverproperty('edition') as varchar(100)), 9) in ('Developer', 'Enterpris')
    --  Online build
    CREATE nonclustered INDEX IX_BookoutThirdPartyVehicles__BookoutID
     on BookoutThirdPartyVehicles (BookoutID)
     with (online = on, sort_in_tempdb = on)
ELSE
    --  Offline (i.e. table-locked) build
    CREATE nonclustered INDEX IX_BookoutThirdPartyVehicles__BookoutID
     on BookoutThirdPartyVehicles (BookoutID)
     with (sort_in_tempdb = on)

GO
