/* MAK  08/16/2010  This script(tested today on a week old backup) deletes */
/*This script deletes all but one ReportProcessorCommandID per Dealer\Vehicle\Day for a 
Carfax Renewal Report.*/


IF OBJECT_ID('tempdb..#BadRequests') IS NOT NULL DROP TABLE #BadRequests
CREATE TABLE #BadRequests (RequestID INT)
IF OBJECT_ID('tempdb..#BadRPC') IS NOT NULL DROP TABLE #BadRPC
CREATE TABLE #BadRPC(ReportProcessorCommandID INT)
IF OBJECT_ID('tempdb..#GoodRPC') IS NOT NULL DROP TABLE #GoodRPC
CREATE TABLE #GoodRPC(ReportProcessorCommandID INT, VehicleID INT, DealerID INT, InsertDate DATETIME)

--  The table #GoodRPC (or Good report Processor command) collects information about all the ReportProcessorCommandIDs that
--	we do NOT want to delete.  A 'Good' Report processor command ID is defined as the Maximum ReportProcessorCommandID
--	for a Day\Vehicle\Dealer Combination that was a successful (ReportProcessorCommandStatusID=3) automatic renewal (ReportProcessorCommandTypeID =2).

INSERT INTO #GoodRPC (ReportProcessorCommandID, VehicleID,DealerID,InsertDate)
SELECT   MAX(RPC.ReportProcessorCommandID) AS ReportProcessorCommandID,
		 RPC.VehicleID,
		 RPC.DealerID,
		  DATEADD(dd, DATEDIFF(dd, 0, RPC.InsertDate), 0)
FROM     Carfax.ReportProcessorCommand RPC
WHERE    ReportProcessorCommandTypeID = 2
      AND ReportProcessorCommandStatusID = 3
               GROUP BY RPC.VehicleID,
                        RPC.DealerID,
                        DATEADD(dd, DATEDIFF(dd, 0, RPC.InsertDate), 0)

-- Change the value of the ReportProcessorCommandID In the #GoodRPC table to the one that coressponds to the Request
-- in the Vehicle Dealer Table if that ReportProcessorCommandID is not chosen for that Vehicle\Dealer.

UPDATE GP 
SET		ReportProcessorCommandID       = RPC.ReportProcessorCommandID
FROM	Carfax.ReportProcessorCommand RPC
JOIN	Carfax.ReportProcessorCommand_Request RQ ON RPC.ReportProcessorCommandID = RQ.ReportProcessorCommandID
JOIN	Carfax.Vehicle_Dealer VD ON RQ.RequestID = VD.RequestID
JOIN	#GoodRPC GP ON VD.DealerID = GP.DealerID AND GP.VehicleID=VD.VehicleID AND  DATEADD(dd, DATEDIFF(dd, 0, RPC.InsertDate), 0)=GP.InsertDate
WHERE	RPC.ReportProcessorCommandStatusID=3
                 
--  The table #BadRPC (or Bad report Processor command)Contains all the ReportProcessorCommandIDs that
--	we do  want to delete.  A 'Bad' Report processor command ID is defined as being a ReportProcessorCommandID
--	for a Day\Vehicle\Dealer Combination that was a successful (ReportProcessorCommandStatusID=3) automatic renewal (ReportProcessorCommandTypeID =2)
--	and NOT in the #GoodRPC table.

INSERT INTO #BadRPC
        ( ReportProcessorCommandID )
SELECT	RPC.ReportProcessorCommandID
FROM    Carfax.ReportProcessorCommand RPC    
LEFT JOIN	#GoodRPC GR ON RPC.ReportProcessorCommandID = GR.ReportProcessorCommandID
 WHERE    RPC.ReportProcessorCommandTypeID = 2
      AND RPC.ReportProcessorCommandStatusID = 3
      AND GR.ReportProcessorCommandID IS NULL
               
--  The table #BadRequests contains all the RequestIDs that
--	we  want to delete.  A 'Bad' Request is the result of a 'Bad' ReportProcessorCommandID.
         
INSERT 
INTO	#BadRequests (RequestID)
SELECT  RequestID
FROM	Carfax.ReportPRocessorCommand_Request RQ
JOIN	#BadRPC RPC ON RQ.ReportProcessorCommandID	=RPC.ReportProcessorCommandID			

 
-- Delete all references to the bad requests in the ReportProcessorCommand_Request table. 

DELETE FROM Carfax.ReportPRocessorCommand_Request
WHERE RequestID IN 
(SELECT RequestID FROM #BadRequests)
OR  ReportProcessorCommandID IN 
(SELECT ReportProcessorCommandID FROM #BadRPC)



-- Delete all references to the bad requests in the Carfax.ReportInspectionResult, Carfax.Report, Carfax.Response and
-- finally the Carfax.Request tables. 
DELETE FROM Carfax.ReportInspectionResult
WHERE RequestID IN 
(SELECT RequestID FROM #BadRequests)

DELETE FROM Carfax.Report
WHERE RequestID IN 
(SELECT RequestID FROM #BadRequests)

DELETE FROM Carfax.Response
WHERE RequestID IN 
(SELECT RequestID FROM #BadRequests)

DELETE FROM Carfax.Request
WHERE RequestID IN 
(SELECT RequestID FROM #BadRequests)


DELETE FROM Carfax.ReportPRocessorCommand_Exception 
WHERE ReportProcessorCommandID IN 
(SELECT ReportProcessorCommandID FROM #BadRPC)

DELETE FROM Carfax.ReportPRocessorCommand 
WHERE ReportProcessorCommandID IN 
(SELECT ReportProcessorCommandID FROM #BadRPC) 

 