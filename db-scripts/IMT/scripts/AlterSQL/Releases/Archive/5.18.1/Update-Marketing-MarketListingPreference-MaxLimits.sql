UPDATE Marketing.MarketListingPreference
SET MileageDeltaBelow = 1000000
WHERE MileageDeltaBelow = 2147483647

UPDATE Marketing.MarketListingPreference
SET MileageDeltaAbove = 1000000
WHERE MileageDeltaAbove = 2147483647

UPDATE Marketing.MarketListingPreference
SET PriceDeltaAbove = 1000000
WHERE PriceDeltaAbove = 2147483647

UPDATE Marketing.MarketListingPreference
SET PriceDeltaBelow = 1000000
WHERE PriceDeltaBelow = 2147483647
