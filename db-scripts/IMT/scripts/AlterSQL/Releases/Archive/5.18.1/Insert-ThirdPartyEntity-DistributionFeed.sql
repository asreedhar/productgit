/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *  Insertion of a new internet advertiser called 'Distribution Feed' for controlling of bulk
 *  export for EDTs swallowed up by Aultec. Is associated with all dealers that currently export.
 *
 * History
 * ----------
 * 
 * CGC  07/16/2010	
 * 						
 * ------------------------------------------------------------------------------------------- */

DECLARE @ThirdPartyEntityID int
DECLARE @Name varchar(20)

SET @Name = 'Distribution Feed'

INSERT INTO dbo.ThirdPartyEntity (BusinessUnitID, ThirdPartyEntityTypeID, Name)
VALUES (100150, 4, @Name)

SET @ThirdPartyEntityID = SCOPE_IDENTITY()

INSERT INTO dbo.InternetAdvertiser_ThirdPartyEntity (InternetAdvertiserID, DatafeedCode)
VALUES (@ThirdPartyEntityID, @Name)

-- Associate this need feed with all dealers that currently export.
INSERT INTO dbo.InternetAdvertiserDealership (InternetAdvertiserID, BusinessUnitID, IsLive, Active, status, IncrementalExport)
SELECT DISTINCT @ThirdPartyEntityID, BusinessUnitID, 1, 1, 1, 0 FROM dbo.InternetAdvertiserDealership

GO
