/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *  Distribution schema changes with respect to 'MessageTypes' tracking. 
 *	 - Rename 'MessageTypeFlag' columns to 'MessageTypes' since it is not actually a flag, and 
 *     this has caused confusion.
 *   - Adds MessageTypes column to Distribution.Message.
 *
 * History
 * ----------
 * 
 * CGC  07/19/2010	
 * 						
 * ------------------------------------------------------------------------------------------- */

EXEC sp_rename 'Distribution.ProviderMessageType.MessageTypeFlag', 'MessageTypes', 'COLUMN'
GO

EXEC sp_rename 'Distribution.AccountPreferences.MessageTypeFlag', 'MessageTypes', 'COLUMN'
GO

EXEC sp_rename 'Distribution.SubmissionError.MessageTypeFlag', 'MessageTypes', 'COLUMN'
GO

EXEC sp_rename 'Distribution.SubmissionSuccess.MessageTypeFlag', 'MessageTypes', 'COLUMN'
GO

ALTER TABLE Distribution.Message ADD MessageTypes INT NULL
GO
