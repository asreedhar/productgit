/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *  Adds the id of the user running the app when an exception occured for Distribution.
 *
 * History
 * ----------
 * 
 * CGC  07/19/2010	
 * 						
 * ------------------------------------------------------------------------------------------- */

ALTER TABLE Distribution.Exception ADD ExceptionUserID INT NULL
GO
