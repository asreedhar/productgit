/*
Change market listings to be not displayed by default.
Update all existing dealer preferences as well as the system preference.
The user does not have the ability to change this.
*/

UPDATE Marketing.MarketListingPreference
SET IsDisplayed = 0
