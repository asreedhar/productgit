/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *  Remove constraint on text length in Distribution.ContentText, and change the column Text to
 *  allow nulls.
 *
 * History
 * ----------
 * 
 * CGC  07/21/2010	
 * 						
 * ------------------------------------------------------------------------------------------- */
 
ALTER TABLE Distribution.ContentText
DROP CONSTRAINT CK_Distribution_ContentText__Text

ALTER TABLE Distribution.ContentText 
ALTER COLUMN Text VARCHAR(2000) NULL

GO
