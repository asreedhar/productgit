CREATE TABLE dbo.AppraisalSource (
	AppraisalSourceID 	TINYINT NOT NULL ,
	Description 		VARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
	)
GO

ALTER TABLE dbo.AppraisalSource ADD CONSTRAINT PK_AppraisalSource PRIMARY KEY CLUSTERED (AppraisalSourceID)  
GO

INSERT
INTO	AppraisalSource
SELECT	1, 'The Edge'
UNION
SELECT	2, 'Wavis'
UNION
SELECT	3, 'AutoBase'
UNION
SELECT	4, 'HigherGear'
GO

SET IDENTITY_INSERT [dbo].[CredentialType] ON
GO
INSERT INTO [dbo].[CredentialType]
           ([CredentialTypeID], [Name])
     VALUES
           (5, 'Wavis')
GO
INSERT INTO [dbo].[CredentialType]
           ([CredentialTypeID], [Name])
     VALUES
           (6, 'AutoBase')
GO
SET IDENTITY_INSERT [dbo].[CredentialType] OFF
GO

ALTER TABLE Appraisals DROP COLUMN VehicleGuideBookID
GO
ALTER TABLE Appraisals DROP COLUMN DealTrackSalesperson
GO

ALTER TABLE dbo.Appraisals ADD
	AppraisalSourceID tinyint NOT NULL CONSTRAINT DF_Appraisals_AppraisalSourceID DEFAULT 1
GO

ALTER TABLE dbo.Appraisals ADD CONSTRAINT FK_Appraisals_AppraisalSource FOREIGN KEY (AppraisalSourceID) REFERENCES dbo.AppraisalSource(AppraisalSourceID)
GO

UPDATE	Appraisals
SET	AppraisalSourceID = 2
WHERE	IsWirelessAppraisal = 1
GO

ALTER TABLE Appraisals DROP CONSTRAINT DF_IsWirelessAppraisal
GO

ALTER TABLE Appraisals DROP COLUMN IsWirelessAppraisal
GO

------------------------------------------------------------------------------------------------
--	Add new AppraisalActionType
------------------------------------------------------------------------------------------------
-- this table is a dupe of appraisalActions
drop table dbo.RedistributionAction
GO

SET IDENTITY_INSERT [dbo].[AppraisalActionTypes] ON
GO
INSERT INTO [dbo].[AppraisalActionTypes]
           ([AppraisalActionTypeID], [Description])
     VALUES
           (6, 'Awaiting Appraisal')
GO
SET IDENTITY_INSERT [dbo].[AppraisalActionTypes] OFF
GO

------------------------------------------------------------------------------------------------
--	Set fields to nullable
------------------------------------------------------------------------------------------------
ALTER TABLE dbo.Appraisals
ALTER COLUMN MemberID int NULL
GO
ALTER TABLE dbo.Appraisals
ALTER COLUMN DealTrackNewOrUsed tinyint NULL
GO

ALTER TABLE tbl_Vehicle
ALTER COLUMN VehicleTrim varchar(50) NULL
GO

------------------------------------------------------------------------------------------------
--	Change Datatype of AppraisalValues.Value from decimal(12,2) to int
------------------------------------------------------------------------------------------------

ALTER TABLE dbo.AppraisalValues
DROP CONSTRAINT [DF_AppraisalValues_Value]
GO
ALTER TABLE dbo.AppraisalValues
ALTER COLUMN Value int NOT NULL
GO
ALTER TABLE dbo.AppraisalValues
ADD CONSTRAINT [DF_AppraisalValues_Value] DEFAULT (0) FOR [Value]
GO

------------------------------------------------------------------------------------------------
--	Copy AppraisalValues.Initials into AppraisalValues.AppraiserName, 
--	Drop AppraisalValues.Initials
------------------------------------------------------------------------------------------------
UPDATE dbo.AppraisalValues SET AppraiserName = Initials
GO
ALTER TABLE dbo.AppraisalValues
DROP COLUMN Initials
GO

------------------------------------------------------------------------------------------------
--	Change relationship of AppraisalCustomer to Appraisal to be 1 to 1
------------------------------------------------------------------------------------------------
ALTER TABLE dbo.AppraisalCustomer DROP CONSTRAINT PK_AppraisalCustomerID
GO

--delete the duplicates, leaving the duplicate with max(appraisalCustomerId)
delete from AppraisalCustomer where AppraisalCustomerID IN (
	select a.AppraisalCustomerID
	from AppraisalCustomer a
		join
		(
			select c.AppraisalID, max(c.AppraisalCustomerID) as AppraisalCustomerID 
			from AppraisalCustomer c
			group by AppraisalID
			having count(*) > 1
		) b
	on a.AppraisalID = b.AppraisalID and a.AppraisalCustomerID < b.AppraisalCustomerID
)

--reassign Primary Key to appraisalID making relationship 1 to 1
ALTER TABLE dbo.AppraisalCustomer ADD CONSTRAINT PK_AppraisalID PRIMARY KEY (AppraisalID) 
GO

--clean up table!
ALTER TABLE dbo.AppraisalCustomer 
DROP COLUMN AppraisalCustomerID 
GO

--Too many required fields.  Making them all NULLABLE
ALTER TABLE dbo.AppraisalCustomer
ALTER COLUMN FirstName varchar(40) NULL
GO

ALTER TABLE dbo.AppraisalCustomer
ALTER COLUMN LastName varchar(40) NULL
GO

ALTER TABLE dbo.AppraisalCustomer
ALTER COLUMN PhoneNumber varchar(40) NULL
GO
------------------------------------------------------------------------------------------------
--  Tidbit: as of Dec. 13, 2006 7:12 pm, there are 36 rows out of 584374 which actually have 
--  		more than 1 AppraisalAction per Appraisal!
--	Making AppraisalAction -> Appraisal relationship into one-to-one on a foriegn key.
------------------------------------------------------------------------------------------------
--delete duplicates, assuming max(primarykey) is same as the last entry modified.  should be safe.
delete from AppraisalActions where AppraisalActionID IN (
	select a.AppraisalActionID
	from AppraisalActions a
		join
		(
			select aa.AppraisalID, max(aa.AppraisalActionID) as AppraisalActionID
			from AppraisalActions aa
			group by AppraisalID
			having count(*) > 1
		) b
	on a.AppraisalID = b.AppraisalID and a.AppraisalActionID < b.AppraisalActionID
)
GO

--add unique key
ALTER TABLE AppraisalActions
ADD CONSTRAINT [UK_AppraisalActions_AppraisalID] UNIQUE (AppraisalID)
GO

------------------------------------------------------------------------------------------------
--  Add unique key onto appraisalWindowSticker and appraisalFormOptions onto appraisal
--	Making AppraisalAction -> Appraisal relationship into one-to-one on a foriegn key.
------------------------------------------------------------------------------------------------
--delete duplicates, assuming max(primarykey) is same as the last entry modified.  should be safe.
delete from AppraisalFormOptions where AppraisalFormID IN (
	select a.AppraisalFormID
	from AppraisalFormOptions a
		join
		(
			select aa.AppraisalID, max(aa.AppraisalFormID) as AppraisalFormID
			from AppraisalFormOptions aa
			group by AppraisalID
			having count(*) > 1
		) b
	on a.AppraisalID = b.AppraisalID and a.AppraisalFormID < b.AppraisalFormID
)
GO

--add unique key
ALTER TABLE AppraisalFormOptions
ADD CONSTRAINT [UK_AppraisalFormOptions_AppraisalID] UNIQUE (AppraisalID)
GO

--delete duplicates, assuming max(primarykey) is same as the last entry modified.  should be safe.
delete from AppraisalWindowStickers where AppraisalWindowStickerID IN (
	select a.AppraisalWindowStickerID
	from AppraisalWindowStickers a
		join
		(
			select aa.AppraisalID, max(aa.AppraisalWindowStickerID) as AppraisalWindowStickerID
			from AppraisalWindowStickers aa
			group by AppraisalID
			having count(*) > 1
		) b
	on a.AppraisalID = b.AppraisalID and a.AppraisalWindowStickerID < b.AppraisalWindowStickerID
)
GO

--add unique key
ALTER TABLE AppraisalWindowStickers
ADD CONSTRAINT [UK_AppraisalWindowStickers_AppraisalID] UNIQUE (AppraisalID)
GO

------------------------------------------------------------------------------------------------
--	Creating Staging Tables for Incoming Bookouts
------------------------------------------------------------------------------------------------
CREATE TABLE Staging_VehicleHistoryReport (
            Staging_VehicleHistoryReportId int IDENTITY (1,1) CONSTRAINT [PK_StagingVehicleHistoryReport] PRIMARY KEY NOT NULL,
            AppraisalID int NOT NULL CONSTRAINT FK_StagingVehicleHistoryReport_Appraisals FOREIGN KEY REFERENCES Appraisals ([AppraisalID]),
            CredentialTypeID tinyint NOT NULL CONSTRAINT FK_StagingVehicleHistoryReport_CredentialType FOREIGN KEY REFERENCES CredentialType([CredentialTypeID]),
            UserName varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
) ON DATA
GO

CREATE TABLE Staging_Bookouts (
	Staging_BookoutID int IDENTITY(1,1) CONSTRAINT [PK_StagingBookouts] PRIMARY KEY NOT NULL,
	AppraisalID int NOT NULL CONSTRAINT [FK_StagingBookout_AppraisalID] FOREIGN KEY REFERENCES dbo.Appraisals([AppraisalID]),
	ThirdPartyID tinyint NOT NULL CONSTRAINT [FK_StagingBookout_ThirdParty] FOREIGN KEY REFERENCES dbo.ThirdParties([ThirdPartyID]),
	ThirdPartyVehicleCode varchar(20) NOT NULL,
)
GO

CREATE TABLE Staging_BookoutOptions (
	Staging_BookoutOptionID int IDENTITY(1,1) CONSTRAINT [PK_StagingBookoutOptions] PRIMARY KEY NOT NULL,
	Staging_BookoutID int NOT NULL CONSTRAINT [FK_StagingBookoutOptions_StagingBookouts] FOREIGN KEY REFERENCES dbo.Staging_Bookouts([Staging_BookoutID]),
	ThirdPartyCategoryID int NULL CONSTRAINT [FK_StagingBookoutOptions_ThirdPartyCategory] FOREIGN KEY REFERENCES dbo.ThirdPartyCategories ([ThirdPartyCategoryID]),
	ThirdPartyOptionTypeID tinyint NOT NULL CONSTRAINT [FK_StagingBookoutOptions_ThirdPartyOptionType] FOREIGN KEY REFERENCES dbo.ThirdPartyOptionTypes ([ThirdPartyOptionTypeID]),
	OptionKey varchar(255) NOT NULL,
	OptionName varchar(255) NULL,
	isSelected tinyint NOT NULL,
	Value int NOT NULL
)
GO

CREATE TABLE Staging_BookoutValues (
	Staging_BookoutValueID int IDENTITY(1,1) CONSTRAINT [PK_StagingBookoutValues] PRIMARY KEY NOT NULL,
	Staging_BookoutID int NOT NULL CONSTRAINT [FK_StagingBookoutValues_StagingBookouts] FOREIGN KEY REFERENCES dbo.Staging_Bookouts([Staging_BookoutID]),
	ThirdPartyCategoryID int NOT NULL CONSTRAINT [FK_StagingBookoutValue_ThirdPartyCategory] FOREIGN KEY REFERENCES dbo.ThirdPartyCategories ([ThirdPartyCategoryID]),
	BaseValue int NOT NULL,
	MileageAdjustment int NULL,
	FinalValue int NULL
)
GO