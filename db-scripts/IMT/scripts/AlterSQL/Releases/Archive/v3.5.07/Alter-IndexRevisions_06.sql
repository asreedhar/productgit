/*

Create an overlooked index

*/

CREATE nonclustered INDEX IX_VehicleAttributeCatalog_1M__SquishVIN
 on VehicleAttributeCatalog_1M (SquishVIN)
  with fillfactor = 95
GO
