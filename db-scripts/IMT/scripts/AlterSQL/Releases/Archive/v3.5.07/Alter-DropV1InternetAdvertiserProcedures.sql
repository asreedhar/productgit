/*

Drop old stored procedures IF they're present on the box

*/

IF objectproperty(object_id('InternetAdvertiserExport_eBizAutos'), 'isProcedure') = 1
    DROP PROCEDURE InternetAdvertiserExport_eBizAutos

IF objectproperty(object_id('InternetAdvertiserExport_AutoTrader'), 'isProcedure') = 1
    DROP PROCEDURE InternetAdvertiserExport_AutoTrader

IF objectproperty(object_id('InternetAdvertiserExport_CarsDotCom'), 'isProcedure') = 1
    DROP PROCEDURE InternetAdvertiserExport_CarsDotCom

GO
