-- Alter changes for standalone search and acquisition.

if not exists (select * from syscolumns
	where id=object_id('dbo.tbl_MemberATCAccessGroup') and name='MaxVehicleMileage')
ALTER TABLE dbo.tbl_MemberATCAccessGroup
	ADD MaxVehicleMileage INTEGER
GO
if not exists (select * from syscolumns
	where id=object_id('dbo.tbl_MemberATCAccessGroup') and name='MinVehicleMileage')
ALTER TABLE dbo.tbl_MemberATCAccessGroup
	ADD MinVehicleMileage INTEGER
GO

insert into dbo.lu_ProgramType (ProgramType_CD, ProgramTypeName)
values(4, 'Firstlook')
