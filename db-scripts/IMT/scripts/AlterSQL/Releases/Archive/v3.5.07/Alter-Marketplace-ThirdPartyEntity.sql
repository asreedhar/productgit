------------------------------------------------------------------------
--	CHANGE PLURAL OBJECT NAMES TO THE NON-PLURAL "STANDARD"
------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.FK_MarketplaceSubmissions_MarketplaceSubmissionStatusCodes') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.MarketplaceSubmissions DROP CONSTRAINT FK_MarketplaceSubmissions_MarketplaceSubmissionStatusCodes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.FK_Marketplace_MarketplaceTypes') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.Marketplaces DROP CONSTRAINT FK_Marketplace_MarketplaceTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.FK_MarketplaceSubmissions_Marketplaces') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE dbo.MarketplaceSubmissions DROP CONSTRAINT FK_MarketplaceSubmissions_Marketplaces
GO

if exists (select * from dbo.sysobjects where id = object_id(N'dbo.PK_Marketplace') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE dbo.Marketplaces DROP CONSTRAINT PK_Marketplace
GO

EXEC sp_rename @objname = 'Marketplaces',  @newname  = 'Marketplace_ThirdPartyEntity', @objtype = 'OBJECT'
GO 

EXEC sp_rename @objname = 'MarketplaceSubmissions',  @newname  = 'MarketplaceSubmission', @objtype = 'OBJECT'

EXEC sp_rename @objname = 'PK_MarketplaceSubmissions',  @newname  = 'PK_MarketplaceSubmission', @objtype = 'OBJECT'
EXEC sp_rename @objname = 'DF_MarketplaceSubmissions_DateCreated',  @newname  = 'DF_MarketplaceSubmission_DateCreated', @objtype = 'OBJECT'
EXEC sp_rename @objname = 'FK_MarketplaceSubmissions_Inventory',  @newname  = 'FK_MarketplaceSubmission_Inventory', @objtype = 'OBJECT'
EXEC sp_rename @objname = 'FK_MarketplaceSubmissions_Member',  @newname  = 'FK_MarketplaceSubmission_Member', @objtype = 'OBJECT'


EXEC sp_rename @objname = 'MarketplaceSubmissionStatusCodes',  @newname  = 'MarketplaceSubmissionStatusCode', @objtype = 'OBJECT'
EXEC sp_rename @objname = 'PK_MarketplaceSubmissionStatusCodes',  @newname  = 'PK_MarketplaceSubmissionStatusCode', @objtype = 'OBJECT'

ALTER TABLE dbo.Marketplace_ThirdPartyEntity ALTER COLUMN MarketplaceID INT NOT NULL
ALTER TABLE dbo.MarketplaceSubmission ALTER COLUMN MarketplaceID INT NOT NULL

ALTER TABLE dbo.Marketplace_ThirdPartyEntity WITH NOCHECK ADD CONSTRAINT PK_Marketplace PRIMARY KEY CLUSTERED (MarketplaceID)  
GO

--------------------------------------------------------------------------------------
--	NOW, MERGE THE SCHEMAS
--------------------------------------------------------------------------------------


UPDATE	ThirdPartyEntityType
SET	Description = 'Listing Service'
WHERE	ThirdPartyEntityTypeID = 5

DECLARE @ThirdPartyEntityID INT
INSERT	
INTO	ThirdPartyEntity (BusinessUnitID,ThirdPartyEntityTypeID,Name)
SELECT	100150, 5, 'GMAC Smart Auction'

SET @ThirdPartyEntityID = @@IDENTITY

UPDATE	MarketplaceSubmission
SET	MarketplaceID = @ThirdPartyEntityID
WHERE	MarketplaceID = 1

UPDATE	Marketplace_ThirdPartyEntity
SET	MarketplaceID = @ThirdPartyEntityID
WHERE	MarketplaceID = 1

GO

--------------------------------------------------------------------------------------
--	THE "TYPE" IS NOW ON THE ENTITY TABLE
--------------------------------------------------------------------------------------

ALTER TABLE Marketplace_ThirdPartyEntity DROP COLUMN MarketplaceTypeID
GO

ALTER TABLE dbo.MarketplaceSubmission ADD 
	CONSTRAINT FK_MarketplaceSubmission_Marketplace_ThirdPartyEntity FOREIGN KEY (MarketplaceID) REFERENCES dbo.Marketplace_ThirdPartyEntity (MarketplaceID),
	CONSTRAINT FK_MarketplaceSubmission_MarketplaceSubmissionStatusCode FOREIGN KEY (MarketplaceSubmissionStatusCD) REFERENCES dbo.MarketplaceSubmissionStatusCode (MarketplaceSubmissionStatusCD)
GO


DROP TABLE MarketplaceTypes
GO

INSERT
INTO	AIP_EventCategory
SELECT	8, 'Marketplace'
GO

INSERT
INTO	AIP_EventType (AIP_EventTypeID, Description, AIP_EventCategoryID, CurrentWhenSingleDayPlan, CurrentAtCreateDate, ThirdPartyEntityTypeID)
SELECT	18, 'Internet Marketplace',8,1,1,5
GO

