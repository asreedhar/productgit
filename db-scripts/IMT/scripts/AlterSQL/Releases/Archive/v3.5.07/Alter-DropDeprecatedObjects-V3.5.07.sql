if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetTradeInPerformance]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[GetTradeInPerformance]
GO
ALTER TABLE Bookouts DROP COLUMN LegacyID
GO
ALTER TABLE BookoutThirdPartyCategories DROP COLUMN LegacyID
GO
ALTER TABLE CIABodyTypeDetails DROP COLUMN LegacyID
GO
ALTER TABLE CIASummary DROP COLUMN LegacyID
GO
ALTER TABLE CIAGroupingItems DROP COLUMN LegacyID
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InventoryZeroLights]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[InventoryZeroLights]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_dbVersion]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_dbVersion]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_VehicleSegmentOverrides]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_VehicleSegmentOverrides]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tbl_CIAPlan]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[tbl_CIAPlan]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vw_GuideBookDescriptionCategories]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vw_GuideBookDescriptionCategories]
GO