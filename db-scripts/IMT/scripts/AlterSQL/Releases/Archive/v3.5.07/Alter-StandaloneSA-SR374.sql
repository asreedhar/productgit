--For click tracking channelId now refers to the specific marketId.
--I have not removed code references yet so we can't just drop it but I believe that
--we can remove the purchasingCenterChannels table.

if exists (select * from syscolumns
	where id=object_id('dbo.FLUSAN_EventLog') and name='FK_FLUSAN_EventLog_PurchasingCenterChannels')
ALTER TABLE dbo.FLUSAN_EventLog DROP FK_FLUSAN_EventLog_PurchasingCenterChannels
GO


--Cia upgrade is now referred to as purchasing center.

update dbo.lu_DealerUpgrade set dealerUpgradeDESC = 'Purchasing Center' where DealerUpgradeCd = 3