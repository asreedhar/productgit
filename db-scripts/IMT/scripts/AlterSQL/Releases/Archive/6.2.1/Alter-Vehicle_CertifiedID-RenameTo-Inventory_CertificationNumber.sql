-- Create new table ------------------------------------------------------------
IF (OBJECT_ID('dbo.Inventory_CertificationNumber') IS NULL)
CREATE TABLE dbo.Inventory_CertificationNumber(
	InventoryID int NOT NULL,
	CertificationNumber varchar(20) NOT NULL,
	ManufacturerID int NOT NULL,
	ModifiedByUser varchar(100) NOT NULL,
	ModifiedDate datetime NOT NULL,
 CONSTRAINT PK_Inventory_CertificationNumber PRIMARY KEY CLUSTERED 
(
	InventoryID ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON DATA
) ON DATA
GO

IF (OBJECT_ID('dbo.FK_InventoryCertificationNumber_Inventory') IS NULL)
ALTER TABLE dbo.Inventory_CertificationNumber  WITH CHECK ADD  CONSTRAINT FK_InventoryCertificationNumber_Inventory FOREIGN KEY(InventoryID)
REFERENCES dbo.Inventory (InventoryID)
GO


-- New Manufacturer Specific Constraint(s) -------------------------------------
IF (OBJECT_ID('dbo.CK_InventoryCertificationNumber_GM') IS NULL)
ALTER TABLE dbo.Inventory_CertificationNumber WITH CHECK ADD CONSTRAINT CK_InventoryCertificationNumber_GM
CHECK (
	ManufacturerID = 5 /*5->GM*/ 
	AND (
		CertificationNumber LIKE replicate('[0-9]', 6)
		OR CertificationNumber LIKE replicate('[0-9]', 7)
	)
)


-- Copy from old table to new table --------------------------------------------
IF (OBJECT_ID('dbo.VehicleCertifiedID') IS NOT NULL AND OBJECT_ID('dbo.Inventory_CertificationNumber') IS NOT NULL)
INSERT	dbo.Inventory_CertificationNumber
(InventoryID, CertificationNumber, ManufacturerID , ModifiedByUser, ModifiedDate)
SELECT	vc.InventoryID, vc.CertifiedID, 5, vc.ModifiedByUser, vc.ModifiedDate 
  FROM	IMT.dbo.VehicleCertifiedID vc
		JOIN IMT.dbo.Inventory i ON i.InventoryID = vc.InventoryID
		JOIN IMT.dbo.tbl_Vehicle v ON v.VehicleID = i.VehicleID
		JOIN IMT.dbo.MakeModelGrouping mmg ON mmg.MakeModelGroupingID = v.MakeModelGroupingID
		JOIN Merchandising.builder.manufacturerCertifications mc ON mc.[name] LIKE mmg.Make
 WHERE	EXISTS ( 
			SELECT * 
			  FROM Merchandising.settings.cpoCertifieds 
			 WHERE cpoGroupId = 5 AND certificationTypeId = mc.certificationTypeId
		)
		AND (vc.CertifiedID LIKE replicate('[0-9]', 6)
			OR vc.CertifiedID LIKE replicate('[0-9]', 7))


-- Remove old table and related stored procedure(s) ----------------------------
IF (OBJECT_ID('dbo.VehicleCertifiedID') IS NOT NULL)
DROP TABLE dbo.VehicleCertifiedID
GO

IF (OBJECT_ID('dbo.VehicleCertifiedID#Update') IS NOT NULL)
DROP PROCEDURE dbo.[VehicleCertifiedID#Update]



