CREATE TABLE Extract.Inventory#eBizCustomHendrick#BuildLogDealerMetrics (
			BuildLogID		INT NOT NULL, 
			BusinessUnitID		INT NOT NULL,
			Units			SMALLINT NOT NULL,
			CONSTRAINT PK_Inventory#eBizCustomHendrick#BuildLogDealerMetrics PRIMARY KEY CLUSTERED (BuildLogID, BusinessUnitID)
			)
