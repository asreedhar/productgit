ALTER TABLE dbo.SearchCandidate DROP CONSTRAINT UQ_SearchCandidate
CREATE UNIQUE CLUSTERED INDEX UQC_SearchCandidate ON dbo.SearchCandidate(SearchCandidateListID,ModelID) WITH (FILLFACTOR = 99)
GO

DROP INDEX dbo.ThirdPartyVehicles.IX_ThirdPartyVehicles__ThirdPartyVehicleCode

GO

--	ALREADY DROPPED ON PROD.  COMPLETELY WORTHLESS INDEX.

IF EXISTS(	SELECT	1 
		FROM	sys.indexes I  
		WHERE	name = 'IX_VehicleEntityID_VehicleEntityTypeID' AND object_id = object_id('dbo.Inventory')
		)
		
	DROP INDEX dbo.Inventory.IX_VehicleEntityID_VehicleEntityTypeID
