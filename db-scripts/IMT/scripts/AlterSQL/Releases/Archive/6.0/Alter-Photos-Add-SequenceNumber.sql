ALTER TABLE dbo.Photos ADD SequenceNumber SMALLINT NULL


GO


UPDATE	P
SET	SequenceNumber = PS.SequenceNumber
FROM	dbo.Photos P 
	INNER JOIN (	SELECT	P.PhotoID, 
				SequenceNumber = ROW_NUMBER() OVER (PARTITION BY IP.InventoryID ORDER BY P.IsPrimaryPhoto DESC, P.PhotoID)
			FROM	dbo.Photos P
				INNER JOIN dbo.InventoryPhotos IP ON P.PhotoID = IP.PhotoID
			WHERE	SequenceNumber IS NULL
			) PS ON P.PhotoID = PS.PhotoID

GO

UPDATE	P
SET	SequenceNumber = PS.SequenceNumber
FROM	dbo.Photos P 
	INNER JOIN (	SELECT	P.PhotoID, 
				SequenceNumber = ROW_NUMBER() OVER (PARTITION BY AP.AppraisalID ORDER BY P.IsPrimaryPhoto DESC, P.PhotoID)
			FROM	dbo.Photos P
				INNER JOIN dbo.AppraisalPhotos AP ON P.PhotoID = AP.PhotoID
			WHERE	SequenceNumber IS NULL
			) PS ON P.PhotoID = PS.PhotoID

----------------------------------------------------------------------------------------
--	ORPHANS! Should delete, not sure how they were inserted in the first place!
--	Will defer deletes to another script once lineage is figured out...
----------------------------------------------------------------------------------------
GO

UPDATE	dbo.Photos
SET	SequenceNumber = 0
WHERE	SequenceNumber IS NULL

GO

ALTER TABLE dbo.Photos ALTER COLUMN SequenceNumber SMALLINT NOT NULL