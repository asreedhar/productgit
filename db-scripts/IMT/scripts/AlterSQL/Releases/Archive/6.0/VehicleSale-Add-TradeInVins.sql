
ALTER TABLE dbo.tbl_VehicleSale ADD TradeInVIN_1 CHAR(17) NULL
ALTER TABLE dbo.tbl_VehicleSale ADD TradeInVIN_2 CHAR(17) NULL

GO

ALTER TABLE dbo.tbl_VehicleSaleReversals ADD TradeInVIN_1 CHAR(17) NULL
ALTER TABLE dbo.tbl_VehicleSaleReversals ADD TradeInVIN_2 CHAR(17) NULL

GO

SELECT	MAX(DH.Load_ID) AS LOAD_ID, DRH.DEALER_ID
INTO	#LatestLoadByDealerID
FROM	DBASTAT.dbo.Dataload_History DH
	INNER JOIN DBASTAT.dbo.DATALOAD_RAW_HISTORY DRH ON DH.Load_ID = DRH.LOAD_ID						
WHERE	FileType = 2	
GROUP
BY	DRH.DEALER_ID

UPDATE	VS
SET	TradeInVIN_1 = NULLIF(FILLER12,''),
	TradeInVIN_2 = NULLIF(FILLER13,'')
FROM	dbo.tbl_VehicleSale VS
	INNER JOIN dbo.Inventory I ON VS.InventoryID = I.InventoryID
	INNER JOIN dbo.BusinessUnit BU ON I.BusinessUnitID = BU.BusinessUnitID
	INNER JOIN #LatestLoadByDealerID LL ON BU.BusinessUnitCode = LL.DEALER_ID
	INNER JOIN DBASTAT.dbo.DATALOAD_RAW_HISTORY R ON LL.LOAD_ID = R.LOAD_ID  
								AND BU.BusinessUnitCode = R.DEALER_ID 
								AND R.AUDIT_ID = VS.Audit_ID