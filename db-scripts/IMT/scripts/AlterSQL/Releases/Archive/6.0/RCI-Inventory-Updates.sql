ALTER TABLE dbo.Inventory ADD BranchNumber VARCHAR(20) NULL
ALTER TABLE dbo.Inventory ADD DealershipIdentifier VARCHAR(20) NULL
ALTER TABLE dbo.Inventory ADD MSRP DECIMAL(9,2) NULL
ALTER TABLE dbo.Inventory DROP CONSTRAINT DF_Inventory__LotPrice		-- model this properly; should NOT default to zero!
GO
ALTER TABLE dbo.Inventory ALTER COLUMN LotPrice DECIMAL(9,2) NULL
GO

ALTER TABLE dbo.tbl_Vehicle ADD ExteriorColorCode VARCHAR(5) NULL
ALTER TABLE dbo.tbl_Vehicle ADD InteriorColorCode VARCHAR(5) NULL