--------------------------------------------------------------------------------
--	Part 2 of migration of DMS load audit tables to DBASTAT.
--	Run after the companion script in DBASTAT (DMS-Inventory-Version-2.5.sql)
--------------------------------------------------------------------------------

DROP TABLE dbo.Audit_Exceptions
DROP TABLE dbo.Audit_Inventory
DROP TABLE dbo.Audit_Sales

GO

--------------------------------------------------------------------------------
--	Add the synonyms to support legacy queries
--	Drop once all have been migrated
--------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'dbo.Audit_Exceptions')
CREATE SYNONYM dbo.Audit_Exceptions FOR DBASTAT.dbo.Dataload_Audit_Exceptions
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'dbo.Audit_Inventory')
CREATE SYNONYM dbo.Audit_Inventory FOR DBASTAT.dbo.Dataload_Audit_Inventory
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'dbo.Audit_Sales')
CREATE SYNONYM dbo.Audit_Sales FOR DBASTAT.dbo.Dataload_Audit_Sales

GO
