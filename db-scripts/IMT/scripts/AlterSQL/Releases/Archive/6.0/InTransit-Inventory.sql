CREATE TABLE dbo.InventoryState (
	InventoryStateID	TINYINT NOT NULL CONSTRAINT PK_InventoryState PRIMARY KEY CLUSTERED,
	Description		VARCHAR(20) NOT NULL,
	InsertDate		SMALLDATETIME NOT NULL CONSTRAINT DF_InventoryState__InsertDate DEFAULT (GETDATE())
	)
GO

INSERT
INTO	dbo.InventoryState (InventoryStateID, Description)
SELECT	1, 'on-hand'
UNION
SELECT	2, 'in-transit'

GO


ALTER TABLE dbo.Inventory ADD InventoryStateID TINYINT NOT NULL CONSTRAINT DF_Inventory__InventoryStateID DEFAULT(1)
GO

ALTER TABLE dbo.VehicleSources ALTER COLUMN Description VARCHAR(20)
GO
SET IDENTITY_INSERT dbo.VehicleSources ON 
INSERT
INTO	dbo.VehicleSources (VehicleSourceID, Description)
SELECT	3, 'In-Transit Inventory'
SET IDENTITY_INSERT dbo.VehicleSources OFF



/*	Rollback

DROP TABLE dbo.InventoryState

ALTER TABLE dbo.Inventory DROP CONSTRAINT DF_Inventory__InventoryStateID
 
ALTER TABLE dbo.Inventory DROP COLUMN InventoryStateID

DELETE FROM dbo.VehicleSources WHERE VehicleSourceID = 3
ALTER TABLE dbo.VehicleSources ALTER COLUMN Description VARCHAR(10)

*/