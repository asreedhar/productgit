
if not exists(
	select * from IMT.sys.indexes where object_id = object_id('IMT.dbo.Photos') and name = 'IX_PhotoStatusCode'
)
	create nonclustered index IX_PhotoStatusCode on IMT.dbo.Photos (PhotoStatusCode)
go


--drop index dbo.Photos.IX_PhotoStatusCode