IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Extract].[InventoryExtract#GMAC]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Extract].[InventoryExtract#GMAC]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Extract].[OptimalInventoryExtract#GMAC]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Extract].[OptimalInventoryExtract#GMAC]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Extract].[SalesExtract#GMAC]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Extract].[SalesExtract#GMAC]
GO

DELETE	DC 
FROM	Extract.DealerConfiguration DC
WHERE	DestinationName = 'GMAC'


DELETE	D
FROM	Extract.Destination D
WHERE	Name = 'GMAC'
