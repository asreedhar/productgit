--Bugzid 31632: Change default JDPower circle setting to 3 from 3.5 including existing entries set to 3.5
UPDATE IMT.Marketing.ConsumerHighlightPreference
SET MinimumJDPowerCircleRating = 3
FROM IMT.Marketing.ConsumerHighlightPreference
WHERE MinimumJDPowerCircleRating = 3.5