IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[Marketing].[MarketAnalysisOwnerPriceProvider]') 
         AND name = 'MinimumDollarVariance'
)
BEGIN
	ALTER TABLE IMT.Marketing.MarketAnalysisOwnerPriceProvider ADD MinimumDollarVariance int NOT NULL default(1)
END
GO

IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[Marketing].[MarketAnalysisOwnerPriceProvider]') 
         AND name = 'VarianceDirection'
)
BEGIN
ALTER TABLE IMT.Marketing.MarketAnalysisOwnerPriceProvider ADD VarianceDirection tinyint NULL
END
GO

UPDATE IMT.Marketing.MarketAnalysisOwnerPriceProvider SET MinimumDollarVariance = 1, VarianceDirection = 2 WHERE OwnerId IS NULL
GO
