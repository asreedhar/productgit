-- BUGZID: 31699 - reorder the default ranking for pricing comparison providers.

ALTER TABLE [Marketing].[MarketAnalysisOwnerPriceProvider] DROP CONSTRAINT [UQ_MarketAnalysisOwnerPriceProvider_OwnerIdRank]
GO

BEGIN TRANSACTION;

BEGIN TRY

	UPDATE Marketing.MarketAnalysisOwnerPriceProvider 
	SET [Rank] = 1 WHERE OwnerId IS NULL AND PriceProviderId = 2
	
	UPDATE Marketing.MarketAnalysisOwnerPriceProvider 
	SET [Rank] = 2 WHERE OwnerId IS NULL AND PriceProviderId = 8
	
	UPDATE Marketing.MarketAnalysisOwnerPriceProvider 
	SET [Rank] = 3 WHERE OwnerId IS NULL AND PriceProviderId = 7
	
	UPDATE Marketing.MarketAnalysisOwnerPriceProvider 
	SET [Rank] = 4 WHERE OwnerId IS NULL AND PriceProviderId = 3
	
	UPDATE Marketing.MarketAnalysisOwnerPriceProvider 
	SET [Rank] = 5 WHERE OwnerId IS NULL AND PriceProviderId = 4
	
	UPDATE Marketing.MarketAnalysisOwnerPriceProvider 
	SET [Rank] = 6 WHERE OwnerId IS NULL AND PriceProviderId = 6	


END TRY
BEGIN CATCH
    SELECT 
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH;

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;
GO

ALTER TABLE [Marketing].[MarketAnalysisOwnerPriceProvider] ADD CONSTRAINT [UQ_MarketAnalysisOwnerPriceProvider_OwnerIdRank] UNIQUE NONCLUSTERED  ([OwnerId], [Rank])
GO
