BEGIN TRANSACTION;

BEGIN TRY
    DELETE FROM Marketing.MarketAnalysisOwnerPriceProvider WHERE PriceProviderId IN (5,9,10)
	DELETE FROM Marketing.MarketAnalysisPriceProvider WHERE PriceProviderId IN (5, 9, 10)
END TRY
BEGIN CATCH
    SELECT 
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH;

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;
GO

