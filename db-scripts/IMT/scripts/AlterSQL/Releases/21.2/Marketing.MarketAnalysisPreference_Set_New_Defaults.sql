--BUGZID: 31699 - Set a new default
UPDATE IMT.Marketing.MarketAnalysisPreference 
SET UseCustomComparisonPricePriority = 1
WHERE OwnerId IS NULL
