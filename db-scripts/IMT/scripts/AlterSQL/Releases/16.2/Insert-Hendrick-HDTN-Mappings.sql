INSERT
INTO	dbo.tbl_DealerATCAccessGroups (BusinessUnitID, AccessGroupId, AccessGroupPriceId, AccessGroupActorId, AccessGroupValue)

SELECT	BU.BusinessUnitID, 151, 1, 3, 1
FROM	dbo.BusinessUnitRelationship BUR
	INNER JOIN dbo.BusinessUnit BU ON BUR.BusinessUnitID = BU.BusinessUnitID AND BU.Active = 1
WHERE	ParentID = 100068
