-- this should have been checked into the Merchandising script folder


IF NOT EXISTS (	SELECT	1 
		FROM	Merchandising.settings.cpoCertifieds
		WHERE	certificationTypeId = 32
			AND cpoGroupId = 2
		)
		
INSERT INTO Merchandising.settings.cpoCertifieds
(certificationTypeId, cpoGroupId)
VALUES (32, 2)