--------------------------------------------------------------------------------
-- 	ADD 2015
--------------------------------------------------------------------------------

UPDATE	CIABuckets
SET	RANK = RANK + 1
WHERE	CIABucketGroupID = 1

INSERT
INTO	CIABuckets (CIABucketGroupID, Rank, Description)
SELECT	1, 1, '2015'

INSERT
INTO	CIABucketRules (CIABucketID, CIABucketRuleTypeID, Value, Value2)
SELECT	27, 1, 2015, NULL

--------------------------------------------------------------------------------
-- 	DROP 2007
--------------------------------------------------------------------------------

DELETE 
FROM	CIABucketRules
WHERE	CIABucketRuleTypeID = 1
	AND Value = '2007'

DELETE 
FROM	CIABuckets
WHERE	CIABucketGroupID = 1
	AND Description = '2007'
