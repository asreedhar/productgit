ALTER TABLE Extract.DealerConfiguration#OVEHendrick#SellerGroup ADD IsDefault BIT NOT NULL CONSTRAINT DF_ExtractDealerConfiguration#OVEHendrick#SellerGroup__IsDefault DEFAULT 0
GO

UPDATE	Extract.DealerConfiguration#OVEHendrick#SellerGroup
SET	IsDefault = 1
WHERE	SellerGroupID = 1
