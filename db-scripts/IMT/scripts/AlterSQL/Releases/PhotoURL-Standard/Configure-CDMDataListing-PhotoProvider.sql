DECLARE @PhotoProviderID INT

INSERT
INTO	dbo.ThirdPartyEntity
        (BusinessUnitID,
         ThirdPartyEntityTypeID,
         Name
        )
VALUES  (101590,			-- BusinessUnitID - int
         6,				-- ThirdPartyEntityTypeID - tinyint
         'CDMData Lot Provider Photos'  -- Name - varchar(50)
        )
SET @PhotoProviderID = SCOPE_IDENTITY()

INSERT
INTO	dbo.PhotoProvider_ThirdPartyEntity
        (PhotoProviderID,
         DatafeedCode,
         PhotoStorageCode
        )
VALUES  (@PhotoProviderID,	-- PhotoProviderID - int
         'CDMDataListing',	-- DatafeedCode - varchar(20)
         2			-- PhotoStorageCode - tinyint
        )        

INSERT 
INTO	dbo.PhotoProviderDealership (PhotoProviderID, BusinessUnitID, PhotoProvidersDealershipCode, PhotoStorageCode_Override)
SELECT	@PhotoProviderID, 103424, 'CENTENNI02', 2
UNION
SELECT	@PhotoProviderID, 100833, 'MCGRATHA02', 2
UNION
SELECT	@PhotoProviderID, 100834, 'MCGRATHH01', 2
GO