--	Change this to a clustered index.  Been wrong for eight years.

ALTER TABLE AutoCheck.Account DROP CONSTRAINT [FK_AutoCheck_Account__InsertMember]
ALTER TABLE AutoCheck.Account DROP CONSTRAINT [FK_AutoCheck_Account__UpdateMember]
ALTER TABLE AutoCheck.Account DROP CONSTRAINT [FK_AutoCheck_Account__DeleteMember]
ALTER TABLE Marketing.VehicleHistoryReportSourcePreference DROP CONSTRAINT [FK_Marketing_VehicleHistoryReportSourcePreference__InsertMember]
ALTER TABLE Marketing.VehicleHistoryReportSourcePreference DROP CONSTRAINT [FK_Marketing_VehicleHistoryReportSourcePreference__UpdateMember]
ALTER TABLE dbo.DealerPreference DROP CONSTRAINT [FK_DealerPreferenceLithiaCarCenterMember_Member]
ALTER TABLE Marketing.ConsumerHighlightPreference DROP CONSTRAINT [FK_Marketing_ConsumerHighlightPreference__InsertMember]
ALTER TABLE Marketing.ConsumerHighlightPreference DROP CONSTRAINT [FK_Marketing_ConsumerHighlightPreference__UpdateMember]
ALTER TABLE AutoCheck.Vehicle DROP CONSTRAINT [FK_AutoCheck_Vehicle__Member_UpdateUser]
ALTER TABLE AutoCheck.Vehicle DROP CONSTRAINT [FK_AutoCheck_Vehicle__Member_InsertUser]
ALTER TABLE dbo.FLUSAN_EventLog DROP CONSTRAINT [FK_FLUSAN_EventLog_Member]
ALTER TABLE Purchasing.List DROP CONSTRAINT [FK_Purchasing_List__InsertUser]
ALTER TABLE Purchasing.List DROP CONSTRAINT [FK_Purchasing_List__UpdateUser]
ALTER TABLE AutoCheck.Request DROP CONSTRAINT [FK_AutoCheck_Request__Member_InsertUser]
ALTER TABLE dbo.MemberAccess DROP CONSTRAINT [FK_MemberAccess_MemberID]
ALTER TABLE dbo.ReportCenterSessions DROP CONSTRAINT [FK_ReportCenterSession_Member]
ALTER TABLE Marketing.EquipmentList DROP CONSTRAINT [FK_Marketing_EquipmentList__InsertUser]
ALTER TABLE Promotion.Promotion DROP CONSTRAINT [FK_Promotion_Promotion__InsertUser]
ALTER TABLE dbo.map_MemberToInvStatusFilter DROP CONSTRAINT [FK_MemberToInvStatusFilters_Member]
ALTER TABLE Promotion.Promotion DROP CONSTRAINT [FK_Promotion_Promotion__UpdateUser]
ALTER TABLE Marketing.ConsumerHighlightSectionPreference DROP CONSTRAINT [FK_Marketing_ConsumerHighlightSectionPreference__InsertMember]
ALTER TABLE Marketing.ConsumerHighlightSectionPreference DROP CONSTRAINT [FK_Marketing_ConsumerHighlightSectionPreference__UpdateMember]
ALTER TABLE Marketing.Equipment DROP CONSTRAINT [FK_Marketing_Equipment__InsertUser]
ALTER TABLE AutoCheck.Account_Member DROP CONSTRAINT [FK_AutoCheck_Account_Member__MemberID]
ALTER TABLE Marketing.Equipment DROP CONSTRAINT [FK_Marketing_Equipment__UpdateUser]
ALTER TABLE dbo.tbl_MemberATCAccessGroupList DROP CONSTRAINT [FK_MemberATCAccessGroupList_Member]
ALTER TABLE AutoCheck.Account_Member DROP CONSTRAINT [FK_AutoCheck_Account_Member__MemberID_InsertUser]
ALTER TABLE Marketing.SnippetSourcePreference DROP CONSTRAINT [FK_Marketing_SnippetSourcePreference__InsertMember]
ALTER TABLE Marketing.SnippetSourcePreference DROP CONSTRAINT [FK_Marketing_SnippetSourcePreference__UpdateMember]
ALTER TABLE AutoCheck.Account_Dealer DROP CONSTRAINT [FK_AutoCheck_Account_Dealer__InsertMember]
ALTER TABLE Carfax.Account DROP CONSTRAINT [FK_Carfax_Account__InsertMember]
ALTER TABLE Distribution.Vehicle DROP CONSTRAINT [FK_Distribution_Vehicle__Member]
ALTER TABLE Carfax.Account DROP CONSTRAINT [FK_Carfax_Account__UpdateMember]
ALTER TABLE Carfax.Account DROP CONSTRAINT [FK_Carfax_Account__DeleteMember]
ALTER TABLE Marketing.VehicleEquipmentProvider DROP CONSTRAINT [FK_Marketing_VehicleEquipmentProvider__InsertUser]
ALTER TABLE dbo.MemberInsightTypePreference DROP CONSTRAINT [FK_MemberInsightType_Member]
ALTER TABLE Marketing.VehicleEquipmentProvider DROP CONSTRAINT [FK_Marketing_VehicleEquipmentProvider__UpdateUser]
ALTER TABLE dbo.tbl_RedistributionLog DROP CONSTRAINT [FK_tbl_RedistributionLog_Member]
ALTER TABLE dbo.MemberCredential DROP CONSTRAINT [FK_MemberCredential__Member]
ALTER TABLE dbo.AuditBookOuts DROP CONSTRAINT [FK_AuditBookOuts_MemberId]
ALTER TABLE Marketing.PhotoVehiclePreference DROP CONSTRAINT [FK_Marketing_PhotoVehiclePreference__InsertUser]
ALTER TABLE Distribution.Message DROP CONSTRAINT [FK_Distribution_Message__Member]
ALTER TABLE Promotion.Activity DROP CONSTRAINT [FK_Promotion_Activity__InsertUser]
ALTER TABLE Marketing.PhotoVehiclePreference DROP CONSTRAINT [FK_Marketing_PhotoVehiclePreference__UpdateUser]
ALTER TABLE Carfax.Vehicle DROP CONSTRAINT [FK_Carfax_Vehicle_Member__UpdateUser]
ALTER TABLE dbo.KBBArchiveDatasetOverride DROP CONSTRAINT [FK_KBBArchiveDatasetOverride_Member]
ALTER TABLE Carfax.Vehicle DROP CONSTRAINT [FK_Carfax_Vehicle_Member__InsertUser]
ALTER TABLE Marketing.EquipmentProviderAssignmentList DROP CONSTRAINT [FK_Marketing_EquipmentProviderAssignmentList__InsertUser]
ALTER TABLE dbo.MemberAppraisalReviewPreferences DROP CONSTRAINT [FK_MemberAppraisalReviewPreferences_MemberID]
ALTER TABLE Marketing.ValueAnalyzerVehiclePreference DROP CONSTRAINT [FK_Marketing_ValueAnalyzerVehiclePreference__InsertUser]
ALTER TABLE Marketing.ValueAnalyzerVehiclePreference DROP CONSTRAINT [FK_Marketing_ValueAnalyzerVehiclePreference__UpdateUser]
ALTER TABLE Carfax.Request DROP CONSTRAINT [FK_Request__InsertUser]
ALTER TABLE dbo.MemberRole DROP CONSTRAINT [FK_MemberRole_Member]
ALTER TABLE Marketing.EquipmentProviderAssignment DROP CONSTRAINT [FK_Marketing_EquipmentProviderAssignment__InsertUser]
ALTER TABLE Marketing.EquipmentProviderAssignment DROP CONSTRAINT [FK_Marketing_EquipmentProviderAssignment__UpdateUser]
ALTER TABLE WebImport.Account DROP CONSTRAINT [FK_WebImport.Account.InsertUserID]
ALTER TABLE WebImport.Account DROP CONSTRAINT [FK_WebImport.Account.UpdateUserID]
ALTER TABLE dbo.MemberBusinessUnitSet DROP CONSTRAINT [FK_MemberBusinessUnitSet_Member]
ALTER TABLE AutoCheck.Account_ServiceType DROP CONSTRAINT [FK_AutoCheck_Account_ServiceType__InsertMember]
ALTER TABLE Marketing.MarketListingVehiclePreferenceSearchOverrides DROP CONSTRAINT [FK_Marketing_MarketListingVehiclePreferenceSearchOverrides_Member_InsertUserId]
ALTER TABLE Marketing.MarketListingVehiclePreferenceSearchOverrides DROP CONSTRAINT [FK_Marketing_MarketListingVehiclePreferenceSearchOverrides_Member_UpdateUserId]
ALTER TABLE Carfax.ReportProcessorCommand DROP CONSTRAINT [FK_Carfax_ReportProcessorCommand_Member__InsertUser]
ALTER TABLE Carfax.ReportProcessorCommand DROP CONSTRAINT [FK_Carfax_ReportProcessorCommand_Member__UpdateUser]
ALTER TABLE Carfax.Account_Member DROP CONSTRAINT [FK_Account_Member__Member]
ALTER TABLE Carfax.Account_Member DROP CONSTRAINT [FK_Account_Member__InsertUser]
ALTER TABLE dbo.SoftwareSystemComponentState DROP CONSTRAINT [FK_SoftwareSystemComponentState_Member_Authorized]
ALTER TABLE dbo.SoftwareSystemComponentState DROP CONSTRAINT [FK_SoftwareSystemComponentState_Member_Impersonating]
ALTER TABLE dbo.SystemErrors DROP CONSTRAINT [FK_SystemErrors_Memeber]
ALTER TABLE Carfax.Account_Dealer DROP CONSTRAINT [FK_Account_Dealer__InsertMember]
ALTER TABLE Distribution.Account DROP CONSTRAINT [FK_Distribution_Account__InsertMember]
ALTER TABLE Distribution.Account DROP CONSTRAINT [FK_Distribution_Account__UpdateMember]
ALTER TABLE dbo.Person DROP CONSTRAINT [FK_Person_Member]
ALTER TABLE dbo.MarketplaceSubmission DROP CONSTRAINT [FK_MarketplaceSubmission_Member]
ALTER TABLE Distribution.Account_Audit DROP CONSTRAINT [FK_Distribution_AccountAudit__Member]
ALTER TABLE Carfax.ReportPreference DROP CONSTRAINT [FK_Carfax_ReportPreference__InsertUser]
ALTER TABLE Carfax.ReportPreference DROP CONSTRAINT [FK_Carfax_ReportPreference__UpdateUser]
ALTER TABLE Distribution.Publication DROP CONSTRAINT [FK_Distribution_Publication__InsertMember]
ALTER TABLE Distribution.Publication DROP CONSTRAINT [FK_Distribution_Publication__UpdateMember]
ALTER TABLE dbo.MemberLicenseAcceptance DROP CONSTRAINT [FK_MemberLicenseAcceptance_MemberID]
ALTER TABLE Marketing.AdPreviewVehiclePreference DROP CONSTRAINT [FK_Marketing_AdPreviewVehiclePreference_Member_InsertUser]
ALTER TABLE Marketing.AdPreviewVehiclePreference DROP CONSTRAINT [FK_Marketing_AdPreviewVehiclePreference_Member_UpdateUser]
ALTER TABLE Marketing.MarketListingPreference DROP CONSTRAINT [FK_Marketing_MarketListingPreference_Member_InsertUser]
ALTER TABLE Marketing.MarketListingPreference DROP CONSTRAINT [FK_Marketing_MarketListingPreference_Member_UpdateUser]
ALTER TABLE Marketing.VehicleHistoryReportPreference DROP CONSTRAINT [FK_Marketing_VehicleHistoryReportPreference__InsertMember]
ALTER TABLE Marketing.MarketListingVehiclePreference DROP CONSTRAINT [FK_Marketing_MarketListingVehiclePreference_Member_InsertUser]
ALTER TABLE Marketing.MarketListingVehiclePreference DROP CONSTRAINT [FK_Marketing_MarketListingVehiclePreference_Member_UpdateUser]
ALTER TABLE dbo.SearchCandidateList DROP CONSTRAINT [FK_SearchCandidateList_Member]

ALTER TABLE dbo.Member DROP CONSTRAINT PK_Member

ALTER TABLE [dbo].[Member] ADD CONSTRAINT [PK_Member] PRIMARY KEY CLUSTERED  ([MemberID])  ON [IDX]

ALTER TABLE AutoCheck.Account ADD CONSTRAINT [FK_AutoCheck_Account__InsertMember] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE AutoCheck.Account ADD CONSTRAINT [FK_AutoCheck_Account__UpdateMember] FOREIGN KEY (UpdateUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE AutoCheck.Account ADD CONSTRAINT [FK_AutoCheck_Account__DeleteMember] FOREIGN KEY (DeleteUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.VehicleHistoryReportSourcePreference ADD CONSTRAINT [FK_Marketing_VehicleHistoryReportSourcePreference__InsertMember] FOREIGN KEY (InsertUserId) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.VehicleHistoryReportSourcePreference ADD CONSTRAINT [FK_Marketing_VehicleHistoryReportSourcePreference__UpdateMember] FOREIGN KEY (UpdateUserId) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.DealerPreference ADD CONSTRAINT [FK_DealerPreferenceLithiaCarCenterMember_Member] FOREIGN KEY (DefaultLithiaCarCenterMemberID) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.ConsumerHighlightPreference ADD CONSTRAINT [FK_Marketing_ConsumerHighlightPreference__InsertMember] FOREIGN KEY (InsertUserId) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.ConsumerHighlightPreference ADD CONSTRAINT [FK_Marketing_ConsumerHighlightPreference__UpdateMember] FOREIGN KEY (UpdateUserId) REFERENCES dbo.Member (MemberID)
ALTER TABLE AutoCheck.Vehicle ADD CONSTRAINT [FK_AutoCheck_Vehicle__Member_UpdateUser] FOREIGN KEY (UpdateUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE AutoCheck.Vehicle ADD CONSTRAINT [FK_AutoCheck_Vehicle__Member_InsertUser] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.FLUSAN_EventLog ADD CONSTRAINT [FK_FLUSAN_EventLog_Member] FOREIGN KEY (MemberID) REFERENCES dbo.Member (MemberID)
ALTER TABLE Purchasing.List ADD CONSTRAINT [FK_Purchasing_List__InsertUser] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Purchasing.List ADD CONSTRAINT [FK_Purchasing_List__UpdateUser] FOREIGN KEY (UpdateUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE AutoCheck.Request ADD CONSTRAINT [FK_AutoCheck_Request__Member_InsertUser] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.MemberAccess ADD CONSTRAINT [FK_MemberAccess_MemberID] FOREIGN KEY (MemberID) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.ReportCenterSessions ADD CONSTRAINT [FK_ReportCenterSession_Member] FOREIGN KEY (MemberId) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.EquipmentList ADD CONSTRAINT [FK_Marketing_EquipmentList__InsertUser] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Promotion.Promotion ADD CONSTRAINT [FK_Promotion_Promotion__InsertUser] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.map_MemberToInvStatusFilter ADD CONSTRAINT [FK_MemberToInvStatusFilters_Member] FOREIGN KEY (MemberID) REFERENCES dbo.Member (MemberID)
ALTER TABLE Promotion.Promotion ADD CONSTRAINT [FK_Promotion_Promotion__UpdateUser] FOREIGN KEY (UpdateUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.ConsumerHighlightSectionPreference ADD CONSTRAINT [FK_Marketing_ConsumerHighlightSectionPreference__InsertMember] FOREIGN KEY (InsertUserId) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.ConsumerHighlightSectionPreference ADD CONSTRAINT [FK_Marketing_ConsumerHighlightSectionPreference__UpdateMember] FOREIGN KEY (UpdateUserId) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.Equipment ADD CONSTRAINT [FK_Marketing_Equipment__InsertUser] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE AutoCheck.Account_Member ADD CONSTRAINT [FK_AutoCheck_Account_Member__MemberID] FOREIGN KEY (MemberID) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.Equipment ADD CONSTRAINT [FK_Marketing_Equipment__UpdateUser] FOREIGN KEY (UpdateUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.tbl_MemberATCAccessGroupList ADD CONSTRAINT [FK_MemberATCAccessGroupList_Member] FOREIGN KEY (MemberID) REFERENCES dbo.Member (MemberID)
ALTER TABLE AutoCheck.Account_Member ADD CONSTRAINT [FK_AutoCheck_Account_Member__MemberID_InsertUser] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.SnippetSourcePreference ADD CONSTRAINT [FK_Marketing_SnippetSourcePreference__InsertMember] FOREIGN KEY (InsertUserId) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.SnippetSourcePreference ADD CONSTRAINT [FK_Marketing_SnippetSourcePreference__UpdateMember] FOREIGN KEY (UpdateUserId) REFERENCES dbo.Member (MemberID)
ALTER TABLE AutoCheck.Account_Dealer ADD CONSTRAINT [FK_AutoCheck_Account_Dealer__InsertMember] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Carfax.Account ADD CONSTRAINT [FK_Carfax_Account__InsertMember] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Distribution.Vehicle ADD CONSTRAINT [FK_Distribution_Vehicle__Member] FOREIGN KEY (InsertUserID) REFERENCES dbo.Member (MemberID)
ALTER TABLE Carfax.Account ADD CONSTRAINT [FK_Carfax_Account__UpdateMember] FOREIGN KEY (UpdateUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Carfax.Account ADD CONSTRAINT [FK_Carfax_Account__DeleteMember] FOREIGN KEY (DeleteUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.VehicleEquipmentProvider ADD CONSTRAINT [FK_Marketing_VehicleEquipmentProvider__InsertUser] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.MemberInsightTypePreference ADD CONSTRAINT [FK_MemberInsightType_Member] FOREIGN KEY (MemberID) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.VehicleEquipmentProvider ADD CONSTRAINT [FK_Marketing_VehicleEquipmentProvider__UpdateUser] FOREIGN KEY (UpdateUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.tbl_RedistributionLog ADD CONSTRAINT [FK_tbl_RedistributionLog_Member] FOREIGN KEY (MemberId) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.MemberCredential ADD CONSTRAINT [FK_MemberCredential__Member] FOREIGN KEY (MemberID) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.AuditBookOuts ADD CONSTRAINT [FK_AuditBookOuts_MemberId] FOREIGN KEY (MemberId) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.PhotoVehiclePreference ADD CONSTRAINT [FK_Marketing_PhotoVehiclePreference__InsertUser] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Distribution.Message ADD CONSTRAINT [FK_Distribution_Message__Member] FOREIGN KEY (InsertUserID) REFERENCES dbo.Member (MemberID)
ALTER TABLE Promotion.Activity ADD CONSTRAINT [FK_Promotion_Activity__InsertUser] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.PhotoVehiclePreference ADD CONSTRAINT [FK_Marketing_PhotoVehiclePreference__UpdateUser] FOREIGN KEY (UpdateUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Carfax.Vehicle ADD CONSTRAINT [FK_Carfax_Vehicle_Member__UpdateUser] FOREIGN KEY (UpdateUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.KBBArchiveDatasetOverride ADD CONSTRAINT [FK_KBBArchiveDatasetOverride_Member] FOREIGN KEY (MemberID) REFERENCES dbo.Member (MemberID)
ALTER TABLE Carfax.Vehicle ADD CONSTRAINT [FK_Carfax_Vehicle_Member__InsertUser] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.EquipmentProviderAssignmentList ADD CONSTRAINT [FK_Marketing_EquipmentProviderAssignmentList__InsertUser] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.MemberAppraisalReviewPreferences ADD CONSTRAINT [FK_MemberAppraisalReviewPreferences_MemberID] FOREIGN KEY (MemberID) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.ValueAnalyzerVehiclePreference ADD CONSTRAINT [FK_Marketing_ValueAnalyzerVehiclePreference__InsertUser] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.ValueAnalyzerVehiclePreference ADD CONSTRAINT [FK_Marketing_ValueAnalyzerVehiclePreference__UpdateUser] FOREIGN KEY (UpdateUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Carfax.Request ADD CONSTRAINT [FK_Request__InsertUser] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.MemberRole ADD CONSTRAINT [FK_MemberRole_Member] FOREIGN KEY (MemberID) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.EquipmentProviderAssignment ADD CONSTRAINT [FK_Marketing_EquipmentProviderAssignment__InsertUser] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.EquipmentProviderAssignment ADD CONSTRAINT [FK_Marketing_EquipmentProviderAssignment__UpdateUser] FOREIGN KEY (UpdateUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE WebImport.Account ADD CONSTRAINT [FK_WebImport.Account.InsertUserID] FOREIGN KEY (InsertUserID) REFERENCES dbo.Member (MemberID)
ALTER TABLE WebImport.Account ADD CONSTRAINT [FK_WebImport.Account.UpdateUserID] FOREIGN KEY (UpdateUserID) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.MemberBusinessUnitSet ADD CONSTRAINT [FK_MemberBusinessUnitSet_Member] FOREIGN KEY (MemberID) REFERENCES dbo.Member (MemberID)
ALTER TABLE AutoCheck.Account_ServiceType ADD CONSTRAINT [FK_AutoCheck_Account_ServiceType__InsertMember] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.MarketListingVehiclePreferenceSearchOverrides ADD CONSTRAINT [FK_Marketing_MarketListingVehiclePreferenceSearchOverrides_Member_InsertUserId] FOREIGN KEY (InsertUserId) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.MarketListingVehiclePreferenceSearchOverrides ADD CONSTRAINT [FK_Marketing_MarketListingVehiclePreferenceSearchOverrides_Member_UpdateUserId] FOREIGN KEY (UpdateUserId) REFERENCES dbo.Member (MemberID)
ALTER TABLE Carfax.ReportProcessorCommand ADD CONSTRAINT [FK_Carfax_ReportProcessorCommand_Member__InsertUser] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Carfax.ReportProcessorCommand ADD CONSTRAINT [FK_Carfax_ReportProcessorCommand_Member__UpdateUser] FOREIGN KEY (UpdateUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Carfax.Account_Member ADD CONSTRAINT [FK_Account_Member__Member] FOREIGN KEY (MemberID) REFERENCES dbo.Member (MemberID)
ALTER TABLE Carfax.Account_Member ADD CONSTRAINT [FK_Account_Member__InsertUser] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.SoftwareSystemComponentState ADD CONSTRAINT [FK_SoftwareSystemComponentState_Member_Authorized] FOREIGN KEY (AuthorizedMemberID) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.SoftwareSystemComponentState ADD CONSTRAINT [FK_SoftwareSystemComponentState_Member_Impersonating] FOREIGN KEY (MemberID) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.SystemErrors ADD CONSTRAINT [FK_SystemErrors_Memeber] FOREIGN KEY (MemberID) REFERENCES dbo.Member (MemberID)
ALTER TABLE Carfax.Account_Dealer ADD CONSTRAINT [FK_Account_Dealer__InsertMember] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Distribution.Account ADD CONSTRAINT [FK_Distribution_Account__InsertMember] FOREIGN KEY (InsertUserID) REFERENCES dbo.Member (MemberID)
ALTER TABLE Distribution.Account ADD CONSTRAINT [FK_Distribution_Account__UpdateMember] FOREIGN KEY (UpdateUserID) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.Person ADD CONSTRAINT [FK_Person_Member] FOREIGN KEY (MemberID) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.MarketplaceSubmission ADD CONSTRAINT [FK_MarketplaceSubmission_Member] FOREIGN KEY (MemberID) REFERENCES dbo.Member (MemberID)
ALTER TABLE Distribution.Account_Audit ADD CONSTRAINT [FK_Distribution_AccountAudit__Member] FOREIGN KEY (ChangeUserID) REFERENCES dbo.Member (MemberID)
ALTER TABLE Carfax.ReportPreference ADD CONSTRAINT [FK_Carfax_ReportPreference__InsertUser] FOREIGN KEY (InsertUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Carfax.ReportPreference ADD CONSTRAINT [FK_Carfax_ReportPreference__UpdateUser] FOREIGN KEY (UpdateUser) REFERENCES dbo.Member (MemberID)
ALTER TABLE Distribution.Publication ADD CONSTRAINT [FK_Distribution_Publication__InsertMember] FOREIGN KEY (InsertUserID) REFERENCES dbo.Member (MemberID)
ALTER TABLE Distribution.Publication ADD CONSTRAINT [FK_Distribution_Publication__UpdateMember] FOREIGN KEY (UpdateUserID) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.MemberLicenseAcceptance ADD CONSTRAINT [FK_MemberLicenseAcceptance_MemberID] FOREIGN KEY (MemberID) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.AdPreviewVehiclePreference ADD CONSTRAINT [FK_Marketing_AdPreviewVehiclePreference_Member_InsertUser] FOREIGN KEY (InsertUserID) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.AdPreviewVehiclePreference ADD CONSTRAINT [FK_Marketing_AdPreviewVehiclePreference_Member_UpdateUser] FOREIGN KEY (UpdateUserId) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.MarketListingPreference ADD CONSTRAINT [FK_Marketing_MarketListingPreference_Member_InsertUser] FOREIGN KEY (InsertUserId) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.MarketListingPreference ADD CONSTRAINT [FK_Marketing_MarketListingPreference_Member_UpdateUser] FOREIGN KEY (UpdateUserId) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.VehicleHistoryReportPreference ADD CONSTRAINT [FK_Marketing_VehicleHistoryReportPreference__InsertMember] FOREIGN KEY (InsertUserId) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.MarketListingVehiclePreference ADD CONSTRAINT [FK_Marketing_MarketListingVehiclePreference_Member_InsertUser] FOREIGN KEY (InsertUserID) REFERENCES dbo.Member (MemberID)
ALTER TABLE Marketing.MarketListingVehiclePreference ADD CONSTRAINT [FK_Marketing_MarketListingVehiclePreference_Member_UpdateUser] FOREIGN KEY (UpdateUserId) REFERENCES dbo.Member (MemberID)
ALTER TABLE dbo.SearchCandidateList ADD CONSTRAINT [FK_SearchCandidateList_Member] FOREIGN KEY (MemberID) REFERENCES dbo.Member (MemberID)


/*

SELECT	'ALTER TABLE ' + OBJECT_SCHEMA_NAME(objc.object_id )  + '.' + objc.name + ' DROP CONSTRAINT [' + fk.name +']',
	'ALTER TABLE ' + OBJECT_SCHEMA_NAME(objc.object_id )  + '.' + objc.name + ' ADD CONSTRAINT [' + fk.name + '] FOREIGN KEY (' + coc.name + ') REFERENCES ' +  OBJECT_SCHEMA_NAME(objp.object_id )  + '.' + objp.name + ' (' + cop.name + ')'
from	sys.foreign_keys fk
	inner join sys.foreign_key_columns fkc on fkc.constraint_object_id = fk.object_id
	inner join sys.objects objc on objc.object_id = fk.parent_object_id
	inner join sys.columns coc on coc.object_id = objc.object_id and coc.column_id = fkc.parent_column_id
	inner join sys.objects objp on objp.object_id = fk.referenced_object_id
	inner join sys.columns cop on cop.object_id = objp.object_id and cop.column_id = fkc.referenced_column_id
WHERE	objp.name = 'Member'


*/