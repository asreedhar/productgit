
--------------------------------------------------------------------------------
-- 	Remove nearly worthless index, replace with general-purpose lookup 
--------------------------------------------------------------------------------

DROP INDEX dbo.Inventory.IX_Inventory__InventoryReceivedDate_EtAl

CREATE NONCLUSTERED INDEX IX_Inventory__BusinessUnitIDActiveTypeDate ON dbo.Inventory (BusinessUnitID, InventoryActive, InventoryType,InventoryReceivedDate ) INCLUDE (VehicleID, InventoryID) WITH (FILLFACTOR=95) ON IDX
GO

CREATE INDEX IX_Appraisals_BUIDTypeStatus on dbo.Appraisals(BusinessUnitID, AppraisalTypeID, AppraisalStatusID)
GO

ALTER TABLE dbo.DealerPreference_Dataload DROP CONSTRAINT FK_DealerPreference_Dataload__DealerPreference
ALTER TABLE dbo.DealerRisk DROP CONSTRAINT FK_DealerRisk__DealerPreference
GO
ALTER TABLE dbo.DealerPreference DROP CONSTRAINT PK_DealerPreference
ALTER TABLE dbo.DealerPreference DROP CONSTRAINT IX_DealerPreference
GO

------------------------------------------------------------------------------------------------
--	We could probably drop DealerPreferenceID altogether.  It might be used for Hibernate 
--	mappings in the old Java app, so best to leave in for now.
------------------------------------------------------------------------------------------------

CREATE UNIQUE CLUSTERED INDEX IXC_DealerPreference ON dbo.DealerPreference(BusinessUnitID)
ALTER TABLE dbo.DealerPreference ADD CONSTRAINT PK_DealerPreference PRIMARY KEY (DealerPreferenceID)

GO
ALTER TABLE dbo.DealerPreference_Dataload ADD CONSTRAINT FK_DealerPreference_Dataload__DealerPreference FOREIGN KEY (BusinessUnitID) REFERENCES dbo.DealerPreference (BusinessUnitID)
ALTER TABLE dbo.DealerRisk ADD CONSTRAINT FK_DealerRisk__DealerPreference FOREIGN KEY (BusinessUnitID) REFERENCES dbo.DealerRisk (BusinessUnitID)
GO

CREATE INDEX IX_Distribution_Message__DealerVehicle ON Distribution.Message(DealerID, VehicleEntityID, VehicleEntityTypeID) INCLUDE (MessageID)
CREATE INDEX IX_Distribution_Publication__MessageID ON Distribution.Publication(MessageID) INCLUDE (PublicationID)

GO