
ALTER TABLE dbo.AppraisalValues DROP CONSTRAINT PK_AppraisalValues
ALTER TABLE dbo.AppraisalValues DROP CONSTRAINT UK_AppraisalValues
ALTER TABLE dbo.AppraisalValues ALTER COLUMN AppraisalID INT NOT NULL
GO
ALTER TABLE dbo.AppraisalValues ADD CONSTRAINT PK_AppraisalValues PRIMARY KEY NONCLUSTERED (AppraisalValueID)
CREATE UNIQUE CLUSTERED INDEX UKC_AppraisalValues ON dbo.AppraisalValues(AppraisalID, SequenceNumber)
GO

--------------------------------------------------------------------------------
--	AppraisalFormID is not required and should be removed
--------------------------------------------------------------------------------

ALTER TABLE dbo.AppraisalFormOptions DROP CONSTRAINT PK_AppraisalFormOptions
ALTER TABLE dbo.AppraisalFormOptions DROP CONSTRAINT UK_AppraisalFormOptions_AppraisalID
ALTER TABLE dbo.AppraisalFormOptions ALTER COLUMN AppraisalID INT NOT NULL
GO
ALTER TABLE dbo.AppraisalFormOptions ADD CONSTRAINT PK_AppraisalFormOptions PRIMARY KEY NONCLUSTERED (AppraisalFormID)
CREATE UNIQUE CLUSTERED INDEX UKC_AppraisalFormOptions ON dbo.AppraisalFormOptions(AppraisalID)

GO
--------------------------------------------------------------------------------
ALTER TABLE dbo.AppraisalActions DROP CONSTRAINT PK_AppraisalActions
ALTER TABLE dbo.AppraisalActions DROP CONSTRAINT UK_AppraisalActions_AppraisalID

GO
ALTER TABLE dbo.AppraisalActions ADD CONSTRAINT PK_AppraisalActions PRIMARY KEY NONCLUSTERED (AppraisalActionID)
CREATE UNIQUE CLUSTERED INDEX UKC_AppraisalActions ON dbo.AppraisalActions(AppraisalID)

GO
--------------------------------------------------------------------------------
ALTER TABLE dbo.CIABodyTypeDetails DROP CONSTRAINT FK_CIABodyTypeDetails__Segment
ALTER TABLE dbo.MakeModelGrouping DROP CONSTRAINT FK_MakeModelGrouping__Segment
GO
ALTER TABLE dbo.Segment DROP CONSTRAINT PK_Segment
ALTER TABLE dbo.Segment ADD CONSTRAINT PK_Segment PRIMARY KEY NONCLUSTERED (SegmentID)
GO

ALTER TABLE dbo.CIABodyTypeDetails ADD CONSTRAINT FK_CIABodyTypeDetails__Segment FOREIGN KEY (SegmentID) REFERENCES dbo.Segment(SegmentID)
ALTER TABLE dbo.MakeModelGrouping ADD CONSTRAINT FK_MakeModelGrouping__Segment FOREIGN KEY (SegmentID) REFERENCES dbo.Segment(SegmentID)
--------------------------------------------------------------------------------

