ALTER TABLE IMT.dbo.Inventory_CertificationNumber DROP CONSTRAINT CK_InventoryCertificationNumber_GM
GO

ALTER TABLE IMT.dbo.Inventory_CertificationNumber  WITH CHECK ADD CONSTRAINT CK_InventoryCertificationNumber_GM 
CHECK  (
	ManufacturerID = 5 
	AND (
		CertificationNumber like replicate('[0-9]',6) 
		OR CertificationNumber like replicate('[0-9]',7)
		OR CertificationNumber like replicate('[0-9]',8)
	)
)
GO