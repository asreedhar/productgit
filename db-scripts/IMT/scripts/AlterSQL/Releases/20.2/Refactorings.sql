--------------------------------------------------------------------------------
--	CASE 30567
--
--	Recreate table w/ANSI NULLS ON 
--------------------------------------------------------------------------------

SELECT	*
INTO	#DU
FROM	dbo.DealerUpgrade


DROP TABLE dbo.DealerUpgrade

GO
SET ANSI_NULLS ON
GO

CREATE TABLE [dbo].[DealerUpgrade]
(
[BusinessUnitID] [int] NOT NULL,
[DealerUpgradeCD] [tinyint] NOT NULL,
[StartDate] [smalldatetime] NULL,
[EndDate] [smalldatetime] NULL,
[Active] [tinyint] NOT NULL,
[EffectiveDateActive] AS (CONVERT([tinyint],case when dateadd(day,datediff(day,(0),getdate()),(0))>=dateadd(day,datediff(day,(0),coalesce([StartDate],getdate())),(0)) AND dateadd(day,datediff(day,(0),getdate()),(0))<=dateadd(day,datediff(day,(0),coalesce([EndDate],getdate())),(0)) then (1) else (0) end&[Active],0))
) ON [DATA]
GO
ALTER TABLE [dbo].[DealerUpgrade] ADD CONSTRAINT [PK_DealerUpgrade] PRIMARY KEY CLUSTERED  ([BusinessUnitID], [DealerUpgradeCD]) WITH (FILLFACTOR=95) ON [DATA]
GO
ALTER TABLE [dbo].[DealerUpgrade] ADD CONSTRAINT [FK_DealerUpgrade_lu_DealerUpgrade] FOREIGN KEY ([DealerUpgradeCD]) REFERENCES [dbo].[lu_DealerUpgrade] ([DealerUpgradeCD])
GO

CREATE INDEX IX_DealerUpgrade_CDActive ON dbo.DealerUpgrade(DealerUpgradeCD, Active) INCLUDE (BusinessUnitID, StartDate, EndDate)
GO

INSERT
INTO	dbo.DealerUpgrade (BusinessUnitID,DealerUpgradeCD,StartDate,EndDate,Active)
SELECT	BusinessUnitID,DealerUpgradeCD,StartDate,EndDate,Active FROM #DU

GO

--------------------------------------------------------------------------------
--	CASE 22095
--
--	IMT.dbo.Inventory_Level4Analysis -- DESTROY!
--------------------------------------------------------------------------------

DROP TABLE dbo.Inventory_Level4Analysis
GO

--------------------------------------------------------------------------------
--	INCORRECT, REDUNDANT
--------------------------------------------------------------------------------

ALTER TABLE dbo.Inventory_Insight DROP CONSTRAINT FK_Inventory_Level4Analysis__InventoryID 
