/*
The name has officially changed (again).  From this point forward, she shall be known as 'MAX Merchandising'.
*/

UPDATE	dbo.lu_DealerUpgrade
SET	DealerUpgradeDESC = 'MAX Merchandising'
WHERE	DealerUpgradeCD = 23
