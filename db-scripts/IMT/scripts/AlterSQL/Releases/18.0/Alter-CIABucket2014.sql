
--------------------------------------------------------------------------------
--	ORIGINALLY PLANNED TO BE DONE IN AN ADMIN TOOL, THIS NEEDS TO BE DONE
--	EVERY YEAR AS LONG AS THE CIA BUCKETRON IS DRIVEN COMPLETELY FROM DATA
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- 	ADD 2013
--------------------------------------------------------------------------------

UPDATE	CIABuckets
SET	RANK = RANK + 1
WHERE	CIABucketGroupID = 1

INSERT
INTO	CIABuckets (CIABucketGroupID, Rank, Description)
SELECT	1, 1, '2013'

INSERT
INTO	CIABucketRules (CIABucketID, CIABucketRuleTypeID, Value, Value2)
SELECT	26, 1, 2013, NULL

--------------------------------------------------------------------------------
-- 	DROP 2005
--------------------------------------------------------------------------------

DELETE 
FROM	CIABucketRules
WHERE	CIABucketID = 2

DELETE 
FROM	CIABuckets
WHERE	CIABucketID = 2


--------------------------------------------------------------------------------
-- 	ADD 2014
--------------------------------------------------------------------------------

UPDATE	CIABuckets
SET	RANK = RANK + 1
WHERE	CIABucketGroupID = 1

INSERT
INTO	CIABuckets (CIABucketGroupID, Rank, Description)
SELECT	1, 1, '2014'

INSERT
INTO	CIABucketRules (CIABucketID, CIABucketRuleTypeID, Value, Value2)
SELECT	27, 1, 2014, NULL

--------------------------------------------------------------------------------
-- 	DROP 2006
--------------------------------------------------------------------------------

DELETE 
FROM	CIABucketRules
WHERE	CIABucketID = 1

DELETE 
FROM	CIABuckets
WHERE	CIABucketID = 1
