USE [IMT]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Distribution].[FK_Distribution_AppraisalValue__Message]') AND parent_object_id = OBJECT_ID(N'[Distribution].[AppraisalValue]'))
ALTER TABLE [Distribution].[AppraisalValue] DROP CONSTRAINT [FK_Distribution_AppraisalValue__Message]
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Distribution].[CK_Distribution_AppraisalValue__Value]') AND parent_object_id = OBJECT_ID(N'[Distribution].[AppraisalValue]'))
ALTER TABLE [Distribution].[AppraisalValue] DROP CONSTRAINT [CK_Distribution_AppraisalValue__Value]
GO

USE [IMT]
GO

/****** Object:  Table [Distribution].[AppraisalValue]    Script Date: 01/14/2014 15:32:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Distribution].[AppraisalValue]') AND type in (N'U'))
DROP TABLE [Distribution].[AppraisalValue]
GO

USE [IMT]
GO

/****** Object:  Table [Distribution].[AppraisalValue]    Script Date: 01/14/2014 15:32:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Distribution].[AppraisalValue](
	[MessageID] [int] NOT NULL,
	[Value] [int] NOT NULL,
 CONSTRAINT [PK_Distribution_AppraisalValue] PRIMARY KEY CLUSTERED 
(
	[MessageID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]

GO

ALTER TABLE [Distribution].[AppraisalValue]  WITH CHECK ADD  CONSTRAINT [FK_Distribution_AppraisalValue__Message] FOREIGN KEY([MessageID])
REFERENCES [Distribution].[Message] ([MessageID])
GO

ALTER TABLE [Distribution].[AppraisalValue] CHECK CONSTRAINT [FK_Distribution_AppraisalValue__Message]
GO

ALTER TABLE [Distribution].[AppraisalValue]  WITH CHECK ADD  CONSTRAINT [CK_Distribution_AppraisalValue__Value] CHECK  (([Value]>=(0) AND [Value]<=(999999)))
GO

ALTER TABLE [Distribution].[AppraisalValue] CHECK CONSTRAINT [CK_Distribution_AppraisalValue__Value]
GO


