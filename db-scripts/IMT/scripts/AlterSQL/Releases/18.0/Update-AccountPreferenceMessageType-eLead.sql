Declare @TempAccountPreferenceTableVariable TABLE(
RowNum int,
DealerID int)

insert into @TempAccountPreferenceTableVariable select ROW_NUMBER() OVER(ORDER BY DealerID DESC),DealerID from Distribution.AccountPreferences where ProviderID=6 and  MessageTypes=1
delete from distribution.AccountPreferences where ProviderID=6 and MessageTypes=1
delete from distribution.ProviderMessageType where ProviderID=6 and MessageTypes=1

declare @RowNum int,
@newDealerId int

select @RowNum=count(1) from @TempAccountPreferenceTableVariable

WHILE (@RowNum>0)
BEGIN
select @newDealerId=dealerId from @TempAccountPreferenceTableVariable where RowNum=@RowNum;
insert into distribution.AccountPreferences (DealerID,MessageTypes,ProviderID) values (@newDealerId,8,6)
SET @RowNum=@RowNum-1
END