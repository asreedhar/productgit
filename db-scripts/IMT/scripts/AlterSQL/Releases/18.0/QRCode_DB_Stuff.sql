USE IMT;

IF NOT EXISTS ( SELECT * FROM WindowSticker.DataPointKey WHERE DataPointKeyId = 26 )
BEGIN
	INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] )
	VALUES ( 26, 'QRCodeURL' );
END

IF NOT EXISTS ( SELECT * FROM sys.columns WHERE Name = N'ExtraDataJSON' AND Object_ID = Object_ID(N'WindowSticker.ContentArea') )
BEGIN
	ALTER TABLE WindowSticker.ContentArea ADD ExtraDataJSON text NULL
END

