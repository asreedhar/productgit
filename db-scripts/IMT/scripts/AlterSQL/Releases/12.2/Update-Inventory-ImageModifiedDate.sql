
UPDATE	I WITH (ROWLOCK)
SET	ImageModifiedDate = IMD.PhotoUpdated
FROM	dbo.Inventory I
	INNER JOIN (	SELECT	IP.InventoryID, MAX(P.PhotoUpdated) PhotoUpdated
			FROM	dbo.InventoryPhotos IP WITH (NOLOCK)
			    	INNER JOIN dbo.Photos P  WITH (NOLOCK) ON IP.PhotoID = P.PhotoID
			WHERE	YEAR(PhotoUpdated) > 1900			    	
			GROUP
			BY	IP.InventoryID
			HAVING	COUNT(DISTINCT P.PhotoUpdated) = 1	
			) IMD ON I.InventoryID = IMD.InventoryID
WHERE	I.InventoryActive = 1


ALTER TABLE dbo.Photos DROP COLUMN PhotoUpdated	