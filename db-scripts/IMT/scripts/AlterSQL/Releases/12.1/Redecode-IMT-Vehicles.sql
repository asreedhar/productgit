------------------------------------------------------------------------------------------------
--
--	Re-decode vehicles in IMT using new logic that properly decodes to a level two 
--	when there is no valid trim for the given vehicle in the catalog by ignoring the source 
--	trim altogether. 
--
------------------------------------------------------------------------------------------------


UPDATE	V
SET	VehicleCatalogID = VC.VehicleCatalogID
FROM	dbo.Inventory I
	INNER JOIN dbo.Vehicle V ON I.VehicleID = V.VehicleID
	INNER JOIN VehicleCatalog.Firstlook.VehicleCatalog VCI ON V.VehicleCatalogID = VCI.VehicleCatalogID
	CROSS APPLY (
		SELECT	VehicleCatalogID		= COALESCE(VC2T.VehicleCatalogID,VC2.VehicleCatalogID,VC1.VehicleCatalogID),
			VehicleCatalogLevelID		= COALESCE(VC2T.VehicleCatalogLevelID,VC2.VehicleCatalogLevelID,VC1.VehicleCatalogLevelID),
			Transmission			= COALESCE(VC2T.Transmission,VC2.Transmission,VC1.Transmission)

		FROM	VehicleCatalog.Firstlook.VehicleCatalog VC1 
				
			LEFT JOIN VehicleCatalog.Firstlook.VehicleCatalog VC2 ON VC1.VehicleCatalogID = VC2.ParentID
										AND VC2.IsDistinctSeries = 1
										AND CASE WHEN ISNULL(V.VehicleTrim,'') IN ('','N/A') THEN UPPER(COALESCE(NULLIF(VC1.Series,''),  '')) ELSE V.VehicleTrim END = VC2.Series
			
			LEFT JOIN VehicleCatalog.Firstlook.VINPatternPriority VPP ON VC1.CountryCode = VPP.CountryCode AND COALESCE(VC1.VINPattern,VC2.VINPattern) = VPP.VINPattern AND V.VIN LIKE VPP.PriorityVINPattern 

			LEFT JOIN VehicleCatalog.Firstlook.VehicleCatalog VC2T ON VC1.VehicleCatalogID = VC2T.ParentID
										AND VC2T.IsDistinctSeriesTransmission = 1
										AND (	(CASE WHEN ISNULL(V.VehicleTrim,'') IN ('','N/A') THEN UPPER(COALESCE(NULLIF(VC1.Series,''),  '')) ELSE V.VehicleTrim END = VC2T.Series)		-- EXACT MATCH
											OR (COALESCE(VC1.Series,'') = COALESCE(VC2T.Series,''))			-- NO NEED TO MATCH ON SERIES, ALL VALUES EMPTY
											)	
										AND Market.Listing.GetStandardizedTransmission(V.VehicleTransmission) = VC2T.Transmission 
			
			LEFT JOIN (	VehicleCatalog.Firstlook.Model MD 
					INNER JOIN VehicleCatalog.Firstlook.Line L ON MD.LineID = L.LineID
					INNER JOIN VehicleCatalog.Firstlook.Make MK ON L.MakeID = MK.MakeID
					) ON COALESCE(VC2T.ModelID,VC2.ModelID,VC1.ModelID) = MD.ModelID
					
			LEFT JOIN dbo.MakeModelGrouping MMG ON COALESCE(VC2T.ModelID,VC2.ModelID,VC1.ModelID) = MMG.ModelID
											
		WHERE   VC1.CountryCode = 1
			AND VC1.VehicleCatalogLevelID = 1
			AND SUBSTRING(V.VIN,1,8) + SUBSTRING (V.VIN,10,1) = VC1.SquishVin
			AND V.VIN LIKE VC1.VINPattern
			AND VPP.PriorityVINPattern IS NULL  
		) VC	
	
WHERE	I.InventoryActive = 1
	AND V.VehicleCatalogID <> VC.VehicleCatalogID
	AND VCI.VehicleCatalogLevelID = 1
	AND VC.VehicleCatalogLevelID = 2



IF OBJECT_ID('FLDW.dbo.Vehicle') IS NOT NULL

	UPDATE	V
	SET	VehicleCatalogID = IV.VehicleCatalogID
	FROM	FLDW.dbo.Vehicle V
		INNER JOIN dbo.Vehicle IV ON V.Vin = IV.VIN
	WHERE	V.VehicleCatalogID <> IV.VehicleCatalogID	
