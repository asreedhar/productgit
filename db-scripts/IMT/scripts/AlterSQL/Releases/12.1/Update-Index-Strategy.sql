ALTER TABLE dbo.CIAGroupingItemDetails DROP CONSTRAINT PK_CIAGroupingItemDetails 
ALTER TABLE dbo.CIAGroupingItemDetails add CONSTRAINT PK_CIAGroupingItemDetails PRIMARY KEY NONCLUSTERED  (CIAGroupingItemDetailID) ON DATA
GO
CREATE CLUSTERED INDEX IXC_CIAGroupingItemDetails__IDs ON dbo.CIAGroupingItemDetails (CIAGroupingItemID, CIAGroupingItemDetailLevelID, CIAGroupingItemDetailTypeID) ON DATA
GO

DROP INDEX dbo.CIAGroupingItemDetails.IX_CIAGroupingItemDetails__CIAGroupingItemID 
GO
DROP INDEX dbo.CIAGroupingItemDetails.IX_CIAGroupingItemDetailsGroupingItemIdGroupingItemDetailLevelIdGroupingItemDetailTypeId 
GO