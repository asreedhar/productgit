ALTER TABLE dbo.DealerGroupPreference
ADD CarSearchTitle VARCHAR(100)
GO

UPDATE dbo.DealerGroupPreference
SET CarSearchTitle = 'Group Car Search'
GO

ALTER TABLE dbo.DealerGroupPreference
ALTER COLUMN CarSearchTitle VARCHAR(100) NOT NULL
GO

UPDATE dbo.DealerGroupPreference
SET CarSearchTitle = 'Luther Car Search'
WHERE BusinessUnitID = 101676

UPDATE dbo.DealerGroupPreference
SET CarSearchTitle = 'Hendrick Car Search'
WHERE BusinessUnitID = 100068
GO