
ALTER TABLE dbo.DealerGroupPreference
ADD ShowCarSearch bit NOT NULL DEFAULT (0)
GO

UPDATE dbo.DealerGroupPreference
SET ShowCarSearch = IncludeTransferPricing
FROM dbo.DealerGroupPreference