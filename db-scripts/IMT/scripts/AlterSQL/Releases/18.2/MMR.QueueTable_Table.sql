USE [IMT]
GO

/****** Object:  Table [MMR].[QueueTable]    Script Date: 03/10/2014 17:13:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MMR].[QueueTable]') AND type in (N'U'))
DROP TABLE [MMR].[QueueTable]
GO

USE [IMT]
GO

/****** Object:  Table [MMR].[QueueTable]    Script Date: 03/10/2014 17:13:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [MMR].[QueueTable](
	[InventoryID] [int] NOT NULL,
	[VIN] [varchar](17) NULL,
	[Mileage] [int] NULL,
	[MID] [varchar](100) NULL,
	[Year] [varchar](4) NULL,
	[StatusID] [smallint] NOT NULL,
	[Attempt] [int] NOT NULL,
	[ErrorMessage] [varchar](2000) NULL,
	[DateCreated] [datetime] NULL,
	[DateUpdated] [datetime] NULL,
	[QueueID] [int] IDENTITY(1,1) NOT NULL,
	[RegionCode] [varchar](20) NULL,
 CONSTRAINT [PK_QueueTable] PRIMARY KEY CLUSTERED 
(
	[QueueID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]

GO

SET ANSI_PADDING OFF
GO

