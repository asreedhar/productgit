CREATE TYPE MMR.QueueResult AS TABLE
	(
	  QueueID INT,
	  StatusID Int,
	  ErrorMessage varchar(2000)
	);