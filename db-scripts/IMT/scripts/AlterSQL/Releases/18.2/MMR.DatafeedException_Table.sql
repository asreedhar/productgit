USE [IMT]
GO

/****** Object:  Table [MMR].[DatafeedException]    Script Date: 03/10/2014 17:09:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MMR].[DatafeedException]') AND type in (N'U'))
DROP TABLE [MMR].[DatafeedException]
GO

USE [IMT]
GO

/****** Object:  Table [MMR].[DatafeedException]    Script Date: 03/10/2014 17:09:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [MMR].[DatafeedException](
	[ExceptionID] [int] IDENTITY(1,1) NOT NULL,
	[ExceptionTime] [datetime] NOT NULL,
	[MachineName] [nvarchar](256) NOT NULL,
	[Message] [nvarchar](1024) NOT NULL,
	[ExceptionType] [nvarchar](256) NULL,
	[Details] [ntext] NULL,
	[ExceptionUserID] [int] NULL,
	[RequestMessage] [varchar](max) NULL,
 CONSTRAINT [PK_MMR_DatafeedException] PRIMARY KEY CLUSTERED 
(
	[ExceptionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA] TEXTIMAGE_ON [DATA]

GO

SET ANSI_PADDING OFF
GO


