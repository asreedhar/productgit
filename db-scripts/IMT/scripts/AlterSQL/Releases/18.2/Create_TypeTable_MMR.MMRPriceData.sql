CREATE TYPE MMR.MMRPriceData AS TABLE
	(
	  InventoryID INT,
	  AveragePrice Decimal(9,2),
	  MID varchar(50),
	  Region varchar(20)
	);