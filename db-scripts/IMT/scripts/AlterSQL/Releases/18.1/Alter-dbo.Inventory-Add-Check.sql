--------------------------------------------------------------------------------
--
--	case 23820
--	mostly meaningless, but since someone at some point mangled data in
--	production, makes sense to add
--
-------------------------------------------------------------------------------- 

ALTER TABLE dbo.Inventory ADD CONSTRAINT CK_Inventory__InventoryActive CHECK (InventoryActive IN (0,1))

