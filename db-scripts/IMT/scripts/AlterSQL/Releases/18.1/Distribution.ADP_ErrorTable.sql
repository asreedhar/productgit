USE [IMT]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Distribution].[FK_Distribution_ADPError__ADP_ErrorType]') AND parent_object_id = OBJECT_ID(N'[Distribution].[ADP_Error]'))
ALTER TABLE [Distribution].[ADP_Error] DROP CONSTRAINT [FK_Distribution_ADPError__ADP_ErrorType]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Distribution].[FK_Distribution_ADPError__SubmissionError]') AND parent_object_id = OBJECT_ID(N'[Distribution].[ADP_Error]'))
ALTER TABLE [Distribution].[ADP_Error] DROP CONSTRAINT [FK_Distribution_ADPError__SubmissionError]
GO

USE [IMT]
GO

/****** Object:  Table [Distribution].[ADP_Error]    Script Date: 01/24/2014 17:24:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Distribution].[ADP_Error]') AND type in (N'U'))
DROP TABLE [Distribution].[ADP_Error]
GO

USE [IMT]
GO

/****** Object:  Table [Distribution].[ADP_Error]    Script Date: 01/24/2014 17:24:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Distribution].[ADP_Error](
	[ErrorID] [int] NOT NULL,
	[ErrorTypeID] [int] NOT NULL,
	[Request] [varchar](2000) NULL,
	[ErrorMessage] [varchar](2000) NULL,
 CONSTRAINT [PK_Distribution_ADPError] PRIMARY KEY CLUSTERED 
(
	[ErrorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [Distribution].[ADP_Error]  WITH CHECK ADD  CONSTRAINT [FK_Distribution_ADPError__ADP_ErrorType] FOREIGN KEY([ErrorTypeID])
REFERENCES [Distribution].[ADP_ErrorType] ([ErrorTypeID])
GO

ALTER TABLE [Distribution].[ADP_Error] CHECK CONSTRAINT [FK_Distribution_ADPError__ADP_ErrorType]
GO

ALTER TABLE [Distribution].[ADP_Error]  WITH CHECK ADD  CONSTRAINT [FK_Distribution_ADPError__SubmissionError] FOREIGN KEY([ErrorID])
REFERENCES [Distribution].[SubmissionError] ([ErrorID])
GO

ALTER TABLE [Distribution].[ADP_Error] CHECK CONSTRAINT [FK_Distribution_ADPError__SubmissionError]
GO


