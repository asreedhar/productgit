INSERT
INTO	Extract.DealerConfigurationFlag(DestinationName, Name, BitPosition, Description)
SELECT	'AULtec', 'ExportZeroSpecialPrice', 5, 'Flags whether to export a zero SpecialPrice for a dealer (instead of NULL)'

--------------------------------------------------------------------
--	Enable for Newman Chevrolet
--------------------------------------------------------------------
BEGIN TRY
        
	EXEC Extract.DealerConfiguration#SetFlag @BusinessUnitID = 104696,
	        @DestinationName = 'AULtec',
	        @FlagName = 'ExportZeroSpecialPrice',
	        @Value = 1
        
END TRY
BEGIN CATCH
	PRINT 'CATCH ERROR FOR SCRATCH BUILD'
END CATCH	        