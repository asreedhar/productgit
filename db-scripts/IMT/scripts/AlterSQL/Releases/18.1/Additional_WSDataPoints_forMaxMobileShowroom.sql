USE IMT;


IF NOT EXISTS ( SELECT * FROM WindowSticker.DataPointKey WHERE DataPointKeyId = 27 )
BEGIN
	INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] )
	VALUES ( 27, 'MaxShowroomDirectURL' );
END


IF NOT EXISTS ( SELECT * FROM WindowSticker.DataPointKey WHERE DataPointKeyId = 28 )
BEGIN
	INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] )
	VALUES ( 28, 'MaxShowroomSMSCode' );
END


IF NOT EXISTS ( SELECT * FROM WindowSticker.DataPointKey WHERE DataPointKeyId = 29 )
BEGIN
	INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] )
	VALUES ( 29, 'MaxShowroomSMSPhoneNumber' );
END


