USE [IMT]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
/****** Object:  Table [Distribution].[ADP_ErrorType]    Script Date: 02/04/2014 22:55:43 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Distribution].[ADP_ErrorType]') AND type in (N'U'))
BEGIN
/****** Object:  Table [Distribution].[ADP_ErrorType]    Script Date: 02/04/2014 22:55:48 ******/


CREATE TABLE [Distribution].[ADP_ErrorType](
	[ErrorTypeID] [int] NOT NULL,
	[Description] [varchar](2000) NOT NULL,
 CONSTRAINT [PK_Distribution_ADP_ErrorType] PRIMARY KEY CLUSTERED 
(
	[ErrorTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]


SET ANSI_PADDING OFF

ALTER TABLE [Distribution].[ADP_ErrorType]  WITH CHECK ADD  CONSTRAINT [CK_Distribution_ADP_ErrorType__Description] CHECK  ((len([Description])>(0)))

ALTER TABLE [Distribution].[ADP_ErrorType] CHECK CONSTRAINT [CK_Distribution_ADP_ErrorType__Description]

END
GO