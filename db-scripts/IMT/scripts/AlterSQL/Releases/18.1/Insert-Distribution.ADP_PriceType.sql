IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_PriceType] where PriceTypeID=0)
BEGIN

INSERT [IMT].[Distribution].[ADP_PriceType] ([PriceTypeID], [Description]) VALUES (0, N'RETAIL')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_PriceType] where PriceTypeID=1)
BEGIN

INSERT [Distribution].[ADP_PriceType] ([PriceTypeID], [Description]) VALUES (1, N'INVOICE')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_PriceType] where PriceTypeID=2)
BEGIN

INSERT [Distribution].[ADP_PriceType] ([PriceTypeID], [Description]) VALUES (2, N'SELLING PRICE')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_PriceType] where PriceTypeID=3)
BEGIN

INSERT [Distribution].[ADP_PriceType] ([PriceTypeID], [Description]) VALUES (3, N'BASE INVOICE')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_PriceType] where PriceTypeID=4)
BEGIN

INSERT [Distribution].[ADP_PriceType] ([PriceTypeID], [Description]) VALUES (4, N'BASE RETAIL')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_PriceType] where PriceTypeID=5)
BEGIN

INSERT [Distribution].[ADP_PriceType] ([PriceTypeID], [Description]) VALUES (5, N'COMMISSION PRICE')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_PriceType] where PriceTypeID=6)
BEGIN

INSERT [Distribution].[ADP_PriceType] ([PriceTypeID], [Description]) VALUES (6, N'DRAFT AMOUNT')

END

