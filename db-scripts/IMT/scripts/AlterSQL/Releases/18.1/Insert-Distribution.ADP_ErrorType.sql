IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=0)
BEGIN

insert into distribution.ADP_ErrorType(ErrorTypeID,Description) values(0,'Undefined') 

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1)
BEGIN

INSERT [IMT].[Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1, N'Could not open FullFileName file.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=2)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (2, N'Cannot read PRIVLIB GLJRNL.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=14)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (14, N'ParameterName, ParameterValue, is not in the FileName file.')

END


IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=18)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (18, N'The number of Option Descriptions entries (nn) exceeds the number of Options (n)."')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=500)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (500, N'General error. Usually means that a file cannot be opened, or an important item cannot be read, etc. Generally, this error is unrecoverable.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1000)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1000, N'Customer, CustNo, not on file.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1003)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1003, N'Invalid InventoryCompany. Company number entered must be valid for the Accounting application logon (-A).')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1004)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1004, N'Invalid SaleCompany. Company number entered must be valid for the Accounting application logon (-A).')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1006)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1006, N'InventoryCompany is missing. This is a required field.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1007)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1007, N'SaleCompany is missing. This field is required when a SaleAcct is specified.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1008)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1008, N'Invalid InventoryAcct.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1009)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1009, N'Invalid SaleAcct.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1011)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1011, N'All errors generated when updating the VEHICLES file are returned with this error number. Error text describes the specific problem.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1012)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1012, N'InventoryAcct is missing. InventoryAcct is a required field.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1013)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1013, N'StockNo is required input. Assign a value to StockNo.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1014)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1014, N'StockNo must be 17 or less, upper case, alphanumeric characters.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1015)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1015, N'StockNo cannot have leading zeros.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1017)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1017, N'StockNo is not in CAR-INV.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1019)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1019, N'Invalid Stock Type. Stock Type is a required field.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1020)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1020, N'StockType not set up on DMS. Use MENU AUN, Option P, to set up the stock type.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1027)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1027, N'Invalid Vehid. Vehid (vehicle ID) must be 25 characters or less.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1028)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1028, N'Invalid Vehid. Vehid (vehicle ID) contains non-dash characters.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1029)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1029, N'Invalid Vehid. Vehid (vehicle ID) contains lowercase characters.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1031)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1031, N'EntryDate is a required field.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1033)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1033, N'Make, Make, is invalid.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1034)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1034, N'Model is a required field.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1035)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1035, N'Model, Model, is invalid for Make Make.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1038)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1038, N'Model, Model, is invalid.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1042)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1042, N'Invalid vehicle year.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1043)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1043, N'Invalid vehicle year. Year cannot be more than one year into the future.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1044)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1044, N'Invalid VIN. VIN has VINLength characters, but should have 17.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1045)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1045, N'Invalid VIN. VIN year digit does not match vehicle year.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1046)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1046, N'Invalid VIN. VIN did not pass checksum test.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1047)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1047, N'Invalid VIN. VIN year digit is VINYearDigit while year digit is TwoDigitModelYear.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1048)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1048, N'Invalid VIN. VIN trailing digits are not numeric.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1049)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1049, N'Invalid VIN. VIN should have ProperLength trailing digits, but has ActualLength.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1056)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1056, N'StockNo and Vehid are not on file.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1057)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1057, N'Vehid, Vehid, is not in VEHICLES.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1058)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1058, N'Stock number, StockNo, is currently locked by port Port.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1059)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1059, N'Vehid, Vehid, is currently locked by port Port.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1061)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1061, N'Input StockNo does not match value in VEHICLES Item Vehid.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1063)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1063, N'StockNo is already in CAR-INV, no update will take place.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1064)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1064, N'VehID is already in VEHICLES, no update will take place.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1065)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1065, N'Cannot read SE-FLAG from Service PRIVLIB file.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1067)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1067, N'Input Vehid does not match value in CAR-INV Vehid.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1068)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1068, N'Vehid must be null. Vehid cannot be added to CAR-INV.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1069)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1069, N'EntryDate has invalid date format.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1070)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1070, N'SoldDate has invalid date format.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1074)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1074, N'Cannot update CAR-INV item that does not exist.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1075)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1075, N'Lease units cannot be updated with this application.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1076)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1076, N'Not allowed to change StockType.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1077)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1077, N'Not allowed to change InventoryAcct.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1078)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1078, N'Not allowed to change InventoryCompany.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1079)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1079, N'SaleCompany cannot be deleted without deleting SaleAcct.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1080)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1080, N'Not allowed to change make.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1081)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1081, N'Not allowed to change model.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1082)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1082, N'Not allowed to change VIN.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1083)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1083, N'Not allowed to change year.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1084)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1084, N'Invalid model year. VIN cannot be verified.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1085)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1085, N'Invalid model. Make input as model. VIN cannot be verified.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1086)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1086, N'Invalid make. VIN cannot be verified.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1087)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1087, N'Invalid model. VIN cannot be verified.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1091)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1091, N'An error getting the application account information. Error text for this error is passed from a General Ledger (GL) common routine.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=1092)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (1092, N'Accounting and Service accounts specified in Group are not interfaced.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=10900000)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (10900000, N'Unable to read from socket.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=11500000)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (11500000, N'Application returned an error.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=11600000)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (11600000, N'Internal error.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=11900000)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (11900000, N'Read on reply timed out. (Socket connection timeout.)')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=12000000)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (12000000, N'Unable to set sockets option.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=12100000)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (12100000, N'Unable to write to socket.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=12200000)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (12200000, N'Unable to get option.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=12300000)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (12300000, N'Pooled connection initialization failed.')

END

IF NOT EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=12400000)
BEGIN

INSERT [Distribution].[ADP_ErrorType] ([ErrorTypeID], [Description]) VALUES (12400000, N'Request failed.')

END
