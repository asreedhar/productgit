USE [IMT]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

/****** Object:  Table [Distribution].[ADP_PriceType]    Script Date: 01/24/2014 17:26:41 ******/
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Distribution].[ADP_PriceType]') AND type in (N'U'))
BEGIN
/****** Object:  Table [Distribution].[ADP_PriceType]    Script Date: 01/24/2014 17:26:46 ******/


CREATE TABLE [Distribution].[ADP_PriceType](
	[PriceTypeID] [int] NOT NULL,
	[Description] [varchar](500) NOT NULL,
 CONSTRAINT [PK_Distribution.ADP_PriceType] PRIMARY KEY CLUSTERED 
(
	[PriceTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]


END

GO

SET ANSI_PADDING OFF
GO