USE [IMT]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Distribution].[FK_Distribution.ADP_DealerPriceMapping_BusinessUnit]') AND parent_object_id = OBJECT_ID(N'[Distribution].[ADP_DealerPriceMapping]'))
ALTER TABLE [Distribution].[ADP_DealerPriceMapping] DROP CONSTRAINT [FK_Distribution.ADP_DealerPriceMapping_BusinessUnit]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Distribution].[FK_Distribution.ADP_DealerPriceMapping_Distribution.ADP_PriceType]') AND parent_object_id = OBJECT_ID(N'[Distribution].[ADP_DealerPriceMapping]'))
ALTER TABLE [Distribution].[ADP_DealerPriceMapping] DROP CONSTRAINT [FK_Distribution.ADP_DealerPriceMapping_Distribution.ADP_PriceType]
GO

USE [IMT]
GO

/****** Object:  Table [Distribution].[ADP_DealerPriceMapping]    Script Date: 01/27/2014 22:14:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Distribution].[ADP_DealerPriceMapping]') AND type in (N'U'))
DROP TABLE [Distribution].[ADP_DealerPriceMapping]
GO

USE [IMT]
GO

/****** Object:  Table [Distribution].[ADP_DealerPriceMapping]    Script Date: 01/27/2014 22:14:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Distribution].[ADP_DealerPriceMapping](
	[DealerID] [int] NOT NULL,
	[PriceTypeID] [int] NOT NULL,
 CONSTRAINT [PK_Distribution.ADP_DealerPriceMapping] PRIMARY KEY CLUSTERED 
(
	[DealerID] ASC,
	[PriceTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]

GO

ALTER TABLE [Distribution].[ADP_DealerPriceMapping]  WITH CHECK ADD  CONSTRAINT [FK_Distribution.ADP_DealerPriceMapping_BusinessUnit] FOREIGN KEY([DealerID])
REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO

ALTER TABLE [Distribution].[ADP_DealerPriceMapping] CHECK CONSTRAINT [FK_Distribution.ADP_DealerPriceMapping_BusinessUnit]
GO

ALTER TABLE [Distribution].[ADP_DealerPriceMapping]  WITH CHECK ADD  CONSTRAINT [FK_Distribution.ADP_DealerPriceMapping_Distribution.ADP_PriceType] FOREIGN KEY([PriceTypeID])
REFERENCES [Distribution].[ADP_PriceType] ([PriceTypeID])
GO

ALTER TABLE [Distribution].[ADP_DealerPriceMapping] CHECK CONSTRAINT [FK_Distribution.ADP_DealerPriceMapping_Distribution.ADP_PriceType]
GO

