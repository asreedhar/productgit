IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_InventoryCertificationNumber_GM]') AND parent_object_id = OBJECT_ID(N'[dbo].[Inventory_CertificationNumber]'))

ALTER TABLE [dbo].[Inventory_CertificationNumber] DROP CONSTRAINT [CK_InventoryCertificationNumber_GM]

GO

ALTER TABLE [dbo].[Inventory_CertificationNumber]  WITH CHECK ADD  CONSTRAINT [CK_InventoryCertificationNumber_GM] CHECK  (([ManufacturerID]!=(6) OR ([CertificationNumber] = '' OR [CertificationNumber] like replicate('[0-9]',(6)) OR [CertificationNumber] like replicate('[0-9]',(7)) OR [CertificationNumber] like replicate('[0-9]',(8)))))

GO