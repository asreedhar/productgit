INSERT
INTO	Extract.DealerConfigurationFlag (DestinationName, Name, BitPosition, Description)
SELECT	'OVE-Hendrick', 'AULTecPhotoImport', 2, 'Flags if we should import the AULtec photos for this dealer for export to OVE-Hendrick'


UPDATE	DC
SET	Flags = CAST(Flags | POWER(2, 2) AS BINARY(2))	-- both at bit position zero

FROM	Extract.DealerConfiguration DC
	INNER JOIN IMT.dbo.BusinessUnit BU ON DC.BusinessUnitID = BU.BusinessUnitID
WHERE   DC.DestinationName = 'OVE-Hendrick'
	AND BusinessUnit IN ( 'Rick Hendrick Chevrolet-Duluth', 'Hendrickcarshickory.com', 'Performance Automall - Hendrick', 'Rick Hendrick BMW Charleston',
                              'Rick Hendrick Chevrolet-Charleston', 'Hendrick Lexus', 'Hendrick Chevrolet Shawnee Mission', 'East Bay BMW',
                              'Honda Cars of McKinney', 'Hendrick Honda Hickory', 'Rick Hendrick Chevrolet Buford', 'Dale Earnhardt Jr Chevrolet',
                              'Volvo of Charleston', 'Lexus of Charleston', 'BMW of Murrieta', 'Lexus NorthLake', 'Dale Earnhardt Jr Buick GMC Cadillac',
                              'Mall of Georgia Mazda', 'Rick Hendrick Buick GMC-Duluth', 'Rick Hendrick Dodge Project 5', 'Hendrick Chevrolet AL' )
