CREATE TABLE [dbo].[AppraisalPhotoKeys](
	[PhotoID] [int] IDENTITY(1,1)NOT NULL,
	[AppraisalID] [int] NULL,
	[ThumbKey] [varchar](100) NULL,
	[OriginalKey] [varchar](100) NULL,
	PRIMARY KEY ([PhotoID])
)