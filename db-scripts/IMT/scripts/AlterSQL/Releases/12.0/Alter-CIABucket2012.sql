
--------------------------------------------------------------------------------
--	ORIGINALLY PLANNED TO BE DONE IN AN ADMIN TOOL, THIS NEEDS TO BE DONE
--	EVERY YEAR AS LONG AS THE CIA BUCKETRON IS DRIVEN COMPLETELY FROM DATA
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- 	ADD 2012
--------------------------------------------------------------------------------

UPDATE	CIABuckets
SET	RANK = RANK + 1
WHERE	CIABucketGroupID = 1

INSERT
INTO	CIABuckets (CIABucketGroupID, Rank, Description)
SELECT	1, 1, '2012'

INSERT
INTO	CIABucketRules (CIABucketID, CIABucketRuleTypeID, Value, Value2)
SELECT	25, 1, 2012, NULL

--------------------------------------------------------------------------------
-- 	DROP 2004
--------------------------------------------------------------------------------

DELETE 
FROM	CIABucketRules
WHERE	CIABucketID = 3

DELETE 
FROM	CIABuckets
WHERE	CIABucketID = 3
