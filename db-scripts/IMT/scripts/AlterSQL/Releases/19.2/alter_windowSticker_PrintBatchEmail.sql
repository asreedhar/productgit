IF NOT EXISTS(SELECT * FROM sys.columns 
        WHERE [name] = N'MessageId' AND [object_id] = OBJECT_ID(N'windowSticker.PrintBatchEmail'))
BEGIN
	ALTER TABLE windowSticker.PrintBatchEmail
		ADD MessageId varchar(80) null
END
GO

IF EXISTS (SELECT * 
				FROM sys.indexes 
				WHERE name='IX_PrintBatchEmail_MessageId' AND object_id = OBJECT_ID('windowSticker.PrintBatchEmail'))
BEGIN
	DROP INDEX windowSticker.PrintBatchEmail.IX_PrintBatchEmail_MessageId
END
GO

CREATE INDEX IX_PrintBatchEmail_MessageId ON windowSticker.PrintBatchEmail(MessageId)
GO

