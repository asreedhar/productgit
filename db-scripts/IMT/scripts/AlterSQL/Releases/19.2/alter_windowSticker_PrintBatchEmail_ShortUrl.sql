IF NOT EXISTS(SELECT * FROM sys.columns 
        WHERE [name] = N'ShortUrl' AND [object_id] = OBJECT_ID(N'windowSticker.PrintBatchEmail'))
BEGIN
	ALTER TABLE windowSticker.PrintBatchEmail
		ADD ShortUrl varchar(50) null
END
GO

