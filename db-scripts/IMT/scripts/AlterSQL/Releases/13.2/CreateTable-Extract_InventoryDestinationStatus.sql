CREATE TABLE Extract.InventoryDestinationStatus (
        DestinationName VARCHAR(100) NOT NULL,
        InventoryID     INT NOT NULL,
        Export          BIT NOT NULL,
        CONSTRAINT PK_Extract_InventoryDestinationStatus
                PRIMARY KEY CLUSTERED (
                        InventoryID,
                        DestinationName
                )
                ON [IDX],
        CONSTRAINT FK_Extract_InventoryDestinationStatus__Inventory
                FOREIGN KEY (
                        InventoryID
                )
                REFERENCES dbo.Inventory (
                        InventoryID
                ),
        CONSTRAINT FK_Extract_InventoryDestinationStatus__Destination
                FOREIGN KEY (
                        DestinationName
                )
                REFERENCES Extract.Destination (
                        [Name]
                )
)
ON [DATA]
GO

EXEC dbo.sp_SetTableDescription 'Extract.InventoryDestinationStatus',
	'Track inventory export status to supported destinations in the Extract.Destination table.'

EXEC dbo.sp_SetColumnDescription 'Extract.InventoryDestinationStatus.DestinationName',
	'Specifies the DestinationName (via FK to Extract.Destination#Name).'

EXEC dbo.sp_SetColumnDescription 'Extract.InventoryDestinationStatus.InventoryID',
	'Specifies the InventoryID (via FK to IMT.dbo.Inventory).'

EXEC dbo.sp_SetColumnDescription 'Extract.InventoryDestinationStatus.Export',
	'If set, indicates this inventory item has been marked for export to the specified DestinationName.'

GO