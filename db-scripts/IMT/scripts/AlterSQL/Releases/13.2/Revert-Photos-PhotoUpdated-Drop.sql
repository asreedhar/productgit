--------------------------------------------------------------------------------
--	Reinstate the PhotoUpdated column, as it is used by
--	the Legacy Photo Manager for Appraisals et al.
--
--	Using SMALLDATETIME as there isn't a need to store 
--	detail down to the millisecond
--------------------------------------------------------------------------------

ALTER TABLE dbo.Photos ADD PhotoUpdated SMALLDATETIME NULL

