update DP 
set GalvesMobileOptInFlag=1 , GalvesOptInDate = getdate()
from IMT.dbo.DealerPreference DP join IMT.dbo.BusinessUnit BU on DP.BusinessUnitID=BU.BusinessUnitID
where       GoLiveDate is not null 
            and (GuideBookID=4 or GuideBook2Id=4)
            and BU.Active=1 
            and BU.BusinessUnitTypeID=4;
