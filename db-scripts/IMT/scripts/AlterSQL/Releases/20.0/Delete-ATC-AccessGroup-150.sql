WITH DM AS (

SELECT	MGL.BusinessUnitID, MGL.MemberID, 	
	Has150		= SIGN(SUM(CASE WHEN MG.AccessGroupID = 150 THEN 1 ELSE 0 END)),
	Has0penLane		= SIGN(SUM(CASE WHEN MG.AccessGroupID BETWEEN 0 AND 51 THEN 1 ELSE 0 END))
FROM	dbo.tbl_MemberATCAccessGroupList MGL
	INNER JOIN IMT.dbo.tbl_MemberATCAccessGroup MG ON MGL.MemberATCAccessGroupListID = MG.MemberATCAccessGroupListID
	INNER JOIN IMT.dbo.BusinessUnit BU ON MGL.BusinessUnitID = BU.BusinessUnitID AND Active = 1
GROUP
BY	MGL.BusinessUnitID, MGL.MemberID 	
)
UPDATE	MG
SET	AccessGroupID = 0
FROM	DM
	INNER JOIN dbo.tbl_MemberATCAccessGroupList MGL ON DM.BusinessUnitID = MGL.BusinessUnitID AND DM.MemberID = MGL.MemberID
	INNER JOIN IMT.dbo.tbl_MemberATCAccessGroup MG ON MGL.MemberATCAccessGroupListID = MG.MemberATCAccessGroupListID
WHERE	Has150 = 1
	AND Has0penLane = 0
	AND MG.AccessGroupID = 150

; WITH D AS (

SELECT	DAG.BusinessUnitID,
	Has150		= SIGN(SUM(CASE WHEN DAG.AccessGroupID = 150 THEN 1 ELSE 0 END)),
	Has0penLane	= SIGN(SUM(CASE WHEN DAG.AccessGroupID BETWEEN 0 AND 51 THEN 1 ELSE 0 END))
FROM	tbl_DealerATCAccessGroups DAG
	INNER JOIN IMT.dbo.BusinessUnit BU ON DAG.BusinessUnitID = BU.BusinessUnitID AND Active = 1
GROUP
BY	DAG.BusinessUnitID
)
UPDATE	DAG
SET	AccessGroupId = 0
FROM	D
	INNER JOIN dbo.tbl_DealerATCAccessGroups DAG ON D.BusinessUnitID = DAG.BusinessUnitID 
WHERE	Has150 = 1
	AND Has0penLane = 0
	AND DAG.AccessGroupID = 150

DELETE	MGL
FROM	dbo.tbl_MemberATCAccessGroupList MGL
	INNER JOIN IMT.dbo.tbl_MemberATCAccessGroup MG ON MGL.MemberATCAccessGroupListID = MG.MemberATCAccessGroupListID
WHERE	AccessGroupId = 150

DELETE	MG
FROM	dbo.tbl_MemberATCAccessGroup MG
WHERE	AccessGroupId = 150


DELETE	DG
FROM	dbo.tbl_DealerATCAccessGroups DG
WHERE	AccessGroupId = 150
