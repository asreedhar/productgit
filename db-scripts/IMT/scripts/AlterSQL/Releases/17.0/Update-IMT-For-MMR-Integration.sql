
/** Create Script for MMR **/
 
 
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'MMR')
EXEC('CREATE SCHEMA MMR AUTHORIZATION dbo')
GO

CREATE TABLE [MMR].[Vehicle](
	[MMRVehicleID] [int] IDENTITY(1,1) NOT NULL,
	[MID] [varchar](15) NOT NULL, 
	[RegionCode] [varchar](20) NOT NULL,
 CONSTRAINT [PK_MMRVehicle] PRIMARY KEY CLUSTERED 
(
	[MMRVehicleID] ASC
)
)
GO


ALTER TABLE [dbo].[Appraisals]
ADD MMRVehicleID int NULL
GO

ALTER TABLE [dbo].Appraisals  WITH CHECK ADD  CONSTRAINT [FK_Appraisals_MMRVehicle] FOREIGN KEY([MMRVehicleID])
REFERENCES [MMR].[Vehicle] ([MMRVehicleID])
GO
