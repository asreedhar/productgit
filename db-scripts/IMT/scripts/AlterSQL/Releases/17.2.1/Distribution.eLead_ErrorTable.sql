USE [IMT]
GO

/****** Object:  Table [Distribution].[eLead_Error]    Script Date: 12/11/2013 16:50:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Distribution].[FK_Distribution_eLeadError__eLead_ErrorType]') AND parent_object_id = OBJECT_ID(N'[Distribution].[eLead_Error]'))
ALTER TABLE [Distribution].[eLead_Error] DROP CONSTRAINT [FK_Distribution_eLeadError__eLead_ErrorType]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Distribution].[FK_Distribution_eLeadError__SubmissionError]') AND parent_object_id = OBJECT_ID(N'[Distribution].[eLead_Error]'))
ALTER TABLE [Distribution].[eLead_Error] DROP CONSTRAINT [FK_Distribution_eLeadError__SubmissionError]
GO

IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'Distribution' 
                 AND  TABLE_NAME = 'eLead_Error'))
BEGIN
   DROP TABLE Distribution.eLead_Error
END


CREATE TABLE [Distribution].[eLead_Error](
	[ErrorID] [int] NOT NULL,
	[ErrorTypeID] [int] NOT NULL,
	[ErrorMessage] [varchar](2000) NULL,
 CONSTRAINT [PK_Distribution_eLeadError] PRIMARY KEY CLUSTERED 
(
	[ErrorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [Distribution].[eLead_Error]  WITH CHECK ADD  CONSTRAINT [FK_Distribution_eLeadError__eLead_ErrorType] FOREIGN KEY([ErrorTypeID])
REFERENCES [Distribution].[eLead_ErrorType] ([ErrorTypeID])
GO

ALTER TABLE [Distribution].[eLead_Error] CHECK CONSTRAINT [FK_Distribution_eLeadError__eLead_ErrorType]
GO

ALTER TABLE [Distribution].[eLead_Error]  WITH CHECK ADD  CONSTRAINT [FK_Distribution_eLeadError__SubmissionError] FOREIGN KEY([ErrorID])
REFERENCES [Distribution].[SubmissionError] ([ErrorID])
GO

ALTER TABLE [Distribution].[eLead_Error] CHECK CONSTRAINT [FK_Distribution_eLeadError__SubmissionError]
GO