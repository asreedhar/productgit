USE [IMT]
GO

/****** Object:  Table [Distribution].[eLead_ErrorType]    Script Date: 12/11/2013 16:51:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[Distribution].[CK_Distribution_eLead_ErrorType__Description]') AND parent_object_id = OBJECT_ID(N'[Distribution].[eLead_ErrorType]'))
ALTER TABLE [Distribution].[eLead_ErrorType] DROP CONSTRAINT [CK_Distribution_eLead_ErrorType__Description]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Distribution].[FK_Distribution_eLeadError__eLead_ErrorType]') AND parent_object_id = OBJECT_ID(N'[Distribution].[eLead_Error]'))
ALTER TABLE [Distribution].[eLead_Error] DROP CONSTRAINT [FK_Distribution_eLeadError__eLead_ErrorType]
GO

IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'Distribution' 
                 AND  TABLE_NAME = 'eLead_ErrorType'))
BEGIN
   DROP TABLE Distribution.eLead_ErrorType
END


CREATE TABLE [Distribution].[eLead_ErrorType](
	[ErrorTypeID] [int] IDENTITY(0,1) NOT NULL,
	[Description] [varchar](2000) NOT NULL,
 CONSTRAINT [PK_Distribution_eLead_ErrorType] PRIMARY KEY CLUSTERED 
(
	[ErrorTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [Distribution].[eLead_ErrorType]  WITH CHECK ADD CONSTRAINT [CK_Distribution_eLead_ErrorType__Description] CHECK  ((len([Description])>(0)))
GO

ALTER TABLE [Distribution].[eLead_ErrorType] CHECK CONSTRAINT [CK_Distribution_eLead_ErrorType__Description]
GO

IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'Distribution' 
                 AND  TABLE_NAME = 'eLead_Error'))
	ALTER TABLE [Distribution].[eLead_Error]  WITH CHECK ADD  CONSTRAINT [FK_Distribution_eLeadError__eLead_ErrorType] FOREIGN KEY([ErrorTypeID])
	REFERENCES [Distribution].[eLead_ErrorType] ([ErrorTypeID])
	GO
IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'Distribution' 
                 AND  TABLE_NAME = 'eLead_Error'))
	ALTER TABLE [Distribution].[eLead_Error] CHECK CONSTRAINT [FK_Distribution_eLeadError__eLead_ErrorType]
	GO
