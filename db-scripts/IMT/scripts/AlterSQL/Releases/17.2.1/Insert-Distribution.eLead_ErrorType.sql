IF NOT EXISTS(SELECT * FROM [IMT].[Distribution].[eLead_ErrorType] where ErrorTypeID = 0)
BEGIN
	INSERT INTO [IMT].[Distribution].[eLead_ErrorType]
           ([Description])
     	VALUES
           ('vehicle not found')
END


IF NOT EXISTS(SELECT * FROM [IMT].[Distribution].[eLead_ErrorType] where ErrorTypeID = 1)
BEGIN
	INSERT INTO [IMT].[Distribution].[eLead_ErrorType]
           ([Description])
	VALUES
           ('invalid dealer')
END


IF NOT EXISTS(SELECT * FROM [IMT].[Distribution].[eLead_ErrorType] where ErrorTypeID = 2)
BEGIN
	INSERT INTO [IMT].[Distribution].[eLead_ErrorType]
           ([Description])
	VALUES
           ('not authenticated')
END


IF NOT EXISTS(SELECT * FROM [IMT].[Distribution].[eLead_ErrorType] where ErrorTypeID = 3)
BEGIN
	INSERT INTO [IMT].[Distribution].[eLead_ErrorType]
           ([Description])
        VALUES
           ('invalid appraisal value')
END


IF NOT EXISTS(SELECT * FROM [IMT].[Distribution].[eLead_ErrorType] where ErrorTypeID = 4)
BEGIN
	INSERT INTO [IMT].[Distribution].[eLead_ErrorType]
           ([Description])
	VALUES
           ('malformed json')
END


IF NOT EXISTS(SELECT * FROM [IMT].[Distribution].[eLead_ErrorType] where ErrorTypeID = 5)
BEGIN
	INSERT INTO [IMT].[Distribution].[eLead_ErrorType]
           ([Description])
	VALUES
           ('internal server error')

END


IF NOT EXISTS(SELECT * FROM [IMT].[Distribution].[eLead_ErrorType] where ErrorTypeID = 6)
BEGIN
	INSERT INTO [IMT].[Distribution].[eLead_ErrorType]
           ([Description])
	VALUES
           ('Vin is required')

END


IF NOT EXISTS(SELECT * FROM [IMT].[Distribution].[eLead_ErrorType] where ErrorTypeID = 7)
BEGIN
	INSERT INTO [IMT].[Distribution].[eLead_ErrorType]
           ([Description])
	VALUES
           ('DealerId is required')

END


IF NOT EXISTS(SELECT * FROM [IMT].[Distribution].[eLead_ErrorType] where ErrorTypeID = 8)
BEGIN
	INSERT INTO [IMT].[Distribution].[eLead_ErrorType]
           ([Description])
	VALUES
           ('AppraisalValue is required')
END


IF NOT EXISTS(SELECT * FROM [IMT].[Distribution].[eLead_ErrorType] where ErrorTypeID = 9)
BEGIN
	INSERT INTO [IMT].[Distribution].[eLead_ErrorType]
           ([Description])
	VALUES
           ('Unrecognized')
END

