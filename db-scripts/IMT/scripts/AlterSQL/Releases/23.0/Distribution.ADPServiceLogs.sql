USE [IMT]
GO

/****** Object:  Table [Distribution].[ADPServiceLogs]    Script Date: 04/14/2015 23:09:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Distribution].[ADPServiceLogs](
	[EventId] [int] IDENTITY(1,1) NOT NULL,
	[DealerId] [int] NOT NULL,
	[StockNumber] [varchar](15) NOT NULL,
	[XmlRequest] [nvarchar](max) NULL,
	[XmlResponse] [nvarchar](max) NULL,
	[SubmissionId] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_Distribution.ADPServiceLogs] PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]

GO

SET ANSI_PADDING OFF
GO


