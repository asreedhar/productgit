DECLARE @ProviderID INT = 8

SET IDENTITY_INSERT [Distribution].[Provider] ON

INSERT  INTO [Distribution].[Provider]
        ( [ProviderID], [Name] )
VALUES  ( @ProviderID, 'HomeNet' )

SET IDENTITY_INSERT [Distribution].[Provider] OFF

INSERT  INTO [IMT].[Distribution].[ProviderMessageType]
        ( [ProviderID], [MessageTypes] )
VALUES  ( @ProviderID, 1 ),
        ( @ProviderID, 2 ),
        ( @ProviderID, 3 )