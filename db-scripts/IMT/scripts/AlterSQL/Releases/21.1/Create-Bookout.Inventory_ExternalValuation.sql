IF OBJECT_ID('Bookout.Inventory_ExternalValuation') IS NOT NULL DROP TABLE Bookout.Inventory_ExternalValuation
GO
CREATE TABLE Bookout.Inventory_ExternalValuation (
	InventoryID		INT NOT NULL,
	ThirdPartyID		TINYINT NOT NULL,
	VehicleCode		VARCHAR(20) NOT NULL,
	EquipmentCodes		VARCHAR(500) NOT NULL,
	CPOCode			VARCHAR(20) NOT NULL,
	InputHash		BINARY(20) NOT NULL,
	LoadID			INT NOT NULL,
	CONSTRAINT PK_BookoutInventory_ExternalValuation PRIMARY KEY CLUSTERED (InventoryID, ThirdPartyID),
	CONSTRAINT FK_BookoutInventory_ExternalValuation__Inventory FOREIGN KEY (InventoryID) REFERENCES dbo.Inventory(InventoryID),
	CONSTRAINT FK_BookoutInventory_ExternalValuation__ThirdParties FOREIGN KEY (ThirdPartyID) REFERENCES dbo.ThirdParties(ThirdPartyID)
	)
	
GO
EXEC sp_SetTableDescription 'Bookout.Inventory_ExternalValuation', 'For the bookout processor, this table stores vehicle valuation input data from external sources for inventory.'
GO
EXEC sp_SetColumnDescription 'Bookout.Inventory_ExternalValuation.InventoryID', 'IMS Inventory ID'
GO
EXEC sp_SetColumnDescription 'Bookout.Inventory_ExternalValuation.ThirdPartyID', 'IMS "ThirdPartyID" use to identify the valuation provider (e.g. KBB, Blackbook, etc.)'
GO
EXEC sp_SetColumnDescription 'Bookout.Inventory_ExternalValuation.VehicleCode', 'Valuation provider (ThirdPartyID) vehicle code'
GO
EXEC sp_SetColumnDescription 'Bookout.Inventory_ExternalValuation.EquipmentCodes', 'Valuation provider (ThirdPartyID) equipment codes'
GO
EXEC sp_SetColumnDescription 'Bookout.Inventory_ExternalValuation.CPOCode', 'CPO codes for the vehicle.  Not currently loaded.'
GO
EXEC sp_SetColumnDescription 'Bookout.Inventory_ExternalValuation.InputHash', 'Input hash (SHA1).  Used for change-detection'
GO
EXEC sp_SetColumnDescription 'Bookout.Inventory_ExternalValuation.LoadID', 'Source file load id.  FK to DBASTAT.dbo.Dataload_History'
