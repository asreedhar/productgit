ALTER TABLE IMT.Certified.CertifiedProgram_Make ADD ExternalID VARCHAR(100) NULL
GO

INSERT IMT.Certified.CertifiedProgram
        ( Name,Text,Active,InsertUser,InsertDate,UpdateUser,UpdateDate,Icon,BusinessUnitId,ExternalName)
VALUES  ( 'MINI NEXT 575', 'MINI NEXT 575',1,0,GETDATE(),0,GETDATE(), 'mini.jpg',NULL, NULL)
UPDATE IMT.Certified.CertifiedProgram
	SET NAme='MINI NEXT CERTIFIED PRE-OWNED',Text='MINI NEXT CERTIFIED PRE-OWNED' WHERE CertifiedProgramID=44


INSERT IMT.Certified.CertifiedProgram_Make (MakeID,CertifiedProgramID)
SELECT Makeid=2, CertifiedProgramID
FROM IMT.Certified.CertifiedProgram cp WHERE cp.Name='MINI NEXT 575'

UPDATE 	cpm
	SET ExternalID = CASE WHEN m.MakeId=2 THEN
			CASE WHEN NAME LIKE '%575%' THEN '0075000' ELSE '0100000' END
		WHEN m.MakeId=5 THEN
			CASE when name like '%Elite%' THEN '0075000' ELSE '0100000' END      
		
		END          
FROM IMT.Certified.CertifiedProgram_Make cpm
INNER JOIN IMT.Certified.CertifiedProgram cp ON cp.CertifiedProgramID=cpm.CertifiedProgramID --AND Active=1 --AND cp.ProgramType=1
INNER JOIN [VehicleCatalog].Firstlook.Make m ON m.MakeID=cpm.MakeID
WHERE m.Make IN ('BMW','MINI')