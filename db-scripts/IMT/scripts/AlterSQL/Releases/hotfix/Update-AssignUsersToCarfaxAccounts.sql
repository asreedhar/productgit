

/* This script assigns all members to have permissions in Carfax
that have an Carfax account.*/

DECLARE @InsertUser VARCHAR(80)
SET @InsertUser ='dadams'


DECLARE @MemberID INT
DECLARE @AccountID INT
DECLARE @CarfaxAccountUsers TABLE
	(idx INT IDENTITY(1,1),
	AccountID INT,
	MemberID iNT)

INSERT
INTO	@CarfaxAccountUsers
	(AccountID,
	MemberID)
SELECT	AC.AccountID,
	MA.MemberID
FROM	Carfax.Account AC
JOIN	Carfax.Account_Dealer AD
ON	AC.AccountID =AD.AccountID
JOIN	dbo.BusinessUnit BU
ON	AD.DealerID =BU.BusinessUnitID
JOIN	dbo.MemberAccess MA
ON	MA.BusinessUnitID =BU.BusinessUnitID
JOIN	(	SELECT AD.DealerID
	FROM	Carfax.Account A
	LEFT
	JOIN	Carfax.Account_Member AM
	ON	AM.AccountID =A.AccountID
	JOIN	Carfax.Account_Dealer AD
	ON	AD.AccountID =A.AccountID 
	GROUP BY AD.DealerID
	HAVING COUNT(AM.MemberID)=0
 ) X
ON	AD.DealerID =X.DealerID

DECLARE @i INT
DECLARE @c INT
SELECT @c = COUNT(*) FROM @CarfaxAccountUsers
SET @i =1

/* this step ensures that all the Members of the  BusinessUnits are associated
with the correct Carfax.Account.*/
WHILE (@i <= @c)
BEGIN
	SELECT	@MemberID =MemberID,
		@AccountID =AccountID	
	FROM	@CarfaxAccountUsers
	WHERE	idx =@i

	IF NOT EXISTS (Select	1
			FROM Carfax.Account_Member
			WHERE	AccountID=@AccountID
			AND	MemberID =@MemberID)
	EXEC Carfax.Account#AddAssignment @AccountID, @MemberID,@InsertUser

	SET @i =@i+1
END


GO
