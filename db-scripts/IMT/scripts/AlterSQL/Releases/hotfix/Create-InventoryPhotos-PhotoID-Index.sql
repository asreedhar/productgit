IF NOT EXISTS (SELECT 1 FROM sys.indexes I WHERE object_id = OBJECT_ID('dbo.InventoryPhotos') AND name = 'IX_InventoryPhotos__PhotoID')
	CREATE NONCLUSTERED INDEX [IX_InventoryPhotos__PhotoID] ON [dbo].[InventoryPhotos] ([PhotoID]) ON IDX
GO
