

/* This script assigns all members to have permissions in AutoCheck
that have an AutoCheck account.*/

DECLARE @InsertUser VARCHAR(80)
SET @InsertUser ='dadams'


DECLARE @MemberID INT
DECLARE @AccountID INT
DECLARE @AutoCheckAccountUsers TABLE
	(idx INT IDENTITY(1,1),
	AccountID INT,
	MemberID iNT)

INSERT
INTO	@AutoCheckAccountUsers
	(AccountID,
	MemberID)
SELECT	AC.AccountID,
	MA.MemberID
FROM	AutoCheck.Account AC
JOIN	AutoCheck.Account_Dealer AD
ON	AC.AccountID =AD.AccountID
JOIN	dbo.BusinessUnit BU
ON	AD.DealerID =BU.BusinessUnitID
JOIN	dbo.MemberAccess MA
ON	MA.BusinessUnitID =BU.BusinessUnitID
JOIN	(	SELECT AD.DealerID
	FROM	AutoCheck.Account A
	LEFT
	JOIN	AutoCheck.Account_Member AM
	ON	AM.AccountID =A.AccountID
	JOIN	AutoCheck.Account_Dealer AD
	ON	AD.AccountID =A.AccountID 
	GROUP BY AD.DealerID
	HAVING COUNT(AM.MemberID)=0
 ) X
ON	AD.DealerID =X.DealerID

DECLARE @i INT
DECLARE @c INT
SELECT @c = COUNT(*) FROM @AutoCheckAccountUsers
SET @i =1

/* this step ensures that all the Members of the Lithia BusinessUnits are associated
with the correct AutoCheck.Account.*/
WHILE (@i <= @c)
BEGIN
	SELECT	@MemberID =MemberID,
		@AccountID =AccountID	
	FROM	@AutoCheckAccountUsers
	WHERE	idx =@i

	IF NOT EXISTS (Select	1
			FROM AutoCheck.Account_Member
			WHERE	AccountID=@AccountID
			AND	MemberID =@MemberID)
	EXEC AutoCheck.Account#AddAssignment @AccountID, @MemberID,@InsertUser

	SET @i =@i+1
END
GO