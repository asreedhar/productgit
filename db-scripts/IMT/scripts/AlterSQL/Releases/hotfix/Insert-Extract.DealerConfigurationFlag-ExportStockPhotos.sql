--------------------------------------------------------------------------------------------------------------------
--	Until now, we've never needed any configuration for the "master" export as it's been implied that all active 
--	inventory is passed to AULtec for photo processing.  We will use the existing Extract.DealerConfiguration now
--	to just store optional configuration(s); eventually, we may use this to control the dealers exported in 
--	AULtec-Master just as we do for AULtec (AULtec-GID)
--
--	Config values will be exposed in the view Extract.DealerConfiguration#AULtecMaster
--
--------------------------------------------------------------------------------------------------------------------


INSERT
INTO	Extract.Destination (Name, DefaultIncludeHistorical)
SELECT	'AULtec-Master', 0

INSERT
INTO	Extract.DealerConfigurationFlag(DestinationName, Name, BitPosition, Description)
SELECT	'AULtec-Master', 'ExportStockPhotos', 0, 'Flags whether to export StockPhotoURL(s) from VDS for a dealer'


BEGIN TRY

	INSERT
	INTO	Extract.DealerConfiguration(BusinessUnitID, DestinationName, ExternalIdentifier, StartDate, Active)
	SELECT	106464, 'AULtec-Master', 'LOEBERMO03', DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0),1
	
	
	--------------------------------------------------------------------
	--	Enable for Loeber
	--------------------------------------------------------------------
	
	EXEC Extract.DealerConfiguration#SetFlag 
		@BusinessUnitID = 106464,
	        @DestinationName = 'AULtec-Master',
	        @FlagName = 'ExportStockPhotos',
	        @Value = 1
	
	--------------------------------------------------------------------
	--	Enable for Kelly Volkswagen
	--------------------------------------------------------------------
	
	INSERT
	INTO	Extract.DealerConfiguration(BusinessUnitID, DestinationName, ExternalIdentifier, StartDate, Active)
	SELECT	105931, 'AULtec-Master', 'KELLYVOL01', DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0),1
	
	
	EXEC Extract.DealerConfiguration#SetFlag
		@BusinessUnitID = 105931,
	        @DestinationName = 'AULtec-Master', 
	        @FlagName = 'ExportStockPhotos',
	        @Value = 1 -- bit
        
END TRY
BEGIN CATCH
	PRINT 'CATCH ERROR FOR SCRATCH BUILD'
END CATCH	        