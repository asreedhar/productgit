
DELETE SCOA
FROM dbo.SearchCandidateOptionAnnotation SCOA
JOIN dbo.SearchCandidateOption SCO ON SCO.SearchCandidateOptionID = SCOA.SearchCandidateOptionID
JOIN dbo.SearchCandidate SC ON SC.SearchCandidateID = SCO.SearchCandidateID
JOIN dbo.SearchCandidateList SCL ON SCL.SearchCandidateListID = SC.SearchCandidateListID
JOIN dbo.Member M ON M.MemberID = SCL.MemberID
WHERE M.Login IN ('keynotelongo', 'keynotecity', 'keynoteluther')

DELETE SCO
FROM dbo.SearchCandidateOption SCO
JOIN dbo.SearchCandidate SC ON SC.SearchCandidateID = SCO.SearchCandidateID
JOIN dbo.SearchCandidateList SCL ON SCL.SearchCandidateListID = SC.SearchCandidateListID
JOIN dbo.Member M ON M.MemberID = SCL.MemberID
WHERE M.Login IN ('keynotelongo', 'keynotecity', 'keynoteluther')

DELETE SCO
FROM dbo.SearchCandidateCIACategoryAnnotation SCO
JOIN dbo.SearchCandidate SC ON SC.SearchCandidateID = SCO.SearchCandidateID
JOIN dbo.SearchCandidateList SCL ON SCL.SearchCandidateListID = SC.SearchCandidateListID
JOIN dbo.Member M ON M.MemberID = SCL.MemberID
WHERE M.Login IN ('keynotelongo', 'keynotecity', 'keynoteluther')

DELETE SC
FROM dbo.SearchCandidate SC
JOIN dbo.SearchCandidateList SCL ON SCL.SearchCandidateListID = SC.SearchCandidateListID
JOIN dbo.Member M ON M.MemberID = SCL.MemberID
WHERE M.Login IN ('keynotelongo', 'keynotecity', 'keynoteluther')

DELETE SCL
FROM dbo.SearchCandidateList SCL
JOIN dbo.Member M ON M.MemberID = SCL.MemberID
WHERE M.Login IN ('keynotelongo', 'keynotecity', 'keynoteluther')
