--------------------------------------------------------------------------------
--	CREATE A NEW PHOTO PROVIDER TO HANDLE ADHOC DROPS OF FILES CONFORMING 
--	TO THE "AdHoc-PhotoURL" FEED SPEC.  NOTE THAT THIS FEED SPEC CAN BE
--	CHANGED ON A PER-FILE BASIS TO TRULY SUPPORT "AD-HOC" LOADS, BUT IT'S 
--	PROBABLY BEST TO STICK TO A COMMON FORMAT.
--------------------------------------------------------------------------------


DECLARE @PhotoProviderID INT

INSERT	
INTO	dbo.ThirdPartyEntity
        (BusinessUnitID,
         ThirdPartyEntityTypeID,
         Name
        )
VALUES  (100150, -- BusinessUnitID - int
         6, -- ThirdPartyEntityTypeID - tinyint
         'Ad-Hoc PhotoURL Files'  -- Name - varchar(50)
        )
        
SET @PhotoProviderID = SCOPE_IDENTITY()
        
INSERT
INTO	dbo.PhotoProvider_ThirdPartyEntity
        (PhotoProviderID,
         DatafeedCode,
         PhotoStorageCode
        )
VALUES  (@PhotoProviderID, -- PhotoProviderID - int
         'AdHoc-PhotoURL', -- DatafeedCode - varchar(20)
         2  -- PhotoStorageCode - tinyint
        )

--------------------------------------------------------------------------------
--	CREATE DEALER CODE-->BU MAPPING
--------------------------------------------------------------------------------

INSERT
INTO	dbo.PhotoProviderDealership
        (PhotoProviderID,
         BusinessUnitID,
         PhotoProvidersDealershipCode,
         PhotoStorageCode_Override
        )
SELECT	12296, BusinessUnitID, 'GanleyNissan', 2
FROM	dbo.BusinessUnit BU
WHERE	BusinessUnitID = 104422