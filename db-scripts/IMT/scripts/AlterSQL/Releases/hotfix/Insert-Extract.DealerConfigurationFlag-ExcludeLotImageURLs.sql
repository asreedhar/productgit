INSERT
INTO	Extract.DealerConfigurationFlag(DestinationName, Name, BitPosition, Description)
SELECT	'AULtec-Master', 'ExcludeLotImageURLs', 3, 'Flags whether to exclude the ImageURLs from Lot for a dealer'


BEGIN TRY
	--------------------------------------------------------------------
	--	Enable for Rick Hendrick BMW of Charleston 
	--------------------------------------------------------------------

	INSERT
	INTO	Extract.DealerConfiguration(BusinessUnitID, DestinationName, ExternalIdentifier, StartDate, Active)
	SELECT	100590, 'AULtec-Master', 'RICKHEND02', DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0),1
	
	
	EXEC Extract.DealerConfiguration#SetFlag @BusinessUnitID = 100590,
	        @DestinationName = 'AULtec-Master',
	        @FlagName = 'ExcludeLotImageURLs',
	        @Value = 1
	
        
END TRY
BEGIN CATCH
	PRINT 'CATCH ERROR FOR SCRATCH BUILD'
END CATCH