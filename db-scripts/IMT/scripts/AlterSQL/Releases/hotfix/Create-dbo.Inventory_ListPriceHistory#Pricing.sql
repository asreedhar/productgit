CREATE TABLE dbo.Inventory_ListPriceHistory#Pricing
(
[InventoryID] [int] NOT NULL,
[DateCreated] [DATETIME] NOT NULL CONSTRAINT DF_Inventory_ListPriceHistory#Pricing__DateCreated DEFAULT (GETDATE()),
[ListPrice] [decimal] (8, 2) NULL,
[WasListPriceLocked] [bit] NOT NULL
) ON [DATA]
GO
ALTER TABLE dbo.Inventory_ListPriceHistory#Pricing ADD CONSTRAINT [PK_Inventory_ListPriceHistory#Pricing] PRIMARY KEY CLUSTERED ([InventoryID],[DateCreated]) 

GO

