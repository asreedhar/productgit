ALTER TABLE dbo.DealerPreference_Dataload ADD eStockCardLockOnListPrice BIT CONSTRAINT DF_DealerPreference_Dataload__eStockCardLockOnListPrice DEFAULT(1)
GO

UPDATE	dbo.DealerPreference_Dataload
SET	eStockCardLockOnListPrice = CASE WHEN BusinessUnitID = 102952 THEN 0 ELSE 1 END
GO

ALTER TABLE dbo.DealerPreference_Dataload ALTER COLUMN eStockCardLockOnListPrice BIT NOT NULL 
GO

