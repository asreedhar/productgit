--------------------------------------------------------------------------------
--
--	Refactor the existing DealerPreferenceDataload into a vertical partition  
--	of DealerPreference and add the additional column "ResuseStockNumbers" 
--	as a hofix for the inventory load process to appease an unhappy  
--	dealer.  
--
--	Note we will follow up this with another alter script in V5_18_03 to
--	retire the old table and of course any other required updates.
--
--------------------------------------------------------------------------------


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DealerPreference_Dataload](
	[BusinessUnitID] [int] NOT NULL,
	[TradeorPurchaseFromSales] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_Dataload_TradeorPurchaseFromSales]  DEFAULT (0),
	[TradeorPurchaseFromSalesDaysThreshold] [smallint] NOT NULL CONSTRAINT [DF_DealerPreference_Dataload_TradeorPurchaseFromSalesDaysThreshold]  DEFAULT (0),
	[GenerateInventoryDeltas] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_Dataload_GenerateInventroyDeltas]  DEFAULT (1),
	[CalculateUnitCost] [bit] NOT NULL CONSTRAINT [DF_DealerPreference_Dataload__CalculateUnitCost]  DEFAULT (0),
	[VINDecodeCountryCodePrimary] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_Dataload__VINDecodeCountryCodePrimary]  DEFAULT ((1)),
	[VINDecodeCountryCodeSecondary] [tinyint] NOT NULL CONSTRAINT [DF_DealerPreference_Dataload__VINDecodeCountryCodeSecondary]  DEFAULT ((2)),
	[ReuseStockNumbers] [BIT] NOT NULL CONSTRAINT [DF_DealerPreference_Dataload__ReuseStockNumbers]  DEFAULT (0),
 CONSTRAINT [PK_DealerPreference_Dataload] PRIMARY KEY CLUSTERED 
(
	[BusinessUnitID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [DATA]
) ON [DATA]

GO
ALTER TABLE [dbo].[DealerPreference_Dataload]  WITH CHECK ADD  CONSTRAINT [FK_DealerPreference_Dataload__DealerPreference] FOREIGN KEY([BusinessUnitID])
REFERENCES [dbo].[DealerPreference] ([BusinessUnitID])
GO
ALTER TABLE [dbo].[DealerPreference_Dataload] CHECK CONSTRAINT [FK_DealerPreference_Dataload__DealerPreference]
GO

INSERT
INTO	DealerPreference_Dataload (BusinessUnitID, TradeorPurchaseFromSales, TradeorPurchaseFromSalesDaysThreshold, GenerateInventoryDeltas, CalculateUnitCost, VINDecodeCountryCodePrimary, VINDecodeCountryCodeSecondary, ReuseStockNumbers)
SELECT	BusinessUnitID, TradeorPurchaseFromSales, TradeorPurchaseFromSalesDaysThreshold, GenerateInventoryDeltas, CalculateUnitCost, VINDecodeCountryCodePrimary, VINDecodeCountryCodeSecondary, 0
FROM	dbo.DealerPreferenceDataload DPD

GO

UPDATE	DealerPreference_Dataload
SET	ReuseStockNumbers = 1
WHERE	BusinessUnitID=  102568 
GO