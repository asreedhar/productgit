INSERT
INTO	Extract.DealerConfigurationFlag (DestinationName, Name, BitPosition, Description)
SELECT	'AULTec', 'MiniGID', 1, 'Specifies whether the dealer inventory is included in the 6am export (a.k.a. the "Germain Hack")'

UPDATE	DC
SET	Flags = CAST(Flags | POWER(2, BitPosition) AS BINARY(2))
FROM	Extract.DealerConfiguration DC
	INNER JOIN Extract.DealerConfigurationFlag DCF ON DC.DestinationName = DCF.DestinationName AND DCF.Name = 'MiniGID'
	INNER JOIN Utility.String.Split('105258,105259,105260,105323,103706,105890,105891,105893,105894,105307',',') BU ON DC.BusinessUnitID = CAST(BU.Value AS INT)
