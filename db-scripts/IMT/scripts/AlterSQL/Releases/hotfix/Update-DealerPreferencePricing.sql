/* 09/11/2009	CGC
		Per Case 6985: Request from Steve Fitzgerald. Since we are getting 
		a lot of QuickTurn accounts now, need to turn the default package for 
		Sales Accelerator to package #1 rather than #4. */
		
UPDATE dbo.DealerPreference_Pricing SET PackageID = 1 WHERE BusinessUnitID = 100150
GO