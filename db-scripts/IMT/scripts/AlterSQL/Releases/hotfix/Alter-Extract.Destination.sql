--------------------------------------------------------------------------------
--
--	ADD THE DEFAULT "IncludeHistorical" ON THE Extract.Destination TABLE
--	FOR USE BY THE PROC Extract.DealerConfiguration#Insert.  PREVIOUSLY, IT
--	DEFAULT TO "1" FOR ALL DESTINATIONS; THIS MIGHT LIMIT US IN THE FUTURE.
--
--------------------------------------------------------------------------------


ALTER TABLE Extract.Destination ADD DefaultIncludeHistorical BIT NOT NULL CONSTRAINT DF_ExtractDestination__DefaultIncludeHistorical DEFAULT (0)
GO

UPDATE	Extract.Destination
SET	DefaultIncludeHistorical = 1

GO


UPDATE	Extract.DealerConfiguration
SET	IncludeHistorical = 1
WHERE	DestinationName = 'Edmunds'

GO