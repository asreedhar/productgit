INSERT
INTO	Extract.DealerConfigurationFlag(DestinationName, Name, BitPosition, Description)
SELECT	'AULtec', 'ExportVideoURL', 3, 'Flags whether to export the VideoURL for a dealer'


BEGIN TRY
	--------------------------------------------------------------------
	--	Enable for Fields BMW of Northfield and Liquidus dealers
	--------------------------------------------------------------------
	        
	EXEC sp_map_exec 
	'EXEC Extract.DealerConfiguration#SetFlag @BusinessUnitID = ?,
	        @DestinationName = "AULtec",
	        @FlagName = "ExportVideoURL",
	        @Value = 1',
	'SELECT	BusinessUnitID
	FROM	IMT.dbo.BusinessUnit BU
	WHERE	Active = 1
		AND (	BusinessUnitID = 105910 
			OR BusinessUnitCode IN (SELECT	DISTINCT DealerCode
						FROM	Staging.Listing.Vehicle#Active VA WITH (NOLOCK)
							INNER JOIN Staging.Listing.Provider P ON VA.[_ProviderID] = P.ProviderID
						WHERE	P.DatafeedCode = "Liquidus"
						)
			)'
	        
END TRY
BEGIN CATCH
	PRINT 'CATCH ERROR FOR SCRATCH BUILD'
END CATCH		