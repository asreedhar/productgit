------------------------------------------------------------------------------------------------
--	Remove V2 views
------------------------------------------------------------------------------------------------


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'Extract.Inventory#AULtec2') AND OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW Extract.Inventory#AULtec2
GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'Extract.InventoryBookoutValues#AULtec2') AND OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW Extract.InventoryBookoutValues#AULtec2
GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'Extract.Master#AULtec2') AND OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW Extract.Master#AULtec2
GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'Extract.MasterFile#AULtec') AND OBJECTPROPERTY(id, N'IsView') = 1)
DROP VIEW Extract.MasterFile#AULtec
GO
