/**************************************************************************************
*	OperationsGrantSelectForSalesForceExtractorService.sql
*
*	Operations - Hotfix
*	Grant Select on 3 additional tables for the SalesForceExtractorService
*	12/18/2009
****************************************************************************************/

GRANT SELECT ON [dbo].[ThirdParties] TO [SalesForceExtractorService]
GRANT SELECT ON [dbo].[ThirdPartyCategories] TO [SalesForceExtractorService]
GRANT SELECT ON [dbo].[DealerPreference_KBBConsumerTool] TO [SalesForceExtractorService]