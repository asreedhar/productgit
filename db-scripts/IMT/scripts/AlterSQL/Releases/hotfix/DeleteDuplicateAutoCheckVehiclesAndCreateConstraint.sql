/* 07/24/2009  This is the same code as the Carfax fix, but the multiple VIN has
not been found to be a problem.  I shall include this code for debugging purposes. 

CREATE 
TABLE	#DuplicateAutoCheckVINS
	(VIN VARCHAR(17),
	VehicleID INT,
	CNT	INT,
	Keep TINYINT)
The
INSERT
INTO	#DuplicateAutoCheckVINS
	(VIN ,
	VehicleID ,
	CNT,
	Keep )
SELECT	MP.VIN,
	V.VehicleID,
	COUNT(RQ.RequestID) as CNTVIN,
	0
FROM	(SELECT	VIN
	FROM	AutoCheck.Vehicle
	GROUP 
	BY	VIN
	HAVING 
	COUNT(*) >1 ) MP
JOIN	AutoCheck.Vehicle V
ON	MP.VIN =V.VIN
LEFT
JOIN	IMT.AutoCheck.Request RQ
ON	V.VehicleID =RQ.VehicleID
GROUP 
BY	MP.VIN,
	V.VehicleID 


UPDATE #DuplicateAutoCheckVINS
SET	Keep =1
WHERE	CNT >0


UPDATE #DuplicateAutoCheckVINS
SET	Keep =1
WHERE VehicleID IN
	(SELECT MIN(VehicleID)
	FROM	#DuplicateAutoCheckVINS VV
	JOIN	(SELECT		VIN
		FROM		#DuplicateAutoCheckVINS
		GROUP BY VIN
		HAVING	MAX(Keep)=0)V
	ON	V.VIN =VV.VIN
	GROUP BY V.VIN)

/*  TEST QUERY */
SELECT V.VIN,
	K1.KeepOne,
	K1.CountKeepOne,
	K0.KeepZero,
	K0.CountKeepZero
FROM 
	(SELECT DISTINCT(VIN)
	FROM #DuplicateAutoCheckVINS) V
LEFT
JOIN
(SELECT VIN,
	Keep as KeepOne,
	COUNT(*) as CountKeepOne
FROM #DuplicateAutoCheckVINS
WHERE Keep =1
GROUP 
BY	VIN,
	Keep)K1
ON V.VIN = K1.VIN
JOIN
(SELECT VIN,
	Keep as KeepZero,
	COUNT(*) as CountKeepZero
FROM #DuplicateAutoCheckVINS
WHERE Keep =0
GROUP 
BY	VIN,
	Keep)K0

ON V.VIN =K0.VIN
ORDER BY CountKeepOne DESC, CountKeepZero DESC

DELETE 
FROM	AutoCheck.Vehicle
WHERE	VehicleID IN
	(SELECT VehicleID 
	FROM	#DuplicateAutoCheckVINS
	WHERE Keep =0)

DROP TABLE #DuplicateAutoCheckVINS*/

CREATE UNIQUE NONCLUSTERED INDEX [UK_AutoCheck_Vehicle__VIN] 
	ON [AutoCheck].[Vehicle] 
	([VIN] ASC)
GO
