
/* Script to update/add report preferences for Hendrick stores with
Carfax accounts */

DECLARE @UpdateUser VARCHAR(80)
SET @UpdateUser ='kstevens'

DECLARE @InventoryEntityTypeID TINYINT
DECLARE @AppraisalEntityTypeID TINYINT

SET @InventoryEntityTypeID =1
SET @AppraisalEntityTypeID =2

DECLARE @ActiveCarfaxHendrickStores TABLE
	(idx INT IDENTITY(1,1),
	BusinessUnitID INT,
	HasInventoryReportPreference BIT,
	HasAppraisalReportPreference BIT)

INSERT
INTO	@ActiveCarfaxHendrickStores
	(BusinessUnitID, 
	HasInventoryReportPreference,
	HasAppraisalReportPreference)
SELECT	B.BusinessUnitID,
	CASE WHEN RPI.VehicleEntityTypeID IS NOT NULL THEN 1 ELSE 0 END,
	CASE WHEN RPA.VehicleEntityTypeID IS NOT NULL THEN 1 ELSE 0 END
FROM	dbo.BusinessUnit B
JOIN	dbo.BusinessUnitRelationship BUR
ON	B.BusinessUnitID =BUR.BusinessUnitID
JOIN	dbo.BusinessUnit BUP
ON	BUR.ParentID =BUP.BusinessUnitID
JOIN	Carfax.Account_Dealer AD
ON	B.BusinessUnitID =AD.DealerID
LEFT
JOIN	Carfax.ReportPreference RPI
ON	B.BusinessUnitID =RPI.DealerID
AND	RPI.VehicleEntityTypeID =@InventoryEntityTypeID
LEFT
JOIN	Carfax.ReportPreference RPA
ON	B.BusinessUnitID =RPA.DealerID
AND	RPA.VehicleEntityTypeID =@AppraisalEntityTypeID
WHERE	BUP.BusinessUnit like 'Hendrick%'
AND	BUP.BusinessunitTypeID  =3
AND	B.Active =1


DECLARE @i INT
DECLARE @c INT


DECLARE @HotListingOn BIT
DECLARE @HotListingOff BIT
DECLARE @AutoPurchaseOn BIT
DECLARE @InventoryReportType CHAR(3)
DECLARE @AppraisalReportType CHAR(3)

SET @HotListingOn =1
SET @HotListingOff =0
SET @AutoPurchaseOn =1
SET @InventoryReportType ='VHR'
SET @AppraisalReportType ='VHR'

SELECT @c =COUNT(*) FROM @ActiveCarfaxHendrickStores
SET @i =1

DECLARE @DealerID INT
DECLARE @HPInventory BIT	 
DECLARE @HPAppraisal BIT
DECLARE @Version BINARY(8)
DECLARE @NewVersion BINARY(8)


WHILE (@i<=@c)
BEGIN
	SELECT @DealerID =BusinessUnitID,
		@HPInventory =HasInventoryReportPreference,
		@HPAppraisal =HasAppraisalReportPreference
	FROM	@ActiveCarfaxHendrickStores
	WHERE	idx =@i

	IF (@HPInventory =1)
	BEGIN
		SELECT @Version =Version
		FROM	Carfax.ReportPreference
		WHERE	DealerID =@DealerID
		AND	VehicleEntityTypeID=@InventoryEntityTypeID

		EXEC Carfax.ReportPreference#Update @DealerID,@InventoryEntityTypeID,@HotListingOn,@AutoPurchaseOn,@InventoryReportType,@Version,@UpdateUser,@NewVersion OUTPUT
		
	END
	ELSE 
	BEGIN
		EXEC Carfax.ReportPreference#Insert @DealerID,@InventoryEntityTypeID,@HotListingOn,@AutoPurchaseOn,@InventoryReportType, @UpdateUser,@NewVersion OUTPUT
	END

	IF (@HPAppraisal =1)
	BEGIN
		SELECT @Version =Version
		FROM	Carfax.ReportPreference
		WHERE	DealerID =@DealerID
		AND	VehicleEntityTypeID=@AppraisalEntityTypeID

		EXEC Carfax.ReportPreference#Update @DealerID,@AppraisalEntityTypeID,@HotListingOff,@AutoPurchaseOn,@AppraisalReportType,@Version,@UpdateUser,@NewVersion OUTPUT
		
	END
	ELSE 
	BEGIN
		EXEC Carfax.ReportPreference#Insert @DealerID,@AppraisalEntityTypeID,@HotListingOff,@AutoPurchaseOn,@AppraisalReportType, @UpdateUser,@NewVersion OUTPUT
	END


	SET @i =@i +1
END
GO