
/* MAK 09/15/2009	This script enters jobs into the processor to remove deleted\inactive inventory units from the Carfax.Hotlist.*/

DECLARE @err INT
DECLARE @CarfaxUserName VARCHAR(50)	
SET @CarfaxUserName ='CarfaxSystemUserAdmin'

DECLARE @CarfaxAdmin INT
EXEC @err =IMT.dbo.ValidateParameter_MemberID#UserName @CarfaxUserName, @CarfaxAdmin OUTPUT


DECLARE @ReportProcessorHotListCommandTypeID SMALLINT
SET @ReportProcessorHotListCommandTypeID= 4 --- ChangeHotList Status

DECLARE @ReportProcessorCommandStatusID TINYINT
SET @ReportProcessorCommandStatusID =1 -- StatusID of 1

DECLARE @VehicleEntityTypeID TINYINT
SET @VehicleEntityTypeID =1 --Inventory

/* The  code below enters in commands into the processor queue to turn off from the hot list reports for sold\inactive vehicles. */
INSERT
INTO	IMT.Carfax.ReportProcessorCommand
        ( DealerID ,
          VehicleID ,
          VehicleEntityTypeID ,
          ReportProcessorCommandTypeID ,
          ReportProcessorCommandStatusID ,
          ProcessAfterDate ,
          InsertUser ,
          InsertDate )
          
SELECT	  I.DealerID,
	  VV.VehicleID,
	  @VehicleEntityTypeID,
	  @ReportProcessorHotListCommandTypeID,
	  @ReportProcessorCommandStatusID,
	  GETDATE(),
	  @CarfaxAdmin,
	  GETDATE()
FROM	IMT.Carfax.Vehicle VV  	
JOIN	IMT.Carfax.Vehicle_Dealer VD ON	VV.VehicleID =VD.VehicleID
JOIN	IMT.Carfax.Account_Dealer AD ON VD.DealerID =AD.DealerID
JOIN	(SELECT	MAX(RPT.RequestID) AS RequestID,
		RQ.VehicleID,
		RQ.AccountID
	FROM	IMT.Carfax.Request RQ 
	JOIN	IMT.Carfax.Report RPT ON RPT.RequestID =RQ.RequestID
	WHERE	RPT.ExpirationDate>=GETDATE()
	GROUP BY RQ.VehicleID,RQ.AccountID) RX ON VV.VehicleID =RX.VehicleID AND AD.AccountID=RX.AccountID
 JOIN	(SELECT  SI.BusinessUnitID AS DealerID,
		V.Vin AS VIN 
	FROM	FLDW.dbo.InventoryInactive  SI
	JOIN	FLDW.dbo.Vehicle V ON  SI.VehicleID = V.VehicleID AND   SI.BusinessUnitID =V.BusinessUnitID
	WHERE	SI.InventoryType=2 AND SI.InventoryActive =0
	GROUP BY  SI.BusinessUnitID, V.VIN )  I ON VV.VIN =I.VIN AND VD.DealerID =I.DealerID
JOIN	IMT.Carfax.Request RQQ ON RX.RequestID =RQQ.RequestID	
LEFT
JOIN	(SELECT DealerID,
		VehicleID
	FROM	IMT.Carfax.ReportProcessorCommand RPC
	WHERE	RPC.ReportProcessorCommandTypeID =@ReportProcessorHotListCommandTypeID
	AND	RPC.ReportProcessorCommandStatusID =@ReportProcessorCommandStatusID) RPC ON VD.DealerID =RPC.DealerID AND VV.VehicleID =RPC.VehicleID

WHERE	RQQ.DisplayInHotList=1 AND RPC.VehicleID IS NULL

INSERT
INTO	IMT.Carfax.ReportProcessorCommand
        ( DealerID ,
          VehicleID ,
          VehicleEntityTypeID ,
          ReportProcessorCommandTypeID ,
          ReportProcessorCommandStatusID ,
          ProcessAfterDate ,
          InsertUser ,
          InsertDate )
          
SELECT		I.DealerID,
	  VV.VehicleID,
	  @VehicleEntityTypeID,
	  @ReportProcessorHotListCommandTypeID,
	  @ReportProcessorCommandStatusID,
	  GETDATE(),
	  @CarfaxAdmin,
	  GETDATE()
FROM	IMT.Carfax.Vehicle VV  	
JOIN	IMT.Carfax.Vehicle_Dealer VD ON	VV.VehicleID =VD.VehicleID
JOIN	IMT.Carfax.Account_Dealer AD ON VD.DealerID =AD.DealerID
JOIN	(SELECT	MAX(RPT.RequestID) AS RequestID,
		RQ.VehicleID,
		RQ.AccountID
	FROM	IMT.Carfax.Request RQ 
	JOIN	IMT.Carfax.Report RPT ON RPT.RequestID =RQ.RequestID
	WHERE	RPT.ExpirationDate>=GETDATE()
	GROUP BY RQ.VehicleID,RQ.AccountID) RX ON VV.VehicleID =RX.VehicleID AND AD.AccountID=RX.AccountID
 JOIN	(SELECT SI.BusinessUnitID AS DealerID,
		V.Vin AS VIN 
	FROM	FLDW.dbo.InventoryActive SI
	JOIN	FLDW.dbo.Vehicle V ON SI.VehicleID = V.VehicleID AND SI.BusinessUnitID =V.BusinessUnitID
	 JOIN	FLDW.dbo.Appraisals A ON V.VehicleID = A.VehicleID AND V.BusinessUnitID=A.BusinessUnitID
	WHERE	SI.InventoryActive=1 AND SI.InventoryType=2
	GROUP BY  SI.BusinessUnitID, V.VIN )  I ON VV.VIN =I.VIN AND VD.DealerID =I.DealerID
JOIN	IMT.Carfax.Request RQQ ON RX.RequestID =RQQ.RequestID	
LEFT
JOIN	IMT.Carfax.ReportPreference RPI ON  VD.DealerID =RPI.DealerID AND RPI.VehicleEntityTypeID =1
LEFT
JOIN	(SELECT DealerID,
		VehicleID
	FROM	IMT.Carfax.ReportProcessorCommand RPC
	WHERE	RPC.ReportProcessorCommandTypeID =@ReportProcessorHotListCommandTypeID
	AND	RPC.ReportProcessorCommandStatusID =@ReportProcessorCommandStatusID) RPC ON VD.DealerID =RPC.DealerID AND VV.VehicleID =RPC.VehicleID
  
WHERE	RQQ.DisplayInHotList<> COALESCE(RPI.DisplayInHotList,0)
AND	RPC.VehicleID IS NULL

GO     
	     