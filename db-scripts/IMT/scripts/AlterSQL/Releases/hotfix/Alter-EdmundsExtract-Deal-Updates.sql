

ALTER TABLE dbo.DealTradeIn ALTER COLUMN ActualCashValue DECIMAL(9,2) NULL
GO
ALTER TABLE dbo.DealTradeIn ALTER COLUMN CreditAmount DECIMAL(9,2) NULL
GO

ALTER TABLE dbo.DealFinance ADD InvoiceAmount DECIMAL(9,2) NULL
GO