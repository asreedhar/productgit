/*  09/22/2009	This script updates The Carfax Users Name and Password for HendrickLexus to the accepted value and repurchases all Appraisals
non-expired inventory report for that dealer.

09/22/2009	Several Changes:  All reports (even for inactive vehicles) can be repurchased.
				Reports are renewals (CommandTypeID =2)
				Accounts will be changed by the PDC.
				
*/
DECLARE @BusinessUnitID INT
SET @BusinessUnitID =101929

DECLARE @CarfaxUserName VARCHAR(10)
DECLARE @CarfaxPassword VARCHAR(5)

SET @CarfaxUserName ='c356367'
SET @CarfaxPassword ='7016'

DECLARE @AccountID INT
SET @AccountID =693

DECLARE @UpdateUser INT
SET @UpdateUser =115530 -- This is the Carfax System Admin User
 

DECLARE @ReportProcessorCommandTypeID SMALLINT
SET @ReportProcessorCommandTypeID =2 -- We want these to be renews

DECLARE @ReportCommandStatusID SMALLINT
SET @ReportCommandStatusID =1

INSERT
INTO	Carfax.ReportProcessorCommand
        ( DealerID ,
          VehicleID ,
          VehicleEntityTypeID ,
          ReportProcessorCommandTypeID ,
          ReportProcessorCommandStatusID ,
          ProcessAfterDate ,
          InsertUser ,
          InsertDate           
        )
 

SELECT	CF.BusinessUnitID,
	CF.CarfaxVehicleID,
	CASE WHEN I.INventoryID IS NOT NULL THEN 1 ELSE 2 END,
	@ReportProcessorCommandTypeID , -- ReportProcessorCommandTypeID - tinyint
        @ReportCommandStatusID , -- ReportProcessorCommandStatusID - tinyint
        GETDATE()  , -- ProcessAfterDate - datetime
        @UpdateUser , -- InsertUser - int
        GETDATE()
FROM
	(SELECT		B.BusinessUnit,
		B.BusinessUnitID,
		MAX(RQ.InsertDate)AS RequestDate ,
		V.VIN,
		V.VehicleID AS CarfaxVehicleID,
		VV.VehicleID AS FLDWVehicleID
	FROM	Carfax.Account A
	JOIN	Carfax.Account_Dealer AD ON	A.AccountID =AD.AccountID
	JOIN	IMT.dbo.BusinessUnit B ON	AD.dealerID =B.BusinessUnitID AND B.BusinessUnitID=@BusinessUnitID
	JOIN	Carfax.Request RQ ON	RQ.AccountID =A.AccountID
	JOIN	Carfax.Report RP ON RQ.RequestID=RP.requestID
	JOIN	Carfax.Vehicle V ON RP.RequestID = V.RequestID
	JOIN	FLDW.dbo.Vehicle VV ON V.VIN=VV.VIN
	GROUP BY	B.BusinessUnit,
			B.BusinessUnitID,
			 V.VIN,
			 V.VehicleID,
			VV.VehicleID) CF
LEFT 
JOIN	FLDW.dbo.Inventory I ON I.VehicleID =CF.FLDWVehicleID AND I.BusinessUnitID =@BusinessUnitID 
GO