
DECLARE @PhotoProviderID INT

INSERT
INTO	dbo.ThirdPartyEntity (BusinessUnitID, ThirdPartyEntityTypeID, Name)
SELECT	BusinessUnitID, 6, 'DealerPeak Lot Provider Photos'
FROM	dbo.BusinessUnit BU
WHERE	BusinessUnit = 'Firstlook'
	AND BusinessUnitTypeID = 5
	
SET @PhotoProviderID = SCOPE_IDENTITY()

INSERT
INTO	dbo.PhotoProvider_ThirdPartyEntity (PhotoProviderID, DatafeedCode, PhotoStorageCode)
SELECT	@PhotoProviderID, 'DealerPeakListing', 2
		

INSERT
INTO	dbo.PhotoProviderDealership (PhotoProviderID, BusinessUnitID, PhotoProvidersDealershipCode, PhotoStorageCode_Override)
SELECT	@PhotoProviderID, BU.BusinessUnitID, BU.BusinessUnitCode, 2
FROM	dbo.BusinessUnit BU
WHERE	BusinessUnitCode IN ('RONTONKI11','RONTONKI10','RONTONKI12','RONTONKI08','RONTONKI02','RONTONKI13','RONTONKI06','RONTONKI04','ACURAOFR01','RONTONKI03','LARSENMO02')
