INSERT
INTO	Extract.DealerConfigurationFlag(DestinationName, Name, BitPosition, Description)
SELECT	'AULtec', 'UseMaxAdMSRP', 6, 'Flags whether to export the MSRP from Merchandising (prioritized over Inventory)'

--------------------------------------------------------------------
--	Passport BMW.  Enable via PitStop and SF ticket
--------------------------------------------------------------------
BEGIN TRY
        
	EXEC Extract.DealerConfiguration#SetFlag @BusinessUnitID = 107016,
	        @DestinationName = 'AULtec',
	        @FlagName = 'UseMaxAdMSRP',
	        @Value = 0
        
END TRY
BEGIN CATCH
	PRINT 'CATCH ERROR FOR SCRATCH BUILD'
END CATCH	        