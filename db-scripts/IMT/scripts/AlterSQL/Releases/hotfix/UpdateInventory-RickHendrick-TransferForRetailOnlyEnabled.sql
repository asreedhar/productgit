DECLARE @BusinessUnitID INT

SELECT  @BusinessUnitID = BusinessUnitID
FROM    dbo.BusinessUnit
WHERE  BusinessUnit LIKE '%Rick Hendrick Imports%'

UPDATE dbo.Inventory
SET TransferForRetailOnlyEnabled=1
--, TransferForRetailOnly = 1
WHERE  BusinessUnitID = @BusinessUnitID
AND    InventoryActive = 1
AND    InventoryType = 2

   
GO