
/* MAK	08/06/2009	Add destination type to table and corresponding Dealers to Configuration table*/
IF NOT EXISTS (SELECT 1
		FROM	Extract.Destination
		WHERE	Name= 'AULTec')
	INSERT
	INTO	Extract.Destination 
		(Name)
	VALUES	('AULTec')

DELETE FROM Extract.DealerConfiguration WHERE DestinationName ='AULTec'

INSERT 
INTO Extract.DealerConfiguration 
	(BusinessUnitID,
	DeStinationName, 
	ExternalIdentifier, 
	StartDate,
	EndDate,
	Active,
	IncludeHistorical,
	InsertDate,InsertUser)VALUES(100148,'AULTec',8256,GETDATE(),NULL,1,0,Getdate(),'mkipiniak')

INSERT 
INTO Extract.DealerConfiguration 
	(BusinessUnitID,
	DeStinationName, 
	ExternalIdentifier, 
	StartDate,
	EndDate,
	Active,
	IncludeHistorical,
	InsertDate,InsertUser)VALUES(100215,'AULTec',8328,GETDATE(),NULL,1,0,Getdate(),'mkipiniak')
INSERT 
INTO Extract.DealerConfiguration 
	(BusinessUnitID,
	DeStinationName, 
	ExternalIdentifier, 
	StartDate,
	EndDate,
	Active,
	IncludeHistorical,
	InsertDate,InsertUser)VALUES(101929,'AULTec',8264,GETDATE(),NULL,1,0,Getdate(),'mkipiniak')
INSERT 
INTO Extract.DealerConfiguration 
	(BusinessUnitID,
	DeStinationName, 
	ExternalIdentifier, 
	StartDate,
	EndDate,
	Active,
	IncludeHistorical,
	InsertDate,InsertUser)VALUES(102761,'AULTec',8157,GETDATE(),NULL,1,0,Getdate(),'mkipiniak')
GO

