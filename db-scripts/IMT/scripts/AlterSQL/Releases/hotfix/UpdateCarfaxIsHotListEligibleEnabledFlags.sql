/*MAK 09/15/2009	 This is the repair script for updating the IsHotListEligible AND the IsHotListEnabled flags.*/

	UPDATE	V 
	SET	IsHotListEligible =1
	FROM	Carfax.Vehicle V
	JOIN	(SELECT	MAX(RequestID) AS RequestID,
			VehicleID
		FROM	Carfax.Request
		GROUP BY VehicleID) R ON	V.VehicleID =R.VehicleID
	JOIN	Carfax.Response RR ON R.RequestID =RR.RequestID
	WHERE	RR.ResponseCode =500
	
	UPDATE	V 
	SET	IsHotListEligible =0
	FROM	Carfax.Vehicle V
	JOIN	(SELECT	MAX(RequestID) AS RequestID,
			VehicleID
		FROM	Carfax.Request
		GROUP BY VehicleID) R ON	V.VehicleID =R.VehicleID
	JOIN	Carfax.Response RR ON R.RequestID =RR.RequestID
	WHERE	RR.ResponseCode =501


	UPDATE  VD
	SET	IsHotListEnabled =1
	FROM	Carfax.Vehicle_Dealer VD
	JOIN	Carfax.Vehicle V ON	VD.VehicleID =V.VehicleID
	JOIN	(SELECT	MAX(RequestID) AS RequestID,
			VehicleID,
			AccountID
		FROM	Carfax.Request
		GROUP BY VehicleID,AccountID) R ON	V.VehicleID =R.VehicleID
	JOIN	Carfax.Account A ON	R.AccountID =A.AccountID
	JOIN	Carfax.Account_Dealer AD ON A.AccountID =AD.AccountID AND AD.DealerID =VD.DealerID
	JOIN	Carfax.Request RR ON R.RequestID =RR.RequestID
	WHERE	 V.IsHotListEligible=1 AND RR.DisplayInHotList=1
	
	
	