/**************************************************************************************
*	EliteOpsReportsDataDrivenSubscriptionSetupMcGrath.sql
*
*	Elite Ops Reports - Hotfix #2
*	Add additional McGrath users to first wave of recipients
*	12/15/2009
****************************************************************************************/

--
-- Change Subscription info for existing subscriber
--
UPDATE dbo.Subscriptions
SET SubscriptionFrequencyDetailID=9
WHERE SubscriberID=116093  -- Greg Joutras
AND SubscriptionTypeID BETWEEN 16 AND 19
AND SubscriptionFrequencyDetailID=7


--
-- Update Email Address for existing Member
--
UPDATE dbo.Member
SET EmailAddress='pguardino@mcgrathag.com'
WHERE LastName ='Guardino'
AND FirstName='Pam'
AND MemberID=114829
AND Login='pguardino'
AND LEN(EmailAddress)=0

--
-- Insert New Subscription Rows
--
INSERT INTO dbo.Subscriptions
        ( SubscriberID ,
          SubscriberTypeID ,
          SubscriptionTypeID ,
          SubscriptionDeliveryTypeID ,
          BusinessUnitID ,
          SubscriptionFrequencyDetailID
        )
SELECT 102348, 1, 16, 1, 100833, 9 
UNION SELECT 102348, 1, 17, 1, 100833, 9 
UNION SELECT 102348, 1, 18, 1, 100833, 9 
UNION SELECT 102348, 1, 19, 1, 100833, 9 
UNION SELECT 102348, 1, 19, 1, 100834, 9 
UNION SELECT 102348, 1, 18, 1, 100834, 9 
UNION SELECT 102348, 1, 17, 1, 100834, 9 
UNION SELECT 102348, 1, 16, 1, 100834, 9 
UNION SELECT 102348, 1, 16, 1, 100835, 9 
UNION SELECT 102348, 1, 17, 1, 100835, 9 
UNION SELECT 102348, 1, 18, 1, 100835, 9 
UNION SELECT 102348, 1, 19, 1, 100835, 9 
UNION SELECT 102348, 1, 19, 1, 100837, 9 
UNION SELECT 102348, 1, 18, 1, 100837, 9 
UNION SELECT 102348, 1, 17, 1, 100837, 9 
UNION SELECT 102348, 1, 16, 1, 100837, 9 
UNION SELECT 114264, 1, 16, 1, 100835, 9 
UNION SELECT 114264, 1, 17, 1, 100835, 9 
UNION SELECT 114264, 1, 18, 1, 100835, 9 
UNION SELECT 114264, 1, 19, 1, 100835, 9 
UNION SELECT 114829, 1, 19, 1, 100833, 9 
UNION SELECT 114829, 1, 18, 1, 100833, 9 
UNION SELECT 114829, 1, 16, 1, 100833, 9 
UNION SELECT 114829, 1, 17, 1, 100833, 9 
UNION SELECT 117330, 1, 16, 1, 100837, 9 
UNION SELECT 117330, 1, 17, 1, 100837, 9 
UNION SELECT 117330, 1, 18, 1, 100837, 9 
UNION SELECT 117330, 1, 19, 1, 100837, 9
UNION SELECT 112849, 1, 16, 1, 102278, 7	-- Bryan Nightingale
UNION SELECT 112849, 1, 17, 1, 102278, 7
UNION SELECT 112849, 1, 18, 1, 102278, 7
UNION SELECT 112849, 1, 19, 1, 102278, 7
UNION SELECT 112849, 1, 16, 1, 102279, 7
UNION SELECT 112849, 1, 17, 1, 102279, 7
UNION SELECT 112849, 1, 18, 1, 102279, 7
UNION SELECT 112849, 1, 19, 1, 102279, 7