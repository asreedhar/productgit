/* 07/24/2009	MAK	This script eliminates duplicate vehicles in the Carfax.Vehicle table.
The #DuplicateCarfaxVINS table holds all the Vehicles where there is more than one count of the 
VIN.  It marks the Keep flag for all Vehicles with values in the request table or Vehicles the minimum 
Vehicle ID amongst the VIN.  It eliminates all values from the tables where Keep =0.*/

 

CREATE 
TABLE	#DuplicateCarfaxVINS
	(VIN VARCHAR(17),
	VehicleID INT,
	CNT	INT,
	Keep TINYINT)

INSERT
INTO	#DuplicateCarfaxVINS
	(VIN ,
	VehicleID ,
	CNT,
	Keep )
SELECT	MP.VIN,
	V.VehicleID,
	COUNT(RQ.RequestID) as CNTVIN,
	0
FROM	(SELECT	VIN
	FROM	Carfax.Vehicle
	GROUP 
	BY	VIN
	HAVING 
	COUNT(*) >1 ) MP
JOIN	Carfax.Vehicle V
ON	MP.VIN =V.VIN
LEFT
JOIN	IMT.Carfax.Request RQ
ON	V.VehicleID =RQ.VehicleID
GROUP 
BY	MP.VIN,
	V.VehicleID 


UPDATE #DuplicateCarfaxVINS
SET	Keep =1
WHERE	CNT >0


UPDATE #DuplicateCarfaxVINS
SET	Keep =1
WHERE VehicleID IN
	(SELECT MIN(VehicleID)
	FROM	#DuplicateCarfaxVINS VV
	JOIN	(SELECT		VIN
		FROM		#DuplicateCarfaxVINS
		GROUP BY VIN
		HAVING	MAX(Keep)=0)V
	ON	V.VIN =VV.VIN
	GROUP BY V.VIN)


DELETE
FROM	 Carfax.ReportProcessorCommand_Exception  
WHERE	 ReportProcessorCommandID IN
	(SELECT	ReportProcessorCommandID
	FROM	Carfax.ReportProcessorCommand RPC
	JOIN	#DuplicateCarfaxVINS V
	ON	RPC.VehicleID =V.VehicleID
	AND	V.Keep =0)

DELETE
FROM	Carfax.ReportProcessorCommand_DataMigration  
WHERE	 ReportProcessorCommandID IN
	(SELECT	ReportProcessorCommandID
	FROM	Carfax.ReportProcessorCommand RPC
	JOIN	#DuplicateCarfaxVINS V
	ON	RPC.VehicleID =V.VehicleID
	AND	V.Keep =0)

DELETE
FROM	Carfax.ReportProcessorCommand_Request  
WHERE	 ReportProcessorCommandID IN
	(SELECT	ReportProcessorCommandID
	FROM	Carfax.ReportProcessorCommand RPC
	JOIN	#DuplicateCarfaxVINS V
	ON	RPC.VehicleID =V.VehicleID
	AND	V.Keep =0)

DELETE 
FROM	Carfax.ReportProcessorCommand  
WHERE	VehicleID in
	(Select VehicleID
	FROM	#DuplicateCarfaxVINS
	WHERE	Keep =0)
DELETE 
FROM	Carfax.Vehicle
WHERE	VehicleID IN
	(SELECT VehicleID 
	FROM	#DuplicateCarfaxVINS
	WHERE Keep =0)


CREATE UNIQUE NONCLUSTERED INDEX [UK_Carfax_Vehicle__VIN] 
	ON [Carfax].[Vehicle] 
	([VIN] ASC) 

 DROP TABLE #DuplicateCarfaxVINS

GO