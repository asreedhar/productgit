INSERT
INTO	Extract.DealerConfigurationFlag(DestinationName, Name, BitPosition, Description)
SELECT	'AULtec', 'ExportChromeStyleID', 4, 'Flags whether to export the Chrome StyleID for a dealer'

--------------------------------------------------------------------
--	Enable for the Bob Ridings Auto Group
--------------------------------------------------------------------

BEGIN TRY        
	EXEC sp_map_exec 
	'EXEC Extract.DealerConfiguration#SetFlag @BusinessUnitID = ?,
	        @DestinationName = "AULtec",
	        @FlagName = "ExportChromeStyleID",
	        @Value = 1',
	'
	SELECT	BUR.BusinessUnitID
	FROM	IMT.dbo.BusinessUnit BU
		INNER JOIN IMT.dbo.BusinessUnitRelationship BUR ON BU.BusinessUnitID = BUR.ParentID
		INNER JOIN IMT.dbo.BusinessUnit BU2 ON BUR.BusinessUnitID = BU2.BusinessUnitID
	WHERE	BU.BusinessUnit = ''Bob Ridings Auto Group''
		AND BU.BusinessUnitTypeID = 3
		AND BU2.Active = 1'
	
END TRY
BEGIN CATCH
	PRINT 'CATCH ERROR FOR SCRATCH BUILD'
END CATCH	