/****************************************************************************************
*
*	Purpose:	Elite Ops Data Driven Subscription Setup Wave 6
*
*	Date:		5/19/2010
*
*	Databases:	IMT (data only)
*
*****************************************************************************************/

CREATE TABLE #EliteOps (SubscriberID INT ,
                        SubscriberTypeID INT ,
                        SubscriptionTypeID INT ,
                        SubscriptionDeliveryTypeID INT ,
                        BusinessUnitID INT ,
                        SubscriptionFrequencyDetailID INT)

BULK INSERT #EliteOps FROM 'C:\EliteOpsWave6Final.txt'
     WITH
      (
        DATAFILETYPE = 'char'
       ,FIELDTERMINATOR = ','
       ,FIRSTROW = 1
       ,ROWS_PER_BATCH  = 10000
       ,MAXERRORS       = 0
       ,TABLOCK
      )
      

INSERT  INTO imt.dbo.Subscriptions
        (SubscriberID ,
         SubscriberTypeID ,
         SubscriptionTypeID ,
         SubscriptionDeliveryTypeID ,
         BusinessUnitID ,
         SubscriptionFrequencyDetailID)
SELECT  SubscriberID ,
        SubscriberTypeID ,
        SubscriptionTypeID ,
        SubscriptionDeliveryTypeID ,
        BusinessUnitID ,
        SubscriptionFrequencyDetailID
FROM    #EliteOps



--SELECT  
--        M.MemberID,
--        M.FirstName,
--        M.LastName,
--        M.EmailAddress,
--        BU.BusinessUnit,
--        SFD.Description DayOfWeek
--FROM    #EliteOps E
--LEFT JOIN IMT.dbo.Member M ON E.SubscriberID=M.MemberID
--LEFT JOIN IMT.dbo.BusinessUnit BU ON E.BusinessUnitID=BU.BusinessUnitID
--LEFT JOIN IMT.dbo.SubscriptionFrequencyDetail SFD ON E.SubscriptionFrequencyDetailID=SFD.SubscriptionFrequencyDetailID
--WHERE     E.SubscriptionTypeID=16
--ORDER BY BU.BusinessUnit, M.FirstName, M.LastName

DROP TABLE #EliteOps

GO
