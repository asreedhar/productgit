INSERT
INTO	Extract.DealerConfigurationFlag(DestinationName, Name, BitPosition, Description)
SELECT	'AULtec', 'ExportDmsOptionCodes', 2, 'Flags whether to export the DMS option codes for a dealer'

--------------------------------------------------------------------
--	Enable for Charles Barker Infiniti
--------------------------------------------------------------------


BEGIN TRY
	EXEC Extract.DealerConfiguration#SetFlag @BusinessUnitID = 103986,
	        @DestinationName = 'AULtec',
	        @FlagName = 'ExportDmsOptionCodes',
	        @Value = 1
END TRY
BEGIN CATCH
	PRINT 'CATCH ERROR FOR SCRATCH BUILD'
END CATCH