
/*
 * There was a defect in Carfax.Report#Insert that meant that the code would not register
 * a record in the table Carfax.Vehicle_Dealer.  This meant that the procedure
 * Carfax.Account#HasPurchasedReport would always return false (which was not the case).
 * This insert statement adds the missing records into the Vehicle_Dealer table.
 * 
 * https://incisent.fogbugz.com/default.asp?9922
 * 
 */

INSERT INTO Carfax.Vehicle_Dealer
        ( VehicleID ,
          DealerID ,
          RequestID ,
          IsHotListEnabled
        )

SELECT  T.VehicleID, T.DealerID, T.RequestID, IsHotListEnabled=CONVERT(BIT,
                CASE WHEN RQ.DisplayInHotList = 1 AND RS.ResponseCode = 500 THEN 1 ELSE 0 END)
FROM
(
        SELECT  D.DealerID, V.VehicleID, RequestID=MAX(RQ.RequestID) 
        FROM    Carfax.Vehicle V
        JOIN    Carfax.Request RQ ON RQ.VehicleID = V.VehicleID
        JOIN    Carfax.Account A ON A.AccountID = RQ.AccountID
        JOIN    Carfax.Account_Dealer D ON D.AccountID = A.AccountID
        JOIN    Carfax.Report RP ON RP.RequestID = RQ.RequestID -- limit to successful reports
        WHERE   NOT EXISTS (
                        SELECT  1
                        FROM    Carfax.Vehicle_Dealer X
                        WHERE   X.VehicleID = V.VehicleID
                        AND     D.DealerID = X.DealerID
                )
        GROUP
        BY      D.DealerID, V.VehicleID
) T
JOIN    Carfax.Request RQ ON RQ.RequestID = T.RequestID
JOIN    Carfax.Response RS ON RS.RequestID = RQ.RequestID
