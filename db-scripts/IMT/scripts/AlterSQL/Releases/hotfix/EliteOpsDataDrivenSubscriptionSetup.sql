/****************************************************************************************
*
*	Purpose:	Elite Ops Alter Script
*
*	Date:		12/3/2009
*
*	Databases:	IMT (data only)
*
*****************************************************************************************/

--
--	Insert New Subscription Delivery Type for PDF
--
--INSERT INTO dbo.SubscriptionDeliveryTypes
--(SubscriptionDeliveryTypeID, Description)
--Values (5, 'PDF')

--
--	Insert New Subscription Frequency Detail ID
--
--INSERT INTO dbo.SubscriptionFrequencyDetail
--(SubscriptionFrequencyDetailID, SubscriptionFrequencyID, [Rank], Description)
--Values (11, 2, 1, 'Monthly - 11th')

--	!!!Future Upgrade, not happening now!!!
--	Insert New Dealer Upgrade Code for Elite Ops Reports dbo.lu_DealerUpgrade
--
--INSERT INTO dbo.lu_DealerUpgrade
--(DealerUpgradeCD, DealerUpgradeDESC)
--Values (22, 'EliteOps Reports')


--	!!!Future Upgrade, not happening now!!!
--	Insert New Dealer Upgrade Code for Elite Ops Reports dbo.DealerUpgrade
--	(Note:  Perform this step for each group of BusinessUnits being activated)
--
--INSERT INTO DealerUpgrade
--(BusinessUnitID, DealerUpgradeCD, Active)
--VALUES(BusinessUnitID, 22, 1)


--
--	Register the report
--
INSERT INTO dbo.SubscriptionTypes
(SubscriptionTypeID, [Description], Notes, UserSelectable, DefaultSubscriptionFrequencyDetailID, Active)
VALUES(16, 'Elite Ops Pre-Owned Retail Performance Overview', 'Elite Ops Pre-Owned Retail Performance Overview',1,7,1)

INSERT INTO dbo.SubscriptionTypes
(SubscriptionTypeID, [Description], Notes, UserSelectable, DefaultSubscriptionFrequencyDetailID, Active)
VALUES(17, 'Elite Ops Aged Wholesale', 'Elite Ops Aged Wholesale',1,7,1)

INSERT INTO dbo.SubscriptionTypes
(SubscriptionTypeID, [Description], Notes, UserSelectable, DefaultSubscriptionFrequencyDetailID, Active)
VALUES(18, 'Elite Ops Potential Missed Profit', 'Elite Ops Potential Missed Profit',1,7,1)
 
INSERT INTO dbo.SubscriptionTypes
(SubscriptionTypeID, [Description], Notes, UserSelectable, DefaultSubscriptionFrequencyDetailID, Active)
VALUES(19, 'Elite Ops Storms on the Horizon', 'Elite Ops Storms on the Horizon',1,7,1)

--INSERT INTO dbo.SubscriptionTypes
--(SubscriptionTypeID, [Description], Notes, UserSelectable, DefaultSubscriptionFrequencyDetailID, Active)
--VALUES(20, 'Elite Ops Drive Your Numbers', 'Elite Ops Drive Your Numbers',1,3,1)



--
-- Associate the report to the DealerUpgrades
--
INSERT INTO dbo.SubscriptionTypeDealerUpgrade
(SubscriptionTypeID, DealerUpgradeCD)
SELECT 16, 12	-- (EliteOps Pre-Owned Retail Performance Overview, Platinum Package Upgrade)

INSERT INTO dbo.SubscriptionTypeDealerUpgrade
(SubscriptionTypeID, DealerUpgradeCD)
SELECT 17, 12	-- (Elite Ops Aged Wholesale, Platinum Package Upgrade)

INSERT INTO dbo.SubscriptionTypeDealerUpgrade
(SubscriptionTypeID, DealerUpgradeCD)
SELECT 18, 12	-- (Elite Ops Potential Missed Profit, Platinum Package Upgrade)

INSERT INTO dbo.SubscriptionTypeDealerUpgrade
(SubscriptionTypeID, DealerUpgradeCD)
SELECT 19, 12	-- (Elite Ops Storms on the Horizon, Platinum Package Upgrade)

--INSERT INTO dbo.SubscriptionTypeDealerUpgrade
--(SubscriptionTypeID, DealerUpgradeCD)
--SELECT 20, 12	-- (Elite Ops Drive Your Numbers, Platinum Package Upgrade)

--
-- Register the Available Formats for the report
--
INSERT INTO dbo.SubscriptionTypeSubscriptionDeliveryType
(SubscriptionTypeID, SubscriptionDeliveryTypeID)
SELECT 16, 1	-- (EliteOps Pre-Owned Retail Performance Overview , Email-HTML)

INSERT INTO dbo.SubscriptionTypeSubscriptionDeliveryType
(SubscriptionTypeID, SubscriptionDeliveryTypeID)
SELECT 17, 1	-- (Elite Ops Aged Wholesale , Email-HTML)

INSERT INTO dbo.SubscriptionTypeSubscriptionDeliveryType
(SubscriptionTypeID, SubscriptionDeliveryTypeID)
SELECT 18, 1	-- (Elite Ops Potential Missed Profit , Email-HTML)

INSERT INTO dbo.SubscriptionTypeSubscriptionDeliveryType
(SubscriptionTypeID, SubscriptionDeliveryTypeID)
SELECT 19, 1	-- (Elite Ops Storms on the Horizon , Email-HTML)

--INSERT INTO dbo.SubscriptionTypeSubscriptionDeliveryType
--(SubscriptionTypeID, SubscriptionDeliveryTypeID)
--SELECT 20, 1	-- (Elite Ops Drive Your Numbers , Email-HTML)


--
-- Register the FrequencyDetails available to the report
--
INSERT INTO dbo.SubscriptionTypeSubscriptionFrequencyDetail
(SubscriptionTypeID, SubscriptionFrequencyDetailID)
SELECT 16, 1		-- (No Alerts)
UNION SELECT 16,4	-- Sunday
UNION SELECT 16,5	-- Monday
UNION SELECT 16,6	-- Tuesday
UNION SELECT 16,7	-- Wednesday
UNION SELECT 16,8	-- Thursday
UNION SELECT 16,9	-- Friday
UNION SELECT 16,10	-- Saturday

INSERT INTO dbo.SubscriptionTypeSubscriptionFrequencyDetail
(SubscriptionTypeID, SubscriptionFrequencyDetailID)
SELECT 17, 1		-- (No Alerts)
UNION SELECT 17,4	-- Sunday
UNION SELECT 17,5	-- Monday
UNION SELECT 17,6	-- Tuesday
UNION SELECT 17,7	-- Wednesday
UNION SELECT 17,8	-- Thursday
UNION SELECT 17,9	-- Friday
UNION SELECT 17,10	-- Saturday

INSERT INTO dbo.SubscriptionTypeSubscriptionFrequencyDetail
(SubscriptionTypeID, SubscriptionFrequencyDetailID)
SELECT 18, 1		-- (No Alerts)
UNION SELECT 18,4	-- Sunday
UNION SELECT 18,5	-- Monday
UNION SELECT 18,6	-- Tuesday
UNION SELECT 18,7	-- Wednesday
UNION SELECT 18,8	-- Thursday
UNION SELECT 18,9	-- Friday
UNION SELECT 18,10	-- Saturday

INSERT INTO dbo.SubscriptionTypeSubscriptionFrequencyDetail
(SubscriptionTypeID, SubscriptionFrequencyDetailID)
SELECT 19, 1		-- (No Alerts)
UNION SELECT 19,4	-- Sunday
UNION SELECT 19,5	-- Monday
UNION SELECT 19,6	-- Tuesday
UNION SELECT 19,7	-- Wednesday
UNION SELECT 19,8	-- Thursday
UNION SELECT 19,9	-- Friday
UNION SELECT 19,10	-- Saturday

--INSERT INTO dbo.SubscriptionTypeSubscriptionFrequencyDetail
--(SubscriptionTypeID, SubscriptionFrequencyDetailID)
--SELECT 20, 1		-- (No Alerts)
--UNION SELECT 20,3	-- Always
----UNION SELECT 20,11	-- Monthly First Weekday after the 10th

--
-- Update 4 Members with blank email addresses
--

UPDATE dbo.Member
SET EmailAddress='jknaggs@bergerchevy.com'
WHERE Login='jknaggs'
AND FirstName='John'
AND LastName='Knaggs'
AND LEN(EmailAddress)=0
AND MemberID=108046

UPDATE dbo.Member
SET EmailAddress='jim.haertzen@lutherbrookdalehonda.com'
WHERE Login='jhaertzen'
AND FirstName='Jim'
AND LastName='haertzen'
AND LEN(EmailAddress)=0
AND MemberID=110360

UPDATE dbo.Member
SET EmailAddress='dan.moen@wbacura.com'
WHERE Login='dmoen'
AND FirstName='Dan'
AND LastName='Moen'
AND LEN(EmailAddress)=0
AND MemberID=110368

UPDATE dbo.Member
SET EmailAddress='greggmiddlekauff@hotmail.com'
WHERE Login='gmiddlekauff'
AND FirstName='Gregg'
AND LastName='middlekauff'
AND LEN(EmailAddress)=0
AND MemberID=116139


--
-- Insert Subscription Data
--

Insert into dbo.Subscriptions(SubscriberID, SubscriberTypeID, SubscriptionTypeID, SubscriptionDeliveryTypeID, SubscriptionFrequencyDetailID, BusinessUnitID) 
SELECT 100905, 1, 16, 1, 7, 100650 
UNION SELECT 100905, 1, 17, 1, 7, 100650 
UNION SELECT 100905, 1, 18, 1, 7, 100650 
UNION SELECT 100905, 1, 19, 1, 7, 100650 
UNION SELECT 101315, 1, 18, 1, 7, 102715 
UNION SELECT 101315, 1, 19, 1, 7, 102715 
UNION SELECT 101315, 1, 17, 1, 7, 102715 
UNION SELECT 101315, 1, 16, 1, 7, 102715 
UNION SELECT 101625, 1, 17, 1, 9, 100506 
UNION SELECT 101625, 1, 16, 1, 9, 100506 
UNION SELECT 101625, 1, 18, 1, 9, 100506 
UNION SELECT 101625, 1, 19, 1, 9, 100506 
UNION SELECT 101625, 1, 19, 1, 9, 100507 
UNION SELECT 101625, 1, 18, 1, 9, 100507 
UNION SELECT 101625, 1, 17, 1, 9, 100507 
UNION SELECT 101625, 1, 16, 1, 9, 100507 
UNION SELECT 101627, 1, 16, 1, 9, 100506 
UNION SELECT 101627, 1, 17, 1, 9, 100506 
UNION SELECT 101627, 1, 18, 1, 9, 100506 
UNION SELECT 101627, 1, 19, 1, 9, 100506 
UNION SELECT 101627, 1, 19, 1, 9, 100507 
UNION SELECT 101627, 1, 18, 1, 9, 100507 
UNION SELECT 101627, 1, 17, 1, 9, 100507 
UNION SELECT 101627, 1, 16, 1, 9, 100507 
UNION SELECT 101713, 1, 16, 1, 7, 101885 
UNION SELECT 101713, 1, 17, 1, 7, 101885 
UNION SELECT 101713, 1, 18, 1, 7, 101885 
UNION SELECT 101713, 1, 19, 1, 7, 101885 
UNION SELECT 102348, 1, 18, 1, 7, 100834 
UNION SELECT 102348, 1, 19, 1, 7, 100834 
UNION SELECT 102348, 1, 17, 1, 7, 100834 
UNION SELECT 102348, 1, 16, 1, 7, 100834 
UNION SELECT 102612, 1, 16, 1, 7, 100968 
UNION SELECT 102612, 1, 17, 1, 7, 100968 
UNION SELECT 102612, 1, 18, 1, 7, 100968 
UNION SELECT 102612, 1, 19, 1, 7, 100968 
UNION SELECT 102612, 1, 19, 1, 7, 100969 
UNION SELECT 102612, 1, 18, 1, 7, 100969 
UNION SELECT 102612, 1, 17, 1, 7, 100969 
UNION SELECT 102612, 1, 16, 1, 7, 100969 
UNION SELECT 103294, 1, 16, 1, 7, 101005 
UNION SELECT 103294, 1, 17, 1, 7, 101005 
UNION SELECT 103294, 1, 18, 1, 7, 101005 
UNION SELECT 103294, 1, 19, 1, 7, 101005 
UNION SELECT 103294, 1, 19, 1, 7, 103296 
UNION SELECT 103294, 1, 18, 1, 7, 103296 
UNION SELECT 103294, 1, 17, 1, 7, 103296 
UNION SELECT 103294, 1, 16, 1, 7, 103296 
UNION SELECT 106541, 1, 16, 1, 7, 102189 
UNION SELECT 106541, 1, 17, 1, 7, 102189 
UNION SELECT 106541, 1, 18, 1, 7, 102189 
UNION SELECT 106541, 1, 19, 1, 7, 102189 
UNION SELECT 106541, 1, 19, 1, 7, 102488 
UNION SELECT 106541, 1, 18, 1, 7, 102488 
UNION SELECT 106541, 1, 17, 1, 7, 102488 
UNION SELECT 106541, 1, 16, 1, 7, 102488 
UNION SELECT 106541, 1, 16, 1, 7, 102489 
UNION SELECT 106541, 1, 17, 1, 7, 102489 
UNION SELECT 106541, 1, 18, 1, 7, 102489 
UNION SELECT 106541, 1, 19, 1, 7, 102489 
UNION SELECT 106675, 1, 19, 1, 7, 101739 
UNION SELECT 106675, 1, 18, 1, 7, 101739 
UNION SELECT 106675, 1, 17, 1, 7, 101739 
UNION SELECT 106675, 1, 16, 1, 7, 101739 
UNION SELECT 106718, 1, 16, 1, 7, 100957 
UNION SELECT 106718, 1, 17, 1, 7, 100957 
UNION SELECT 106718, 1, 18, 1, 7, 100957 
UNION SELECT 106718, 1, 19, 1, 7, 100957 
UNION SELECT 108046, 1, 19, 1, 7, 101885 
UNION SELECT 108046, 1, 18, 1, 7, 101885 
UNION SELECT 108046, 1, 17, 1, 7, 101885 
UNION SELECT 108046, 1, 16, 1, 7, 101885 
UNION SELECT 108288, 1, 16, 1, 7, 102095 
UNION SELECT 108288, 1, 17, 1, 7, 102095 
UNION SELECT 108288, 1, 18, 1, 7, 102095 
UNION SELECT 108288, 1, 19, 1, 7, 102095 
UNION SELECT 109079, 1, 19, 1, 7, 102488 
UNION SELECT 109079, 1, 18, 1, 7, 102488 
UNION SELECT 109079, 1, 17, 1, 7, 102488 
UNION SELECT 109079, 1, 16, 1, 7, 102488 
UNION SELECT 109487, 1, 16, 1, 7, 102337 
UNION SELECT 109487, 1, 17, 1, 7, 102337 
UNION SELECT 109487, 1, 18, 1, 7, 102337 
UNION SELECT 109487, 1, 19, 1, 7, 102337 
UNION SELECT 109548, 1, 19, 1, 7, 102819 
UNION SELECT 109548, 1, 18, 1, 7, 102819 
UNION SELECT 109548, 1, 17, 1, 7, 102819 
UNION SELECT 109548, 1, 16, 1, 7, 102819 
UNION SELECT 109922, 1, 16, 1, 7, 100957 
UNION SELECT 109922, 1, 17, 1, 7, 100957 
UNION SELECT 109922, 1, 18, 1, 7, 100957 
UNION SELECT 109922, 1, 19, 1, 7, 100957 
UNION SELECT 110360, 1, 19, 1, 7, 102189 
UNION SELECT 110360, 1, 18, 1, 7, 102189 
UNION SELECT 110360, 1, 17, 1, 7, 102189 
UNION SELECT 110360, 1, 16, 1, 7, 102189 
UNION SELECT 110368, 1, 16, 1, 7, 102489 
UNION SELECT 110368, 1, 17, 1, 7, 102489 
UNION SELECT 110368, 1, 18, 1, 7, 102489 
UNION SELECT 110368, 1, 19, 1, 7, 102489 
UNION SELECT 110735, 1, 19, 1, 7, 102450 
UNION SELECT 110735, 1, 18, 1, 7, 102450 
UNION SELECT 110735, 1, 17, 1, 7, 102450 
UNION SELECT 110735, 1, 16, 1, 7, 102450 
UNION SELECT 110737, 1, 16, 1, 7, 102450 
UNION SELECT 110737, 1, 17, 1, 7, 102450 
UNION SELECT 110737, 1, 18, 1, 7, 102450 
UNION SELECT 110737, 1, 19, 1, 7, 102450 
UNION SELECT 112385, 1, 19, 1, 7, 102758 
UNION SELECT 112385, 1, 18, 1, 7, 102758 
UNION SELECT 112385, 1, 17, 1, 7, 102758 
UNION SELECT 112385, 1, 16, 1, 7, 102758 
UNION SELECT 112527, 1, 16, 1, 7, 102774 
UNION SELECT 112527, 1, 17, 1, 7, 102774 
UNION SELECT 112527, 1, 18, 1, 7, 102774 
UNION SELECT 112527, 1, 19, 1, 7, 102774 
UNION SELECT 112701, 1, 19, 1, 7, 102819 
UNION SELECT 112701, 1, 18, 1, 7, 102819 
UNION SELECT 112701, 1, 17, 1, 7, 102819 
UNION SELECT 112701, 1, 16, 1, 7, 102819 
UNION SELECT 113105, 1, 16, 1, 7, 102884 
UNION SELECT 113105, 1, 17, 1, 7, 102884 
UNION SELECT 113105, 1, 18, 1, 7, 102884 
UNION SELECT 113105, 1, 19, 1, 7, 102884 
UNION SELECT 113780, 1, 19, 1, 7, 102958 
UNION SELECT 113780, 1, 18, 1, 7, 102958 
UNION SELECT 113780, 1, 17, 1, 7, 102958 
UNION SELECT 113780, 1, 16, 1, 7, 102958 
UNION SELECT 116093, 1, 16, 1, 7, 100834 
UNION SELECT 116093, 1, 17, 1, 7, 100834 
UNION SELECT 116093, 1, 19, 1, 7, 100834 
UNION SELECT 116093, 1, 18, 1, 7, 100834 
UNION SELECT 116139, 1, 18, 1, 7, 103538 
UNION SELECT 116139, 1, 19, 1, 7, 103538 
UNION SELECT 116139, 1, 17, 1, 7, 103538 
UNION SELECT 116139, 1, 16, 1, 7, 103538