IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Extract.DealerConfiguration#GermainHack') AND type in (N'P', N'PC'))
DROP PROCEDURE Extract.DealerConfiguration#GermainHack
GO

BEGIN TRY
	------------------------------------------------------------------------------------------------
	--	Turn off any dealers set to MiniGID
	------------------------------------------------------------------------------------------------
	
	EXEC sp_map_exec 'EXEC IMT.Extract.DealerConfiguration#SetFlag @BusinessUnitID = ?, @DestinationName = ''AULtec'', @FlagName = ''MiniGID'', @Value = 0', 
	'SELECT BusinessUnitID FROM	IMT.Extract.DealerConfiguration#ExtendedProperties WHERE MiniGID = 1',0
END TRY
BEGIN CATCH
	PRINT 'CATCH ERROR FOR SCRATCH BUILD'
END CATCH
	
------------------------------------------------------------------------------------------------
--	MiniGID no longer req'd.  Replace w/ExportKelleyConsumerValue
------------------------------------------------------------------------------------------------

UPDATE	DCF
SET	Name		= 'ExportKelleyConsumerValue', 
	Description	= 'Flags whether the Kelley Consumer Value is exported in the GID'

FROM	Extract.DealerConfigurationFlag DCF
WHERE	Name = 'MiniGID'


BEGIN TRY	
	------------------------------------------------------------------------------------------------
	--	Set for initial dealer
	------------------------------------------------------------------------------------------------


	EXEC Extract.DealerConfiguration#SetFlag @BusinessUnitID = 105855, @DestinationName = 'AULtec', @FlagName = 'ExportKelleyConsumerValue', @Value = 1
END TRY
BEGIN CATCH
	PRINT 'CATCH ERROR FOR SCRATCH BUILD'
END CATCH