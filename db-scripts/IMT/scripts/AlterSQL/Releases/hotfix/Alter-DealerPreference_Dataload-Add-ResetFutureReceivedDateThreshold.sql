ALTER TABLE dbo.DealerPreference_Dataload DROP COLUMN SetFutureReceivedDateToPresentDate


ALTER TABLE dbo.DealerPreference_Dataload ADD ResetFutureReceivedDateThreshold TINYINT NULL CONSTRAINT DF_DealerPreference_Dataload__ResetFutureReceivedDateThreshold DEFAULT (0)
GO

UPDATE	dbo.DealerPreference_Dataload
SET	ResetFutureReceivedDateThreshold = CASE WHEN BusinessUnitID = 102651 THEN 90 ELSE 0 END
GO

ALTER TABLE dbo.DealerPreference_Dataload ALTER COLUMN ResetFutureReceivedDateThreshold TINYINT NOT NULL 
GO
