IF (EXISTS(SELECT 1 FROM sys.indexes WHERE NAME = 'ix_RepriceExport__RepriceEventId' AND OBJECT_ID = OBJECT_ID('dbo.RepriceExport')))
	DROP INDEX dbo.RepriceExport.ix_RepriceExport__RepriceEventId
GO
CREATE INDEX ix_RepriceExport__RepriceEventId ON dbo.RepriceExport (RepriceEventId)
