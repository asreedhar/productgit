
DECLARE @UserName  VARCHAR(20)
DECLARE @MemberID INT
SET @UserName ='dnelson'

SELECT @MemberID =MemberID
FROM	dbo.Member
WHERE	Login =@UserName



INSERT
INTO	Carfax.Account_Member
	(AccountID,
	MemberID,
	InsertUser,
	InsertDate)
SELECT	A.AccountID,
	M.MemberID,
	@MemberID,
	GETDATE()
FROM	Carfax.Account_Dealer AD
JOIN	Carfax.Account A
ON	AD.AccountID =A.AccountID
JOIN	IMT.dbo.BusinessUnit B
ON	AD.DealerID =B.BusinessUnitID
JOIN	IMT.dbo.MemberAccess MA
ON	MA.BusinessUnitID =B.BusinessUnitID
JOIN	IMT.dbo.Member M
ON	MA.MEmberID =M.MemberID
AND	M.Active =1
LEFT
JOIN	Carfax.Account_Member AM
ON	AM.MemberID =M.MemberID
AND	AM.AccountID = AD.AccountID
WHERE	AM.MemberID IS NULL


INSERT
INTO	AutoCheck.Account_Member
	(AccountID,
	MemberID,
	InsertUser,
	InsertDate)
SELECT	A.AccountID,
	M.MemberID,
	@MemberID,
	GETDATE()
FROM	AutoCheck.Account_Dealer AD
JOIN	AutoCheck.Account A
ON	AD.AccountID =A.AccountID
JOIN	IMT.dbo.BusinessUnit B
ON	AD.DealerID =B.BusinessUnitID
JOIN	IMT.dbo.MemberAccess MA
ON	MA.BusinessUnitID =B.BusinessUnitID
JOIN	IMT.dbo.Member M
ON	MA.MEmberID =M.MemberID
AND	M.Active =1
LEFT
JOIN	AutoCheck.Account_Member AM
ON	AM.MemberID =M.MemberID
AND	AM.AccountID = AD.AccountID
WHERE	AM.MemberID IS NULL

GO