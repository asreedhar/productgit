/****************************************************************************************
*
*	Purpose:	Elite Ops Data Driven Subscription Setup Wave 5
*
*	Date:		5/7/2010
*
*	Databases:	IMT (data only)
*
*****************************************************************************************/

CREATE TABLE #EliteOps (SubscriberID INT,
                                                SubscriberTypeID INT,
                                                SubscriptionTypeID INT,
                                                SubscriptionDeliveryTypeID INT,
                                                BusinessUnitID INT,
                                                SubscriptionFrequencyDetailID INT
                                                )

BULK INSERT #EliteOps from '\\proddb01sql\C$\EliteOpsWave2FinalList.txt'
     with
      (
        datafiletype = 'char'
       ,fieldterminator = ','
       ,firstrow = 1
       ,rows_per_batch  = 10000
       ,maxErrors       = 0
       ,tablock
      )
      


INSERT INTO IMT.dbo.Subscriptions
(
SubscriberID ,SubscriberTypeID ,SubscriptionTypeID ,SubscriptionDeliveryTypeID ,BusinessUnitID ,SubscriptionFrequencyDetailID
)
SELECT SubscriberID ,SubscriberTypeID ,SubscriptionTypeID ,SubscriptionDeliveryTypeID ,BusinessUnitID ,SubscriptionFrequencyDetailID FROM #EliteOps

DROP TABLE #EliteOps

