-----------------------------------------------------------------------------------------------------------------------------
--	BookoutThirdPartyCategories
-----------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------
--	DELETE duplicates from some flustercluck back in the day
------------------------------------------------------------------------


SELECT	BookoutThirdPartyCategoryID
INTO	#delete
FROM	dbo.BookoutThirdPartyCategories BTPC
	INNER JOIN (	SELECT	BookoutID, ThirdPartyCategoryID, MAX(BTPC.BookoutThirdPartyCategoryID) MaxBookoutThirdPartyCategoryID
			FROM	dbo.BookoutThirdPartyCategories BTPC
			GROUP
			BY	BookoutID, ThirdPartyCategoryID
			HAVING	COUNT(*) > 1
			) D ON BTPC.BookoutID = D.BookoutID AND BTPC.ThirdPartyCategoryID = D.ThirdPartyCategoryID
				AND BookoutThirdPartyCategoryID <> MaxBookoutThirdPartyCategoryID

DELETE	BV
FROM	dbo.BookoutValues BV
	INNER JOIN  #delete D ON BV.BookoutThirdPartyCategoryID = D.BookoutThirdPartyCategoryID
	
DELETE	BTPC
FROM	dbo.BookoutThirdPartyCategories BTPC
	INNER JOIN  #delete D ON BTPC.BookoutThirdPartyCategoryID = D.BookoutThirdPartyCategoryID	


------------------------------------------------------------------------
--	Reorg the indexes (indices? wha'ev)
------------------------------------------------------------------------

ALTER TABLE dbo.BookoutValues DROP CONSTRAINT FK_BookoutValues_BookoutThirdPartyCategories

ALTER TABLE dbo.BookoutThirdPartyCategories DROP CONSTRAINT PK_BookoutThirdPartyCategories
DROP INDEX dbo.BookoutThirdPartyCategories.IX_BookoutThirdPartyCategories_BookoutID

CREATE UNIQUE CLUSTERED INDEX IXC_BookoutThirdPartyCategories__BookoutIDThirdPartyCategoryID ON dbo.BookoutThirdPartyCategories(BookoutID, ThirdPartyCategoryID)
GO

ALTER TABLE dbo.BookoutThirdPartyCategories ADD CONSTRAINT PK_BookoutThirdPartyCategories PRIMARY KEY NONCLUSTERED (BookoutThirdPartyCategoryID)

ALTER TABLE dbo.BookoutValues WITH CHECK ADD CONSTRAINT FK_BookoutValues__BookoutThirdPartyCategories FOREIGN KEY(BookoutThirdPartyCategoryID) REFERENCES BookoutThirdPartyCategories (BookoutThirdPartyCategoryID)

GO


if exists (select * from dbo.sysindexes where name = 'IX_BookoutID' and id = object_id(N'dbo.BookoutThirdPartyCategories'))
	DROP INDEX dbo.BookoutThirdPartyCategories.IX_BookoutID



-----------------------------------------------------------------------------------------------------------------------------
--	DealerInsightTargetPreference
-----------------------------------------------------------------------------------------------------------------------------

ALTER TABLE dbo.DealerInsightTargetPreference DROP CONSTRAINT PK_DealerInsightTargetPreference
ALTER TABLE [dbo].[DealerInsightTargetPreference] ADD CONSTRAINT [PK_DealerInsightTargetPreference] PRIMARY KEY NONCLUSTERED  ([DealerInsightTargetPreferenceID]) ON [DATA]
CREATE UNIQUE CLUSTERED INDEX IXC_DealerInsightTargetPreference__BusinessUnitInsightIDs ON dbo.DealerInsightTargetPreference(BusinessUnitID, InsightTargetID)
