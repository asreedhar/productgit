USE IMT
GO
CREATE TABLE [Utility].[ExecStatus](
	[ExecStatusId] [tinyint] NOT NULL  CONSTRAINT [PK_ExecStatus] PRIMARY KEY CLUSTERED ,
	[ExecStatusName] [varchar](20) NOT NULL,
) 
GO
CREATE TABLE [Utility].[ExecHistory](
	[ExecQueueId] [int] NOT NULL  CONSTRAINT [PK_ExecHistory] PRIMARY KEY CLUSTERED,
	[BusinessUnitId] [int] NOT NULL,
	[ExecStatusId] [tinyint] NOT NULL CONSTRAINT [FK_ExecQueueExecHistory_ExecStatus] FOREIGN KEY REFERENCES [Utility].[ExecStatus] ([ExecStatusId]),
	[ExecDate] [smalldatetime] NOT NULL,
	[QueueDate] [smalldatetime] NOT NULL,
	[ExitCode] [int] NOT NULL,
)
CREATE TABLE [Utility].[ExecQueue](
	[ExecQueueId] [int] IDENTITY(1,1) NOT NULL CONSTRAINT [PK_ExecQueue] PRIMARY KEY CLUSTERED ,
	[BusinessUnitId] [int] NOT NULL,
	[ExecStatusId] [tinyint] NOT NULL CONSTRAINT [DF_ExecQueue_ExecStatusId]  DEFAULT ((1))  CONSTRAINT [FK_ExecQueue_ExecStatus] FOREIGN KEY REFERENCES [Utility].[ExecStatus] ([ExecStatusId]),
	[QueueDate] [smalldatetime] NOT NULL CONSTRAINT [DF_ExecQueue_QueueDate]  DEFAULT (getdate()),
)
GO

INSERT INTO Utility.ExecStatus ( ExecStatusId, ExecStatusName ) VALUES	( 1, 'Pending' )
INSERT INTO Utility.ExecStatus ( ExecStatusId, ExecStatusName ) VALUES	( 2, 'In Process' )
INSERT INTO Utility.ExecStatus ( ExecStatusId, ExecStatusName ) VALUES	( 3, 'Complete' )
INSERT INTO Utility.ExecStatus ( ExecStatusId, ExecStatusName ) VALUES	( 4, 'Error' )