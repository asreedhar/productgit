
-- These self-referential foreign key constraints were created automatically and unnecessarily courtesy of Management Studio (BUGZID 24455)!

if exists (select name from sysindexes where name ='FK_PhotoTransfers_PhotoTransfers') 
	ALTER TABLE [dbo].[PhotoTransfers]  DROP  CONSTRAINT [FK_PhotoTransfers_PhotoTransfers];

if exists (select name from sysindexes where name ='FK_PhotoTransfers_PhotoTransfers1') 
	ALTER TABLE [dbo].[PhotoTransfers]  DROP CONSTRAINT [FK_PhotoTransfers_PhotoTransfers1];



