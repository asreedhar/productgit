--EXEC Utility.dbo.ScriptData @schema = 'Extract', -- varchar(100)
--    @table = 'Destination', -- varchar(100)
--    @Where = ' WHERE Name=''RapidRecon''', -- varchar(max)
--    @DB = 'IMT', -- varchar(100)
--    @IfNotExists = 1, -- bit
--    @Update = 1, -- bit
--    @IDInsert = NULL, -- bit
--    @UseTransaction = NULL, -- bit
--    @ScriptDatabase = 1, -- bit
--    @Debug = NULL -- bit


/*
IMT.Extract.Destination
*/
IF NOT EXISTS (SELECT 1 FROM IMT.Extract.Destination WHERE 1=1 AND [Name] = 'RapidRecon' )
	INSERT IMT.Extract.Destination ([Name],[DefaultIncludeHistorical])
	VALUES ('RapidRecon',1)
ELSE
	UPDATE IMT.Extract.Destination
	SET  [DefaultIncludeHistorical] = 1 
	WHERE 1=1  AND [Name] = 'RapidRecon' 


/*
IMT.Extract.DealerConfigurationFlag
Disabled these inserts because flag settings are inherited in view from aultecmaster.

*/
--IF NOT EXISTS (SELECT 1 FROM IMT.Extract.DealerConfigurationFlag WHERE 1=1 AND [DestinationName] = 'RapidRecon'  AND [Name] = 'ExcludeLotImageURLs' )
--	INSERT IMT.Extract.DealerConfigurationFlag ([DestinationName],[Name],[BitPosition],[Description])
--	VALUES ('RapidRecon','ExcludeLotImageURLs',3,'Flags whether to exclude the ImageURLs from Lot for a dealer')
--ELSE
--	UPDATE IMT.Extract.DealerConfigurationFlag
--	SET  [BitPosition] = 3,[Description] = 'Flags whether to exclude the ImageURLs from Lot for a dealer' 
--	WHERE 1=1  AND [DestinationName] = 'RapidRecon'   AND [Name] = 'ExcludeLotImageURLs' 
--IF NOT EXISTS (SELECT 1 FROM IMT.Extract.DealerConfigurationFlag WHERE 1=1 AND [DestinationName] = 'RapidRecon'  AND [Name] = 'ExportStockPhotos' )
--	INSERT IMT.Extract.DealerConfigurationFlag ([DestinationName],[Name],[BitPosition],[Description])
--	VALUES ('RapidRecon','ExportStockPhotos',0,'Flags whether to export StockPhotoURL(s) from VDS for a dealer')
--ELSE
--	UPDATE IMT.Extract.DealerConfigurationFlag
--	SET  [BitPosition] = 0,[Description] = 'Flags whether to export StockPhotoURL(s) from VDS for a dealer' 
--	WHERE 1=1  AND [DestinationName] = 'RapidRecon'   AND [Name] = 'ExportStockPhotos' 

/*
IMT.Extract.DealerConfiguration
*/
DECLARE @BUID INT = 106126,@flags BINARY(2) = 0x0000
DECLARE @table TABLE (buid INT)
INSERT @table
VALUES  (106126),(101772),(102198),(102187)

SELECT TOP 1 @BUID=buid FROM @table
WHILE @BUID IS NOT NULL
BEGIN
	IF NOT EXISTS (SELECT 1 FROM IMT.Extract.DealerConfiguration WHERE 1=1 AND [BusinessUnitID] = @BUID  AND [DestinationName] = 'RapidRecon' )
		INSERT IMT.Extract.DealerConfiguration ([BusinessUnitID],[DestinationName],[ExternalIdentifier],[StartDate],[EndDate],[Active],[IncludeHistorical],[InsertDate],[InsertUser],[UpdateDate],[UpdateUser],[Flags])
		VALUES (@BUID,'RapidRecon',CAST(@buid AS VARCHAR(10)),'May  9 2014 12:00AM',NULL,1,1,'May  9 2014  3:17PM','bfultz','May  9 2014  3:17PM','bfultz', @flags )
	ELSE
		UPDATE IMT.Extract.DealerConfiguration
		SET  [ExternalIdentifier] = CAST(@buid AS VARCHAR(10)),[StartDate] = 'May  9 2014 12:00AM',[EndDate] = NULL,[Active] = 1,[IncludeHistorical] = 1,[InsertDate] = 'May  9 2014  3:17PM',[InsertUser] = 'bfultz',[UpdateDate] = 'May  9 2014  3:17PM',[UpdateUser] = 'bfultz',[Flags] =   @flags  
		WHERE 1=1  AND [BusinessUnitID] = @BUID   AND [DestinationName] = 'RapidRecon' 
	
	DELETE @table WHERE buid=@BUID
	SET @BUID = NULL
	SELECT TOP 1 @BUID=buid FROM @table  
END	

--SELECT * FROM imt.Extract.Destination WHERE Name='RapidRecon'

--SELECT * FROM imt.Extract.DealerConfigurationFlag WHERE  DestinationName='RapidRecon'

--SELECT bu.BusinessUnit,bu.Active ,dc.* FROM imt.Extract.DealerConfiguration dc
--INNER JOIN imt.dbo.BusinessUnit bu ON bu.BusinessUnitID=dc.BusinessUnitID
--WHERE DestinationName='RapidRecon'
