if exists (select * from dbo.sysobjects where id = object_id(N'[windowSticker].[PrintBatchAccessLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table [windowSticker].[PrintBatchAccessLog]
GO

CREATE TABLE windowSticker.PrintBatchAccessLog
(
	PrintBatchAccessLogId int not null identity(1,1),
	PrintBatchId int not null,
	AccessBy varchar(80) not null,
	AccessOn smalldatetime not null default getdate()
)
go

ALTER TABLE windowSticker.PrintBatchAccessLog
	ADD CONSTRAINT PK_PrintBatchAccessLog
	PRIMARY KEY NONCLUSTERED (PrintBatchAccessLogId)
GO

ALTER TABLE windowSticker.PrintBatchAccessLog
	ADD CONSTRAINT FK_PrintBatchAccessLog_PrintBatch
	FOREIGN KEY (PrintBatchId)
	REFERENCES WindowSticker.PrintBatch(PrintBatchId)
GO

CREATE INDEX IX_PrintBatchAccessLog_PrintBatchId ON windowSticker.PrintBatchAccessLog(PrintBatchId)
go


