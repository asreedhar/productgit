if exists (select * from dbo.sysobjects where id = object_id(N'windowSticker.temp_PrintLog') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table windowSticker.temp_PrintLog
go

sp_rename 'windowSticker.PrintLog', 'temp_PrintLog'
go	

CREATE TABLE windowSticker.PrintLog
(
	PrintLogId			int identity(1,1) not null,
	InventoryId			int not null,
	TemplateTypeId		tinyint	not null,
	TemplateId			int null,
	AutoPrintTemplate	bit not null default 0,
	PrintDate			smalldatetime  not null default getdate()
)
go

INSERT INTO windowSticker.PrintLog
	(InventoryID, TemplateTypeId, PrintDate)
SELECT InventoryId, TemplateTypeId, PrintDate
	FROM windowSticker.temp_PrintLog
go

DROP TABLE windowSticker.temp_PrintLog
go

ALTER TABLE windowSticker.PrintLog
	ADD CONSTRAINT PK_PrintLog
	PRIMARY KEY NONCLUSTERED (PrintLogId)
go

CREATE CLUSTERED INDEX IX_PrintLog_InventoryId ON windowSticker.PrintLog(InventoryId)
go
