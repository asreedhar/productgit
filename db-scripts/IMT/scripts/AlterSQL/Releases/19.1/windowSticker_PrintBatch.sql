if exists (select * from dbo.sysobjects where id = object_id(N'[windowSticker].[PrintBatch]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table [windowSticker].[PrintBatch]
GO

CREATE TABLE [windowSticker].[PrintBatch]
(
	PrintBatchId int identity(1,1) not null,
	BusinessUnitId	int not null,
	PrintFileName	int not null,
	MessageId		varchar(50) null,
	CreatedOn	smalldatetime  not null default getdate(),
	CreatedBy	varchar(80) null
)
go

ALTER TABLE [windowSticker].[PrintBatch]
	ADD CONSTRAINT PK_PrintBatch
	PRIMARY KEY NONCLUSTERED (PrintBatchId)
GO

CREATE INDEX IX_PrintBatch_BusinessUnitId ON windowSticker.PrintBatch(BusinessUnitId)
go
