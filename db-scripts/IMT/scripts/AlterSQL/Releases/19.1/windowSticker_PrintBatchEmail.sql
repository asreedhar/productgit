if exists (select * from dbo.sysobjects where id = object_id(N'[windowSticker].[PrintBatchEmail]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table [windowSticker].[PrintBatchEmail]
GO

CREATE TABLE windowSticker.PrintBatchEmail
(
	PrintBatchEmailId int not null identity(1,1),
	PrintBatchId int not null,
	Email varchar(80) not null,
	EmailBounced bit not null default 0,
	CreatedOn smalldatetime not null default getdate()
)
go

ALTER TABLE windowSticker.PrintBatchEmail
	ADD CONSTRAINT PK_PrintBatchEmail
	PRIMARY KEY NONCLUSTERED (PrintBatchEmailId)
GO

ALTER TABLE windowSticker.PrintBatchEmail
	ADD CONSTRAINT FK_PrintBatchEmail_PrintBatch
	FOREIGN KEY (PrintBatchId)
	REFERENCES WindowSticker.PrintBatch(PrintBatchId)
GO

CREATE INDEX IX_PrintBatchEmail_PrintBatchId ON windowSticker.PrintBatchEmail(PrintBatchId)
go


