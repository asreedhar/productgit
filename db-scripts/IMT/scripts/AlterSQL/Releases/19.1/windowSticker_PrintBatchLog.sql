if exists (select * from dbo.sysobjects where id = object_id(N'windowSticker.PrintBatchLog') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table windowSticker.PrintBatchLog
go


CREATE TABLE windowSticker.PrintBatchLog
(
	PrintBatchId		int not null,
	PrintLogId			int not null
)
go

ALTER TABLE windowSticker.PrintBatchLog
	ADD CONSTRAINT PK_PrintLogBatch
	PRIMARY KEY (PrintBatchId, PrintLogId)
go

ALTER TABLE windowSticker.PrintBatchLog
	ADD CONSTRAINT FK_PrintLogBatch_PrintLog
	FOREIGN KEY (PrintLogId)
	REFERENCES windowSticker.PrintLog (PrintLogID)
go


ALTER TABLE windowSticker.PrintBatchLog
	ADD CONSTRAINT FK_PrintLogBatch_PrintBatch
	FOREIGN KEY (PrintBatchId)
	REFERENCES windowSticker.PrintBatch(PrintBatchId)
go
