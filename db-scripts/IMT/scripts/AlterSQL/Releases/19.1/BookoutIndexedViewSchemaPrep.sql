USE [IMT]
GO

IF NOT EXISTS(SELECT 1 FROM sys.schemas WHERE name='DW')
	EXEC sp_executesql N'CREATE SCHEMA DW'
GO


--DROP TABLE [dbo].[BookoutValues_tmp]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BookoutValues_tmp](
	[BookoutValueID] [int] IDENTITY(1,1) NOT NULL,
	[BookoutThirdPartyCategoryID] [int] NOT NULL,
	[BookoutValueTypeID] [tinyint] NOT NULL,
	[ThirdPartySubCategoryID] [int] NULL,
	[Value] [int] NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NULL,
 CONSTRAINT [PK_BookoutValues_1] PRIMARY KEY NONCLUSTERED 
(
	[BookoutValueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [DATA]
)

CREATE CLUSTERED INDEX [IXC_BookoutValues_BookoutThirdPartyCategoryID_1] ON [dbo].[BookoutValues_tmp]
(
	[BookoutThirdPartyCategoryID] ASC,
	[BookoutValueTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)


ALTER TABLE [dbo].[BookoutValues_tmp] ADD  CONSTRAINT [DF__BookoutValues_DateCreated_1]  DEFAULT (getdate()) FOR [DateCreated]


ALTER TABLE [dbo].[BookoutValues_tmp]  WITH CHECK ADD  CONSTRAINT [FK_BookoutValues_BookoutThirdPartyCategories_1] FOREIGN KEY([BookoutThirdPartyCategoryID])
REFERENCES [dbo].[BookoutThirdPartyCategories] ([BookoutThirdPartyCategoryID])


ALTER TABLE [dbo].[BookoutValues_tmp] CHECK CONSTRAINT [FK_BookoutValues_BookoutThirdPartyCategories_1]


ALTER TABLE [dbo].[BookoutValues_tmp]  WITH CHECK ADD  CONSTRAINT [FK_BookoutValues_BookoutValueTypes_1] FOREIGN KEY([BookoutValueTypeID])
REFERENCES [dbo].[BookoutValueTypes] ([BookoutValueTypeID])


ALTER TABLE [dbo].[BookoutValues_tmp] CHECK CONSTRAINT [FK_BookoutValues_BookoutValueTypes_1]


ALTER TABLE [dbo].[BookoutValues_tmp]  WITH CHECK ADD  CONSTRAINT [FK_BookoutValues_ThirdPartySubCategory_1] FOREIGN KEY([ThirdPartySubCategoryID])
REFERENCES [dbo].[ThirdPartySubCategory] ([ThirdPartySubCategoryID])


ALTER TABLE [dbo].[BookoutValues_tmp] CHECK CONSTRAINT [FK_BookoutValues_ThirdPartySubCategory_1]

ALTER TABLE dbo.BookoutValues SWITCH TO dbo.BookoutValues_tmp

EXEC sp_rename 'dbo.BookoutValues','BookoutValues_old'
EXEC sp_rename 'dbo.BookoutValues_tmp','BookoutValues'
drop table BookoutValues_old

EXEC sp_rename 'DF__BookoutValues_DateCreated_1','DF_BookoutValues_DateCreated'
EXEC sp_rename 'PK_BookoutValues_1','PK_BookoutValues'
EXEC sp_rename 'FK_BookoutValues_BookoutThirdPartyCategories_1','FK_BookoutValues_BookoutThirdPartyCategories'
EXEC sp_rename 'FK_BookoutValues_BookoutValueTypes_1','FK_BookoutValues_BookoutValueTypes'
EXEC sp_rename 'FK_BookoutValues_ThirdPartySubCategory_1','FK_BookoutValues_ThirdPartySubCategory'
EXEC sp_rename 'dbo.BookoutValues.IXC_BookoutValues_BookoutThirdPartyCategoryID_1','IXC_BookoutValues_BookoutThirdPartyCategoryID','index'



IF EXISTS (SELECT 1 FROM sys.triggers t WHERE t.name='TR_U_Bookouts')
	DROP TRIGGER [dbo].[TR_U_Bookouts] 

