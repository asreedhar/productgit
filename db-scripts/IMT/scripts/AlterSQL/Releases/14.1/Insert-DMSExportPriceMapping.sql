SET IDENTITY_INSERT dbo.DMSExportPriceMapping ON

INSERT
INTO	dbo.DMSExportPriceMapping (DMSExportPriceMappingID, Description)
SELECT	31, 'INTERNET_PRICE'
UNION
SELECT	32, 'FLRPLN_INTRRATE'

SET IDENTITY_INSERT dbo.DMSExportPriceMapping OFF