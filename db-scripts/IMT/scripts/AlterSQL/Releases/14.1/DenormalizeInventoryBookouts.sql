BEGIN TRANSACTION

CREATE TABLE dbo.InventoryBookoutHistory (InventoryId INT NOT NULL, BookoutId INT NOT NULL, ThirdPartyID TINYINT NULL)
GO
ALTER TABLE InventoryBookouts ADD ThirdPartyId TINYINT NULL
GO
DECLARE	@Maxbookouts TABLE
(
  BusinessUnitId INT NOT NULL
, InventoryId INT NOT NULL
, ThirdPartyID TINYINT NOT NULL
, BookoutId INT NOT NULL
)

INSERT	INTO @MaxBookouts
		SELECT	I.BusinessUnitID
			  , I.InventoryID
			  , B.ThirdPartyID
			  , MAX(B.BookoutID)
		FROM	InventoryBookouts IB
				JOIN dbo.Inventory I ON IB.InventoryID = I.InventoryID
				JOIN Bookouts B ON IB.BookoutID = B.BookoutID
		GROUP BY I.BusinessUnitID
			  , I.InventoryID
			  , B.ThirdPartyID	
	
DELETE	InventoryBookouts
OUTPUT	deleted.InventoryID
	  , deleted.BookoutID
	  , null
		INTO dbo.InventoryBookoutHistory (InventoryID, BookoutID, ThirdPartyID)
FROM	dbo.InventoryBookouts a
		LEFT OUTER JOIN @MaxBookouts b ON a.InventoryID = b.InventoryId
										  AND a.BookoutID = b.BookoutID
WHERE	b.InventoryId IS NULL

UPDATE	ib
SET		ThirdPartyID = b.ThirdPartyID
FROM	dbo.InventoryBookouts ib
		INNER JOIN dbo.Bookouts b ON ib.BookoutID = b.BookoutID
GO
COMMIT TRANSACTION