IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[Certified].[CertifiedProgram_Make]') AND name = N'PK_Certified_CertifiedProgram_Make')
ALTER TABLE [Certified].[CertifiedProgram_Make] 
DROP CONSTRAINT [PK_Certified_CertifiedProgram_Make]
GO

ALTER TABLE [Certified].[CertifiedProgram_Make] 
ADD  CONSTRAINT [PK_Certified_CertifiedProgram_Make] PRIMARY KEY CLUSTERED ( MakeID asc, CertifiedProgramID asc )
GO


