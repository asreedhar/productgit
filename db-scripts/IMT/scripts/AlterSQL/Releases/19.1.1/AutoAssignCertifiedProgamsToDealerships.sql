declare @RC int
declare @OwnerHandle varchar(36)
declare @CertifiedProgramID int
declare @InsertUser varchar(80)
declare @OwnerCertifiedProgramID int

declare @i int, @max int
declare @dealership varchar(100), @programName varchar(50)

declare @parameters table(idx int identity(1,1), ownerHandle varchar(36), certifiedProgramId int, dealership varchar(100), programName varchar(50));

with 
MaxAdDealers as 
(
	-- Get all active Max Ad Dealers
	select *
	from imt.dbo.BusinessUnit bu
	where bu.Active = 1
	and exists
	(
		select *
		from imt.dbo.DealerUpgrade du 
		where du.BusinessUnitID = bu.BusinessUnitID
		and du.DealerUpgradeCD = 24
		and du.EffectiveDateActive = 1
	)
),
MaxDealershipMakes as
(
	-- Get New Cars in active inventory at the Max Ad Dealerships
	select d.BusinessUnitID, d.BusinessUnit, m.MakeID, mmg.Make, count(*) as NewCarsInInventory
	from MaxAdDealers d
	join fldw.dbo.InventoryActive ia on ia.BusinessUnitID = d.BusinessUnitID
	join fldw.dbo.Vehicle v on ia.BusinessUnitID = v.BusinessUnitID and ia.VehicleID = v.VehicleID
	join imt.dbo.MakeModelGrouping mmg on v.MakeModelGroupingID = mmg.MakeModelGroupingID
	join VehicleCatalog.Firstlook.Make m on mmg.Make = m.Make
	where ia.InventoryType = 1 -- new cars
	group by d.BusinessUnitID, d.BusinessUnit, m.MakeID, mmg.Make
	having count(*) > 1	-- there were some Chevy's at Chrysler Jeep Dodge stores, require more than 1 Make in inventory to make the assignment
)
insert into @parameters( ownerHandle, certifiedProgramId, dealership, programName )
select distinct o.Handle, cp.CertifiedProgramID, o.OwnerName, cp.Name
from MaxDealershipMakes mdm
join imt.Certified.CertifiedProgram_Make cpm  on mdm.MakeID = cpm.MakeID
join imt.Certified.CertifiedProgram cp on cpm.CertifiedProgramID = cp.CertifiedProgramID
join VehicleCatalog.Firstlook.Make m on cpm.MakeID = m.MakeID
join Market.pricing.Owner o on o.OwnerEntityID = mdm.BusinessUnitID and o.OwnerTypeID = 1
where not exists (
	-- dealership does not participate in this program for the new Makes in their inventory
	select *
	from imt.Certified.OwnerCertifiedProgram ocp
	where ocp.CertifiedProgramID = cp.CertifiedProgramID
	and ocp.OwnerId = o.OwnerID
);

select @i = 1, @max = max(idx) from @parameters

while @i <= @max
begin
	select @OwnerHandle = ownerHandle, @CertifiedProgramID = certifiedProgramId, @dealership = dealership, @programName = programName
	from @parameters
	where idx = @i
	
	print 'executing with @OwnerHandle = ' + @OwnerHandle + ', @CertifiedProgramId = ' + cast(@CertifiedProgramID as varchar) + ', @dealership = ' + @dealership + ', @programName = ' + @programName
	EXECUTE @RC = IMT.Certified.OwnerCertifiedProgramBenefitCollection#Create @OwnerHandle, @CertifiedProgramID ,@InsertUser = 'dhillis', @OwnerCertifiedProgramID = @OwnerCertifiedProgramID output
	print 'OwnerCertifiedProgramId ' + cast(@OwnerCertifiedProgramID as varchar) + ' created.'
	set @i = @i + 1
end