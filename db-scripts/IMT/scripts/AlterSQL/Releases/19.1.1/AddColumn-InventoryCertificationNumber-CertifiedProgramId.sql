alter table imt.dbo.Inventory_CertificationNumber
add CertifiedProgramId int;
GO

alter table imt.dbo.Inventory_CertificationNumber
add constraint FK_Inventory_CertificationNumber__CertifiedProgramId FOREIGN KEY ( CertifiedProgramId ) references IMT.Certified.CertifiedProgram(CertifiedProgramID)
GO

