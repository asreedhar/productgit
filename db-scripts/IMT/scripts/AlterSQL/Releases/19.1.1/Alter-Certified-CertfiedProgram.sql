alter table Certified.CertifiedProgram
add BusinessUnitId int null,
ProgramType as (case when [BusinessUnitId] IS NULL then (1) else (2) end)
