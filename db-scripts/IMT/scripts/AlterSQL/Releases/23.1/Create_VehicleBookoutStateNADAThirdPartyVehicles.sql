USE [IMT]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleBookoutStateNADAThirdPartyVehicles_ThirdPartyVehicles]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleBookoutStateNADAThirdPartyVehicles]'))
ALTER TABLE [dbo].[VehicleBookoutStateNADAThirdPartyVehicles] DROP CONSTRAINT [FK_VehicleBookoutStateNADAThirdPartyVehicles_ThirdPartyVehicles]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleBookoutStateNADAThirdPartyVehicles_VehicleBookoutStateNADA]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleBookoutStateNADAThirdPartyVehicles]'))
ALTER TABLE [dbo].[VehicleBookoutStateNADAThirdPartyVehicles] DROP CONSTRAINT [FK_VehicleBookoutStateNADAThirdPartyVehicles_VehicleBookoutStateNADA]
GO

USE [IMT]
GO

/****** Object:  Table [dbo].[VehicleBookoutStateNADAThirdPartyVehicles]    Script Date: 04/22/2015 17:38:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehicleBookoutStateNADAThirdPartyVehicles]') AND type in (N'U'))
DROP TABLE [dbo].[VehicleBookoutStateNADAThirdPartyVehicles]
GO

USE [IMT]
GO

/****** Object:  Table [dbo].[VehicleBookoutStateNADAThirdPartyVehicles]    Script Date: 04/22/2015 17:38:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VehicleBookoutStateNADAThirdPartyVehicles](
	[ThirdPartyVehicleID] [int] NOT NULL,
	[VehicleBookoutStateNADAID] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ThirdPartyVehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]

GO

ALTER TABLE [dbo].[VehicleBookoutStateNADAThirdPartyVehicles]  WITH CHECK ADD  CONSTRAINT [FK_VehicleBookoutStateNADAThirdPartyVehicles_ThirdPartyVehicles] FOREIGN KEY([ThirdPartyVehicleID])
REFERENCES [dbo].[ThirdPartyVehicles] ([ThirdPartyVehicleID])
GO

ALTER TABLE [dbo].[VehicleBookoutStateNADAThirdPartyVehicles] CHECK CONSTRAINT [FK_VehicleBookoutStateNADAThirdPartyVehicles_ThirdPartyVehicles]
GO

ALTER TABLE [dbo].[VehicleBookoutStateNADAThirdPartyVehicles]  WITH CHECK ADD  CONSTRAINT [FK_VehicleBookoutStateNADAThirdPartyVehicles_VehicleBookoutStateNADA] FOREIGN KEY([VehicleBookoutStateNADAID])
REFERENCES [dbo].[VehicleBookoutStateNADA] ([VehicleBookoutStateNADAID])
GO

ALTER TABLE [dbo].[VehicleBookoutStateNADAThirdPartyVehicles] CHECK CONSTRAINT [FK_VehicleBookoutStateNADAThirdPartyVehicles_VehicleBookoutStateNADA]
GO


