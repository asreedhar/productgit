USE [IMT]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VehicleBookoutStateNADA_BusinessUnit]') AND parent_object_id = OBJECT_ID(N'[dbo].[VehicleBookoutStateNADA]'))
ALTER TABLE [dbo].[VehicleBookoutStateNADA] DROP CONSTRAINT [FK_VehicleBookoutStateNADA_BusinessUnit]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__VehicleBo__DateC__4FEBDA35]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[VehicleBookoutStateNADA] DROP CONSTRAINT [DF__VehicleBo__DateC__4FEBDA35]
END

GO

USE [IMT]
GO

/****** Object:  Table [dbo].[VehicleBookoutStateNADA]    Script Date: 04/22/2015 17:39:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehicleBookoutStateNADA]') AND type in (N'U'))
DROP TABLE [dbo].[VehicleBookoutStateNADA]
GO

USE [IMT]
GO

/****** Object:  Table [dbo].[VehicleBookoutStateNADA]    Script Date: 04/22/2015 17:39:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[VehicleBookoutStateNADA](
	[VehicleBookoutStateNADAID] [int] IDENTITY(1,1) NOT NULL,
	[VIN] [varchar](17) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
 CONSTRAINT [PK_VehicleBookoutStateNADA] PRIMARY KEY CLUSTERED 
(
	[VehicleBookoutStateNADAID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [DATA]
) ON [DATA]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[VehicleBookoutStateNADA]  WITH CHECK ADD  CONSTRAINT [FK_VehicleBookoutStateNADA_BusinessUnit] FOREIGN KEY([BusinessUnitID])
REFERENCES [dbo].[BusinessUnit] ([BusinessUnitID])
GO

ALTER TABLE [dbo].[VehicleBookoutStateNADA] CHECK CONSTRAINT [FK_VehicleBookoutStateNADA_BusinessUnit]
GO

ALTER TABLE [dbo].[VehicleBookoutStateNADA] ADD  DEFAULT (getdate()) FOR [DateCreated]
GO


