CREATE TABLE [Distribution].[HomeNet_Error]
    (
      [ErrorID] [INT] NOT NULL
                      CONSTRAINT [PK_Distribution_HomeNet_Error] PRIMARY KEY CLUSTERED
    , [ErrorTypeID] [INT] NOT NULL
    , [ErrorMessage] [NVARCHAR](MAX) NULL
    )
GO
CREATE TABLE [Distribution].[HomeNet_ErrorType]
    (
      [ErrorTypeID] [INT] NOT NULL
                          CONSTRAINT [PK_Distribution_HomeNet_ErrorType] PRIMARY KEY CLUSTERED
    , [Description] [VARCHAR](2000) NOT NULL
    )

INSERT  INTO [Distribution].[HomeNet_ErrorType]
        ( [ErrorTypeID], [Description] )
VALUES  ( 0, 'Ok' ),
        ( 1, 'InvalidIntegrationToken' ),
        ( 2, 'IntegrationTokenNotFound' ),
        ( 3, 'InvalidPartner' ),
        ( 4, 'InvalidDealerId' ) ,
        ( 5, 'NoDealerFound' ),
        ( 6, 'NoUserPermissionFound' ),
        ( 7, 'NoVehiclesProvided' ),
        ( 8, 'InvalidUser' ),
        ( 9, 'UnknownError' )

GO

ALTER TABLE [Distribution].[HomeNet_Error]  WITH CHECK ADD  CONSTRAINT [FK_Distribution_HomeNetError__HomeNet_ErrorType] FOREIGN KEY([ErrorTypeID])
REFERENCES [Distribution].[HomeNet_ErrorType] ([ErrorTypeID])
GO

ALTER TABLE [Distribution].[HomeNet_Error]  WITH CHECK ADD  CONSTRAINT [FK_Distribution_HomeNetError__SubmissionError] FOREIGN KEY([ErrorID])
REFERENCES [Distribution].[SubmissionError] ([ErrorID])
GO

ALTER TABLE [Distribution].[HomeNet_Error] CHECK CONSTRAINT [FK_Distribution_HomeNetError__SubmissionError]
GO
