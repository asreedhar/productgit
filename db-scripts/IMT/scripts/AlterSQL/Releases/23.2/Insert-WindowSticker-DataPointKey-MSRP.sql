USE IMT;


IF NOT EXISTS ( SELECT * FROM WindowSticker.DataPointKey WHERE DataPointKeyId = 30 )
BEGIN
	INSERT INTO WindowSticker.DataPointKey( DataPointKeyId, [Key] )
	VALUES ( 30, 'MSRP' );
END
