/* query to clear the IMT reliable message queue for HAL Appraisal Review Workbooks  */
USE IMT
DELETE dbo.ReliableMessageQueue
WHERE	QueueName = 'offline_pdf_queue'