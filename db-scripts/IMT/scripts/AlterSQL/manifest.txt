Releases\10.0\MapMiniToBmw.sql
#
Releases\10.1\Update-Index-Strategy.sql
Releases\hotfix\Alter-IndexStrategy-20120127.sql
Releases\10.1\Update-Index-Strategy-2.sql
Releases\10.1\Alter-Member-PK.sql

Releases\10.2\AdPricingAPIAccounts.sql

Releases\11.0\Add_Packages_DataPoint.sql


Releases\11.1\Alter_certified_check_constraint.sql

#
Releases\12.0\Alter-CIABucket2011.sql
Releases\12.0\Alter-CIABucket2012.sql
Releases\12.0\Insert-OVE-Hendrick-Extract.Destination.sql

#
Releases\12.1\Alter-DealerGroupPreference-Add-CarSearchTitle.sql
Releases\12.1\Alter-DealerGroupPreference-Add-ShowCarSearch.sql
Releases\12.1\Alter-Inventory-Add-ImageModifiedDate.sql
Releases\12.1\Update-Index-Strategy.sql
Releases\12.1\Alter-DealerGroupPref-Add-ExcludeWholesale.sql
Releases\12.1\Alter-DealerPref-Add-ExcludeWholesale.sql
Releases\12.1\Redecode-IMT-Vehicles.sql

#
Releases\12.2\Insert-DealerUpgrade-Add-Firstlook__3_0.sql
Releases\12.2\Alter-DealerGroupPreference.sql
Releases\12.2\Update-Inventory-ImageModifiedDate.sql
Releases\12.2\Alter-Inventory-Add-Locks.sql
#
Releases\13.0\Drop-Deprecated-Objects.sql
Releases\13.0\Alter-ExtractDealerConfiguration-Add-Flags.sql
#
Releases\13.1\CreateTable-PhotoTransferStatus.sql
Releases\13.1\CreateTable-PhotoTransfers.sql
Releases\13.1\PopulateTable-PhotoTransferStatus.sql
Releases\13.1\CreateTable-PhotoTransferGroups.sql
Releases\13.1\PopulateTable-PhotoTransferGroups.sql
Releases\13.1\Create-OVE-Hendrick-ExtendedConfiguration.sql
#
Releases\hotfix\Create-Extract.DealerConfiguration-MiniGID-ExtendedProperty.sql
Releases\13.2\CreateTable-Extract_InventoryDestinationStatus.sql
Releases\13.2\Revert-Photos-PhotoUpdated-Drop.sql
#
Releases\14.0\CreateTable-ExecQueue.sql
Releases\14.0\Update-Index-Strategy.sql
Releases\14.0\PhotoTransfers-DropIndexes.sql
#
Releases\14.1\Insert-Mobile-dbo.AppraisalSource.sql
Releases\14.1\DenormalizeInventoryBookouts.sql
Releases\14.1\Insert-DMSExportPriceMapping.sql
Releases\14.1\Alter-tbl_VehicleSale-Add-Salesperson.sql
#
Releases\15.0\Drop-GMAC-Export.sql
Releases\15.0\Alter-tbl_Vehicle-ColorCodes.sql
#
Releases\hotfix\Drop-AULtec-Export-V2-Views.sql
Releases\hotfix\AULtec-GID-Add-KelleyConsumerValue.sql
#
Releases\15.1\Alter-tbl_dbo_AppraisalActions-Add-TargetGrossProfit.sql
#
Releases\hotfix\Alter-DMSExportBusinessUnitPreference-ReynoldsDealerNo.sql
Releases\hotfix\Insert-Extract.DealerConfigurationFlag-ExportDmsOptionCodes.sql
Releases\hotfix\Alter-DealerPreference_Dataload-Add-InventoryDeletePctThreshold.sql

Releases\16.0\NadaWindowStickerPoint.sql
#
Releases\16.1\Clear-IMT-ReliableMessageQueue.sql
Releases\hotfix\Insert-Extract.DealerConfigurationFlag-ExportVideoURL.sql
Releases\hotfix\Insert-Extract.DealerConfigurationFlag-ExportStockPhotos.sql
Releases\16.2\Insert-Hendrick-HDTN-Mappings.sql
#
Releases\hotfix\Insert-Extract.DealerConfigurationFlag-ExcludeLotImageURLs.sql
#
Releases\17.0\Update-IMT-For-MMR-Integration.sql
#
Releases\17.1\InsertNewIMTThirdPartyCategorys.sql
Releases\17.1\InsertNewIMTThirdPartySubcategorys.sql
Releases\17.1\Alter-tbl_dbo_DealerPreference-Add-requireNameOnAppraisals.sql
#
Releases\hotfix\Alter_MMR_Vehicle_Mid_16.sql
Releases\hotfix\Insert-Extract.DealerConfigurationFlag-ExportChromeStyleID.sql
#
Releases\17.2\Alter-tbl_dbo_DealerPreference-Add-requireEstReconCostOnAppraisals.sql
Releases\17.2\Alter-tbl_dbo_DealerPreference-Add-requireReconNotesOnAppraisals.sql 

Releases\17.2.1\Distribution.eLead_ErrorTypeTable.sql
Releases\17.2.1\Distribution.eLead_ErrorTable.sql
Releases\17.2.1\Insert-Distribution.eLead_ErrorType.sql
Releases\17.2.1\Insert-Distribution.Provider.sql
Releases\17.2.1\Insert-Distribution.PriceType.sql
Releases\17.2.1\Insert-Distribution.VehicleEntityType.sql
Releases\17.2.1\Insert-Distribution.ProviderMessageType .sql

Releases\18.0\QRCode_DB_Stuff.sql
#
Releases\18.0\Alter-tbl_dbo_DealerPreference-Add-BlackBookMobileOptInFlag.sql
Releases\18.0\Alter-tbl_dbo_DealerPreference-Add-BlackBookOptInDate.sql
Releases\18.0\Alter-tbl_dbo_DealerPreference-Add-BlackBookOptOutDate.sql
Releases\18.0\Alter_tbl_dbo_BusinessUnitCredential_Add_UpdatedDate.sql

Releases\18.0\Insert-PingCodesAndGroupingCodes.sql
Releases\18.0\Update-PingCodesDelete_SERIES.sql
Releases\18.0\Update-PingCodesALLCAPS.sql

Releases\18.0\Update-lu_DealerUpgrade-MAX-Merch.sql
Releases\18.0\Alter-CIABucket2014.sql

Releases\18.0\Insert-Distribution.ProviderMessageType.sql 
Releases\18.0\Distribution.AppraisalValueTable.sql
Releases\18.0\Insert-Distribution.MessageType.sql
Releases\18.0\Update-AccountPreferenceMessageType-eLead.sql

#
Releases\18.1\Alter-dbo.Inventory-Add-Check.sql

Releases\18.1\Distribution.ADP_PriceTypeTable.sql
Releases\18.1\Distribution.ADP_ErrorTypeTable.sql
Releases\18.1\Distribution.ADP_ErrorTable.sql
Releases\18.1\Distribution.ADP_DealerPriceMappingTable.sql
Releases\18.1\Insert-Distribution.ADP_ErrorType.sql
Releases\18.1\Insert-Distribution.Provider.sql
Releases\18.1\Insert-Distribution.ADP_PriceType.sql
Releases\18.1\Insert-Distribution.ProviderMessageType.sql
Releases\18.1\Alter_Table_Distribution.ADP_Error.sql

Releases\18.1\Additional_WSDataPoints_forMaxMobileShowroom.sql
Releases\18.1\Insert-Extract.DealerConfigurationFlag-ExportZeroSpecialPrice.sql

#
Releases\18.2\Alter_Table_dbo.Inventory.sql
Releases\18.2\Alter_Table_MMR.Vehicle.sql
Releases\18.2\Create_TypeTable_MMR.MMRPriceData.sql
Releases\18.2\Create_TypeTable_MMR.QueueResult.sql
Releases\18.2\MMR.DatafeedException_Table.sql
Releases\18.2\MMR.QueueStatus_Table.sql
Releases\18.2\MMR.QueueTable_Table.sql
Releases\18.2\Insert-MMR.QueueStatus.sql
Releases\18.2\Insert-dbo.AppraisalActionTypes.sql

#
Releases\19.0\Distribution.ADP_FieldTypes_Table.sql
Releases\19.0\Distribution.ADP_DealerDefinedFields_Table.sql
Releases\19.0\Insert-Distribution.ADP_DealerDefinedFields.sql
Releases\19.0\Insert-Distribution.ADP_FieldTypes.sql
Releases\19.0\Alter-Distribution.ADP_DealerMapping_Table.sql
Releases\19.0\Insert-dbo.ThirdParties.sql
Releases\19.0\Insert-dbo.ThirdPartyCategories.sql
Releases\19.0\Update_Marketing_MarketAnalysisPriceProvider_OriginalMSRP.sql
Releases\19.0\Alter-MMR.QueueTable.sql
Releases\19.0\MMR.MMR_ErrorType.sql
Releases\19.0\Insert-MMR.MMR_ErrorTypes.sql
Releases\19.0\Insert-MMR.QueueStatus.sql
Releases\19.0\Alter-dbo.Inventory_Table.sql
Releases\19.0\Drop_Columns_Marketing_MarketAnalyisis-OriginalListPriceRelated.sql

#
Releases\19.1\Insert-MMR.MMr_ErrorType.sql
Releases\19.1\MerchandisingGrants.sql
Releases\19.1\Alter-Marketing.DealerGeneralPreference-ExtendedTagLine.sql
Releases\19.1\BookoutIndexedViewSchemaPrep.sql
Releases\19.1\Alter-dbo.AppraisalFormOptions.sql
Releases\19.1\windowSticker_PrintJob.sql
Releases\19.1\windowSticker_PrintBatch.sql
Releases\19.1\windowSticker_PrintBatchLog.sql
Releases\19.1\windowSticker_printBatch_alter.sql
Releases\19.1\windowSticker_PrintBatchEmail.sql
Releases\19.1\RapidReconDataSetup.sql
Releases\19.1\windowSticker_PrintBatchAccessLog.sql
#
Releases\hotfix\Create-dbo.Inventory_ListPriceHistory#Pricing.sql

#19.1.1
Releases\19.1.1\Alter-Certified-CertfiedProgram.sql
Releases\19.1.1\DropProcedure-Certified-OnwerCertifiedProgramExists.sql
Releases\19.1.1\Certified-Alters.sql
Releases\19.1.1\AlterTable-AddCertfiiedProgram.ExternalName.sql
Releases\19.1.1\AddColumn-InventoryCertificationNumber-CertifiedProgramId.sql
Releases\19.1.1\UpdateTableAndConstraint-InventoryCertificationNumberManufacturerId.sql
Releases\19.1.1\Drop-ManufacturerOrDealerCertifiedProgramBenefitCollection#Fetch.sql
Releases\19.1.1\Alter-Certified-CertifiedProgramMake-recreatePK.sql
Releases\19.1.1\AutoAssignCertifiedProgamsToDealerships.sql

#19.2
Releases\19.2\Alter-dbo.DealerGroupPreference.sql
Releases\19.2\alter_windowSticker_PrintBatchEmail.sql
Releases\19.2\Insert-BookoutStatus-InGroupTransfer.sql
Releases\19.2\alter_windowSticker_PrintBatchEmail_ShortUrl.sql
Releases\19.2\Add-ICN_CertifiedProgramAutoSavedDate.sql


#20.0
Releases\20.0\Alter-tbl_dbo_DealerPreference-Add-GalvesMobileOptInFlag.sql
Releases\20.0\Alter-tbl_dbo_DealerPreference-Add-GalvesOptInDate.sql
Releases\20.0\Alter-tbl_dbo_DealerPreference-Add-GalvesOptOutDate.sql
Releases\20.0\Update_DelearPreference.sql
Releases\20.0\Delete-ATC-AccessGroup-150.sql


#20.1
Releases\20.1\Alter-dbo.Inventory.sql
Releases\20.1\alter-IMT.dbo.ThirdPartyCategories.sql
Releases\20.1\Alter-IMT.dbo.Appraisalvalues.AppraiserName.sql
Releases\20.1\Alter-Inventory_ListPriceHistory#Pricing-Add-LoadID.sql
Releases\20.1\dbo.Inventory_CertificationNumber.Constraint.sql

#20.2
Releases\20.2\Refactorings.sql

#21.0
Releases\21.0\Create-IMT.dbo.AppraisalPhotoKeys.sql
Releases\21.0\Alter-IMT.dbo.AppraisalPhotoKeys.sql
Releases\21.0\Alter-IMT.dbo.Appraisals.sql
Releases\21.0\Extract.DealerConfigurationFlag-Add-AULTecPhotoImport.sql

#21.1
Releases\21.1\Alter-Imt.Certified.CertifiedProgram_Make.sql
Releases\21.1\Create-Bookout.Inventory_ExternalValuation.sql
Releases\21.1\Insert-BookoutStatus-partial.sql

#21.2
Releases\21.2\Marketing.MarketAnalysisOwnerPriceProvider_Add_MinimumDollarVariance.sql
Releases\21.2\Marketing.MarketAnalysisPreference_Set_New_Defaults.sql
Releases\21.2\Marketing.MarketAnalysisPriceProvider_RemoveDeadDataSources.sql
Releases\21.2\Marketing.MarketAnalysisOwnerPriceProvider_Update_Default_Provider_Rank.sql
Releases\21.2\Marketing.ConsumerHighlightPreference_JDPower3.5_to_3.sql
Releases\21.2\dbo.SearchCandidateForSearch.sql
Releases\21.2\dbo.AccessGroupsForSearch.sql

#22.0
Releases\22.0\Alter-Extract.DealerConfiguration#OVEHendrick#SellerGroup.sql
Releases\22.0\Alter-Certified.CertifiedProgram.ExternalName-RenameCol.sql

#22.2
Releases\22.2\Alter_DealerPreference_Pricing.sql

#23.0
Releases\23.0\Insert_dbo.lu_DealerUpgrade.sql
Releases\23.0\Alter_dbo.DealerPreference.sql
Releases\23.0\Update_dbo.DealerPreference.sql
Releases\23.0\AddHomeNetDistributionProvider.sql
Releases\23.0\Alter_dbo.DealerPreference_Pricing.sql
Releases\23.0\Distribution.ADPServiceLogs.sql


#23.1
Releases\23.1\Alter_BusinessUnit.sql
Releases\23.1\Update_BusinessUnit_GeoCode.sql
Releases\23.1\Create_VehicleBookoutStateNADA.sql
Releases\23.1\Create_VehicleBookoutStateNADAThirdPartyVehicles.sql
Releases\23.1\CreateHomeNetError.sql
#23.2
Releases\23.2\Drop-Deprecated-Objects.sql
Releases\23.2\Update_Riskbucket.sql
Releases\23.2\Insert-WindowSticker-DataPointKey-MSRP.sql

#24.0
Releases\24.0\Alter_IMT_dbo_AppraisalValues.sql
hotfix\Insert-Extract.DealerConfigurationFlag-UseMaxAdMSRP.sql

#24.1
Releases\24.1\Alter_dbo.DealerPreference.sql
Releases\24.1\Alter_dbo.Bookout_AdvertisingStatus.sql