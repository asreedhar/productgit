
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'StdHeaders')
CREATE SYNONYM [dbo].[StdHeaders] FOR [VehicleCatalog].[Chrome].[StdHeaders]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'Subdivisions')
CREATE SYNONYM [dbo].[Subdivisions] FOR [VehicleCatalog].[Chrome].[Subdivisions]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'TechTitleHeader')
CREATE SYNONYM [dbo].[TechTitleHeader] FOR [VehicleCatalog].[Chrome].[TechTitleHeader]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'Models')
CREATE SYNONYM [dbo].[Models] FOR [VehicleCatalog].[Chrome].[Models]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'GetSquishVIN')
CREATE SYNONYM [dbo].[GetSquishVIN] FOR [IMT_VC].[dbo].[GetSquishVIN]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'TechTitles')
CREATE SYNONYM [dbo].[TechTitles] FOR [VehicleCatalog].[Chrome].[TechTitles]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'CategoryHeaders')
CREATE SYNONYM [dbo].[CategoryHeaders] FOR [VehicleCatalog].[Chrome].[CategoryHeaders]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'BodyStyles')
CREATE SYNONYM [dbo].[BodyStyles] FOR [VehicleCatalog].[Chrome].[BodyStyles]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'CITypes')
CREATE SYNONYM [dbo].[CITypes] FOR [VehicleCatalog].[Chrome].[CITypes]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'ConsInfo')
CREATE SYNONYM [dbo].[ConsInfo] FOR [VehicleCatalog].[Chrome].[ConsInfo]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'Colors')
CREATE SYNONYM [dbo].[Colors] FOR [VehicleCatalog].[Chrome].[Colors]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'Divisions')
CREATE SYNONYM [dbo].[Divisions] FOR [VehicleCatalog].[Chrome].[Divisions]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'Styles')
CREATE SYNONYM [dbo].[Styles] FOR [VehicleCatalog].[Chrome].[Styles]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'Manufacturers')
CREATE SYNONYM [dbo].[Manufacturers] FOR [VehicleCatalog].[Chrome].[Manufacturers]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'MktClass')
CREATE SYNONYM [dbo].[MktClass] FOR [VehicleCatalog].[Chrome].[MktClass]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'Options')
CREATE SYNONYM [dbo].[Options] FOR [VehicleCatalog].[Chrome].[Options]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'StyleCats')
CREATE SYNONYM [dbo].[StyleCats] FOR [VehicleCatalog].[Chrome].[StyleCats]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'TechSpecs')
CREATE SYNONYM [dbo].[TechSpecs] FOR [VehicleCatalog].[Chrome].[TechSpecs]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'Version')
CREATE SYNONYM [dbo].[Version] FOR [VehicleCatalog].[Chrome].[Version]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'YearMakeModelStyle')
CREATE SYNONYM [dbo].[YearMakeModelStyle] FOR [VehicleCatalog].[Chrome].[YearMakeModelStyle]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'VINPattern')
CREATE SYNONYM [dbo].[VINPattern] FOR [VehicleCatalog].[Chrome].[VINPattern]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'VINPatternStyleMapping')
CREATE SYNONYM [dbo].[VINPatternStyleMapping] FOR [VehicleCatalog].[Chrome].[VINPatternStyleMapping]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'VINEquipment')
CREATE SYNONYM [dbo].[VINEquipment] FOR [VehicleCatalog].[Chrome].[VINEquipment]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'Category')
CREATE SYNONYM [dbo].[Category] FOR [VehicleCatalog].[Chrome].[Category]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'StyleWheelBase')
CREATE SYNONYM [dbo].[StyleWheelBase] FOR [VehicleCatalog].[Chrome].[StyleWheelBase]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'StyleGenericEquipment')
CREATE SYNONYM [dbo].[StyleGenericEquipment] FOR [VehicleCatalog].[Chrome].[StyleGenericEquipment]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'Categories')
CREATE SYNONYM [dbo].[Categories] FOR [VehicleCatalog].[Chrome].[Categories]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'OptHeaders')
CREATE SYNONYM [dbo].[OptHeaders] FOR [VehicleCatalog].[Chrome].[OptHeaders]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'OptKinds')
CREATE SYNONYM [dbo].[OptKinds] FOR [VehicleCatalog].[Chrome].[OptKinds]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'Prices')
CREATE SYNONYM [dbo].[Prices] FOR [VehicleCatalog].[Chrome].[Prices]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'Standards')
CREATE SYNONYM [dbo].[Standards] FOR [VehicleCatalog].[Chrome].[Standards]
GO
