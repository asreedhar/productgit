IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'InventoryBookout_F')
	CREATE SYNONYM [dbo].[InventoryBookout_F] FOR [FLDW].[dbo].[InventoryBookout_F]
GO

IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'InventoryInactiveBookout_F')
	CREATE SYNONYM [dbo].[InventoryInactiveBookout_F] FOR [FLDW].[dbo].[InventoryInactiveBookout_F]
GO

IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'Time_D')
	CREATE SYNONYM [dbo].[Time_D] FOR [FLDW].[dbo].[Time_D]
GO
