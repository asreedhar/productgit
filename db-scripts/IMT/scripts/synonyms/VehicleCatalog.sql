IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'VehicleCatalogVINPatternPriority')
DROP SYNONYM [dbo].[VehicleCatalogVINPatternPriority]
GO

CREATE SYNONYM [dbo].[VehicleCatalogVINPatternPriority] FOR [VehicleCatalog].[Firstlook].[VINPatternPriority]
GO
