IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'ConfigureDealershipForMAX')
CREATE SYNONYM [dbo].[ConfigureDealershipForMAX] FOR [HAL].[dbo].[ConfigureDealershipForMAX]
GO
IF NOT EXISTS (SELECT * FROM sys.synonyms WHERE name = N'GuidebookCorrectionForMax')
CREATE SYNONYM [dbo].[GuidebookCorrectionForMax] FOR [HAL].[dbo].[GuidebookCorrectionForMax]
GO