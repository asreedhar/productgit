/* Update tbl_makemodelgrouping after inserting from edmunds */

/* Tahoe and Yukon Update */
Update mmg
Set groupingdescriptionID = gd.groupingdescriptionID
from
    tbl_makemodelgrouping mmg,
    tbl_groupingdescription gd
where
    (mmg.model like '%yukon%' or mmg.model like '%tahoe%')
    and ( mmg.model not like '%Denali%'
        and
        mmg.model not like '%Limited%'
        and
        mmg.model not like '%XL%')
    and gd.groupingdescription = 'TAHOE / YUKON'
GO

UPDATE tbl_groupingdescription
SET groupingdescription = 'TAHOE / YUKON SUV'
where groupingdescription = 'TAHOE / YUKON'
GO

/* SUBURBAN and YUKON XL Update */
Update mmg
Set groupingdescriptionID = gd.groupingdescriptionID
from
    tbl_makemodelgrouping mmg,
    tbl_groupingdescription gd
where
    (mmg.model like '%SUBURBAN%' or mmg.model like '%yukon XL%')
    and gd.groupingdescription = 'SUBURBAN / YUKON XL'
GO

UPDATE tbl_groupingdescription
SET groupingdescription = 'SUBURBAN / YUKON XL SUV'
where groupingdescription = 'SUBURBAN / YUKON XL'
GO

/* ASTRO and SAFARI Update */
Update mmg
Set groupingdescriptionID = gd.groupingdescriptionID
from
    tbl_makemodelgrouping mmg,
    tbl_groupingdescription gd
where
    (mmg.model like '%ASTRO%' or mmg.model like '%SAFARI%')
    and ( mmg.model not like '%cargo%')
    and gd.groupingdescription = 'ASTRO / SAFARI'
GO

UPDATE tbl_groupingdescription
SET groupingdescription = 'ASTRO / SAFARI VAN'
where groupingdescription = 'ASTRO / SAFARI'
GO

DELETE from tbl_groupingdescription
where 
    groupingdescription = 'CHEVROLET ASTRO VAN' or
    groupingdescription = 'GMC SAFARI VAN'
GO


/* BLAZER and JIMMY Update */
Update mmg
Set groupingdescriptionID = gd.groupingdescriptionID
from
    tbl_makemodelgrouping mmg,
    tbl_groupingdescription gd
where
    (mmg.model like '%BLAZER%' or mmg.model like '%JIMMY%')
    and ( mmg.model not like '%s-10%'
        and mmg.model not like '%s-15%'
        and mmg.model not like '%TrailBlazer%')
    and gd.groupingdescription = 'BLAZER / JIMMY'
GO

UPDATE tbl_groupingdescription
SET groupingdescription = 'BLAZER / JIMMY SUV'
where groupingdescription = 'BLAZER / JIMMY'
GO

/* TrailBlazer and ENVOY Update */
Update mmg
Set groupingdescriptionID = gd.groupingdescriptionID
from
    tbl_makemodelgrouping mmg,
    tbl_groupingdescription gd
where
    (mmg.model like '%TrailBlazer%' or mmg.model like '%ENVOY%')
    and gd.groupingdescription = 'TRAILBLAZER / ENVOY'
GO

UPDATE tbl_groupingdescription
SET groupingdescription = 'TRAILBLAZER / ENVOY SUV'
where groupingdescription = 'TRAILBLAZER / ENVOY'
GO

/* SILVERADO and SIERRA 1500 Update */
Update mmg
Set groupingdescriptionID = gd.groupingdescriptionID
from
    tbl_makemodelgrouping mmg,
    tbl_groupingdescription gd
where
    mmg.model like '%1500%'
    and mmg.model not like '%van%'
    and ( mmg.make like '%chev%' or  mmg.make like '%gmc%')
    and gd.groupingdescription = 'SILVERADO / SIERRA 1500'
GO

UPDATE tbl_groupingdescription
SET groupingdescription = 'SILVERADO / SIERRA 1500 TRUCK'
where groupingdescription = 'SILVERADO / SIERRA 1500'
GO

/* SILVERADO and SIERRA 2500 Update */
Update mmg
Set groupingdescriptionID = gd.groupingdescriptionID
from
    tbl_makemodelgrouping mmg,
    tbl_groupingdescription gd
where
    mmg.model like '%2500%'
    and mmg.model not like '%van%'
    and ( mmg.make like '%chev%' or  mmg.make like '%gmc%')
    and gd.groupingdescription = 'SILVERADO / SIERRA 2500'
Go

UPDATE tbl_groupingdescription
SET groupingdescription = 'SILVERADO / SIERRA 2500 TRUCK'
where groupingdescription = 'SILVERADO / SIERRA 2500'
GO

/* SILVERADO and SIERRA 3500 Update */
Update mmg
Set groupingdescriptionID = gd.groupingdescriptionID
from
    tbl_makemodelgrouping mmg,
    tbl_groupingdescription gd
where 
    model like '%3500%'
    and (model not like '%van%' and model not like '%R/V%')
    and ( make like '%chev%' or  make like '%gmc%')
    and gd.groupingdescription = 'SILVERADO / SIERRA 3500'
GO

UPDATE tbl_groupingdescription
SET groupingdescription = 'SILVERADO / SIERRA 3500 TRUCK'
where groupingdescription = 'SILVERADO / SIERRA 3500'
GO

/* s10 and sonoma Update */
Update mmg
Set groupingdescriptionID = gd.groupingdescriptionID
from
    tbl_makemodelgrouping mmg,
    tbl_groupingdescription gd
where 
    (model like '%s10%' or model like '%sonoma%' or model like '%s-10%')
    and mmg.model not like '%blazer%'
    and gd.groupingdescription = 'S10 / SONOMA'
GO

UPDATE tbl_groupingdescription
SET groupingdescription = 'S10 / SONOMA TRUCK'
where groupingdescription = 'S10 / SONOMA'
GO


/*  Remove problem models with duplicate mappings for wagons */
delete from tbl_MakeModelGrouping
where 
(model = 'Tracer' and EdmundsBodyType = 'WAGN' and dbo.GetDisplayBodyType(DisplayBodyTypeID) = 'Sedan')
or (model = '9-5' and EdmundsBodyType = 'WAGN' and dbo.GetDisplayBodyType(DisplayBodyTypeID) = 'Sedan')
or (model = 'Legacy' and EdmundsBodyType = 'WAGN' and dbo.GetDisplayBodyType(DisplayBodyTypeID) = 'Sedan')

update mmg
SET GroupingDescriptionID = NULL
from tbl_MakeModelGrouping mmg
where 
(model = 'Tracer' and EdmundsBodyType = 'SDN' and dbo.GetDisplayBodyType(DisplayBodyTypeID) = 'Sedan')
or (model = '9-5' and EdmundsBodyType = 'SDN' and dbo.GetDisplayBodyType(DisplayBodyTypeID) = 'Sedan')
or (model = 'Legacy' and EdmundsBodyType = 'SDN' and dbo.GetDisplayBodyType(DisplayBodyTypeID) = 'Sedan')


/*  remove groupings that are no longer needed   */
delete from tbl_groupingdescription
where groupingdescriptionid not in (select distinct groupingdescriptionid
                    from tbl_makemodelgrouping where groupingdescriptionid is not NULL)
go

/* restore constraints */
ALTER TABLE MarketShare CHECK CONSTRAINT FK_MarketShare_GroupingDescriptionID
GO

ALTER TABLE tbl_Vehicle CHECK CONSTRAINT FK_tbl_Vehicle_MakeModelGrouping
GO

ALTER TABLE Appraisal CHECK CONSTRAINT FK_Appraisal_MakeModelGrouping
GO

ALTER TABLE tbl_MakeModelGrouping ADD CONSTRAINT UNK_MakeModelBodyType UNIQUE  NONCLUSTERED 
    (
        [Make],
        [Model],
        [EdmundsBodyType]
    ) WITH  FILLFACTOR = 80  ON [IDX]
GO

------------------------------------------------------------------------        
--  INSERT THE REMAINING GROUPING DESCRIPTIONS
--  REPLACES THE UNION CLAUSE IN THE AUGMENTED GroupingDescription VIEW
--  IN ORDER TO MAINTAIN FK RELATIONSHIPS
------------------------------------------------------------------------

insert
into    tbl_GroupingDescription (GroupingDescription)

select  distinct upper(MMG.Make) + ' ' + dbo.GetEnhancedModel(MMG.Model, dbo.GetDisplayBodyType(DisplayBodyTypeID)) as GroupingDescription

from    tbl_MakeModelGrouping MMG
where   MMG.GroupingDescriptionID is NULL
        and not exists (select 1 from tbl_GroupingDescription GD where upper(MMG.Make) + ' ' + dbo.GetEnhancedModel(MMG.Model, dbo.GetDisplayBodyType(DisplayBodyTypeID)) = GD.GroupingDescription)

------------------------------------------------------------------------        
--  UPDATE EXISTING TABLES WITH MMG FK'S
--
--  FIRST, UPDATE THE DSV
------------------------------------------------------------------------

exec PopulateDecodedSquishVINs

------------------------------------------------------------------------        
--  NEED TO HAVE AN "UNKNOWN" MMG FOR THE FK RELATIONSHIP
------------------------------------------------------------------------

set identity_insert tbl_MakeModelGrouping on

insert
into    tbl_MakeModelGrouping (MakeModelGroupingID, Make, Model)
select  0, 'Unknown','Unknown'

set identity_insert tbl_MakeModelGrouping off

------------------------------------------------------------------------        
--  NEED TO HAVE AN "UNKNOWN" GD FOR THE FK RELATIONSHIP
------------------------------------------------------------------------

set identity_insert tbl_GroupingDescription on

insert
into    tbl_GroupingDescription (GroupingDescriptionID, GroupingDescription)
select  0, 'Unknown'

set identity_insert tbl_GroupingDescription off

------------------------------------------------------------------------        
--  UPDATE APPRAISAL
------------------------------------------------------------------------

update  A
set 	A.MakeModelGroupingID = MMG.MakeModelGroupingID
from    Appraisal A
	join DecodedSquishVINs DSV on dbo.fn_SquishVIN(A.Vin) = DSV.Squish_VIN
	join MakeModelGrouping MMG on DSV.MakeModelGroupingID = MMG.MakeModelGroupingID
where	DSV.MakeModelGroupingID is not null

------------------------------------------------------------------------        
--  UPDATE VEHICLE VIA SQUISH VIN
------------------------------------------------------------------------

update  V
set 	V.MakeModelGroupingID = MMG.MakeModelGroupingID
from    Vehicle V
    	join DecodedSquishVINs DSV on dbo.fn_SquishVIN(V.Vin) = DSV.Squish_VIN
	join MakeModelGrouping MMG on DSV.MakeModelGroupingID = MMG.MakeModelGroupingID
where	DSV.MakeModelGroupingID is not null     

go
        
------------------------------------------------------------------------        
--  UPDATE VEHICLE AND APPRAISAL VIA MAPPING BETWEEN BODY STYLE AND 
--	DISPLAY BODY TYPE
------------------------------------------------------------------------

declare @mapping table (StandardBodyStyleID int, DisplayBodyTypeID int)

insert
into	@mapping (StandardBodyStyleID, DisplayBodyTypeID)
select 1, 4 union select 2,7 union select 3,4 union select 4,2
union select 5,6 union select 6,4 union select 7,2 union select 8,3
union select 9,2 union select 10,2 union select 11,3 union select 12,6
union select 13,5 union select 14,8 union select 15,2 union select 16,2
union select 17,5 union select 18,5 union select 19,2 union select 20,2
union select 21,2 union select 22,6 union select 23,5 union select 99,1 
union select 28,2 union select 29,8 union select 32,2 union select 35,2 
union select 36,2 union select 37,5 union select 38,7 union select 39,5 
union select 41,5 union select 42,5 union select 44,1 union select 45,1 
union select 46,6 union select 48,2

------------------------------------------------------------------------
--	CREATE SUPPLEMENTAL MAPPING
------------------------------------------------------------------------

declare @mmg_mapping table (OldMakeModelGroupingID int, NewMakeModelGroupingID int)

insert
into	@mmg_mapping
select	distinct MMGM.MakeModelGroupingID, MMG.MakeModelGroupingID
from	tbl_Vehicle V 
	join MakeModelGroupingMigration MMGM on V.MakeModelGroupingID = MMGM.MakeModelGroupingID
	join VehicleBodyStyleMapping BSM on V.VehicleBodyStyleID = BSM.VehicleBodyStyleMappingID
	join VehicleBodyStyle VBS on BSM.StandardBodyStyleID = VBS.StandardBodyStyleID
	join @mapping M on VBS.StandardBodyStyleID = M.StandardBodyStyleID
	join tbl_MakeModelGrouping MMG on MMGM.Make = MMG.Make and MMGM.Model = MMG.Model and M.DisplayBodyTypeID = MMG.DisplayBodyTypeID
	join lu_DisplayBodyType DBT on M.DisplayBodyTypeID = DBT.DisplayBodyTypeID        

------------------------------------------------------------------------
--	UPDATE VEHICLE AND APPRAISAL
------------------------------------------------------------------------

update	V
set	MakeModelGroupingID = M.NewMakeModelGroupingID
from	tbl_Vehicle V
	join	@mmg_mapping M on V.MakeModelGroupingID = M.OldMakeModelGroupingID

update	A
set	MakeModelGroupingID = M.NewMakeModelGroupingID
from	Appraisal A
	join	@mmg_mapping M on A.MakeModelGroupingID = M.OldMakeModelGroupingID
       
drop table MakeModelGroupingMigration		-- BUILT IN mmg-gd-migration-2.sql

go

------------------------------------------------------------------------
--	ONE LAST UPDATE, USING THE MAKE AND MODEL COLUMNS.  DEFAULT
--	UNMATCHED COLUMNS TO 0 (54 OUT OF 23K)
------------------------------------------------------------------------

update	A
set	A.MakeModelGroupingID = isnull(MMG.MakeModelGroupingID,0)

from 	Appraisal A
	left join tbl_MakeModelGrouping MMG on A.Make = MMG.Make and A.Model = MMG.Model
where	not exists (	select 	1 
			from 	MakeModelGrouping MMG 
			where 	A.MakeModelGroupingID = MMG.MakeModelGroupingID)

go
        
------------------------------------------------------------------------        
--  UPDATE MMG WITH GROUPING DESCRIPTIONS
------------------------------------------------------------------------        
        
update  MMG
set MMG.GroupingDescriptionID = GD.GroupingDescriptionID
from    tbl_MakeModelGrouping MMG
    join tbl_GroupingDescription GD on upper(MMG.Make) + ' ' + dbo.GetEnhancedModel(MMG.Model, dbo.GetDisplayBodyType(MMG.DisplayBodyTypeID)) = GD.GroupingDescription
where   MMG.GroupingDescriptionID is null     

go

------------------------------------------------------------------------        
--  UPDATE FIRSTLOOK DECODED SQUISH VINS (DSV MANUAL OVERRIDES)
------------------------------------------------------------------------  

declare @seg_dbt_map table (SegmentID tinyint, DisplayBodyTypeID int)

insert
into	@seg_dbt_map
select	1,4		-- COUPE
union			
select	2,3		-- SEDAN
union
select	3,6		-- SUV
union
select	4,2		-- TRUCK
union
select	5,5		-- VAN
union
select	6,8		-- WAGON

update	V
set	DisplayBodyTypeID = M.DisplayBodyTypeID
from	DecodedFirstLookSquishVINs V
	join @seg_dbt_map M on V.type = M.SegmentID

update	V
set	MakeModelGroupingID = coalesce(MMG.MakeModelGroupingID, MMG2.MakeModelGroupingID,0)	-- EXACT MAKE-MODEL-DBT MATCH, BEST GUESS MAKE-MODEL MATCH, AND ZERO-KEY TO KEEP FK REFERENCE
from	DecodedFirstLookSquishVINs V
	left join tbl_MakeModelGrouping MMG on V.make = MMG.make and V.model = MMG.model and V.DisplayBodyTypeID = MMG.DisplayBodyTypeID
	left join (	select 	Make, Model, MakeModelGroupingID
			from	tbl_MakeModelGrouping MMG
			group
			by	Make, Model, MakeModelGroupingID
			having	count(distinct DisplayBodyTypeID) = 1
			) MMG2 on V.make = MMG2.make and V.model = MMG2.model



exec PopulateDecodedSquishVINs 1		-- RELOAD THE FL DSV'S
go

------------------------------------------------------------------------        
--  NOW, UPDATE AuctionNet MMG'S.  SINCE SeriesBodyStyle_DXM IS A 
--	DIMENSION EXTENSION, WE CAN SIMPLY DROP AND RECREATE.
------------------------------------------------------------------------  

truncate table AuctionNet..SeriesBodyStyle_DXM

insert
into	AuctionNet..SeriesBodyStyle_DXM (SeriesBodyStyleID, MakeModelGroupingID, ModelYear)

select 	distinct SBS.SeriesBodyStyleID, isnull(DSV.MakeModelGroupingID,0), isnull(DSV.Year, 0)
from 	AuctionNet..AuctionTransaction_F T
	join AuctionNet..SeriesBodyStyle_D SBS on T.SeriesID = SBS.SeriesID and T.BodyStyleID = SBS.BodyStyleID
	left join DecodedSquishVins DSV on dbo.fn_SquishVIN(VIN) = DSV.Squish_VIN
where	not exists (	select 	1
			from	AuctionNet..SeriesBodyStyle_DXM XM
			where	SBS.SeriesBodyStyleID = XM.SeriesBodyStyleID 
				and isnull(DSV.MakeModelGroupingID,0) = XM.MakeModelGroupingID
				and isnull(DSV.Year, 0) = XM.ModelYear
			)
go
			
------------------------------------------------------------------------        
--  NOW, UPDATE MARKET MMG'S AND REAGGREGATE.  THESE ARE COMMENTED OUT 
--	SINCE THE JOB MAY NOT BE ON THE TARGET SERVER, AND IS NOT CURRENTLY
--	VERSIONED (I.E. WE CAN'T SET THE SOURCE/TARGETS IN CODE.)
------------------------------------------------------------------------        
/*
update	D
set	D.MMGID = DSV.MakeModelGroupingID	
from 	MARKET..Market_AutoCount_Detail D
	join DecodedSquishVINs DSV on D.Squish_Vin = DSV.Squish_vin
where	DSV.MakeModelGroupingID is not null

exec msdb..sp_start_job @job_name = 'On Demand: Market Aggregation', @step_name = 'Market Aggregation Package'

delete from MARKET..Market_Summary where isnumeric(Zip) = 0

update	Inventory 
set	MileageReceived = -1
where 	MileageReceived is null

*/


------------------------------------------------------------------------------------------------