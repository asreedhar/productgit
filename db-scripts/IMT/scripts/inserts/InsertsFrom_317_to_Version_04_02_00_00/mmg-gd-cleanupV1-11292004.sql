/* update the saturn sedans */

update
	mmg
set
	GroupingDescriptionID = (select gd.GroupingDescriptionID from GroupingDescription gd where gd.GroupingDescription = 'SATURN S-SERIES SEDAN')
from
	tbl_makemodelgrouping mmg
	join tbl_GroupingDescription gd
	on mmg.GroupingDescriptionID = gd.GroupingDescriptionID
where
	gd.GroupingDescription like 'saturn%'
	AND gd.GroupingDescription not like '%sedan'
	AND (mmg.displaybodytypeID = 3)

/* update the saturn coupes */

update
	mmg
set
	GroupingDescriptionID = (select gd.GroupingDescriptionID from GroupingDescription gd where gd.GroupingDescription = 'SATURN S-SERIES COUPE')
from
	tbl_makemodelgrouping mmg
	join tbl_GroupingDescription gd
	on mmg.GroupingDescriptionID = gd.GroupingDescriptionID
where
	gd.GroupingDescription like 'saturn%'
	AND gd.GroupingDescription not like '%coupe'
	AND (mmg.displaybodytypeID = 4)

/* update the MERCEDES-BENZ GroupingDescription w/o sedan */

update
	gd
set
	GroupingDescription = GroupingDescription + ' ' + 'SEDAN'
from
	tbl_makemodelgrouping mmg
	join tbl_GroupingDescription gd
	on mmg.GroupingDescriptionID = gd.GroupingDescriptionID
where
	gd.GroupingDescription like 'MERCEDES-BENZ%'
	AND gd.GroupingDescription not like '%sedan'
	AND (mmg.displaybodytypeID = 3)

/* update the DODGE RAM 1500 GroupingDescription to TRUCK */

update
	gd
set
	GroupingDescription =  GroupingDescription + ' ' + 'TRUCK'

--select mmg.*, gd.*

from
	tbl_makemodelgrouping mmg
	join tbl_GroupingDescription gd
	on mmg.GroupingDescriptionID = gd.GroupingDescriptionID
where
	gd.GroupingDescription like 'DODGE RAM 1500%'
	AND gd.GroupingDescription not like '%Truck'
	AND (mmg.displaybodytypeID = 2)

/* update the CLASSIC VAN */

Update gd
set GroupingDescription = 'CHEVROLET CLASSIC VAN'
from tbl_GroupingDescription gd
where gd.GroupingDescription = 'CHEVROLET CHEVY VAN CLASSIC VAN'


update
	mmg
set
	GroupingDescriptionID =  (select gd.GroupingDescriptionID from GroupingDescription gd where gd.GroupingDescription = 'CHEVROLET CLASSIC VAN')
from
	tbl_makemodelgrouping mmg
	join tbl_GroupingDescription gd
	on mmg.GroupingDescriptionID = gd.GroupingDescriptionID
where
	mmg.Model like 'CLASSIC'
	AND (mmg.displaybodytypeID = 5)

/* update the FORD ECONOLINE 450 GroupingDescription to VAN */

update
	gd
set
	GroupingDescription =  GroupingDescription + ' ' + 'VAN'
from
	tbl_makemodelgrouping mmg
	join tbl_GroupingDescription gd
	on mmg.GroupingDescriptionID = gd.GroupingDescriptionID
where
	gd.GroupingDescription like 'FORD ECONOLINE 450%'
	AND gd.GroupingDescription not like '%VAN'
	AND (mmg.displaybodytypeID = 5)
