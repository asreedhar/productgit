ALTER TABLE [dbo].[DealerPreference] ADD CONSTRAINT [DF_DealerPreference_UnitsSoldThresholdInvOverview] DEFAULT (13) FOR [UnitsSoldThresholdInvOverview]
GO

update dbo.DealerPreference set UnitsSoldThresholdInvOverview = 13