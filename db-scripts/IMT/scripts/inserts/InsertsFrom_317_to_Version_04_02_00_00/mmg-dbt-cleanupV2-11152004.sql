Update 
	v
set
	makemodelgroupingID = (
				select
					makemodelgroupingID
				from 
					tbl_makemodelgrouping mmg
				where
					mmg.make =  'HONDA' and mmg.model = 'ACCORD' and DisplayBodyTypeID = 4)
from 
	tbl_vehicle v
	join tbl_makemodelgrouping mmg2
	on v.makemodelgroupingID = mmg2.makemodelgroupingID
where
	mmg2.make =  'HONDA' and mmg2.model = 'ACCORD COUPE'
GO

Update 
	a
set
	makemodelgroupingID = (
				select
					makemodelgroupingID
				from 
					tbl_makemodelgrouping mmg
				where
					mmg.make =  'HONDA' and mmg.model = 'ACCORD' and DisplayBodyTypeID = 4)
from 
	Appraisal a
	join tbl_makemodelgrouping mmg2
	on a.makemodelgroupingID = mmg2.makemodelgroupingID
where
	mmg2.make =  'HONDA' and mmg2.model = 'ACCORD COUPE'
GO


DELETE FROM tbl_makemodelgrouping where make =  'HONDA' and model = 'ACCORD COUPE'
GO

Update 
	v
set
	makemodelgroupingID = (
				select
					makemodelgroupingID
				from 
					tbl_makemodelgrouping mmg
				where
					mmg.make =  'HONDA' and mmg.model = 'ACCORD' and DisplayBodyTypeID = 3)
from 
	tbl_vehicle v
	join tbl_makemodelgrouping mmg2
	on v.makemodelgroupingID = mmg2.makemodelgroupingID
where
	mmg2.make =  'HONDA' and mmg2.model = 'ACCORD SEDAN'
GO

Update 
	a
set
	makemodelgroupingID = (
				select
					makemodelgroupingID
				from 
					tbl_makemodelgrouping mmg
				where
					mmg.make =  'HONDA' and mmg.model = 'ACCORD' and DisplayBodyTypeID = 3)
from 
	Appraisal a
	join tbl_makemodelgrouping mmg2
	on a.makemodelgroupingID = mmg2.makemodelgroupingID
where
	mmg2.make =  'HONDA' and mmg2.model = 'ACCORD SEDAN'
GO


DELETE FROM tbl_makemodelgrouping where make =  'HONDA' and model = 'ACCORD SEDAN'
GO

Update 
	v
set
	makemodelgroupingID = (
				select
					makemodelgroupingID
				from 
					tbl_makemodelgrouping mmg
				where
					mmg.make =  'HONDA' and mmg.model = 'CIVIC' and DisplayBodyTypeID = 4 and EdmundsBodyType = 'COUP')
from 
	tbl_vehicle v
	join tbl_makemodelgrouping mmg2
	on v.makemodelgroupingID = mmg2.makemodelgroupingID
where
	mmg2.make =  'HONDA' and mmg2.model = 'CIVIC COUPE'
GO


Update 
	a
set
	makemodelgroupingID = (
				select
					makemodelgroupingID
				from 
					tbl_makemodelgrouping mmg
				where
					mmg.make =  'HONDA' and mmg.model = 'CIVIC' and DisplayBodyTypeID = 4 and EdmundsBodyType = 'COUP')
from 
	Appraisal a
	join tbl_makemodelgrouping mmg2
	on a.makemodelgroupingID = mmg2.makemodelgroupingID
where
	mmg2.make =  'HONDA' and mmg2.model = 'CIVIC COUPE'
GO


DELETE FROM tbl_makemodelgrouping where make =  'HONDA' and model = 'CIVIC COUPE'
GO

Update 
	v
set
	makemodelgroupingID = (
				select
					makemodelgroupingID
				from 
					tbl_makemodelgrouping mmg
				where
					mmg.make =  'HONDA' and mmg.model = 'CIVIC' and DisplayBodyTypeID = 3)
from 
	tbl_vehicle v
	join tbl_makemodelgrouping mmg2
	on v.makemodelgroupingID = mmg2.makemodelgroupingID
where
	mmg2.make =  'HONDA' and mmg2.model = 'CIVIC SEDAN'
GO



Update 
	a
set
	makemodelgroupingID = (
				select
					makemodelgroupingID
				from 
					tbl_makemodelgrouping mmg
				where
					mmg.make =  'HONDA' and mmg.model = 'CIVIC' and DisplayBodyTypeID = 3)
from 
	Appraisal a
	join tbl_makemodelgrouping mmg2
	on a.makemodelgroupingID = mmg2.makemodelgroupingID
where
	mmg2.make =  'HONDA' and mmg2.model = 'CIVIC SEDAN'
GO


DELETE FROM tbl_makemodelgrouping where make =  'HONDA' and model = 'CIVIC SEDAN'
GO