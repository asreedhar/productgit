delete from dealerUpgrade where dealerUpgradeCD = 8

insert into dealerUpgrade
(
	businessunitid,
	dealerupgradecd,
	startdate,
	active
)

select distinct
	businessunitid,
	8,
	getDate(),
	1
from dbo.map_BusinessUnitToMarketDealer