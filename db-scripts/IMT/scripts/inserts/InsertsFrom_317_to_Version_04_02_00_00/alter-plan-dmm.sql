update dp
set

	AgingInvPlan = 'Remember: Enhanced Lights Analysis '
			+ 'and Stocking Analysis '
			+ 'is based on vehicle performance' 
			+  CASE 
				WHEN IncludeBackEndInValuation = 1 THEN ' (including F&I profit)' 
				ELSE '' 
			   END
			+ ' over last 3 months and forecast 3 months based on prior year'
FROM 
	DealerPreference dp