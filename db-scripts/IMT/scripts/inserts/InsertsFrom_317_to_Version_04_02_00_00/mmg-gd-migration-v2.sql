/*  delete all make models that exist in edmunds from tbl_makemodelgrouping  */

select	*
into	dbo.MakeModelGroupingMigration 
from    tbl_MakeModelGrouping mmg
where   exists (select 1 from vw_SquishVin sv where mmg.make = sv.make and mmg.model = sv.model) 

delete  mmg
from    tbl_MakeModelGrouping mmg
where   exists (select 1 from vw_SquishVin sv where mmg.make = sv.make and mmg.model = sv.model)
go

/*  insert the make, models from edmunds into tbl_makemodelgrouping  */
INSERT INTO tbl_makemodelgrouping
select distinct
    (Select
        mmg.groupingdescriptionid
       from
        tbl_makemodelgrouping mmg
       where
        mmg.make = sv.make
        and mmg.model = sv.model
          )
    ,sv.make
    ,sv.model
    ,1
    ,1
    ,sv.body_type as EdmundsBodyType
    ,dbo.GetDisplayBodyTypeID(sv.body_type, s.vehicle_type, sv.style, sv.doors) as DisplayBodyTypeID
from
    vw_SquishVin sv
    left join vw_Style s
    on sv.ed_style_id = s.ed_style_id
order by sv.make, sv.model
GO

/*  insert any new groupings needed after inserting into tbl_makemodelgrouping  */
insert into tbl_groupingdescription
Select
    UPPER(make) + ' ' + RTRIM(UPPER(model) + ' ' + ISNULL(UPPER(dbo.GetDisplayBodyType(DisplayBodyTypeID)),'')), 0
from 
    tbl_makemodelgrouping
where
    DisplayBodyTypeID is not null
group by 
    make
    ,model
    ,DisplayBodyTypeID
having count(*) > 1
GO

/*  update the groupingdescriptionID in tbl_makemodelgrouping based on the previous step   */
UPDATE
    mmg
SET
    GroupingDescriptionID = gd.GroupingDescriptionID
from
    tbl_makemodelgrouping mmg
    join tbl_groupingdescription gd
    ON UPPER(mmg.make) + ' ' + dbo.GetEnhancedModel(mmg.model,dbo.GetDisplayBodyType(mmg.DisplayBodyTypeID)) = gd.GroupingDescription
where
    mmg.DisplayBodyTypeID is not NULL
    and mmg.GroupingDescriptionID is NULL
GO

/*  update the makemodelgroupingID in vehicle to account for the the new body type field  */
Update v
set MakeModelGroupingID = mmg.MakeModelGroupingID
from
    tbl_Vehicle v
    join vw_SquishVin sv
    on dbo.fn_squishvin(v.vin) = squish_vin
    join tbl_makemodelgrouping mmg
    on sv.make = mmg.MAKE
    and sv.model = mmg.MODEL
    and sv.body_type = mmg.EdmundsBodyType
go

/*  remove any make models not in the updated tbl_makemodelgrouping table  */
delete
from tbl_makemodelgrouping
where
EdmundsBodyType is NULL
and makemodelgroupingid not in (select makemodelgroupingid from tbl_vehicle)
GO