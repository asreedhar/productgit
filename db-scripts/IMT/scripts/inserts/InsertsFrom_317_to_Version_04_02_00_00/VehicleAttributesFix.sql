insert dbo.vehicleattributes
(vehicleid, vehiclesegmentID, vehiclesubsegmentID)

select 	vehicleid, 
	SegmentID, 
	CASE
	when SegmentID = 6 and SubSegmentID = 3 then 3
	when SegmentID = 6 and SubSegmentID <> 3 then 4
	else SubSegmentID
	END as SubSegmentID
from
	
(  	
select v.vehicleid , ISNULL(seg1.vehiclesegmentid,99) as SegmentID , ISNULL(seg2.subsegmentid,99) as SubSegmentID
from vehicle v
left join
(
	select 	distinct v.vehicleid, 
		CASE
		when v.make = 'Mazda' and v.model = 'MPV' then 3 
		when v.make = 'ISUZU' and v.model = 'AMIGO' then 3
		else seg.vehiclesegmentid  
		END as vehiclesegmentid
	from vehicle v
	join lu_edmunds_squishvin sq
	on substring (v.vin,1,8) + substring (v.vin,10,2) = sq.squish_vin
	join vw_style vw
	on sq.ed_style_id = vw.ed_style_id
	join  lu_vehicleType ty
	on vw.Vehicle_Type = ty.VehicleTypeDesc
	join map_vehicletypetosegment map1
	on  ty.vehicletypeid = map1.vehicletypeid
	join lu_vehiclesegment seg
	on map1.vehiclesegmentid = seg.vehiclesegmentid
	union
	select 	distinct vehicleid, 
		CASE
		when v.make = 'Mazda' and v.model = 'MPV' then 3 
		when v.make = 'ISUZU' and v.model = 'AMIGO' then 3	
		else seg.vehiclesegmentid 
		END as vehiclesegmentid
	from vehicle v
	join lu_edmunds_squishvin sq
	on substring (v.vin,1,8) + substring (v.vin,10,2) = sq.squish_vin
	join vw_style vw
	on sq.used_ed_style_id = vw.ed_style_id
	join  lu_vehicleType ty
	on vw.Vehicle_Type = ty.VehicleTypeDesc
	join map_vehicletypetosegment map1
	on  ty.vehicletypeid = map1.vehicletypeid
	join lu_vehiclesegment seg
	on map1.vehiclesegmentid = seg.vehiclesegmentid
)  seg1
on v.vehicleid = seg1.vehicleid

left join
(
	select  distinct v.vehicleid, 
		CASE
		when v.make = 'Mazda' and v.model = 'MPV' then 5 
		when v.make = 'ISUZU' and v.model = 'AMIGO' then 5	
		else seg.subsegmentid 
		END as subsegmentid 
	from vehicle v
	join lu_edmunds_squishvin sq
	on substring (v.vin,1,8) + substring (v.vin,10,2) = sq.squish_vin
	join vw_style vw
	on sq.ed_style_id = vw.ed_style_id 
	join  lu_vehicleSubType ty
	on vw.Vehicle_SubType = ty.VehicleSubTypeDesc
	join map_vehiclesubtypetosubsegment map1
	on  ty.vehiclesubtypeid = map1.vehiclesubtypeid
	join lu_vehiclesubsegment seg
	on map1.subsegmentid = seg.subsegmentid
	union
	select  distinct v.vehicleid, 
		CASE
		when v.make = 'Mazda' and v.model = 'MPV' then 5 
		when v.make = 'ISUZU' and v.model = 'AMIGO' then 5	
		else seg.subsegmentid 
		END as subsegmentid 
	from vehicle v
	join lu_edmunds_squishvin sq
	on substring (v.vin,1,8) + substring (v.vin,10,2) = sq.squish_vin
	join vw_style vw
	on sq.used_ed_style_id = vw.ed_style_id
	join  lu_vehicleSubType ty
	on vw.Vehicle_SubType = ty.VehicleSubTypeDesc
	join map_vehiclesubtypetosubsegment map1
	on  ty.vehiclesubtypeid = map1.vehiclesubtypeid
	join lu_vehiclesubsegment seg
	on map1.subsegmentid = seg.subsegmentid
) seg2
on v.vehicleid = seg2.vehicleid
) sub1
go
