ALTER TABLE [dbo].[VehicleBodyStyleMapping] DROP CONSTRAINT [FK_VehicleBodyStyleMapping_VehicleBodyStyle_StandardBodyStyleID]

GO

ALTER TABLE [dbo].[tbl_Vehicle] DROP CONSTRAINT [FK_tbl_Vehicle_VehicleBodyStyleMapping]

GO

truncate table VehicleBodyStyle

insert into dbo.VehicleBodyStyle
Select distinct
	UPPER(case
		when isnumeric(sv.doors) = 1 then 
			cast(sv.doors as varchar) + 'DR ' else '' end +
				case sv.body_type
					when 'CRWC' then 'Crew Cab'
					when 'EXTL' then 'Extended Van'
					when 'HBK' then 'Hatchback'
					when 'EXTC' then 'Extended Cab'
					when 'REGC' then 'Regular Cab'
					else ''
	end) as new_bodystyle
from
	tbl_Vehicle v 
	left join vw_SquishVin sv
	on dbo.fn_squishvin(v.vin) = sv.squish_vin
	left join vw_Style s
	on sv.ed_style_id = s.ed_style_id
GO


SET IDENTITY_INSERT VehicleBodyStyle ON
GO

insert into VehicleBodyStyle
(StandardBodyStyleID, VehicleBodyStyle) Values(99, 'UNKNOWN')
GO

SET IDENTITY_INSERT VehicleBodyStyle OFF
GO

truncate table VehicleBodyStyleMapping
go

insert into VehicleBodyStyleMapping
select
	StandardBodyStyleID
	,VehicleBodyStyle
from
	dbo.VehicleBodyStyle
where
	StandardBodyStyleID <> 99
go

SET IDENTITY_INSERT VehicleBodyStyleMapping ON
GO

insert into VehicleBodyStyleMapping
(VehicleBodyStyleMappingID, StandardBodyStyleID,OriginalVehicleBodyStyle) Values(99, 99, 'UNKNOWN')
GO

SET IDENTITY_INSERT VehicleBodyStyleMapping OFF
GO

update tbl_Vehicle
set VehicleBodyStyleID = 99
go


update v
	set VehicleBodyStyleID = vm.VehicleBodyStyleMappingID
from
	tbl_vehicle v 
	left join vw_SquishVin sv
	on dbo.fn_squishvin(v.vin) = sv.squish_vin
	left join vw_Style s
	on sv.ed_style_id = s.ed_style_id
	join vehiclebodystylemapping vm
	on case
		when isnumeric(sv.doors) = 1 then 
			cast(sv.doors as varchar) + 'DR ' else '' end +
				case sv.body_type
					when 'CRWC' then 'Crew Cab'
					when 'EXTL' then 'Extended Van'
					when 'HBK' then 'Hatchback'
					when 'EXTC' then 'Extended Cab'
					when 'REGC' then 'Regular Cab'
					else '' 
		  end  =  vm.OriginalVehicleBodyStyle
go

update v
	set VehicleBodyStyleID = vm.VehicleBodyStyleMappingID
from
	tbl_vehicle v 
	join vehiclebodystylemapping vm
	on v.OriginalBodyStyle  =  vm.OriginalVehicleBodyStyle
where
	v.VehicleBodyStyleID = 99
go

update v
	set VehicleBodyStyleID = vm.VehicleBodyStyleMappingID
from
	tbl_vehicle v 
	join vehiclebodystylemapping vm
	on cast(v.doorcount as varchar) + 'DR' =  vm.OriginalVehicleBodyStyle
	
where
	v.VehicleBodyStyleID = 99
go

insert into VehicleBodyStyleMapping
select distinct
	99, OriginalBodyStyle
from
	tbl_vehicle v
where
	VehicleBodyStyleID = 99
	and OriginalBodyStyle is not NULL
	and OriginalBodyStyle <> ''
	and OriginalBodyStyle not in (select distinct OriginalVehicleBodyStyle from VehicleBodyStyleMapping)
	and OriginalBodyStyle not in ('Truck','Sedan','Coupe','Van','Coupe','SUV','Convertible')
go

update v
	set VehicleBodyStyleID = vm.VehicleBodyStyleMappingID
from
	tbl_vehicle v 
	join vehiclebodystylemapping vm
	on v.OriginalBodyStyle =  vm.OriginalVehicleBodyStyle
where
	v.VehicleBodyStyleID = 99
go

ALTER TABLE [dbo].[tbl_Vehicle] ADD CONSTRAINT [FK_tbl_Vehicle_VehicleBodyStyleMapping] FOREIGN KEY 
	(
		[VehicleBodyStyleID]
	) REFERENCES [VehicleBodyStyleMapping] (
		[VehicleBodyStyleMappingID]
	)
GO


ALTER TABLE [dbo].[VehicleBodyStyleMapping] ADD CONSTRAINT [FK_VehicleBodyStyleMapping_VehicleBodyStyle_StandardBodyStyleID] FOREIGN KEY 
	(
		[StandardBodyStyleID]
	) REFERENCES [VehicleBodyStyle] (
		[StandardBodyStyleID]
	)
GO

