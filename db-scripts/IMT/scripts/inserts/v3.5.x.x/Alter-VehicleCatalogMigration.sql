----------------------------------------------------------------------------------------------------------------
--
----------------------------------------------------------------------------------------------------------------
EXEC dbo.PopulateVehicleAttributeCatalog

----------------------------------------------------------------------------------------------------------------
--	NOW, UPDATE THE "VEHICLE CATALOG KEY" WHERE THERE IS ONE UNIQUE VALUE
----------------------------------------------------------------------------------------------------------------

UPDATE	V
SET	V.CatalogKey = VC1.CatalogKey
FROM	tbl_Vehicle V
	join VehicleAttributeCatalog_11 VC1 ON dbo.fn_SquishVIN(V.VIN) = VC1.SquishVIN

GO

DECLARE @Candidates TABLE (VehicleID INT)

INSERT
INTO	@Candidates
SELECT	VehicleID
FROM	tbl_Vehicle V
	join VehicleAttributeCatalog_1M VCM ON dbo.fn_SquishVIN(V.VIN) = VCM.SquishVIN 
							AND V.MakeModelGroupingID = VCM.MakeModelGroupingID
 							AND V.VehicleTrim = VCM.Series 
WHERE	V.CatalogKey IS NULL
GROUP
BY	VehicleID
HAVING	COUNT(DISTINCT VCM.CatalogKey) = 1

UPDATE	V
SET	V.CatalogKey = VCM.CatalogKey
FROM	tbl_Vehicle V
	JOIN @Candidates C ON V.VehicleID = C.VehicleID
	JOIN  VehicleAttributeCatalog_1M VCM ON dbo.fn_SquishVIN(V.VIN) = VCM.SquishVIN 
							AND V.MakeModelGroupingID = VCM.MakeModelGroupingID
 							AND V.VehicleTrim = VCM.Series 

