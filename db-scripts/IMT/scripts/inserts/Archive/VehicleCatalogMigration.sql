
--------------------------------------------------------------------------------------------------------
--	tbl_Vehicle
--------------------------------------------------------------------------------------------------------

ALTER TABLE tbl_Vehicle DISABLE TRIGGER ALL

UPDATE	V
SET	V.VehicleCatalogID	= COALESCE(VCM.VehicleCatalogID,VC.VehicleCatalogID, 0),
	VehicleTrim		= CASE WHEN ISNULL(VehicleTrim,'') IN ('','N/A') THEN UPPER(COALESCE(NULLIF(VCM.Series,''), NULLIF(VC.Series,''), V.VehicleTrim, '')) ELSE VehicleTrim END,

	CatalogKey		= COALESCE(VCM.Catalogkey, VC.CatalogKey, V.CatalogKey),
	MakeModelGroupingID	= COALESCE(VCM.MakeModelGroupingID, VC.MakeModelGroupingID, V.MakeModelGroupingID, 0),
	BodyTypeID		= COALESCE(VCM.BodyTypeID, VC.BodyTypeID, V.BodyTypeID)	
	
FROM	tbl_Vehicle V

	LEFT JOIN VehicleCatalog VC ON VC.CountryCode = 1 AND SUBSTRING(V.VIN,1,8) + SUBSTRING (V.VIN,10,1) = VC.SquishVin and V.VIN like VC.VINPattern
	
	LEFT JOIN VehicleCatalog_XM VCM ON VCM.CountryCode = 1
						AND VCM.IsDistinctSeries = 1 
						AND SUBSTRING(V.VIN,1,8) + SUBSTRING (V.VIN,10,1) = VCM.SquishVin 
						AND V.VIN like VCM.VINPattern 
						AND CASE WHEN ISNULL(V.VehicleTrim,'') IN ('','N/A') THEN UPPER(COALESCE(NULLIF(VC.Series,''), V.VehicleTrim, '')) ELSE VehicleTrim END = VCM.Series


	LEFT JOIN  VehicleCatalogVINPatternPriority VPP ON VPP.CountryCode = 1 AND COALESCE(VC.VINPattern,VCM.VINPattern) = VPP.VINPattern AND V.VIN LIKE  VPP.PriorityVINPattern 

WHERE	VPP.PriorityVINPattern IS null	


UPDATE	V
SET	V.VehicleCatalogID	= COALESCE(VCM.VehicleCatalogID,VC.VehicleCatalogID, 0),
	VehicleTrim		= CASE WHEN ISNULL(VehicleTrim,'') IN ('','N/A') THEN UPPER(COALESCE(NULLIF(VCM.Series,''), NULLIF(VC.Series,''), V.VehicleTrim, '')) ELSE VehicleTrim END,
	
	CatalogKey		= COALESCE(VCM.Catalogkey, VC.CatalogKey, V.CatalogKey),
	MakeModelGroupingID	= COALESCE(VCM.MakeModelGroupingID, VC.MakeModelGroupingID, V.MakeModelGroupingID, 0),
	BodyTypeID		= COALESCE(VCM.BodyTypeID, VC.BodyTypeID, V.BodyTypeID)	
	
FROM	tbl_Vehicle V

	LEFT JOIN VehicleCatalog VC ON VC.CountryCode = 2 AND dbo.GetSquishVIN(V.Vin) = VC.SquishVin and V.VIN like VC.VINPattern
	
	LEFT JOIN VehicleCatalog_XM VCM ON VCM.CountryCode = 2
						AND VCM.IsDistinctSeries = 1 
						AND dbo.GetSquishVIN(V.Vin) = VCM.SquishVin 
						AND V.VIN like VCM.VINPattern 
						AND CASE WHEN ISNULL(V.VehicleTrim,'') IN ('','N/A') THEN UPPER(COALESCE(NULLIF(VC.Series,''), V.VehicleTrim, '')) ELSE VehicleTrim END = VCM.Series

	LEFT JOIN  VehicleCatalogVINPatternPriority VPP ON VPP.CountryCode = 2 AND COALESCE(VC.VINPattern,VCM.VINPattern) = VPP.VINPattern AND V.VIN LIKE  VPP.PriorityVINPattern 

WHERE	V.VehicleCatalogID = 0
	AND VPP.PriorityVINPattern IS NULL		

--------------------------------------------------------------------------------------------------------
--	NOW, UPDATE ANY LEVEL ONE ID'S TO LEVEL TWO IF THERE IS ONLY ONE CHILD
--------------------------------------------------------------------------------------------------------

UPDATE	V
SET	V.VehicleCatalogID = VC.VehicleCatalogID_2
FROM	IMT..tbl_Vehicle V
	JOIN (	SELECT	VC1.VehicleCatalogID VehicleCatalogID_1, MIN(VC2.VehicleCatalogID) VehicleCatalogID_2
		FROM	VehicleCatalog.Firstlook.VehicleCatalog VC1
			JOIN VehicleCatalog.Firstlook.VehicleCatalog VC2 ON VC1.VehicleCatalogID = VC2.ParentID
		GROUP
		BY	VC1.VehicleCatalogID
		HAVING	COUNT(DISTINCT VC2.VehicleCatalogID) = 1
		) VC ON V.VehicleCatalogID = VC.VehicleCatalogID_1
		
		
--------------------------------------------------------------------------------------------------------
--	PICK UP SOME LEVEL 2'S WITH DISTINCT TRANSMISSIONS
--------------------------------------------------------------------------------------------------------

SELECT	VC.*
INTO	#IsDistinctTransmission
FROM	VehicleCatalog.Firstlook.VehicleCatalog VC
	JOIN (	SELECT	MIN(VehicleCatalogID) VehicleCatalogID	-- FIND DISTINCT VP/SERIES
		FROM	VehicleCatalog.Firstlook.VehicleCatalog VC
		WHERE	VehicleCatalogLevelID = 2
		GROUP
		BY	CountryCode, SquishVIN, VINPattern, ISNULL(NULLIF(REPLACE(REPLACE(REPLACE(Transmission,'A/T','Automatic'), 'M/T', 'Manual'),'-',' '),''),'N/A')
		HAVING	COUNT(DISTINCT VehicleCatalogID) = 1
		) DS ON VC.VehicleCatalogID = DS.VehicleCatalogID

UPDATE	V
SET	V.VehicleCatalogID	= VCT.VehicleCatalogID
FROM	tbl_Vehicle V
	JOIN VehicleCatalog.Firstlook.VehicleCatalog VC ON V.VehicleCatalogID = VC.VehicleCatalogID
	JOIN #IsDistinctTransmission VCT ON VCT.CountryCode = 1
						AND SUBSTRING(V.VIN,1,8) + SUBSTRING (V.VIN,10,1)= VCT.SquishVin 
						AND V.VIN like VCT.VINPattern 
						AND V.VehicleTransmission = VCT.Transmission
						AND VCT.IsDistinctSeries = 0

	LEFT JOIN  VehicleCatalogVINPatternPriority VPP ON VPP.CountryCode = 1 AND VCT.VINPattern = VPP.VINPattern AND V.VIN LIKE VPP.PriorityVINPattern 

WHERE	VC.VehicleCatalogLevelID = 1
	AND VPP.PriorityVINPattern IS null	
	
--------------------------------------------------------------------------------------------------------
--	PICK UP SOME LEVEL 2'S WITH DISTINCT SERIES-TRANSMISSIONS
--------------------------------------------------------------------------------------------------------

SELECT	VC.*
INTO	#IsDistinctSeriesTransmission
FROM	VehicleCatalog.Firstlook.VehicleCatalog VC
	JOIN (	SELECT	MIN(VehicleCatalogID) VehicleCatalogID	-- FIND DISTINCT VP/SERIES
		FROM	VehicleCatalog.Firstlook.VehicleCatalog VC
		WHERE	VehicleCatalogLevelID = 2
		GROUP
		BY	CountryCode, SquishVIN, VINPattern, Series, ISNULL(NULLIF(REPLACE(REPLACE(REPLACE(Transmission,'A/T','Automatic'), 'M/T', 'Manual'),'-',' '),''),'N/A')
		HAVING	COUNT(DISTINCT VehicleCatalogID) = 1
		) DS ON VC.VehicleCatalogID = DS.VehicleCatalogID


UPDATE	V
SET	V.VehicleCatalogID	= VCST.VehicleCatalogID,
	V.VehicleTrim		= CASE WHEN ISNULL(VehicleTrim,'') IN ('','N/A') THEN UPPER(COALESCE(NULLIF(VCST.Series,''), NULLIF(VC.Series,''), V.VehicleTrim, '')) ELSE VehicleTrim END

FROM	tbl_Vehicle V
	JOIN VehicleCatalog.Firstlook.VehicleCatalog VC ON V.VehicleCatalogID = VC.VehicleCatalogID
	JOIN #IsDistinctSeriesTransmission VCST ON VCST.CountryCode = 1
						AND SUBSTRING(V.VIN,1,8) + SUBSTRING (V.VIN,10,1)= VCST.SquishVin 
						AND V.VIN like VCST.VINPattern 
						AND CASE WHEN ISNULL(V.VehicleTrim,'') IN ('','N/A') THEN UPPER(COALESCE(NULLIF(VC.Series,''), V.VehicleTrim, '')) ELSE VehicleTrim END = VCST.Series
						AND V.VehicleTransmission = VCST.Transmission
						AND VCST.IsDistinctSeries = 0

	LEFT JOIN  VehicleCatalogVINPatternPriority VPP ON VPP.CountryCode = 1 AND VCST.VINPattern = VPP.VINPattern AND V.VIN LIKE VPP.PriorityVINPattern 

WHERE	VC.VehicleCatalogLevelID = 1
	AND VPP.PriorityVINPattern IS null	
			
		
ALTER TABLE tbl_Vehicle ENABLE TRIGGER ALL

--------------------------------------------------------------------------------------------------------
--	TO DO:
--	MIGHT BE ABLE TO USE THE TRANSMISSION TO AUGMENT THE LOOKUP TO LEVEL 2 OF THE VC
--------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------
--	SearchCandidate
--------------------------------------------------------------------------------------------------------


UPDATE	SC
SET	Line = UPPER(SC.Line + ' ' + Segment)
FROM	dbo.SearchCandidate SC
	JOIN dbo.Segment S ON SC.SegmentID = S.SegmentID

--	LEFT JOIN MakeModelGrouping MMG ON SC.Make = MMG.Make AND UPPER(SC.Line + ' ' + Segment) = MMG.Model