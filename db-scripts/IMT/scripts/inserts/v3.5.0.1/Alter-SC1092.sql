UPDATE 	A
SET 	A.Mileage = ISNULL(B.Mileage,0)
FROM 	Appraisals A 
	JOIN AppraisalBookouts AB ON A.AppraisalID = AB.AppraisalID
	JOIN Bookouts B ON AB.BookoutID = B.BookoutID
	JOIN ( 	SELECT AB.AppraisalID, max(BookoutID) MaxBookoutID
		FROM AppraisalBookouts AB
		GROUP 
		BY AB.AppraisalID
		) LB ON A.AppraisalID = LB.AppraisalID AND B.BookoutID = LB.MaxBookoutID
GO
ALTER TABLE dbo.BookOuts DROP COLUMN Mileage
GO

