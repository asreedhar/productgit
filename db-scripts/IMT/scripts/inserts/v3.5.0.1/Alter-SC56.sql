if not exists(select * from dbo.syscolumns where name = 'ImageName' )
ALTER TABLE dbo.ThirdParties
    ADD ImageName varchar(200) NULL
GO

update dbo.ThirdParties set ImageName = 'images/tools/black_book_logo.gif' where thirdPartyId = 1
update dbo.ThirdParties set ImageName = 'images/tools/kelley_blue_book_logo.gif' where thirdPartyId = 3
update dbo.ThirdParties set ImageName = 'images/tools/nada_logo.gif' where thirdPartyId = 2