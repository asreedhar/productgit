DROP TABLE AppraisalWindowSticker
DROP TABLE AppraisalBump
DROP TABLE AppraisalInGroupDemand
DROP TABLE Audit_Bookouts_TradeAnalyzer

DROP TABLE AppraisalGuideBookOptions
DROP TABLE AppraisalGuideBookValues

DROP TABLE Appraisal

DROP TABLE lu_AppraisalGuideBookValueTypes
DROP TABLE tbl_VehicleGuideBookIdentifier

DROP TABLE GuideBookValue
ALTER TABLE [dbo].[DealerPreference] DROP CONSTRAINT FK_DealerPreference_GuideBookID

DROP TABLE GuideBook

DROP TABLE Audit_Bookouts_Inventory
	

ALTER TABLE AuditBookOuts DROP CONSTRAINT FK_AuditBookOuts_BookId

GO


ALTER TABLE [dbo].[AuditBookouts] ADD 
CONSTRAINT [FK_AuditBookOuts_BookId] FOREIGN KEY 
	(
		[BookId]
	) REFERENCES [dbo].[ThirdParties] (
		[ThirdPartyID]
	)

ALTER TABLE [dbo].[DealerPreference] ADD 
CONSTRAINT [FK_DealerPreference_BookOutPreferenceID] FOREIGN KEY 
	(
		[BookOutPreferenceID]
	) REFERENCES [dbo].[ThirdPartyCategories] (
		[ThirdPartyCategoryID]
	)

GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MigrateAppraisals]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE MigrateAppraisals

GO

DROP TABLE lu_GuideBook

GO

DROP TABLE DMI_STAGING_INVENTORY
GO