------------------------------------------------------------------------------------------------
--
--	NOTE! THIS ALTER SCRIPT AFFECTS THE DBASTAT DATBASE
--	WILL NEED TO GO WITH THIS APPROACH UNTIL DBASTAT IS VERSIONED CORRECTLY AND
--	CAN BE BUILT THROUGH THE AUTOMATED DEPLOYMENT PROCESS
--
------------------------------------------------------------------------------------------------

insert
into	DBASTAT..DTS_Dynamic_Views (PackageName, ViewName, SourceText, SourceType)
select	'Dataload: Process Data','vw_InventoryBookouts','InventoryBookouts',0
where	not exists (	select	1
			from	DBASTAT..DTS_Dynamic_Views
			where	PackageName = 'Dataload: Process Data'
				and ViewName = 'vw_InventoryBookouts'	
			)
insert
into	DBASTAT..DTS_Dynamic_Views (PackageName, ViewName, SourceText, SourceType)
select	'Dataload: Process Data','vw_Bookouts','Bookouts',0
where	not exists (	select	1
			from	DBASTAT..DTS_Dynamic_Views
			where	PackageName = 'Dataload: Process Data'
				and ViewName = 'vw_Bookouts'	
			)
GO
------------------------------------------------------------------------------------------------
--
--	NOW, MAKE SURE THE VIEW INFO IS IN DBASTAT.  REBUILD THE VIEWS, SO THE WE CAN
--	ALTER THE PROCEDURE SUCCESSFULLY.
--
------------------------------------------------------------------------------------------------

declare @database varchar(128)
set @database = db_name()

exec DBASTAT..Create_DTS_Dynamic_Views 'Dataload: Process Data',@database
GO

USE DBASTAT
GO

alter proc dbo.Dataload_ProcessInventoryUpdates
------------------------------------------------------------------------------------------------
--
--	Updates Inventory Rows for Updates
--
---History--------------------------------------------------------------------------------------
--	
--	JMB	08/18/2004	Initial design/dev
--	JMB	09/22/2004	JIRA: DBA-130
--	JMB	11/09/2004	JIRA: INS-2760
--	JMB	11/16/2004	JIRA: INS-2844
--	WGH	02/15/2005	JIRA: INS-2940 - Repricing Vehicles
--		08/08/2005	RALLY: SC254-TA298 -  Data Load should check the 
--				appraisal for "Accurate Bookout"
------------------------------------------------------------------------------------------------
as
set nocount on
------------------------------------------------------------------------------------------------
--	DTSStep_DTSExecuteSQLTask_38:		Update Inventory for Updates
------------------------------------------------------------------------------------------------

update 
	vw_Inventory
set
	InventoryReceivedDate = S.InventoryReceivedDate,
	ModifiedDT = S.ModifiedDT,
	DMSReferenceDt	= S.DMSReferenceDt,
	UnitCost = S.UnitCost,
	AcquisitionPrice = S.AcquisitionPrice,
	Pack = S.Pack,
	TradeOrPurchase = S.TradeOrPurchase,
	ListPrice = case when I.ListPriceLock = 1 then I.ListPrice else S.ListPrice end,
	InventoryType = S.InventoryType,
	ReconditionCost = S.ReconditionCost,
	UsedSellingPrice = S.UsedSellingPrice,
	Certified = S.Certified,
	VehicleLocation = S.VehicleLocation,
	Audit_ID = S.Audit_ID,

	MileageReceived = case 	when isnull(AIB.IsAccurate,0) = 0 		-- Only update when there is no Bookout for the Inventory
				then S.MileageReceived				-- or if the existing Bookout's IsAccurate flag is set to zero
				else I.MileageReceived
                          end,

	InventoryActive = case	when S.Delta_Flag = 'A' then 1 --unwinds
				else I.inventoryactive
		  	  end,

	DeleteDT = case when S.Delta_Flag = 'A' then null
			else I.DeleteDT
		   end
	
	
from
	vw_Inventory I
	join DMI_STAGING_INVENTORY S on S.StockNumber = I.StockNumber and S.BusinessUnitID = I.BusinessUnitID and S.VehicleID = I.VehicleID
	left join (	select 	IB.InventoryID, IsAccurate
			from 	vw_InventoryBookouts IB
				join vw_Bookouts B on IB.BookoutID = B.BookoutID
			) AIB on I.InventoryID = AIB.InventoryID
where
	(S.delta_flag = 'U' and dbo.ToDate(S.ModifiedDT) > dbo.ToDate(I.ModifiedDt) )
	or (S.delta_flag = 'A') -- already checked the date in previous step to get unwindws into update bucket
GO