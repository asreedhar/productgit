------------------------------------------------------------------------------------------------
--	NEW CIA MODEL
--
--	POPULATE REFERENCE TABLES	
----------------------------------------------------------------------------------------------------------------

SET IDENTITY_INSERT CIASummaryStatus ON

INSERT
INTO	CIASummaryStatus (CIASummaryStatusID, Description)
SELECT 	StatusId,StatusName
FROM	lu_Status		--	SEEMS A BIT GENERIC?

SET IDENTITY_INSERT CIASummaryStatus OFF


SET IDENTITY_INSERT CIAGroupingItemDetailTypes ON

INSERT
INTO	CIAGroupingItemDetailTypes (CIAGroupingItemDetailTypeID, Description)
SELECT	0, 'No Preference'
UNION 
SELECT	1, 'Prefer'
UNION
SELECT	2, 'Avoid'
UNION	
SELECT	3, 'Buy'
UNION	
SELECT	4, 'OptimalStockingLevel'

SET IDENTITY_INSERT CIAGroupingItemDetailTypes OFF


SET IDENTITY_INSERT CIAGroupingItemDetailLevels ON

INSERT
INTO	CIAGroupingItemDetailLevels (CIAGroupingItemDetailLevelID,Description)
SELECT 	CIAPlanTypeId, Description 
FROM 	lu_CIAPlanType


SET IDENTITY_INSERT CIAGroupingItemDetailLevels ON

SET IDENTITY_INSERT CIAGroupingItemDetailLevels ON

INSERT
INTO	CIAGroupingItemDetailLevels (CIAGroupingItemDetailLevelID, Description)
SELECT	4, 'UnitCost'
UNION
SELECT	5, 'Grouping'

SET IDENTITY_INSERT CIAGroupingItemDetailLevels OFF

SET IDENTITY_INSERT CIACategories ON 

INSERT
INTO	CIACategories (CIACategoryID, Description)
SELECT	CIACategoryId, CIACategoryDescription
FROM	lu_CIACategory

SET IDENTITY_INSERT CIACategories OFF
