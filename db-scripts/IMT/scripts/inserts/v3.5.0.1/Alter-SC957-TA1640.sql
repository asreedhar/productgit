------------------------------------------------------------------------------------------------
--	write alter script to find appraisals with only one ThirdPartyVehicle that has 
--	status = 'false' (script should set the status = 'true')
------------------------------------------------------------------------------------------------

UPDATE	TPV
SET	TPV.Status = 1
FROM 	AppraisalThirdPartyVehicles ATPV
 	JOIN ThirdPartyVehicles TPV ON ATPV.ThirdPartyVehicleID = TPV.ThirdPartyVehicleID
	JOIN (	SELECT 	ATPV.AppraisalID
		FROM 	AppraisalThirdPartyVehicles ATPV
		 	JOIN ThirdPartyVehicles TPV ON ATPV.ThirdPartyVehicleID = TPV.ThirdPartyVehicleID
		WHERE	TPV.ThirdPartyID <> 5
		GROUP
		BY 	ATPV.AppraisalID
		HAVING 	SUM(Status) = 0
		) ZS ON ATPV.AppraisalID = ZS.AppraisalID AND TPV.ThirdPartyID <> 5
GO