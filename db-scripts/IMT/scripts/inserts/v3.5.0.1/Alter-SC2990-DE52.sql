IF EXISTS (SELECT * FROM dbo.sysobjects where id = OBJECT_ID(N'[dbo].[FK_Appraisal__MakeModelGroupingId__MakeModelGroupingId]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
	ALTER TABLE APPRAISAL DROP CONSTRAINT FK_Appraisal__MakeModelGroupingId__MakeModelGroupingId
GO


DECLARE @Mapping TABLE (Search VARCHAR(25),Replace VARCHAR(25))

INSERT
INTO	@Mapping (Search, Replace)
SELECT	'VAN VAN' Search,'VAN' Replace
UNION	
SELECT	'COUPE COUPE','COUPE'
UNION
SELECT	'WAGON WAGON','WAGON'
UNION
SELECT	'SUV SUV','SUV'
UNION
SELECT	'SEDAN SEDAN','SEDAN'
UNION
SELECT	'TRUCK TRUCK','TRUCK'
UNION
SELECT	'CONVERTIBLE CONVERTIBLE','CONVERTIBLE'

UPDATE	GD
SET	GroupingDescription = REPLACE(GroupingDescription, ' ' + Search, ' ' + UPPER(Replace))

FROM	tbl_GroupingDescription GD
	JOIN @Mapping M ON GD.GroupingDescription LIKE '%' + ' ' + Search + '%'
WHERE	NOT EXISTS (SELECT 1 FROM tbl_GroupingDescription GD2 WHERE GD2.GroupingDescription = REPLACE(GD.GroupingDescription, ' ' + Search, ' ' + UPPER(Replace)))

DECLARE @GroupingDescriptionID INT,
	@Replacement		INT

SELECT 	@GroupingDescriptionID = GroupingDescriptionID 
FROM 	tbl_GroupingDescription 
WHERE 	GroupingDescription = 'CHEVROLET EXPRESS VAN VAN'

SELECT 	@Replacement = GroupingDescriptionID 
FROM 	tbl_GroupingDescription 
WHERE 	GroupingDescription = 'CHEVROLET EXPRESS VAN'

UPDATE	tbl_MakeModelGrouping
SET	GroupingDescriptionID = @Replacement
WHERE 	GroupingDescriptionID = @GroupingDescriptionID

DELETE FROM GDLight WHERE GroupingDescriptionID = @GroupingDescriptionID
DELETE FROM tbl_GroupingDescription WHERE GroupingDescriptionID = @GroupingDescriptionID

DECLARE @MakeModelGroupingID 	INT

SELECT 	@MakeModelGroupingID = MakeModelGroupingID
FROM	tbl_MakeModelGrouping 
WHERE	Make = 'DODGE'
	AND Model = 'RAM 1500 TRUCK'

SELECT 	@Replacement = MakeModelGroupingID
FROM	tbl_MakeModelGrouping 
WHERE	Make = 'DODGE'
	AND Model = 'RAM 1500'

UPDATE	Vehicle
SET	MakeModelGroupingID = @Replacement
WHERE	MakeModelGroupingID = @MakeModelGroupingID

DELETE
FROM	tbl_MakeModelGrouping
WHERE	MakeModelGroupingID = @MakeModelGroupingID

SELECT 	@MakeModelGroupingID = MakeModelGroupingID
FROM	tbl_MakeModelGrouping 
WHERE	Make = 'CHEVROLET'
	AND Model = 'EXPRESS VAN'

SELECT 	@Replacement = MakeModelGroupingID
FROM	tbl_MakeModelGrouping 
WHERE	Make = 'CHEVROLET'
	AND Model = 'EXPRESS'

UPDATE	Vehicle
SET	MakeModelGroupingID = @Replacement
WHERE	MakeModelGroupingID = @MakeModelGroupingID

DELETE
FROM	tbl_MakeModelGrouping
WHERE	MakeModelGroupingID = @MakeModelGroupingID


UPDATE	MMG1
SET	MMG1.Model = REPLACE(MMG1.Model, ' ' + M.Replace, '')
FROM	tbl_MakeModelGrouping MMG1
	JOIN MakeModelGrouping MMG2 ON MMG1.MakeModelGroupingID = MMG2.MakeModelGroupingID
	JOIN @Mapping M ON MMG2.Model LIKE '%' + ' ' + Search + '%'
WHERE	NOT EXISTS (SELECT 1 FROM tbl_MakeModelGrouping MMG3 WHERE MMG1.Make = MMG3.Make AND MMG1.EdmundsBodyType = MMG3.EdmundsBodyType AND MMG3.Model = REPLACE(MMG1.Model, ' ' + M.Replace, ''))
