----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------
--	BUCKETRON
----------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------

SET IDENTITY_INSERT CIABucketGroups ON 

INSERT
INTO	CIABucketGroups (CIABucketGroupID, Description)
SELECT	1, 'Year'
UNION	
SELECT	2, 'UnitCost'
UNION	
SELECT	3, 'Age in Years'


SET IDENTITY_INSERT CIABucketGroups OFF 

SET IDENTITY_INSERT CIABucketRuleTypes ON 
INSERT
INTO	CIABucketRuleTypes (CIABucketRuleTypeID, Description, ColumnName)
SELECT	1, 'Year', 'VehicleYear'
UNION
SELECT	2, 'Unit Cost Range','UnitCost'
UNION
SELECT	3, 'Age in Years','n/a'

SET IDENTITY_INSERT CIABucketRuleTypes OFF

DELETE FROM CIABuckets

SET IDENTITY_INSERT CIABuckets ON

INSERT
INTO	CIABuckets (CIABucketID, CIABucketGroupID, Rank, Description)
SELECT	1, 1, 1, '2006'
UNION
SELECT	2, 1, 2, '2005'
UNION
SELECT	3, 1, 3, '2004'
UNION
SELECT	4, 1, 4, '2003'
UNION
SELECT	5, 1, 5, '2002'
UNION
SELECT	6, 1, 6, '2001'
UNION
SELECT	7, 1, 7, '2000'
UNION
SELECT	8, 2, 1, '4000-8000'
UNION
SELECT	9, 2, 2, '8000-12000'
UNION
SELECT	10, 2, 3, '12000-16000'
UNION
SELECT	11, 2, 4, '16000-20000'
UNION
SELECT	12, 2, 5, '20000-24000'
UNION
SELECT	13, 3, 1, '0-1'
UNION
SELECT	14, 3, 2, '2'
UNION
SELECT	15, 3, 3, '3'
UNION
SELECT	16, 3, 4, '4'
UNION
SELECT	17, 3, 5, '5'
UNION
SELECT	18, 3, 6, '6'
UNION
SELECT	19, 1, 8, '1999'

SET IDENTITY_INSERT CIABuckets OFF

INSERT
INTO	CIABucketRules (CIABucketID, CIABucketRuleTypeID, Value, Value2)
SELECT	1, 1, 2006, NULL
UNION
SELECT	2, 1, 2005, NULL
UNION
SELECT	3, 1, 2004, NULL
UNION
SELECT	4, 1, 2003, NULL
UNION
SELECT	5, 1, 2002, NULL
UNION
SELECT	6, 1, 2001, NULL
UNION
SELECT	7, 1, 2000, NULL
UNION
SELECT	8, 1, 4000, 8000
UNION	
SELECT	9, 1, 8000, 12000
UNION
SELECT	10, 2, 12000,16000
UNION
SELECT	11, 2, 16000,20000
UNION
SELECT	13, 3, 0, NULL
UNION
SELECT	13, 3, 1, NULL
UNION
SELECT	14, 3, 2, NULL
UNION
SELECT	15, 3, 3, NULL
UNION
SELECT	16, 3, 4, NULL
UNION
SELECT	17, 3, 5, NULL
UNION
SELECT	18, 3, 6, NULL
UNION
SELECT	19, 1, 1999, NULL