------------------------------------------------------------------------------------------------
--	POPULATE REFERENCE TABLES
------------------------------------------------------------------------------------------------

INSERT
INTO	ThirdParties (ThirdPartyID,Description,Name,DefaultFooter)

SELECT 	GuideBookId,GuideBookDescription,GuideBookName , null
FROM	lu_GuideBook
UNION
SELECT 	5, 'Edmunds', 'EDMUNDS', null

UPDATE	ThirdParties	
SET	DefaultFooter = 'All Black Book values are reprinted with permission of Black Book &#174;.'
WHERE 	ThirdPartyID = 1

UPDATE	ThirdParties
SET 	DefaultFooter = 'All NADA values are reprinted with permission of N.A.D.A Official Used Car Guide &#174; Company &#169; NADASC 2000'
WHERE 	ThirdPartyID = 2

UPDATE	ThirdParties	
SET 	DefaultFooter = 'All Kelley Blue Book values are reprinted with permission of Kelley Blue Book.'
WHERE 	ThirdPartyID = 3

GO

SET IDENTITY_INSERT AppraisalActionTypes ON

INSERT
INTO	AppraisalActionTypes (AppraisalActionTypeID, Description)
SELECT 	1, 'Place in Retail Inventory'
UNION
SELECT 	2, 'Offer to Group (Wholesale)'
UNION
SELECT 	3, 'Sell to Wholesaler or Auction'
UNION
SELECT 	4, 'Not Traded-In'
UNION
SELECT 	5, 'Decide Later'

SET IDENTITY_INSERT AppraisalActionTypes OFF

GO

SET IDENTITY_INSERT ThirdPartyOptionTypes ON

INSERT
INTO	ThirdPartyOptionTypes (ThirdPartyOptionTypeID, OptionTypeName)
SELECT	0, 'General'
UNION
SELECT	1, 'Equipment'
UNION
SELECT	2, 'Engine'
UNION
SELECT	3, 'Transmission'
UNION
SELECT	4, 'Drivetrain'

SET IDENTITY_INSERT ThirdPartyOptionTypes OFF

GO

SET IDENTITY_INSERT ThirdPartyVehicleOptionValueTypes ON

INSERT
INTO	ThirdPartyVehicleOptionValueTypes (ThirdPartyOptionValueTypeID, OptionValueTypeName)
SELECT	0, 'N/A'
UNION
SELECT	1, 'Retail'
UNION
SELECT	2, 'Wholesale'

SET IDENTITY_INSERT ThirdPartyVehicleOptionValueTypes OFF

GO

SET IDENTITY_INSERT BookoutValueTypes ON
INSERT
INTO	BookoutValueTypes (BookoutValueTypeID, Description)
SELECT	1, 'Base Value'
UNION
SELECT	2, 'Final Value'
UNION
SELECT	3, 'Mileage Independent'

SET IDENTITY_INSERT BookoutValueTypes OFF

GO

SET IDENTITY_INSERT BookoutSources ON

INSERT
INTO	BookoutSources (BookoutSourceID, Description)
SELECT	1, 'Appraisal'
UNION
SELECT	2, 'Bookout Processor'
UNION
SELECT	3, 'Vehicle Detail Page'

SET IDENTITY_INSERT BookoutSources OFF

------------------------------------------------------------------------------------------------
--	INSERT GUIDEBOOK CATEGORIES
------------------------------------------------------------------------------------------------

INSERT
INTO	ThirdPartyCategories (ThirdPartyID, Category)
SELECT 	LGB.GuideBookID, dbo.ToMixedCase(GuideBookCategory)
FROM 	GuideBook GB
	JOIN lu_GuideBook LGB on GB.GuideBookName = LGB.GuideBookName

UPDATE	ThirdPartyCategories
SET	Category = 'Trade-In'
WHERE	Category = 'Trade-in'

UPDATE	ThirdPartyCategories
SET	Category = 'Extra Clean'
WHERE	Category = 'Extraclean'

GO

------------------------------------------------------------------------------------------------
--	CREATE BOOKOUTS FROM APPRAISAL 
--	Bookouts.VehicleGuideBookID IS FOR MIGRATION PURPOSES ONLY
------------------------------------------------------------------------------------------------

insert
into	Bookouts (BookoutSourceID, ThirdPartyID, DateCreated, Region, DatePublished, BookPrice, Weight, MileageCostAdjustment, MSRP, Mileage, LegacyID)
select	1, GuideBookID, TradeAnalyzerDate, GuideBookRegion, KelleyPublishInfo, BookPrice, Weight, MileageCostAdjustment, MSRP, Mileage, A.VehicleGuideBookID
from	Appraisal A

insert
into	BookoutThirdPartyCategories (BookoutID, ThirdPartyCategoryID, DateCreated)
select 	distinct B.BookoutID, TPC.ThirdPartyCategoryID, B.DateCreated
from 	AppraisalGuideBookValues AGBV
	join Appraisal A on AGBV.VehicleGuideBookID = A.VehicleGuideBookID
 	join Bookouts B on B.BookoutSourceID = 1 and AGBV.VehicleGuideBookID = B.LegacyID
 	join ThirdPartyCategories TPC on B.ThirdPartyID = TPC.ThirdPartyID and AGBV.Title = TPC.Category

insert
into	BookoutValues (BookoutThirdPartyCategoryID, BookoutValueTypeID, Value, DateCreated)
select 	distinct BTPC.BookoutThirdPartyCategoryID, 
	case ValueType when 1 then 1	-- Base Value
		       when 2 then 2	-- Final Value
		       when 3 then 3	-- Mileage Independent  (MAPPING NOT REQ'D, LEFT IN JUST-IN-CASE)
	end BookoutValueTypeID, 
	AGBV.Value, BTPC.DateCreated
from 	AppraisalGuideBookValues AGBV
	join Bookouts B on B.BookoutSourceID = 1 and AGBV.VehicleGuideBookID = B.LegacyID
	join ThirdPartyCategories TPC on B.ThirdPartyID = TPC.ThirdPartyID and AGBV.Title = TPC.Category
	join BookoutThirdPartyCategories BTPC on B.BookoutID = BTPC.BookoutID and TPC.ThirdPartyCategoryID = BTPC.ThirdPartyCategoryID 

GO

------------------------------------------------------------------------------------------------
--	CREATE BOOKOUTS FROM GuideBookValue
--	Bookouts.LegacyID IS FOR MIGRATION PURPOSES ONLY
------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------------------------
--	CREATE A BOOKOUT FOR THE INITIAL AND CURRENT BOOKOUTS
--	FOR MIGRATION ONLY: USE THE IsAccurate FIELD TO STORE WHETHER OR NOT IT IS A CURRENT OR ACCURATE BOOKOUT
--------------------------------------------------------------------------------------------------------------------------------

insert
into	Bookouts (BookoutSourceID, ThirdPartyID, DateCreated, LegacyID, IsAccurate, Mileage)

select 	2, TP.ThirdPartyID, max(InitialDT) DateCreated, GBV.InventoryID, 1, I.MileageReceived		
from 	GuideBookValue GBV
	join GuideBook GB on GBV.GuideBookID = GB.GuideBookID
	join ThirdParties TP on GB.GuideBookName = TP.Name
	join Inventory I on  GBV.InventoryID = I.InventoryID
where	GBV.GuideBookValueID not in (17242, 17241, 17240, 17239)
group
by	GBV.InventoryID, I.MileageReceived, TP.ThirdPartyID

union	all

select 	2, TP.ThirdPartyID, max(CurrentDT) DateCreated, GBV.InventoryID, 2, I.MileageReceived			
from 	GuideBookValue GBV
	join GuideBook GB on GBV.GuideBookID = GB.GuideBookID
	join ThirdParties TP on GB.GuideBookName = TP.Name
	join Inventory I on  GBV.InventoryID = I.InventoryID
where	GBV.GuideBookValueID not in (17242, 17241, 17240, 17239)
	and InitialDT <> CurrentDT and InitialValue <> CurrentValue		-- ONLY STORE IF MORE THAN ONE
group
by	GBV.InventoryID, I.MileageReceived, TP.ThirdPartyID

insert
into	BookoutThirdPartyCategories (BookoutID, ThirdPartyCategoryID, IsValid, DateCreated, LegacyID)

select 	B.BookoutID, TPC.ThirdPartyCategoryID, valid, B.DateCreated, GBV.GuideBookValueID
from 	GuideBookValue GBV
	join GuideBook GB on GBV.GuideBookID = GB.GuideBookID
	join ThirdParties TP on GB.GuideBookName = TP.Name
	join Bookouts B on B.BookoutSourceID = 2 and  TP.ThirdPartyID = B.ThirdPartyID and GBV.InventoryID = B.LegacyID
	join ThirdPartyCategories TPC on TP.ThirdPartyID = TPC.ThirdPartyID and case GB.GuideBookCategory when 'Extraclean' then 'Extra Clean' else GB.GuideBookCategory end = TPC.Category
where	GBV.GuideBookValueID not in (17242, 17241, 17240, 17239)		-- REMOVE THE ONLY DUPLICATES FROM THE LEGACY DATA

insert
into	BookoutValues (BookoutThirdPartyCategoryID, BookoutValueTypeID, Value, DateCreated)

select 	BTPC.BookoutThirdPartyCategoryID, 2 BookoutValueTypeID, InitialValue, InitialDT
from 	GuideBookValue GBV
	join GuideBook GB on GBV.GuideBookID = GB.GuideBookID
	join ThirdParties TP on GB.GuideBookName = TP.Name
	join Bookouts B on B.BookoutSourceID = 2 and B.IsAccurate = 1 and TP.ThirdPartyID = B.ThirdPartyID and GBV.InventoryID = B.LegacyID
	join ThirdPartyCategories TPC on TP.ThirdPartyID = TPC.ThirdPartyID and case GB.GuideBookCategory when 'Extraclean' then 'Extra Clean' else GB.GuideBookCategory end  = TPC.Category
	join BookoutThirdPartyCategories BTPC on B.BookoutID = BTPC.BookoutID and TPC.ThirdPartyCategoryID = BTPC.ThirdPartyCategoryID 
where	GBV.GuideBookValueID not in (17242, 17241, 17240, 17239)		-- REMOVE THE ONLY DUPLICATES FROM THE LEGACY DATA

union all
select  BTPC.BookoutThirdPartyCategoryID, 2 BookoutValueTypeID, CurrentValue, CurrentDT
from 	GuideBookValue GBV
	join GuideBook GB on GBV.GuideBookID = GB.GuideBookID
	join ThirdParties TP on GB.GuideBookName = TP.Name
	join Bookouts B on B.BookoutSourceID = 2 and B.IsAccurate = 2 and TP.ThirdPartyID = B.ThirdPartyID and GBV.InventoryID = B.LegacyID
	join ThirdPartyCategories TPC on TP.ThirdPartyID = TPC.ThirdPartyID and case GB.GuideBookCategory when 'Extraclean' then 'Extra Clean' else GB.GuideBookCategory end  = TPC.Category
	join BookoutThirdPartyCategories BTPC on B.BookoutID = BTPC.BookoutID and TPC.ThirdPartyCategoryID = BTPC.ThirdPartyCategoryID 
where	GBV.GuideBookValueID not in (17242, 17241, 17240, 17239)		-- REMOVE THE ONLY DUPLICATES FROM THE LEGACY DATA
GO

insert
into	InventoryBookouts (InventoryID, BookoutID)

select 	B.LegacyID, B.BookoutID
from 	Bookouts B
where	BookoutSourceID = 2

GO

update	Bookouts
set	IsAccurate = 0
where	BookoutSourceID = 2

GO
	
------------------------------------------------------------------------------------------------
--	INSERT VEHICLES FROM APPRAISAL
------------------------------------------------------------------------------------------------

insert
into	tbl_Vehicle (VehicleSourceID, Vin, VehicleYear, MakeModelGroupingID, VehicleTrim, VehicleBodyStyleID, BaseColor, VehicleEngine, VehicleDriveTrain, 
	VehicleTransmission, InteriorColor, InteriorDescription, CylinderCount, DoorCount, FuelType, CreateDt, OriginalTrim, OriginalMake, OriginalModel, OriginalYear, OriginalBodyStyle)

select 	2 VehicleSourceID, A.Vin, Year, MakeModelGroupingId, ActualTrim, 1 VehicleBodyStyleID, isnull(Color, 'N/A') Color, isnull(EngineDescription, 'N/A') EngineDescription,  isnull(DriveTrainDescription, 'N/A') DriveTrainDescription,
	isnull(Transmission, 'N/A') Transmission, InteriorColor, InteriorDescription, CylinderCount, DoorCount, isnull(FuelType, 'N/A') FuelType, A.TradeAnalyzerDate, Trim, Make, Model, Year OriginalYear, Body
from	Appraisal A
	join (	select	VIN, max(VehicleGuideBookID)  VehicleGuideBookID			-- TAKE THE LATEST VEHICLE INFO FROM APPRAISAL
		from 	Appraisal 								-- SINCE WE CAN ADD A VEHICLE ONLY ONCE
		group 
		by 	VIN 
		) LA on A.VIN = LA.VIN and A.VehicleGuideBookID = LA.VehicleGuideBookID
where	not exists (	select	1
			from	tbl_Vehicle V
			where 	A.VIN = V.VIN
			)

	------------------------------------------------------------------------------------------------
	--	SHOULD WE UPDATE ANY VEHICLE ATTRIBUTES FROM APPRAISAL?
	------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------
--	INSERT NEW Appraisals, AppraisalBumps, AppraisalActions
------------------------------------------------------------------------------------------------

insert
into	Appraisals (BusinessUnitID, VehicleID, MemberID, DealCompleted, Sold, Color, DateCreated, VehicleGuideBookID )
select	A.BusinessUnitID, V.VehicleID, A.MemberID, null DealCompleted, Sold, Color, A.TradeAnalyzerDate, A.VehicleGuideBookID
from	Appraisal A
	join tbl_Vehicle V on A.VIN = V.VIN
	join (	select	A.BusinessUnitID, A.VIN, max(A.VehicleGuideBookID) VehicleGuideBookID
		from	Appraisal A
			join tbl_Vehicle V on A.VIN = V.VIN
		group
		by	A.BusinessUnitID, A.VIN
		) LA on A.VehicleGuideBookID = LA.VehicleGuideBookID

------------------------------------------------------------------------------------------------
--	INSERT INTO APPRAISAL-BOOKOUT INTERSECTION TABLE
------------------------------------------------------------------------------------------------

insert
into	AppraisalBookouts (AppraisalID, BookoutID)

select	A.AppraisalID, B.BookoutID
from	Appraisals A
	join tbl_Vehicle V on A.VehicleID = V.VehicleID
	join Appraisal OA on A.BusinessUnitID = OA.BusinessUnitID and V.VIN = OA.VIN 
	join Bookouts B on B.BookoutSourceID = 1 and OA.VehicleGuideBookID = B.LegacyID		
GO

------------------------------------------------------------------------------------------------
--	INSERT INITIAL APPRAISAL VALUES, SEQUENCE NUMBER ONE
------------------------------------------------------------------------------------------------

insert
into	AppraisalValues (AppraisalID, SequenceNumber, DateCreated, Value, Initials)

select	AppraisalID, 1, OA.TradeAnalyzerDate, ISNULL(OA.AppraisalValue,0.0), OA.AppraisalInitials
from	Appraisals A
	join Appraisal OA on A.VehicleGuideBookID = OA.VehicleGuideBookID

------------------------------------------------------------------------------------------------
--	INSERT APPRAISAL BUMPS -- SEQUENCE NUMBER STARTS AT TWO
--	REMOVE DUPLICATES WITH THE SUB-QUERIES, JOIN TO Appraisals THROUGH Bookouts TO AVOID
--	AVOID MISSING ANY BUMPS.  FILTER OUT DUPLICATE BUMPS THAT WHACK OUT THE MIGRATION
------------------------------------------------------------------------------------------------

insert
into	AppraisalValues (AppraisalID, SequenceNumber, DateCreated, Value, Initials)

select	A.AppraisalID, SQ.SequenceNumber+1 SequenceNumber, A.DateCreated, Bump, Initials
from	Appraisals A
	join AppraisalBookouts AB on A.AppraisalID = AB.AppraisalID
	join Bookouts B on AB.BookoutID = B.BookoutID
	join AppraisalBump BP on B.LegacyID = BP.VehicleGuideBookID

	join (	select	AB1.VehicleGuideBookBumpID, count(*) SequenceNumber
		from	(	select	BP.VehicleGuideBookID, BP.Bump, BP.Initials, max(VehicleGuideBookBumpID) VehicleGuideBookBumpID
				from	Appraisals A
					join AppraisalBookouts AB on A.AppraisalID = AB.AppraisalID
					join Bookouts B on AB.BookoutID = B.BookoutID
					join AppraisalBump BP on B.LegacyID = BP.VehicleGuideBookID
				where	BP.VehicleGuideBookBumpID not in (2098,2500)
				group
				by	BP.VehicleGuideBookID, BP.Bump, BP.Initials
				) AB1
			join (	select	BP.VehicleGuideBookID, BP.Bump, BP.Initials, max(VehicleGuideBookBumpID) VehicleGuideBookBumpID
				from	Appraisals A
					join AppraisalBookouts AB on A.AppraisalID = AB.AppraisalID
					join Bookouts B on AB.BookoutID = B.BookoutID
					join AppraisalBump BP on B.LegacyID = BP.VehicleGuideBookID
				where	BP.VehicleGuideBookBumpID not in (2098,2500)
				group
				by	BP.VehicleGuideBookID, BP.Bump, BP.Initials
				) AB2 on AB1.VehicleGuideBookID = AB2.VehicleGuideBookID and AB1.VehicleGuideBookBumpID >= AB2.VehicleGuideBookBumpID
		group
		by	AB1.VehicleGuideBookBumpID
		) SQ on BP.VehicleGuideBookBumpID = SQ.VehicleGuideBookBumpID

where	BP.VehicleGuideBookBumpID not in (2098,2500)

------------------------------------------------------------------------------------------------
--	INSERT APPRAISAL ACTIONS
------------------------------------------------------------------------------------------------
insert
into	AppraisalActions (AppraisalID, AppraisalActionTypeID, WholesalePrice, EstimatedReconditioningCost, ConditionDescription, DateCreated)

select	A.AppraisalID, ActionID, Price, case when isnumeric(OA.ReconditioningNotes) = 1 then dbo.ToInteger(OA.ReconditioningNotes) else null end EstReconCost, ConditionDisclosure,
	max(B.DateCreated) DateCreated
from	Appraisals A
	join AppraisalBookouts AB on A.AppraisalID = AB.AppraisalID
	join Bookouts B on AB.BookoutID = B.BookoutID
	join Appraisal OA on B.LegacyID = OA.VehicleGuideBookID

group
by	A.AppraisalID, ActionID, Price, case when isnumeric(OA.ReconditioningNotes) = 1 then dbo.ToInteger(OA.ReconditioningNotes) else null end, ConditionDisclosure

insert
into 	AppraisalBusinessUnitGroupDemand (AppraisalID, BusinessUnitID)
select	AB.AppraisalID, D.BusinessUnitID
from	AppraisalInGroupDemand D 
	join Bookouts B on B.BookoutSourceID = 1 and D.VehicleGuideBookID = B.LegacyID
	join AppraisalBookouts AB on B.BookoutID = AB.BookoutID
GO	

------------------------------------------------------------------------------------------------
--	INSERT INTO ThirdPartyOptions
------------------------------------------------------------------------------------------------

insert
into	ThirdPartyOptions (OptionName, OptionKey, ThirdPartyOptionTypeID)
select	distinct OptionName, OptionKey, isnull(Type,0)
from 	AppraisalGuideBookOptions

------------------------------------------------------------------------------------------------
--	INSERT INTO ThirdPartyVehicles.  ALL ARE "SELECTED" IN THE OLD MODEL
--	NOT SURE WE NEED A DISTINCT...NEED BU AS KEY?
------------------------------------------------------------------------------------------------

insert
into	ThirdPartyVehicles (ThirdPartyID, ThirdPartyVehicleCode, Description, MakeCode, ModelCode, DateCreated, Status, VehicleGuideBookID)

select	A.GuideBookID, A.SelectedKey, A.GuideBookDescription, A.KelleyMakeCode, A.KelleyModelCode, A.TradeAnalyzerDate, 1, LA.VehicleGuideBookID
from	Appraisal A 
	join Vehicle V on A.VIN = V.VIN
	join (	select	A.BusinessUnitID, A.VIN, max(A.VehicleGuideBookID) VehicleGuideBookID
		from	Appraisal A
			join tbl_Vehicle V on A.VIN = V.VIN
		group
		by	A.BusinessUnitID, A.VIN
		) LA on A.BusinessUnitID = LA.BusinessUnitID and A.VIN = LA.VIN
union
select	5, A.EdStyleID, S.style, null, null, A.TradeAnalyzerDate, 1, VehicleGuideBookID
from	Appraisal A 
	join Vehicle V on A.VIN = V.VIN
	join vw_Style S on A.EdStyleID = S.ed_style_id

insert
into	AppraisalThirdPartyVehicles (AppraisalID, ThirdPartyVehicleID)
select 	AppraisalID, TPV.ThirdPartyVehicleID
from 	Appraisals A
	join Appraisal A1 on A.VehicleGuideBookID = A1.VehicleGuideBookID
	join ThirdPartyVehicles TPV on A1.VehicleGuideBookID = TPV.VehicleGuideBookID
	
GO

------------------------------------------------------------------------------------------------
--	INSERT INTO ThirdPartyVehicleOptions.  
------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------
--	For a given VehicleGuideBookID, there may be duplicates (???) so just take the distinct 
--	set to be sure
------------------------------------------------------------------------------------------------
	
------------------------------------------------------------------------------------------------
--	FILTER DUPLICATES FROM WORKING SET
------------------------------------------------------------------------------------------------

	
if  object_id('tempdb.dbo.#AGBO_F') is not null drop table #AGBO_F
if  object_id('tempdb.dbo.#AGBO_SO') is not null drop table #AGBO_SO

create table #AGBO_F (	VehicleGuideBookOptionsID int not null, Vin varchar(17) not null, OptionName varchar(255) not null, 
			OptionKey varchar(255) not null, IsSelected tinyint not null, VehicleGuideBookID int null, 
			OptionValue int not null, Type tinyint not null, WholesaleValue int null, standardOption tinyint not null
			)
insert
into	#AGBO_F (VehicleGuideBookOptionsID, Vin, OptionName, OptionKey, IsSelected, VehicleGuideBookID, OptionValue, Type, WholesaleValue, standardOption)

select 	max(VehicleGuideBookOptionsID) VehicleGuideBookOptionsID, AGBO.Vin, OptionName, OptionKey, IsSelected, AGBO.VehicleGuideBookID, OptionValue, Type, WholesaleValue, standardOption
from  	AppraisalGuideBookOptions AGBO
	join Appraisal A on AGBO.VehicleGuideBookID = A.VehicleGuideBookID
group
by	AGBO.Vin, OptionName, OptionKey, IsSelected, AGBO.VehicleGuideBookID, OptionValue, Type, WholesaleValue, standardOption

create index #IX_AGBO_F on #AGBO_F(VehicleGuideBookID, VehicleGuideBookOptionsID)

----------------------------------------------------------------------------------------------------
--	NOW, GET SORT ORDER BASED ON IDENTITY COLUMN VehicleGuideBookOptionsID
--	THE ROWS WERE INSERTED IN THE PROPER ORDER, USE THE IDENTITY COLUMN TO GET A TRUE ORDERING
----------------------------------------------------------------------------------------------------

create table #AGBO_SO (VehicleGuideBookOptionsID int not null, SortOrder int not null)

insert
into	#AGBO_SO
select	A.VehicleGuideBookOptionsID, count(*) SortOrder
from 	#AGBO_F A
	join #AGBO_F B on A.VehicleGuideBookID = B.VehicleGuideBookID and A.VehicleGuideBookOptionsID >= B.VehicleGuideBookOptionsID
group
by	A.VehicleGuideBookOptionsID

create index #IX_AGBO_F_2 on #AGBO_F(VehicleGuideBookOptionsID)
create index #IX_AGBO_SO on #AGBO_SO(VehicleGuideBookOptionsID)

select	AGBO.Vin, OptionName, OptionKey, IsSelected, AGBO.VehicleGuideBookID, OptionValue, Type, WholesaleValue, standardOption, SO.SortOrder
into	#AGBO
from 	#AGBO_F AGBO
	join #AGBO_SO SO on AGBO.VehicleGuideBookOptionsID = SO.VehicleGuideBookOptionsID

----------------------------------------------------------------------------------------------------
--	POPULATE TPVO
----------------------------------------------------------------------------------------------------
		
insert
into	ThirdPartyVehicleOptions (ThirdPartyVehicleID, ThirdPartyOptionID, IsStandardOption, Status, SortOrder, DateCreated)

select	distinct TPV.ThirdPartyVehicleID, TPO.ThirdPartyOptionID, cast(AGBO.standardOption as tinyint), AGBO.IsSelected, AGBO.SortOrder - 1, A.DateCreated
from 	#AGBO AGBO
	join Appraisal AO on AGBO.VehicleGuideBookID = AO.VehicleGuideBookID
	join Appraisals A on AO.BusinessUnitID = A.BusinessUnitID 
	join Vehicle V on A.VehicleID = V.VehicleID and AO.VIN = V.VIN
 	join AppraisalThirdPartyVehicles ATPV on A.AppraisalID = ATPV.AppraisalID
 	join ThirdPartyVehicles TPV on TPV.ThirdPartyID <> 5 and ATPV.ThirdPartyVehicleID = TPV.ThirdPartyVehicleID and AO.GuideBookID = TPV.ThirdPartyID
 	join ThirdPartyOptions TPO on AGBO.OptionName = TPO.OptionName and AGBO.OptionKey = TPO.OptionKey

insert
into	ThirdPartyVehicleOptionValues (ThirdPartyVehicleOptionID, ThirdPartyOptionValueTypeID, Value, DateCreated)

select 	TVO.ThirdPartyVehicleOptionID, case when TPV.ThirdPartyID = 3 then 1 else 0 end ThirdPartyOptionValueTypeID, OptionValue, A.DateCreated
from 	#AGBO AGBO
	join Appraisal AO on AGBO.VehicleGuideBookID = AO.VehicleGuideBookID
	join Appraisals A on AO.BusinessUnitID = A.BusinessUnitID 
	join Vehicle V on A.VehicleID = V.VehicleID and AO.VIN = V.VIN
	join AppraisalThirdPartyVehicles ATPV on A.AppraisalID = ATPV.AppraisalID
	join ThirdPartyVehicles TPV on TPV.ThirdPartyID <> 5 and ATPV.ThirdPartyVehicleID = TPV.ThirdPartyVehicleID and AO.GuideBookID = TPV.ThirdPartyID
	join ThirdPartyOptions TPO on AGBO.OptionName = TPO.OptionName and AGBO.OptionKey = TPO.OptionKey
	join ThirdPartyVehicleOptions TVO on TPV.ThirdPartyVehicleID = TVO.ThirdPartyVehicleID
						and TPO.ThirdPartyOptionID = TVO.ThirdPartyOptionID
						and A.DateCreated = TVO.DateCreated
union
select 	TVO.ThirdPartyVehicleOptionID, 2 ThirdPartyOptionValueTypeID, WholesaleValue, A.DateCreated
from 	#AGBO AGBO
	join Appraisal AO on AGBO.VehicleGuideBookID = AO.VehicleGuideBookID
	join Appraisals A on AO.BusinessUnitID = A.BusinessUnitID 
	join Vehicle V on A.VehicleID = V.VehicleID and AO.VIN = V.VIN
	join AppraisalThirdPartyVehicles ATPV on A.AppraisalID = ATPV.AppraisalID
	join ThirdPartyVehicles TPV on TPV.ThirdPartyID <> 5 and ATPV.ThirdPartyVehicleID = TPV.ThirdPartyVehicleID and AO.GuideBookID = TPV.ThirdPartyID
	join ThirdPartyOptions TPO on AGBO.OptionName = TPO.OptionName and AGBO.OptionKey = TPO.OptionKey
	join ThirdPartyVehicleOptions TVO on TPV.ThirdPartyVehicleID = TVO.ThirdPartyVehicleID
						and TPO.ThirdPartyOptionID = TVO.ThirdPartyOptionID
						and A.DateCreated = TVO.DateCreated
where	TPV.ThirdPartyID = 3
GO
	
------------------------------------------------------------------------------------------------
--	Migrate AppraisalWindowSticker
------------------------------------------------------------------------------------------------

INSERT
INTO	AppraisalWindowStickers (AppraisalID, StockNumber, SellingPrice)
SELECT	AB.AppraisalID, StockNumber, SellingPrice
FROM	AppraisalWindowSticker AWS
	JOIN Bookouts B ON AWS.AppraisalID = B.LegacyID
	JOIN AppraisalBookouts AB ON B.BookoutID = AB.BookoutID

------------------------------------------------------------------------------------------------
--	Add indexes post-migration inserts
------------------------------------------------------------------------------------------------

CREATE  INDEX [IX_BookoutValues_ValueTypeCategoryID] ON [dbo].[BookoutValues]([BookoutThirdPartyCategoryID], [BookoutValueTypeID] ) ON [DATA]
GO

CREATE  INDEX [IX_InventoryBookouts_InventoryID] ON [dbo].[InventoryBookouts]([InventoryID]) ON [DATA]
GO

CREATE  INDEX [IX_BookoutThirdPartyCategories_BookoutID] ON [dbo].[BookoutThirdPartyCategories]([BookoutID]) ON [DATA]
GO


------------------------------------------------------------------------------------------------
--	Add dummy rows to tables for DRI-issues related to AuditBookouts table
------------------------------------------------------------------------------------------------


SET IDENTITY_INSERT Bookouts ON

INSERT
INTO	Bookouts (BookoutID, BookoutSourceID, ThirdPartyID, LegacyID)
SELECT	-1, 1, 1, 0

SET IDENTITY_INSERT Bookouts OFF

SET IDENTITY_INSERT Member ON

INSERT
INTO	Member (MemberID, Login, FirstName, LastName, OfficePhoneNumber, UserRoleCD, JobTitleID)
SELECT	-1, 'DUMMY FOR DRI', 'DUMMY', 'DUMMY','(XXX) XXX-XXXX', 1, 17

SET IDENTITY_INSERT Member OFF

