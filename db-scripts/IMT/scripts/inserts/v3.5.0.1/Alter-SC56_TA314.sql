IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[FK_AuditBookOuts_BookOutId]') AND OBJECTPROPERTY(id, N'IsForeignKey') = 1)

	ALTER TABLE dbo.AuditBookOuts
	    ADD CONSTRAINT [FK_AuditBookOuts_BookOutId] FOREIGN KEY 
	    (
	        [BookOutId]
	    ) REFERENCES [BookOuts] (
	        [BookOutId]
	    )
GO