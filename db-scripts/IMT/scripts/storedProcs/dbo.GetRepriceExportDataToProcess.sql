CREATE PROCEDURE dbo.GetRepriceExportDataToProcess
AS
/* MAK 12/22/08 
	This stored procedure replaces the Java code.  It is similar to the original
	in line SQLbut has No Locks (locking was the original problem), and the subquery has
	been put into a temporary table.*/
/* Not in manifest yet as not scheduled for deployment.*/

CREATE TABLE #MaxLastRepriceInventory
	( InventoryID Int,
	LastRepriceDate DateTime,
	MaxID	INT)
INSERT
INTO	#MaxLastRepriceInventory
	(InventoryID,
	LastRepriceDate,
	MaxID)
SELECT	re.InventoryID, 
	max(re.Created) as LastestRepriceDate, 
	max(e.RepriceExportID) as maxId  		
FROM	RepriceExport e  	  WITH (NOLOCK)
JOIN	RepriceEvent re  WITH (NOLOCK)
on	e.RepriceEventId = re.RepriceEventId  		
JOIN	ThirdPartyEntity tp 
on	e.ThirdPartyEntityID = tp.ThirdPartyEntityID  		
JOIN	Inventory i2  WITH (NOLOCK)
ON	i2.inventoryId = re.inventoryId  		
LEFT 
JOIN	DMSExportBusinessUnitPreference bup 
on	bup.businessUnitId = i2.businessUnitId  
LEFT
JOIN	DMSExport_ThirdPartyEntity DTP
ON	TP.ThirdPartyEntityID =DTP.DMSExportID		
WHERE	(bup.isActive = 1  		
AND	(bup.DMSExportFrequencyID = 2 
	OR (bup.dmsExportDailyHourOfDay <= DatePart(hh,GetDate())
	AND e.DateCreated < dateadd(hh, bup.dmsExportDailyHourOfDay,dbo.ToDate(getdate())) 
	))) 		
	OR	DTP.DMSExportID is null 		
	GROUP BY re.InventoryID, 
		tp.ThirdPartyEntityID 
	 
	
	
SELECT	e.ThirdPartyEntityID, 
	tp.Name, 
	e.RepriceExportID, 
	i.BusinessUnitId , 
	v.Vin, 
	v.VehicleYear, 
	v.Make, 
	v.Model, 
	i.StockNumber, 
	i.InventoryType, 
	i.MileageReceived , 
	re.NewPrice, 
	e.RepriceExportStatusID, 
	e.FailureCount, 
	tp.ThirdPartyEntityTypeId  
FROM	RepriceExport e  WITH (NOLOCK)
JOIN	ThirdPartyEntity tp 
on	e.ThirdPartyEntityID = tp.ThirdPartyEntityID 
JOIN	RepriceEvent re  WITH (NOLOCK)
ON	e.RepriceEventId = re.RepriceEventId  
JOIN	#MaxLastRepriceInventory h2 
on	re.InventoryID = h2.InventoryID 
and	re.Created = h2.LastRepriceDate 
and	e.RepriceExportID = h2.maxId 
JOIN	Inventory i  WITH (NOLOCK)
on	re.inventoryId = i.inventoryId  
JOIN	Vehicle v  WITH (NOLOCK)
on	i.vehicleId = v.vehicleId  
WHERE	e.RepriceExportStatusID = 1 
	OR (e.RepriceExportStatusId = 4 AND e.FailureCount <= 5) 

DROP TABLE #MaxLastRepriceInventory