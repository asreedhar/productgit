SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetReportGrouping]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetReportGrouping]
GO


CREATE PROCEDURE dbo.usp_GetReportGrouping 
	@BusinessUnitID		INT,
	@MinimumDealDate	DATETIME,
	@MaximumDealDate	DATETIME,
	@GroupingDescriptionID	INT,
	@IncludeDealerGroup 	INT = 0,
	@InvType		INT,
	@Mileage		INT

/***************************************************************
**
**	usp_GetReportGrouping
**	Stored Procedure
**	
**	23APR04	pc	Created stored proc
**      28SEP06 wgh	Redirected to FLDW version
***************************************************************/

AS

SET NOCOUNT ON
DECLARE @err INT

EXEC @err = [FLDW]..GetReportGrouping @BusinessUnitID, @MinimumDealDate, @MaximumDealDate, @GroupingDescriptionID, @IncludeDealerGroup, @InvType, @Mileage

RETURN @err

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO
