
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'AutoCheck.HandshakeAccount#Exists') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE  AutoCheck.HandshakeAccount#Exists  AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on AutoCheck.HandshakeAccount#Exists to [AutoCheckUser]
GO

ALTER PROCEDURE [AutoCheck].[HandshakeAccount#Exists]
	-- no parameters
AS

/* --------------------------------------------------------------------
 * 
 * $Id: AutoCheck.HandshakeAccount#Exists.sql,v 1.4 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns 1 if the System Account Exists.
 * 
 * Parameters
 * ----------
 *
 * NO PARAMETERS
 * 
 * Exceptions
 * ----------
 * 
 * 50200 - Expected one Row
 *
 * History
 * -------
 *
 * SBW	06/07/2009	Create procedure.
 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

-- NO VALIDATION

------------------------------------------------------------------------------------------------
-- Declare Result Set
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE
	([Exists] BIT)

------------------------------------------------------------------------------------------------
-- Populate Result Set
------------------------------------------------------------------------------------------------

IF (EXISTS (
	SELECT	1
	FROM	AutoCheck.Account
	WHERE	AccountTypeID = 3 -- Handshake Account
	))
BEGIN
	INSERT
	INTO	@Results
		([Exists])
	VALUES	(1)
END
ELSE
BEGIN
	INSERT
	INTO	@Results
		([Exists])
	VALUES	(0)
END

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF (@err <> 0) GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

-- there must be one row in the table

IF (@rc <> 1)
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM  @Results

RETURN 0
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
