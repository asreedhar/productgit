IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'AutoCheck.ValidateParameter_AccountID') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE AutoCheck.ValidateParameter_AccountID AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on AutoCheck.ValidateParameter_AccountID to [AutoCheckUser]
GO

ALTER PROCEDURE [AutoCheck].[ValidateParameter_AccountID]
	@AccountID INT
AS
/* --------------------------------------------------------------------
 * 
 * $Id: AutoCheck.ValidateParameter_AccountID.sql,v 1.4 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 *	Verifies that the AccountID
 *	1. Is Not Null
 *	2. Exists in AutoCheck. Account
 *
 *
 *
 * Parameters
 * ----------
 *
 * @AccountID  - PK from AutoCheck.Account
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @AccountID is null
 * 50106 - @AccountID exists
 *
 * History
 * -------
 * MAK	05/26/2009	Create procedure.

 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

IF (@AccountID IS NULL)
BEGIN
	RAISERROR (50100,16,1,'AccountID')
	RETURN @@ERROR
END


IF (NOT EXISTS (
	SELECT	1
	FROM	AutoCheck.Account A
	WHERE	A.AccountID =@AccountID
))
	BEGIN
		RAISERROR (50106,16,1,'AccountID',@AccountID)
		RETURN @@ERROR
	END



------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err
GO




