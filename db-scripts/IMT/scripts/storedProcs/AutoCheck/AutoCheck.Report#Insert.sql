IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'AutoCheck.Report#Insert') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE AutoCheck.Report#Insert AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on AutoCheck.Report#Insert to [AutoCheckUser]
GO

ALTER PROCEDURE [AutoCheck].[Report#Insert]
	(@Id	INT,
	@Document	XML,
	@Score INT,
	@CompareScoreRangeLow INT,
	@CompareScoreRangeHigh INT,
	@Assured BIT,
	@OwnerCount INT)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: AutoCheck.Report#Insert.sql,v 1.4 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 *	Insert the actual Report (XML plus extracted meta-data).
 *
 * Exceptions
 * ----------
 * 
 *	 50100 Id is NULL
 *	 50106 Request with Id does not exist
 *	 50116 Response for Request does not have a success code (500 or 403)
 *	 50100 Document is NULL
 *	 50100 Score is NULL
 *	 50100 CompareScoreRangeLow is NULL
 *	 50100 CompareScoreRangeHigh is NULL
 *	 50100 Assured is NULL
 *	 50100 OwnerCount is NULL
 *	 50200 Expected to insert one row (but inserted 0 or > 1)
 *
 *
 *
 *	Returns:
 *		No ResultSet returned.
 *
 * History
 * ----------
 * 
 * JRC	05/27/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT


IF (@Id IS NULL)
BEGIN
	RAISERROR (50100,16,1,'ReportID')
	RETURN @@ERROR
END

IF ( NOT EXISTS (SELECT 1
		FROM 	AutoCheck.Request
		WHERE 	RequestID =@Id))
BEGIN
	RAISERROR (50106,16,1,'Id',@Id)
	RETURN @@ERROR
END
IF (NOT EXISTS (SELECT 1
		FROM 	AutoCheck.Response
		WHERE 	RequestID =@ID
		AND 	ResponseCode = 200))
BEGIN
	RAISERROR (50116,16,1,'Id',@Id)
	RETURN @@ERROR
END


IF (@Document IS NULL)
BEGIN
	RAISERROR (50100,16,1,'Document')
	RETURN @@ERROR
END

IF (@Score IS NULL)
BEGIN
	RAISERROR (50100,16,1,'Score')
	RETURN @@ERROR
END

IF (@CompareScoreRangeLow IS NULL)
BEGIN
	RAISERROR (50100,16,1,'CompareScoreRangeLow')
	RETURN @@ERROR
END

IF (@CompareScoreRangeHigh IS NULL)
BEGIN
	RAISERROR (50100,16,1,'CompareScoreRangeHigh')
	RETURN @@ERROR
END

IF (@Assured IS NULL)
BEGIN
	RAISERROR (50100,16,1,'Assured')
	RETURN @@ERROR
END

IF (@OwnerCount IS NULL)
BEGIN
	RAISERROR (50100,16,1,'OwnerCount')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT
INTO	AutoCheck.Report
	(RequestID,
	Report,
	Score,
	CompareScoreRangeLow,
	CompareScoreRangeHigh,
	Assured,
	OwnerCount)
			
VALUES	(@Id,
	@Document,
	@Score,
	@CompareScoreRangeLow,
	@CompareScoreRangeHigh,
	@Assured,
	@OwnerCount)

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF (@err <> 0) GOTO Failed

IF (@rc <>1)
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

UPDATE	V
SET	RequestID =@Id
FROM	AutoCheck.Vehicle V
JOIN	AutoCheck.Request R
ON	R.VehicleID =V.VehicleID
WHERE	R.RequestID =@Id

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF (@err <> 0) GOTO Failed

IF (@rc <>1)
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END
-----------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------
RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err
 
GO
