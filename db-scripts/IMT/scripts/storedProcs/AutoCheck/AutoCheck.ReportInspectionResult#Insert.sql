IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'AutoCheck.ReportInspectionResult#Insert') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE AutoCheck.ReportInspectionResult#Insert AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on AutoCheck.ReportInspectionResult#Insert to [AutoCheckUser]
GO

ALTER PROCEDURE [AutoCheck].[ReportInspectionResult#Insert]
	(@ReportId	INT,
	@ReportInspectionId	INT,
	@Selected	BIT)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: AutoCheck.ReportInspectionResult#Insert.sql,v 1.4 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 *	This procedure associates a report with its inspection results.  
 *	It has no result set and no output parameters.
 *
 * Exceptions
 * ----------
 * 
 *	 50100 ReportId is NULL
 *	 50106 Report with RequestId=ReportId does not exist
 *	 50100 ReportInspectionId is NULL
 *	 50106 Record with ReportInspectionId does not exist
 *	 50100 Selected is NULL
 *	 50200 Expected to insert one row (but inserted 0 or > 1)
 *
 *
 *
 *	Returns:
 *		No ResultSet returned.
 *
 * History
 * ----------
 * 
 * JRC	05/27/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT


IF (@ReportId IS NULL)
BEGIN
	RAISERROR (50100,16,1,'ReportID')
	RETURN @@ERROR
END

IF  (NOT EXISTS (SELECT 1
		FROM 	AutoCheck.Report
		WHERE 	RequestID =@ReportId))
BEGIN
	RAISERROR (50106,16,1,'ReportId',@ReportID)
	RETURN @@ERROR
END


IF (@ReportInspectionId IS NULL)
BEGIN
	RAISERROR (50100,16,1,'ReportInspectionId')
	RETURN @@ERROR
END

IF  (NOT EXISTS (SELECT 1
		FROM 	AutoCheck.ReportInspection
		WHERE 	ReportInspectionID =@ReportInspectionId))
BEGIN
	RAISERROR (50106,16,1,'ReportInspectionId',@ReportInspectionId)
	RETURN @@ERROR
END


IF (@Selected IS NULL)
BEGIN
	RAISERROR (50100,16,1,'Selected')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT
INTO	AutoCheck.ReportInspectionResult
	(RequestID,
	ReportInspectionID,
	Selected)
VALUES	(@ReportId,
	@ReportInspectionId,
	@Selected)

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF (@rc <>1)
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END


------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

Return 0
	
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
