
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'AutoCheck.ServiceAccount#Exists') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE  AutoCheck.ServiceAccount#Exists  AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on AutoCheck.ServiceAccount#Exists to [AutoCheckUser]
GO

ALTER PROCEDURE [AutoCheck].[ServiceAccount#Exists]
	@ServiceTypeId INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: AutoCheck.ServiceAccount#Exists.sql,v 1.4 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns 1 if the Service Account Exists.
 * 
 * Parameters
 * ----------
 *
 * @ServiceTypeId - The service type (NOT NULL)
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - ServiceTypeId IS NULL
 * 50106 - ServiceTypeId does not exist
 * 50200 - Expected one Row
 *
 * History
 * -------
 *
 * SBW	06/07/2009	Create procedure.
 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

IF @ServiceTypeId IS NULL BEGIN
	RAISERROR (50100,16,1,'ServiceTypeId')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM AutoCheck.ServiceType WHERE ServiceTypeID = @ServiceTypeId) BEGIN
	RAISERROR (50106,16,1,'ServiceTypeId')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Declare Result Set
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE
	([Exists] BIT)

------------------------------------------------------------------------------------------------
-- Populate Result Set
------------------------------------------------------------------------------------------------

IF EXISTS (
	SELECT	1
	FROM	AutoCheck.Account_ServiceType A
	WHERE	ServiceTypeID = @ServiceTypeID
	)
BEGIN
	INSERT
	INTO	@Results
		([Exists])
	VALUES	(1)
END
ELSE
BEGIN
	INSERT
	INTO	@Results
		([Exists])
	VALUES	(0)
END

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

-- there must be one row in the table

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM  @Results

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
