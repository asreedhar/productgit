
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'AutoCheck.SystemAccount#Fetch') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE AutoCheck.SystemAccount#Fetch  AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on AutoCheck.SystemAccount#Fetch to [AutoCheckUser]
GO

ALTER PROCEDURE [AutoCheck].[SystemAccount#Fetch]
	-- NO PARAMETERS
AS

/* --------------------------------------------------------------------
 * 
 * $Id: AutoCheck.SystemAccount#Fetch.sql,v 1.6 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns the system account information.  Members are not assigned to
 * this account.
 *
 * Parameters
 * ----------
 *
 * NO PARAMTERS
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @DealerId is null
 * 50106 - Record for DealerID does not exist
 * 50106 - Record for Dealer_Account does not exist
 * 50200 - Expected one Row
 *
 * History
 * ----------
 * 
 * SBW	06/07/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

DECLARE @AutoCheckAccount TABLE
	(Id		INT	NOT NULL,
	AccountType	INT	NOT NULL,
	AccountStatus	INT	NOT NULL,
	Active		BIT NOT NULL,
	UserName	VARCHAR(20)	NOT NULL,
	Password	VARCHAR(20)	NULL,
	Version		BINARY(8)	NOT NULL)

INSERT
INTO	@AutoCheckAccount
	(Id,
	AccountType,
	AccountStatus,
	Active,
	UserName,
	Password,
	Version)
SELECT	A.AccountID,
	A.AccountTypeID,
	A.AccountStatusID,
	A.Active,
	A.UserName,
	A.Password,
	A.Version
FROM	AutoCheck.Account A
WHERE	A.AccountTypeID = 2

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF (@err <> 0) GOTO Failed

IF (@rc <> 1)
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @AutoCheckAccount

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
