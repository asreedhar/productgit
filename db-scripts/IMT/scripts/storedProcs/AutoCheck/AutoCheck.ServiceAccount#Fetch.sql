
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'AutoCheck.ServiceAccount#Fetch') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE AutoCheck.ServiceAccount#Fetch  AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on AutoCheck.ServiceAccount#Fetch to [AutoCheckUser]
GO

ALTER PROCEDURE [AutoCheck].[ServiceAccount#Fetch]
	@ServiceTypeId INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: AutoCheck.ServiceAccount#Fetch.sql,v 1.6 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns the service account information.  Members are not assigned to
 * this account.
 *
 * Parameters
 * ----------
 *
 * @ServiceTypeId - The service type (NOT NULL)
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - ServiceTypeId IS NULL
 * 50106 - ServiceTypeId does not exist
 * 50200 - Expected one Row
 *
 * History
 * ----------
 * 
 * SBW	06/07/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

IF @ServiceTypeId IS NULL BEGIN
	RAISERROR (50100,16,1,'ServiceTypeId')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM AutoCheck.ServiceType WHERE ServiceTypeID = @ServiceTypeId) BEGIN
	RAISERROR (50106,16,1,'ServiceTypeId')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Declare Result Set
------------------------------------------------------------------------------------------------

DECLARE @AutoCheckAccount TABLE
	(ServiceTypeId  INT		NOT NULL,
	Id		INT		NOT NULL,
	AccountType	INT		NOT NULL,
	AccountStatus	INT		NOT NULL,
	Active		BIT NOT NULL,
	UserName	VARCHAR(20)	NOT NULL,
	Password	VARCHAR(20)	NULL,
	Version		BINARY(8)	NOT NULL)

INSERT
INTO	@AutoCheckAccount
	(ServiceTypeId,
	Id,
	AccountType,
	AccountStatus,
	Active,
	UserName,
	Password,
	Version)
SELECT	S.ServiceTypeID,
	A.AccountID,
	A.AccountTypeID,
	A.AccountStatusID,
	A.Active,
	A.UserName,
	A.Password,
	A.Version
FROM	AutoCheck.Account A
JOIN	AutoCheck.Account_ServiceType S ON S.AccountID = A.AccountID
WHERE	A.AccountTypeID = 3
AND	S.ServiceTypeID = @ServiceTypeID

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @AutoCheckAccount

RETURN 0
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
