
IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'AutoCheck.Account#HasPurchasedReport')
                        AND type IN ( N'P', N'PC' ) ) 
    EXECUTE
           ( 'CREATE PROCEDURE AutoCheck.Account#HasPurchasedReport  AS SELECT 1'
           )
GO

GRANT EXECUTE, VIEW DEFINITION ON AutoCheck.Account#HasPurchasedReport TO [AutoCheckUser]
GO

ALTER PROCEDURE [AutoCheck].[Account#HasPurchasedReport] @Id INT,
    @Vin VARCHAR(17)
AS /* --------------------------------------------------------------------
 * 
 * $Id: AutoCheck.Account#HasPurchasedReport.sql,v 1.4.4.1 2010/06/02 18:40:53 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Returns 1 Result Set with a single BIT column Exists.
 *
 * Parameters
 * ----------
 *
 * @Id   - Id that maps to a PK in the AutoCheck.Account table.
 * @Vin  - Vin is the UK of the vehicle for whom we are searching for a report
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @Id is null
 * 50106 - Record for Id does not exist
 * 50100 - @VIN is null
 * 50114 - LEN(Vin) <> 17
 * 50200 - Expected one Row
 *
 * History
 * ----------
 * 
 * JRC	05/26/2009	Create procedure.
 * JRC	05/29/2009	Updated Message code and included expiration date.
 * MAK	05/06/2010	Added pseudo expiration date - 90 days after insert.
 * 						
 * -------------------------------------------------------------------- */

    SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

    DECLARE @rc INT,@err INT

    EXEC @err = AutoCheck.ValidateParameter_AccountID @Id
    IF ( @err <> 0 )
        GOTO Failed

    EXEC @err = dbo.ValidateParameter_VIN @Vin
    IF ( @err <> 0 ) 
        GOTO Failed

------------------------------------------------------------------------------------------------
-- Declare constant
--	SS stands for 'Simon Says'
------------------------------------------------------------------------------------------------
    DECLARE @SSDaysAfterInsert TINYINT
    SET @SSDaysAfterInsert = 90

------------------------------------------------------------------------------------------------
-- Declare Result Set
------------------------------------------------------------------------------------------------

    DECLARE @Results TABLE ( [Exists] BIT )

------------------------------------------------------------------------------------------------
-- Populate Result Set
------------------------------------------------------------------------------------------------

    IF ( EXISTS ( SELECT    1
                  FROM      AutoCheck.Account A
                            JOIN AutoCheck.Request R ON R.AccountID = A.AccountID
                            JOIN AutoCheck.Vehicle V ON V.VehicleID = R.VehicleID
                            JOIN AutoCheck.Report E ON E.RequestID = R.RequestID
                  WHERE     V.VIN = @Vin
                            AND A.AccountID = @Id
                            AND DATEADD(d, @SSDaysAfterInsert, R.InsertDate) >= GETDATE() ) ) 
        INSERT  INTO @Results ( [Exists] )
        VALUES  ( 1 )
    ELSE 
        INSERT  INTO @Results ( [Exists] )
        VALUES  ( 0 )

    SELECT  @rc = @@ROWCOUNT,
            @err = @@ERROR
    IF ( @err <> 0 ) 
        GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

-- there must be one row in the tables

    IF ( @rc <> 1 ) 
        BEGIN
            RAISERROR ( 50200, 16, 1, @rc )
            RETURN @@ERROR
        END


------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

    SELECT  *
    FROM    @Results

    RETURN 0
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

    Failed:

    RETURN @err

GO