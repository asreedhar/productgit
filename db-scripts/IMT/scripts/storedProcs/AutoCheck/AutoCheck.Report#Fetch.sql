
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'AutoCheck.Report#Fetch') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE AutoCheck.Report#Fetch AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on AutoCheck.Report#Fetch to [AutoCheckUser]
GO

ALTER PROCEDURE [AutoCheck].[Report#Fetch] 
	@AccountId INT,
	@Vin CHAR(17)

AS

/* --------------------------------------------------------------------
 * 
 * $Id: AutoCheck.Report#Fetch.sql,v 1.6 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Request the most recent report from the supplied account for the
 * given VIN.
 * 
 * Parameters
 * -------
 * 
 * AccountId INT
 * VIN CHAR(17)
 * 
 * Exceptions
 * ----------
 * 
 * 50100 AccountId is NULL
 * 50106 Record with AccountId does not exist
 * 50100 VIN is NULL
 * 50114 LEN(VIN) != 17
 * 50200 Expected to insert one row (but inserted 0 or > 1)
 *
 * Result Set
 * ----------
 * 
 * 1)	Id INT NOT NULL
 *	AccountId INT NOT NULL
 *	Vin CHAR(17) NOT NULL
 *	ReportType TINYINT NOT NULL
 *	Document XML NOT NULL
 *	Score INT NOT NULL
 *	CompareScoreRangeLow INT NOT NULL
 *	CompareScoreRangeHigh INT NOT NULL
 *	Assured BIT NOT NULL
 *	OwnerCount INT NOT NULL
 *
 * 2)	ReportInspectionId INT NOT NULL
 *	Selected INT NOT NULL
 *
 * History
 * ----------
 * 
 * JRC	05/27/2009	Create procedure.
 * JRC	05/29/2009	Update Error Code.
 * MAK	08/31/2009	Update code so that requests only return for correct VIN\Account combination
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

EXEC  @err =AutoCheck.ValidateParameter_AccountID @AccountId
IF (@err <> 0) GOTO Failed

EXEC  @err =dbo.ValidateParameter_VIN @VIN 
IF (@err <> 0) GOTO Failed


DECLARE @VehicleID INT

SELECT	@VehicleID =VehicleID
FROM	AutoCheck.Vehicle
WHERE	VIN=@Vin

------------------------------------------------------------------------------------------------
-- Result Set
------------------------------------------------------------------------------------------------

DECLARE @Report TABLE (
	Id INT NOT NULL,
	AccountId INT NOT NULL,
	Vin CHAR(17) NOT NULL,
	ReportTypeID INT NOT NULL,
	Document XML NOT NULL,
	Score INT NULL,
	CompareScoreRangeLow INT NULL,
	CompareScoreRangeHigh INT NULL,
	Assured BIT NULL,
	OwnerCount INT NOT NULL
)

DECLARE @ReportInspection TABLE (
	ReportInspectionId INT NOT NULL,
	Selected BIT NOT NULL
)

------------------------------------------------------------------------------------------------
-- Populate It
------------------------------------------------------------------------------------------------

INSERT INTO @Report (
	Id,
	AccountId,
	Vin,
	ReportTypeID,
	Document,
	Score,
	CompareScoreRangeLow,
	CompareScoreRangeHigh,
	Assured,
	OwnerCount
)
SELECT	Id = E.RequestID,
	AccountId = R.AccountId,
	Vin = V.VIN,
	ReportTypeID = R.ReportTypeID,
	Document = E.Report,
	Score = E.Score,
	CompareScoreRangeLow  = E.CompareScoreRangeLow,
	CompareScoreRangeHigh  = E.CompareScoreRangeHigh,
	Assured  = E.Assured,
	OwnerCount = E.OwnerCount

FROM	AutoCheck.Vehicle V
JOIN	AutoCheck.Request R ON R.VehicleID = V.VehicleID
JOIN	AutoCheck.Report E ON E.RequestID = R.RequestID
JOIN	(
		SELECT	RR.VehicleID,
			MAX(RE.RequestID) RequestID
		FROM	AutoCheck.Request RR
		JOIN	AutoCheck.Report RE ON RE.RequestID = RR.RequestID
		WHERE	RR.AccountID =@AccountID
		AND		RR.VehicleID =@VehicleID
		GROUP
		BY	RR.VehicleID
	) M ON M.VehicleID = V.VehicleID AND E.RequestID = M.RequestID
WHERE	R.AccountId = @AccountId
AND	V.VehicleID =@VehicleID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF (@err <> 0) GOTO Failed

IF (@rc <> 1)
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

INSERT INTO @ReportInspection (
	ReportInspectionId,
	Selected
)
SELECT	ReportInspectionId,
	Selected
FROM	AutoCheck.ReportInspectionResult R
JOIN	@Report T ON T.Id = R.RequestId


------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Report

SELECT * FROM @ReportInspection

Return 0
	
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
