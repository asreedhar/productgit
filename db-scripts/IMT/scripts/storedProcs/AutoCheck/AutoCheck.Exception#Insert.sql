IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID('AutoCheck.Exception#Insert') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE AutoCheck.Exception#Insert AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on AutoCheck.Exception#Insert to [CarfaxUser]
GO

ALTER PROCEDURE [AutoCheck].[Exception#Insert]
	 @MachineName VARCHAR(256),
	@Time DATETIME,
	@Type VARCHAR(256),
	@Message VARCHAR(1024),
	@Detail TEXT,
	@ReportID INT

AS

/* --------------------------------------------------------------------
 * 
 * 
 * Summary
 * -------
 * 
 *	Record an exception.
 *
 * Exceptions
 * ----------
 * 
 *
 *	Returns:
 *		No ResultSet returned.
 *
 * History
 * ----------
 * 
 * JRC	05/27/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

BEGIN TRY
 
	INSERT INTO AutoCheck.Exception
	(	ExceptionTime,
		MachineName,
		ExceptionType,
		Details,
		Message,
		RequestID
	)
	VALUES 	
	(	@Time,
		@MachineName,
		@Type,
		@Detail,
		@Message,
		@ReportID)
END TRY

BEGIN CATCH
	EXEC sp_ErrorHandler	
			
END CATCH
GO