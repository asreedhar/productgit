IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'AutoCheck.Report#Create') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE AutoCheck.Report#Create AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on AutoCheck.Report#Create to [AutoCheckUser]
GO

ALTER PROCEDURE [AutoCheck].[Report#Create] 
	@AccountId INT,
	@VIN CHAR(17),
	@ReportTypeID TINYINT,
	@InsertUser VARCHAR(80),
	@Id INT OUTPUT

AS
/* --------------------------------------------------------------------
 * 
 * $Id: AutoCheck.Report#Create.sql,v 1.4 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 *	Insert a row into the table AutoCheck.Request returning the RequestID (PK of report).
 *
 *	Parameters:
 *		AccountId INT
 *		VIN CHAR(17)
 *		ReportType CHAR(3),
 *		InsertUser VARCHAR(80)
 *		Id INT OUTPUT
 *
 *	Exceptions:
 *		50100 AccountId is NULL
 *		50106 Record with AccountId does not exist
 *		50100 VIN is NULL
 *		50114 LEN(VIN) != 17
 *		50100 ReportType is NULL
 *		50106 Record with ReportType does not exist
 *		50100 InsertUser is NULL
 *		50106 Record with InsertUser does not exist
 *		50200 Expected to insert one row (but inserted 0 or > 1)
 *
 *	Result Set
 *	NONE (Just OUTPUT parameter).
 *
 *	Returns:
 *		None
 *
 *
 * History
 * ----------
 * 
 * JRC	05/27/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

EXEC  @err =AutoCheck.ValidateParameter_AccountID @AccountId
	 
IF (@err = 0) EXEC  @err =dbo.ValidateParameter_VIN @Vin
	 
IF (@err = 0) EXEC  @err =AutoCheck.ValidateParameter_ReportType @ReportTypeID
	 
DECLARE @InsertUserID INT

IF (@err = 0) EXEC  @err =dbo.ValidateParameter_MemberID#UserName  @InsertUser, @InsertUserID OUTPUT


If (@err = 0)
BEGIN TRY
	DECLARE @VehicleID INT

	SELECT	@VehicleID =VehicleID
	FROM	AutoCheck.Vehicle
	WHERE	VIN=@VIN

	BEGIN TRANSACTION
		IF	(@VehicleID is null)
			BEGIN
			INSERT
			INTO	AutoCheck.Vehicle
				(VIN,
				InsertUser,
				InsertDate)
			VALUES
				(@VIN,
				@InsertUserID,
				GETDATE())

			SET @VehicleID =SCOPE_IDENTITY()
			END

			DECLARE @AccountTypeID TINYINT, @DealerID INT

			SELECT	@AccountTypeID = AccountTypeID
			FROM	AutoCheck.Account
			WHERE	AccountID=@AccountID

			IF (@AccountTypeID = 1) BEGIN

				SELECT  @DealerID =DealerID
				FROM	AutoCheck.Account_Dealer
				WHERE	AccountID=@AccountID

			IF (@DealerID is null)
			BEGIN
				RAISERROR (50106,16,1,'DealerID')
				RETURN @@ERROR
			END

		END
		
	------------------------------------------------------------------------------------------------
	-- Perform INSERT
	------------------------------------------------------------------------------------------------

		INSERT
		INTO	AutoCheck.Request
			(VehicleID,
			AccountID,
			ReportTypeID,
			InsertUser,
			InsertDate)
		Values  (@VehicleID,
			@AccountID,
			@ReportTypeID,
			@InsertUserID,
			GETDATE())
		
		SET @Id =SCOPE_IDENTITY()

		IF (@AccountTypeID = 1) BEGIN
			IF NOT EXISTS (
				SELECT 1
				FROM	AutoCheck.Vehicle_Dealer
				WHERE	VehicleID=@VehicleID
				AND	DealerID =@DealerID)

				INSERT
				INTO	AutoCheck.Vehicle_Dealer
					(VehicleID,
					DealerID,
					RequestID)
				Values  (@VehicleID,
					@DealerID,
					@Id)
			ELSE
				UPDATE  AutoCheck.Vehicle_Dealer
				SET 	RequestID =@Id
				WHERE	DealerID =@DealerID
				AND	VehicleID =@VehicleID


		END
	COMMIT TRANSACTION

	RETURN 0		
END TRY
	
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

BEGIN CATCH
	
	DECLARE @ErrNum INT
	DECLARE @ErrorMessage VARCHAR(2000)
	SET @ErrorMessage =ERROR_MESSAGE()  
	SET @ErrNum =@@ERROR 

	IF (XACT_STATE() =1 OR XACT_STATE()=-1)
	 ROLLBACK TRANSACTION
	
	RAISERROR(@ErrorMessage, 16, 1)
	RETURN @ErrNum

END CATCH

GO
