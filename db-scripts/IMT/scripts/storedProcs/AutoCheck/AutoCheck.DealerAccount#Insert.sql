IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'AutoCheck.DealerAccount#Insert') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE AutoCheck.DealerAccount#Insert  AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on AutoCheck.DealerAccount#Insert to [AutoCheckUser]
GO

ALTER PROCEDURE [AutoCheck].[DealerAccount#Insert]
	@DealerID	INT,
	@AccountID	INT,
	@InsertUser	VARCHAR(80)
AS
/* --------------------------------------------------------------------
 * 
 * $Id: AutoCheck.DealerAccount#Insert.sql,v 1.4 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 *	Insert a Value into the AutoCheck.Dealer_Account table.
 *
 *
 *	Parameters:
 *	@DealerID  
 *	@AccountID  
 *	@InsertUser   
 *
 *	Validation
 *
 *	50100 DealerId IS NULL
 *	50106 Record with DealerId does not exist
 *	50100 AccountId IS NULL
 *	50106 Record with AccountId does not exist
 *	50112 Record with AccountId assigned to another dealer already
 *	50100 InsertUser IS NULL
 *
 * History
 * ----------
 * 
 * JRC	05/26/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

EXEC  @err =dbo.ValidateParameter_DealerID @DealerID
IF (@err <> 0) GOTO Failed

EXEC  @err =AutoCheck.ValidateParameter_AccountID @AccountID
IF (@err <> 0) GOTO Failed


DECLARE @InsertUserID INT

EXEC  @err =dbo.ValidateParameter_MemberID#UserName  @InsertUser, @InsertUserID OUTPUT
IF (@err <> 0) GOTO Failed


IF (EXISTS (SELECT	1 
	FROM	AutoCheck.Account_Dealer
	WHERE	DealerID =@DealerId))
BEGIN
	RAISERROR (50112,16,1,'DealerId')
	RETURN @@ERROR
END	

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------


INSERT
INTO	AutoCheck.Account_Dealer
	(AccountID,
	DealerID,
	InsertUser,
	InsertDate)
VALUES	(@AccountID,
	@DealerID,
	@InsertUserID,
	GETDATE())

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF (@rc <> 1)
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

Return 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err
 
GO