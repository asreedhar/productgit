IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'AutoCheck.Account#Fetch') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE AutoCheck.Account#Fetch  AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on AutoCheck.Account#Fetch to [AutoCheckUser]
GO

ALTER PROCEDURE [AutoCheck].[Account#Fetch]
	@Id	INT
AS
/* --------------------------------------------------------------------
 * 
 * $Id: AutoCheck.Account#Fetch.sql,v 1.6 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns 2 Result Sets:
 *	RS #1  Account Information:	DealerID 
 *								AccountType
 *								AccountStatus
 *								Active
 *								UserName
 *								Password
 *								Version
 * 
 *	RS #2 Account Assignment:	MemberID
 *								Assigned
 *
 *
 * Parameters
 * ----------
 *
 * @Id   - Id that maps to a PK in the AutoCheck.Account table.
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @Id is null
 * 50106 - Record for Id does not exist
 * 50200 - Expected one Row
 *
 * History
 * ----------
 * 
 * JRC	05/26/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

EXEC @err = AutoCheck.ValidateParameter_AccountID @Id

IF (@err <> 0) GOTO Failed

DECLARE @AutoCheckAccount TABLE
	(Id		INT	NOT NULL,
	AccountType	INT	NOT NULL,
	AccountStatus	INT	NOT NULL,
	Active		BIT NOT NULL,
	UserName	VARCHAR(20) NOT NULL,
	Password	VARCHAR(20) NULL,
	Version		BINARY(8) NOT NULL)

DECLARE @AutoCheckAccountAssignment TABLE
	(MemberID	INT	NOT NULL,
	Assigned	DATETIME NOT NULL)

INSERT
INTO	@AutoCheckAccount
	(Id,
	AccountType,
	AccountStatus,
	Active,
	UserName,
	Password,
	Version)
SELECT	A.AccountID,
	A.AccountTypeID,
	A.AccountStatusID,
	A.Active,
	A.UserName,
	A.Password,
	A.Version
FROM	AutoCheck.Account A
WHERE	A.AccountID =@Id

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF (@rc <>1)
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END


INSERT
INTO	@AutoCheckAccountAssignment
	(MemberID,
	Assigned)
SELECT	AM.MemberID,
	AM.InsertDate as Assigned
FROM	AutoCheck.Account A
JOIN	AutoCheck.Account_Member AM
ON	A.AccountID =AM.AccountID
WHERE	A.AccountID =@Id


------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @AutoCheckAccount

SELECT * FROM @AutoCheckAccountAssignment

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
