
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'AutoCheck.ServiceAccount#Insert') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE AutoCheck.ServiceAccount#Insert  AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on AutoCheck.ServiceAccount#Insert to [AutoCheckUser]
GO

ALTER PROCEDURE [AutoCheck].[ServiceAccount#Insert]
	@ServiceTypeID	INT,
	@AccountID	INT,
	@InsertUser	VARCHAR(80)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: AutoCheck.ServiceAccount#Insert.sql,v 1.4 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Insert a record into the AutoCheck.Account_ServiceType table.
 *
 *
 *	Parameters:
 *	@ServiceTypeID
 *	@AccountID
 *	@InsertUser
 *
 *	Validation
 *
 *	50100 ServiceTypeId IS NULL
 *	50106 Record with ServiceTypeId does not exist
 *	50100 AccountId IS NULL
 *	50106 Record with AccountId does not exist
 *	50112 Record with AccountId assigned to another dealer already
 *	50100 InsertUser IS NULL
 *
 * History
 * ----------
 * 
 * JRC	05/26/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

IF @ServiceTypeId IS NULL BEGIN
	RAISERROR (50100,16,1,'ServiceTypeId')
	RETURN @@ERROR
END

IF NOT EXISTS (SELECT 1 FROM AutoCheck.ServiceType WHERE ServiceTypeID = @ServiceTypeId) BEGIN
	RAISERROR (50106,16,1,'ServiceTypeId')
	RETURN @@ERROR
END

EXEC  @err =AutoCheck.ValidateParameter_AccountID @AccountID
IF @err <> 0 GOTO Failed

DECLARE @InsertUserID INT

EXEC  @err =dbo.ValidateParameter_MemberID#UserName  @InsertUser, @InsertUserID OUTPUT
IF @err <> 0 GOTO Failed

IF EXISTS (SELECT	1 
	FROM	AutoCheck.Account_ServiceType
	WHERE	ServiceTypeID =@ServiceTypeID)
BEGIN
	RAISERROR (50112,16,1,'ServiceTypeID')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Declare Result Set
------------------------------------------------------------------------------------------------

-- NO RESULT SET

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT
INTO	AutoCheck.Account_ServiceType
	(AccountID,
	ServiceTypeID,
	InsertUser,
	InsertDate)
VALUES	(@AccountID,
	@ServiceTypeID,
	@InsertUserID,
	GETDATE())

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

Return 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err
 
GO
