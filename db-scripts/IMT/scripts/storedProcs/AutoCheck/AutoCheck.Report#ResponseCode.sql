IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'AutoCheck.Report#ResponseCode') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE AutoCheck.Report#ResponseCode AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on AutoCheck.Report#ResponseCode to [AutoCheckUser]
GO

ALTER PROCEDURE [AutoCheck].[Report#ResponseCode] 
	(@ReportId INT,
	@ResponseCode INT)
	
AS

/* --------------------------------------------------------------------
 * 
 * $Id: AutoCheck.Report#ResponseCode.sql,v 1.4 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 *	Insert into the AutoCheck.Response table.
 *
 * Exceptions
 * ----------
 * 
 *	50100 ReportId is NULL
 *	50106 Request with Id=ReportId does not exist
 *	50100 ResponseCode is NULL
 *	50106 Record with ResponseCode does not exist
 *	50200 Expected to insert one row (but inserted 0 or > 1)
 *
 *
 *
 *	Returns:
 *		No ResultSet returned.
 *
 * History
 * ----------
 * 
 * MAK	05/27/2009	Create procedure.
 * MAK	06/09/2009	Added Update of AccountStatus for erroneous codes.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

BEGIN TRY
	IF (@ReportId IS NULL)
	BEGIN
		RAISERROR (50100,16,1,'ReportId')
		RETURN @@ERROR
	END

	IF  (NOT EXISTS (SELECT 1
			FROM 	AutoCheck.Request
			WHERE 	RequestID =@ReportId))
	BEGIN
		RAISERROR (50106,16,1,'ID',@ReportID)
		RETURN @@ERROR
	END


	IF   (NOT EXISTS (SELECT 1
			FROM 	AutoCheck.ResponseCode
			WHERE 	ResponseCode =@ResponseCode))
	BEGIN
		RAISERROR (50106,16,1,'ResponseCode',@ResponseCode)
		RETURN @@ERROR
	END

 -----------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

	 BEGIN TRANSACTION

	INSERT
	INTO	AutoCheck.Response
		(RequestID,
		ResponseCode,
		ReceivedDate)
	VALUES	(@ReportId,
		@ResponseCode,
		GETDATE())

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR

	IF (@rc <>1)
	BEGIN
		RAISERROR (50200,16,1,'AutoCheck.Response')
		RETURN @@ERROR
	END

	IF (@ResponseCode =403)
	BEGIN
		DECLARE @AutoCheckAdmin INT
	
		SELECT @AutoCheckAdmin =MemberID
		FROM	dbo.Member 
		WHERE	Login  ='AutoCheckSystemUserAdmin'

		IF (@AutoCheckAdmin is null)
		BEGIN
			RAISERROR (50106,16,1,'AutoCheckAdmin')
			RETURN @@ERROR
		END

		UPDATE A
		SET	AccountStatusID =
			CASE WHEN @ResponseCode =403 THEN 3 END,
			UpdateUser = @AutoCheckAdmin,
			UpdateDate =GETDATE()
		FROM	AutoCheck.Account A
		JOIN	AutoCheck.Request  RQ
		ON	A.AccountID =RQ.AccountID
		JOIN	AutoCheck.Report RP
		ON	RP.RequestID =RQ.RequestID
		WHERE	RP.RequestID =@ReportID
		SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	
		IF (@rc <>1)
		BEGIN
		RAISERROR (50200,16,1,'AutoCheck.Account')
		RETURN @@ERROR
		END

	END


------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

COMMIT TRANSACTION 
RETURN 0

END TRY	
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------
BEGIN CATCH
	
	DECLARE @ErrNum INT
	DECLARE @ErrorMessage VARCHAR(2000)
	SET @ErrorMessage =ERROR_MESSAGE()  
	SET @ErrNum =@@ERROR 

	IF (XACT_STATE() =1 OR XACT_STATE()=-1)
	 ROLLBACK TRANSACTION
	
	RAISERROR(@ErrorMessage, 16, 1)
	RETURN @ErrNum

END CATCH

GO