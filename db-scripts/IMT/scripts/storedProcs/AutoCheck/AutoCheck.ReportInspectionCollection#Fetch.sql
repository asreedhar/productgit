IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'AutoCheck.ReportInspectionCollection#Fetch') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE AutoCheck.ReportInspectionCollection#Fetch AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on AutoCheck.ReportInspectionCollection#Fetch to [AutoCheckUser]
GO

ALTER PROCEDURE AutoCheck.ReportInspectionCollection#Fetch
	
AS

/* --------------------------------------------------------------------
 * 
 * $Id: AutoCheck.ReportInspectionCollection#Fetch.sql,v 1.4 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 *	Returns all available ReportInspections to be used in the middle tier.
 *
 * Exceptions
 * ----------
 * 
 *	 None
 *
 *
 *	Returns:
 *		ID		INT NOT NULL
 *		Name		VARCHAR(50) NOT NULL
 *		XPath		VARCHAR(500) NOT NULL
 *
 * History
 * ----------
 * 
 * JRC	05/27/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Result Set table
------------------------------------------------------------------------------------------------
 DECLARE @ReportInspection TABLE
	(Id	INT		NOT NULL,
	Name	VARCHAR(100)	NOT NULL,
	xPath	VARCHAR(500)	NOT NULL)

INSERT
INTO	@ReportInspection
	(Id,
	Name,
	xPath)
SELECT  ReportInspectionID,
	Description,
	xPath
FROM	AutoCheck.ReportInspection


------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @ReportInspection
	
RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO