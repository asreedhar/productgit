SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[InsertRepriceExport]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[InsertRepriceExport]
GO

CREATE PROCEDURE [dbo].[InsertRepriceExport]
--------------------------------------------------------------------------------
--
--	Creates a reprice event for when the list price is changed on estock card
--	and IMP.  This is used in dbo.InsertRepriceEvent.PRC and also invoked
--  	through JDBC in the application.
--
---Parmeters--------------------------------------------------------------------
@InventoryID		INT,
@RepriceEventID		INT,
@Value 			INT
---History----------------------------------------------------------------------
--	
--	BF	Feb 13, 2007	Initial design/development
--	NK/DW 12/10/ 07		Deleted Reprice History, it has been replaced 
--				with RepriceEvent view on top of AIP_Event
--	ffoiii	2009.03.17	rewrote deletes using join syntax instead of nested subqueries
--				changed error handling and transaction logic
--------------------------------------------------------------------------------
AS

SET NOCOUNT ON
DECLARE @Rowcount INT
DECLARE	@Err INT
DECLARE	@sErrMsg VARCHAR(255)
DECLARE	@bLocalTran bit
DECLARE	@sProcname sysname
DECLARE @bTrue BIT
DECLARE @bFalse BIT
SELECT	@sProcName = object_name(@@procid),
	@bTrue = 1,
	@bFalse = 0,
	@sErrMsg = ''
SET	@bLocalTran = @bFalse
--END DEFAULT ENVIRONMENT AND VARIABLE SETTINGS

IF (@@TRANCOUNT = 0)
	BEGIN
		BEGIN TRANSACTION @sProcName
		SET @bLocalTran = @bTrue
	END

-- get the business unit id
DECLARE @BusinessUnitID INT
SELECT @BusinessUnitID = businessUnitID
FROM dbo.Inventory
WHERE inventoryId=@InventoryId

--- create reprice export entry for Internet Advertisers
--- IF	an internet ad has been built for this Inventory
---		AND the advertiser selected is active with IncrementalExport ON
---		AND the export rule is NOT set by the user
IF(EXISTS(SELECT 1
	FROM dbo.InternetAdvertiserDealerShip pref
		INNER JOIN dbo.InternetAdvertiserBuildList bl on pref.InternetAdvertiserId = bl.InternetAdvertiserId
	WHERE pref.businessUnitId = @businessUnitId
		AND pref.IncrementalExport = 1
		AND pref.Active = 1
		AND bl.InventoryID = @inventoryID
		AND bl.ExportStatusCD != 10 --Do NOT export, set by User
		AND bl.ExportStatusCD != 11 --Export, set by User
	))
	BEGIN
		-- Delete older PENDING exports. We only need to send the latest reprice event
		DELETE dbo.RepriceExport
		FROM dbo.RepriceExport re
			INNER JOIN dbo.RepriceExportStatus res ON res.RepriceExportStatusId = re.RepriceExportStatusId
			INNER JOIN dbo.InternetAdvertiserDealership d ON d.InternetAdvertiserId = re.ThirdPartyEntityId
			INNER JOIN dbo.RepriceEvent rev ON rev.RepriceEventId = re.RepriceEventId
		WHERE rev.InventoryId = @InventoryId
			AND res.Description IN ('InQueue', 'Failed')
			AND re.FailureCount < 6
			AND d.BusinessUnitId = @BusinessUnitId
			AND d.IncrementalExport = 1
			AND d.Active = 1
			
		-- Create the new event
		INSERT INTO [dbo].[RepriceExport]
			   ([RepriceEventID]
			   ,[RepriceExportStatusID]
			   ,[ThirdPartyEntityID]
			   ,[DateCreated])
		SELECT @repriceEventID
     			,rStatus.RepriceExportStatusID
     			,pref.InternetAdvertiserID
     			,GetDate()
		FROM dbo.RepriceExportStatus rStatus 
			CROSS JOIN dbo.InternetAdvertiserDealerShip pref
		WHERE	rStatus.description = 'InQueue'	
			AND pref.businessUnitId = @businessUnitId
			AND pref.IncrementalExport = 1
			AND pref.Active = 1
		
		IF (@@ERROR <> 0)
			BEGIN
				-- Raise an error and return
				SELECT @sErrMsg = 'Error inserting into RepriceExport'
				GOTO Failed
			END
	END

--- create reprice export entry for DMS Exports
--- IF	the business Unit is set to active for DMS Export (DMSExportBusinessUnitPreference.isActive = 1)
---		AND the export rule is NOT set by the user
IF(EXISTS(SELECT 1
	FROM dbo.DMSExportBusinessUnitPreference pref
	WHERE pref.businessUnitId = @businessUnitId
		AND pref.isActive = 1
	))
	BEGIN
		-- Delete older PENDING exports. We only need to send the latest reprice 
		DELETE RepriceExport
		FROM dbo.RepriceExport re
			INNER JOIN dbo.RepriceExportStatus res ON res.RepriceExportStatusId = re.RepriceExportStatusId
			INNER JOIN dbo.DMSExportBusinessUnitPreference pref ON pref.DMSExportId = re.ThirdPartyEntityId
			INNER JOIN dbo.RepriceEvent rev ON rev.RepriceEventId = re.RepriceEventId
		WHERE res.Description IN ('InQueue', 'Failed')
			AND pref.IsActive = 1
			AND pref.BusinessUnitId = @BusinessUnitId
			AND rev.InventoryId = @InventoryId
			AND FailureCount < 6
		
		-- Create the new event
		INSERT INTO [dbo].[RepriceExport]
			   ([RepriceEventID]
			   ,[RepriceExportStatusID]
			   ,[ThirdPartyEntityID]
			   ,[DateCreated])
		SELECT
    		@repriceEventID
     		, rStatus.RepriceExportStatusID
     		, pref.DMSExportID
     		, GetDate()
		FROM dbo.RepriceExportStatus rStatus CROSS JOIN
			dbo.DMSExportBusinessUnitPreference pref
		WHERE rStatus.description = 'InQueue'	
			AND pref.businessUnitId = @businessUnitId
			AND pref.isActive = 1
		
		IF (@@ERROR <> 0)
			BEGIN
				select @sErrMsg = 'Error inserting into RepriceExport'
				GOTO Failed
			END
	END


------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------
--all commands successful
IF(@bLocalTran = @bTrue)
	BEGIN
		COMMIT TRAN @sProcName
	END

--exit the procedure
RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------
--Error encountered
Failed:
IF (@bLocalTran = @bTrue)
	BEGIN
		ROLLBACK TRAN @sProcName
	END
IF (@sErrMsg = '')
	BEGIN
		SET @sErrMsg = 'Unspecified Error.'
	END
EXEC sp_LogEvent @Log_Code = 'E', @sp_Name = @sProcName, @Log_Text = @sErrMsg, @Originating_Log_Id = NULL
RAISERROR(@sErrmsg, 16, 1)
IF (@Err IS NULL)
	SET @Err = -2
RETURN @Err

GO
