USE [IMT]
GO

/****** Object:  StoredProcedure [dbo].[GetPricingListReport]    Script Date: 01/24/2014 19:36:35 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetPricingListReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetPricingListReport]
GO

USE [IMT]
GO

/****** Object:  StoredProcedure [dbo].[GetPricingListReport]    Script Date: 01/24/2014 19:36:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[GetPricingListReport]
----------------------------------------------------------------------------------------------------
--
--	Pricing List Report refactoring -- moved from application to DB layer
--	
---Parmeters----------------------------------------------------------------------------------------
-- 
@BusinessUnitID 	int,
@ShowRepricing		bit = 1,
@RetailOnly		bit = 0
--
---History------------------------------------------------------------------------------------------
--	
--	WGH	06/27/2005	Initial design/development
--		01/10/2006	Added "Retail Only" flag
--		01/17/2006	Fixed filter bug
--		02/20/2006	Added 0.0 default for Price field
--  	WGH/DW	10/05/2006	Updated the Retail only version of this proc to go against the new 
--				AIP schema
--	WGH	12/12/2006	Update code to retrieve the latest repricing event to account for
--				multiple events per day.
--		01/12/2007	Bug: Removed category filter on subquery--prevented HasNoEvent23 
--				from working correctly
--		01/18/2007	Changed objective check logic to look at the most recent objective
--
--	BYF	04/09/2008	Added NOLOCK Query hints to prevent longo toyota from timing out.
--	MAK	10/20/2008	Changed sproc so that we only query IMT.dbo.Inventory once and
--				eliminate the use of the view IMT.dbo.AIPUserEvent
----------------------------------------------------------------------------------------------------
AS

SET NOCOUNT ON 

DECLARE @BaseDate DATETIME
SET 	@BaseDate = dbo.ToDate(GETDATE())

CREATE TABLE #Inventory
	(InventoryID int,
	VehicleID	int,
	StockNumber varchar(15),
	LotPrice	int,
	ListPrice decimal(8,2),
	inventoryReceivedDate datetime,
	MileageReceived int,
	InventoryType tinyint,
	InventoryActive tinyint,
	PRIMARY KEY CLUSTERED (InventoryID ASC ))

INSERT
INTO	#Inventory
	(InventoryID,
	VehicleID,
	StockNumber,
	LotPrice,
	ListPrice ,
	inventoryReceivedDate,
	MileageReceived,
	InventoryType,
	InventoryActive)
Select  InventoryID,
	VehicleID,
	StockNumber,
	LotPrice,
	ListPrice ,
	inventoryReceivedDate,
	MileageReceived,
	InventoryType,
	InventoryActive
FROM	dbo.Inventory  I
WHERE	BusinessUnitID = @BusinessUnitID
AND	I.InventoryActive = 1
	AND I.InventoryType = 2
 
CREATE 
TABLE	#AIP_EventType5
	(AIP_EventID int,
	InventoryID int,
	Value decimal(9,2),
	AIP_EventTypeID tinyint,
	PRIMARY KEY CLUSTERED (AIP_EventID ASC ))

INSERT
INTO	#AIP_EventType5
	(AIP_EventID ,
	InventoryID,
	Value,
	AIP_EventTypeID)
SELECT  E.AIP_EventID,
	E.InventoryID,
	E.Value,
	E.AIP_EventTypeID
FROM	dbo.AIP_Event E
JOIN	#Inventory I
ON	E.InventoryID =I.InventoryID
WHERE	E.AIP_EventTypeID =5

CREATE 
TABLE	#AIP_UserEventType
	(AIP_EventID int,
	InventoryID int,
	AIP_EventTypeID tinyint,
	AIP_EventCategoryID int,
	Value decimal(9,2),
	EndDate datetime,
	Primary Key CLUSTERED (AIP_EventID ASC))

INSERT
INTO	#AIP_UserEventType
	(AIP_EventID ,
	InventoryID,
	AIP_EventTypeID,
	AIP_EventCategoryID,
	Value,
	EndDate)
SELECT  E.AIP_EventID ,
	E.InventoryID,
	E.AIP_EventTypeID,
	ET.AIP_EventCategoryID,
	E.Value,
	E.EndDate
FROM	#Inventory I
JOIN	dbo.AIP_Event E 
	ON I.InventoryID = E.InventoryID
JOIN dbo.AIP_EventType ET 
	ON E.AIP_EventTypeID = ET.AIP_EventTypeID
WHERE	I.InventoryActive = 1
AND	E.AIP_EventTypeID <>14
AND	ET.AIP_EventCategoryID in (1,2,3,4)


SELECT 	V.VehicleYear VehicleYear, 
	LEFT(V.Make + ' '+  V.Model + ISNULL(' ' + NULLIF(V.VehicleTrim,'N/A'),'') + ' (' + VehicleDriveTrain + ' - ' + VehicleEngine + ')',80) VehicleDescription,
	V.BaseColor Color, 
	MileageReceived Mileage, 
	I.StockNumber StockNumber,
	I.InventoryID,
	I.LotPrice LotPrice,
	Price.Notes PricingNotes,
	CASE WHEN @ShowRepricing = 1 THEN COALESCE(RP.Reprice, I.ListPrice, 0.0) ELSE ISNULL(I.ListPrice,0.0) END Price,
	CAST(CASE WHEN RP.Reprice IS NOT NULL THEN 1 ELSE 0 END AS BIT) Repriced,
	1 - MRO.HasWholesaleOrSoldObjective, 		-- NOT SURE IF THIS NEEDS TO BE RETURNED
	S.Segment
FROM 	#Inventory I 
	JOIN Vehicle V WITH(NOLOCK) ON I.VehicleID = V.VehicleID
	LEFT JOIN (	SELECT	E.InventoryID, E.Value Reprice
			FROM	#AIP_EventType5 E
				JOIN (	SELECT	InventoryID, 
						MAX(AIP_EventID) AIP_EventID	-- SINCE WE DON'T EDIT REPRICING EVENTS, TAKE THE LAST BY IDENTITY 
					FROM	#AIP_EventType5
					GROUP
					BY	InventoryID
					) R ON E.InventoryID = R.InventoryID 
					AND E.AIP_EventID = R.AIP_EventID
			) RP ON I.InventoryID = RP.InventoryID

	LEFT JOIN (	SELECT	InventoryID			= E.InventoryID, 
				ObjectiveID			= E.AIP_EventCategoryID, 
				HasWholesaleOrSoldObjective	= CASE WHEN E.AIP_EventCategoryID IN (2,3) THEN 1 ELSE 0 END
			FROM	#AIP_UserEventType E  
				JOIN (	SELECT	E.InventoryID, 
						MAX(E.AIP_EventID) AIP_EventID					-- IN CASE OF MULTIPLE EVENTS ON DATE
					FROM 	#AIP_UserEventType E	 								-- OF THE LAST "Objective EVENT", ONLY 
						JOIN (	SELECT	E.InventoryID, 
								MAX(ISNULL(E.EndDate,'6/6/2079')) EndDate	-- GET ONE ROW TO PREVENT DUPLICATES
							FROM	#AIP_UserEventType E
							GROUP
							BY	E.InventoryID
							) MRO 
					ON E.InventoryID = MRO.InventoryID 
					AND ISNULL(E.EndDate,'6/6/2079') = ISNULL(MRO.EndDate, '6/6/2079')
					GROUP
					BY	E.InventoryID	
					) SE ON E.InventoryID = SE.InventoryID 
					AND E.AIP_EventID = SE.AIP_EventID
			) MRO ON I.InventoryID = MRO.InventoryID
	LEFT OUTER JOIN Market.Pricing.VehiclePricingDecisionInput Price on Price.vehicleEntityId=I.inventoryId and Price.VehicleEntityTypeID = 1 
	LEFT JOIN Segment S on V.segmentId = S.SegmentId
WHERE 	(@RetailOnly = 0 
	OR (MRO.InventoryID IS NULL OR MRO.HasWholesaleOrSoldObjective = 0))	-- PARENS NOT REQ'D JUST FOR GROUPING
	--AND (Price.vehicleEntityTypeID=1 OR Price.vehicleEntityTypeID is null) 
ORDER BY
	I.InventoryID,	
	V.make ASC, 
	V.model ASC, 
	V.VehicleYear DESC, 
	I.inventoryReceivedDate ASC



GO


