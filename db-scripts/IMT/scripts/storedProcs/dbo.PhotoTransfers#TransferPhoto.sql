USE [IMT]
GO
/****** Object:  StoredProcedure [dbo].[PhotoTransfers#TransferPhoto]    Script Date: 10/30/2012 15:52:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PhotoTransfers#TransferPhoto]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[PhotoTransfers#TransferPhoto]
GO

CREATE PROCEDURE [dbo].[PhotoTransfers#TransferPhoto]
(
	@destinationInventoryId int, 
	@photoUrl varchar(200),
	@photoSequenceNumber smallint,
	@photoIsPrimary bit
)
AS
BEGIN

	SET NOCOUNT ON;

	insert into IMT.dbo.Photos 
	(
		PhotoTypeID, 
		PhotoProviderID, 
		PhotoStatusCode, 
		PhotoURL, OriginalPhotoURL,
		DateCreated, 
		IsPrimaryPhoto, 
		SequenceNumber,
		LoadID
	)
	values
	(
		2			/* Type is Inventory */,		
		null		/* TODO: Do we know after transfer? */,		
		2			/* 2 = Stored remotely */,
		@photoUrl,	
		@photoUrl,	
		getdate(),
		@photoIsPrimary,
		@photoSequenceNumber,
		null					/* TODO: what is load ID? */		
	);
		
	insert into imt.dbo.InventoryPhotos (InventoryID, PhotoID) 
	values (@destinationInventoryId, @@IDENTITY);
    
END
