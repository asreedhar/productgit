USE [IMT]
GO
/****** Object:  StoredProcedure [dbo].[PhotoTransfers#UpdateStatus]    Script Date: 10/30/2012 15:53:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PhotoTransfers#UpdateStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[PhotoTransfers#UpdateStatus]
GO

CREATE PROCEDURE [dbo].[PhotoTransfers#UpdateStatus]
	@dstInventoryId int,
	@statusCode int
AS
BEGIN
	
	update photoTransfers set statusDt=getdate(), status=@statusCode where dstInventoryId=@dstInventoryId;
    
END
