if object_id('dbo.PhotoProcessorStatus') is not null
	drop procedure dbo.PhotoProcessorStatus
go

/*
	5/24/2011 Dave Speer
	
	This proc uses the dbo.PhotoProcessorLog as a rolling log for the photo processor.
	
	The number of records retained by this proc is controlled by @LogRowsToKeep (by @Machine / @Mode).
	
	@NeedsRestart output parameter is set to 1 (true) when:
		@Mode = 'scrape'
			and PhotosInStatus3 count is the same count as the last time we checked
			and	PhotosInStatus7 count is the same count as the last time we checked
		or
		
		@Mode = 'replace'
			and PhotosInStatus3 count is the same count as the last time we checked
			
	
	we can't enforce a count DECREASE because load jobs / manual processes may be adding photos
	to the queue at any given time.  What we are looking for is no change / "hung" state.

	This proc should not be called more often than the photo processor is expected to
	make progress for a given machine.

	Transaction isolation level	is monkeyed with to implement the rolling log.
	
	
	--- test script:

	declare @NeedsRestart bit
		
	exec IMT.dbo.PhotoProcessorStatus
		@MachineName = 'dspeer01',
		@Mode = 'replace',
		@LogRowsToKeep = 10,
		@NeedsRestart = @NeedsRestart output
		
	select NeedsRestart = @NeedsRestart
*/

create procedure dbo.PhotoProcessorStatus
	@MachineName varchar(256),
	@Mode varchar(20),
	@LogRowsToKeep int = 10,
	@NeedsRestart bit output
as
set nocount on

declare 
	@PhotosInStatus1 int,
	@PhotosInStatus3 int,
	@PhotosInStatus7 int,
	@LastPhotosInStatus1 int,
	@LastPhotosInStatus3 int,
	@LastPhotosInStatus7 int,
	@ReuseLogCyclePointer binary(8)
	
-- for counting the photos / reading the log
set transaction isolation level read uncommitted

select
	@PhotosInStatus1 = sum(case when PhotoStatusCode = 1 then 1 else 0 end),
	@PhotosInStatus3 = sum(case when PhotoStatusCode = 3 then 1 else 0 end),
	@PhotosInStatus7 = sum(case when PhotoStatusCode = 7 then 1 else 0 end)
from IMT.dbo.Photos



begin try
	-- for rolling the log cleanly
	set transaction isolation level serializable
	begin tran

	
	-- LOCK THE TABLE and get the previous execution's stats
	select
		@LastPhotosInStatus1 = PhotosInStatus1,
		@LastPhotosInStatus3 = PhotosInStatus3,
		@LastPhotosInStatus7 = PhotosInStatus7
	from dbo.PhotoProcessorLog with (tablockx, holdlock)
	where
		LogCyclePointer = 
		(
			select max(LogCyclePointer)
			from dbo.PhotoProcessorLog
			where
				MachineName = @MachineName
				and Mode = @Mode
		)


	if @LogRowsToKeep > 
	(
		select count(*)
		from dbo.PhotoProcessorLog
		where
			MachineName = @MachineName
			and Mode = @Mode
	)
	begin
		-- insert this log record
		insert dbo.PhotoProcessorLog
		(
			MachineName,
			Mode,
			LogDate,
			PhotosInStatus1,
			PhotosInStatus3,
			PhotosInStatus7
		)
		values
		(
			@MachineName,
			@Mode,
			getdate(),
			@PhotosInStatus1,
			@PhotosInStatus3,
			@PhotosInStatus7
		)
	end
	else
	begin
		-- reuse a previous log row
		
		select @ReuseLogCyclePointer = min(LogCyclePointer)
		from dbo.PhotoProcessorLog
		where
			MachineName = @MachineName
			and Mode = @Mode

		update dbo.PhotoProcessorLog set
			MachineName = @MachineName,
			Mode = @Mode,
			LogDate = getdate(),
			PhotosInStatus1 = @PhotosInStatus1,
			PhotosInStatus3 = @PhotosInStatus3,
			PhotosInStatus7 = @PhotosInStatus7
		where LogCyclePointer = @ReuseLogCyclePointer
	end

	-- now that we have the last run's stats and have logged this run, should we restart?
	
	if 
		(lower(@Mode) = 'scrape' and @PhotosInStatus3 = @LastPhotosInStatus3 and @PhotosInStatus7 = @LastPhotosInStatus7)
		or
		(lower(@Mode) = 'replace' and @PhotosInStatus3 = @LastPhotosInStatus3)
	begin
		set @NeedsRestart = 1
	end
	else
	begin
		set @NeedsRestart = 0
	end
	
	commit
	
	-- back to sql server default for connection pooling
	set transaction isolation level read committed
	
end try
begin catch
	rollback
	-- back to sql server default for connection pooling
	set transaction isolation level read committed
end catch


return 0

go