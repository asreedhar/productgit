IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.ValidateParameter_MemberID#UserName') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE dbo.ValidateParameter_MemberID#UserName AS SELECT 1')
GO

ALTER PROCEDURE  [dbo].[ValidateParameter_MemberID#UserName]
	@UserName VARCHAR(80),
	@MemberID INT OUTPUT
AS
/* --------------------------------------------------------------------
 * 
 * $Id: dbo.ValidateParameter_MemberID#UserName.sql,v 1.4 2010/02/10 21:54:23 bfung Exp $
 * 
 * Summary
 * -------
 * 
 *	Verifies that the User Name exists and return memberID
 *	1. Is Not Null
 *	2. Exists in IMT..Member
 *
 *
 *
 * Parameters
 * ----------
 *
 * @UserName  - UserName
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @UserName is null
 * 50110 - @UserName is not found in the Member table
 *
 * History
 * -------
 * MAK	05/26/2009	Create procedure.
 * DGH  06/17/2015  Fix username error handling
 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

IF @UserName IS NULL
BEGIN
	RAISERROR (50100,16,1,'@UserName')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Populate Result Set
------------------------------------------------------------------------------------------------
SELECT @MemberID = MemberID
FROM	dbo.Member
WHERE	Login =@UserName

IF @MemberID is null

BEGIN
	RAISERROR (50110,16,1,'@UserName', @UserName)
	RETURN @@ERROR
END

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN

GO