

/****** Object:  StoredProcedure [dbo].[ProfitabilityByBuyerDetail]    Script Date: 06/12/2014 19:37:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProfitabilityByBuyerDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ProfitabilityByBuyerDetail]
GO



/****** Object:  StoredProcedure [dbo].[ProfitabilityByBuyerDetail]    Script Date: 06/12/2014 19:37:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
  
CREATE PROCEDURE [dbo].[ProfitabilityByBuyerDetail]   ---2,101126,1
  --DECLARE
 @PeriodID INT,  
 @DealerID INT,  
 @TradeOrPurchase  INT--1 for Purchase and 2 for Trade  
  
 AS  
BEGIN  
DECLARE  
 @LocalPeriodID INT,    
 @LocalDealerID INT,    
 @LocalTradeOrPurchase  INT 
   
SELECT  
  @LocalPeriodID=@PeriodID,  
  @LocalDealerID=@DealerID,  
  @LocalTradeOrPurchase=@TradeOrPurchase  
  
 SET NOCOUNT ON;  
 SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
 
DECLARE @DateField DATE,@EndDate DATE 
  
Select @DateField=StartDate,@EndDate=EndDate FROM IMT.dbo.GetDatesFromPeriodId(@LocalPeriodID,0);
  
  
SELECT  
 case when ((FirstName IS NULL OR LTRIM(RTRIM(FirstName))='' OR FirstName='firstName lastName'  OR ASCII(ltrim(rtrim(FirstName))) = 160) AND @LocalTradeOrPurchase=1)--
 then 'No Buyer' 
 when ((AV.AppraiserName IS NULL OR LTRIM(RTRIM(AV.AppraiserName))='' OR AV.AppraiserName='firstName lastName'OR ASCII(ltrim(rtrim(AV.AppraiserName))) = 160)  AND @LocalTradeOrPurchase=2)-- 
 then 'No Appraiser' 
 when @LocalTradeOrPurchase=1 then (P.FirstName + ' ' + P.LastName)
 else AV.AppraiserName end as AppraiserName,      
 --Retail Sales  
 SUM(CASE WHEN VS.SaleDescription = 'R' THEN FrontEndGross ELSE NULL END ) as RetailTotalGross,  
 SUM(CASE WHEN VS.SaleDescription = 'R' THEN 1 ELSE 0 END) as RetailSold,   
 CAST(ROUND(AVG(CASE WHEN VS.SaleDescription = 'R' THEN FrontEndGross ELSE NULL END ),0)AS Int) as RetailAvgGross,  
 SUM(CASE WHEN VS.SaleDescription = 'R' THEN I.daysToSale ELSE NULL END ) as RetailAvgSaleDays,  
 --Wholesale Sales  
 SUM(CASE WHEN VS.SaleDescription = 'W' AND I.DaysToSale < 30 THEN FrontEndGross ELSE NULL END ) as WholesaleTotalGross,--added to get wholesale data in profitByppraiser report  
 SUM(CASE WHEN VS.SaleDescription = 'W' AND I.DaysToSale < 30 THEN 1 ELSE 0 END) as WholesaleSold,   
 CAST(ROUND(AVG(CASE WHEN VS.SaleDescription = 'W' AND I.DaysToSale < 30 THEN FrontEndGross ELSE NULL END ),0)AS Int) as WholesaleAvgGross,   
 SUM(CASE WHEN VS.SaleDescription = 'W' AND I.DaysToSale < 30 THEN I.daysToSale ELSE NULL END ) as WholesaleAvgSaleDays,  
 --No Sales  
 SUM(CASE WHEN VS.SaleDescription = 'W' AND I.DaysToSale > 29 THEN FrontEndGross ELSE NULL END ) as NoSaleTotalLoss,  
 SUM(CASE WHEN VS.SaleDescription = 'W' AND I.DaysToSale > 29 THEN 1 ELSE 0 END) as NoSales,   
 CAST(ROUND(AVG(CASE WHEN VS.SaleDescription = 'W' AND I.DaysToSale > 29 THEN FrontEndGross ELSE NULL END ),0)AS Int) as NoSaleAvgLoss,
 SUM(CASE WHEN VS.SaleDescription = 'W' AND I.DaysToSale > 29 THEN I.daysToSale ELSE NULL END ) as NoSaleAvgSaleDays  
 --INTO #Temp  
  
FROM   
 [FLDW].dbo.Inventory AS I   
 JOIN [FLDW].dbo.VehicleSale AS VS ON I.InventoryID = VS.InventoryID   AND I.InventoryType = 2
 JOIN [FLDW].dbo.Vehicle AS V ON I.BusinessUnitID = V.BusinessUnitID AND I.VehicleID = V.VehicleID 
 INNER JOIN IMT.dbo.DealerPreference D on D.BusinessUnitID = I.BusinessUnitID
 INNER JOIN [imt].dbo.Appraisals A 
ON I.BusinessUnitID = A.BusinessUnitID AND V.vehicleId = A.vehicleId 


LEFT JOIN FLDW.DBO.Appraisal_F AF 
ON I.BusinessUnitID = AF.BusinessUnitID AND V.vehicleId = AF.vehicleId  

LEFT JOIN [FLDW].dbo.AppraisalValues AV 
on A.AppraisalId = AV.AppraisalID And A.AppraisalID=AF.AppraisalID and  AF.Value = AV.Value and AF.AppraisalValueID=AV.AppraisalValueID  

LEFT Join imt.dbo.Person P 
on P.PersonID=A.SelectedBuyerId

WHERE  
 I.BusinessUnitID = @LocalDealerID  
 AND VS.DealDate BETWEEN @DateField and @EndDate
 AND CAST(A.DateCreated AS DATE) <= I.InventoryReceivedDate 
 AND CAST(A.DateCreated AS DATE) between DATEADD(DD,-D.AppraisalLookBackPeriod,I.InventoryReceivedDate) and I.InventoryReceivedDate  
 AND I.TradeOrPurchase = @LocalTradeOrPurchase 
 AND A.AppraisalTypeID = CASE WHEN @LocalTradeOrPurchase = 2
                                                    THEN 1
                                                    ELSE 2
                                               END  
 --AND I.InventoryType = 2 -- Used Vehicles only  
 --AND I.InventoryID = 35542064
  
GROUP   
BY  --AppraiserName  
 case when ((FirstName IS NULL OR LTRIM(RTRIM(FirstName))='' OR FirstName='firstName lastName'  OR ASCII(ltrim(rtrim(FirstName))) = 160) AND @LocalTradeOrPurchase=1)--
 then 'No Buyer' 
 when ((AV.AppraiserName IS NULL OR LTRIM(RTRIM(AV.AppraiserName))='' OR AV.AppraiserName='firstName lastName'OR ASCII(ltrim(rtrim(AV.AppraiserName))) = 160)  AND @LocalTradeOrPurchase=2)-- 
 then 'No Appraiser' 
 when @LocalTradeOrPurchase=1 then (P.FirstName + ' ' + P.LastName)
 else AV.AppraiserName end

ORDER  
BY RetailSold DESC  
    
  --UPDATE #Temp   
  --SET AppraiserName='No Buyer'  
  --WHERE AppraiserName IS NULL OR LTRIM(RTRIM(AppraiserName))='' OR AppraiserName='firstName lastName'  OR ASCII(ltrim(rtrim(AppraiserName))) = 160  
  --SELECT * FROM #Temp  
  --DROP TABLE #Temp  
  
  
--END  

  
  
END  

GO


