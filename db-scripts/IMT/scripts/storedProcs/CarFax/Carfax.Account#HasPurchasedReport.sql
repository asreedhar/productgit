
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[Account#HasPurchasedReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Carfax].[Account#HasPurchasedReport]
GO

CREATE PROCEDURE [Carfax].[Account#HasPurchasedReport]  
 @Id INT,  
 @Vin VARCHAR(17)  
AS  
/* --------------------------------------------------------------------  
 *   
 * $Id: Carfax.Account#HasPurchasedReport.sql,v 1.5 2010/02/10 21:54:16 bfung Exp $  
 *   
 * Summary  
 * -------  
 *   
 * Returns 1 Result Set with a single BIT column Exists.  
 *  
 * Parameters  
 * ----------  
 *  
 * @Id   - Id that maps to a PK in the Carfax.Account table.  
 * @Vin  - Vin is the UK of the vehicle for whom we are searching for a report  
 *   
 * Exceptions  
 * ----------  
 *   
 * 50100 - @Id is null  
 * 50106 - Record for Id does not exist  
 * 50100 - @VIN is null  
 * 50114 - LEN(Vin) <> 17  
 * 50200 - Expected one Row  
 *  
 * History  
 * ----------  
 *   
 * MAK 05/26/2009 Create procedure.  
 * MAK 05/29/2009 Updated Message code and included expiration date.  
 *         
 * -------------------------------------------------------------------- */  
  
SET NOCOUNT ON  
  
------------------------------------------------------------------------------------------------  
-- Validate Parameters  
------------------------------------------------------------------------------------------------  
  
DECLARE @rc INT, @err INT  
  
EXEC @err = Carfax.ValidateParameter_AccountID @Id  
IF (@err <> 0) GOTO Failed  
  
EXEC @err = dbo.ValidateParameter_VIN @Vin  
IF (@err <> 0) GOTO Failed  
  
------------------------------------------------------------------------------------------------  
-- Declare Result Set  
------------------------------------------------------------------------------------------------  
  
DECLARE @Results TABLE  
 ([Exists] BIT)  
  
------------------------------------------------------------------------------------------------  
-- Populate Result Set  
------------------------------------------------------------------------------------------------  
  
DECLARE @AccountTypeID TINYINT, @Exists BIT  
  
SELECT @AccountTypeID = AccountTypeID  
FROM Carfax.Account  
WHERE AccountID = @Id  
  
SET @Exists = 0  
  
IF (@AccountTypeID = 1) BEGIN  
   
 /* Use Carfax.Vehicle_Dealer.RequestID */  
   
 IF EXISTS (  
   SELECT 1  
   FROM Carfax.Account_Dealer A  
   JOIN Carfax.Vehicle_Dealer D ON D.DealerID = A.DealerID  
   JOIN Carfax.Vehicle V ON V.VehicleID = D.VehicleID  
   JOIN Carfax.Request R ON R.RequestID = D.RequestID  
   JOIN Carfax.Report E ON E.RequestID = R.RequestID  
   WHERE V.VIN = @Vin  
   AND A.AccountID = @Id  
   --AND E.ExpirationDate >= GETDATE()    BUZID:28727
   AND E.ExpirationDate >= convert(Date,GETDATE())
  )  
  SET @Exists = 1  
   
END ELSE BEGIN  
   
 /* Use Carfax.Vehicle.RequestID */  
   
 IF EXISTS (  
   SELECT 1  
   FROM Carfax.Account A  
   JOIN Carfax.Request R ON R.AccountID = A.AccountID  
   JOIN Carfax.Vehicle V ON V.VehicleID = R.VehicleID  
   JOIN Carfax.Report E ON E.RequestID = R.RequestID  
   WHERE V.VIN = @Vin  
   AND A.AccountID = @Id  
      --AND E.ExpirationDate >= GETDATE() BUZID:28727
   AND E.ExpirationDate >= convert(Date,GETDATE())
  )  
  SET @Exists = 1  
   
END  
  
INSERT INTO @Results ([Exists]) VALUES (@Exists)  
  
SELECT @rc = @@ROWCOUNT, @err = @@ERROR  
IF (@err <> 0) GOTO Failed  
  
------------------------------------------------------------------------------------------------  
-- Validate Results  
------------------------------------------------------------------------------------------------  
  
-- there must be one row in the tables  
  
IF (@rc <> 1)  
BEGIN  
 RAISERROR (50200,16,1,@rc)  
 RETURN @@ERROR  
END  
  
  
------------------------------------------------------------------------------------------------  
-- Success  
------------------------------------------------------------------------------------------------  
  
SELECT * FROM @Results  
  
RETURN 0  
------------------------------------------------------------------------------------------------  
-- Failure  
------------------------------------------------------------------------------------------------  
  
Failed:  
  
RETURN @err  

GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.Account#HasPurchasedReport to [CarfaxUser]
GO