IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.ReportProcessorCommand#Exception ') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Carfax.ReportProcessorCommand#Exception AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.ReportProcessorCommand#Exception to [CarfaxUser]
GO

ALTER  PROCEDURE [Carfax].[ReportProcessorCommand#Exception] 
	@Id INT,
 	@MachineName VARCHAR(256),
	@Time DATETIME,
	@Type VARCHAR(256),
	@Message VARCHAR(1024),
	@Detail TEXT,
	@ReportID INT
AS
/* --------------------------------------------------------------------
 * 
 * 
 * Summary
 * -------
 * 
 *	Records an Exception from the processor.
 *	Updates ReportProcessorCommand table and Inserts into the
 *	Exception and the the ProcessorCommand_Exception table.
 *
 * Exceptions
 * ----------
 * 
 *	50100 @id IS NULL
 *	50106 @Id does not exist
 *	50100 @MachineName  IS NULL
 *	50100 @Time  IS NULL
 *	
 *
 *	Returns:
 *
 *
 * History
 * ----------
 * 
 * MAK	06/01/2009	Create procedure.
 * MAK	07/09/2009	Update procedure to comply to requirements updated in FB. 6078.
 *					to recover from outages
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

BEGIN TRY

IF (@Id  is null)
BEGIN
	RAISERROR (50100,16,1,'@Id')
	RETURN @@ERROR
END

IF (NOT EXISTS (	SELECT	1
		FROM 	Carfax.ReportProcessorCommand
		WHERE	ReportProcessorCommandID =@Id))
BEGIN
	RAISERROR (50106,16,1,'@Id',@Id)
	RETURN @@ERROR
END

IF (@MachineName  is null)
BEGIN
	RAISERROR (50100,16,1,'MachineName')
	RETURN @@ERROR
END

IF (@Time  is null)
BEGIN
	RAISERROR (50100,16,1,'Time')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Perform Update
------------------------------------------------------------------------------------------------

BEGIN TRANSACTION
	
------------------------------------------------------------------------------------------------
-- Perform Inserts
------------------------------------------------------------------------------------------------

	INSERT
	INTO	Carfax.Exception
		(ExceptionTime,
		MachineName,
		Message,
		ExceptionType,
		Details,
		RequestID)
	VALUES (@Time,
		@MachineName,
		@Message,
		@Type,
		@Detail,
		@ReportID)

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR

	IF  (@rc <> 1)
	BEGIN
		RAISERROR (50200,16,1,@rc)
		SET @err=  @@ERROR
	END
	
	DECLARE @ExceptionID INT
	SET @ExceptionID =SCOPE_IDENTITY()

	INSERT
	INTO 	Carfax.ReportProcessorCommand_Exception
		(ReportProcessorCommandID,
		ExceptionID)
	VALUES 	(@Id,
		@ExceptionID)
	
	SELECT @rc = @@ROWCOUNT, @err = @@ERROR

	IF (@RC <> 1)
	BEGIN
		RAISERROR (50200,16,1,@rc)
		SET @err=  @@ERROR
	END

	DECLARE @IsTryAgain BIT
	SET @IsTryAgain =0
	DECLARE @ResponseCode CHAR(3)
	SET @ResponseCode =''
	DECLARE @AccountStatusResponseCode CHAR(3)
	SET @AccountStatusResponseCode ='903' -- From Response Code Table.

 	IF (@ReportID IS NOT NULL)
	BEGIN
		INSERT
		INTO	Carfax.ReportProcessorCommand_Request
			(ReportProcessorCommandID,
		 	RequestID)
		VALUES	(@Id,
			@ReportID)

		SELECT 	@ResponseCode = RS.ResponseCode,
			@IsTryAgain =RC.IsTryAgain
		FROM	Carfax.Response RS
		JOIN	Carfax.ResponseCode RC
		ON	RS.ResponseCode =RC.ResponseCode
		WHERE	RS.RequestID =@ReportID
		
	END
	
	DECLARE @FailedWorkStatusID TINYINT
	SET @FailedWorkStatusID=4
	DECLARE @ReadyStatusID TINYINT
	SET @ReadyStatusID =1
	DECLARE @AccountProblemStatusID TINYINT
	SET @AccountProblemStatusID =5
	DECLARE @ProcessAfterTimeInMinutes TINYINT
	SET @ProcessAfterTimeInMinutes =30

	
	BEGIN
	-- If the Response code is that of AccountProblem,  update
	--  All RPC
		IF (@ResponseCode =@AccountStatusResponseCode) 
		BEGIN
		
			DECLARE @AccountID INT
		
			SELECT @AccountID = AD.AccountID
			FROM 	Carfax.ReportProcessorCommand RPC
			JOIN	Carfax.Account_Dealer AD
			ON	RPC.DealerID =AD.DealerID
			WHERE	ReportProcessorCommandID =@Id	
		
			UPDATE	RPC
			SET		ReportProcessorCommandStatusID =@AccountProblemStatusID,
					ProcessAfterDate =GETDATE()
			FROM	Carfax.ReportProcessorCommand RPC
			JOIN	Carfax.Account_Dealer AD
			ON		RPC.DealerID =AD.DealerID
			WHERE	AD.AccountID =@AccountID
			AND		RPC.ReportProcessorCommandStatusID =@ReadyStatusID
		

		END
		-- If the Response code is not of AccountProblem and the TryAgain value of that code is 1
		-- update the status to ready and set a ProcessAfterDate to the delay (this case 30 minutes).
		IF (@ResponseCode <>@AccountStatusResponseCode AND @IsTryAgain =1) 
			UPDATE	Carfax.ReportProcessorCommand
			SET		ReportProcessorCommandStatusID =@ReadyStatusID,
					ProcessAfterDate =DATEADD(mi,@ProcessAfterTimeInMinutes,GETDATE())
			WHERE	ReportProcessorCommandID =@Id
		-- If the ResponseCode is not one of TryAgain, set the status to failed.
 		IF (@ResponseCode <>@AccountStatusResponseCode AND @IsTryAgain <>1) 
			UPDATE	Carfax.ReportProcessorCommand
			SET		ReportProcessorCommandStatusID = @FailedWorkStatusID
			WHERE	ReportProcessorCommandID =@Id
	END


------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------
 	COMMIT TRANSACTION
	RETURN 0
END TRY



--exit the procedure
BEGIN CATCH

	DECLARE @ErrNum INT
	DECLARE @ErrorMessage VARCHAR(2000)
	SET @ErrorMessage =ERROR_MESSAGE()  
	SET @ErrNum =ERROR_NUMBER() 

	IF (XACT_STATE() =1 OR XACT_STATE()=-1)
	 ROLLBACK TRANSACTION
	
	RAISERROR(@ErrorMessage, 16, 1)
	 
	RETURN @ErrNum

END CATCH

GO
 


