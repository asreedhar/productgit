IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.Report#Insert') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Carfax.Report#Insert AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.Report#Insert to [CarfaxUser]
GO


ALTER  PROCEDURE [Carfax].[Report#Insert]
	(@Id	INT,
	@Document	XML,
	@ExpirationDate	DATETIME,
	@OwnerCount	INT,
	@HasProblem	BIT)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Carfax.Report#Insert.sql,v 1.5.4.3 2010/06/02 18:40:42 whummel Exp $
 * 
 * Summary
 * -------
 * 
 *	Insert the actual Report (XML plus extracted meta-data).
 *
 * Exceptions
 * ----------
 * 
 *	 50100 Id is NULL
 *	 50106 Request with Id does not exist
 *	 50116 Response for Request does not have a success code (500 or 501)
 *	 50100 Document is NULL
 *	 50100 ExpirationDate is NULL
 *	 50100 Document is NULL
 *	 50100 OwnerCount is NULL
 *	 50117 OwnerCount <0
 *	 50100 HasProblem is NULL
 *	 50200 Expected to insert one row (but inserted 0 or > 1)
 *
 *
 *
 *	Returns:
 *		No ResultSet returned.
 *
 * History
 * ----------
 * 
 * MAK	05/27/2009	Create procedure.
 * MAK	06/08/2009	Update procedure so that Vehicle is updated with the 
 *			lasted RequestID (that is in Report).
 *	ffo	2009.06.11	Added transactions
 * MAK	06/29/2009	Fixed Transactions to use TRY CATCH.
 * MAK  09/15/2009	FB 7032  Update Carfax.Vehicle_Dealer IsHotListEnabled flag.
 * MAK	02/15/2010	FB 7360	 Flag on Carfax.Vehicle_Dealer should match Last Request.
 * MAK	03/23/2010	FM 9269  Carfax.Vehicle_Dealers should be updated only upon successful request.
 *
 * -------------------------------------------------------------------- */

--DEFAULT SETTINGS
SET NOCOUNT ON

BEGIN TRY
------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------
	DECLARE @RowCount INT, @err INT

	IF (@Id IS NULL)
	BEGIN
		RAISERROR (50100,16,1,'ReportID')
	END

	IF (NOT EXISTS (SELECT 1
		FROM 	Carfax.Request
		WHERE 	RequestID =@Id))
	BEGIN
		RAISERROR (50106,16,1,'Id',@Id)
	END

	IF (NOT EXISTS (SELECT 1
		FROM 	Carfax.Response
		WHERE 	RequestID =@ID
		AND 	ResponseCode IN ('500','501')))
	BEGIN
		RAISERROR (50116,16,1,'Id',@Id)
	END

	IF (@Document IS NULL)
	BEGIN
		RAISERROR (50100,16,1,'Document')
	END

	IF (@ExpirationDate IS NULL)
	BEGIN
		RAISERROR (50100,16,1,'ExpirationDate')
	END

	IF (@ExpirationDate < GETDATE())
	BEGIN
		RAISERROR (50118,16,1)
	END

	IF (@OwnerCount IS NULL)
	BEGIN
		RAISERROR (50100,16,1,'OwnerCount')
	END

	IF (@OwnerCount <0)
	BEGIN
		RAISERROR (50117,16,1,'OwnerCount')
	END

	IF (@HasProblem IS NULL)
	BEGIN
		RAISERROR (50100,16,1,'HasProblem')
	END

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------
	BEGIN TRANSACTION

	INSERT
	INTO	Carfax.Report
			(RequestID,
			Report,
			ExpirationDate,
			OwnerCount,
			HasProblem)
	VALUES	(@Id,
			@Document,
			@ExpirationDate,
			@OwnerCount,
			@HasProblem)

	SELECT @Rowcount = @@ROWCOUNT, @err = @@ERROR

	IF (@RowCount <> 1)
	BEGIN
		RAISERROR (50200,16,1,@RowCount)
	END

	UPDATE	V
	SET		RequestID =@Id
	FROM	Carfax.Vehicle V
	JOIN 	Carfax.Request R ON R.VehicleID = V.VehicleID
	WHERE	R.RequestID =@Id

	SELECT @Rowcount = @@ROWCOUNT, @err = @@ERROR
	IF (@RowCount <>1)
	BEGIN
		RAISERROR (50200,16,1,@Rowcount)
	END
	
	IF	(NOT EXISTS (SELECT 1
						FROM	Carfax.Vehicle_Dealer VD
						JOIN	Carfax.Request R ON VD.VehicleID =R.VehicleID
						JOIN	Carfax.Account_Dealer AD ON AD.AccountID = R.AccountID
						WHERE	R.RequestID =@Id AND VD.DealerID = AD.DealerID))
		BEGIN
			INSERT
			INTO	Carfax.Vehicle_Dealer
					(VehicleID,
					DealerID,
					RequestID,
					IsHotListEnabled)
			SELECT	V.VehicleID,
					AD.DealerID,
					R.RequestID,
					CASE WHEN R.DisplayInHotList =1 AND V.IsHotListEligible =1 THEN 1
									ELSE 0 END
			FROM	Carfax.Vehicle V
			JOIN	Carfax.Request R ON	V.VehicleID =R.VehicleID
			JOIN	Carfax.Account A ON	R.AccountID =A.AccountID
			JOIN	Carfax.Account_Dealer AD ON A.AccountID =AD.AccountID
			WHERE	R.RequestID =@Id
		END
		ELSE
			BEGIN
			
			UPDATE VD
			SET		IsHotListEnabled =  CASE WHEN R.DisplayInHotList =1 AND V.IsHotListEligible =1 THEN 1
									ELSE 0 END,
					RequestID=@ID		
			FROM	Carfax.Vehicle_Dealer VD
			JOIN	Carfax.Vehicle V ON	VD.VehicleID =V.VehicleID
			JOIN	Carfax.Request R ON	V.VehicleID =R.VehicleID
			JOIN	Carfax.Account A ON	R.AccountID =A.AccountID
			JOIN	Carfax.Account_Dealer AD ON A.AccountID =AD.AccountID AND AD.DealerID =VD.DealerID
			WHERE	R.RequestID =@Id
		
			END
	 
	
------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

	COMMIT TRANSACTION
	RETURN 0
END TRY
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

BEGIN CATCH

	 
	DECLARE @ErrNum INT
	DECLARE @ErrorMessage VARCHAR(2000)
	SET @ErrorMessage =ERROR_MESSAGE()  
	SET @ErrNum =ERROR_NUMBER() 

	IF (XACT_STATE() =1 OR XACT_STATE()=-1)
	 ROLLBACK TRANSACTION
	
	RAISERROR(@ErrorMessage, 16, 1)
	 
	RETURN @ErrNum

END CATCH


GO
