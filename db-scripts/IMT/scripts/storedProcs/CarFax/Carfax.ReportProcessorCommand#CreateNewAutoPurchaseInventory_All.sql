IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.ReportProcessorCommand#CreateNewAutoPurchaseInventory_All ') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE  Carfax.ReportProcessorCommand#CreateNewAutoPurchaseInventory_All  AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.ReportProcessorCommand#CreateNewAutoPurchaseInventory_All to [CarfaxUser]
GO

ALTER PROCEDURE [Carfax].[ReportProcessorCommand#CreateNewAutoPurchaseInventory_All]
AS /* --------------------------------------------------------------------
 * 
 * 
 * Summary
 * -------
 * 
 *	Call the procedure Carfax.ReportProcessorCommand#CreateNewAutoPurchaseInventory
 *	for each dealer set to AutoPurchase.
 *
 * Exceptions
 * ----------
 * 
 *	
 *
 *	Returns:
 *
 *
 * History
 * ----------
 * 
 * MAK		06/15/2009	Create procedure.	
 * WGH		12/28/2009	Added DealerID message for failure tracking	
 * MAK		11/03/2010	Do not include Accounts with bad username\passwords.
 *				
 * -------------------------------------------------------------------- */

    SET NOCOUNT ON
  
    DECLARE @Table TABLE
        (
          idx INT IDENTITY(1, 1),
          DealerID INT
        )

    INSERT  INTO @Table ( DealerID )
            SELECT  RP.DealerID
            FROM    Carfax.ReportPreference RP
                    JOIN Carfax.Account_Dealer AD ON RP.DealerID = AD.DealerID
                    JOIN Carfax.Account A ON AD.AccountID = A.AccountID
            WHERE   VehicleEntityTypeID = 1
                    AND AutoPurchase = 1
                    AND AccountStatusID IN ( 1, 2 ) --New or Okay

    DECLARE @i INT,
        @c INT,
        @DealerID INT,
        @Message VARCHAR(20)

    SELECT  @i = 1,
            @c = COUNT(*)
    FROM    @Table

    WHILE( @i <= @c )
        BEGIN

            SELECT  @DealerID = DealerID,
                    @Message = CAST(DealerID AS VARCHAR(20))
            FROM    @Table
            WHERE   idx = @i

            BEGIN TRY
	
                EXEC Carfax.ReportProcessorCommand#CreateNewAutoPurchaseInventory @DealerID
	
            END TRY
            BEGIN CATCH

                EXEC dbo.sp_ErrorHandler @Message = @Message	-- ADD THE DealerID TO THE MESSAGE TO TRACK FAILURES
			
            END CATCH

            SET @i = @i + 1
		
        END
	
		
GO