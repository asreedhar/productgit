IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].[Account#Insert]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE  [Carfax].[Account#Insert]AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Carfax].[Account#Insert] to [CarfaxUser]
GO

ALTER PROCEDURE [Carfax].[Account#Insert]
	@AccountTypeId INT,
	@Active BIT,
	@UserName VARCHAR(20),
	@Password VARCHAR(5),
	@InsertUser VARCHAR(80),
	@Id INT OUTPUT,
	@NewVersion BINARY(8) OUTPUT 
AS
/* --------------------------------------------------------------------
 * 
 * $Id: Carfax.Account#Insert.sql,v 1.6 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 *	Insert a Value into the Carfax.Dealer_Account table.
 *
 *
 *	Parameters:
 *	@AccountTypeId 
 *	@Active
 *	@UserName
 *	@Password
 *	@InsertUser 
 *	@Id
 *	@NewVersion
 *
 *	Validation
 *
* Exceptions
 * ----------
 * 
 *  	50100 UserName is NULL
 *    	50111 LEN(UserName) is not between 1 and 20 characters
 *    	50100 Password is NULL
 *    	50111 LEN(Password) is not between 1 and 5 characters
 *    	50100 Version IS NULL
 *    	50106 Record with Id and Version does not exist (i.e. record changed by someone else).
 *   	50200 Expected one row to update (got zero or more than one)
 *
 *
 *
 * History
 * ----------
 * 
 * MAK	05/26/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT


IF (@AccountTypeId IS NULL)
BEGIN
	RAISERROR (50100,16,1,'AccountTypeID')
	RETURN @@ERROR
END

IF (NOT EXISTS (	SELECT 	1
		FROM 	Carfax.AccountType
		WHERE 	AccountTypeID =@AccountTypeID))
BEGIN
	RAISERROR (50106,16,1,'AccountTypeID')
	RETURN @@ERROR
END


IF (@Active IS NULL)
BEGIN
	RAISERROR (50100,16,1,'Active')
	RETURN @@ERROR
END


IF (@UserName IS NULL)
BEGIN
	RAISERROR (50100,16,1,'UserName')
	RETURN @@ERROR
END

IF (LEN(@UserName) <1 OR LEN(@UserName) >20)
BEGIN
	RAISERROR (50111,16,1,'UserName',1,20)
	RETURN @@ERROR
END

IF (@Password IS NULL)
BEGIN
	RAISERROR (50100,16,1,'Password')
	RETURN @@ERROR
END

IF (LEN(@Password) <1 OR LEN(@Password) >5)
BEGIN
	RAISERROR (50111,16,1,'UserName',1,5)
	RETURN @@ERROR
END

DECLARE @InsertUserID INT

EXEC @err = dbo.ValidateParameter_MemberID#UserName @InsertUser, @InsertUserID OUTPUT
IF (@err <> 0) GOTO Failed

DECLARE @AccountStatusID TINYINT
SET @AccountStatusID =1 ---- This is the value for 'New'

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------


INSERT
INTO	Carfax.Account
	(AccountStatusId,
	AccountTypeId,
	Active,
	UserName,	
	Password,
	InsertUser,
	InsertDate)
VALUES
	(@AccountStatusId,
	@AccountTypeID,
	@Active,
	@UserName,
	@Password,
	@InsertUserID,
	GETDATE())

SET @ID = SCOPE_IDENTITY()

SELECT  @NewVersion =Version
FROM 	Carfax.Account
WHERE	AccountID =@ID

SET @rc = @@rowcount

IF (@rc <>1)
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err
GO