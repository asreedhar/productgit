IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.ValidateParameter_ReportType') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Carfax.ValidateParameter_ReportType AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.ValidateParameter_ReportType to [CarfaxUser]
GO

ALTER PROCEDURE [Carfax].[ValidateParameter_ReportType]
	@ReportType CHAR(3)
AS
/* --------------------------------------------------------------------
 * 
 * 
 * Summary
 * -------
 * 
 *	Verifies that the ReportType
 *	1. Is Not Null
 *	2. Exists in Carfax.ReportType
 *
 *
 *
 * Parameters
 * ----------
 *
 * @ReportTypeID  
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @ReportType is null
 * 50106 - @ReportType exists
 *
 * History
 * -------
 * MAK	06/10/2009	Create procedure.

 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

IF (@ReportType IS NULL)
BEGIN
	RAISERROR (50100,16,1,'ReportType')
	RETURN @@ERROR
END


IF( NOT EXISTS (
	SELECT	1
	FROM	Carfax.ReportType RT
	WHERE	RT.ReportType =@ReportType
))
	BEGIN
		RAISERROR (50106,16,1,'ReportType')
		RETURN @@ERROR
	END



------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO