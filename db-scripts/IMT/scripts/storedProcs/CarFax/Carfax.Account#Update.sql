IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.Account#Update') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE  Carfax.Account#Update AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.Account#Update to [CarfaxUser]
GO

ALTER PROCEDURE [Carfax].[Account#Update]
	@Id	INT,
	@Active		BIT,
	@UserName	VARCHAR(20),
	@Password	VARCHAR(5),
	@UpdateUser	VARCHAR(80),	
	@Version	BINARY(8),
	@NewVersion	BINARY(8) OUTPUT
AS
/* --------------------------------------------------------------------
 * 
 * $Id: Carfax.Account#Update.sql,v 1.6 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns NewVersion OUTPUT parameter
 *
 *
 * Parameters
 * ----------
 *
 *	Id
 *	Active
 *	UserName  
 *	Password 
 *	UpdateUser  
 *	Version  
 *	NewVersion
 *
 * Exceptions
 * ----------
 * 
 *  	50100 Active is NULL
 *  	50100 UserName is NULL
 *    	50111 LEN(UserName) is not between 1 and 20 characters
 *    	50100 Password is NULL
 *    	50111 LEN(Password) is not between 1 and 5 characters
 *    	50100 Version IS NULL
 *    	50106 Record with Id and Version does not exist (i.e. record changed by someone else).
 *   	50200 Expected one row to update (got zero or more than one)
 *
 *
 *
 * History
 * ----------
 * 
 * MAK	05/26/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

EXEC  @err =Carfax.ValidateParameter_AccountID @Id
IF( @err <> 0) GOTO Failed


DECLARE @MemberID INT

EXEC  @err =dbo.ValidateParameter_MemberID#UserName @UpdateUser, @MemberID OUTPUT
IF( @err <> 0) GOTO Failed


IF (@Active IS NULL)
BEGIN
	RAISERROR (50100,16,1,'Active')
	RETURN @@ERROR
END


IF (@UserName IS NULL)
BEGIN
	RAISERROR (50100,16,1,'UserName')
	RETURN @@ERROR
END

IF (LEN(@UserName) <1 OR LEN(@UserName) >20)
BEGIN
	RAISERROR (50111,16,1,'UserName',1,20)
	RETURN @@ERROR
END

IF (@Password IS NULL)
BEGIN
	RAISERROR (50100,16,1,'Password')
	RETURN @@ERROR
END

IF (LEN(@Password) <1 OR LEN(@Password) >5)
BEGIN
	RAISERROR (50111,16,1,'UserName',1,5)
	RETURN @@ERROR
END

IF (@Version IS NULL)
BEGIN
	RAISERROR (50100,16,1,'Version')
	RETURN @@ERROR
END


IF (NOT EXISTS(SELECT 1 
		FROM	Carfax.Account 
		WHERE 	AccountID =@Id
		AND		Version = @Version))
BEGIN
	RAISERROR (50106,16,1,'Version')
	RETURN @@ERROR
END


------------------------------------------------------------------------------------------------
-- Perform Update
------------------------------------------------------------------------------------------------
DECLARE @NewAccountStatusID TINYINT
SET @NewAccountStatusID =1

UPDATE	Carfax.Account
SET	UserName =@UserName,
	Password=@Password,
	UpdateUser =@MemberID,
	UpdateDate =GETDATE(),
	AccountStatusID =@NewAccountStatusID,
	Active = @Active
WHERE	AccountID =@Id
AND	Version = @Version

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF(@rc<>1)
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

SELECT 	@NewVersion =Version
FROM	Carfax.Account
WHERE	AccountID =@Id

EXEC Carfax.ReportProcessorCommand#AccountProblemResolved @Id

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err
GO