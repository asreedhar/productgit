IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.ReportProcessorCommand#Success ') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Carfax.ReportProcessorCommand#Success AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.ReportProcessorCommand#Success to [CarfaxUser]
GO

ALTER  PROCEDURE [Carfax].[ReportProcessorCommand#Success] 
	 @Id INT,
 	 @ReportID INT   
AS
/* --------------------------------------------------------------------
 * 
 * 
 * Summary
 * -------
 * 
 *	Updates ReportProcessorCommand table a with completed\success status.
 *
 * Exceptions
 * ----------
 * 
 *	50100 @id IS NULL
 *	50106 @Id does not exist
  *	50100 @Reportid IS NULL
 *	50106 @ReportId does not exist
 
 *	
 *
 *	Returns:
 *
 *
 * History
 * ----------
 * 
 * MAK	06/01/2009	Create procedure.
 * MAK - 07/07/2009	SET @SuccessWorkStatusID =3 is completed status while 4 is error. 
 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT
BEGIN TRY

IF (@Id  is null)
BEGIN
	RAISERROR (50100,16,1,'@Id')
	RETURN @@ERROR
END

IF (NOT EXISTS (	SELECT	1
		FROM 	Carfax.ReportProcessorCommand
		WHERE	ReportProcessorCommandID =@Id))
BEGIN
	RAISERROR (50106,16,1,'@ID')
	RETURN @@ERROR
END

IF (@ReportId  is null)
BEGIN
	RAISERROR (50100,16,1,'@ReportId')
	RETURN @@ERROR
END

IF (NOT EXISTS (	SELECT	1
		FROM 	Carfax.Report
		WHERE	RequestID =@ReportId))
BEGIN
	RAISERROR (50106,16,1,'@ReportID')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Perform Update
------------------------------------------------------------------------------------------------


BEGIN TRANSACTION
	DECLARE @SuccessWorkStatusID TINYINT
	SET @SuccessWorkStatusID=3
	DECLARE @ExceptionID INT

	UPDATE	Carfax.ReportProcessorCommand
	SET	ReportProcessorCommandStatusID =@SuccessWorkStatusID
	WHERE	ReportProcessorCommandID =@Id

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	
	IF (@rc <> 1)
	BEGIN
		RAISERROR (50200,16,1,@rc)
		RETURN @@ERROR
	END

	INSERT
	INTO	Carfax.ReportProcessorCommand_Request
		(ReportProcessorCommandID,
		RequestID)
	VALUES	(@Id,
		@ReportID)



	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	

	IF @rc <> 1
	BEGIN
		RAISERROR (50200,16,1,@rc)
		RETURN @@ERROR
	END



------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------
 	COMMIT TRANSACTION
	RETURN 0
END TRY



--exit the procedure
BEGIN CATCH

	DECLARE @ErrNum INT
	DECLARE @ErrorMessage VARCHAR(2000)
	SET @ErrorMessage =ERROR_MESSAGE()  
	SET @ErrNum =ERROR_NUMBER() 

	IF (XACT_STATE() =1 OR XACT_STATE()=-1)
	 ROLLBACK TRANSACTION
	
	RAISERROR(@ErrorMessage, 16, 1)
	 
	RETURN @ErrNum

END CATCH

GO