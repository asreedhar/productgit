
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.ReportProcessorCommand#CreateNewReportUpgradeWork') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE  Carfax.ReportProcessorCommand#CreateNewReportUpgradeWork AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.ReportProcessorCommand#CreateNewReportUpgradeWork to [CarfaxUser]
GO

ALTER PROCEDURE [Carfax].[ReportProcessorCommand#CreateNewReportUpgradeWork]
(	@DealerId INT,
	@VehicleEntityTypeId INT,
	@NewReportType CHAR(3)
 )

AS
/* --------------------------------------------------------------------
 * 
 * 
 * Summary
 * -------
 * 
 *	Update ReportProcessorCommand table when  a report type
 *	going from BTC to VHR, BTC TO CIP or VHR to CIP. 
 * Exceptions
 * ----------
 * 
 *	50100 DealerId IS NULL
 *	50106 Record with DealerId does not exist
 *	50100 VehicleEntityTypeId IS NULL
 *	50106 Record with VehicleEntityTypeId does not exist
 *	
 *
 *	Returns:
 *
 *
 * History
 * ----------
 * 
 *	MAK	05/29/2009	Create procedure.
 *	ffo	2009.06.11	added transaction
 *	MAK 06/29/2009	Rewrote transactions for consistency.
 *	MAK 11/17/2010	Updated new format and better error handling.
 *
 * -------------------------------------------------------------------- */


--DEFAULT SETTINGS
SET NOCOUNT ON
DECLARE @Rowcount INT
DECLARE @Err INT

BEGIN TRY
------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

    EXEC @err = dbo.ValidateParameter_DealerID @DealerID

    EXEC @err = Carfax.ValidateParameter_VehicleEntityTypeID @VehicleEntityTypeID

    IF ( @VehicleEntityTypeID <> 1 ) 
        BEGIN
            RAISERROR ( 50119, 16, 1, @VehicleEntityTypeID )
        END

    DECLARE @CarfaxUserName VARCHAR(80)
    SET @CarfaxUserName = 'CarfaxSystemUserAdmin'

    DECLARE @CarfaxAdmin INT

    EXEC @err = dbo.ValidateParameter_MemberID#UserName @CarfaxUserName,
        @CarfaxAdmin OUTPUT

    BEGIN TRANSACTION
	--- Perform only if AutoPurchase is on --
    IF EXISTS ( SELECT  1
                FROM    Carfax.ReportPreference AG
                        JOIN Carfax.Account_Dealer AD ON AG.DealerID = AD.DealerID
                        JOIN Carfax.Account A ON AD.AccountID = A.AccountID
                WHERE   AG.DealerID = @DealerID
                        AND A.AccountStatusID IN ( 1, 2 )
                        AND AG.VehicleEntityTypeID = @VehicleEntityTypeID
                        AND AG.AutoPurchase = 1 ) 
        BEGIN
		------------------------------------------------------------------------------------------------
		-- Declare and set Default variables
		------------------------------------------------------------------------------------------------
		
            DECLARE @WorkTypeIDRenew TINYINT
            DECLARE @WorkPendingStatusID TINYINT
            DECLARE @ProcessAfterDate DATETIME
            SET @WorkTypeIDRenew = 3-- Upgrade Report
            SET @WorkPendingStatusID = 1
            SET @ProcessAfterDate = GETDATE()
		
            CREATE TABLE #UpgradedReport
                (
                  VIN CHAR(17) NOT NULL,
                  DealerID INT NULL,
                  VehicleID INT NULL,
                  ReportType CHAR(3) NULL,
                  ReportProcessorCommandID INT,
                )
		
            INSERT  INTO #UpgradedReport
                    (
                      VIN,
                      DealerID,
                      VehicleID,
                      ReportType,
                      ReportProcessorCommandID
                    )
                    SELECT  V.VIN,
                            VD.DealerID,
                            CFV.VehicleID,
                            RQ.ReportType,
                            RPC.ReportProcessorCommandID
                    FROM    dbo.Inventory I
                            INNER JOIN dbo.Vehicle V ON I.VehicleID = V.VehicleID
                            INNER JOIN Carfax.Vehicle CFV ON V.VIN = CFV.VIN
                            JOIN Carfax.Vehicle_Dealer VD ON CFV.VehicleID = VD.VehicleID
                                                             AND VD.DealerID = I.BusinessUnitID
                            JOIN Carfax.Report RPT ON VD.RequestID = RPT.RequestID
                            JOIN Carfax.Request RQ ON RPT.RequestID = RQ.RequestID
                            LEFT JOIN Carfax.ReportProcessorCommand RPC ON CFV.VehicleID = RPC.VehicleID
                                                                           AND RPC.DealerID = VD.DealerID
                                                                           AND RPC.ReportProcessorCommandStatusID = @WorkPendingStatusID
                                                                           AND RPC.ReportProcessorCommandTypeID = @WorkTypeIDRenew
                    WHERE   I.InventoryActive = 1
                            AND I.InventoryType = 2
                            AND I.BusinessUnitID = @DealerID
                            AND RPT.ExpirationDate >= GETDATE()
                            AND ( ( @NewReportType = 'CIP'
                                    AND ( RQ.ReportType = 'VHR'
                                          OR RQ.ReportType = 'BTC'
                                        )
                                  )
                                  OR ( @NewReportType = 'VHR'
                                       AND RQ.ReportType = 'BTC'
                                     )
                                )
            SELECT  *
            FROM    #UpgradedReport
 
            UPDATE  RPC
            SET     ProcessAfterDate = @ProcessAfterDate,
                    UpdateUser = @CarfaxAdmin,
                    UpdateDate = GETDATE()
            FROM    Carfax.ReportProcessorCommand RPC
                    JOIN #UpgradedReport NR ON RPC.DealerID = NR.DealerID
                                               AND RPC.VehicleID = NR.VehicleID
                                               AND RPC.ReportProcessorCommandID = NR.ReportProcessorCommandID
			 			    

            INSERT  INTO Carfax.ReportProcessorCommand
                    (
                      DealerID,
                      VehicleID,
                      VehicleEntityTypeID,
                      ReportProcessorCommandTypeId,
                      ReportProcessorCommandStatusID,
                      ProcessAfterDate,
                      InsertUser,
                      InsertDate
                    )
                    SELECT  @DealerID,
                            VehicleID,
                            @VehicleEntityTypeID,
                            @WorkTypeIDRenew,
                            @WorkPendingStatusID,
                            @ProcessAfterDate,
                            @CarfaxAdmin,
                            GETDATE()
                    FROM    #UpgradedReport
                    WHERE   ReportProcessorCommandID IS NULL
		
            END


------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------
    COMMIT TRANSACTION
    RETURN 0
END TRY



--exit the procedure
BEGIN CATCH

    IF OBJECT_ID('tempdb..#Inventory') IS NOT NULL 
        DROP TABLE #Inventory
    DECLARE @ErrNum INT
    DECLARE @ErrorMessage VARCHAR(2000)
    SET @ErrorMessage = ERROR_MESSAGE()  
    SET @ErrNum = ERROR_NUMBER() 

    IF ( XACT_STATE() = 1
         OR XACT_STATE() = -1
       ) 
        ROLLBACK TRANSACTION
	
    RAISERROR ( @ErrorMessage, 16, 1 )
	 
    RETURN @ErrNum

END CATCH


GO

