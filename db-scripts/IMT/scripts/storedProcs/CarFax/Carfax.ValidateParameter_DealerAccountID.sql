SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.ValidateParameter_DealerAccountID') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Carfax.ValidateParameter_DealerAccountID AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.ValidateParameter_DealerAccountID to [CarfaxUser]
GO

ALTER PROCEDURE [Carfax].[ValidateParameter_DealerAccountID]
	@DealerID INT
AS
/* --------------------------------------------------------------------
 * 
 * $Id: Carfax.ValidateParameter_DealerAccountID.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns 1 if the Dealer Exists. 
 * 
 * Parameters
 * ----------
 *
 * @DealerID   - ID that maps to a BusinessUnitID of Type 4 (Dealer) in the IMT..BusinessUnit table.
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @DealerID is null
 * 50106 - @DealerID does not exist.
 * 50106 - @DealerID does not exist in Dealer_Account.
 * 50200 - Expected one Row
 *
 * History
 * -------
 * MAK	05/26/2009	Create procedure.

 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

EXEC @err = dbo.ValidateParameter_DealerID @DealerID 
IF (@err <> 0) GOTO Failed

 ------------------------------------------------------------------------------------------------
-- Populate Result Set
------------------------------------------------------------------------------------------------
DECLARE @ADCount TINYINT

SELECT @ADCount = COUNT(*)
FROM	dbo.BusinessUnit B
JOIN	Carfax.Account_Dealer AD
ON	B.BusinessUnitID =AD.DealerID
WHERE	BusinessUnitID =@DealerID
AND	BusinessUnitTypeID =4 -- Type Dealer
 
	
IF (@ADCount <> 1)
BEGIN
	RAISERROR (50106,16,1,'DealerID')
	RETURN @@ERROR
END

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF (@err <> 0) GOTO Failed

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO