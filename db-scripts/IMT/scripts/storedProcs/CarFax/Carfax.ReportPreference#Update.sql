IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.ReportPreference#Update') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Carfax.ReportPreference#Update AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.ReportPreference#Update to [CarfaxUser]
GO

ALTER PROCEDURE [Carfax].[ReportPreference#Update] 
	@DealerID INT,
	@VehicleEntityTypeID INT,
	@DisplayInHotList BIT,
	@PurchaseReport BIT,
	@ReportType CHAR(3),
	@Version BINARY(8),
	@UpdateUser VARCHAR(80),
	@NewVersion BINARY(8) OUTPUT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Carfax.ReportPreference#Update.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 *	Update Values by Dealer\VehicleEntityType into the Carfax.ReportPreference table
 *
 * Exceptions
 * ----------
 * 
 *	50100 DealerId IS NULL
 *	50106 Record with DealerId does not exist
 *	50100 VehicleEntityTypeId IS NULL
 *	50106 Record with VehicleEntityTypeId does not exist
 *	50100 DisplayInHotList IS NULL
 *	50100 PurchaseReport IS NULL
 *	50100 ReportType IS NULL
 *	50106 Record with ReportType does not exist
 *	50100 InsertUser IS NULL
 *	50106 Record with UpdateUser does not exist
 *	50106 Report preference with PK (DealerId, VehicleEntityTypeId) does not exists.
 *	50115 Report preference with PK (DealerId, VehicleEntityTypeId) has different row-version (i.e. concurrent update).
 *	50200 Expected to update one row (but updated 0 or > 1)
 *
 *
 *	Returns:
 *		Version (OUTPUT Parameter).
 *
 *
 * History
 * ----------
 * 
 * MAK	05/27/2009	Create procedure.
 * ffo	2009.06.11	added transactions
 * MAK	06/25/2009	When cancelling RPC commands, there was an erroneous
 *			check to ensure only one row was updated.  Many rows can be updated.
 *
 * MAK	06/29/2009	Switched FOO transaction style to default style.
 * -------------------------------------------------------------------- */

--DEFAULT SETTINGS
SET NOCOUNT ON
DECLARE @Rowcount INT
DECLARE	@Err INT
DECLARE	@sErrMsg VARCHAR(255)


BEGIN TRY
------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

EXEC @err =dbo.ValidateParameter_DealerID @DealerID

DECLARE @UpdateUserID INT
IF (@err=0) EXEC @err =dbo.ValidateParameter_MemberID#UserName  @UpdateUser, @UpdateUserID OUTPUT

IF (@err=0) EXEC @err =Carfax.ValidateParameter_VehicleEntityTypeID @VehicleEntityTypeID


IF (@err=0) EXEC @err =Carfax.ValidateParameter_ReportType @ReportType


IF (@DisplayInHotList is null )
	BEGIN
		RAISERROR (50100,16,1,'DisplayInHotList')
	END

IF (@PurchaseReport is null )
	BEGIN
		RAISERROR (50100,16,1,'Purchase Report')
	 END



DECLARE @OldVersionID BINARY(8)
DECLARE @OldAutoPurchase BIT
DECLARE @OldReportType CHAR(3)

SELECT @OldVersionID =Version,
	@OldAutoPurchase =AutoPurchase,
	@OldReportType =ReportType
FROM	Carfax.ReportPreference
WHERE	DealerID=@DealerId
AND	VehicleEntityTypeID =@VehicleEntityTypeId
 
IF (@OldVersionID is null AND @err=0)
	BEGIN
		RAISERROR (50106,16,1,'DealerID\VehicleEntityTypeID')
	END

IF (@OldVersionID <> @Version AND @err=0)
	BEGIN
		RAISERROR (50115,16,1,'Carfax.ReportPreferences')
	END

BEGIN TRANSACTION
------------------------------------------------------------------------------------------------
-- Perform Update
------------------------------------------------------------------------------------------------

	UPDATE	RP
	SET 	DisplayInHotList =@DisplayInHotList,
		AutoPurchase =@PurchaseReport,
		ReportType =@ReportType,
		UpdateUser =@UpdateUserID,
		UpdateDate =GETDATE()
	FROM	Carfax.ReportPreference RP
	WHERE	DealerID =@DealerId
		AND	VehicleEntityTypeID =@VehicleEntityTypeId

	
	IF (@RowCount <> 1)
	BEGIN
			RAISERROR (50200,16,1,@RowCount)
	END

	DECLARE @AutoPurchaseChanged BIT
	DECLARE @ReportTypeChanged BIT

	IF (@OldAutoPurchase =0 AND @PurchaseReport=1)
		SET @AutoPurchaseChanged=1

	IF(	   (@OldReportType ='BTC' AND @ReportType ='VHR')
		OR (@OldReportType ='BTC' AND @ReportType ='CIP')
		OR (@OldReportType ='VHR' AND @ReportType ='CIP'))
		SET @ReportTypeChanged =1

	IF (@VehicleEntityTypeID = 1 AND @AutoPurchaseChanged =1) 
	BEGIN
		EXEC Carfax.ReportProcessorCommand#CreateNewAutoPurchaseInventory @DealerId
	END

	IF (@VehicleEntityTypeID = 1 AND @ReportTypeChanged =1)
	BEGIN
		EXEC Carfax.ReportProcessorCommand#CreateNewReportUpgradeWork @DealerId, @VehicleEntityTypeId,@ReportType
	END

	IF (@OldAutoPurchase =1 AND @PurchaseReport =0)
	BEGIN
		UPDATE	Carfax.ReportProcessorCommand
		SET	ReportProcessorCommandStatusID = Cancelled.ReportProcessorCommandStatusID
		FROM	Carfax.ReportProcessorCommand rpc
			INNER JOIN Carfax.ReportProcessorCommandStatus rpcs on rpcs.ReportProcessorCommandStatusId = rpc.ReportProcessorCommandStatusId
			INNER JOIN Carfax.ReportProcessorCommandStatus Cancelled on Cancelled.Name = 'Cancelled'
		WHERE	rpc.DealerID =@DealerID
			AND VehicleEntityTypeID =@VehicleEntityTypeID
		AND rpcs.Name = 'Ready'
			
	END

	SELECT	@NewVersion= Version 
	FROM	Carfax.ReportPreference
	WHERE	DealerID=@DealerId
	AND	VehicleEntityTypeID =@VehicleEntityTypeId

	SELECT @RowCount = @@ROWCOUNT, @err = @@ERROR
 
	IF (@RowCount <> 1 AND @err=0)
	BEGIN
		RAISERROR (50200,16,1,@RowCount)
	END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

	COMMIT TRANSACTION
	RETURN 0
END TRY
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

BEGIN CATCH
	 
	DECLARE @ErrNum INT
	DECLARE @ErrorMessage VARCHAR(2000)
	SET @ErrorMessage =ERROR_MESSAGE()  
	SET @ErrNum =ERROR_NUMBER() 

	PRINT @ErrNum
	IF (XACT_STATE() =1 OR XACT_STATE()=-1)
	 ROLLBACK TRANSACTION
	
	RAISERROR(@ErrorMessage, 16, 1)
	 
	RETURN @ErrNum

END CATCH

GO
