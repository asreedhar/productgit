IF NOT EXISTS ( SELECT  *
                FROM    sys.objects
                WHERE   object_id = OBJECT_ID(N'Carfax.ReportProcessorCommand#CreateNewAutoPurchaseInventory')
                        AND type IN (N'P', N'PC') ) 
        EXECUTE('CREATE PROCEDURE  Carfax.ReportProcessorCommand#CreateNewAutoPurchaseInventory AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION ON Carfax.ReportProcessorCommand#CreateNewAutoPurchaseInventory TO [CarfaxUser]
GO

ALTER PROCEDURE [Carfax].[ReportProcessorCommand#CreateNewAutoPurchaseInventory] (@DealerId INT)
AS 
/* --------------------------------------------------------------------------------------------
 * 
 * 
 * Summary
 * -------
 * 
 *	Update ReportProcessorCommand table when the ReportPreference of the
 *	AutoPurchase Status goes from False to True.
 *
 * Exceptions
 * ----------
 * 
 *	50100 DealerId IS NULL
 *	50106 Record with DealerId does not exist
 *	50100 VehicleEntityTypeId IS NULL
 *	50106 Record with VehicleEntityTypeId does not exist
 *	
 *
 *	Returns:
 *
 *
 * History
 * ----------
 * 
 * MAK	05/29/2009	Create procedure.
 * MAK	06/01/2009	Separate out AutoPurchase from ReportType change.
 * MAK	06/10/2009	Updated name changes.
 * ffo	2009.06.11	added transaction	
 * MAK	06/18/2009	The transactions were causing the procedure to hang.
 *			I rewrote it in the Try\Catch format		
 * MAK	07/20/2009	Removed  the ability to do all dealers (dealerID is Null)
 * 			and do not include pieces of work in the queue (DealerID,VehicleID)
 *			that are listed as 'Ready' in the queue.
 * WGH	12/29/2009	Update #Inventory insert to prevent duplicate VINs
 *			Formatting and error handling updates
 * MAK	11/01/2010	If a pending request exists, update the ProcessAfter and Update dates,
 *			Insert new commands only if there is not a pending request in the queue.
 * WGH	04/02/2011	Added filter to prevent VINs w/length <> 17 
 * --------------------------------------------------------------------------------------------- */

--DEFAULT SETTINGS

SET NOCOUNT ON
DECLARE @Rowcount INT
DECLARE @Err INT

------------------------------------------------------------------------------------------------
-- Declare and set Default variables
------------------------------------------------------------------------------------------------

DECLARE @CarfaxAdmin INT

SELECT  @CarfaxAdmin = MemberID
FROM    dbo.Member
WHERE   Login = 'CarfaxSystemUserAdmin'

BEGIN TRY
	------------------------------------------------------------------------------------------------
	-- Validate Parameters
	------------------------------------------------------------------------------------------------

	EXEC @err = dbo.ValidateParameter_DealerID @DealerID

	IF @err <> 0 
		RAISERROR (50106,16,1,'DealerID')


	IF (@CarfaxAdmin IS NULL) 
		RAISERROR (50106,16,1,'CarfaxAdmin')


	IF (NOT EXISTS ( SELECT 1
			 FROM   Carfax.ReportPreference RP
			 WHERE  DealerID = @DealerID
				AND VehicleEntityTypeID = 1
				AND AutoPurchase = 1 )
		) 
		RAISERROR (50106,16,1,'AutoPurchase Report Prefererence')

        BEGIN TRANSACTION
        
    DECLARE @WorkTypeIDPurchase TINYINT
	DECLARE @WorkStatusID TINYINT
    DECLARE @ProcessAfterDate DATETIME

    SET @WorkTypeIDPurchase = 1 --- This is = to 'PurchaseDefaultReport'
    SET @WorkStatusID = 1
    SET @ProcessAfterDate = GETDATE()

	CREATE TABLE #NewReports
	(DealerID   INT,
	VehicleID   INT,
	ReportProcessorCommandID INT)
	
        CREATE TABLE #Inventory (
                 VIN CHAR(17) NOT NULL,
                 DealerID INT NOT NULL,
                 ExistingVehicleID INT NULL,
                 NewVehicleID INT NULL,
                 ReportType CHAR(3) NULL
                );
	
	--------------------------------------------------------------------------------
	--	ONLY INCLUDE VINs WHERE THERE IS ONLY ONE DISTINCT VIN PER DEALER
	--------------------------------------------------------------------------------
	
	WITH Vehicles (VIN, DealerID)
	AS
	(
	SELECT	V.VIN,
		I.BusinessUnitID AS DealerID
	FROM	dbo.Inventory I
		INNER JOIN dbo.Vehicle V ON I.VehicleID = V.VehicleID
	WHERE	I.InventoryActive = 1
		AND I.InventoryType = 2
		AND I.BusinessUnitID = @DealerId
		AND LEN(V.VIN) = 17
	GROUP	
	BY	V.VIN, I.BusinessUnitID
	HAVING COUNT(*) = 1					
	)
	
        INSERT  
        INTO	#Inventory (DealerID, VIN, ExistingVehicleID)
	
	SELECT  V.DealerID,
                V.VIN,
                CFV.VehicleID
        FROM	Vehicles V
                LEFT JOIN Carfax.Vehicle CFV ON V.VIN = CFV.VIN
	
	------------------------------------------------------------------------------------------------
	-- Perform Insert
	------------------------------------------------------------------------------------------------

        INSERT  
        INTO 	Carfax.Vehicle
                (VIN,
                 IsHotListEligible,
                 InsertUser,
                 InsertDate
                )
	SELECT  VIN,
        	0,
                @CarfaxAdmin,
                GETDATE()
	FROM    #Inventory I
        WHERE   I.ExistingVehicleID IS NULL


        UPDATE  I
        SET     NewVehicleID = V.VehicleID
        FROM    Carfax.Vehicle V
                INNER JOIN #Inventory I ON I.VIN = V.VIN	 
        WHERE   I.ExistingVehicleID IS NULL
        
        
        INSERT INTO #NewReports
                ( DealerID ,
                  VehicleID ,
                  ReportProcessorCommandID)
                SELECT  @DealerID,
                    COALESCE(i.ExistingVehicleID, i.NewVehicleID) ,
                    RPC.ReportProcessorCommandID
            FROM    #Inventory I
                    INNER JOIN Carfax.Vehicle V ON I.VIN = V.VIN
                    LEFT JOIN ( SELECT  VD.VehicleID,
                                        VD.DealerID,
                                        RP.RequestID,
                                        RP.ExpirationDate
                                FROM    Carfax.Vehicle_Dealer VD
                                INNER JOIN Carfax.Request RQ ON VD.RequestID=RQ.RequestID
                                        INNER JOIN Carfax.Report RP ON VD.RequestID = RP.RequestID
                                WHERE   VD.DealerID = @DealerID 
                              ) RX ON I.ExistingVehicleID =RX.VehicleID
			LEFT JOIN Carfax.ReportProcessorCommand RPC ON V.VehicleID = RPC.VehicleID
                                                                   AND I.DealerID = RPC.DealerID
								    AND ReportProcessorCommandTypeID = 1
                                                                   AND RPC.DealerID = @DealerID
                                                                   AND ReportProcessorCommandStatusID=@WorkStatusID
                                                               
            WHERE   ( RX.ExpirationDate IS NULL
                      OR RX.ExpirationDate <= GETDATE()
                    )
			 
	

 
			UPDATE	RPC
			SET		ProcessAfterDate =@ProcessAfterDate,
					UpdateUser =@CarfaxAdmin,
					UpdateDate = GETDATE()
			FROM    Carfax.ReportProcessorCommand RPC
			JOIN    #NewReports NR ON RPC.DealerID=NR.DealerID AND RPC.VehicleID = NR.VehicleID AND RPC.ReportProcessorCommandID = NR.ReportProcessorCommandID
			 			    

   INSERT  INTO Carfax.ReportProcessorCommand
                (DealerID,
                 VehicleID,
                 VehicleEntityTypeID,
                 ReportProcessorCommandTypeId,
                 ReportProcessorCommandStatusID,
                 ProcessAfterDate,
                 InsertUser,
                 InsertDate
                )
	SELECT  	@DealerID,
		 		VehicleID,
                1,
                @WorkTypeIDPurchase,
                @WorkStatusID,
                @ProcessAfterDate,
                @CarfaxAdmin,
                GETDATE()
        FROM    #NewReports
        WHERE	ReportProcessorCommandID IS NULL
	------------------------------------------------------------------------------------------------
	-- Success
	------------------------------------------------------------------------------------------------
 
        COMMIT TRANSACTION

END TRY

BEGIN CATCH

        IF (XACT_STATE() = 1
            OR XACT_STATE() = -1
           ) 
                ROLLBACK TRANSACTION
	
	EXEC dbo.sp_ErrorHandler	
	 
        RETURN ERROR_NUMBER() 

END CATCH


GO