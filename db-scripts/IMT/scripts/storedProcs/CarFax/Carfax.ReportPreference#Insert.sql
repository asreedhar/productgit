IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.ReportPreference#Insert') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Carfax.ReportPreference#Insert AS SELECT 1')
GO


GRANT EXECUTE, VIEW DEFINITION on Carfax.ReportPreference#Insert to [CarfaxUser]
GO

 ALTER PROCEDURE [Carfax].[ReportPreference#Insert]
	@DealerId INT,
	@VehicleEntityTypeId INT,
	@DisplayInHotLIst BIT,
	@PurchaseReport BIT,
	@ReportType CHAR(3),
	@InsertUser VARCHAR(80),
	@NewVersion BINARY(8) OUTPUT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Carfax.ReportPreference#Insert.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 *	Inserts Values by Dealer\VehicleEntityType into the Carfax.ReportPreference table
 *
 * Exceptions
 * ----------
 * 
 *	50100 DealerId IS NULL
 *	50106 Record with DealerId does not exist
 *	50100 VehicleEntityTypeId IS NULL
 *	50106 Record with VehicleEntityTypeId does not exist
 *	50100 DisplayInHotList IS NULL
 *	50100 PurchaseReport IS NULL
 *	50100 ReportType IS NULL
 *	50106 Record with ReportType does not exist
 *	50100 InsertUser IS NULL
 *	50106 Record with InsertUser does not exist
 *	50113 Report preference with PK (DealerId, VehicleEntityTypeId) already exists.
 *
 *
 *	Returns:
 *		Version (OUTPUT Parameter).
 *
 *
 * History
 * ----------
 * 
 * MAK	05/27/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

EXEC  @err =dbo.ValidateParameter_DealerID @DealerId
IF @err <> 0 GOTO Failed

EXEC  @err =Carfax.ValidateParameter_VehicleEntityTypeID @VehicleEntityTypeID
IF @err <> 0 GOTO Failed

IF (@DisplayInHotList  is null)
BEGIN
	RAISERROR (50200,16,1,'DisplayInHotList')
	RETURN @@ERROR
END

IF (@PurchaseReport  is null)
BEGIN
	RAISERROR (50200,16,1,'Purchase Report')
	RETURN @@ERROR
END

EXEC  @err =Carfax.ValidateParameter_ReportType @ReportType
IF @err <> 0 GOTO Failed


DECLARE @InsertUserID INT

EXEC  @err =dbo.ValidateParameter_MemberID#UserName @InsertUser, @InsertUserID OUTPUT
IF @err <> 0 GOTO Failed

IF EXISTS (SELECT 1
	FROM 	Carfax.ReportPreference
	WHERE	DealerID =@DealerID
	AND	VehicleEntityTypeID =@VehicleEntityTypeID)
BEGIN
	RAISERROR (50113,16,1,'ReportPreference')
	RETURN @@ERROR
END	


INSERT
INTO	Carfax.ReportPreference
	(DealerID,
	VehicleEntityTypeID,
	AutoPurchase,
	DisplayInHotList,
	ReportType,
	InsertUser,
	InsertDate)
VALUES	(@DealerId,
	@VehicleEntityTypeId,
	@PurchaseReport,
	@DisplayInHotList,
	@ReportType,
	@InsertUserID,
	GETDATE())

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF (@err <> 0) GOTO Failed

IF (@rc <>1)
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

IF @PurchaseReport=1 and @VehicleEntityTypeID =1
EXEC Carfax.ReportProcessorCommand#CreateNewAutoPurchaseInventory @DealerId

SELECT @NewVersion =Version
FROM	Carfax.ReportPreference
WHERE	DealerID =@DealerID
AND	VehicleEntityTypeID =@VehicleEntityTypeID

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

Return 0
	
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO