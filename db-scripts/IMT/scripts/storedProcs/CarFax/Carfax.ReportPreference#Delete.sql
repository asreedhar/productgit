IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.ReportPreference#Delete') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Carfax.ReportPreference#Delete AS SELECT 1')
GO


GRANT EXECUTE, VIEW DEFINITION on Carfax.ReportPreference#Delete to [CarfaxUser]
GO

ALTER PROCEDURE [Carfax].[ReportPreference#Delete] 
	@DealerId INT,
	@VehicleEntityTypeId INT

AS

/* --------------------------------------------------------------------
 * 
 * $Id: Carfax.ReportPreference#Delete.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 *	Delete Values by Dealer\VehicleEntityType into the Carfax.ReportPreference table
 *
 * Exceptions
 * ----------
 * 
 *	50100 DealerId IS NULL
 *	50106 Record with DealerId does not exist
 *	50100 VehicleEntityTypeId IS NULL
 *	50106 Record with VehicleEntityTypeId does not exist
 *	50200 Expected to delete one row (but deleted 0 or > 1)
 *
 *
 *	Returns:
 *		None
 *
 *
 * History
 * ----------
 * 
 * MAK	05/27/2009	Create procedure.
 * MAK	06/10/2009	Update Carfax.ReportProcessorCommand with 'Cancelled' status.
 * ffo	2009.06.11	Added transaction	
 * MAK	06/29/2009	Updated transactions to consistent format.
 * -------------------------------------------------------------------- */

--DEFAULT SETTINGS
SET NOCOUNT ON
DECLARE @Rowcount INT
DECLARE	@Err INT
 


BEGIN TRY
------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

EXEC @err =dbo.ValidateParameter_DealerID @DealerId

IF (@err =0) EXEC @err =Carfax.ValidateParameter_VehicleEntityTypeID  @VehicleEntityTypeID

BEGIN TRANSACTION
------------------------------------------------------------------------------------------------
-- Perform Delete
------------------------------------------------------------------------------------------------
	DELETE Carfax.ReportPreference 
	WHERE	DealerID =@DealerId
		AND VehicleEntityTypeID =@VehicleEntityTypeId

	SELECT @RowCount = @@ROWCOUNT, @err = @@ERROR

	IF (@RowCount <>1)
	BEGIN
		RAISERROR (50200,16,1,@RowCount)
	
	END

	UPDATE	Carfax.ReportProcessorCommand
	SET	ReportProcessorCommandStatusID = Cancelled.ReportProcessorCommandStatusID
	FROM Carfax.ReportProcessorCommand rpc
		INNER JOIN Carfax.ReportProcessorCommandStatus rpcs on rpcs.ReportProcessorCommandStatusId = rpc.ReportProcessorCommandStatusId
		INNER JOIN Carfax.ReportProcessorCommandStatus Cancelled on Cancelled.Name = 'Cancelled'
	WHERE	rpc.DealerID =@DealerID
		AND VehicleEntityTypeID =@VehicleEntityTypeID
		AND rpcs.Name = 'Ready'

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

	COMMIT TRANSACTION
	RETURN 0
END TRY
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

BEGIN CATCH

	 
	DECLARE @ErrNum INT
	DECLARE @ErrorMessage VARCHAR(2000)
	SET @ErrorMessage =ERROR_MESSAGE()  
	SET @ErrNum =ERROR_NUMBER() 

	PRINT @ErrNum
	IF (XACT_STATE() =1 OR XACT_STATE()=-1)
	 ROLLBACK TRANSACTION
	
	RAISERROR(@ErrorMessage, 16, 1)
	 
	RETURN @ErrNum

END CATCH

