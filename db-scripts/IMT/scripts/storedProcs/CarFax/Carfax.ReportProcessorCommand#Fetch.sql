IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.ReportProcessorCommand#Fetch') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Carfax.ReportProcessorCommand#Fetch AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.ReportProcessorCommand#Fetch to [CarfaxUser]
GO

ALTER PROCEDURE [Carfax].[ReportProcessorCommand#Fetch] 
	AS
/* --------------------------------------------------------------------
 * 
 * 
 * Summary
 * -------
 * 
 *	Return the next command for the processor to execute.  The command types are:
 *
 *	0 = Undefined
 *	1 = PurchaseReport
 *	2 = RenewReport
 *	3 = UpgradeReport
 *	4 = ChangeHotListStatus
 *	5 = Data Migration
 *
 * Exceptions
 * ----------
 * 
 *	50200 Expect one row in 1st result set.
 *	
 *
 *	Returns:Result Set 1: Report Command Type
 * 		CommandType INT NOT NULL
 * 		Delay 	INT NOT NULL
 * 		Id 	INT NOT NULL
 *
 *
 * History
 * ----------
 * 
 * MAK	06/01/2009	Create procedure.
 * MAK	07/28/2009	Added CommandType of 6.	
 * MAK	09/16/2009	Raise an error if the @ReportCommand table is empty for a command type that is not type 2.
 * MAK	10/15/2009 	Added logic to flip hot list values.						
 * MAK	12/08/2009	Mark row as failure  in catch.
 * MAK	03/26/2010	FB 9668  Reactiveated inventory to have owner preference.
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON
DECLARE @rc INT, @err INT


DECLARE @ReportCommandType TABLE
	(CommandType	INT NOT NULL,
	Delay		INT NOT NULL,
	Id		INT NOT NULL,
	UserName	VARCHAR(50))
	
DECLARE @ReportCommand TABLE
	(DealerId		INT 	NOT NULL,
	VIN			CHAR(17) NOT NULL,
	TrackingCode		CHAR(3) NOT NULL,
	ReportType		CHAR(3)	NULL,
	DisplayInHotList 	BIT	NOT NULL)	
	
DECLARE @HasDelay SMALLINT
DECLARE @NoDelay SMALLINT
DECLARE @ReadyStatusID TINYINT
DECLARE @InProcessStatusID TINYINT
DECLARE @TrackingCode  CHAR(3)
DECLARE @ReportCommandTypeID TINYINT
DECLARE @ReportProcessorCommandID INT
DECLARE @ErrorStatusID	TINYINT

SET @TrackingCode ='FLA'
SET @ReadyStatusID =1
SET @InProcessStatusID =2
SET @ErrorStatusID =4

SET @HasDelay =60
SET @NoDelay =0

BEGIN TRY

		SELECT	@ReportProcessorCommandID =  MIN(ReportProcessorCommandID)
		FROM	Carfax.ReportProcessorCommand RPC
		JOIN	Carfax.Account_Dealer AD
		ON		RPC.DealerID =AD.DealerID
		JOIN	Carfax.Account A
		ON		AD.AccountID =A.AccountID
		WHERE	ReportProcessorCommandStatusID = @ReadyStatusID
		AND		A.AccountStatusID in (1,2) -- Executes for 'New' and 'Okay' Accounts.	

		INSERT
		INTO	@ReportCommandType
				(CommandType,
				Delay,
				Id,
				UserName)
		VALUES	(0,
				@HasDelay,
				0,
				'CarfaxSystemUserAdmin')

		UPDATE @ReportCommandType
		SET		CommandType =ReportProcessorCommandTypeID,
				Delay =@NoDelay,
				Id =ReportProcessorCommandID
		FROM	Carfax.ReportProcessorCommand RPC
		WHERE	ReportProcessorCommandID =@ReportProcessorCommandID

		SELECT @rc = @@ROWCOUNT, @err = @@ERROR
		 
		IF (@rc  >1)
		
		BEGIN
			RAISERROR (50200,16,1,@rc)
			RETURN @@ERROR
		END

	SELECT 	@ReportCommandTypeID=MAX(CommandType)
	FROM	@ReportCommandType

	If (@rc = 1)
	BEGIN
		IF (@ReportCommandTypeID=5 OR @ReportCommandTypeID=6) --Special Conditions for migrated tables.

		BEGIN

		INSERT
		INTO	 @ReportCommand 
				(DealerId,
				VIN,
				TrackingCode,
				ReportType,
				DisplayInHotList)
		SELECT 	RPC.DealerID,
				V.VIN,
				@TrackingCode,
				DM.ReportType,
				DM.IsHotListed
		FROM	@ReportCommandType RCT
		JOIN	Carfax.ReportProcessorCommand RPC ON	RCT.ID =RPC.ReportProcessorCommandID
		JOIN	Carfax.Vehicle V ON	RPC.VehicleID =V.VehicleID
		JOIN	Carfax.ReportProcessorCommand_DataMigration DM ON RCT.Id =DM.ReportProcessorCommandID --SPECIAL TABLE FOR DATA MIGRATED --
		
		SELECT @rc = @@ROWCOUNT, @err = @@ERROR

		IF (@rc  >1 OR (@rc=0 AND @ReportCommandTypeID<>2))
		BEGIN
			RAISERROR (50200,16,1,@rc)
			RETURN @@ERROR
		END

		UPDATE  RPC
		SET	ReportProcessorCommandStatusID =@InProcessStatusID
		FROM	@ReportCommandType RCT
		JOIN	Carfax.ReportProcessorCommand RPC ON RCT.ID =RPC.ReportProcessorCommandID

		SELECT @rc = @@ROWCOUNT, @err = @@ERROR
		
		IF (@rc  >1)
			BEGIN
				RAISERROR (50200,16,1,@rc)
				RETURN @@ERROR
			END
	END

	ELSE
	 	BEGIN
		

		-- Calculate HotList Value based on Command Type.--
		DECLARE @HotListValue BIT	
		
		SELECT	@HotListValue = CASE WHEN 	@ReportCommandTypeID =1  THEN	RP.DisplayInHotList --NewPurchase
									 WHEN	@ReportCommandTypeID IN (2,3) THEN VD.IsHotListEnabled --(Renew or Upgrade)
									 WHEN	@ReportCommandTypeID =4 AND VD.IsHotListEnabled = 1 THEN 0 -- Flip Off
									 WHEN	@ReportCommandTypeID =4 AND VD.IsHotListEnabled = 0 THEN 1 
								END
	
		FROM	@ReportCommandType RCT
		JOIN	Carfax.ReportProcessorCommand RPC ON RCT.ID =RPC.ReportProcessorCommandID
		LEFT JOIN	Carfax.Vehicle_Dealer VD ON RPC.VehicleID =VD.VehicleID AND RPC.DealerID =VD.DealerID
		LEFT JOIN	Carfax.ReportPreference RP ON	RPC.DealerID =RP.DealerID AND	RPC.VehicleEntityTypeID =RP.VehicleEntityTypeID
	
			
		IF (@HotListValue IS NULL)
 
			BEGIN
				RAISERROR ('HotList Value cannot be NULL.',16,1,@rc)
				RETURN @@ERROR
			END
 
			
		INSERT
		INTO	 @ReportCommand 
				(DealerId,
				VIN,
				TrackingCode,
				ReportType,
				DisplayInHotList)
		SELECT 	RPC.DealerID,
				V.VIN,
				@TrackingCode,
				RP.ReportType,
				@HotListValue
		FROM	@ReportCommandType RCT
		JOIN	Carfax.ReportProcessorCommand RPC ON	RCT.ID =RPC.ReportProcessorCommandID
		JOIN	Carfax.Vehicle V ON RPC.VehicleID =V.VehicleID
		LEFT JOIN	Carfax.ReportPreference RP ON	RPC.DealerID =RP.DealerID
		AND		RPC.VehicleEntityTypeID =RP.VehicleEntityTypeID
		LEFT
		JOIN	Carfax.Vehicle_Dealer VD ON	V.VehicleID =VD.VehicleID AND RPC.DealerID =VD.DealerID

		SELECT @rc = @@ROWCOUNT, @err = @@ERROR
	 
		IF (@rc  >1  OR (@rc=0 AND @ReportCommandTypeID <>2))
		BEGIN
			RAISERROR (50200,16,1,@rc)
			RETURN @@ERROR
		END

		UPDATE  RPC
		SET		ReportProcessorCommandStatusID =@InProcessStatusID
		FROM	@ReportCommandType RCT
		JOIN	Carfax.ReportProcessorCommand RPC ON	RCT.ID =RPC.ReportProcessorCommandID

		SELECT @rc = @@ROWCOUNT, @err = @@ERROR

		IF (@rc  >1)
			BEGIN
				RAISERROR (50200,16,1,@rc)
				RETURN @@ERROR
			END
	END
END
 
-----------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT  CASE WHEN CommandType =5 OR CommandType=6  THEN 1 ELSE	CommandType END as CommandType,
		Delay,
		Id,
		UserName 
FROM	@ReportCommandType

SELECT * FROM @ReportCommand

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

END TRY

BEGIN CATCH

	UPDATE  Carfax.ReportProcessorCommand	
	SET		ReportProcessorCommandStatusID =@ErrorStatusID
	WHERE	ReportProcessorCommandID =	@ReportProcessorCommandID 

	RETURN @err

END CATCH

GO