IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.Report#ResponseCode') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Carfax.Report#ResponseCode AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.Report#ResponseCode to [CarfaxUser]
GO

ALTER PROCEDURE [Carfax].[Report#ResponseCode] 
	(@ReportId INT,
	@ResponseCode INT)
	
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Carfax.Report#ResponseCode.sql,v 1.5 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 *	Insert into the Carfax.Response table.
 *
 * Exceptions
 * ----------
 * 
 *	50100 ReportId is NULL
 *	50106 Request with Id=ReportId does not exist
 *	50100 ResponseCode is NULL
 *	50106 Record with ResponseCode does not exist
 *	50200 Expected to insert one row (but inserted 0 or > 1)
 *
 *
 *
 *	Returns:
 *		No ResultSet returned.
 *
 * History
 * ----------
 * 
 * MAK	05/27/2009	Create procedure.
 * MAK	06/09/2009	Added Update of AccountStatus for erroneous codes.
 * MAK	09/15/2009	Added Update of Vehicle.IsHotListEligible. (FB 7032)
 * 						
 * -------------------------------------------------------------------- */

--DEFAULT SETTINGS
SET NOCOUNT ON
DECLARE @Rowcount INT
DECLARE	@Err INT

BEGIN TRY

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------
	IF (@ReportId IS NULL)
	BEGIN
		RAISERROR (50100,16,1,'ReportId')
		
	END

	IF (NOT EXISTS (SELECT 1
		FROM 	Carfax.Request
		WHERE 	RequestID =@ReportId))
	BEGIN
		RAISERROR (50106,16,1,'ID')
			END


	IF (NOT EXISTS (SELECT 1
			FROM 	Carfax.ResponseCode
			WHERE 	ResponseCode =@ResponseCode))
	BEGIN
		RAISERROR (50106,16,1,'ResponseCode',@ResponseCode)
		
	END

	 -----------------------------------------------------------------------------------------
	-- Perform Insert
	------------------------------------------------------------------------------------------------

	BEGIN TRANSACTION
	
	INSERT
	INTO	Carfax.Response
		(RequestID,
		ResponseCode,
		ReceivedDate)
	VALUES	(@ReportId,
		@ResponseCode,
		GETDATE())

	SELECT @RowCount = @@ROWCOUNT, @err = @@ERROR

	IF (@RowCount <>1)
	BEGIN
		RAISERROR (50200,16,1,'Carfax.Response')
	END
	
	IF (@ResponseCode= 500) -- Response is successful.  Car can be hotlisted.
	BEGIN
		UPDATE V
		SET	IsHotListEligible =1
		FROM	Carfax.Vehicle V
		JOIN	Carfax.Request R ON V.VehicleID =R.VehicleID
		WHERE	R.RequestID=@ReportId
	END
	
	IF (@ResponseCode= 501) -- Response is successsful but car CANNOT be hotlisted.
	
	BEGIN
		UPDATE V
		SET	IsHotListEligible =0
		FROM	Carfax.Vehicle V
		JOIN	Carfax.Request R ON V.VehicleID =R.VehicleID
		WHERE	R.RequestID=@ReportId
	END
	-- 903,904 and 905 are legitimate response codes that map to errors (bad password,
	-- bad username and bad account
	IF (@ResponseCode =903
		OR @ResponseCode =904
		OR @ResponseCode =905)
	BEGIN
		DECLARE @CarfaxAdmin INT

		SELECT @CarfaxAdmin =MemberID
		FROM	dbo.Member 
		WHERE	Login  ='CarfaxSystemUserAdmin'

		IF (@CarfaxAdmin is null)
		BEGIN
			RAISERROR (50106,16,1,'CarfaxAdmin')
		END

		UPDATE A
		SET	AccountStatusID =  CASE WHEN @ResponseCode =903 THEN 5
						WHEN @ResponseCode =904 THEN 3
						WHEN @ResponseCode =905 THEN 4 END,
			UpdateUser = @CarfaxAdmin,
			UpdateDate =GETDATE()
		FROM	Carfax.Account A
			INNER JOIN Carfax.Request RQ ON A.AccountID =RQ.AccountID
		WHERE	RQ.RequestID =@ReportID
				
		SELECT @RowCount = @@ROWCOUNT, @err = @@ERROR
		
		IF (@RowCount <>1)
		BEGIN
			RAISERROR (50200,16,1,'Carfax.Account')
		END

	END


------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

	COMMIT TRANSACTION
	RETURN 0
END TRY
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

BEGIN CATCH

	 
	DECLARE @ErrNum INT
	DECLARE @ErrorMessage VARCHAR(2000)
	SET @ErrorMessage =ERROR_MESSAGE()  
	SET @ErrNum =ERROR_NUMBER() 

	IF (XACT_STATE() =1 OR XACT_STATE()=-1)
	 ROLLBACK TRANSACTION
	
	RAISERROR(@ErrorMessage, 16, 1)
	 
	RETURN @ErrNum

END CATCH

GO