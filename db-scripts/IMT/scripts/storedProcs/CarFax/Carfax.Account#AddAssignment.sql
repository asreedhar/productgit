
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.Account#AddAssignment') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Carfax.Account#AddAssignment AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.Account#AddAssignment to [CarfaxUser]
GO


 ALTER PROCEDURE [Carfax].[Account#AddAssignment]
	@AccountId INT,
	@MemberId INT,
	@InsertUser VARCHAR(80)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Carfax.Account#AddAssignment.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 *	Insert into the Carfax.Account_Member table
 *
* Exceptions
 * ----------
 * 
 *    50100 Account IS NULL
 *    50106 No such account!
 *    50100 MemberID is null
 *    50113 Entry exists in Carfax.Account_Member table
 *
 *
 *    Return Nothing
 *
 * History
 * ----------
 * 
 * MAK	05/26/2009	Create procedure.
 * MAK	05/29/2009	Update Error Messages. 
 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

EXEC  @err =Carfax.ValidateParameter_AccountID @AccountId

IF @err <> 0 GOTO Failed

IF (@MemberID) is null
BEGIN
	RAISERROR (50100,16,1,'MemberId')
	RETURN @@ERROR
END

If (EXISTS (	SELECT 1
		FROM	Carfax.Account_Member
		WHERE	AccountID =@AccountId
		AND	MemberID =@MemberId))
BEGIN
	RAISERROR (50113,16,1,'Account_Member')
	RETURN @@ERROR
END
	
DECLARE @InsertUserID INT

EXEC  @err =dbo.ValidateParameter_MemberID#UserName @InsertUser, @InsertUserID OUTPUT


INSERT
INTO	Carfax.Account_Member
	(AccountID,
	MemberID,
	InsertUser,
	InsertDate)
VALUES (@AccountId,
	@MemberId,
	@InsertUserID,
	GETDATE())
	
SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF (@err <> 0) GOTO Failed

IF (@rc <>1)
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END


------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------	
RETURN 0
	
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO