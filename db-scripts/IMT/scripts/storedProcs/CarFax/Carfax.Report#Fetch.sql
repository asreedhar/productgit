
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.Report#Fetch') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Carfax.Report#Fetch AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.Report#Fetch to [CarfaxUser]
GO

ALTER PROCEDURE [Carfax].[Report#Fetch] 
	@AccountId INT,
	@Vin CHAR(17)

AS

/* --------------------------------------------------------------------
 * 
 * $Id: Carfax.Report#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Request the most recent report from the supplied account for the
 * given VIN.
 * 
 * Parameters
 * -------
 * 
 * AccountId INT
 * VIN CHAR(17)
 * 
 * Exceptions
 * ----------
 * 
 * 50100 AccountId is NULL
 * 50106 Record with AccountId does not exist
 * 50100 VIN is NULL
 * 50114 LEN(VIN) != 17
 * 50200 Expected to insert one row (but inserted 0 or > 1)
 *
 * Result Set
 * ----------
 * 
 * 1)	Id INT NOT NULL
 *	AccountId INT NOT NULL
 *	Vin CHAR(17) NOT NULL
 *	ReportType CHAR(3) NOT NULL
 *	DisplayInHotList BIT NOT NULL
 *	Document XML NOT NULL
 *	ExpirationDate DATETIME NOT NULL
 *	OwnerCount INT NOT NULL
 *	HasProblem BIT NOT NULL
 *
 * 2)	ReportInspectionId INT NOT NULL
 *	Selected INT NOT NULL
 *
 * History
 * ----------
 * 
 * MAK	05/27/2009	Create procedure.
 * MAK	05/29/2009	Update Error Code.
 * MAK	06/24/2009	Per 5821, Use IF... construct to diide SQL						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

EXEC  @err =Carfax.ValidateParameter_AccountID @AccountId
IF (@err <> 0) GOTO Failed

EXEC  @err =dbo.ValidateParameter_VIN @VIN 
IF (@err <> 0) GOTO Failed


DECLARE @VehicleID INT

SELECT	@VehicleID =VehicleID
FROM	Carfax.Vehicle
WHERE	VIN=@Vin

DECLARE @IsAdmin BIT
SET @IsAdmin =0

IF EXISTS (SELECT 1
	FROM	Carfax.Account
	WHERE	AccountID =@AccountID
	AND	AccountTypeID =2) SET @IsAdmin =1

------------------------------------------------------------------------------------------------
-- Result Set
------------------------------------------------------------------------------------------------

DECLARE @Report TABLE (
	Id INT NOT NULL,
	AccountId INT NOT NULL,
	Vin CHAR(17) NOT NULL,
	ReportType CHAR(3) NOT NULL,
	DisplayInHotList BIT NOT NULL,
	Document XML NOT NULL,
	ExpirationDate DATETIME NOT NULL,
	OwnerCount INT NOT NULL,
	HasProblem BIT NOT NULL
)

DECLARE @ReportInspection TABLE (
	ReportInspectionId INT NOT NULL,
	Selected BIT NOT NULL
)

------------------------------------------------------------------------------------------------
-- Populate It
------------------------------------------------------------------------------------------------
IF (@IsAdmin =1)
	BEGIN
	
	INSERT 
	INTO	@Report (
		Id,
		AccountId,
		Vin,	
		ReportType,
		DisplayInHotList,
		Document,
		ExpirationDate,
		OwnerCount,
		HasProblem)
	SELECT	E.RequestID,
		R.AccountId,
		V.VIN,
		R.ReportType,
		CASE
		WHEN V.IsHotListEligible = 1 THEN
			 R.DisplayInHotList 
		ELSE
			0
		END,
		E.Report,
		E.ExpirationDate,
		E.OwnerCount,
		E.HasProblem
	FROM	Carfax.Vehicle V
	JOIN	Carfax.Request R 
	ON	R.VehicleID = V.VehicleID
	AND	R.RequestID = V.RequestID
	JOIN	Carfax.Report E 
	ON	E.RequestID = R.RequestID
	WHERE	R.AccountId = @AccountId
	AND	V.VehicleID = @VehicleID
	SELECT @rc = @@ROWCOUNT, @err = @@ERROR

	IF (@err <> 0) GOTO Failed

	IF (@rc <> 1)
		BEGIN
			RAISERROR (50200,16,1,@rc)
			RETURN @@ERROR
		END
	END
ELSE
	BEGIN
	INSERT 
	INTO	@Report (
		Id,
		AccountId,
		Vin,	
		ReportType,
		DisplayInHotList,
		Document,
		ExpirationDate,
		OwnerCount,
		HasProblem)
	SELECT	E.RequestID,
		R.AccountId,
		V.VIN,
		R.ReportType,
		D.IsHotListEnabled,
		E.Report,
		E.ExpirationDate,
		E.OwnerCount,
		E.HasProblem
	FROM	Carfax.Vehicle V
	JOIN	Carfax.Request R 
	ON	R.VehicleID = V.VehicleID
	JOIN	Carfax.Report E 
	ON	E.RequestID = R.RequestID
	JOIN	Carfax.Account_Dealer A 
	ON	 R.AccountID = A.AccountID
	JOIN	Carfax.Vehicle_Dealer D 
	ON	D.DealerID = A.DealerID 
	AND	D.VehicleID = V.VehicleID
	AND	D.RequestID =R.RequestID
	WHERE	R.AccountId = @AccountId
	AND	V.VehicleID = @VehicleID

	SELECT @rc = @@ROWCOUNT, @err = @@ERROR

	IF (@err <> 0) GOTO Failed

	IF (@rc <> 1)
		BEGIN
			RAISERROR (50200,16,1,@rc)
			RETURN @@ERROR
		END
END

 

INSERT 
INTO	@ReportInspection (
	ReportInspectionId,
	Selected
	)
SELECT	ReportInspectionId,
	Selected
FROM	Carfax.ReportInspectionResult R
JOIN	@Report T 
ON	T.Id = R.RequestId



------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Report

SELECT * FROM @ReportInspection



Return 0
	
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO