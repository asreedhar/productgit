IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.DealerAccount#Exists') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE  Carfax.DealerAccount#Exists  AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.DealerAccount#Exists to [CarfaxUser]
GO

ALTER PROCEDURE [Carfax].[DealerAccount#Exists]
	@DealerId	INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Carfax.DealerAccount#Exists.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns 1 if the Dealer Exists. 
 * 
 * Parameters
 * ----------
 *
 * @DealerId   - ID that maps to a BusinessUnitID of Type 4 (Dealer) in the IMT..BusinessUnit table.
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @DealerId is null
 * 50106 - Dealer Record does not exist.
 * 50200 - Expected one Row
 *
 * History
 * -------
 * MAK	05/26/2009	Create procedure.
 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

EXEC @err = dbo.ValidateParameter_DealerID @DealerId 
IF (@err <> 0) GOTO Failed


------------------------------------------------------------------------------------------------
-- Declare Result Set
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE
	([Exists] BIT)

------------------------------------------------------------------------------------------------
-- Populate Result Set
------------------------------------------------------------------------------------------------

IF EXISTS (SELECT	1
	FROM	dbo.BusinessUnit B
	JOIN	Carfax.Account_Dealer AD
	ON	B.BusinessUnitID =AD.DealerID
	WHERE	BusinessUnitID =@DealerId
	AND		BusinessUnitTypeID =4 -- Type Dealer
	)
BEGIN
	INSERT
	INTO	@Results
		([Exists])
	VALUES	(1)
END
ELSE
BEGIN
	INSERT
	INTO	@Results
		([Exists])
	VALUES	(0)
END

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF (@err <> 0) GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

-- there must be one row in the tables

IF (@rc <> 1)
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM  @Results

RETURN 0
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO