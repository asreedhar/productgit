IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.ValidateParameter_VehicleEntityTypeID') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Carfax.ValidateParameter_VehicleEntityTypeID AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.ValidateParameter_VehicleEntityTypeID to [CarfaxUser]
GO


ALTER PROCEDURE [Carfax].[ValidateParameter_VehicleEntityTypeID]
	@VehicleEntityTypeID INT
AS
/* --------------------------------------------------------------------
 * 
 * 
 * Summary
 * -------
 * 
 *	Verifies that the VehicleEntityTypeID
 *	1. Is Not Null
 *	2. Exists in Carfax.VehicleEntityType
 *
 *
 *
 * Parameters
 * ----------
 *
 * @VehicleEntityTypeID  
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @VehicleEntityTypeID is null
 * 50106 - @VehicleEntityTypeID exists
 *
 * History
 * -------
 * MAK	06/10/2009	Create procedure.

 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

IF (@VehicleEntityTypeID IS NULL)
BEGIN
	RAISERROR (50100,16,1,'VehicleEntityTypeID')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Populate Result Set
------------------------------------------------------------------------------------------------

IF (NOT EXISTS (
	SELECT	1
	FROM	Carfax.VehicleEntityType VT
	WHERE	VT.VehicleEntityTypeID =@VehicleEntityTypeID)
)
	BEGIN
		RAISERROR (50106,16,1,'VehicleEntityTypeID')
		RETURN @@ERROR
	END



------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err



