IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.DealerAccount#Fetch') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Carfax.DealerAccount#Fetch  AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.DealerAccount#Fetch to [CarfaxUser]
GO

ALTER PROCEDURE [Carfax].[DealerAccount#Fetch]
	@DealerId	INT
AS
/* --------------------------------------------------------------------
 * 
 * $Id: Carfax.DealerAccount#Fetch.sql,v 1.6 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns 2 Result Sets:
 *	RS #1  Account Information:	DealerID 
 *					AccountType
 *					AccountStatus
 *					Active
 *					UserName
 *					Password
 *					Version
 * 
 *	RS #2 Account Assignment:	MemberID
 *					Assigned
 *
 *
 * Parameters
 * ----------
 *
 * @DealerID   - ID that maps to a BusinessUnitID of Type 4 (Dealer) in the IMT..BusinessUnit table.
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @DealerId is null
 * 50106 - Record for DealerID does not exist
 * 50106 - Record for Dealer_Account does not exist
 * 50200 - Expected one Row
 *
 * History
 * ----------
 * 
 * MAK	05/26/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

EXEC  @err =Carfax.ValidateParameter_DealerAccountID @DealerId

IF (@err <> 0) GOTO Failed

DECLARE @CarfaxAccount TABLE
	(Id		INT	NOT NULL,
	AccountType	INT	NOT NULL,
	AccountStatus	INT	NOT NULL,
	Active		BIT NOT NULL,
	UserName	VARCHAR(20)	NOT NULL,
	Password	VARCHAR(5)	NOT NULL,
	Version		BINARY(8)	NOT NULL)

DECLARE @CarfaxAccountAssignment TABLE
	(MemberID	INT		NOT NULL,
	Assigned	DATETIME NOT NULL)

INSERT
INTO	@CarfaxAccount
	(Id,
	AccountType,
	AccountStatus,
	Active,
	UserName,
	Password,
	Version)
SELECT	A.AccountID,
	A.AccountTypeID,
	A.AccountStatusID,
	A.Active,
	A.UserName,
	A.Password,
	A.Version
FROM	Carfax.Account A
JOIN	Carfax.Account_Dealer AD
ON	A.AccountID =AD.AccountID
WHERE	AD.DealerID =@DealerID

IF (@rc <> 1)
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

INSERT
INTO	@CarfaxAccountAssignment
	(MemberID,
	Assigned)
SELECT	AM.MemberID,
	AM.InsertDate as Assigned
FROM	Carfax.Account A
JOIN	Carfax.Account_Member AM
ON	A.AccountID =AM.AccountID
JOIN	Carfax.Account_Dealer AD
ON	A.AccountID =AD.AccountID
WHERE	AD.DealerID =@DealerID



------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @CarfaxAccount

SELECT * FROM @CarfaxAccountAssignment

RETURN 0
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err
GO