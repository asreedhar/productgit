IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Carfax].ReportProcessorCommand#AccountProblemResolved') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Carfax].ReportProcessorCommand#AccountProblemResolved  AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Carfax].ReportProcessorCommand#AccountProblemResolved  to [CarfaxUser]
GO

ALTER PROCEDURE [Carfax].ReportProcessorCommand#AccountProblemResolved 
	(@AccountID	INT)
/* --------------------------------------------------------------------
 * 
 * 
 * Summary
 * -------
 * 
 * This procedure updates the ReportProcessorCommand queue with a status of ready
 * for all commands of the given account with and accountstatusID of  AccountProblem 
 *
 *
 * Parameters
 * ----------
 *
 *	AccountID
 *
 * Exceptions
 * ----------
 * 
 *  validate account.
 *
 *
 * History
 * ----------
 * 
 * MAK	07/09/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */
AS
SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

EXEC  @err =Carfax.ValidateParameter_AccountID @AccountId
IF( @err <> 0) GOTO Failed
	
DECLARE @ReadyStatusID TINYINT
SET	@ReadyStatusID =1

DECLARE @AccountProblemStatusID TINYINT
SET	@AccountProblemStatusID =5

UPDATE	RPC
SET	ReportProcessorCommandStatusID= @ReadyStatusID
FROM	Carfax.ReportProcessorCommand RPC
JOIN	Carfax.Account_Dealer AD
ON	RPC.DealerID=AD.DealerID
WHERE	AD.AccountID =@AccountId
AND	RPC.ReportProcessorCommandStatusID =@AccountProblemStatusID


------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO