IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.ReportInspectionCollection#Fetch') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Carfax.ReportInspectionCollection#Fetch AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.ReportInspectionCollection#Fetch to [CarfaxUser]
GO

ALTER PROCEDURE Carfax.ReportInspectionCollection#Fetch
	
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Carfax.ReportInspectionCollection#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 *	Returns all available ReportInspections to be used in the middle tier.
 *
 * Exceptions
 * ----------
 * 
 *	 None
 *
 *
 *	Returns:
 *		ID		INT NOT NULL
 *		Name		VARCHAR(50) NOT NULL
 *		XPath		VARCHAR(500) NOT NULL
 *
 * History
 * ----------
 * 
 * MAK	05/27/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON


------------------------------------------------------------------------------------------------
-- Result Set table
------------------------------------------------------------------------------------------------
 DECLARE @ReportInspection TABLE
	(Id	INT		NOT NULL,
	Name	VARCHAR(100)	NOT NULL,
	xPath	VARCHAR(500)	NOT NULL)

INSERT
INTO	@ReportInspection
	(Id,
	Name,
	xPath)
SELECT  ReportInspectionID,
	Description,
	xPath
FROM	Carfax.ReportInspection


------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @ReportInspection
	
RETURN 0;

GO