IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.ReportProcessorCommand#RepurchaseExpiredInventoryReports') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Carfax.ReportProcessorCommand#RepurchaseExpiredInventoryReports AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.ReportProcessorCommand#RepurchaseExpiredInventoryReports to [CarfaxUser]
GO

ALTER PROCEDURE [Carfax].[ReportProcessorCommand#RepurchaseExpiredInventoryReports]
AS /* --------------------------------------------------------------------
 * 
 * 
 * Summary
 * -------
 * 
 *	Insert into the ReportCommand Queue work orders for all expired reports
 *	for dealers with AutoPurchase selected for Inventory.
 *
 * Exceptions
 * ----------
 * 
 *
 *	Returns:
 *
 *
 * History
 * ----------
 * 
 * MAK	06/01/2009	Create procedure.
 * MAK 	07/22/2009	Added Insertuser. 			
 * MAK  02/07/2010	Change to so that only the maximum report for that dealer is expired.
 * MAK	11/04/2010	Update code so that it does not renew reports for invalid accounts and
 *			it does not add another	request if a pending one exists in the queue.	
 * MAK	11/11/2010	Updated code with correct Dealer\Vehicle Order.  
 *	
 * -------------------------------------------------------------------- */

    SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

    DECLARE @rc INT,
        @err INT


------------------------------------------------------------------------------------------------
-- Declare and set Default variables
------------------------------------------------------------------------------------------------

    DECLARE @WorkTypeIDRenew TINYINT
    DECLARE @WorkStatusIDPending TINYINT
    DECLARE @ProcessAfterDate DATETIME
    DECLARE @VehicleEntityTypeIDInventory TINYINT

    SET @WorkTypeIDRenew = 2-- Renew Report
    SET @WorkStatusIDPending = 1
    SET @ProcessAfterDate = GETDATE()
    SET @VehicleEntityTypeIDInventory = 1

    DECLARE @CarfaxAdmin INT

    SELECT  @CarfaxAdmin = MemberID
    FROM    dbo.Member
    WHERE   Login = 'CarfaxSystemUserAdmin'

    IF ( @CarfaxAdmin is null ) 
        BEGIN
            RAISERROR ( 50106, 16, 1, 'CarfaxAdmin' )
            RETURN @@ERROR
        END

    CREATE TABLE #ReportsToRenew
        (
          DealerID INT,
          VehicleID INT,
          ReportProcessorCommandID INT
        )

    INSERT  INTO #ReportsToRenew
            (
              DealerID,
              VehicleID,
              ReportProcessorCommandID
            )
            SELECT  RPTS.DealerID,
                    RPTS.VehicleID,
                    RPC.ReportProcessorCommandID
            FROM    ( SELECT    VD.DealerID,
                                V.VehicleID
                      FROM      Carfax.Report R
                                JOIN Carfax.Request RQ ON R.RequestID = RQ.RequestID
                                JOIN Carfax.Vehicle V ON RQ.VehicleID = V.VehicleID 
                                JOIN Carfax.Vehicle_Dealer VD ON V.VehicleID = VD.VehicleID AND RQ.RequestID =VD.RequestID
                                JOIN Carfax.Account_Dealer AD ON VD.DealerID = AD.DealerID
                                JOIN Carfax.Account A ON A.AccountID = AD.AccountID
                                                       AND RQ.AccountID = A.AccountID
                                JOIN Carfax.ReportPreference RP ON VD.DealerID = RP.DealerID
                                                                   AND RP.VehicleEntityTypeID = 1
                                JOIN dbo.Inventory I ON VD.DealerID = i.BusinessUnitID
                                JOIN dbo.Vehicle VV ON I.VehicleID = VV.VehicleID
                                                       AND V.VIN = VV.VIN
                      WHERE     I.InventoryActive = 1
                                AND I.InventoryType = 2
                                AND RP.AutoPurchase = 1
                                AND A.AccountStatusID IN ( 1, 2 ) --New, Okay.
                      GROUP BY  VD.DealerID,
                                V.VehicleID
                      HAVING    MAX(R.ExpirationDate) < GETDATE()
                    ) RPTS
                    LEFT JOIN Carfax.ReportProcessorCommand RPC ON RPTS.VehicleID = RPC.VehicleID
                                                                   AND RPTS.DealerID = RPC.DealerID
                                                                   AND RPC.ReportProcessorCommandTypeID = @WorkTypeIDRenew
                                                                   AND RPC.ReportProcessorCommandStatusID = @WorkStatusIDPending
	

    UPDATE  RPC
    SET     UpdateUser = @CarfaxAdmin,
            UpdateDate = GETDATE(),
            ProcessAfterDate = @ProcessAfterDate
    FROM    Carfax.ReportProcessorCommand RPC
            JOIN #ReportsToRenew RR ON RPC.ReportProcessorCommandID = RR.ReportProcessorCommandID
	
    INSERT  INTO Carfax.ReportProcessorCommand
            (
              DealerID,
              VehicleID,
              VehicleEntityTypeID,
              ReportProcessorCommandTypeId,
              ReportProcessorCommandStatusID,
              ProcessAfterDate,
              InsertUser,
              InsertDate
            )
            SELECT  DealerID,
                    VehicleID,
                    @VehicleEntityTypeIDInventory,
                    @WorkTypeIDRenew,
                    @WorkStatusIDPending,
                    @ProcessAfterDate,
                    @CarfaxAdmin,
                    GETDATE()
            FROM    #ReportsToRenew
            WHERE   ReportProcessorCommandID IS NULL

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

    RETURN 0
	
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

    Failed:

    RETURN @err


GO