IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.Exception#Insert') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Carfax.Exception#Insert AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.Exception#Insert to [CarfaxUser]
GO


ALTER PROCEDURE [Carfax].[Exception#Insert]
	 @MachineName VARCHAR(256),
	@Time DATETIME,
	@Type VARCHAR(256),
	@Message VARCHAR(1024),
	@Detail TEXT,
	@ReportID INT

AS

/* --------------------------------------------------------------------
 * 
 * 
 * Summary
 * -------
 * 
 *	Record an exception.
 *
 * Exceptions
 * ----------
 * 
 *
 *	Returns:
 *		No ResultSet returned.
 *
 * History
 * ----------
 * 
 * MAK	05/27/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------
BEGIN TRY
 
INSERT
INTO	Carfax.Exception
	(ExceptionTime,
	MachineName,
	Message,
	ExceptionType,
	Details,
	RequestID)
VALUES (@Time,
	@MachineName,
	@Message,
	@Type,
	@Detail,
	@ReportID)
END TRY

BEGIN CATCH
	EXEC sp_ErrorHandler	
			
END CATCH

GO