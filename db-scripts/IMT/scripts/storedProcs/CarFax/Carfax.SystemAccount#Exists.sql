
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.SystemAccount#Exists') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE  Carfax.SystemAccount#Exists  AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.SystemAccount#Exists to [CarfaxUser]
GO

ALTER PROCEDURE [Carfax].[SystemAccount#Exists]
	-- no parameters
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Carfax.SystemAccount#Exists.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns 1 if the System Account Exists.
 * 
 * Parameters
 * ----------
 *
 * NO PARAMETERS
 * 
 * Exceptions
 * ----------
 * 
 * 50200 - Expected one Row
 *
 * History
 * -------
 *
 * SBW	06/07/2009	Create procedure.
 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

-- NO VALIDATION

------------------------------------------------------------------------------------------------
-- Declare Result Set
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE
	([Exists] BIT)

------------------------------------------------------------------------------------------------
-- Populate Result Set
------------------------------------------------------------------------------------------------

IF (EXISTS (
	SELECT	1
	FROM	Carfax.Account
	WHERE	AccountTypeID = 2 ) -- System Account
	)
BEGIN
	INSERT
	INTO	@Results
		([Exists])
	VALUES	(1)
END
ELSE
BEGIN
	INSERT
	INTO	@Results
		([Exists])
	VALUES	(0)
END

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

-- there must be one row in the table

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM  @Results

RETURN 0
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
