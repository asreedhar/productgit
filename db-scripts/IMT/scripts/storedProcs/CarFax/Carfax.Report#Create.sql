IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.Report#Create') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Carfax.Report#Create AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.Report#Create to [CarfaxUser]
GO

ALTER PROCEDURE [Carfax].[Report#Create] 
	@AccountId INT,
	@VIN CHAR(17),
	@TrackingCode CHAR(3),
	@RequestType CHAR(3),
	@ReportType CHAR(3),
	@WindowSticker CHAR(3),
	@DisplayInHotList BIT,
	@InsertUser VARCHAR(80),
	@Id INT OUTPUT

AS

/* --------------------------------------------------------------------
 * 
 * $Id: Carfax.Report#Create.sql,v 1.5.4.2 2010/06/02 18:40:42 whummel Exp $
 * 
 * Summary
 * -------
 * 
 *	Insert a row into the table Carfax.Request returning the RequestID (PK of report).
 *
 *	Parameters:
 *		AccountId INT
 *		VIN CHAR(17)
 *		TrackingCode CHAR(3)
 *		RequestType CHAR(3)
 *		ReportType CHAR(3)
 *		WindowSticker CHAR(3)
 *		DisplayInHotList BIT
 *		InsertUser VARCHAR(80)
 *		Id INT OUTPUT
 *
 *	Exceptions:
 *		50100 AccountId is NULL
 *		50106 Record with AccountId does not exist
 *		50100 VIN is NULL
 *		50114 LEN(VIN) != 17
 *		50100 TrackingCode is NULL
 *		50106 Record with TrackingCode does not exist
 *		50100 RequestType is NULL
 *		50106 Record with RequestType does not exist
 *		50100 ReportType is NULL
 *		50106 Record with ReportType does not exist
 *		50100 WindowSticker is NULL
 *		50106 Record with WindowSticker does not exist
 *		50100 DisplayInHotList is NULL
 *		50100 InsertUser is NULL
 *		50106 Record with InsertUser does not exist
 *		50200 Expected to insert one row (but inserted 0 or > 1)
 *
 *	Result Set
 *	NONE (Just OUTPUT parameter).
 *
 *	Returns:
 *		None
 *
 *
 * History
 * ----------
 * 
 * MAK	05/27/2009	Create procedure.
 * MAK  06/10/2009	Update Carfax.Vehicle_Dealer with new RequestID.
 * MAK  06/22/2009	Per 5657, divided into 2 SQL Statements.		
 * MAK	03/23/2010	The RequestID, IsHotListEligible flags should only be updated
 *					upon successful completion of a report.  Thus, the update\Insert
 *					to Carfax.Vehicle_Dealer should occur in Carfax.Report#Insert.
 *				
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT
BEGIN TRY
	
	EXEC  @err =Carfax.ValidateParameter_AccountID @AccountId
	EXEC  @err =dbo.ValidateParameter_VIN @Vin
 
	IF (@TrackingCode  is null)
	BEGIN
		RAISERROR (50100,16,1,'TrackingCode')
		RETURN @@ERROR
	END

	IF (NOT EXISTS (SELECT 1
		FROM 	Carfax.TrackingCode
		WHERE 	TrackingCode =@TrackingCode))
	BEGIN
		RAISERROR (50106,16,1,'TrackingCode')
		RETURN @@ERROR
	END

	IF (@RequestType  is null)
	BEGIN
		RAISERROR (50100,16,1,'RequestType')
		RETURN @@ERROR
	END

	IF (NOT EXISTS (SELECT 1
		FROM 	Carfax.RequestType
		WHERE 	RequestType =@RequestType))
	BEGIN
		RAISERROR (50106,16,1,'RequestType')
		RETURN @@ERROR
	END

	EXEC  @err =Carfax.ValidateParameter_ReportType @ReportType

	IF (@WindowSticker  is null)
	BEGIN
		RAISERROR (50100,16,1,'WindowSticker')
		RETURN @@ERROR
	END

	IF (NOT EXISTS (SELECT 	1
		FROM 	Carfax.WindowSticker
		WHERE 	WindowSticker =@WindowSticker))
	BEGIN
		RAISERROR (50106,16,1,'WindowSticker')
		RETURN @@ERROR
	END

	IF (@DisplayInHotList   is null)
	BEGIN
		RAISERROR (50100,16,1,'DisplayInHotList')
		RETURN @@ERROR
	END

	DECLARE @InsertUserID INT

	EXEC  @err =dbo.ValidateParameter_MemberID#UserName  @InsertUser, @InsertUserID OUTPUT

	BEGIN TRANSACTION

	DECLARE @VehicleID INT

	SELECT @VehicleID =VehicleID
	FROM	Carfax.Vehicle
	WHERE	VIN=@VIN

	IF (@VehicleID is null)
	BEGIN
		INSERT
		INTO	Carfax.Vehicle
				(VIN,
				IsHotListEligible,
				InsertUser,
				InsertDate)
		VALUES
				(@VIN,
				0,
				@InsertUserID,
				GETDATE())

		SET @VehicleID =SCOPE_IDENTITY()
	END

	DECLARE @AccountTypeID TINYINT, @DealerID INT

	SELECT	@AccountTypeID = AccountTypeID
	FROM	Carfax.Account
	WHERE	AccountID=@AccountID

	IF (@AccountTypeID = 1 )
	BEGIN

		SELECT  @DealerID =DealerID
		FROM	Carfax.Account_Dealer
		WHERE	AccountID=@AccountID

		IF @DealerID is null
		BEGIN
			RAISERROR (50100,16,1,@DealerID)
			RETURN @@ERROR
		END

	END
	
------------------------------------------------------------------------------------------------
-- Perform INSERT
------------------------------------------------------------------------------------------------

	INSERT
	INTO	Carfax.Request
			(VehicleID,
			AccountID,
			TrackingCode,
			RequestType,
			ReportType,
			WindowSticker,
			DisplayInHotList,
			InsertUser,
			InsertDate)
	Values  (@VehicleID,
			@AccountID,
			@TrackingCode,
			@RequestType,
			@ReportType,
			@WindowSticker,
			@DisplayInHotList,
			@InsertUserID,
			GETDATE())
	
	SET @Id =SCOPE_IDENTITY()

	
------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

	COMMIT TRANSACTION
	RETURN 0
END TRY
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

BEGIN CATCH

	 
	DECLARE @ErrNum INT
	DECLARE @ErrorMessage VARCHAR(2000)
	SET @ErrorMessage =ERROR_MESSAGE()  
	SET @ErrNum =ERROR_NUMBER() 

	IF (XACT_STATE() =1 OR XACT_STATE()=-1)
	 ROLLBACK TRANSACTION
	
	RAISERROR(@ErrorMessage, 16, 1)
	 
	RETURN @ErrNum

END CATCH

GO