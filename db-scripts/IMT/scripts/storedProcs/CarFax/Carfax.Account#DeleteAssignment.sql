IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.Account#DeleteAssignment') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE  Carfax.Account#DeleteAssignment AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on Carfax.Account#DeleteAssignment to [CarfaxUser]
GO

ALTER PROCEDURE [Carfax].[Account#DeleteAssignment]
	@AccountId	INT,
	@MemberId	INT,
	@DeleteUser	VARCHAR(80)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Carfax.Account#DeleteAssignment.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 *  Delete Relationship in the Carfax.Account_Member table between the AccountID
 *  and the MemberID by setting the Delete User to the parameter.
 *
 *	Parameters:
 *	AccountId INT
 *	MemberId INT
 *	DeleteUser VARCHAR(80)
 *
 *	Validation
 *
 *	50100 AccountId is null
 *	50106 Record with AccountId does not exist
 *	50100 MemberId IS NULL
 *	50106 Record with MemberID does not exist
 *	50100 DeleteUser IS NULL
 *	50106 Record with DeleteUser does not exist
 *	50200 Expected to delete one row (but zero or 2+ changed)
 *
 * History
 * ----------
 * 
 * MAK	05/26/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

EXEC  @err =Carfax.ValidateParameter_AccountId @AccountId

IF (@err <> 0) GOTO Failed


IF (@MemberID IS NULL)
BEGIN
	RAISERROR (50100,16,1,'MemberId')
	RETURN @@ERROR
END

IF (NOT EXISTS(	SELECT 1
		FROM	Carfax.Account_Member
		WHERE	AccountID =@AccountId
		AND	MemberID =@MemberId))

BEGIN
	RAISERROR (50106,16,1,'MemberID and AccountID')
	RETURN @@ERROR
END

 
DECLARE @DeleteUserID INT

EXEC  @err =dbo.ValidateParameter_MemberID#UserName @DeleteUser, @DeleteUserID OUTPUT

IF (@err <> 0) GOTO Failed

------------------------------------------------------------------------------------------------
-- Perform Update
------------------------------------------------------------------------------------------------


DELETE 
FROM 	Carfax.Account_Member
WHERE	AccountID =@AccountId
AND	MemberID = @MemberId


SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF (@rc <> 1)
BEGIN
	RAISERROR (50200,16,1,'Account_Member')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err


GO