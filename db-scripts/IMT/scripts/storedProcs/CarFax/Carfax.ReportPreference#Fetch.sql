IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Carfax.ReportPreference#Fetch') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE Carfax.ReportPreference#Fetch AS SELECT 1')
GO


GRANT EXECUTE, VIEW DEFINITION on Carfax.ReportPreference#Exists to [CarfaxUser]
GO

ALTER PROCEDURE [Carfax].[ReportPreference#Fetch]
	@DealerId INT,
	@VehicleEntityTypeId INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Carfax.ReportPreference#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 *	Returns Report Preferences for a given Dealer\Vehicle EntityType
 *
 * Exceptions
 * ----------
 * 
 *	50100 DealerId IS NULL
 *	50106 Record with DealerId does not exist
 *	50100 VehicleEntityTypeId IS NULL
 *	50106 Record with VehicleEntityTypeId does not exist
 *	50200 Expected one result set row (but got zero or > 1)

 *
 *
 *	Returns:
 *		DealerId		INT NOT NULL
 *		VehicleEntityTypeId	INT NOT NULL
 *		DisplayInHotList	BIT NOT NULL
 *		PurchaseReport		BIT NOT NULL
 *		ReportType		CHAR(3) NOT NULL
 *		Version			BINARY(8) NOT NULL
 *
 * History
 * ----------
 * 
 * MAK	05/27/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

EXEC  @err =dbo.ValidateParameter_DealerID @DealerId

IF @err <> 0 GOTO Failed

EXEC  @err =Carfax.ValidateParameter_VehicleEntityTypeID @VehicleEntityTypeID

IF @err <> 0 GOTO Failed

DECLARE @ReportPreferences TABLE
	(DealerID		INT	NOT NULL,
	VehicleEntityTypeID	INT	NOT NULL,
	DisplayInHotList	BIT	NOT NULL,
	PurchaseReport		BIT	NOT NULL,
	ReportType		CHAR(3)	NOT NULL,
	Version			BINARY(8) NOT NULL)

INSERT
INTO	@ReportPreferences
	(DealerID,
	VehicleEntityTypeID,
	DisplayInHotList,
	PurchaseReport,
	ReportType,
	Version)
SELECT  DealerID,
	VehicleEntityTypeID,
	DisplayInHotList,
	AutoPurchase,
	ReportType,
	Version
FROM	Carfax.ReportPreference
WHERE	DealerID =@DealerId
AND	VehicleEntityTypeID =@VehicleEntityTypeId 

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF @rc <>1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT	DealerID,
	VehicleEntityTypeID,
	DisplayInHotList,
	PurchaseReport,
	Upper(ReportType) as ReportType,
	Version 
FROM	@ReportPreferences

RETURN 0	
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO