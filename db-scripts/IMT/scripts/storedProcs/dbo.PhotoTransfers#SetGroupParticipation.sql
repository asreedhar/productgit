USE [IMT]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PhotoTransfers#SetGroupParticipation]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[PhotoTransfers#SetGroupParticipation]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Enables or disables photo transfers given a business unit ID for either a dealership or a group of dealerships
CREATE PROCEDURE [dbo].[PhotoTransfers#SetGroupParticipation]
	@businessUnitId int,
	@enabled bit
AS
BEGIN

	declare @buTypeID int;
	declare @buParentID int;

	SET NOCOUNT ON;
	
	-- fetch type and parent of business unit	
	select @buTypeID = bu.businessUnitTypeID, @buParentID = bur.parentID
	from imt.dbo.businessUnit bu 
	join imt.dbo.businessUnitRelationship bur on bur.businessUnitId = bu.businessUnitId
	where bu.businessUnitId = @businessUnitID;
	
	-- if business unit is a dealership, enable photo transfers for it's parent group
	if (@buTypeID = 4)
		set @businessUnitId = @buParentID

	if exists (select 1 from PhotoTransferGroups where groupBusinessUnitId=@businessUnitId)
		update dbo.photoTransferGroups set enabled = @enabled where groupBusinessUnitId = @businessUnitId
	else
		insert into dbo.photoTransferGroups (groupBusinessUnitId, enabled) select @businessUnitId, @enabled
		
END
