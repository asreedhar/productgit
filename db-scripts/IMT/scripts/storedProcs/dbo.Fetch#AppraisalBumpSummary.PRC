/****** Object:  StoredProcedure [dbo].[Fetch#AppraisalBumpSummary]    Script Date: 06/27/2014 20:36:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Fetch#AppraisalBumpSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Fetch#AppraisalBumpSummary]
GO

/****** Object:  StoredProcedure [dbo].[Fetch#AppraisalBumpSummary]    Script Date: 06/27/2014 20:36:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO








-- =============================================
-- Author:		Vivek Rane
-- Create date: 2014-05-26
-- Modified Date : 2014-06-26
-- Description:	This stored procedure fetches Summary of Appraisal bumps
-- History : 2014-06-26- 1. Updated SP to remove Sequence no 1 as Initial appraisal. 			
-- =============================================
CREATE PROCEDURE [dbo].[Fetch#AppraisalBumpSummary]
	@Period INT -- Period Id from FLDW.dbo.Period_D.
	,@SelectedDealerId varchar(1000) -- DealerId seperated by Comma	
	,@AppraiserName varchar(1000)=NULL -- Optional Appraiser Name. If passed NULL then it will return Dealer level Data.
	,@AppraisalType INT = 1 -- Type of Appraisal 1- Trade-In, 2 - Purchase. Master Table IMT.dbo.AppraisalType
	,@AppraisalSource INT = 1 -- Appraisal Source from IMT.dbo.AppraisalSource. Value 1 is The Edge
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE
    @DateField DATE
    ,@EndDate DATE
    ,@Name VARCHAR(1000)='firstName lastName'
    ,@AllAppraiser VARCHAR(1000)='All Appraisers'
    ,@NoAppraiser VARCHAR(1000)='No Appraiser'
    ,@LocalPeriod INT -- Period Id from FLDW.dbo.Period_D.
    ,@LocalSelectedDealerId varchar(1000) -- DealerId seperated by Comma    
    ,@LocalAppraiserName varchar(1000)=NULL -- Optional Appraiser Name. If passed NULL then it will return Dealer level Data.
    ,@LocalAppraisalType INT = 1 -- Type of Appraisal 1- Trade-In, 2 - Purchase. Master Table IMT.dbo.AppraisalType
    ,@LocalAppraisalSource INT = 1 -- Appraisal Source from IMT.dbo.AppraisalSource. Value 1 is The Edge
    ,@BumpedAppraisalCount INT =0;
          
    
     SELECT  @LocalPeriod = @Period, 
        @LocalSelectedDealerId = @SelectedDealerId, 
        @LocalAppraiserName = @AppraiserName, 
        @LocalAppraisalType = @AppraisalType,
        @LocalAppraisalSource = @AppraisalSource;
        
    DECLARE  @BusinessUnit TABLE (BusinessUnitId INT)
    INSERT into @BusinessUnit SELECT CAST(NULLIF(S.Value,'') AS INT) FROM IMT.dbo.Split(@LocalSelectedDealerId,',') S  
    WHERE NULLIF(@LocalSelectedDealerId,'') IS NOT NULL;

    Select @DateField=StartDate,@EndDate=EndDate FROM IMT.dbo.GetDatesFromPeriodId(@LocalPeriod,0);

    Create TABLE #AppraisalValues (AppraisalId INT
			,AppraisalValueID INT --Primary Key
			,BusinessUnitID INT
			,SequenceNumber INT
			,AppraisalValue FLOAT
			,DateCreated SMALLDATETIME
			,AppraiserName varchar(max))
			
	--Get All Appraisal values filtered by Date,Source and Type		
    INSERT into #AppraisalValues
    Select AV.AppraisalID
    ,AV.AppraisalValueID
    ,A.BusinessUnitID
    ,AV.SequenceNumber,AV.Value,AV.DateCreated,LTRIM(RTRIM(AV.AppraiserName))
    from AppraisalValues AV 
    Inner Join Appraisals A on A.AppraisalID=AV.AppraisalID
    INNER JOIN @BusinessUnit BU on BU.BusinessUnitId=A.BusinessUnitID
    where
			-- AV.SequenceNumber>0 AND 
					     A.AppraisalTypeID=@LocalAppraisalType        
                         AND A.AppraisalSourceID=@LocalAppraisalSource
              --           AND AV.Value>0
                         AND CAST(A.DateCreated AS DATE) between @DateField AND @EndDate
        
    Create TABLE #BumpedAppraisalIds (AppraisalId INT, AppraisalValueID INT,BusinessUnitID INT, SequenceNumber int, BumpeeName varchar(max))
        
    --Get All appraisals with Bumps. This will also give the Sequence number and AppraiserName.
    INSERT into #BumpedAppraisalIds 
    SELECT AV1.AppraisalID
    ,AV1.AppraisalValueID
    ,AV1.BusinessUnitId 
    ,MIN(AV1.SequenceNumber)
    ,MIN(CASE WHEN ISNULL(AV1.AppraiserName,@Name)=@Name OR AV1.AppraiserName='' OR AV1.AppraiserName = '-'  OR ASCII(ltrim(rtrim(AV1.AppraiserName))) = 160 THEN @NoAppraiser ELSE LTRIM(RTRIM(AV1.AppraiserName)) END ) as AppraiserName
    from #AppraisalValues AV
    INNER JOIN  #AppraisalValues AV1 on AV.AppraisalID=AV1.AppraisalID 
		 AND AV.SequenceNumber>0 AND AV1.SequenceNumber>0 
		 AND AV.AppraisalValue>0
		 AND AV1.AppraisalValue>0 
		 AND AV.SequenceNumber=AV1.SequenceNumber+1 AND AV.AppraisalValue<>AV1.AppraisalValue -- Self JOin to compare previous appraisal value with current. If there is not change in appraisal value it is not counted as bump            
    group by     
    AV1.AppraisalID
    ,AV1.AppraisalValueID
    ,AV1.BusinessUnitId,AV1.AppraiserName 
    
    --select * from #BumpedAppraisalIds
    
    Create TABLE #InitialAppraisers (BusinessUnitId Int
			,AppraisalId INT --Primary key
			,AppraiserName Varchar(max)
			,SequenceNumber int
			,InitialValue FLOAT NULL
			,DateCreated SMALLDATETIME NULL)
    
    --Get Initial Appraiser names. 
    --Logic:- 
		--	1. If Appraisal is not bumped then first non blank appraiser name is Initial Appraiser for that appraisal.
		--	2. If Appraisal Name is blank and appraisal name is updated and appraisal bumped at same time then initial appraiser name will be considered as No Appraiser(blank name)
    INSERT into #InitialAppraisers 
    Select AV.BusinessUnitId,AV.AppraisalID
    --,CASE WHEN ISNULL(AV.AppraiserName,@Name)=@Name OR AV.AppraiserName='' OR AV.AppraiserName = '-'  OR ASCII(ltrim(rtrim(AV.AppraiserName))) = 160 THEN @NoAppraiser ELSE AV.AppraiserName END  as AppraiserName
    ,NULL
    ,MIN(AV.SequenceNumber)
    ,NULL
    ,NULL
    from #AppraisalValues AV
    --INNER JOIN IMT..Appraisals A on A.AppraisalID=AV.AppraisalID
    --INNER JOIN @BusinessUnit BU on BU.BusinessUnitId=A.BusinessUnitID
    WHERE 
        				--AV.SequenceNumber>0 AND
						 --A.AppraisalTypeID=@LocalAppraisalType       
                         --AND A.AppraisalSourceID=@LocalAppraisalSource
                         --AND AV.Value>0
                         --AND CAST(A.DateCreated AS DATE) between @DateField AND @EndDate
    --AND 
    ISNULL(LTRIM(RTRIM(AV.AppraiserName)),'')<>'' 
    AND ASCII(ltrim(rtrim(AV.AppraiserName))) <> 160
    group by AV.AppraisalID,
    AV.BusinessUnitId    
      
    update #InitialAppraisers  
    SET AppraiserName = CASE WHEN ISNULL(AV.AppraiserName,@Name)=@Name OR AV.AppraiserName='' OR AV.AppraiserName = '-'  OR ASCII(ltrim(rtrim(AV.AppraiserName))) = 160 THEN @NoAppraiser ELSE LTRIM(RTRIM(AV.AppraiserName)) END
	FROM   #InitialAppraisers IA    
    INNER JOIN #AppraisalValues AV on AV.SequenceNumber=IA.SequenceNumber AND IA.AppraisalId=AV.AppraisalId
       
   -- Set initial Appraisal which are blank as 'No appraiser'
    INSERT into #InitialAppraisers 
    Select AV.BusinessUnitId,AV.AppraisalID
    ,MIN(@NoAppraiser) as AppraiserName
    ,MIN(AV.SequenceNumber)
    ,NULL
    ,NULL
    from #AppraisalValues AV    
    --INNER JOIN IMT..Appraisals A on A.AppraisalID=AV.AppraisalID 
    --INNER JOIN @BusinessUnit BU on BU.BusinessUnitId=A.BusinessUnitID   
    WHERE NOT EXISTS(select 1 from #InitialAppraisers IA where AV.AppraisalID=IA.AppraisalId ) 
    	-- AND A.AppraisalTypeID=@LocalAppraisalType        
        --                 AND A.AppraisalSourceID=@LocalAppraisalSource
        --                 AND CAST(A.DateCreated AS DATE) between @DateField AND @EndDate
    AND (ISNULL(LTRIM(RTRIM(AV.AppraiserName)),'')='' OR ASCII(ltrim(rtrim(AV.AppraiserName))) = 160)
    group by AV.AppraisalID,AV.BusinessUnitId
         
    update #InitialAppraisers  
    SET AppraiserName = CASE WHEN ISNULL(AV.AppraiserName,@Name)=@Name OR AV.AppraiserName='' OR AV.AppraiserName = '-'  OR ASCII(ltrim(rtrim(AV.AppraiserName))) = 160 THEN @NoAppraiser ELSE LTRIM(RTRIM(AV.AppraiserName)) END 
    ,SequenceNumber=AV.SequenceNumber
    FROM   #InitialAppraisers IA
    INNER JOIN #BumpedAppraisalIds BA ON IA.AppraisalId = BA.AppraisalId  AND IA.SequenceNumber>BA.SequenceNumber
    INNER JOIN imt..AppraisalValues AV on AV.AppraisalID=BA.AppraisalId --and AV.AppraisalValueID=BA.AppraisalValueID AND (IA.AppraiserName='' OR ASCII(ltrim(rtrim(IA.AppraiserName))) = 160)
    
    update #InitialAppraisers  
    SET InitialValue = AV.AppraisalValue    
    FROM   #InitialAppraisers IA    
    INNER JOIN #AppraisalValues AV on AV.AppraisalId=IA.AppraisalId
    INNER JOIN 		
		(select AppraisalId, MIN(SequenceNumber) AS SequenceNumber from #AppraisalValues where AppraisalValue>0 group by AppraisalID) 
			AV1 on AV1.AppraisalId=AV.AppraisalId and AV.SequenceNumber=AV1.SequenceNumber
      
	update #InitialAppraisers  
    SET DateCreated = AV.DateCreated
	FROM   #InitialAppraisers IA    
    INNER JOIN #AppraisalValues AV on AV.SequenceNumber=0 AND IA.AppraisalId=AV.AppraisalId
    
   
    select @BumpedAppraisalCount=COUNT(1) from #BumpedAppraisalIds;
    
    IF @BumpedAppraisalCount>0 
    BEGIN                     
        IF @LocalAppraiserName IS NULL OR @LocalAppraiserName=@AllAppraiser        
			BEGIN      
				WITH MAXSequenceNo AS(
					SELECT ROW_NUMBER() OVER(PARTITION BY APPV.AppraisalID  ORDER BY  APPV.SequenceNumber DESC) AS Row, 
					APPV.SequenceNumber,APPV.AppraisalID  ,APPV.AppraisalValue , APPV.AppraiserName 
					FROM #AppraisalValues APPV
				) 
	            
				 SELECT DISTINCT BA.BusinessUnitId			 
				 ,ISNULL(TradesAnalyzedCount.TradesAnalyzed,0) AS TradeAnalyzed
				 ,ISNULL(AppraisalsRecordedCount.AppraisalValueRecorded,0) AS AppraisalWithValueRecorded
				 ,AppraisalsBumpedCount.AppraisalsBumped As AppraisalsBumped
				 ,TotalBumpedCount.AppraisalsBumped AS TotalBumped
				 ,CASE WHEN ISNULL(TotalBumpedCount.AppraisalsBumped,0)=0 THEN 0 ELSE CAST(AppraisalBumpValueTotal.VALUE AS FLOAT)/ CAST(TotalBumpedCount.AppraisalsBumped AS FLOAT) END AS AverageBump
				 ,CASE WHEN ISNULL(AppraisalsBumpedCount.AppraisalsBumped,0)=0 THEN 0 ELSE CAST(AvgChangeInAppraisalTotal.AvgChangeInAppraisal AS FLOAT)/ CAST(AppraisalsBumpedCount.AppraisalsBumped AS FLOAT) END AS AverageChangeFromInitialToLast
				 FROM
				 #BumpedAppraisalIds BA			    
				 LEFT JOIN
				(
					 select IA.BusinessUnitId,COUNT(IA.AppraisalID) AS TradesAnalyzed						
						from #InitialAppraisers IA                 
						GROUP By BusinessUnitId
				) TradesAnalyzedCount
				ON BA.BusinessUnitID=TradesAnalyzedCount.BusinessUnitId			
				LEFT JOIN
				(
					select IA.BusinessUnitId,COUNT(IA.AppraisalID) AS AppraisalValueRecorded					
					from #InitialAppraisers IA
					Where ISNULL(IA.InitialValue,0)>0
					GROUP By BusinessUnitId
				)AppraisalsRecordedCount 
				ON BA.BusinessUnitID=AppraisalsRecordedCount.BusinessUnitId 			
				LEFT JOIN
				(
					Select BA.BusinessUnitId        								
								,COUNT(1) AS AppraisalsBumped
								FROM( 
									SELECT  DISTINCT BA.BusinessUnitID, BA.AppraisalId, AV1.AppraisalValueID, AV1.SequenceNumber
									,CASE WHEN ISNULL(AV1.AppraiserName,@Name)=@Name OR AV1.AppraiserName='' OR AV1.AppraiserName = '-'  OR ASCII(ltrim(rtrim(AV1.AppraiserName))) = 160 THEN @NoAppraiser ELSE AV1.AppraiserName END as AppraiserName
										from #BumpedAppraisalIds BA
									INNER JOIN #InitialAppraisers IA on IA.AppraisalId=BA.AppraisalId
									INNER JOIN #AppraisalValues AV on AV.AppraisalID=BA.AppraisalID AND AV.SequenceNumber>0
									INNER JOIN #AppraisalValues AV1 on AV.AppraisalID=AV1.AppraisalID AND AV.SequenceNumber>0 AND AV.AppraisalValue>0 AND AV1.AppraisalValue>0  AND AV.SequenceNumber=AV1.SequenceNumber+1 AND AV.AppraisalValue<>AV1.AppraisalValue -- Self JOin to compare previous appraisal value with current. If there is not change in appraisal value it is not counted as bump
								) A
								INNER JOIN #BumpedAppraisalIds BA on BA.AppraisalId=a.AppraisalId AND A.SequenceNumber=BA.SequenceNumber
								INNER JOIN #InitialAppraisers IA on IA.AppraisalId=A.AppraisalId AND BA.SequenceNumber=A.SequenceNumber								
								GROUP BY BA.BusinessUnitId					
				)TotalBumpedCount
				ON BA.BusinessUnitID=TotalBumpedCount.BusinessUnitId 			
				LEFT JOIN
				(
					Select A.BusinessUnitID,COUNT(A.AppraisalsBumped) AS AppraisalsBumped FROM (
					 select DISTINCT BA.BusinessUnitId,BA.AppraisalID AS AppraisalsBumped 
					 from #BumpedAppraisalIds BA
					 ) A            
					 GROUP By A.BusinessUnitId             
		            
				)AppraisalsBumpedCount
				ON BA.BusinessUnitID=AppraisalsBumpedCount.BusinessUnitId 			
				LEFT JOIN
				(  
						 SELECT  A.BusinessUnitID,SUM(A.BumpedValue-A.InitialValue) AS VALUE FROM(
							SELECT DISTINCT BA.BusinessUnitID                
							,AV.AppraisalValueID AS BumpedAppraisalValueID
							,AV1.AppraisalValueID AS InitialAppraisalValueID
							,AV.AppraisalValue AS BumpedValue
							,AV1.AppraisalValue AS InitialValue
							,AV1.AppraiserName AS AppraiserName
							from #BumpedAppraisalIds BA 
							--INNER JOIN #InitialAppraisers IA on IA.AppraisalId=BA.AppraisalId
							INNER JOIN #AppraisalValues AV on AV.AppraisalID=BA.AppraisalID
							INNER JOIN #AppraisalValues AV1 on AV.AppraisalID=AV1.AppraisalID AND AV.SequenceNumber=AV1.SequenceNumber+1  AND AV.AppraisalValue>0 AND AV1.AppraisalValue>0 AND AV.AppraisalValue<>AV1.AppraisalValue -- Self JOin to compare previous appraisal value with current. If there is not change in appraisal value it is not counted as bump
							) A
							GROUP BY A.BusinessUnitID
				)AppraisalBumpValueTotal 
				ON BA.BusinessUnitID=AppraisalBumpValueTotal.BusinessUnitId 			
				LEFT JOIN
				( 
					 SELECT SUM(A.LastValue-A.InitialValue) AS AvgChangeInAppraisal, A.BusinessUnitID FROM (
						Select DISTINCT BA.BusinessUnitId						
						,IA.AppraisalId AS AppraisalID
						,IA.InitialValue AS InitialValue
						,AV1.AppraisalValue AS LastValue				
						FROM #InitialAppraisers IA
						INNER JOIN #BumpedAppraisalIds BA on BA.AppraisalId=IA.AppraisalId
						INNER JOIN MAXSequenceNo AV1 on AV1.AppraisalID =IA.AppraisalID  and (AV1.Row =1)                        
						) A
					GROUP BY A.BusinessUnitId											
				)AvgChangeInAppraisalTotal
				ON BA.BusinessUnitID=AvgChangeInAppraisalTotal.BusinessUnitId 						
         END
		ELSE
			BEGIN
				 IF @LocalAppraiserName=@NoAppraiser
					BEGIN
						--Get Last Appraisal Details.
						 WITH MAXSequenceNo AS(
								SELECT ROW_NUMBER() OVER(PARTITION BY APPV.AppraisalID  ORDER BY  APPV.SequenceNumber DESC) AS Row, 
								APPV.SequenceNumber,APPV.AppraisalID  ,APPV.AppraisalValue , APPV.AppraiserName 
								FROM #AppraisalValues APPV
						 )

						 SELECT DISTINCT BA.BusinessUnitId			 
							 ,ISNULL(TradesAnalyzedCount.TradesAnalyzed,0) AS TradeAnalyzed
							 ,ISNULL(AppraisalsRecordedCount.AppraisalValueRecorded,0) AS AppraisalWithValueRecorded
							 ,AppraisalsBumpedCount.AppraisalsBumped As AppraisalsBumped
							 ,TotalBumpedCount.AppraisalsBumped AS TotalBumped
							 ,CASE WHEN ISNULL(TotalBumpedCount.AppraisalsBumped,0)=0 THEN 0 ELSE CAST(AppraisalBumpValueTotal.VALUE AS FLOAT)/ CAST(TotalBumpedCount.AppraisalsBumped AS FLOAT) END AS AverageBump
							 ,CASE WHEN ISNULL(AppraisalsBumpedCount.AppraisalsBumped,0)=0 THEN 0 ELSE CAST(AvgChangeInAppraisalTotal.AvgChangeInAppraisal AS FLOAT)/ CAST(AppraisalsBumpedCount.AppraisalsBumped AS FLOAT) END AS AverageChangeFromInitialToLast
							 FROM
							 #BumpedAppraisalIds BA			    
							 LEFT JOIN
							(
								 select IA.BusinessUnitId,COUNT(IA.AppraisalID) AS TradesAnalyzed						
									from #InitialAppraisers IA   
									WHERE IA.AppraiserName=@NoAppraiser              
									GROUP By BusinessUnitId
							) TradesAnalyzedCount
							ON BA.BusinessUnitID=TradesAnalyzedCount.BusinessUnitId			
							LEFT JOIN
							(
								select IA.BusinessUnitId,COUNT(IA.AppraisalID) AS AppraisalValueRecorded					
								from #InitialAppraisers IA
								Where ISNULL(IA.InitialValue,0)>0
								AND IA.AppraiserName=@NoAppraiser     
								GROUP By BusinessUnitId
							)AppraisalsRecordedCount 
							ON BA.BusinessUnitID=AppraisalsRecordedCount.BusinessUnitId 			
							LEFT JOIN
							(
								Select BA.BusinessUnitId        								
								,COUNT(1) AS AppraisalsBumped
								FROM( 
									SELECT  DISTINCT BA.BusinessUnitID, BA.AppraisalId, AV1.AppraisalValueID, AV1.SequenceNumber
									,CASE WHEN ISNULL(AV1.AppraiserName,@Name)=@Name OR AV1.AppraiserName='' OR AV1.AppraiserName = '-'  OR ASCII(ltrim(rtrim(AV1.AppraiserName))) = 160 THEN @NoAppraiser ELSE AV1.AppraiserName END as AppraiserName
										from #BumpedAppraisalIds BA
									INNER JOIN #InitialAppraisers IA on IA.AppraisalId=BA.AppraisalId
									INNER JOIN #AppraisalValues AV on AV.AppraisalID=BA.AppraisalID AND AV.SequenceNumber>0
									INNER JOIN #AppraisalValues AV1 on AV.AppraisalID=AV1.AppraisalID AND AV.SequenceNumber>0 AND AV.AppraisalValue>0 AND AV1.AppraisalValue>0  AND AV.SequenceNumber=AV1.SequenceNumber+1 AND AV.AppraisalValue<>AV1.AppraisalValue -- Self JOin to compare previous appraisal value with current. If there is not change in appraisal value it is not counted as bump
								) A
								INNER JOIN #BumpedAppraisalIds BA on BA.AppraisalId=a.AppraisalId AND A.SequenceNumber=BA.SequenceNumber
								INNER JOIN #InitialAppraisers IA on IA.AppraisalId=A.AppraisalId AND BA.SequenceNumber=A.SequenceNumber
								WHERE A.AppraiserName =@NoAppraiser
								GROUP BY BA.BusinessUnitId					
							)TotalBumpedCount
							ON BA.BusinessUnitID=TotalBumpedCount.BusinessUnitId 			
							LEFT JOIN
							(
								Select A.BusinessUnitID,COUNT(A.AppraisalsBumped) AS AppraisalsBumped FROM (
									select DISTINCT BA.BusinessUnitId,BA.AppraisalID AS AppraisalsBumped
									from #BumpedAppraisalIds BA
									WHERE BA.BumpeeName=@NoAppraiser 
								 ) A            
								 GROUP By A.BusinessUnitId 								
							)AppraisalsBumpedCount
							ON BA.BusinessUnitID=AppraisalsBumpedCount.BusinessUnitId 			
							LEFT JOIN
							(  
									SELECT  A.BusinessUnitID,SUM(A.BumpedValue-A.InitialValue) AS VALUE FROM(
									SELECT DISTINCT BA.BusinessUnitID                
									,AV.AppraisalValueID AS BumpedAppraisalValueID
									,AV1.AppraisalValueID AS InitialAppraisalValueID
									,AV.AppraisalValue AS BumpedValue
									,AV1.AppraisalValue AS InitialValue									
									from #BumpedAppraisalIds BA 
									--INNER JOIN #InitialAppraisers IA on IA.AppraisalId=BA.AppraisalId
									INNER JOIN #AppraisalValues AV on AV.AppraisalID=BA.AppraisalID
									INNER JOIN #AppraisalValues AV1 on AV.AppraisalID=AV1.AppraisalID AND AV.SequenceNumber=AV1.SequenceNumber+1  AND AV.AppraisalValue>0 AND AV1.AppraisalValue>0 AND AV.AppraisalValue<>AV1.AppraisalValue -- Self JOin to compare previous appraisal value with current. If there is not change in appraisal value it is not counted as bump
									WHERE (ISNULL(AV1.AppraiserName,@Name)=@Name OR AV1.AppraiserName='' OR AV1.AppraiserName = '-'  OR ASCII(ltrim(rtrim(AV1.AppraiserName))) = 160)
									) A
									GROUP BY A.BusinessUnitID
							)AppraisalBumpValueTotal 
							ON BA.BusinessUnitID=AppraisalBumpValueTotal.BusinessUnitId 			
							LEFT JOIN
							( 
								  SELECT SUM(A.LastValue-A.InitialValue) AS AvgChangeInAppraisal, A.BusinessUnitID FROM (
									Select DISTINCT BA.BusinessUnitId									
									,IA.AppraisalId AS AppraisalID
									,IA.InitialValue AS InitialValue
									,AV1.AppraisalValue AS LastValue				
									FROM #InitialAppraisers IA
									INNER JOIN #BumpedAppraisalIds BA on BA.AppraisalId=IA.AppraisalId
									INNER JOIN MAXSequenceNo AV1 on AV1.AppraisalID =IA.AppraisalID  and (AV1.Row =1)
										WHERE BA.BumpeeName=@NoAppraiser
									) A
								GROUP BY A.BusinessUnitId								
							)AvgChangeInAppraisalTotal
							ON BA.BusinessUnitID=AvgChangeInAppraisalTotal.BusinessUnitId											
					END
					ELSE
					BEGIN
						WITH MAXSequenceNo AS(
							SELECT ROW_NUMBER() OVER(PARTITION BY APPV.AppraisalID  ORDER BY  APPV.SequenceNumber DESC) AS Row, 
							APPV.SequenceNumber,APPV.AppraisalID  ,APPV.AppraisalValue , APPV.AppraiserName 
							FROM #AppraisalValues APPV
						) 
						
						SELECT DISTINCT BA.BusinessUnitId			 
							 ,ISNULL(TradesAnalyzedCount.TradesAnalyzed,0) AS TradeAnalyzed
							 ,ISNULL(AppraisalsRecordedCount.AppraisalValueRecorded,0) AS AppraisalWithValueRecorded
							 ,AppraisalsBumpedCount.AppraisalsBumped As AppraisalsBumped
							 ,TotalBumpedCount.AppraisalsBumped AS TotalBumped
							 ,CASE WHEN ISNULL(TotalBumpedCount.AppraisalsBumped,0)=0 THEN 0 ELSE CAST(AppraisalBumpValueTotal.VALUE AS FLOAT)/ CAST(TotalBumpedCount.AppraisalsBumped AS FLOAT) END AS AverageBump
							 ,CASE WHEN ISNULL(AppraisalsBumpedCount.AppraisalsBumped,0)=0 THEN 0 ELSE CAST(AvgChangeInAppraisalTotal.AvgChangeInAppraisal AS FLOAT)/ CAST(AppraisalsBumpedCount.AppraisalsBumped AS FLOAT) END AS AverageChangeFromInitialToLast
							 FROM
							 #BumpedAppraisalIds BA			    
							 LEFT JOIN
							(
								 select IA.BusinessUnitId,COUNT(IA.AppraisalID) AS TradesAnalyzed						
									from #InitialAppraisers IA   
									WHERE IA.AppraiserName=@LocalAppraiserName              
									GROUP By BusinessUnitId
							) TradesAnalyzedCount
							ON BA.BusinessUnitID=TradesAnalyzedCount.BusinessUnitId			
							LEFT JOIN
							(
								select IA.BusinessUnitId,COUNT(IA.AppraisalID) AS AppraisalValueRecorded					
								from #InitialAppraisers IA
								Where ISNULL(IA.InitialValue,0)>0
								AND IA.AppraiserName=@LocalAppraiserName     
								GROUP By BusinessUnitId
							)AppraisalsRecordedCount 
							ON BA.BusinessUnitID=AppraisalsRecordedCount.BusinessUnitId 			
							LEFT JOIN
							(
								Select BA.BusinessUnitId        								
								,COUNT(1) AS AppraisalsBumped
								FROM( 
									SELECT  DISTINCT BA.BusinessUnitID, BA.AppraisalId, AV1.AppraisalValueID, AV1.SequenceNumber
									,CASE WHEN ISNULL(AV1.AppraiserName,@Name)=@Name OR AV1.AppraiserName='' OR AV1.AppraiserName = '-'  OR ASCII(ltrim(rtrim(AV1.AppraiserName))) = 160 THEN @NoAppraiser ELSE AV1.AppraiserName END as AppraiserName
										from #BumpedAppraisalIds BA
									INNER JOIN #InitialAppraisers IA on IA.AppraisalId=BA.AppraisalId
									INNER JOIN #AppraisalValues AV on AV.AppraisalID=BA.AppraisalID AND AV.SequenceNumber>0
									INNER JOIN #AppraisalValues AV1 on AV.AppraisalID=AV1.AppraisalID AND AV.SequenceNumber>0 AND AV.AppraisalValue>0 AND AV1.AppraisalValue>0  AND AV.SequenceNumber=AV1.SequenceNumber+1 AND AV.AppraisalValue<>AV1.AppraisalValue -- Self JOin to compare previous appraisal value with current. If there is not change in appraisal value it is not counted as bump
								) A
								INNER JOIN #BumpedAppraisalIds BA on BA.AppraisalId=a.AppraisalId AND A.SequenceNumber=BA.SequenceNumber
								INNER JOIN #InitialAppraisers IA on IA.AppraisalId=A.AppraisalId AND BA.SequenceNumber=A.SequenceNumber
								WHERE A.AppraiserName =@LocalAppraiserName
								GROUP BY BA.BusinessUnitId
										
							)TotalBumpedCount
							ON BA.BusinessUnitID=TotalBumpedCount.BusinessUnitId 			
							LEFT JOIN
							(
								Select A.BusinessUnitID,COUNT(A.AppraisalsBumped) AS AppraisalsBumped FROM (
									select DISTINCT BA.BusinessUnitId,BA.AppraisalID AS AppraisalsBumped 
									from #BumpedAppraisalIds BA
									WHERE BA.BumpeeName=@LocalAppraiserName 
								 ) A            
								 GROUP By A.BusinessUnitId               
					            
							)AppraisalsBumpedCount
							ON BA.BusinessUnitID=AppraisalsBumpedCount.BusinessUnitId 			
							LEFT JOIN
							(  
									SELECT  A.BusinessUnitID,SUM(A.BumpedValue-A.InitialValue) AS VALUE FROM(
									SELECT DISTINCT BA.BusinessUnitID                
									,AV.AppraisalValueID AS BumpedAppraisalValueID
									,AV1.AppraisalValueID AS InitialAppraisalValueID
									,AV.AppraisalValue AS BumpedValue
									,AV1.AppraisalValue AS InitialValue
									,AV1.AppraiserName AS AppraiserName
									from #BumpedAppraisalIds BA 
									--INNER JOIN #InitialAppraisers IA on IA.AppraisalId=BA.AppraisalId
									INNER JOIN #AppraisalValues AV on AV.AppraisalID=BA.AppraisalID
									INNER JOIN #AppraisalValues AV1 on AV.AppraisalID=AV1.AppraisalID AND AV.SequenceNumber=AV1.SequenceNumber+1  AND AV.AppraisalValue>0 AND AV1.AppraisalValue>0 AND AV.AppraisalValue<>AV1.AppraisalValue -- Self JOin to compare previous appraisal value with current. If there is not change in appraisal value it is not counted as bump
									WHERE AV1.AppraiserName=@LocalAppraiserName
									) A
									GROUP BY A.BusinessUnitID
							)AppraisalBumpValueTotal 
							ON BA.BusinessUnitID=AppraisalBumpValueTotal.BusinessUnitId 			
							LEFT JOIN
							( 
								  SELECT SUM(A.LastValue-A.InitialValue) AS AvgChangeInAppraisal, A.BusinessUnitID FROM (
									Select DISTINCT BA.BusinessUnitId									
									,IA.AppraisalId AS AppraisalID
									,IA.InitialValue AS InitialValue
									,AV1.AppraisalValue AS LastValue				
									FROM #InitialAppraisers IA
									INNER JOIN #BumpedAppraisalIds BA on BA.AppraisalId=IA.AppraisalId
									INNER JOIN MAXSequenceNo AV1 on AV1.AppraisalID =IA.AppraisalID  and (AV1.Row =1)
										WHERE BA.BumpeeName=@LocalAppraiserName
									) A
									GROUP BY A.BusinessUnitId								
							)AvgChangeInAppraisalTotal
							ON BA.BusinessUnitID=AvgChangeInAppraisalTotal.BusinessUnitId												
					END
			END     
    END
    ELSE
    BEGIN
		IF @LocalAppraiserName IS NULL OR @LocalAppraiserName=@AllAppraiser        
        BEGIN   
			SELECT DISTINCT BU.BusinessUnitId             
             ,ISNULL(TradesAnalyzedCount.TradesAnalyzed,0) AS TradeAnalyzed
             ,ISNULL(AppraisalsRecordedCount.AppraisalValueRecorded,0) AS AppraisalWithValueRecorded
             ,0 As AppraisalsBumped
             ,0 AS TotalBumped
             ,0 AS AverageBump
             ,0 AS AverageChangeFromInitialToLast
             FROM 
				#InitialAppraisers IA 
				RIGHT JOIN @BusinessUnit BU on IA.BusinessUnitId=BU.BusinessUnitId    
				LEFT JOIN
				(
					select IA.BusinessUnitId,COUNT(IA.AppraisalID) AS TradesAnalyzed					
					from #InitialAppraisers IA                 
					GROUP By BusinessUnitId
				) TradesAnalyzedCount
				ON BU.BusinessUnitID=TradesAnalyzedCount.BusinessUnitId						
				LEFT JOIN
				(
					select IA.BusinessUnitId,COUNT(IA.AppraisalID) AS AppraisalValueRecorded					
					from #InitialAppraisers IA
					Where ISNULL(IA.InitialValue,0)>0
					GROUP By BusinessUnitId
				)AppraisalsRecordedCount 
				ON BU.BusinessUnitID=AppraisalsRecordedCount.BusinessUnitId 
				 
		END
		ELSE IF @LocalAppraiserName=@NoAppraiser
		BEGIN
			SELECT DISTINCT BU.BusinessUnitId             
             ,ISNULL(TradesAnalyzedCount.TradesAnalyzed,0) AS TradeAnalyzed
             ,ISNULL(AppraisalsRecordedCount.AppraisalValueRecorded,0) AS AppraisalWithValueRecorded
             ,0 As AppraisalsBumped
             ,0 AS TotalBumped
             ,0 AS AverageBump
             ,0 AS AverageChangeFromInitialToLast
             FROM 
				#InitialAppraisers IA   
				RIGHT JOIN @BusinessUnit BU on IA.BusinessUnitId=BU.BusinessUnitId    
				LEFT JOIN
				(
					select IA.BusinessUnitId,COUNT(IA.AppraisalID) AS TradesAnalyzed					
					from #InitialAppraisers IA      
					WHERE IA.AppraiserName=@NoAppraiser           
					GROUP By BusinessUnitId
				) TradesAnalyzedCount
				ON BU.BusinessUnitID=TradesAnalyzedCount.BusinessUnitId						
				LEFT JOIN
				(
					select IA.BusinessUnitId,COUNT(IA.AppraisalID) AS AppraisalValueRecorded					
					from #InitialAppraisers IA
					Where ISNULL(IA.InitialValue,0)>0
					AND IA.AppraiserName=@NoAppraiser
					GROUP By BusinessUnitId
				)AppraisalsRecordedCount 
				ON BU.BusinessUnitID=AppraisalsRecordedCount.BusinessUnitId 
				
		END
		ELSE
		BEGIN
			SELECT DISTINCT BU.BusinessUnitId             
             ,ISNULL(TradesAnalyzedCount.TradesAnalyzed,0) AS TradeAnalyzed
             ,ISNULL(AppraisalsRecordedCount.AppraisalValueRecorded,0) AS AppraisalWithValueRecorded
             ,0 As AppraisalsBumped
             ,0 AS TotalBumped
             ,0 AS AverageBump
             ,0 AS AverageChangeFromInitialToLast
             FROM 
				#InitialAppraisers IA 
				RIGHT JOIN @BusinessUnit BU on IA.BusinessUnitId=BU.BusinessUnitId      
				LEFT JOIN
				(
					select IA.BusinessUnitId,COUNT(IA.AppraisalID) AS TradesAnalyzed					
						from #InitialAppraisers IA  
						WHERE IA.AppraiserName=@LocalAppraiserName               
						GROUP By BusinessUnitId
				) TradesAnalyzedCount
				ON BU.BusinessUnitID=TradesAnalyzedCount.BusinessUnitId						
				LEFT JOIN
				(
					select IA.BusinessUnitId,COUNT(IA.AppraisalID) AS AppraisalValueRecorded					
					from #InitialAppraisers IA
					Where ISNULL(IA.InitialValue,0)>0
					AND IA.AppraiserName=@LocalAppraiserName
					GROUP By BusinessUnitId
				)AppraisalsRecordedCount 
				ON BU.BusinessUnitID=AppraisalsRecordedCount.BusinessUnitId 
				
		END
    END    
    
    DROP TABLE #BumpedAppraisalIds    
    DROP TABLE #InitialAppraisers
    DROP TABLE #AppraisalValues 
	
END







GO

