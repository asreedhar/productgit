IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'dbo.InTransitInventoryDelete#Insert')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE dbo.InTransitInventoryDelete#Insert
GO
-- =============================================
-- Author:		<Author,,AJaffer>
-- Create date: <7/10/2013>
-- Description:	<Delete inventory for DMSI Tool Kit Website>
-- =============================================
CREATE PROCEDURE dbo.InTransitInventoryDelete#Insert
	-- Add the parameters for the stored procedure here
    @BusinessUnitID INT
      --  The business unit code of the dealership to wipe data from.  Code is used  
      --  instead of ID, to make sure you're specifying the correct dealership.  
    ,
    @InventoryID INT 
	-- Delete an individual inventory item.  Useful for deletes during 
	-- operational hours      
    ,
    @UserName VARCHAR(50) = NULL --system_user
AS 
    BEGIN
        SET NOCOUNT ON;
        IF ( @UserName IS NULL ) 
            SET @UserName = SYSTEM_USER
        IF ( @BusinessUnitID IS NULL
             OR @InventoryID IS NULL
           ) 
            BEGIN
                RAISERROR('Please supply both @BusinessUnitID and @InventoryID parameters.',15,1) 
                RETURN 1
            END
        ELSE 
            BEGIN
			
                BEGIN TRANSACTION
                INSERT  INTO [IMT].[dbo].[InTransitInventoryDelete]
                        ( [InventoryId] ,
                          [BusinessUnitId] ,
                          [UserName] ,
                          [Vin] ,
                          [StockNumber]
			            )
                        SELECT  i.Inventoryid ,
                                i.BusinessUnitID ,
                                @UserName ,
                                v.VIN ,
                                i.StockNumber
                        FROM    imt..inventory i
                                JOIN imt..vehicle v ON i.vehicleid = v.vehicleid
                        WHERE   businessunitid = @BusinessUnitID
                                AND inventoryactive = 1
                                AND inventorytype = 2
                                AND InventoryStateID = 2
                                AND InventoryID = @InventoryID
			
                IF ( @@ROWCOUNT = 1 ) 
                    BEGIN 
                        COMMIT TRANSACTION 
                    END
                ELSE 
                    BEGIN
                        ROLLBACK TRANSACTION
                        RAISERROR('Invalid @BusinessUnitID and @InventoryID combination.  Verify that vehicle is in-transit and active.', 15, 1)
                        RETURN 1
                    END			
					
					
            END
	
    END
