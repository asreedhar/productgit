SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Extract].[DealerConfiguration#Insert]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Extract].[DealerConfiguration#Insert] AS SELECT 1')
GO

ALTER PROCEDURE [Extract].[DealerConfiguration#Insert]
@DealerId		INT,
@DestinationName	VARCHAR(100),
@ExternalIdentifier	VARCHAR(30),
@StartDate		SMALLDATETIME,
@EndDate		SMALLDATETIME = NULL,
@Active			BIT,
@InsertUser		VARCHAR(100)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Extract.DealerConfiguration#Insert.PRC,v 1.5 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * saves
 * 
 * Parameters
 * ----------
 *
 * @DealerId		- The dealer id
 * @DestinationName 	- name of the place the extract is going to be sent
 * @ExternalIdentifier	- The ID the @DestinationName uses as their DealerID
 * @StartDate 		- The Date to start sending the extract
 * @Active 		- Flag indicating whether or not to send the extract
 * @InsertUser		- User who last updated the DealerConfiguration
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @DealerId is null
 * 50106 - @DealerId does not belong to a known dealer
 * 50100 - @DestinationName is null
 * 50106 - @DestinationName does not belong to a known dealer
 * 50100 - @ExternalIdentifier is null
 * 50100 - @StartDate is null
 * 50100 - @Active is null
 * 50100 - @InsertUser is null
 *
 * History
 * -------
 * BYF 	05/07/2009	Create the proc.
 * WGH	09/09/2009	Updated IncludeHistorical to default to value in
 *			Extract.Destination
 *
 * Comments
 * --------
 *
 * Transaction checking not necessary.
 * Should the parameter checking be done in the middle tier?  Will this
 * proc ever be called from an entry point other than FLADMIN? 
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @Rowcount	INT
DECLARE @Err		INT
DECLARE @sErrMsg	VARCHAR(255)
DECLARE @bLocalTran	BIT
DECLARE @sProcname	SYSNAME
DECLARE @bTrue		BIT
DECLARE @bFalse		BIT

SELECT	@sProcName = OBJECT_NAME(@@procid),
	@bTrue = 1,
	@bFalse = 0,
	@sErrMsg = ''
SET	@bLocalTran = @bFalse

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

IF (@DealerID IS NULL)
	BEGIN
		RAISERROR (50100,16,1,'DealerID')
		RETURN @@ERROR
	END
	
IF (NOT EXISTS(SELECT 1 FROM dbo.BusinessUnit WHERE BusinessUnitID = @DealerID AND BusinessUnitTypeID = 4))
	BEGIN
		RAISERROR (50106, 16, 1, 'IMT.dbo.BusinessUnit', @DealerID)
		RETURN @@ERROR
	END

IF (@DestinationName IS NULL)
	BEGIN
		RAISERROR (50100,16,1,'DestinationName')
		RETURN @@ERROR
	END
	
IF (NOT EXISTS(SELECT 1 FROM Extract.Destination WHERE [Name] = @DestinationName))
	BEGIN
		RAISERROR (50106, 16, 1, 'Extract.Destination', @DestinationName)
		RETURN @@ERROR
	END
	
IF (@ExternalIdentifier IS NULL)
	BEGIN
		RAISERROR (50100,16,1,'ExternalIdentifier')
		RETURN @@ERROR
	END

IF (@ExternalIdentifier IS NULL)
	BEGIN
		RAISERROR (50100,16,1,'ExternalIdentifier')
		RETURN @@ERROR
	END

IF (@StartDate IS NULL)
	BEGIN
		RAISERROR (50100,16,1,'StartDate')
		RETURN @@ERROR
	END
	
IF (@EndDate IS NOT NULL AND @EndDate < @StartDate)
	BEGIN
		RAISERROR (N'EndDate must be on or after StartDate', 16, 1)
		RETURN @@ERROR
	END

IF(@Active IS NULL)
	BEGIN
		RAISERROR (50100,16,1,'Active')
		RETURN @@ERROR
	END
	
IF(@InsertUser IS NULL)
	BEGIN
		RAISERROR (50100,16,1,'InsertUser')
		RETURN @@ERROR
	END

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

DECLARE @IncludeHistorical BIT

SELECT	@IncludeHistorical = DefaultIncludeHistorical
FROM	Extract.Destination
WHERE	Name = @DestinationName

INSERT  
INTO	Extract.DealerConfiguration
        (BusinessUnitID,
         DestinationName,
         ExternalIdentifier,
         StartDate,
         EndDate,
         Active,
         IncludeHistorical,
         InsertDate,
         InsertUser,
         UpdateDate,
         UpdateUser
        )
VALUES  (@DealerID,
         @DestinationName,
         @ExternalIdentifier,
         @StartDate,
         @EndDate,
         @Active,
         @IncludeHistorical,
         GETDATE(),
         @InsertUser,
         GETDATE(),
         @InsertUser
        )

SELECT @Rowcount = @@ROWCOUNT, @Err = @@ERROR
IF @Err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Row Count
------------------------------------------------------------------------------------------------

IF @Rowcount <> 1
BEGIN
	RAISERROR (50200,16,1,@Rowcount)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------
-- Success Exit
------------------------------------------------------------------------------------------------
--all commands successful
IF(@bLocalTran = @bTrue)
	BEGIN
		COMMIT TRAN @sProcName
	END

--exit the procedure
RETURN 0
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

--Error encountered
Failed:
IF (@bLocalTran = @bTrue)
	BEGIN
		ROLLBACK TRAN @sProcName
	END
IF (@sErrMsg = '')
	BEGIN
		SET @sErrMsg = 'Unspecified Error.'
	END
EXEC sp_LogEvent @Log_Code = 'E', @sp_Name = @sProcName, @Log_Text = @sErrMsg, @Originating_Log_Id = null
RETURN @Err

GO
