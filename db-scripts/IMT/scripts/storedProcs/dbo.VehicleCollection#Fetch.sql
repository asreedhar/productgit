IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.VehicleCollection#Fetch') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE dbo.VehicleCollection#Fetch  AS SELECT 1')
GO



ALTER PROCEDURE [dbo].[VehicleCollection#Fetch]
	@DealerID INT,
	@VehicleEntityTypeID TINYINT
AS

/* --------------------------------------------------------------------
 * 
 * 
 * Summary
 * -------
 * 
 *	Return a list of vehicles for the dealer of the given type.
 *
 *      Parameters:
 *		DealerId INT
 *		VehicleEntityTypeId INT
 *
 *
 * Exceptions
 * ----------
 * 
 *	 Validation
 *		50100 DealerId	is null
 *		50106 DealerId  does not exist
 *		50100 VehicleEntityTypeId is null
 *		50106 VehicleEntityTypeId does not exist?

 *
 *
 *
 *	Returns ResultSets:
 *		Vin CHAR(17) NOT NULL
 *
 * History
 * ----------
 * 
 * MAK	06/15/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON
------------------------------------------------------------------------------------------------
--  Validate Parameters
------------------------------------------------------------------------------------------------
DECLARE @err INT

EXEC  @err =dbo.ValidateParameter_DealerID @DealerId
IF (@err <> 0) GOTO Failed

EXEC  @err =Carfax.ValidateParameter_VehicleEntityTypeID @VehicleEntityTypeID
IF (@err <> 0) GOTO Failed

------------------------------------------------------------------------------------------------
-- Result Set table
------------------------------------------------------------------------------------------------
 DECLARE @Vehicles TABLE
	(VIN	CHAR(17)	NOT NULL)

IF @VehicleEntityTypeID =1
BEGIN
	INSERT
	INTO	@Vehicles
		(VIN)
	SELECT  V.VIN
	FROM	dbo.Inventory I
	JOIN	dbo.Vehicle V
	ON	I.VehicleID =V.VehicleID
	WHERE	I.BusinessUnitID=@DealerID
	AND	I.InventoryActive =1
	AND	I.InventoryType =2

END
ELSE IF @VehicleEntityTypeID =2
BEGIN
	DECLARE @DaysBack INT
	
	SELECT  @DaysBack =SearchAppraisalDaysBackThreshold * -1
	FROM	dbo.DealerPreference
	WHERE	BusinessUnitID =@DealerID
	
	
	INSERT
	INTO	@Vehicles
		(VIN)
	SELECT  V.VIN
	FROM	dbo.Appraisals A
	JOIN	dbo.Vehicle V
	ON	A.VehicleID =V.VehicleID
	JOIN	dbo.AppraisalActions AA
	ON	A.AppraisalID =AA.AppraisalID
	WHERE	A.BusinessUnitID=@DealerID
	AND	A.AppraisalStatusID =1 -- Active
	AND	A.AppraisalTypeID = 1 -- Trade-IN
	AND	AA.AppraisalActionTypeID = 5-- Decide later Action Type
	AND	A.DateCreated BETWEEN DATEADD(d,@DaysBack,GETDATE()) and GETDATE()

END


------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Vehicles
RETURN 0
	
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO