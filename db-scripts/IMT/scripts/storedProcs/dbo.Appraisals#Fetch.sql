IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.Appraisals#Fetch') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE dbo.Appraisals#Fetch AS SELECT 1')
GO
 
ALTER PROCEDURE [dbo].[Appraisals#Fetch]
	@AppraisalID	INT
AS

/* --------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * Returns appraisal details from IMT.dbo.Appraisals.
 * 
 * Parameters
 * ----------
 *
 * @AppraisalID   - Primary key.
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @AppraisalID is null
 * 50106 - Record with AppraisalID does not exist
 * 50200 - Expected one Row
 *
 * History
 * ----------
 * 
 * CGC	11/29/2010	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

IF @AppraisalID IS NULL
BEGIN
	RAISERROR (50100,16,1,'Id')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Declare Result Set
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE
		(
			ID				INT NOT NULL,
			BusinessUnitID	INT NOT NULL,
			VehicleID		INT NOT NULL,
			VIN				VARCHAR(17) NOT NULL,
			Mileage			INT NOT NULL,
			DealType		INT NOT NULL
		)

------------------------------------------------------------------------------------------------
-- Populate Result Set
------------------------------------------------------------------------------------------------

INSERT INTO @Results
(
	ID,
	BusinessUnitID,
	VehicleID,
	VIN,
	Mileage,
	DealType
)
SELECT	
	ID				= A.AppraisalID,
	BusinessUnitID	= A.BusinessUnitID,
	VehicleID		= A.VehicleID,
	VIN				= V.VIN,
	Mileage 		= A.Mileage,
	DealType		= CASE WHEN A.AppraisalTypeID = 1 THEN 2 WHEN A.AppraisalTypeID = 2 THEN 1 ELSE 0 END
FROM
	dbo.Appraisals A
JOIN
	dbo.Vehicle V ON A.VehicleID = V.VehicleID
WHERE	
	A.AppraisalID = @AppraisalID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Result Set
------------------------------------------------------------------------------------------------

IF @rc = 0
BEGIN
	RAISERROR (50106,16,1,'ID',@AppraisalId)
	RETURN @@ERROR
END

IF @rc > 1
BEGIN
	RAISERROR (50200,16,1,'ID',@AppraisalId)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO