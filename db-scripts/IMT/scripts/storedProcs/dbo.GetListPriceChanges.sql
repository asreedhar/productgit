 IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.GetListPriceChanges') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.GetListPriceChanges
GO

CREATE PROCEDURE dbo.GetListPriceChanges
	@begin_time datetime,
	@end_time datetime
	
AS

		DECLARE @begin_lsn BINARY(10)
		  , @end_lsn BINARY(10)
		  , @min_lsn BINARY(10)
		  , @max_lsn BINARY(10)
		  
		DECLARE @lookback_time datetime
		 
		       
		SET @begin_lsn = [IMT].[sys].[fn_cdc_map_time_to_lsn]('smallest greater than', @begin_time);
		SET @end_lsn = [IMT].[sys].[fn_cdc_map_time_to_lsn]('largest less than or equal', @end_time);

		SET @min_lsn = [IMT].[sys].[fn_cdc_get_min_lsn]('dbo_inventory')
		SET @max_lsn = [IMT].[sys].[fn_cdc_get_max_lsn]()
		
		IF @begin_lsn < @min_lsn 
			SET @begin_lsn = @min_lsn
		                    
		IF @end_lsn > @max_lsn 
			SET @end_lsn = @max_lsn

		DECLARE @listPriceOrdinal INT
		       
		SET @listPriceOrdinal = [IMT].[sys].[fn_cdc_get_column_ordinal]('dbo_Inventory', 'ListPrice')
		 
		DECLARE @inventoryIds TABLE
			(
			  [InventoryID] INTEGER NOT NULL
									PRIMARY KEY
			)

		IF @begin_lsn < @end_lsn 
			begin
				INSERT  INTO @inventoryIds
						( [InventoryID] 
						)
						SELECT  [a].[InventoryID]
						FROM    [IMT].[cdc].[fn_cdc_get_all_changes_dbo_Inventory](@begin_lsn, @end_lsn, 'all update old') a
						GROUP BY [a].[InventoryID]
						HAVING  MIN([a].[ListPrice]) != MAX([a].[ListPrice])
			 
				SELECT  DISTINCT
						[i].[BusinessUnitID]
					  , [i].[InventoryID]
					  , [i].[ListPrice]
				FROM    @inventoryIds a
						INNER JOIN [IMT].[dbo].[Inventory] i ON [a].[InventoryID] = [i].[InventoryID]
						INNER JOIN [IMT].[dbo].[DealerUpgrade] du ON [du].[DealerUpgradeCD] BETWEEN 23 AND 26 -- max / merch / website upgrade
																	 AND [du].[EffectiveDateActive] = 1
																	 AND [i].[BusinessUnitID] = [du].[BusinessUnitID]
			
			end
GO

GRANT EXECUTE, VIEW DEFINITION on dbo.GetListPriceChanges to [Firstlook]
GO
