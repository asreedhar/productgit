

/****** Object:  StoredProcedure [dbo].[Carfax#ReportAvailability]    Script Date: 08/07/2015 07:52:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Carfax#ReportAvailability]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Carfax#ReportAvailability]
GO


/****** Object:  StoredProcedure [dbo].[Carfax#ReportAvailability]    Script Date: 08/07/2015 07:52:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE  [dbo].[Carfax#ReportAvailability] 

@Vin VARCHAR(36)

AS
BEGIN

	SET NOCOUNT ON;
	DECLARE @count int;
	DECLARE @countReport int;
	DECLARE @Avail bit=1;
SELECT @count=count(*) FROM Carfax.Vehicle V
JOIN Carfax.Request R ON V.VehicleID =R.VehicleID
WHERE V.VIN=@Vin

SELECT @countReport=count(*) FROM Carfax.Vehicle V
JOIN Carfax.Request R ON V.VehicleID =R.VehicleID
JOIN Carfax.Report CR ON CR.RequestID=R.RequestID
WHERE V.VIN=@Vin

IF (@count>0 AND @countReport=0)
SET @Avail=0

SELECT @Avail AS Avail
END

GO


