IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[BusinessUnit#Deactivate]')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE [dbo].[BusinessUnit#Deactivate]
GO

CREATE PROCEDURE [dbo].[BusinessUnit#Deactivate]
    (
      @BusinessUnitId INT
    , @UserName VARCHAR(200)
    )
AS 
    BEGIN
        BEGIN TRANSACTION

-- What is the AULTEC ID.  It is required for submission back to AULTec in the cancellation request.
--  AND COLLECT FLOID(S) ASSOCIATED WITH STORE FOR SIS NOTIFICATION.  This is required for cancellation request to SIS.
        SELECT  [DC].[DestinationName]
              , [dc].[ExternalIdentifier]
			  , bu.[BusinessUnitShortName] AS DealerShortName
        FROM    [IMT].[Extract].[DealerConfiguration] AS DC
				INNER JOIN IMT.[dbo].[BusinessUnit] bu ON [DC].[BusinessUnitID] = [bu].[BusinessUnitID]
        WHERE   [dc].[BusinessUnitID] = @BusinessUnitId
                AND [DC].[DestinationName] = 'AULTec'
        UNION ALL
        SELECT  'SIS'
              , [c].[UserName]
			  , bu.[BusinessUnitShortName]
        FROM    [SIS].[Configuration].[Credential] c
                JOIN [SIS].[Configuration].[Connection] cc ON [c].[id] = [cc].[siscredentialid]
                JOIN [SIS].[Configuration].[DealerEnvironmentConnection] dec ON [dec].[connectionid] = [cc].[id]
                JOIN [SIS].[configuration].[DealerEnvironment] de ON [dec].[dealerenvironmentid] = [de].[id]
                JOIN [IMT].[dbo].[BusinessUnit] bu ON [de].[DealerId] = [bu].[BusinessUnitID]
        WHERE   [de].[DealerId] = @BusinessUnitId
                AND [de].[Environmentid] = 1   

		-- TURN OFF EXTERNAL FEEDs LIKE AUTLEC.
        UPDATE  dc
        SET     [dc].[UpdateUser] = @UserName
              , [dc].[Active] = 0
              , [dc].[UpdateDate] = GETDATE()
              , [dc].[EndDate] = GETDATE()
        FROM    [IMT].[Extract].[DealerConfiguration] dc
        WHERE   [dc].[BusinessUnitID] = @BusinessUnitId
                AND [dc].[Active] = 1
                          
		-- TURN OFF SIS EXTRACTION if it is an SIS dealer.
        UPDATE  de
        SET     [de].[ExecuteDailyLoad] = 0
        FROM    [SIS].[Configuration].[dealerenvironment] de
        WHERE   [de].[dealerid] = @BusinessUnitId
                AND [de].[ExecuteDailyLoad] = 1

        UPDATE  bu
        SET     [bu].[Active] = 0
        FROM    [IMT].[dbo].[BusinessUnit] bu
        WHERE   [bu].[BusinessUnitID] = @BusinessUnitId
                AND [bu].[Active] = 1
		
        ROLLBACK TRANSACTION
    END
GO