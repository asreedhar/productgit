SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PopulateDiscountRateAndTimeHorizon]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[PopulateDiscountRateAndTimeHorizon]
GO


CREATE PROCEDURE dbo.PopulateDiscountRateAndTimeHorizon
--------------------------------------------------------------------------------------------
--
--	"R and T Processor"
--
--	Calculates the discount rate and time horizon values and save to the dealer
--	valuation table.
--
---Parmeters--------------------------------------------------------------------------------
--
@Mode			bit = 0,		-- 0 = Full, 1 = Incremental
@BaseDate 		datetime = null,	-- Optional; defaults to current date
@BusinessUnitID		int = null,		-- Optional filtering Business Unit,
@RunDayOfWeekFilter	bit = 0, 		-- Optional; filter dealers by run day of 
						-- week
@Debug			bit = 0
--
---History----------------------------------------------------------------------------------
--	
--	WGH	12/19/2005	Genesis
--		05/10/2006	Added RunDayOfWeek filter
--	BYF	09/07/2006	Updated Composite Basis period call per FE82.
--	WGH	01/15/2009	Removed @temps, updated insert and error handling
--		06/03/2010	Added NOLOCKs to BU set INSERT	
--------------------------------------------------------------------------------------------

as

--------------------------------------------------------------------------------------------
--	Initialize
--------------------------------------------------------------------------------------------

SET NOCOUNT ON 

DECLARE @err 		int,
	@rc		int,
	@msg		varchar(255),
	@proc_name	varchar(128),
	@ParentLogID	INT
    	
SET @BaseDate = isnull(@BaseDate,dbo.ToDate(getdate()))

SET @proc_name = OBJECT_NAME(@@PROCID)
SET @msg = 'Started, Base Date = ' + CAST(@BaseDate AS VARCHAR) + ', Mode = ' + CAST(@Mode AS VARCHAR) + ISNULL(', Business Unit ID = ' + CAST(@BusinessUnitID AS VARCHAR),'')
EXEC dbo.sp_LogEvent 'I', @proc_name, @msg, @ParentLogID OUTPUT

BEGIN TRY

	--------------------------------------------------------------------------------------------
	SET @msg = 'Determine set of business units for R&T calculation'
	--------------------------------------------------------------------------------------------

	CREATE TABLE #BusinessUnits (BusinessUnitID INT)

	IF @BusinessUnitID IS NOT NULL
		INSERT
		INTO	#BusinessUnits (BusinessUnitID)
		SELECT	@BusinessUnitID
	ELSE 

		INSERT
		INTO	#BusinessUnits (BusinessUnitID)

		SELECT	DISTINCT BU.BusinessUnitID
		FROM	dbo.BusinessUnit BU
			JOIN dbo.Inventory I WITH (NOLOCK) ON BU.BusinessUnitID = I.BusinessUnitID
			JOIN dbo.VehicleSale VS WITH (NOLOCK) ON I.InventoryID = VS.InventoryID
			JOIN dbo.GetCompositeBasisPeriod(null,@BaseDate,'CoreModelYearAllocation') CBP 
					ON I.BusinessUnitID = CBP.BusinessUnitID AND CBP.Forecast = 0 AND cast(convert(varchar(10),VS.DealDate,101) as datetime) BETWEEN CBP.BeginDate AND CBP.EndDate
			JOIN dbo.DealerPreference DP ON BU.BusinessUnitID = DP.BusinessUnitID AND (@RunDayOfWeekFilter = 0 OR DP.RunDayOfWeek = DATEPART(dw, @BaseDate))						
			
		WHERE	@Mode = 0
			AND BU.Active = 1
			OR NOT EXISTS (	SELECT	1						-- INCREMENTAL MODE, MAKE SURE THERE IS NOT AN EXISTING ROW
					FROM	dbo.tbl_DealerValuation DV				-- SHOULD THERE BE A DATE RANGE TO CHECK?
					WHERE	BU.BusinessUnitID = DV.DealerID
					)

	--------------------------------------------------------------------------------------------
	SET @msg = 'Insert into tbl_DealerValuation'
	--------------------------------------------------------------------------------------------

	IF @Debug = 1
		SELECT	BusinessUnitID
		FROM	#BusinessUnits
	ELSE

		WITH DealerValuation (DealerID, DiscountRate, TimeHorizon, BaseDate)
		AS
		(
		SELECT	BU.BusinessUnitID, 
			dbo.GetDiscountRateTimeHorizonValue(BU.BusinessUnitID, 1, @BaseDate),
			dbo.GetDiscountRateTimeHorizonValue(BU.BusinessUnitID, 2, @BaseDate),
			@BaseDate
		FROM	#BusinessUnits BU
		)

		INSERT
		INTO	dbo.tbl_DealerValuation (DealerID , DiscountRate, TimeHorizon, Date)
		
		SELECT	DealerID, 
			DiscountRate,
			TimeHorizon,
			BaseDate
		FROM	DealerValuation
		WHERE	NOT DiscountRate IS NULL
			AND NOT TimeHorizon IS NULL


	SET @rc = @@ROWCOUNT

	SET @msg = @msg + ' (' + cast(@rc as varchar) + ' rows)'
	EXEC dbo.sp_LogEvent 'I', @proc_name, @msg, @ParentLogID

	EXEC dbo.sp_LogEvent 'I', @proc_name, 'Completed without error', @ParentLogID
END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler @ParentLogID
	
END CATCH

GO
