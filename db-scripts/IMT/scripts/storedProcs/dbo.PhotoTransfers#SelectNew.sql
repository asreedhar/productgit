USE [IMT]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PhotoTransfers#SelectNew]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[PhotoTransfers#SelectNew]
GO

/*
 * Returns all transfers (that have not already been processed) between participating dealer groups regardless of active state of source inventory.
 *
 */
CREATE PROCEDURE [dbo].[PhotoTransfers#SelectNew]
AS
BEGIN

	SET NOCOUNT ON;
	
	-- Fetch destination inventory i.e. the current inventory

	select vehicleId, max(inventoryId) as 'inventoryId'	-- current inventory ID for each vehicle
	into #dst
	from inventory inv
	join businessUnitRelationship bur on bur.businessUnitId = inv.businessUnitId
	join photoTransferGroups grp on grp.groupBusinessUnitId = bur.parentId	and grp.enabled=1	-- for dealerships participating in photo tranfers
	group by vehicleId
	having count(*) > 1

	-- Next get source inventory i.e. the inventory previous to the current 

	select vehicleId, max(inventoryId) as 'inventoryId'	-- last active inventory ID
	into #src
	from imt.dbo.inventory inv
	join businessUnitRelationship bur on bur.businessUnitId = inv.businessUnitId
	join photoTransferGroups grp on grp.groupBusinessUnitId = bur.parentId and grp.enabled=1		-- for dealerships participating in photo tranfers
	where inventoryId not in (select inventoryId from #dst)
	group by vehicleId;

	-- Now we have two latest inventory records for each vehicle. The most recent is in #dst and the previous in #src.

	select
		v.vehicleId 'vehicleId', v.vin 'vin', 
		srcInv.BusinessUnitID 'srcBusinessUnitId', srcBu.BusinessUnitCode 'srcBusinessUnitCode', srcInv.InventoryID 'srcInventoryId', srcInv.StockNumber 'srcStockNumber',
		dstInv.BusinessUnitID 'dstBusinessUnitId', dstBu.BusinessUnitCode 'dstBusinessUnitCode', dstInv.InventoryID 'dstInventoryId', dstInv.StockNumber 'dstStockNumber'
	from #src
	join #dst on #dst.vehicleId = #src.vehicleId -- for same vehicle
	join tbl_vehicle v on v.vehicleId = #src.vehicleId
	join inventory srcInv on srcInv.inventoryId = #src.inventoryId
	join businessUnit srcBu on srcBu.businessUnitId = srcInv.businessUnitId
	join businessUnitRelationship srcBur on srcBur.businessUnitId = srcInv.businessUnitId
	join inventory dstInv on dstInv.inventoryId = #dst.inventoryId
	join businessUnit dstBu on dstBu.businessUnitId = dstInv.businessUnitId
	join businessUnitRelationship dstBur on dstBur.businessUnitId = dstInv.businessUnitId
	left join photoTransfers pt on pt.dstInventoryId = #dst.inventoryId 
	where 
		pt.dstInventoryId is null					-- not already processed
		and #src.inventoryId != #dst.inventoryId	-- don't think can really but happen, source cannot contain same inventory as destination	
		/* and srcInv.inventoryActive=0 */		-- don't require source inventory to be inactive, sometimes it doesn't promptly get marked as inactive
		and dstInv.inventoryActive=1			-- do require destination inventory to be active? Otherwise we will transfer photos for cars that have already left the dealer.
		and srcBu.active = 1 and dstBu.active=1	-- do require both dealers to be active
		and srcBur.parentId = dstBur.parentId	-- both dealers belong to same group!
	order by v.vehicleId;

	drop table #dst;
	drop table #src;
    
END
