/****** Object:  StoredProcedure [dbo].[ClosingRatesByAppraiserDetail]    Script Date: 05/17/2014 17:30:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClosingRatesByAppraiserDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ClosingRatesByAppraiserDetail]
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Pradnya Sawant
-- Create date: 2014-05-16
-- Last Modified By : Vivek Rane
-- Last Modified Date : 2014-06-06
-- Description:	This stored procedure returns the Appraisal Closing Data by Appraiser.
-- =============================================


CREATE PROCEDURE [dbo].[ClosingRatesByAppraiserDetail]
	@Period INT,
    @SelectedDealerId VARCHAR(1000)    
   
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets FROM
	-- INterferINg with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	DECLARE @DateField date
	,@EndDate date
	,@LocalPeriod INT 
	,@LocalSelectedDealerId VARCHAR(1000)   
	
	Select @LocalPeriod=@Period,@LocalSelectedDealerId=@SelectedDealerId;
	
	DECLARE @Name VARCHAR(1000)='firstName lastName';

	Select @DateField=StartDate,@EndDate=EndDate FROM IMT.dbo.GetDatesFromPeriodId(@LocalPeriod,1);

	DECLARE  @BusinessUnit TABLE (BusinessUnitId INT,BusinessUnit varchar(100))

	INSERT into @BusinessUnit SELECT CAST(NULLIF(S.Value,'') AS INT),BU.BusinessUnit FROM IMT.dbo.Split(@LocalSelectedDealerId,',') S  
	INNER JOIN IMT.dbo.BusinessUnit BU ON BU.BusinessUnitID=S.Value
	WHERE NULLIF(@LocalSelectedDealerId,'') IS NOT NULL AND LTRIM(RTRIM(S.Value))<>'';
		
		
	--AvgImmediateCost Calculation
	SELECT  BU.BusinessUnitId AS BusinessUnitId
	,CASE WHEN ISNULL(TCR.AppraiserName,@Name)=@Name OR TCR.AppraiserName='' OR TCR.AppraiserName = '-' OR ASCII(LTRIM(RTRIM(TCR.AppraiserName))) = 160
		THEN 'No Appraiser' ELSE TCR.AppraiserName END AS AppraiserName
	,sum(VS.FrontEndGross)/count(I.INventoryID) As AvgImmediateWholesale 
	INTO #AvgImmediateCost
	FROM  [FLDW].dbo.VehicleSale AS VS
	JOIN  [FLDW].dbo.INventory AS I  ON I.INventoryID = VS.INventoryID
	JOIN  [FLDW].dbo.Vehicle AS v ON I.BusinessUnitId = v.BusinessUnitId AND I.VehicleID = v.VehicleID
	JOIN  [FLDW].dbo.Appraisal_F A ON I.BusinessUnitId = A.BusinessUnitId AND V.vehicleId = A.vehicleId 
	JOIN [HAL].dbo.TradeClosingRateReport TCR on TCR.AppraisalID = A.AppraisalID
	INNER JOIN IMT.dbo.DealerPreference D on D.BusinessUnitID = TCR.BusinessUnitID
	INNER JOIN @BusinessUnit BU on BU.BusinessUnitId=VS.BusinessUnitID
	WHERE 
		I.DaysToSale < 30
--		AND I.TradeOrPurchase = 2
		AND I.InventoryType = 2
		AND TCR.AppraisalDate <= I.InventoryReceivedDate 
		AND cast(A.datemodified AS date) >=  cast(DATEADD(DD,-D.AppraisalLookBackPeriod,I.InventoryReceivedDate) as date) --and I.InventoryReceivedDate 
		AND  CAST(TCR.AppraisalDate AS DATE)  BETWEEN @DateField AND @EndDate
		AND VS.SaleDescription = 'W'
		AND VS.DealDate >= @DateField
	group by BU.BusinessUnitId
	,CASE WHEN ISNULL(TCR.AppraiserName,@Name)=@Name OR TCR.AppraiserName='' OR TCR.AppraiserName = '-' OR ASCII(LTRIM(RTRIM(TCR.AppraiserName))) = 160 
		THEN 'No Appraiser' ELSE TCR.AppraiserName END ;

		
	--Calculate Total Closed
	SELECT DISTINCT BU.BusinessUnitID
	,CASE WHEN ISNULL(TCR.AppraiserName,@Name)=@Name OR TCR.AppraiserName='' OR TCR.AppraiserName = '-' OR ASCII(LTRIM(RTRIM(TCR.AppraiserName))) = 160
		 THEN 'No Appraiser' ELSE TCR.AppraiserName END AS AppraiserName
	,ClosedAppraisals.Closed AS Closed
	,TotalAppraisals.Total AS Total
	,CASE WHEN TotalAppraisals.Total=0 THEN 0 ELSE CAST(ClosedAppraisals.Closed AS FLOAT)/ CAST(TotalAppraisals.Total AS FLOAT) END AS AppraisalClosingRate
	,ISNULL(AIC.AvgImmediateWholesale,0) AS AvgImmediateWholesale
	FROM 
	[HAL].dbo.TradeClosingRateReport TCR
	LEFT JOIN
	(SELECT  BU.BusinessUnitID
	,CASE WHEN ISNULL(TCR.AppraiserName,@Name)=@Name OR TCR.AppraiserName='' OR TCR.AppraiserName = '-' OR ASCII(LTRIM(RTRIM(TCR.AppraiserName))) = 160
		THEN 'No Appraiser' ELSE TCR.AppraiserName END AS AppraiserName
	,count(TCR.AppraisalID)as Closed 
	FROM [HAL].dbo.TradeClosINgRateReport TCR
	INNER JOIN IMT.dbo.Appraisals A ON A.AppraisalID=TCR.AppraisalID
	INNER JOIN IMT.dbo.DealerPreference D on D.BusinessUnitID = TCR.BusinessUnitID
	INNER JOIN IMT.dbo.Inventory I on I.BusinessUnitID = A.BusinessUnitID  and I.VehicleID = A.VehicleID 
	LEFT JOIN IMT.MMR.Vehicle MV ON MV.MMRVehicleID=A.MMRVehicleID
	INNER JOIN @BusinessUnit BU on BU.BusinessUnitId=A.BusinessUnitID
	WHERE  
	 A.AppraisalSourceID=1
	 AND  I.InventoryReceivedDate IS NOT NULL
	 AND I.TradeOrPurchase = 2
	AND CAST(AppraisalDate AS DATE) BETWEEN @DateField AND @EndDate
	 AND TCR.AppraisalDate <= I.InventoryReceivedDate 
	--AND TCR.AppraisalDate between DATEADD(DD,-D.AppraisalLookBackPeriod,I.InventoryReceivedDate) and I.InventoryReceivedDate 
	AND cast(A.datemodified AS date) >=  cast(DATEADD(DD,-D.AppraisalLookBackPeriod,I.InventoryReceivedDate) as date) --and I.InventoryReceivedDate 
	GROUP  BY BU.BusinessUnitID
	,CASE WHEN ISNULL(TCR.AppraiserName,@Name)=@Name OR TCR.AppraiserName='' OR TCR.AppraiserName = '-' OR ASCII(LTRIM(RTRIM(TCR.AppraiserName))) = 160
		THEN 'No Appraiser' ELSE TCR.AppraiserName END 
	)ClosedAppraisals
	ON ClosedAppraisals.BusinessUnitID=TCR.BusinessUnitID AND ClosedAppraisals.AppraiserName=CASE WHEN ISNULL(TCR.AppraiserName,@Name)=@Name OR TCR.AppraiserName='' OR TCR.AppraiserName = '-' OR ASCII(LTRIM(RTRIM(TCR.AppraiserName))) = 160 
		THEN 'No Appraiser' ELSE TCR.AppraiserName END 
	INNER JOIN
	--Calculate Total Appraisals
	(
	SELECT BU.BusinessUnitID
	,CASE WHEN ISNULL(TCR.AppraiserName,@Name)=@Name OR TCR.AppraiserName='' OR TCR.AppraiserName = '-' OR ASCII(LTRIM(RTRIM(TCR.AppraiserName))) = 160 
		THEN 'No Appraiser' ELSE TCR.AppraiserName END AS AppraiserName
	,COUNT(TCR.AppraisalID) AS Total
	FROM [HAL].dbo.TradeClosINgRateReport TCR
	INNER JOIN IMT.dbo.Appraisals A ON A.AppraisalID=TCR.AppraisalID
	LEFT JOIN IMT.dbo.DealerPreference D on D.BusinessUnitID = tcr.BusinessUnitID --AND   TCR.AppraisalDate >=DATEADD(DD,-D.AppraisalLookBackPeriod,TCR.InventoryReceivedDate)
	LEFT JOIN  IMT.dbo.Inventory I ON A.BusinessUnitID = I.BusinessUnitID  and A.VehicleID = I.VehicleID  AND I.TradeOrPurchase = 2 AND I.InventoryReceivedDate IS NOT null  AND TCR.AppraisalDate <= I.InventoryReceivedDate   AND cast(A.datemodified AS date) >=  cast(DATEADD(DD,-D.AppraisalLookBackPeriod,I.InventoryReceivedDate) as date) --and I.InventoryReceivedDate --AND TCR.AppraisalDate between DATEADD(DD,-D.AppraisalLookBackPeriod,I.InventoryReceivedDate) and I.InventoryReceivedDate 
	LEFT JOIN IMT.MMR.Vehicle MV ON MV.MMRVehicleID=A.MMRVehicleID 
	INNER JOIN @BusinessUnit BU on BU.BusinessUnitId=A.BusinessUnitID
	WHERE  A.AppraisalSourceID=1
	  AND CAST(AppraisalDate AS DATE) BETWEEN @DateField AND @EndDate
	GROUP  BY BU.BusinessUnitID
	,CASE WHEN ISNULL(TCR.AppraiserName,@Name)=@Name OR TCR.AppraiserName='' OR TCR.AppraiserName = '-' OR ASCII(LTRIM(RTRIM(TCR.AppraiserName))) = 160 
		THEN 'No Appraiser' ELSE TCR.AppraiserName END 
	)TotalAppraisals
	ON TotalAppraisals.BusinessUnitID=TCR.BusinessUnitID AND TotalAppraisals.AppraiserName=CASE WHEN ISNULL(TCR.AppraiserName,@Name)=@Name OR TCR.AppraiserName='' OR TCR.AppraiserName = '-' OR ASCII(LTRIM(RTRIM(TCR.AppraiserName))) = 160 
		THEN 'No Appraiser' ELSE TCR.AppraiserName END 
	RIGHT JOIN @BusinessUnit BU ON BU.BusinessUnitId=TCR.BusinessUnitID
	LEFT JOIN #AvgImmediateCost AIC ON AIC.BusinessUnitID=BU.BusinessUnitID AND AIC.AppraiserName=CASE WHEN ISNULL(TCR.AppraiserName,@Name)=@Name OR TCR.AppraiserName='' OR TCR.AppraiserName = '-' OR ASCII(LTRIM(RTRIM(TCR.AppraiserName))) = 160  THEN 'No Appraiser' ELSE TCR.AppraiserName END 

	DROP Table #AvgImmediateCost

END





GO


