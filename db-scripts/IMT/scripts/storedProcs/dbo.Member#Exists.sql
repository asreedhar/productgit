
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.Member#Exists') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE dbo.Member#Exists AS SELECT 1')
GO
 
ALTER PROCEDURE [dbo].[Member#Exists]
	@UserName	VARCHAR(80)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.Member#Exists.sql,v 1.4 2010/02/10 21:54:23 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns 1 if the Member Exists. 
 * 
 * Parameters
 * ----------
 *
 * @UserName   - user name of the member
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @UserName is null
 * 50200 - Expected one Row
 *
 * History
 * -------
 * MAK	05/29/2009	Create procedure.
 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

IF @UserName IS NULL
BEGIN
	RAISERROR (50100,16,1,'UserName')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Declare Result Set
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE
	([Exists] BIT)

------------------------------------------------------------------------------------------------
-- Populate Result Set
------------------------------------------------------------------------------------------------

IF EXISTS (
	SELECT	1
	FROM	dbo.Member
	WHERE	Login = @UserName
)
	INSERT INTO @Results ([Exists]) VALUES (CAST(1 AS BIT))
ELSE
	INSERT INTO @Results ([Exists]) VALUES (CAST(0 AS BIT))

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

-- there must be one row in the tables

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO