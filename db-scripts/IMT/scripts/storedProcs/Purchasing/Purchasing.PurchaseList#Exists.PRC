SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Purchasing.PurchaseList#Exists') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Purchasing].[PurchaseList#Exists] AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on [Purchasing].[PurchaseList#Exists] to [PurchasingUser]
GO

ALTER PROCEDURE [Purchasing].[PurchaseList#Exists]
( 	@OwnerHandle	VARCHAR(36) )
AS

/* --------------------------------------------------------------------
 * 
 * 
 * Summary
 * -------
 * 
 * Verify list exists for an owner.
 * 
 * Parameters
 * ----------
 *
 *	@OwnerHandle	VARCHAR(36)
 * 
 * Return BIT
 * -------------
 *

 *
 *
 *
 *
 * Exceptions
 * ----------
 *
 *	See ValidateParameter_OwnerHandle 
  *
 * 
 * History
 * ----------
 *
 * 11/05/2009 Create procedures.
 *
 * -------------------------------------------------------------------- */	


SET NOCOUNT ON

DECLARE @rc INT, @err INT

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT 


EXEC @err = [Market].Pricing.ValidateParameter_OwnerHandle   @OwnerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT
IF (@err <> 0) GOTO Failed

DECLARE @List TABLE ([EXISTS] BIT NOT NULL)

INSERT
INTO	@List
		([EXISTS])
SELECT 	CASE WHEN L.ListID IS NOT NULL THEN 1 ELSE 0 END	
FROM	[Market].Pricing.Owner O
LEFT
JOIN	Purchasing.List L ON O.OwnerID =L.OwnerID		
WHERE	O.OwnerID =@OwnerID	

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @List 

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO