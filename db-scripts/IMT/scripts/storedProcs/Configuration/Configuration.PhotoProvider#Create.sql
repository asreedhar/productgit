IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Configuration.PhotoProvider#Create') AND type in (N'P', N'PC'))
	DROP PROCEDURE Configuration.PhotoProvider#Create
GO


CREATE PROC Configuration.PhotoProvider#Create
--------------------------------------------------------------------------------
--
--	Creates a new photo provider for the passed description and DatafeedCode 
--	if it does not exist and returns the PhotoProviderID.  Developed for the 
--	PhotoURL-Standard automated configuration process.
--
---Parameters-------------------------------------------------------------------
--
@ProviderDescription	VARCHAR(50),
@DatafeedCode		VARCHAR(20),
@PhotoStorageCode	TINYINT = 2,
@PhotoProviderID 	INT OUTPUT
--
---History----------------------------------------------------------------------
--
--	WGH	05/15/2010	Initial design/development
--		09/27/2010	Update BUID to proper value (dumb.)
--------------------------------------------------------------------------------

AS
SET NOCOUNT ON

BEGIN TRY
	
	SELECT	@PhotoProviderID = PP.PhotoProviderID
	FROM	dbo.ThirdPartyEntity TPE
		INNER JOIN dbo.PhotoProvider_ThirdPartyEntity PP ON TPE.ThirdPartyEntityID = PP.PhotoProviderID
	WHERE	ThirdPartyEntityTypeID = 6
		AND TPE.Name = @ProviderDescription
		AND PP.DatafeedCode = @DatafeedCode
	
	IF @PhotoProviderID IS NULL BEGIN
	
		BEGIN TRANSACTION
		
		INSERT
		INTO	dbo.ThirdPartyEntity (BusinessUnitID, ThirdPartyEntityTypeID, Name)
		SELECT	BusinessUnitID, 6, @ProviderDescription
		FROM	IMT.dbo.BusinessUnit BU
		WHERE	BusinessUnit = 'Firstlook'
			AND BusinessUnitTypeID = 5
			
		SET @PhotoProviderID = SCOPE_IDENTITY()

		INSERT
		INTO	dbo.PhotoProvider_ThirdPartyEntity (PhotoProviderID, DatafeedCode, PhotoStorageCode)
		SELECT	@PhotoProviderID, @DatafeedCode, @PhotoStorageCode
		
		COMMIT TRANSACTION
	END 	
	
END TRY

BEGIN CATCH

    IF XACT_STATE() <> 0 ROLLBACK TRANSACTION
    
    EXEC dbo.sp_ErrorHandler
    
END CATCH
GO

EXEC dbo.sp_SetStoredProcedureDescription 'Configuration.PhotoProvider#Create',
'Creates a new photo provider for the passed description and DatafeedCode if it does not exist and returns the PhotoProviderID.  Developed for the PhotoURL-Standard automated configuration process.'

GO

GRANT EXECUTE ON [Configuration].[PhotoProvider#Create] TO [FIRSTLOOK\DMSI]