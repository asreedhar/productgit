
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[WindowSticker].[LastPrintDate#Save]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE WindowSticker.LastPrintDate#Save

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE WindowSticker.LastPrintDate#Save
    @InventoryId INT ,
    @TemplateTypeId INT ,
    @LatestPrintDate DATETIME, -- not used but don't know if it should remove it, tureen 5/2/2014
    @TemplateId int = null,
    @AutoPrintTemplate bit = 0,
    @PrintBatchId int = null,
    @PrintLogId int = null OUTPUT
    
    
AS 
    BEGIN

        SET NOCOUNT ON ;
            
        IF NOT EXISTS ( SELECT  1
                        FROM    FLDW.dbo.InventoryActive
                        WHERE   InventoryID = @InventoryId ) 
            BEGIN
                RAISERROR('An invalid value was found in the @InventoryId parameter, value: %d',16,2,@InventoryId)
            END

        IF NOT EXISTS ( SELECT  1
                        FROM    WindowSticker.TemplateType
                        WHERE   TemplateTypeId = @TemplateTypeId ) 
            BEGIN
                RAISERROR('An invalid value was found in the @TemplateTypeId parameter, value: %d',16,2,@TemplateTypeId)
            END
            
            
		-- now just insert
        INSERT  INTO WindowSticker.PrintLog
				( 
					InventoryId ,
					TemplateTypeId ,
					PrintDate,
					TemplateId,
					AutoPrintTemplate
	              )
        VALUES  ( 
					@InventoryId ,
					@TemplateTypeId ,
					GETDATE(),
					@TemplateId,
					@AutoPrintTemplate
                )
                
        SET @PrintLogId = SCOPE_IDENTITY()
                
        -- was it part of a print batch?
        if @PrintBatchId IS NOT NULL
			INSERT INTO WindowSticker.printBatchLog
				(PrintBatchId, PrintLogId)
				VALUES(@PrintBatchId, @PrintLogId)
    
        
        /*
        IF NOT EXISTS ( SELECT  1
                        FROM    WindowSticker.PrintLog
                        WHERE   InventoryId = @InventoryId
                                AND TemplateTypeId = @TemplateTypeId ) 
            BEGIN
	 
                INSERT  INTO WindowSticker.PrintLog
                        ( InventoryId ,
                          TemplateTypeId ,
                          PrintDate
	                  )
                VALUES  ( @InventoryId ,
                          @TemplateTypeId ,
                          GETDATE()
                        )
	 
            END
        ELSE 
            BEGIN
	    
                UPDATE  WindowSticker.PrintLog
                SET     PrintDate = GETDATE()
                WHERE   InventoryId = @InventoryId
                        AND TemplateTypeId = @TemplateTypeId
	
            END
         */

    END



GO