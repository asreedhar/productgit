IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'WindowSticker.PrintBatchReport')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE WindowSticker.PrintBatchReport
GO

CREATE PROCEDURE WindowSticker.PrintBatchReport
					@since datetime = null
AS 
    SET NOCOUNT ON

    BEGIN TRY
    
    
		if @since IS NULL
			SET @since = DATEADD(DAY, -14, GETDATE())
    
    
		SELECT PrintBatch.PrintBatchId, PrintBatch.BusinessUnitId, BusinessUnit.BusinessUnit, PrintBatch.PrintFileName, PrintBatch.CreatedOn, batchCount.VehicleCount, LastEmailSent, LastAccess, LastAccessBy
			FROM WindowSticker.PrintBatch PrintBatch
		 INNER JOIN imt.dbo.BusinessUnit BusinessUnit oN PrintBatch.BusinessUnitID = BUSINESSuNIT.BusinessUnitID
			INNER JOIN (SELECT printBatchID, VehicleCount = COUNT(*)
							FROM WindowSticker.PrintBatchLog
							GROUP BY printBatchID
							) batchCount ON batchCount.PrintBatchId = PrintBatch.PrintBatchId
			LEFT OUTER JOIN (
						SELECT printBatchID, LastEmailSent = MAX(CreatedOn)
							FROM WindowSticker.PrintBatchEmail
							WHERE EmailBounced = 0
							GROUP BY printBatchID
						) lastSent ON PrintBatch.PrintBatchId = lastSent.PrintBatchId
			LEFT OUTER JOIN (
				SELECT PrintBatchAccessLog.printBatchID, LastAccess = PrintBatchAccessLog.AccessON, LastAccessBy = Min(PrintBatchAccessLog.AccessBy)
					FROM WindowSticker.PrintBatchAccessLog PrintBatchAccessLog
					WHERE EXISTS (
									SELECT printBatchID, MAX(AccessOn)
										FROM WindowSticker.PrintBatchAccessLog PrintBatchAccessLog2
										WHERE PrintBatchAccessLog.printBatchID = PrintBatchAccessLog2.printBatchID
										GROUP BY printBatchID
										HAVING PrintBatchAccessLog.AccessOn = MAX(PrintBatchAccessLog2.AccessOn)
								  )
					GROUP BY PrintBatchAccessLog.printBatchID, PrintBatchAccessLog.AccessON
					) lastAccessed ON PrintBatch.PrintBatchId = lastAccessed.PrintBatchId
			WHERE PrintBatch.CreatedOn >= @Since
   			ORDER BY CreatedOn DESC, BusinessUnit
				

    END TRY
    BEGIN CATCH

			DECLARE @ErrorMessage NVARCHAR(4000),
			  @ErrorNumber INT,
			  @ErrorSeverity INT,
			  @ErrorState INT,
			  @ErrorLine INT,
			  @ErrorProcedure NVARCHAR(200) ;

				-- Assign variables to error-handling functions that 
				-- capture information for RAISERROR.
			SELECT @ErrorNumber = ERROR_NUMBER(), @ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE(), @ErrorLine = ERROR_LINE(),
					@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-') ;

				-- Building the message string that will contain original
				-- error information.
			SET @ErrorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' +
					'Message: ' + ERROR_MESSAGE() ;

				-- Raise an error: msg_str parameter of RAISERROR will contain
				-- the original error information.
			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, -- parameter: original error number.
			  @ErrorSeverity, -- parameter: original error severity.
			  @ErrorState, -- parameter: original error state.
			  @ErrorProcedure, -- parameter: original error procedure name.
			  @ErrorLine-- parameter: original error line number.
					)

    END CATCH
GO

