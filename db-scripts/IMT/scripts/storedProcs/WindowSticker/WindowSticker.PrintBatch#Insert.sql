IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'WindowSticker.[PrintBatch#Insert]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE WindowSticker.PrintBatch#Insert
GO


CREATE PROCEDURE WindowSticker.PrintBatch#Insert
					@BusinessUnitId int,
					@PrintFileName varchar(100) = null,
					@MessageId varchar(50) = null,
					@User varchar(80),
					@PrintBatchId int = NULL OUTPUT
AS 
    SET NOCOUNT ON

    BEGIN TRY

		INSERT INTO WindowSticker.PrintBatch
			(
				BusinessUnitId,
				PrintFileName,
				MessageId,
				CreatedOn,
				CreatedBy			
			)
			VALUES
			(
				@BusinessUnitId,
				@PrintFileName,
				@MessageId,
				GETDATE(),
				@User
			)
			
		SET @PrintBatchId = SCOPE_IDENTITY()

    END TRY
    BEGIN CATCH

			DECLARE @ErrorMessage NVARCHAR(4000),
			  @ErrorNumber INT,
			  @ErrorSeverity INT,
			  @ErrorState INT,
			  @ErrorLine INT,
			  @ErrorProcedure NVARCHAR(200) ;

				-- Assign variables to error-handling functions that 
				-- capture information for RAISERROR.
			SELECT  @ErrorNumber = ERROR_NUMBER(), @ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE(), @ErrorLine = ERROR_LINE(),
					@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-') ;

				-- Building the message string that will contain original
				-- error information.
			SELECT  @ErrorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' +
					'Message: ' + ERROR_MESSAGE() ;

				-- Raise an error: msg_str parameter of RAISERROR will contain
				-- the original error information.
			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, -- parameter: original error number.
			  @ErrorSeverity, -- parameter: original error severity.
			  @ErrorState, -- parameter: original error state.
			  @ErrorProcedure, -- parameter: original error procedure name.
			  @ErrorLine-- parameter: original error line number.
					)

    END CATCH

