
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'WindowSticker.[DataPoint#Insert]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE WindowSticker.DataPoint#Insert

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE WindowSticker.DataPoint#Insert
    @ContentAreaId INT ,
    @DataPointKeyId TINYINT = NULL ,
    @Text VARCHAR(1000) = NULL ,
    @Prompt VARCHAR(1000) = NULL ,
    @DataPointId INT OUT
AS 
    SET NOCOUNT ON ;

    BEGIN TRY

        IF @DataPointKeyId IS NOT NULL
            AND NOT EXISTS ( SELECT 1
                             FROM   WindowSticker.DataPointKey
                             WHERE  DataPointKeyId = @DataPointKeyId ) 
            BEGIN
                RAISERROR('An invalid value was found in the @DataPointKeyId parameter, value: %d',16,2,@DataPointKeyId)
            END

        IF ( @Text IS NULL
             AND @DataPointKeyId IS NULL
           ) 
            BEGIN
                RAISERROR('Either @Text or @DataPointKeyId must be non-null',16,2)
            END

        IF ( @Prompt IS NOT NULL
             AND LEN(LTRIM(RTRIM(@Prompt))) = 0
           ) 
            BEGIN
                RAISERROR('@Prompt must be null or non-empty',16,2)
            END

        IF NOT EXISTS ( SELECT  1
                        FROM    WindowSticker.ContentArea
                        WHERE   ContentAreaId = @ContentAreaId ) 
            BEGIN
                RAISERROR('An invalid value was found in the @ContentAreaId parameter, value: %d',16,2,@ContentAreaId)
            END

    -- right now, a content area can have only one datapoint.
        IF EXISTS ( SELECT  1
                    FROM    WindowSticker.ContentAreaDataPoint
                    WHERE   ContentAreaId = @ContentAreaId ) 
            BEGIN
                RAISERROR('The contentarea already contains a datapoint, @ContentAreaId: %d',16,2,@ContentAreaId)
            END

    -- insert the datapoint and link it to the content area.
        INSERT  INTO WindowSticker.DataPoint
                ( DataPointKeyId, Text, Prompt )
        VALUES  ( @DataPointKeyId, @Text, @Prompt )

        SELECT  @DataPointId = SCOPE_IDENTITY()

        INSERT  INTO WindowSticker.ContentAreaDataPoint
                ( ContentAreaId, DataPointId )
        VALUES  ( @ContentAreaId, @DataPointId )
    
    END TRY
    BEGIN CATCH

        EXEC dbo.sp_ErrorHandler

    END CATCH
GO

SET QUOTED_IDENTIFIER OFF
SET ANSI_NULLS OFF