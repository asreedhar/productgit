USE [IMT]

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[WindowSticker].[Template#Share]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE WindowSticker.Template#Share

GO
/****** Object:  StoredProcedure [WindowSticker].[Template#Remove]    Script Date: 03/28/2011 13:27:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [WindowSticker].[Template#Share] 
	@TemplateId INT, 
	@OwnerId INT,
	@User VARCHAR(80)
AS
BEGIN

	SET NOCOUNT ON;
  
        IF NOT EXISTS ( SELECT  1
                        FROM    WindowSticker.Template
                        WHERE   TemplateId = @TemplateId ) 
            BEGIN
                RAISERROR('An invalid value was found in the @TemplateId parameter, value: %d',16,1,@TemplateId)
            END

		IF NOT EXISTS ( SELECT  1
                        FROM    Market.Pricing.Owner
                        WHERE   OwnerID = @OwnerId )
                        
            BEGIN
                RAISERROR('An invalid value was found in the @OwnerId parameter, value: %d',16,1,@OwnerId)
            END


		IF EXISTS ( SELECT 1
					FROM WindowSticker.OwnerTemplate
					WHERE TemplateId = @TemplateId AND OwnerId = @OwnerId)
			-- Record already exists so do nothing
			RETURN


        DECLARE @InsertUserID INT
        EXEC dbo.ValidateParameter_MemberID#UserName @User,
            @InsertUserID OUTPUT

	INSERT INTO [WindowSticker].[OwnerTemplate]
	(OwnerId, TemplateId, InsertUser, InsertDate)
	VALUES (@OwnerId, @TemplateId, @InsertUserId, GETDATE())

	RETURN @@ROWCOUNT
END
GO
