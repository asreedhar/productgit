IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[WindowSticker].[TemplateInfo#FetchByOwner_Type]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE WindowSticker.TemplateInfo#FetchByOwner_Type

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE WindowSticker.TemplateInfo#FetchByOwner_Type
    @OwnerHandle VARCHAR(36) ,
    @TemplateTypeId TINYINT
AS 
    BEGIN

        SET NOCOUNT ON ;


        DECLARE @OwnerEntityTypeID INT ,
            @OwnerEntityID INT ,
            @OwnerID INT
        EXEC Market.Pricing.ValidateParameter_OwnerHandle @OwnerHandle,
            @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT

        IF @OwnerEntityTypeID <> 1 
            BEGIN
                RAISERROR('Only dealers are currently supported.  @OwnerHandle: %s ',16,2,@OwnerHandle)
            END
            
        IF NOT EXISTS ( SELECT  1
                        FROM    WindowSticker.TemplateType
                        WHERE   TemplateTypeId = @TemplateTypeId ) 
            BEGIN
                RAISERROR('An invalid value was found in the @TemplateTypeId parameter, value: %d',16,2,@TemplateTypeId)
            END


        DECLARE @Templates TABLE
            (
              TemplateId INT NOT NULL ,
              TemplateName VARCHAR(100) NOT NULL,
              Shared BIT NOT NULL
            );

      WITH Shared AS
      (
      SELECT   TemplateId ,
                                        COUNT(OwnerId) ownerCount
                               FROM     WindowSticker.OwnerTemplate
                               GROUP BY templateId
                               HAVING   COUNT(OwnerId) > 1  -- template is "shared"
      )
        INSERT  INTO @Templates
                ( TemplateId ,
                  TemplateName,
                  Shared
                )
                SELECT  t.TemplateId ,
                        t.Name,
                        CAST (CASE WHEN shared.TemplateId IS NULL THEN 0 ELSE 1 END AS BIT)
                FROM    WindowSticker.Template t
                        JOIN WindowSticker.OwnerTemplate ot ON ot.TemplateId = t.TemplateId
            LEFT JOIN Shared ON SHARED.templateId = t.templateId
                WHERE   ot.OwnerId = @OwnerId
                        AND t.TemplateTypeId = @TemplateTypeId
                        
                UNION
                SELECT  t.TemplateId ,
                        t.Name,
                        CAST (CASE WHEN shared.TemplateId IS NULL THEN 0 ELSE 1 END AS BIT)
                FROM    WindowSticker.Template t
                        JOIN WindowSticker.OwnerTemplate ot ON ot.TemplateId = t.TemplateId
                  LEFT JOIN Shared ON shared.templateId = t.templateId
                WHERE   ot.OwnerId = 0
                        AND t.TemplateTypeId = @TemplateTypeId


        SELECT  *
        FROM    @Templates

    END

GO
