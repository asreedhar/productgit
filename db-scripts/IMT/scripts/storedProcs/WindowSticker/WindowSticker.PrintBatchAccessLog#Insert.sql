IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'WindowSticker.[PrintBatchAccessLog#Insert]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE WindowSticker.PrintBatchAccessLog#Insert
GO

CREATE PROCEDURE WindowSticker.PrintBatchAccessLog#Insert
					@PrintBatchId int,
					@AccessBy varchar(80),
					@AccessOn smalldatetime = null,
					@PrintBatchAccessLogId int = null OUTPUT
AS 
    SET NOCOUNT ON

    BEGIN TRY
    
		if @AccessOn IS NULL
			SET @AccessOn = GETDATE()
			

		INSERT INTO WindowSticker.PrintBatchAccessLog
			(
				PrintBatchId,
				AccessBy,
				AccessOn			
				)
			VALUES
			(
				@PrintBatchId,
				@AccessBy,
				@AccessOn			
			)
			
		SET @PrintBatchAccessLogId = SCOPE_IDENTITY()

    END TRY
    BEGIN CATCH

			DECLARE @ErrorMessage NVARCHAR(4000),
			  @ErrorNumber INT,
			  @ErrorSeverity INT,
			  @ErrorState INT,
			  @ErrorLine INT,
			  @ErrorProcedure NVARCHAR(200) ;

				-- Assign variables to error-handling functions that 
				-- capture information for RAISERROR.
			SELECT  @ErrorNumber = ERROR_NUMBER(), @ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE(), @ErrorLine = ERROR_LINE(),
					@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-') ;

				-- Building the message string that will contain original
				-- error information.
			SELECT  @ErrorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' +
					'Message: ' + ERROR_MESSAGE() ;

				-- Raise an error: msg_str parameter of RAISERROR will contain
				-- the original error information.
			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, -- parameter: original error number.
			  @ErrorSeverity, -- parameter: original error severity.
			  @ErrorState, -- parameter: original error state.
			  @ErrorProcedure, -- parameter: original error procedure name.
			  @ErrorLine-- parameter: original error line number.
					)

    END CATCH
GO

