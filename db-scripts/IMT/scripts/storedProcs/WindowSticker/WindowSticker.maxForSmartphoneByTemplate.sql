-- FB:22023

if object_id('WindowSticker.maxForSmartphoneByTemplate') is not null
	drop procedure WindowSticker.maxForSmartphoneByTemplate
go

/*

-- removed because it would create a direct dependency by IMT upon Merchandising, which is critically disfavourable

CREATE PROC WindowSticker.maxForSmartphoneByTemplate @TemplateId INT

AS


	SET NOCOUNT ON
	
	set transaction isolation level read uncommitted
	
	if EXISTS (
				SELECT	BusinessUnit.BusinessUnitID
					FROM WindowSticker.Template Template
					INNER JOIN WindowSticker.OwnerTemplate OwnerTemplate ON Template.TemplateId = OwnerTemplate.TemplateId
					INNER JOIN market.Pricing.Owner Owner ON OwnerTemplate.OwnerId = Owner.OwnerID
					INNER JOIN dbo.BusinessUnit BusinessUnit ON Owner.OwnerEntityID = BusinessUnit.BusinessUnitID 
																AND Owner.OwnerTypeID = 1
					INNER JOIN Merchandising.settings.Merchandising Merchandising ON BusinessUnit.BusinessUnitID = Merchandising.businessUnitID
					WHERE	OwnerTemplate.TemplateId = @TemplateId
					AND Merchandising.maxForSmartphone = 1
			)
		SELECT maxForSmartphone = 1
	else
		SELECT maxForSmartphone = 0
	
	
	SET NOCOUNT OFF
	
	
RETURN
*/
-- go
