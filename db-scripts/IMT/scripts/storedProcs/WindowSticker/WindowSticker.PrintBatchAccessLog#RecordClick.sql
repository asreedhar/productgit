IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'WindowSticker.[PrintBatchAccessLog#RecordClick]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE WindowSticker.PrintBatchAccessLog#RecordClick
GO

CREATE PROCEDURE WindowSticker.PrintBatchAccessLog#RecordClick
					@ShortUrl varchar(50),
					@EpochTime  int,
					@Clicks int,
					@PrintBatchAccessLogId int = null OUTPUT
AS 
    SET NOCOUNT ON

    BEGIN TRY
    
		DECLARE @PrintBatchId int, 
				@AccessBy varchar(80), 
				@AccessOn smalldatetime,
				@timeZoneOffset int

		-- convert to date time
		SET @AccessOn = DATEADD(SECOND, @EpochTime, '1/1/1970')

		-- convert to server time zone
		SET @timeZoneOffset = DATEDIFF(HOUR, GETUTCDATE(), GETDATE())
		SET @AccessOn = DATEADD(HOUR, @timeZoneOffset, @AccessOn)
		
		-- find associated email
		SELECT @PrintBatchId = PrintBatchID,
				@AccessBy = Email
			FROM Windowsticker.PrintBatchEmail
			WHERE ShortUrl = @ShortUrl
			
		if @@ROWCOUNT < 1
			RAISERROR('Cannot find ShortURL for: %s in WindowStick.PrintBatchEmail', 16, 1, @ShortUrl)
		
		-- number of clicks already recorded for these arguements?
		IF (SELECT COUNT(*)
				FROM WindowSticker.PrintBatchAccessLog
				WHERE PrintBatchId = @PrintBatchId
				AND AccessBy = @AccessBy
				AND AccessOn = @AccessOn) < @Clicks
			BEGIN
				-- insert
				EXEC WindowSticker.PrintBatchAccessLog#Insert	@PrintBatchId = @PrintBatchId,
																@AccessBy = @AccessBy,
																@AccessOn = @AccessOn,
																@PrintBatchAccessLogId = @PrintBatchAccessLogId OUTPUT
			END
		
		

    END TRY
    BEGIN CATCH

			DECLARE @ErrorMessage NVARCHAR(4000),
			  @ErrorNumber INT,
			  @ErrorSeverity INT,
			  @ErrorState INT,
			  @ErrorLine INT,
			  @ErrorProcedure NVARCHAR(200) ;

				-- Assign variables to error-handling functions that 
				-- capture information for RAISERROR.
			SELECT @ErrorNumber = ERROR_NUMBER(), @ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE(), @ErrorLine = ERROR_LINE(),
					@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-') ;

				-- Building the message string that will contain original
				-- error information.
			SET @ErrorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' +
					'Message: ' + ERROR_MESSAGE() ;

				-- Raise an error: msg_str parameter of RAISERROR will contain
				-- the original error information.
			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, -- parameter: original error number.
			  @ErrorSeverity, -- parameter: original error severity.
			  @ErrorState, -- parameter: original error state.
			  @ErrorProcedure, -- parameter: original error procedure name.
			  @ErrorLine-- parameter: original error line number.
					)

    END CATCH
GO

