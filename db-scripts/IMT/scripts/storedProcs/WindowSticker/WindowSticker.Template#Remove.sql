USE [IMT]

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[WindowSticker].[Template#Remove]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE WindowSticker.Template#Remove

GO
/****** Object:  StoredProcedure [WindowSticker].[Template#Remove]    Script Date: 03/28/2011 13:27:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [WindowSticker].[Template#Remove] 
	@TemplateId INT, 
	@OwnerId INT = NULL
AS
BEGIN

	SET NOCOUNT ON;
  
        IF NOT EXISTS ( SELECT  1
                        FROM    WindowSticker.Template
                        WHERE   TemplateId = @TemplateId ) 
            BEGIN
                RAISERROR('An invalid value was found in the @TemplateId parameter, value: %d',16,1,@TemplateId)
            END

		IF NOT EXISTS ( SELECT  1
                        FROM    Market.Pricing.Owner
                        WHERE   OwnerID = @OwnerId ) 
           AND @OwnerId IS NOT NULL
                        
            BEGIN
                RAISERROR('An invalid value was found in the @OwnerId parameter, value: %d',16,1,@OwnerId)
            END

	DELETE FROM [WindowSticker].[OwnerTemplate]
	WHERE TemplateId = @TemplateId
	AND (@OwnerId IS NULL OR OwnerId = @OwnerId)

END
GO
