
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'WindowSticker.[ContentArea#Insert]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE WindowSticker.ContentArea#Insert

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE WindowSticker.ContentArea#Insert
    (
      @TemplateId INT ,
      @TopLeftX DECIMAL(9, 3) ,
      @TopLeftY DECIMAL(9, 3) ,
      @BottomRightX DECIMAL(9, 3) ,
      @BottomRightY DECIMAL(9, 3) ,
      @FontFamilyId TINYINT ,
      @FontAlignId TINYINT ,
      @FontSize DECIMAL(9, 3) ,
      @LineHeight DECIMAL(9, 3) ,
      @CalculatedCharacterAverage SMALLINT ,
      @CalculatedEmSize DECIMAL(9, 3) ,
      @CalculatedEnSize DECIMAL(9, 3) ,
      @CalculatedLineHeight DECIMAL(9, 3) ,
      @CalculatedBulletSize DECIMAL(9, 3) ,
      @Italics BIT ,
      @Underline BIT ,
      @Bold BIT ,
      @Columns TINYINT,
      @ExtraDataJSON TEXT = NULL,
      @ContentAreaId INT OUT
    )
AS 
    BEGIN TRY

    -- validate input
        IF NOT EXISTS ( SELECT  1
                        FROM    WindowSticker.FontFamily
                        WHERE   FontFamilyId = @FontFamilyId ) 
            BEGIN
                RAISERROR('The @FontFamilyId parameter had an invalid value, value: %i',16,2,@FontFamilyId)
            END

        IF NOT EXISTS ( SELECT  1
                        FROM    WindowSticker.FontAlign
                        WHERE   FontAlignId = @FontAlignId ) 
            BEGIN
                RAISERROR('The @FontAlignId parameter had an invalid value, value: %i',16,2,@FontAlignId)
            END

        IF NOT EXISTS ( SELECT  1
                        FROM    WindowSticker.Template
                        WHERE   TemplateId = @TemplateId ) 
            BEGIN
                RAISERROR('The @TemplateId parameter had an invalid value, value: %i',16,2,@TemplateId)
            END
            
	IF @FontSize <= 0
	BEGIN
	     DECLARE @FontSizeVarchar VARCHAR(10)
	     SET @FontSizeVarchar = CAST(@FontSize AS VARCHAR(10))
	     RAISERROR('The @FontSize parameter had an invalid value, value: %s',16,2,@FontSizeVarchar)
	END            

        INSERT  INTO WindowSticker.ContentArea
                ( TopLeftX ,
                  TopLeftY ,
                  BottomRightX ,
                  BottomRightY ,
                  FontFamilyId ,
                  FontAlignId ,
                  FontSize ,
                  LineHeight ,
                  CalculatedCharacterAverage ,
                  CalculatedEmSize ,
                  CalculatedEnSize ,
                  CalculatedLineHeight ,
                  CalculatedBulletSize ,
                  Italics ,
                  Underline ,
                  Bold ,
                  Columns ,
                  ExtraDataJSON
                )
        VALUES  ( @TopLeftX ,
                  @TopLeftY ,
                  @BottomRightX ,
                  @BottomRightY ,
                  @FontFamilyId ,
                  @FontAlignId ,
                  @FontSize ,
                  @LineHeight ,
                  @CalculatedCharacterAverage ,
                  @CalculatedEmSize ,
                  @CalculatedEnSize ,
                  @CalculatedLineHeight ,
                  @CalculatedBulletSize ,
                  @Italics ,
                  @Underline ,
                  @Bold ,
                  @Columns ,
                  @ExtraDataJSON
                )

        SELECT  @ContentAreaId = SCOPE_IDENTITY()
    
        INSERT  INTO WindowSticker.TemplateContentArea
                ( TemplateId, ContentAreaId )
        VALUES  ( @TemplateId, @ContentAreaId )
    
    END TRY
    BEGIN CATCH

        EXEC dbo.sp_ErrorHandler

    END CATCH
GO

SET QUOTED_IDENTIFIER OFF
SET ANSI_NULLS OFF
