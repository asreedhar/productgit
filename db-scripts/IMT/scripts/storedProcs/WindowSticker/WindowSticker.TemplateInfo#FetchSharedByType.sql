IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[WindowSticker].[TemplateInfo#FetchSharedByType]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE WindowSticker.TemplateInfo#FetchSharedByType

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE WindowSticker.TemplateInfo#FetchSharedByType
    @TemplateTypeId TINYINT
AS 
    BEGIN

        SET NOCOUNT ON ;

        IF NOT EXISTS ( SELECT  1
                        FROM    WindowSticker.TemplateType
                        WHERE   TemplateTypeId = @TemplateTypeId ) 
            BEGIN
                RAISERROR('An invalid value was found in the @TemplateTypeId parameter, value: %d',16,2,@TemplateTypeId)
            END


        DECLARE @Templates TABLE
            (
              TemplateId INT NOT NULL ,
              TemplateName VARCHAR(100) NOT NULL ,
              Dealers VARCHAR(MAX) NOT NULL
            )

        DECLARE @dealers VARCHAR(MAX)
        INSERT  INTO @Templates
                ( TemplateId ,
                  TemplateName ,
                  Dealers
                )
                SELECT  t.TemplateId ,
                        t.Name ,
                        WindowSticker.GetBusinessUnitListForTemplate(t.TemplateId,'comma')
                FROM    WindowSticker.Template t
                        JOIN WindowSticker.OwnerTemplate ot ON ot.TemplateId = t.TemplateId
                        JOIN ( SELECT   TemplateId ,
                                        COUNT(OwnerId) ownerCount
                               FROM     WindowSticker.OwnerTemplate
                               GROUP BY templateId
                               HAVING   COUNT(OwnerId) > 1  -- template is "shared"
                             ) AS shared ON shared.templateId = t.templateId
                WHERE   t.TemplateTypeId = @TemplateTypeId

        SELECT  DISTINCT *
        FROM    @Templates

    END

GO
