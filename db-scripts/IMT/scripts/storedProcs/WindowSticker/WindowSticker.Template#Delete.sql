
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'WindowSticker.[Template#Delete]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE WindowSticker.Template#Delete

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE WindowSticker.Template#Delete
    @TemplateId INT
AS 
    SET NOCOUNT ON ;

    BEGIN TRY
    
    	DECLARE @dp TABLE(dataPointId INT NOT NULL)

	DECLARE @ca TABLE(contentAreaId INT NOT NULL)

	-- Get DataPointIds to delete
	INSERT INTO @dp
		( dataPointId )
	SELECT dp.DataPointId
	FROM WindowSticker.Template t
	JOIN WindowSticker.TemplateContentArea tca ON tca.TemplateId = t.TemplateId
	JOIN WindowSticker.ContentArea ca ON ca.ContentAreaId = tca.ContentAreaId
	JOIN WindowSticker.ContentAreaDataPoint cadp ON cadp.ContentAreaId = ca.ContentAreaId
	JOIN WindowSticker.DataPoint dp ON dp.DataPointId = cadp.DataPointId
	WHERE t.TemplateId = @TemplateId

	-- Get ContentAreaIds to delete
	INSERT INTO @ca
		( contentAreaId )
	SELECT ca.ContentAreaId
	FROM WindowSticker.Template t
	JOIN WindowSticker.TemplateContentArea tca ON tca.TemplateId = t.TemplateId
	JOIN WindowSticker.ContentArea ca ON ca.ContentAreaId = tca.ContentAreaId
	WHERE t.TemplateId = @TemplateId

	-- delete ContentAreaDataPoints
	DELETE cadp
	FROM @ca del
	JOIN WindowSticker.ContentAreaDataPoint cadp ON cadp.ContentAreaId = del.ContentAreaId

	-- delete DataPoint
	DELETE dp
	FROM @dp del
	JOIN WindowSticker.DataPoint dp ON dp.DataPointId = del.DataPointId

	-- delete TemplateContentAreas
	DELETE tca
	FROM @ca del
	JOIN WindowSticker.TemplateContentArea tca ON tca.ContentAreaId = del.ContentAreaId

	-- delete ContentAreas
	DELETE ca
	FROM @ca del
	JOIN WindowSticker.ContentArea ca ON ca.ContentAreaId = del.contentAreaId

	-- delete OwnerTemplates
	DELETE 
	FROM WindowSticker.OwnerTemplate
	WHERE TemplateId = @templateId

	-- delete Template
	DELETE 
	FROM WindowSticker.Template 
	WHERE TemplateId = @TemplateId

    END TRY
    BEGIN CATCH

        EXEC dbo.sp_ErrorHandler

    END CATCH
GO

SET QUOTED_IDENTIFIER OFF
SET ANSI_NULLS OFF