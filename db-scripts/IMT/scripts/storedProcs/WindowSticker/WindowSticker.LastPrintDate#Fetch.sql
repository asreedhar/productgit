
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[WindowSticker].[LastPrintDate#Fetch]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE WindowSticker.LastPrintDate#Fetch

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE WindowSticker.LastPrintDate#Fetch
    @InventoryId INT ,
    @TemplateTypeId INT ,
    @LatestPrintDate DATETIME OUT
AS 
    BEGIN

        SET NOCOUNT ON ;
            
        IF NOT EXISTS ( SELECT  1
                        FROM    FLDW.dbo.InventoryActive
                        WHERE   InventoryID = @InventoryId ) 
            BEGIN
                RAISERROR('An invalid value was found in the @InventoryId parameter, value: %d',16,2,@InventoryId)
            END

        IF NOT EXISTS ( SELECT  1
                        FROM    WindowSticker.TemplateType
                        WHERE   TemplateTypeId = @TemplateTypeId ) 
            BEGIN
                RAISERROR('An invalid value was found in the @TemplateTypeId parameter, value: %d',16,2,@TemplateTypeId)
            END

        SELECT  @LatestPrintDate = MAX(PrintDate)
        FROM    WindowSticker.PrintLog
        WHERE   InventoryId = @InventoryId
                AND TemplateTypeId = @TemplateTypeId

    END

GO
