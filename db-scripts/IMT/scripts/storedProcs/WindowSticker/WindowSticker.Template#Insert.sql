
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'WindowSticker.[Template#Insert]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE WindowSticker.Template#Insert

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE WindowSticker.Template#Insert
    @OwnerHandle VARCHAR(36) = NULL ,
    @TemplateTypeId TINYINT ,
    @Height DECIMAL(9,3) ,
    @Width DECIMAL(9,3) ,
    @Units CHAR(2) ,
    @Name VARCHAR(100) ,
    @StockPdf VARBINARY(MAX) = NULL ,
    @User VARCHAR(80) ,
    @SystemTemplate BIT ,
    @BackgroundImage VARCHAR(50) = NULL,
    @TemplateId INT OUT
AS 
    SET NOCOUNT ON ;

    BEGIN TRY
    -- validate input
	IF @SystemTemplate = 0 AND @OwnerHandle IS NULL
	BEGIN
	    RAISERROR('Non-System templates must have an owner associated with them.  @OwnerHandle is null.',16,2)
	END

        DECLARE @OwnerEntityTypeID INT ,
            @OwnerEntityID INT ,
            @OwnerID INT

	IF @SystemTemplate = 0
	BEGIN
	    -- dealer template, validate the owner handle, get the owner id
	    EXEC Market.Pricing.ValidateParameter_OwnerHandle @OwnerHandle,
		@OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT	
    
	    IF @OwnerEntityTypeID <> 1 
		BEGIN
		    RAISERROR('Only dealers are currently supported.  @OwnerHandle: %s ',16,2,@OwnerHandle)
		END
        END
        ELSE
        BEGIN
	    SET @OwnerId = 0	--  System template
        END
	
	-- make sure name is not in use as system template
        IF EXISTS ( SELECT  1
                    FROM    WindowSticker.OwnerTemplate ot
                            JOIN WindowSticker.Template t ON ot.TemplateId = t.TemplateId
                    WHERE   ot.OwnerId = 0
                            AND t.Name = @Name ) 
            BEGIN
                RAISERROR('A system template of this name already exists.  name: %s , owner handle: %s',16,2,@name, @OwnerHandle)
            END
        
	-- make sure name is not in use as dealer template
        IF EXISTS ( SELECT  1
                    FROM    WindowSticker.OwnerTemplate ot
                            JOIN WindowSticker.Template t ON ot.TemplateId = t.TemplateId
                    WHERE   ot.OwnerId = @OwnerID
                            AND t.Name = @Name ) 
            BEGIN
                RAISERROR('A template of this name already exists for this owner.  name: %s , owner handle: %s',16,2,@name, @OwnerHandle)
            END

        IF NOT EXISTS ( SELECT  1
                        FROM    WindowSticker.TemplateType
                        WHERE   TemplateTypeId = @TemplateTypeId ) 
            BEGIN
                RAISERROR('An invalid value was found in the @TemplateTypeId parameter.',16,2)
            END

	IF @Height <= 0
	BEGIN
	    RAISERROR('An invalid value was found in the @Height parameter.  Height must be greater than zero.',16,2)
	END

	IF @Width <= 0
	BEGIN
	    RAISERROR('An invalid value was found in the @Width parameter.  Width must be greater than zero.',16,2)
	END


        DECLARE @InsertUserID INT
        EXEC dbo.ValidateParameter_MemberID#UserName @User,
            @InsertUserID OUTPUT

    -- If you change this, be sure to change the CK_Template_Units constraint as well.
    -- this will just provide a more friendly error message than a constraint violation.
        IF ( @Units NOT IN ( 'pc', 'pt', 'mm', 'cm', 'in' ) ) 
            BEGIN
                RAISERROR('An invalid value was found in the @Units parameter - unsupported unit value, value: %d',16,2,@Units)
            END

    -- insert the template
        INSERT  INTO WindowSticker.Template
                ( TemplateTypeId ,
                  Height ,
                  Width ,
                  Units ,
                  Name ,
                  StockPdf ,
                  InsertUser ,
                  InsertDate,
                  BackgroundImage
                )
        VALUES  ( @TemplateTypeId ,
                  @Height ,
                  @Width ,
                  @Units ,
                  @Name ,
                  @StockPdf ,
                  @InsertUserID ,
                  GETDATE(),
                  @BackgroundImage
                )

    -- grab the templateId
        SELECT  @TemplateId = SCOPE_IDENTITY()
    
    -- link the template to the owner
        INSERT  INTO WindowSticker.OwnerTemplate
                ( OwnerId ,
                  TemplateId ,
                  InsertUser ,
                  InsertDate
                )
        VALUES  ( @OwnerID ,
                  @TemplateId ,
                  @InsertUserID ,
                  GETDATE()
                )

    END TRY
    BEGIN CATCH

        EXEC dbo.sp_ErrorHandler

    END CATCH
GO

SET QUOTED_IDENTIFIER OFF
SET ANSI_NULLS OFF