
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[WindowSticker].[Template#Fetch]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE WindowSticker.Template#Fetch

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE WindowSticker.Template#Fetch @TemplateId INT
AS 
    BEGIN

        SET NOCOUNT ON ;
            
        IF NOT EXISTS ( SELECT  1
                        FROM    WindowSticker.Template
                        WHERE   TemplateId = @TemplateId ) 
            BEGIN
                RAISERROR('An invalid value was found in the @TemplateId parameter, value: %d',16,2,@TemplateId)
            END


        DECLARE @Templates TABLE
            (
              TemplateId INT NOT NULL ,
              TemplateName VARCHAR(100) NOT NULL ,
              Height DECIMAL(9, 3) NOT NULL ,
              Width DECIMAL(9, 3) NOT NULL ,
              Units CHAR(2) NOT NULL ,
              TemplateTypeId TINYINT ,
              StockPdf VARBINARY(MAX) NULL,
              BackgroundImage VARCHAR(50) NULL
            )

        INSERT  INTO @Templates
                ( TemplateId ,
                  TemplateName ,
                  Height ,
                  Width ,
                  Units ,
                  TemplateTypeId ,
                  StockPdf,
                  BackgroundImage
                )
                SELECT  TemplateId ,
                        Name ,
                        Height ,
                        Width ,
                        Units ,
                        TemplateTypeId ,
                        StockPdf,
                        BackgroundImage
                FROM    WindowSticker.Template
                WHERE   TemplateId = @TemplateId

        DECLARE @ContentAreas TABLE
            (
              ContentAreaId INT NOT NULL ,
              TopLeftX DECIMAL(9, 3) NOT NULL ,
              TopLeftY DECIMAL(9, 3) NOT NULL ,
              BottomRightX DECIMAL(9, 3) NOT NULL ,
              BottomRightY DECIMAL(9, 3) NOT NULL ,
              FontFamily VARCHAR(50) NOT NULL ,
              FontAlign VARCHAR(50) NOT NULL ,
              FontSize DECIMAL(9, 3) NULL ,
              LineHeight DECIMAL(9, 3) NULL ,
              CalculatedCharacterAverage SMALLINT NOT NULL ,
              CalculatedEmSize DECIMAL(9, 3) NOT NULL ,
              CalculatedEnSize DECIMAL(9, 3) NOT NULL ,
              CalculatedLineHeight DECIMAL(9, 3) NOT NULL ,
              CalculatedBulletSize DECIMAL(9, 3) NOT NULL ,
              Italics BIT NOT NULL ,
              Underline BIT NOT NULL ,
              Bold BIT NOT NULL ,
              DataPointId INT NULL ,
              DataPointText VARCHAR(1000) NULL ,
              DataPointKey VARCHAR(50) NULL ,
              DataPointPrompt VARCHAR(1000) NULL ,
              [Columns] TINYINT NULL,
              ExtraDataJSON TEXT NULL
            )

        INSERT  INTO @ContentAreas
                ( ContentAreaId ,
                  TopLeftX ,
                  TopLeftY ,
                  BottomRightX ,
                  BottomRightY ,
                  FontFamily ,
                  FontAlign ,
                  FontSize,
                  LineHeight ,
                  CalculatedCharacterAverage ,
                  CalculatedEmSize ,
                  CalculatedEnSize ,
                  CalculatedLineHeight ,
                  CalculatedBulletSize ,
                  Italics ,
                  Underline ,
                  Bold ,
                  DataPointId ,
                  DataPointText ,
                  DataPointKey ,
                  DataPointPrompt ,
                  [Columns],
                  ExtraDataJSON
                )
                SELECT  ca.ContentAreaId ,
                        ca.TopLeftX ,
                        ca.TopLeftY ,
                        ca.BottomRightX ,
                        ca.BottomRightY ,
                        caff.Name ,
                        caa.Name ,
                        ca.FontSize ,
                        ca.LineHeight ,
                        ca.CalculatedCharacterAverage ,
                        ca.CalculatedEmSize ,
                        ca.CalculatedEnSize ,
                        ca.CalculatedLineHeight ,
                        ca.CalculatedBulletSize ,
                        ca.Italics ,
                        ca.Underline ,
                        ca.Bold ,
                        dp.DataPointId ,
                        dp.Text ,
                        dpk.[Key] ,
                        dp.Prompt ,
                        ca.Columns ,
                        ca.ExtraDataJSON
                FROM    @Templates t
                        JOIN WindowSticker.TemplateContentArea tca ON tca.TemplateId = t.TemplateId
                        JOIN WindowSticker.ContentArea ca ON tca.ContentAreaId = ca.ContentAreaId
                        JOIN WindowSticker.FontFamily caff ON caff.FontFamilyId = ca.FontFamilyId
                        JOIN WindowSticker.FontAlign caa ON caa.FontAlignId = ca.FontAlignId
                        LEFT JOIN WindowSticker.ContentAreaDataPoint cdp ON cdp.ContentAreaId = ca.ContentAreaId
                        LEFT JOIN WindowSticker.DataPoint dp ON cdp.DataPointId = dp.DataPointId
                        LEFT JOIN WindowSticker.DataPointKey dpk ON dpk.DataPointKeyId = dp.DataPointKeyId

        SELECT  *
        FROM    @Templates
        SELECT  *
        FROM    @ContentAreas
    END

GO
