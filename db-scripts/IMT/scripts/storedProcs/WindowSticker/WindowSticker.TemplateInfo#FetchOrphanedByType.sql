IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[WindowSticker].[TemplateInfo#FetchOrphanedByType]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE WindowSticker.TemplateInfo#FetchOrphanedByType

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE WindowSticker.TemplateInfo#FetchOrphanedByType
    @TemplateTypeId TINYINT
AS 
    BEGIN

        SET NOCOUNT ON ;

        IF NOT EXISTS ( SELECT  1
                        FROM    WindowSticker.TemplateType
                        WHERE   TemplateTypeId = @TemplateTypeId ) 
            BEGIN
                RAISERROR('An invalid value was found in the @TemplateTypeId parameter, value: %d',16,2,@TemplateTypeId)
            END


        DECLARE @Templates TABLE
            (
              TemplateId INT NOT NULL ,
              TemplateName VARCHAR(100) NOT NULL
            )

                INSERT  INTO @Templates
                ( TemplateId ,
                  TemplateName
                )
                SELECT  t.TemplateId ,
                        t.Name
                FROM    WindowSticker.Template t
                        LEFT JOIN WindowSticker.OwnerTemplate ot ON ot.TemplateId = t.TemplateId
                WHERE   t.TemplateTypeId = @TemplateTypeId
                        AND ot.TemplateId IS NULL

        SELECT  *
        FROM    @Templates

    END

GO

