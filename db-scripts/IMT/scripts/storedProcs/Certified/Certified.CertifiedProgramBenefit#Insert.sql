IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[CertifiedProgramBenefit#Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[CertifiedProgramBenefit#Insert]



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[CertifiedProgramBenefit#Insert]
    @CertifiedProgramID INT,
    @Name VARCHAR(50),
    @Text VARCHAR(100),
    @Rank SMALLINT,
    @IsProgramHighlight BIT,
    @InsertUser VARCHAR(80),
    @CertifiedProgramBenefitID INT OUTPUT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Certified.CertifiedProgramBenefit#Insert.sql,v 1.1.4.1.2.2 2010/06/02 18:40:29 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Inserts a certified program benefit
 * 
 * Input Parameters
 * ----------
 *
 * @CertifiedProgramID
 * @Name
 * @Text
 * @Rank
 * @IsProgramHighlight
 * @InsertUser
 * 
 * Output Parameters
 * ----------
 *
 * @CertifiedProgramBenefitID
 *
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @CertifiedProgramID IS NULL
 * 50100 - @Name IS NULL
 * 50100 - @Text IS NULL
 * 50100 - @Rank IS NULL
 * 50100 - @IsProgramHighlight IS NULL
 * 50111 - @Name length
 * 50111 - @Text length
 * 50106 - @CertifiedProgramID does not exist
 * 50113 - CertifiedProgramBenefit row exists already (by Name)
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @CertifiedProgramID IS NULL
    BEGIN
	RAISERROR (50100,16,1,'CertifiedProgramID')
	RETURN @@ERROR
    END

    IF @Name IS NULL
    BEGIN
	RAISERROR (50100,16,1,'Name')
	RETURN @@ERROR
    END

    IF @Text IS NULL
    BEGIN
	RAISERROR (50100,16,1,'Text')
	RETURN @@ERROR
    END

    IF @Rank IS NULL
    BEGIN
	RAISERROR (50100,16,1,'Rank')
	RETURN @@ERROR
    END

    IF @IsProgramHighlight IS NULL
    BEGIN
	RAISERROR (50100,16,1,'IsProgramHighlight')
	RETURN @@ERROR
    END

    IF (LEN(@Name) <1 OR LEN(@Name) >50)
    BEGIN
	RAISERROR (50111,16,1,'Name',1,50)
	RETURN @@ERROR
    END

    IF (LEN(@Text) <1 OR LEN(@Text) >100)
    BEGIN
	RAISERROR (50111,16,1,'Text',1,100)
	RETURN @@ERROR
    END

    DECLARE @InsertUserID INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @InsertUser, @InsertUserID OUTPUT

    IF NOT EXISTS (SELECT 1 FROM Certified.CertifiedProgram WHERE CertifiedProgramID = @CertifiedProgramID)
    BEGIN
	RAISERROR (50106,16,1,'CertifiedProgram', @CertifiedProgramID)
	RETURN @@ERROR
    END

    IF EXISTS (SELECT 1 FROM Certified.CertifiedProgramBenefit WHERE CertifiedProgramID = @CertifiedProgramID AND Name = @Name)
    BEGIN
	RAISERROR (50113,16,1,'Name')
	RETURN @@ERROR
    END

    /*
    If the Rank is already being used by this CertifiedProgram, set all of the Ranks that are equal to our desired rank up by 1.
    We need to make room for our new benefit at the desired rank.
    */
    IF EXISTS (SELECT 1 FROM Certified.CertifiedProgramBenefit WHERE CertifiedProgramID = @CertifiedProgramID AND Rank = @Rank)
    BEGIN
	UPDATE Certified.CertifiedProgramBenefit
	SET Rank = Rank + 1
	WHERE CertifiedProgramID = @CertifiedProgramID
	AND Rank >= @Rank 
    END


    /*
    Insert the new benefit at the desired rank.
    */
    INSERT 
    INTO    Certified.CertifiedProgramBenefit
	    (CertifiedProgramID,
	    Name,
	    Text,
	    Rank,
	    IsProgramHighlight,
	    InsertUser,
	    InsertDate)
    VALUES
	    (@CertifiedProgramID,
	    @Name,
	    @Text,
	    @Rank,
	    @IsProgramHighlight,
	    @InsertUserID,
	    GETDATE())
    
    SELECT @CertifiedProgramBenefitID = SCOPE_IDENTITY()

    /*
    If we already have OwnerCertifiedPrograms that relate to this program.
    We need to add this benefit to these programs as the last rank.
    */
    IF EXISTS (SELECT 1 FROM Certified.OwnerCertifiedProgram WHERE CertifiedProgramID = @CertifiedProgramID)
    BEGIN

	DECLARE @NewRank SMALLINT

	SELECT @NewRank = MAX(Rank)
	FROM Certified.CertifiedProgramBenefit
	WHERE CertifiedProgramID = @CertifiedProgramID

	INSERT 
	INTO	Certified.OwnerCertifiedProgramBenefit
		(OwnerCertifiedProgramID,
		CertifiedProgramBenefitID,
		Rank,
		IsProgramHighlight,
		InsertUser,
		InsertDate)
	SELECT
		ocp.OwnerCertifiedProgramID,
		@CertifiedProgramBenefitID,
		@NewRank,
		@IsProgramHighlight,
		@InsertUserID,
		GETDATE()
	FROM	OwnerCertifiedProgram ocp
	WHERE	CertifiedProgramID = @CertifiedProgramID

    END

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
