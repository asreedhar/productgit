if exists ( select  *
            from    sys.objects
            where   object_id = object_id(N'[Certified].[CertifiedProgramBusinessUnitCollection#Fetch]')
                    and type in ( N'P', N'PC' ) ) 
    drop procedure [Certified].[CertifiedProgramBusinessUnitCollection#Fetch]



set ANSI_NULLS on
GO
set QUOTED_IDENTIFIER off
GO

create procedure [Certified].[CertifiedProgramBusinessUnitCollection#Fetch]
    @BusinessUnitTypeId int
as 

    begin try

        if @BusinessUnitTypeId is null 
            begin
                raiserror (50100,16,1,'BusinessUnitTypeId')
                return @@ERROR
            end

        if not exists ( select  *
                        from    IMT.dbo.BusinessUnitType
                        where    BusinessUnitTypeID = @BusinessUnitTypeId ) 
            begin
                raiserror (50106,16,1,'BusinessUnitTypeId', @BusinessUnitTypeId)
                return @@ERROR
            end


		/*

		BusinessUnitTypeID	BusinessUnitType
		3	DealerGroup
		4	Dealer

		*/


		declare @CPODealers table(businessUnitId int)

		insert into @CPODealers
				( businessUnitId )
		select bu.BusinessUnitID
		from imt.dbo.BusinessUnit bu
		where bu.Active = 1
		and exists (
			-- dealers should have at least one of these upgrades
			select * 
			from imt.dbo.DealerUpgrade du
			where du.BusinessUnitID = bu.BusinessUnitID
			and du.DealerUpgradeCD in 
			(
				23 -- MAX Merchandising
				,24 -- MAX AD (Wanamaker)
			)
		)


		if (@BusinessUnitTypeId = 3) -- dealer group
		begin

			select bu.BusinessUnitID, bu.BusinessUnit
			from imt.dbo.BusinessUnit bu
			where bu.BusinessUnitTypeID = @BusinessUnitTypeId
			and exists (
				-- dealer has a child bu that is a CPO dealer
				select * 
				from @CPODealers d 
				join imt.dbo.BusinessUnitRelationship bur 
					on bur.ParentID = bu.BusinessUnitId
					and d.businessUnitId = bur.BusinessUnitID
			)
			order by bu.BusinessUnit

		end
		else if (@BusinessUnitTypeId = 4) -- dealer
		begin

			select bu.BusinessUnitID, bu.BusinessUnit
			from imt.dbo.BusinessUnit bu
			join @CPODealers d on bu.BusinessUnitID = d.businessUnitId
			where bu.BusinessUnitTypeID = @BusinessUnitTypeId
			order by bu.BusinessUnit

		end
end try

begin catch
    exec dbo.sp_ErrorHandler
end catch
