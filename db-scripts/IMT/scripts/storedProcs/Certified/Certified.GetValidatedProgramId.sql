if exists ( select * from sys.objects where    object_id = object_id(N'Certified.GetValidatedProgramId') and type in ( N'P', N'PC' ) ) 
   drop procedure Certified.GetValidatedProgramId
GO


create procedure Certified.GetValidatedProgramId
	@BusinessUnitId int,
	@InventoryId int,
	@MakeName varchar(50),
	@ValidatedProgramId int output,
	@IsManfacturerSpecified bit output,
	@ProgramIsPersisted bit output
as 
begin
set nocount on

begin try
   

/*
Logic:

Manufacturer specified:

BMW logic:
We currently get a feed from BMW that indicates to which certified program, if any, applies to a particular VIN.
If we haven't saved a program for a vehicle yet, we will initially select the program that is in the feed for a particular VIN - as long as the dealer participates in the program.
Once we have a saved program for a BMW, we will use that program.  It can be changed, but must be manually changed from that point on.

Normal - No feed available from manufacturer:
If ProgramId is specified and the specified program is valid at that dealership for that make
		return benefits from that programId
		(regardless of Manufacturer/Dealer program)
else (we have no valid program to use)
if Manufacturer certified
	autoselect a program:
		if Dealer has exactly 1 program for this Make
			use that programId
		else -- can't single out a program
			return empty results
else -- not certified at all
	return empty results
end



Note:  The output parameter @ProgramIsPersisted is a bad name for how it is used.  What this really means is "NotAutoSelected".
TODO: Rename the @ProgramIsPersisted to AutoSelected and reverse the logic.  It would make this easier to read and understand.  Right now I do not want to change the interface to this proc.
*/

	-- get the ownerid for this dealer	
	declare @ownerId int
	select @ownerId = OwnerId 
	from Market.Pricing.Owner 
	where OwnerTypeID = 1 and OwnerEntityID = @BusinessUnitId

	if @ownerId is null
		raiserror('An Owner row (where OwnerTypeID = 1 (Dealer)) was not found for this businessUnitId: %d', 16,1,@BusinessUnitId)

	declare @configuredProgramId int
	-- get configured programid (dealer OR manufacturer certified)
	select @configuredProgramId = c.CertifiedProgramId
	from imt.dbo.Inventory_CertificationNumber c
	where c.InventoryID = @InventoryId
	and c.CertifiedProgramId > 0
	
	-- validate the program (participation in programs can change)
	declare @ProgramIsValid bit
	select @ProgramIsValid = 
		case when exists (
			select *
			from imt.Certified.GetPrograms(@MakeName, @OwnerId)
			where ProgramId = @configuredProgramId
		) then 1 
		else 0 
	end

	if (@configuredProgramId is not null and @ProgramIsValid = 1) -- configured and valid
	begin
		set @ProgramIsPersisted = 1  -- (i.e., Not Auto-Selected)
		set @ValidatedProgramId = @configuredProgramId -- use the configured program
		set @IsManfacturerSpecified = 0
		return	-- done, no more processing needed
	end
	else if (@configuredProgramId is not null and @ProgramIsValid = 0)
	begin
		set @configuredProgramId = null; -- act like it isn't there
	end
	
	/*
	
	From here on out, we have no valid, saved program that we can use.  We must auto-select a program if we can.
	We can only auto-select Manufacturer programs for vehicles.  We do not auto-select dealer programs for vehicles.
	For BMWs, we must use a feed to determine the certification status for auto-selection.

	These should be all true now:
		@configuredProgramId = null -- we don't have a program saved that we can use
		@ProgramIsPersisted = 0 -- nothing saved that we can use
		@IsManfacturerSpecified = 0 -- haven't tried pulling from Mfr feed yet

	*/

	-- get ManufacturerCertified status
	declare @IsManufacturerCertified bit
	select @IsManufacturerCertified = Certified
	from FLDW.dbo.InventoryActive ia
	where ia.BusinessUnitID = @BusinessUnitId
	and ia.InventoryID = @InventoryId
	
	-- we can only auto select Mfr prorams.  If it isn't Mfr Certified, get out.
	if (@IsManufacturerCertified = 0) 
	begin
		-- (these sets should be unnecessary, but doing them anyway for readability)
		set @ValidatedProgramId = null
		set @IsManfacturerSpecified = 0
		set @ProgramIsPersisted = 1 -- -- (i.e., Not Auto-Selected) nothing for the user to change
		return
	end
	
	
	-- from here on out, we have a Mfr Certified vehicle and don't have a valid program saved for this vehicle.
	-- We need to try to determine which one to use.
	-- If we do not get a feed for this make (i.e., it is not a BMW), and the dealer participates in exactly 1 Mfr program for the vehicle's make, use that program.
	-- If we do get a feed for this make (i.e., it is a BMW), we can hit the feed data and see if the manufacturer tells us which program to use.

	declare @autoSelectedProgramId int

	if (@MakeName != 'BMW' )
	begin
		select @autoSelectedProgramId = cp.CertifiedProgramID
		from imt.Certified.CertifiedProgram cp
		where 
		cp.BusinessUnitId is null -- Mfr program
		and cp.Active = 1 -- program is active
		and exists (
			-- program is for the specified Make
			select * from imt.Certified.CertifiedProgram_Make cpm
			join VehicleCatalog.Firstlook.Make m on cpm.MakeID = m.MakeID
			where m.Make = @makeName
			and cpm.CertifiedProgramID = cp.CertifiedProgramID
		)
		and exists (
			-- dealer participates in the program
			select *
			from imt.Certified.OwnerCertifiedProgram ocp 
			where ocp.CertifiedProgramID = cp.CertifiedProgramID
			and ocp.OwnerId = @ownerId
		)
		if (@@ROWCOUNT = 1 and @autoSelectedProgramId is not null) 
		begin
			-- we got exactly one, we can use it.
			set @ValidatedProgramId = @autoSelectedProgramId
			set @IsManfacturerSpecified = 0
			set @ProgramIsPersisted = 0 -- (i.e., Auto-Selected)
			return
		end

		-- autoselection was not possible at this time.
		set @ValidatedProgramId = null
		set @IsManfacturerSpecified = 0
		set @ProgramIsPersisted = 1 -- (i.e., Not Auto-Selected)
		return
	end

	-- from here on out, we're dealing with BMWs and don't have a program to use yet.  Hit the feed.
	
	declare @MfrReportedProgramId int

	select @MfrReportedProgramId = 
		case 
			when va.attributevalue = '0075000' then 
				-- 'BMW Certified Pre-Owned Elite'
				( 
					select CertifiedProgramID 
					from imt.Certified.CertifiedProgram cp
					where Name = 'BMW Certified Pre-Owned Elite' 
				)
			when va.attributevalue = '0100000' then 
			-- 'BMW Certified Pre-Owned'
				( 
					select CertifiedProgramID 
					from imt.Certified.CertifiedProgram cp
					where Name = 'BMW Certified Pre-Owned' 
				)

			else null
		end
	from
		FLDW.dbo.InventoryActive i with (nolock)
		inner join FLDW.dbo.Vehicle v ON i.VehicleID = v.VehicleID and i.BusinessUnitID = v.BusinessUnitID
		inner join IMT.dbo.BusinessUnit bu on i.BusinessUnitID = bu.BusinessUnitID
		left join VehicleCatalog.vds.VehicleAttribute va ON va.vin = v.vin AND va.AttributeId=3
	where i.InventoryID = @InventoryId
	and i.BusinessUnitID = @BusinessUnitId
	
	
	
	if (@MfrReportedProgramId is not null)
	begin
		select @ProgramIsValid = 
			case when exists 
			(
				select *
				from imt.Certified.GetPrograms(@MakeName, @OwnerId)
				where ProgramId = @MfrReportedProgramId
			) then 1 
			else 0 
			end	
	
		if (@ProgramIsValid = 1)
		begin
			set @ValidatedProgramId = @MfrReportedProgramId
			set @IsManfacturerSpecified = 1; -- coming from BMW
			set @ProgramIsPersisted = 0; -- (i.e., Auto-Selected)
			return	-- we're done here
		end
	end	
	
	-- no program valid for this vehicle.
	set @ValidatedProgramId = null
	set @IsManfacturerSpecified = 0;
	set @ProgramIsPersisted = 1;  -- (i.e., Not Auto-Selected)
	return

	      
end try

begin catch
  exec dbo.sp_ErrorHandler
end catch

end
go


grant execute on [Certified].[GetValidatedProgramId] TO MerchandisingUser 
GO