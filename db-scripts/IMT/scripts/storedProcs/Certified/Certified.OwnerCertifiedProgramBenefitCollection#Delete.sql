IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[OwnerCertifiedProgramBenefitCollection#Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[OwnerCertifiedProgramBenefitCollection#Delete]


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[OwnerCertifiedProgramBenefitCollection#Delete]
    @OwnerCertifiedProgramID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Certified.OwnerCertifiedProgramBenefitCollection#Delete.sql,v 1.1.4.1.2.2 2010/06/02 18:40:29 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Deletes an owner certified program benefit collection
 * 
 * Input Parameters
 * ----------
 *
 * @OwnerCertifiedProgramID
 * 
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerCertifiedProgramID IS NULL
 * 50106 - @OwnerCertifiedProgramID does not exist
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @OwnerCertifiedProgramID IS NULL
    BEGIN
	RAISERROR (50100,16,1,'OwnerCertifiedProgramID')
	RETURN @@ERROR
    END

    IF NOT EXISTS (SELECT 1 FROM Certified.OwnerCertifiedProgram WHERE OwnerCertifiedProgramID = @OwnerCertifiedProgramID)
    BEGIN
	RAISERROR (50106,16,1,'OwnerCertifiedProgram',@OwnerCertifiedProgramID )
	RETURN @@ERROR
    END

    -- Delete the owner certified program benefits
    DELETE 
    FROM Certified.OwnerCertifiedProgramBenefit
    WHERE OwnerCertifiedProgramID = @OwnerCertifiedProgramID

    -- Delete the owner certified program
    DELETE 
    FROM Certified.OwnerCertifiedProgram
    WHERE OwnerCertifiedProgramID = @OwnerCertifiedProgramID

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
