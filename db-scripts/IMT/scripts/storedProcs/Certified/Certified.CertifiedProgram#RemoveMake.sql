IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[CertifiedProgram#RemoveMake]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[CertifiedProgram#RemoveMake]



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[CertifiedProgram#RemoveMake]
    @CertifiedProgramID INT,
    @MakeID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Certified.CertifiedProgram#RemoveMake.sql,v 1.1.4.1.2.2 2010/06/02 18:40:29 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Removes (deletes) the association of a make with a certified program
 * 
 * Input Parameters
 * ----------
 *
 * @CertifiedProgramID
 * @MakeID
 * 
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @CertifiedProgramID IS NULL
 * 50100 - @MakeID IS NULL
 * 50106 - @CertifiedProgramID does not exist
 * 50106 - @MakeID does not exist
 * 50113 - Make is not assigned to certified program
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @CertifiedProgramID IS NULL
    BEGIN
	RAISERROR (50100,16,1,'CertifiedProgramID')
	RETURN @@ERROR
    END

    IF @MakeID IS NULL
    BEGIN
	RAISERROR (50100,16,1,'MakeID')
	RETURN @@ERROR
    END

    IF NOT EXISTS (SELECT 1 FROM Certified.CertifiedProgram WHERE CertifiedProgramID = @CertifiedProgramID)
    BEGIN
	RAISERROR (50106,16,1,'CertifiedProgram',@CertifiedProgramID)
	RETURN @@ERROR
    END

    IF NOT EXISTS (SELECT 1 FROM VehicleCatalog.Firstlook.Make WHERE MakeID = @MakeID)
    BEGIN
	RAISERROR (50106,16,1,'Make', @MakeID)
	RETURN @@ERROR
    END

    DELETE 
    FROM    Certified.CertifiedProgram_Make
    WHERE   MakeID = @MakeID 
    AND CertifiedProgramID = @CertifiedProgramID

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
