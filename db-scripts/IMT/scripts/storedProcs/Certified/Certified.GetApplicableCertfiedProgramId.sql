if exists ( select * from sys.objects where object_id = object_id(N'Certified.GetApplicableCertfiedProgramId') and type in ( N'P', N'PC' ) ) 
   drop procedure Certified.GetApplicableCertfiedProgramId
GO

create procedure [Certified].[GetApplicableCertfiedProgramId]
     @OwnerHandle varchar(36)
     ,@VehicleHandle varchar(36)
   , @ProgramId int output 
as 
   set NOCOUNT on

   begin try
         
	declare @OwnerEntityTypeID int
		, @OwnerEntityID int
		, @OwnerID int
		, @VehicleEntityTypeID int
		, @VehicleEntityID int
	
	exec Market.Pricing.ValidateParameter_OwnerHandle @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out,@OwnerID out

    if @OwnerEntityTypeID != 1
		raiserror('Only ownerIds of a dealer type are supported.', 16, 1)
      
	exec Market.Pricing.ValidateParameter_VehicleHandle @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
	
	declare @MakeName varchar(50)
	select @MakeName = Make
	from IMT.Certified.InventoryMake
	where InventoryID = @VehicleEntityID
	
	declare @MfrSpecified bit, @ProgramIsPersisted bit
	exec imt.Certified.GetValidatedProgramId @OwnerEntityID, @VehicleEntityID, @MakeName, @ValidatedProgramId = @ProgramId output, @IsManfacturerSpecified = @MfrSpecified output, @ProgramIsPersisted = @ProgramIsPersisted output
	
   end try

   begin catch
      exec dbo.sp_ErrorHandler
   end catch

