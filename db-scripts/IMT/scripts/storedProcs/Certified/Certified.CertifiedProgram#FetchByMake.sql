IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[CertifiedProgram#FetchByMake]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[CertifiedProgram#FetchByMake]



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[CertifiedProgram#FetchByMake]
	@MakeId INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Certified.CertifiedProgram#FetchByMake.sql,v 1.1.4.1.2.2 2010/06/02 18:40:29 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Returns the certified program id which is mapped to the specified make id.
 * 
 * Parameters
 * ----------
 *
 * @MakeId - The make id
 *
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @MakeId IS NULL
 * 
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @MakeId IS NULL
    BEGIN
	RAISERROR (50100,16,1,'MakeId')
	RETURN @@ERROR
    END

    SELECT CertifiedProgramID FROM Certified.CertifiedProgram_Make WHERE MakeID = @MakeId

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
