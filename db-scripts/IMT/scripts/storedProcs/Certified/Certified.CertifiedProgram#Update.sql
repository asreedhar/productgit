IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[CertifiedProgram#Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[CertifiedProgram#Update]



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[CertifiedProgram#Update]
    @CertifiedProgramID INT,
    @Name VARCHAR(50),
    @Text VARCHAR(100),
    @Active BIT,
    @UpdateUser VARCHAR(80),
    @MarketingName varchar(30) = null,
    @Icon VARCHAR(50) = NULL 
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Certified.CertifiedProgram#Update.sql,v 1.1.4.1.2.3 2010/06/03 19:42:39 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Updates a certified program
 * 
 * Input Parameters
 * ----------
 *
 * @CertifiedProgramID
 * @Name
 * @Text
 * @Active
 * @UpdateUser
 *
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @CertifiedProgramID IS NULL
 * 50100 - @Name IS NULL
 * 50100 - @Text IS NULL
 * 50100 - @Active IS NULL
 * 50111 - @Name Length
 * 50111 - @Text Length
 * 50106 - @CertifiedProgramID does not exist
 * 50113 - CertifiedProgram row exists already (by Name)
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @CertifiedProgramID IS NULL
    BEGIN
	RAISERROR (50100,16,1,'CertifiedProgramID')
	RETURN @@ERROR
    END

    IF @Name IS NULL
    BEGIN
	RAISERROR (50100,16,1,'Name')
	RETURN @@ERROR
    END

    IF @Text IS NULL
    BEGIN
	RAISERROR (50100,16,1,'Text')
	RETURN @@ERROR
    END

    IF @Active IS NULL
    BEGIN
	RAISERROR (50100,16,1,'Active')
	RETURN @@ERROR
    END

    IF (LEN(@Name) <1 OR LEN(@Name) >50)
    BEGIN
	RAISERROR (50111,16,1,'Name',1,50)
	RETURN @@ERROR
    END

    IF (@MarketingName is not null and LEN(@MarketingName) >30)
    BEGIN
	RAISERROR (50111,16,1,'MarketingName',0,30)
	RETURN @@ERROR
    END

    IF (LEN(@Text) <1 OR LEN(@Text) >100)
    BEGIN
	RAISERROR (50111,16,1,'Text',1,100)
	RETURN @@ERROR
    END

    IF NOT EXISTS (SELECT 1 FROM Certified.CertifiedProgram WHERE CertifiedProgramID = @CertifiedProgramID)
    BEGIN
	RAISERROR (50106,16,1,'CertifiedProgram', @CertifiedProgramID)
	RETURN @@ERROR
    END

    DECLARE @UpdateUserID INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @UpdateUser, @UpdateUserID OUTPUT

    IF EXISTS (SELECT 1 FROM Certified.CertifiedProgram WHERE Name = @Name AND CertifiedProgramID <> @CertifiedProgramID)
    BEGIN
	RAISERROR (50113,16,1,'CertifiedProgram')
	RETURN @@ERROR
    END
    
    UPDATE  Certified.CertifiedProgram
    SET	    Name = @Name,
        MarketingName = nullif(@MarketingName, ''),
	    Text = @Text,
	    -- BusinessUnitId is not updatable for now.
	    Active = @Active,
	    Icon = @Icon,
	    UpdateUser = @UpdateUserID,
	    UpdateDate = GETDATE()
    WHERE CertifiedProgramId = @CertifiedProgramID

    -- Success
    RETURN 0

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
