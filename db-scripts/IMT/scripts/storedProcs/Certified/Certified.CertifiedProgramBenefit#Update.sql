IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[CertifiedProgramBenefit#Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[CertifiedProgramBenefit#Update]



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[CertifiedProgramBenefit#Update]
    @CertifiedProgramBenefitID INT,
    @Name VARCHAR(50),
    @Text VARCHAR(100),
    @Rank SMALLINT,
    @IsProgramHighlight BIT,
    @UpdateUser VARCHAR(80)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Certified.CertifiedProgramBenefit#Update.sql,v 1.1.4.1.2.2 2010/06/02 18:40:29 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Updates a certified program benefit
 * 
 * Input Parameters
 * ----------
 *
 * @CertifiedProgramBenefitID
 * @Name
 * @Text
 * @Rank
 * @IsProgramHighlight
 * @UpdateUser
 *
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @CertifiedProgramBenefitID IS NULL
 * 50100 - @Name IS NULL
 * 50100 - @Text IS NULL
 * 50100 - @Rank IS NULL
 * 50100 - @IsProgramHighlight IS NULL
 * 50111 - @Name length
 * 50111 - @Text length
 * 50106 - @CertifiedProgramBenefitID does not exist
 * 50113 - CertifiedProgramBenefit row exists already (by Name)
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @CertifiedProgramBenefitID IS NULL
    BEGIN
	RAISERROR (50100,16,1,'CertifiedProgramBenefitID')
	RETURN @@ERROR
    END

    IF @Name IS NULL
    BEGIN
	RAISERROR (50100,16,1,'Name')
	RETURN @@ERROR
    END

    IF @Text IS NULL
    BEGIN
	RAISERROR (50100,16,1,'Text')
	RETURN @@ERROR
    END

    IF @Rank IS NULL
    BEGIN
	RAISERROR (50100,16,1,'Rank')
	RETURN @@ERROR
    END

    IF @IsProgramHighlight IS NULL
    BEGIN
	RAISERROR (50100,16,1,'IsProgramHighlight')
	RETURN @@ERROR
    END

    IF (LEN(@Name) <1 OR LEN(@Name) >50)
    BEGIN
	RAISERROR (50111,16,1,'Name',1,50)
	RETURN @@ERROR
    END

    IF (LEN(@Text) <1 OR LEN(@Text) >100)
    BEGIN
	RAISERROR (50111,16,1,'Text',1,100)
	RETURN @@ERROR
    END

    DECLARE @UpdateUserID INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @UpdateUser, @UpdateUserID OUTPUT

    IF NOT EXISTS (SELECT 1 FROM Certified.CertifiedProgramBenefit WHERE CertifiedProgramBenefitID = @CertifiedProgramBenefitID)
    BEGIN
	RAISERROR (50106,16,1,'CertifiedProgramBenefit', @CertifiedProgramBenefitID)
	RETURN @@ERROR
    END

    DECLARE @CertifiedProgramID INT
    SET @CertifiedProgramID = (SELECT CertifiedProgramID FROM Certified.CertifiedProgramBenefit WHERE CertifiedProgramBenefitID = @CertifiedProgramBenefitID)

    IF EXISTS (SELECT 1 FROM Certified.CertifiedProgramBenefit WHERE CertifiedProgramID = @CertifiedProgramID AND Name = @Name AND CertifiedProgramBenefitID <> @CertifiedProgramBenefitID)
    BEGIN
	RAISERROR (50113,16,1,'Name')
	RETURN @@ERROR
    END

    -- Capture the rank of this row before the update
    DECLARE @OldRank SMALLINT
    SELECT  @OldRank = Rank 
    FROM    Certified.CertifiedProgramBenefit
    WHERE   CertifiedProgramBenefitID = @CertifiedProgramBenefitID

    -- Update everything but the rank
    UPDATE  Certified.CertifiedProgramBenefit
    SET	    Name = @Name,
	    Text = @Text,
	    IsProgramHighlight = @IsProgramHighlight,
	    UpdateUser = @UpdateUserID,
	    UpdateDate = GETDATE()
    FROM    Certified.CertifiedProgramBenefit
    WHERE   CertifiedProgramBenefitID = @CertifiedProgramBenefitID

    IF (@OldRank <> @Rank)
    BEGIN
	-- The rank has been updated, this will involve updating multiple rows (at least two)
	UPDATE  Certified.CertifiedProgramBenefit
	SET Rank = 
	    CASE
		-- this is the primary update, the others adjust according to this one.
		WHEN Rank = @OldRank THEN @Rank

		-- The primary rank is decreasing in value (moving up the list), so we need to
		-- increase the value of the other affected ranks (move them down the list).
		WHEN @Rank < @OldRank AND Rank >= @Rank AND Rank <= @OldRank THEN Rank + 1

		-- The primary rank is increasing in value (moving down the list), so we need to
		-- decrease the value of the other affected ranks (move them up the list).
		WHEN @Rank > @OldRank AND Rank <= @Rank AND Rank >= @OldRank THEN Rank - 1

		-- Just for sanity-sake, any that don't fit the above conditions will have
		-- an unchanged rank
		ELSE Rank
	    END,
	    UpdateUser = @UpdateUserID,
	    UpdateDate = GETDATE()
	FROM    Certified.CertifiedProgramBenefit
	WHERE CertifiedProgramID = @CertifiedProgramID
	AND (
	    (@Rank < @OldRank AND Rank >= @Rank AND Rank <= @OldRank)	-- Rank decreased in value (moved up the list)
		OR 
	    (@Rank > @OldRank AND Rank <= @Rank AND Rank >= @OldRank)	-- Rank increased in value (moved down the list)
	    )
    END

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
