if exists (select * from dbo.sysobjects where id = object_id(N'[Certified].[GetInventoryInconsistentWithMfrFeed]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Certified].[GetInventoryInconsistentWithMfrFeed]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Daniel Hillis
-- Create date: 2014-06-11
-- =============================================

CREATE PROCEDURE Certified.GetInventoryInconsistentWithMfrFeed
AS
BEGIN

SET NOCOUNT ON;

select i.BusinessUnitID, feed.Make, feed.InventoryID, feed.CertifiedProgramID, i.Certified as CurrentOEMCertificationStatus
FROM imt.Certified.InventoryMfrFeedCertifiedProgram feed
JOIN FLDW.dbo.InventoryActive i ON i.InventoryID = feed.InventoryID
WHERE NOT EXISTS
(
	SELECT *
	FROM imt.dbo.Inventory_CertificationNumber cn
	WHERE cn.InventoryID = feed.InventoryID
	AND cn.CertifiedProgramId = feed.CertifiedProgramID
)


END
GO
