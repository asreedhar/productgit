IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[OwnerCertifiedProgramInfoCollection#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[OwnerCertifiedProgramInfoCollection#Fetch]


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[OwnerCertifiedProgramInfoCollection#Fetch]
    @OwnerHandle VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Certified.OwnerCertifiedProgramInfoCollection#Fetch.sql,v 1.1.4.1.2.2 2010/06/02 18:40:29 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Fetches data that summarizes the certified programs with which an owner has chosen to be affiliated.
 * 
 * Input Parameters
 * ----------
 *
 * @OwnerHandle
 * 
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle IS NULL
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @OwnerHandle IS NULL
    BEGIN
	RAISERROR (50100,16,1,'OwnerHandle')
	RETURN @@ERROR
    END

    DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
    EXEC Market.Pricing.ValidateParameter_OwnerHandle @OwnerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT

    SELECT  ocp.OwnerCertifiedProgramID, 
	    cp.Name,
	    cp.Text
    FROM    Certified.OwnerCertifiedProgram ocp
    JOIN    Certified.CertifiedProgram cp
    ON	    ocp.CertifiedProgramID = cp.CertifiedProgramID
    WHERE   ocp.OwnerID = @OwnerID
    AND	    cp.Active = 1


END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
