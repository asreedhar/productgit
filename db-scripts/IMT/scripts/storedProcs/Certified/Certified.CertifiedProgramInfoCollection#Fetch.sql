IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[CertifiedProgramInfoCollection#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[CertifiedProgramInfoCollection#Fetch]



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[CertifiedProgramInfoCollection#Fetch]
    @ProgramType int
    ,@OwnerHandle varchar(36) = null
    ,@ActiveOnly bit = 0
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Certified.CertifiedProgramInfoCollection#Fetch.sql,v 1.1.4.1.2.2 2010/06/02 18:40:29 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * 
 
 Returns the certified programs by Program Type (Manufacturer/Dealer/Any) and Owner (optional).
 
 Program Type Values
 0 - None specified (Any)
 1 - Manufacturer
 2 - Dealer
 
 When Owner Handle is NULL:
	and Program Type is 0, return all Manufacturer and Dealer programs.
	and Program Type is 1, return all Manufacturer programs.
	and Program Type is 2, return all Dealer programs.
 
 
 When Owner Handle is NOT NULL:
	and Program Type is 0, return all Manufacturer programs and Dealer programs associated to dealer.
	and Program Type is 1, return all Manufacturer programs.
	and Program Type is 2, return Dealer programs associated to dealer.
 
 
 * 
 * Parameters
 * ----------
 *
 * @ProgramType
 * @OwnerHandle
 *
 * Exceptions
 * ----------
 *  
 * none
 * 
 * -------------------------------------------------------------------- */

BEGIN TRY

	if @ProgramType not in 
	(
		0, -- No program type specified
		1, -- manufacturer
		2 -- dealer
	)
		raiserror (50106,16,1,'ProgramType', @ProgramType);
		

	declare @ProgramIds table(id int)
	
	if (@OwnerHandle is null)
	begin
	-- no dealer specified
		if @ProgramType = 0
			insert into @ProgramIds
			select  CertifiedProgramID
			from Certified.CertifiedProgram cp
		else -- (1 or 2)
			insert into @ProgramIds
			select  CertifiedProgramID
			from Certified.CertifiedProgram cp
			where ProgramType = @ProgramType
	end
	else
	begin
	-- dealer specified

		 declare @OwnerEntityTypeID int
		, @OwnerEntityID int
		, @OwnerID int
		
		 exec Market.Pricing.ValidateParameter_OwnerHandle @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out,
		  @OwnerID out
		  
		  if @ProgramType in(0, 1)
		  begin
			-- we have a dealer specified, but it really doesn't matter for manufacturer programs
			insert into @ProgramIds
			select  CertifiedProgramID
			from Certified.CertifiedProgram cp
			where ProgramType = 1
		  end
		  
		  
		  if @ProgramType in(0, 2)
		  begin
			insert into @ProgramIds
			select  CertifiedProgramID
			from Certified.CertifiedProgram cp
			where cp.ProgramType = 2
			and exists 
			(
				select *
				from imt.dbo.BusinessUnitRelationship bur
				where bur.BusinessUnitID = @OwnerEntityID
				and (
						bur.BusinessUnitID = cp.BusinessUnitId  -- program belongs to dealer
						or bur.ParentID = cp.BusinessUnitId -- program belongs to dealer's parent
					)
			)
			
		  end
	end
	
	select CertifiedProgramID,
		Name,
		BusinessUnitId,
		Text,
		Active,
		MarketingName
	from Certified.CertifiedProgram cp
	where exists (select * from @ProgramIds p where cp.CertifiedProgramID = p.id)
	and ( @ActiveOnly = 0 or cp.Active = 1 )
	

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
