IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[CertifiedProgram#ExistsByName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[CertifiedProgram#ExistsByName]



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[CertifiedProgram#ExistsByName]
	@Name VARCHAR(50)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Certified.CertifiedProgram#ExistsByName.sql,v 1.1.4.1.2.2 2010/06/02 18:40:29 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Returns a value indicating whether or not the certified program exists, by certified program name
 * 
 * Parameters
 * ----------
 *
 * @Name - Certified Program Name
 *
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @Name IS NULL
 * 
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @Name IS NULL
    BEGIN
	RAISERROR (50100,16,1,'Name')
	RETURN @@ERROR
    END

    DECLARE @Exists BIT
    SET @Exists = 0

    IF EXISTS ( SELECT 1 FROM Certified.CertifiedProgram WHERE Name = @Name)
	SET @Exists = 1

    RETURN @Exists

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
