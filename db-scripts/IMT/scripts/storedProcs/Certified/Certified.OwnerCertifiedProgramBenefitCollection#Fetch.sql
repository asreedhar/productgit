IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[OwnerCertifiedProgramBenefitCollection#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[OwnerCertifiedProgramBenefitCollection#Fetch]


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[OwnerCertifiedProgramBenefitCollection#Fetch]
    @OwnerHandle VARCHAR(36),
    @CertifiedProgramID INT,
    @OwnerCertifiedProgramID INT OUTPUT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Certified.OwnerCertifiedProgramBenefitCollection#Fetch.sql,v 1.1.4.1.2.2 2010/06/02 18:40:29 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Fetches rows that represent an owner certified program benefit collection
 * 
 * Input Parameters
 * ----------
 *
 * @OwnerHandle
 * @CertifiedProgramID
 * 
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle IS NULL
 * 50100 - @CertifiedProgramID IS NULL
 * 50106 - @CertifiedProgramID does not exist
 * 50106 - OwnerCertifiedProgram does not exist
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @OwnerHandle IS NULL
    BEGIN
	RAISERROR (50100,16,1,'OwnerHandle')
	RETURN @@ERROR
    END

    IF @CertifiedProgramID IS NULL
    BEGIN
	RAISERROR (50100,16,1,'CertifiedProgramID')
	RETURN @@ERROR
    END

    IF NOT EXISTS (SELECT 1 FROM Certified.CertifiedProgram WHERE CertifiedProgramID = @CertifiedProgramID)
    BEGIN
	RAISERROR (50106,16,1,'CertifiedProgram', @CertifiedProgramID)
	RETURN @@ERROR
    END

    DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
    EXEC Market.Pricing.ValidateParameter_OwnerHandle @OwnerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT

    SELECT  @OwnerCertifiedProgramID = OwnerCertifiedProgramID
    FROM    Certified.OwnerCertifiedProgram
    WHERE   OwnerID = @OwnerID
    AND	    CertifiedProgramID = @CertifiedProgramID 

    IF @OwnerCertifiedProgramID IS NULL
    BEGIN
	RAISERROR (50106,16,1,'OwnerCertifiedProgram')
	RETURN @@ERROR
    END

    SELECT  ocpb.OwnerCertifiedProgramBenefitID,
	    cpb.Name,
	    cpb.Text,
	    ocpb.Rank,
	    ocpb.IsProgramHighlight
    FROM    Certified.OwnerCertifiedProgramBenefit ocpb
    JOIN    Certified.CertifiedProgramBenefit cpb
    ON	    ocpb.CertifiedProgramBenefitID = cpb.CertifiedProgramBenefitID
    WHERE   ocpb.OwnerCertifiedProgramID = @OwnerCertifiedProgramID
    ORDER BY ocpb.Rank


END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
