IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[CertifiedProgram#Exists]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[CertifiedProgram#Exists]



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[CertifiedProgram#Exists]
	@CertifiedProgramID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Certified.CertifiedProgram#Exists.sql,v 1.1.4.1.2.2 2010/06/02 18:40:29 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Returns a value indicating whether or not the certified program exists, by certified program id
 * 
 * Parameters
 * ----------
 *
 * @CertifiedProgramID - Certified Program ID
 *
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @CertifiedProgramID IS NULL
 * 
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @CertifiedProgramID IS NULL
    BEGIN
	RAISERROR (50100,16,1,'CertifiedProgramID')
	RETURN @@ERROR
    END

    DECLARE @Exists BIT
    SET @Exists = 0

    IF EXISTS ( SELECT 1 FROM Certified.CertifiedProgram WHERE CertifiedProgramID = @CertifiedProgramID)
	SET @Exists = 1

    RETURN @Exists

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
