if exists (select * from dbo.sysobjects where id = object_id(N'[Certified].[GetCertifiedProgramsForOwnerMake]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Certified].[GetCertifiedProgramsForOwnerMake]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Daniel Hillis
-- Create date: 2014-05-14
-- =============================================

CREATE PROCEDURE Certified.GetCertifiedProgramsForOwnerMake
	@OwnerHandle varchar(36),
	@Make varchar(50)
AS
BEGIN

SET NOCOUNT ON;

	if @Make is null 
		raiserror (50100,16,1,'Make')

	if not exists ( select *
					from   VehicleCatalog.Firstlook.Make
					where  Make = @Make )
		raiserror ('No Make row found with a Make value of %s.', 16, 1, @Make)
		
	declare @OwnerEntityTypeID int, @OwnerEntityID int, @OwnerID int 

	-- these calls will raise the appropriate errors if the handles are invalid.
	exec Market.Pricing.ValidateParameter_OwnerHandle @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out

	if (@OwnerEntityTypeID != 1)
		raiserror('Only dealers (OwnerEntityTypeID = 1) are supported.  Owner with Handle %s resolved to OwnerEntityTypeID %d', 16, 1, @OwnerHandle, @OwnerEntityTypeID)

	select * from Certified.GetPrograms(@Make, @OwnerId)

END
GO

GRANT EXECUTE ON [Certified].[GetCertifiedProgramsForOwnerMake] TO MerchandisingUser 
GO