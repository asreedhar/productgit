
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[MakeInfoCollection#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[MakeInfoCollection#Fetch]



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[MakeInfoCollection#Fetch]
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Certified.MakeInfoCollection#Fetch.sql,v 1.1.4.1.2.2 2010/06/02 18:40:29 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Returns a collection of makes and their associated certified program id.
 * 
 * Parameters
 * ----------
 *
 * (none)
 *
 *
 * Exceptions
 * ----------
 * 
 * (none)
 * 
 * -------------------------------------------------------------------- */

BEGIN TRY

    SELECT  m.MakeID,
	    m.Make as Name,
	    cpm.CertifiedProgramID
    FROM    VehicleCatalog.Firstlook.Make m
    LEFT JOIN Certified.CertifiedProgram_Make cpm
    ON cpm.MakeID = m.MakeID

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
