IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[OwnerCertifiedProgram#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[OwnerCertifiedProgram#Fetch]


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[OwnerCertifiedProgram#Fetch]
	@OwnerHandle VARCHAR(36),
	@CertifiedProgramID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Certified.OwnerCertifiedProgram#Fetch.sql,v 1.1.4.1.2.3 2010/06/03 19:42:39 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Return an owner certified program given a programId and an owner handle.
 * 
 * Parameters
 * ----------
 *
 *
 * Exceptions
 * ----------
 * 
 * None
 *
 * History
 * ------- 
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

BEGIN TRY

    DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

    -- these calls will raise the appropriate errors if the handles are invalid.
    EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
    
	select cp.CertifiedProgramID, ocp.OwnerCertifiedProgramID, cp.Name, cp.Icon
	from Certified.CertifiedProgram cp
	left join Certified.OwnerCertifiedProgram ocp
		on cp.CertifiedProgramID = ocp.CertifiedProgramID
		and ocp.OwnerId = @OwnerId
	where cp.Active = 1
	and cp.CertifiedProgramID = @CertifiedProgramID    
	
END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH


 
 