IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[CertifiedProgramBenefit#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[CertifiedProgramBenefit#Fetch]



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[CertifiedProgramBenefit#Fetch]
	@CertifiedProgramBenefitID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Certified.CertifiedProgramBenefit#Fetch.sql,v 1.1.4.1.2.2 2010/06/02 18:40:29 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Returns a Certified Program Benefit by the certified program benefit id.
 * 
 * Parameters
 * ----------
 *
 * @CertifiedProgramBenefitID - Certified Program Benefit ID
 *
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @CertifiedProgramBenefitID IS NULL
 * 50106 - @CertifiedProgramBenefitID does not exist
 * 
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @CertifiedProgramBenefitID IS NULL
    BEGIN
		RAISERROR (50100,16,1,'CertifiedProgramBenefitID')
		RETURN @@ERROR
    END

    IF NOT EXISTS (SELECT 1 FROM Certified.CertifiedProgramBenefit WHERE CertifiedProgramBenefitID = @CertifiedProgramBenefitID)
    BEGIN
	    RAISERROR (50106,16,1,'CertifiedProgramBenefit', @CertifiedProgramBenefitID)
	    RETURN @@ERROR
    END


    SELECT  CertifiedProgramID,
	    CertifiedProgramBenefitID,
	    Name,
	    Text,
	    Rank,
	    IsProgramHighlight
    FROM    Certified.CertifiedProgramBenefit
    WHERE   CertifiedProgramBenefitID = @CertifiedProgramBenefitID


END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
