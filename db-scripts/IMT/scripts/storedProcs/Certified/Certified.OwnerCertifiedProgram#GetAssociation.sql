IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[OwnerCertifiedProgram#GetAssociation]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[OwnerCertifiedProgram#GetAssociation]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[OwnerCertifiedProgram#GetAssociation]
    @OwnerHandle VARCHAR(36),
    @CertifiedProgramID int,
    @Result int output

AS

/* --------------------------------------------------------------------
 * 
 * $Id: Certified.OwnerCertifiedProgram#GetAssociation.sql,v 1.1.4.1.2.2 2014/05/07 23:59:59 dhilis $
 * 
 * Summary
 * -------
 * 
 * Returns a value indicating the nature of the relationship between the dealer and program.
 * 
 * 
 * Input Parameters
 * ----------
 *
 * @OwnerHandle
 * @CertifiedProgramID
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle IS NULL
 * 50100 - @CertifiedProgramID IS NULL
 * 50106 - @CertifiedProgramID does not exist
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @OwnerHandle IS NULL
    BEGIN
	RAISERROR (50100,16,1,'OwnerHandle')
	RETURN @@ERROR
    END

    IF @CertifiedProgramID IS NULL
    BEGIN
	RAISERROR (50100,16,1,'CertifiedProgramID')
	RETURN @@ERROR
    END

    DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
    EXEC Market.Pricing.ValidateParameter_OwnerHandle @OwnerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT

    IF NOT EXISTS (SELECT * FROM Certified.CertifiedProgram WHERE CertifiedProgramID = @CertifiedProgramID)
    BEGIN
	RAISERROR (50106,16,1,'CertifiedProgram',@CertifiedProgramID)
	RETURN @@ERROR
    END

	-- order is important here.
	-- Explicit, then Implicit, else None

	select @Result = 
	case 
	when exists (
		select * 
		from Certified.OwnerCertifiedProgram 
		where OwnerID = @OwnerID AND CertifiedProgramID = @CertifiedProgramID
	) then 2 -- explicit association
	when exists(
		select * 
		from imt.Certified.CertifiedProgram cp
		where CertifiedProgramID = @CertifiedProgramID
		and exists (
			select *
			from imt.dbo.BusinessUnitRelationship bur
			where bur.BusinessUnitID = @OwnerEntityID
			and 
			( 
				bur.BusinessUnitID = cp.BusinessUnitId -- dealer program
				or bur.ParentID = cp.BusinessUnitId -- dealer group program
			)
		)
	) then 1 -- implicit association
	else 0 -- no  association
	end

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
