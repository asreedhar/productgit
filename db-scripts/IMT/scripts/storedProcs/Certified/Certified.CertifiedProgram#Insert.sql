IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[CertifiedProgram#Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[CertifiedProgram#Insert]



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[CertifiedProgram#Insert]
    @Name VARCHAR(50),
    @MarketingName VARCHAR(50) = null,
    @Text VARCHAR(100),
    @BusinessUnitId int,
    @Active BIT,
    @InsertUser VARCHAR(80),
    @CertifiedProgramID INT OUTPUT,
    @Icon VARCHAR(50) = NULL
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Certified.CertifiedProgram#Insert.sql,v 1.1.4.1.2.3 2010/06/03 19:42:39 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Inserts a certified program
 * 
 * Input Parameters
 * ----------
 *
 * @Name
 * @Text
 * @Active
 * @InsertUser
 * @Icon
 * 
 * Output Parameters
 * ----------
 *
 * @CertifiedProgramID
 *
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @Name IS NULL
 * 50100 - @Text IS NULL
 * 50100 - @Active IS NULL
 * 50111 - @Name
 * 50111 - @Text
 * 50113 - CertifiedProgram row exists already (by Name)
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @Name IS NULL
    BEGIN
	RAISERROR (50100,16,1,'Name')
	RETURN @@ERROR
    END

    IF @Text IS NULL
    BEGIN
	RAISERROR (50100,16,1,'Text')
	RETURN @@ERROR
    END

    IF @Active IS NULL
    BEGIN
	RAISERROR (50100,16,1,'Active')
	RETURN @@ERROR
    END

    IF (LEN(@Name) <1 OR LEN(@Name) >50)
    BEGIN
	RAISERROR (50111,16,1,'Name',1,50)
	RETURN @@ERROR
    END

    IF (LEN(@Text) <1 OR LEN(@Text) >100)
    BEGIN
	RAISERROR (50111,16,1,'Text',1,100)
	RETURN @@ERROR
    END

    IF (@MarketingName is not null and LEN(@MarketingName) >30)
    BEGIN
	RAISERROR (50111,16,1,'MarketingName',0,30)
	RETURN @@ERROR
    END

    DECLARE @InsertUserID INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @InsertUser, @InsertUserID OUTPUT

    IF EXISTS (SELECT 1 FROM Certified.CertifiedProgram WHERE Name = @Name)
    BEGIN
	RAISERROR (50113,16,1,'CertifiedProgram')
	RETURN @@ERROR
    END

    INSERT 
    INTO    Certified.CertifiedProgram
	    (Name,
	    MarketingName,
	    Text,
	    BusinessUnitId,
	    Icon,
	    Active,
	    InsertUser,
	    InsertDate)
    VALUES
	    (@Name,
	    nullif(@MarketingName,''),
	    @Text,
	    @BusinessUnitId,
	    @Icon,
	    @Active,
	    @InsertUserID,
	    GETDATE())

    SET	@CertifiedProgramID = SCOPE_IDENTITY()

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
