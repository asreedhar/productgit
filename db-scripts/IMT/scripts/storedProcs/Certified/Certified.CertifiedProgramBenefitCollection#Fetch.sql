IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[CertifiedProgramBenefitCollection#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[CertifiedProgramBenefitCollection#Fetch]


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[CertifiedProgramBenefitCollection#Fetch]
	@CertifiedProgramID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Certified.CertifiedProgramBenefitCollection#Fetch.sql,v 1.1.4.1.2.2 2010/06/02 18:40:29 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Returns a collection of certified program benefits for the certified program id
 * 
 * Parameters
 * ----------
 *
 * @CertifiedProgramID - Certified Program ID
 *
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @CertifiedProgramID IS NULL
 * 50106 - @CertifiedProgramID does not exist
 * 
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @CertifiedProgramID IS NULL
    BEGIN
		RAISERROR (50100,16,1,'CertifiedProgramID')
		RETURN @@ERROR
    END

    IF NOT EXISTS (SELECT 1 FROM Certified.CertifiedProgram WHERE CertifiedProgramID = @CertifiedProgramID)
    BEGIN
	    RAISERROR (50106,16,1,'CertifiedProgram', @CertifiedProgramID )
	    RETURN @@ERROR
    END


    SELECT  CertifiedProgramID,
	    CertifiedProgramBenefitID,
	    Name,
	    Text,
	    Rank,
	    IsProgramHighlight
    FROM    Certified.CertifiedProgramBenefit
    WHERE   CertifiedProgramID = @CertifiedProgramID
    ORDER BY Rank


END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
