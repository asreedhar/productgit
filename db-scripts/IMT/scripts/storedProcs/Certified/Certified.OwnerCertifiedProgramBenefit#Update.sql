IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[OwnerCertifiedProgramBenefit#Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[OwnerCertifiedProgramBenefit#Update]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[OwnerCertifiedProgramBenefit#Update]
    @OwnerCertifiedProgramBenefitID INT,
    @Rank SMALLINT,
    @IsProgramHighlight BIT,
    @UpdateUser VARCHAR(80)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Certified.OwnerCertifiedProgramBenefit#Update.sql,v 1.1.4.1.2.2 2010/06/02 18:40:29 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Updates an owner certified program benefit row
 * 
 * Input Parameters
 * ----------
 *
 * @OwnerCertifiedProgramBenefitID
 * @Rank
 * @IsProgramHighlight
 * @UpdateUser
 * 
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerCertifiedProgramBenefitID IS NULL
 * 50100 - @Rank IS NULL
 * 50100 - @IsProgramHighlight IS NULL
 * 50106 - @OwnerCertifiedProgramBenefitID does not exist
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @OwnerCertifiedProgramBenefitID IS NULL
    BEGIN
	RAISERROR (50100,16,1,'OwnerCertifiedProgramBenefitID')
	RETURN @@ERROR
    END

    IF @Rank IS NULL
    BEGIN
	RAISERROR (50100,16,1,'Rank')
	RETURN @@ERROR
    END

    IF @IsProgramHighlight IS NULL
    BEGIN
	RAISERROR (50100,16,1,'IsProgramHighlight')
	RETURN @@ERROR
    END

    IF NOT EXISTS (SELECT 1 FROM Certified.OwnerCertifiedProgramBenefit WHERE OwnerCertifiedProgramBenefitID = @OwnerCertifiedProgramBenefitID)
    BEGIN
	RAISERROR (50106,16,1,'OwnerCertifiedProgramBenefit', @OwnerCertifiedProgramBenefitID)
	RETURN @@ERROR
    END

    DECLARE @UpdateUserID INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @UpdateUser, @UpdateUserID OUTPUT


    -- Capture the rank and the ownercertifiedprogramid of this row before the update
    DECLARE @OldRank SMALLINT
    DECLARE @OwnerCertifiedProgramID INT
    SELECT  @OldRank = Rank,
	    @OwnerCertifiedProgramID = OwnerCertifiedProgramID
    FROM    Certified.OwnerCertifiedProgramBenefit
    WHERE   OwnerCertifiedProgramBenefitID = @OwnerCertifiedProgramBenefitID

    -- Update everything but the rank
    UPDATE  Certified.OwnerCertifiedProgramBenefit
    SET	    IsProgramHighlight = @IsProgramHighlight,
	    UpdateUser = @UpdateUserID,
	    UpdateDate = GETDATE()
    FROM    Certified.OwnerCertifiedProgramBenefit
    WHERE   OwnerCertifiedProgramBenefitID = @OwnerCertifiedProgramBenefitID

    IF (@OldRank <> @Rank)
    BEGIN
	-- The rank has been updated, this will involve updating multiple rows (at least two)
	UPDATE  Certified.OwnerCertifiedProgramBenefit
	SET Rank = 
	    CASE
		-- this is the primary update, the others adjust according to this one.
		WHEN Rank = @OldRank THEN @Rank

		-- The primary rank is decreasing in value (moving up the list), so we need to
		-- increase the value of the other affected ranks (move them down the list).
		WHEN @Rank < @OldRank AND Rank >= @Rank AND Rank <= @OldRank THEN Rank + 1

		-- The primary rank is increasing in value (moving down the list), so we need to
		-- decrease the value of the other affected ranks (move them up the list).
		WHEN @Rank > @OldRank AND Rank <= @Rank AND Rank >= @OldRank THEN Rank - 1

		-- Just for sanity-sake, any that don't fit the above conditions will have
		-- an unchanged rank
		ELSE Rank
	    END,
	    UpdateUser = @UpdateUserID,
	    UpdateDate = GETDATE()
	FROM Certified.OwnerCertifiedProgramBenefit
	WHERE OwnerCertifiedProgramID = @OwnerCertifiedProgramID
	AND (
	    (@Rank < @OldRank AND Rank >= @Rank AND Rank <= @OldRank)	-- Rank decreased in value (moved up the list)
		OR 
	    (@Rank > @OldRank AND Rank <= @Rank AND Rank >= @OldRank)	-- Rank increased in value (moved down the list)
	    )
    END

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
