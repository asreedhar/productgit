IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[ManufacturerOrDealerCertifiedProgram#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[ManufacturerOrDealerCertifiedProgram#Fetch]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[ManufacturerOrDealerCertifiedProgram#Fetch]
	@OwnerHandle VARCHAR(36),
	@CertifiedProgramID INT
AS

/* --------------------------------------------------------------------
 * 
 * 
 * Summary
 * -------
 * 
 * Return an owner certified program given a programId and an owner handle.
 * 
 * Parameters
 * ----------
 *
 *
 * Exceptions
 * ----------
 * 
 * None
 *
 * History
 * ------- 
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

BEGIN TRY

    DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

    -- these calls will raise the appropriate errors if the handles are invalid.
    EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
	
	select cp.CertifiedProgramID, ocp.OwnerCertifiedProgramID, cp.Name, cp.Icon
	from Certified.CertifiedProgram cp
	left join Certified.OwnerCertifiedProgram ocp
		on cp.CertifiedProgramID = ocp.CertifiedProgramID
		and ocp.OwnerId = @OwnerId
	where cp.Active = 1
	and cp.CertifiedProgramID = @CertifiedProgramID
	
	select cpb.Name, cpb.Text, cpb.CertifiedProgramBenefitID, ocpb.OwnerCertifiedProgramBenefitID, coalesce(ocpb.IsProgramHighlight,cpb.IsProgramHighlight) as IsProgramHighlight, coalesce(ocpb.Rank,cpb.Rank) as [Rank]
	from Certified.CertifiedProgram cp
	join Certified.CertifiedProgramBenefit cpb 
		on cp.CertifiedProgramID = cpb.CertifiedProgramID
	left join Certified.OwnerCertifiedProgram ocp
		on cp.CertifiedProgramID = ocp.CertifiedProgramID
		and ocp.OwnerId = @OwnerId
	left join Certified.OwnerCertifiedProgramBenefit ocpb 
		on cpb.CertifiedProgramBenefitID = ocpb.CertifiedProgramBenefitID
		and ocp.OwnerCertifiedProgramID = ocpb.OwnerCertifiedProgramID
	where cp.Active = 1
	and cp.CertifiedProgramID = @CertifiedProgramID	
	order by coalesce(ocpb.Rank, cpb.Rank)
	
END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH


 
 