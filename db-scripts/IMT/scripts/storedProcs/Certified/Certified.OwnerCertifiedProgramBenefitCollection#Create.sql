IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[OwnerCertifiedProgramBenefitCollection#Create]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[OwnerCertifiedProgramBenefitCollection#Create]


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[OwnerCertifiedProgramBenefitCollection#Create]
    @OwnerHandle VARCHAR(36),
    @CertifiedProgramID INT,
    @InsertUser VARCHAR(80),
    @OwnerCertifiedProgramID INT OUTPUT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Certified.OwnerCertifiedProgramBenefitCollection#Create.sql,v 1.1.4.1.2.2 2010/06/02 18:40:29 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Creates rows required for an owner certified program benefit collection
 * 
 * Input Parameters
 * ----------
 *
 * @OwnerHandle
 * @CertifiedProgramID
 * @InsertUser
 * 
 * Output Parameters
 * ----------
 *
 * @OwnerCertifiedProgramID
 *
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle IS NULL
 * 50100 - @CertifiedProgramID IS NULL
 * 50106 - @OwnerHandle does not exist
 * 50106 - @CertifiedProgramID does not exist
 * 50113 - OwnerCertifiedProgram exists already
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @OwnerHandle IS NULL
    BEGIN
	RAISERROR (50100,16,1,'OwnerHandle')
	RETURN @@ERROR
    END

    IF @CertifiedProgramID IS NULL
    BEGIN
	RAISERROR (50100,16,1,'CertifiedProgramID')
	RETURN @@ERROR
    END

    IF NOT EXISTS (SELECT 1 FROM Certified.CertifiedProgram WHERE CertifiedProgramID = @CertifiedProgramID)
    BEGIN
	RAISERROR (50106,16,1,'CertifiedProgram', @CertifiedProgramID )
	RETURN @@ERROR
    END

    DECLARE @InsertUserID INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @InsertUser, @InsertUserID OUTPUT

    DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
    EXEC Market.Pricing.ValidateParameter_OwnerHandle @OwnerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT

    IF EXISTS (SELECT 1 FROM Certified.OwnerCertifiedProgram WHERE OwnerID = @OwnerID AND CertifiedProgramID = @CertifiedProgramID)
    BEGIN
	RAISERROR (50113,16,1,'OwnerCertifiedProgram')
	RETURN @@ERROR
    END

    -- Add the OwnerCertifiedProgram row
    INSERT
    INTO    Certified.OwnerCertifiedProgram
	    (OwnerID,
	    CertifiedProgramID,
	    InsertUser,
	    InsertDate)
    VALUES
	    (@OwnerID,
	    @CertifiedProgramID,
	    @InsertUserID,
	    GETDATE())

    -- Get the ID of the 'just added' OwnerCertifiedProgram row
    -- This value is returned by this stored proc
    SELECT @OwnerCertifiedProgramID = SCOPE_IDENTITY()

    -- Add all of the benefits for this program under the specfied Owner
    INSERT 
    INTO    Certified.OwnerCertifiedProgramBenefit
	    (OwnerCertifiedProgramID,
	    CertifiedProgramBenefitID,
	    Rank,
	    IsProgramHighlight,
	    InsertUser,
	    InsertDate)
    SELECT
	    @OwnerCertifiedProgramID,
	    CertifiedProgramBenefitID,
	    Rank,
	    IsProgramHighlight,
	    @InsertUserID,
	    GETDATE()
    FROM    Certified.CertifiedProgramBenefit
    WHERE   CertifiedProgramID = @CertifiedProgramID


END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
