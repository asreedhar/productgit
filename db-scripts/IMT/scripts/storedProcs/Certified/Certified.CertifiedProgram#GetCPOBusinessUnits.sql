if exists ( select  *
            from    sys.objects
            where   object_id = object_id(N'[Certified].[CertifiedProgram#Fetch]')
                    and type in ( N'P', N'PC' ) ) 
    drop procedure [Certified].[CertifiedProgram#Fetch]



set ANSI_NULLS on
GO
set QUOTED_IDENTIFIER off
GO

create procedure [Certified].[CertifiedProgram#Fetch]
    @CertifiedProgramID int
as 

    begin try

        if @CertifiedProgramID is null 
            begin
                raiserror (50100,16,1,'CertifiedProgramID')
                return @@ERROR
            end

        if not exists ( select  1
                        from    Certified.CertifiedProgram
                        where   CertifiedProgramID = @CertifiedProgramID ) 
            begin
                raiserror (50106,16,1,'CertifiedProgram', @CertifiedProgramID)
                return @@ERROR
            end

        select  cp.CertifiedProgramID ,
                cp.Name ,
                cp.Text ,
                cp.Active ,
                cp.Icon ,
                p.PhotoURL
        from    Certified.CertifiedProgram cp
                left join Certified.CertifiedProgramPhoto cpp on cp.CertifiedProgramID = cpp.CertifiedProgramID
                left join dbo.Photos p on p.PhotoID = cpp.PhotoID
                left join dbo.PhotoTypes pt on pt.PhotoTypeID = p.PhotoTypeID
        where   cp.CertifiedProgramID = @CertifiedProgramID
                and ( pt.PhotoTypeID is null
                      or pt.Description = 'Logo'
                    )


    end try

    begin catch
        exec dbo.sp_ErrorHandler
    end catch
