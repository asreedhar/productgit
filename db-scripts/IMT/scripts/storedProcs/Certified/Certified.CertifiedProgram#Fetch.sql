IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Certified].[CertifiedProgram#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Certified].[CertifiedProgram#Fetch]



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Certified].[CertifiedProgram#Fetch]
	@CertifiedProgramID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Certified.CertifiedProgram#Fetch.sql,v 1.1.4.1.2.3 2010/06/03 19:42:39 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Returns a certified program by the certified program id.
 * 
 * Parameters
 * ----------
 *
 * @CertifiedProgramID - Certified Program ID
 *
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @CertifiedProgramID IS NULL
 * 50106 - @CertifiedProgramID does not exist
 * 
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @CertifiedProgramID IS NULL
    BEGIN
		RAISERROR (50100,16,1,'CertifiedProgramID')
		RETURN @@ERROR
    END

    IF NOT EXISTS (SELECT 1 FROM Certified.CertifiedProgram WHERE CertifiedProgramID = @CertifiedProgramID)
    BEGIN
	    RAISERROR (50106,16,1,'CertifiedProgram', @CertifiedProgramID)
	    RETURN @@ERROR
    END

    SELECT  cp.CertifiedProgramID,
	    cp.Name,
	    cp.MarketingName,
	    cp.Text,
	    cp.Active,
	    cp.Icon,
	    p.PhotoURL,
	    cp.BusinessUnitId
    FROM	Certified.CertifiedProgram cp
    LEFT JOIN	Certified.CertifiedProgramPhoto cpp ON cp.CertifiedProgramID = cpp.CertifiedProgramID
    LEFT JOIN	dbo.Photos p ON p.PhotoID = cpp.PhotoID
    LEFT JOIN	dbo.PhotoTypes pt on pt.PhotoTypeID = p.PhotoTypeID
    WHERE   cp.CertifiedProgramID = @CertifiedProgramID
    AND	    (pt.PhotoTypeID is null or pt.Description = 'Logo')


END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
