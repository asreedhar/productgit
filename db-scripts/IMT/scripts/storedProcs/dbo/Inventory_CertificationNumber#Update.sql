if exists (select * from dbo.sysobjects where id = object_id(N'dbo.Inventory_CertificationNumber#Update ') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure dbo.[Inventory_CertificationNumber#Update]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.[Inventory_CertificationNumber#Update]
	
	@InventoryId int,
	@CertifiedID varchar(50) = null,
	@ManufacturerID int,
	@UserName varchar(100),
	@CertifiedProgramId int = NULL,
	@CertifiedProgramAutoSavedDate datetime = NULL
	
AS
BEGIN

	SET NOCOUNT ON;
	
	IF (EXISTS( SELECT * FROM [IMT].dbo.Inventory_CertificationNumber WHERE InventoryID = @InventoryID))
		UPDATE [IMT].dbo.Inventory_CertificationNumber
			SET CertificationNumber = isnull(@CertifiedID, ''), -- not a nullable column. empty is valid.
				 ManufacturerID = @ManufacturerID,
				 ModifiedByUser = @UserName,
				 ModifiedDate = GETDATE(),
				 CertifiedProgramId = @CertifiedProgramId,
				 CertifiedProgramAutoSavedDate = 
					CASE 
						WHEN ((CertifiedProgramId IS NULL AND @CertifiedProgramId IS NULL)
						OR (CertifiedProgramId = @CertifiedProgramId)) 
							-- CertifiedProgramId did not change
						THEN CertifiedProgramAutoSavedDate -- so don't change the autosaveddate
						ELSE @CertifiedProgramAutoSavedDate
					END
		 WHERE InventoryID = @InventoryID
	ELSE
		INSERT [IMT].dbo.Inventory_CertificationNumber
		(InventoryID, CertificationNumber, ManufacturerID, ModifiedByUser, ModifiedDate, CertifiedProgramId, CertifiedProgramAutoSavedDate) 
		VALUES
		(@InventoryID, isnull(@CertifiedID, ''), @ManufacturerID, @UserName, GETDATE(), @CertifiedProgramId, @CertifiedProgramAutoSavedDate)
	
END
GO
