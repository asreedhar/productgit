IF OBJECT_ID('[dbo].[DsrUsage]', 'P') IS NOT NULL
    DROP PROCEDURE [dbo].[DsrUsage]
GO
CREATE PROCEDURE [dbo].[DsrUsage]
    (
      @startDate AS DATE
    , @endDate AS DATE
    )
AS
    BEGIN
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
        
                   SELECT   [i].[BusinessUnitID]
                              , COUNT(DISTINCT [i].[VehicleID]) AS [ActiveInventory]
							  INTO #inventory 
                       FROM     [IMT].[dbo].[Inventory] i
                       WHERE    [i].[InventoryType] = 2
                                AND [i].[InsertDate] <= @endDate
                                AND ISNULL([i].[DeleteDt], GETDATE()) >= @startDate
                       GROUP BY [i].[BusinessUnitID]
                  
                
                  SELECT   [i].[BusinessUnitID]
                              , COUNT(DISTINCT [i].[VehicleID]) AS [UsedRetailSales]
							  INTO #sales
                       FROM     [IMT].[dbo].[Inventory] i
                                INNER JOIN [IMT].[dbo].[tbl_VehicleSale] vs ON [vs].[InventoryID] = [i].[InventoryID]
                       WHERE    [vs].[DealDate] > @startDate
                                AND [vs].[SaleDescription] = 'R'
                                AND [i].[InventoryActive] = 0
                                AND [i].[InventoryType] = 2
                       GROUP BY [i].[BusinessUnitID]

               SELECT   [i].[BusinessUnitID]
                              , COUNT(DISTINCT [i].[VehicleID]) AS [AssumedSales]
							  INTO #assumedsales
                       FROM     [IMT].[dbo].[Inventory] i
                       WHERE    [i].[InventoryType] = 2
                                AND [i].[InventoryActive] = 0

                                AND [i].[DeleteDt] > @startDate
                       GROUP BY [i].[BusinessUnitID]
 
                SELECT   SWITCHOFFSET([a].[RequestTime], DATEPART(TZOFFSET, SYSDATETIMEOFFSET())) AS [DSR View]
                              , [a].[RemoteIP]
                              , [a].[VIN]
                              , [a].[LogName]
                              , [v].[VehicleID]
                              , [i].[InventoryID]
                              , [bu].[BusinessUnitID]
                              , ISNULL([bu].[BusinessUnit], 'Unknown') AS [BusinessUnit]
                              , [i].[InsertDate]
                              , [i].[DeleteDt]
                              , [vs].[DealDate]
                              , [vs].[SaleDescription]
                              , DATEDIFF(DAY, SWITCHOFFSET([a].[RequestTime], DATEPART(TZOFFSET, SYSDATETIMEOFFSET())), COALESCE([vs].[DealDate], [i].[DeleteDt])) AS [Days after View]
                              , CASE WHEN [vs].[InventoryID] IS NOT NULL THEN 'Valid sale record'
                                     WHEN [v].[VehicleID] IS NULL THEN 'Cannot match VIN'
                                     WHEN [i].[InventoryID] IS NULL THEN 'No Inventory Active At Request Time'
                                     WHEN [i].[DeleteDt] IS NULL THEN 'Inventory active'
                                     WHEN [i].[DeleteDt] IS NOT NULL
                                          AND [vs].[InventoryID] IS NULL THEN 'Dropped off inventory without sale record'
                                END AS [Disposition]
							INTO #details
                       FROM     [Insight].[dbo].[DigitalShowroomLog] a
                                LEFT OUTER JOIN [IMT].[dbo].[tbl_Vehicle] v ON [v].[Vin] = [a].[VIN]
                                LEFT OUTER JOIN [IMT].[dbo].[Inventory] i ON [i].[VehicleID] = [v].[VehicleID]
                                                                             AND [i].[InventoryType] = 2
                                                                             AND SWITCHOFFSET([a].[RequestTime], DATEPART(TZOFFSET, SYSDATETIMEOFFSET())) BETWEEN [i].[InsertDate]
                                                                                                                                                AND
                                                                                                                                                ISNULL([i].[DeleteDt],
                                                                                                                                                GETDATE())
                                LEFT OUTER JOIN [IMT].[dbo].[BusinessUnit] bu ON [bu].[BusinessUnitID] = [i].[BusinessUnitID]
                                LEFT OUTER JOIN [IMT].[dbo].[tbl_VehicleSale] vs ON [vs].[InventoryID] = [i].[InventoryID]
                       WHERE    SWITCHOFFSET([a].[RequestTime], DATEPART(TZOFFSET, SYSDATETIMEOFFSET())) BETWEEN @startDate
                                                                                                       AND     @endDate
                     
            SELECT  [cte].[BusinessUnitID]
                  , ISNULL([bu].[BusinessUnit], 'Unknown') AS [Dealer]
                  , ISNULL([bu2].[BusinessUnit], 'Unknown') AS [Group]
                  , [cte].[ActiveInventory] AS [Active Inventory]
                  , COUNT(DISTINCT [b].[VIN]) / CAST([cte].[ActiveInventory] AS SMALLMONEY) AS [% Inv with 1+ view]
                  , COUNT([b].[VIN]) / CAST([cte].[ActiveInventory] AS SMALLMONEY) AS [Inv vs DSR Views]
                  , ISNULL([C].[UsedRetailSales] / CAST(COUNT([b].[VIN]) AS SMALLMONEY), 0) AS [Sales vs DSR Views]
                  , ISNULL(COUNT(DISTINCT CASE WHEN [b].[DealDate] > [b].[DSR View]
                                             AND [b].[DealDate] BETWEEN @startDate AND @endDate THEN [b].[VIN]
                                        ELSE NULL
                                   END) / CAST([C].[UsedRetailSales] AS SMALLMONEY), 0) AS [% Sales with 1+ view]
                  , ISNULL([a].[Segmentation_Tier__c], 'Unknown') AS [Tier]
                  , ISNULL(NULLIF([a].[Account_Owner_Text__c], ''), 'Unknown') AS [AE]
                  , ISNULL([a].[Type], 'Unknown') AS [SalesForce Account Type]
            FROM    #details b
					INNER JOIN [Merchandising].[settings].[Merchandising] m ON [m].[businessUnitID] = [b].[BusinessUnitID] AND m.[MaxDigitalShowroom] = 1
                    LEFT OUTER JOIN [#inventory] cte ON [b].[BusinessUnitID] = [cte].[BusinessUnitID]
                    LEFT OUTER JOIN [IMT].[dbo].[BusinessUnit] bu ON [bu].[BusinessUnitID] = [cte].[BusinessUnitID]
                    LEFT OUTER JOIN [Insight].[SalesForce].[Account] a ON [bu].[BusinessUnitID] = [a].[Business_Unit_ID__c]
                                                                          AND [a].[ParentId] != ''
                    LEFT OUTER JOIN [IMT].[dbo].[BusinessUnitRelationship] bur ON [bur].[BusinessUnitID] = [bu].[BusinessUnitID]
                    LEFT OUTER JOIN [IMT].[dbo].[BusinessUnit] bu2 ON [bu2].[BusinessUnitID] = [bur].[ParentID]
                    LEFT OUTER JOIN [#sales] C ON [C].[BusinessUnitID] = [bu].[BusinessUnitID]
                    LEFT OUTER JOIN [#assumedsales] d ON [d].[BusinessUnitID] = [bu].[BusinessUnitID]
            GROUP BY [cte].[BusinessUnitID]
                  , [bu].[BusinessUnit]
                  , [C].[UsedRetailSales]
                  , [d].[AssumedSales]
                  , [bu2].[BusinessUnit]
                  , [cte].[ActiveInventory]
                  , [a].[Segmentation_Tier__c]
                  , [a].[Account_Owner_Text__c]
                  , [a].[Type]
            ORDER BY  2;
    END
GO