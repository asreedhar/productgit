USE IMT
GO

IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'MemberContactInfo#Fetch' AND SCHEMA_NAME(schema_id) = 'dbo')
	BEGIN
		DROP  Procedure  [dbo].[MemberContactInfo#Fetch]
	END
GO

CREATE Procedure [dbo].[MemberContactInfo#Fetch]
	@userName		VARCHAR(80),
	@businessUnitID INT
AS

BEGIN TRY

-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


IF @userName IS NULL
BEGIN
	RAISERROR (50100,16,1,'UserName')
	RETURN @@ERROR
END


SELECT M.FirstName,
	M.LastName,
	OfficePhoneNumber = COALESCE(M.OfficePhoneNumber, ''),
	OfficePhoneExtension = COALESCE(M.OfficePhoneExtension, ''),
	MobilePhoneNumber = COALESCE(M.MobilePhoneNumber, ''),
	EmailAddress = COALESCE(M.EmailAddress, '')
FROM dbo.Member M
WHERE M.Login = @userName


SELECT BU.BusinessUnit,
	OfficePhone = COALESCE(BU.OfficePhone, '')
FROM dbo.BusinessUnit BU
WHERE BU.BusinessUnitTypeID = 4 AND BusinessUnitID = @businessUnitID


END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH

GO