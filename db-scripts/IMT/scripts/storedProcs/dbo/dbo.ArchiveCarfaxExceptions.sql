IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'dbo.ArchiveCarfaxExceptions') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE dbo.ArchiveCarfaxExceptions
GO

CREATE PROCEDURE dbo.ArchiveCarfaxExceptions
AS 
    BEGIN
        SET NOCOUNT ON
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
        DECLARE @retentionDate DATETIME2(0) = DATEADD(MONTH, -1, CAST(GETDATE() AS DATE))
        DECLARE @exceptionsToDeleteCount INT
          , @interation INT = 1
          , @maxIterations INT = 50
          , @batchSize INT = 1000

        DECLARE @exceptionsToDelete TABLE
            (
              ExceptionId INT NOT NULL
            )

        INSERT  INTO @exceptionsToDelete
                ( [ExceptionId] )
                SELECT TOP ( @batchSize )
                        e.[ExceptionID]
                FROM    [Carfax].[Exception] e
                WHERE   [ExceptionTime] < @retentionDate
        SET @exceptionsToDeleteCount = @@ROWCOUNT

        WHILE ( @exceptionsToDeleteCount > 0
                AND @interation < @maxIterations
              ) 
            BEGIN
                SET @interation = @interation + 1
                DELETE  rpc_e
                OUTPUT  deleted.*
                        INTO Archive.Carfax.[ReportProcessorCommand_Exception]
                FROM    [Carfax].[ReportProcessorCommand_Exception] rpc_e
                        INNER JOIN @exceptionsToDelete e2d ON [rpc_e].[ExceptionID] = [e2d].[ExceptionId]

                DELETE  e
                OUTPUT  deleted.*
                        INTO Archive.Carfax.Exception
                FROM    [Carfax].[Exception] e
                        INNER JOIN @exceptionsToDelete e2d ON [e].[ExceptionID] = [e2d].[ExceptionId]

                DELETE  @exceptionsToDelete

                INSERT  INTO @exceptionsToDelete
                        ( [ExceptionId] )
                        SELECT TOP ( @batchSize )
                                e.[ExceptionID]
                        FROM    [Carfax].[Exception] e
                        WHERE   [ExceptionTime] < @retentionDate
                SET @exceptionsToDeleteCount = @@ROWCOUNT

        
            END
        SET NOCOUNT OFF    
        SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    END