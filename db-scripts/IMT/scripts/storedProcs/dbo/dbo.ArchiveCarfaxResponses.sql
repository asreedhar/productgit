IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'dbo.ArchiveCarfaxResponses')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE dbo.ArchiveCarfaxResponses
GO

CREATE PROCEDURE dbo.ArchiveCarfaxResponses
AS 
    BEGIN
        SET NOCOUNT OFF
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
        DECLARE @retentionDate DATETIME2(0) = DATEADD(MONTH, -6, CAST(GETDATE() AS DATE))
        DECLARE @exceptionsToDeleteCount INT
          , @interation INT = 1
          , @maxIterations INT = 50
          , @batchSize INT = 5000

        DECLARE @ResponsesToDelete TABLE
            (
              RequestId INT NOT NULL
            )

        INSERT  INTO @ResponsesToDelete
                SELECT TOP ( @batchSize )
                        [r].[RequestID]
                FROM    [Carfax].[Response] r
                        LEFT OUTER JOIN [Carfax].[Request] r2 ON [r2].[RequestID] = [r].[RequestID]
                WHERE   [r].[ReceivedDate] < @retentionDate
                        AND [r2].[RequestID] IS NULL
                        

        SET @exceptionsToDeleteCount = @@ROWCOUNT

        WHILE ( @exceptionsToDeleteCount > 0
                AND @interation < @maxIterations
              )
            BEGIN
                SET @interation = @interation + 1

                DELETE  r
                OUTPUT  deleted.*
                        INTO [Archive].[Carfax].[Response]
                FROM    [Carfax].[Response] r
                        INNER JOIN @ResponsesToDelete r2d ON [r].[RequestID] = [r2d].[RequestId]

                DELETE  @ResponsesToDelete

                INSERT  INTO @ResponsesToDelete
                        SELECT TOP ( @batchSize )
                                [r].[RequestID]
                        FROM    [Carfax].[Response] r
                        WHERE   [r].[ReceivedDate] < @retentionDate
                SET @exceptionsToDeleteCount = @@ROWCOUNT

        
            END
        SET NOCOUNT OFF    
        SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    END

GO


