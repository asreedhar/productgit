if object_id('dbo.InventoryCertified#Update') is not null
	drop procedure dbo.InventoryCertified#Update
go

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.InventoryCertified#Update
--------------------------------------------------------------------------------------------
--
--	Update the inventory certified flag and eStockCardLock/CertifiedLock ONLY if the 
--	flag changes
--	
---Parmeters--------------------------------------------------------------------------------
--	
@BusinessUnitId 	INT,
@InventoryId 		INT,
@IsCertified 		BIT
--
---History----------------------------------------------------------------------------------
--	
--	kmoeller	02/24/2011	7.?	Initial design/dev	
--	wgh		06/14/2011	hotfix	Adjusted eStockCardLock update	
--			09/13/2012	12.2	Replace eStockCardLock with CertifiedLock
--
--------------------------------------------------------------------------------------------
	
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE 	dbo.Inventory
	
	SET 	Certified 	= @IsCertified,
		CertifiedLock	= CASE WHEN Certified <> @IsCertified THEN 1 ELSE CertifiedLock END
				
	WHERE 	BusinessUnitID = @BusinessUnitID
		AND InventoryID = @InventoryID
			
END
