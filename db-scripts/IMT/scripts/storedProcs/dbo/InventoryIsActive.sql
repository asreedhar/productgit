if exists (select * from dbo.sysobjects where id = object_id(N'dbo.InventoryIsActive ') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure dbo.[InventoryIsActive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.[InventoryIsActive]
	@InventoryId int
AS
BEGIN

	SELECT InventoryActive
	FROM IMT.dbo.Inventory WITH (NOLOCK)
	WHERE InventoryID = @InventoryId
	
END
GO
