USE [IMT]
GO
IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'dbo.ArchiveCarfaxReports')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE dbo.ArchiveCarfaxReports
GO
CREATE PROCEDURE dbo.ArchiveCarfaxReports
AS 
    BEGIN
        SET NOCOUNT OFF
        SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
        DECLARE @retentionDate DATETIME2(0) = DATEADD(MONTH, -6, CAST(GETDATE() AS DATE))
        DECLARE @ReportToDeleteCount INT
          , @interation INT = 1
          , @maxIterations INT = 1
          , @batchSize INT = 10

        DECLARE @ReportsToDelete TABLE
            (
              [RequestID] INT NOT NULL
            )

        INSERT  INTO @ReportsToDelete
                SELECT TOP ( @batchSize )
                        [RequestID]
                FROM    [Carfax].[Report] r
                WHERE   r.[ExpirationDate] < @retentionDate

        SET @ReportToDeleteCount = @@ROWCOUNT

        WHILE ( @ReportToDeleteCount > 0
                AND @interation <= @maxIterations
              ) 
            BEGIN
                SET @interation = @interation + 1


                DELETE  r
                OUTPUT  deleted.*
                        INTO Archive.Carfax.Request
                FROM    [Carfax].Request r
                        INNER JOIN @ReportsToDelete r2d ON [r].[RequestID] = [r2d].[RequestID]

                DELETE  r
                OUTPUT  deleted.*
                        INTO Archive.Carfax.Report
                FROM    [Carfax].[Report] r
                        INNER JOIN @ReportsToDelete r2d ON [r].[RequestID] = [r2d].[RequestID]



                DELETE  @ReportsToDelete

                INSERT  INTO @ReportsToDelete
                SELECT TOP ( @batchSize )
                        [RequestID]
                FROM    [Carfax].[Report] r
                WHERE   r.[ExpirationDate] < @retentionDate
                SET @ReportToDeleteCount = @@ROWCOUNT

        
            END
        SET NOCOUNT OFF    
        SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    END
