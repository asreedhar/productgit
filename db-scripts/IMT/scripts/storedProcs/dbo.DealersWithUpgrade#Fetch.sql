-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-04-22
-- Description:	Get a list of BusinessUnitIds with a particular upgrade.
-- =============================================
USE IMT
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[DealersWithUpgrade#Fetch]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[DealersWithUpgrade#Fetch]
GO

CREATE PROCEDURE [dbo].[DealersWithUpgrade#Fetch] 
	-- Add the parameters for the stored procedure here
	@UpgradeId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT BusinessUnitID FROM IMT.dbo.DealerUpgrade WHERE Active = 1 AND DealerUpgradeCD = @UpgradeId
END
GO

GRANT EXECUTE ON [dbo].[DealersWithUpgrade#Fetch] TO firstlook
GO


