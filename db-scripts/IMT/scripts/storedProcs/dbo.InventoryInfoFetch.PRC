/****** Object:  StoredProcedure [dbo].[InventoryInfoFetch]    Script Date: 05/27/2015 16:41:11 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InventoryInfoFetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InventoryInfoFetch]
GO


/****** Object:  StoredProcedure [dbo].[InventoryInfoFetch]    Script Date: 05/27/2015 16:41:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Bhavesh G Khetpal>
-- Create date: <15/05/2015>
-- Description:	<SP to retrieve Inventory details for the MAX Pricing tool>
-- =============================================
CREATE PROCEDURE [dbo].[InventoryInfoFetch]
	-- Add the parameters for the stored procedure here
	@InventoryId INT,
	@DealerId INT
	
AS
BEGIN

DECLARE @VehicleId INT
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  
	SELECT @VehicleId = VehicleId 
	FROM IMT.dbo.Inventory IV WHERE IV.InventoryId = @InventoryId 
	
	
	
	IF (@DealerId != (SELECT BusinessUnitId  FROM IMT.dbo.Inventory IA
	WHERE  IA.InventoryId = @InventoryId))
	RAISERROR ( 'No users found for provided id ', 16, 10)
	
	ELSE
	
	SELECT  I.stocknumber AS StockNumber,I.InventoryID, V.VIN,V.VehicleYear AS Year,MMG.Make AS Make,MMG.Model AS Model,OC.chromeStyleID AS Trim ,CASE WHEN I.AgeInDays<1 THEN 1 ELSE I.AgeInDays END AS AgeInDays,V.BaseColor AS Color,  
    I.MileageReceived,I.ListPrice,I.UnitCost,CAST (I.CurrentVehicleLight AS INT) AS RiskLight,I.Certified,CN.CertifiedProgramId,CP.Name,MMG.ModelID,
    CAST (VCM.MakeID AS INT) AS MakeID 
  
    FROM FLDW.dbo.Vehicle V 
    JOIN Merchandising.builder.OptionsConfiguration OC on OC.InventoryId=@InventoryId
    LEFT JOIN FLDW.dbo.InventoryActive I ON I.VehicleID= V.VehicleId
    LEFT JOIN FLDW.dbo.MakeModelGrouping MMG ON MMG.GroupingDescriptionID=V.GroupingDescriptionID AND MMG.MakeModelGroupingID=V.MakeModelGroupingID
    LEFT JOIN IMT.dbo.Inventory_CertificationNumber CN ON I.InventoryID = CN.InventoryID 
    LEFT JOIN IMT.Certified.CertifiedProgram CP ON CN.CertifiedProgramId = CP.CertifiedProgramId
    LEFT JOIN [VehicleCatalog].[Categorization].[Make] VCM ON MMG.Make = VCM.Name
	WHERE V.Vehicleid = @VehicleId 
	
	

END







GO


