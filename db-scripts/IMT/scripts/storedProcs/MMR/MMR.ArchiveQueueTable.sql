IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MMR].[ArchiveQueueTable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [MMR].[ArchiveQueueTable]
GO
CREATE PROCEDURE [MMR].[ArchiveQueueTable] AS
BEGIN
DECLARE @rows AS INTEGER = 1
WHILE ( @rows > 0 )
    BEGIN
        DELETE TOP ( 10000 )
                qt
        OUTPUT  DELETED.*
                INTO [Archive].[MMR].[QueueTable]
        FROM    [IMT].[MMR].[QueueTable] qt
        WHERE   [qt].[DateCreated] < DATEADD(DAY, -30, CAST(GETDATE() AS DATE))
        SET @rows = @@ROWCOUNT
    END
END