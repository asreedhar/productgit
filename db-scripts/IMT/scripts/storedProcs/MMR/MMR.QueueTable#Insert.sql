/****** Object:  StoredProcedure [MMR].[QueueTable#Insert]    Script Date: 04/08/2014 18:48:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MMR].[QueueTable#Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [MMR].[QueueTable#Insert]
GO

USE [IMT]
GO

/****** Object:  StoredProcedure [MMR].[QueueTable#Insert]    Script Date: 04/08/2014 18:48:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	This stored procedure will insert data in Queue table. 
--				This will be called by Insert and update trigger on Inventory table
-- =============================================
CREATE PROCEDURE [MMR].[QueueTable#Insert]
	@InventoryId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE
@MID varchar(100)=NULL,
@RegionCode varchar(50)=NULL,
@AwaitingStatus INT = 1
	
	--GET MID and Region from Inventory
	Select @MID=MV.MID,@RegionCode=MV.RegionCode from IMT.MMR.Vehicle MV
	Inner Join Inventory I on I.MMRVehicleID=MV.MMRVehicleID
	Where I.InventoryID=@InventoryId
	
	--GET MID and region from Appraisal	If MID is not found from Inventory
	IF @MID IS NULL
	BEGIN
		Select top 1 @MID=MID,@RegionCode=RegionCode from IMT.MMR.Vehicle MV
		inner join IMT.dbo.Appraisals A on A.MMRVehicleID=MV.MMRVehicleID 
		inner join Inventory I on A.VehicleID=I.VehicleID
		where A.BusinessUnitID=I.BusinessUnitID and I.InventoryID=@InventoryId
		order by A.DateCreated desc
	END
	
	-- Insert statements for procedure here
	Insert into IMT.MMR.QueueTable
	(InventoryID
	,VIN
	,Mileage
	,MID
	,RegionCode
	,[Year]
	,StatusID	
	,Attempt
	,DateCreated)
	Select I.InventoryID,V.VIN,I.MileageReceived,@MID,@RegionCode,V.VehicleYear,@AwaitingStatus,0,GETDATE() from Inventory I 
	Inner Join IMT.dbo.Vehicle V on I.VehicleID=V.VehicleID	
	Where I.InventoryID=@InventoryId and I.InventoryType=2
	
END




GO