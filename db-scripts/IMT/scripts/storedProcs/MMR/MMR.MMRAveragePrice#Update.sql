IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[MMR].[MMRAveragePrice#Update]')
                    AND type IN ( N'P', N'PC' ) )
    DROP PROCEDURE [MMR].[MMRAveragePrice#Update]
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================


CREATE PROCEDURE [MMR].[MMRAveragePrice#Update]
    @UpdateDataTable MMR.MMRPriceData READONLY
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
        SET NOCOUNT ON;
	
	--Update Average Price and update date for Inventory mapped to MMRVehicle
        UPDATE  IMT.MMR.Vehicle
        SET     AveragePrice = dt.AveragePrice
              , DateUpdated = GETDATE()
        FROM    @UpdateDataTable dt
                INNER JOIN IMT.dbo.Inventory I ON I.InventoryID = dt.InventoryID
                INNER JOIN IMT.MMR.Vehicle MV ON I.MMRVehicleId = MV.MMRVehicleID
    
    
        DECLARE @mmrPKTable TABLE
            (
              MmrVehicleId INT
            , InventoryID INT
            )
    
    --Insert MID and Price for Inventory which does not have MMRVehicle ID
        MERGE INTO IMT.MMR.Vehicle
        USING
            ( SELECT    dt.MID
                      , dt.Region
                      , dt.AveragePrice
                      , GETDATE() UpdatedDate
                      , dt.InventoryID
              FROM      @UpdateDataTable AS dt
              WHERE     dt.InventoryID IN ( SELECT  I.InventoryId
                                            FROM    IMT.dbo.Inventory i
                                                    INNER JOIN @UpdateDataTable UDT ON I.InventoryID = UDT.InventoryID
                                                                                       AND I.MMRVehicleID IS NULL )
            ) temp
        ON 1 = 0
        WHEN NOT MATCHED THEN
            INSERT ( MID
                   , RegionCode
                   , AveragePrice
                   , DateUpdated
                   )
            VALUES ( temp.MID
                   , temp.Region
                   , temp.AveragePrice
                   , temp.UpdatedDate
                   )
        OUTPUT
            INSERTED.MMRVehicleId
          , temp.InventoryID
            INTO @mmrPKTable;	
	
		
--	--MAP MMR Vehicle ID in Inventory table
        UPDATE  Inventory
        SET     MMRVehicleID = mmr.MMRVehicleID
        FROM    @mmrPKTable mmr
                INNER JOIN Inventory I ON I.InventoryID = mmr.InventoryID 
	
   
    END


GO


