/****** Object:  StoredProcedure [MMR].[QueueTable#Fetch]    Script Date: 04/02/2014 16:49:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MMR].[QueueTable#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [MMR].[QueueTable#Fetch]
GO

USE [IMT]
GO

/****** Object:  StoredProcedure [MMR].[QueueTable#Fetch]    Script Date: 04/02/2014 16:49:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [MMR].[QueueTable#Fetch]
	-- Add the parameters for the stored procedure here	
	@RecordCount int,
	@MaxAttempts INT
AS
BEGIN

DECLARE
 @AwaitingStatus INT = 1,
 @FailedStatus INT = 4,
 @Processing INT = 2,
 
 @RegionCode varchar(50)
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    
    DECLARE @QueueFetchData TABLE
		(
		RowColumn INT,QueueID INT,InventoryID INT, 
		VIN Varchar (20),MID varchar (100),
		RegionCode varchar (20)
		,Mileage INT
		,[Year] Varchar (4)
		,DateCreated  DateTime) ;
    
    --select top 1 @RegionCode= ISNULL(RegionCode,'') from IMT.MMR.QueueTable Where StatusID<>@SuccessStatus group by RegionCode ORDER BY MAX(DateCreated);	 
    	 
	WITH QueueRecords AS
	(
		SELECT
		ROW_NUMBER() OVER(ORDER BY DateCreated,QueueID DESC) AS RowColumn
		,QueueID
		,InventoryID
		,VIN
		,MID
		,RegionCode
		,Mileage
		,[Year]
		,DateCreated 
		 from IMT.MMR.QueueTable
		 Where (StatusID=@AwaitingStatus  --Include only awaiting and Failed records
		 OR StatusID=@FailedStatus) --
		 AND Attempt<=@MaxAttempts
		 --and ISNULL(RegionCode,'')=@RegionCode
	)	
	
	INSERT INTO @QueueFetchData	
	SELECT 
        RowColumn
		,QueueID
		,InventoryID
		,VIN
		,MID
		,ISNULL(RegionCode,'') RegionCode
		,Mileage
		,[Year]
		,DateCreated 
	FROM QueueRecords 
	WHERE RowColumn <= @RecordCount;
	
	
	Update QueueTable SET
	StatusID=@Processing
	,Attempt=Attempt+1
	,QueueSubmitDate=GETDATE()
    FROM @QueueFetchData QFD 
    INNER JOIN IMT.MMR.QueueTable QT on QT.QueueID=QFD.QueueID
    
    select * from @QueueFetchData
	
END




GO

