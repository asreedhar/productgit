IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MMR].[PurgeQueue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [MMR].[PurgeQueue]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	This stored procedure will delete records from Queue based on Number of days status ID
--				StatusID = 3 For Success
--				StatusID = 4 For Error			
-- =============================================
CREATE PROCEDURE [MMR].[PurgeQueue]
	@Days int,
	@StatusID int = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF @StatusID IS NOT NULL
		BEGIN
			DELETE from IMT.MMR.QueueTable where StatusID=@StatusID and CONVERT(Date,DateCreated) <= CONVERT(date,GETDATE()-@Days)
		END
	ELSE
		BEGIN
			DELETE from IMT.MMR.QueueTable where CONVERT(Date,DateCreated) <= CONVERT(date,GETDATE()-@Days)
		END
	
END


GO