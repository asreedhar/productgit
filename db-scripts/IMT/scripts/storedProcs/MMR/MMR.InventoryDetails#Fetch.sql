IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MMR].[InventoryDetails#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [MMR].[InventoryDetails#Fetch]
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [MMR].[InventoryDetails#Fetch]
	-- Add the parameters for the stored procedure here	
	@InventoryID int
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;    	 
	
	select I.BusinessUnitID,I.MMRVehicleID,I.InventoryActive,I.InventoryType,V.VehicleYear,
	MV.MID,MV.RegionCode,V.VIN,I.MileageReceived
	from IMT.dbo.Inventory I 
	Inner Join IMT.dbo.Vehicle V on V.VehicleID=I.VehicleID
	Left Join IMT.MMR.Vehicle MV on MV.MMRVehicleID=I.MMRVehicleID
	where I.InventoryID=@InventoryID
	 
END





GO