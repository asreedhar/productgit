/****** Object:  StoredProcedure [MMR].[QueueTable#Update]    Script Date: 04/04/2014 15:53:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MMR].[QueueTable#Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [MMR].[QueueTable#Update]
GO

USE [IMT]
GO

/****** Object:  StoredProcedure [MMR].[QueueTable#Update]    Script Date: 04/04/2014 15:53:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	This stored procedure will update Queue table 
-- =============================================
CREATE PROCEDURE [MMR].[QueueTable#Update]
	-- Add the parameters for the stored procedure here
	@QueueResult MMR.QueueResult ReadOnly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE 
	@MMRErrorStatus INT = 5,
	@ErrorStatus INT = 4
	
    Update IMT.MMR.QueueTable 
    Set StatusID=QR.StatusID
    ,ErrorMessage=QR.ErrorMessage   
    ,DateUpdated=GETDATE()
	from @QueueResult QR Inner join IMT.MMR.QueueTable QT 
	on QT.QueueID=QR.QueueID
	
END

GO
