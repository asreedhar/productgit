IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MMR].[PushPeriodicDataToQueue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [MMR].[PushPeriodicDataToQueue]
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	This stored procedure will insert Inventories for which Last MMR update date is 
--              greater than or equal to Number of days passed in Parameter


-- =============================================
CREATE PROCEDURE [MMR].[PushPeriodicDataToQueue]
	@days int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from	
	SET NOCOUNT ON;
	
	DECLARE
	@QueueStatusAwaiting int = 1

    -- Insert statements for procedure here
    insert into MMR.QueueTable
    (InventoryID
    ,VIN
    ,Mileage
    ,MID
    ,RegionCode
    ,[Year]
    ,DateCreated
    ,StatusID
    ,Attempt)
    select I.InventoryID,V.VIN,I.MileageReceived,MV.MID,MV.RegionCode,V.VehicleYear,GETDATE(),@QueueStatusAwaiting,0 from IMT.dbo.Inventory I
    inner join IMT.dbo.Vehicle V on V.VehicleID=I.VehicleID
    inner join IMT.MMR.Vehicle MV on MV.MMRVehicleID=I.MMRVehicleID
    Where I.InventoryActive=1 and MV.MID is not null and CONVERT(date,MV.DateUpdated)<=CONVERT(date,GETDATE()-@days)
	
END




GO


