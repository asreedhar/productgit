IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[MMR].[DatafeedException#Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [MMR].[DatafeedException#Insert]
GO
CREATE PROCEDURE [MMR].[DatafeedException#Insert]
	@MachineName nvarchar(256),
	@Message nvarchar(1024),
	@ExceptionType nvarchar(256),
    @Details ntext,
	@RequestMessage ntext,    
    @ExceptionUser varchar(50)
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the MMR.DatafeedException table.
 * 
 *	Parameters:
 *  @MachineName
 *  @Message
 *  @ExceptionType
 *  @Details
 *  @RequestMessage 
 *  @ExceptionUser 
  						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @ExceptionUserID INT, @err INT

EXEC @err = dbo.ValidateParameter_MemberID#UserName @ExceptionUser, @ExceptionUserID OUTPUT
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO MMR.DatafeedException (
	ExceptionTime,
    MachineName,
    Message,
    ExceptionType,
    Details,    
    ExceptionUserID,
	RequestMessage
) VALUES (
	getdate(),
	@MachineName,
	@Message,
	@ExceptionType,
    @Details,    
    @ExceptionUserID,
	@RequestMessage
)

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err



GO

