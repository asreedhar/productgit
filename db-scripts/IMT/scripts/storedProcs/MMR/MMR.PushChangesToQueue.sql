IF ( OBJECT_ID('[MMR].[PushChangesToQueue]', 'P') IS NOT NULL ) 
    DROP PROCEDURE [MMR].[PushChangesToQueue]
GO
CREATE PROCEDURE [MMR].[PushChangesToQueue]
    (
      @beginDate DATETIME2(0) = NULL
    , @endDate DATETIME2(0) = NULL
    )
AS
    BEGIN
        SET LOCK_TIMEOUT 10000 -- 10s

        DECLARE @beginLsn AS BINARY(10)
          , @endLsn AS BINARY(10)
          , @minLsn AS BINARY(10)
          , @maxLsn AS BINARY(10)
          , @activeOrdinal AS INTEGER
          , @mileageReceivedOrdinal AS INTEGER
          , @lookback_time AS DATETIME
          , @queueStatusAwaiting AS INTEGER = 1      

        SET @minLsn = [IMT].[sys].[fn_cdc_get_min_lsn]('dbo_Inventory')
        SET @maxLsn = [IMT].[sys].[fn_cdc_get_max_lsn]()
			 
        IF @beginDate IS NULL
            SET @beginDate = ( SELECT [c].[LastRun] FROM [MMR].[CdcStatus] [c]
                             )
			
        IF @endDate IS NULL
            SET @endDate = GETDATE()
		       
        SET @beginLsn = [IMT].[sys].[fn_cdc_map_time_to_lsn]('smallest greater than', @beginDate);
        SET @endLsn = [IMT].[sys].[fn_cdc_map_time_to_lsn]('largest less than or equal', @endDate);

		
        IF ( @beginLsn < @minLsn )
            SET @beginLsn = @minLsn
		                    
        IF ( @endLsn > @maxLsn )
            SET @endLsn = @maxLsn      
		       
        SET @activeOrdinal = [IMT].[sys].[fn_cdc_get_column_ordinal]('dbo_Inventory', 'InventoryActive')
        SET @mileageReceivedOrdinal = [IMT].[sys].[fn_cdc_get_column_ordinal]('dbo_Inventory', 'MileageReceived')
		 
        DECLARE @inventoryIds TABLE
            (
              [InventoryID] INTEGER NOT NULL
                                    PRIMARY KEY
            )

        DECLARE @inserts TABLE
            (
              [InventoryID] INTEGER NOT NULL
                                    PRIMARY KEY
            , VIN CHAR(17)
            , BusinessUnitID INT
            , VehicleID INT
            , Mileage INT
            , MID VARCHAR(16)
            , RegionCode VARCHAR(20)
            , [Year] INT
            , DateCreated DATETIME2
            , StatusID SMALLINT
            , Attempt INT
            )

        IF @beginLsn < @endLsn
            BEGIN
                PRINT 'Starting CDC at ' + CONVERT(VARCHAR(19), GETDATE(), 120)     
                INSERT  INTO @inventoryIds
                        ( [InventoryID]
                        )
                        SELECT  [a].[InventoryID]
                        FROM    [IMT].[cdc].[fn_cdc_get_all_changes_dbo_Inventory](@beginLsn, @endLsn, 'all') a
                        WHERE   ( [a].[__$operation] = 2
                                  OR [a].[__$operation] = 4
                                )
                                AND ( [IMT].[sys].[fn_cdc_is_bit_set](@activeOrdinal, [a].[__$update_mask]) = 1
                                      OR [IMT].[sys].[fn_cdc_is_bit_set](@mileageReceivedOrdinal, [a].[__$update_mask]) = 1
                                    )
                        GROUP BY [a].[InventoryID]

                PRINT 'Adding base inserts at ' + CONVERT(VARCHAR(19), GETDATE(), 120)
                INSERT  INTO @inserts
                        SELECT  [I].[InventoryID]
                              , [V].[VIN]
                              , [I].[BusinessUnitID]
                              , [V].[VehicleID]
                              , [I].[MileageReceived] AS Mileage
                              , [MV].[MID] AS MID
                              , [MV].[RegionCode] AS RegionCode
                              , [V].[VehicleYear] AS [Year]
                              , GETDATE() AS DateCreated
                              , @queueStatusAwaiting AS StatusID
                              , 0 AS Attempt
                        FROM    [IMT].[dbo].[Inventory] I
                                INNER JOIN @inventoryIds i2 ON [I].[InventoryID] = [i2].[InventoryID]
                                INNER JOIN [IMT].[dbo].[Vehicle] V ON [V].[VehicleID] = [I].[VehicleID]
                                LEFT OUTER JOIN [IMT].[MMR].[Vehicle] MV ON [MV].[MMRVehicleID] = [I].[MMRVehicleID]
                        WHERE   [I].[InventoryActive] = 1
                                AND [i].[InventoryType] = 2


                PRINT 'Adding appraisal values ' + CONVERT(VARCHAR(19), GETDATE(), 120)
                UPDATE  i
                SET     [RegionCode] = ISNULL(i.[RegionCode], a.[RegionCode])
                      , [MID] = ISNULL(i.[MID], a.[MID])
                FROM    @inserts i
                        INNER JOIN ( SELECT    [a].[VehicleID]
                                                  , [a].[BusinessUnitId]
                                                  , [MV2].[MID]
                                                  , [MV2].[RegionCode]
                                                  , ROW_NUMBER() OVER ( PARTITION BY a.[VehicleID] ORDER BY a.[DateCreated] DESC ) AS [RowNum]
                                          FROM      [IMT].[dbo].[Appraisals] a
                                                    INNER JOIN [IMT].[MMR].[Vehicle] MV2 ON [MV2].[MMRVehicleID] = [a].[MMRVehicleID]
                                        ) a ON [a].[VehicleID] = i.[VehicleID]
                                               AND [a].[BusinessUnitId] = [i].[BusinessUnitId]
                                               AND [a].[RowNum] = 1
                WHERE   i.[MID] IS NULL
                        OR i.[RegionCode] IS NULL

                PRINT 'Starting Queue Insert at ' + CONVERT(VARCHAR(19), GETDATE(), 120)
                INSERT  INTO [MMR].[QueueTable]
                        ( InventoryID
                        , VIN
                        , Mileage
                        , MID
                        , RegionCode
                        , [Year]
                        , DateCreated
                        , StatusID
                        , Attempt
                        )
                        SELECT  InventoryID, VIN, Mileage, MID, RegionCode, [Year], DateCreated, StatusID, Attempt
                        FROM    @inserts


                PRINT 'Updating Last Run value ' + CONVERT(VARCHAR(19), GETDATE(), 120)

                UPDATE  [MMR].[CdcStatus]
                SET     [CdcStatus].[lastrun] = @endDate 

                PRINT 'Completed at ' + CONVERT(VARCHAR(19), GETDATE(), 120)
            END
    END

GO
