IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.ValidateParameter_VIN') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE dbo.ValidateParameter_VIN AS SELECT 1')
GO

ALTER PROCEDURE  [dbo].[ValidateParameter_VIN]
	@VIN CHAR(17)
AS
/* --------------------------------------------------------------------
 * 
  * 
 * Summary
 * -------
 * 
 *	Verifies that the VIN exists  
 *	1. Is Not Null
 *	2. VIN = 17 characters
 *
 *
 *
 * Parameters
 * ----------
 *
 * @VIN  - VIN
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @VIN is null
 * 50114 - @VIN is not 17 characters
 *
 * History
 * -------
 * MAK	06/10/2009	Create procedure.

 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

IF @VIN IS NULL
BEGIN
	RAISERROR (50100,16,1,'VIN')
	RETURN @@ERROR
END


IF LEN(@VIN) <> 17

BEGIN
	RAISERROR (50114,16,1,@VIN)
	RETURN @@ERROR
END
 

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err
GO