IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.Dealer#Fetch') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE dbo.Dealer#Fetch AS SELECT 1')
GO
 
ALTER PROCEDURE [dbo].[Dealer#Fetch]
	@Id	INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.Dealer#Fetch.sql,v 1.4 2010/02/10 21:54:23 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns DealerID and DealerName from IMT..BusinessUnit
 * 
 * Parameters
 * ----------
 *
 * @DealerID   - ID that maps to a BusinessUnitID of Type 3 (Dealer) in the IMT..BusinessUnit table.
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @DealerID is null
 * 50106 - Record with DealerID does not exist
 * 50200 - Expected one Row
 *
 * History
 * ----------
 * 
 * MAK	05/22/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

IF @Id IS NULL
BEGIN
	RAISERROR (50100,16,1,'Id')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Declare Result Set
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE
		(ID	INT,
		Name	VARCHAR(40))

------------------------------------------------------------------------------------------------
-- Populate Result Set
------------------------------------------------------------------------------------------------

INSERT
INTO	@Results
		(ID,
		Name)
SELECT	BusinessUnitID,
		BusinessUnit
FROM	BusinessUnit B
WHERE	BusinessUnitID =@Id
AND		BusinessUnitTypeID = 4 -- Type Dealer

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Result Set
------------------------------------------------------------------------------------------------

IF @rc = 0
BEGIN
	RAISERROR (50106,16,1,'ID',@Id)
	RETURN @@ERROR
END

IF @rc > 1
BEGIN
	RAISERROR (50200,16,1,'ID',@Id)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO