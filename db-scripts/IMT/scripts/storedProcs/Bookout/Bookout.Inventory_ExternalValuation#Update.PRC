IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'Bookout.Inventory_ExternalValuation#Update') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE Bookout.Inventory_ExternalValuation#Update
GO


CREATE PROC Bookout.Inventory_ExternalValuation#Update
----------------------------------------------------------------------------------------------------------------------------
--
--	Update Bookout.Inventory_ExternalValuation and create a new "partial" bookout	 
--
----------------------------------------------------------------------------------------------------------------------------
--
@InventoryID		INT,
@ThirdPartyID		TINYINT,
@VehicleCode		VARCHAR(20),
@EquipmentCodes		VARCHAR(500),
@CPOCode		VARCHAR(20),
@InputHash		BINARY(20),
@LoadID			INT
--
---History------------------------------------------------------------------------------------------------------------------
--	
--	WGH	21.1	11/10/2014	Initial development
--
----------------------------------------------------------------------------------------------------------------------------
AS

SET NOCOUNT ON 

DECLARE @Step VARCHAR(255)

BEGIN TRY

	BEGIN TRANSACTION

	------------------------------------------------------------------------------------------------
	SET @Step = 'Update Bookout.Inventory_ExternalValuation'
	------------------------------------------------------------------------------------------------

	UPDATE	Bookout.Inventory_ExternalValuation
	SET	VehicleCode		= @VehicleCode,
		EquipmentCodes		= @EquipmentCodes,
		CPOCode			= @CPOCode,
		InputHash		= @InputHash,
		LoadID			= @LoadID
	WHERE	InventoryID = @InventoryID
		AND ThirdPartyID = @ThirdPartyID

	------------------------------------------------------------------------------------------------
	SET @Step = 'Create Partial Bookout'
	------------------------------------------------------------------------------------------------
		
	EXEC Bookout.InventoryBookout#CreatePartial @InventoryID, @ThirdPartyID, @VehicleCode

	------------------------------------------------------------------------------------------------
	SET @Step = 'Update Inventory (Bookout Required)'
	------------------------------------------------------------------------------------------------
	
	UPDATE	dbo.Inventory
	SET	BookoutRequired = BookoutRequired | POWER(2, @ThirdPartyID - 1)
	WHERE	InventoryID = @InventoryID	
	
	COMMIT TRANSACTION
	
END TRY
BEGIN CATCH
	WHILE(XACT_STATE()) <> 0
		ROLLBACK TRANSACTION
END CATCH
GO

EXEC sp_SetStoredProcedureDescription 'Bookout.Inventory_ExternalValuation#Update', 'Update Bookout.Inventory_ExternalValuation and create a new "partial" bookout'
