USE [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Bookout].[ThirdPartyVehicleHierarchy#Save]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Bookout].[ThirdPartyVehicleHierarchy#Save] AS SELECT 1')
GO

ALTER PROCEDURE [Bookout].[ThirdPartyVehicleHierarchy#Save]
	@ThirdPartyVehicleXML VARCHAR(MAX),
	@ThirdPartyVehicleID INT OUT,
	@Debug BIT = 0
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Bookout.ThirdPartyVehicleHierarchy#Save.PRC,v 1.4.4.2 2010/06/02 18:40:44 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Saves the ThirdPartyVehicles -> ThirdPartyVehicleOptions 
 *   									-> ThirdPartyVehicleOptionsValues graph.
 * 
 * Parameters
 * ----------
 *
 * @ThirdPartyVehicleXML  - XML describing the list of ThirdPartyOptions.  
 * The reason the XML datatype is not used is because JAVA generates UTF-8 XML
 * and SQL Server expects UTF-16
 *
 * Example:
 * 	<thirdPartyVehicle body="Touring Sport Utility 4D"
 *		description="MDX Touring Sport Utility 4D" engineType=""
 *		make="Acura Truck" makeCode="2" model="MDX" modelCode="3"
 *		status="true" thirdPartyId="3" thirdPartyVehicleCode="3">
 *	  <thirdPartyVehicleOption isStandardOption="false" sortOrder="0"
 *			status="true" thirdPartyOptionId="15528">
 *		<thirdPartyVehicleOptionValue thirdPartyOptionValueTypeId="10" value="0" />
 *		<thirdPartyVehicleOptionValue thirdPartyOptionValueTypeId="5" value="0" />
 *		<thirdPartyVehicleOptionValue thirdPartyOptionValueTypeId="9" value="0" />
 *		...
 *	  </thirdPartyVehicleOption>
 *	  ...
 *  </thirdPartyVehicle> 
 * 
 * Exceptions
 * ----------
 *
 * Changes
 * ----------
 *	12/14/2011	JDK	Modified to prevent duplicate keys
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

DECLARE @XML XML

SET @XML = @ThirdPartyVehicleXML

------------------------------------------------------------------------------------------------
--
-- Parse XML
--
------------------------------------------------------------------------------------------------

DECLARE @ThirdPartyVehicle TABLE (
	ThirdPartyID TINYINT NOT NULL
	, ThirdPartyVehicleCode VARCHAR(20) NOT NULL
	, [Description] VARCHAR(100)
	, MakeCode VARCHAR(2)
	, ModelCode VARCHAR(5)
	, [Status] TinyINT
	, EngineType VARCHAR(20)
	, Make VARCHAR(25)
	, Model VARCHAR(50)
	, Body VARCHAR(75)
)

DECLARE @ThirdPartyVehicleOptions TABLE (
	RowID INT NOT NULL --artificial id for inserting
	, InsertedID INT NULL --placeholder, to be updated after insert into table
	, ThirdPartyOptionID INT NOT NULL
	, IsStandardOption TINYINT NOT NULL
	, [Status] TINYINT
	, SortOrder INT 
)

DECLARE @ThirdPartyVehicleOptionValues TABLE (
	ParentRowID INT NOT NULL -- RowID from @ThirdPartyVehicleOptions
	, ThirdPartyOptionValueTypeID TINYINT
	, [Value] INT
)

INSERT INTO @ThirdPartyVehicle (
	ThirdPartyID
	, ThirdPartyVehicleCode
	, [Description]
	, MakeCode
	, ModelCode
	, [Status]
	, EngineType
	, Make
	, Model
	, Body
)
SELECT x.row.value('@thirdPartyId','TINYINT')
	, x.row.value('@thirdPartyVehicleCode', 'VARCHAR(20)')
	, x.row.value('@description', 'VARCHAR(100)')
	, x.row.value('@makeCode', 'VARCHAR(2)')
	, x.row.value('@modelCode','VARCHAR(5)')
	, CASE WHEN x.row.value('@status','VARCHAR(10)') = 'true' THEN 1 ELSE 0 END
	, x.row.value('@engineType','VARCHAR(20)')
	, x.row.value('@make','VARCHAR(25)')
	, x.row.value('@model','VARCHAR(50)')
	, x.row.value('@body','VARCHAR(75)')
FROM @XML.nodes('/thirdPartyVehicle') AS x(row)

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF(@Debug = 1)
BEGIN
	SELECT 'Parsed into @ThirdPartyVehicle'
	SELECT * FROM @ThirdPartyVehicle	
END

INSERT INTO @ThirdPartyVehicleOptions (
	RowID --artificial id for inserting
	, InsertedID --placeholder, to be updated after insert into table
	, ThirdPartyOptionID
	, IsStandardOption
	, [Status]
	, SortOrder
)
SELECT ROW_NUMBER() OVER (ORDER BY x.row.value('@sortOrder','INT'))
	, NULL
	, x.row.value('@thirdPartyOptionId', 'INT')
	, CASE WHEN x.row.value('@isStandardOption', 'VARCHAR(10)') = 'true' THEN 1 ELSE 0 END
	, CASE WHEN x.row.value('@status', 'VARCHAR(10)') = 'true' THEN 1 ELSE 0 END
	, x.row.value('@sortOrder','INT')
FROM @XML.nodes('/thirdPartyVehicle/thirdPartyVehicleOption') AS x(row)

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF(@Debug = 1)
BEGIN
	SELECT 'Parsed into @ThirdPartyVehicleOptions'
	SELECT * FROM @ThirdPartyVehicleOptions	
END

INSERT INTO @ThirdPartyVehicleOptionValues (
	ParentRowID -- RowID from @ThirdPartyVehicleOptions
	
	, ThirdPartyOptionValueTypeID
	, [Value]
	
)
SELECT VO.RowID AS ParentRowID
	, T2.tpvoValues.value('@thirdPartyOptionValueTypeId', 'TINYINT')
	, T2.tpvoValues.value('@value', 'INT')
FROM @XML.nodes('/thirdPartyVehicle/thirdPartyVehicleOption') AS T1(tpvOption)
CROSS APPLY T1.tpvOption.nodes('./thirdPartyVehicleOptionValue') AS T2(tpvoValues)
JOIN @ThirdPartyVehicleOptions VO ON T1.tpvOption.value('@thirdPartyOptionId','INT') = VO.ThirdPartyOptionID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF(@Debug = 1)
BEGIN
	SELECT 'Parsed into @ThirdPartyVehicleOptionValues'
	SELECT * FROM @ThirdPartyVehicleOptionValues	
END

------------------------------------------------------------------------------------------------
--
-- Inserts
--
------------------------------------------------------------------------------------------------

IF(@Debug = 0)
BEGIN
------------------------------------------------------------------------------------------------
-- ThirdPartyVehicles
------------------------------------------------------------------------------------------------
INSERT INTO dbo.ThirdPartyVehicles(
	ThirdPartyID, ThirdPartyVehicleCode, [Description], 
	MakeCode, ModelCode, [Status], EngineType, 
	Make, Model, Body
)
SELECT 
	ThirdPartyID, ThirdPartyVehicleCode, [Description], 
	MakeCode, ModelCode, [Status], EngineType, 
	Make, Model, Body
FROM 	@ThirdPartyVehicle

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

SELECT @ThirdPartyVehicleID = SCOPE_IDENTITY()

SELECT @err = @@ERROR
IF (@ThirdPartyVehicleID IS NULL OR @err <> 0) GOTO Failed

------------------------------------------------------------------------------------------------
-- ThirdPartyVehicleOptions
------------------------------------------------------------------------------------------------
BEGIN TRANSACTION insTPVO
BEGIN TRY
	
	DECLARE @CurrentRowID INT
		  , @MaxRowID INT
	
	SELECT @CurrentRowID = 1
		, @MaxRowID = MAX(RowID)
	FROM @ThirdPartyVehicleOptions
	
	WHILE (@CurrentRowID <= @MaxRowID)
	BEGIN
		INSERT INTO dbo.ThirdPartyVehicleOptions (
			ThirdPartyVehicleID, ThirdPartyOptionID, IsStandardOption
			, [Status], SortOrder 
		)
		SELECT 
			@ThirdPartyVehicleID, ThirdPartyOptionID, IsStandardOption
			, [Status], SortOrder 
		FROM @ThirdPartyVehicleOptions
		WHERE RowID = @CurrentRowID
	
		UPDATE @ThirdPartyVehicleOptions
		SET InsertedID = SCOPE_IDENTITY()
		WHERE RowID = @CurrentRowID
		
		SET @CurrentRowID = @CurrentRowID + 1
	END
	
	COMMIT TRANSACTION insTPVO
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
	BEGIN
		ROLLBACK TRANSACTION insTPVO
		SET @err = @@ERROR
		GOTO Failed
	END
END CATCH

------------------------------------------------------------------------------------------------
-- ThirdPartyVehicleOptionValues
------------------------------------------------------------------------------------------------
BEGIN TRANSACTION insTPVOV

SELECT 
	O.InsertedId AS [ThirdPartyVehicleOptionID], 
	OV.ThirdPartyOptionValueTypeID AS [ThirdPartyOptionValueTypeID], 
	OV.[Value] AS [Value]
INTO 	#aboutToInsert
FROM 	@ThirdPartyVehicleOptionValues OV
JOIN 	@ThirdPartyVehicleOptions O ON O.RowId = OV.ParentRowID

DELETE 	A 
FROM 	#aboutToInsert A
JOIN 	dbo.ThirdPartyVehicleOptionValues T ON A.ThirdPartyVehicleOptionID = T.ThirdPartyVehicleOptionID
	AND A.ThirdPartyOptionValueTypeID = T.ThirdPartyOptionValueTypeID

DELETE	A 
FROM 	#aboutToInsert A
WHERE 	A.ThirdPartyVehicleOptionID IS NULL OR A.ThirdPartyOptionValueTypeID IS NULL

INSERT INTO dbo.ThirdPartyVehicleOptionValues (
	ThirdPartyVehicleOptionID, ThirdPartyOptionValueTypeID, [Value]
)
SELECT
	ThirdPartyVehicleOptionID, 
	ThirdPartyOptionValueTypeID, 
	CASE 
		WHEN ABS(MAX([Value])) > ABS(MIN([Value])) 
		THEN MAX([Value]) 
		ELSE MIN([Value]) 
		END AS [Value]
FROM 	#aboutToInsert
GROUP 
BY 	ThirdPartyVehicleOptionID, ThirdPartyOptionValueTypeID
	
DROP TABLE #aboutToInsert	

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0
BEGIN
	ROLLBACK TRANSACTION insTPVOV
	GOTO Failed
END

COMMIT TRANSACTION insTPVOV

END

RETURN 0

Failed:
IF(@Debug = 1)
	BEGIN
		SELECT 'Tmp Table Dump...'
		SELECT * FROM @ThirdPartyVehicle
		SELECT * FROM @ThirdPartyVehicleOptions
		SELECT * FROM @ThirdPartyVehicleOptionValues	
	END
	RETURN @err
GO