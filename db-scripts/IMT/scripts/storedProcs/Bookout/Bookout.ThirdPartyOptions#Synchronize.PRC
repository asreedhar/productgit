SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Bookout].[ThirdPartyOptions#Synchronize]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Bookout].[ThirdPartyOptions#Synchronize] AS SELECT 1')
GO

ALTER PROCEDURE [Bookout].[ThirdPartyOptions#Synchronize]
	@OptionsXML VARCHAR(MAX)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Bookout.ThirdPartyOptions#Synchronize.PRC,v 1.4 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Looks up ThirdPartyOptionID for the Options passed.
 * Creates a new record in dbo.ThirdPartyOptions if the Option does not exist.
 * 
 * Parameters
 * ----------
 *
 * @OptionsXML  - XML describing the list of ThirdPartyOptions.  
 * The reason the XML datatype is not used is because JAVA generates UTF-8 XML
 * and SQL Server expects UTF-16
 *
 * Example:
 * <?xml version="1.0" encoding="UTF-8"?>
 * <thirdPartyOptions>
 * 	<thirdPartyOption optionKey="1800" optionName="4-Cyl. 2.0L VTEC" thirdPartyOptionTypeId="2"/>
 *  <thirdPartyOption optionKey="1801" optionName="5 Speed Manual" thirdPartyOptionTypeId="3"/>
 *  ...
 * </thirdPartyOptions>
 * 
 * Exceptions
 * ----------
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

DECLARE @XML XML

SET @XML = @OptionsXML

------------------------------------------------------------------------------------------------
-- Main
------------------------------------------------------------------------------------------------

DECLARE @ThirdPartyOptionsFromBook TABLE (
	OptionName VARCHAR(255) NOT NULL
	, OptionKey VARCHAR(255) NOT NULL
	, ThirdPartyOptionTypeID INT NOT NULL
)

INSERT INTO @ThirdPartyOptionsFromBook (
	OptionName
	, OptionKey
	, ThirdPartyOptionTypeID
)
SELECT 
	x.row.value('@optionName[1]', 'VARCHAR(255)') AS OptionName
	, x.row.value('@optionKey[1]', 'VARCHAR(255)') AS OptionKey
	, x.row.value('@thirdPartyOptionTypeId[1]', 'INT') AS ThirdPartyOptionTypeId
FROM @XML.nodes('/thirdPartyOptions/thirdPartyOption') AS x(row)

SELECT @rc = @@ROWCOUNT, @err = @@errOR
IF @err <> 0 GOTO Failed

/******************************
 * Insert new ThirdPartyOptions
 ******************************/
BEGIN TRANSACTION TpoCache;

INSERT INTO dbo.ThirdPartyOptions (
	OptionName
	, OptionKey
	, ThirdPartyOptionTypeId
)
SELECT 
	O.OptionName
	, O.OptionKey
	, O.ThirdPartyOptionTypeId
FROM @ThirdPartyOptionsFromBook O
LEFT JOIN   dbo.ThirdPartyOptions TPO
	ON O.OptionName = TPO.OptionName
	AND O.OptionKey = TPO.OptionKey
	AND O.ThirdPartyOptionTypeID = TPO.ThirdPartyOptionTypeID
WHERE TPO.ThirdPartyOptionID IS NULL;

COMMIT TRANSACTION TpoCache;

/************************************
 * Return list for app to synchronize
 ************************************/
SELECT
	TPO.ThirdPartyOptionID  
	, TPO.OptionName
	, TPO.OptionKey
	, TPO.ThirdPartyOptionTypeId
FROM @ThirdPartyOptionsFromBook O
LEFT JOIN  dbo.ThirdPartyOptions TPO
	ON O.OptionName = TPO.OptionName
	AND O.OptionKey = TPO.OptionKey
	AND O.ThirdPartyOptionTypeID = TPO.ThirdPartyOptionTypeID
	
SELECT @rc = @@ROWCOUNT, @err = @@errOR;
IF @err <> 0 GOTO Failed;

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
