IF OBJECT_ID ( '[WebImport].[Provider#Fetch]', 'P' ) IS NOT NULL 
DROP PROCEDURE [WebImport].[Provider#Fetch]
GO

CREATE Procedure [WebImport].[Provider#Fetch]
	@userName VARCHAR(50)	
AS

   SELECT ProviderID, Password, ProviderName
   FROM WebImport.Provider
   WHERE UserName = @userName


GO
