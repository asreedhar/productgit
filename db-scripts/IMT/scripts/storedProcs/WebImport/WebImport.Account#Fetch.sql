IF OBJECT_ID ( '[WebImport].[Account#Fetch]', 'P' ) IS NOT NULL 
DROP PROCEDURE [WebImport].[Account#Fetch]
GO

CREATE Procedure [WebImport].[Account#Fetch]
	@businessUnitID int,
	@actionType int	
AS

   SELECT ProviderID, Active
   FROM WebImport.Account
   WHERE BusinessUnitID = @businessUnitID AND ActionType = @actionType


GO