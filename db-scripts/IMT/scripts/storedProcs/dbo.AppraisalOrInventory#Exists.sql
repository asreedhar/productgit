
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.AppraisalOrInventory#Exists') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE dbo.AppraisalOrInventory#Exists AS SELECT 1')
GO
 
ALTER PROCEDURE [dbo].[AppraisalOrInventory#Exists]
	@vin varchar(17),
	@buid int
AS

---Description------------------------------------------------------------------
--
-- 	This proc returns:
--  -  Whether an appraisal exists that is active and has a type of "The Edge"
--     or Mobile, for a vin and dealer.
--  -  Whether there exists in active inventory a vehicle for a vin and dealer.
-- 
--

DECLARE @appExists BIT, @invExists BIT
set @appExists=0
set @invExists=0

------------------------------------------------------------------------------------------------
-- Declare Result Set
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE
	([appraisalExists] BIT, [inventoryExists] BIT)

------------------------------------------------------------------------------------------------
-- Populate Result Set
------------------------------------------------------------------------------------------------

if EXISTS(
	SELECT 1
		from imt.dbo.Appraisals A
		join imt.dbo.Vehicle V ON V.VehicleID=A.VehicleID
		join imt.dbo.DealerPreference DP ON DP.BusinessUnitID=@buid
		where V.VIN=@vin and A.BusinessUnitID=@buid and A.AppraisalStatusID=1
			and (A.AppraisalSourceID=1 or A.AppraisalSourceID=7)
			AND DATEDIFF(DAY, A.DateModified, GETDATE()) <= DP.SearchInactiveInventoryDaysBackThreshold)
	set @appExists=1

if EXISTS(
	SELECT 1
		FROM [IMT].[dbo].[Inventory] i 
		join IMT.dbo.Vehicle V on V.VehicleID = i.VehicleID
		join imt.dbo.DealerPreference DP ON DP.BusinessUnitID=@buid
		where i.BusinessUnitID=@buid and V.VIN=@vin
			AND (i.InventoryActive=1 OR (i.InventoryActive=0 AND DATEDIFF(DAY, i.DeleteDt, GETDATE()) <= dp.SearchInactiveInventoryDaysBackThreshold)))
	set @invExists=1
  
INSERT INTO @Results([appraisalExists], [inventoryExists]) VALUES (CAST(@appExists AS BIT), CAST(@invExists AS BIT))

SELECT * FROM @Results

RETURN 0

GO
grant execute on [dbo].[AppraisalOrInventory#Exists] to firstlook