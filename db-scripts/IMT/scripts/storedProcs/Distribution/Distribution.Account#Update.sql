IF OBJECT_ID(N'Distribution.Account#Update') IS NOT NULL
	DROP PROCEDURE Distribution.Account#Update
GO

CREATE PROCEDURE Distribution.Account#Update
	@DealerID int,
	@ProviderID int,
	@UserName varchar(50) = NULL,
	@Password varchar(50) = NULL,
    @DealershipCode varchar(50) = NULL,
	@Active bit,
    @UpdateUser varchar(50)
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Update a record in the Distribution.Account table. Triggers ensure an entry will be inserted 
 *  into Account_Audit for this operation.
 * 
 *	Parameters: 
 *  @DealerID,
 *  @ProviderID,
 *  @UserName
 *  @Password
 *  @Active
 *  @UpdateUser
 *
 * History
 * ----------
 * 
 * CGC	01/22/2010	Create procedure using CodeSmith 5.0.0.0
 * CGC  06/02/2010  Changed procedure to update dbo.InternetAdvertiserDealership instead of 
 *                  the Account table in the Distribution schema. 
 * CGC  06/18/2010  Changed procedure to read active status from 'DistributionActive' flag in
 *                  dbo.InternetAdvertiserDealership.
 * CGC  11/08/2010  Moving back Distribution.Account tables as we migrate off reprice processor.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @UpdateUserID INT, @err INT

EXEC @err = dbo.ValidateParameter_MemberID#UserName @UpdateUser, @UpdateUserID OUTPUT
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Perform Update
------------------------------------------------------------------------------------------------

UPDATE Distribution.Account 
SET	
	UserName = @UserName,
	Password = @Password,
    DealershipCode = @DealershipCode,
	Active = @Active,
    UpdateUserID = @UpdateUserID,
    UpdateDate = getdate()
WHERE
	DealerID = @DealerID
AND
    ProviderID = @ProviderID       

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err
    
GO
