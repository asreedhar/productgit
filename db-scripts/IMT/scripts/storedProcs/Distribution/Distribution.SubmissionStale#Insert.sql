IF OBJECT_ID(N'Distribution.SubmissionStale#Insert') IS NOT NULL
	DROP PROCEDURE Distribution.SubmissionStale#Insert
GO

CREATE PROCEDURE Distribution.SubmissionStale#Insert
    @SubmissionID int,
	@MessageTypes int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.SubmissionStale table.
 * 
 *	Parameters:
 *	@SubmissionID
 *	@MessageTypes 
 *
 * History
 * ----------
 * 
 * CGC	12/15/2010	Create procedure. 
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO Distribution.SubmissionStale (
	SubmissionID,
	MessageTypes,
	OccuredOn
) VALUES (
	@SubmissionID,
	@MessageTypes,
    getdate()
)

GO
