IF OBJECT_ID(N'Distribution.Publication#Insert') IS NOT NULL
	DROP PROCEDURE Distribution.Publication#Insert
GO

CREATE PROCEDURE Distribution.Publication#Insert
	@MessageID int,	
	@PublicationStatusID int,
	@InsertUser varchar(50),	
	@PublicationID int OUTPUT
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.Publication table.
 * 
 *	Parameters:
 *	@MessageID 
 *  @PublicationStatusID
 *  @InsertUser
 *  @PublicationID (output)
 *
 * History
 * ----------
 * 
 * CGC	01/22/2010	Create procedure using CodeSmith 5.0.0.0
 * CGC  02/22/2010  Changes to fit schema redesign.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @InsertUserID INT, @err INT

EXEC @err = dbo.ValidateParameter_MemberID#UserName @InsertUser, @InsertUserID OUTPUT
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO Distribution.Publication (
	MessageID,	
	PublicationStatusID,
	InsertUserID,
	InsertDate
) VALUES (
	@MessageID,	
	@PublicationStatusID,
	@InsertUserID,
	getdate()
)

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SET @PublicationID = SCOPE_IDENTITY()

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
