IF OBJECT_ID(N'Distribution.Content#Insert') IS NOT NULL
	DROP PROCEDURE Distribution.Content#Insert
GO

CREATE PROCEDURE Distribution.Content#Insert	
    @MessageID int,
    @ContentTypeID int,
    @ContentID int OUTPUT
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.Content and Advertisement_Content tables.
 * 
 *	Parameters:
 *	@MessageID
 *  @ContentTypeID
 *  @ContentID (output)
 *
 * History
 * ----------
 * 
 * CGC	01/22/2010	Create procedure using CodeSmith 5.0.0.0
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
    
------------------------------------------------------------------------------------------------
-- Insert into Content
------------------------------------------------------------------------------------------------    
    
INSERT INTO Distribution.Content (ContentTypeID) 
VALUES (@ContentTypeID)

SET @ContentID = SCOPE_IDENTITY()

------------------------------------------------------------------------------------------------
-- Insert into Advertisement_Content
------------------------------------------------------------------------------------------------

INSERT INTO Distribution.Advertisement_Content (ContentID, MessageID)
VALUES (@ContentID, @MessageID)

GO
