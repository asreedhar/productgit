USE [IMT]
GO

/****** Object:  StoredProcedure [Distribution].[Message#Fetch]    Script Date: 01/14/2014 15:29:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Distribution].[Message#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Distribution].[Message#Fetch]
GO

USE [IMT]
GO

/****** Object:  StoredProcedure [Distribution].[Message#Fetch]    Script Date: 01/14/2014 15:29:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [Distribution].[Message#Fetch]
	@MessageID int,
	@MessageTypes int = -1
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch a message record from the Distribution.Message table and from the appropriate sub-
 *  tables. Optional MessageTypes parameter narrows which message parts are received.
 * 
 *	Parameters:
 *  @MessageID
 *
 * History
 * ----------
 * 
 * CGC	01/22/2010	Create procedure using CodeSmith 5.0.0.0
 * CGC  02/22/2010  Changes to fit schema redesign.
 * CGC  11/04/2010  Updated to take optional MessageTypes parameter.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @VehicleEntityID int, @VehicleEntityTypeID int

------------------------------------------------------------------------------------------------
-- Fetch message data
------------------------------------------------------------------------------------------------

SELECT
	MessageID,
	DealerID,
    VehicleEntityID,
    VehicleEntityTypeID
FROM
	Distribution.Message
WHERE
	MessageID = @MessageID    

------------------------------------------------------------------------------------------------
-- Fetch body part data
------------------------------------------------------------------------------------------------

IF @MessageTypes = -1 OR (@MessageTypes & 1) = 1
	BEGIN
		EXEC Distribution.Price#Fetch @MessageID
	END
ELSE
	BEGIN
		SELECT TOP 0 * FROM Distribution.Price
	END	

IF @MessageTypes = -1 OR (@MessageTypes & 4) = 4
	BEGIN
		EXEC Distribution.VehicleInformation#Fetch @MessageID
	END	
ELSE
	BEGIN
		SELECT TOP 0 * FROM Distribution.VehicleInformation
	END	

IF @MessageTypes = -1 OR (@MessageTypes & 2) = 2
	BEGIN
		EXEC Distribution.Content#Fetch @MessageID
	END
ELSE
	BEGIN
		SELECT TOP 0 * FROM Distribution.Content
	END	
	
IF @MessageTypes = -1 OR (@MessageTypes & 8) = 8
	BEGIN
		EXEC Distribution.AppraisalValue#Fetch @MessageID
	END
ELSE
	BEGIN
		SELECT TOP 0 * FROM Distribution.AppraisalValue
	END	




GO


