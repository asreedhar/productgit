IF OBJECT_ID(N'Distribution.Submission#FetchSupersedingPrice') IS NOT NULL
	DROP PROCEDURE Distribution.Submission#FetchSupersedingPrice
GO

CREATE PROCEDURE Distribution.Submission#FetchSupersedingPrice
	@SubmissionID INT
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch the most recent price sent out for the dealer and provider of the submission with the
 *  given identifier.
 *
 * History
 * ----------
 * 
 * CGC  03/17/2011  Created procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Fetch first attempt of the given submission.
------------------------------------------------------------------------------------------------
DECLARE @FirstAttempt INT

SELECT
	@FirstAttempt = s2.SubmissionID
FROM
	Distribution.Submission s1
JOIN
	Distribution.Submission s2 ON s2.PublicationID = s1.PublicationID AND s2.ProviderID = s1.ProviderID	
WHERE
	s1.SubmissionID = @SubmissionID
AND
	s2.Attempt = 1

------------------------------------------------------------------------------------------------
-- Fetch failures.
------------------------------------------------------------------------------------------------

SELECT TOP 1
	s2.SubmissionID,
	pr.PriceTypeID,
	pr.Value
FROM
	Distribution.Submission s1
JOIN
	Distribution.Publication p1 ON p1.PublicationID = s1.PublicationID
JOIN
	Distribution.Message m1 ON m1.MessageID = p1.MessageID
JOIN
	Distribution.Message m2 ON m2.DealerID = m1.DealerID AND m2.VehicleEntityID = m1.VehicleEntityID AND m2.VehicleEntityTypeID = m1.VehicleEntityTypeID
JOIN
	Distribution.Publication p2 ON p2.MessageID = m2.MessageID
JOIN
	Distribution.Submission s2 ON s2.PublicationID = p2.PublicationID
JOIN
	Distribution.Price pr ON pr.MessageID = m2.MessageID
WHERE
	s2.PushedOnQueue > s1.PushedOnQueue
AND
	s1.SubmissionID = @FirstAttempt
AND
	s2.ProviderID = s1.ProviderID
AND
	s2.PublicationID <> s1.PublicationID
AND
	(s1.MessageTypes & s2.MessageTypes & 1) = 1
ORDER BY
	s2.PushedOnQueue DESC
	
GO
