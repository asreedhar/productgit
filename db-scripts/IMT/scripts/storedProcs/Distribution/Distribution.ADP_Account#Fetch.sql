USE [IMT]
GO

/****** Object:  StoredProcedure [Distribution].[ADP_Account#Fetch]    Script Date: 03/19/2014 16:24:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Distribution].[ADP_Account#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Distribution].[ADP_Account#Fetch]
GO

USE [IMT]
GO

/****** Object:  StoredProcedure [Distribution].[ADP_Account#Fetch]    Script Date: 03/19/2014 16:24:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: 01/21/2010
-- Description:	Returns 
-- =============================================
CREATE PROCEDURE [Distribution].[ADP_Account#Fetch] 
	@DealerID int	 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ADPM.PriceTypeID TargetId ,APT.Description [Description],ADPM.FieldTypeId TargetTypeId
	,CONVERT(varchar,ADPM.PriceTypeID)+'#'+CONVERT(varchar,ADPM.FieldTypeId) SelectedValue from 
	Distribution.ADP_DealerPriceMapping ADPM 
	left join Distribution.ADP_PriceType APT on APT.PriceTypeID=ADPM.PriceTypeID  
	where DealerID=@DealerID and ADPM.FieldTypeId=1
	UNION
	SELECT ADPM.PriceTypeID,DDF.Description,ADPM.FieldTypeId
	,CONVERT(varchar,ADPM.PriceTypeID)+'#'+CONVERT(varchar,ADPM.FieldTypeId) SelectedValue from 
	Distribution.ADP_DealerPriceMapping ADPM 
	left join Distribution.ADP_DealerDefinedFields DDF on DDF.DealerDefinedFieldId=ADPM.PriceTypeID  
	where DealerID=@DealerID and ADPM.FieldTypeId=2;
	
END


GO


