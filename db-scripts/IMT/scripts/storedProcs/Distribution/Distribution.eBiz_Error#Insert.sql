IF OBJECT_ID(N'Distribution.eBiz_Error#Insert') IS NOT NULL
	DROP PROCEDURE Distribution.eBiz_Error#Insert
GO

CREATE PROCEDURE Distribution.eBiz_Error#Insert
    @SubmissionID int,
	@FirstLookErrorTypeID int,
	@eBizErrorTypeID int,
    @MessageTypes int,
    @ErrorMessage varchar(2000) = NULL
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.eBiz_Error table.
 * 
 *	Parameters: 
 *	@SubmissionID
 *	@FirstLookErrorTypeID
 *  @eBizErrorTypeID 
 *  @MessageTypes
 *  @ErrorMessage
 *
 * History
 * ----------
 * 
 * CGC	04/8/2011	Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @ErrorID int

------------------------------------------------------------------------------------------------
-- Perform Insert on SubmissionError
------------------------------------------------------------------------------------------------
EXEC Distribution.SubmissionError#Insert @SubmissionID, @FirstLookErrorTypeID, @MessageTypes, @ErrorID OUTPUT

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO Distribution.eBiz_Error (
	ErrorID,
	ErrorTypeID,
    ErrorMessage
) VALUES (
	@ErrorID,
	@eBizErrorTypeID,
    @ErrorMessage
)

GO
