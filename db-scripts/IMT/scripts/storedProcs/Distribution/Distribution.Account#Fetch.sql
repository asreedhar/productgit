IF OBJECT_ID(N'Distribution.Account#Fetch') IS NOT NULL
	DROP PROCEDURE Distribution.Account#Fetch
GO

CREATE PROCEDURE Distribution.Account#Fetch
	@DealerID int,
    @ProviderID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch all records from the Distribution.Account table for the given dealer and provider.
 *
 *  NOTE: Until reprice processor is officially retired in favor of the Distribution system,
 *  account details will be fetched from dbo.InternetAdvertiserDealership instead of Account.
 * 
 *	Parameters:
 *	@DealerID
 *  @ProviderID
 *
 * History
 * ----------
 * 
 * CGC	01/22/2010	Create procedure using CodeSmith 5.0.0.0
 * CGC  06/02/2010  Changed procedure to read from dbo.InternetAdvertiserDealership instead 
 *                  of from Account. 
 * CGC  06/18/2010  Changed procedure to read active status from 'DistributionActive' flag in
 *                  dbo.InternetAdvertiserDealership.
 * CGC  11/08/2010  Moving back Distribution.Account tables as we migrate off reprice processor.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Perform Fetch
------------------------------------------------------------------------------------------------

SELECT	
	DealerID,
	ProviderID,
	UserName,
	Password,
    DealershipCode,
	Active
FROM
	Distribution.Account
WHERE
	DealerID = @DealerID
AND ProviderID = @ProviderID

GO
    