IF OBJECT_ID(N'Distribution.ContentText#Insert') IS NOT NULL
	DROP PROCEDURE Distribution.ContentText#Insert
GO

CREATE PROCEDURE Distribution.ContentText#Insert	
    @MessageID int,
	@Text varchar(2000)
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.Content, ContentText and 
 *  AdvertisementContent tables.
 * 
 *	Parameters:
 *	@MessageID  
 *  @Text
 *
 * History
 * ----------
 * 
 * CGC	01/22/2010	Create procedure using CodeSmith 5.0.0.0
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON

DECLARE @err INT, @ContentTypeID int, @ContentID int

SET @ContentTypeID = 1

------------------------------------------------------------------------------------------------
-- Insert into Content
------------------------------------------------------------------------------------------------    

EXEC @err = Distribution.Content#Insert @MessageID, @ContentTypeID, @ContentID OUTPUT
IF @err <> 0 GOTO Failed
    
------------------------------------------------------------------------------------------------
-- Insert into ContentText
------------------------------------------------------------------------------------------------    
    
INSERT INTO Distribution.ContentText (ContentID, Text) 
VALUES (@ContentID, @Text)

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
