/****** Object:  StoredProcedure [Distribution].[AppraisalValue#Insert]    Script Date: 01/13/2014 19:14:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Distribution].[AppraisalValue#Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Distribution].[AppraisalValue#Insert]
GO


CREATE PROCEDURE [Distribution].[AppraisalValue#Insert]
	@MessageID int,		
    @Value int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.AppraisalValue table.
 * 
 *	Parameters: 
 *  @MessageID
 *  @Value
 *
 * History
 * ----------
 * 
 * CGC  01/13/2014  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO Distribution.AppraisalValue (	
	MessageID,	
    Value
) VALUES (
	@MessageID,	
    @Value
)



GO


