IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Distribution.Submission#Report') AND type in (N'P', N'PC'))
DROP PROCEDURE Distribution.Submission#Report
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE Distribution.Submission#Report
-------------------------------------------------------------------------------------------------------------------
--
--	Get submissions for a given dealer and vehicle, as well as what parts of the submission 
--	passed or failed.
--
---Parameters------------------------------------------------------------------------------------------------------
--
@DealerCode	VARCHAR(20),
@StartDate	DATETIME,
@StockNumber	VARCHAR(15) = NULL
--
---History---------------------------------------------------------------------------------------------------------
--	
--	DMW	06/24/2011	Created
--	
-------------------------------------------------------------------------------------------------------------------
AS
SET NOCOUNT ON 


SELECT  BUP.BusinessUnit AS 'Dealer Group',
        BU.BusinessUnit AS 'Dealer',
        IA.StockNumber AS 'Stock Number',
        P.Name AS 'Provider',
        ST.Name AS 'Status',
        CASE WHEN PR.Value IS NOT NULL THEN PR.Value
             ELSE ''
        END AS 'New Price',
        CASE WHEN CT.Text IS NOT NULL THEN CT.Text
             ELSE ''
        END AS 'New Description',
        CASE WHEN PR.Value IS NULL THEN 'n/a'
             WHEN EXISTS ( SELECT       1
                           FROM         IMT.Distribution.SubmissionSuccess
                           WHERE        SubmissionID = S.SubmissionID
                                        AND (MessageTypes & 1) = 1 )
             THEN 'Yes'
             ELSE 'No'
        END AS 'Price Succeeded',
        CASE WHEN CT.Text IS NULL THEN 'n/a'
             WHEN EXISTS ( SELECT       1
                           FROM         IMT.Distribution.SubmissionSuccess
                           WHERE        SubmissionID = S.SubmissionID
                                        AND (MessageTypes & 2) = 2 )
             THEN 'Yes'
             ELSE 'No'
        END AS 'Description Succeeded',
        CASE WHEN SETY.Description IS NOT NULL THEN SETY.Description
             ELSE 'n/a'
        END AS 'Failure Reason',
        CASE WHEN EXISTS ( SELECT       1
                           FROM         IMT.Distribution.Exception
                           WHERE        SubmissionID = S.SubmissionID )
             THEN 'Yes'
             ELSE 'No'
        END AS 'System Exception',
        S.SubmissionID AS 'Submission ID',
        S.PublicationID AS 'Publication ID',
        S.PushedOnQueue AS 'Pushed On Queue',
        S.PoppedOffQueue AS 'Popped Off Queue',
        S.SubmissionSent AS 'Submission Sent',
        S.ResponseReceived AS 'Response Received'
FROM    IMT.Distribution.Submission S
        INNER JOIN IMT.Distribution.SubmissionStatus ST ON ST.SubmissionStatusID = S.SubmissionStatusID
        INNER JOIN IMT.Distribution.Provider P ON P.ProviderID = S.ProviderID
        INNER JOIN IMT.Distribution.Publication PUB ON PUB.PublicationID = S.PublicationID
        INNER JOIN IMT.Distribution.Message M ON M.MessageID = PUB.MessageID
        INNER JOIN FLDW.dbo.InventoryActive IA ON IA.InventoryID = M.VehicleEntityID
        INNER JOIN IMT.dbo.BusinessUnit BU ON BU.BusinessUnitID = M.DealerID
        LEFT JOIN IMT.Distribution.Price PR ON PR.MessageID = M.MessageID
        LEFT JOIN IMT.Distribution.Advertisement_Content AC ON AC.MessageID = M.MessageID
        LEFT JOIN IMT.Distribution.ContentText CT ON CT.ContentID = AC.ContentID
        LEFT JOIN IMT.dbo.BusinessUnitType BUT ON BUT.BusinessUnitTypeID = BU.BusinessUnitTypeID
        LEFT JOIN IMT.dbo.BusinessUnitRelationship BUR ON BUR.BusinessUnitID = BU.BusinessUnitID
        LEFT JOIN IMT.dbo.BusinessUnit BUP ON BUP.BusinessUnitID = BUR.ParentID
        LEFT JOIN IMT.Distribution.SubmissionError SE ON SE.SubmissionID = S.SubmissionID
        LEFT JOIN IMT.Distribution.SubmissionErrorType SETY ON SETY.ErrorTypeID = SE.ErrorTypeID
WHERE   BU.BusinessUnitCode = @DealerCode
        AND S.PushedOnQueue >= @StartDate
        AND (IA.StockNumber = @StockNumber OR ISNULL(@StockNumber,'ALL') = 'ALL')
ORDER BY IA.StockNumber, S.PushedOnQueue DESC


GO

GRANT EXEC ON Distribution.Submission#Report TO FirstlookReports

/*
EXEC Distribution.Submission#Report
	@DealerCode = 'WINDYCIT01',
	@StartDate = '1/1/11'
*/