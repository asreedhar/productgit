IF OBJECT_ID(N'Distribution.Provider#Fetch') IS NOT NULL
	DROP PROCEDURE Distribution.Provider#Fetch
GO

CREATE PROCEDURE Distribution.Provider#Fetch
	@ProviderID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch providers and the message types they support from the Distribution.Provider table 
 *  that the dealer with the given identifier supports. 
 * 
 *	Parameters:
 *  @ProviderID
 *
 * History
 * ----------
 * 
 * CGC  03/08/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Fetch message data
------------------------------------------------------------------------------------------------

SELECT
	Name    
FROM
	Distribution.Provider
WHERE
	ProviderID = @ProviderID
GO
