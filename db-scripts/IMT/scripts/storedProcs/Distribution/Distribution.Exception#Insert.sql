IF OBJECT_ID(N'Distribution.Exception#Insert') IS NOT NULL
	DROP PROCEDURE Distribution.Exception#Insert
GO

CREATE PROCEDURE Distribution.Exception#Insert
	@MachineName nvarchar(256),
	@Message nvarchar(1024),
	@ExceptionType nvarchar(256),
    @Details ntext,
	@RequestMessage ntext,
    @SubmissionID int,
    @ExceptionUser varchar(50)
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.Exception table.
 * 
 *	Parameters:
 *  @MachineName
 *  @Message
 *  @ExceptionType
 *  @Details
 *  @RequestMessage
 *  @SubmissionID
 *  @ExceptionUser
 *
 * History
 * ----------
 * 
 * CGC  02/22/2010  Create procedure.
 * CGC  07/19/2010  Added id of user running app when exception occurred.
 * CGC  11/08/2010  Added field for request message.
 * CGC  12/05/2010  Changed PublicationID to SubmissionID.
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @ExceptionUserID INT, @err INT

EXEC @err = dbo.ValidateParameter_MemberID#UserName @ExceptionUser, @ExceptionUserID OUTPUT
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO Distribution.Exception (
	ExceptionTime,
    MachineName,
    Message,
    ExceptionType,
    Details,
    SubmissionID,
    ExceptionUserID,
	RequestMessage
) VALUES (
	getdate(),
	@MachineName,
	@Message,
	@ExceptionType,
    @Details,
    @SubmissionID,
    @ExceptionUserID,
	@RequestMessage
)

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
