IF OBJECT_ID(N'Distribution.Publication#ExistsByDealerVehicle') IS NOT NULL
	DROP PROCEDURE Distribution.Publication#ExistsByDealerVehicle
GO

CREATE PROCEDURE Distribution.Publication#ExistsByDealerVehicle
	@DealerID int,
    @VehicleEntityID int,
    @VehicleEntityTypeID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Check if a record exists in Distribution.Publication table for the given dealer and vehicle.
 * 
 *	Parameters:
 *	@DealerID
 *  @ProviderID
 *
 * History
 * ----------
 *  
 * CGC  02/19/2010  Create procedure.
 * CGC  06/21/2010  Changed proc to always return a result, not just when the publication exists.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Perform Fetch
------------------------------------------------------------------------------------------------

SELECT
    CASE WHEN EXISTS
    (
        SELECT 1
        FROM	
            Distribution.Publication P
        JOIN    
            Distribution.Message M ON P.MessageID = M.MessageID       
        WHERE	
            M.DealerID = @DealerID
        AND     
            M.VehicleEntityID = @VehicleEntityID
        AND     
            M.VehicleEntityTypeID = @VehicleEntityTypeID
    )
    THEN 1 ELSE 0 END AS PublicationExists

GO
