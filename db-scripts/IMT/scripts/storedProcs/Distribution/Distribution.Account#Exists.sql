IF OBJECT_ID(N'Distribution.Account#Exists') IS NOT NULL
	DROP PROCEDURE Distribution.Account#Exists
GO

CREATE PROCEDURE Distribution.Account#Exists
	@DealerID int,
    @ProviderID int
AS

/* --------------------------------------------------------------------------------------------- 
 * 
 * Summary
 * ----------
 * 
 *	Check if a record exists in Distribution.Account table for the given dealer and provider.
 
 * 
 *	Parameters:
 *	@DealerID
 *  @ProviderID
 *
 * History
 * ----------
 *  
 * CGC  02/19/2010  Create procedure.
 * CGC  06/02/2010  Changed procedure to read from dbo.InternetAdvertiserDealership instead 
 *                  of from Account. 
 * CGC  06/21/2010  Changed proc to always return a result, not just when the account exists.
 * CGC  11/08/2010  Moving back Distribution.Account tables as we migrate off reprice processor.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Perform Fetch
------------------------------------------------------------------------------------------------

SELECT 
    CASE WHEN EXISTS
    (
		SELECT 1
		FROM 
			Distribution.Account
		WHERE
			DealerID = @DealerID
		AND
			ProviderID = @ProviderID
	)
    THEN 1 ELSE 0 END AS [AccountExists]

GO
