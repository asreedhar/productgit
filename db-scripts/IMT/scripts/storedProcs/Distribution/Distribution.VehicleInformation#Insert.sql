IF OBJECT_ID(N'Distribution.VehicleInformation#Insert') IS NOT NULL
	DROP PROCEDURE Distribution.VehicleInformation#Insert
GO

CREATE PROCEDURE Distribution.VehicleInformation#Insert
	@MessageID int,	
	@Mileage int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.VehicleInformation table.
 * 
 *	Parameters: 
 *  @MessageID,
 *  @Mileage
 *
 * History
 * ----------
 * 
 * CGC  02/22/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO Distribution.VehicleInformation (	
	MessageID,	
	Mileage
) VALUES (
	@MessageID,
	@Mileage
)

GO
