IF OBJECT_ID(N'Distribution.Submission#FetchByDealerVehicle') IS NOT NULL
	DROP PROCEDURE Distribution.Submission#FetchByDealerVehicle
GO

CREATE PROCEDURE Distribution.Submission#FetchByDealerVehicle
	@DealerID int,
    @VehicleEntityID int, 
    @VehicleEntityTypeID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch a record from the Distribution.Submission table.
 * 
 *	Parameters:
 *	@DealerID
 *  @VehicleEntityID
 *  @VehicleEntityTypeID
 *
 * History
 * ----------
 * 
 * CGC	02/22/2010	Create procedure
 * CGC	06/17/2010	Altered proc to return only submissions for most recent publication.
 * CGC	11/04/2010	Returns MessageTypes now.
 * CGC	11/12/2010	Returns attempt.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
        
------------------------------------------------------------------------------------------------
-- Get the ID of the most recent publication
------------------------------------------------------------------------------------------------
        
DECLARE @PublicationID int
SET @PublicationID =                
    (SELECT TOP 1
        P.PublicationID
    FROM
        Distribution.Publication P
    JOIN
        Distribution.Message M ON M.MessageID = P.MessageID
    WHERE
        M.DealerID = @DealerID
    AND
        M.VehicleEntityID = @VehicleEntityID
    AND
        M.VehicleEntityTypeID = @VehicleEntityTypeID
    AND
        P.InsertDate >= DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0)
    ORDER BY
        P.PublicationID DESC)
        
------------------------------------------------------------------------------------------------
-- Fetch submission for this publication
------------------------------------------------------------------------------------------------        
        
SELECT
	SubmissionID,
	PublicationID,
    ProviderID,
    SubmissionStatusID,
	MessageTypes,	
	Attempt
FROM
	Distribution.Submission
WHERE
    PublicationID = @PublicationID    

GO
    