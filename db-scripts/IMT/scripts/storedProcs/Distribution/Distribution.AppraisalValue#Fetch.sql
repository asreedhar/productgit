

/****** Object:  StoredProcedure [Distribution].[AppraisalValue#Fetch]    Script Date: 01/13/2014 19:13:29 ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[Distribution].[AppraisalValue#Fetch]')
                    AND type IN ( N'P', N'PC' ) )
    DROP PROCEDURE [Distribution].[AppraisalValue#Fetch]
GO




CREATE PROCEDURE [Distribution].[AppraisalValue#Fetch] @MessageID INT
AS /* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch a Appraisal value record from the Distribution.AppraisalValue table.
 * 
 *	Parameters:
 *  @MessageID
 *
 * History
 * ----------
 * 
 * CGC  01/13/2014  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Fetch message data
------------------------------------------------------------------------------------------------

    SELECT  Value
    FROM    Distribution.AppraisalValue
    WHERE   MessageID = @MessageID    

GO


