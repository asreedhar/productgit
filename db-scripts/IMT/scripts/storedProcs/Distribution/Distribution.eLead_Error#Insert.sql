USE [IMT]
GO

/****** Object:  StoredProcedure [Distribution].[eLead_Error#Insert]    Script Date: 12/11/2013 16:56:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


IF EXISTS (SELECT * FROM sys.objects o,sys.schemas s WHERE type = 'P' AND o.name = 'eLead_Error#Insert' and s.name='Distribution' and s.schema_id=o.schema_id)
DROP PROCEDURE Distribution.eLead_Error#Insert

GO

CREATE PROCEDURE [Distribution].[eLead_Error#Insert]
    @SubmissionID int,
	@FirstLookErrorTypeID int,
	@eLeadErrorTypeID int,
    @MessageTypes int,
    @ErrorMessage varchar(2000) = NULL
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.eLead_Error table.
 * 
 *	Parameters: 
 *	@SubmissionID
 *	@FirstLookErrorTypeID
 *  @eLeadErrorTypeID 
 *  @MessageTypes
 *  @ErrorMessage
 *
 * History
 * ----------
 * 
 * CGC	12/11/2013	Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @ErrorID int

------------------------------------------------------------------------------------------------
-- Perform Insert on SubmissionError
------------------------------------------------------------------------------------------------
EXEC Distribution.SubmissionError#Insert @SubmissionID, @FirstLookErrorTypeID, @MessageTypes, @ErrorID OUTPUT

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO Distribution.eLead_Error (
	ErrorID,
	ErrorTypeID,
    ErrorMessage
) VALUES (
	@ErrorID,
	@eLeadErrorTypeID,
    @ErrorMessage
)


GO


