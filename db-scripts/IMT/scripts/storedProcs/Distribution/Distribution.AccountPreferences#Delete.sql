IF OBJECT_ID(N'Distribution.AccountPreferences#Delete') IS NOT NULL
	DROP PROCEDURE Distribution.AccountPreferences#Delete
GO

CREATE PROCEDURE Distribution.AccountPreferences#Delete
	@DealerID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Delete all records from the Distribution.AccountPreferences table that belong to the given 
 *  dealer.
 * 
 *	Parameters:
 *	@DealerID 
 *
 * History
 * ----------
 * 
 * CGC	03/08/2010	Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Perform Delete
------------------------------------------------------------------------------------------------

DELETE FROM Distribution.AccountPreferences
WHERE
	DealerID = @DealerID

GO
