IF OBJECT_ID(N'Distribution.Publication#FetchByDealerVehicle') IS NOT NULL
	DROP PROCEDURE Distribution.Publication#FetchByDealerVehicle
GO

CREATE PROCEDURE Distribution.Publication#FetchByDealerVehicle
	@DealerID int,
    @VehicleEntityID int,
    @VehicleEntityTypeID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch a record from the Distribution.Publication table by its dealer and vehicle ids.
 * 
 *	Parameters:
 *  @DealerID
 *  @VehicleEntityID
 *  @VehicleEntityTypeID
 *
 * History
 * ----------
 *  
 * CGC  02/22/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @MessageID int

------------------------------------------------------------------------------------------------
-- Fetch publication data
------------------------------------------------------------------------------------------------

SELECT
	P.PublicationID,
	P.MessageID,
	P.PublicationStatusID	
FROM
	Distribution.Publication P
JOIN
    Distribution.Message M ON P.MessageID = M.MessageID
WHERE
	M.DealerID = @DealerID
AND
    M.VehicleEntityID = @VehicleEntityID
AND    
    M.VehicleEntityTypeID = @VehicleEntityTypeID
    
------------------------------------------------------------------------------------------------
-- Get the message id.
------------------------------------------------------------------------------------------------       
    
SELECT
	@MessageID = MessageID 
FROM
    Distribution.Message
WHERE
	DealerID = @DealerID
AND
    VehicleEntityID = @VehicleEntityID
AND    
    VehicleEntityTypeID = @VehicleEntityTypeID
    
------------------------------------------------------------------------------------------------
-- Fetch message data
------------------------------------------------------------------------------------------------    
    
EXEC Distribution.Message#Fetch @MessageID

GO
