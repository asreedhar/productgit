IF OBJECT_ID(N'Distribution.Exception#Fetch') IS NOT NULL
	DROP PROCEDURE Distribution.Exception#Fetch
GO

CREATE PROCEDURE Distribution.Exception#Fetch	
    @SubmissionID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch a record from the Distribution.Exception table.
 * 
 *	Parameters: 
 *  @SubmissionID
 *
 * History
 * ----------
 * 
 * CGC  06/14/2010  Create procedure.
 * CGC  07/19/2010  Added id of user running app when exception occurred.
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Perform Fetch
------------------------------------------------------------------------------------------------

SELECT TOP 1
    ExceptionID,
    ExceptionTime,
    MachineName,
    Message,
    ExceptionType,
    Details,
    ExceptionUserID
FROM 
    Distribution.Exception
WHERE
    SubmissionID = @SubmissionID
ORDER BY
    ExceptionTime DESC

GO
