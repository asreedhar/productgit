IF OBJECT_ID(N'Distribution.AccountPreferences#Fetch') IS NOT NULL
	DROP PROCEDURE Distribution.AccountPreferences#Fetch
GO

CREATE PROCEDURE Distribution.AccountPreferences#Fetch
	@DealerID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch dealer preferences for providers and the message types they support from the 
 *  Distribution.ProviderMessageType table.
 * 
 *	Parameters:
 *  @DealerID
 *
 * History
 * ----------
 * 
 * CGC  03/08/2010  Create procedure.
 * CGC  07/19/2010  Changing name of MessageTypeFlag since it's not a flag.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Fetch provider preferences
------------------------------------------------------------------------------------------------

SELECT
	PMT.ProviderID,
    P.Name AS ProviderName,
    PMT.MessageTypes,    
    CASE 
        WHEN EXISTS
            (
                SELECT * 
                FROM Distribution.AccountPreferences AP 
                WHERE AP.ProviderID = PMT.ProviderID
                AND AP.MessageTypes = PMT.MessageTypes
                AND AP.DealerID = @DealerID
            )
    THEN 1 ELSE 0
	END AS Enabled
FROM
	Distribution.ProviderMessageType PMT
JOIN
    Distribution.Provider P ON P.ProviderID = PMT.ProviderID
GO
