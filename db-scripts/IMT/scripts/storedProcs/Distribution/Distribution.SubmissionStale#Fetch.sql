IF OBJECT_ID(N'Distribution.SubmissionStale#Fetch') IS NOT NULL
	DROP PROCEDURE Distribution.SubmissionStale#Fetch
GO

CREATE PROCEDURE Distribution.SubmissionStale#Fetch
	@SubmissionID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch records from the Distribution.SubmissionStale table.
 * 
 *	Parameters:
 *	@SubmissionID
 *
 * History
 * ----------
 * 
 * CGC	12/15/2010	Create procedure. 
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
        
------------------------------------------------------------------------------------------------
-- Fetch submission data
------------------------------------------------------------------------------------------------
        
SELECT
	MessageTypes
FROM
	Distribution.SubmissionStale
WHERE
	SubmissionID = @SubmissionID
    
GO
    