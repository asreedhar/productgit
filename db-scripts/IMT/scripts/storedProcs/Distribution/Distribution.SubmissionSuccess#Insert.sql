IF OBJECT_ID(N'Distribution.SubmissionSuccess#Insert') IS NOT NULL
	DROP PROCEDURE Distribution.SubmissionSuccess#Insert
GO

CREATE PROCEDURE Distribution.SubmissionSuccess#Insert
    @SubmissionID int,
	@MessageTypes int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.SubmissionSuccess table.
 * 
 *	Parameters:
 *	@SubmissionID
 *	@MessageTypes 
 *
 * History
 * ----------
 * 
 * CGC	03/19/2010	Create procedure.
 * CGC  07/19/2010  Changing name of MessageTypeFlag since it's not a flag.
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO Distribution.SubmissionSuccess (
	SubmissionID,
	MessageTypes,
	OccuredOn
) VALUES (
	@SubmissionID,
	@MessageTypes,
    getdate()
)

GO
