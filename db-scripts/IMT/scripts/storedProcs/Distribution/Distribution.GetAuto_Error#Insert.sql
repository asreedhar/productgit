IF OBJECT_ID(N'Distribution.GetAuto_Error#Insert') IS NOT NULL
	DROP PROCEDURE Distribution.GetAuto_Error#Insert
GO

CREATE PROCEDURE Distribution.GetAuto_Error#Insert
    @SubmissionID int,
	@FirstLookErrorTypeID int,
	@GetAutoErrorTypeID int,    
    @MessageTypes int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.GetAuto_Error table.
 * 
 *	Parameters: 
 *	@SubmissionID
 *	@FirstLookErrorTypeID
 *  @GetAutoErrorTypeID 
 *  @MessageTypes
 *
 * History
 * ----------
 * 
 * CGC	12/01/2010	Create procedure. 
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @ErrorID int

------------------------------------------------------------------------------------------------
-- Perform Insert on SubmissionError
------------------------------------------------------------------------------------------------
EXEC Distribution.SubmissionError#Insert @SubmissionID, @FirstLookErrorTypeID, @MessageTypes, @ErrorID OUTPUT

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO Distribution.GetAuto_Error (
	ErrorID,
	ErrorTypeID
) VALUES (
	@ErrorID,
	@GetAutoErrorTypeID
)

GO
