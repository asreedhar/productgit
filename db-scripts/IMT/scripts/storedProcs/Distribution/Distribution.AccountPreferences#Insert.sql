IF OBJECT_ID(N'Distribution.AccountPreferences#Insert') IS NOT NULL
	DROP PROCEDURE Distribution.AccountPreferences#Insert
GO

CREATE PROCEDURE Distribution.AccountPreferences#Insert
	@DealerID int,
	@ProviderID int,
	@MessageTypes int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.AccountPreferences table.
 * 
 *	Parameters:
 *	@DealerID  
 *	@ProviderID
 *  @MessageTypes
 *
 * History
 * ----------
 * 
 * CGC	03/10/2010	Create procedure.
 * CGC  07/19/2010  Changing name of MessageTypeFlag since it's not a flag.
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO Distribution.AccountPreferences (
	DealerID,
	ProviderID,
	MessageTypes
) VALUES (
	@DealerID,
	@ProviderID,
	@MessageTypes
)

GO
