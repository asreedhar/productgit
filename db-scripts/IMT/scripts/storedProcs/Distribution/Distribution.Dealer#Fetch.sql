IF EXISTS ( SELECT  *
            FROM    sys.objects o
                  , sys.schemas s
            WHERE   type = 'P'
                    AND o.name = 'Dealer#Fetch'
                    AND s.name = 'Distribution'
                    AND s.schema_id = o.schema_id )
    DROP PROCEDURE Distribution.Dealer#Fetch

GO

CREATE PROCEDURE [Distribution].[Dealer#Fetch] @DealerID INT
AS /* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch dealer(BusinessUnit) from IMT.dbo.BusinessUnit table.
 * 
 *	Parameters:
 *	@DealerID
 *
 * History
 * ----------
 *  
 * CGC  12/17/2013  Create procedure. 
 * 						
 * ------------------------------------------------------------------------------------------ */

    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Perform Fetch
------------------------------------------------------------------------------------------------

    SELECT  BusinessUnit
          , BusinessUnitShortName
          , BusinessUnitCode
          , Active
    FROM    dbo.BusinessUnit
    WHERE   BusinessUnitID = @DealerID    

GO


