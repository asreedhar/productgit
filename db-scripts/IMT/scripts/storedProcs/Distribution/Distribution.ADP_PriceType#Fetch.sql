USE [IMT]
GO

/****** Object:  StoredProcedure [Distribution].[ADP_PriceType#Fetch]    Script Date: 03/19/2014 16:25:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Distribution].[ADP_PriceType#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Distribution].[ADP_PriceType#Fetch]
GO

USE [IMT]
GO

/****** Object:  StoredProcedure [Distribution].[ADP_PriceType#Fetch]    Script Date: 03/19/2014 16:25:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: 01/21/2010
-- Description:	Returns Price Types of ADP
-- =============================================
CREATE PROCEDURE [Distribution].[ADP_PriceType#Fetch]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
		
	SELECT CONVERT(varchar,PriceTypeID)+'#'+CONVERT(varchar,FT.FieldId) TargetId, PT.[Description] Description,FT.FieldId FielID from 
	Distribution.ADP_PriceType PT, Distribution.ADP_FieldTypes FT Where FT.FieldId=1 
	Union 
	SELECT CONVERT(varchar,DDF.DealerDefinedFieldId)+'#'+CONVERT(varchar,FT.FieldId) TargetId, DDF.[Description] Description,FT.FieldId FielID from 
	Distribution.ADP_DealerDefinedFields DDF, Distribution.ADP_FieldTypes FT Where FT.FieldId=2
	Order by FielID
	
END



GO


