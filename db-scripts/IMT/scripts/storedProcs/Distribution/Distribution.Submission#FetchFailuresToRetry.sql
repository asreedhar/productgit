USE [IMT]
GO

/****** Object:  StoredProcedure [Distribution].[Submission#FetchFailuresToRetry]    Script Date: 01/03/2014 20:14:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Distribution].[Submission#FetchFailuresToRetry]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Distribution].[Submission#FetchFailuresToRetry]
GO

USE [IMT]
GO

/****** Object:  StoredProcedure [Distribution].[Submission#FetchFailuresToRetry]    Script Date: 01/03/2014 20:14:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [Distribution].[Submission#FetchFailuresToRetry]
	@MaxAttempts INT = NULL,
	@MaxDaysBack INT = NULL
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch the failed submissions that should be redistributed. Only submissions that meet the
 *  following criteria will be returned:
 *		- Submission's status is 5 (failure);
 *		- Are the most recent attempt for a publication & provider;
 *		- Are under the maximum number of attempts (@MaxAttempts);
 *		- Are no older than the @MaxDaysBack. 
 *
 * History
 * ----------
 * 
 * CGC  11/15/2010  Created procedure.
 * CGC  05/02/2010  Set default MaxDaysBack to 1 instead of all time in case this proc is somehow
 * invoked without passing in the parameter; don't want millions of submissions going back out.
 * CGC  01/03/2014  Updated to exclude eLeads error for which status is not 'Internal Server Error'.
 * ErrorTypeID=5             
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Ensure parameters are set.
------------------------------------------------------------------------------------------------

IF @MaxAttempts IS NULL OR @MaxAttempts = 0
BEGIN
	SET @MaxAttempts = 5
END	

IF @MaxDaysBack IS NULL OR @MaxDaysBack = 0
BEGIN
	SET @MaxDaysBack = 1
END	

------------------------------------------------------------------------------------------------
-- Fetch failures.
------------------------------------------------------------------------------------------------

--Fetch Submissions for which error type is not 'Internal Server Error' (ErrorTypeID=5)
;With eLeadErrors as(
 select SubmissionID from Distribution.SubmissionError s inner join Distribution.eLead_error e on e.ErrorID=s.ErrorID  where e.ErrorTypeID<>5)
 
SELECT 
	s1.SubmissionID,
	s1.PublicationID,	
	s1.ProviderID,
	s1.SubmissionStatusID,
	s1.Attempt,
	s1.MessageTypes	
FROM
	Distribution.Submission s1
WHERE
	s1.SubmissionStatusID = 5
AND	
	s1.Attempt < @MaxAttempts
AND
	s1.PushedOnQueue >= DATEADD(DD, -@MaxDaysBack, GetDate())
AND
	s1.Attempt = 
	(
		SELECT 
			MAX(s2.Attempt)
		FROM 
			Distribution.Submission s2
		WHERE 
			s2.PublicationID = s1.PublicationID
		AND
			s2.ProviderID = s1.ProviderID
	)
	 
-- Exclude submissions which are of eLead and Where Error is not 'Internal Server Error'
  AND SubmissionID NOT IN (select SubmissionID from eLeadErrors)

GO


