IF OBJECT_ID(N'Distribution.SubmissionError#Insert') IS NOT NULL
	DROP PROCEDURE Distribution.SubmissionError#Insert
GO

CREATE PROCEDURE Distribution.SubmissionError#Insert
    @SubmissionID int,
	@ErrorTypeID int,
    @MessageTypes int,
    @ErrorID int OUTPUT
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.SubmissionError table.
 * 
 *	Parameters:
 *	@SubmissionID
 *	@ErrorTypeID
 *  @MessageTypes
 *  @ErrorID (output)
 *
 * History
 * ----------
 * 
 * CGC	02/22/2010	Create procedure.
 * CGC	06/17/2010	Added message type.
 * CGC  07/19/2010  Changing name of MessageTypeFlag since it's not a flag.
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO Distribution.SubmissionError (
	SubmissionID,
	ErrorTypeID,
	OccuredOn,
    MessageTypes
) VALUES (
	@SubmissionID,
	@ErrorTypeID,
    getdate(),
    @MessageTypes
)

SET @ErrorID = SCOPE_IDENTITY()

GO
