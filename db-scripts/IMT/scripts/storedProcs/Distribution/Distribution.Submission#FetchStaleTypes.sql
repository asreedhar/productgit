IF OBJECT_ID(N'Distribution.Submission#FetchStaleTypes') IS NOT NULL
	DROP PROCEDURE Distribution.Submission#FetchStaleTypes
GO

CREATE PROCEDURE Distribution.Submission#FetchStaleTypes
    @SubmissionID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *  Look to see which message types are stale for a submission. If the submission is a new attempt
 *  to resend an old publication, the starting date of the stale check is from the first attempt.
 *  The return value is which message types are stale, if any.
 * 
 *	Parameters:
 *	@SubmissionID 
 *
 * History
 * ----------
 * 
 * CGC  12/15/2010  Create procedure. 
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE 
	@DealerID int,
	@VehicleEntityID int,
	@VehicleEntityTypeID int,
	@MessageTypes int, 
	@PushedOnQueue datetime,
	@Attempt int,
	@PublicationID int,
	@ProviderID int, 
	@StartingDate datetime
	
------------------------------------------------------------------------------------------------
-- Get values on potentially stale submission
------------------------------------------------------------------------------------------------

SELECT 
	@DealerID            = m.DealerID,
	@VehicleEntityID     = m.VehicleEntityID,
	@VehicleEntityTypeID = m.VehicleEntityTypeID,
	@MessageTypes        = s.MessageTypes,
	@PushedOnQueue       = s.PushedOnQueue,
	@Attempt             = s.Attempt,
	@PublicationID       = s.PublicationID,
	@ProviderID          = s.ProviderID	
FROM 
	Distribution.Submission s
JOIN
	Distribution.Publication p ON s.PublicationID = p.PublicationID
JOIN
	Distribution.Message m ON m.MessageID = p.MessageID	
WHERE
	s.SubmissionID = @SubmissionID	
	
------------------------------------------------------------------------------------------------
-- Get the date to start from. This is the date when the first attempt of submitting for the 
-- same provider and publication was put on the queue.
------------------------------------------------------------------------------------------------	
	
IF @Attempt > 1
BEGIN
	SELECT
		@StartingDate = PushedOnQueue 
	FROM
		Distribution.Submission
	WHERE 
		PublicationID = @PublicationID 
	AND 
		ProviderID = @ProviderID 
	AND 
		Attempt = 1
END	
ELSE
	SET @StartingDate = @PushedOnQueue
			
------------------------------------------------------------------------------------------------
-- Get the list of message types newer than @StartingDate that are:
--
--  a) for the same dealer;
--  b) for the same vehicle;
--  c) for the same provider;
--  d) from a different publication;
--  e) are first attempts.
--
-- The reason attempt plays into it is so that a retry of an earlier submission does not make
-- stale a newer submission.
------------------------------------------------------------------------------------------------				

DECLARE @NewMessageTypes TABLE (
	MessageTypes int not null
)

INSERT INTO @NewMessageTypes
SELECT DISTINCT
	s.MessageTypes
FROM
	Distribution.Submission s
JOIN		
	Distribution.Publication p ON p.PublicationID = s.PublicationID
JOIN
	Distribution.Message m ON m.MessageID = p.MessageID
WHERE
	s.PushedOnQueue > @StartingDate
AND
	m.DealerID = @DealerID
AND
	m.VehicleEntityID = @VehicleEntityID
AND
	m.VehicleEntityTypeID = @VehicleEntityTypeID
AND
	s.ProviderID = @ProviderID
AND
	s.PublicationID <> @PublicationID
AND
	s.MessageTypes IS NOT NULL		
AND
	s.Attempt = 1
			
------------------------------------------------------------------------------------------------
-- Determine the stale types.
------------------------------------------------------------------------------------------------	
	
DECLARE @StalePrice int
DECLARE @StaleDescription int
DECLARE @StaleMileage int
	
SELECT
	@StalePrice = 
	CASE WHEN EXISTS 
	(
		SELECT 1 FROM @NewMessageTypes		
		WHERE (MessageTypes & 1) = 1		
	) THEN 1 ELSE 0 END,
	
	@StaleDescription = 
	CASE WHEN EXISTS 
	(
		SELECT 1 FROM @NewMessageTypes		
		WHERE (MessageTypes & 2) = 2
	) THEN 2 ELSE 0 END,
	
	@StaleMileage = 
	CASE WHEN EXISTS 
	(
		SELECT 1 FROM @NewMessageTypes		
		WHERE (MessageTypes & 4) = 4
	) THEN 4 ELSE 0 END
	
------------------------------------------------------------------------------------------------
-- What we return are the stale types that are applicable to the message types of the submission.
------------------------------------------------------------------------------------------------		

SELECT ((@StalePrice | @StaleDescription | @StaleMileage) & @MessageTypes) AS StaleTypes
	
GO
