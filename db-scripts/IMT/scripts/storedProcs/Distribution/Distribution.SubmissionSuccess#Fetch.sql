IF OBJECT_ID(N'Distribution.SubmissionSuccess#Fetch') IS NOT NULL
	DROP PROCEDURE Distribution.SubmissionSuccess#Fetch
GO

CREATE PROCEDURE Distribution.SubmissionSuccess#Fetch
	@SubmissionID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch records from the Distribution.SubmissionSuccess table.
 * 
 *	Parameters:
 *	@SubmissionID
 *
 * History
 * ----------
 * 
 * CGC	03/19/2010	Create procedure.
 * CGC  07/19/2010  Changing name of MessageTypeFlag since it's not a flag.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
        
------------------------------------------------------------------------------------------------
-- Fetch submission data
------------------------------------------------------------------------------------------------
        
SELECT
	MessageTypes   
FROM
	Distribution.SubmissionSuccess
WHERE
	SubmissionID = @SubmissionID
    
GO
    