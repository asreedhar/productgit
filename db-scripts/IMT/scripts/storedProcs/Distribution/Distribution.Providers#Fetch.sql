IF OBJECT_ID(N'Distribution.Providers#Fetch') IS NOT NULL
	DROP PROCEDURE Distribution.Providers#Fetch
GO

CREATE PROCEDURE Distribution.Providers#Fetch	
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch all providers from the Distribution.Provider table. 
 *
 * History
 * ----------
 * 
 * CGC  03/08/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Fetch message data
------------------------------------------------------------------------------------------------

SELECT
    ProviderID,
    Name
FROM
	Distribution.Provider
GO
