USE [IMT]
GO

/****** Object:  StoredProcedure [Distribution].[Inventory#Fetch]    Script Date: 12/12/2013 19:26:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects o,sys.schemas s WHERE type = 'P' AND o.name = 'Vehicle#Fetch' and s.name='Distribution' and s.schema_id=o.schema_id)
DROP PROCEDURE Distribution.Vehicle#Fetch

GO

CREATE PROCEDURE [Distribution].[Vehicle#Fetch]
	@AppraisalID int	
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch the VIN for a vehicle from the dbo.Vehicle table.
 * 
 *	Parameters:
 *	@AppraisalID
 *	
 *
 * History
 * ----------
 * 
 * CGC	12/18/2013	Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

------------------------------------------------------------------------------------------------
-- Perform Fetch
------------------------------------------------------------------------------------------------

SELECT 
    V.VIN,
	V.VehicleYear	
FROM
    dbo.Appraisals A
JOIN 
    dbo.Vehicle V ON V.VehicleID = A.VehicleID 
WHERE
    A.AppraisalID = @AppraisalID

    


GO


