IF OBJECT_ID(N'Distribution.AutoTrader_Error#Insert') IS NOT NULL
	DROP PROCEDURE Distribution.AutoTrader_Error#Insert
GO

CREATE PROCEDURE Distribution.AutoTrader_Error#Insert
    @SubmissionID int,
	@FirstLookErrorTypeID int,
	@AutoTraderErrorTypeID int,
    @MessageTypes int,
    @ErrorMessage varchar(2000) = NULL
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.AutoTrader_Error table.
 * 
 *	Parameters: 
 *	@SubmissionID
 *	@FirstLookErrorTypeID
 *  @AutoTraderErrorTypeID 
 *  @MessageTypes
 *  @ErrorMessage
 *
 * History
 * ----------
 * 
 * CGC	02/22/2010	Create procedure.
 * CGC	06/17/2010	Added message type.
 * CGC  07/19/2010  Changing name of MessageTypeFlag since it's not a flag.
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @ErrorID int

------------------------------------------------------------------------------------------------
-- Perform Insert on SubmissionError
------------------------------------------------------------------------------------------------
EXEC Distribution.SubmissionError#Insert @SubmissionID, @FirstLookErrorTypeID, @MessageTypes, @ErrorID OUTPUT

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO Distribution.AutoTrader_Error (
	ErrorID,
	ErrorTypeID,
    ErrorMessage
) VALUES (
	@ErrorID,
	@AutoTraderErrorTypeID,
    @ErrorMessage
)

GO
