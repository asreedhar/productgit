IF OBJECT_ID(N'Distribution.Dealer#Exists') IS NOT NULL
	DROP PROCEDURE Distribution.Dealer#Exists
GO

CREATE PROCEDURE Distribution.Dealer#Exists
	@DealerID int    
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Check if a record exists in the dbo.BusinessUnit table for the given dealer.
 * 
 *	Parameters:
 *	@DealerID
 *
 * History
 * ----------
 *  
 * CGC  03/05/2010  Create procedure.
 * CGC  06/21/2010  Changed proc to always return a result, not just when the dealer exists.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Perform Fetch
------------------------------------------------------------------------------------------------

SELECT 
    CASE WHEN EXISTS
    (
        SELECT 1
        FROM 
            dbo.BusinessUnit
        WHERE
            BusinessUnitID = @DealerID
    )
    THEN 1 ELSE 0 END AS DealerExists

GO
