IF OBJECT_ID(N'Distribution.Submission#Fetch') IS NOT NULL
	DROP PROCEDURE Distribution.Submission#Fetch
GO

CREATE PROCEDURE Distribution.Submission#Fetch
	@PublicationID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch a record from the Distribution.Submission table.
 * 
 *	Parameters:
 *	@PublicationID
 *
 * History
 * ----------
 * 
 * CGC	02/22/2010	Create procedure.
 * CGC	11/04/2010	Returns MessageTypes now.
 * CGC	11/12/2010	Returns attempt.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
        
------------------------------------------------------------------------------------------------
-- Fetch submission data
------------------------------------------------------------------------------------------------
        
SELECT
	SubmissionID,
	PublicationID,
    ProviderID,
    SubmissionStatusID,
	MessageTypes,	
	Attempt
FROM
	Distribution.Submission
WHERE
    PublicationID = @PublicationID
ORDER BY    
    SubmissionID DESC

GO
    