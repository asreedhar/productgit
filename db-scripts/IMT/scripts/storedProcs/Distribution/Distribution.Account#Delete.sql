IF OBJECT_ID(N'Distribution.Account#Delete') IS NOT NULL
	DROP PROCEDURE Distribution.Account#Delete
GO

CREATE PROCEDURE Distribution.Account#Delete
	@DealerID int,
    @ProviderID int,
	@DeleteUser varchar(50)
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Delete a record from the Distribution.Account table. Triggers ensure an entry will be 
 *  inserted into Account_Audit for this operation.
 *
 *  An update is done first so that the trigger can properly record the user that is deleting
 *  the account. To prevent the update trigger from executing, it is temporarily disabled. 
 * 
 *	Parameters:
 *	@DealerID
 *  @ProviderID
 *  @DeleteUser
 *
 * History
 * ----------
 * 
 * CGC  01/22/2010	Create procedure with help from CodeSmith 5.0.0.0 
 * CGC  06/02/2010  Disabled procedure until we no longer use dbo.InternetAdvertiserDealership 
 *                  instead of Account.  
 * CGC  11/08/2010  Turning procedure back on as we migrate off reprice processor.
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @DeleteUserID INT, @err INT

EXEC @err = dbo.ValidateParameter_MemberID#UserName @DeleteUser, @DeleteUserID OUTPUT
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Temporarily Disable The Update Trigger
------------------------------------------------------------------------------------------------

ALTER TABLE Distribution.Account
DISABLE TRIGGER TR_U_Account

------------------------------------------------------------------------------------------------
-- Perform Update
------------------------------------------------------------------------------------------------

UPDATE Distribution.Account
SET
	UpdateUserID = @DeleteUserID,
	UpdateDate = getdate()
WHERE
	DealerID = @DealerID
AND
    ProviderID = @ProviderID
	
------------------------------------------------------------------------------------------------
-- Reenable The Update Trigger
------------------------------------------------------------------------------------------------

ALTER TABLE Distribution.Account
ENABLE TRIGGER TR_U_Account

------------------------------------------------------------------------------------------------
-- Perform Delete
------------------------------------------------------------------------------------------------

DELETE FROM Distribution.Account
WHERE
	DealerID = @DealerID
AND
    ProviderID = @ProviderID

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
