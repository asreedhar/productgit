IF OBJECT_ID(N'Distribution.AutoUplink_Error#Insert') IS NOT NULL
	DROP PROCEDURE Distribution.AutoUplink_Error#Insert
GO

CREATE PROCEDURE Distribution.AutoUplink_Error#Insert
    @SubmissionID int,
	@FirstLookErrorTypeID int,
	@AutoUplinkErrorTypeID int,
    @AutoUplinkErrorCode varchar(50),
    @MessageTypes int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.AutoUplink_Error table.
 * 
 *	Parameters: 
 *	@SubmissionID
 *	@FirstLookErrorTypeID
 *  @AutoUplinkErrorTypeID
 *  @AutoUplinkErrorCode
 *  @MessageTypes
 *
 * History
 * ----------
 * 
 * CGC	02/22/2010	Create procedure.
 * CGC	06/17/2010	Added message type.
 * CGC  07/19/2010  Changing name of MessageTypeFlag since it's not a flag.
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @ErrorID int

------------------------------------------------------------------------------------------------
-- Perform Insert on SubmissionError
------------------------------------------------------------------------------------------------
EXEC Distribution.SubmissionError#Insert @SubmissionID, @FirstLookErrorTypeID, @MessageTypes, @ErrorID OUTPUT

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO Distribution.AutoUplink_Error (
	ErrorID,
	ErrorTypeID,
    ErrorCode
) VALUES (
	@ErrorID,
	@AutoUplinkErrorTypeID,
    @AutoUplinkErrorCode
)

GO
