IF OBJECT_ID(N'Distribution.AccountPreference#Delete') IS NOT NULL
	DROP PROCEDURE Distribution.AccountPreference#Delete
GO

CREATE PROCEDURE Distribution.AccountPreference#Delete
	@DealerID int,
    @ProviderID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Delete all records from the Distribution.AccountPreferences table that belong to the given
 *  dealer and provider.
 * 
 *	Parameters:
 *	@DealerID 
 *
 * History
 * ----------
 * 
 * CGC	03/08/2010	Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Perform Delete
------------------------------------------------------------------------------------------------

DELETE FROM Distribution.AccountPreferences
WHERE
	DealerID = @DealerID
AND
    ProviderID = @ProviderID

GO
