IF OBJECT_ID(N'Distribution.VehicleInformation#Fetch') IS NOT NULL
	DROP PROCEDURE Distribution.VehicleInformation#Fetch
GO

CREATE PROCEDURE Distribution.VehicleInformation#Fetch
	@MessageID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch a vehicle information record from the Distribution.VehicleInformation table.
 * 
 *	Parameters:
 *  @MessageID
 *
 * History
 * ----------
 * 
 * CGC  02/22/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Fetch message data
------------------------------------------------------------------------------------------------

SELECT
	Mileage
FROM
	Distribution.VehicleInformation
WHERE
	MessageID = @MessageID
GO
