USE [IMT]
GO

/****** Object:  StoredProcedure [Distribution].[ADP_DealerMapping#Insert]    Script Date: 03/19/2014 16:25:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Distribution].[ADP_DealerMapping#Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Distribution].[ADP_DealerMapping#Insert]
GO

USE [IMT]
GO

/****** Object:  StoredProcedure [Distribution].[ADP_DealerMapping#Insert]    Script Date: 03/19/2014 16:25:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	Insert DealerMapping if not exists
-- =============================================
CREATE PROCEDURE [Distribution].[ADP_DealerMapping#Insert] 
	@DealerId int,
	@ADPTargetId varchar(100) -- ADPTargetId will be Concatenation of TargetId and Target type Id separated by #
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE
	@TargetId int,
	@TargetTypeId int
	
	select @TargetId = CONVERT(INT,substring(@ADPTargetId,1,CHARINDEX('#',@ADPTargetId)-1)),@TargetTypeId = CONVERT(INT,substring(@ADPTargetId,CHARINDEX('#',@ADPTargetId)+1,LEN(@ADPTargetId)));
		
	IF Not Exists (select 1 from Distribution.ADP_DealerPriceMapping where DealerID=@DealerId)
	BEGIN
	
	Insert into Distribution.ADP_DealerPriceMapping(DealerID,PriceTypeID,FieldTypeId) values
	(@DealerId,@TargetId,@TargetTypeId)
    END
    ELSE
    BEGIN
		 Update Distribution.ADP_DealerPriceMapping set PriceTypeID=@TargetId, FieldTypeId=@TargetTypeId where DealerID=@DealerId
    END
END

GO


