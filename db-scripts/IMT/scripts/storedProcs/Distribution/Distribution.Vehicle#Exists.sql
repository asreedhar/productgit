IF OBJECT_ID(N'Distribution.Vehicle#Exists') IS NOT NULL
	DROP PROCEDURE Distribution.Vehicle#Exists
GO

CREATE PROCEDURE Distribution.Vehicle#Exists
	@VehicleEntityID int,
    @VehicleEntityTypeID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Check if a record exists in Distribution.Vehicle table for the given entity and entity 
 *  type ids.
 * 
 *	Parameters:
 *	@VehicleEntityID
 *  @VehicleEntityTypeID
 *
 * History
 * ----------
 * 
 * CGC  02/19/2010  Create procedure.
 * CGC  06/21/2010  Changed proc to always return a result, not just when the vehicle exists.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Perform Fetch
------------------------------------------------------------------------------------------------

SELECT 
    CASE WHEN EXISTS
    (
        SELECT 1
        FROM	
            Distribution.Vehicle
        WHERE	
            VehicleEntityID = @VehicleEntityID
        AND     
            VehicleEntityTypeID = @VehicleEntityTypeID
    )
    THEN 1 ELSE 0 END AS VehicleExists

GO
