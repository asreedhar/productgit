IF OBJECT_ID(N'Distribution.Publication#Exists') IS NOT NULL
	DROP PROCEDURE Distribution.Publication#Exists
GO

CREATE PROCEDURE Distribution.Publication#Exists
	@PublicationID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Check if a record exists in Distribution.Publication table for the given identifier.
 * 
 *	Parameters:
 *	@DealerID
 *  @ProviderID
 *
 * History
 * ----------
 *  
 * CGC  02/19/2010  Create procedure.
 * CGC  06/21/2010  Changed proc to always return a result, not just when the publication exists.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Perform Fetch
------------------------------------------------------------------------------------------------

SELECT
    CASE WHEN EXISTS
    (
        SELECT 1
        FROM
            Distribution.Publication
        WHERE
            PublicationID = @PublicationID
    )
    THEN 1 ELSE 0 END AS PublicationExists

GO
