IF OBJECT_ID(N'Distribution.Message#Insert') IS NOT NULL
	DROP PROCEDURE Distribution.Message#Insert
GO

CREATE PROCEDURE Distribution.Message#Insert
	@DealerID int,
    @VehicleEntityID int, 
    @VehicleEntityTypeID int,
    @InsertUser varchar(50),
    @MessageTypes int,
    @MessageID int OUTPUT
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.Message table.
 * 
 *	Parameters:
 *	@DealerID
 *  @VehicleEntityID
 *  @VehicleEntityTypeID
 *  @InsertUser
 *	@MessageID (output)
 *
 * History
 * ----------
 * 
 * CGC  01/22/2010  Create procedure using CodeSmith 5.0.0.0
 * CGC  02/22/2010  Changes to fit schema redesign.
 * CGC  07/19/2010  Addition of message types.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @InsertUserID INT, @err INT

EXEC @err = dbo.ValidateParameter_MemberID#UserName @InsertUser, @InsertUserID OUTPUT
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO Distribution.Message (
	DealerID,
    VehicleEntityID,
    VehicleEntityTypeID,
    InsertUserID,
    InsertDate,
    MessageTypes
) VALUES (
	@DealerID,
    @VehicleEntityID,
    @VehicleEntityTypeID,
    @InsertUserID,
    getdate(),
    @MessageTypes
)

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SET @MessageID = SCOPE_IDENTITY()

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO

