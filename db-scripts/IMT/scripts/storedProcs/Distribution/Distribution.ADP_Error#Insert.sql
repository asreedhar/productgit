/****** Object:  StoredProcedure [Distribution].[ADP_Error#Insert]    Script Date: 03/13/2015 20:09:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Distribution].[ADP_Error#Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Distribution].[ADP_Error#Insert]
GO

/****** Object:  StoredProcedure [Distribution].[ADP_Error#Insert]    Script Date: 03/13/2015 20:09:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [Distribution].[ADP_Error#Insert]
    @SubmissionID int,
	@FirstLookErrorTypeID int,
	@ADPErrorTypeID int,
    @MessageTypes int,
    @ErrorMessage nvarchar(MAX) = NULL,
    @Request nvarchar(MAX) = NULL
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.ADP_Error table.
 * 
 *	Parameters: 
 *	@SubmissionID
 *	@FirstLookErrorTypeID
 *  @ADPErrorTypeID 
 *  @MessageTypes
 *  @ErrorMessage
 *
 * History
 * ----------
 * 
 * CGC	01/21/2011	Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @ErrorID int

------------------------------------------------------------------------------------------------
-- Perform Insert on SubmissionError
------------------------------------------------------------------------------------------------
EXEC Distribution.SubmissionError#Insert @SubmissionID, @FirstLookErrorTypeID, @MessageTypes, @ErrorID OUTPUT

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

--IF Error type is not defined in the Table mark it as Undefined
IF Not EXISTS(select * from [IMT].[Distribution].[ADP_ErrorType] where ErrorTypeID=@ADPErrorTypeID)
BEGIN

SET @ADPErrorTypeID=0

END

INSERT INTO Distribution.ADP_Error (
	ErrorID,
	ErrorTypeID,
	Request,
    ErrorMessage
) VALUES (
	@ErrorID,
	@ADPErrorTypeID,
	@Request,
    @ErrorMessage
)





GO


