IF OBJECT_ID(N'Distribution.Content#Fetch') IS NOT NULL
	DROP PROCEDURE Distribution.Content#Fetch
GO

CREATE PROCEDURE Distribution.Content#Fetch	
    @MessageID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch records from the content tables for the given message.
 * 
 *	Parameters:
 *	@MessageID 
 *
 * History
 * ----------
 * 
 * CGC	01/22/2010	Create procedure using CodeSmith 5.0.0.0
 * 						
 * ------------------------------------------------------------------------------------------ */
 
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
        
------------------------------------------------------------------------------------------------
-- Fetch content data.
------------------------------------------------------------------------------------------------
 
SELECT
    C.ContentID,
    C.ContentTypeID,
    CASE 
		WHEN C.ContentTypeID = 1 THEN CT.Text		
	END AS Content
FROM
    Distribution.Advertisement_Content AC
JOIN
    Distribution.Content C ON C.ContentID = AC.ContentID
JOIN
    Distribution.ContentText CT ON CT.ContentID = C.ContentID
WHERE
    AC.MessageID = @MessageID   
ORDER BY C.ContentID

GO
    