IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[Distribution].[ADP_Inventory#Fetch]')
                    AND type IN ( N'P', N'PC' ) )
    DROP PROCEDURE [Distribution].[ADP_Inventory#Fetch]
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	This SP returns ADP details from ADP configuration based on InventoryId and BusinessUnit Id
-- =============================================
CREATE PROCEDURE [Distribution].[ADP_Inventory#Fetch]
    @InventoryId INT
  , @BusinessUnitId INT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        SELECT  [i].[DealershipIdentifier] AS [3PA Dealer Id]
              , [i].[StockNumber]
              , [v].[Vin]
              , [i].[InventoryID]
        FROM    [IMT].[dbo].[Inventory] i
                INNER JOIN [IMT].[dbo].[tbl_Vehicle] v ON [i].[VehicleID] = [v].[VehicleID]
        WHERE   [i].[BusinessUnitID] = @BusinessUnitId
                AND [I].[InventoryID] = @InventoryID
                AND [i].[InventoryActive] = 1
                AND [i].[InventoryStateID] = 1 -- 2's should never be 'written back'
                AND [i].[InventoryType] = 2 -- Only Used Vehicles (won't work with current test data)
              
    END


GO


