IF OBJECT_ID(N'Distribution.SubmissionError#Fetch') IS NOT NULL
	DROP PROCEDURE Distribution.SubmissionError#Fetch
GO

CREATE PROCEDURE Distribution.SubmissionError#Fetch
	@SubmissionID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch a record from the Distribution.SubmissionError table.
 * 
 *	Parameters:
 *	@SubmissionID
 *
 * History
 * ----------
 * 
 * CGC	02/22/2010	Create procedure.
 * CGC	06/17/2010	Added message type.
 * CGC  07/19/2010  Changing name of MessageTypeFlag since it's not a flag.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
        
------------------------------------------------------------------------------------------------
-- Fetch submission data
------------------------------------------------------------------------------------------------
        
SELECT
    E.SubmissionID,
	E.ErrorID,
	E.ErrorTypeID,
    E.OccuredOn,
    EC.Description,
    EC.ConcernsUser,
    E.MessageTypes
FROM
	Distribution.SubmissionError E
JOIN
    Distribution.SubmissionErrorType EC ON EC.ErrorTypeID = E.ErrorTypeID
WHERE
	E.SubmissionID = @SubmissionID

GO
    