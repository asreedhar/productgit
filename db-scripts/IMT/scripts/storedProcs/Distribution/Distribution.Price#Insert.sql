IF OBJECT_ID(N'Distribution.Price#Insert') IS NOT NULL
	DROP PROCEDURE Distribution.Price#Insert
GO

CREATE PROCEDURE Distribution.Price#Insert
	@MessageID int,	
	@PriceTypeID int,
    @Value int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.Price table.
 * 
 *	Parameters: 
 *  @MessageID
 *  @PriceTypeID
 *  @Value
 *
 * History
 * ----------
 * 
 * CGC  02/22/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO Distribution.Price (	
	MessageID,	
	PriceTypeID,
    Value
) VALUES (
	@MessageID,
	@PriceTypeID,
    @Value
)

GO
