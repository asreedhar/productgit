IF OBJECT_ID(N'Distribution.Vehicle#Insert') IS NOT NULL
	DROP PROCEDURE Distribution.Vehicle#Insert
GO

CREATE PROCEDURE Distribution.Vehicle#Insert	
    @VehicleEntityID int,
	@VehicleEntityTypeID int,
	@InsertUser varchar(50)
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.Vehicle table.
 * 
 *	Parameters: 
 *  @VehicleEntityID
 *  @VehicleEntityTypeID 
 *  @InsertUser 
 *
 * History
 * ----------
 * 
 * CGC	01/22/2010	Create procedure using CodeSmith 5.0.0.0
 * CGC	02/22/2010	Changes to fit schema redesign.
 * CGC	08/16/2010	Insert only if not exists.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @InsertUserID INT, @err INT

EXEC @err = dbo.ValidateParameter_MemberID#UserName @InsertUser, @InsertUserID OUTPUT
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

IF NOT EXISTS (
	SELECT 1
    FROM Distribution.Vehicle
	WHERE VehicleEntityID = @VehicleEntityID
	AND VehicleEntityTypeID = @VehicleEntityTypeID
) 
INSERT INTO Distribution.Vehicle (
	VehicleEntityID,
	VehicleEntityTypeID,
	InsertUserID,
	InsertDate
) VALUES (
	@VehicleEntityID,
	@VehicleEntityTypeID,
	@InsertUserID,
	getdate()
)

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO

