IF OBJECT_ID(N'Distribution.Price#Fetch') IS NOT NULL
	DROP PROCEDURE Distribution.Price#Fetch
GO

CREATE PROCEDURE Distribution.Price#Fetch
	@MessageID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch a price record from the Distribution.Price table.
 * 
 *	Parameters:
 *  @MessageID
 *
 * History
 * ----------
 * 
 * CGC  02/22/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Fetch message data
------------------------------------------------------------------------------------------------

SELECT
	PriceTypeID,
	Value
FROM
	Distribution.Price
WHERE
	MessageID = @MessageID    
GO
