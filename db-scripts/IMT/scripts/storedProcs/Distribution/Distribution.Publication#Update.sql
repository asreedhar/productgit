IF OBJECT_ID(N'Distribution.Publication#Update') IS NOT NULL
	DROP PROCEDURE Distribution.Publication#Update
GO

CREATE PROCEDURE Distribution.Publication#Update
	@PublicationID int,
	@PublicationStatusID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Update a record in the Distribution.Publication table. Will only update the status field.
 * 
 *	Parameters: 
 *  @PublicationID
 *  @PublicationStatusID
 *
 * History
 * ----------
 * 
 * CGC	03/05/2010	Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Perform Update
------------------------------------------------------------------------------------------------

UPDATE Distribution.Publication SET	
	PublicationStatusID = @PublicationStatusID	
WHERE
	PublicationID = @PublicationID    
GO
