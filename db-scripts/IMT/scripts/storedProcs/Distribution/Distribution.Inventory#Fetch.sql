IF OBJECT_ID(N'Distribution.Inventory#Fetch') IS NOT NULL
	DROP PROCEDURE Distribution.Inventory#Fetch
GO

CREATE PROCEDURE Distribution.Inventory#Fetch
	@InventoryID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch the VIN for a vehicle from the dbo.Inventory table.
 * 
 *	Parameters:
 *	@InventoryID
 *
 * History
 * ----------
 * 
 * CGC	03/05/2010	Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

------------------------------------------------------------------------------------------------
-- Perform Fetch
------------------------------------------------------------------------------------------------

SELECT 
    V.VIN,
	V.VehicleYear,
	MMG.Make,
	MMG.Model
FROM
    FLDW.dbo.InventoryActive IA
JOIN 
    FLDW.dbo.Vehicle V ON V.BusinessUnitID = IA.BusinessUnitID AND V.VehicleID = IA.VehicleID
JOIN
	IMT.dbo.MakeModelGrouping MMG ON MMG.MakeModelGroupingID = V.MakeModelGroupingID
WHERE
    IA.InventoryID = @InventoryID
    
GO
    