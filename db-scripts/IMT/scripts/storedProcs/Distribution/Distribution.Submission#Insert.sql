IF OBJECT_ID(N'Distribution.Submission#Insert') IS NOT NULL
	DROP PROCEDURE Distribution.Submission#Insert
GO

CREATE PROCEDURE Distribution.Submission#Insert
	@PublicationID int,
    @ProviderID int,
    @SubmissionStatusID int,
	@MessageTypes int,
	@Attempt int,
    @SubmissionID int OUTPUT
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.Submission table.
 * 
 *	Parameters:
 *	@PublicationID  
 *	@ProviderID
 *	@SubmissionStatusID
 *  @MessageTypes
 *  @Attempt
 *  @SubmissionID (output)
 *
 * History
 * ----------
 * 
 * CGC	01/22/2010	Create procedure using CodeSmith 5.0.0.0
 * CGC	11/04/2010	Returns MessageTypes now.
 * CGC	11/12/2010	Inserts attempt.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO Distribution.Submission (
	PublicationID,
	ProviderID,
	SubmissionStatusID,
    PushedOnQueue,
	MessageTypes,
	Attempt
) VALUES (
	@PublicationID,
    @ProviderID,
	@SubmissionStatusID,
	getdate(),
	@MessageTypes,
	@Attempt
)

SET @SubmissionID = SCOPE_IDENTITY()

GO
