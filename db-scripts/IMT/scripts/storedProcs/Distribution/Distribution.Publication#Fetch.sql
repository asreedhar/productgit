IF OBJECT_ID(N'Distribution.Publication#Fetch') IS NOT NULL
	DROP PROCEDURE Distribution.Publication#Fetch
GO

CREATE PROCEDURE Distribution.Publication#Fetch
	@PublicationID int,
	@MessageTypes int = -1
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch a record from the Distribution.Publication table. Optional MessageTypes parameter 
 *  narrows which message parts are received.
 * 
 *	Parameters:
 *  @PublicationID
 *  @MessageTypes
 *
 * History
 * ----------
 * 
 * CGC	01/22/2010	Create procedure using CodeSmith 5.0.0.0
 * CGC  02/22/2010  Changes to fit schema redesign.
 * CGC  11/04/2010  Updated to take optional MessageTypes parameter.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @MessageID int

------------------------------------------------------------------------------------------------
-- Fetch publication data
------------------------------------------------------------------------------------------------

SELECT
	PublicationID,
	MessageID,
	PublicationStatusID	
FROM
	Distribution.Publication
WHERE
	PublicationID = @PublicationID
    
------------------------------------------------------------------------------------------------
-- Get the message id.
------------------------------------------------------------------------------------------------    
    
SELECT 
    @MessageID = MessageID 
FROM 
    Distribution.Publication 
WHERE 
    PublicationID = @PublicationID
    
------------------------------------------------------------------------------------------------
-- Fetch message data
------------------------------------------------------------------------------------------------    
    
EXEC Distribution.Message#Fetch @MessageID, @MessageTypes

GO
