IF OBJECT_ID(N'Distribution.Submission#Update') IS NOT NULL
	DROP PROCEDURE Distribution.Submission#Update
GO

CREATE PROCEDURE Distribution.Submission#Update
    @SubmissionID int,    
    @SubmissionStatusID int
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Update a record in the Distribution.Submission table.
 * 
 *	Parameters: 
 *  @SubmissionID
 *	@SubmissionStatusID 
 *
 * History
 * ----------
 * 
 * CGC	01/22/2010	Create procedure.
 * CGC	04/15/2011	Changed proc to take SubmissionID instead of PublicationID + ProviderID since
 *					the latter are not unique.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Perform Update
------------------------------------------------------------------------------------------------

UPDATE Distribution.Submission SET 	
	SubmissionStatusID = @SubmissionStatusID,
    PoppedOffQueue = CASE WHEN @SubmissionStatusID = 2 THEN getdate() ELSE PoppedOffQueue END,
    SubmissionSent = CASE WHEN @SubmissionStatusID = 3 THEN getdate() ELSE SubmissionSent END,
    ResponseReceived = CASE WHEN @SubmissionStatusID = 4 OR @SubmissionStatusID = 5 THEN getdate() ELSE ResponseReceived END
WHERE
    SubmissionID = @SubmissionID
    
GO
