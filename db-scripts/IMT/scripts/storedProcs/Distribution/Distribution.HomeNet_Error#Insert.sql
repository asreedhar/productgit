IF OBJECT_ID('[Distribution].[HomeNet_Error#Insert]', 'P') IS NOT NULL
    DROP PROCEDURE [Distribution].[HomeNet_Error#Insert]
GO
CREATE PROCEDURE [Distribution].[HomeNet_Error#Insert]
    @SubmissionID INT
  , @FirstLookErrorTypeID INT
  , @HomeNetErrorType INT
  , @MessageTypes INT
  , @ErrorMessage NVARCHAR(MAX) = NULL
AS
    BEGIN
        SET NOCOUNT ON 

        DECLARE @ErrorID INT

        EXEC [Distribution].[SubmissionError#Insert] @SubmissionID, @FirstLookErrorTypeID, @MessageTypes, @ErrorID OUTPUT

        INSERT  INTO [Distribution].[HomeNet_Error]
                ( ErrorID
                , ErrorTypeID
                , ErrorMessage
                )
        VALUES  ( @ErrorID
                , @HomeNetErrorType
                , @ErrorMessage
                )
        SET NOCOUNT OFF
    END

GO
