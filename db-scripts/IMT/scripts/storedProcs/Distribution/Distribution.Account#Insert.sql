IF OBJECT_ID(N'Distribution.Account#Insert') IS NOT NULL
	DROP PROCEDURE Distribution.Account#Insert
GO

CREATE PROCEDURE Distribution.Account#Insert
	@DealerID int,
	@ProviderID int,
	@UserName varchar(50) = NULL,
	@Password varchar(50) = NULL,
    @DealershipCode varchar(50) = NULL,
	@Active bit,
    @InsertUser varchar(50)
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Distribution.Account table. Triggers will automatically insert an 
 *  entry into Account_Audit as well.
 * 
 *	Parameters:
 *	@DealerID  
 *	@ProviderID 
 *	@UserName   
 *	@Password
 *  @DealershipCode
 *  @Active
 *	@InsertUser 
 *
 * History
 * ----------
 * 
 * CGC	01/22/2010	Create procedure using CodeSmith 5.0.0.0
 * CGC  06/02/2010  Disabled procedure until we no longer use dbo.InternetAdvertiserDealership 
 *                  instead of Account. 
 * CGC  06/18/2010  Changed procedure to read active status from 'DistributionActive' flag in
 *                  dbo.InternetAdvertiserDealership.
 * CGC  11/08/2010  Moving back to Distribution.Account tables as we migrate off reprice processor.
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @InsertUserID INT, @err INT

EXEC @err = dbo.ValidateParameter_MemberID#UserName @InsertUser, @InsertUserID OUTPUT
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO Distribution.Account (
	DealerID,
	ProviderID,
	UserName,
	Password,
    DealershipCode,
	Active,
    InsertUserID,
    InsertDate
) VALUES (
	@DealerID,
	@ProviderID,
	@UserName,
	@Password,
    @DealershipCode,
	@Active,
    @InsertUserID,
    getdate()
)

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
