IF OBJECT_ID(N'Distribution.Account#MessageTypeEnabled') IS NOT NULL
	DROP PROCEDURE Distribution.Account#MessageTypeEnabled
GO

CREATE PROCEDURE Distribution.Account#MessageTypeEnabled
	@DealerID int,
    @ProviderID int,
	@MessageTypes int
AS

/* --------------------------------------------------------------------------------------------- 
 * 
 * Summary
 * ----------
 * 
 *  Determine if there is valid account information for a dealer with a provider for a message
 *  type.  This determination has three components:
 *
 *		1. Is there active account information for the dealer with the provider? This data is in
 *         Distribution.Account.
 *      2. Does the provider support the message type? This data is in Distribution.
 *         ProviderMessageType. There is no direct lookup into this table as presence in 
 *         Distribution.AccountPreferences implies it.
 *      3. If the provider does support the message type, has the dealer enabled this setting? 
 *         This data is in Distribution.AccountPreferences.
 *
 *  What it means to "support a message type":
 *
 *      - A provider supports messages of type 3 (Price + Description)
 *      - If a message type is passed in as 7 (Price + Description + Mileage), the message type 
 *        is supported (the provider can handle the price and description parts of the message).
 *      - If a message type is passed in as 1 (Price) the message type is not supported (the
 *        needs to have price and description at minumum).  
 * 
 *	Parameters:
 *	@DealerID
 *  @ProviderID
 *  @MessageTypes
 *
 * History
 * ----------
 *  
 * CGC  03/19/2010  Create procedure.
 * CGC  06/02/2010  Changed procedure to read from dbo.InternetAdvertiserDealership instead 
 *                  of from Account. 
 * CGC  06/18/2010  Changed procedure to read active status from 'DistributionActive' flag in
 *                  dbo.InternetAdvertiserDealership.
 * CGC  06/21/2010  Changed proc to always return a result, not just when message is enabled.
 * CGC  07/19/2010  Changing name of MessageTypeFlag since it's not a flag.
 * CGC  11/04/2010  Now returns filtered message types as well.
 * CGC  11/08/2010  Moving back Distribution.Account tables as we migrate off reprice processor.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Determine if account is enabled.
------------------------------------------------------------------------------------------------

SELECT 
    CASE WHEN EXISTS
    (
        SELECT 1 
        FROM
            Distribution.Account A        
        JOIN
            Distribution.AccountPreferences AP ON AP.DealerID = A.DealerID AND AP.ProviderID = A.ProviderID
        WHERE    
            A.DealerID = @DealerID
        AND
            A.ProviderID = @ProviderID
        AND
            A.Active = 1
        AND
            (AP.MessageTypes & @MessageTypes) = AP.MessageTypes
    )
    THEN 1 ELSE 0 END AS Supports
	
------------------------------------------------------------------------------------------------
-- Get subset of message types that is supported, if applicable.
------------------------------------------------------------------------------------------------	
	
SELECT 
	COALESCE(MAX(@MessageTypes & MessageTypes), 0) AS FilteredMessageTypes
FROM 
	Distribution.AccountPreferences
WHERE 
	DealerID = @DealerID
AND 
	ProviderID = @ProviderID	
GO    
    
    
    
    
