IF ( OBJECT_ID(N'[Distribution].[AppraisalValue#Fetch]', 'P') IS NOT NULL )
    DROP PROCEDURE [Distribution].[ArchiveAgedPublications]
GO
CREATE PROCEDURE [Distribution].[ArchiveAgedPublications]
AS
    BEGIN
        SET NOCOUNT ON
        DECLARE @batchCount AS INTEGER = 5000
        DECLARE @largeBatchCount AS INTEGER = 1000000
        DECLARE @currentBatch AS INTEGER = 1
        DECLARE @msg AS VARCHAR(500)
        DECLARE @LogEventMasterID AS INTEGER
        DECLARE @Procedure sysname = OBJECT_NAME(@@PROCID)

        DECLARE @batchPubsToDelete TABLE
            (
              [PublicationID] INTEGER PRIMARY KEY
            , [MessageID] INTEGER
            )

        DECLARE @pubsToDelete TABLE
            (
              [PublicationID] INTEGER PRIMARY KEY
            , [MessageID] INTEGER
            )
        DECLARE @subsToDelete TABLE
            (
              [SubmissionID] INTEGER PRIMARY KEY
            )

        INSERT  INTO @batchPubsToDelete
                ( [PublicationID]
                , [MessageID]
                )
                SELECT TOP ( @largeBatchCount )
                        [p].[PublicationID]
                      , [p].[MessageID]
                FROM    [IMT].[Distribution].[Publication] p
                WHERE   [p].[InsertDate] < DATEADD(MONTH, -3, CAST(GETDATE() AS DATE))
                ORDER BY [p].[InsertDate] DESC
        OPTION  ( MAXDOP 1 )

        WHILE ( @currentBatch > 0 )
            BEGIN
                DELETE  @pubsToDelete
                DELETE  @subsToDelete

                DELETE TOP ( @batchCount )
                        @batchPubsToDelete
                OUTPUT  [Deleted].[PublicationID]
                      , [Deleted].[MessageID]
                        INTO @pubsToDelete ( [PublicationID], [MessageID] )

                SET @currentBatch = @@ROWCOUNT
                SET @msg = 'Queued ' + CAST(@currentBatch AS VARCHAR) + ' publications for archival.'

                EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID OUTPUT, 0

                INSERT  INTO @subsToDelete
                        ( [SubmissionID]
                        )
                        SELECT  [s].[SubmissionID]
                        FROM    @pubsToDelete p
                                INNER JOIN [IMT].[Distribution].[Submission] s ON [s].[PublicationID] = [p].[PublicationID]
                OPTION  ( MAXDOP 1 )

                SET @msg = 'Queued ' + CAST(@@ROWCOUNT AS VARCHAR) + ' submissions for archival.'
                EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID, 0

                DELETE  a
                OUTPUT  [Deleted].*
                        INTO [Archive].[Distribution].[SubmissionSuccess]
                FROM    [IMT].[Distribution].[SubmissionSuccess] a
                        INNER JOIN @subsToDelete c ON [c].[SubmissionID] = [a].[SubmissionID]
                OPTION  ( MAXDOP 1 )
                SET @msg = 'Archived ' + CAST(@@ROWCOUNT AS VARCHAR) + ' SubmissionSuccess records.'
                EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID, 0

                DELETE  a
                OUTPUT  [Deleted].*
                        INTO [Archive].[Distribution].[SubmissionStale]
                FROM    [IMT].[Distribution].[SubmissionStale] a
                        INNER JOIN @subsToDelete c ON [c].[SubmissionID] = [a].[SubmissionID]
                OPTION  ( MAXDOP 1 )
                SET @msg = 'Archived ' + CAST(@@ROWCOUNT AS VARCHAR) + ' SubmissionStale records.'
                EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID, 0

                DELETE  a
                OUTPUT  [Deleted].*
                        INTO [Archive].[Distribution].[Exception]
                FROM    [IMT].[Distribution].[Exception] a
                        INNER JOIN @subsToDelete c ON [c].[SubmissionID] = [a].[SubmissionID]
                OPTION  ( MAXDOP 1 )
                SET @msg = 'Archived ' + CAST(@@ROWCOUNT AS VARCHAR) + ' Exception records.'
                EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID, 0

                DELETE  d
                OUTPUT  [Deleted].*
                        INTO [Archive].[Distribution].[ADP_Error]
                FROM    [IMT].[Distribution].[ADP_Error] d
                        INNER JOIN [IMT].[Distribution].[SubmissionError] a ON [a].[ErrorID] = [d].[ErrorID]
                        INNER JOIN @subsToDelete c ON [c].[SubmissionID] = [a].[SubmissionID]
                OPTION  ( MAXDOP 1 )
                SET @msg = 'Archived ' + CAST(@@ROWCOUNT AS VARCHAR) + ' ADP_Error records.'
                EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID, 0

                DELETE  d
                OUTPUT  [Deleted].*
                        INTO [Archive].[Distribution].[AutoUplink_Error]
                FROM    [IMT].[Distribution].[AutoUplink_Error] d
                        INNER JOIN [IMT].[Distribution].[SubmissionError] a ON [a].[ErrorID] = [d].[ErrorID]
                        INNER JOIN @subsToDelete c ON [c].[SubmissionID] = [a].[SubmissionID]
                OPTION  ( MAXDOP 1 )
                SET @msg = 'Archived ' + CAST(@@ROWCOUNT AS VARCHAR) + ' AutoUplink_Error records.'
                EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID, 0

                DELETE  d
                OUTPUT  [Deleted].*
                        INTO [Archive].[Distribution].[AutoTrader_Error]
                FROM    [IMT].[Distribution].[AutoTrader_Error] d
                        INNER JOIN [IMT].[Distribution].[SubmissionError] a ON [a].[ErrorID] = [d].[ErrorID]
                        INNER JOIN @subsToDelete c ON [c].[SubmissionID] = [a].[SubmissionID]
                OPTION  ( MAXDOP 1 )
                SET @msg = 'Archived ' + CAST(@@ROWCOUNT AS VARCHAR) + ' AutoTrader_Error records.'
                EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID, 0

                DELETE  d
                OUTPUT  [Deleted].*
                        INTO [Archive].[Distribution].[GetAuto_Error]
                FROM    [IMT].[Distribution].[GetAuto_Error] d
                        INNER JOIN [IMT].[Distribution].[SubmissionError] a ON [a].[ErrorID] = [d].[ErrorID]
                        INNER JOIN @subsToDelete c ON [c].[SubmissionID] = [a].[SubmissionID]
                OPTION  ( MAXDOP 1 )
                SET @msg = 'Archived ' + CAST(@@ROWCOUNT AS VARCHAR) + ' GetAuto_Error records.'
                EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID, 0

                DELETE  d
                OUTPUT  [Deleted].*
                        INTO [Archive].[Distribution].[eLead_Error]
                FROM    [IMT].[Distribution].[eLead_Error] d
                        INNER JOIN [IMT].[Distribution].[SubmissionError] a ON [a].[ErrorID] = [d].[ErrorID]
                        INNER JOIN @subsToDelete c ON [c].[SubmissionID] = [a].[SubmissionID]
                OPTION  ( MAXDOP 1 )
                SET @msg = 'Archived ' + CAST(@@ROWCOUNT AS VARCHAR) + ' eLead_Error records.'
                EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID, 0

                DELETE  d
                OUTPUT  [Deleted].*
                        INTO [Archive].[Distribution].[eBiz_Error]
                FROM    [IMT].[Distribution].[eBiz_Error] d
                        INNER JOIN [IMT].[Distribution].[SubmissionError] a ON [a].[ErrorID] = [d].[ErrorID]
                        INNER JOIN @subsToDelete c ON [c].[SubmissionID] = [a].[SubmissionID]
                OPTION  ( MAXDOP 1 )
                SET @msg = 'Archived ' + CAST(@@ROWCOUNT AS VARCHAR) + ' eBiz_Error records.'
                EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID, 0

                DELETE  a
                OUTPUT  [Deleted].*
                        INTO [Archive].[Distribution].[SubmissionError]
                FROM    [IMT].[Distribution].[SubmissionError] a
                        INNER JOIN @subsToDelete c ON [c].[SubmissionID] = [a].[SubmissionID]
                OPTION  ( MAXDOP 1 )
                SET @msg = 'Archived ' + CAST(@@ROWCOUNT AS VARCHAR) + ' SubmissionError records.'
                EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID, 0

                DELETE  a
                OUTPUT  [Deleted].*
                        INTO [Archive].[Distribution].[Submission]
                FROM    [IMT].[Distribution].[Submission] a
                        INNER JOIN @subsToDelete c ON [c].[SubmissionID] = [a].[SubmissionID]
                SET @msg = 'Archived ' + CAST(@@ROWCOUNT AS VARCHAR) + ' Submission records.'
                EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID, 0

                DELETE  a
                OUTPUT  [Deleted].*
                        INTO [Archive].[Distribution].[ContentText]
                FROM    [IMT].[Distribution].[ContentText] a
                        INNER JOIN [IMT].[Distribution].[Advertisement_Content] c ON [c].[ContentID] = [a].[ContentID]
                        INNER JOIN @pubsToDelete b ON [b].[MessageID] = [c].[MessageID]
                OPTION  ( MAXDOP 1 )
                SET @msg = 'Archived ' + CAST(@@ROWCOUNT AS VARCHAR) + ' ContentText records.'
                EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID, 0

                --DELETE  a
                --OUTPUT  [Deleted].*
                --        INTO [Archive].[Distribution].[Content]
                --FROM    [IMT].[Distribution].[Content] a
                --        INNER JOIN [IMT].[Distribution].[Advertisement_Content] c ON [c].[ContentID] = [a].[ContentID]
                --        INNER JOIN @pubsToDelete b ON [b].[MessageID] = [c].[MessageID]
                --OPTION  ( MAXDOP 1 )
                --SET @msg = 'Archived ' + CAST(@@ROWCOUNT AS VARCHAR) + ' Content records.'
                --EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID, 0

                DELETE  a
                OUTPUT  [Deleted].*
                        INTO [Archive].[Distribution].[Advertisement_Content]
                FROM    [IMT].[Distribution].[Advertisement_Content] a
                        INNER JOIN @pubsToDelete b ON [b].[MessageID] = [a].[MessageID]
                OPTION  ( MAXDOP 1 )
                SET @msg = 'Archived ' + CAST(@@ROWCOUNT AS VARCHAR) + ' Advertisement_Content records.'
                EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID, 0

                DELETE  a
                OUTPUT  [Deleted].*
                        INTO [Archive].[Distribution].[Publication]
                FROM    [IMT].[Distribution].[Publication] a
                        INNER JOIN @pubsToDelete b ON [b].[MessageID] = [a].[MessageID]
                OPTION  ( MAXDOP 1 )
                SET @msg = 'Archived ' + CAST(@@ROWCOUNT AS VARCHAR) + ' Publication records.'
                EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID, 0


                DELETE  a
                OUTPUT  [Deleted].*
                        INTO [Archive].[Distribution].[VehicleInformation]
                FROM    [IMT].[Distribution].[VehicleInformation] a
                        INNER JOIN @pubsToDelete b ON [b].[MessageID] = [a].[MessageID]
                OPTION  ( MAXDOP 1 )
                SET @msg = 'Archived ' + CAST(@@ROWCOUNT AS VARCHAR) + ' VehicleInformation records.'
                EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID, 0

                DELETE  a
                OUTPUT  [Deleted].*
                        INTO [Archive].[Distribution].[AppraisalValue]
                FROM    [IMT].[Distribution].[AppraisalValue] a
                        INNER JOIN @pubsToDelete b ON [b].[MessageID] = [a].[MessageID]
                OPTION  ( MAXDOP 1 )
                SET @msg = 'Archived ' + CAST(@@ROWCOUNT AS VARCHAR) + ' AppraisalValue records.'
                EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID, 0


                DELETE  a
                OUTPUT  [Deleted].*
                        INTO [Archive].[Distribution].[Price]
                FROM    [IMT].[Distribution].[Price] a
                        INNER JOIN @pubsToDelete b ON [b].[MessageID] = [a].[MessageID]
                OPTION  ( MAXDOP 1 )
                SET @msg = 'Archived ' + CAST(@@ROWCOUNT AS VARCHAR) + ' Price records.'
                EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID, 0

                DELETE  a
                OUTPUT  [Deleted].*
                        INTO [Archive].[Distribution].[Message]
                FROM    [IMT].[Distribution].[Message] a
                        INNER JOIN @pubsToDelete b ON [b].[MessageID] = [a].[MessageID]
                OPTION  ( MAXDOP 1 )
                SET @msg = 'Archived ' + CAST(@@ROWCOUNT AS VARCHAR) + ' Message records.'
                EXECUTE [DBASTAT].[dbo].[Log_Event] 'I', @Procedure, @msg, @LogEventMasterID, 0
            END 
        SET NOCOUNT OFF
    END
GO
