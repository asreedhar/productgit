USE [IMT]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PhotoTransfers#GetGroupParticipation]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[PhotoTransfers#GetGroupParticipation]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Returns enabled state of photo transfers given a business unit ID for either a dealership or a group of dealerships
CREATE PROCEDURE [dbo].[PhotoTransfers#GetGroupParticipation]
	@businessUnitId int,
	@enabled int output
AS
BEGIN

	declare @buTypeID int;
	declare @buParentID int;

	SET NOCOUNT ON;
	
	-- fetch type and parent of business unit	
	select @buTypeID = bu.businessUnitTypeID, @buParentID = bur.parentID
	from imt.dbo.businessUnit bu 
	join imt.dbo.businessUnitRelationship bur on bur.businessUnitId = bu.businessUnitId
	where bu.businessUnitId = @businessUnitID;
	
	-- if business unit is a dealership use parent group for lookup instead
	if (@buTypeID = 4)
		set @businessUnitId = @buParentID

	select @enabled = count(*) from PhotoTransferGroups where groupBusinessUnitId=@businessUnitId and enabled=1;
		
	return
END
