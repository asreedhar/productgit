IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.ValidateParameter_DealerID') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE dbo.ValidateParameter_DealerID AS SELECT 1')
GO

ALTER PROCEDURE [dbo].[ValidateParameter_DealerID]
	@DealerID INT
AS



/* --------------------------------------------------------------------
 * 
 * $Id: dbo.ValidateParameter_DealerID.sql,v 1.4 2010/02/10 21:54:23 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns 1 if the Dealer Exists. 
 * 
 * Parameters
 * ----------
 *
 * @DealerID   - ID that maps to a BusinessUnitID of Type 4 (Dealer) in the IMT..BusinessUnit table.
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @DealerID is null
 * 50200 - Expected one Row
 *
 * History
 * -------
 * MAK	05/22/2009	Create procedure.

 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

IF @DealerID IS NULL
BEGIN
	RAISERROR (50100,16,1,'DealerID')
	RETURN @@ERROR
END



------------------------------------------------------------------------------------------------
-- Populate Result Set
------------------------------------------------------------------------------------------------

IF NOT EXISTS (
	SELECT	1
	FROM	dbo.BusinessUnit B
	WHERE	BusinessUnitID =@DealerID
	AND	BusinessUnitTypeID =4 -- Type Dealer
)
	BEGIN
		RAISERROR (50106,16,1,'DealerID',@DealerID)
		RETURN @@ERROR
	END


SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO