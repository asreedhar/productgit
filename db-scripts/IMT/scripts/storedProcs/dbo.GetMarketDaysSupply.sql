

/****** Object:  StoredProcedure [dbo].[GetMarketDaysSupply]    Script Date: 05/20/2015 05:12:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetMarketDaysSupply]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetMarketDaysSupply]
GO



/****** Object:  StoredProcedure [dbo].[GetMarketDaysSupply]    Script Date: 05/20/2015 05:12:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetMarketDaysSupply]
@BusinessUnitId INT,
@InventoryId INT

AS

BEGIN

	SET NOCOUNT ON;
	
DECLARE @OwnerID INT
SELECT @OwnerID = OwnerID from market.Pricing.owner where OwnerEntityID = @BusinessUnitId
 
DECLARE @HardLimit INT, @PercentageLimit INT         
SELECT @HardLimit = 3,@PercentageLimit = 10   
    
DECLARE @MarketDaysSupplyBasePeriod INT, @PowerRegionID INT 
         
SELECT @MarketDaysSupplyBasePeriod = COALESCE(DPP.MarketDaysSupplyBasePeriod, DPS.MarketDaysSupplyBasePeriod, 90),          
  @PowerRegionID = CASE WHEN U.Active = 1 THEN PowerRegionID ELSE NULL END          
FROM [IMT]..BusinessUnit B          
LEFT JOIN [IMT]..DealerPreference_Pricing DPP ON B.BusinessUnitID = DPP.BusinessUnitID          
LEFT JOIN [IMT]..DealerPreference_Pricing DPS ON DPS.BusinessUnitID = 100150          
LEFT JOIN [IMT]..DealerPreference_JDPowerSettings DPJ ON B.BusinessUnitID = DPJ.BusinessUnitID          
LEFT JOIN [IMT]..DealerUpgrade U          
    ON B.BusinessUnitID = U.BusinessUnitID          
    AND U.DealerUpgradeCD = 18          
    AND U.Active = 1          
    AND U.EffectiveDateActive = 1          
WHERE B.BusinessUnitID = @BusinessUnitId   
           
           
SELECT   SSF.SearchTypeID, MarketDaysSupply = CASE WHEN COALESCE(SSF.SalesInBasePeriod,0) < @HardLimit THEN NULL          
	      WHEN ((SSF.SalesInBasePeriod / CAST(SRF.Units AS REAL))*100.0) < @PercentageLimit THEN NULL          
	      ELSE ROUND(SRF.Units / (SSF.SalesInBasePeriod / CAST(@MarketDaysSupplyBasePeriod AS REAL)),0)            
	      END         
           
FROM FLDW.dbo.InventoryActive IA      
  LEFT JOIN Market.Pricing.Search S ON S.OwnerID = @OwnerID AND IA.InventoryID = S.VehicleEntityID AND S.VehicleEntityTypeID = 1          
   LEFT JOIN Market.Pricing.SearchResult_F SRF ON SRF.OwnerID = @OwnerID AND S.SearchID = SRF.SearchID  --and  SRF.SearchTypeID  in(1,4)--AND S.DefaultSearchTypeID = SRF.SearchTypeID          
  LEFT JOIN Market.Pricing.SearchSales_F SSF ON SSF.OwnerID = @OwnerID AND S.SearchID = SSF.SearchID  and  ssf.SearchTypeID  in(1,4) aND SSF.SearchTypeID =SrF.SearchTypeID --AND S.DefaultSearchTypeID = SSF.SearchTypeID          
 WHERE IA.InventoryID=@InventoryID and IA.BusinessUnitID=@BusinessUnitID          
       
       
	SET NOCOUNT OFF;     
                
END

GO


