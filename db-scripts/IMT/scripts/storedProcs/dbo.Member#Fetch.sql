
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.Member#Fetch') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE dbo.Member#Fetch AS SELECT 1')
GO
 
ALTER PROCEDURE [dbo].[Member#Fetch]
	@UserName	VARCHAR(80)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.Member#Fetch.sql,v 1.4 2010/02/10 21:54:23 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns the member information for the given user name.
 * 
 * Parameters
 * ----------
 *
 * @UserName   - user name of the member
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @UserName is null
 * 50106 - Record with UserName does not exist
 * 50200 - Expected one Row
 *
 * History
 * ----------
 * 
 * MAK	05/22/2009	Create procedure.
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

IF @UserName IS NULL
BEGIN
	RAISERROR (50100,16,1,'UserName')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Declare Result Set
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE
	(Id		INT		NOT NULL,
	FirstName	VARCHAR(20)	NOT NULL,
	LastName	VARCHAR(20)	NOT NULL,
	UserName	VARCHAR(80)	NOT NULL,
	MemberType	INT		NOT NULL)

------------------------------------------------------------------------------------------------
-- Populate Result Set
------------------------------------------------------------------------------------------------

INSERT
INTO	@Results
	(Id,
	FirstName,
	LastName,
	UserName,
	MemberType)
SELECT	M.MemberID,
	M.FirstName,
	M.LastName,
	M.Login,
	M.MemberType
FROM	dbo.Member M
WHERE	M.Login = @UserName

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Result Set
------------------------------------------------------------------------------------------------

IF @rc = 0
BEGIN
	RAISERROR (50106,16,1,'Member',@UserName)
	RETURN @@ERROR
END

IF @rc > 1
BEGIN
	RAISERROR (50200,16,1,'Member',@UserName)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO