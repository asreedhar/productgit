SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.Dealer#Exists') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE dbo.Dealer#Exists AS SELECT 1')
GO
 
ALTER PROCEDURE [dbo].[Dealer#Exists]
	@Id	INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.Dealer#Exists.sql,v 1.4 2010/02/10 21:54:23 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns 1 if the Dealer Exists. 
 * 
 * Parameters
 * ----------
 *
 * @DealerID   - ID that maps to a BusinessUnitID of Type 4 (Dealer) in the IMT..BusinessUnit table.
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @DealerID is null
 * 50200 - Expected one Row
 *
 * History
 * -------
 * MAK	05/22/2009	Create procedure.
 * SBW	05/22/2009	BU type is 4 for dealer
 *			Exists returns 0 when dealer does not exist (i.e. it throws only on null parameters)
 *			Move comments outside the parameter declarations
 *			Add section headers for
 *				(a) declaring result set
 *				(b) populating result set
 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

IF @Id IS NULL
BEGIN
	RAISERROR (50100,16,1,'Id')
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Declare Result Set
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE
	([Exists] BIT)

------------------------------------------------------------------------------------------------
-- Populate Result Set
------------------------------------------------------------------------------------------------

IF EXISTS (
	SELECT	1
	FROM	dbo.BusinessUnit B
	WHERE	BusinessUnitID =@Id
	AND	BusinessUnitTypeID =4 -- Type Dealer
)
	INSERT INTO @Results ([Exists]) VALUES (CAST(1 AS BIT))
ELSE
	INSERT INTO @Results ([Exists]) VALUES (CAST(0 AS BIT))

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Validate Results
------------------------------------------------------------------------------------------------

-- there must be one row in the tables

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO