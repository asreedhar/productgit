USE [IMT]
GO
/****** Object:  StoredProcedure [dbo].[PhotoTransfers#Insert]    Script Date: 10/30/2012 15:51:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PhotoTransfers#Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[PhotoTransfers#Insert]
GO

CREATE PROCEDURE [dbo].[PhotoTransfers#Insert]
	@srcInventoryId int,
	@dstInventoryId int
AS
BEGIN

	insert into dbo.photoTransfers (srcInventoryId, dstInventoryId, queuedDt, statusDt, status)
	values (@srcInventoryId, @dstInventoryId, getdate(), getdate(), /* status= queued */ 0);

END
