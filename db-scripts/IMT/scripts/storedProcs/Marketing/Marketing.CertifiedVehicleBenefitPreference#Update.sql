IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[CertifiedVehicleBenefitPreference#Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[CertifiedVehicleBenefitPreference#Update]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[CertifiedVehicleBenefitPreference#Update]
    @CertifiedVehicleBenefitPreferenceID INT,
    @IsDisplayed BIT,
    @UpdateUser VARCHAR(80)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.CertifiedVehicleBenefitPreference#Update.sql,v 1.1.4.1.2.2 2010/06/02 18:40:27 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Updates a certified vehicle benefit preference. Update is only performed if something changed.
 * 
 * Input Parameters
 * ----------
 *
 * 
 * Output Parameters
 * ----------
 *
 * (none)
 *
 *
 * Exceptions
 * ----------
 * 
 * -------------------------------------------------------------------- */

BEGIN TRY
    
    DECLARE @UpdateUserID INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @UpdateUser, @UpdateUserID OUTPUT

    IF NOT EXISTS (SELECT 1 FROM Marketing.CertifiedVehicleBenefitPreference WHERE CertifiedVehicleBenefitPreferenceID = @CertifiedVehicleBenefitPreferenceID)
    BEGIN
		RAISERROR (50110,16,1,'CertifiedVehicleBenefitPreference', @CertifiedVehicleBenefitPreferenceID)
		RETURN @@ERROR
    END

    UPDATE  Marketing.CertifiedVehicleBenefitPreference
    SET IsDisplayed = @IsDisplayed,
	UpdateUser = @UpdateUserID,
	UpdateDate = GETDATE()
    WHERE 
		CertifiedVehicleBenefitPreferenceID = @CertifiedVehicleBenefitPreferenceID AND
		IsDisplayed != @IsDisplayed	

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
