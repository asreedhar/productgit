IF OBJECT_ID(N'[Marketing].[VehicleHistoryReportPreference#Insert]') IS NOT NULL
	DROP PROCEDURE [Marketing].[VehicleHistoryReportPreference#Insert]
GO

CREATE PROCEDURE [Marketing].[VehicleHistoryReportPreference#Insert]
	@OwnerHandle varchar(36),
    @InsertUser varchar(50),

	@VehicleHistoryReportPreferenceId INT OUTPUT
AS



SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------


DECLARE @InsertUserID INT, @err INT
EXEC @err = dbo.ValidateParameter_MemberID#UserName @InsertUser, @InsertUserID OUTPUT
IF @err <> 0 GOTO Failed

DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out


INSERT INTO [Marketing].[VehicleHistoryReportPreference](
	[OwnerId],
	[InsertUserId],
	[InsertDate]
)VALUES(
	@OwnerID,
	@InsertUserId,
    getdate()
)

SELECT @VehicleHistoryReportPreferenceId = SCOPE_IDENTITY()



Failed:

RETURN @err

GO
