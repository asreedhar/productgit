
IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'InventoryDocuments#FetchByOwnerHandle' AND SCHEMA_NAME(schema_id) = 'Marketing')
	BEGIN
		DROP  Procedure  [Marketing].[InventoryDocuments#FetchByOwnerHandle]
	END

GO

CREATE Procedure [Marketing].[InventoryDocuments#FetchByOwnerHandle]
	@ownerHandle VARCHAR(36)
AS

BEGIN TRY


-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
    
  	IF @OwnerHandle IS NULL
    BEGIN
		RAISERROR (50100,16,1,'OwnerHandle')
		RETURN @@ERROR
    END

--Let's get the owner id
--this will throw an exception if there is no such user
	DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
    EXEC Market.Pricing.ValidateParameter_OwnerHandle @ownerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT 


	SELECT D.DocumentXml
	FROM Marketing.InventoryDocuments D
	Join FLDW.dbo.InventoryActive I on D.VehicleEntityID = I.InventoryID 
	WHERE D.OwnerID = @OwnerID AND D.VehicleEntityTypeID = 1


END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH

GO


