IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[MarketListingVehiclePreferenceSearchOverrides#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[MarketListingVehiclePreferenceSearchOverrides#Fetch]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[MarketListingVehiclePreferenceSearchOverrides#Fetch]
	@MarketListingVehiclePreferenceId int
	
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.MarketListingVehiclePreferenceSearchOverrides#Fetch.sql
 * 
 * Summary
 * -------
 * 
 * Select search overrides that map back to the specified vehicle preference.
 * 
 * Input Parameters
 * ----------
 *
 *	@MarketListingVehiclePreferenceId int - the vehicle preference id.
 * 
 * -------------------------------------------------------------------- */

Select VehicleId
From Marketing.MarketListingVehiclePreferenceSearchOverrides so
Where 
	so.MarketListingVehiclePreferenceId = @MarketListingVehiclePreferenceId

