
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[MarketListingPreference#Create]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[MarketListingPreference#Create]
GO

CREATE Procedure [Marketing].[MarketListingPreference#Create]
	-- no parameters
AS

/* --------------------------------------------------------------------
 * 
 * $Id:
 * 
 * Summary
 * -------
 * 
 * Fetches a default MarketListingPreference
 * 
 * Parameters  - no params
 *
 * Exceptions
 * ----------
 * -------------------------------------------------------------------- */



BEGIN TRY

-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;




   SELECT
	mp.MarketListingPreferenceId,
	mp.IsDisplayed,
	mp.MileageDeltaBelow,
	mp.MileageDeltaAbove,
	mp.PriceDeltaAbove,
	mp.PriceDeltaBelow
   FROM Marketing.MarketListingPreference mp
   WHERE mp.OwnerID IS null

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH

GO
