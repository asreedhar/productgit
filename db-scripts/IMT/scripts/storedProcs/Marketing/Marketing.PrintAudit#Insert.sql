IF OBJECT_ID(N'[Marketing].[PrintAudit#Insert]') IS NOT NULL
	DROP PROCEDURE [Marketing].[PrintAudit#Insert]
GO

CREATE PROCEDURE [Marketing].[PrintAudit#Insert]
	@PrintXML      text,
	@OwnerHandle   varchar(36),
	@VehicleHandle varchar(36),
    @InsertUser    varchar(50)
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Marketing.PrintAudit table.
 * 
 *	Parameters:
 *	@PrintXML
 *	@OwnerHandle   
 *	@VehicleHandle
 *	@InsertUser 
 *
 * History
 * ----------
 * 
 * CGC	03/19/2010	Create procedure.
 * 10/25/2011  Removed PrintPDF
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

-- Owner
DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

EXEC @err = [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

-- Vehicle
DECLARE	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Market].[Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

-- Insert User
DECLARE @InsertUserID INT

EXEC @err = dbo.ValidateParameter_MemberID#UserName @InsertUser, @InsertUserID OUTPUT
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO [Marketing].[PrintAudit] (
	[PrintXML],
	[OwnerID],
	[VehicleEntityID],
	[VehicleEntityTypeID],
    [InsertUserID],
    [InsertDate]
) VALUES (
	@PrintXML,
	@OwnerID,
	@VehicleEntityID,
	@VehicleEntityTypeID,
    @InsertUserID,
    getdate()
)

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
