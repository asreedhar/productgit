USE IMT
GO

IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'MarketListingVehiclePreference#Update' AND SCHEMA_NAME(schema_id) = 'Marketing')
	BEGIN
		DROP  Procedure  [Marketing].[MarketListingVehiclePreference#Update]
	END

GO

CREATE Procedure [Marketing].[MarketListingVehiclePreference#Update]
	@MarketListingVehiclePreferenceId INT,
	@IsDisplayed BIT,
	@ShowDealerName BIT,
	@SortCode INT,
	@SortAscending BIT,
	@UpdateUser VARCHAR(80)
AS
/* --------------------------------------------------------------------
 * 
 * $Id:
 * 
 * Summary
 * -------
 * 
 * Updates a MarketListingVehiclePreference
 * 
 * Input Parameters
 * ----------
 *
 * @vehicleHandle
 * @OwnerHandle 
 * @offerPrice
 * @ShowDealerName
 * @ShowCertifiedIndicator
 * @MileageLow
 * @PriceHigh
 * @PriceLow
 * @UpdateUser 
 *
 * Output Parameters
 * ----------
 *
 * Exceptions
 *	50100 - Parameter IS NULL
 *  50106 - No such %s record with ID %d. Please reexecute with a more appropriate value.
 * ----------
 * -------------------------------------------------------------------- */

BEGIN TRY

-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


--Check for no nulls

	IF @MarketListingVehiclePreferenceId IS NULL
    BEGIN
	RAISERROR (50100,16,1,'MarketListingVehiclePreferenceId')
	RETURN @@ERROR
    END


    IF @IsDisplayed IS NULL
    BEGIN
	RAISERROR (50100,16,1,'IsDisplayed')
	RETURN @@ERROR
    END
    
    IF @UpdateUser IS NULL
        BEGIN
		RAISERROR (50100,16,1,'UpdateUser')
		RETURN @@ERROR
    END    


	IF NOT EXISTS ( SELECT 1 FROM Marketing.MarketListingVehiclePreference WHERE MarketListingVehiclePreferenceId = @MarketListingVehiclePreferenceId)
    BEGIN
	RAISERROR (50106,16,1,'MarketListingVehiclePreference', @MarketListingVehiclePreferenceId)
	RETURN @@ERROR
    END
    

--Get the update user id
    DECLARE @UpdateUserId INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @UpdateUser, @UpdateUserId OUTPUT    
	

    

UPDATE  Marketing.MarketListingVehiclePreference
    SET			
	    IsDisplayed = @IsDisplayed,
	    ShowDealerName = @ShowDealerName,
		SortCode = @SortCode,
		SortAscending = @SortAscending,
	    UpdateUserId = @UpdateUserId,
	    UpdateDate = GETDATE()
	    
    WHERE MarketListingVehiclePreferenceId  = @MarketListingVehiclePreferenceId

    -- Success
    RETURN 0

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
GO

