
IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'InventoryDocuments#Exists' AND SCHEMA_NAME(schema_id) = 'Marketing')
	BEGIN
		DROP  Procedure  [Marketing].[InventoryDocuments#Exists]
	END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [Marketing].[InventoryDocuments#Exists]
	@vehicleHandle VARCHAR(36), @ownerHandle VARCHAR(36)
AS

BEGIN TRY

	IF @vehicleHandle IS NULL
    BEGIN
		RAISERROR (50100,16,1,'vehicleHandle')
		RETURN @@ERROR
    END  
    
    
  	IF @ownerHandle IS NULL
    BEGIN
		RAISERROR (50100,16,1,'OwnerHandle')
		RETURN @@ERROR
    END


	DECLARE @VehicleEntityTypeID INT, @VehicleEntityID INT
	EXEC Market.Pricing.ValidateParameter_VehicleHandle @vehicleHandle, @VehicleEntityTypeID OUT, @VehicleEntityID OUT


	DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
    EXEC Market.Pricing.ValidateParameter_OwnerHandle @ownerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT 

    DECLARE @Exists BIT
    SET @Exists = 0     
	IF EXISTS ( SELECT 1 FROM Marketing.InventoryDocuments WHERE VehicleEntityID = @VehicleEntityID AND OwnerID = @OwnerID)
	SET @Exists = 1

    RETURN @Exists

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
