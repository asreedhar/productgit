USE IMT
GO

IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'AdPreviewVehiclePreference#Update' AND SCHEMA_NAME(schema_id) = 'Marketing')
	BEGIN
		DROP  Procedure  [Marketing].[AdPreviewVehiclePreference#Update]
	END

GO

CREATE Procedure [Marketing].[AdPreviewVehiclePreference#Update]
	@AdPreviewVehiclePreferenceId INT,
	@CharacterLimit INT,
	@UpdateUser VARCHAR(80)
AS
/* --------------------------------------------------------------------
 * 
 * $Id:
 * 
 * Summary
 * -------
 * 
 * Updates a MarketListingVehiclePreference
 * 
 * Input Parameters
 * ----------
 *
 * @vehicleHandle
 * @OwnerHandle 
 * @offerPrice
 * @ShowDealerName
 * @ShowCertifiedIndicator
 * @MileageLow
 * @PriceHigh
 * @PriceLow
 * @UpdateUser 
 *
 * Output Parameters
 * ----------
 *
 * Exceptions
 *	50100 - Parameter IS NULL
 *  50106 - No such %s record with ID %d. Please reexecute with a more appropriate value.
 * ----------
 * -------------------------------------------------------------------- */

BEGIN TRY

-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


--Check for no nulls

	IF @AdPreviewVehiclePreferenceId IS NULL
    BEGIN
	RAISERROR (50100,16,1,'AdPreviewVehiclePreferenceId')
	RETURN @@ERROR
    END


    IF @CharacterLimit IS NULL
    BEGIN
	RAISERROR (50100,16,1,'CharacterLimit')
	RETURN @@ERROR
    END


	IF NOT EXISTS ( SELECT 1 FROM Marketing.AdPreviewVehiclePreference WHERE AdPreviewVehiclePreferenceId = @AdPreviewVehiclePreferenceId)
    BEGIN
	RAISERROR (50106,16,1,'AdPreviewVehiclePreference', @AdPreviewVehiclePreferenceId)
	RETURN @@ERROR
    END
    

--Get the update user id
    DECLARE @UpdateUserId INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @UpdateUser, @UpdateUserId OUTPUT    
	

UPDATE  Marketing.AdPreviewVehiclePreference
    SET			
	    CharacterLimit = @CharacterLimit,
	    UpdateUserId = @UpdateUserId,
	    UpdateDate = GETDATE()
	    
    WHERE AdPreviewVehiclePreferenceId  = @AdPreviewVehiclePreferenceId

    -- Success
    RETURN 0

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
GO

