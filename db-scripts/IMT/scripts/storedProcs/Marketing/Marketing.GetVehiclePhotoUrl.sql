
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[GetVehiclePhotoUrl]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[GetVehiclePhotoUrl]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[GetVehiclePhotoUrl]
	@OwnerHandle varchar(36),
	@VehicleHandle varchar(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.GetVehiclePhotoUrl.sql,v 1.1.4.1.2.2 2010/06/02 18:40:27 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Return the vehicle photo url string.  Only inventory is currently supported.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle
 * @VehicleHandle
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle does not exists
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle does not exist
 *
 * History
 * ------- 
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

BEGIN TRY

	DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT, @VehicleEntityTypeID INT, @VehicleEntityID INT

	-- these calls will raise the appropriate errors if the handles are invalid.
	exec [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
	exec [Market].[Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out

	SELECT TOP 1 p.PhotoUrl AS Url
	FROM Photos p
	JOIN InventoryPhotos ip ON ip.PhotoID = p.PhotoID
	JOIN Inventory i ON ip.InventoryID = i.InventoryID
	WHERE i.InventoryID = @VehicleEntityID AND 
	      @VehicleEntityTypeID = 1 AND -- Only valid for Inventory at this time
		  p.PhotoTypeID = 2 
	ORDER BY IsPrimaryPhoto DESC, p.PhotoID	-- pick primary photo first		  

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH