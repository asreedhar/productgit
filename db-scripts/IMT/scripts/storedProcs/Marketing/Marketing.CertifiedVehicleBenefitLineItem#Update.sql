IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[CertifiedVehicleBenefitLineItem#Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[CertifiedVehicleBenefitLineItem#Update]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[CertifiedVehicleBenefitLineItem#Update]
    @CertifiedVehicleBenefitPreferenceID INT,
	@OwnerCertifiedProgramBenefitID INT,	
    @IsHighlight BIT,
	@Rank int,
    @UpdateUser VARCHAR(80)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.CertifiedVehicleBenefitLineItem#Update.sql,v 1.1.4.1.2.3 2010/06/03 19:42:39 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Updates a certified vehicle benefit line item. Update is only performed if something changed.
 * 
 * Input Parameters
 * ----------
 *
 * 
 * Output Parameters
 * ----------
 *
 * (none)
 *
 *
 * Exceptions
 * ----------
 * 
 * -------------------------------------------------------------------- */

BEGIN TRY
    
    DECLARE @UpdateUserID INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @UpdateUser, @UpdateUserID OUTPUT

    IF NOT EXISTS (	SELECT 1 FROM Marketing.CertifiedVehicleBenefitLineItem 
					WHERE 
						CertifiedVehicleBenefitPreferenceID = @CertifiedVehicleBenefitPreferenceID AND
						OwnerCertifiedProgramBenefitID = @OwnerCertifiedProgramBenefitID
				  )
    BEGIN
		RAISERROR (50110,16,1,'CertifiedVehicleBenefitLineItem', @CertifiedVehicleBenefitPreferenceID, @OwnerCertifiedProgramBenefitID)
		RETURN @@ERROR
    END

    UPDATE  Marketing.CertifiedVehicleBenefitLineItem
    SET IsHighlight = @IsHighlight,
	Rank = @Rank,
	UpdateUser = @UpdateUserID,
	UpdateDate = GETDATE()
    WHERE 
		CertifiedVehicleBenefitPreferenceID = @CertifiedVehicleBenefitPreferenceID AND
		OwnerCertifiedProgramBenefitID = @OwnerCertifiedProgramBenefitID AND
		(IsHighlight != @IsHighlight OR Rank != @Rank)

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
