
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[GetDealerLogoUrl]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[GetDealerLogoUrl]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[GetDealerLogoUrl]
	@OwnerHandle varchar(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.GetDealerLogoUrl.sql,v 1.1.4.1.2.2 2010/06/02 18:40:27 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Return the dealer logo url string.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle does not exists
 *
 * History
 * ------- 
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

BEGIN TRY

	DECLARE @rc INT, @err INT
	DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

	EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
	 
	Select Url = Marketing.GetDealerLogo (@OwnerEntityID)

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH