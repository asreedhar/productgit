
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[PhotoVehiclePreference#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[PhotoVehiclePreference#Fetch]
GO

CREATE PROCEDURE Marketing.PhotoVehiclePreference#Fetch
        @OwnerHandle            VARCHAR(36),
        @VehicleHandle          VARCHAR(36),
        @IsDisplayed		BIT OUTPUT
AS

/* --------------------------------------------------------------------
 * 
 * 
 * Summary
 * -------
 * 
 * Get the preference for displaying the vehicle photo
 *
 * Parameters
 * ----------
 * 
 * @OwnerHandle            - the owner of the new list
 * @VehicleHandle          - the vehicle whose equipment the list enumerates
 * @IsDisplayed		   - a value indicating whether or not the vehicle photo 
 *                           should be displayed.
 * 
 * Exceptions
 * ----------
 *
 * 50100 - OwnerHandle IS NULL
 * 50106 - The supplied handle does not resolve a known owner
 * 50100 - VehicleHandle IS NULL
 * 50106 - The supplied handle does not resolve a known vehicle
 *
 * History
 * ----------
 * 
 * DGH 04/12/2010 First Revision.
 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT


DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

EXEC @err = [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

DECLARE	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Market].[Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Do the work, default to 1 (visible = true)
------------------------------------------------------------------------------------------------
SELECT  @IsDisplayed = IsDisplayed
FROM    Marketing.PhotoVehiclePreference
WHERE   OwnerID = @OwnerID
AND     VehicleEntityTypeID = @VehicleEntityTypeID
AND     VehicleEntityID = @VehicleEntityID

------------------------------------------------------------------------------------------------
-- Default to 1 (visible = true)
------------------------------------------------------------------------------------------------
SELECT @IsDisplayed = ISNULL(@IsDisplayed, 1)

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
