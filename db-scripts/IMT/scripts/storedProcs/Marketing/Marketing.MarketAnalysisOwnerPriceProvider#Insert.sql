IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[MarketAnalysisOwnerPriceProvider#Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[MarketAnalysisOwnerPriceProvider#Insert]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[MarketAnalysisOwnerPriceProvider#Insert]
    @OwnerHandle VARCHAR(36),
    @PriceProviderId TINYINT,
    @Rank INT,
    @InsertUser VARCHAR(80),
    @VarianceAmount INT = NULL,
	@VarianceDirection TINYINT = NULL,
    @OwnerPriceProviderId INT OUTPUT
    
AS

/* --------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * Inserts a MarketAnalysisOwnerPriceProvider row
 * 
 * Input Parameters
 * ----------
 *
 * @OwnerHandle
 * @PriceProviderId
 * @Rank
 * @InsertUser
 * 
 * Output Parameters
 * ----------
 *
 * @OwnerPriceProviderId
 *
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle IS NULL
 * 50100 - @PriceProviderId IS NULL
 * 50100 - @Rank IS NULL
 * 50106 - ProviderRank does not exist (by @OwnerPriceProviderID)
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @OwnerHandle IS NULL
    BEGIN
        RAISERROR (50100,16,1,'OwnerHandle')
        RETURN @@ERROR
    END

    IF @PriceProviderId IS NULL
    BEGIN
        RAISERROR (50100,16,1,'PriceProviderId')
        RETURN @@ERROR
    END
    
    IF @Rank IS NULL
    BEGIN
        RAISERROR (50100,16,1,'Rank')
        RETURN @@ERROR
    END

    IF NOT EXISTS (SELECT * FROM Marketing.MarketAnalysisPriceProvider WHERE PriceProviderId = @PriceProviderId)
    BEGIN
        RAISERROR (50106,16,1,'MarketAnalysisPriceProvider', @PriceProviderId)
        RETURN @@ERROR
    END

    -- insert a row with a temporary rank, then update the row's rank to be what was passed in.

    DECLARE @InsertUserId INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @InsertUser, @InsertUserId OUTPUT

    DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
    EXEC Market.Pricing.ValidateParameter_OwnerHandle @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID OUT
    
    DECLARE @tempRank INT
    SELECT @tempRank = MAX(RANK) + 1
    FROM marketing.MarketAnalysisOwnerPriceProvider
    WHERE OwnerId = @OwnerID
    
    INSERT INTO Marketing.MarketAnalysisOwnerPriceProvider
            ( OwnerId,
              PriceProviderId,
              Rank,
              InsertUser,
              InsertDate,
              MinimumDollarVariance,
              VarianceDirection
              
            )
    VALUES  ( @OwnerID, 
              @PriceProviderId,
              @tempRank,
              @InsertUserId,
              GETDATE(),
              @VarianceAmount,
              @VarianceDirection
            )
    
    SELECT @OwnerPriceProviderId = SCOPE_IDENTITY()

    -- Capture the rank of this row before the update (tempRank)
    DECLARE @OldRank TINYINT
    SELECT  @OldRank = @tempRank

    IF (@OldRank <> @Rank)
    BEGIN
        -- The rank has been updated, this will involve updating multiple rows (at least two)
        UPDATE  Marketing.MarketAnalysisOwnerPriceProvider
        SET Rank = 
            CASE
                -- this is the primary update, the others adjust according to this one.
                WHEN Rank = @OldRank THEN @Rank

                -- The primary rank is decreasing in value (moving up the list), so we need to
                -- increase the value of the other affected ranks (move them down the list).
                WHEN @Rank < @OldRank AND Rank >= @Rank AND Rank <= @OldRank THEN Rank + 1

                -- The primary rank is increasing in value (moving down the list), so we need to
                -- decrease the value of the other affected ranks (move them up the list).
                WHEN @Rank > @OldRank AND Rank <= @Rank AND Rank >= @OldRank THEN Rank - 1

                -- Just for sanity-sake, any that don't fit the above conditions will have
                -- an unchanged rank
                ELSE Rank
            END
        FROM Marketing.MarketAnalysisOwnerPriceProvider
        WHERE OwnerId = @OwnerId
        AND (
            (@Rank < @OldRank AND Rank >= @Rank AND Rank <= @OldRank)   -- Rank decreased in value (moved up the list)
                OR 
            (@Rank > @OldRank AND Rank <= @Rank AND Rank >= @OldRank)   -- Rank increased in value (moved down the list)
            )
    END

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
