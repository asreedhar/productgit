IF OBJECT_ID(N'[Marketing].[ConsumerHighlightSectionPreference#Update]') IS NOT NULL
	DROP PROCEDURE [Marketing].[ConsumerHighlightSectionPreference#Update]
GO

CREATE PROCEDURE [Marketing].[ConsumerHighlightSectionPreference#Update]    
    @ConsumerHighlightSectionId int,
	@OwnerHandle varchar(36),	
    @Rank int,
    @UpdateUser varchar(50)
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Update a record in the Marketing.ConsumerHighlightSectionPreference table.
 * 
 *	Parameters: 
 *  @ConsumerHighlightSectionId
 *	@OwnerHandle
 *  @Rank 
 *  @UpdateUser
 *
 * Exceptions
 * ----------
 *  
 *  50100 - @MinimumJDPowerCircleRating is null 
 *
 * History
 * ----------
 * 
 *  CGC 04/07/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

IF @Rank IS NULL
    BEGIN
	RAISERROR (50100,16,1,'Rank')
	RETURN @@ERROR
    END    

DECLARE @UpdateUserID INT, @err INT
EXEC @err = dbo.ValidateParameter_MemberID#UserName @UpdateUser, @UpdateUserID OUTPUT
IF @err <> 0 GOTO Failed

DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out

-- Capture the rank of this row before the update
DECLARE @OldRank int

SELECT  @OldRank = Rank            
FROM    
    [Marketing].[ConsumerHighlightSectionPreference]
WHERE   
    [OwnerId] = @OwnerId
AND
    [ConsumerHighlightSectionId] = @ConsumerHighlightSectionId

------------------------------------------------------------------------------------------------
-- Perform Update
------------------------------------------------------------------------------------------------

IF (@OldRank <> @Rank)
BEGIN
    -- The rank has been updated, this will involve updating multiple rows (at least two)
    UPDATE  [Marketing].[ConsumerHighlightSectionPreference]
    SET Rank = 
        CASE
            -- this is the primary update, the others adjust according to this one.
            WHEN Rank = @OldRank THEN @Rank

            -- The primary rank is decreasing in value (moving up the list), so we need to
            -- increase the value of the other affected ranks (move them down the list).
            WHEN @Rank < @OldRank AND Rank >= @Rank AND Rank <= @OldRank THEN Rank + 1

            -- The primary rank is increasing in value (moving down the list), so we need to
            -- decrease the value of the other affected ranks (move them up the list).
            WHEN @Rank > @OldRank AND Rank <= @Rank AND Rank >= @OldRank THEN Rank - 1

            -- Any that don't fit the above conditions will have an unchanged rank
            ELSE Rank
        END,
        [UpdateUserId] = @UpdateUserID,
        [UpdateDate] = getdate()
    FROM 
        [Marketing].[ConsumerHighlightSectionPreference]
    WHERE 
        [OwnerId] = @OwnerId
    AND (
        (@Rank < @OldRank AND Rank >= @Rank AND Rank <= @OldRank)   -- Rank decreased in value (moved up the list)
            OR 
        (@Rank > @OldRank AND Rank <= @Rank AND Rank >= @OldRank)   -- Rank increased in value (moved down the list)
        )
END
    
------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
