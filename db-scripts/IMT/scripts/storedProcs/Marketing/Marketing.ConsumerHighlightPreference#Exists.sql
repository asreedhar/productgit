IF OBJECT_ID(N'[Marketing].[ConsumerHighlightPreference#Exists]') IS NOT NULL
	DROP PROCEDURE [Marketing].[ConsumerHighlightPreference#Exists]
GO

CREATE PROCEDURE [Marketing].[ConsumerHighlightPreference#Exists]
	@OwnerHandle varchar(36)
AS

/* --------------------------------------------------------------------------------------------- 
 * 
 * Summary
 * ----------
 * 
 *	Check if a record exists in Marketing.ConsumerHighlightPreference table for the given 
 *  owner.
 * 
 *	Parameters:
 *	@OwnerHandle
 *
 * History
 * ----------
 *  
 *  CGC 04/07/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out

------------------------------------------------------------------------------------------------
-- Perform Fetch
------------------------------------------------------------------------------------------------

SELECT 1 AS [Exists]
FROM 
    [Marketing].[ConsumerHighlightPreference]
WHERE
    [OwnerId] = @OwnerId

GO
