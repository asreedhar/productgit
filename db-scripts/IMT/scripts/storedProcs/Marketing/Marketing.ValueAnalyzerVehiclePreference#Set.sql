
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[ValueAnalyzerVehiclePreference#Set]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[ValueAnalyzerVehiclePreference#Set]
GO

CREATE PROCEDURE Marketing.ValueAnalyzerVehiclePreference#Set
        @OwnerHandle			VARCHAR(36),
        @VehicleHandle			VARCHAR(36),
        @DisplayVehicleDescription	BIT,
        @UseLongMarketingListingLayout	BIT,
        @ShortUrl			VARCHAR(100) = NULL,
        @LongUrl			VARCHAR(1000) = NULL,
        @User				VARCHAR(80)
AS

/* --------------------------------------------------------------------
 * 
 * 
 * Summary
 * -------
 * 
 * Set the preference for Value Analyzer
 *
 * Parameters
 * ----------
 * 
 * @OwnerHandle			    - the owner of the new list
 * @VehicleHandle		    - the vehicle whose equipment the list enumerates
 * @DisplayVehicleDescription	    - a value indicating whether or not a vehicle description
 *					should be shown for this owner/vehicle combination
 * @UseLongMarketingListingLayout   - a value indicating whether or not the longer, more verbose 
 *					market listing layout mode should be used.
 * @ShortUrl			    - a tiny url
 * @LongUrl			    - a long url
 * 
 * Exceptions
 * ----------
 *
 * 50100 - OwnerHandle IS NULL
 * 50106 - The supplied handle does not resolve a known owner
 * 50100 - VehicleHandle IS NULL
 * 50106 - The supplied handle does not resolve a known vehicle
 * 50100 - @User is null
 * 50106 - @User does not exist
 *
 * History
 * ----------
 * 
 * DGH 05/13/2010 First Revision.
 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT


DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT, @UserID INT

EXEC @err = dbo.ValidateParameter_MemberID#UserName @User, @UserID OUTPUT
IF (@err <> 0) GOTO Failed

EXEC @err = [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

DECLARE	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Market].[Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Do the work
------------------------------------------------------------------------------------------------
IF NOT EXISTS(
    SELECT 1 
    FROM Marketing.ValueAnalyzerVehiclePreference
    WHERE   OwnerID = @OwnerID
    AND     VehicleEntityTypeID = @VehicleEntityTypeID
    AND     VehicleEntityID = @VehicleEntityID)
BEGIN

    INSERT INTO Marketing.ValueAnalyzerVehiclePreference
	( OwnerID,
	  VehicleEntityTypeID,
	  VehicleEntityID,
	  DisplayVehicleDescription,
	  UseLongMarketingListingLayout,
	  ShortUrl,
	  LongUrl,
	  InsertUser,
	  InsertDate
	)
    VALUES  
	( @OwnerID,
	  @VehicleEntityTypeID,
	  @VehicleEntityID,
	  @DisplayVehicleDescription,
	  @UseLongMarketingListingLayout,
	  @ShortUrl,
	  @LongUrl,
	  @UserID,
	  GETDATE()
	)
END
ELSE
BEGIN
    UPDATE Marketing.ValueAnalyzerVehiclePreference
    SET 
	DisplayVehicleDescription = @DisplayVehicleDescription,
	UseLongMarketingListingLayout = @UseLongMarketingListingLayout,
	ShortUrl = @ShortUrl,
	LongUrl = @LongUrl,
	UpdateUser = @UserID,
	UpdateDate = GETDATE()
    WHERE   OwnerID = @OwnerID
    AND     VehicleEntityTypeID = @VehicleEntityTypeID
    AND     VehicleEntityID = @VehicleEntityID
END

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
