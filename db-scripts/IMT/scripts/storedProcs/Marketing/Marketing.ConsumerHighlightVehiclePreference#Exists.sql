IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[ConsumerHighlightVehiclePreference#Exists]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[ConsumerHighlightVehiclePreference#Exists]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[ConsumerHighlightVehiclePreference#Exists]
    @OwnerHandle VARCHAR(36),
    @VehicleHandle VARCHAR(36)

AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.ConsumerHighlightVehiclePreference#Exists.sql,v 1.1.4.1.2.2 2010/06/02 18:40:27 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Returns a value indicating whether or not the ConsumerHighlightVehiclePreference exists, by owner handle and vehicle handle.
 * 
 * Input Parameters
 * ----------
 *
 * @OwnerHandle
 * @VehicleHandle
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle does not exists
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle does not exist
 * -------------------------------------------------------------------- */

BEGIN TRY

	--------------------------------------------------------------------------------------------
	-- Validate Parameter Values
	--------------------------------------------------------------------------------------------

	DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT, @VehicleEntityTypeID INT, @VehicleEntityID INT

	-- these calls will raise the appropriate errors if the handles are invalid.
	exec [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
	exec [Market].[Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out

    IF NOT EXISTS 
	(
		SELECT 1 FROM Marketing.ConsumerHighlightVehiclePreference
		WHERE 
			OwnerID = @OwnerID AND 
			VehicleEntityTypeID = @VehicleEntityTypeID AND 
			VehicleEntityID = @VehicleEntityID 
	)
    BEGIN
	    RETURN 0
    END

	RETURN 1

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
