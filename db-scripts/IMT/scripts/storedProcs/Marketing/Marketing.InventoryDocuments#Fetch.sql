USE IMT
GO

IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'InventoryDocuments#Fetch' AND SCHEMA_NAME(schema_id) = 'Marketing')
	BEGIN
		DROP  Procedure  [Marketing].[InventoryDocuments#Fetch]
	END

GO

CREATE Procedure [Marketing].[InventoryDocuments#Fetch]
	@vehicleHandle VARCHAR(36), @ownerHandle VARCHAR(36)
AS

BEGIN TRY


-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

	IF @vehicleHandle IS NULL
    BEGIN
		RAISERROR (50100,16,1,'vehicleHandle')
		RETURN @@ERROR
    END  
    
    
  	IF @OwnerHandle IS NULL
    BEGIN
		RAISERROR (50100,16,1,'OwnerHandle')
		RETURN @@ERROR
    END
    
--Let's get the vehicle id
--this will throw an exception if there is no such vehicle
	DECLARE @VehicleEntityTypeID INT, @VehicleEntityID INT
	EXEC Market.Pricing.ValidateParameter_VehicleHandle @vehicleHandle, @VehicleEntityTypeID OUT, @VehicleEntityID OUT

--Let's get the owner id
--this will throw an exception if there is no such user
	DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
    EXEC Market.Pricing.ValidateParameter_OwnerHandle @ownerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT 


   SELECT
	DOC.DocumentID,
	DOC.DocumentXml
   FROM Marketing.InventoryDocuments DOC
   WHERE DOC.VehicleEntityID = @VehicleEntityID AND DOC.OwnerID = @OwnerID


END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH

GO


