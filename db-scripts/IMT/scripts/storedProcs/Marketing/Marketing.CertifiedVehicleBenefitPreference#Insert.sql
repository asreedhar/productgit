IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[CertifiedVehicleBenefitPreference#Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[CertifiedVehicleBenefitPreference#Insert]



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[CertifiedVehicleBenefitPreference#Insert]
    @OwnerHandle VARCHAR(36),
    @VehicleHandle VARCHAR(36),
    @IsDisplayed BIT,
	@OwnerCertifiedProgramID INT,	
    @InsertUser VARCHAR(80),
    @CertifiedVehiclePreferenceID INT OUTPUT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.CertifiedVehicleBenefitPreference#Insert.sql,v 1.1.4.1.2.2 2010/06/02 18:40:28 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Inserts a Certified Vehicle Benefit Preference
 *  
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle does not exists
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle does not exist
 *
 * -------------------------------------------------------------------- */

BEGIN TRY

	DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT, @VehicleEntityTypeID INT, @VehicleEntityID INT

	-- these calls will raise the appropriate errors if the handles are invalid.
	exec [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
	exec [Market].[Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out

    DECLARE @InsertUserID INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @InsertUser, @InsertUserID OUTPUT
     
    INSERT 
    INTO    Marketing.CertifiedVehicleBenefitPreference
	    (OwnerID,
	    VehicleEntityTypeID,
	    VehicleEntityID,
		IsDisplayed,
		OwnerCertifiedProgramID,		
	    InsertUser,
	    InsertDate)
    VALUES
	    (@OwnerID,
	    @VehicleEntityTypeID,
	    @VehicleEntityID,
		@IsDisplayed,
		@OwnerCertifiedProgramID,
	    @InsertUserID,
	    GETDATE())

    SET	@CertifiedVehiclePreferenceID = SCOPE_IDENTITY()

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
