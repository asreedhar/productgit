IF OBJECT_ID(N'[Marketing].[ConsumerHighlightSection#Fetch]') IS NOT NULL
	DROP PROCEDURE [Marketing].[ConsumerHighlightSection#Fetch]
GO

CREATE PROCEDURE [Marketing].[ConsumerHighlightSection#Fetch]	
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch all records from the Marketing.ConsumerHighlightSection table.
 *
 * History
 * ----------
 * 
 *  CGC 04/07/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Perform Fetch
------------------------------------------------------------------------------------------------

SELECT	
	[ConsumerHighlightSectionId],
    [Name],
    [DefaultRank]
FROM
	[Marketing].[ConsumerHighlightSection]
ORDER BY 
    [DefaultRank] ASC
    

GO
