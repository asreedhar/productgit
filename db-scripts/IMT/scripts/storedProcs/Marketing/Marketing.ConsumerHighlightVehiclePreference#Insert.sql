IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[ConsumerHighlightVehiclePreference#Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[ConsumerHighlightVehiclePreference#Insert]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[ConsumerHighlightVehiclePreference#Insert]
    @OwnerHandle VARCHAR(36),
    @VehicleHandle VARCHAR(36),
    @IsDisplayed BIT,
    @IncludeCarfaxOneOwnerIcon BIT,
    @InsertUser VARCHAR(80),
    @ConsumerHighlightVehiclePreferenceId INT OUTPUT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.ConsumerHighlightVehiclePreference#Insert.sql,v 1.1.4.1.2.2 2010/06/02 18:40:28 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Inserts a ConsumerHighlightVehiclePreference.
 * 
 * Input Parameters
 * ----------
 *
 * @OwnerHandle
 * @VehicleHandle
 * @IsDisplayed
 * @IncludeCarfaxOneOwnerIcon
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle does not exists
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle does not exist
 * 50113 - Preference already exists
 * 50100 - @IsDisplayed is null
 * 50100 - @IncludeCarfaxOneOwnerIcon is null
 * -------------------------------------------------------------------- */

BEGIN TRY

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------

    IF @IsDisplayed IS NULL
    BEGIN
	RAISERROR (50100,16,1,'IsDisplayed')
	RETURN @@ERROR
    END

    IF @IncludeCarfaxOneOwnerIcon IS NULL
    BEGIN
	RAISERROR (50100,16,1,'IncludeCarfaxOneOwnerIcon')
	RETURN @@ERROR
    END

    DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT, @VehicleEntityTypeID INT, @VehicleEntityID INT

    -- these calls will raise the appropriate errors if the handles are invalid.
    exec [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
    exec [Market].[Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out

    IF EXISTS (
		SELECT  1
		FROM    Marketing.ConsumerHighlightVehiclePreference
		WHERE 
			OwnerID = @OwnerID AND 
			VehicleEntityTypeID = @VehicleEntityTypeID AND 
			VehicleEntityID = @VehicleEntityID  )
    BEGIN
        RAISERROR (50113,16,1,'ConsumerHighlightVehiclePreference')
        RETURN @@ERROR
    END

    DECLARE @InsertUserID INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @InsertUser, @InsertUserID OUTPUT

    INSERT 
    INTO    Marketing.ConsumerHighlightVehiclePreference(
	    OwnerId, 
	    VehicleEntityId, 
	    VehicleEntityTypeID, 
	    IsDisplayed, 
	    IncludeCarfaxOneOwnerIcon, 
	    InsertUser, 
	    InsertDate)
    VALUES(
	    @OwnerID, 
	    @VehicleEntityID, 
	    @VehicleEntityTypeID, 
	    @IsDisplayed, 
	    @IncludeCarfaxOneOwnerIcon, 
	    @InsertUserID, 
	    GETDATE())

    SET @ConsumerHighlightVehiclePreferenceId = SCOPE_IDENTITY()

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
