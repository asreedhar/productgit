IF OBJECT_ID(N'[Marketing].[ConsumerHighlightSectionPreference#Insert]') IS NOT NULL
	DROP PROCEDURE [Marketing].[ConsumerHighlightSectionPreference#Insert]
GO

CREATE PROCEDURE [Marketing].[ConsumerHighlightSectionPreference#Insert]    
	@ConsumerHighlightSectionId int,
    @OwnerHandle varchar(36),
	@Rank int,
    @InsertUser varchar(50)
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Marketing.ConsumerHighlightSectionPreference table.
 * 
 *	Parameters: 
 *  @ConsumerHighlightSectionId
 *	@OwnerHandle
 *  @Rank 
 *  @InsertUser
 *
 * Exceptions
 * ----------
 *  
 *  50100 - @IsDisplayed is null 
 *
 * History
 * ----------
 * 
 *  CGC 04/07/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

IF @Rank IS NULL
    BEGIN
	RAISERROR (50100,16,1,'Rank')
	RETURN @@ERROR
    END    

DECLARE @InsertUserID INT, @err INT
EXEC @err = dbo.ValidateParameter_MemberID#UserName @InsertUser, @InsertUserID OUTPUT
IF @err <> 0 GOTO Failed

DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO [Marketing].[ConsumerHighlightSectionPreference] (
    [ConsumerHighlightSectionId],
    [OwnerId],
    [Rank],
    [InsertUserId],
    [InsertDate]
) VALUES (
    @ConsumerHighlightSectionId,
	@OwnerId,
	@Rank,	
	@InsertUserId,
    getdate()
)

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
