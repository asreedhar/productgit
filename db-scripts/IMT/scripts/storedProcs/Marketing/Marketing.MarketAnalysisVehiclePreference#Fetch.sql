IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[MarketAnalysisVehiclePreference#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[MarketAnalysisVehiclePreference#Fetch]


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[MarketAnalysisVehiclePreference#Fetch]
    @OwnerHandle VARCHAR(36),
    @VehicleHandle VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.MarketAnalysisVehiclePreference#Fetch.sql,v 1.1.4.1.2.3 2010/06/03 19:42:39 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Fetches a market analysis vehicle preference
 * 
 * Input Parameters
 * ----------
 *
 * @OwnerHandle
 * @VehicleHandle
 * 
 * Output Parameters
 * ----------
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle IS NULL
 * 50100 - @VehicleHandle IS NULL
 * 50106 - MarketAnalysisVehiclePreference record does not exist
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @OwnerHandle IS NULL
    BEGIN
        RAISERROR (50100,16,1,'OwnerHandle')
        RETURN @@ERROR
    END

    IF @VehicleHandle IS NULL
    BEGIN
        RAISERROR (50100,16,1,'VehicleHandle')
        RETURN @@ERROR
    END
    
    DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
            @VehicleEntityTypeID INT, @VehicleEntityID INT

    EXEC Market.Pricing.ValidateParameter_OwnerHandle @OwnerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT

    EXEC Market.Pricing.ValidateParameter_VehicleHandle @VehicleHandle, @VehicleEntityTypeID OUT, @VehicleEntityID OUT

    IF NOT EXISTS (
	SELECT 1 
	FROM Marketing.MarketAnalysisVehiclePreference 
	WHERE OwnerID = @OwnerID 
	AND VehicleEntityID = @VehicleEntityID
	AND VehicleEntityTypeId = @VehicleEntityTypeID
	)
    BEGIN
        RAISERROR (50106,16,1,'MarketAnalysisVehiclePreference')
        RETURN @@ERROR
    END

    -- return the vehicle preference
    SELECT
	    MarketAnalysisVehiclePreferenceId,
	    @OwnerHandle as OwnerHandle,
	    @VehicleHandle as VehicleHandle,
	    IsDisplayed,
            ShowPricingGauge,
            HasOverriddenPriceProviders
    FROM    Marketing.MarketAnalysisVehiclePreference
    WHERE   OwnerID = @OwnerID 
    AND	    VehicleEntityID = @VehicleEntityID
    AND	    VehicleEntityTypeId = @VehicleEntityTypeID
    
    -- return the vehicle preference price providers
    SELECT  MarketAnalysisPriceProviderId
    FROM    Marketing.MarketAnalysisVehiclePreferencePriceProvider pp
    JOIN    marketing.MarketAnalysisVehiclePreference vp
    ON	    pp.MarketAnalysisVehiclePreferenceId = vp.MarketAnalysisVehiclePreferenceId
    WHERE   vp.OwnerID = @OwnerID 
    AND	    vp.VehicleEntityID = @VehicleEntityID
    AND	    vp.VehicleEntityTypeId = @VehicleEntityTypeID
    AND	    vp.HasOverriddenPriceProviders = 1
    
END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
