IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[ConsumerHighlightVehiclePreference#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[ConsumerHighlightVehiclePreference#Fetch]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[ConsumerHighlightVehiclePreference#Fetch]
    @OwnerHandle VARCHAR(36),
    @VehicleHandle VARCHAR(36),
    @VehicleCatalogId int

AS

/* --------------------------------------------------------------------
 *
 * Summary
 * -------
 * 
 * Fetches the ConsumerHighlightVehiclePreference by owner handle and vehicle handle.
 * 
 * Input Parameters
 * ----------
 *
 * @OwnerHandle
 * @VehicleHandle
 * @VehicleCatalogId
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle does not exists
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle does not exist
 * 50106 - Preference does not exist
 *
 * History
 * ----------
 *
 * 2010/04/09 - Zac: Created proc.
 * 2010/04/30 - Christian: Altered to fetch auxiliary highlight info. 
 *
 * -------------------------------------------------------------------- */

BEGIN TRY

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------
    DECLARE @err INT, @errMsg VARCHAR(2000)

    DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT, @VehicleEntityTypeID INT, @VehicleEntityID INT

    -- these calls will raise the appropriate errors if the handles are invalid.
    exec [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
    exec [Market].[Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out

    DECLARE @ConsumerHighlightVehiclePreferenceId INT
    SELECT  
        @ConsumerHighlightVehiclePreferenceId = ConsumerHighlightVehiclePreferenceID
    FROM    
        Marketing.ConsumerHighlightVehiclePreference
    WHERE 
	    OwnerID = @OwnerID AND 
	    VehicleEntityTypeID = @VehicleEntityTypeID AND 
	    VehicleEntityID = @VehicleEntityID 

    IF @ConsumerHighlightVehiclePreferenceId IS NULL
    BEGIN
	RAISERROR (50106,16,1,'ConsumerHighlightVehiclePreference', @OwnerID, @VehicleHandle )
	RETURN @@ERROR
    END

--------------------------------------------------------------------------------------------
-- Fetch Basic Preference Data
--------------------------------------------------------------------------------------------
    
    SELECT
	    ConsumerHighlightVehiclePreferenceId,
	    IsDisplayed,
	    IncludeCarfaxOneOwnerIcon,
        CASE WHEN UpdateDate IS NOT NULL THEN UpdateDate ELSE InsertDate END AS ChangedDate
    FROM    
        Marketing.ConsumerHighlightVehiclePreference
    WHERE   
        ConsumerHighlightVehiclePreferenceID = @ConsumerHighlightVehiclePreferenceId

--------------------------------------------------------------------------------------------
-- Fetch Free Text Highlights.
--------------------------------------------------------------------------------------------

    SELECT
        ConsumerHighlightId,
        ConsumerHighlightVehiclePreferenceId,
        HighlightProviderId,
        Text,
        Rank,
        IsDisplayed
    FROM 
        Marketing.ConsumerHighlight
    WHERE 
        ConsumerHighlightVehiclePreferenceId = @ConsumerHighlightVehiclePreferenceId
    AND 
        HighlightProviderId = 1
    ORDER BY Rank ASC

--------------------------------------------------------------------------------------------
-- Fetch Vehicle History Report Highlights.
--------------------------------------------------------------------------------------------

    SELECT
        ConsumerHighlightId,
        ConsumerHighlightVehiclePreferenceId,
        HighlightProviderId,
        Text,
        Rank,
        IsDisplayed,
        ExpirationDate
    FROM 
        Marketing.ConsumerHighlight
    WHERE 
        ConsumerHighlightVehiclePreferenceId = @ConsumerHighlightVehiclePreferenceId
    AND 
        (HighlightProviderId = 2 OR HighlightProviderId = 3)
    ORDER BY Rank ASC

--------------------------------------------------------------------------------------------
-- Fetch Circle Rating Highlights.
--------------------------------------------------------------------------------------------

    SELECT
        CH.ConsumerHighlightId,
        CH.ConsumerHighlightVehiclePreferenceId,
        CH.HighlightProviderId,        
        CH.Rank,
        CH.IsDisplayed,
        CH.ProviderSourceRowId,
        JD.SourceID,
        JD.CircleRating,
        JDS.Description,
        JD.DateCreated
    FROM 
        Marketing.ConsumerHighlight CH
    JOIN 
        [VehicleCatalog].[FirstLook].[VehicleCatalog_JDPowerRating] JD ON JD.JoinID = CH.ProviderSourceRowId AND JD.VehicleCatalogID = @VehicleCatalogId
    JOIN 
        [VehicleCatalog].[FirstLook].[JDPowerRatingSource] JDS ON JDS.SourceID = JD.SourceID
    WHERE 
        CH.ConsumerHighlightVehiclePreferenceId = @ConsumerHighlightVehiclePreferenceId
    AND 
        CH.HighlightProviderId = 5
    AND
		JD.SourceID IN (1,2,3,4)
    ORDER BY CH.Rank ASC
    
--------------------------------------------------------------------------------------------
-- Fetch Snippet Highlights.
--------------------------------------------------------------------------------------------

    SELECT
        CH.ConsumerHighlightId,
        CH.ConsumerHighlightVehiclePreferenceId,
        CH.HighlightProviderId,
        CH.Rank,
        CH.IsDisplayed,
        CH.ProviderSourceRowId,
        MT.MarketingText,
        MT.SourceId,
        MTS.Name,
        MTVC.DateCreated
    FROM 
        Marketing.ConsumerHighlight CH
    JOIN
        [VehicleCatalog].[FirstLook].[MarketingText] MT ON MT.MarketingTextID = CH.ProviderSourceRowId
    JOIN
        [VehicleCatalog].[FirstLook].[MarketingTextSource] MTS ON MTS.MarketingTextSourceID = MT.SourceID
    JOIN
        [VehicleCatalog].[FirstLook].[MarketingTextVehicleCatalog] MTVC ON MTVC.MarketingTextID = MT.MarketingTextID AND MTVC.VehicleCatalogID = @VehicleCatalogId
    WHERE 
        CH.ConsumerHighlightVehiclePreferenceId = @ConsumerHighlightVehiclePreferenceId
    AND 
        CH.HighlightProviderId = 4
    ORDER BY CH.Rank ASC    

--------------------------------------------------------------------------------------------
-- Error Handling
--------------------------------------------------------------------------------------------

END TRY

BEGIN CATCH

	SELECT @err =ERROR_NUMBER(), @errMsg =ERROR_MESSAGE()
	RAISERROR(@errMsg, 16, 1)
	RETURN @err

END CATCH

