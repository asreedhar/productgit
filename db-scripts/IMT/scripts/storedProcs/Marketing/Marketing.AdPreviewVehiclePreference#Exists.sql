USE [IMT]
GO

IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'AdPreviewVehiclePreference#Exists' AND SCHEMA_NAME(schema_id) = 'Marketing')
	BEGIN
		DROP  Procedure  [Marketing].[AdPreviewVehiclePreference#Exists]
	END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [Marketing].[AdPreviewVehiclePreference#Exists]
	@vehicleHandle VARCHAR(36), @ownerHandle VARCHAR(36)
AS
/* --------------------------------------------------------------------
 * 
 * $Id:
 * 
 * Summary
 * -------
 * 
 * Verifies that a AdPreviewVehiclePreference is part of db
 * 
 * Input Parameters
 * ----------
 *
 * @vehicleHandle
 * @OwnerHandle 
 *
 * Output Parameters
 * ----------
 * @Exists
 * 
 *
 * Exceptions
 *	50100 - Parameter IS NULL  
 * ----------
 * -------------------------------------------------------------------- */
BEGIN TRY

	IF @vehicleHandle IS NULL
    BEGIN
		RAISERROR (50100,16,1,'vehicleHandle')
		RETURN @@ERROR
    END  
    
    
  	IF @ownerHandle IS NULL
    BEGIN
		RAISERROR (50100,16,1,'OwnerHandle')
		RETURN @@ERROR
    END

--Let's get the vehicle id
--this will throw an exception if there is no such vehicle
	DECLARE @VehicleEntityTypeID INT, @VehicleEntityID INT
	EXEC Market.Pricing.ValidateParameter_VehicleHandle @vehicleHandle, @VehicleEntityTypeID OUT, @VehicleEntityID OUT

--Let's get the owner id
--this will throw an exception if there is no such user
	DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
    EXEC Market.Pricing.ValidateParameter_OwnerHandle @ownerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT 

    DECLARE @Exists BIT
    SET @Exists = 0     
	IF EXISTS ( SELECT 1 FROM Marketing.AdPreviewVehiclePreference WHERE VehicleEntityID = @VehicleEntityID AND OwnerID = @OwnerID)
	SET @Exists = 1

    RETURN @Exists

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
