IF OBJECT_ID(N'[Marketing].[SnippetSourcePreference#Exists]') IS NOT NULL
	DROP PROCEDURE [Marketing].[SnippetSourcePreference#Exists]
GO

CREATE PROCEDURE [Marketing].[SnippetSourcePreference#Exists]
	@OwnerHandle varchar(36)
AS

/* --------------------------------------------------------------------------------------------- 
 * 
 * Summary
 * ----------
 * 
 *	Check if any records exist in Marketing.SnippetSourcePreference table for the given owner.
 * 
 *	Parameters:
 *	@OwnerHandle 
 *
 * History
 * ----------
 *  
 * CGC  04/07/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out

------------------------------------------------------------------------------------------------
-- Perform Fetch
------------------------------------------------------------------------------------------------

SELECT 1 AS [Exists]
FROM 
    [Marketing].[SnippetSourcePreference]
WHERE
    [OwnerId] = @OwnerId

GO
