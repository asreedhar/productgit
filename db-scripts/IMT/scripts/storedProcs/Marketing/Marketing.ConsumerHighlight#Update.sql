IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[ConsumerHighlight#Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[ConsumerHighlight#Update]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[ConsumerHighlight#Update]
    @ConsumerHighlightId INT,
    @Text VARCHAR(200) = NULL,
    @Rank INT,
    @ProviderSourceRowId INT = NULL,
    @IsDisplayed BIT,
    @UpdateUser VARCHAR(80)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.ConsumerHighlight#Update.sql,v 1.1.4.1.2.3 2010/06/03 19:42:39 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Udpates a ConsumerHighlight.
 * 
 * Input Parameters
 * ----------
 *
 * @ConsumerHighlightId
 * @Text
 * @Rank
 * @IsDisplayed
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @ConsumerHighlightId is null
 * 50106 - ConsumerHighlight does not exists
 * 50100 - @Text is null or empty
 * 50100 - @Rank is null
 * 50100 - @IsDisplayed is null
 * 50000 - Vehicle History Consumer Highlights cannot have their Text values changed
 *
 * History
 * ----------
 * 2010/04/09 - Zac Brown - Create Proc
 * 2010/05/05 - Christian Clouston - Removed empty check on text.
 * -------------------------------------------------------------------- */

BEGIN TRY

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------


    IF @ConsumerHighlightId IS NULL
    BEGIN
	RAISERROR (50100,16,1,'ConsumerHighlightId')
	RETURN @@ERROR
    END

    IF NOT EXISTS (
		SELECT  1
		FROM    Marketing.ConsumerHighlight
		WHERE	ConsumerHighlightId = @ConsumerHighlightId	)
    BEGIN
	RAISERROR (50106,16,1,'ConsumerHighlightId', @ConsumerHighlightId )
	RETURN @@ERROR
    END

    -- Only "Free Text" items can have their text values updated.  
    -- If the highlight is not free text, and the text passed in differs than
    -- the current text, raise an error.
    DECLARE @HighlightProviderId TINYINT
    DECLARE @CurrentText VARCHAR(200)

    SELECT  @HighlightProviderId = HighlightProviderId,
	    @CurrentText = @Text
    FROM    Marketing.ConsumerHighlight
    WHERE   ConsumerHighlightId = @ConsumerHighlightId

    IF ( @HighlightProviderId <> 1 AND @CurrentText <> @Text )
    BEGIN
	    RAISERROR('Vehicle History Consumer Highlights cannot have their Text values changed.',16,1)
	    RETURN @@ERROR
    END

    IF @Rank IS NULL
    BEGIN
	RAISERROR (50100,16,1,'Rank')
	RETURN @@ERROR
    END

    IF @IsDisplayed IS NULL
    BEGIN
	RAISERROR (50100,16,1,'IsDisplayed')
	RETURN @@ERROR
    END

    DECLARE @UpdateUserID INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @UpdateUser, @UpdateUserID OUTPUT

    -- Update
    UPDATE  Marketing.ConsumerHighlight
    SET     Text = @Text,
	    Rank = @Rank,
	    IsDisplayed = @IsDisplayed,
	    UpdateUser = @UpdateUserID,
	    UpdateDate = GETDATE(),
        ProviderSourceRowId = @ProviderSourceRowId
    WHERE   ConsumerHighlightId = @ConsumerHighlightId

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
