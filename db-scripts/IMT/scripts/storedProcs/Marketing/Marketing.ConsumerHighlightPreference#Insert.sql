IF OBJECT_ID(N'[Marketing].[ConsumerHighlightPreference#Insert]') IS NOT NULL
	DROP PROCEDURE [Marketing].[ConsumerHighlightPreference#Insert]
GO

CREATE PROCEDURE [Marketing].[ConsumerHighlightPreference#Insert]    
	@OwnerHandle varchar(36),
	@MinimumJDPowerCircleRating float,
	@MaxVHRItemsInitialDisplay tinyint,
    @InsertUser varchar(50)
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Marketing.ConsumerHighlightPreference table.
 * 
 *	Parameters: 
 *	@OwnerHandle
 *  @MinimumJDPowerCircleRating 
 *  @InsertUser
 *
 * Exceptions
 * ----------
 *  
 *  50100 - @IsDisplayed is null 
 *
 * History
 * ----------
 * 
 *  CGC 04/07/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

IF @MinimumJDPowerCircleRating IS NULL
    BEGIN
	RAISERROR (50100,16,1,'MinimumJDPowerCircleRating')
	RETURN @@ERROR
    END    

DECLARE @InsertUserID INT, @err INT
EXEC @err = dbo.ValidateParameter_MemberID#UserName @InsertUser, @InsertUserID OUTPUT
IF @err <> 0 GOTO Failed

DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO [Marketing].[ConsumerHighlightPreference] (    
	[OwnerId],
	[MinimumJDPowerCircleRating],	
	[MaxVHRItemsInitialDisplay],
	[InsertUserId],
	[InsertDate]
) VALUES (    
	@OwnerId,
	@MinimumJDPowerCircleRating,
	@MaxVHRItemsInitialDisplay,	
	@InsertUserId,
    getdate()
)

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
