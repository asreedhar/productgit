IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[GetListPriceHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[GetListPriceHistory]


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[GetListPriceHistory]
    @OwnerHandle VARCHAR(36),
    @VehicleHandle VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * Returns list price history for an inventory item
 * 
 * Input Parameters
 * ----------
 *
 * @OwnerHandle
 * @VehicleHandle
 * 
 * Output Parameters
 * ----------
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle IS NULL
 * 50100 - @VehicleHandle IS NULL
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @OwnerHandle IS NULL
    BEGIN
	RAISERROR (50100,16,1,'OwnerHandle')
	RETURN @@ERROR
    END

    IF @VehicleHandle IS NULL
    BEGIN
	RAISERROR (50100,16,1,'VehicleHandle')
	RETURN @@ERROR
    END

    DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	    @VehicleEntityTypeID INT, @VehicleEntityID INT

    EXEC Market.Pricing.ValidateParameter_OwnerHandle @OwnerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT

    EXEC Market.Pricing.ValidateParameter_VehicleHandle @VehicleHandle, @VehicleEntityTypeID OUT, @VehicleEntityID OUT

    DECLARE @ListPriceHistory TABLE (source VARCHAR(3), date DATETIME, price INT)

    IF @VehicleEntityTypeID = 1
    BEGIN
	-- Load list price history for inventory items
	INSERT INTO @ListPriceHistory( source, date, price )
	SELECT	'LPH',	DMSReferenceDT,	CONVERT(DECIMAL(9,2), ListPrice)
	FROM IMT.dbo.Inventory_ListPriceHistory
	WHERE InventoryID = @VehicleEntityID
	AND ListPrice <> 0
	UNION
	SELECT 'AIP',CONVERT(DATETIME, BeginDate, 112), Value
	FROM IMT.dbo.AIP_Event
	WHERE	AIP_EventTypeID = 5 AND Value <> 0 AND InventoryID = @VehicleEntityID
    END
    
    SELECT * FROM @ListPriceHistory

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
