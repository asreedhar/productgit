
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[Equipment#Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[Equipment#Update]
GO

CREATE PROCEDURE Marketing.Equipment#Update
        @EquipmentListID        INT,
        @EquipmentID            INT,
        @Name                   VARCHAR(80),
        @Value                  INT,
        @Selected               BIT,
        @Position               INT,
        @UpdateUser             VARCHAR(80),
        @Version                BINARY(8),
        @NewVersion             BINARY(8) OUTPUT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.Equipment#Update.PRC,v 1.1.4.1.2.2 2010/06/02 18:40:28 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Update an equipment item in the supplied list
 *
 * Parameters
 * ----------
 * 
 * PARAMETERS
 * 
 * Exceptions
 * ----------
 *
 * EXCEPTIONS
 *
 * History
 * ----------
 * 
 * SBW 01/05/2010 First Revision.
 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

EXEC @err = Marketing.ValidateParameter_EquipmentList @EquipmentListID
IF (@err <> 0) GOTO Failed

EXEC @err = Marketing.ValidateParameter_Equipment @EquipmentListID, @EquipmentID, @Version
IF (@err <> 0) GOTO Failed

DECLARE @UpdateUserID INT

EXEC @err = dbo.ValidateParameter_MemberID#UserName @UpdateUser, @UpdateUserID OUTPUT
IF (@err <> 0) GOTO Failed

IF (@Name IS NULL OR LEN(@Name) = 0)
BEGIN
        RAISERROR(50100,16,1,'Name')
        RETURN @@ERROR
END

IF (@Selected IS NULL)
BEGIN
        RAISERROR(50100,16,1,'Selected')
        RETURN @@ERROR
END

IF (@Position IS NULL)
BEGIN
        RAISERROR(50100,16,1,'Position')
        RETURN @@ERROR
END

IF (NOT EXISTS(
        SELECT  1
        FROM    Marketing.Equipment
        WHERE   EquipmentListID = @EquipmentListID
        AND     EquipmentID = @EquipmentID
        AND     Version = @Version))
BEGIN
        RAISERROR('Concurrent Modification',16,1)
        RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

-- NO RESULTS

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

UPDATE [Marketing].[Equipment]
SET     Name = @Name,
        Value = @Value,
        Selected = @Selected,
        Position = @Position,
        UpdateUser = @UpdateUserID,
        UpdateDate = GETDATE()
WHERE   EquipmentListID = @EquipmentListID
AND     EquipmentID = @EquipmentID
AND     Version = @Version

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF (@rc <> 1)
BEGIN
        RAISERROR(50200,16,1,@rc)
        RETURN @@ERROR
END

SELECT  @NewVersion = Version
FROM    [Marketing].[Equipment]
WHERE   EquipmentID = @EquipmentID

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO

GRANT EXECUTE, VIEW DEFINITION on Marketing.Equipment#Update to [MarketingUser]
GO
