IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[DealerGeneralPreference#Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[DealerGeneralPreference#Update]



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[DealerGeneralPreference#Update]
    @OwnerHandle VARCHAR(36),
	@AddressLine1 VARCHAR(500),
	@AddressLine2 VARCHAR(500),
	@City VARCHAR(500),
	@DisplayContactInfoOnWebPDF BIT,
	@StateCode VARCHAR(2),
	@ZipCode VARCHAR(5),
	@Url VARCHAR(500),
	@SalesEmailAddress VARCHAR(500),
	@SalesPhoneNumber VARCHAR(14),
	@ExtendedTagLine VARCHAR(2000),
	@DaysValidFor INT,
	@Disclaimer VARCHAR(500),    
	@UpdateUser VARCHAR(80)
AS

/* --------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * Updates a Dealer General Preference
 *  
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle does not exists
 * 50110 - No row found with primary key @OwnerID
 *
 * -------------------------------------------------------------------- */

BEGIN TRY

	DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

	-- these calls will raise the appropriate errors if the handles are invalid.
	exec [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out

	DECLARE @UpdateUserID INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @UpdateUser, @UpdateUserID OUTPUT

    IF NOT EXISTS (SELECT 1 FROM Marketing.DealerGeneralPreference WHERE OwnerId = @OwnerID)
    BEGIN
		RAISERROR (50110,16,1,'DealerGeneralPreference', @OwnerID)
		RETURN @@ERROR
    END
     
    Update Marketing.DealerGeneralPreference
	Set 
		AddressLine1 = @AddressLine1,
		AddressLine2 = @AddressLine2,
		City = @City,
		DisplayContactInfoOnWebPDF = @DisplayContactInfoOnWebPDF,
		StateCode = @StateCode,
		ZipCode = @ZipCode,
		Url = @Url,
		SalesEmailAddress = @SalesEmailAddress,
		SalesPhoneNumber = @SalesPhoneNumber,
		ExtendedTagLine = @ExtendedTagLine,
		DaysValidFor = @DaysValidFor,
		Disclaimer = @Disclaimer,    	
	    UpdateUser = @UpdateUserID,
	    UpdateDate = GetDate()
	Where
		OwnerID = @OwnerID AND
		(
			-- Something must change for us to do an update.
			AddressLine1 != @AddressLine1 OR
			AddressLine2 != @AddressLine2 OR
			City != @City OR
			DisplayContactInfoOnWebPDF != @DisplayContactInfoOnWebPDF OR
			StateCode != @StateCode OR
			ZipCode != @ZipCode OR
			Url != @Url OR
			SalesEmailAddress != @SalesEmailAddress OR
			SalesPhoneNumber != @SalesPhoneNumber OR
			ExtendedTagLine != @ExtendedTagLine OR
			DaysValidFor != @DaysValidFor OR
			Disclaimer != @Disclaimer 
		)    

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
