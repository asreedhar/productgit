IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'GetInventoryID' AND SCHEMA_NAME(schema_id) = 'Marketing')
	BEGIN
		DROP  Procedure  [Marketing].[GetInventoryID]
	END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [Marketing].[GetInventoryID]
        @businessUnitID INT,
        @vin VARCHAR(17)
AS


SELECT	I.InventoryID, I.InventoryType
FROM	FLDW.dbo.InventoryActive I
JOIN	IMT.dbo.tbl_Vehicle V ON I.VehicleID = V.VehicleID AND V.Vin = @vin
WHERE	I.BusinessUnitID = @businessUnitID

