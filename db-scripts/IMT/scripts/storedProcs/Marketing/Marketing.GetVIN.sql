
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[GetVIN]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[GetVIN]
GO

CREATE PROCEDURE Marketing.GetVIN
        @VehicleHandle	VARCHAR(36),
        @VIN		VARCHAR(17) OUT
AS

/* --------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * Takes a VehicleHandle, returns the vehicle's VIN.
 *
 * Parameters
 * ----------
 * 
 * @VehicleHandle       - string that logically encodes the vehicle entity type-id and id
 * @VIN	(output)		- the vehicle's VIN
 * 
 * Exceptions
 * ----------
 *
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 *
 * History
 * ----------
 * 
 * DGH  04/01/2010	First Revision.
 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

DECLARE	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Market].[Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed


    IF @VehicleEntityTypeID IN (1,4)
    BEGIN
	SELECT  @VIN = Vin
	FROM    dbo.Inventory i
	JOIN    dbo.tbl_Vehicle v
	    ON  v.VehicleID = i.VehicleID
	WHERE   i.InventoryID = @VehicleEntityID
    END
    ELSE IF @VehicleEntityTypeID IN (2)
    BEGIN
	SELECT  @VIN = Vin
	FROM    dbo.Appraisals a
	JOIN    dbo.tbl_Vehicle v
	    ON  v.VehicleID = a.VehicleID
	WHERE   a.AppraisalID = @VehicleEntityID
    END

SELECT @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO