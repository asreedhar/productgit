IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[GetVehicleSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[GetVehicleSummary]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[GetVehicleSummary]
	@VehicleCatalogID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.VehicleSummary.GetVehicleSummary.sql,v 1.1.4.1.2.2 2010/06/02 18:40:27 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Returns the vehicle catalog record for a given catalog id.
 * 
 * Parameters
 * ----------
 *
 * Exceptions
 * ----------
 * 
 * History
 * ------- 
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

BEGIN TRY

	Select *
	From [VehicleCatalog].[Firstlook].[VehicleCatalog]
	WHERE VehicleCatalogID = @VehicleCatalogID

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH