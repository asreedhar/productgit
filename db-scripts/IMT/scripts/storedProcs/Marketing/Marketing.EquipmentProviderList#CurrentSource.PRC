
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[EquipmentProviderList#CurrentSource]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[EquipmentProviderList#CurrentSource]
GO

CREATE PROCEDURE Marketing.EquipmentProviderList#CurrentSource
        @OwnerHandle            VARCHAR(36),
        @VehicleHandle          VARCHAR(36),
        @EquipmentProviderID    INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.EquipmentProviderList#CurrentSource.PRC,v 1.1.4.1.2.3 2010/06/03 19:42:39 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Fetch the current 'source' entry for the given vehicle and equipment
 * provider.  The returned value is compared to the last known value.
 * If greater than the last known value the application tier will request
 * the full list.  The purpose of this procedure is to short-circuit calls
 * to the expensive list fetch procedure.
 *
 * Parameters
 * ----------
 * 
 * @OwnerHandle         - string that logically encodes the owner entity type-id and id
 * @VehicleHandle       - string that logically encodes the vehicle entity type-id and id
 * @EquipmentProviderID - id of the equipment provider
 * 
 * Exceptions
 * ----------
 *
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle record does not exist
 *
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle record does not exist
 *
 * 50100 - @EquipmentProviderID is null
 * 50106 - @EquipmentProviderID record does not exist
 *
 * 50??? - Owner not associated with the equipment provider
 * 50200 - Expected one row but got n
 *
 * History
 * ----------
 * 
 * DGH  04/01/2010 SA/CVA phase 2 - implemented Edmunds TMV and KBB Consumer Guide
 * SBW  01/04/2010	First Revision.
 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

EXEC @err = [Market].[Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

EXEC @err = [Marketing].[ValidateParameter_EquipmentProvider] @EquipmentProviderID
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
        EquipmentProviderID     INT     NOT NULL,
        IdentityValue           BIGINT  NOT NULL
)

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

IF (EXISTS (
        SELECT  1
        FROM    Marketing.EquipmentProvider
        WHERE   EquipmentProviderID = @EquipmentProviderID
        AND     ThirdPartyID IS NOT NULL))
BEGIN
        
        DECLARE @ThirdPartyID TINYINT
        
        SELECT  @ThirdPartyID = ThirdPartyID
        FROM    Marketing.EquipmentProvider
        WHERE   EquipmentProviderID = @EquipmentProviderID

        IF (@VehicleEntityTypeID IN (1,4))
        BEGIN
                -- inventory bookout
                ; WITH AllBookouts (BookoutID, DateCreated) AS
	        (
		        SELECT	B.BookoutID, B.DateCreated
		        FROM	dbo.InventoryBookouts IB
		        JOIN	dbo.Bookouts B ON IB.BookoutID = B.BookoutID
		        WHERE	IB.InventoryID = @VehicleEntityID
		        AND	B.ThirdPartyID = @ThirdPartyID
	        ),
	        MostRecentBookout (BookoutID) AS
	        (
		        SELECT	BookoutID
		        FROM	AllBookouts AB
		        JOIN	(
                                        SELECT	MAX(DateCreated) DateCreated
				        FROM	AllBookouts
			        ) MB ON MB.DateCreated = AB.DateCreated
	        )
                INSERT INTO @Results (EquipmentProviderID, IdentityValue)
                SELECT  @EquipmentProviderID, BookoutID 
                FROM    MostRecentBookout

        END
        
        IF (@VehicleEntityTypeID = 2)
        BEGIN
                -- appraisal bookout
                ; WITH AllBookouts (BookoutID, DateCreated) AS
	        (
		        SELECT	B.BookoutID, B.DateCreated
		        FROM	dbo.AppraisalBookouts AB
		        JOIN	dbo.Bookouts B ON AB.BookoutID = B.BookoutID
		        WHERE	AB.AppraisalID = @VehicleEntityID
		        AND	B.ThirdPartyID = @ThirdPartyID
	        ),
	        MostRecentBookout (BookoutID) AS
	        (
		        SELECT	BookoutID
		        FROM	AllBookouts AB
		        JOIN	(
                                        SELECT	MAX(DateCreated) DateCreated
				        FROM	AllBookouts
			        ) MB ON MB.DateCreated = AB.DateCreated
	        )
                INSERT INTO @Results (EquipmentProviderID, IdentityValue)
                SELECT  @EquipmentProviderID, BookoutID 
                FROM    MostRecentBookout

        END

END


IF NOT EXISTS (SELECT 1 FROM @Results)
BEGIN
    
    -- We need the vin, so get it from the vehicle handle (VehicleEntityTypeId, VehicleEntityId)
    DECLARE @VIN VARCHAR(17)

    IF @VehicleEntityTypeID IN (1,4)
    BEGIN
	SELECT @VIN = Vin
	FROM dbo.Inventory i
	JOIN dbo.tbl_Vehicle v
	    ON v.VehicleID = i.VehicleID
	WHERE i.BusinessUnitID = @OwnerEntityID
	AND i.InventoryID = @VehicleEntityID
    END
    ELSE IF @VehicleEntityTypeID IN (2)
    BEGIN
	SELECT @VIN = Vin
	FROM dbo.Appraisals a
	JOIN dbo.tbl_Vehicle v
	    ON v.VehicleID = a.VehicleID
	WHERE a.BusinessUnitID = @OwnerEntityID
	AND a.AppraisalID = @VehicleEntityID
    END

    -- KBB Consumer Guide
    IF @EquipmentProviderID = 3
    -- KBB is not a guidebook for this dealer:
    AND NOT EXISTS (
	SELECT 1 
	FROM dbo.DealerPreference 
	WHERE BusinessUnitID = @OwnerEntityID 
	AND ( GuideBookID = @ThirdPartyID OR GuideBook2Id = @ThirdPartyID )
	)
    BEGIN

	INSERT INTO @Results (EquipmentProviderID, IdentityValue)
	SELECT @EquipmentProviderID, CAST(vbas.DateUpdated AS BINARY(8))
	FROM dbo.VehicleBookoutState vbs
	JOIN dbo.VehicleBookoutAdditionalState vbas
	    ON vbs.VehicleBookoutStateID = vbas.VehicleBookoutStateID
	JOIN dbo.VehicleBookoutStateThirdPartyVehicles vbstpv
	    ON vbstpv.VehicleBookoutStateID = vbs.VehicleBookoutStateID
	JOIN dbo.ThirdPartyVehicles tpv
	    ON tpv.ThirdPartyVehicleID = vbstpv.ThirdPartyVehicleID
	WHERE vbs.BusinessUnitID = @OwnerEntityID
	AND vbs.VIN = @VIN
	AND tpv.ThirdPartyID = @ThirdPartyID

    END

    -- Edmunds TMV:
    IF (@EquipmentProviderID = 5 AND @VehicleEntityTypeID IN (1,4,2) )
    BEGIN
	-- This should work the same for both inventory items and appraisals.
	INSERT INTO @Results (EquipmentProviderID, IdentityValue)
	SELECT @EquipmentProviderID, RowVersion
	FROM Market.Pricing.TmvInputRecord
	WHERE OwnerId = @OwnerID
	AND VehicleEntityID = @VehicleEntityID
	AND VehicleEntityTypeID = @VehicleEntityTypeID
    END
    
END



--IF (@EquipmentProviderID IN (6,7,8))
--BEGIN

        -- TODO: Alter Market.Listing.VehicleOption_Interface table adding a
        -- ROWVERSION column.  Add a ROWVERSION column to Market.Listing.Vehicle
        -- also (called OptionVersion) populating it with the row version value
        -- from the interface table.  The identity value will be from the
        -- Listing.Vehicle.OptionVersion column.

--END

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO

GRANT EXECUTE, VIEW DEFINITION on Marketing.EquipmentProviderList#CurrentSource to [MarketingUser]
GO

--EXECUTE Marketing.EquipmentProviderList#CurrentSource 'C5B4C338-B5C3-DC11-9377-0014221831B0','112921171', 1
