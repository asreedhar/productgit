IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[CertifiedVehicleBenefitLineItem#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[CertifiedVehicleBenefitLineItem#Fetch]


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[CertifiedVehicleBenefitLineItem#Fetch]
	@CertifiedVehicleBenefitPreferenceID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.CertifiedVehicleBenefitLineItem#Fetch.sql,v 1.1.4.1.2.2 2010/06/02 18:40:27 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Returns benefit line items by preference id
 * 
 * Parameters
 * ----------
 *
 * @CertifiedVehicleBenefitPreferenceID
 *
 * Exceptions
 * ----------
 * 
 * None
 *
 * History
 * ------- 
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

BEGIN TRY
 
    SELECT  li.*
    FROM	Marketing.CertifiedVehicleBenefitLineItem li
    WHERE   li.CertifiedVehicleBenefitPreferenceID = @CertifiedVehicleBenefitPreferenceID
	
END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH


 
 