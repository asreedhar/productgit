IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[DealerGeneralPreference#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[DealerGeneralPreference#Fetch]



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[DealerGeneralPreference#Fetch]
	@OwnerHandle varchar(36) 
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.DealerGeneralPreference#Fetch.sql,v 1.1.4.1.2.2 2010/06/02 18:40:27 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Returns a dealer general preference by owner handle
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle does not exists
 * 50106 - No preference record found for specified owner.
 *
 * History
 * ------- 
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

BEGIN TRY

	--------------------------------------------------------------------------------------------
	-- Validate Parameter Values
	--------------------------------------------------------------------------------------------

	DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

	-- these calls will raise the appropriate errors if the handles are invalid.
	exec [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out

    IF NOT EXISTS 
	(
		SELECT 1 FROM Marketing.DealerGeneralPreference 
		WHERE 
			OwnerID = @OwnerID
	)
    BEGIN
	    RAISERROR (50106,16,1,'DealerGeneralPreference', @OwnerID  )
	    RETURN @@ERROR
    END

	Declare @Name Varchar(64)

	IF @OwnerEntityTypeID = 1
	BEGIN
		-- A dealer
		Select @Name = BusinessUnitShortName From IMT.dbo.BusinessUnit bu WHERE bu.BusinessUnitID = @OwnerEntityID
	END
	ELSE IF @OwnerEntityTypeID = 2
	BEGIN
		-- a seller
		Select @Name = Name From Market.Listing.Seller s WHERE s.SellerID = @OwnerEntityID
	END

    SELECT  p.*, @Name as Name, @OwnerHandle as OwnerHandle
    FROM	Marketing.DealerGeneralPreference p
    WHERE   p.OwnerID = @OwnerID  
	
END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH


 
 