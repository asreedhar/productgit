
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[MarketListingPreference#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[MarketListingPreference#Fetch]
GO

CREATE Procedure [Marketing].[MarketListingPreference#Fetch]
	@OwnerHandle VARCHAR(36)	
AS

/* --------------------------------------------------------------------
 * 
 * $Id:
 * 
 * Summary
 * -------
 * 
 * Fetches a MarketListingPreference according to the provided Owner
 * 
 * Input Parameters
 * ----------
 *
 * @OwnerHandle
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle IS NULL
 * -------------------------------------------------------------------- */



BEGIN TRY


-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

	IF @OwnerHandle IS NULL
    BEGIN
		RAISERROR (50100,16,1,'OwnerHandle')
		RETURN @@ERROR
    END   
    
	DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
    EXEC Market.Pricing.ValidateParameter_OwnerHandle @OwnerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT     

   SELECT
	mp.MarketListingPreferenceId,
	mp.IsDisplayed,	
	mp.MileageDeltaBelow,
	mp.MileageDeltaAbove,
	mp.PriceDeltaAbove,
	mp.PriceDeltaBelow
   FROM Marketing.MarketListingPreference mp
   WHERE mp.OwnerID = @OwnerID


END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH

GO
