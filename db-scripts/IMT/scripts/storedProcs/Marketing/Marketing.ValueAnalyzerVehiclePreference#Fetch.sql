
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[ValueAnalyzerVehiclePreference#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[ValueAnalyzerVehiclePreference#Fetch]
GO

CREATE PROCEDURE Marketing.ValueAnalyzerVehiclePreference#Fetch
        @OwnerHandle            VARCHAR(36),
        @VehicleHandle          VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * 
 * Summary
 * -------
 * 
 * Get the preference for displaying the Value Analyzer
 *
 * Parameters
 * ----------
 * 
 * @OwnerHandle            - the owner of the new list
 * @VehicleHandle          - the vehicle whose equipment the list enumerates
 * 
 * Exceptions
 * ----------
 *
 * 50100 - OwnerHandle IS NULL
 * 50106 - The supplied handle does not resolve a known owner
 * 50100 - VehicleHandle IS NULL
 * 50106 - The supplied handle does not resolve a known vehicle
 *
 * History
 * ----------
 * 
 * DGH 05/13/2010 First Revision.
 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT


DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

EXEC @err = [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

DECLARE	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Market].[Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Get the preference
------------------------------------------------------------------------------------------------
DECLARE @DisplayVehicleDescription BIT
DECLARE @UseLongMarketingListingLayout BIT
DECLARE @ShortUrl VARCHAR(MAX)
DECLARE @LongUrl VARCHAR(MAX)

SELECT  @DisplayVehicleDescription = DisplayVehicleDescription,
	@UseLongMarketingListingLayout = UseLongMarketingListingLayout,
	@ShortUrl = ShortUrl,
	@LongUrl = LongUrl
FROM    Marketing.ValueAnalyzerVehiclePreference
WHERE   OwnerID = @OwnerID
AND     VehicleEntityTypeID = @VehicleEntityTypeID
AND     VehicleEntityID = @VehicleEntityID

------------------------------------------------------------------------------------------------
-- Defaults, if something was null (no preference yet saved):
--	DisplayVehicleDescription	= true
--	UseLongMarketingListingLayout	= false
------------------------------------------------------------------------------------------------
SELECT 
    ISNULL(@DisplayVehicleDescription, 1) AS DisplayVehicleDescription, 
    ISNULL(@UseLongMarketingListingLayout, 0) AS UseLongMarketingListingLayout,
    ISNULL(@ShortUrl, '') AS ShortUrl,
    ISNULL(@LongUrl, '') AS LongUrl
    

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
