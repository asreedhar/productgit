IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[Marketing].[DealerGeneralPreference#Insert]')
                    AND type IN ( N'P', N'PC' ) )
    DROP PROCEDURE [Marketing].[DealerGeneralPreference#Insert]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[DealerGeneralPreference#Insert]
    @OwnerHandle VARCHAR(36)
  , @AddressLine1 VARCHAR(500)
  , @AddressLine2 VARCHAR(500)
  , @City VARCHAR(500)
  , @DisplayContactInfoOnWebPDF BIT
  , @StateCode VARCHAR(2)
  , @ZipCode VARCHAR(5)
  , @Url VARCHAR(500)
  , @SalesEmailAddress VARCHAR(500)
  , @SalesPhoneNumber VARCHAR(14)
  , @ExtendedTagLine VARCHAR(2000)
  , @DaysValidFor INT
  , @Disclaimer VARCHAR(500)
  , @InsertUser VARCHAR(80)
AS /* --------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * Inserts a Dealer General Preference
 *  
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle does not exists
 *
 * -------------------------------------------------------------------- */

    BEGIN TRY

        DECLARE @OwnerEntityTypeID INT
          , @OwnerEntityID INT
          , @OwnerID INT

	-- these calls will raise the appropriate errors if the handles are invalid.
        EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT

        DECLARE @InsertUserID INT
        EXEC dbo.ValidateParameter_MemberID#UserName @InsertUser, @InsertUserID OUTPUT
     
        INSERT  INTO Marketing.DealerGeneralPreference
                ( OwnerID
                , AddressLine1
                , AddressLine2
                , City
                , DisplayContactInfoOnWebPDF
                , StateCode
                , ZipCode
                , Url
                , SalesEmailAddress
                , SalesPhoneNumber
                , ExtendedTagLine
                , DaysValidFor
                , Disclaimer
                , InsertUser
                , InsertDate
                )
        VALUES  ( @OwnerID
                , @AddressLine1
                , @AddressLine2
                , @City
                , @DisplayContactInfoOnWebPDF
                , @StateCode
                , @ZipCode
                , @Url
                , @SalesEmailAddress
                , @SalesPhoneNumber
                , @ExtendedTagLine
                , @DaysValidFor
                , @Disclaimer
                , @InsertUserID
                , GETDATE()
                )
    

    END TRY

    BEGIN CATCH
        EXEC dbo.sp_ErrorHandler
    END CATCH
GO

GRANT EXECUTE ON [Marketing].[DealerGeneralPreference#Insert] TO [merchandisingWebsite]