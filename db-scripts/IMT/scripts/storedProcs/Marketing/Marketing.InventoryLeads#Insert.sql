
IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'InventoryLeads#Insert' AND SCHEMA_NAME(schema_id) = 'Marketing')
	BEGIN
		DROP  Procedure  [Marketing].[InventoryLeads#Insert]
	END
GO

CREATE Procedure [Marketing].[InventoryLeads#Insert]
	@OwnerHandle VARCHAR(36),
	@vehicleHandle VARCHAR(36),
	@FirstName VARCHAR(50),
	@LastName VARCHAR(50),
	@Email VARCHAR(100),
	@PhoneNumber VARCHAR(15),
	@Address VARCHAR(50),
	@City VARCHAR(20),
	@State VARCHAR(20),
	@ZipCode VARCHAR(10)	
AS


BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Check for no nulls

    IF @OwnerHandle IS NULL
    BEGIN
	RAISERROR (50100,16,1,'OwnerHandle')
	RETURN @@ERROR
    END
    
  	IF @vehicleHandle IS NULL
    BEGIN
		RAISERROR (50100,16,1,'vehicleHandle')
		RETURN @@ERROR
    END  
    
   	DECLARE @VehicleEntityTypeID INT, @VehicleEntityID INT
	EXEC Market.Pricing.ValidateParameter_VehicleHandle @vehicleHandle, @VehicleEntityTypeID OUT, @VehicleEntityID OUT
    
  	DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
    EXEC Market.Pricing.ValidateParameter_OwnerHandle @OwnerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT     


    /*
    Insert the new vehicle preference
    */
    INSERT 
    INTO    Marketing.InventoryLeads
	    (
			VehicleEntityID,
			VehicleEntityTypeID,
			Ownerid,
			FirstName,
			LastName,
			Email,
			PhoneNumber,
			Address,
			City,
			State,
			ZipCode,
			InsertDate
	    )
    VALUES
	    (
	    	@VehicleEntityID,
			@VehicleEntityTypeID,
			@Ownerid,
			@FirstName,
			@LastName,
			@Email,
			@PhoneNumber,
			@Address,
			@City,
			@State,
			@ZipCode,
			GETDATE()
	    )    


END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH

GO
