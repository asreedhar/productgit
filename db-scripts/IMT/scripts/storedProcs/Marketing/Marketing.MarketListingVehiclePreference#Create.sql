USE IMT
GO


IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'MarketListingVehiclePreference#Create' AND SCHEMA_NAME(schema_id) = 'Marketing')
	BEGIN
		DROP  Procedure  [Marketing].[MarketListingVehiclePreference#Create]
	END

GO

-- Make sure we can get the row with the null OwnerID
SET ANSI_NULLS OFF
GO

CREATE Procedure [Marketing].[MarketListingVehiclePreference#Create]
	@vehicleHandle VARCHAR(36), @ownerHandle VARCHAR(36), @offerPrice INT

AS
/* --------------------------------------------------------------------
 * 
 * $Id:
 * 
 * Summary
 * -------
 * 
 * Fetches a default MarketListingVehiclePreference
 * 
 * Input Parameters
 * ----------
 *
 * @vehicleHandle
 * @OwnerHandle 
 * @offerPrice 
 * Output Parameters
 * ----------
 * 
 *
 * Exceptions
 *	50100 - Parameter IS NULL
 *  50500 - does not exist in table
 *	50600 Id already in use in table %s.
 * ----------
 * -------------------------------------------------------------------- */



BEGIN TRY

-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

	IF @vehicleHandle IS NULL
    BEGIN
		RAISERROR (50100,16,1,'vehicleHandle')
		RETURN @@ERROR
    END  
    
    
  	IF @ownerHandle IS NULL
    BEGIN
		RAISERROR (50100,16,1,'OwnerHandle')
		RETURN @@ERROR
    END
    
    
   	IF @offerPrice IS NULL
    BEGIN
		RAISERROR (50100,16,1,'offerPrice')
		RETURN @@ERROR
    END  


--Let's get the vehicle id
--this will throw an exception if there is no such vehicle
	DECLARE @VehicleEntityTypeID INT, @VehicleEntityID INT
	EXEC Market.Pricing.ValidateParameter_VehicleHandle @vehicleHandle, @VehicleEntityTypeID OUT, @VehicleEntityID OUT
	
--Make sure the @VehicleEntityTypeID is 5  or 1 - in other words - listing or inventory
--if that is the case @VehicleEntityID if the vehicle id.
--now we should join with Market.Listing.Vehicle to get the mileage.
	
	DECLARE @Mileage INT
	
		 
	IF @VehicleEntityTypeID = 1  --Inventory		
		 SELECT @Mileage =  MileageReceived FROM IMT.dbo.Inventory WITH (NOLOCK) WHERE InventoryID = @VehicleEntityID		
	 ELSE IF @VehicleEntityTypeID = 5 --Listing
		 SELECT @Mileage =  Mileage From Market.Listing.Vehicle WHERE VehicleID = @VehicleEntityID		
	 ELSE
		RAISERROR (50500,16,1,'MarketListingVehiclePreference')
	

--Let's get the owner id
--this will throw an exception if there is no such user
	DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
    EXEC Market.Pricing.ValidateParameter_OwnerHandle @ownerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT 

	IF EXISTS ( SELECT 1 FROM Marketing.MarketListingVehiclePreference WHERE VehicleEntityID = @VehicleEntityID AND  OwnerID = @OwnerID)
    BEGIN
	RAISERROR (50600,16,1,'MarketListingVehiclePreference')
	RETURN @@ERROR
    END
    
-- Check if we indeed have the preference for an owner

    IF EXISTS ( SELECT 1 FROM Marketing.MarketListingPreference WHERE OwnerID = @OwnerID)
		BEGIN
	--Fetch the information from the Marketing.MarketListingPreference according to the owner
		   SELECT
			@VehicleEntityID AS VehicleEntityID,
			@VehicleEntityTypeID AS VehicleEntityTypeID,
			@OwnerID AS OwnerID,
			mp.IsDisplayed,
			CAST(1 as bit) AS HasMarketListingPreference,
			@Mileage - mp.MileageDeltaBelow AS MileageLow,
			@Mileage + mp.MileageDeltaAbove AS MileageHigh,
			@offerPrice - mp.PriceDeltaBelow AS PriceLow,
			@offerPrice + mp.PriceDeltaAbove AS PriceHigh			   
		   FROM Marketing.MarketListingPreference mp
		   WHERE mp.OwnerID = @OwnerID
		END
    ELSE
    --Fetch the default
		BEGIN
		  SELECT	
			@VehicleEntityID AS VehicleEntityID,
			@VehicleEntityTypeID AS VehicleEntityTypeID,
			@OwnerID AS OwnerID,
			mp.IsDisplayed,
			CAST(0 as bit) AS HasMarketListingPreference,
			@Mileage - mp.MileageDeltaBelow AS MileageLow,
			@Mileage + mp.MileageDeltaAbove AS MileageHigh,
			@offerPrice - mp.PriceDeltaBelow AS PriceLow,
			@offerPrice + mp.PriceDeltaAbove AS PriceHigh			   
		 FROM Marketing.MarketListingPreference mp
		 WHERE mp.OwnerID IS null
		END
    


END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH

GO
