
IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'InventoryDocuments#Insert' AND SCHEMA_NAME(schema_id) = 'Marketing')
	BEGIN
		DROP  Procedure  [Marketing].[InventoryDocuments#Insert]
	END
GO

CREATE Procedure [Marketing].[InventoryDocuments#Insert]
	@vehicleHandle VARCHAR(36),
	@OwnerHandle VARCHAR(36),	
	@XmlDoc VARCHAR(MAX),
	@InsertUser VARCHAR(80),	
	
	@DocumentID INT OUTPUT

AS


BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Check for no nulls

    IF @OwnerHandle IS NULL
    BEGIN
	RAISERROR (50100,16,1,'OwnerHandle')
	RETURN @@ERROR
    END
    
  	IF @vehicleHandle IS NULL
    BEGIN
		RAISERROR (50100,16,1,'vehicleHandle')
		RETURN @@ERROR
    END  
    
    IF @InsertUser IS NULL
    BEGIN
	RAISERROR (50100,16,1,'InsertUser')
	RETURN @@ERROR
    END
    
    DECLARE @InsertUserId INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @InsertUser, @InsertUserId OUTPUT
    
   	DECLARE @VehicleEntityTypeID INT, @VehicleEntityID INT
	EXEC Market.Pricing.ValidateParameter_VehicleHandle @vehicleHandle, @VehicleEntityTypeID OUT, @VehicleEntityID OUT
    
  	DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
    EXEC Market.Pricing.ValidateParameter_OwnerHandle @OwnerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT     


    /*
    Insert the new vehicle preference
    */
    INSERT 
    INTO    Marketing.InventoryDocuments
	    (
			VehicleEntityID,
			VehicleEntityTypeID,
			Ownerid,
			DocumentXml,
			InsertUserId,
			InsertDate,
			UpdateUserId,
			UpdateDate
	    )
    VALUES
	    (
	    	@VehicleEntityID,
			@VehicleEntityTypeID,
			@Ownerid,
			@XmlDoc,
			@InsertUserId,
			GETDATE(),
			@InsertUserId, --When we insert the insert user is also the update user
			GETDATE()
	    )
    
    SELECT @DocumentID = SCOPE_IDENTITY()


END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH

GO
