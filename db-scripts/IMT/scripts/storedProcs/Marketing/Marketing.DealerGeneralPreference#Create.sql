IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[DealerGeneralPreference#Create]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[DealerGeneralPreference#Create]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [Marketing].[DealerGeneralPreference#Create]
	@OwnerHandle varchar(36) 
AS

/* --------------------------------------------------------------------
 * 
 * $Id:
 * 
 * Summary
 * -------
 * 
 * Fetches a default DealerGeneralPreference
 * 
 * Input Parameters
 * ----------
 * @OwnerHandle 
 * ----------
 * 
 *
 * Exceptions
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle does not exists
 * ------- 
 * -------------------------------------------------------------------- */

BEGIN TRY

	DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

	-- these calls will raise the appropriate errors if the handles are invalid.
	EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out

	DECLARE @Results TABLE
	(
		Name VARCHAR(64),
		Address1 VARCHAR(100),
		Address2 VARCHAR(100),
		City VARCHAR(50),
		State VARCHAR(3),
		ZipCode VARCHAR(10),
		PhoneNumber	VARCHAR(50)
	)

	IF @OwnerEntityTypeID = 1
		BEGIN
			-- A dealer
			INSERT INTO @Results (Name, Address1, Address2, City, State, ZipCode, PhoneNumber)
			SELECT BusinessUnitShortName, Address1, Address2, City, State, ZipCode, OfficePhone
			From IMT.dbo.BusinessUnit bu
			WHERE bu.BusinessUnitID = @OwnerEntityID
		END
	ELSE IF @OwnerEntityTypeID = 2
		BEGIN
			-- a seller
			INSERT INTO @Results (Name, Address1, Address2, City, State, ZipCode, PhoneNumber)
			SELECT Name, Address1, Address2, City, State, ZipCode, PhoneNumber
			From Market.Listing.Seller s
			WHERE s.SellerID = @OwnerEntityID
		END

	Select * FROM @Results

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
GO
GRANT EXECUTE ON [Marketing].[DealerGeneralPreference#Create] TO [merchandisingWebsite]