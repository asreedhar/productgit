IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[MarketAnalysisVehiclePreferencePriceProvider#Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[MarketAnalysisVehiclePreferencePriceProvider#Insert]


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[MarketAnalysisVehiclePreferencePriceProvider#Insert]
    @MarketAnalysisVehiclePreferenceID INT,
    @MarketAnalysisPriceProviderId INT,
    @InsertUser VARCHAR(80)
AS

/* --------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * Inserts a new market analysis vehicle preference price provider
 * 
 * Input Parameters
 * ----------
 *
 * @MarketAnalysisVehiclePreferenceID
 * @MarketAnalysisPriceProviderId
 * @InsertUser
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - MarketAnalysisVehiclePreferenceID IS NULL
 * 50100 - MarketAnalysisPriceProviderId IS NULL
 * 50100 - InsertUser IS NULL
 * 50106 - MarketAnalysisVehiclePreference row does not exist
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @MarketAnalysisVehiclePreferenceID IS NULL
    BEGIN
        RAISERROR (50100,16,1,'MarketAnalysisVehiclePreferenceID')
        RETURN @@ERROR
    END

    IF @MarketAnalysisPriceProviderId IS NULL
    BEGIN
        RAISERROR (50100,16,1,'MarketAnalysisPriceProviderId')
        RETURN @@ERROR
    END

    IF @InsertUser IS NULL
    BEGIN
        RAISERROR (50100,16,1,'InsertUser')
        RETURN @@ERROR
    END

    DECLARE @InsertUserID INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @InsertUser, @InsertUserID OUTPUT

    IF NOT EXISTS (
	SELECT 1 
	FROM Marketing.MarketAnalysisVehiclePreference 
	WHERE MarketAnalysisVehiclePreferenceId = @MarketAnalysisVehiclePreferenceID
	)
    BEGIN
	RAISERROR (50106,16,1,'MarketAnalysisVehiclePreference', @MarketAnalysisVehiclePreferenceID )
	RETURN @@ERROR
    END
    
    IF NOT EXISTS (
	SELECT 1 
	FROM Marketing.MarketAnalysisPriceProvider 
	WHERE PriceProviderId = @MarketAnalysisPriceProviderId
	)
    BEGIN
	RAISERROR (50106,16,1,'MarketAnalysisPriceProvider', @MarketAnalysisPriceProviderId )
	RETURN @@ERROR
    END    

    INSERT
    INTO    Marketing.MarketAnalysisVehiclePreferencePriceProvider
	(	
	    MarketAnalysisVehiclePreferenceId,
	    MarketAnalysisPriceProviderId,
	    InsertUser,
	    InsertDate
	)
    VALUES
        (	
	    @MarketAnalysisVehiclePreferenceID,
	    @MarketAnalysisPriceProviderId,
	    @InsertUserID,
	    GETDATE()
	)

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
