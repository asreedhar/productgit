IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[MarketAnalysisVehiclePreference#Create]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[MarketAnalysisVehiclePreference#Create]


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[MarketAnalysisVehiclePreference#Create]
    @OwnerHandle VARCHAR(36),
    @VehicleHandle VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * Returns the default market analysis vehicle preference for the specified vehicle
 * 
 * Input Parameters
 * ----------
 *
 * @OwnerHandle
 * @VehicleHandle
 * 
 * Output Parameters
 * ----------
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle IS NULL
 * 50100 - @VehicleHandle IS NULL
 * 50113 - MarketAnalysisVehiclePreference record exists already
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @OwnerHandle IS NULL
    BEGIN
        RAISERROR (50100,16,1,'OwnerHandle')
        RETURN @@ERROR
    END

    IF @VehicleHandle IS NULL
    BEGIN
        RAISERROR (50100,16,1,'VehicleHandle')
        RETURN @@ERROR
    END
    
    DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
            @VehicleEntityTypeID INT, @VehicleEntityID INT

    EXEC Market.Pricing.ValidateParameter_OwnerHandle @OwnerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT

    EXEC Market.Pricing.ValidateParameter_VehicleHandle @VehicleHandle, @VehicleEntityTypeID OUT, @VehicleEntityID OUT

    IF EXISTS (
	SELECT 1 
	FROM Marketing.MarketAnalysisVehiclePreference 
	WHERE OwnerID = @OwnerID 
	AND VehicleEntityID = @VehicleEntityID
	AND VehicleEntityTypeId = @VehicleEntityTypeID
	)
    BEGIN
        RAISERROR (50113,16,1,'MarketAnalysisVehiclePreference')
        RETURN @@ERROR
    END

    -- return the vehicle preference
    SELECT
	    0 AS MarketAnalysisVehiclePreferenceId,
	    @OwnerHandle as OwnerHandle,
	    @VehicleHandle as VehicleHandle,
	    CAST(1 AS BIT) AS IsDisplayed,
            CAST(1 AS BIT) AS ShowPricingGauge,
            CAST(0 AS BIT) AS HasOverriddenPriceProviders

    
END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
