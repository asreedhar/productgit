IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[DealerGeneralPreference#Exists]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[DealerGeneralPreference#Exists]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[DealerGeneralPreference#Exists]
    @OwnerHandle VARCHAR(36) 

AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.DealerGeneralPreference#Exists.sql,v 1.1.4.1.2.2 2010/06/02 18:40:28 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Returns a value indicating whether or not the DealerGeneralPreference exists, by owner handle.
 * 
 * Input Parameters
 * ----------
 *
 * @OwnerHandle
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle does not exists
 * -------------------------------------------------------------------- */

BEGIN TRY

	--------------------------------------------------------------------------------------------
	-- Validate Parameter Values
	--------------------------------------------------------------------------------------------

	DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT

	-- these calls will raise the appropriate errors if the handles are invalid.
	exec [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out

    IF NOT EXISTS 
	(
		SELECT 1 FROM Marketing.DealerGeneralPreference
		WHERE 
			OwnerID = @OwnerID
	)
    BEGIN
	    RETURN 0
    END

	RETURN 1

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
