IF OBJECT_ID(N'[Marketing].[VehicleHistorySourcePreference#Fetch]') IS NOT NULL
	DROP PROCEDURE [Marketing].[VehicleHistorySourcePreference#Fetch]
GO

CREATE PROCEDURE [Marketing].[VehicleHistorySourcePreference#Fetch]
	@OwnerHandle varchar(36)
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch all records from the Marketing.VehicleHistoryReportSourcePreference table for the given owner.
 * 
 *	Parameters:
 *	@OwnerHandle
 *
 * History
 * ----------
 * 
 *  kmoeller 09/20/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out

DECLARE @ownerPref TABLE(riid TINYINT, pid TINYINT, rid TINYINT, vhrpid int, [desc] varchar(100), [rank] tinyint, isdisplayed bit)

DECLARE @sysPref TABLE(riid TINYINT, pid TINYINT, rid TINYINT, vhrpid int, [desc] varchar(100), [rank] tinyint, isdisplayed bit)

INSERT INTO @ownerPref( riid, pid, rid, vhrpid, [desc], rank, isdisplayed )
SELECT 
    sp.VehicleHistoryReportInspectionId,
    ri.VehicleHistoryProviderId,
	COALESCE(ri.CarfaxReportInspectionId, ri.AutocheckReportInspectionId),
	sp.VehicleHistoryReportPreferenceId,
    vhp.Name + ':' + COALESCE(afi.Description, cfi.Description),
    sp.Rank,
    sp.IsDisplayed
FROM marketing.VehicleHistoryReportPreference op
JOIN marketing.VehicleHistoryReportSourcePreference sp ON op.VehicleHistoryReportPreferenceId = sp.VehicleHistoryReportPreferenceId
JOIN marketing.VehicleHistoryReportInspection ri ON ri.VehicleHistoryReportInspectionId = sp.VehicleHistoryReportInspectionId
JOIN marketing.VehicleHistoryProvider vhp ON vhp.VehicleHistoryProviderId = ri.VehicleHistoryProviderId
LEFT JOIN Carfax.ReportInspection cfi ON cfi.ReportInspectionID = ri.CarfaxReportInspectionId
LEFT JOIN AutoCheck.ReportInspection afi ON afi.ReportInspectionID = ri.AutocheckReportInspectionId
WHERE op.OwnerId = @OwnerId

INSERT INTO @sysPref( riid, pid, rid, vhrpid, [desc], rank, isdisplayed )
SELECT 
    sp.VehicleHistoryReportInspectionId,
    ri.VehicleHistoryProviderId,
	COALESCE(ri.CarfaxReportInspectionId, ri.AutocheckReportInspectionId),
	sp.VehicleHistoryReportPreferenceId,
    vhp.Name + ':' + COALESCE(afi.Description, cfi.Description),
    sp.Rank,
    sp.IsDisplayed
FROM marketing.VehicleHistoryReportPreference op
JOIN marketing.VehicleHistoryReportSourcePreference sp ON op.VehicleHistoryReportPreferenceId = sp.VehicleHistoryReportPreferenceId
JOIN marketing.VehicleHistoryReportInspection ri ON ri.VehicleHistoryReportInspectionId = sp.VehicleHistoryReportInspectionId
JOIN marketing.VehicleHistoryProvider vhp ON vhp.VehicleHistoryProviderId = ri.VehicleHistoryProviderId
LEFT JOIN Carfax.ReportInspection cfi ON cfi.ReportInspectionID = ri.CarfaxReportInspectionId
LEFT JOIN AutoCheck.ReportInspection afi ON afi.ReportInspectionID = ri.AutocheckReportInspectionId
WHERE op.OwnerId IS NULL

-- return the shown (isdisplay=1) rows as the first result set
SELECT 
    VehicleHistoryReportInspectionId = sp.riid,
    VehicleHistoryProviderId = sp.pid,
	ReportInspectionId = sp.rid,
    [Description] = sp.[desc],
    VehicleHistoryReportPreferenceId = COALESCE(op.vhrpid, sp.vhrpid),
    IsDisplayed = COALESCE(op.isdisplayed, sp.isdisplayed)
FROM @sysPref sp 
LEFT JOIN @ownerPref op ON sp.riid = op.riid
WHERE COALESCE(op.isdisplayed, sp.isdisplayed) = 1
ORDER BY COALESCE(op.rank, sp.rank)

-- return the hidden (isdisplay=0) rows as the second result set
SELECT 
    VehicleHistoryReportInspectionId = sp.riid,
    VehicleHistoryProviderId = sp.pid,
	ReportInspectionId = sp.rid,
    [Description] = sp.[desc],
    VehicleHistoryReportPreferenceId = COALESCE(op.vhrpid, sp.vhrpid),
    IsDisplayed = COALESCE(op.isdisplayed, sp.isdisplayed)
FROM @sysPref sp 
LEFT JOIN @ownerPref op ON sp.riid = op.riid
WHERE COALESCE(op.isdisplayed, sp.isdisplayed) = 0
ORDER BY COALESCE(op.rank, sp.rank)




