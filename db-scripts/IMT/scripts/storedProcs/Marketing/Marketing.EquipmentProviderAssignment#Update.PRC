
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[EquipmentProviderAssignment#Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[EquipmentProviderAssignment#Update]
GO

CREATE PROCEDURE Marketing.EquipmentProviderAssignment#Update
        @EquipmentProviderAssignmentListID      INT,
        @EquipmentProviderID                    INT,
        @IsDefault                              BIT,
        @Position                               INT,
        @UpdateUser                             VARCHAR(80),
        @Version                                BINARY(8),
        @NewVersion                             BINARY(8) OUTPUT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.EquipmentProviderAssignment#Update.PRC,v 1.1.4.1.2.2 2010/06/02 18:40:27 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Update an equipment provider assignment
 *
 * Parameters
 * ----------
 * 
 * @EquipmentProviderAssignmentListID - the list to which the supplied
 *                        equipment provider is assigned
 * @EquipmentProviderID - the equipment provider being modified
 * @IsDefault - whether the equipment provider is the lists default entry
 * @Position - list index of the equipment provider
 * @UpdateUser - identity of the user making the update
 * @Version - client copy of the records row version
 * @NewVersion - updated row version of the record
 * 
 * Exceptions
 * ----------
 *
 * 50100 - @EquipmentProviderAssignmentListID IS NULL
 * 50106 - No such list with identifier @EquipmentProviderAssignmentListID 
 * 50100 - @EquipmentProviderID is null
 * 50106 - @EquipmentProviderID record does not exist
 * 50100 - @IsDefault IS NULL
 * 50100 - @Position IS NULL
 * 50xxx - Concurrent modification
 * 50100 - @UpdateUser is null
 * 50106 - @UpdateUser does not exist
 *
 * History
 * ----------
 * 
 * SBW 01/05/2010 First Revision.
 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

EXEC @err = [Marketing].[ValidateParameter_EquipmentProviderAssignmentList] @EquipmentProviderAssignmentListID
IF @err <> 0 GOTO Failed

EXEC @err = [Marketing].[ValidateParameter_EquipmentProvider] @EquipmentProviderID
IF @err <> 0 GOTO Failed

DECLARE @UpdateUserID INT

EXEC @err = dbo.ValidateParameter_MemberID#UserName @UpdateUser, @UpdateUserID OUTPUT
IF (@err <> 0) GOTO Failed

IF (@IsDefault IS NULL)
BEGIN
        RAISERROR(50100,16,1,'IsDefault')
        RETURN @@ERROR
END

IF (@Position IS NULL)
BEGIN
        RAISERROR(50100,16,1,'Position')
        RETURN @@ERROR
END

IF (NOT EXISTS(
        SELECT  1
        FROM    Marketing.EquipmentProviderAssignment
        WHERE   EquipmentProviderAssignmentListID = @EquipmentProviderAssignmentListID
        AND     EquipmentProviderID = @EquipmentProviderID))
BEGIN
        RAISERROR('Equipment provider not assigned to list',16,1)
        RETURN @@ERROR
END

IF (NOT EXISTS(
        SELECT  1
        FROM    Marketing.EquipmentProviderAssignment
        WHERE   EquipmentProviderAssignmentListID = @EquipmentProviderAssignmentListID
        AND     EquipmentProviderID = @EquipmentProviderID
        AND     Version = @Version))
BEGIN
        RAISERROR('Concurrent modification',16,1)
        RETURN @@ERROR
END

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

-- NO RESULTS

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

UPDATE  Marketing.EquipmentProviderAssignment
SET     IsDefault       = @IsDefault,
        Position        = @Position,
        UpdateUser      = @UpdateUserID,
        UpdateDate      = GETDATE()
WHERE   EquipmentProviderAssignmentListID = @EquipmentProviderAssignmentListID
AND     EquipmentProviderID = @EquipmentProviderID
AND     Version = @Version

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF @rc <> 1
BEGIN
        RAISERROR(50200,16,1)
        RETURN @@ERROR
END

SELECT  @NewVersion = Version
FROM    Marketing.EquipmentProviderAssignment
WHERE   EquipmentProviderAssignmentListID = @EquipmentProviderAssignmentListID
AND     EquipmentProviderID = @EquipmentProviderID

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO

GRANT EXECUTE, VIEW DEFINITION on Marketing.EquipmentProviderAssignment#Update to [MarketingUser]
GO
