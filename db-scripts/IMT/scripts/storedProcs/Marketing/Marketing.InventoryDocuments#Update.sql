USE IMT
GO

IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'InventoryDocuments#Update' AND SCHEMA_NAME(schema_id) = 'Marketing')
	BEGIN
		DROP  Procedure  [Marketing].[InventoryDocuments#Update]
	END

GO

CREATE Procedure [Marketing].[InventoryDocuments#Update]
	@DocumentID INT,
	@DocumentXml VARCHAR(MAX),
	@UpdateUser VARCHAR(80)
AS


BEGIN TRY

-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


--Check for no nulls

	IF @DocumentID IS NULL
    BEGIN
	RAISERROR (50100,16,1,'DocumentID')
	RETURN @@ERROR
    END
    
    IF @UpdateUser IS NULL
        BEGIN
		RAISERROR (50100,16,1,'UpdateUser')
		RETURN @@ERROR
    END    


	IF NOT EXISTS ( SELECT 1 FROM Marketing.InventoryDocuments WHERE DocumentID = @DocumentID)
    BEGIN
	RAISERROR (50106,16,1,'InventoryDocuments', @DocumentXml)
	RETURN @@ERROR
    END
    

--Get the update user id
    DECLARE @UpdateUserId INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @UpdateUser, @UpdateUserId OUTPUT    
	

UPDATE  Marketing.InventoryDocuments
    SET			
	    DocumentXml = @DocumentXml,
	    UpdateUserId = @UpdateUserId,
	    UpdateDate = GETDATE()
	    
    WHERE DocumentID = @DocumentID 

    -- Success
    RETURN 0

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
GO

