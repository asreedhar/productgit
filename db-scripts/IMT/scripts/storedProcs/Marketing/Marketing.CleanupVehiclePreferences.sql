IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[CleanupVehiclePreferences]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[CleanupVehiclePreferences]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [Marketing].[CleanupVehiclePreferences]
	@DeleteLow SMALLDATETIME,
	@DeleteHigh SMALLDATETIME
AS

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

DECLARE @Inactive table(InventoryID INT NOT NULL PRIMARY KEY)

INSERT @Inactive (InventoryID)
SELECT InventoryID
FROM dbo.Inventory
WHERE
	InventoryActive = 0
	AND DeleteDt BETWEEN @DeleteLow AND @DeleteHigh

DECLARE @InactiveBatch TABLE(InventoryID INT NOT NULL PRIMARY KEY)

WHILE EXISTS (SELECT * FROM @Inactive)
BEGIN -- loop

	INSERT @InactiveBatch( InventoryID )
	SELECT TOP(50000) InventoryID FROM @Inactive

-- Process the batch of inactive inventory items

	BEGIN TRAN

	BEGIN TRY

		-- blank comment lines below delineate relationships

		--

		DELETE t
		FROM
			Marketing.CertifiedVehicleBenefitLineItem t
			JOIN Marketing.CertifiedVehicleBenefitPreference p
				ON t.CertifiedVehicleBenefitPreferenceID = p.CertifiedVehicleBenefitPreferenceID
			JOIN @InactiveBatch i
				ON i.InventoryID = p.VehicleEntityID AND p.VehicleEntityTypeID = 1

		DELETE t
		FROM
			Marketing.CertifiedVehicleBenefitPreference t
			JOIN @InactiveBatch i
				ON i.InventoryID = t.VehicleEntityID AND t.VehicleEntityTypeID = 1

		--

		DELETE t
		FROM
			Marketing.ConsumerHighlight t
			JOIN Marketing.ConsumerHighlightVehiclePreference p
				ON t.ConsumerHighlightVehiclePreferenceId = p.ConsumerHighlightVehiclePreferenceId
			JOIN @InactiveBatch i
				ON i.InventoryID = p.VehicleEntityID AND p.VehicleEntityTypeID = 1
				
		DELETE t
		FROM
			Marketing.ConsumerHighlightVehiclePreference t
			JOIN @InactiveBatch i
				ON i.InventoryID = t.VehicleEntityID AND t.VehicleEntityTypeID = 1

		--

		DELETE t
		FROM
			Marketing.Equipment t
			JOIN Marketing.EquipmentList p
				ON t.EquipmentListID = p.EquipmentListID
			JOIN @InactiveBatch i
				ON i.InventoryID = p.VehicleEntityID AND p.VehicleEntityTypeID = 1

		DELETE t
		FROM
			Marketing.EquipmentList t
			JOIN @InactiveBatch i
				ON i.InventoryID = t.VehicleEntityID AND t.VehicleEntityTypeID = 1

		--

		DELETE t
		FROM
			Marketing.MarketAnalysisVehiclePreferencePriceProvider t
			JOIN Marketing.MarketAnalysisVehiclePreference p
				ON t.MarketAnalysisVehiclePreferenceId = p.MarketAnalysisVehiclePreferenceId
			JOIN @InactiveBatch i
				ON i.InventoryID = p.VehicleEntityID AND p.VehicleEntityTypeID = 1


		DELETE t
		FROM
			Marketing.MarketAnalysisVehiclePreference t
			JOIN @InactiveBatch i
				ON i.InventoryID = t.VehicleEntityID AND t.VehicleEntityTypeID = 1

		--

		DELETE t
		FROM
			Marketing.MarketListingVehiclePreferenceSearchOverrides t
			JOIN Marketing.MarketListingVehiclePreference p
				ON t.MarketListingVehiclePreferenceId = p.MarketListingVehiclePreferenceId
			JOIN @InactiveBatch i
				ON i.InventoryID = p.VehicleEntityID AND p.VehicleEntityTypeID = 1

		DELETE t
		FROM
			Marketing.MarketListingVehiclePreference t
			JOIN @InactiveBatch i
				ON i.InventoryID = t.VehicleEntityID AND t.VehicleEntityTypeID = 1

		-- no child records to worry about below this line

		DELETE t
		FROM
			Marketing.PhotoVehiclePreference t
			JOIN @InactiveBatch i
				ON i.InventoryID = t.VehicleEntityID AND t.VehicleEntityTypeID = 1

		DELETE t
		FROM
			Marketing.ValueAnalyzerVehiclePreference t
			JOIN @InactiveBatch i
				ON i.InventoryID = t.VehicleEntityID AND t.VehicleEntityTypeID = 1


		DELETE t
		FROM
			Marketing.VehicleEquipmentProvider t
			JOIN @InactiveBatch i
				ON i.InventoryID = t.VehicleEntityID AND t.VehicleEntityTypeID = 1

		DELETE t
		FROM
			Marketing.AdPreviewVehiclePreference t
			JOIN @InactiveBatch i
				ON i.InventoryID = t.VehicleEntityID AND t.VehicleEntityTypeID = 1

		DELETE t
		FROM
			Marketing.PrintAudit t
			JOIN @InactiveBatch i
				ON i.InventoryID = t.VehicleEntityID AND t.VehicleEntityTypeID = 1

	END TRY	
	BEGIN CATCH

		IF XACT_STATE() != 0
			ROLLBACK TRAN;	

		EXEC sp_ErrorHandler
		
	END CATCH

	IF XACT_STATE() = 1
		COMMIT TRAN

-- end of batch processing

DELETE @Inactive
WHERE InventoryID IN 
(
	SELECT InventoryID 
	FROM @InactiveBatch
)

DELETE @InactiveBatch

END -- loop

GO


