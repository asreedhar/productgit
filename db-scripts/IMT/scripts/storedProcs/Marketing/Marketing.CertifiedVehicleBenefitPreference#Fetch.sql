IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[CertifiedVehicleBenefitPreference#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[CertifiedVehicleBenefitPreference#Fetch]



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[CertifiedVehicleBenefitPreference#Fetch]
	@OwnerHandle varchar(36),
	@VehicleHandle varchar(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.CertifiedVehicleBenefitPreference#Fetch.sql,v 1.1.4.1.2.2 2010/06/02 18:40:28 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Returns a certified vehicle benefit preference by owner and vehicle handles
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle
 * @VehicleHandle
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle does not exists
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle does not exist
 *
 * History
 * ------- 
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

BEGIN TRY

	--------------------------------------------------------------------------------------------
	-- Validate Parameter Values
	--------------------------------------------------------------------------------------------

	DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT, @VehicleEntityTypeID INT, @VehicleEntityID INT

	-- these calls will raise the appropriate errors if the handles are invalid.
	exec [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
	exec [Market].[Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out

    IF NOT EXISTS 
	(
		SELECT 1 FROM Marketing.CertifiedVehicleBenefitPreference 
		WHERE 
			OwnerID = @OwnerID AND 
			VehicleEntityTypeID = @VehicleEntityTypeID AND 
			VehicleEntityID = @VehicleEntityID 
	)
    BEGIN
	    RAISERROR (50106,16,1,'CertifiedVehicleBenefitPreference', @OwnerID, @VehicleEntityTypeID, @VehicleEntityID )
	    RETURN @@ERROR
    END

    SELECT  
		CertifiedVehicleBenefitPreferenceID,
		OwnerID, 
		VehicleEntityTypeID,
		VehicleEntityID,
		IsDisplayed,
		OwnerCertifiedProgramID

    FROM	Marketing.CertifiedVehicleBenefitPreference p
    WHERE   p.OwnerID = @OwnerID AND
			p.VehicleEntityTypeID = @VehicleEntityTypeID AND
			p.VehicleEntityID = @VehicleEntityID

	
END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH


 
 