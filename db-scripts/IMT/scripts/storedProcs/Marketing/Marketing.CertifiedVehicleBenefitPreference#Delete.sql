IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[CertifiedVehicleBenefitPreference#Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[CertifiedVehicleBenefitPreference#Delete]



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[CertifiedVehicleBenefitPreference#Delete]
    @CertifiedVehicleBenefitPreferenceID INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.CertifiedVehicleBenefitPreference#Delete.sql,v 1.1.4.1.2.2 2010/06/02 18:40:27 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Deletes a Certified Vehicle Benefit Preference.  Due to the cascade delete, this also deletes the line items.
 *  
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

BEGIN TRY
     
	Delete From Marketing.CertifiedVehicleBenefitPreference
	WHERE CertifiedVehicleBenefitPreferenceID = @CertifiedVehicleBenefitPreferenceID


END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
