IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[ConsumerHighlightVehiclePreference#Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[ConsumerHighlightVehiclePreference#Delete]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[ConsumerHighlightVehiclePreference#Delete]
    @ConsumerHighlightVehiclePreferenceId INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.ConsumerHighlightVehiclePreference#Delete.sql,v 1.1.4.1.2.2 2010/06/02 18:40:27 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Deletes a ConsumerHighlightVehiclePreference.
 * 
 * Input Parameters
 * ----------
 *
 * @ConsumerHighlightVehiclePreferenceId
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @ConsumerHighlightVehiclePreferenceId is null
 * -------------------------------------------------------------------- */

BEGIN TRY

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------

    IF @ConsumerHighlightVehiclePreferenceId IS NULL
    BEGIN
	RAISERROR (50100,16,1,'ConsumerHighlightVehiclePreferenceId')
	RETURN @@ERROR
    END

    DELETE 
    FROM    Marketing.ConsumerHighlightVehiclePreference
    WHERE   ConsumerHighlightVehiclePreferenceId = @ConsumerHighlightVehiclePreferenceId

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
