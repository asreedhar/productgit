IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'GetInventoryInfoFromVin' AND SCHEMA_NAME(schema_id) = 'Marketing')
	BEGIN
		DROP  Procedure  [Marketing].[GetInventoryInfoFromVin]
	END

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [Marketing].[GetInventoryInfoFromVin]
        @vin VARCHAR(17)
AS

SELECT	I.InventoryID, I.StockNumber, BU.BusinessUnitID, BU.BusinessUnitCode
FROM	dbo.Inventory I WITH (NOLOCK)
	INNER JOIN dbo.tbl_Vehicle V WITH (NOLOCK) ON I.VehicleID = V.VehicleID AND V.Vin = @vin
	INNER JOIN dbo.BusinessUnit BU WITH (NOLOCK) ON BU.BusinessUnitID = I.BusinessUnitID 
WHERE	I.InventoryActive = 1