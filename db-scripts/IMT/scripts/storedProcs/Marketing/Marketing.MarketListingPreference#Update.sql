USE [IMT]
GO

IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'MarketListingPreference#Update' AND SCHEMA_NAME(schema_id) = 'Marketing')
	BEGIN
		DROP  Procedure  [Marketing].[MarketListingPreference#Update]
	END

GO

/****** Object:  StoredProcedure [Marketing].[MarketListingPreference#Update]    Script Date: 12/01/2009 12:07:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [Marketing].[MarketListingPreference#Update]
	@MarketListingPreferenceId INT,	
	@IsDisplayed BIT,
	@MileageDeltaBelow INT,
	@MileageDeltaAbove INT,
	@PriceDeltaAbove INT,
	@PriceDeltaBelow INT,
	@UpdateUser VARCHAR(80)	
AS

/* --------------------------------------------------------------------
 * 
 * $Id:
 * 
 * Summary
 * -------
 * 
 * Updates a MarketListingPreference
 * 
 * Input Parameters
 * ----------
 *
 * @MarketListingPreferenceId
 * @IsDisplayed
 * @ShowDealerName
 * @ShowCertifiedIndicator
 * @MileageDeltaBelow
 * @MileageDeltaAbove
 * @PriceDeltaAbove
 * @PriceDeltaBelow
 * @UpdateUser
 *  
 * Exceptions
 * ----------
 * 
 * 50100 - @MarketListingPreferenceId IS NULL
 * 50100 - @IsDisplayed IS NULL
 * 50100 - @ShowDealerName IS NULL
 * 50100 - @ShowCertifiedIndicator IS NULL
 * 50100 - @MileageDeltaBelow IS NULL
 * 50100 - @MileageDeltaAbove IS NULL
 * 50100 - @PriceDeltaAbove IS NULL
 * 50100 - @PriceDeltaBelow IS NULL
 * 50100 - @InsertUser IS NULL
 * 50106 - @MarketListingPreferenceId does not exists
 * -------------------------------------------------------------------- */




BEGIN TRY

-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


--Check for no nulls

    IF @MarketListingPreferenceId IS NULL
    BEGIN
	RAISERROR (50100,16,1,'MarketListingPreferenceId')
	RETURN @@ERROR
    END

    IF @IsDisplayed IS NULL
    BEGIN
	RAISERROR (50100,16,1,'IsDisplayed')
	RETURN @@ERROR
    END

    IF @MileageDeltaBelow IS NULL
    BEGIN
	RAISERROR (50100,16,1,'MileageDeltaBelow')
	RETURN @@ERROR
    END
    
    IF @MileageDeltaAbove IS NULL
    BEGIN
	RAISERROR (50100,16,1,'MileageDeltaAbove')
	RETURN @@ERROR
    END

    IF @PriceDeltaAbove IS NULL
    BEGIN
	RAISERROR (50100,16,1,'PriceDeltaAbove')
	RETURN @@ERROR
    END

    IF @PriceDeltaBelow IS NULL
    BEGIN
	RAISERROR (50100,16,1,'PriceDeltaBelow')
	RETURN @@ERROR
    END
    
    IF @UpdateUser IS NULL
    BEGIN
	RAISERROR (50100,16,1,'UpdateUser')
	RETURN @@ERROR
    END
    
--Get the update user id
    DECLARE @UpdateUserId INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @UpdateUser, @UpdateUserId OUTPUT    

    
    IF NOT EXISTS (SELECT 1 FROM Marketing.MarketListingPreference WHERE MarketListingPreferenceId = @MarketListingPreferenceId)
    BEGIN
	RAISERROR (50106,16,1,'MarketListingPreference', @MarketListingPreferenceId)
	RETURN @@ERROR
    END

UPDATE  Marketing.MarketListingPreference
    SET			
	    IsDisplayed = @IsDisplayed,
	    MileageDeltaBelow = @MileageDeltaBelow,
	    MileageDeltaAbove = @MileageDeltaAbove,
	    PriceDeltaAbove = @PriceDeltaAbove,	    
	    PriceDeltaBelow = @PriceDeltaBelow,
	    UpdateUserId = @UpdateUserId,
	    UpdateDate = GETDATE()
	    
    WHERE MarketListingPreferenceId = @MarketListingPreferenceId


    -- Success
    RETURN 0

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
