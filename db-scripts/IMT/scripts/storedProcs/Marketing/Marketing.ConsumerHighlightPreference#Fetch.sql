IF OBJECT_ID(N'[Marketing].[ConsumerHighlightPreference#Fetch]') IS NOT NULL
	DROP PROCEDURE [Marketing].[ConsumerHighlightPreference#Fetch]
GO

CREATE PROCEDURE [Marketing].[ConsumerHighlightPreference#Fetch]
	@OwnerHandle varchar(36)    
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch the record from the Marketing.ConsumerHighlightPreference table for the given owner.
 *  Also, fetch the highlight section, snippet source and vehicle history report preferences.
 * 
 *	Parameters:
 *	@OwnerHandle
 *
 * History
 * ----------
 * 
 *  kmoeller 09/20/2010  Update procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out

------------------------------------------------------------------------------------------------
-- Perform Fetch
------------------------------------------------------------------------------------------------

SELECT		
	[OwnerId],
	[MinimumJDPowerCircleRating],
	[MaxVHRItemsInitialDisplay]	
FROM
	[Marketing].[ConsumerHighlightPreference]
WHERE
	[OwnerId] = @OwnerId
    
------------------------------------------------------------------------------------------------
-- Fetch Section Ordering
------------------------------------------------------------------------------------------------
    
EXEC [Marketing].[ConsumerHighlightSectionPreference#Fetch] @OwnerHandle

------------------------------------------------------------------------------------------------
-- Fetch Snippet Ordering
------------------------------------------------------------------------------------------------

EXEC [Marketing].[SnippetSourcePreference#Fetch] @OwnerHandle
    
------------------------------------------------------------------------------------------------
-- Fetch Vehicle History Report Source Ordering
------------------------------------------------------------------------------------------------

EXEC [Marketing].[VehicleHistorySourcePreference#Fetch] @OwnerHandle
