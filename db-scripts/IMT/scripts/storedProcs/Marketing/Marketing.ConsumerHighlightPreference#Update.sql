IF OBJECT_ID(N'[Marketing].[ConsumerHighlightPreference#Update]') IS NOT NULL
	DROP PROCEDURE [Marketing].[ConsumerHighlightPreference#Update]
GO

CREATE PROCEDURE [Marketing].[ConsumerHighlightPreference#Update]    
	@OwnerHandle varchar(36),	
    @MinimumJDPowerCircleRating float,
    @UpdateUser varchar(50),
	@MaxVHRItemsInitialDisplay tinyint
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Update a record in the Marketing.ConsumerHighlightPreference table.
 * 
 *	Parameters: 
 *	@OwnerHandle
 *  @MinimumJDPowerCircleRating 
 *  @UpdateUser
 *
 * Exceptions
 * ----------
 *  
 *  50100 - @MinimumJDPowerCircleRating is null 
 *
 * History
 * ----------
 * 
 *  CGC 04/07/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

IF @MinimumJDPowerCircleRating IS NULL
    BEGIN
	RAISERROR (50100,16,1,'MinimumJDPowerCircleRating')
	RETURN @@ERROR
    END    

DECLARE @UpdateUserID INT, @err INT
EXEC @err = dbo.ValidateParameter_MemberID#UserName @UpdateUser, @UpdateUserID OUTPUT
IF @err <> 0 GOTO Failed

DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out

------------------------------------------------------------------------------------------------
-- Perform Update
------------------------------------------------------------------------------------------------

UPDATE [Marketing].[ConsumerHighlightPreference] 
SET    
	[MinimumJDPowerCircleRating] = @MinimumJDPowerCircleRating,
	[UpdateUserId] = @UpdateUserId,
	[MaxVHRItemsInitialDisplay] = @MaxVHRItemsInitialDisplay,
	[UpdateDate] = getdate()
WHERE    
    [OwnerId] = @OwnerId

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
