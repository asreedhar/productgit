IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[MarketListingVehiclePreferenceSearchOverrides#Save]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[MarketListingVehiclePreferenceSearchOverrides#Save]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[MarketListingVehiclePreferenceSearchOverrides#Save]
	@MarketListingVehiclePreferenceId int,
	@RemovedIds varchar(4000),	
	@DisplayedIds varchar(4000),
	@InsertUser varchar(80)
	
AS

SET NOCOUNT ON;

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.MarketListingVehiclePreferenceSearchOverrides#Save.sql
 * 
 * Summary
 * -------
 * 
 * Inserts a Market listing vehicle preference search override.
 * 
 * Input Parameters
 * ----------
 *
 *	@MarketListingVehiclePreferenceId int
 *	@VehicleId int
 *	@RemovedIds varchar(4000) - A csv delimited list of vehicleIds to remove
 *	@DisplayedIds varchar(4000) - A csv delimited list of vehicleIds to display
 *
 *	@InsertUser varchar(80)
 * 
 * Output Parameters
 * ----------
 *
 *
 * Notes
 * -----
 * Hidden and Displayed are passed as lists of ids to avoid making lots of "chatty" calls to the db. 
 *
 * Exceptions
 * ----------
 * 
 * -------------------------------------------------------------------- */

BEGIN TRY


    DECLARE @InsertUserID INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @InsertUser, @InsertUserID OUTPUT

	-- Split out the list of hidden and displayed ids.
	declare @removed table
	(
		VehicleId int
	) 

	declare @displayed table 
	(
		VehicleId int
	) 

	if( len(@RemovedIds) > 0 )
	begin
		insert into @removed select Value from dbo.Split(@RemovedIds,',');
	end

	if( len(@DisplayedIds) > 0 )
	begin
		insert into @displayed select Value from dbo.Split(@DisplayedIds, ',');
	end


	Delete so
	From Marketing.MarketListingVehiclePreferenceSearchOverrides so JOIN @removed r
		ON so.VehicleId = r.VehicleId
	Where 
		so.MarketListingVehiclePreferenceId = @MarketListingVehiclePreferenceId

	--
 	-- If the id _only_ exists in the @displayed table, then we need to insert data into the overrides table.
	--
	Insert Into Marketing.MarketListingVehiclePreferenceSearchOverrides
		( MarketListingVehiclePreferenceId,VehicleId,InsertUserId, InsertDate )

	Select   @MarketListingVehiclePreferenceId,
			 d.VehicleId,
			 @InsertUserID,
			 GetDate()

	From @displayed d Left OUTER JOIN Marketing.MarketListingVehiclePreferenceSearchOverrides so
	ON 
		so.MarketListingVehiclePreferenceId = @MarketListingVehiclePreferenceId AND
		so.VehicleId = d.VehicleId
	Where so.MarketListingVehiclePreferenceId is null

	-- If the id is already in the override table, we have nothing to do.  The listing is already hidden.

END TRY

BEGIN CATCH
	SET NOCOUNT OFF;
    EXEC dbo.sp_ErrorHandler
END CATCH

SET NOCOUNT OFF;
