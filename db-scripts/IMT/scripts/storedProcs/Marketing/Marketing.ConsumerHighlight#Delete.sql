IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[ConsumerHighlight#Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[ConsumerHighlight#Delete]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[ConsumerHighlight#Delete]
    @ConsumerHighlightId INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.ConsumerHighlight#Delete.sql,v 1.1.4.1.2.3 2010/06/03 19:42:39 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Deletes a ConsumerHighlight.
 * 
 * Input Parameters
 * ----------
 *
 * @ConsumerHighlightId
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @ConsumerHighlightId is null
 * -------------------------------------------------------------------- */

BEGIN TRY

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------

    IF @ConsumerHighlightId IS NULL
    BEGIN
	RAISERROR (50100,16,1,'ConsumerHighlightId')
	RETURN @@ERROR
    END

    DECLARE @HighlightProviderId TINYINT

    SELECT  @HighlightProviderId = HighlightProviderId
    FROM    Marketing.ConsumerHighlight
    WHERE   ConsumerHighlightId = @ConsumerHighlightId

    IF @HighlightProviderId = 2 OR @HighlightProviderID = 3    -- only Free Text items can be deleted
    BEGIN
	    RAISERROR('Vehicle History Consumer Highlights cannot be deleted.',16,1)
	    RETURN @@ERROR
    END

    DELETE 
    FROM    Marketing.ConsumerHighlight
    WHERE   ConsumerHighlightId = @ConsumerHighlightId

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
