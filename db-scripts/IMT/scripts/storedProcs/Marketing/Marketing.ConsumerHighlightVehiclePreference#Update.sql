IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[ConsumerHighlightVehiclePreference#Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[ConsumerHighlightVehiclePreference#Update]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[ConsumerHighlightVehiclePreference#Update]
    @OwnerHandle VARCHAR(36),
    @VehicleHandle VARCHAR(36),
    @IsDisplayed BIT,
    @IncludeCarfaxOneOwnerIcon BIT,
    @UpdateUser VARCHAR(80)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.ConsumerHighlightVehiclePreference#Update.sql,v 1.1.4.1.2.2 2010/06/02 18:40:28 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Updates a ConsumerHighlightVehiclePreference.
 * 
 * Input Parameters
 * ----------
 *
 * @OwnerHandle
 * @VehicleHandle
 * @IsDisplayed
 * @IncludeCarfaxOneOwnerIcon
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle does not exists
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle does not exist
 * 50106 - Preference does not exist
 * 50100 - @IsDisplayed is null
 * 50100 - @IncludeCarfaxOneOwnerIcon is null
 * -------------------------------------------------------------------- */

BEGIN TRY

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------

    IF @IsDisplayed IS NULL
    BEGIN
	RAISERROR (50100,16,1,'IsDisplayed')
	RETURN @@ERROR
    END

    IF @IncludeCarfaxOneOwnerIcon IS NULL
    BEGIN
	RAISERROR (50100,16,1,'IncludeCarfaxOneOwnerIcon')
	RETURN @@ERROR
    END

    DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT, @VehicleEntityTypeID INT, @VehicleEntityID INT

    -- these calls will raise the appropriate errors if the handles are invalid.
    exec [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
    exec [Market].[Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out

    DECLARE @ConsumerHighlightVehiclePreferenceId INT
    SELECT  @ConsumerHighlightVehiclePreferenceId = ConsumerHighlightVehiclePreferenceID
    FROM    Marketing.ConsumerHighlightVehiclePreference
    WHERE 
	    OwnerID = @OwnerID AND 
	    VehicleEntityTypeID = @VehicleEntityTypeID AND 
	    VehicleEntityID = @VehicleEntityID 

    IF @ConsumerHighlightVehiclePreferenceId IS NULL
    BEGIN
	RAISERROR (50106,16,1,'ConsumerHighlightVehiclePreference', @OwnerID, @VehicleHandle )
	RETURN @@ERROR
    END

    DECLARE @UpdateUserID INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @UpdateUser, @UpdateUserID OUTPUT

    UPDATE  Marketing.ConsumerHighlightVehiclePreference
    SET 
	IsDisplayed = @IsDisplayed,
	IncludeCarfaxOneOwnerIcon = @IncludeCarfaxOneOwnerIcon,
	UpdateUser = @UpdateUserID,
	UpdateDate = GETDATE()
    WHERE   ConsumerHighlightVehiclePreferenceId = @ConsumerHighlightVehiclePreferenceId

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
