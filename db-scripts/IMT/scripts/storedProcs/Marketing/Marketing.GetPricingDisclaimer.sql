
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[GetPricingDisclaimer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[GetPricingDisclaimer]
GO


CREATE PROCEDURE [Marketing].[GetPricingDisclaimer]
    @OwnerHandle VARCHAR(36),
    @VehicleHandle VARCHAR(36),
    @SearchHandle VARCHAR(36)   
AS

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER OFF

DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT,
            @VehicleEntityTypeID INT, @VehicleEntityID INT, @SearchID INT

EXEC Market.Pricing.ValidateParameter_OwnerHandle @OwnerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT
EXEC Market.Pricing.ValidateParameter_VehicleHandle @VehicleHandle, @VehicleEntityTypeID OUT, @VehicleEntityID OUT
EXEC Market.Pricing.ValidateParameter_SearchHandle @SearchHandle, @SearchID out	

DECLARE @InternetDistance INT
DECLARE @KbbPublicationDate VARCHAR(50)
DECLARE @JDPowerPeriodBeginDate DATETIME 
DECLARE @JDPowerPeriodEndDate DATETIME
DECLARE @JDPowerRegion VARCHAR(240)
DECLARE @NadaPublicationDate VARCHAR(50)

SELECT	@InternetDistance = D.Distance
FROM	Market.Pricing.Search S
JOIN	Market.Pricing.SearchResult_F SRF ON S.OwnerID = SRF.OwnerID AND S.SearchID = SRF.SearchID AND S.DefaultSearchTypeID = SRF.SearchTypeID
JOIN	Market.Pricing.DistanceBucket D ON D.DistanceBucketID = S.DistanceBucketID
WHERE	S.OwnerID = @OwnerID	
	AND S.SearchID = @SearchID AND SRF.ComparableUnits >= 5

IF @VehicleEntityTypeID = 1
BEGIN

	SELECT	@KbbPublicationDate = B.DatePublished
	FROM	FLDW.dbo.InventoryBookout_F IBF
		JOIN Bookouts B ON IBF.BookoutID = B.BookoutID
	WHERE	IBF.InventoryID = @VehicleEntityID
		AND	IBF.ThirdPartyCategoryID = 9
		AND	IBF.BookoutValueTypeID = 2

	SELECT	@JDPowerPeriodBeginDate = P.BeginDate, 
			@JDPowerPeriodEndDate   = P.EndDate
	FROM	FLDW.JDPower.Period_D P
	WHERE	P.PeriodID = 1

	SELECT @JDPowerRegion = PR.Name
    FROM Market.Pricing.Owner O
		JOIN Market.Pricing.OwnerPreference OP ON OP.OwnerID = O.OwnerID
		LEFT JOIN Market.Pricing.Seller_JDPower JP ON JP.SellerID = O.OwnerEntityID
		JOIN Market.JDPower.PowerRegion_D PR ON PR.PowerRegionID = COALESCE(OP.PowerRegionID,JP.PowerRegionID)
	WHERE O.OwnerID = @OwnerID

	SELECT @NadaPublicationDate = sp.Value
	FROM	FLDW.dbo.InventoryBookout_F IBF	
		JOIN imt.dbo.Bookouts bo ON IBF.BookoutID = bo.BookoutID
		CROSS APPLY imt.dbo.Split(DatePublished, ' ') sp
	WHERE sp.Rank = 1
		AND IBF.InventoryID = @VehicleEntityID
		AND	IBF.ThirdPartyCategoryID = 1
		AND	IBF.BookoutValueTypeID = 2

END

SELECT InternetDistance = @InternetDistance,
	KbbPublicationDate = @KbbPublicationDate,
	JDPowerPeriodBeginDate = @JDPowerPeriodBeginDate,
	JDPowerPeriodEndDate = @JDPowerPeriodEndDate,
	JDPowerRegion = @JDPowerRegion,
	NadaPublicationDate = @NadaPublicationDate


RETURN 0
