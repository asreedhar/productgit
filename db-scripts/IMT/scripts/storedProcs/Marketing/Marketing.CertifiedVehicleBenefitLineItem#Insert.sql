IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[CertifiedVehicleBenefitLineItem#Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[CertifiedVehicleBenefitLineItem#Insert]


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[CertifiedVehicleBenefitLineItem#Insert]
	@CertifiedVehicleBenefitPreferenceID INT,
	@OwnerCertifiedProgramBenefitID INT,
	@IsHighlight bit,
	@Rank int,
	@InsertUser VARCHAR(80)
	
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.CertifiedVehicleBenefitLineItem#Insert.sql,v 1.1.4.1.2.3 2010/06/03 19:42:39 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Inserts a new line item.
 * 
 * Parameters
 * ----------
 *
 *
 * Exceptions
 * ----------
 * 
 * None
 *
 * History
 * ------- 
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

BEGIN TRY
 
    DECLARE @InsertUserID INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @InsertUser, @InsertUserID OUTPUT
     
    INSERT 
    INTO    Marketing.CertifiedVehicleBenefitLineItem
	    (CertifiedVehicleBenefitPreferenceID,
		OwnerCertifiedProgramBenefitID,
		IsHighlight,
		Rank,
	    InsertUser,
	    InsertDate)
    VALUES
	    (@CertifiedVehicleBenefitPreferenceID,
	    @OwnerCertifiedProgramBenefitID,
	    @IsHighlight,
		@Rank,
	    @InsertUserID,
	    GETDATE())
	
END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH


 
 