IF OBJECT_ID(N'[Marketing].[SnippetSourcePreference#Insert]') IS NOT NULL
	DROP PROCEDURE [Marketing].[SnippetSourcePreference#Insert]
GO

CREATE PROCEDURE [Marketing].[SnippetSourcePreference#Insert]
    @SnippetSourceId int,
	@OwnerHandle varchar(36),
	@IsDisplayed bit,
    @Rank int,
    @InsertUser varchar(50)
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Insert a record into the Marketing.SnippetSourcePreference table.
 * 
 *	Parameters:
 *  @SnippetSourceId
 *	@OwnerHandle
 *  @IsDisplayed
 *  @Rank
 *  @InsertUser
 *
 * Exceptions
 * ----------
 *  
 *  50100 - @IsDisplayed is null
 *  50100 - @Rank is null
 *
 * History
 * ----------
 * 
 * CGC	04/07/2010	Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

IF @IsDisplayed IS NULL
    BEGIN
	RAISERROR (50100,16,1,'IsDisplayed')
	RETURN @@ERROR
    END
    
IF @Rank IS NULL
    BEGIN
	RAISERROR (50100,16,1,'Rank')
	RETURN @@ERROR
    END

DECLARE @InsertUserID INT, @err INT
EXEC @err = dbo.ValidateParameter_MemberID#UserName @InsertUser, @InsertUserID OUTPUT
IF @err <> 0 GOTO Failed

DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out

------------------------------------------------------------------------------------------------
-- Perform Insert
------------------------------------------------------------------------------------------------

INSERT INTO [Marketing].[SnippetSourcePreference] (
    [SnippetSourceId],
	[OwnerId],
	[IsDisplayed],
	[Rank],
	[InsertUserId],
	[InsertDate]
) VALUES (
    @SnippetSourceId,
	@OwnerId,
	@IsDisplayed,
	@Rank,
	@InsertUserId,
    getdate()
)

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
