
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[GetVehicleIdentifiers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[GetVehicleIdentifiers]



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[GetVehicleIdentifiers]
	@OwnerHandle varchar(36),
	@VehicleHandle varchar(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.VehicleSummary.GetVehicleIdentifiers.sql,v 1.1.4.1.2.2 2010/06/02 18:40:28 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Returns a bunch of identifiers given an owner and vehicle handle.
 * 
 * Parameters
 * ----------
 *
 * @OwnerHandle
 * @VehicleHandle
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle is null
 * 50106 - @OwnerHandle does not exists
 * 50100 - @VehicleHandle is null
 * 50106 - @VehicleHandle does not exist
 *
 * History
 * ------- 
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

BEGIN TRY

	DECLARE @rc INT, @err INT
	DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT, @VehicleEntityTypeID INT, @VehicleEntityID INT

	EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
	EXEC [Market].[Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out

	Declare @VehicleID INT

	Declare @VehicleID_Inventory INT
	Declare @VehicleID_Listings INT

	--
	-- Look at Inventory and market listings.
	--
	IF @VehicleEntityTypeID = 1
	BEGIN
		-- Inventory
		Select @VehicleID_Inventory = VehicleID From IMT.dbo.Inventory WITH (NOLOCK) WHERE InventoryID = @VehicleEntityID
	END

	IF @VehicleEntityTypeID = 5
	BEGIN
		-- Market Listing
		Select @VehicleID_Listings = VehicleID From Market.Listing.Vehicle WHERE VehicleID = @VehicleEntityID
	END

	-- There should be only one non-null
	Select @VehicleID = Coalesce( @VehicleID_Inventory, @VehicleID_Listings)

	Declare @VehicleCatalogID INT
	Declare @MakeModelGroupingID INT
	Declare @BodyTypeID INT

	Select @VehicleCatalogID=VehicleCatalogID, @MakeModelGroupingID=MakeModelGroupingID,@BodyTypeID=BodyTypeID  
	From IMT.dbo.GetVehicleCatalogID(@VehicleID)


	Select @OwnerEntityTypeID as OwnerEntityTypeID, @OwnerEntityID as OwnerEntityID, @OwnerID as OwnerID, @VehicleEntityTypeID as VehicleEntityTypeID,
	@VehicleEntityID as VehicleEntityID, @VehicleID as VehicleID, 
	@VehicleCatalogID as VehicleCatalogID, 
	@MakeModelGroupingID as MakeModelGroupingID, @BodyTypeID as BodyTypeID

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH