
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[MarketListingPreference#Exists]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[MarketListingPreference#Exists]
GO

CREATE Procedure [Marketing].[MarketListingPreference#Exists]
	@OwnerHandle VARCHAR(36)
AS

/* --------------------------------------------------------------------
 * 
 * $Id:
 * 
 * Summary
 * -------
 * 
 * Checks to see if a MarketListingPreference Exists for the provided owner
 * 
 * Input Parameters
 * ----------
 *
 * @OwnerHandle 
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle IS NULL
 * -------------------------------------------------------------------- */



BEGIN TRY

	IF @OwnerHandle IS NULL
    BEGIN
		RAISERROR (50100,16,1,'OwnerHandle')
		RETURN @@ERROR
    END  
    
    DECLARE @Exists BIT
    SET @Exists = 0 

--First let's get the owner id
	DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
    EXEC Market.Pricing.ValidateParameter_OwnerHandle @OwnerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT 
    
    
    IF EXISTS ( SELECT 1 FROM Marketing.MarketListingPreference WHERE OwnerID = @OwnerID)
	SET @Exists = 1

    RETURN @Exists

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH

GO
