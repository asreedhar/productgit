IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[MarketAnalysisVehiclePreferencePriceProvider#DeleteAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[MarketAnalysisVehiclePreferencePriceProvider#DeleteAll]


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[MarketAnalysisVehiclePreferencePriceProvider#DeleteAll]
    @MarketAnalysisVehiclePreferenceID INT
AS

/* --------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * Deletes all market analysis vehicle preference price provider rows for
 * the specified vehicle preference.
 * 
 * Input Parameters
 * ----------
 *
 * @MarketAnalysisVehiclePreferenceID
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - MarketAnalysisVehiclePreferenceID IS NULL
 * 50106 - MarketAnalysisVehiclePreference row does not exist
 * -------------------------------------------------------------------- */

BEGIN TRY

    IF @MarketAnalysisVehiclePreferenceID IS NULL
    BEGIN
        RAISERROR (50100,16,1,'MarketAnalysisVehiclePreferenceID')
        RETURN @@ERROR
    END

    IF NOT EXISTS (
	SELECT 1 
	FROM Marketing.MarketAnalysisVehiclePreference 
	WHERE MarketAnalysisVehiclePreferenceId = @MarketAnalysisVehiclePreferenceID
	)
    BEGIN
	RAISERROR (50106,16,1,'MarketAnalysisVehiclePreference', @MarketAnalysisVehiclePreferenceID )
	RETURN @@ERROR
    END
    
    DELETE 
    FROM    Marketing.MarketAnalysisVehiclePreferencePriceProvider
    WHERE   MarketAnalysisVehiclePreferenceId = @MarketAnalysisVehiclePreferenceID

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
