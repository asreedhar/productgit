IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[ConsumerHighlight#Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[ConsumerHighlight#Insert]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[ConsumerHighlight#Insert]
    @ConsumerHighlightVehiclePreferenceId INT,
    @HighlightProviderId TINYINT,
    @Text VARCHAR(200) = NULL,
    @ProviderSourceRowId INT = NULL,
    @Rank INT,
    @IsDisplayed BIT,
    @ExpirationDate DATETIME = NULL,
    @InsertUser VARCHAR(80),
    @ConsumerHighlightId INT OUTPUT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.ConsumerHighlight#Insert.sql,v 1.1.4.1.2.3 2010/06/03 19:42:39 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Inserts a ConsumerHighlight (requires an existing ConsumerHighlightList).
 * 
 * Input Parameters
 * ----------
 *
 * @ConsumerHighlightVehiclePreferenceId
 * @HighlightProviderId
 * @Text
 * @Rank
 * @IsDisplayed
 * @ExpirationDate
 * @InsertUser
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @ConsumerHighlightListId is null
 * 50106 - @ConsumerHighlightList does not exists
 * 50100 - @HighlightProviderId is null
 * 50106 - @HighlightProvider does not exist 
 * 50100 - @Rank is null
 * 50100 - @IsDisplayed is null
 *
 * History
 * ----------
 * 2010/04/09 - Zac Brown - Create Proc
 * 2010/05/05 - Christian Clouston - Removed empty check on text.
 * -------------------------------------------------------------------- */

BEGIN TRY

--------------------------------------------------------------------------------------------
-- Validate Parameter Values
--------------------------------------------------------------------------------------------


    IF @ConsumerHighlightVehiclePreferenceId IS NULL
    BEGIN
	RAISERROR (50100,16,1,'ConsumerHighlightVehiclePreferenceId')
	RETURN @@ERROR
    END

    IF NOT EXISTS (
		SELECT  1
		FROM    Marketing.ConsumerHighlightVehiclePreference
		WHERE	ConsumerHighlightVehiclePreferenceId = @ConsumerHighlightVehiclePreferenceId	)
    BEGIN
	RAISERROR (50106,16,1,'ConsumerHighlightVehiclePreference', @ConsumerHighlightVehiclePreferenceId )
	RETURN @@ERROR
    END

    IF @HighlightProviderId IS NULL
    BEGIN
	RAISERROR (50100,16,1,'HighlightProviderId')
	RETURN @@ERROR
    END

    IF NOT EXISTS (
		SELECT  1
		FROM    Marketing.HighlightProvider
		WHERE	HighlightProviderId = @HighlightProviderId	)
    BEGIN
	RAISERROR (50106,16,1,'HighlightProvider', @HighlightProviderId )
	RETURN @@ERROR
    END    

    IF @Rank IS NULL
    BEGIN
	RAISERROR (50100,16,1,'Rank')
	RETURN @@ERROR
    END

    IF @IsDisplayed IS NULL
    BEGIN
	RAISERROR (50100,16,1,'IsDisplayed')
	RETURN @@ERROR
    END

    DECLARE @InsertUserID INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @InsertUser, @InsertUserID OUTPUT

    INSERT 
    INTO    Marketing.ConsumerHighlight(
	    ConsumerHighlightVehiclePreferenceId, 
	    HighlightProviderId, 
	    Text, 
	    Rank, 
	    IsDisplayed, 
	    ExpirationDate, 
	    InsertUser, 
	    InsertDate,
        ProviderSourceRowId)
    VALUES(
	    @ConsumerHighlightVehiclePreferenceId, 
	    @HighlightProviderId, 
	    @Text, 
	    @Rank, 
	    @IsDisplayed, 
	    @ExpirationDate, 
	    @InsertUserID, 
	    GETDATE(),
        @ProviderSourceRowId)

    SET @ConsumerHighlightId = SCOPE_IDENTITY()

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
