
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[PhotoVehiclePreference#Set]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[PhotoVehiclePreference#Set]
GO

CREATE PROCEDURE Marketing.PhotoVehiclePreference#Set
        @OwnerHandle            VARCHAR(36),
        @VehicleHandle          VARCHAR(36),
        @IsDisplayed		BIT,
        @User			VARCHAR(80)
AS

/* --------------------------------------------------------------------
 * 
 * 
 * Summary
 * -------
 * 
 * Set the preference for displaying the vehicle photo
 *
 * Parameters
 * ----------
 * 
 * @OwnerHandle            - the owner of the new list
 * @VehicleHandle          - the vehicle whose equipment the list enumerates
 * @IsDisplayed		   - a value indicating whether or not a vehicle photo 
 *                           should be shown for this owner/vehicle combination
 * 
 * Exceptions
 * ----------
 *
 * 50100 - OwnerHandle IS NULL
 * 50106 - The supplied handle does not resolve a known owner
 * 50100 - VehicleHandle IS NULL
 * 50106 - The supplied handle does not resolve a known vehicle
 * 50100 - @User is null
 * 50106 - @User does not exist
 *
 * History
 * ----------
 * 
 * DGH 04/12/2010 First Revision.
 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT


DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT, @UserID INT

EXEC @err = dbo.ValidateParameter_MemberID#UserName @User, @UserID OUTPUT
IF (@err <> 0) GOTO Failed

EXEC @err = [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
IF @err <> 0 GOTO Failed

DECLARE	@VehicleEntityTypeID INT, @VehicleEntityID INT

EXEC @err = [Market].[Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out
IF @err <> 0 GOTO Failed

------------------------------------------------------------------------------------------------
-- Do the work, default to 1 (visible = true)
------------------------------------------------------------------------------------------------
IF NOT EXISTS(
    SELECT 1 
    FROM Marketing.PhotoVehiclePreference
    WHERE   OwnerID = @OwnerID
    AND     VehicleEntityTypeID = @VehicleEntityTypeID
    AND     VehicleEntityID = @VehicleEntityID)
BEGIN

    INSERT INTO Marketing.PhotoVehiclePreference
	( OwnerID,
	  VehicleEntityTypeID,
	  VehicleEntityID,
	  IsDisplayed,
	  InsertUser,
	  InsertDate
	)
    VALUES  
	( @OwnerID,
	  @VehicleEntityTypeID,
	  @VehicleEntityID,
	  @IsDisplayed,
	  @UserID,
	  GETDATE()
	)
END
ELSE
BEGIN
    UPDATE Marketing.PhotoVehiclePreference
    SET 
	IsDisplayed = @IsDisplayed,
	UpdateUser = @UserID,
	UpdateDate = GETDATE()
    WHERE   OwnerID = @OwnerID
    AND     VehicleEntityTypeID = @VehicleEntityTypeID
    AND     VehicleEntityID = @VehicleEntityID
END

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
