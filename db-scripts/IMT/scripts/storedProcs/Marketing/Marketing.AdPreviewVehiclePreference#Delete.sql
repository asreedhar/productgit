USE IMT
GO

IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'AdPreviewVehiclePreference#Delete' AND SCHEMA_NAME(schema_id) = 'Marketing')
	BEGIN
		DROP  Procedure  [Marketing].[AdPreviewVehiclePreference#Delete]
	END

GO

CREATE Procedure [Marketing].[AdPreviewVehiclePreference#Delete]
	@AdPreviewVehiclePreferenceId INT
AS
/* --------------------------------------------------------------------
 * 
 * $Id:
 * 
 * Summary
 * -------
 * 
 * Deletes a AdPreviewVehiclePreference
 * 
 * Input Parameters
 * ----------
 *
 * @AdPreviewVehiclePreferenceId
 *
 * Exceptions
 *	50100 - Parameter IS NULL
 *  50106 - No such %s record with ID %d. Please reexecute with a more appropriate value.
 * ----------
 * -------------------------------------------------------------------- */


BEGIN TRY

-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


--Check for no nulls

	IF @AdPreviewVehiclePreferenceId IS NULL
    BEGIN
	RAISERROR (50100,16,1,'AdPreviewVehiclePreferenceId')
	RETURN @@ERROR
    END


	IF NOT EXISTS ( SELECT 1 FROM Marketing.AdPreviewVehiclePreference WHERE AdPreviewVehiclePreferenceId = @AdPreviewVehiclePreferenceId)
    BEGIN
	RAISERROR (50106,16,1,'AdPreviewVehiclePreference', @AdPreviewVehiclePreferenceId)
	RETURN @@ERROR
    END
    
	
    DELETE 
    FROM    Marketing.AdPreviewVehiclePreference
    WHERE AdPreviewVehiclePreferenceId  = @AdPreviewVehiclePreferenceId

    -- Success
    RETURN 0

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
GO

