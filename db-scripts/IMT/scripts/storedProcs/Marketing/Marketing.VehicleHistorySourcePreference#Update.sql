IF OBJECT_ID(N'[Marketing].[VehicleHistorySourcePreference#Update]') IS NOT NULL
	DROP PROCEDURE [Marketing].[VehicleHistorySourcePreference#Update]
GO

CREATE PROCEDURE [Marketing].[VehicleHistorySourcePreference#Update]
    @VehicleHistoryReportPreferenceId int,
	@VehicleHistoryReportInspectionId int,
	@OwnerHandle varchar(36),
	@IsDisplayed bit,
    @Rank int,
    @UpdateUser varchar(50)
AS


SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

IF @IsDisplayed IS NULL
    BEGIN
	RAISERROR (50100,16,1,'IsDisplayed')
	RETURN @@ERROR
    END
    
IF @Rank IS NULL
    BEGIN
	RAISERROR (50100,16,1,'Rank')
	RETURN @@ERROR
    END

DECLARE @UpdateUserID INT, @err INT
EXEC @err = dbo.ValidateParameter_MemberID#UserName @UpdateUser, @UpdateUserID OUTPUT
IF @err <> 0 GOTO Failed

DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out


UPDATE [Marketing].[VehicleHistoryReportSourcePreference] 
SET
	[Rank] = @Rank,
	[IsDisplayed] = @IsDisplayed,	
	[UpdateUserId] = @UpdateUserId,
	[UpdateDate] = getdate()
WHERE
    [VehicleHistoryReportPreferenceId] = @VehicleHistoryReportPreferenceId AND [VehicleHistoryReportInspectionId] = @VehicleHistoryReportInspectionId

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
