IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'InventoryDocuments#FetchActive' AND SCHEMA_NAME(schema_id) = 'Marketing')
	BEGIN
		DROP  Procedure  [Marketing].[InventoryDocuments#FetchActive]
	END

GO

CREATE Procedure [Marketing].[InventoryDocuments#FetchActive]
	
AS

SELECT BU.BusinessUnitID, I.InventoryID
FROM IMT.Marketing.InventoryDocuments D
JOIN FLDW.dbo.InventoryActive I ON I.InventoryID = D.VehicleEntityID
JOIN Market.Pricing.Owner O ON O.OwnerID = D.OwnerID
JOIN IMT.dbo.BusinessUnit BU ON BU.BusinessUnitID = O.OwnerEntityID AND BU.Active = 1