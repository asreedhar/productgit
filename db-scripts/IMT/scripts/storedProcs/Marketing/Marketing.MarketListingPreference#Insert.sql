IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'MarketListingPreference#Insert' AND SCHEMA_NAME(schema_id) = 'Marketing')
	BEGIN
		DROP  Procedure  [Marketing].[MarketListingPreference#Insert]
	END
GO


/****** Object:  StoredProcedure [Marketing].[MarketListingPreference#Insert]    Script Date: 12/01/2009 12:01:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [Marketing].[MarketListingPreference#Insert]
	@OwnerHandle VARCHAR(36),
	@IsDisplayed BIT,
	@MileageDeltaBelow INT,
	@MileageDeltaAbove INT,
	@PriceDeltaAbove INT,
	@PriceDeltaBelow INT,
	@InsertUser VARCHAR(80),	
	
	@MarketListingPreferenceId INT OUTPUT

AS

/* --------------------------------------------------------------------
 * 
 * $Id:
 * 
 * Summary
 * -------
 * 
 * Inserts a MarketListingPreference
 * 
 * Input Parameters
 * ----------
 *
 * @OwnerHandle
 * @IsDisplayed
 * @ShowDealerName
 * @ShowCertifiedIndicator
 * @MileageDeltaBelow
 * @MileageDeltaAbove
 * @PriceDeltaAbove
 * @PriceDeltaBelow
 * @InsertUser
 * 
 * Output Parameters
 * ----------
 *
 * @MarketListingPreferenceId
 *
 *
 * Exceptions
 * ----------
 * 
 * 50100 - @OwnerHandle IS NULL
 * 50100 - @IsDisplayed IS NULL
 * 50100 - @ShowDealerName IS NULL
 * 50100 - @ShowCertifiedIndicator IS NULL
 * 50100 - @MileageDeltaBelow IS NULL
 * 50100 - @MileageDeltaAbove IS NULL
 * 50100 - @PriceDeltaAbove IS NULL
 * 50100 - @PriceDeltaBelow IS NULL
 * 50100 - @InsertUser IS NULL
 * -------------------------------------------------------------------- */



BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--Check for no nulls

    IF @OwnerHandle IS NULL
    BEGIN
	RAISERROR (50100,16,1,'OwnerHandle')
	RETURN @@ERROR
    END

    IF @IsDisplayed IS NULL
    BEGIN
	RAISERROR (50100,16,1,'IsDisplayed')
	RETURN @@ERROR
    END
    
    IF @MileageDeltaBelow IS NULL
    BEGIN
	RAISERROR (50100,16,1,'MileageDeltaBelow')
	RETURN @@ERROR
    END
    
    IF @MileageDeltaAbove IS NULL
    BEGIN
	RAISERROR (50100,16,1,'MileageDeltaAbove')
	RETURN @@ERROR
    END

    IF @PriceDeltaAbove IS NULL
    BEGIN
	RAISERROR (50100,16,1,'PriceDeltaAbove')
	RETURN @@ERROR
    END

    IF @PriceDeltaBelow IS NULL
    BEGIN
	RAISERROR (50100,16,1,'PriceDeltaBelow')
	RETURN @@ERROR
    END
    
    IF @InsertUser IS NULL
    BEGIN
	RAISERROR (50100,16,1,'InsertUser')
	RETURN @@ERROR
    END
    
    DECLARE @InsertUserId INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @InsertUser, @InsertUserId OUTPUT
    
  	DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
    EXEC Market.Pricing.ValidateParameter_OwnerHandle @OwnerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT     


    /*
    Insert the new benefit at the desired rank.
    */
    INSERT 
    INTO    Marketing.MarketListingPreference
	    (
			OwnerID,
			IsDisplayed,
			MileageDeltaBelow,
			MileageDeltaAbove,
			PriceDeltaAbove,
			PriceDeltaBelow,
			InsertUserId,
			InsertDate,
			UpdateUserId,
			UpdateDate
	    )
    VALUES
	    (
	    	@OwnerID,
			@IsDisplayed,
			@MileageDeltaBelow,
			@MileageDeltaAbove,
			@PriceDeltaAbove,
			@PriceDeltaBelow,
			@InsertUserId,
			GETDATE(),
			@InsertUserId, --When we insert the insert user is also the update user
			GETDATE()
	    )
    
    SELECT @MarketListingPreferenceId = SCOPE_IDENTITY()


END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH

GO


