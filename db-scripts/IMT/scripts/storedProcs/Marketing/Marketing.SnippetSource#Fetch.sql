IF OBJECT_ID(N'[Marketing].[SnippetSource#Fetch]') IS NOT NULL
	DROP PROCEDURE [Marketing].[SnippetSource#Fetch]
GO

CREATE PROCEDURE [Marketing].[SnippetSource#Fetch]	
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch all records from the Marketing.SnippetSource table.
 *
 * History
 * ----------
 * 
 *  CGC 04/07/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Perform Fetch
------------------------------------------------------------------------------------------------

SELECT	
	[SnippetSourceId],
    [Name],
    [HasCopyright],
    [DefaultRank]
FROM
	[Marketing].[SnippetSource]
ORDER BY
    [DefaultRank] ASC

GO
