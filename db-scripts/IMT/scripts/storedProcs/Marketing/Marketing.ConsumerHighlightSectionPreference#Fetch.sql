IF OBJECT_ID(N'[Marketing].[ConsumerHighlightSectionPreference#Fetch]') IS NOT NULL
	DROP PROCEDURE [Marketing].[ConsumerHighlightSectionPreference#Fetch]
GO

CREATE PROCEDURE [Marketing].[ConsumerHighlightSectionPreference#Fetch]
	@OwnerHandle varchar(36)    
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch the record from the Marketing.ConsumerHighlightSectionPreference table for the given 
 *  owner.
 * 
 *	Parameters:
 *	@OwnerHandle
 *
 * History
 * ----------
 * 
 *  CGC 04/07/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out

------------------------------------------------------------------------------------------------
-- Perform Fetch
------------------------------------------------------------------------------------------------

SELECT	
	P.[ConsumerHighlightSectionId],
    S.[Name],
    P.[OwnerId],
    P.[Rank]    
FROM
	[Marketing].[ConsumerHighlightSectionPreference] P
JOIN
    [Marketing].[ConsumerHighlightSection] S ON S.[ConsumerHighlightSectionId] = P.[ConsumerHighlightSectionId]
WHERE
	[OwnerId] = @OwnerId
ORDER BY P.[Rank] ASC

GO
