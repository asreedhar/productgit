IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Marketing].[HighlightProviderInfoList#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Marketing].[HighlightProviderInfoList#Fetch]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [Marketing].[HighlightProviderInfoList#Fetch]

AS


/* --------------------------------------------------------------------
 * 
 * $Id: Marketing.HighlightProviderInfoList#Fetch.sql,v 1.1.4.1.2.2 2010/06/02 18:40:27 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Fetches the HighlightProviders
 * 
 * Input Parameters
 * ----------
 *
 *
 * Exceptions
 * ----------
 * 
 * -------------------------------------------------------------------- */

BEGIN TRY

    DECLARE @err INT, @errMsg VARCHAR(2000)

    DECLARE @Results TABLE(
	    HighlightProviderId INT NOT NULL,
	    Name VARCHAR(50),
	    SupportsEdits BIT NOT NULL,
	    SupportsDeletes BIT NOT NULL,
	    PRIMARY KEY (HighlightProviderId),
	    UNIQUE (Name))

    INSERT INTO @Results (HighlightProviderId, Name, SupportsEdits, SupportsDeletes)
    SELECT
	    HighlightProviderId,
	    Name,
	    SupportsEdits,
	    SupportsDeletes
    FROM    Marketing.HighlightProvider


    SELECT * FROM @Results

END TRY

BEGIN CATCH

	SELECT @err =ERROR_NUMBER(), @errMsg =ERROR_MESSAGE()
	RAISERROR(@errMsg, 16, 1)
	RETURN @err

END CATCH

