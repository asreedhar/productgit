IF OBJECT_ID(N'[Marketing].[ConsumerHighlightSectionPreference#Exists]') IS NOT NULL
	DROP PROCEDURE [Marketing].[ConsumerHighlightSectionPreference#Exists]
GO

CREATE PROCEDURE [Marketing].[ConsumerHighlightSectionPreference#Exists]    
	@OwnerHandle varchar(36)
AS

/* --------------------------------------------------------------------------------------------- 
 * 
 * Summary
 * ----------
 * 
 *	Check if any records exist in Marketing.ConsumerHighlightSectionPreference table for the 
 *  given owner.
 * 
 *	Parameters: 
 *	@OwnerHandle
 *
 * History
 * ----------
 *  
 * CGC  04/07/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out

------------------------------------------------------------------------------------------------
-- Perform Fetch
------------------------------------------------------------------------------------------------

SELECT 1 AS [Exists]
FROM 
    [Marketing].[ConsumerHighlightSectionPreference]
WHERE
    [OwnerId] = @OwnerId

GO
