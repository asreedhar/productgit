USE IMT
GO

IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'MarketListingVehiclePreference#Delete' AND SCHEMA_NAME(schema_id) = 'Marketing')
	BEGIN
		DROP  Procedure  [Marketing].[MarketListingVehiclePreference#Delete]
	END

GO

CREATE Procedure [Marketing].[MarketListingVehiclePreference#Delete]
	@MarketListingVehiclePreferenceId INT,
	@DeleteUser VARCHAR(80)
AS
/* --------------------------------------------------------------------
 * 
 * $Id:
 * 
 * Summary
 * -------
 * 
 * Deletes a MarketListingVehiclePreference
 * 
 * Input Parameters
 * ----------
 *
 * @MarketListingVehiclePreferenceId
 * @DeleteUser
 *
 * Exceptions
 *	50100 - Parameter IS NULL
 *  50106 - No such %s record with ID %d. Please reexecute with a more appropriate value.
 * ----------
 * -------------------------------------------------------------------- */


BEGIN TRY

-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


--Check for no nulls

	IF @MarketListingVehiclePreferenceId IS NULL
    BEGIN
	RAISERROR (50100,16,1,'MarketListingVehiclePreferenceId')
	RETURN @@ERROR
    END


	IF NOT EXISTS ( SELECT 1 FROM Marketing.MarketListingVehiclePreference WHERE MarketListingVehiclePreferenceId = @MarketListingVehiclePreferenceId)
    BEGIN
	RAISERROR (50106,16,1,'MarketListingVehiclePreference', @MarketListingVehiclePreferenceId)
	RETURN @@ERROR
    END
    

--Try to get a valid user for the delete
    DECLARE @DeleteUserId INT
    EXEC  dbo.ValidateParameter_MemberID#UserName  @DeleteUser, @DeleteUserId OUTPUT    
	

    DELETE 
    FROM    Marketing.MarketListingVehiclePreference
    WHERE MarketListingVehiclePreferenceId  = @MarketListingVehiclePreferenceId

    -- Success
    RETURN 0

END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH
GO

