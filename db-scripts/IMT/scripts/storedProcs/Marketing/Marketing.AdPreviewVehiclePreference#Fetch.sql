USE IMT
GO

IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'AdPreviewVehiclePreference#Fetch' AND SCHEMA_NAME(schema_id) = 'Marketing')
	BEGIN
		DROP  Procedure  [Marketing].[AdPreviewVehiclePreference#Fetch]
	END

GO

CREATE Procedure [Marketing].[AdPreviewVehiclePreference#Fetch]
	@vehicleHandle VARCHAR(36), @ownerHandle VARCHAR(36)
AS
/* --------------------------------------------------------------------
 * 
 * $Id:
 * 
 * Summary
 * -------
 * 
 * Fetches an existing AdPreviewVehiclePreference
 * 
 * Input Parameters
 * ----------
 *
 * @vehicleHandle
 * @OwnerHandle 
 * Output Parameters
 * ----------
 * 
 *
 * Exceptions
 *	50100 - Parameter IS NULL 
 * ----------
 * -------------------------------------------------------------------- */
BEGIN TRY


-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

	IF @vehicleHandle IS NULL
    BEGIN
		RAISERROR (50100,16,1,'vehicleHandle')
		RETURN @@ERROR
    END  
    
    
  	IF @OwnerHandle IS NULL
    BEGIN
		RAISERROR (50100,16,1,'OwnerHandle')
		RETURN @@ERROR
    END
    
--Let's get the vehicle id
--this will throw an exception if there is no such vehicle
	DECLARE @VehicleEntityTypeID INT, @VehicleEntityID INT
	EXEC Market.Pricing.ValidateParameter_VehicleHandle @vehicleHandle, @VehicleEntityTypeID OUT, @VehicleEntityID OUT

--Let's get the owner id
--this will throw an exception if there is no such user
	DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
    EXEC Market.Pricing.ValidateParameter_OwnerHandle @ownerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT 


   SELECT
	AdPreviewVehiclePreferenceId,
	CharacterLimit
   FROM Marketing.AdPreviewVehiclePreference p
   WHERE p.VehicleEntityID = @VehicleEntityID AND p.OwnerID = @OwnerID


END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH

GO


