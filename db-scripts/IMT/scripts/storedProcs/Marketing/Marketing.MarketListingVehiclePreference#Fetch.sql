USE IMT
GO

IF EXISTS (SELECT * FROM sys.procedures WHERE name = 'MarketListingVehiclePreference#Fetch' AND SCHEMA_NAME(schema_id) = 'Marketing')
	BEGIN
		DROP  Procedure  [Marketing].[MarketListingVehiclePreference#Fetch]
	END

GO

CREATE Procedure [Marketing].[MarketListingVehiclePreference#Fetch]
	@vehicleHandle VARCHAR(36), @ownerHandle VARCHAR(36), @offerPrice INT
AS
/* --------------------------------------------------------------------
 * 
 * $Id:
 * 
 * Summary
 * -------
 * 
 * Fetches an existing MarketListingVehiclePreference
 * 
 * Input Parameters
 * ----------
 *
 * @vehicleHandle
 * @OwnerHandle 
 * Output Parameters
 * ----------
 * 
 *
 * Exceptions
 *	50100 - Parameter IS NULL 
 * ----------
 * -------------------------------------------------------------------- */
BEGIN TRY


-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

	IF @vehicleHandle IS NULL
    BEGIN
		RAISERROR (50100,16,1,'vehicleHandle')
		RETURN @@ERROR
    END  
    
    
  	IF @OwnerHandle IS NULL
    BEGIN
		RAISERROR (50100,16,1,'OwnerHandle')
		RETURN @@ERROR
    END
    
--Let's get the vehicle id
--this will throw an exception if there is no such vehicle
	DECLARE @VehicleEntityTypeID INT, @VehicleEntityID INT
	EXEC Market.Pricing.ValidateParameter_VehicleHandle @vehicleHandle, @VehicleEntityTypeID OUT, @VehicleEntityID OUT

--Let's get the owner id
--this will throw an exception if there is no such user
	DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
    EXEC Market.Pricing.ValidateParameter_OwnerHandle @ownerHandle, @OwnerEntityTypeID OUT, @OwnerEntityID OUT, @OwnerID OUT 


	DECLARE @Mileage INT
		 
	IF @VehicleEntityTypeID = 1  --Inventory		
		 SELECT @Mileage =  MileageReceived FROM IMT.dbo.Inventory WHERE InventoryID = @VehicleEntityID		
	 ELSE IF @VehicleEntityTypeID = 5 --Listing
		 SELECT @Mileage =  Mileage From Market.Listing.Vehicle WHERE VehicleID = @VehicleEntityID		
	 ELSE
		RAISERROR (50500,16,1,'MarketListingVehiclePreference')

	DECLARE @MileageLow INT, @MileageHigh INT, @PriceLow INT, @PriceHigh INT, @HasMarketListingPreference BIT

	-- Check if we indeed have the preference for an owner

    IF EXISTS ( SELECT 1 FROM Marketing.MarketListingPreference WHERE OwnerID = @OwnerID)
		BEGIN
	--Fetch the information from the Marketing.MarketListingPreference according to the owner
		   SELECT
			@HasMarketListingPreference = 1,
			@MileageLow = @Mileage - mp.MileageDeltaBelow,
			@MileageHigh = @Mileage + mp.MileageDeltaAbove,
			@PriceLow = @offerPrice - mp.PriceDeltaBelow,
			@PriceHigh = @offerPrice + mp.PriceDeltaAbove			   
		   FROM Marketing.MarketListingPreference mp
		   WHERE mp.OwnerID = @OwnerID
		END
    ELSE
    --Fetch the default
		BEGIN
		  SELECT	
			@HasMarketListingPreference = 0,
			@MileageLow = @Mileage - mp.MileageDeltaBelow,
			@MileageHigh = @Mileage + mp.MileageDeltaAbove,
			@PriceLow = @offerPrice - mp.PriceDeltaBelow,
			@PriceHigh = @offerPrice + mp.PriceDeltaAbove				   
		 FROM Marketing.MarketListingPreference mp
		 WHERE mp.OwnerID IS null
		END


   SELECT
	mvp.MarketListingVehiclePreferenceId,
	mvp.IsDisplayed,
	mvp.ShowDealerName,
	@HasMarketListingPreference AS HasMarketListingPreference,	
	@MileageLow AS MileageLow,
	@MileageHigh AS MileageHigh,
	@PriceLow AS PriceLow,
	@PriceHigh AS PriceHigh,
	mvp.SortCode,
	mvp.SortAscending
   FROM Marketing.MarketListingVehiclePreference  mvp
   WHERE mvp.VehicleEntityID = @VehicleEntityID AND mvp.OwnerID = @OwnerID


END TRY

BEGIN CATCH
    EXEC dbo.sp_ErrorHandler
END CATCH

GO


