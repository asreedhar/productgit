IF OBJECT_ID(N'[Marketing].[SnippetSourcePreference#Fetch]') IS NOT NULL
	DROP PROCEDURE [Marketing].[SnippetSourcePreference#Fetch]
GO

CREATE PROCEDURE [Marketing].[SnippetSourcePreference#Fetch]
	@OwnerHandle varchar(36)    
AS

/* ---------------------------------------------------------------------------------------------
 * 
 * Summary
 * ----------
 * 
 *	Fetch all records from the Marketing.SnippetSourcePreference table for the given owner.
 * 
 *	Parameters:
 *	@OwnerHandle
 *
 * History
 * ----------
 * 
 *  CGC 04/07/2010  Create procedure.
 * 						
 * ------------------------------------------------------------------------------------------ */

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out

------------------------------------------------------------------------------------------------
-- Perform Fetch For Displayed Snippet Sources
------------------------------------------------------------------------------------------------

SELECT	
	SSP.[SnippetSourceId],
    MTS.[Name],
    MTS.[HasCopyright],
	SSP.[OwnerId],
	SSP.[IsDisplayed],
	SSP.[Rank]	
FROM
	[Marketing].[SnippetSourcePreference] SSP
JOIN
    [VehicleCatalog].[FirstLook].[MarketingTextSource] MTS ON MTS.[MarketingTextSourceID] = SSP.[SnippetSourceId]
WHERE
	SSP.[OwnerId] = @OwnerId
AND
    SSP.[IsDisplayed] = 1

------------------------------------------------------------------------------------------------
-- Perform Fetch For Hidden Snippet Sources
------------------------------------------------------------------------------------------------

SELECT	
	SSP.[SnippetSourceId],
    MTS.[Name],
    MTS.[HasCopyright],
	SSP.[OwnerId],
	SSP.[IsDisplayed],
	SSP.[Rank]	
FROM
	[Marketing].[SnippetSourcePreference] SSP
JOIN
    [VehicleCatalog].[FirstLook].[MarketingTextSource] MTS ON MTS.[MarketingTextSourceID] = SSP.[SnippetSourceId]
WHERE
	SSP.[OwnerId] = @OwnerId
AND
    SSP.[IsDisplayed] = 0
    
GO
