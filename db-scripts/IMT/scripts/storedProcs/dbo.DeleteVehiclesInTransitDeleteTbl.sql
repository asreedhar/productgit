IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'dbo.DeleteVehiclesInTransitDeleteTbl')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE [dbo].[DeleteVehiclesInTransitDeleteTbl]
GO

-- =============================================
-- Author:		Ali Jaffer
-- Create date: 7/12/2013
-- Description: Delete vehicles from IMT that are stored in 
-- =============================================
CREATE PROCEDURE [dbo].[DeleteVehiclesInTransitDeleteTbl]
AS 
    BEGIN 

 
        DECLARE @InventoryId INT
        DECLARE @BUID INT 
        DECLARE @BusinessUnitCode VARCHAR(10)

        SELECT TOP 1
                @InventoryId = InventoryId ,
                @BUID = bu.BusinessUnitId ,
                @BusinessUnitCode = BusinessUnitCode
        FROM    IMT.[dbo].[InTransitInventoryDelete] id
                JOIN IMT.[dbo].BusinessUnit bu ON id.BusinessUnitId = bu.BusinessUnitID
        WHERE   [StatusId] = 1
        ORDER BY InsertDate ASC


        WHILE ( @InventoryId IS NOT NULL ) 
            BEGIN
			
                BEGIN TRY
                    BEGIN TRANSACTION ofDeletingInv
    
                    DECLARE @BusinessUnitId INT
                    DECLARE @MyResult INT
				 
                    EXEC @MyResult = imt.dbo.DeleteDealershipInventory @BusinessUnitCode = @BusinessUnitCode,
                        @SalesOnly = 0, @InventoryID = @InventoryID,
                        @Debug = 0, @ParentLogID = NULL
                 
                    IF ( @MyResult = 0
                         AND @@ERROR = 0
                       ) 
                        BEGIN
                            UPDATE  IMT.[dbo].[InTransitInventoryDelete]
                            SET     [StatusId] = 2 ,
                                    DeleteDate = GETDATE()
                            WHERE   InventoryId = @InventoryId
                            COMMIT TRANSACTION ofDeletingInv
                        END
                    ELSE 
                        BEGIN 
                            IF ( XACT_STATE() = 1 ) 
                                ROLLBACK TRANSACTION ofDeletingInv
                            UPDATE  IMT.[dbo].[InTransitInventoryDelete]
                            SET     [StatusId] = 3
                            WHERE   InventoryId = @InventoryId                
					
                        END          
                END TRY
                BEGIN CATCH
			
                    PRINT ERROR_MESSAGE()     
            
                    IF ( XACT_STATE() = 1 ) 
                        BEGIN
                            ROLLBACK TRANSACTION ofDeletingInv
                        END
			
                    UPDATE  IMT.[dbo].[InTransitInventoryDelete]
                    SET     [StatusId] = 3
                    WHERE   InventoryId = @InventoryId 
                END CATCH

		
                SET @InventoryId = NULL
        
                SELECT TOP 1
                        @InventoryId = InventoryId ,
                        @BUID = bu.BusinessUnitId ,
                        @BusinessUnitCode = BusinessUnitCode
                FROM    IMT.[dbo].[InTransitInventoryDelete] id
                        JOIN IMT.[dbo].BusinessUnit bu ON id.BusinessUnitId = bu.BusinessUnitID
                WHERE   [StatusId] = 1
                ORDER BY InsertDate ASC

            END
    
    END

GO


