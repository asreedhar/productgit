/****** Object:  StoredProcedure [dbo].[InsertRepriceEvent]    Script Date: 01/15/2014 20:02:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InsertRepriceEvent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertRepriceEvent]
GO

CREATE proc [dbo].[InsertRepriceEvent]
--------------------------------------------------------------------------------
---	 * Inserting a reprice event has the following effects:
---	 * 	1) Updates the Inventory's ListPrice with the new value and set ListPriceLock
---	 * 	2) Creates a new RepriceEvent
---	 *  3) Updates the Inventory Item's Next Plan Date
---	 *  4) Creates all RepriceExport rows necessary for the Reprice Processor to 
---	 *  	update third parties (Internet Advertisers, DMS)
---	 ***** When the InitDMS flag = 1 (true), only steps 2 and 4 are executed
---Parmeters--------------------------------------------------------------------
--
@inventoryID		INT,
@beginDate		SMALLDATETIME,
@endDate		SMALLDATETIME,
@value 			INT,
@createdBy		VARCHAR (128),
@originalListPrice	INT,
@lastModified		DATETIME,
@Confirmed		BIT,
@source			TINYINT,
@InitDMS		BIT = 0
--
---History----------------------------------------------------------------------
--	
--	DM	07/14/2006	Initial design/development -- This 
--				procedure is only for creating reprice 
--				events.
--	DW	11/02/2006	Added orignalListPrice and lastModified params
--	DW	12/07/2006	Added logic for confirmed text
--				we need to know if on the reprice event whether or not the
--				user had to click the confirm button
--	DW	12/11/2007		moved cofirm logic into java layer, is now passed in as flag
--
--	BYF	20080211	Set ListPriceLock
--	BYF	20080521	Added InitDMS flag.  Originally wanted to insert straight
--					into RepriceExport table, but the dependency on AIP_Event
--					prevents that.
--	ffoiii	2009.03.17	Changed statement order to prevent deadlocking
--				Updated transaction and error handling logic
--------------------------------------------------------------------------------
AS
SET NOCOUNT ON
DECLARE @Rowcount INT
DECLARE	@Err INT
DECLARE	@sErrMsg VARCHAR(255)
DECLARE	@bLocalTran bit
DECLARE	@sProcname sysname
DECLARE @bTrue BIT
DECLARE @bFalse BIT
DECLARE @lReturn INT
DECLARE @eventCount INT
declare @newCatId INT
declare @newId INT

SELECT	@lReturn = 0,
    @newCatId=0,
    @eventCount = 0,
	@sProcName = object_name(@@procid),
	@bTrue = 1,
	@bFalse = 0,
	@sErrMsg = ''
SET	@bLocalTran = @bFalse
--END DEFAULT ENVIRONMENT AND VARIABLE SETTINGS

IF (@@TRANCOUNT = 0)
	BEGIN
		BEGIN TRAN @sProcName
		SELECT @bLocalTran = @bTrue
	END

DECLARE @newEventID INT,
	@nextPlanDate smalldatetime,
	@confirmedText	VARCHAR(20),
	@reminderEventID INT

SET @reminderEventID = 0

IF (@Confirmed = 0)
	SET @confirmedText = ''
ELSE
	SET @confirmedText = 'confirmed'

IF (@InitDMS = 1)
	SET @confirmedText = @confirmedText + ' DMS synch'
	
-- update list price and next plan date on inventory 
	SELECT @nextPlanDate = dbo.GetNextPlanDate(@InventoryID,0)
	
	UPDATE INVENTORY 
	SET ListPrice = @value,
		PlanReminderDate = @nextPlanDate,
		ListPriceLock = 1
	WHERE InventoryID = @inventoryID
	
	SELECT @err = @@ERROR 
	IF (@err <> 0)
		BEGIN
			SELECT @sErrMsg = 'Error updating dbo.Inventory.'
			GOTO Failed
		END

-- create a RepriceEvent in the AIP_Event table 

	INSERT INTO AIP_Event (InventoryID, AIP_EventTypeID, BeginDate, EndDate,
			Value, Created, CreatedBy, LastModified, LastModifiedBy, Notes2, Notes3, RepriceSourceID)
	VALUES 	(@inventoryID, 5, dbo.ToDate(@beginDate), dbo.ToDate(@endDate), 
			@value, @lastModified, @createdBy, @lastModified, @createdBy, @originalListPrice, @confirmedText, @source )
	SELECT @err = @@ERROR, @newEventID = SCOPE_IDENTITY()
	IF (@err <> 0)
		BEGIN
			SELECT @sErrMsg = 'Error inserting into AIP_Event.'
			GOTO Failed
		END

-- do not update reminder date when doing a 1 time synch back to dms
IF (@InitDMS = 0)
	BEGIN	
	
	    select @eventCount = count(1)
		from AIP_Event
		where inventoryId = @inventoryID and AIP_EventTypeID not in (11,12,14, 18) and EndDate is null
		
	-- create or update a Reminder Date Event 
		-- check if one exists
		SELECT @reminderEventID=AIP_EventID
		FROM AIP_Event
		WHERE 	inventoryId = @inventoryID 
			and (datediff(d,beginDate,GETDATE()) = 0)
			and AIP_EventTypeID = 14
		
	    -- if none exists, create one with AIP_EventTypeID=14 (REMINDER_DATE)
		IF (ISNULL(@reminderEventID,0) = 0) 
			BEGIN
				INSERT INTO AIP_Event (InventoryID, AIP_EventTypeID, BeginDate, EndDate, Created, CreatedBy, LastModified, LastModifiedBy)
				VALUES (@inventoryID, 14, dbo.ToDate(@beginDate), dbo.ToDate(@endDate), @lastModified, @createdBy, @lastModified, @createdBy)  
				
				-- RE:FOGBUGZ 27616 - do not add retail up doing just a reprice
				--      INSERT INTO AIP_Event (InventoryID, AIP_EventTypeID, BeginDate, EndDate, Created, CreatedBy, LastModified, LastModifiedBy)
				--      VALUES (@inventoryID, 15, dbo.ToDate(@beginDate), null, @lastModified, @createdBy, @lastModified, @createdBy)  
				
				
				-- RE:FOGBUGZ 27616 - Use this logic instead of the above commented out code
				IF (ISNULL(@eventCount,0) = 0) 
				  begin
				    -- find the last
					select top 1 @newCatId=AIP_EventTypeID
					from AIP_Event
					WHERE InventoryID = @inventoryID and AIP_EventTypeID not in (5,11,12,14, 18)
					order by BeginDate desc
					
					
					-- if we have history, use it
					if( ISNULL(@newCatId, 0) > 0 )
						begin
												
							--insert new item, with the last type
							INSERT INTO AIP_Event (InventoryID, AIP_EventTypeID, BeginDate, EndDate, Created, CreatedBy, LastModified, LastModifiedBy)
							VALUES (@inventoryID, @newCatId, dbo.ToDate(@beginDate), null, @lastModified, @createdBy, @lastModified, @createdBy)  
						end
				  end
				else
				------------------------------------------
				IF (ISNULL(@eventCount,0)=1) 
				  begin
				    -- find the last
					select top 1 @newCatId=AIP_EventTypeID
					from AIP_Event
					WHERE InventoryID = @inventoryID and AIP_EventTypeID not in (5,11,12,14, 18)
					order by BeginDate desc
					
					Select @newId=AIP_EventCategoryID from AIP_EventType where AIP_EventTypeID=@newCatId
				
					-- if we have history, use it
					if( ISNULL(@newCatId, 0) > 0 )
						begin

					If(@newId=1)
					set @newCatId=15
					Else If(@newId=2)
					set @newCatId=16
					Else If(@newId=3)
					set @newCatId=17
					Else If(@newId=4)
					set @newCatId=0
												
							--insert new item, with the last type
							INSERT INTO AIP_Event (InventoryID, AIP_EventTypeID, BeginDate, EndDate, Created, CreatedBy, LastModified, LastModifiedBy)
							VALUES (@inventoryID, @newCatId, dbo.ToDate(@beginDate), null, @lastModified, @createdBy, @lastModified, @createdBy)  
						end
				  end
				  -----------------------
				  else
					begin 
						IF (ISNULL(@eventCount,0) > 1) 
						begin
						    -- find the last
							select top 1 @newCatId=AIP_EventTypeID
							from AIP_Event
							WHERE InventoryID = @inventoryID and AIP_EventTypeID not in (5, 11,12,14, 18)
							order by BeginDate desc
						
						    -- close all of the open items
							update AIP_Event set EndDate=dbo.ToDate( GETDATE() )
							where InventoryID = @inventoryID and AIP_EventTypeID not in (11,12,14, 18) and EndDate is null

								
							if( ISNULL(@newCatId, 0) > 0 )
								begin
								Select @newId=AIP_EventCategoryID from AIP_EventType where AIP_EventTypeID=@newCatId
					If(@newId=1)
					set @newCatId=15
					Else If(@newId=2)
					set @newCatId=16
					Else If(@newId=3)
					set @newCatId=17
					Else If(@newId=4)
					set @newCatId=0
				
									--insert new item, with the last type		
									INSERT INTO AIP_Event (InventoryID, AIP_EventTypeID, BeginDate, EndDate, Created, CreatedBy, LastModified, LastModifiedBy)
									VALUES (@inventoryID, @newCatId, dbo.ToDate(@beginDate), null, @lastModified, @createdBy, @lastModified, @createdBy)  
								end
						end
					end
				
				
			END
		-- else update existing
		ELSE
			UPDATE	AIP_Event
			SET	LastModified=GETDATE(), LastModifiedBy=@createdBy
			WHERE 	AIP_EventID = @reminderEventID 
				
			
		SELECT @err = @@ERROR
		IF (@err <> 0)
			BEGIN
				SELECT @sErrMsg = 'Error updating AIP_Event.'
				GOTO Failed
			END

	END

-- load rows into the RepriceExport table 
	EXECUTE @lReturn = dbo.InsertRepriceExport 
			@InventoryID
			,@newEventID
			,@Value

	IF (@@ERROR <> 0 OR @lReturn < 0)
		BEGIN
			SELECT @sErrMsg = 'Error in dbo.InsertRepriceExport.'
			GOTO Failed
		END
	

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------
--all commands successful
IF(@bLocalTran = @bTrue)
	BEGIN
		COMMIT TRAN @sProcName
	END

--exit the procedure
RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------
--Error encountered
Failed:
IF (@bLocalTran = @bTrue)
	BEGIN
		ROLLBACK TRAN @sProcName
	END
IF (@sErrMsg = '')
	BEGIN
		SET @sErrMsg = 'Unspecified Error.'
	END
EXEC sp_LogEvent @Log_Code = 'E', @sp_Name = @sProcName, @Log_Text = @sErrMsg, @Originating_Log_Id = NULL
RAISERROR(@sErrmsg, 16, 1)
IF (@Err IS NULL)
	SET @Err = -2
RETURN @Err



GO


