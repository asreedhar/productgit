IF objectproperty(object_id('LithiaTrainingReset'), 'isProcedure') = 1
    DROP PROCEDURE LithiaTrainingReset

GO
/******************************************************************************
**
**  Procedure: LithiaTrainingReset
**  Description: Configure the Lithia training dealerships for another round
**   of training.
**
**  Return values:  none
** 
**  Input parameters:   none
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  06/13/2006  PKelley   Procedure created based Bill's ad-hoc work
**  07/14/2006  PKelley   Added in new table "AppraisalCustomer"
**  11/07/2006  PKelley   Added in new table "AppraisalFormOptions"
**  05/24/2007	BFung 	  Removed reference from InventoryThirdPartyVehicles
**	      	          and AppraisalThirdPartyVehicles and Replaced with
**			  and BookoutThirdPartyVehicles  
**  12/11/2007  PKelley   Added in rigamarole for AppraisalPhoto deletion
**  05/04/2009	WGH       Added in filter to preserve Appraisal for 
**                        (BusinessUnitID = 101306 AND VehicleID = 1468060)
**
***********************************************************************/
CREATE PROCEDURE dbo.LithiaTrainingReset

AS

    SET NOCOUNT on

    DECLARE
      @ThisProcedure     varchar(128)
     ,@LogMessage        varchar(255)
     ,@LogEventMasterID  int
     ,@WeeksToAge        tinyint
     ,@WeekDay           tinyint
     ,@Error             int


    SET @WeeksToAge = 1
      --  Number of weeks to "age" the data

    SET @WeekDay = datepart(dw, getdate())
      --  Current day of week (1=Sunday, 2=Monday, 7=Saturday [at least, this is the SQL default])

    SET @Error = 0

    SET @ThisProcedure = object_name(@@procid)  
    SET @LogMessage = 'Lithia training reset called for day of week value ' + cast(@WeekDay as varchar(10))
    SET @LogEventMasterID = null


    --  Initial call
    EXECUTE DBASTAT.dbo.Log_Event 'I', @ThisProcedure, @LogMessage, @LogEventMasterID OUTPUT

    IF @WeekDay not in (3,4)
        --  For fussy timing reasons, routines can only be run on Tuesdays and Wednesdays
        GOTO _Done_


    --  Configure and load table with all dealerships for the Lithia testing dealer group
    DECLARE @TrainingTargets TABLE
     (BizID  int  not null primary key)

    INSERT @TrainingTargets (BizID)
     select bu.BusinessUnitID
      from BusinessUnit bu
       inner join BusinessUnitRelationship bur
        on bur.BusinessUnitID = bu.BusinessUnitID
       inner join BusinessUnit buP
        on buP.BusinessUnitID = bur.ParentID
      where buP.BusinessUnitCode = 'LITHIATE01'


    IF @WeekDay = 3  --  Tuesday
     BEGIN
        --  On tuesdays only, reset and clean up the training dealership data.  Note that
        --  problems will crop up if this is run more than once a week.


        --  "Roll" dates  -----------------------------------------------------
        UPDATE Inventory
         set 
           InventoryReceivedDate = dateadd(wk, @WeeksToAge, InventoryReceivedDate)
          ,DeleteDt              = dateadd(wk, @WeeksToAge, DeleteDt)
         where BusinessUnitID in (select BizID from @TrainingTargets)

        UPDATE tbl_VehicleSale
         set DealDate = dateadd(wk, @WeeksToAge, vs.DealDate)
         from tbl_VehicleSale vs
          inner join Inventory inv
           on inv.InventoryID = vs.InventoryID
         where inv.BusinessUnitID in (select BizID from @TrainingTargets)
     END


    --  Update RunDayOfWeek  --------------------------------------------------
    --
    --  The CIA routine needs to pick up and process these dealerships before both the
    --  Tuesday and Wednesday training sessions.  The CIA process runs at 3:30am every
    --  day, so we just have to ensure that the correct RunDayOfWeek value is present
    --  before the CIA process runs.  Run this routine at 2:00am every day, and the
    --  correct values will be in place when required.
    UPDATE DealerPreference
     SET RunDayOfWeek = @WeekDay
     where BusinessUnitID in (select BizID from @TrainingTargets)


    --  Delete Appraisals  ------------------------------------------------

    --  Select appraisals to be dropped
    DECLARE @AppraisalTargets TABLE
     (ApprID  int  not null primary key)

    INSERT @AppraisalTargets (ApprID)
     select AppraisalID
      from Appraisals
      where BusinessUnitID in (select BizID from @TrainingTargets)
       --and DateCreated >= 'May 1, 2006'  --  As of June 13 2006, we are to delete ALL appraisals

	AND NOT (BusinessUnitID  = 101306
		AND VehicleID = 1468060
		)
		
		

    --  Select relevant third party vehicles to be dropped (this reduces query complexity)
    DECLARE @ThirdPartyTargets TABLE
     (ThirdID int  not null  primary key)

    INSERT @ThirdPartyTargets (ThirdID)
     select BTPV.ThirdPartyVehicleID
	  from AppraisalBookouts AB
	   join Bookouts B on AB.BookoutID = B.BookoutID
	   join BookoutThirdPartyVehicles BTPV on B.BookoutID = BTPV.BookoutID
	  where AB.AppraisalID in (select ApprID from @AppraisalTargets)

    --  Select relevant bookouts to be dropped (this also reduces query complexity)
    DECLARE	@BookoutTargets TABLE
     (BookID int  not null  primary key)

    INSERT @BookoutTargets
     select BookoutID
      from AppraisalBookouts
      where AppraisalID in (select ApprID from @AppraisalTargets)


    --  Appraisal tables
    SET @LogMessage = 'AppraisalActions'
    DELETE AppraisalActions
     where AppraisalID in (select ApprID from @AppraisalTargets)

    SELECT @Error = @@Error
    IF @Error <> 0 GOTO _Failed_

    SET @LogMessage = 'AppraisalValues'
    DELETE AppraisalValues
     where AppraisalID in (select ApprID from @AppraisalTargets)

    SELECT @Error = @@Error
    IF @Error <> 0 GOTO _Failed_

    SET @LogMessage = 'AppraisalWindowStickers'
    DELETE AppraisalWindowStickers
     where AppraisalID in (select ApprID from @AppraisalTargets)

    SELECT @Error = @@Error
    IF @Error <> 0 GOTO _Failed_

    SET @LogMessage = 'AppraisalBusinessUnitGroupDemand'
    DELETE AppraisalBusinessUnitGroupDemand
     where AppraisalID in (select ApprID from @AppraisalTargets)

    SELECT @Error = @@Error
    IF @Error <> 0 GOTO _Failed_
    
        --  Third Party Vehicle tables
    SET @LogMessage = 'BookoutThirdPartyVehicles'
    DELETE BookoutThirdPartyVehicles
     where ThirdPartyVehicleID in (select ThirdID from @ThirdPartyTargets)

    SELECT @Error = @@Error
    IF @Error <> 0 GOTO _Failed_

    SET @LogMessage = 'ThirdPartyVehicleOptionValues'
    DELETE ThirdPartyVehicleOptionValues
     from ThirdPartyVehicleOptionValues TPVOV
      inner join ThirdPartyVehicleOptions TPVO
       on TPVOV.ThirdPartyVehicleOptionID = TPVO.ThirdPartyVehicleOptionID
     where TPVO.ThirdPartyVehicleID in (select ThirdID from @ThirdPartyTargets)

    SELECT @Error = @@Error
    IF @Error <> 0 GOTO _Failed_

    SET @LogMessage = 'ThirdPartyVehicleOptions'
    DELETE ThirdPartyVehicleOptions
     where ThirdPartyVehicleID in (select ThirdID from @ThirdPartyTargets)

    SELECT @Error = @@Error
    IF @Error <> 0 GOTO _Failed_

    SET @LogMessage = 'ThirdPartyVehicles'
    DELETE ThirdPartyVehicles
     where ThirdPartyVehicleID in (select ThirdID from @ThirdPartyTargets)

    SELECT @Error = @@Error
    IF @Error <> 0 GOTO _Failed_


    --  Bookout tables
    SET @LogMessage = 'BookoutValues'
    DELETE BookoutValues
     from BookoutValues bv
      inner join BookoutThirdPartyCategories bc
       on bv.BookoutThirdPartyCategoryID = bc.BookoutThirdPartyCategoryID
     where bc.BookoutID in (select BookID from @BookoutTargets)

    SELECT @Error = @@Error
    IF @Error <> 0 GOTO _Failed_

    SET @LogMessage = 'BookoutThirdPartyCategories'
    DELETE BookoutThirdPartyCategories
     where BookoutID in (select BookID from @BookoutTargets)

    SELECT @Error = @@Error
    IF @Error <> 0 GOTO _Failed_

    SET @LogMessage = 'AppraisalBookouts'
    DELETE AppraisalBookouts
     where BookoutID in (select BookID from @BookoutTargets)

    SELECT @Error = @@Error
    IF @Error <> 0 GOTO _Failed_

    SET @LogMessage = 'AuditBookouts'
    DELETE AuditBookouts
     where BookoutID in (select BookID from @BookoutTargets)

    SELECT @Error = @@Error
    IF @Error <> 0 GOTO _Failed_

    SET @LogMessage = 'Bookouts'
    DELETE Bookouts
     where BookoutID in (select BookID from @BookoutTargets)

    SELECT @Error = @@Error
    IF @Error <> 0 GOTO _Failed_

    --  AppraisalCustomer added July 4, 2006
    SET @LogMessage = 'AppraisalCustomer'
    DELETE AppraisalCustomer
     where AppraisalID in (select ApprID from @AppraisalTargets)

    SELECT @Error = @@Error
    IF @Error <> 0 GOTO _Failed_

    --  AppraisalFormOptions added November 7, 2006
    SET @LogMessage = 'AppraisalFormOptions'
    DELETE AppraisalFormOptions
     where AppraisalID in (select ApprID from @AppraisalTargets)

    SELECT @Error = @@Error
    IF @Error <> 0 GOTO _Failed_

    --  AppraisalPhotos added December 11, 2007
    --  Must add to log to ensure the matching files are eventually deleted
    SET @LogMessage = 'AppraisalPhotos'
    INSERT PhotosToBeDeleted (PhotoID, Reason)
     select PhotoID, 'Lithia Training reset'
      from AppraisalPhotos
      where AppraisalID in (select ApprID from @AppraisalTargets)
       and PhotoID not in (select PhotoID from PhotosToBeDeleted)

    DELETE AppraisalPhotos
     where AppraisalID in (select ApprID from @AppraisalTargets)

    SELECT @Error = @@Error
    IF @Error <> 0 GOTO _Failed_


    SET @LogMessage = 'Appraisals'
    DELETE Appraisals
     where AppraisalID in (select ApprID from @AppraisalTargets)

    SELECT @Error = @@Error
    IF @Error <> 0 GOTO _Failed_


    --  Done  -----------------------------------------------------------------
    SET @LogMessage = 'Processing completed'
    EXECUTE DBASTAT.dbo.Log_Event 'I', @ThisProcedure, @LogMessage, @LogEventMasterID
    GOTO _Done_

    _Failed_:

    SET @LogMessage = 'Process failed while deleting from ' + @LogMessage
     + ' with error number ' + cast(@Error as varchar(10))
    EXECUTE DBASTAT.dbo.Log_Event 'E', @ThisProcedure, @LogMessage, @LogEventMasterID

RETURN @Error    

    _Done_:

RETURN 0
