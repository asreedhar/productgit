
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.MemberCollection#Fetch') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE dbo.MemberCollection#Fetch AS SELECT 1')
GO

ALTER PROCEDURE [dbo].[MemberCollection#Fetch]
	@DealerId INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.MemberCollection#Fetch.sql,v 1.4 2010/02/10 21:54:23 bfung Exp $
 * 
 * Summary
 * -------
 * 
 *	Return information (MemberID,FirstName,LastName, UserName)
 *	about all members for a given dealer.
 *
* Exceptions
 * ----------
 * 
 *    50100 DealerId IS NULL
 *    50106 No such dealer!
 *
 *
 *
 * History
 * ----------
 * 
 * MAK	05/26/2009	Create procedure.
 * MAK  06/30/2009  Added order by. 
 *						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

DECLARE @rc INT, @err INT

EXEC  @err =dbo.ValidateParameter_DealerID @DealerId

IF @err <> 0 GOTO Failed

DECLARE @MemberCollection TABLE
	(Id		INT		NOT NULL,
	FirstName	VARCHAR(20)	NOT NULL,
	LastName	VARCHAR(20)	NOT NULL,
	UserName	VARCHAR(80)	NOT NULL,
	MemberType	INT		NOT NULL)

INSERT
INTO	@MemberCollection
	(Id,
	FirstName,
	LastName,
	UserName,
	MemberType)
SELECT	M.MemberID,
	M.FirstName,
	M.LastName,
	M.Login,
	M.MemberType
FROM	IMT..Member M
JOIN	IMT..MemberAccess MB
ON	M.MemberID = MB.MemberID
WHERE	MB.BusinessUnitID =@DealerID

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @MemberCollection ORDER BY LastName, FirstName, UserName

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO
