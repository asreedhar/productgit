SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetPerformanceAnalyzerNewCar]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetPerformanceAnalyzerNewCar]
GO
CREATE           PROCEDURE dbo.usp_GetPerformanceAnalyzerNewCar  
   @DealerId         INT,  
   @NumberOfWeeks      INT,   
   @Make       VARCHAR(50),  
   @Model       VARCHAR(100),	-- Should be @Line as that is passed int from the app
   @VehicleTrim  VARCHAR(50),  
   @BodyTypeID                                  INT,  
   @Forecast       BIT = 0,  
   @GroupingParameter     NVARCHAR(50),  
   @InvType      INT  
 /***********************************************************  
**  
** usp_GetPerformanceAnalyzerNewCar  
** Stored Procedure  
**  
** 11NOV03 jmb cleaned up syntax on dates and declare stmts  
** 20JAN04 jmb changed all averaged values to decimal and add round at end  
** 31OCT05 PHK Factored out table vehiclebodystylemapping
** 14NOV07 WGH DESTROY THIS PROCEDURE AND REPLACE 
***********************************************************/  
AS  
  
set nocount on  
  
DECLARE @MinimumDealDate DATETIME,  
 @MaximumDealDate DATETIME,  
 @currentdate  DATETIME  
  
set @currentdate =  convert(varchar,getdate(),101)  
  
IF (@Forecast <> 0)  
  BEGIN  
 SET @MinimumDealDate = dateadd (d,-365,@currentdate)  
 SET @MaximumDealDate = dateadd (ww,@numberofweeks,convert(varchar,@MinimumDealDate,101))  
  END  
ELSE  
  BEGIN     
    SELECT @MinimumDealDate = dateadd (ww,-(@numberofweeks),@currentdate)  
    SELECT @MaximumDealDate = @currentdate  
  END  
  
  
create table #tmp  
  (  
  GroupingColumn  varchar (30)  not null,  
  TotalUnitsSold  int   not null,  
  TotalFrontEndGross decimal(15,2)  not null,  
  TotalVehicleMileage decimal(15,2)  not null,  
  TotalDaysToSale  int   not null,  
  TotalSalePrice  decimal(15,2)  not null,  
  TotalUnitCost  decimal(15,2)  not null,  
  TotalUnitsInStock int   not null,  
  TotalNoSales  int   not null,  
  TotalBackEndGross decimal(15,2)  not null  
  )  
INSERT INTO  
 #tmp  
  (  
  GroupingColumn  ,  
  TotalUnitsSold  ,  
  TotalFrontEndGross ,  
  TotalBackEndGross ,  
  TotalVehicleMileage ,  
  TotalDaysToSale  ,  
  TotalSalePrice  ,  
  TotalUnitCost  ,  
  TotalUnitsInStock ,  
  TotalNoSales    
  )  
 SELECT   
  v.BaseColor,   
    isnull(COUNT(*),0),  
  isnull(sum(vs.FrontEndGross), 0),  
  isnull(sum(vs.BackEndGross), 0),  
    isnull(sum(vs.VehicleMileage),0),  
  isnull(sum( DATEDIFF(dd,i.inventoryreceiveddate,vs.dealdate)),0),  
      isnull(SUM(vs.SalePrice),0),  
  0,  
  0,  
  0  
 FROM   
  VehicleSale vs  
  JOIN Inventory i  
   ON vs.InventoryId = i.InventoryId  
  JOIN Vehicle v  
   ON i.VehicleId = v.VehicleId  
  join BodyType vbs
  on v.BodyTypeID = vbs.BodyTypeID
   
 WHERE   
  vs.SaleDescription = 'R'  
  AND vs.DealDate >= @MinimumDealDate  
    AND vs.DealDate <= @MaximumDealDate  
    AND i.BusinessUnitId = @DealerId  
  AND i.InventoryType = @InvType  
    AND v.Make = @Make   
    AND v.Model = @Model  
    AND v.VehicleTrim = @VehicleTrim   
  AND vbs.BodyTypeID = @BodyTypeID  
      
 GROUP BY   
  v.BaseColor  
 union  
   
 SELECT    
  v.BaseColor,  
  0,  
  0,  
  0,  
  0,  
  0,  
  0,  
  ISNULL(SUM(i.UNITCOST),0),  
  isnull(count(*),0),  
  0  
   
 FROM   
  Inventory i  
  JOIN Vehicle v  
   ON i.VehicleId = v.VehicleId  
  join BodyType vbs
  on v.BodyTypeID = vbs.BodyTypeID
    
 WHERE   
  i.BusinessUnitId = @DealerId  
  AND i.InventoryActive = 1    
  AND i.InventoryType = @InvType  
    AND v.Make = @Make   
    AND v.Model = @Model  
  AND v.VehicleTrim = @VehicleTrim    
  AND vbs.BodyTypeID = @BodyTypeID  
 GROUP BY   
  v.BaseColor  
   
 union  
   
 select  
  v.BaseColor,  
  0,  
  0,  
  0,  
  0,  
  0,  
  0,  
  0,  
  0,  
  isnull(count(*),0)  
 from  
  vehiclesale vs  
  join inventory i  
  on vs.inventoryid = i.inventoryid  
  join vehicle v  
  on i.vehicleid = v.vehicleid  
  join BodyType BT
  on v.BodyTypeID = BT.BodyTypeID
 where  
  vs.DealDate >= @MinimumDealDate  
    AND vs.DealDate <= @MaximumDealDate  
  and datediff (dd,i.inventoryreceiveddate,vs.dealdate) > 30  
  and vs.SaleDescription = 'W'  
  and i.inventorytype = @invtype  
  and i.businessunitid = @dealerid  
    AND v.Make = @Make   
    AND v.Model = @Model  
  and v.vehicletrim = @vehicletrim  
  AND BT.BodyTypeID = @BodyTypeID  
 group by  
  v.basecolor  
  
wrapup:  
 select  
  GroupingColumn      ,  
  case  
  when sum(TotalUnitsSold) = 0 then 0  
  else cast(round(sum(TotalDaysToSale)/sum(TotalUnitsSold),0) as integer)  
  end        as AverageDays,  
  case   
  when sum(TotalUnitsSold) = 0 then 0  
  else cast(round(sum(TotalFrontEndGross)/sum(TotalUnitsSold),0) as integer)  
  end        as AverageGrossProfit,  
  case  
  when sum(TotalUnitsSold) = 0 then 0  
  else cast(round(sum(TotalVehicleMileage)/sum(TotalUnitsSold),0) as integer)  
  end        as AverageMileage,  
  cast(round(sum(TotalSalePrice),0) as integer)     as TotalRevenue,  
  cast(round(sum(TotalFrontEndGross),0) as integer)    as TotalGrossMargin,  
  cast(round(sum(TotalUnitCost),0) as integer)     as TotalInventoryDollars,  
  sum (TotalUnitsSold)      as UnitsSold,  
  sum (TotalUnitsInStock)      as UnitsInStock,  
  sum (TotalNoSales)      as No_Sales,  
  cast(round(sum(TotalBackEndGross),0) as integer)     as TotalBackEndGross,  
  case   
  when sum(TotalUnitsSold) = 0 then 0  
  else cast(round(sum(TotalBackEndGross)/sum(TotalUnitsSold),0) as integer)  
  end as AverageBackEndGrossProfit,  
  sum(TotalDaysToSale) as TotalDays,  
  sum(TotalFrontEndGross) as TotalFrontEndGross,  
  sum(TotalBackEndGross) as TotalBackEndGross  
    
  
from  
  #tmp  
group by  
  GroupingColumn  
order by  
  sum(TotalUnitsSold) desc  

GO
