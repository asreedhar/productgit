/****** Object:  StoredProcedure [dbo].[AppraisalLog]    Script Date: 05/17/2014 17:30:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AppraisalLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AppraisalLog]
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		Pradnya Sawant
-- Create date: 2014-05-09
-- Last Modified By : Vivek Rane
-- Last Modified Date : 2014-06-06
-- Description:	Returns Appraisal Closing Data in detail by Appraiser Name
-- =============================================
-- =============================================



CREATE  PROCEDURE [dbo].[AppraisalLog]

  @AppraiserName varchar(1000)
 ,@AppraisalType INT     -----0 for Not Traded In, 1 for Closed and 2 fro AllAppraisals
 ,@Period INT
 ,@DealerId INT
 
AS
BEGIN
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
/*  DEclare @AppraiserName varchar(1000)
 ,@AppraisalType INT     -----0 for Not Traded In, 1 for Closed and 2 fro AllAppraisals
 ,@Period INT
 ,@DealerId INT
 
set @Period = 1
set @DealerId = 100148
set @AppraisalType = 2
set @AppraiserName = 'All Appraisers' */

	SET ANSI_WARNINGS OFF;


		DECLARE @DateField DATE
		,@EndDate DATE
		,@LocalAppraiserName varchar(1000)
		,@LocalAppraisalType INT     -----0 for Not Traded In, 1 for Closed and 2 fro AllAppraisals
		,@LocalPeriod INT
		,@LocalDealerId INT
		DECLARE @Name VARCHAR(1000)='firstName lastName';

		Select @LocalAppraiserName=@AppraiserName,@LocalAppraisalType =@AppraisalType
		,@LocalPeriod =@Period ,@LocalDealerId =@DealerId;
		
		Select @DateField=StartDate,@EndDate=EndDate FROM IMT.dbo.GetDatesFromPeriodId(@LocalPeriod,1);


		IF @LocalAppraiserName='All Appraisers' AND @LocalAppraisalType=0 
		BEGIN

			SELECT  Distinct Convert(Date,AppraisalDate) As Analyzed,
			Convert(Date,InventoryReceivedDate) AS Received, InventoryVehicleLight
			,ModelYear, VehicleMake+' '+ VehicleLine+' '+ VehicleSeries AS MakeModelTrim,A.Mileage 
			,AppraisalValue, CustomerOffer, BookValue
			, CASE WHEN ISNULL(AppraiserName,@Name)=@Name OR AppraiserName='' OR ASCII(ltrim(rtrim(AppraiserName))) = 160
				THEN 'No Appraiser'
			ELSE AppraiserName END AS AppraiserName
			, MV.AveragePrice,VIN,'Not Closed' as [Status]
			FROM [HAL].dbo.TradeClosingRateReport tcr
			INNER JOIN IMT.dbo.Appraisals A ON A.AppraisalID=tcr.AppraisalID AND TCR.businessunitid=A.BusinessUnitId
			LEFT JOIN IMT.MMR.Vehicle MV ON MV.MMRVehicleID=A.MMRVehicleID
			WHERE  InventoryReceivedDate IS NULL
			and  tcr.BusinessUnitID=@LocalDealerId
			AND A.AppraisalSourceID=1
			AND CAST(tcr.AppraisalDate AS DATE)  BETWEEN @DateField AND @EndDate
			ORDER BY Analyzed ASC

		END

		ELSE IF @LocalAppraiserName ='No Appraiser'  AND @LocalAppraisalType=0 
		BEGIN
			SELECT  Distinct Convert(Date,AppraisalDate) As Analyzed,Convert(Date,InventoryReceivedDate) AS Received, InventoryVehicleLight,
			ModelYear, VehicleMake+' '+ VehicleLine+' '+ VehicleSeries AS MakeModelTrim,A.Mileage, 
			AppraisalValue, CustomerOffer, BookValue, 'No Appraiser' AS AppraiserName, MV.AveragePrice,VIN,'Not Closed' as [Status]
			FROM [HAL].dbo.TradeClosingRateReport tcr
			INNER JOIN IMT.dbo.Appraisals A ON A.AppraisalID=tcr.AppraisalID AND TCR.BusinessUnitId=A.BusinessUnitId
			LEFT JOIN IMT.MMR.Vehicle MV ON MV.MMRVehicleID=A.MMRVehicleID
			WHERE InventoryReceivedDate IS NULL
			AND  tcr.BusinessUnitID=@LocalDealerId
			AND A.AppraisalSourceID=1
			AND CAST(tcr.AppraisalDate AS DATE) BETWEEN @DateField AND @EndDate
			AND  (tcr.AppraiserName IS NULL  OR tcr.AppraiserName='firstName lastName' OR tcr.AppraiserName = '-' 
			OR  LTRIM(RTRIM(Tcr.AppraiserName))='' OR ASCII(ltrim(rtrim(TCR.AppraiserName))) = 160) 
			
			ORDER BY Analyzed ASC

		END

		ELSE IF @LocalAppraisalType=0 

		BEGIN

		SELECT Distinct Convert(Date,AppraisalDate) As Analyzed,Convert(Date,InventoryReceivedDate) AS Received, InventoryVehicleLight,
		  ModelYear, VehicleMake+' '+ VehicleLine+' '+ VehicleSeries AS MakeModelTrim,A.Mileage, 
		  AppraisalValue, CustomerOffer, BookValue, AppraiserName, MV.AveragePrice,VIN,'Not Closed' as [Status]
		FROM [HAL].dbo.TradeClosingRateReport tcr
		INNER JOIN IMT.dbo.Appraisals A ON A.AppraisalID=tcr.AppraisalID
		LEFT JOIN IMT.MMR.Vehicle MV ON MV.MMRVehicleID=A.MMRVehicleID
		WHERE A.AppraisalSourceID=1 
		AND InventoryReceivedDate IS NULL
		AND  tcr.BusinessUnitID=@LocalDealerId
		AND  tcr.AppraiserName=@LocalAppraiserName		
		AND CAST(tcr.AppraisalDate AS DATE) BETWEEN @DateField AND @EndDate

		ORDER BY Analyzed ASC

		END

		ELSE IF @LocalAppraiserName='All Appraisers' AND @LocalAppraisalType=1

		BEGIN
		SELECT Distinct Convert(Date,AppraisalDate) As Analyzed,Convert(Date,I.InventoryReceivedDate) AS Received, InventoryVehicleLight,
		  ModelYear, VehicleMake+' '+ VehicleLine+' '+ VehicleSeries AS MakeModelTrim,A.Mileage, 
		  AppraisalValue, CustomerOffer, BookValue
		  ,CASE WHEN ISNULL(AppraiserName,@Name)=@Name OR AppraiserName='' OR ASCII(ltrim(rtrim(AppraiserName))) = 160 THEN 'No Appraiser' ELSE AppraiserName END AS AppraiserName
		  , MV.AveragePrice,VIN,'Closed' as [Status]
		FROM [HAL].dbo.TradeClosingRateReport tcr
		INNER JOIN IMT.dbo.Appraisals A ON A.AppraisalID=tcr.AppraisalID AND A.BusinessUnitId=TCR.BusinessUnitId
		INNER JOIN  IMT.dbo.Inventory I ON A.BusinessUnitID = I.BusinessUnitID  and A.VehicleID = I.VehicleID  --AND I.TradeOrPurchase = 2
		INNER JOIN IMT.dbo.DealerPreference D on D.BusinessUnitID = tcr.BusinessUnitID
		LEFT JOIN IMT.MMR.Vehicle MV ON MV.MMRVehicleID=A.MMRVehicleID
		WHERE  
		 I.InventoryReceivedDate IS NOT null 
		 and  tcr.BusinessUnitID=@LocalDealerId
		 AND A.AppraisalSourceID=1
		 AND I.TradeOrPurchase = 2
		 AND (CAST(AppraisalDate AS DATE) BETWEEN @DateField AND @EndDate)   	  
		 AND TCR.AppraisalDate <= I.InventoryReceivedDate 
		 --AND TCR.AppraisalDate between DATEADD(DD,-D.AppraisalLookBackPeriod,I.InventoryReceivedDate) and I.InventoryReceivedDate 
		 AND cast(A.datemodified AS date) >= cast(DATEADD(DD,-D.AppraisalLookBackPeriod,I.InventoryReceivedDate) AS date ) -- and I.InventoryReceivedDate
		 ORDER BY Analyzed ASC

		END

		ELSE IF @LocalAppraiserName ='No Appraiser'  AND @LocalAppraisalType=1

		BEGIN

		SELECT Distinct Convert(Date,AppraisalDate) As Analyzed,Convert(Date,I.InventoryReceivedDate) AS Received, InventoryVehicleLight,
		  ModelYear, VehicleMake+' '+ VehicleLine+' '+ VehicleSeries AS MakeModelTrim,A.Mileage, 
		  AppraisalValue, CustomerOffer, BookValue, 'No Appraiser' AS AppraiserName, MV.AveragePrice,VIN,'Closed' as [Status]
		FROM [HAL].dbo.TradeClosingRateReport tcr
		INNER JOIN IMT.dbo.Appraisals A ON A.AppraisalID=tcr.AppraisalID
		INNER JOIN  IMT.dbo.Inventory I ON A.BusinessUnitID = I.BusinessUnitID  and A.VehicleID = I.VehicleID  --AND I.TradeOrPurchase = 2
		INNER JOIN IMT.dbo.DealerPreference D on D.BusinessUnitID = tcr.BusinessUnitID
		LEFT JOIN IMT.MMR.Vehicle MV ON MV.MMRVehicleID=A.MMRVehicleID 
		WHERE    
		I.InventoryReceivedDate IS NOT null 
		and  tcr.BusinessUnitID=@LocalDealerId
		AND A.AppraisalSourceID=1
		AND I.TradeOrPurchase = 2
		AND (CAST(AppraisalDate AS DATE) BETWEEN @DateField AND @EndDate)   	  
		AND TCR.AppraisalDate <= I.InventoryReceivedDate 
		--AND TCR.AppraisalDate between DATEADD(DD,-D.AppraisalLookBackPeriod,I.InventoryReceivedDate) and I.InventoryReceivedDate 
		AND cast(A.datemodified AS date) >= cast(DATEADD(DD,-D.AppraisalLookBackPeriod,I.InventoryReceivedDate) AS date ) -- and I.InventoryReceivedDate
		AND (tcr.AppraiserName IS NULL OR LTRIM(RTRIM(tcr.AppraiserName))='' OR tcr.AppraiserName='firstName lastName' OR tcr.AppraiserName = '-' OR ASCII(ltrim(rtrim(AppraiserName))) = 160) 
		ORDER BY Analyzed ASC
		END

		ELSE IF @LocalAppraisalType=1
		BEGIN

		SELECT Distinct Convert(Date,AppraisalDate) As Analyzed,Convert(Date,I.InventoryReceivedDate) AS Received, InventoryVehicleLight,
		  ModelYear, VehicleMake+' '+ VehicleLine+' '+ VehicleSeries AS MakeModelTrim,A.Mileage, 
		  AppraisalValue, CustomerOffer, BookValue, tcr.AppraiserName, MV.AveragePrice,VIN,'Closed' as [Status]
		FROM [HAL].dbo.TradeClosingRateReport tcr
		INNER JOIN IMT.dbo.Appraisals A ON A.AppraisalID=tcr.AppraisalID
		INNER JOIN  IMT.dbo.Inventory I ON A.BusinessUnitID = I.BusinessUnitID  and A.VehicleID = I.VehicleID  --AND I.TradeOrPurchase = 2
		INNER JOIN IMT.dbo.DealerPreference D on D.BusinessUnitID = tcr.BusinessUnitID
		LEFT JOIN IMT.MMR.Vehicle MV ON MV.MMRVehicleID=A.MMRVehicleID
		WHERE I.InventoryReceivedDate IS NOT null 	
			and  tcr.BusinessUnitID=@LocalDealerId		
			AND I.TradeOrPurchase = 2
			AND A.AppraisalSourceID=1
			AND (CAST(AppraisalDate AS DATE) BETWEEN @DateField AND @EndDate)   	  
			AND TCR.AppraisalDate <= I.InventoryReceivedDate 
			--AND TCR.AppraisalDate between DATEADD(DD,-D.AppraisalLookBackPeriod,I.InventoryReceivedDate) and I.InventoryReceivedDate 
			AND cast(A.datemodified AS date) >= cast(DATEADD(DD,-D.AppraisalLookBackPeriod,I.InventoryReceivedDate) AS date ) -- and I.InventoryReceivedDate
			AND tcr.AppraiserName=@LocalAppraiserName
		ORDER BY Analyzed ASC


		END
        -- Here we may need to add Inventory table with Left join. With all nested If's When we select all Apprisals (Closed and not closed)
		ELSE IF @LocalAppraiserName='All Appraisers' AND @LocalAppraisalType=2 

		BEGIN

		SELECT  Distinct Convert(Date,AppraisalDate) As Analyzed,Convert(Date,I.InventoryReceivedDate) AS Received, InventoryVehicleLight,
		  ModelYear, VehicleMake+' '+ VehicleLine+' '+ VehicleSeries AS MakeModelTrim,A.Mileage, 
		  AppraisalValue, CustomerOffer, BookValue
		  , CASE WHEN ISNULL(AppraiserName,@Name)=@Name OR AppraiserName='' OR ASCII(ltrim(rtrim(AppraiserName))) = 160 
				THEN 'No Appraiser' ELSE AppraiserName END AS AppraiserName
		  , MV.AveragePrice,VIN,
		  CASE WHEN I.InventoryReceivedDate IS NOT NULL   THEN 'Closed'   ELSE 'Not Closed'    END as [Status]
			FROM [HAL].dbo.TradeClosingRateReport tcr
			INNER JOIN IMT.dbo.Appraisals A ON A.AppraisalID=tcr.AppraisalID AND TCR.businessUnitid=A.BusinessUnitId
			LEFT JOIN IMT.dbo.DealerPreference D on D.BusinessUnitID = tcr.BusinessUnitID --AND   TCR.AppraisalDate >=DATEADD(DD,-D.AppraisalLookBackPeriod,TCR.InventoryReceivedDate)
			LEFT JOIN  IMT.dbo.Inventory I ON A.BusinessUnitID = I.BusinessUnitID  and A.VehicleID = I.VehicleID  AND I.TradeOrPurchase = 2 AND I.InventoryReceivedDate IS NOT null  AND TCR.AppraisalDate <= I.InventoryReceivedDate  AND cast(A.datemodified AS date) >= cast(DATEADD(DD,-D.AppraisalLookBackPeriod,I.InventoryReceivedDate) AS date ) -- and I.InventoryReceivedDate --AND TCR.AppraisalDate between DATEADD(DD,-D.AppraisalLookBackPeriod,I.InventoryReceivedDate) and I.InventoryReceivedDate 
	  
			LEFT JOIN IMT.MMR.Vehicle MV ON MV.MMRVehicleID=A.MMRVehicleID
			WHERE  tcr.BusinessUnitID=@LocalDealerId
			AND A.AppraisalSourceID=1 
			AND CAST(tcr.AppraisalDate AS DATE) BETWEEN @DateField AND @EndDate
			ORDER BY Analyzed ASC
		END

		ELSE IF @LocalAppraiserName ='No Appraiser'  AND @LocalAppraisalType=2

		BEGIN

		SELECT Distinct Convert(Date,AppraisalDate) As Analyzed,Convert(Date,I.InventoryReceivedDate) AS Received, InventoryVehicleLight,
		  ModelYear, VehicleMake+' '+ VehicleLine+' '+ VehicleSeries AS MakeModelTrim,A.Mileage, 
		  AppraisalValue, CustomerOffer, BookValue, 'No Appraiser' AS AppraiserName, MV.AveragePrice,VIN,
		  CASE 
		   WHEN I.InventoryReceivedDate IS NOT NULL
		   THEN 'Closed'
		   ELSE 'Not Closed'
			END as [Status]
		FROM [HAL].dbo.TradeClosingRateReport tcr
		INNER JOIN IMT.dbo.Appraisals A ON A.AppraisalID=tcr.AppraisalID
		LEFT JOIN IMT.dbo.DealerPreference D on D.BusinessUnitID = tcr.BusinessUnitID --AND   TCR.AppraisalDate >=DATEADD(DD,-D.AppraisalLookBackPeriod,TCR.InventoryReceivedDate)
		LEFT JOIN  IMT.dbo.Inventory I ON A.BusinessUnitID = I.BusinessUnitID  and A.VehicleID = I.VehicleID  AND I.TradeOrPurchase = 2 AND I.InventoryReceivedDate IS NOT null  AND TCR.AppraisalDate <= I.InventoryReceivedDate  AND cast(A.datemodified AS date) >= cast(DATEADD(DD,-D.AppraisalLookBackPeriod,I.InventoryReceivedDate) AS date ) -- and I.InventoryReceivedDate --AND TCR.AppraisalDate between DATEADD(DD,-D.AppraisalLookBackPeriod,I.InventoryReceivedDate) and I.InventoryReceivedDate 
		LEFT JOIN IMT.MMR.Vehicle MV ON MV.MMRVehicleID=A.MMRVehicleID
		WHERE   
			tcr.BusinessUnitID=@LocalDealerId
			AND A.AppraisalSourceID=1
			AND	CAST(tcr.AppraisalDate AS DATE) BETWEEN @DateField AND @EndDate
			AND  (tcr.AppraiserName IS NULL OR LTRIM(RTRIM(tcr.AppraiserName))=''  OR tcr.AppraiserName='firstName lastName' OR tcr.AppraiserName = '-' OR ASCII(ltrim(rtrim(AppraiserName))) = 160)
		ORDER BY Analyzed ASC
		END

		ELSE IF @LocalAppraisalType=2
		BEGIN
		SELECT  Distinct Convert(Date,AppraisalDate) As Analyzed,Convert(Date,I.InventoryReceivedDate) AS Received, InventoryVehicleLight,
		  ModelYear, VehicleMake+' '+ VehicleLine+' '+ VehicleSeries AS MakeModelTrim,A.Mileage, 
		  AppraisalValue, CustomerOffer, BookValue, tcr.AppraiserName, MV.AveragePrice,VIN,
		  CASE 
		   WHEN I.InventoryReceivedDate IS NOT NULL
		   THEN 'Closed'
		   ELSE 'Not Closed'
			END as [Status]
		FROM [HAL].dbo.TradeClosingRateReport tcr
		INNER JOIN IMT.dbo.Appraisals A ON A.AppraisalID=tcr.AppraisalID
		LEFT JOIN IMT.dbo.DealerPreference D on D.BusinessUnitID = tcr.BusinessUnitID --AND   TCR.AppraisalDate >=DATEADD(DD,-D.AppraisalLookBackPeriod,TCR.InventoryReceivedDate)
		LEFT JOIN  IMT.dbo.Inventory I ON A.BusinessUnitID = I.BusinessUnitID  and A.VehicleID = I.VehicleID  AND I.TradeOrPurchase = 2 AND I.InventoryReceivedDate IS NOT null  AND TCR.AppraisalDate <= I.InventoryReceivedDate AND cast(A.datemodified AS date) >= cast(DATEADD(DD,-D.AppraisalLookBackPeriod,I.InventoryReceivedDate) AS date ) -- and I.InventoryReceivedDate  --AND TCR.AppraisalDate between DATEADD(DD,-D.AppraisalLookBackPeriod,I.InventoryReceivedDate) and I.InventoryReceivedDate 
		LEFT JOIN IMT.MMR.Vehicle MV ON MV.MMRVehicleID=A.MMRVehicleID 
		WHERE   tcr.BusinessUnitID=@LocalDealerId
		AND A.AppraisalSourceID=1
		AND CAST(tcr.AppraisalDate AS DATE) BETWEEN @DateField AND @EndDate
		AND  tcr.AppraiserName=@LocalAppraiserName
		ORDER BY Analyzed ASC
		END

END

-------------------------------------------------------------------




GO


