IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Utility.ExecQueue#LoadCIA') AND type in (N'P', N'PC'))
DROP PROCEDURE Utility.ExecQueue#LoadCIA
GO
CREATE PROC Utility.ExecQueue#LoadCIA
------------------------------------------------------------------------------
--
--	Loads the table Utility.ExecQueue with BUIDs for CIA processor runs 
--	on AUX
--
---Parameters------------------------------------------------------------------
--
@BusinessUnitID	INT = NULL,	-- Optional BusinessUnitID filter
@RunDayOfWeek	INT = NULL,	-- Optional @RunDayOfWeek. If not passed,
				-- @BusinessUnitID MUST be passed or it will be 
				-- derived from the current day of the week
@ParentLogID	INT = NULL,	-- Optional Originating, or parent, log ID
@Debug		BIT = 0
--
---History---------------------------------------------------------------------
--	
--	12/07/2012	WGH	Initial design/development
--
-------------------------------------------------------------------------------
AS 
SET NOCOUNT ON   
  
DECLARE @Step VARCHAR(255),
        @ProcName SYSNAME

BEGIN TRY
	SET @ProcName = OBJECT_SCHEMA_NAME(@@procid) + '.' + OBJECT_NAME(@@procid)

	SET @Step = 'Started ' + ISNULL(', @BusinessUnitID = ' + CAST(@BusinessUnitID AS VARCHAR), '') + ISNULL(', @RunDayOfWeek = ' + CAST(@RunDayOfWeek AS VARCHAR), '')

	EXEC sp_LogEvent 'I', @ProcName, @Step, @ParentLogID OUTPUT

	IF @BusinessUnitID IS NULL AND @RunDayOfWeek IS NULL 
		SET @RunDayOfWeek = DATEPART(dw, GETDATE())

	--------------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Insert Missing Grid Thresholds and Values'
	--------------------------------------------------------------------------------------------------------------------------------

	INSERT 
	INTO	dbo.DealerGridThresholds (BusinessUnitID, firstValuationThreshold, secondValuationThreshold, thirdValuationThreshold, fourthValuationThreshold, firstDealThreshold, secondDealThreshold, thirdDealThreshold, fourthDealThreshold)
	      
	SELECT	DP.BusinessUnitID, firstValuationThreshold, secondValuationThreshold, thirdValuationThreshold, fourthValuationThreshold, firstDealThreshold, secondDealThreshold, thirdDealThreshold, fourthDealThreshold

	FROM	dbo.DealerGridThresholds DGT   
		CROSS JOIN dbo.DealerPreference DP

	WHERE	DGT.BusinessUnitID = 100001
		AND DP.BusinessUnitID NOT IN (	SELECT	BusinessUnitID
						FROM	dbo.DealerGridThresholds
						)
	INSERT
	INTO	dbo.DealerGridValues (BusinessUnitID, LightValue, TypeValue, indexKey)
	SELECT	DP.BusinessUnitID, LightValue, TypeValue, indexKey
	FROM	dbo.DealerGridValues DGV
		CROSS JOIN dbo.DealerPreference DP
	WHERE	DGV.BusinessUnitID = 100001
		AND DP.BusinessUnitID NOT IN (	SELECT	BusinessUnitID
						FROM	dbo.DealerGridValues
						)
						
	--------------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Insert BusinessUnitIDs into Utility.ExecQueue for processing'
	--------------------------------------------------------------------------------------------------------------------------------
	
	INSERT  
	INTO	Utility.ExecQueue (BusinessUnitId)
	
	SELECT  DP.BusinessUnitID
	FROM    dbo.DealerPreference DP
		INNER JOIN dbo.BusinessUnit BU ON DP.BusinessUnitID = BU.BusinessUnitID
		INNER JOIN dbo.DealerUpgrade DU ON DP.BusinessUnitID = DU.BusinessUnitID
	
	WHERE   BU.Active = 1
		AND DU.DealerUpgradeCD = 3
		AND ISNULL(@BusinessUnitID, DP.BusinessUnitID) = DP.BusinessUnitID
		AND ISNULL(@RunDayOfWeek, DP.RunDayOfWeek) = DP.RunDayOfWeek
		AND NOT EXISTS(	SELECT	1						-- no need to load if already pending
				FROM	Utility.ExecQueue EQ
				WHERE	DP.BusinessUnitID = EQ.BusinessUnitId
					AND EQ.ExecStatusId = 1	-- Pending
					)	
					
	SET @Step = CAST(@@ROWCOUNT AS VARCHAR) + ' Business Units inserted into Utility.ExecQueue for processing. Done.'
	EXEC sp_LogEvent 'I', @ProcName, @Step, @ParentLogID


END TRY
BEGIN CATCH

	EXEC sp_ErrorHandler @LogID = @ParentLogID, @Message = @Step
	
END CATCH

GO

EXEC dbo.sp_SetStoredProcedureDescription 
	'Utility.ExecQueue#LoadCIA',
	'Loads the table Utility.ExecQueue table with BUIDs for CIA processor runs on AUX'
