
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'Utility.Photos#DeleteByInactiveInventory') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Utility.Photos#DeleteByInactiveInventory
GO

CREATE PROC Utility.Photos#DeleteByInactiveInventory
----------------------------------------------------------------------------------------------------
--
--	Utility proc to delete the N oldest photos in the database wired to inactive inventory that
--	were created before the associated dealer's unwind threshold date.
--
---Parameters---------------------------------------------------------------------------------------
--
@Photos		INT = 100,
@Mode		BIT = 0, 		-- 0 = nightly, 1 =  delete photos only 
@ParentLogID	INT = NULL OUTPUT
--
---History------------------------------------------------------------------------------------------
--	
--	WGH	03/08/2010	Initial version
--
----------------------------------------------------------------------------------------------------	

AS

SET NOCOUNT ON 
DECLARE @Message VARCHAR(255)

----------------------------------------------------------------------------------------------------	
--	Update ImageModifiedDate to NULL for any active inventory w/o photos 
----------------------------------------------------------------------------------------------------	

IF @Mode = 0

	UPDATE	I
	SET	ImageModifiedDate = NULL
	FROM	IMT.dbo.Inventory I
	WHERE	ImageModifiedDate IS NOT NULL
		AND InventoryActive = 1
		AND NOT EXISTS (SELECT 1 FROM dbo.InventoryPhotos IP WHERE I.InventoryID = IP.InventoryID)
		
----------------------------------------------------------------------------------------------------	

SELECT	TOP (@Photos) P.PhotoID
INTO	#Photos
FROM	dbo.Photos P
	INNER JOIN dbo.InventoryPhotos IP ON P.PhotoID = IP.PhotoID
	INNER JOIN dbo.Inventory I ON IP.InventoryID = I.InventoryID
	INNER JOIN dbo.DealerPreference DP ON I.BusinessUnitID = DP.BusinessUnitID
WHERE	I.InventoryActive = 0 AND I.DeleteDt < DATEADD(dd,- UnwindDaysThreshold, IMT.dbo.ToDate(GETDATE()))
ORDER
BY	P.PhotoID		

SET @Message = CAST(@@ROWCOUNT AS VARCHAR) + ' photo delete candidate(s) found'

BEGIN TRY

	BEGIN TRANSACTION
		
	DELETE	IP
	FROM	dbo.InventoryPhotos IP
		INNER JOIN #Photos P ON IP.PhotoID = P.PhotoID

	SET @Message = @Message +', ' + CAST(@@ROWCOUNT AS VARCHAR) + ' row(s) deleted in dbo.InventoryPhotos'
		
	DELETE	P
	FROM	dbo.Photos P
		INNER JOIN #Photos D ON P.PhotoID = d.PhotoID

	SET @Message = @Message +', ' + CAST(@@ROWCOUNT AS VARCHAR) + ' row(s) deleted in dbo.Photos'			

	COMMIT TRANSACTION
	
	EXEC dbo.sp_LogEvent 'I', 'Utility.Photos#DeleteByInactiveInventory', @Message, @ParentLogID OUTPUT
	 
END TRY
BEGIN CATCH
	IF XACT_STATE() <> 0
		ROLLBACK TRANSACTION

	EXEC dbo.sp_ErrorHandler @LogID = @ParentLogID
	
END CATCH		
	
GO

EXEC dbo.sp_SetStoredProcedureDescription 'Utility.Photos#DeleteByInactiveInventory',
'Utility proc to delete the N oldest photos in the database wired to inactive inventory that were created before the associated dealer''s unwind threshold date' 
GO	