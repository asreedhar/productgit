IF objectproperty(object_id('Utility.ArchiveAIP_Event'), 'isProcedure') = 1
    DROP PROCEDURE Utility.ArchiveAIP_Event

GO
CREATE PROC Utility.ArchiveAIP_Event
----------------------------------------------------------------------------------------------------
--
--	Moves AIP_Event history to the Archive database
--
---Parameters---------------------------------------------------------------------------------------
--
@MaxToArchive   INT = 5000
--
---History------------------------------------------------------------------------------------------
--	
--	WGH	07/01/2010
--			
----------------------------------------------------------------------------------------------------
AS
SET NOCOUNT ON
DECLARE @Step VARCHAR(255)

BEGIN TRY

	--------------------------------------------------------------------------------------------
	SET @Step = 'Create temp table #EventIDs'
	--------------------------------------------------------------------------------------------

	CREATE TABLE #EventIDs (AIP_EventID INT)

	--------------------------------------------------------------------------------------------
	SET @Step = 'Insert Archive.dbo.AIP_Event_Archive'
	--------------------------------------------------------------------------------------------
	
	INSERT 
	INTO	Archive.dbo.AIP_Event_Archive (AIP_EventID, InventoryID, AIP_EventTypeID, BeginDate, EndDate, Notes, Notes2, ThirdPartyEntityID, Value, Created, CreatedBy, LastModified, LastModifiedBy, VersionNumber, UserBeginDate, UserEndDate, Notes3, RepriceSourceID)
	
	OUTPUT	INSERTED.AIP_EventID INTO #EventIDs
	
	SELECT	TOP(@MaxToArchive) AIP_EventID, E.InventoryID, AIP_EventTypeID, BeginDate, EndDate, Notes, Notes2, ThirdPartyEntityID, Value, Created, CreatedBy, LastModified, LastModifiedBy, E.VersionNumber, UserBeginDate, UserEndDate, Notes3, RepriceSourceID
	FROM	dbo.AIP_Event E WITH (NOLOCK)
		INNER JOIN dbo.Inventory I WITH (NOLOCK) ON E.InventoryID = I.InventoryID
		INNER JOIN dbo.DealerPreference DP WITH (NOLOCK) ON I.BusinessUnitID = DP.BusinessUnitID
	WHERE	I.InventoryActive = 0
		AND I.DeleteDt < DATEADD(dd,- UnwindDaysThreshold, IMT.dbo.ToDate(GETDATE()))
	ORDER
	BY	E.AIP_EventID 
	

	--------------------------------------------------------------------------------------------
	SET @Step = 'Insert Archive.dbo.RepriceExport_Archive'
	--------------------------------------------------------------------------------------------
		
	INSERT
	INTO	Archive.dbo.RepriceExport_Archive (RepriceExportID, RepriceEventID, RepriceExportStatusID, ThirdPartyEntityID, FailureReason, FailureCount, DateCreated, DateSent, StatusModifiedDate)
	SELECT	RepriceExportID, RepriceEventID, RepriceExportStatusID, ThirdPartyEntityID, FailureReason, FailureCount, DateCreated, DateSent, StatusModifiedDate
	FROM	dbo.RepriceExport RX
		INNER JOIN #EventIDs E ON RX.RepriceEventID = E.AIP_EventID

	--------------------------------------------------------------------------------------------
	SET @Step = 'Delete dbo.RepriceExport'
	--------------------------------------------------------------------------------------------
			
	DELETE	RX
	FROM	dbo.RepriceExport RX
		INNER JOIN #EventIDs E ON RX.RepriceEventID = E.AIP_EventID	

	--------------------------------------------------------------------------------------------
	SET @Step = 'Delete dbo.AIP_Event'
	--------------------------------------------------------------------------------------------
				
	DELETE	E
	FROM	dbo.AIP_Event E
		INNER JOIN #EventIDs A ON E.AIP_EventID = A.AIP_EventID
		
	
END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler @Message = @Step

END CATCH
GO

