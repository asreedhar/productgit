
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'Utility.Photos#FetchDeletesByInactiveInventory') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Utility.Photos#FetchDeletesByInactiveInventory
GO

CREATE PROC Utility.Photos#FetchDeletesByInactiveInventory
----------------------------------------------------------------------------------------------------
--
--	Fetches the local paths of photo files that are still in the system, "wired" to inactive 
--	inventory, and can be deleted.
--
---Parameters---------------------------------------------------------------------------------------
--
--	None
--
---History------------------------------------------------------------------------------------------
--	
--	WGH	03/30/2011	Initial version
--
----------------------------------------------------------------------------------------------------	

AS

SET NOCOUNT ON

CREATE TABLE #Numbers (N INT)
INSERT
INTO	#Numbers
SELECT	Value
FROM	Market.Listing.GetIntegerRange(1,5000) GIR


SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

DECLARE @OptionDelimiter CHAR(1)
SET @OptionDelimiter = ','

SELECT	P.PhotoURL
FROM	dbo.Photos P
	INNER JOIN dbo.InventoryPhotos IP ON P.PhotoID = IP.PhotoID
	INNER JOIN dbo.Inventory I ON IP.InventoryID = I.InventoryID
	INNER JOIN dbo.DealerPreference DP ON I.BusinessUnitID = DP.BusinessUnitID
WHERE	I.InventoryActive = 0 AND I.DeleteDt < DATEADD(dd,- DP.UnwindDaysThreshold, DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0))
	AND P.PhotoStatusCode IN (1,5,6,7)	-- these should all be local
UNION 

SELECT	REPLACE(PhotoURL, 'http://www.firstlook.biz/digitalimages/', '')
FROM (	SELECT	LTRIM(RTRIM(LEFT(SUBSTRING(@OptionDelimiter+IP.photoURLs+@OptionDelimiter,N+1,CHARINDEX(@OptionDelimiter,@OptionDelimiter+IP.photoURLs+@OptionDelimiter,N+1)-N-1), 200))) AS PhotoURL
	FROM	#Numbers N
		CROSS JOIN (	SELECT	I.InventoryID,
					AR.photoUrls
				FROM	MerchandisingInterface.Interface.AdReleases AR	
					INNER JOIN dbo.Inventory I ON AR.businessUnitId = I.BusinessUnitID AND AR.inventoryId = I.InventoryID
					INNER JOIN dbo.DealerPreference DP ON I.BusinessUnitID = DP.BusinessUnitID
				WHERE	I.InventoryActive = 0 AND I.DeleteDt < DATEADD(dd,- DP.UnwindDaysThreshold, DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0))
					AND NULLIF(photoUrls,'') IS NOT NULL	
				) IP

		WHERE	N < LEN(@OptionDelimiter+IP.photoURLs+@OptionDelimiter)
				
			AND SUBSTRING(@OptionDelimiter+IP.photoURLs+@OptionDelimiter,N,1) = @OptionDelimiter
			AND SUBSTRING(@OptionDelimiter+IP.photoURLs+@OptionDelimiter,N+1,CHARINDEX(@OptionDelimiter,@OptionDelimiter+IP.photoURLs+@OptionDelimiter,N+1)-N-1) <>''
	) MP 
WHERE	MP.PhotoURL LIKE 'http://www.firstlook.biz/digitalimages/%'
		
		
/*	ORPHANS`


SELECT	COUNT(*)
FROM	dbo.Photos P
	LEFT JOIN dbo.InventoryPhotos IP ON P.PhotoID = IP.PhotoID
	LEFT JOIN dbo.AppraisalPhotos AP ON P.PhotoID = AP.PhotoID
	LEFT JOIN dbo.BusinessUnitPhotos BUP ON P.PhotoID = BUP.PhotoID
	LEFT JOIN Certified.CertifiedProgramPhoto CPP ON P.PhotoID = CPP.PhotoID
WHERE	IP.PhotoID IS NULL	
	AND AP.PhotoID IS NULL
	AND BUP.PhotoID IS NULL
	AND CPP.PhotoID IS NULL
	
	
*/			
		