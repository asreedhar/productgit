if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Alter_Script]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Alter_Script]
GO

CREATE TABLE [dbo].[Alter_Script] (
	[AlterScriptName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ApplicationDate] [datetime] NOT NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Alter_Script] ADD 
	CONSTRAINT [DF_Alter_Script_ApplicationDate] DEFAULT (getdate()) FOR [ApplicationDate],
	CONSTRAINT [PK_Alter_Script] PRIMARY KEY  CLUSTERED 
	(
		[AlterScriptName]
	)  ON [PRIMARY] 
GO