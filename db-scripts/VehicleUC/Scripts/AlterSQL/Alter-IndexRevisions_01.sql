/*

Add some suggested indexes

*/

CREATE nonclustered INDEX IX_vehicle_values__uid
 on vehicle_values (uid)
 with (sort_in_tempdb = on)

CREATE nonclustered INDEX IX_vehicle_desc_codes__vic
 on vehicle_desc_codes (vic)
 with (sort_in_tempdb = on)

 GO
 