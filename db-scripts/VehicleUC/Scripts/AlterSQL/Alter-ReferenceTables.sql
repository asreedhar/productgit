CREATE TABLE dbo.VIC_Catalog (	VIC 		CHAR(10) NOT NULL CONSTRAINT PK_VIC_Catalog PRIMARY KEY CLUSTERED (VIC) ,
				ModelYear	SMALLINT NOT NULL,
				Make 		VARCHAR(50) NOT NULL, 
				Series 		VARCHAR(50) NOT NULL, 
				Body 		VARCHAR(50) NOT NULL)

GO

CREATE TABLE dbo.VINPrefix_VIC (VINPrefix CHAR(10) NOT NULL, VIC CHAR(10) NOT NULL)
GO

ALTER TABLE dbo.VINPrefix_VIC ADD CONSTRAINT PK_VINPrefix_VIC PRIMARY KEY CLUSTERED (VINPrefix, VIC)
GO

ALTER TABLE dbo.VINPrefix_VIC ADD CONSTRAINT FK_VINPrefix_VIC__VIC FOREIGN KEY (VIC) REFERENCES dbo.VIC_Catalog(VIC)
GO