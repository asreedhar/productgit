
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Customizations_Customization]') AND type in (N'U'))
DROP TABLE [Customization].[Customizations_Customization]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Customization_Associate]') AND type in (N'U'))
DROP TABLE [Customization].[Customization_Associate]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Customization_ChangeName]') AND type in (N'U'))
DROP TABLE [Customization].[Customization_ChangeName]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Customization_ChangeDescription]') AND type in (N'U'))
DROP TABLE [Customization].[Customization_ChangeDescription]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Customization_Hide]') AND type in (N'U'))
DROP TABLE [Customization].[Customization_Hide]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Customization_Merge]') AND type in (N'U'))
DROP TABLE [Customization].[Customization_Merge]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Customization_ChangePosition]') AND type in (N'U'))
DROP TABLE [Customization].[Customization_ChangePosition]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Customization_ChangeLocation]') AND type in (N'U'))
DROP TABLE [Customization].[Customization_ChangeLocation]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Customization_Package]') AND type in (N'U'))
DROP TABLE [Customization].[Customization_Package]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Customization]') AND type in (N'U'))
DROP TABLE [Customization].[Customization]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Memento_Standard]') AND type in (N'U'))
DROP TABLE [Customization].[Memento_Standard]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Memento_Option]') AND type in (N'U'))
DROP TABLE [Customization].[Memento_Option]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Memento_Category]') AND type in (N'U'))
DROP TABLE [Customization].[Memento_Category]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Memento_CategoryHeader]') AND type in (N'U'))
DROP TABLE [Customization].[Memento_CategoryHeader]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Memento_Option]') AND type in (N'U'))
DROP TABLE [Customization].[Memento_Option]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Memento_OptionHeader]') AND type in (N'U'))
DROP TABLE [Customization].[Memento_OptionHeader]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Memento_Standard]') AND type in (N'U'))
DROP TABLE [Customization].[Memento_Standard]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Memento_StandardHeader]') AND type in (N'U'))
DROP TABLE [Customization].[Memento_StandardHeader]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Memento]') AND type in (N'U'))
DROP TABLE [Customization].[Memento]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[MementoType]') AND type in (N'U'))
DROP TABLE [Customization].[MementoType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Location]') AND type in (N'U'))
DROP TABLE [Customization].[Location]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[CustomizationType]') AND type in (N'U'))
DROP TABLE [Customization].[CustomizationType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Customizations_Client]') AND type in (N'U'))
DROP TABLE [Customization].[Customizations_Client]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Customizations]') AND type in (N'U'))
DROP TABLE [Customization].[Customizations]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Customization].[Owner]') AND type in (N'U'))
DROP TABLE [Customization].[Owner]
GO
