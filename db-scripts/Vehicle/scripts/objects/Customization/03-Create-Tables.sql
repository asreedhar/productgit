CREATE TABLE Customization.MementoType (
        MementoTypeID    TINYINT NOT NULL,
        Name            VARCHAR(100) NOT NULL,
        CONSTRAINT PK_Customization_MementoType
                PRIMARY KEY (MementoTypeID),
        CONSTRAINT UK_Customization_MementoType__Name
                UNIQUE (Name),
        CONSTRAINT CK_Customization_MementoType__Name
                CHECK (LEN(Name) > 0)
)
GO

INSERT INTO Customization.MementoType (MementoTypeID, Name) VALUES (1, 'Category Header')
INSERT INTO Customization.MementoType (MementoTypeID, Name) VALUES (2, 'Option Header')
INSERT INTO Customization.MementoType (MementoTypeID, Name) VALUES (3, 'Standard Header')
INSERT INTO Customization.MementoType (MementoTypeID, Name) VALUES (4, 'Category')
INSERT INTO Customization.MementoType (MementoTypeID, Name) VALUES (5, 'Option')
INSERT INTO Customization.MementoType (MementoTypeID, Name) VALUES (6, 'Standard')
GO

-- the information in a memento is immutable; there is no sense in updating it; being an
-- audited schema there is no sense in marking it as deleted only to re-insert it at a
-- future point in time.  we could use the auditrowid as a primary key ... it would save
-- disk space but make the schema less intelligable to others ...

-- the interesting consequence of this is that the first person to customize a memento
-- will be the person associated with the auditing mechanism.  all other people will be
-- able to make use of the inserted record.

CREATE TABLE Customization.Memento (
        MementoID       INT IDENTITY(1,1) NOT NULL,
        MementoTypeID   TINYINT NOT NULL,
        AuditRowID      INT NOT NULL,
        CONSTRAINT PK_Customization_Memento
                PRIMARY KEY (MementoID),
        CONSTRAINT FK_Customization_Memento__MementoType
                FOREIGN KEY (MementoTypeID)
                REFERENCES Customization.MementoType (MementoTypeID),
        CONSTRAINT FK_Customization_Memento__AuditRow
                FOREIGN KEY (AuditRowID)
                REFERENCES Audit.AuditRow (AuditRowID)
)
GO

CREATE TABLE Customization.Memento_CategoryHeader (
        MementoID               INT NOT NULL,
        CategoryHeaderID        INT NOT NULL,
        CONSTRAINT PK_Memento_CategoryHeader
                PRIMARY KEY (MementoID),
        CONSTRAINT FK_Memento_CategoryHeader__Memento
                FOREIGN KEY (MementoID)
                REFERENCES Customization.Memento (MementoID)
)
GO

CREATE TABLE Customization.Memento_OptionHeader (
        MementoID       INT NOT NULL,
        OptionHeaderID  INT NOT NULL,
        CONSTRAINT PK_Memento_OptionHeader
                PRIMARY KEY (MementoID),
        CONSTRAINT FK_Memento_OptionHeader__Memento
                FOREIGN KEY (MementoID)
                REFERENCES Customization.Memento (MementoID)
)
GO

CREATE TABLE Customization.Memento_StandardHeader (
        MementoID               INT NOT NULL,
        StandardHeaderID        INT NOT NULL,
        CONSTRAINT PK_Memento_StandardHeader
                PRIMARY KEY (MementoID),
        CONSTRAINT FK_Memento_StandardHeader__Memento
                FOREIGN KEY (MementoID)
                REFERENCES Customization.Memento (MementoID)
)
GO

CREATE TABLE Customization.Memento_Category (
        MementoID       INT NOT NULL,
        CategoryID      INT NOT NULL,
        CONSTRAINT PK_Memento_Category
                PRIMARY KEY (MementoID),
        CONSTRAINT FK_Memento_Category__Memento
                FOREIGN KEY (MementoID)
                REFERENCES Customization.Memento (MementoID)
)
GO

CREATE TABLE Customization.Memento_Option (
        MementoID        INT NOT NULL,
        StyleID         INT NOT NULL,
        Sequence        INT NOT NULL,
        OptionCode      VARCHAR(20) NOT NULL,
        CONSTRAINT PK_Memento_Option
                PRIMARY KEY (MementoID),
        CONSTRAINT FK_Memento_Option__Memento
                FOREIGN KEY (MementoID)
                REFERENCES Customization.Memento (MementoID),
        CONSTRAINT UK_Memento_Option
                UNIQUE (StyleID, Sequence, OptionCode)
)
GO

CREATE TABLE Customization.Memento_Standard (
        MementoID        INT NOT NULL,
        StyleID         INT NOT NULL,
        Sequence        INT NOT NULL,
        CONSTRAINT PK_Memento_Standard
                PRIMARY KEY (MementoID),
        CONSTRAINT FK_Memento_Standard__Memento
                FOREIGN KEY (MementoID)
                REFERENCES Customization.Memento (MementoID),
        CONSTRAINT UK_Memento_Standard
                UNIQUE (StyleID, Sequence)
)
GO

CREATE TABLE Customization.Location (
        LocationID      TINYINT NOT NULL,
        Name            VARCHAR(100) NOT NULL,
        CONSTRAINT PK_Customization_Location
                PRIMARY KEY (LocationID),
        CONSTRAINT UK_Customization_Location__Name
                UNIQUE (Name),
        CONSTRAINT CK_Customization_Location__Name
                CHECK (LEN(Name) > 0)
)
GO

INSERT INTO Customization.Location (LocationID, Name) VALUES (1, 'Vehicle')
INSERT INTO Customization.Location (LocationID, Name) VALUES (2, 'Interior')
INSERT INTO Customization.Location (LocationID, Name) VALUES (3, 'Exterior')
GO

CREATE TABLE Customization.Owner (
        OwnerID TINYINT NOT NULL,
        Name    VARCHAR(100) NOT NULL,
        CONSTRAINT PK_Customization_Owner
                PRIMARY KEY (OwnerID),
        CONSTRAINT UK_Customization_Owner__Name
                UNIQUE (Name),
        CONSTRAINT CK_Customization_Owner__Name
                CHECK (LEN(Name) > 0)
)
GO

INSERT INTO Customization.Owner (OwnerID, Name) VALUES (1, 'System')
INSERT INTO Customization.Owner (OwnerID, Name) VALUES (2, 'Dealer')
GO

-- the sytem has one list of customizations; each dealer has one and exactly one list
-- of customizations; only a sub-list is extracted at a time; the list once created
-- will not be updated or deleted;

CREATE TABLE Customization.Customizations (
        CustomizationsID        INT IDENTITY(1,1) NOT NULL,
        OwnerID                 TINYINT NOT NULL,
        AuditRowID              INT NOT NULL,
        CONSTRAINT PK_Customization_Customizations
                PRIMARY KEY (CustomizationsID),
        CONSTRAINT FK_Customization_Customizations__Owner
                FOREIGN KEY (OwnerID)
                REFERENCES Customization.Owner (OwnerID),
        CONSTRAINT FK_Customization_Customizations__AuditRow
                FOREIGN KEY (AuditRowID)
                REFERENCES Audit.AuditRow (AuditRowID)
)
GO

CREATE TABLE Customization.Customizations_Client (
        CustomizationsID        INT NOT NULL,
        ClientID                INT NOT NULL,
        AuditRowID              INT NOT NULL,
        CONSTRAINT PK_Customization_Customizations_Client
                PRIMARY KEY NONCLUSTERED (CustomizationsID),
        CONSTRAINT FK_Customization_Customizations_Client__Customizations
                FOREIGN KEY (CustomizationsID)
                REFERENCES Customization.Customizations (CustomizationsID),
        CONSTRAINT FK_Customization_Customizations_Client__Client
                FOREIGN KEY (ClientID)
                REFERENCES Client.Client (ClientID),
        CONSTRAINT FK_Customization_Customizations_Client__AuditRow
                FOREIGN KEY (AuditRowID)
                REFERENCES Audit.AuditRow (AuditRowID)
)
GO

CREATE CLUSTERED INDEX IX_Customization_Customizations_Client__Client
        ON Customization.Customizations_Client (
                ClientID
        )
GO

CREATE TABLE Customization.CustomizationType (
        CustomizationTypeID     TINYINT NOT NULL,
        Name                    VARCHAR(100) NOT NULL,
        CONSTRAINT PK_Customization_CustomizationType
                PRIMARY KEY (CustomizationTypeID),
        CONSTRAINT UK_Customization_CustomizationType__Name
                UNIQUE (Name),
        CONSTRAINT CK_Customization_CustomizationType__Name
                CHECK (LEN(Name) > 0)
)
GO

INSERT INTO Customization.CustomizationType (CustomizationTypeID, Name) VALUES (1, 'Associate')
INSERT INTO Customization.CustomizationType (CustomizationTypeID, Name) VALUES (2, 'Change Description')
INSERT INTO Customization.CustomizationType (CustomizationTypeID, Name) VALUES (3, 'Change Name')
INSERT INTO Customization.CustomizationType (CustomizationTypeID, Name) VALUES (4, 'Change Location')
INSERT INTO Customization.CustomizationType (CustomizationTypeID, Name) VALUES (5, 'Change Position')
INSERT INTO Customization.CustomizationType (CustomizationTypeID, Name) VALUES (6, 'Hide')
INSERT INTO Customization.CustomizationType (CustomizationTypeID, Name) VALUES (7, 'Merge')
INSERT INTO Customization.CustomizationType (CustomizationTypeID, Name) VALUES (8, 'Package')
GO

-- it takes two audit rows to insert and delete an entity; the entity must have the same
-- id value across these two records.  the identity column must be on an immutable table
-- and all data that changes must be on a derived table.

CREATE TABLE Customization.Customization (
        CustomizationID         INT IDENTITY(1,1) NOT NULL,
        CustomizationTypeID     TINYINT NOT NULL,
        AuditRowID              INT NOT NULL,
        CONSTRAINT PK_Customization_Customization
                PRIMARY KEY (CustomizationID),
        CONSTRAINT FK_Customization_Customization__CustomizationType
                FOREIGN KEY (CustomizationTypeID)
                REFERENCES Customization.CustomizationType (CustomizationTypeID),
        CONSTRAINT FK_Customization_Customization__AuditRow
                FOREIGN KEY (AuditRowID)
                REFERENCES Audit.AuditRow (AuditRowID)
)
GO

CREATE TABLE Customization.Customizations_Customization (
        CustomizationsID        INT NOT NULL,
        CustomizationID         INT NOT NULL,
        AuditRowID              INT NOT NULL,
        CONSTRAINT PK_Customization_Customizations_Customization
                PRIMARY KEY (CustomizationsID, CustomizationID, AuditRowID),
        CONSTRAINT FK_Customization_Customizations_Customization__Customizations
                FOREIGN KEY (CustomizationsID)
                REFERENCES Customization.Customizations (CustomizationsID),
        CONSTRAINT FK_Customization_Customizations_Customization__Customization
                FOREIGN KEY (CustomizationID)
                REFERENCES Customization.Customization (CustomizationID),
        CONSTRAINT FK_Customization_Customizations_Customization__AuditRow
                FOREIGN KEY (AuditRowID)
                REFERENCES Audit.AuditRow (AuditRowID)
)
GO

CREATE TABLE Customization.Customization_ChangeName (
        CustomizationID INT NOT NULL,
        MementoID       INT NOT NULL,
        Name            VARCHAR(2000) NOT NULL,
        AuditRowID      INT NOT NULL,
        CONSTRAINT PK_Customization_Customization_ChangeName
                PRIMARY KEY (CustomizationID, AuditRowID),
        CONSTRAINT FK_Customization_Customization_ChangeName__Customization
                FOREIGN KEY (CustomizationID)
                REFERENCES Customization.Customization (CustomizationID),
        CONSTRAINT FK_Customization_Customization_ChangeName__Memento
                FOREIGN KEY (MementoID)
                REFERENCES Customization.Memento (MementoID),
        CONSTRAINT FK_Customization_Customization_ChangeName__AuditRow
                FOREIGN KEY (AuditRowID)
                REFERENCES Audit.AuditRow (AuditRowID),
        CONSTRAINT CK_Customization_Customization_ChangeName__Name
                CHECK (LEN(Name) > 0)
)
GO

CREATE TABLE Customization.Customization_ChangeDescription (
        CustomizationID INT NOT NULL,
        MementoID       INT NOT NULL,
        Description     VARCHAR(2000) NOT NULL,
        AuditRowID      INT NOT NULL,
        CONSTRAINT PK_Customization_Customization_ChangeDescription
                PRIMARY KEY (CustomizationID, AuditRowID),
        CONSTRAINT FK_Customization_Customization_ChangeDescription__Customization
                FOREIGN KEY (CustomizationID)
                REFERENCES Customization.Customization (CustomizationID),
        CONSTRAINT FK_Customization_Customization_ChangeDescription__Memento
                FOREIGN KEY (MementoID)
                REFERENCES Customization.Memento (MementoID),
        CONSTRAINT FK_Customization_Customization_ChangeDescription__AuditRow
                FOREIGN KEY (AuditRowID)
                REFERENCES Audit.AuditRow (AuditRowID),
        CONSTRAINT CK_Customization_ChangeDescription__Description
                CHECK (LEN(Description) > 0)
)
GO

CREATE TABLE Customization.Customization_ChangeLocation (
        CustomizationID INT NOT NULL,
        MementoID       INT NOT NULL,
        LocationID      TINYINT NOT NULL,
        AuditRowID      INT NOT NULL,
        CONSTRAINT PK_Customization_Customization_ChangeLocation
                PRIMARY KEY (CustomizationID, AuditRowID),
        CONSTRAINT FK_Customization_Customization_ChangeLocation__Customization
                FOREIGN KEY (CustomizationID)
                REFERENCES Customization.Customization (CustomizationID),
        CONSTRAINT FK_Customization_Customization_ChangeLocation__Memento
                FOREIGN KEY (MementoID)
                REFERENCES Customization.Memento (MementoID),
        CONSTRAINT FK_Customization_Customization_ChangeLocation__Location
                FOREIGN KEY (LocationID)
                REFERENCES Customization.Location (LocationID),
        CONSTRAINT FK_Customization_Customization_ChangeLocation__AuditRow
                FOREIGN KEY (AuditRowID)
                REFERENCES Audit.AuditRow (AuditRowID)
)
GO

CREATE TABLE Customization.Customization_Hide (
        CustomizationID INT NOT NULL,
        MementoID       INT NOT NULL,
        AuditRowID      INT NOT NULL,
        CONSTRAINT PK_Customization_Customization_Hide
                PRIMARY KEY (CustomizationID, AuditRowID),
        CONSTRAINT FK_Customization_Customization_Hide__Customization
                FOREIGN KEY (CustomizationID)
                REFERENCES Customization.Customization (CustomizationID),
        CONSTRAINT FK_Customization_Customization_Hide__Memento
                FOREIGN KEY (MementoID)
                REFERENCES Customization.Memento (MementoID),
        CONSTRAINT FK_Customization_Customization_Hide__AuditRow
                FOREIGN KEY (AuditRowID)
                REFERENCES Audit.AuditRow (AuditRowID)
)
GO

CREATE TABLE Customization.Customization_ChangePosition (
        CustomizationID INT NOT NULL,
        MementoID       INT NOT NULL,
        Position        INT NOT NULL,
        AuditRowID      INT NOT NULL,
        CONSTRAINT PK_Customization_Customization_Position
                PRIMARY KEY (CustomizationID, AuditRowID),
        CONSTRAINT FK_Customization_Customization_Position__Customization
                FOREIGN KEY (CustomizationID)
                REFERENCES Customization.Customization (CustomizationID),
        CONSTRAINT FK_Customization_Customization_Position__Memento
                FOREIGN KEY (MementoID)
                REFERENCES Customization.Memento (MementoID),
        CONSTRAINT FK_Customization_Customization_Position__AuditRow
                FOREIGN KEY (AuditRowID)
                REFERENCES Audit.AuditRow (AuditRowID),
        CONSTRAINT CK_Customization_Customization_Position__Position
                CHECK (Position >= 0)
)
GO

CREATE TABLE Customization.Customization_Associate (
        CustomizationID INT NOT NULL,
        MementoID       INT NOT NULL,
        RelationID      INT NOT NULL,
        AuditRowID      INT NOT NULL,
        CONSTRAINT PK_Customization_Customization_Associate
                PRIMARY KEY (CustomizationID, AuditRowID),
        CONSTRAINT FK_Customization_Customization_Associate__Customization
                FOREIGN KEY (CustomizationID)
                REFERENCES Customization.Customization (CustomizationID),
        CONSTRAINT FK_Customization_Customization_Associate__Memento
                FOREIGN KEY (MementoID)
                REFERENCES Customization.Memento (MementoID),
        CONSTRAINT FK_Customization_Customization_Associate__Relation
                FOREIGN KEY (RelationID)
                REFERENCES Customization.Memento (MementoID),
        CONSTRAINT FK_Customization_Customization_Associate__AuditRow
                FOREIGN KEY (AuditRowID)
                REFERENCES Audit.AuditRow (AuditRowID),
)
GO

CREATE TABLE Customization.Customization_Merge (
        CustomizationID INT NOT NULL,
        MementoID       INT NOT NULL,
        ParentID        INT NOT NULL,
        AuditRowID      INT NOT NULL,
        CONSTRAINT PK_Customization_Customization_Merge
                PRIMARY KEY (CustomizationID, AuditRowID),
        CONSTRAINT FK_Customization_Customization_Merge__Customization
                FOREIGN KEY (CustomizationID)
                REFERENCES Customization.Customization (CustomizationID),
        CONSTRAINT FK_Customization_Customization_Merge__Memento
                FOREIGN KEY (MementoID)
                REFERENCES Customization.Memento (MementoID),
        CONSTRAINT FK_Customization_Customization_Merge__Parent
                FOREIGN KEY (ParentID)
                REFERENCES Customization.Memento (MementoID),
        CONSTRAINT FK_Customization_Customization_Merge__AuditRow
                FOREIGN KEY (AuditRowID)
                REFERENCES Audit.AuditRow (AuditRowID)
)
GO

CREATE TABLE Customization.Customization_Package (
        CustomizationID INT NOT NULL,
        MementoID       INT NOT NULL,
        AuditRowID      INT NOT NULL,
        CONSTRAINT PK_Customization_Customization_Package
                PRIMARY KEY (CustomizationID, AuditRowID),
        CONSTRAINT FK_Customization_Customization_Package__Customization
                FOREIGN KEY (CustomizationID)
                REFERENCES Customization.Customization (CustomizationID),
        CONSTRAINT FK_Customization_Customization_Package__Memento
                FOREIGN KEY (MementoID)
                REFERENCES Customization.Memento (MementoID),
        CONSTRAINT FK_Customization_Customization_Package__AuditRow
                FOREIGN KEY (AuditRowID)
                REFERENCES Audit.AuditRow (AuditRowID)
)
GO
