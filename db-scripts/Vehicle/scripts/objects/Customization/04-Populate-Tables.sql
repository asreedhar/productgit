
SET NOCOUNT ON;

--BEGIN TRANSACTION T1

--BEGIN TRY

--============--
-- categories --
--============--

--------------
-- location --
--------------

DECLARE @CategoryHeaderLocation TABLE (
        CategoryHeaderID INT NOT NULL,
        LocationID TINYINT NOT NULL,
        CustomizationID INT NULL,
        MementoID TINYINT NULL,
        PRIMARY KEY (CategoryHeaderID)
)

-- vehicle

INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (1, 0)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (1, 30)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (1, 13)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (1, 20)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (1, 15)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (1, 6)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (1, 37)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (1, 4)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (1, 46)

-- interior

INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (2, 32)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (2, 21)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (2, 22)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (2, 23)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (2, 24)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (2, 25)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (2, 26)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (2, 27)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (2, 28)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (2, 29)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (2, 43)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (2, 44)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (2, 38)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (2, 39)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (2, 42)

-- exterior

INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (3, 8)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (3, 17)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (3, 7)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (3, 10)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (3, 31)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (3, 33)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (3, 34)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (3, 35)
INSERT INTO @CategoryHeaderLocation (LocationID, CategoryHeaderID) VALUES  (3, 36)

------------
-- merge  --
------------

DECLARE @CategoryHeaderMerge TABLE (
        CategoryHeaderID INT NOT NULL,
        ParentCategoryHeaderID INT NOT NULL,
        CustomizationID INT NULL,
        MementoID INT NULL,
        ParentMementoID INT NULL,
        PRIMARY KEY (CategoryHeaderID)
)

-- parent:      Convenience
-- child:       Air Conditioning
-- child:       D�cor
-- child:       Floor Mats
-- child:       Locks

INSERT INTO @CategoryHeaderMerge (ParentCategoryHeaderID, CategoryHeaderID) VALUES (23, 21)
INSERT INTO @CategoryHeaderMerge (ParentCategoryHeaderID, CategoryHeaderID) VALUES (23, 24)
INSERT INTO @CategoryHeaderMerge (ParentCategoryHeaderID, CategoryHeaderID) VALUES (23, 25)
INSERT INTO @CategoryHeaderMerge (ParentCategoryHeaderID, CategoryHeaderID) VALUES (23, 26)

-- parent:      Audio
-- child:       Telephone

INSERT INTO @CategoryHeaderMerge (ParentCategoryHeaderID, CategoryHeaderID) VALUES (22, 28)

-- parent:      Seats
-- child:       Seat Trim

INSERT INTO @CategoryHeaderMerge (ParentCategoryHeaderID, CategoryHeaderID) VALUES (44, 43)

-- parent:      Safety Features
-- child:       Air Bag - Frontal
-- child:       Air Bag - Side

INSERT INTO @CategoryHeaderMerge (ParentCategoryHeaderID, CategoryHeaderID) VALUES (42, 38)
INSERT INTO @CategoryHeaderMerge (ParentCategoryHeaderID, CategoryHeaderID) VALUES (42, 39)

-- parent:      Suspension
-- child:       Trailering

INSERT INTO @CategoryHeaderMerge (ParentCategoryHeaderID, CategoryHeaderID) VALUES (17, 8)

-- parent:      Tires
-- child:       Tire - Front
-- child:       Tire - Rear
-- child:       Tire - Spare
-- child:       Wheels

INSERT INTO @CategoryHeaderMerge (ParentCategoryHeaderID, CategoryHeaderID) VALUES (7, 34)
INSERT INTO @CategoryHeaderMerge (ParentCategoryHeaderID, CategoryHeaderID) VALUES (7, 35)
INSERT INTO @CategoryHeaderMerge (ParentCategoryHeaderID, CategoryHeaderID) VALUES (7, 36)
INSERT INTO @CategoryHeaderMerge (ParentCategoryHeaderID, CategoryHeaderID) VALUES (7, 10)

-- parent:      Running Boards
-- child:       Pickup Box

INSERT INTO @CategoryHeaderMerge (ParentCategoryHeaderID, CategoryHeaderID) VALUES (33, 31)

-------------
-- rename  --
-------------

DECLARE @CategoryHeaderChangeName TABLE (
        CategoryHeaderID INT NOT NULL,
        Name VARCHAR(2000) NOT NULL,
        CustomizationID INT NULL,
        MementoID INT NULL,
        PRIMARY KEY (CategoryHeaderID)
)

-- before:      Audio
-- after:       Entertainment

INSERT INTO @CategoryHeaderChangeName (CategoryHeaderID, Name) VALUES (22, 'Entertainment')

-- before:      Suspension
-- after:       Suspension & Towing

INSERT INTO @CategoryHeaderChangeName (CategoryHeaderID, Name) VALUES (17, 'Suspension & Towing')

-- before:      Tires
-- after:       Tires & Wheels

INSERT INTO @CategoryHeaderChangeName (CategoryHeaderID, Name) VALUES (7, 'Tires & Wheels')

-- before:      Running Boards
-- after:       Accessories

INSERT INTO @CategoryHeaderChangeName (CategoryHeaderID, Name) VALUES (33, 'Accessories')

---------------
-- position  --
---------------

DECLARE @CategoryHeaderChangePosition TABLE (
        CategoryHeaderID INT NOT NULL,
        Position INT NOT NULL,
        CustomizationID INT NULL,
        MementoID INT NULL,
        PRIMARY KEY (CategoryHeaderID)
)

-- vehicle

INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (0, 0)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (1, 30)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (2, 13)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (3, 20)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (4, 15)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (5, 6)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (6, 37)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (7, 4)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (8, 46)

-- interior

INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (6, 32)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (2, 21)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (1, 22)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (2, 23)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (2, 24)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (2, 25)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (2, 26)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (4, 27)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (1, 28)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (4, 29)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (3, 43)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (3, 44)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (5, 38)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (5, 39)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (5, 42)

-- exterior

INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (2, 8)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (2, 17)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (1, 7)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (1, 10)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (3, 31)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (3, 33)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (1, 34)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (1, 35)
INSERT INTO @CategoryHeaderChangePosition (Position, CategoryHeaderID) VALUES  (1, 36)

--===========--
-- standards --
--===========--

--------------
-- location --
--------------

-- specify the location ...



-----------
-- hide  --
-----------

DECLARE @StandardHeaderHide TABLE (
        StandardHeaderID INT NOT NULL,
        CustomizationID INT NULL,
        MementoID INT NULL,
        PRIMARY KEY (StandardHeaderID)
)

-- EMISSIONS CERTIFICATION
-- EPA FUEL ECONOMY
-- EPA FUEL ECONOMY RATING
-- EPA FUEL ECONOMY RATINGS
-- EPA LISTINGS
-- NOTE
-- ENERGUIDE FUEL CONSUMPTION
-- WARRANTY

INSERT INTO @StandardHeaderHide (StandardHeaderID) VALUES (1157)
INSERT INTO @StandardHeaderHide (StandardHeaderID) VALUES (1166)
INSERT INTO @StandardHeaderHide (StandardHeaderID) VALUES (1167)
INSERT INTO @StandardHeaderHide (StandardHeaderID) VALUES (1168)
INSERT INTO @StandardHeaderHide (StandardHeaderID) VALUES (1169)
INSERT INTO @StandardHeaderHide (StandardHeaderID) VALUES (1246)
INSERT INTO @StandardHeaderHide (StandardHeaderID) VALUES (10206)
INSERT INTO @StandardHeaderHide (StandardHeaderID) VALUES (10210)

--=========--
-- options --
--=========--

--------------
-- location --
--------------

DECLARE @OptionHeaderLocation TABLE (
        OptionHeaderID INT NOT NULL,
        LocationID TINYINT NOT NULL,
        CustomizationID INT NULL,
        MementoID INT NULL,
        PRIMARY KEY (OptionHeaderID)
)

-- vehicle

INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 26)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 80)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1055)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1102)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1105)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1106)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1108)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1109)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1113)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1115)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1116)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1121)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1142)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1147)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1148)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1186)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1187)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1201)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1202)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1206)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1215)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1226)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1232)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1236)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1240)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1241)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1306)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1307)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1308)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1317)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1337)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1339)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1355)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1356)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1361)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1365)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1366)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1368)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1382)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1383)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 1384)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 10090)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 10091)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 10100)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 10104)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 10194)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 10241)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 10259)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 10341)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (1, 10386)

-- interior

INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 29)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 30)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 31)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1003)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1009)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1010)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1017)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1024)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1025)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1028)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1029)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1030)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1042)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1043)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1048)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1054)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1091)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1092)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1094)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1097)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1098)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1099)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1128)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1139)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1141)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1143)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1144)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1146)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1152)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1209)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1220)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1221)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1222)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1223)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1224)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1225)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1237)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1262)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1295)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1301)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1302)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1303)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1305)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1312)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1313)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1345)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1347)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1348)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 1349)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 10096)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 10106)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 10138)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 10157)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 10179)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 10181)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 10182)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 10190)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 10193)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 10198)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 10216)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 10225)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 10234)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 10406)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 10407)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (2, 10594)

-- exterior

INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 27)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 65)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 76)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1023)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1027)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1044)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1046)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1053)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1090)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1095)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1103)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1110)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1111)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1112)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1117)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1126)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1127)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1129)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1132)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1133)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1140)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1153)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1160)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1162)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1164)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1165)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1192)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1193)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1194)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1195)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1196)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1197)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1198)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1231)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1238)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1239)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1243)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1256)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1259)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1264)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1289)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1318)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1319)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1320)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1321)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1322)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1323)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1324)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1340)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1341)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1352)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1353)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1354)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1357)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1358)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1362)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1372)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1376)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1377)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1378)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1390)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1391)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1392)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1393)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1395)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1396)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 1397)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10089)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10105)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10159)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10186)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10209)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10227)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10233)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10244)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10247)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10256)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10318)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10346)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10466)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10468)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10477)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10558)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10560)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10570)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10591)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10609)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10649)
INSERT INTO @OptionHeaderLocation (LocationID, OptionHeaderID) VALUES  (3, 10650)

----------
-- hide --
----------

DECLARE @OptionHeaderHide TABLE (
        OptionHeaderID INT NOT NULL,
        CustomizationID INT NULL,
        MementoID INT NULL,
        PRIMARY KEY (OptionHeaderID)
)

INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (21)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (22)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (23)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (28)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (41)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (64)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (69)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (73)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (79)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1002)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1004)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1005)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1006)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1007)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1008)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1012)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1013)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1018)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1019)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1020)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1021)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1022)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1031)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1032)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1033)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1034)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1035)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1036)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1041)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1047)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1049)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1051)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1052)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1056)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1089)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1096)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1104)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1107)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1114)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1118)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1119)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1120)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1122)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1123)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1124)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1125)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1130)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1134)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1135)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1136)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1137)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1149)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1150)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1151)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1154)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1155)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1156)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1157)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1158)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1163)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1174)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1175)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1178)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1180)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1181)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1182)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1183)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1184)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1185)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1188)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1191)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1203)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1204)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1205)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1207)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1212)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1213)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1214)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1217)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1218)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1219)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1227)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1228)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1242)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1245)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1246)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1247)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1248)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1252)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1253)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1254)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1258)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1260)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1261)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1263)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1275)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1276)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1277)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1280)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1281)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1282)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1283)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1284)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1285)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1286)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1290)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1299)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1300)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1304)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1309)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1310)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1311)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1316)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1325)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1326)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1327)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1328)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1329)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1330)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1331)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1338)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1342)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1346)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1350)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1351)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1359)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1367)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1369)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1370)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1371)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1374)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1375)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1381)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (1399)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10094)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10095)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10097)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10098)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10101)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10108)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10109)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10112)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10115)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10116)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10158)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10183)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10184)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10191)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10195)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10196)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10197)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10207)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10258)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10261)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10319)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10320)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10321)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10344)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10365)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10426)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10489)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10509)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10510)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10559)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10561)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10562)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10563)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10564)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10565)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10566)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10567)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10568)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10569)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10571)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10590)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10592)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10593)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10630)
INSERT INTO @OptionHeaderHide ( OptionHeaderID ) VALUES  (10669)

-------------
-- rename  --
-------------

DECLARE @OptionHeaderChangeName TABLE (
        OptionHeaderID INT NOT NULL,
        Name VARCHAR(2000) NOT NULL,
        CustomizationID INT NULL,
        MementoID INT NULL,
        PRIMARY KEY (OptionHeaderID)
)

INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1001,'ADDITIONAL PACKAGES')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1002,'AERO KIT')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1003,'AIR VENTS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1004,'BRAKE CALIPERS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1005,'BRAKE HANDLE/SHIFT KNOB GROUPS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1006,'BRAKE HANDLE/SHIFT KNOB GROUPS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1007,'CARPET WELTING')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1008,'CARPETING')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1009,'CELLULAR TELEPHONE EQUIPMENT')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1010,'CENTER CONSOLE')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1011,'CHASSIS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1012,'CONSOLE FRAME')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1013,'CONSOLE TRAY')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1014,'CONVENIENCE PACKAGES')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1015,'DASH PKG')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1016,'DASH PKG')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1017,'DASHBOARD')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1018,'DOOR ACCESSORIES')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1019,'DOOR ENTRANCE PANELS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1020,'DOOR HANDLE')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1021,'DOOR PANELS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1022,'DOOR SILL')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1023,'DOOR TRIM')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1024,'FLOOR MATS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1025,'FLOOR MATS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1027,'HUB CAPS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1028,'INSIDE DOOR OPENERS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1029,'INSTRUMENTATION')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1030,'INTERIOR ROOF')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1031,'KNEEBAR')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1032,'PORT INSTALLED CONVENIENCE PACKAGES')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1033,'PORT INSTALLED OPTION GROUPS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1034,'PORT INSTALLED OPTION GROUPS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1035,'PORT INSTALLED OPTION PACKAGES')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1036,'PORT INSTALLED PACKAGES')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1038,'PORT INSTALLED PREMIUM SOUND PACKAGES')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1039,'PORT INSTALLED PREMIUM SOUND PACKAGES')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1040,'PORT INSTALLED SPORT ACCESSORY GROUPS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1041,'PORT INSTALLED SPORT PACKAGES')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1042,'REAR CENTER CONSOLE')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1043,'REAR CENTER CONSOLE')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1044,'RIM CAPS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1046,'ROLL BAR')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1047,'SEAT BELT')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1048,'SEAT OPTIONS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1049,'SHIFT KNOB/BRAKE HANDLE')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1050,'SPORT PACKAGES')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1051,'STEERING COLUMN')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1052,'STEERING WHEEL')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1053,'STORAGE SHELF')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1054,'SUNVISORS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1055,'TRIM PKG')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1056,'TRUNK CARPETING')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (1057,'WHEELS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10120,'SEAT TYPE')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10138,'FRONT CENTER CONSOLE')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10179,'INTERIOR PKG')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10181,'SPEAKER SYSTEM')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10182,'INSTRUMENT DIALS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10183,'DOOR SILLS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10188,'MEMORY PKGS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10190,'INTERIOR OPTIONS')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10345,'PREFERRED EQUIPMENT GROUP')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10347,'PREFERRED EQUIPMENT GROUP')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10348,'PREFERRED EQUIPMENT GROUP')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10349,'MARKETING OPTION PKG')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10469,'OPTION PKG')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10470,'OPTION PKG')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10471,'OPTION PKG')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10472,'OPTION PKG')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10473,'OPTION PKG')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10474,'OPTION PKG')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10475,'OPTION PKG')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10476,'OPTION PKG')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10478,'PREFERRED EQUIPMENT PKG')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10479,'PREFERRED EQUIPMENT PKG')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10480,'PREFERRED EQUIPMENT PKG')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10481,'PREFERRED EQUIPMENT PKG')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10482,'PREFERRED EQUIPMENT PKG')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10483,'PREFERRED EQUIPMENT PKG')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10484,'PREFERRED EQUIPMENT PKG')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10485,'OPTION PACKAGE')
INSERT INTO @OptionHeaderChangeName ( OptionHeaderID, Name ) VALUES (10486,'MARKETING OPTION PKG')

--============--
-- JUST DO IT --
--============--

        DECLARE @ValidUpTo DATETIME

        SELECT  @ValidUpTo = '2038-01-01 00:00:00'

        DECLARE @MemberID INT, @UserID INT

        SELECT  @MemberID = MemberID
        FROM    IMT.dbo.Member
        WHERE   Login = 'admin'

        IF (NOT EXISTS(SELECT 1 FROM Reference.Member WHERE MemberID = @MemberID)) BEGIN
                INSERT INTO Reference.Member ( MemberID ) VALUES  ( @MemberID )
        END

        SELECT  @UserID = U.UserID
        FROM    Client.[User] U
        JOIN    Client.User_Member M ON M.UserID = U.UserID
        WHERE   U.UserTypeID = 1
        AND     M.MemberID = @MemberID
        
        IF (@UserID IS NULL) BEGIN
                INSERT INTO Client.[User] ( UserTypeID ) VALUES  ( 1 )
                SELECT  @UserID = @@IDENTITY
                INSERT INTO Client.User_Member ( UserID, MemberID ) VALUES  ( @UserID, @MemberID )
        END
        
        DECLARE @AuditID INT, @AuditRowID INT, @ID INT

        INSERT INTO Audit.Audit
                ( AuditDate, UserID )
        VALUES  ( GETDATE(),
                  @UserID
                  )

        SELECT  @AuditID = @@IDENTITY

        DECLARE @CustomizationsID INT

        SELECT  @CustomizationsID = CustomizationsID
        FROM    Customization.Customizations
        WHERE   OwnerID = 1

        IF (@CustomizationsID IS NULL) BEGIN

                INSERT INTO Audit.AuditRow
                        ( AuditID ,
                          ValidFrom ,
                          ValidUpTo ,
                          WasInsert ,
                          WasUpdate ,
                          WasDelete
                        )
                VALUES  ( @AuditID ,
                          GETDATE() ,
                          @ValidUpTo ,
                          1 ,
                          0 ,
                          0  
                        )
                
                SELECT  @AuditRowID = @@IDENTITY
                
                INSERT INTO Customization.Customizations
                        ( OwnerID, AuditRowID )
                VALUES  ( 1,
                          @AuditRowID
                          )
                
                SELECT  @CustomizationsID = @@IDENTITY
                
        END
        
        --=========--
        -- MEMENTO --
        --=========--
        
        ---------------------
        -- Category Header --
        ---------------------

        -- note headers with id's
                
        UPDATE  C
        SET     MementoID = T.MementoID
        FROM    @CategoryHeaderLocation C
        JOIN    Customization.Memento_CategoryHeader T ON T.CategoryHeaderID = C.CategoryHeaderID
        
        UPDATE  C
        SET     MementoID = T.MementoID
        FROM    @CategoryHeaderMerge C
        JOIN    Customization.Memento_CategoryHeader T ON T.CategoryHeaderID = C.CategoryHeaderID
        
        UPDATE  C
        SET     ParentMementoID = T.MementoID
        FROM    @CategoryHeaderMerge C
        JOIN    Customization.Memento_CategoryHeader T ON T.CategoryHeaderID = C.ParentCategoryHeaderID
        
        UPDATE  C
        SET     MementoID = T.MementoID
        FROM    @CategoryHeaderChangeName C
        JOIN    Customization.Memento_CategoryHeader T ON T.CategoryHeaderID = C.CategoryHeaderID
        
        UPDATE  C
        SET     MementoID = T.MementoID
        FROM    @CategoryHeaderChangePosition C
        JOIN    Customization.Memento_CategoryHeader T ON T.CategoryHeaderID = C.CategoryHeaderID
        
        -- note headers without id's
        
        DECLARE @CategoryHeaders TABLE (CategoryHeaderID INT NOT NULL, MementoID INT NULL)
        
        INSERT INTO @CategoryHeaders ( CategoryHeaderID )
        SELECT  CategoryHeaderID
        FROM    @CategoryHeaderLocation C
        WHERE   NOT EXISTS (SELECT 1 FROM @CategoryHeaders X WHERE X.CategoryHeaderID = C.CategoryHeaderID)
        AND     C.MementoID IS NULL
        
        INSERT INTO @CategoryHeaders ( CategoryHeaderID )
        SELECT  CategoryHeaderID
        FROM    @CategoryHeaderMerge C
        WHERE   NOT EXISTS (SELECT 1 FROM @CategoryHeaders X WHERE X.CategoryHeaderID = C.CategoryHeaderID)
        AND     C.MementoID IS NULL
        
        INSERT INTO @CategoryHeaders ( CategoryHeaderID )
        SELECT  ParentCategoryHeaderID
        FROM    @CategoryHeaderMerge C
        WHERE   NOT EXISTS (SELECT 1 FROM @CategoryHeaders X WHERE X.CategoryHeaderID = C.ParentCategoryHeaderID)
        AND     C.MementoID IS NULL
        
        INSERT INTO @CategoryHeaders ( CategoryHeaderID )
        SELECT  CategoryHeaderID
        FROM    @CategoryHeaderChangeName C
        WHERE   NOT EXISTS (SELECT 1 FROM @CategoryHeaders X WHERE X.CategoryHeaderID = C.CategoryHeaderID)
        AND     C.MementoID IS NULL
        
        INSERT INTO @CategoryHeaders ( CategoryHeaderID )
        SELECT  CategoryHeaderID
        FROM    @CategoryHeaderChangePosition C
        WHERE   NOT EXISTS (SELECT 1 FROM @CategoryHeaders X WHERE X.CategoryHeaderID = C.CategoryHeaderID)
        AND     C.MementoID IS NULL
        
        -- generate headers
        
        DECLARE @CategoryHeaderID INT
        
        SELECT @CategoryHeaderID = -1
        
        WHILE (@CategoryHeaderID IS NOT NULL) BEGIN
        
                SELECT  @CategoryHeaderID = MIN(CategoryHeaderID)
                FROM    @CategoryHeaders
                WHERE   CategoryHeaderID > @CategoryHeaderID
                AND     MementoID IS NULL
                
                IF (@CategoryHeaderID IS NOT NULL) BEGIN
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Memento
                                ( MementoTypeID, AuditRowID )
                        VALUES  ( 1,
                                  @AuditRowID
                                  )

                        SELECT  @ID = @@IDENTITY
                        
                        INSERT INTO Customization.Memento_CategoryHeader
                                ( MementoID, CategoryHeaderID )
                        VALUES  ( @ID,
                                  @CategoryHeaderID
                                  )
                        
                        UPDATE  @CategoryHeaders
                        SET     MementoID = @ID
                        WHERE   CategoryHeaderID = @CategoryHeaderID
                        
                END
                
        END
        
        -- update source tables
        
        UPDATE  C
        SET     MementoID = T.MementoID
        FROM    @CategoryHeaderLocation C
        JOIN    Customization.Memento_CategoryHeader T ON T.CategoryHeaderID = C.CategoryHeaderID
        
        UPDATE  C
        SET     MementoID = T.MementoID
        FROM    @CategoryHeaderMerge C
        JOIN    Customization.Memento_CategoryHeader T ON T.CategoryHeaderID = C.CategoryHeaderID
        
        UPDATE  C
        SET     ParentMementoID = T.MementoID
        FROM    @CategoryHeaderMerge C
        JOIN    Customization.Memento_CategoryHeader T ON T.CategoryHeaderID = C.ParentCategoryHeaderID
        
        UPDATE  C
        SET     MementoID = T.MementoID
        FROM    @CategoryHeaderChangeName C
        JOIN    Customization.Memento_CategoryHeader T ON T.CategoryHeaderID = C.CategoryHeaderID
        
        UPDATE  C
        SET     MementoID = T.MementoID
        FROM    @CategoryHeaderChangePosition C
        JOIN    Customization.Memento_CategoryHeader T ON T.CategoryHeaderID = C.CategoryHeaderID
        
        -------------------
        -- Option Header --
        -------------------
        
        -- note headers with id's
                
        UPDATE  C
        SET     MementoID = T.MementoID
        FROM    @OptionHeaderLocation C
        JOIN    Customization.Memento_OptionHeader T ON T.OptionHeaderID = C.OptionHeaderID
        
        UPDATE  C
        SET     MementoID = T.MementoID
        FROM    @OptionHeaderHide C
        JOIN    Customization.Memento_OptionHeader T ON T.OptionHeaderID = C.OptionHeaderID
        
        UPDATE  C
        SET     MementoID = T.MementoID
        FROM    @OptionHeaderChangeName C
        JOIN    Customization.Memento_OptionHeader T ON T.OptionHeaderID = C.OptionHeaderID
        
        -- note headers without id's
        
        DECLARE @OptionHeaders TABLE (OptionHeaderID INT NOT NULL, MementoID INT NULL)
        
        INSERT INTO @OptionHeaders ( OptionHeaderID )
        SELECT  OptionHeaderID
        FROM    @OptionHeaderLocation C
        WHERE   NOT EXISTS (SELECT 1 FROM @OptionHeaders X WHERE X.OptionHeaderID = C.OptionHeaderID)
        AND     C.MementoID IS NULL
        
        INSERT INTO @OptionHeaders ( OptionHeaderID )
        SELECT  OptionHeaderID
        FROM    @OptionHeaderHide C
        WHERE   NOT EXISTS (SELECT 1 FROM @OptionHeaders X WHERE X.OptionHeaderID = C.OptionHeaderID)
        AND     C.MementoID IS NULL
        
        INSERT INTO @OptionHeaders ( OptionHeaderID )
        SELECT  OptionHeaderID
        FROM    @OptionHeaderChangeName C
        WHERE   NOT EXISTS (SELECT 1 FROM @OptionHeaders X WHERE X.OptionHeaderID = C.OptionHeaderID)
        AND     C.MementoID IS NULL
        
        -- generate headers
        
        DECLARE @OptionHeaderID INT
        
        SELECT @OptionHeaderID = -1
        
        WHILE (@OptionHeaderID IS NOT NULL) BEGIN
        
                SELECT  @OptionHeaderID = MIN(OptionHeaderID)
                FROM    @OptionHeaders
                WHERE   OptionHeaderID > @OptionHeaderID
                AND     MementoID IS NULL
                
                IF (@OptionHeaderID IS NOT NULL) BEGIN
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Memento
                                ( MementoTypeID, AuditRowID )
                        VALUES  ( 2,
                                  @AuditRowID
                                  )

                        SELECT  @ID = @@IDENTITY
                        
                        INSERT INTO Customization.Memento_OptionHeader
                                ( MementoID, OptionHeaderID )
                        VALUES  ( @ID,
                                  @OptionHeaderID
                                  )
                        
                        UPDATE  @OptionHeaders
                        SET     MementoID = @ID
                        WHERE   OptionHeaderID = @OptionHeaderID
                        
                END
                
        END
        
        -- update source tables
        
        UPDATE  C
        SET     MementoID = T.MementoID
        FROM    @OptionHeaderLocation C
        JOIN    Customization.Memento_OptionHeader T ON T.OptionHeaderID = C.OptionHeaderID
        
        UPDATE  C
        SET     MementoID = T.MementoID
        FROM    @OptionHeaderChangeName C
        JOIN    Customization.Memento_OptionHeader T ON T.OptionHeaderID = C.OptionHeaderID
        
        UPDATE  C
        SET     MementoID = T.MementoID
        FROM    @OptionHeaderHide C
        JOIN    Customization.Memento_OptionHeader T ON T.OptionHeaderID = C.OptionHeaderID
        
        ---------------------
        -- Standard Header --
        ---------------------
        
        -- note headers with id's
                
        UPDATE  C
        SET     MementoID = T.MementoID
        FROM    @StandardHeaderHide C
        JOIN    Customization.Memento_StandardHeader T ON T.StandardHeaderID = C.StandardHeaderID
        
        -- note headers without id's
        
        DECLARE @StandardHeaders TABLE (StandardHeaderID INT NOT NULL, MementoID INT NULL)
        
        INSERT INTO @StandardHeaders ( StandardHeaderID )
        SELECT  StandardHeaderID
        FROM    @StandardHeaderHide C
        WHERE   NOT EXISTS (SELECT 1 FROM @StandardHeaders X WHERE X.StandardHeaderID = C.StandardHeaderID)
        AND     C.MementoID IS NULL
        
        -- generate headers
        
        DECLARE @StandardHeaderID INT
        
        SELECT @StandardHeaderID = -1
        
        WHILE (@StandardHeaderID IS NOT NULL) BEGIN
        
                SELECT  @StandardHeaderID = MIN(StandardHeaderID)
                FROM    @StandardHeaders
                WHERE   StandardHeaderID > @StandardHeaderID
                AND     MementoID IS NULL
                
                IF (@StandardHeaderID IS NOT NULL) BEGIN
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Memento
                                ( MementoTypeID, AuditRowID )
                        VALUES  ( 3,
                                  @AuditRowID
                                  )

                        SELECT  @ID = @@IDENTITY
                        
                        INSERT INTO Customization.Memento_StandardHeader
                                ( MementoID, StandardHeaderID )
                        VALUES  ( @ID,
                                  @StandardHeaderID
                                  )
                        
                        UPDATE  @StandardHeaders
                        SET     MementoID = @ID
                        WHERE   StandardHeaderID = @StandardHeaderID
                        
                END
                
        END
        
        -- update source tables
        
        UPDATE  C
        SET     MementoID = T.MementoID
        FROM    @StandardHeaderHide C
        JOIN    Customization.Memento_StandardHeader T ON T.StandardHeaderID = C.StandardHeaderID
        
        --================--
        -- CUSTOMIZATIONS --
        --================--
        
        DECLARE @MementoID INT
        
        SELECT @MementoID = -1
        
        WHILE (@MementoID IS NOT NULL) BEGIN
                
                SELECT  @MementoID = MIN(MementoID)
                FROM    @CategoryHeaderLocation
                WHERE   CustomizationID IS NULL
                AND     MementoID > @MementoID
                
                IF (@MementoID IS NOT NULL) BEGIN
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customization
                                ( CustomizationTypeID, AuditRowID )
                        VALUES  ( 4,
                                  @AuditRowID
                                  )

                        SELECT  @ID = @@IDENTITY
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customizations_Customization
                                ( CustomizationsID ,
                                  CustomizationID ,
                                  AuditRowID
                                )
                        VALUES  ( @CustomizationsID,
                                  @ID,
                                  @AuditRowID
                                )
                        
                        UPDATE  @CategoryHeaderLocation
                        SET     CustomizationID = @ID
                        WHERE   MementoID = @MementoID
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customization_ChangeLocation
                                ( CustomizationID ,
                                  MementoID ,
                                  LocationID ,
                                  AuditRowID
                                )
                        SELECT  CustomizationID ,
                                MementoID ,
                                LocationID ,
                                @AuditRowID
                        FROM    @CategoryHeaderLocation
                        WHERE   CustomizationID = @ID
                        
                END
        END
                
        SELECT @MementoID = -1
        
        WHILE (@MementoID IS NOT NULL) BEGIN
        
                SELECT  @MementoID = MIN(MementoID)
                FROM    @CategoryHeaderMerge
                WHERE   CustomizationID IS NULL
                AND     MementoID > @MementoID
        
                IF (@MementoID IS NOT NULL) BEGIN
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customization
                                ( CustomizationTypeID, AuditRowID )
                        VALUES  ( 7,
                                  @AuditRowID
                                  )

                        SELECT  @ID = @@IDENTITY
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customizations_Customization
                                ( CustomizationsID ,
                                  CustomizationID ,
                                  AuditRowID
                                )
                        VALUES  ( @CustomizationsID,
                                  @ID,
                                  @AuditRowID
                                )
                        
                        UPDATE  @CategoryHeaderMerge
                        SET     CustomizationID = @ID
                        WHERE   MementoID = @MementoID
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customization_Merge
                                ( CustomizationID ,
                                  MementoID ,
                                  ParentID ,
                                  AuditRowID
                                )
                        SELECT  CustomizationID ,
                                MementoID ,
                                ParentMementoID ,
                                @AuditRowID
                        FROM    @CategoryHeaderMerge
                        WHERE   CustomizationID = @ID
                        
                END
        END
        
        SELECT @MementoID = -1
        
        WHILE (@MementoID IS NOT NULL) BEGIN
        
                SELECT  @MementoID = MIN(MementoID)
                FROM    @CategoryHeaderChangeName
                WHERE   CustomizationID IS NULL
                AND     MementoID > @MementoID
        
                IF (@MementoID IS NOT NULL) BEGIN
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customization
                                ( CustomizationTypeID, AuditRowID )
                        VALUES  ( 3,
                                  @AuditRowID
                                  )

                        SELECT  @ID = @@IDENTITY
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customizations_Customization
                                ( CustomizationsID ,
                                  CustomizationID ,
                                  AuditRowID
                                )
                        VALUES  ( @CustomizationsID,
                                  @ID,
                                  @AuditRowID
                                )
                        
                        UPDATE  @CategoryHeaderChangeName
                        SET     CustomizationID = @ID
                        WHERE   MementoID = @MementoID
                        
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customization_ChangeName
                                ( CustomizationID ,
                                  MementoID ,
                                  Name ,
                                  AuditRowID
                                )
                        SELECT  CustomizationID ,
                                MementoID ,
                                Name ,
                                @AuditRowID
                        FROM    @CategoryHeaderChangeName
                        WHERE   CustomizationID = @ID
                        
                END
        END
        
        SELECT @MementoID = -1
        
        WHILE (@MementoID IS NOT NULL) BEGIN
        
                SELECT  @MementoID = MIN(MementoID)
                FROM    @CategoryHeaderChangePosition
                WHERE   CustomizationID IS NULL
                AND     MementoID > @MementoID
        
                IF (@MementoID IS NOT NULL) BEGIN
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customization
                                ( CustomizationTypeID, AuditRowID )
                        VALUES  ( 5,
                                  @AuditRowID
                                  )

                        SELECT  @ID = @@IDENTITY
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customizations_Customization
                                ( CustomizationsID ,
                                  CustomizationID ,
                                  AuditRowID
                                )
                        VALUES  ( @CustomizationsID,
                                  @ID,
                                  @AuditRowID
                                )
                        
                        UPDATE  @CategoryHeaderChangePosition
                        SET     CustomizationID = @ID
                        WHERE   MementoID = @MementoID
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customization_ChangePosition
                                ( CustomizationID ,
                                  MementoID ,
                                  Position ,
                                  AuditRowID
                                )
                        SELECT  CustomizationID ,
                                MementoID ,
                                Position ,
                                @AuditRowID
                        FROM    @CategoryHeaderChangePosition
                        WHERE   CustomizationID = @ID
                        
                END
        END
        
        SELECT @MementoID = -1
        
        WHILE (@MementoID IS NOT NULL) BEGIN
        
                SELECT  @MementoID = MIN(MementoID)
                FROM    @StandardHeaderHide
                WHERE   CustomizationID IS NULL
                AND     MementoID > @MementoID
        
                IF (@MementoID IS NOT NULL) BEGIN
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customization
                                ( CustomizationTypeID, AuditRowID )
                        VALUES  ( 6,
                                  @AuditRowID
                                  )

                        SELECT  @ID = @@IDENTITY
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customizations_Customization
                                ( CustomizationsID ,
                                  CustomizationID ,
                                  AuditRowID
                                )
                        VALUES  ( @CustomizationsID,
                                  @ID,
                                  @AuditRowID
                                )
                        
                        UPDATE  @StandardHeaderHide
                        SET     CustomizationID = @ID
                        WHERE   MementoID = @MementoID
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customization_Hide
                                ( CustomizationID ,
                                  MementoID ,
                                  AuditRowID
                                )
                        SELECT  CustomizationID ,
                                MementoID ,
                                @AuditRowID
                        FROM    @StandardHeaderHide
                        WHERE   CustomizationID = @ID
                        
                END
        END
        
        SELECT @MementoID = -1
        
        WHILE (@MementoID IS NOT NULL) BEGIN
        
                SELECT  @MementoID = MIN(MementoID)
                FROM    @OptionHeaderLocation
                WHERE   CustomizationID IS NULL
                AND     MementoID > @MementoID
        
                IF (@MementoID IS NOT NULL) BEGIN
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customization
                                ( CustomizationTypeID, AuditRowID )
                        VALUES  ( 4,
                                  @AuditRowID
                                  )

                        SELECT  @ID = @@IDENTITY
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customizations_Customization
                                ( CustomizationsID ,
                                  CustomizationID ,
                                  AuditRowID
                                )
                        VALUES  ( @CustomizationsID,
                                  @ID,
                                  @AuditRowID
                                )
                        
                        UPDATE  @OptionHeaderLocation
                        SET     CustomizationID = @ID
                        WHERE   MementoID = @MementoID
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customization_ChangeLocation
                                ( CustomizationID ,
                                  MementoID ,
                                  LocationID ,
                                  AuditRowID
                                )
                        SELECT  CustomizationID ,
                                MementoID ,
                                LocationID ,
                                @AuditRowID
                        FROM    @OptionHeaderLocation
                        WHERE   CustomizationID = @ID
                        
                END
        END
        
        SELECT @MementoID = -1
        
        WHILE (@MementoID IS NOT NULL) BEGIN
        
                SELECT  @MementoID = MIN(MementoID)
                FROM    @OptionHeaderHide
                WHERE   CustomizationID IS NULL
                AND     MementoID > @MementoID
        
                IF (@MementoID IS NOT NULL) BEGIN
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customization
                                ( CustomizationTypeID, AuditRowID )
                        VALUES  ( 6,
                                  @AuditRowID
                                  )

                        SELECT  @ID = @@IDENTITY
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customizations_Customization
                                ( CustomizationsID ,
                                  CustomizationID ,
                                  AuditRowID
                                )
                        VALUES  ( @CustomizationsID,
                                  @ID,
                                  @AuditRowID
                                )
                        
                        UPDATE  @OptionHeaderHide
                        SET     CustomizationID = @ID
                        WHERE   MementoID = @MementoID
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customization_Hide
                                ( CustomizationID ,
                                  MementoID ,
                                  AuditRowID
                                )
                        SELECT  CustomizationID ,
                                MementoID ,
                                @AuditRowID
                        FROM    @OptionHeaderHide
                        WHERE   CustomizationID = @ID
                        
                END
        END
        
        SELECT @MementoID = -1
        
        WHILE (@MementoID IS NOT NULL) BEGIN
        
                SELECT  @MementoID = MIN(MementoID)
                FROM    @OptionHeaderChangeName
                WHERE   CustomizationID IS NULL
                AND     MementoID > @MementoID
        
                IF (@MementoID IS NOT NULL) BEGIN
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customization
                                ( CustomizationTypeID, AuditRowID )
                        VALUES  ( 3,
                                  @AuditRowID
                                  )

                        SELECT  @ID = @@IDENTITY
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customizations_Customization
                                ( CustomizationsID ,
                                  CustomizationID ,
                                  AuditRowID
                                )
                        VALUES  ( @CustomizationsID,
                                  @ID,
                                  @AuditRowID
                                )
                        
                        UPDATE  @OptionHeaderChangeName
                        SET     CustomizationID = @ID
                        WHERE   MementoID = @MementoID
                        
                        INSERT INTO Audit.AuditRow
                                ( AuditID ,
                                  ValidFrom ,
                                  ValidUpTo ,
                                  WasInsert ,
                                  WasUpdate ,
                                  WasDelete
                                )
                        VALUES  ( @AuditID,
                                  GETDATE(),
                                  @ValidUpTo,
                                  1,
                                  0,
                                  0
                                )
                                
                        SELECT  @AuditRowID = @@IDENTITY
                        
                        INSERT INTO Customization.Customization_ChangeName
                                ( CustomizationID ,
                                  MementoID ,
                                  Name ,
                                  AuditRowID
                                )
                        SELECT  CustomizationID ,
                                MementoID ,
                                Name ,
                                @AuditRowID
                        FROM    @OptionHeaderChangeName
                        WHERE   CustomizationID = @ID
                        
                END
        END
        
--        COMMIT TRANSACTION T1
--
--END TRY
--
--BEGIN CATCH
--
--        ROLLBACK TRANSACTION T1
--
--END CATCH
