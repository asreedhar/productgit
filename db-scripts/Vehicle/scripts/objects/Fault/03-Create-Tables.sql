CREATE TABLE Fault.Application (
        ApplicationID	INT IDENTITY	NOT NULL,
        [Name]          VARCHAR(50)		NOT NULL,
        CONSTRAINT PK_Fault_Application PRIMARY KEY (ApplicationID),
		CONSTRAINT UK_Fault_Application
			UNIQUE ([Name]),
		CONSTRAINT CK_Fault_Application__Name
			CHECK (LEN([Name]) > 0)
)
ON [DATA]
GO

CREATE TABLE Fault.Machine (
        MachineID		INT IDENTITY	NOT NULL,
        [Name]          VARCHAR(50)		NOT NULL,
        CONSTRAINT PK_Fault_Machine PRIMARY KEY (MachineID),
		CONSTRAINT UK_Fault_Machine
			UNIQUE ([Name]),
		CONSTRAINT CK_Fault_Machine__Name
			CHECK (LEN([Name]) > 0)

)
ON [DATA]
GO

CREATE TABLE Fault.[Platform] (
        PlatformID		INT IDENTITY	NOT NULL,
        [Name]          VARCHAR(50)		NOT NULL,
        CONSTRAINT PK_Platform PRIMARY KEY (PlatformID),
		CONSTRAINT UK_Fault_Platform 
			UNIQUE ([Name]),
		CONSTRAINT CK_Fault_Platform__Name
			CHECK (LEN([Name]) > 0)
)
ON [DATA]
GO

CREATE TABLE Fault.Stack (
        StackID			INT IDENTITY		NOT NULL,
        HashCode		BIGINT				NOT NULL,
        CONSTRAINT PK_Fault_Stack PRIMARY KEY (StackID),
		CONSTRAINT UK_Fault_Stack
			UNIQUE (HashCode)
)
ON [DATA]
GO

CREATE TABLE Fault.Fault (
        FaultID			INT IDENTITY		NOT NULL,
        StackID         INT					NOT NULL,  
		CauseID			INT					NULL,
		ExceptionType	VARCHAR(512)		NOT NULL,
		HashCode		BIGINT				NOT NULL,
        CONSTRAINT PK_Fault_Fault PRIMARY KEY (FaultID),
		CONSTRAINT FK_Fault_Fault__Stack
			FOREIGN KEY (StackID)
			REFERENCES Fault.Stack (StackID),
		CONSTRAINT UK_Fault_Fault
			UNIQUE (HashCode),
		CONSTRAINT CK_Fault_Fault__ExceptionType
			CHECK (LEN(ExceptionType) > 0)
)
ON [DATA]
GO

CREATE TABLE Fault.FaultEvent (
        FaultEventID	INT IDENTITY		NOT NULL,
		FaultTime		DATETIME			NOT NULL,
	    ApplicationID	INT					NULL,
		MachineID		INT					NULL,
		PlatformID		INT					NULL,
		FaultID			INT					NOT NULL,
        CONSTRAINT PK_Fault_FaultEvent PRIMARY KEY (FaultEventID),
		CONSTRAINT FK_Fault_FaultEvent__Application
				FOREIGN KEY (ApplicationID)
				REFERENCES Fault.Application (ApplicationID),
		CONSTRAINT FK_Fault_FaultEvent__Machine
				FOREIGN KEY (MachineID)
				REFERENCES Fault.Machine (MachineID),
		CONSTRAINT FK_Fault_FaultEvent__Platform
				FOREIGN KEY (PlatformID)
				REFERENCES Fault.[Platform] (PlatformID),
		CONSTRAINT FK_Fault_FaultEvent__Fault
				FOREIGN KEY (FaultID)
				REFERENCES Fault.Fault (FaultID)
)
ON [DATA]
GO

CREATE TABLE Fault.Message (
        MessageID		INT IDENTITY		NOT NULL,
        [Message]		VARCHAR(1024),		
        CONSTRAINT PK_Fault_Message PRIMARY KEY (MessageID)
)
ON [DATA]
GO

CREATE TABLE Fault.FaultEvent_Fault_Message (
        FaultEventID	INT  				NOT NULL,
        FaultID			INT  				NOT NULL,
        MessageID       INT					NOT NULL,
        [Index]			INT					NOT NULL,
        CONSTRAINT PK_Fault_FaultEvent_Fault_Message PRIMARY KEY (FaultEventID, FaultID, MessageID, [Index]),
		CONSTRAINT FK_Fault_FaultEvent_Fault_Message__FaultEvent
			FOREIGN KEY (FaultEventID)
			REFERENCES Fault.FaultEvent (FaultEventID),      
		CONSTRAINT FK_Fault_FaultEvent_Fault__Fault
			FOREIGN KEY (FaultID)
			REFERENCES Fault.Fault (FaultID),
		CONSTRAINT FK_Fault_FaultEvent_Fault_Message__Message
			FOREIGN KEY (MessageID)
			REFERENCES Fault.Message (MessageID)			
)
ON [DATA]
GO

CREATE TABLE Fault.FaultData (
        FaultDataID		INT IDENTITY		NOT NULL,
		[Key]    		VARCHAR(256)        NOT NULL,
		[Value] 		VARCHAR(2000)       NOT NULL,
		FaultEventID	INT					NOT NULL,
        CONSTRAINT PK_Fault_FaultData PRIMARY KEY (FaultDataID),
		CONSTRAINT FK_Fault_FaultData__FaultEvent
				FOREIGN KEY (FaultEventID)
				REFERENCES Fault.FaultEvent (FaultEventID),
		CONSTRAINT CK_Fault_FaultData__Key
			CHECK (LEN([Key]) > 0),
		CONSTRAINT CK_Fault_FaultData__Value
			CHECK (LEN([Value]) > 0)
)
ON [DATA]
GO

CREATE TABLE Fault.RequestInformation (
        RequestInformationID   INT IDENTITY NOT NULL,
		[Path]			VARCHAR(2000)		NULL,
		Url				VARCHAR(2000)		NULL,
		[User]			VARCHAR(200)		NULL,
		UserHostAddress VARCHAR(15)			NULL,
		FaultEventID	INT					NOT NULL,
        CONSTRAINT PK_Fault_RequestInformation PRIMARY KEY (RequestInformationID),
		CONSTRAINT FK_Fault_RequestInformation__FaultEvent
				FOREIGN KEY (FaultEventID)
				REFERENCES Fault.FaultEvent (FaultEventID),
		CONSTRAINT CK_Fault_RequestInformation__Path
			CHECK ([Path] IS NULL OR LEN([Path]) > 0),
		CONSTRAINT CK_Fault_RequestInformation__Url
			CHECK (Url IS NULL OR LEN([Url]) > 0),
		CONSTRAINT CK_Fault_RequestInformation__User
			CHECK ([User] IS NULL OR LEN([User]) > 0),
		CONSTRAINT CK_Fault_RequestInformation__UserHostAddress
			CHECK (UserHostAddress IS NULL OR LEN(UserHostAddress) > 0)

)
ON [DATA]
GO

CREATE TABLE Fault.StackFrame (
		StackFrameID	INT IDENTITY		NOT NULL,
        HashCode		BIGINT				NOT NULL,
		ClassName		VARCHAR(2000)		NULL,
		[FileName]		VARCHAR(2000)		NULL,
		IsNativeMethod	BIT					NULL,
		IsUnknownSource	BIT					NULL,
		LineNumber		INT     			NULL,
		MethodName		VARCHAR(2000)		NULL,
        CONSTRAINT PK_Fault_StackFrame PRIMARY KEY (StackFrameID),
		CONSTRAINT CK_Fault_StackFrame__ClassName
			CHECK (ClassName IS NULL OR LEN(ClassName) > 0),
		CONSTRAINT CK_Fault_StackFrame__FileName
			CHECK ([FileName] IS NULL OR LEN([FileName]) > 0),
		CONSTRAINT CK_Fault_StackFrame__MethodName
			CHECK (MethodName IS NULL OR LEN(MethodName) > 0)

)
ON [DATA]
GO

CREATE TABLE Fault.Stack_StackFrame (
		StackID			INT				NOT NULL,
        StackFrameID	INT				NOT NULL,
		SequenceID      SMALLINT        NOT NULL,
        CONSTRAINT PK_Fault_Stack_StackFrame PRIMARY KEY (StackID, StackFrameID, SequenceID),
		CONSTRAINT FK_Fault_Stack_StackFrame__Stack
			FOREIGN KEY (StackID)
			REFERENCES Fault.Stack (StackID),
		CONSTRAINT FK_Fault_Stack_StackFrame__StackFrame
			FOREIGN KEY (StackFrameID)
			REFERENCES Fault.StackFrame (StackFrameID)		

)
ON [DATA]
GO