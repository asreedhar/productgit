IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Fault].[FaultData]') AND type in (N'U'))
DROP TABLE [Fault].[FaultData]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Fault].[RequestInformation]') AND type in (N'U'))
DROP TABLE [Fault].[RequestInformation]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Fault].[Stack_StackFrame]') AND type in (N'U'))
DROP TABLE [Fault].[Stack_StackFrame]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Fault].[StackFrame]') AND type in (N'U'))
DROP TABLE [Fault].[StackFrame]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Fault].[FaultEvent_Fault_Message]') AND type in (N'U'))
DROP TABLE [Fault].[FaultEvent_Fault_Message]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Fault].[Message]') AND type in (N'U'))
DROP TABLE [Fault].[Message]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Fault].[FaultEvent]') AND type in (N'U'))
DROP TABLE [Fault].[FaultEvent]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Fault].[Fault]') AND type in (N'U'))
DROP TABLE [Fault].[Fault]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Fault].[Stack]') AND type in (N'U'))
DROP TABLE [Fault].[Stack]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Fault].[Application]') AND type in (N'U'))
DROP TABLE [Fault].[Application]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Fault].[Machine]') AND type in (N'U'))
DROP TABLE [Fault].[Machine]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Fault].[Platform]') AND type in (N'U'))
DROP TABLE [Fault].[Platform]
GO



