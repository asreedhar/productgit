SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE Employee.Role
(
	RoleID TINYINT NOT NULL,
	Name VARCHAR(50) NULL,
	CONSTRAINT PK_Employee_Role
		PRIMARY KEY CLUSTERED (
			RoleID
		),
	CONSTRAINT UK_Employee_Role__Name
		UNIQUE NONCLUSTERED (
			Name
		),
	CONSTRAINT CK_Employee_Role__Name
		CHECK (
			LEN(NAME) > 0
		)
)
ON [DATA]
GO

INSERT INTO Employee.Role (RoleID, Name) VALUES  (1, 'Salesperson')
INSERT INTO Employee.Role (RoleID, Name) VALUES  (2, 'Appraiser')
INSERT INTO Employee.Role (RoleID, Name) VALUES  (3, 'Buyer')
GO

CREATE TABLE Employee.Employee
(
	EmployeeID INT IDENTITY(1,1) NOT NULL,
	ClientID INT NOT NULL,
	PersonID INT NOT NULL,
	AssociationID INT NOT NULL,
	RevisionNumber SMALLINT NOT NULL,
	CONSTRAINT PK_Employee_Employee
		PRIMARY KEY CLUSTERED (
			EmployeeID
		),
	CONSTRAINT FK_Employee_Employee__Client
		FOREIGN KEY (ClientID)
		REFERENCES Client.Client (ClientID),
	CONSTRAINT FK_Employee_Employee__Person
		FOREIGN KEY (PersonID)
		REFERENCES Person.Person (PersonID),
	CONSTRAINT FK_Employee_Employee__Association
		FOREIGN KEY (AssociationID)
		REFERENCES Audit.Association (AssociationID),
	CONSTRAINT CK_Employee_Employee__RevisionNumber
		CHECK (RevisionNumber > 0)
)
ON [DATA]
GO

CREATE TABLE Employee.Employee_Role
(
	EmployeeID int NOT NULL,
	RoleID tinyint NOT NULL,
	AssociationID int NOT NULL,
	CONSTRAINT PK_Employee_Employee_Role
		PRIMARY KEY CLUSTERED (
			EmployeeID,
			RoleID
		),
	CONSTRAINT UK_Employee_Role__Association
		UNIQUE NONCLUSTERED (
			AssociationID
		),
	CONSTRAINT FK_Employee_Role__Employee
		FOREIGN KEY (
			EmployeeID
		)
		REFERENCES Employee.Employee (
			EmployeeID
		),
	CONSTRAINT FK_Employee_Role__Role
		FOREIGN KEY (
			RoleID
		)
		REFERENCES Employee.Role (
			RoleID
		),
	CONSTRAINT FK_Employee_Role__Association
		FOREIGN KEY (
			AssociationID
		)
		REFERENCES Audit.Association (
			AssociationID
		)
)
ON [DATA]
GO
