
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Employee].[Employee_Role]') AND type in (N'U'))
DROP TABLE [Employee].[Employee_Role]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Employee].[Role]') AND type in (N'U'))
DROP TABLE [Employee].[Role]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Employee].[Employee]') AND type in (N'U'))
DROP TABLE [Employee].[Employee]
GO
