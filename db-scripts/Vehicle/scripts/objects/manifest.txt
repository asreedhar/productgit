
# ---------------------------------------
# CREATE SCHEMA
# ---------------------------------------
Reference\01-Create-Schema.sql
Client\01-Create-Schema.sql
Audit\01-Create-Schema.sql
#Specification\01-Create-Schema.sql
#Notification\01-Create-Schema.sql
#Customization\01-Create-Schema.sql
BlackBook\01-Create-Schema.sql
KelleyBlueBook\01-Create-Schema.sql
Nada\01-Create-Schema.sql
Galves\01-Create-Schema.sql
Edmunds\01-Create-Schema.sql
License\01-Create-Schema.sql
Fault\01-Create-Schema.sql
#Contact\01-Create-Schema.sql
#Person\01-Create-Schema.sql
#Party\01-Create-Schema.sql
#Employee\01-Create-Schema.sql
#Deal\01-Create-Schema.sql

# ---------------------------------------
# DROP TABLES
# ---------------------------------------
Edmunds\02-Drop-Tables.sql
Galves\02-Drop-Tables.sql
Nada\02-Drop-Tables.sql
BlackBook\02-Drop-Tables.sql
KelleyBlueBook\02-Drop-Tables.sql
#Customization\02-Drop-Tables.sql
#Notification\02-Drop-Tables.sql
#Specification\02-Drop-Tables.sql
Audit\02-Drop-Tables.sql
Client\02-Drop-Tables.sql
Reference\02-Drop-Tables.sql
License\02-Drop-Tables.sql
Fault\02-Drop-Tables.sql
#Contact\02-Drop-Tables.sql
#Person\02-Drop-Tables.sql
#Party\02-Drop-Tables.sql
#Employee\02-Drop-Tables.sql
#Deal\02-Drop-Tables.sql

# ---------------------------------------
# CREATE TABLES
# ---------------------------------------
Reference\03-Create-Tables.sql
Client\03-Create-Tables.sql
Audit\03-Create-Tables.sql
Client\04-Create-Tag-Tables.sql
#Specification\03-Create-Tables.sql
#Notification\03-Create-Tables.sql
#Customization\03-Create-Tables.sql
BlackBook\03-Create-File-Groups.sql
BlackBook\04-Create-Partition-Scheme.sql
BlackBook\05-Create-Tables.sql
KelleyBlueBook\03-Create-File-Groups.sql
KelleyBlueBook\04-Create-Partition-Scheme.sql
KelleyBlueBook\05-Create-Tables.sql
Nada\03-Create-File-Groups.sql
Nada\04-Create-Partition-Scheme.sql
Nada\05-Create-Tables.sql
Galves\03-Create-File-Groups.sql
Galves\04-Create-Partition-Scheme.sql
Galves\05-Create-Tables.sql
Edmunds\03-Create-File-Groups.sql
Edmunds\04-Create-Partition-Scheme.sql
Edmunds\05-Create-Tables.sql
License\03-Create-Tables.sql
Fault\03-Create-Tables.sql
#Contact\03-Create-Tables.sql
#Person\03-Create-Tables.sql
#Party\03-Create-Tables.sql
#Employee\03-Create-Tables.sql
#Deal\03-Create-Tables.sql

# ---------------------------------------
# POPULATE TABLES
# ---------------------------------------
#Customization\04-Populate-Tables.sql
Client\05-Create-System-Accounts.sql
