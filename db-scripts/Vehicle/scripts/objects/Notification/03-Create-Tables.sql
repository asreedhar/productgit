
CREATE TABLE Notification.SpecificationProvider (
        SpecificationProviderID TINYINT NOT NULL,
        Name                    VARCHAR(50) NOT NULL,
        CONSTRAINT PK_Notification_SpecificationProvider
                PRIMARY KEY CLUSTERED (
                        SpecificationProviderID
                )
                ON [IDX],
        CONSTRAINT UK_Notification_SpecificationProvider__Name
                UNIQUE NONCLUSTERED (
                        Name
                )
                ON [IDX],
        CONSTRAINT CK_Notification_SpecificationProvider__Name
                CHECK (
                        LEN (Name) > 0
                )
)
ON [DATA]
GO

INSERT INTO Notification.SpecificationProvider
        ( SpecificationProviderID, Name )
VALUES  ( 1,
          'Inventory Load'
          )
GO

CREATE TABLE Notification.SpecificationInformation (
        SpecificationInformationID      INT IDENTITY(1,1) NOT NULL,
        SpecificationProviderID         TINYINT NOT NULL,
        VehicleID                       INT NOT NULL,
        Document                        XML NOT NULL,
        AuditRowID                      INT NOT NULL,
        CONSTRAINT PK_Notification_SpecificationInformation
                PRIMARY KEY CLUSTERED (
                        SpecificationInformationID
                )
                ON [IDX],
        CONSTRAINT FK_Notification_SpecificationInformation__SpecificationProvider
                FOREIGN KEY (
                        SpecificationProviderID
                )
                REFERENCES Notification.SpecificationProvider (
                        SpecificationProviderID
                ),
        CONSTRAINT FK_Notification_SpecificationInformation__Vehicle
                FOREIGN KEY (
                        VehicleID
                )
                REFERENCES Reference.Vehicle (
                        VehicleID
                ),
        CONSTRAINT FK_Notification_SpecificationInformation__AuditRow
                FOREIGN KEY (
                        AuditRowID
                )
                REFERENCES Audit.AuditRow (
                        AuditRowID
                )
)
ON [DATA]
GO

CREATE TABLE Notification.SpecificationInstructionSet (
        SpecificationInstructionSetID   INT IDENTITY(1,1) NOT NULL,
        SpecificationProviderID         TINYINT NOT NULL,
        VehicleID                       INT NOT NULL,
        AuditRowID                      INT NOT NULL,
        CONSTRAINT PK_Notification_SpecificationInstructionSet
                PRIMARY KEY CLUSTERED (
                        SpecificationInstructionSetID
                )
                ON [IDX],
        CONSTRAINT FK_Notification_SpecificationInstructionSet__SpecificationProvider
                FOREIGN KEY (
                        SpecificationProviderID
                )
                REFERENCES Notification.SpecificationProvider (
                        SpecificationProviderID
                ),
        CONSTRAINT FK_Notification_SpecificationInstructionSet__Vehicle
                FOREIGN KEY (
                        VehicleID
                )
                REFERENCES Reference.Vehicle (
                        VehicleID
                ),
        CONSTRAINT FK_Notification_SpecificationInstructionSet__AuditRow
                FOREIGN KEY (
                        AuditRowID
                )
                REFERENCES Audit.AuditRow (
                        AuditRowID
                )
)
ON [DATA]
GO

CREATE TABLE Notification.SpecificationInstructionSet_Body (
        SpecificationInstructionSetID   INT NOT NULL,
        Document                        XML NOT NULL,
        DocumentVersion                 SMALLINT NOT NULL,
        SummaryStyleIsIdentified        BIT NOT NULL,
        SummaryCountInformation         SMALLINT NOT NULL,
        SummaryCountCategories          SMALLINT NOT NULL,
        SummaryCountOptions             SMALLINT NOT NULL,
        AuditRowID                      INT NOT NULL,
        CONSTRAINT PK_Notification_SpecificationInstructionSet_Body
                PRIMARY KEY CLUSTERED (
                        SpecificationInstructionSetID,
                        DocumentVersion
                )
                ON [IDX],
        CONSTRAINT FK_Notification_SpecificationInstructionSet_Body__AuditRow
                FOREIGN KEY (
                        AuditRowID
                )
                REFERENCES Audit.AuditRow (
                        AuditRowID
                ),
        CONSTRAINT CK_Notification_SpecificationInstructionSet__DocumentVersion
                CHECK (
                        DocumentVersion >= 0
                ),
        CONSTRAINT CK_Notification_SpecificationInstructionSet__SummaryCountInformation
                CHECK (
                        SummaryCountInformation >= 0
                ),
        CONSTRAINT CK_Notification_SpecificationInstructionSet__SummaryCountCategories
                CHECK (
                        SummaryCountCategories >= 0
                ),
        CONSTRAINT CK_Notification_SpecificationInstructionSet__SummaryCountOptions
                CHECK (
                        SummaryCountOptions >= 0
                ),
        CONSTRAINT CK_Notification_SpecificationInstructionSet__SummaryStyleIsIdentified
                CHECK (
                        ((SummaryStyleIsIdentified = 0 AND SummaryCountOptions = 0) OR
                         (SummaryStyleIsIdentified = 1 AND SummaryCountOptions >= 0))
                )
)
ON [DATA]
GO

CREATE TABLE Notification.SpecificationInstructionSet_SpecificationInformation (
        SpecificationInstructionSetID   INT NOT NULL,
        DocumentVersion                 SMALLINT NOT NULL,
        SpecificationInformationID      INT NOT NULL,
        AuditRowID                      INT NOT NULL,
        CONSTRAINT PK_Notification_SpecificationInstructionSet_SpecificationInformation
                PRIMARY KEY CLUSTERED (
                        SpecificationInstructionSetID,
                        DocumentVersion
                )
                ON [IDX],
        CONSTRAINT FK_Notification_SpecificationInstructionSet_SpecificationInformation__Information
                FOREIGN KEY (
                        SpecificationInformationID
                )
                REFERENCES Notification.SpecificationInformation (
                        SpecificationInformationID
                ),
        CONSTRAINT FK_Notification_SpecificationInstructionSet_SpecificationInformation__InstructionSet
                FOREIGN KEY (
                        SpecificationInstructionSetID,
                        DocumentVersion
                )
                REFERENCES Notification.SpecificationInstructionSet_Body (
                        SpecificationInstructionSetID,
                        DocumentVersion
                ),
        CONSTRAINT FK_Notification_SpecificationInstructionSet_SpecificationInformation__AuditRow
                FOREIGN KEY (
                        AuditRowID
                )
                REFERENCES Audit.AuditRow (
                        AuditRowID
                )
)
ON [DATA]
GO

CREATE TABLE Notification.SpecificationInstructionSet_Style (
        SpecificationInstructionSetID   INT NOT NULL,
        DocumentVersion                 SMALLINT NOT NULL,
        StyleID                         INT NOT NULL,
        AuditRowID                      INT NOT NULL,
        CONSTRAINT PK_Notification_SpecificationInstructionSet_Style
                PRIMARY KEY NONCLUSTERED (
                        SpecificationInstructionSetID,
                        DocumentVersion
                )
                ON [IDX],
        CONSTRAINT FK_Notification_SpecificationInstructionSet_Style__SpecificationInstructionSet
                FOREIGN KEY (
                        SpecificationInstructionSetID,
                        DocumentVersion
                )
                REFERENCES Notification.SpecificationInstructionSet_Body (
                        SpecificationInstructionSetID,
                        DocumentVersion
                ),
        CONSTRAINT FK_Notification_SpecificationInstructionSet_Style__Style
                FOREIGN KEY (
                        StyleID
                )
                REFERENCES Specification.Style (
                        StyleID
                ),
        CONSTRAINT FK_Notification_SpecificationInstructionSet_Style__AuditRow
                FOREIGN KEY (
                        AuditRowID
                )
                REFERENCES Audit.AuditRow (
                        AuditRowID
                )
)
ON [DATA]
GO

CREATE TABLE Notification.SpecificationInstructionSet_Specification (
        SpecificationInstructionSetID   INT NOT NULL,
        DocumentVersion                 SMALLINT NOT NULL,
        SpecificationID                 INT NOT NULL,
        SpecificationVersion            SMALLINT NOT NULL,
        AuditRowID                      INT NOT NULL,
        CONSTRAINT PK_Notification_SpecificationInstructionSet_Specification
                PRIMARY KEY CLUSTERED (
                        SpecificationID,
                        SpecificationVersion,
                        SpecificationInstructionSetID,
                        DocumentVersion
                )
                ON [IDX],
        CONSTRAINT FK_Notification_SpecificationInstructionSet_Specification__SpecificationInstructionSet
                FOREIGN KEY (
                        SpecificationInstructionSetID,
                        DocumentVersion
                )
                REFERENCES Notification.SpecificationInstructionSet_Body (
                        SpecificationInstructionSetID,
                        DocumentVersion
                ),
        CONSTRAINT FK_Notification_SpecificationInstructionSet_Specification__Specification
                FOREIGN KEY (
                        SpecificationID,
                        SpecificationVersion
                )
                REFERENCES Specification.Specification_Body (
                        SpecificationID,
                        SpecificationVersion
                ),
        CONSTRAINT FK_Notification_SpecificationInstructionSet_Specification__AuditRow
                FOREIGN KEY (
                        AuditRowID
                )
                REFERENCES Audit.AuditRow (
                        AuditRowID
                )
)
ON [DATA]
GO

CREATE TABLE Notification.StyleChange (
        StyleChangeID   INT IDENTITY(1,1) NOT NULL,
        StyleProviderID TINYINT NOT NULL,
        Document        XML NOT NULL,
        AuditRowID      INT NOT NULL,
        CONSTRAINT PK_Notification_StyleChange
                PRIMARY KEY NONCLUSTERED (
                        StyleChangeID
                )
                ON [IDX],
        CONSTRAINT FK_Notification_StyleChange__StyleProvider
                FOREIGN KEY (
                        StyleProviderID
                )
                REFERENCES Specification.StyleProvider (
                        StyleProviderID
                ),
        CONSTRAINT FK_Notification_StyleChange__AuditRow
                FOREIGN KEY (
                        AuditRowID
                )
                REFERENCES Audit.AuditRow (
                        AuditRowID
                )
)
ON [DATA]
GO
