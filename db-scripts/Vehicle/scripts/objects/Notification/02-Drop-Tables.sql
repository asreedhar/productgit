
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Notification].[StyleChange]') AND type in (N'U'))
DROP TABLE [Notification].[StyleChange]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Notification].[SpecificationInstructionSet_Style]') AND type in (N'U'))
DROP TABLE [Notification].[SpecificationInstructionSet_Style]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Notification].[SpecificationInstructionSet_Specification]') AND type in (N'U'))
DROP TABLE [Notification].[SpecificationInstructionSet_Specification]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Notification].[SpecificationInstructionSet_SpecificationInformation]') AND type in (N'U'))
DROP TABLE [Notification].[SpecificationInstructionSet_SpecificationInformation]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Notification].[SpecificationInstructionSet_Body]') AND type in (N'U'))
DROP TABLE [Notification].[SpecificationInstructionSet_Body]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Notification].[SpecificationInstructionSet]') AND type in (N'U'))
DROP TABLE [Notification].[SpecificationInstructionSet]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Notification].[SpecificationInformation]') AND type in (N'U'))
DROP TABLE [Notification].[SpecificationInformation]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Notification].[SpecificationProvider]') AND type in (N'U'))
DROP TABLE [Notification].[SpecificationProvider]
GO
