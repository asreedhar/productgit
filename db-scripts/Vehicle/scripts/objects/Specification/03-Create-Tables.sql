
CREATE TABLE Specification.StyleProvider (
        StyleProviderID TINYINT NOT NULL,
        Name            VARCHAR(50) NOT NULL,
        CONSTRAINT PK_Specification_StyleProvider
                PRIMARY KEY CLUSTERED (
                        StyleProviderID
                )
                ON [DATA],
        CONSTRAINT UK_Specification_StyleProvider
                UNIQUE NONCLUSTERED (
                        Name
                )
                ON [DATA],
        CONSTRAINT CK_Specification_StyleProvider__Name
                CHECK (
                        LEN(Name) > 0
                )
)
ON [DATA]
GO

INSERT INTO Specification.StyleProvider (StyleProviderID, Name) VALUES (1, 'Chrome')
GO

CREATE TABLE Specification.StyleProviderDocumentType (
        StyleProviderDocumentTypeID     TINYINT NOT NULL,
        StyleProviderID                 TINYINT NOT NULL,
        Name                            VARCHAR(50) NOT NULL,
        CONSTRAINT PK_Specification_StyleProviderDocumentType
                PRIMARY KEY CLUSTERED (
                        StyleProviderDocumentTypeID
                )
                ON [IDX],
        CONSTRAINT UK_Specification_StyleProviderDocumentType
                UNIQUE NONCLUSTERED (
                        StyleProviderID,
                        Name
                )
                ON [IDX],
        CONSTRAINT FK_Specification_StyleProviderDocumentType__StyleProvider
                FOREIGN KEY (
                        StyleProviderID
                )
                REFERENCES Specification.StyleProvider (
                        StyleProviderID
                ),
        CONSTRAINT CK_Specification_StyleProviderDocumentType__Name
                CHECK (
                        LEN(Name) > 0
                )
)
ON [DATA]
GO

INSERT INTO Specification.StyleProviderDocumentType (
        StyleProviderDocumentTypeID,
        StyleProviderID,
        Name
) VALUES  (1, 1, 'Chrome Style')

INSERT INTO Specification.StyleProviderDocumentType (
        StyleProviderDocumentTypeID,
        StyleProviderID,
        Name
) VALUES  (2, 1, 'Chrome Reference')

GO

CREATE TABLE Specification.StyleProviderDocument (
        StyleProviderDocumentID         INT IDENTITY(1,1) NOT NULL,
        StyleProviderDocumentTypeID     TINYINT NOT NULL,
        Document                        XML NOT NULL,
        DocumentVersion                 INT NOT NULL,
        AuditRowID                      INT NOT NULL,
        CONSTRAINT PK_Specification_StyleProviderDocument
                PRIMARY KEY CLUSTERED (
                        StyleProviderDocumentID
                )
                ON [IDX],
        CONSTRAINT FK_Specification_StyleProviderDocument__StyleProviderDocumentType
                FOREIGN KEY (
                        StyleProviderDocumentTypeID
                )
                REFERENCES Specification.StyleProviderDocumentType (
                        StyleProviderDocumentTypeID
                ),
        CONSTRAINT FK_Specification_StyleProviderDocument__AuditRow
                FOREIGN KEY (
                        AuditRowID
                )
                REFERENCES Audit.AuditRow (
                        AuditRowID
                ),
        CONSTRAINT CK_Specification_StyleProviderDocument__DocumentVersion
                CHECK (
                        DocumentVersion > 0
                )
)
ON [DATA]
GO

CREATE TABLE Specification.Style (
        StyleID         INT IDENTITY(1,1) NOT NULL,
        StyleProviderID TINYINT NOT NULL,
        AuditRowID      INT NOT NULL,
        CONSTRAINT PK_Specification_Style
                PRIMARY KEY CLUSTERED (
                        StyleID
                )
                ON [IDX],
        CONSTRAINT FK_Specification_Style__AuditRow
                FOREIGN KEY (
                        AuditRowID
                )
                REFERENCES Audit.AuditRow (
                        AuditRowID
                )
)
ON [DATA]
GO

CREATE TABLE Specification.StyleSummary (
        StyleID         INT NOT NULL,
        Manufacturer    VARCHAR(100) NULL,
        Division        VARCHAR(100) NULL,
        Subdivision     VARCHAR(100) NULL,
        Model           VARCHAR(100) NULL,
        Style           VARCHAR(100) NULL,
        Trim            VARCHAR(100) NULL,
        BodyStyle       VARCHAR(100) NULL,
        ModelYear       SMALLINT NOT NULL,
        AuditRowID      INT NOT NULL,
        CONSTRAINT PK_Specification_StyleSummary
                PRIMARY KEY CLUSTERED (
                        StyleID
                )
                ON [IDX],
        CONSTRAINT FK_Specification_StyleSummary__Style
                FOREIGN KEY (
                        StyleID
                )
                REFERENCES Specification.Style (
                        StyleID
                ),
        CONSTRAINT FK_Specification_StyleSummary__AuditRow
                FOREIGN KEY (
                        AuditRowID
                )
                REFERENCES Audit.AuditRow (
                        AuditRowID
                ),
        CONSTRAINT CK_Specification_StyleSummary__ModelYear
                CHECK (
                        ModelYear >= 1980
                )
)
ON [DATA]
GO

CREATE TABLE Specification.Style_Chrome (
        StyleID                         INT NOT NULL,
        ChromeStyleID                   INT NOT NULL,
        ChromeStyleDocumentID           INT NOT NULL,
        ChromeReferenceDocumentID       INT NOT NULL,
        AuditRowID                      INT NOT NULL,
        CONSTRAINT PK_Specification_Style_Chrome
                PRIMARY KEY CLUSTERED (
                        StyleID
                )
                ON [IDX],
        CONSTRAINT UK_Specification_Style_Chrome__StyleDocument
                UNIQUE NONCLUSTERED (
                        ChromeStyleDocumentID
                )
                ON [IDX],
        CONSTRAINT FK_Specification_Style_Chrome__Style
                FOREIGN KEY (
                        StyleID
                )
                REFERENCES Specification.Style (
                        StyleID
                ),
        CONSTRAINT FK_Specification_Style_Chrome__ChromeStyleDocument
                FOREIGN KEY (
                        ChromeStyleDocumentID
                )
                REFERENCES Specification.StyleProviderDocument (
                        StyleProviderDocumentID
                ),
        CONSTRAINT FK_Specification_Style_Chrome__ChromeReferenceDocument
                FOREIGN KEY (
                        ChromeReferenceDocumentID
                )
                REFERENCES Specification.StyleProviderDocument (
                        StyleProviderDocumentID
                ),
        CONSTRAINT FK_Specification_Style_Chrome__AuditRow
                FOREIGN KEY (
                        AuditRowID
                )
                REFERENCES Audit.AuditRow (
                        AuditRowID
                )
)
ON [DATA]
GO

CREATE TABLE Specification.Style_Chrome_Status (
        StyleID                 INT NOT NULL,
        IsMappingUpdated        BIT NOT NULL,
        IsDeleted               BIT NOT NULL,
        IsUpdated               BIT NOT NULL,
        AuditRowID              INT NOT NULL,
        CONSTRAINT PK_Specification_Style_Chrome_Status
                PRIMARY KEY CLUSTERED (
                        StyleID,
                        AuditRowID
                ),
        CONSTRAINT FK_Specification_Style_Chrome_Status__Style
                FOREIGN KEY (
                        StyleID
                )
                REFERENCES Specification.Style_Chrome (
                        StyleID
                ),
        CONSTRAINT FK_Specification_Style_Chrome_Status__AuditRow
                FOREIGN KEY (
                        AuditRowID
                )
                REFERENCES Audit.AuditRow (
                        AuditRowID
                )
)
ON [DATA]
GO

CREATE TABLE Specification.SpecificationType (
        SpecificationTypeID     TINYINT NOT NULL,
        Name                    VARCHAR(50) NOT NULL,
        CONSTRAINT PK_Specification_SpecificationType
                PRIMARY KEY CLUSTERED (
                        SpecificationTypeID
                )
                ON [IDX],
        CONSTRAINT UK_Specification_SpecificationType__Name
                UNIQUE (
                        Name
                )
                ON [IDX],
        CONSTRAINT CK_Specification_SpecificationType__Name
                CHECK (
                        LEN(Name) > 0
                )
)
ON [DATA]
GO

INSERT INTO Specification.SpecificationType (SpecificationTypeID, Name) VALUES  (1, 'System')
INSERT INTO Specification.SpecificationType (SpecificationTypeID, Name) VALUES  (2, 'Dealer')
GO

CREATE TABLE Specification.Specification (
        SpecificationID         INT IDENTITY(1,1) NOT NULL,
        SpecificationTypeID     TINYINT NOT NULL,
        VehicleID               INT NOT NULL,
        AuditRowID              INT NOT NULL,
        CONSTRAINT PK_Specification_Specification
                PRIMARY KEY CLUSTERED (
                        SpecificationID
                )
                ON [IDX],
        CONSTRAINT FK_Specification_Specification__SpecificationType
                FOREIGN KEY (
                        SpecificationTypeID
                )
                REFERENCES Specification.SpecificationType (
                        SpecificationTypeID
                ),
        CONSTRAINT FK_Specification_Specification__Vehicle
                FOREIGN KEY (
                        VehicleID
                )
                REFERENCES Reference.Vehicle (
                        VehicleID
                ),
        CONSTRAINT FK_Specification_Specification__AuditRow
                FOREIGN KEY (
                        AuditRowID
                )
                REFERENCES Audit.AuditRow (
                        AuditRowID
                )
)
ON [DATA]
GO

CREATE TABLE Specification.Specification_Body (
        SpecificationID         INT NOT NULL,
        SpecificationVersion    SMALLINT NOT NULL,
        StyleID                 INT NOT NULL,
        VehicleDocument         XML NOT NULL,
        BuildDocument           XML NOT NULL,
        AuditRowID              INT NOT NULL,
        CONSTRAINT PK_Specification_Specification_Body
                PRIMARY KEY (
                        SpecificationID,
                        SpecificationVersion
                )
                ON [IDX],
        CONSTRAINT FK_Specification_Specification_Body__Specification
                FOREIGN KEY (
                        SpecificationID
                )
                REFERENCES Specification.Specification (
                        SpecificationID
                ),
        CONSTRAINT FK_Specification_Specification_Body__Style
                FOREIGN KEY (
                        StyleID
                )
                REFERENCES Specification.Style (
                        StyleID
                ),
        CONSTRAINT FK_Specification_Specification_Body__AuditRow
                FOREIGN KEY (
                        AuditRowID
                )
                REFERENCES Audit.AuditRow (
                        AuditRowID
                )
)
ON [DATA]
GO

CREATE TABLE Specification.Specification_Client (
        SpecificationID INT NOT NULL,
        ClientID        INT NOT NULL,
        AuditRowID      INT NOT NULL,
        CONSTRAINT PK_Specification_Specification_Client
                PRIMARY KEY CLUSTERED (
                        SpecificationID
                )
                ON [IDX],
        CONSTRAINT FK_Specification_Specification_Client__Specification
                FOREIGN KEY (
                        SpecificationID
                )
                REFERENCES Specification.Specification (
                        SpecificationID
                ),
        CONSTRAINT FK_Specification_Specification_Client__Client
                FOREIGN KEY (
                        ClientID
                )
                REFERENCES Client.Client (
                        ClientID
                ),
        CONSTRAINT FK_Specification_Specification_Client__AuditRowID
                FOREIGN KEY (
                        AuditRowID
                )
                REFERENCES Audit.AuditRow (
                        AuditRowID
                )
)
ON [DATA]
GO
