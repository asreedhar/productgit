
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Specification].[Specification_Client]') AND type in (N'U'))
DROP TABLE [Specification].[Specification_Client]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Specification].[Specification_Body]') AND type in (N'U'))
DROP TABLE [Specification].[Specification_Body]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Specification].[Specification]') AND type in (N'U'))
DROP TABLE [Specification].[Specification]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Specification].[SpecificationType]') AND type in (N'U'))
DROP TABLE [Specification].[SpecificationType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Specification].[Style_Chrome_Status]') AND type in (N'U'))
DROP TABLE [Specification].[Style_Chrome_Status]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Specification].[Style_Chrome]') AND type in (N'U'))
DROP TABLE [Specification].[Style_Chrome]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Specification].[StyleSummary]') AND type in (N'U'))
DROP TABLE [Specification].[StyleSummary]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Specification].[Style]') AND type in (N'U'))
DROP TABLE [Specification].[Style]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Specification].[StyleProviderDocument]') AND type in (N'U'))
DROP TABLE [Specification].[StyleProviderDocument]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Specification].[StyleProviderDocumentType]') AND type in (N'U'))
DROP TABLE [Specification].[StyleProviderDocumentType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Specification].[StyleProvider]') AND type in (N'U'))
DROP TABLE [Specification].[StyleProvider]
GO