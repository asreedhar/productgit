
-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.DataLoad -> Partitioning
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.DataLoad (
    DataLoadID      INT         NOT NULL,
    DataLoadTime    DATETIME    NOT NULL,
    
    CONSTRAINT PK_BlackBook_DataLoad
        PRIMARY KEY CLUSTERED (DataLoadID)
)    
ON PS_BlackBook__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.State -> Reference Data; Static
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.State (
    StateID     TINYINT IDENTITY(1,1)   NOT NULL,
    Name        VARCHAR(64)             NOT NULL,
    Code        CHAR(2)                 NOT NULL,
    
    CONSTRAINT PK_BlackBook_State
        PRIMARY KEY CLUSTERED (StateID),
    
    CONSTRAINT UK_BlackBook_State__Name
        UNIQUE NONCLUSTERED (Name),
    
    CONSTRAINT UK_BlackBook_State__Code
        UNIQUE NONCLUSTERED (Code)
)
ON [DATA]
GO

INSERT INTO BlackBook.State ( Code, Name ) VALUES ( '',   'National')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'AL', 'Alabama')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'AK', 'Alaska')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'AZ', 'Arizona')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'AR', 'Arkansas')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'CA', 'California')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'CO', 'Colorado')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'CT', 'Connecticut')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'DE', 'Delaware')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'FL', 'Florida')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'GA', 'Georgia')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'HI', 'Hawaii')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'ID', 'Idaho')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'IL', 'Illinois')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'IN', 'Indiana')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'IA', 'Iowa')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'KS', 'Kansas')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'KY', 'Kentucky')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'LA', 'Louisiana')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'ME', 'Maine')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'MD', 'Maryland')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'MA', 'Massachusetts')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'MI', 'Michigan')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'MN', 'Minnesota')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'MS', 'Mississippi')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'MO', 'Missouri')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'MT', 'Montana')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'NE', 'Nebraska')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'NV', 'Nevada')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'NH', 'New Hampshire')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'NJ', 'New Jersey')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'NM', 'New Mexico')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'NY', 'New York')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'NC', 'North Carolina')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'ND', 'North Dakota')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'OH', 'Ohio')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'OK', 'Oklahoma')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'OR', 'Oregon')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'PA', 'Pennsylvania')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'RI', 'Rhode Island')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'SC', 'South Carolina')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'SD', 'South Dakota')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'TN', 'Tennessee')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'TX', 'Texas')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'UT', 'Utah')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'VT', 'Vermont')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'VA', 'Virginia')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'WA', 'Washington')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'WV', 'West Virginia')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'WI', 'Wisconsin')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'WY', 'Wyoming')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'PR', 'Puerto Rico')
INSERT INTO BlackBook.State ( Code, Name ) VALUES ( 'DC', 'District of Columbia')
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.Car -> Reference Data
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.Car (
    CarID           INT IDENTITY(1,1)   NOT NULL,
    Uvc             CHAR(10)            NOT NULL,
    Vin             CHAR(9)             NOT NULL, -- characters 1 through 8 and 10
    StateID         TINYINT             NOT NULL,
    
    CONSTRAINT PK_BlackBook_Car
	    PRIMARY KEY CLUSTERED (CarID),
    
    CONSTRAINT UK_BlackBook_Car
	    UNIQUE NONCLUSTERED (Uvc, Vin, StateID),
    
    CONSTRAINT FK_BlackBook_Car__State
	    FOREIGN KEY (StateID)
	    REFERENCES BlackBook.State (StateID),
    
    CONSTRAINT CK_BlackBook_Car__LenUvc
	    CHECK (LEN(Uvc) = 10),
    
    CONSTRAINT CK_BlackBook_Car__LenVin
	    CHECK (LEN(Vin) = 9)
)
ON [DATA]
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.CachePolicy -> Static
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.CachePolicy (
    CachePolicy TINYINT NOT NULL,
    [Hour]      TINYINT NOT NULL,
    [Minute]    TINYINT NOT NULL,
    [Second]    TINYINT NOT NULL,
    
    CONSTRAINT PK_BlackBook_CachePolicy
        PRIMARY KEY CLUSTERED (CachePolicy),
    
    CONSTRAINT CK_BlackBook_CachePolicy__CachePolicy
        CHECK (CachePolicy = 1),
    
    CONSTRAINT CK_BlackBook_CachePolicy__Hour
        CHECK ([Hour] BETWEEN 0 AND 23),
    
    CONSTRAINT CK_BlackBook_CachePolicy__Minute
        CHECK ([Minute] BETWEEN 0 AND 59),
    
    CONSTRAINT CK_BlackBook_CachePolicy__Second
        CHECK ([Second] BETWEEN 0 AND 59)
)
ON [DATA]
GO

INSERT INTO BlackBook.CachePolicy ( CachePolicy, Hour, Minute, Second )
VALUES  ( 1,
          23, -- Hour - tinyint
          59, -- Minute - tinyint
          59  -- Second - tinyint
          )
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.Car_CachePolicy -> Reference Data
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.Car_CachePolicy (
    CarID       INT NOT NULL,
    LastCheck   DATETIME NOT NULL,
    NextCheck   DATETIME NOT NULL,
    
    CONSTRAINT PK_BlackBook_Car_CachePolicy
        PRIMARY KEY CLUSTERED (CarID),
    
    CONSTRAINT CK_BlackBook_Car_CachePolicy
        CHECK (NextCheck > LastCheck)
)
ON [DATA]
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.Car_History -> Reference Data; Partitioned
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.Car_History (
	CarHistoryID    INT IDENTITY(1,1)   NOT NULL,
	CarID           INT                 NOT NULL,
    DataLoadID      INT                 NOT NULL,
	CarHashCode     BIGINT              NOT NULL,
	AuditRowID      INT                 NOT NULL,
	
    CONSTRAINT PK_BlackBook_Car_History
	    PRIMARY KEY CLUSTERED (CarHistoryID, DataLoadID),
    
    CONSTRAINT UK_BlackBook_Car_History__Car_CarHashCode
	    UNIQUE NONCLUSTERED (CarID, CarHashCode, DataLoadID),
    
    CONSTRAINT FK_BlackBook_Car_History__Car
        FOREIGN KEY (CarID)
        REFERENCES BlackBook.Car (CarID),
    
    CONSTRAINT FK_BlackBook_Car_History__AuditRow
        FOREIGN KEY (AuditRowID)
        REFERENCES Audit.AuditRow (AuditRowID),
        
    CONSTRAINT FK_BlackBook_Car_History__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES BlackBook.DataLoad (DataLoadID)
)
ON PS_BlackBook__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.ResidualValue -> Reference Data; Partitioned
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.ResidualValue (
    CarHistoryID    INT                 NOT NULL,
    DataLoadID      INT                 NOT NULL,
    ResidualValueID INT IDENTITY(1,1)   NOT NULL,    
    Resid12         INT                 NULL,
    Resid24         INT                 NULL,
    Resid30         INT                 NULL,
    Resid36         INT                 NULL,
    Resid42         INT                 NULL,
    Resid48         INT                 NULL,
    Resid60         INT                 NULL,
    Resid72         INT                 NULL,
    
    CONSTRAINT PK_BlackBook_ResidualValue
        PRIMARY KEY CLUSTERED (CarHistoryID, ResidualValueID, DataLoadID),
    
    CONSTRAINT FK_BlackBook_ResidualValue__Car_History
        FOREIGN KEY (CarHistoryID, DataLoadID)
        REFERENCES BlackBook.Car_History (CarHistoryID, DataLoadID),
        
    CONSTRAINT FK_BlackBook_ResidualValue__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES BlackBook.DataLoad (DataLoadID)
)
ON PS_BlackBook__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.Value -> Reference Data; Partitioned
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.Value (
    CarHistoryID    INT                 NOT NULL,
    DataLoadID      INT                 NOT NULL,
    ValueID         INT IDENTITY(1,1)   NOT NULL,    
    ExtraClean      INT                 NULL,
    Clean           INT                 NULL,
    Average         INT                 NULL,
    Rough           INT                 NULL,
    
    CONSTRAINT PK_BlackBook_Value
        PRIMARY KEY CLUSTERED (CarHistoryID, ValueID, DataLoadID),
    
    CONSTRAINT FK_BlackBook_Value__Car_History
        FOREIGN KEY (CarHistoryID, DataLoadID)
        REFERENCES BlackBook.Car_History (CarHistoryID, DataLoadID),
        
    CONSTRAINT FK_BlackBook_Value__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES BlackBook.DataLoad (DataLoadID)
)
ON PS_BlackBook__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.Car_Description -> Reference Data; Partitioned
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.Car_Description (
    CarHistoryID    INT             NOT NULL,
    DataLoadID      INT             NOT NULL,
    Year            INT             NOT NULL,
    Make            VARCHAR(128)    NOT NULL,
    Model           VARCHAR(128)    NOT NULL,
    Series          VARCHAR(128)    NOT NULL,
    BodyStyle       VARCHAR(128)    NOT NULL,
  
    CONSTRAINT PK_BlackBook_Car_Description
        PRIMARY KEY CLUSTERED (CarHistoryID, DataLoadID),

    CONSTRAINT FK_BlackBook_Car_Description__Car_History
        FOREIGN KEY (CarHistoryID, DataLoadID)
        REFERENCES BlackBook.Car_History (CarHistoryID, DataLoadID),
        
    CONSTRAINT FK_BlackBook_Car_Description__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES BlackBook.DataLoad (DataLoadID)
)
ON PS_BlackBook__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.Car_Prices -> Reference Data; Partitioned
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.Car_Prices (
    CarHistoryID      INT             NOT NULL,
    DataLoadID        INT             NOT NULL,
    Mileage           INT             NULL,
    Msrp              INT             NULL,
    FinanceAdvance    INT             NULL,
    ResidualValueID   INT             NOT NULL,
    TradeInValueID    INT             NOT NULL,
    WholesaleValueID  INT             NOT NULL,
    RetailValueID     INT             NOT NULL,
    PriceIncludes     VARCHAR(64)     NULL,
    
    CONSTRAINT PK_BlackBook_Car_Prices
        PRIMARY KEY CLUSTERED (CarHistoryID, DataLoadID),
    
    CONSTRAINT FK_BlackBook_Car_Prices__Car_History
        FOREIGN KEY (CarHistoryID, DataLoadID)
        REFERENCES BlackBook.Car_History (CarHistoryID, DataLoadID),
    
    CONSTRAINT FK_BlackBook_Car_Prices__ResidualValue
        FOREIGN KEY (CarHistoryID, ResidualValueID, DataLoadID)
        REFERENCES BlackBook.ResidualValue (CarHistoryID, ResidualValueID, DataLoadID),
    
    CONSTRAINT FK_BlackBook_Car_Prices__TradeInValue
        FOREIGN KEY (CarHistoryID, TradeInValueID, DataLoadID)
        REFERENCES BlackBook.Value (CarHistoryID, ValueID, DataLoadID),
	
    CONSTRAINT FK_BlackBook_Car_Prices__WholesaleValue
        FOREIGN KEY (CarHistoryID, WholesaleValueID, DataLoadID)
        REFERENCES BlackBook.Value (CarHistoryID, ValueID, DataLoadID),
	
    CONSTRAINT FK_BlackBook_Car_Prices__RetailValue
        FOREIGN KEY (CarHistoryID, RetailValueID, DataLoadID)
        REFERENCES BlackBook.Value (CarHistoryID, ValueID, DataLoadID),
        
    CONSTRAINT FK_BlackBook_Car_Prices__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES BlackBook.DataLoad (DataLoadID)
)
ON PS_BlackBook__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.Car_Attributes -> Reference Data; Partitioned
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.Car_Attributes (
    CarHistoryID        INT          NOT NULL,
    DataLoadID          INT          NOT NULL,
    WheelBase           VARCHAR(128) NOT NULL,
    TaxableHorsepower   VARCHAR(128) NOT NULL,
    Weight              VARCHAR(128) NOT NULL,
    TireSize            VARCHAR(128) NOT NULL,
    BaseHorsepower      VARCHAR(128) NOT NULL,
    FuelType            VARCHAR(128) NOT NULL,
    Cylinders           VARCHAR(128) NOT NULL,
    DriveTrain          VARCHAR(128) NOT NULL,
    Transmission        VARCHAR(128) NOT NULL,
    Engine              VARCHAR(128) NOT NULL,
    VehicleClass        VARCHAR(128) NOT NULL,
    
    CONSTRAINT PK_BlackBook_Car_Attributes
        PRIMARY KEY CLUSTERED (CarHistoryID, DataLoadID),
    
    CONSTRAINT FK_BlackBook_Car_Attributes__Car_History
        FOREIGN KEY (CarHistoryID, DataLoadID)
        REFERENCES BlackBook.Car_History (CarHistoryID, DataLoadID),
        
    CONSTRAINT FK_BlackBook_Car_Attributes__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES BlackBook.DataLoad (DataLoadID)
)
ON PS_BlackBook__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.Color -> Reference Data; Partitioned
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.Color (
    CarHistoryID    INT                 NOT NULL,
    DataLoadID      INT                 NOT NULL,
    ColorID         INT IDENTITY(1,1)   NOT NULL,
    Category        VARCHAR(128)        NOT NULL,
    Description     VARCHAR(128)        NOT NULL,
    
    CONSTRAINT PK_BlackBook_Color
        PRIMARY KEY CLUSTERED (CarHistoryID, ColorID, DataLoadID),
    
    CONSTRAINT UK_BlackBook_Color
        UNIQUE NONCLUSTERED (CarHistoryID, Category, Description, DataLoadID),
    
    CONSTRAINT FK_BlackBook_Color__Car_History
        FOREIGN KEY (CarHistoryID, DataLoadID)
        REFERENCES BlackBook.Car_History (CarHistoryID, DataLoadID),
        
    CONSTRAINT FK_BlackBook_Color__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES BlackBook.DataLoad (DataLoadID)
)
ON PS_BlackBook__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.Swatch -> Reference Data; Partitioned
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.Swatch (
    CarHistoryID    INT     NOT NULL,
    DataLoadID      INT     NOT NULL,
    ColorID         INT     NOT NULL,
    HtmlIndex       TINYINT NOT NULL,
    HtmlColor       CHAR(7) NOT NULL,
    
    CONSTRAINT PK_BlackBook_Swatch
        PRIMARY KEY CLUSTERED (CarHistoryID, ColorID, HtmlColor, DataLoadID),
    
    CONSTRAINT FK_BlackBook_Swatch__Car_History
        FOREIGN KEY (CarHistoryID, DataLoadID)
        REFERENCES BlackBook.Car_History (CarHistoryID, DataLoadID),
    
    CONSTRAINT FK_BlackBook_Swatch__Color
        FOREIGN KEY (CarHistoryID, ColorID, DataLoadID)
        REFERENCES BlackBook.Color (CarHistoryID, ColorID, DataLoadID),
        
    CONSTRAINT FK_BlackBook_Swatch__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES BlackBook.DataLoad (DataLoadID),
    
    CONSTRAINT CK_BlackBook_Swatch__HtmlIndex
        CHECK (HtmlIndex >= 0),
    
    CONSTRAINT CK_BlackBook_Swatch__HtmlColor
        CHECK (LEN(LTRIM(RTRIM(HtmlColor))) = 7)
)
ON PS_BlackBook__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.OptionAdjustmentSign -> Reference Data; Static
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.OptionAdjustmentSign (
    OptionAdjustmentSignID  TINYINT     NOT NULL,
    Name                    VARCHAR(8)  NOT NULL,
    
    CONSTRAINT PK_BlackBook_OptionAdjustmentSign
        PRIMARY KEY CLUSTERED (OptionAdjustmentSignID),
    
    CONSTRAINT UK_BlackBook_OptionAdjustmentSign
        UNIQUE (Name),
    
    CONSTRAINT CK_BlackBook_OptionAdjustmentSign
        CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

INSERT INTO BlackBook.OptionAdjustmentSign
        ( OptionAdjustmentSignID, Name )
VALUES  ( 1,
          'Add'
          )
GO

INSERT INTO BlackBook.OptionAdjustmentSign
        ( OptionAdjustmentSignID, Name )
VALUES  ( 2,
          'Deduct'
          )
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.Options -> Reference Data; Partitioned
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.Options (
    CarHistoryID            INT             NOT NULL,
    DataLoadID              INT             NOT NULL,
    AdCode                  CHAR(2)         NOT NULL,
    Description             VARCHAR(128)    NOT NULL,
    OptionAdjustmentSignID  TINYINT         NOT NULL,
    Amount                  INT             NULL,
    ResidualValueID         INT             NOT NULL,
    Flag                    BIT             NOT NULL,
    
    CONSTRAINT PK_BlackBook_Options
        PRIMARY KEY (CarHistoryID, AdCode, DataLoadID),
    
    CONSTRAINT FK_BlackBook_Options__Car_History
        FOREIGN KEY (CarHistoryID, DataLoadID)
        REFERENCES BlackBook.Car_History (CarHistoryID, DataLoadID),
    
    CONSTRAINT FK_BlackBook_Options__OptionAdjustmentSign
        FOREIGN KEY (OptionAdjustmentSignID)
        REFERENCES BlackBook.OptionAdjustmentSign (OptionAdjustmentSignID),
    
    CONSTRAINT FK_BlackBook_Options__ResidualValue
        FOREIGN KEY (CarHistoryID, ResidualValueID, DataLoadID)
        REFERENCES BlackBook.ResidualValue (CarHistoryID, ResidualValueID, DataLoadID),

    CONSTRAINT FK_BlackBook_Options__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES BlackBook.DataLoad (DataLoadID)
)
ON PS_BlackBook__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.MileageAdjustment -> Reference Data; Partitioned
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.MileageAdjustment (
    CarHistoryID        INT                 NOT NULL,        
    MileageAdjustmentID INT IDENTITY(1,1)   NOT NULL,
    DataLoadID          INT                 NOT NULL,
    BeginRange          INT                 NOT NULL,
    EndRange            INT                 NOT NULL,
    FinanceAdvance      INT                 NULL,
    ValueID             INT                 NOT NULL,
    
    CONSTRAINT PK_BlackBook_MileageAdjustment
        PRIMARY KEY CLUSTERED (CarHistoryID, MileageAdjustmentID, DataLoadID),
    
    CONSTRAINT UK_BlackBook_MileageAdjustment
        UNIQUE NONCLUSTERED (CarHistoryID, BeginRange, EndRange, DataLoadID),
    
    CONSTRAINT FK_BlackBook_MileageAdjustment__Car_History
        FOREIGN KEY (CarHistoryID, DataLoadID)
        REFERENCES BlackBook.Car_History (CarHistoryID, DataLoadID),
	
    CONSTRAINT FK_BlackBook_MileageAdjustment__Value
        FOREIGN KEY (CarHistoryID, ValueID, DataLoadID)
        REFERENCES BlackBook.Value (CarHistoryID, ValueID, DataLoadID),
        
    CONSTRAINT FK_BlackBook_MileageAdjustment__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES BlackBook.DataLoad (DataLoadID),
    
    CONSTRAINT CK_BlackBook_MileageAdjustment__BeginRange
        CHECK (BeginRange >= 0),
    
    CONSTRAINT CK_BlackBook_MileageAdjustment__EndRange
        CHECK (EndRange >= BeginRange)
)
ON PS_BlackBook__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.Category -> Standard Equipment; Static
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.Category (
    CategoryID          INT IDENTITY(1,1)   NOT NULL,
    Name                VARCHAR(128) COLLATE Latin1_General_CS_AS NOT NULL,

    CONSTRAINT PK_BlackBook_Category
        PRIMARY KEY CLUSTERED (CategoryID),

    CONSTRAINT UK_BlackBook_Category
        UNIQUE NONCLUSTERED (Name),

    CONSTRAINT CK_BlackBook_Category__Name
        CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.SubCategory -> Standard Equipment; Static
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.SubCategory (
    SubCategoryID       INT IDENTITY(1,1)   NOT NULL,
    CategoryID          INT                 NOT NULL,
    Name                VARCHAR(128) COLLATE Latin1_General_CS_AS NOT NULL,
    
    CONSTRAINT PK_BlackBook_SubCategory
        PRIMARY KEY CLUSTERED (SubCategoryID),
    
    CONSTRAINT UK_BlackBook_SubCategory
        UNIQUE NONCLUSTERED (CategoryID, Name),
    
    CONSTRAINT FK_BlackBook_SubCategory__Category
        FOREIGN KEY (CategoryID)
        REFERENCES BlackBook.Category (CategoryID),
    
    CONSTRAINT CK_BlackBook_SubCategory__Name
        CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.StandardEquipment -> Standard Equipment; Static
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.StandardEquipment (
    StandardEquipmentID INT IDENTITY(1,1)   NOT NULL,
    Uvc                 CHAR(10)            NOT NULL,
    
    CONSTRAINT PK_BlackBook_StandardEquipment
        PRIMARY KEY CLUSTERED (StandardEquipmentID),
    
    CONSTRAINT UK_BlackBook_StandardEquipment
        UNIQUE NONCLUSTERED (Uvc)
)
ON [DATA]
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.StandardEquipment_CachePolicy -> Standard Equipment; Static
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.StandardEquipment_CachePolicy (
    StandardEquipmentID INT NOT NULL,
    LastCheck           DATETIME NOT NULL,
    NextCheck           DATETIME NOT NULL,
    
    CONSTRAINT PK_BlackBook_StandardEquipment_CachePolicy
        PRIMARY KEY CLUSTERED (StandardEquipmentID),
    
    CONSTRAINT CK_BlackBook_StandardEquipment_CachePolicy
        CHECK (NextCheck > LastCheck)
)
ON [DATA]
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.StandardEquipment_History -> Standard Equipment; Partitioned
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.StandardEquipment_History (
    StandardEquipmentHistoryID  INT IDENTITY(1,1)   NOT NULL,
    DataLoadID                  INT                 NOT NULL,
    StandardEquipmentID         INT                 NOT NULL,    
	StandardEquipmentHashCode   BIGINT              NOT NULL,
    AuditRowID                  INT                 NOT NULL,	
    
    CONSTRAINT PK_BlackBook_StandardEquipment_History
        PRIMARY KEY (StandardEquipmentHistoryID, DataLoadID),
	
    CONSTRAINT UK_BlackBook_StandardEquipment_History__ID_HashCode
	    UNIQUE NONCLUSTERED (StandardEquipmentID, StandardEquipmentHashCode, DataLoadID),
    
    CONSTRAINT FK_BlackBook_StandardEquipment_History__StandardEquipment
        FOREIGN KEY (StandardEquipmentID)
        REFERENCES BlackBook.StandardEquipment (StandardEquipmentID),
        
    CONSTRAINT FK_BlackBook_StandardEquipment_History__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES BlackBook.DataLoad (DataLoadID)
)
ON PS_BlackBook__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.StandardEquipmentRecord -> Standard Equipment; Partitioned
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.StandardEquipmentRecord (
    StandardEquipmentHistoryID  INT             NOT NULL,
    DataLoadID                  INT             NOT NULL,
    CategoryID                  INT             NOT NULL,
    SubCategoryID               INT             NOT NULL,
    Value                       VARCHAR(128)    NOT NULL,
    
    CONSTRAINT PK_BlackBook_StandardEquipmentRecord
        PRIMARY KEY (StandardEquipmentHistoryID, CategoryID, SubCategoryID, Value, DataLoadID),
    
    CONSTRAINT FK_BlackBook_StandardEquipmentRecord__StandardEquipmentHistory
        FOREIGN KEY (StandardEquipmentHistoryID, DataLoadID)
        REFERENCES BlackBook.StandardEquipment_History (StandardEquipmentHistoryID, DataLoadID),
    
    CONSTRAINT FK_BlackBook_StandardEquipmentRecord__Category
        FOREIGN KEY (CategoryID)
        REFERENCES BlackBook.Category (CategoryID),
    
    CONSTRAINT FK_BlackBook_StandardEquipmentRecord__SubCategory
        FOREIGN KEY (SubCategoryID)
        REFERENCES BlackBook.SubCategory (SubCategoryID),
        
    CONSTRAINT FK_BlackBook_StandardEquipmentRecord__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES BlackBook.DataLoad (DataLoadID)
)
ON PS_BlackBook__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.VehicleConfiguration -> Vehicle Configuration
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.VehicleConfiguration (
    VehicleConfigurationID  INT IDENTITY(1,1)   NOT NULL,
    VehicleID               INT                 NULL,    
    
    CONSTRAINT PK_BlackBook_VehicleConfiguration
        PRIMARY KEY CLUSTERED (VehicleConfigurationID),
    
    CONSTRAINT FK_BlackBook_VehicleConfiguration__Vehicle
        FOREIGN KEY (VehicleID)
        REFERENCES Reference.Vehicle (VehicleID)
)
ON [DATA]
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.ChangeAgent -> Vehicle Configuration; Static
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.ChangeAgent (
	ChangeAgentID TINYINT     NOT NULL,
	Name          VARCHAR(32) NOT NULL,
	
	CONSTRAINT PK_BlackBook_ChangeAgent
		PRIMARY KEY CLUSTERED (ChangeAgentID),		
	
    CONSTRAINT CK_BlackBook_ChangeAgent__Name
		CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

INSERT INTO BlackBook.ChangeAgent (ChangeAgentID, Name) VALUES (1, 'User')
INSERT INTO BlackBook.ChangeAgent (ChangeAgentID, Name) VALUES (2, 'System')
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.VehicleConfiguration_History -> Vehicle Configuration; Partitioned
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.VehicleConfiguration_History (
    VehicleConfigurationHistoryID   INT IDENTITY(1,1)   NOT NULL,
    DataLoadID                      INT                 NOT NULL,
    VehicleConfigurationID          INT                 NOT NULL,
    CarHistoryID                    INT                 NOT NULL,    
    Mileage                         INT                 NULL,
    AuditRowID                      INT                 NOT NULL,
	ChangeAgentID                   TINYINT             NOT NULL,
	ChangeTypeBitFlags              INT                 NOT NULL,
	ChangeTypeBit_State             AS CONVERT(BIT, ChangeTypeBitFlags & POWER(2,0)),
	ChangeTypeBit_Uvc               AS CONVERT(BIT, ChangeTypeBitFlags & POWER(2,1)),
	ChangeTypeBit_Mileage           AS CONVERT(BIT, ChangeTypeBitFlags & POWER(2,2)),
	ChangeTypeBit_OptionActions     AS CONVERT(BIT, ChangeTypeBitFlags & POWER(2,3)),
	ChangeTypeBit_OptionStates      AS CONVERT(BIT, ChangeTypeBitFlags & POWER(2,4)),
    ChangeTypeBit_BookDate          AS CONVERT(BIT, ChangeTypeBitFlags & POWER(2,5)),
	
    CONSTRAINT PK_BlackBook_VehicleConfiguration_History
        PRIMARY KEY CLUSTERED (VehicleConfigurationHistoryID, DataLoadID),
    
    CONSTRAINT FK_BlackBook_VehicleConfiguration_History__VehicleConfiguration
        FOREIGN KEY (VehicleConfigurationID)
        REFERENCES BlackBook.VehicleConfiguration (VehicleConfigurationID),
    
    CONSTRAINT FK_BlackBook_VehicleConfiguration_History__Car_History
        FOREIGN KEY (CarHistoryID, DataLoadID)
        REFERENCES BlackBook.Car_History (CarHistoryID, DataLoadID),
	
    CONSTRAINT FK_BlackBook_VehicleConfiguration_History__ChangeAgent
		FOREIGN KEY (ChangeAgentID)
		REFERENCES BlackBook.ChangeAgent (ChangeAgentID),
    
    CONSTRAINT FK_BlackBook_VehicleConfiguration_History__AuditRow
        FOREIGN KEY (AuditRowID)
        REFERENCES Audit.AuditRow (AuditRowID),
        
    CONSTRAINT FK_BlackBook_VehicleConfiguration_History__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES BlackBook.DataLoad (DataLoadID),
    
    CONSTRAINT CK_BlackBook_VehicleConfiguration_History__Mileage
        CHECK (Mileage IS NULL OR Mileage >= 0)
)
ON PS_BlackBook__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.OptionActionType -> Vehicle Configuration; Static
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.OptionActionType (
    OptionActionTypeID  TINYINT     NOT NULL,
    Name                VARCHAR(32) NOT NULL,
    
    CONSTRAINT PK_BlackBook_OptionActionType
        PRIMARY KEY CLUSTERED (OptionActionTypeID),
    
    CONSTRAINT UK_BlackBook_OptionActionType
        UNIQUE NONCLUSTERED (Name),
    
    CONSTRAINT CK_BlackBook_OptionActionType__Name
        CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

INSERT INTO BlackBook.OptionActionType ( OptionActionTypeID, Name ) VALUES  ( 1, 'Add' )
INSERT INTO BlackBook.OptionActionType ( OptionActionTypeID, Name ) VALUES  ( 2, 'Remove' )
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.OptionAction -> Vehicle Configuration; Partitioned
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.OptionAction (
    VehicleConfigurationHistoryID   INT                 NOT NULL,    
    DataLoadID                      INT                 NOT NULL,
    OptionActionTypeID              TINYINT             NOT NULL,
    OptionActionIndex               TINYINT             NOT NULL,
    Uoc                             CHAR(4)             NOT NULL,
    
    CONSTRAINT PK_BlackBook_OptionAction
        PRIMARY KEY CLUSTERED (VehicleConfigurationHistoryID, OptionActionIndex, DataLoadID),
    
    CONSTRAINT UK_BlackBook_OptionAction__Uoc
        UNIQUE NONCLUSTERED (VehicleConfigurationHistoryID, Uoc, DataLoadID),
    
    CONSTRAINT FK_BlackBook_OptionAction__VehicleConfiguration_History
        FOREIGN KEY (VehicleConfigurationHistoryID, DataLoadID)
        REFERENCES BlackBook.VehicleConfiguration_History (VehicleConfigurationHistoryID, DataLoadID),
    
    CONSTRAINT FK_BlackBook_OptionAction__OptionActionType
        FOREIGN KEY (OptionActionTypeID)
        REFERENCES BlackBook.OptionActionType (OptionActionTypeID),
        
    CONSTRAINT FK_BlackBook_OptionAction__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES BlackBook.DataLoad (DataLoadID),
    
    CONSTRAINT CK_BlackBook_OptionAction__OptionActionType
        CHECK (OptionActionIndex >= 0),
    
    CONSTRAINT CK_BlackBook_OptionAction__Uoc
        CHECK (LEN(LTRIM(RTRIM(Uoc))) = 4)
)
ON PS_BlackBook__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.OptionState -> Vehicle Configuration; Partitioned
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.OptionState (
    VehicleConfigurationHistoryID   INT         NOT NULL,
    DataLoadID                      INT         NOT NULL,
    Uoc                             CHAR(4)     NOT NULL,
    Enabled                         BIT         NOT NULL,
    Selected                        BIT         NOT NULL,
    
    CONSTRAINT PK_BlackBook_OptionState
        PRIMARY KEY CLUSTERED (VehicleConfigurationHistoryID, Uoc, DataLoadID),
    
    CONSTRAINT FK_BlackBook_OptionState__VehicleConfiguration_History
        FOREIGN KEY (VehicleConfigurationHistoryID, DataLoadID)
        REFERENCES BlackBook.VehicleConfiguration_History (VehicleConfigurationHistoryID, DataLoadID),

    CONSTRAINT FK_BlackBook_OptionState__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES BlackBook.DataLoad (DataLoadID),
        
    CONSTRAINT CK_BlackBook_OptionState__Uoc
        CHECK (LEN(LTRIM(RTRIM(Uoc))) = 4)
)
ON PS_BlackBook__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.Market -> Vehicle Configuration; Static
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.Market (
    MarketID    TINYINT     NOT NULL,
    Name        VARCHAR(32) NOT NULL,
    
    CONSTRAINT PK_BlackBook_Market
        PRIMARY KEY CLUSTERED (MarketID),
    
    CONSTRAINT UK_BlackBook_Market
        UNIQUE NONCLUSTERED (Name),
    
    CONSTRAINT CK_BlackBook_Market_Name
        CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

INSERT INTO BlackBook.Market ( MarketID, Name ) VALUES ( 1, 'Retail' )
INSERT INTO BlackBook.Market ( MarketID, Name ) VALUES ( 2, 'Wholesale' )
INSERT INTO BlackBook.Market ( MarketID, Name ) VALUES ( 3, 'Trade In' )
INSERT INTO BlackBook.Market ( MarketID, Name ) VALUES ( 4, 'Finance Advance' )
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.Valuation -> Vehicle Configuration; Static
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.Valuation (
    ValuationID TINYINT     NOT NULL,
    Name        VARCHAR(32) NOT NULL,
    
    CONSTRAINT PK_BlackBook_Valuation
        PRIMARY KEY CLUSTERED (ValuationID),
    
    CONSTRAINT UK_BlackBook_Valuation
        UNIQUE NONCLUSTERED (Name),
    
    CONSTRAINT CK_BlackBook_Valuation_Name
        CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

INSERT INTO BlackBook.Valuation ( ValuationID, Name ) VALUES ( 1, 'Base' )
INSERT INTO BlackBook.Valuation ( ValuationID, Name ) VALUES ( 2, 'Option' )
INSERT INTO BlackBook.Valuation ( ValuationID, Name ) VALUES ( 3, 'Mileage' )
INSERT INTO BlackBook.Valuation ( ValuationID, Name ) VALUES ( 4, 'Final' )
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.Condition -> Vehicle Configuration; Static
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.Condition (
    ConditionID TINYINT     NOT NULL,
    Name        VARCHAR(32) NOT NULL,
    
    CONSTRAINT PK_BlackBook_Condition
        PRIMARY KEY CLUSTERED (ConditionID),
    
    CONSTRAINT UK_BlackBook_Condition
        UNIQUE NONCLUSTERED (Name),
    
    CONSTRAINT CK_BlackBook_Condition_Name
        CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

INSERT INTO BlackBook.Condition ( ConditionID, Name ) VALUES  ( 1,  'Extra Clean' )
INSERT INTO BlackBook.Condition ( ConditionID, Name ) VALUES  ( 2,  'Clean' )
INSERT INTO BlackBook.Condition ( ConditionID, Name ) VALUES  ( 3,  'Average' )
INSERT INTO BlackBook.Condition ( ConditionID, Name ) VALUES  ( 4,  'Rough' )
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.Matrix -> Vehicle Configuration; Partitioned
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.Matrix (
    MatrixID                        INT IDENTITY(1,1)   NOT NULL,
    VehicleConfigurationHistoryID   INT                 NOT NULL,    
    CarHistoryID                    INT                 NOT NULL,
    DataLoadID                      INT                 NOT NULL,
    
    CONSTRAINT PK_BlackBook_Matrix
        PRIMARY KEY NONCLUSTERED (MatrixID, DataLoadID),
    
    CONSTRAINT UK_BlackBook_Matrix
        UNIQUE CLUSTERED (VehicleConfigurationHistoryID, CarHistoryID, DataLoadID),
    
    CONSTRAINT FK_BlackBook_Matrix__VehicleConfiguration_History
        FOREIGN KEY (VehicleConfigurationHistoryID, DataLoadID)
        REFERENCES BlackBook.VehicleConfiguration_History (VehicleConfigurationHistoryID, DataLoadID),
    
    CONSTRAINT FK_BlackBook_Matrix__Car_History
        FOREIGN KEY (CarHistoryID, DataLoadID)
        REFERENCES BlackBook.Car_History (CarHistoryID, DataLoadID),
        
    CONSTRAINT FK_BlackBook_Matrix__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES BlackBook.DataLoad (DataLoadID)
)
ON PS_BlackBook__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--	BlackBook.MatrixCell -> Vehicle Configuration; Partitioned
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE BlackBook.MatrixCell (
    MatrixID    INT         NOT NULL,
    DataLoadID  INT         NOT NULL,
    MarketID    TINYINT     NOT NULL,
    ConditionID TINYINT     NOT NULL,
    ValuationID TINYINT     NOT NULL,
    Visible     BIT         NOT NULL,
    Value       INT         NULL,
    
    CONSTRAINT PK_BlackBook_MatrixCell
        PRIMARY KEY CLUSTERED (MatrixID, MarketID, ConditionID, ValuationID, DataLoadID),
    
    CONSTRAINT FK_BlackBook_MatrixCell__Matrix
        FOREIGN KEY (MatrixID, DataLoadID)
        REFERENCES BlackBook.Matrix (MatrixID, DataLoadID),
    
    CONSTRAINT FK_BlackBook_MatrixCell__Market
        FOREIGN KEY (MarketID)
        REFERENCES BlackBook.Market (MarketID),
    
    CONSTRAINT FK_BlackBook_MatrixCell__Condition
        FOREIGN KEY (ConditionID)
        REFERENCES BlackBook.Condition (ConditionID),
    
    CONSTRAINT FK_BlackBook_MatrixCell__Valuation
        FOREIGN KEY (ValuationID)
        REFERENCES BlackBook.Valuation (ValuationID),
        
    CONSTRAINT FK_BlackBook_MatrixCell__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES BlackBook.DataLoad (DataLoadID)
)
ON PS_BlackBook__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--	Client.Vehicle_BlackBook -> Vehicle Configuration
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Client.Vehicle_BlackBook (
    VehicleConfigurationID  INT NOT NULL,
    VehicleID               INT NOT NULL,
    
    CONSTRAINT PK_Client_Vehicle_BlackBook
        PRIMARY KEY CLUSTERED (VehicleConfigurationID, VehicleID),
    
    CONSTRAINT FK_Client_Vehicle_BlackBook__VehicleConfiguration
        FOREIGN KEY (VehicleConfigurationID)
        REFERENCES BlackBook.VehicleConfiguration (VehicleConfigurationID),
    
    CONSTRAINT FK_Client_Vehicle_BlackBook__Vehicle
        FOREIGN KEY (VehicleID)
        REFERENCES Client.Vehicle (VehicleID)
)
ON [DATA]
GO
