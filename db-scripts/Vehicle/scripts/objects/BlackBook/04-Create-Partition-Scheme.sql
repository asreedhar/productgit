
------------------------------------------------------------------------------------------------
-- Magic Database Settings
------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

------------------------------------------------------------------------------------------------
-- Partition Function
------------------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.partition_functions WHERE name = 'PF_BlackBook__DataLoad')

	CREATE PARTITION FUNCTION PF_BlackBook__DataLoad(int)
	    AS RANGE LEFT FOR VALUES (
	        2011060100, -- 2011Q2
	        2011090100) -- 2011Q3

GO

------------------------------------------------------------------------------------------------
-- Partition Scheme
------------------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.partition_schemes WHERE name = 'PS_BlackBook__DataLoad')
	
 	CREATE PARTITION SCHEME PS_BlackBook__DataLoad
		AS PARTITION PF_BlackBook__DataLoad TO (
            [DATA_BlackBook_FG1],
            [DATA_BlackBook_FG2],
            [PRIMARY])

GO
