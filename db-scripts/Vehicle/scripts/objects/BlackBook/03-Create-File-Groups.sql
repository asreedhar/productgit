
------------------------------------------------------------------------------------------------
-- Magic Database Settings
------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

------------------------------------------------------------------------------------------------
-- Variables
------------------------------------------------------------------------------------------------

DECLARE @l_names TABLE (
        idx int identity(1,1) not null,
        l_name VARCHAR(50),
		initial_size VARCHAR(50))

DECLARE @idx INT, @initial_size VARCHAR(50)

DECLARE @data_path nvarchar(256), @stmt nvarchar(2048)

DECLARE @g_name NVARCHAR(128), @l_name NVARCHAR(128), @p_name NVARCHAR(128), @x NVARCHAR(16)

DECLARE @database_name NVARCHAR(128), @physical_name VARCHAR(100)

------------------------------------------------------------------------------------------------
-- Get physical path to data directory
------------------------------------------------------------------------------------------------

SELECT  @database_name = DB_NAME()

SELECT 	@physical_name = LOWER(physical_name)
FROM 	master.sys.master_files
WHERE	database_id = DB_ID(@database_name) AND file_id = 1 	

SET @data_path =  LEFT(@physical_name, LEN(@physical_name) - CHARINDEX('\', REVERSE(@physical_name)) + 1)

------------------------------------------------------------------------------------------------
-- Declare new file groups
------------------------------------------------------------------------------------------------

INSERT INTO @l_names VALUES ('2011Q2', '51200KB')
INSERT INTO @l_names VALUES ('2011Q3', '51200KB')

SELECT @idx = MIN(idx) FROM @l_names

SELECT @x = 'BlackBook'

WHILE (@idx IS NOT NULL) BEGIN
        
        SELECT  @l_name       = @x + '_' + l_name,
                @initial_size = initial_size
        FROM    @l_names
        WHERE   idx = @idx
        
        SELECT  @g_name = 'DATA_' + @x + '_FG' + CONVERT(VARCHAR, @idx),
                @p_name = @data_path + 'Vehicle_' + @l_name + '.ndf'
        
		if not exists (select * from sys.filegroups where name=@g_name)
		BEGIN
		
        	SELECT @stmt = 'ALTER DATABASE ' + @database_name + ' ADD FILEGROUP [' + @g_name + ']'
        
        	EXECUTE(@stmt)

        	SELECT @stmt = '
        	ALTER DATABASE ' + @database_name + ' ADD FILE (
        	        NAME = ''' + @l_name + ''',
        	        FILENAME = ''' + @p_name + ''',
        	        SIZE = ' + @initial_size + ',
        	        FILEGROWTH = 100MB
        	)
        	TO FILEGROUP [' + @g_name + ']'
        
        	EXECUTE(@stmt)

		END

        SELECT @idx = MIN(idx) FROM @l_names WHERE idx > @idx

END
