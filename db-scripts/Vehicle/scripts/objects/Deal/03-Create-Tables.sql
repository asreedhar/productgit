CREATE TABLE Deal.PriceType (
        PriceTypeID		TINYINT IDENTITY(1,1) NOT NULL,
		[Name]			VARCHAR(50), 
        CONSTRAINT PK_Deal_PriceType 
               PRIMARY KEY CLUSTERED (PriceTypeID),
		CONSTRAINT UK_Deal_PriceType
				UNIQUE NONCLUSTERED ([Name])
)
ON [DATA]
GO

CREATE TABLE Deal.Price (
        PriceID         INT IDENTITY(1,1) NOT NULL,
        PriceTypeID     TINYINT NOT NULL,
        EmployeeID		INT NOT NULL,
        [Value]			MONEY NOT NULL, 
		AuditRowID		INT NOT NULL,
        CONSTRAINT PK_Deal_Price
                PRIMARY KEY CLUSTERED (PriceID),
        CONSTRAINT FK_Deal_Price__PriceType
                FOREIGN KEY (PriceTypeID)
                REFERENCES Deal.[PriceType] (PriceTypeID),
        CONSTRAINT FK_Deal_Price__Employee
                FOREIGN KEY (EmployeeID)
                REFERENCES Employee.[Employee] (EmployeeID),
        CONSTRAINT FK_Deal_Price__Audit
                FOREIGN KEY (AuditRowID)
                REFERENCES Audit.[AuditRow] (AuditRowID)
)
ON [DATA]
GO

CREATE TABLE Deal.Deal (
		DealID			INT IDENTITY(1,1) NOT NULL,
		ClientID		INT NOT NULL,
		AuditRowID		INT NOT NULL,
		CONSTRAINT PK_Deal_Deal
			PRIMARY KEY CLUSTERED (DealID),
        CONSTRAINT FK_Deal_Deal__ClientClient
                FOREIGN KEY (ClientID)
                REFERENCES Client.[Client] (ClientID),
        CONSTRAINT FK_Deal_Deal__Audit
                FOREIGN KEY (AuditRowID)
                REFERENCES Audit.[AuditRow] (AuditRowID)
)
ON [DATA]
GO


CREATE TABLE Deal.Vehicle (
        VehicleID         INT IDENTITY(1,1) NOT NULL,
        ClientVehicleID   INT NOT NULL,
        DealID			  INT NOT NULL,
		AuditRowID		  INT NOT NULL,
        CONSTRAINT PK_Deal_Vehicle
                PRIMARY KEY CLUSTERED (VehicleID),
        CONSTRAINT FK_Deal_Vehicle__Client_Vehicle
                FOREIGN KEY (ClientVehicleID)
                REFERENCES Client.[Vehicle] (VehicleID),
        CONSTRAINT FK_Deal_Vehicle__Deal
                FOREIGN KEY (DealID)
                REFERENCES Deal.[Deal] (DealID),
        CONSTRAINT FK_Deal_Vehicle__Audit
                FOREIGN KEY (AuditRowID)
                REFERENCES Audit.[AuditRow] (AuditRowID),
		CONSTRAINT UK_Deal_Vehicle
				UNIQUE NONCLUSTERED (ClientVehicleID, DealID)
)
ON [DATA]
GO

CREATE TABLE Deal.Vehicle_Price (
        VehicleID       INT NOT NULL,
        PriceID			INT NOT NULL,
        CONSTRAINT PK_Deal_Vehicle_Price
                PRIMARY KEY (VehicleID,PriceID),
        CONSTRAINT FK_Deal_Vehicle_Price__Vehicle
                FOREIGN KEY (VehicleID)
                REFERENCES Deal.Vehicle (VehicleID),
        CONSTRAINT FK_Deal_Vehicle_Price__Price
                FOREIGN KEY (PriceID)
                REFERENCES Deal.Price (PriceID),
		CONSTRAINT UK_Deal_Vehicle_Price
				UNIQUE NONCLUSTERED (PriceID)
)
ON [DATA]
GO

CREATE TABLE Deal.InventoryStrategy (
		InventoryStrategyID	INT IDENTITY(1,1) NOT NULL,
		[Name]				VARCHAR(50),
		IsRetail			BIT,
		IsWholesale			BIT,
		CONSTRAINT PK_Deal_InventoryStrategy
                PRIMARY KEY (InventoryStrategyID),
		CONSTRAINT UK_Deal_InventoryStrategy
				UNIQUE NONCLUSTERED ([Name])	 
)
ON [DATA]
GO

CREATE TABLE Deal.Vehicle_InventoryStrategy  (
		VehicleID			INT	NOT NULL,
		InventoryStrategyID	INT NOT NULL,
		AssociationID		INT NOT NULL,
		CONSTRAINT PK_Deal_Vehicle_InventoryStrategy
			PRIMARY KEY(VehicleID, InventoryStrategyID),
		CONSTRAINT FK_Deal_Vehicle_InventoryStrategy__Vehicle
                FOREIGN KEY (VehicleID)
                REFERENCES Deal.Vehicle (VehicleID),
		CONSTRAINT FK_Deal_Vehicle_InventoryStrategy__InventoryStrategy
				FOREIGN KEY (InventoryStrategyID)
				REFERENCES Deal.InventoryStrategy (InventoryStrategyID),
		CONSTRAINT FK_Deal_Vehicle_InventoryStrategy__Association
				FOREIGN KEY (AssociationID)
				REFERENCES Audit.Association (AssociationID)
)
ON [DATA]
GO

CREATE TABLE Deal.TransactionType (
		TransactionTypeID	TINYINT IDENTITY(1,1) NOT NULL,
		[Name]				VARCHAR(50),
		CONSTRAINT PK_Deal_TransactionType
                PRIMARY KEY (TransactionTypeID),
		CONSTRAINT UK_Deal_TransactionType
				UNIQUE NONCLUSTERED ([Name])	 
)
ON [DATA]
GO

CREATE TABLE Deal.Vehicle_TransactionType  (
		VehicleID			INT	NOT NULL,
		TransactionTypeID	TINYINT NOT NULL,
		AssociationID		INT NOT NULL,
		CONSTRAINT PK_Deal_Vehicle_TransactionType
			PRIMARY KEY(VehicleID, TransactionTypeID),
		CONSTRAINT FK_Deal_Vehicle_TransactionType__Vehicle
                FOREIGN KEY (VehicleID)
                REFERENCES Deal.Vehicle (VehicleID),
		CONSTRAINT FK_Deal_Vehicle_TransactionType__TransactionType
				FOREIGN KEY (TransactionTypeID)
				REFERENCES Deal.TransactionType (TransactionTypeID),
		CONSTRAINT FK_Deal_Vehicle_TransactionType__Association
				FOREIGN KEY (AssociationID)
				REFERENCES Audit.Association (AssociationID)
)
ON [DATA]
GO

CREATE TABLE Deal.NoteType (
        NoteTypeID		TINYINT IDENTITY(1,1) NOT NULL,
		[Name]			VARCHAR(50), 
        CONSTRAINT PK_Deal_NoteType 
               PRIMARY KEY CLUSTERED (NoteTypeID),
		CONSTRAINT UK_Deal_NoteType
				UNIQUE NONCLUSTERED ([Name])	
)
ON [DATA]
GO

CREATE TABLE Deal.Note (
		NoteID			INT IDENTITY(1,1) NOT NULL,
        NoteTypeID		TINYINT NOT NULL,
		[Text]			VARCHAR(1000), 
        CONSTRAINT PK_Deal_Note 
               PRIMARY KEY CLUSTERED (NoteID),
		CONSTRAINT FK_Deal_Note__NoteType
				FOREIGN KEY (NoteTypeID)
				REFERENCES Deal.NoteType (NoteTypeID)
)
ON [DATA]
GO

CREATE TABLE Deal.Vehicle_Note  (
		VehicleID			INT	NOT NULL,
		NoteID				INT NOT NULL,
		CONSTRAINT PK_Deal_Vehicle_Note
			PRIMARY KEY(VehicleID, NoteID),
		CONSTRAINT FK_Deal_Vehicle_Note__Vehicle
                FOREIGN KEY (VehicleID)
                REFERENCES Deal.Vehicle (VehicleID),
		CONSTRAINT FK_Deal_Vehicle_Note__Note
				FOREIGN KEY (NoteID)
				REFERENCES Deal.Note (NoteID),
		CONSTRAINT UK_Deal_Vehicle_Note
				UNIQUE NONCLUSTERED ([NoteID])	
)
ON [DATA]
GO

CREATE TABLE Deal.Motivation (
		MotivationID		INT IDENTITY(1,1) NOT NULL,
		[Name]				VARCHAR(50),
		IsRetail			BIT,
		IsWholesale			BIT,
		CONSTRAINT PK_Deal_Motivation
			PRIMARY KEY(MotivationID),
		CONSTRAINT UK_Deal_Motivation
				UNIQUE NONCLUSTERED ([Name])	
)
ON [DATA]
GO

CREATE TABLE Deal.Deal_Motivation  (
		DealID				INT	NOT NULL,
		MotivationID		INT NOT NULL,
		AssociationID		INT NOT NULL,
		CONSTRAINT PK_Deal_Deal_Motivation
			PRIMARY KEY(DealID, MotivationID),
		CONSTRAINT FK_Deal_Deal_Motivation__Deal
                FOREIGN KEY (DealID)
                REFERENCES Deal.Deal (DealID),
		CONSTRAINT FK_Deal_Deal_Motivation__Motivation
				FOREIGN KEY (MotivationID)
				REFERENCES Deal.Motivation (MotivationID),
		CONSTRAINT FK_Deal_Deal_Motivation__Association
				FOREIGN KEY (AssociationID)
				REFERENCES Audit.Association (AssociationID)
)
ON [DATA]
GO

CREATE TABLE Deal.Status (
		StatusID			TINYINT IDENTITY(1,1) NOT NULL,
		[Name]				VARCHAR(50),
		IsOpen				BIT,
		IsClosed			BIT,
		CONSTRAINT PK_Deal_Status
			PRIMARY KEY(StatusID),
		CONSTRAINT UK_Deal_Status
				UNIQUE NONCLUSTERED ([Name])	
)
ON [DATA]
GO

CREATE TABLE Deal.Deal_Status  (
		DealID				INT	NOT NULL,
		StatusID			TINYINT NOT NULL,
		AssociationID		INT NOT NULL,
		CONSTRAINT PK_Deal_Deal_Status
			PRIMARY KEY(DealID, StatusID),
		CONSTRAINT FK_Deal_Deal_Status__Deal
                FOREIGN KEY (DealID)
                REFERENCES Deal.Deal (DealID),
		CONSTRAINT FK_Deal_Deal_Status__Status
				FOREIGN KEY (StatusID)
				REFERENCES Deal.Status (StatusID),
		CONSTRAINT FK_Deal_Deal_Status__Association
				FOREIGN KEY (AssociationID)
				REFERENCES Audit.Association (AssociationID)
)
ON [DATA]
GO

CREATE TABLE Deal.Deal_Party  (
		DealID				INT	NOT NULL,
		PartyID				INT NOT NULL,
		AssociationID		INT NOT NULL,
		CONSTRAINT PK_Deal_Deal_Party
			PRIMARY KEY(DealID, PartyID),
		CONSTRAINT FK_Deal_Deal_Party__Deal
                FOREIGN KEY (DealID)
                REFERENCES Deal.Deal (DealID),
		CONSTRAINT FK_Deal_Deal_Party__Party
				FOREIGN KEY (PartyID)
				REFERENCES Party.Party (PartyID),
		CONSTRAINT FK_Deal_Deal_Party__Association
				FOREIGN KEY (AssociationID)
				REFERENCES Audit.Association (AssociationID)
)
ON [DATA]
GO

CREATE TABLE Deal.Properties  (
		DealID				INT	NOT NULL,
		Created				DATETIME,
		Modified			DATETIME,
		IsReadOnly			BIT,
		RevisionNumber		SMALLINT,
		CONSTRAINT PK_Deal_Properties
			PRIMARY KEY(DealID),
		CONSTRAINT FK_Deal_Properties__Deal
                FOREIGN KEY (DealID)
                REFERENCES Deal.Deal (DealID)
)
ON [DATA]
GO

CREATE TABLE Deal.Salesperson  (
		DealID				INT	NOT NULL,
		EmployeeID			INT NOT NULL,
		AssociationID		INT NOT NULL,
		CONSTRAINT PK_Deal_Salesperson
			PRIMARY KEY(DealID, EmployeeID),
		CONSTRAINT FK_Deal_Salesperson__Deal
                FOREIGN KEY (DealID)
                REFERENCES Deal.Deal (DealID),
		CONSTRAINT FK_Deal_Salesperson__Employee
				FOREIGN KEY (EmployeeID)
				REFERENCES Employee.Employee (EmployeeID),
		CONSTRAINT FK_Deal_Salesperson__Association
				FOREIGN KEY (AssociationID)
				REFERENCES Audit.Association (AssociationID)
)
ON [DATA]
GO
