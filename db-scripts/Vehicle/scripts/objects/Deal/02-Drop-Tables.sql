IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Deal].[Vehicle_Price]') AND type in (N'U'))
DROP TABLE [Deal].[Vehicle_Price]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Deal].[Price]') AND type in (N'U'))
DROP TABLE [Deal].[Price]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Deal].[PriceType]') AND type in (N'U'))
DROP TABLE [Deal].[PriceType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Deal].[Vehicle_InventoryStrategy]') AND type in (N'U'))
DROP TABLE [Deal].[Vehicle_InventoryStrategy]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Deal].[InventoryStrategy]') AND type in (N'U'))
DROP TABLE [Deal].[InventoryStrategy]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Deal].[Vehicle_TransactionType]') AND type in (N'U'))
DROP TABLE [Deal].[Vehicle_TransactionType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Deal].[TransactionType]') AND type in (N'U'))
DROP TABLE [Deal].[TransactionType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Deal].[Vehicle_Note]') AND type in (N'U'))
DROP TABLE [Deal].[Vehicle_Note]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Deal].[Note]') AND type in (N'U'))
DROP TABLE [Deal].[Note]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Deal].[NoteType]') AND type in (N'U'))
DROP TABLE [Deal].[NoteType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Deal].[Deal_Motivation]') AND type in (N'U'))
DROP TABLE [Deal].[Deal_Motivation]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Deal].[Motivation]') AND type in (N'U'))
DROP TABLE [Deal].[Motivation]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Deal].[Deal_Status]') AND type in (N'U'))
DROP TABLE [Deal].[Deal_Status]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Deal].[Status]') AND type in (N'U'))
DROP TABLE [Deal].[Status]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Deal].[Deal_Party]') AND type in (N'U'))
DROP TABLE [Deal].[Deal_Party]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Deal].[Properties]') AND type in (N'U'))
DROP TABLE [Deal].[Properties]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Deal].[Salesperson]') AND type in (N'U'))
DROP TABLE [Deal].[Salesperson]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Deal].[Vehicle]') AND type in (N'U'))
DROP TABLE [Deal].[Vehicle]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Deal].[Deal]') AND type in (N'U'))
DROP TABLE [Deal].[Deal]
GO
