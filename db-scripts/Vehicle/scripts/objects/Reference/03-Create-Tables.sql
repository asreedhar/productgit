
CREATE TABLE Reference.Seller (
        SellerID        INT NOT NULL,
        CONSTRAINT PK_Reference_Seller
                PRIMARY KEY CLUSTERED (
                        SellerID
                )
                ON [IDX]
)
ON [DATA]
GO

CREATE TABLE Reference.Dealer (
        DealerID        INT NOT NULL,
        CONSTRAINT PK_Reference_Dealer
                PRIMARY KEY CLUSTERED (
                        DealerID
                )
                ON [IDX]
)
ON [DATA]
GO

CREATE TABLE Reference.Member (
        MemberID        INT NOT NULL,
        CONSTRAINT PK_Reference_Member
                PRIMARY KEY CLUSTERED (
                        MemberID
                )
                ON [IDX]
)
ON [DATA]
GO

CREATE TABLE Reference.Vehicle (
        VehicleID       INT IDENTITY(1,1) NOT NULL,
        VIN             CHAR(17) NOT NULL,
        CONSTRAINT PK_Reference_Vehicle__VehicleID
                PRIMARY KEY CLUSTERED (
                        VehicleID
                )
                ON [IDX],
        CONSTRAINT UK_Reference_Vehicle__VIN
                UNIQUE NONCLUSTERED (
                        VIN
                )
                ON [IDX],
        CONSTRAINT CK_Reference_Vehicle__VIN
                CHECK (
                        LEN(LTRIM(RTRIM(VIN))) = 17
                )
)
ON [DATA]
GO
