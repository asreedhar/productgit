
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reference].[Seller]') AND type in (N'U'))
DROP TABLE [Reference].[Seller]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reference].[Dealer]') AND type in (N'U'))
DROP TABLE [Reference].[Dealer]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reference].[Member]') AND type in (N'U'))
DROP TABLE [Reference].[Member]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Reference].[Vehicle]') AND type in (N'U'))
DROP TABLE [Reference].[Vehicle]
GO
