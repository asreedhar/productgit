
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- our data-set key

CREATE TABLE Galves.dataload (
    data_load_id    INT         NOT NULL,
    data_load_time  DATETIME    NOT NULL,
    CONSTRAINT pk_dbo_dataload
        PRIMARY KEY CLUSTERED (data_load_id)
)
ON PS_Galves__DataLoad (data_load_id)
GO

-- the Galves data

CREATE TABLE [Galves].curryear (
    data_load_id   INT         NOT NULL,
	curr_year   SMALLINT    NOT NULL,
	y1          SMALLINT    NULL,
	y2          SMALLINT    NULL,
	y3          SMALLINT    NULL,
	y4          SMALLINT    NULL,
	y5          SMALLINT    NULL,
	y6          SMALLINT    NULL,
	y7          SMALLINT    NULL,
	y8          SMALLINT    NULL,
	y9          SMALLINT    NULL,
	y10         SMALLINT    NULL,
	y11         SMALLINT    NULL,
	y12         SMALLINT    NULL,
	y13         SMALLINT    NULL,
	y14         SMALLINT    NULL,
	y15         SMALLINT    NULL,
	y16         SMALLINT    NULL,
	y17         SMALLINT    NULL,
	y18         SMALLINT    NULL,
	y19         SMALLINT    NULL,
	y20         SMALLINT    NULL,
	CONSTRAINT PK_Galves_curryear
	    PRIMARY KEY (data_load_id),
    CONSTRAINT FK_Galves_curryear__dataload
	    FOREIGN KEY (data_load_id)
	    REFERENCES Galves.dataload (data_load_id)
)
ON PS_Galves__DataLoad (data_load_id)
GO

CREATE TABLE [Galves].exceptions (
    data_load_id   INT         NOT NULL,
	year        SMALLINT    NOT NULL,
	manuf       VARCHAR(25) NOT NULL,
	model       VARCHAR(25) NULL,
	bodystyle   VARCHAR(25) NULL,
	engine      VARCHAR(25) NULL,
	exception   VARCHAR(50) NOT NULL,
	corrfeatur  VARCHAR(50) NOT NULL,
	mf_type     CHAR(1)     NOT NULL,
	CONSTRAINT UK_Galves_exceptions
	    UNIQUE CLUSTERED (data_load_id, year, manuf, model, bodystyle, engine, exception, corrfeatur, mf_type), -- source has duplicates
    CONSTRAINT FK_Galves_exceptions__dataload
	    FOREIGN KEY (data_load_id)
	    REFERENCES Galves.dataload (data_load_id)
)
ON PS_Galves__DataLoad (data_load_id)
GO

CREATE TABLE [Galves].region_milefactors (
    data_load_id   INT         NOT NULL,
	ph_mclass   CHAR(1)     NOT NULL,
	year        VARCHAR(3)  NOT NULL,
	adj_Region2 INT         NOT NULL,
	adj_Region3 INT         NOT NULL,
	adj_Region4 INT         NOT NULL,
	adj_Region5 INT         NOT NULL,
	adj_Region6 INT         NOT NULL,
	CONSTRAINT PK_Galves_region_milefactors
	    PRIMARY KEY CLUSTERED (data_load_id, ph_mclass, year),
    CONSTRAINT FK_Galves_region_milefactors__dataload
	    FOREIGN KEY (data_load_id)
	    REFERENCES Galves.dataload (data_load_id)
)
ON PS_Galves__DataLoad (data_load_id)
GO

CREATE TABLE [Galves].region_states (
    data_load_id   INT         NOT NULL,
	state       VARCHAR(50) NOT NULL,
	region      TINYINT     NOT NULL,
	CONSTRAINT PK_Galves_region_states
	    PRIMARY KEY CLUSTERED (data_load_id, state),
    CONSTRAINT FK_Galves_region_states__dataload
	    FOREIGN KEY (data_load_id)
	    REFERENCES Galves.dataload (data_load_id)
)
ON PS_Galves__DataLoad (data_load_id)
GO

CREATE TABLE [Galves].vehicle_mileage (
    data_load_id   INT         NOT NULL,
    class       VARCHAR(3)  NOT NULL,
    year        VARCHAR(3)  NOT NULL,
    miles       FLOAT       NOT NULL,
    abvfactor   FLOAT       NOT NULL,
    blwfactor   FLOAT       NOT NULL,
    CONSTRAINT PK_Galves_vehicle_mileage
	    PRIMARY KEY CLUSTERED (data_load_id, class, year, miles),
    CONSTRAINT FK_Galves_vehicle_mileage__dataload
	    FOREIGN KEY (data_load_id)
	    REFERENCES Galves.dataload (data_load_id)
)
ON PS_Galves__DataLoad (data_load_id)
GO

CREATE TABLE [Galves].vehicles(
    data_load_id        INT         NOT NULL,
	ph_book             CHAR(1)     NOT NULL,
	ph_change           bit         NOT NULL,
	ph_group            CHAR(1)     NULL,
	ph_date             SMALLDATETIME NOT NULL,
	ph_page             INT         NOT NULL,
	ph_seq              INT         NOT NULL,
	ph_keyseq           INT         NOT NULL,
	ph_ccode            CHAR(3)     NOT NULL,
	ph_manuf            VARCHAR(25) NOT NULL,
	ph_year             SMALLINT    NOT NULL,
	ph_engtype          VARCHAR(20) NOT NULL,
	ph_model            VARCHAR(25) NOT NULL,
	ph_body             VARCHAR(25) NOT NULL,
	ph_mclass           CHAR(1)     NOT NULL,
	ph_mcamt            INT         NOT NULL,
	ph_mbase            INT         NOT NULL,
	ph_lprice           INT         NOT NULL,
	ph_madj             INT         NOT NULL,
	ph_cadj             INT         NOT NULL,
	ph_dadj             INT         NOT NULL,
	ph_cprice           INT         NOT NULL,
	ph_vincode          VARCHAR(50) NULL,
	ph_comm1            VARCHAR(50) NULL,
	ph_comm2            VARCHAR(50) NULL,
	ph_comm3            VARCHAR(50) NULL,
	ph_comm4            VARCHAR(50) NULL,
	ph_guid             INT         NOT NULL,
	ph_price2           INT         NULL,
	ph_cprice_region2   INT         NULL,
	ph_cprice_region3   INT         NULL,
	ph_cprice_region4   INT         NULL,
	ph_cprice_region5   INT         NULL,
	ph_cprice_region6   INT         NULL,
	ph_price2_region2   INT         NULL,
	ph_price2_region3   INT         NULL,
	ph_price2_region4   INT         NULL,
	ph_price2_region5   INT         NULL,
	ph_price2_region6   INT         NULL,
	ph_aft              VARCHAR(50) NULL,
	CONSTRAINT PK_Galves_vehicles
	    PRIMARY KEY CLUSTERED (data_load_id, ph_guid),
    CONSTRAINT FK_Galves_vehicles__dataload
	    FOREIGN KEY (data_load_id)
	    REFERENCES Galves.dataload (data_load_id)
)
ON PS_Galves__DataLoad (data_load_id)
GO

CREATE NONCLUSTERED INDEX IX_Galves_vehicles__ph_year
    ON Galves.vehicles (
        data_load_id, ph_year
    )
ON PS_Galves__DataLoad (data_load_id)
GO

CREATE NONCLUSTERED INDEX IX_Galves_vehicles__ph_year_ph_manuf_ph_aft
    ON Galves.vehicles (
        data_load_id, ph_year, ph_manuf, ph_aft
    )
ON PS_Galves__DataLoad (data_load_id)
GO

CREATE NONCLUSTERED INDEX IX_Galves_vehicles__ph_year_ph_manuf_ph_aft_ph_model
    ON Galves.vehicles (
        data_load_id, ph_year, ph_manuf, ph_aft, ph_model
    )
ON PS_Galves__DataLoad (data_load_id)
GO

CREATE NONCLUSTERED INDEX IX_Galves_vehicles__ph_year_ph_manuf_ph_aft_ph_model_ph_body_ph_engtype
    ON Galves.vehicles (
        data_load_id, ph_year, ph_manuf, ph_aft, ph_model, ph_body, ph_engtype
    )
ON PS_Galves__DataLoad (data_load_id)
GO

CREATE TABLE [Galves].vindecoder (
    data_load_id   INT         NOT NULL,
	ph_guid     INT         NOT NULL,
	VIN         VARCHAR(17) NOT NULL,
	CONSTRAINT PK_Galves_vindecoder
	    PRIMARY KEY CLUSTERED (data_load_id, ph_guid, VIN), -- source has duplicates
    CONSTRAINT FK_Galves_vindecoder__dataload
	    FOREIGN KEY (data_load_id)
	    REFERENCES Galves.dataload (data_load_id)
)
ON PS_Galves__DataLoad (data_load_id)
GO

CREATE TABLE [Galves].addncost (
    data_load_id   INT         NOT NULL,
	year        SMALLINT    NOT NULL,
	manuf       VARCHAR(25) NOT NULL,
	description VARCHAR(50) NOT NULL,
	amt         INT         NOT NULL,
	type        CHAR(3)     NOT NULL,
	model       VARCHAR(25) NULL,
	bodytype    VARCHAR(25) NULL,
	engine      VARCHAR(25) NULL,
	amt_code    CHAR(3)     NOT NULL,
	[except]    CHAR(1)     NOT NULL,
	mf_type     CHAR(1)     NOT NULL,
	amt_region2 INT         NULL,
	amt_region3 INT         NULL,
	amt_region4 INT         NULL,
	amt_region5 INT         NULL,
	amt_region6 INT         NULL,
	CONSTRAINT PK_Galves_addncost
	    UNIQUE CLUSTERED (data_load_id, type, year, manuf, model, bodytype, engine, description), -- source has duplicates
    CONSTRAINT FK_Galves_addncost__dataload
	    FOREIGN KEY (data_load_id)
	    REFERENCES Galves.dataload (data_load_id)
)
ON PS_Galves__DataLoad (data_load_id)
GO

CREATE TABLE [Galves].bgmanuf (
    data_load_id   INT         NOT NULL,
	year        SMALLINT    NOT NULL,
	mf_name     VARCHAR(25) NOT NULL,
	mf_model    VARCHAR(25) NOT NULL,
	mf_type     CHAR(1)     NOT NULL,
	webmanuf    VARCHAR(35) NOT NULL,
	webmodel    VARCHAR(35) NOT NULL,
	webbasemod  VARCHAR(25) NULL,
	CONSTRAINT PK_Galves_bgmanuf
	    PRIMARY KEY CLUSTERED (data_load_id, year, mf_name, mf_model, mf_type), -- source has (bad) duplicates
    CONSTRAINT FK_Galves_bgmanuf__dataload
	    FOREIGN KEY (data_load_id)
	    REFERENCES Galves.dataload (data_load_id)
)
ON PS_Galves__DataLoad (data_load_id)
GO

CREATE TABLE [Galves].bookdate (
    data_load_id   INT         NOT NULL,
	book_date   VARCHAR(50) NOT NULL,
	CONSTRAINT PK_Galves_bookdate
	    PRIMARY KEY CLUSTERED (data_load_id, book_date),
    CONSTRAINT FK_Galves_bookdate__dataload
	    FOREIGN KEY (data_load_id)
	    REFERENCES Galves.dataload (data_load_id)
)
ON PS_Galves__DataLoad (data_load_id)
GO

CREATE TABLE [Galves].cltrans (
    data_load_id   INT     NOT NULL,
	cl_code     CHAR(2) NOT NULL,
	s_code      CHAR(3) NOT NULL,
	CONSTRAINT PK_Galves_cltrans
	    PRIMARY KEY CLUSTERED (data_load_id, cl_code, s_code),
    CONSTRAINT FK_Galves_cltrans__dataload
	    FOREIGN KEY (data_load_id)
	    REFERENCES Galves.dataload (data_load_id)
)
ON PS_Galves__DataLoad (data_load_id)
GO

-- vehicle configuration

CREATE TABLE Galves.VehicleConfiguration (
    VehicleConfigurationID  INT IDENTITY(1,1)   NOT NULL,
    VehicleID               INT                 NOT NULL,
    CONSTRAINT PK_Galves_VehicleConfiguration
        PRIMARY KEY CLUSTERED (VehicleConfigurationID),
    CONSTRAINT FK_Galves_VehicleConfiguration__Vehicle
        FOREIGN KEY (VehicleID)
        REFERENCES Reference.Vehicle (VehicleID)
)
ON [DATA]
GO

CREATE TABLE Galves.VehicleConfiguration_History (
    VehicleConfigurationHistoryID   INT IDENTITY(1,1)   NOT NULL,
    VehicleConfigurationID          INT                 NOT NULL,
    DataLoadID                      INT                 NOT NULL,
    VehicleID                       INT                 NOT NULL,
    State                           VARCHAR(50)         NOT NULL,
    Mileage                         INT                 NULL,
    ChangeTypeBitFlags              INT                 NOT NULL,
    AuditRowID                      INT                 NOT NULL,
    ChangeTypeBit_DataSet           AS (CONVERT(BIT, ChangeTypeBitFlags&POWER(2,0))),
    ChangeTypeBit_State             AS (CONVERT(BIT, ChangeTypeBitFlags&POWER(2,1))),
    ChangeTypeBit_VehicleId         AS (CONVERT(BIT, ChangeTypeBitFlags&POWER(2,2))),
    ChangeTypeBit_Mileage           AS (CONVERT(BIT, ChangeTypeBitFlags&POWER(2,3))),
    ChangeTypeBit_AdjustmentActions AS (CONVERT(BIT, ChangeTypeBitFlags&POWER(2,4))),
    ChangeTypeBit_AdjustmentStates  AS (CONVERT(BIT, ChangeTypeBitFlags&POWER(2,5))),
    CONSTRAINT PK_Galves_VehicleConfiguration_History
        PRIMARY KEY CLUSTERED (DataLoadID, VehicleConfigurationHistoryID),
    CONSTRAINT FK_Galves_VehicleConfiguration_History__VehicleConfiguration
        FOREIGN KEY (VehicleConfigurationID)
        REFERENCES Galves.VehicleConfiguration (VehicleConfigurationID),
    CONSTRAINT FK_Galves_VehicleConfiguration_History__dataload
        FOREIGN KEY (DataLoadID)
        REFERENCES Galves.dataload (data_load_id),
    CONSTRAINT FK_Galves_VehicleConfiguration_History__Vehicles
        FOREIGN KEY (DataLoadID, VehicleID)
        REFERENCES Galves.Vehicles (data_load_id, ph_guid),
    CONSTRAINT FK_Galves_VehicleConfiguration_History__State
        FOREIGN KEY (DataLoadID, State)
        REFERENCES Galves.region_states (data_load_id, state),
    CONSTRAINT FK_Galves_VehicleConfiguration_History__AuditRow
        FOREIGN KEY (AuditRowID)
        REFERENCES Audit.AuditRow (AuditRowID),
    CONSTRAINT CK_Galves_VehicleConfiguration_History__Mileage
        CHECK (Mileage IS NULL OR Mileage >= 0)
)
ON PS_Galves__DataLoad (DataLoadID)
GO

CREATE TABLE Galves.AdjustmentActionType (
    AdjustmentActionTypeID  TINYINT     NOT NULL,
    Name                    VARCHAR(32) NOT NULL,
    CONSTRAINT PK_Galves_AdjustmentActionType
        PRIMARY KEY CLUSTERED (AdjustmentActionTypeID),
    CONSTRAINT UK_Galves_AdjustmentActionType
        UNIQUE NONCLUSTERED (Name),
    CONSTRAINT CK_Galves_AdjustmentActionType__Name
        CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

INSERT INTO Galves.AdjustmentActionType ( AdjustmentActionTypeID, Name ) VALUES  ( 1, 'Add' )
INSERT INTO Galves.AdjustmentActionType ( AdjustmentActionTypeID, Name ) VALUES  ( 2, 'Remove' )
GO

CREATE TABLE Galves.AdjustmentAction (
    VehicleConfigurationHistoryID   INT                 NOT NULL,
    AdjustmentActionTypeID          TINYINT             NOT NULL,
    AdjustmentActionIndex           TINYINT             NOT NULL,
    DataLoadID                      INT                 NOT NULL,
    Adjustment                      VARCHAR(50)         NOT NULL,
    CONSTRAINT PK_Galves_AdjustmentAction
        PRIMARY KEY CLUSTERED (DataLoadID, VehicleConfigurationHistoryID, Adjustment),
    CONSTRAINT UK_Galves_AdjustmentAction
        UNIQUE NONCLUSTERED (DataLoadID, VehicleConfigurationHistoryID, AdjustmentActionIndex),
    CONSTRAINT FK_Galves_AdjustmentAction__VehicleConfiguration_History
        FOREIGN KEY (DataLoadID, VehicleConfigurationHistoryID)
        REFERENCES Galves.VehicleConfiguration_History (DataLoadID, VehicleConfigurationHistoryID),
    CONSTRAINT FK_Galves_AdjustmentAction__AdjustmentActionType
        FOREIGN KEY (AdjustmentActionTypeID)
        REFERENCES Galves.AdjustmentActionType (AdjustmentActionTypeID),
    CONSTRAINT FK_Galves_AdjustmentAction__dataload
        FOREIGN KEY (DataLoadID)
        REFERENCES Galves.dataload (data_load_id),
    CONSTRAINT CK_Galves_AdjustmentAction__AdjustmentActionType
        CHECK (AdjustmentActionIndex >= 0)
)
ON PS_Galves__DataLoad (DataLoadID)
GO

CREATE TABLE Galves.AdjustmentState (
    VehicleConfigurationHistoryID   INT         NOT NULL,
    DataLoadID                      INT         NOT NULL,
    Adjustment                      VARCHAR(50) NOT NULL,
    Enabled                         BIT         NOT NULL,
    Selected                        BIT         NOT NULL,
    CONSTRAINT PK_Galves_AdjustmentState
        PRIMARY KEY CLUSTERED (DataLoadID, VehicleConfigurationHistoryID, Adjustment),
    CONSTRAINT FK_Galves_AdjustmentState__VehicleConfiguration_History
        FOREIGN KEY (DataLoadID, VehicleConfigurationHistoryID)
        REFERENCES Galves.VehicleConfiguration_History (DataLoadID, VehicleConfigurationHistoryID),
    CONSTRAINT FK_Galves_AdjustmentState__dataload
        FOREIGN KEY (DataLoadID)
        REFERENCES Galves.dataload (data_load_id)
)
ON PS_Galves__DataLoad (DataLoadID)
GO

-- price matrix

CREATE TABLE Galves.Valuation (
    ValuationID TINYINT     NOT NULL,
    Name        VARCHAR(32) NOT NULL,
    CONSTRAINT PK_Galves_Valuation
        PRIMARY KEY CLUSTERED (ValuationID),
    CONSTRAINT UK_Galves_Valuation
        UNIQUE NONCLUSTERED (Name),
    CONSTRAINT CK_Galves_Valuation_Name
        CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

INSERT INTO Galves.Valuation ( ValuationID, Name ) VALUES ( 1, 'Base' )
INSERT INTO Galves.Valuation ( ValuationID, Name ) VALUES ( 2, 'Option' )
INSERT INTO Galves.Valuation ( ValuationID, Name ) VALUES ( 3, 'Mileage' )
INSERT INTO Galves.Valuation ( ValuationID, Name ) VALUES ( 4, 'Final' )
GO

CREATE TABLE Galves.PriceType (
    PriceTypeID TINYINT     NOT NULL,
    Name        VARCHAR(32) NOT NULL,
    BrandName   VARCHAR(32) NOT NULL,
    CONSTRAINT PK_Galves_PriceType
        PRIMARY KEY CLUSTERED (PriceTypeID),
    CONSTRAINT UK_Galves_PriceType
        UNIQUE NONCLUSTERED (Name),
    CONSTRAINT CK_Galves_PriceType_Name
        CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

INSERT INTO Galves.PriceType ( PriceTypeID, Name, BrandName ) VALUES ( 1, 'Trade In', 'Galves Value' )
INSERT INTO Galves.PriceType ( PriceTypeID, Name, BrandName ) VALUES ( 2, 'Retail', 'Market Ready Value' )
GO

CREATE TABLE Galves.Matrix (
    MatrixID    INT IDENTITY(1,1)   NOT NULL,
    DataLoadID  INT                 NOT NULL,
    CONSTRAINT PK_Galves_Matrix
        PRIMARY KEY NONCLUSTERED (DataLoadID, MatrixID),
    CONSTRAINT FK_Galves_Matrix__dataload
        FOREIGN KEY (DataLoadID)
        REFERENCES Galves.dataload (data_load_id)
)
ON PS_Galves__DataLoad (DataLoadID)
GO

CREATE TABLE Galves.MatrixCell (
    MatrixID    INT         NOT NULL,
    DataLoadID  INT         NOT NULL,
    PriceTypeID TINYINT     NOT NULL,
    ValuationID TINYINT     NOT NULL,
    Visible     BIT         NOT NULL,
    Value       SMALLMONEY  NULL,
    CONSTRAINT PK_Galves_MatrixCell
        PRIMARY KEY CLUSTERED (DataLoadID, MatrixID, PriceTypeID, ValuationID),
    CONSTRAINT FK_Galves_MatrixCell__Matrix
        FOREIGN KEY (DataLoadID, MatrixID)
        REFERENCES Galves.Matrix (DataLoadID, MatrixID),
    CONSTRAINT FK_Galves_MatrixCell__PriceType
        FOREIGN KEY (PriceTypeId)
        REFERENCES Galves.PriceType (PriceTypeId),
    CONSTRAINT FK_Galves_MatrixCell__Valuation
        FOREIGN KEY (ValuationID)
        REFERENCES Galves.Valuation (ValuationID),
    CONSTRAINT FK_Galves_MatrixCell__dataload
        FOREIGN KEY (DataLoadID)
        REFERENCES Galves.dataload (data_load_id)
)
ON PS_Galves__DataLoad (DataLoadID)
GO

CREATE TABLE Galves.VehicleConfiguration_History_Matrix (
    VehicleConfigurationHistoryID   INT NOT NULL,
    MatrixID                        INT NOT NULL,
    DataLoadID                      INT NOT NULL,
    CONSTRAINT PK_Galves_VehicleConfigurationHistory_Matrix
        PRIMARY KEY CLUSTERED (DataLoadID, VehicleConfigurationHistoryID),
    CONSTRAINT UK_Galves_VehicleConfigurationHistory_Matrix__Matrix
        UNIQUE NONCLUSTERED (DataLoadID, MatrixID),
    CONSTRAINT FK_Galves_VehicleConfiguration_History_Matrix__dataload
        FOREIGN KEY (DataLoadID)
        REFERENCES Galves.dataload (data_load_id)
)
ON PS_Galves__DataLoad (DataLoadID)
GO

CREATE TABLE Client.Vehicle_Galves (
    VehicleConfigurationID  INT NOT NULL,
    VehicleID               INT NOT NULL,
    CONSTRAINT PK_Client_Vehicle_Galves
        PRIMARY KEY CLUSTERED (VehicleConfigurationID, VehicleID),
    CONSTRAINT FK_Client_Vehicle_Galves__VehicleConfiguration
        FOREIGN KEY (VehicleConfigurationID)
        REFERENCES Galves.VehicleConfiguration (VehicleConfigurationID),
    CONSTRAINT FK_Client_Vehicle_Galves__Vehicle
        FOREIGN KEY (VehicleID)
        REFERENCES Client.Vehicle (VehicleID)
)
ON [DATA]
GO
