
------------------------------------------------------------------------------------------------
-- Magic Database Settings
------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

------------------------------------------------------------------------------------------------
-- Partition Function
------------------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.partition_functions WHERE name = 'PF_Galves__DataLoad')

	CREATE PARTITION FUNCTION PF_Galves__DataLoad(int)
	    AS RANGE LEFT FOR VALUES (
	        2011060100, -- 2011Q2
	        2011090100) -- 2011Q3

GO

------------------------------------------------------------------------------------------------
-- Partition Scheme
------------------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.partition_schemes WHERE name = 'PS_Galves__DataLoad')
	
 	CREATE PARTITION SCHEME PS_Galves__DataLoad
		AS PARTITION PF_Galves__DataLoad TO (
            [DATA_Galves_FG1],
            [DATA_Galves_FG2],
            [PRIMARY])

GO
