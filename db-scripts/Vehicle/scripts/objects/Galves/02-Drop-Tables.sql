
-- client tables

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Client].[Vehicle_Galves]') AND type in (N'U'))
DROP TABLE [Client].[Vehicle_Galves]
GO

-- configuration + matrix tables

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[VehicleConfiguration_History_Matrix]') AND type in (N'U'))
DROP TABLE [Galves].[VehicleConfiguration_History_Matrix]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[MatrixCell]') AND type in (N'U'))
DROP TABLE [Galves].[MatrixCell]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[Matrix]') AND type in (N'U'))
DROP TABLE [Galves].[Matrix]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[PriceType]') AND type in (N'U'))
DROP TABLE [Galves].[PriceType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[Valuation]') AND type in (N'U'))
DROP TABLE [Galves].[Valuation]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[AdjustmentState]') AND type in (N'U'))
DROP TABLE [Galves].[AdjustmentState]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[AdjustmentAction]') AND type in (N'U'))
DROP TABLE [Galves].[AdjustmentAction]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[AdjustmentActionType]') AND type in (N'U'))
DROP TABLE [Galves].[AdjustmentActionType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[VehicleConfiguration_History]') AND type in (N'U'))
DROP TABLE [Galves].[VehicleConfiguration_History]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[VehicleConfiguration]') AND type in (N'U'))
DROP TABLE [Galves].[VehicleConfiguration]
GO

-- their tables

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[cltrans]') AND type in (N'U'))
DROP TABLE [Galves].[cltrans]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[bookdate]') AND type in (N'U'))
DROP TABLE [Galves].[bookdate]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[bgmanuf]') AND type in (N'U'))
DROP TABLE [Galves].[bgmanuf]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[addncost]') AND type in (N'U'))
DROP TABLE [Galves].[addncost]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[vindecoder]') AND type in (N'U'))
DROP TABLE [Galves].[vindecoder]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[vehicles]') AND type in (N'U'))
DROP TABLE [Galves].[vehicles]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[vehicle_mileage]') AND type in (N'U'))
DROP TABLE [Galves].[vehicle_mileage]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[region_states]') AND type in (N'U'))
DROP TABLE [Galves].[region_states]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[region_milefactors]') AND type in (N'U'))
DROP TABLE [Galves].[region_milefactors]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[exceptions]') AND type in (N'U'))
DROP TABLE [Galves].[exceptions]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[curryear]') AND type in (N'U'))
DROP TABLE [Galves].[curryear]
GO

-- our partition

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[dataload]') AND type in (N'U'))
DROP TABLE [Galves].[dataload]
GO
