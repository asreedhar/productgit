IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Person].[Name]') AND type in (N'U'))
DROP TABLE [Person].[Name]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Person].[Demographics]') AND type in (N'U'))
DROP TABLE [Person].[Demographics]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Person].[Contact]') AND type in (N'U'))
DROP TABLE [Person].[Contact]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Person].[Person]') AND type in (N'U'))
DROP TABLE [Person].[Person]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Person].[Title]') AND type in (N'U'))
DROP TABLE [Person].[Title]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Person].[MaritalStatus]') AND type in (N'U'))
DROP TABLE [Person].[MaritalStatus]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Person].[Gender]') AND type in (N'U'))
DROP TABLE [Person].[Gender]
GO