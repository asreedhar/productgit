CREATE TABLE Person.Gender (
		GenderCode		CHAR	NOT NULL,
		[Name]			VARCHAR(50),
        CONSTRAINT PK_Person_Gender PRIMARY KEY (GenderCode)
)
ON [DATA]
GO

CREATE TABLE Person.MaritalStatus (
		MaritalStatusCode	CHAR	NOT NULL,
		[Name]			VARCHAR(50),
        CONSTRAINT PK_Person_MaritalStatus PRIMARY KEY (MaritalStatusCode)
)
ON [DATA]
GO

CREATE TABLE Person.Title (
		TitleID		TINYINT IDENTITY(1,1) NOT NULL,
		[Name]		VARCHAR(50),
		Ordinal		TINYINT,
		CONSTRAINT PK_Person_Title PRIMARY KEY (TitleID)
)
ON [DATA]
GO

CREATE TABLE Person.Person (
        PersonID	INT IDENTITY(1,1) NOT NULL,
        ClientID    INT	NOT NULL,
		RevisionNumber SMALLINT,
		CONSTRAINT PK_Person_Person PRIMARY KEY CLUSTERED ([PersonID] ASC), 
		CONSTRAINT FK_Person_Person__Client
			FOREIGN KEY (ClientID)
			REFERENCES Client.Client (ClientID)
)
ON [DATA]
GO

CREATE TABLE Person.Contact (
        PersonID	INT NOT NULL,
		ContactID	INT NOT NULL,
        CONSTRAINT PK_Person_Contact PRIMARY KEY CLUSTERED ([PersonID] ASC),
		CONSTRAINT FK_Person_Contact__Person
			FOREIGN KEY (PersonID)
			REFERENCES Person.Person (PersonID),
		CONSTRAINT FK_Person_Contact__Contact
			FOREIGN KEY (ContactID)
			REFERENCES Contact.Contact (ContactID)
)
ON [DATA]
GO

CREATE TABLE Person.Demographics (
		PersonID	INT NOT NULL,
		BirthDate	DATETIME,
		Occupation	VARCHAR(100),
		MaritalStatusCode CHAR,
		GenderCode	CHAR,
        CONSTRAINT PK_Person_Demographics PRIMARY KEY CLUSTERED ([PersonID] ASC),
		CONSTRAINT FK_Person_Demographics__Person
			FOREIGN KEY (PersonID)
			REFERENCES Person.Person (PersonID),
		CONSTRAINT FK_Person_Demographics__MaritalStatus
			FOREIGN KEY (MaritalStatusCode)
			REFERENCES Person.MaritalStatus (MaritalStatusCode),
		CONSTRAINT FK_Person_Demographics__Gender
			FOREIGN KEY (GenderCode)
			REFERENCES Person.Gender (GenderCode)
)
ON [DATA]
GO

CREATE TABLE Person.Name (
		PersonID	INT NOT NULL,
		TitleID		TINYINT NOT NULL,
		Given		VARCHAR(100),
		Middle		VARCHAR(100),
		Family		VARCHAR(100),
		CONSTRAINT PK_Person_Name PRIMARY KEY CLUSTERED ([PersonID] ASC),
		CONSTRAINT FK_Person_Name__Person
			FOREIGN KEY (PersonID)
			REFERENCES Person.Person (PersonID),
		CONSTRAINT FK_Person_Name__Title
			FOREIGN KEY (TitleID)
			REFERENCES Person.Title (TitleID)
)
ON [DATA]
GO