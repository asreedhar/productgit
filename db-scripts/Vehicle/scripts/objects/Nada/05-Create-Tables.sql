
-- their data

CREATE TABLE Nada.Periods(
	Period  INT NOT NULL,
	CONSTRAINT PK_Nada_Periods
	    PRIMARY KEY CLUSTERED (Period)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.Years(
	Period      INT NOT NULL,
	VehicleYear INT NOT NULL,
	CONSTRAINT PK_Nada_Years
	    PRIMARY KEY CLUSTERED (Period, VehicleYear),
    CONSTRAINT FK_Nada_Years__Periods
	    FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.Makes(
	Period int NOT NULL,
	MakeCode int NOT NULL,
	MakeDescr varchar(50) NOT NULL,
	CONSTRAINT PK_Nada_Makes
	    PRIMARY KEY CLUSTERED (Period, MakeCode),
    CONSTRAINT FK_Nada_Makes__Periods
	    FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.Series(
	Period int NOT NULL,
	SeriesCode int NOT NULL,
	SeriesDescr varchar(50) NOT NULL,
	CONSTRAINT PK_Nada_Series
	    PRIMARY KEY (Period, SeriesCode),
    CONSTRAINT FK_Nada_Series__Periods
	    FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.Bodies(
	Period int NOT NULL,
	BodyCode int NOT NULL,
	BodyDescr varchar(50) NOT NULL,
	CONSTRAINT PK_Nada_Bodies
	    PRIMARY KEY CLUSTERED (Period, BodyCode),
    CONSTRAINT FK_Nada_Bodies__Periods
	    FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.Regions(
	Period int NOT NULL,
	RegionId int NOT NULL,
	RegionDescr varchar(50) NOT NULL,
	CONSTRAINT PK_Nada_Regions
	    PRIMARY KEY CLUSTERED (Period, RegionId),
    CONSTRAINT FK_Nada_Regions__Periods
	    FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.States(
	Period int NOT NULL,
	StateCode char(3) NOT NULL,
	RegionId int NOT NULL,
	StateDescr varchar(50) NOT NULL,
	StateAbbr char(2) NULL,
	CONSTRAINT PK_Nada_States
	    PRIMARY KEY CLUSTERED (Period, StateCode),
    CONSTRAINT FK_Nada_States__Periods
	    FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period),
    CONSTRAINT FK_Nada_States__StateCodes
	    FOREIGN KEY (StateCode)
	    REFERENCES Nada.StateCodes (StateCode),
    CONSTRAINT FK_Nada_States__Regions
	    FOREIGN KEY (Period, RegionId)
	    REFERENCES Nada.Regions (Period, RegionId)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.Mileages(
	Period int NOT NULL,
	VehicleYear int NOT NULL,
	MileageCode char(1) NOT NULL,
	CONSTRAINT PK_Nada_Mileages
	    PRIMARY KEY CLUSTERED (Period, VehicleYear, MileageCode),
    CONSTRAINT FK_Nada_Mileages__Periods
	    FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period),
    CONSTRAINT FK_Nada_Mileages__Years
	    FOREIGN KEY (Period, VehicleYear)
	    REFERENCES Nada.Years (Period, VehicleYear)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.MileageValues(
	Period int NOT NULL,
	VehicleYear int NOT NULL,
	MileageCode char(1) NOT NULL,
	Low int NOT NULL,
	High int NOT NULL,
	Adjustment int NOT NULL,
	AdjustmentRate decimal(9, 6) NOT NULL,
	AdjustmentPercent decimal(18, 0) NOT NULL,
	Descr varchar(50) NOT NULL,
	CONSTRAINT PK_Nada_MileageValues
	    PRIMARY KEY CLUSTERED (Period, VehicleYear, MileageCode, Low, High),
    CONSTRAINT FK_Nada_MileageValues__Periods
	    FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period),
    CONSTRAINT FK_Nada_MileageValues__Years
	    FOREIGN KEY (Period, VehicleYear)
	    REFERENCES Nada.Years (Period, VehicleYear),
    CONSTRAINT FK_Nada_MileageValues__Mileages
	    FOREIGN KEY (Period, VehicleYear, MileageCode)
	    REFERENCES Nada.Mileages (Period, VehicleYear, MileageCode)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.ValueTypes(
	Period int NOT NULL,
	ValueTypeId int NOT NULL,
	ValueDescr varchar(20) NOT NULL,
	CONSTRAINT PK_Nada_ValueTypes
	    PRIMARY KEY CLUSTERED (Period, ValueTypeId),
    CONSTRAINT FK_Nada_ValueTypes__Periods
	    FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.TotalAdjFloorValues(
	Period int NOT NULL,
	ValueTypeId int NOT NULL,
	ValueAmount int NOT NULL,
	CONSTRAINT PK_Nada_TotalAdjFloorValues
	    PRIMARY KEY CLUSTERED (Period, ValueTypeId),
    CONSTRAINT FK_Nada_TotalAdjFloorValues__Periods
	    FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period),
    CONSTRAINT FK_Nada_TotalAdjFloorValues__ValueTypes
	    FOREIGN KEY (Period, ValueTypeId)
	    REFERENCES Nada.ValueTypes (Period, ValueTypeId)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.Vehicles(
	Period int NOT NULL,
	Uid int NOT NULL,
	VehicleYear int NOT NULL,
	MakeCode int NOT NULL,
	SeriesCode int NOT NULL,
	BodyCode int NOT NULL,
	MileageCode char(1) NOT NULL,
	Vic varchar(15) NOT NULL,
	CurbWeight int NOT NULL,
	GVW int NOT NULL,
	GCW int NOT NULL,
	MSRP int NOT NULL,
	CONSTRAINT PK_Nada_Vehicles
	    PRIMARY KEY CLUSTERED (Period, Uid),
    CONSTRAINT FK_Nada_Vehicles__Periods
	    FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period),
    CONSTRAINT FK_Nada_Vehicles__Years
	    FOREIGN KEY (Period, VehicleYear)
	    REFERENCES Nada.Years (Period, VehicleYear),
    CONSTRAINT FK_Nada_Vehicles__Makes
	    FOREIGN KEY (Period, MakeCode)
	    REFERENCES Nada.Makes (Period, MakeCode),
    CONSTRAINT FK_Nada_Vehicles__Series
	    FOREIGN KEY (Period, SeriesCode)
	    REFERENCES Nada.Series (Period, SeriesCode),
    CONSTRAINT FK_Nada_Vehicles__Bodies
	    FOREIGN KEY (Period, BodyCode)
	    REFERENCES Nada.Bodies (Period, BodyCode),
    CONSTRAINT FK_Nada_Vehicles__MileageCode
	    FOREIGN KEY (Period, VehicleYear, MileageCode)
	    REFERENCES Nada.Mileages (Period, VehicleYear, MileageCode),
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.VehicleValues(
	Period int NOT NULL,
	Uid int NOT NULL,
	RegionId int NOT NULL,
	ValueTypeId int NOT NULL,
	ValueAmount int NULL,
	CONSTRAINT PK_Nada_VehicleValues
	    PRIMARY KEY CLUSTERED (Period, Uid, RegionId, ValueTypeId),
    CONSTRAINT FK_Nada_VehicleValues__Periods
        FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period),
    CONSTRAINT FK_Nada_VehicleValues__Regions
        FOREIGN KEY (Period, RegionId)
	    REFERENCES Nada.Regions (Period, RegionId),
    CONSTRAINT FK_Nada_VehicleValues__ValueTypes
        FOREIGN KEY (Period, ValueTypeId)
	    REFERENCES Nada.ValueTypes (Period, ValueTypeId),
    CONSTRAINT FK_Nada_VehicleValues__Vehicle
        FOREIGN KEY (Period, Uid)
	    REFERENCES Nada.Vehicles (Period, Uid)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.VinVehicles(
	Period int NOT NULL,
	VinVehicleId int NOT NULL,
	Vin varchar(17) NOT NULL,
	CONSTRAINT PK_Nada_VinVehicles
	    PRIMARY KEY CLUSTERED (Period, VinVehicleId),
    CONSTRAINT UK_Nada_VinVehicles
	    UNIQUE (Period, Vin),
    CONSTRAINT FK_Nada_VinVehicles__Periods
        FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.VinVehicleMaps(
	Period int NOT NULL,
	VinVehicleId int NOT NULL,
	Uid int NOT NULL,
	Priority int NULL,
	CONSTRAINT PK_Nada_VinVehicleMaps
	    PRIMARY KEY CLUSTERED (Period, VinVehicleId, Uid),
    CONSTRAINT FK_Nada_VinVehicleMaps__Periods
        FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period),
    CONSTRAINT FK_Nada_VinVehicleMaps__Vehicle
        FOREIGN KEY (Period, Uid)
	    REFERENCES Nada.Vehicles (Period, Uid),
    CONSTRAINT FK_Nada_VinVehicleMaps__VinVehicles
        FOREIGN KEY (Period, VinVehicleId)
	    REFERENCES Nada.VinVehicles (Period, VinVehicleId)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.AccessoryCategories(
	Period int NOT NULL,
	AccCategoryCode char(3) NOT NULL,
	AccCategoryDescr varchar(50) NOT NULL,
	CONSTRAINT PK_Nada_AccessoryCategories
	    PRIMARY KEY CLUSTERED (Period, AccCategoryCode),
    CONSTRAINT FK_Nada_AccessoryCategories__Periods
        FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.Accessories(
	Period int NOT NULL,
	AccCode char(3) NOT NULL,
	AccDescr varchar(50) NOT NULL,
	AccCategoryCode char(3) NULL,
	CONSTRAINT PK_Nada_Accessories
	    PRIMARY KEY CLUSTERED (Period, AccCode),
    CONSTRAINT FK_Nada_Accessories__Periods
        FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period),
    CONSTRAINT FK_Nada_Accessories__AccessoryCategories
        FOREIGN KEY (Period, AccCategoryCode)
	    REFERENCES Nada.AccessoryCategories (Period, AccCategoryCode)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.VehicleAccessories(
	Period int NOT NULL,
	Uid int NOT NULL,
	AccCode char(3) NOT NULL,
	IsIncluded bit NOT NULL,
	CONSTRAINT PK_Nada_VehicleAccessories
	    PRIMARY KEY CLUSTERED (Period, Uid, AccCode),
    CONSTRAINT FK_Nada_VehicleAccessories__Periods
        FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period),
    CONSTRAINT FK_Nada_VehicleAccessories__Vehicles
        FOREIGN KEY (Period, Uid)
	    REFERENCES Nada.Vehicles (Period, Uid),
    CONSTRAINT FK_Nada_VehicleAccessories__Accessories
        FOREIGN KEY (Period, AccCode)
	    REFERENCES Nada.Accessories (Period, AccCode)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.VinVehicleAccessories(
	Period int NOT NULL,
	VinVehicleId int NOT NULL,
	Uid int NOT NULL,
	AccCode char(3) NOT NULL,
	CONSTRAINT PK_Nada_VinVehicleAccessories
	    PRIMARY KEY CLUSTERED (Period, VinVehicleId, Uid, AccCode),
    CONSTRAINT FK_Nada_VinVehicleAccessories__Periods
        FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period),
    CONSTRAINT FK_Nada_VinVehicleAccessories__Vehicles
        FOREIGN KEY (Period, Uid)
	    REFERENCES Nada.Vehicles (Period, Uid),
    CONSTRAINT FK_Nada_VinVehicleAccessories__VinVehicles
        FOREIGN KEY (Period, VinVehicleId)
	    REFERENCES Nada.VinVehicles (Period, VinVehicleId),
    CONSTRAINT FK_Nada_VinVehicleAccessories__Accessories
        FOREIGN KEY (Period, AccCode)
	    REFERENCES Nada.Accessories (Period, AccCode)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.VehicleAccessoryValues(
	Period int NOT NULL,
	Uid int NOT NULL,
	AccCode char(3) NOT NULL,
	RegionId int NOT NULL,
	ValueTypeId int NOT NULL,
	ValueAmount int NOT NULL,
	CONSTRAINT PK_Nada_VehicleAccessoryValues
	    PRIMARY KEY CLUSTERED (Period, Uid, AccCode, RegionId, ValueTypeId),
    CONSTRAINT FK_Nada_VehicleAccessoryValues__Periods
        FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period),
    CONSTRAINT FK_Nada_VehicleAccessoryValues__Vehicles
        FOREIGN KEY (Period, Uid)
	    REFERENCES Nada.Vehicles (Period, Uid),
    CONSTRAINT FK_Nada_VehicleAccessoryValues__Regions
        FOREIGN KEY (Period, RegionId)
	    REFERENCES Nada.Regions (Period, RegionId),
    CONSTRAINT FK_Nada_VehicleAccessoryValues__ValueTypes
        FOREIGN KEY (Period, ValueTypeId)
	    REFERENCES Nada.ValueTypes (Period, ValueTypeId),
    CONSTRAINT FK_Nada_VehicleAccessoryValues__Accessories
        FOREIGN KEY (Period, AccCode)
	    REFERENCES Nada.Accessories (Period, AccCode)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.InclusiveAccessories(
	Period int NOT NULL,
	Uid int NOT NULL,
	AccCode char(3) NOT NULL,
	AccInclCode char(3) NOT NULL,
	CONSTRAINT PK_Nada_InclusiveAccessories
	    PRIMARY KEY CLUSTERED (Period, Uid, AccCode, AccInclCode),
    CONSTRAINT FK_Nada_InclusiveAccessories__Periods
        FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period),
    CONSTRAINT FK_Nada_InclusiveAccessories__Vehicles
        FOREIGN KEY (Period, Uid)
	    REFERENCES Nada.Vehicles (Period, Uid),
    CONSTRAINT FK_Nada_InclusiveAccessories__Accessories
        FOREIGN KEY (Period, AccCode)
	    REFERENCES Nada.Accessories (Period, AccCode),
    CONSTRAINT FK_Nada_InclusiveAccessories__Accessories_Incl
        FOREIGN KEY (Period, AccInclCode)
	    REFERENCES Nada.Accessories (Period, AccCode)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.ExclusiveAccessories(
	Period int NOT NULL,
	Uid int NOT NULL,
	AccCode char(3) NOT NULL,
	AccExcludeCode char(3) NOT NULL,
    CONSTRAINT PK_Nada_ExclusiveAccessories
	    PRIMARY KEY CLUSTERED (Period, Uid, AccCode, AccExcludeCode),
    CONSTRAINT FK_Nada_ExclusiveAccessories__Periods
        FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period),
    CONSTRAINT FK_Nada_ExclusiveAccessories__Vehicles
        FOREIGN KEY (Period, Uid)
	    REFERENCES Nada.Vehicles (Period, Uid),
    CONSTRAINT FK_Nada_ExclusiveAccessories__Accessories
        FOREIGN KEY (Period, AccCode)
	    REFERENCES Nada.Accessories (Period, AccCode),
    CONSTRAINT FK_Nada_ExclusiveAccessories__Accessories_Excl
        FOREIGN KEY (Period, AccExcludeCode)
	    REFERENCES Nada.Accessories (Period, AccCode)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.AttributeTypes(
	Period int NOT NULL,
	AttributeTypeId int NOT NULL,
	AttributeTypeDescr varchar(50) NOT NULL,
    CONSTRAINT PK_Nada_AttributeTypes
	    PRIMARY KEY CLUSTERED (Period, AttributeTypeId),
    CONSTRAINT FK_Nada_AttributeTypes__Periods
        FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period)
    
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.VehicleAttributes(
	Period int NOT NULL,
	Uid int NOT NULL,
	AttributeTypeId int NOT NULL,
	IntValue int NULL,
	VarcharValue varchar(50) NULL,
	CONSTRAINT PK_Nada_VehicleAttributes
	    PRIMARY KEY CLUSTERED (Period, Uid, AttributeTypeId),
    CONSTRAINT FK_Nada_VehicleAttributes__Periods
        FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period),
    CONSTRAINT FK_Nada_VehicleAttributes__Vehicles
        FOREIGN KEY (Period, Uid)
	    REFERENCES Nada.Vehicles (Period, Uid),
    CONSTRAINT FK_Nada_VehicleAttributes__AttributeTypes
        FOREIGN KEY (Period, AttributeTypeId)
	    REFERENCES Nada.AttributeTypes (Period, AttributeTypeId)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.VinVehicleAttributes(
	Period int NOT NULL,
	VinVehicleId int NOT NULL,
	AttributeTypeId int NOT NULL,
	IntValue int NULL,
	VarcharValue varchar(50) NULL,
	CONSTRAINT PK_Nada_VinVehicleAttributes
	    PRIMARY KEY CLUSTERED (Period, VinVehicleId, AttributeTypeId),
    CONSTRAINT FK_Nada_VinVehicleAttributes__Periods
        FOREIGN KEY (Period)
	    REFERENCES Nada.Periods (Period),
    CONSTRAINT FK_Nada_VinVehicleAttributes__VinVehicles
        FOREIGN KEY (Period, VinVehicleId)
	    REFERENCES Nada.VinVehicles (Period, VinVehicleId),
    CONSTRAINT FK_Nada_VinVehicleAttributes__AttributeTypes
        FOREIGN KEY (Period, AttributeTypeId)
	    REFERENCES Nada.AttributeTypes (Period, AttributeTypeId)
)
ON PS_Nada__Period (Period)
GO

-- vehicle configuration

CREATE TABLE Nada.VehicleConfiguration (
    VehicleConfigurationID  INT IDENTITY(1,1)   NOT NULL,
    VehicleID               INT                 NOT NULL,
    CONSTRAINT PK_Nada_VehicleConfiguration
        PRIMARY KEY CLUSTERED (VehicleConfigurationID),
    CONSTRAINT FK_Nada_VehicleConfiguration__Vehicle
        FOREIGN KEY (VehicleID)
        REFERENCES Reference.Vehicle (VehicleID)
)
ON [DATA]
GO

CREATE TABLE Nada.VehicleConfiguration_History (
    VehicleConfigurationHistoryID   INT IDENTITY(1,1)   NOT NULL,
    VehicleConfigurationID          INT                 NOT NULL,
    Period                          INT                 NOT NULL,
    Uid                             INT                 NOT NULL,
    StateID                         INT                 NOT NULL,
    Mileage                         INT                 NULL,
    ChangeTypeBitFlags              INT                 NOT NULL,
    AuditRowID                      INT                 NOT NULL,
    ChangeTypeBit_Period            AS (CONVERT(BIT, ChangeTypeBitFlags&POWER(2,0))),
    ChangeTypeBit_State             AS (CONVERT(BIT, ChangeTypeBitFlags&POWER(2,1))),
    ChangeTypeBit_Uid               AS (CONVERT(BIT, ChangeTypeBitFlags&POWER(2,2))),
    ChangeTypeBit_Mileage           AS (CONVERT(BIT, ChangeTypeBitFlags&POWER(2,3))),
    ChangeTypeBit_AccessoryActions  AS (CONVERT(BIT, ChangeTypeBitFlags&POWER(2,4))),
    ChangeTypeBit_AccessoryStates   AS (CONVERT(BIT, ChangeTypeBitFlags&POWER(2,5))),
    CONSTRAINT PK_Nada_VehicleConfiguration_History
        PRIMARY KEY CLUSTERED (Period, VehicleConfigurationHistoryID),
    CONSTRAINT FK_Nada_VehicleConfiguration_History__VehicleConfiguration
        FOREIGN KEY (VehicleConfigurationID)
        REFERENCES Nada.VehicleConfiguration (VehicleConfigurationID),
    CONSTRAINT FK_Nada_VehicleConfiguration_History__Periods
        FOREIGN KEY (Period)
        REFERENCES Nada.Periods (Period),
    CONSTRAINT FK_Nada_VehicleConfiguration_History__Vehicles
        FOREIGN KEY (Period, Uid)
        REFERENCES Nada.Vehicles (Period, Uid),
    CONSTRAINT FK_Nada_VehicleConfiguration_History__State
        FOREIGN KEY (StateID)
        REFERENCES Nada.StateCodes (StateID),
    CONSTRAINT FK_Nada_VehicleConfiguration_History__AuditRow
        FOREIGN KEY (AuditRowID)
        REFERENCES Audit.AuditRow (AuditRowID),
    CONSTRAINT CK_Nada_VehicleConfiguration_History__Mileage
        CHECK (Mileage IS NULL OR Mileage >= 0)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.AccessoryActionType (
    AccessoryActionTypeID   TINYINT     NOT NULL,
    Name                    VARCHAR(32) NOT NULL,
    CONSTRAINT PK_Nada_AccessoryActionType
        PRIMARY KEY CLUSTERED (AccessoryActionTypeID),
    CONSTRAINT UK_Nada_AccessoryActionType
        UNIQUE NONCLUSTERED (Name),
    CONSTRAINT CK_Nada_AccessoryActionType__Name
        CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

INSERT INTO Nada.AccessoryActionType ( AccessoryActionTypeID, Name ) VALUES  ( 1, 'Add' )
INSERT INTO Nada.AccessoryActionType ( AccessoryActionTypeID, Name ) VALUES  ( 2, 'Remove' )
GO

CREATE TABLE Nada.AccessoryAction (
    VehicleConfigurationHistoryID   INT                 NOT NULL,
    AccessoryActionTypeID           TINYINT             NOT NULL,
    AccessoryActionIndex            TINYINT             NOT NULL,
    Period                          INT                 NOT NULL,
    AccCode                         CHAR(3)             NOT NULL,
    CONSTRAINT PK_Nada_AccessoryAction
        PRIMARY KEY CLUSTERED (Period, VehicleConfigurationHistoryID, AccCode),
    CONSTRAINT UK_Nada_AccessoryAction
        UNIQUE NONCLUSTERED (Period, VehicleConfigurationHistoryID, AccessoryActionIndex),
    CONSTRAINT FK_Nada_AccessoryAction__VehicleConfiguration_History
        FOREIGN KEY (Period, VehicleConfigurationHistoryID)
        REFERENCES Nada.VehicleConfiguration_History (Period, VehicleConfigurationHistoryID),
    CONSTRAINT FK_Nada_AccessoryAction__AccessoryActionType
        FOREIGN KEY (AccessoryActionTypeID)
        REFERENCES Nada.AccessoryActionType (AccessoryActionTypeID),
    CONSTRAINT FK_Nada_AccessoryAction__Periods
        FOREIGN KEY (Period)
        REFERENCES Nada.Periods (Period),
    CONSTRAINT FK_Nada_AccessoryAction__Accessories
        FOREIGN KEY (Period, AccCode)
        REFERENCES Nada.Accessories (Period, AccCode),
    CONSTRAINT CK_Nada_AccessoryAction__AccessoryActionType
        CHECK (AccessoryActionIndex >= 0)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.AccessoryState (
    VehicleConfigurationHistoryID   INT     NOT NULL,
    Period                          INT     NOT NULL,
    AccCode                         CHAR(3) NOT NULL,
    Enabled                         BIT     NOT NULL,
    Selected                        BIT     NOT NULL,
    CONSTRAINT PK_Nada_AccessoryState
        PRIMARY KEY CLUSTERED (Period, VehicleConfigurationHistoryID, AccCode),
    CONSTRAINT FK_Nada_AccessoryState__VehicleConfiguration_History
        FOREIGN KEY (Period, VehicleConfigurationHistoryID)
        REFERENCES Nada.VehicleConfiguration_History (Period, VehicleConfigurationHistoryID),
    CONSTRAINT FK_Nada_AccessoryState__Periods
        FOREIGN KEY (Period)
        REFERENCES Nada.Periods (Period),
    CONSTRAINT FK_Nada_AccessoryState__Accessories
        FOREIGN KEY (Period, AccCode)
        REFERENCES Nada.Accessories (Period, AccCode)
)
ON PS_Nada__Period (Period)
GO

-- price matrix

CREATE TABLE Nada.Valuation (
    ValuationID TINYINT     NOT NULL,
    Name        VARCHAR(32) NOT NULL,
    CONSTRAINT PK_Nada_Valuation
        PRIMARY KEY CLUSTERED (ValuationID),
    CONSTRAINT UK_Nada_Valuation
        UNIQUE NONCLUSTERED (Name),
    CONSTRAINT CK_Nada_Valuation_Name
        CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

INSERT INTO Nada.Valuation ( ValuationID, Name ) VALUES ( 1, 'Base' )
INSERT INTO Nada.Valuation ( ValuationID, Name ) VALUES ( 2, 'Option' )
INSERT INTO Nada.Valuation ( ValuationID, Name ) VALUES ( 3, 'Mileage' )
INSERT INTO Nada.Valuation ( ValuationID, Name ) VALUES ( 4, 'Final' )
GO

CREATE TABLE Nada.Matrix (
    MatrixID    INT IDENTITY(1,1) NOT NULL,
    Period      INT NOT NULL,
    CONSTRAINT PK_Nada_Matrix
        PRIMARY KEY CLUSTERED (Period, MatrixID),
    CONSTRAINT FK_Nada_Matrix__Periods
        FOREIGN KEY (Period)
        REFERENCES Nada.Periods (Period)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.MatrixCell (
    MatrixID    INT         NOT NULL,
    Period      INT         NOT NULL,
    ValueTypeID INT         NOT NULL,
    ValuationID TINYINT     NOT NULL,
    Visible     BIT         NOT NULL,
    Value       SMALLMONEY  NULL,
    CONSTRAINT PK_Nada_MatrixCell
        PRIMARY KEY CLUSTERED (Period, MatrixID, ValueTypeID, ValuationID),
    CONSTRAINT FK_Nada_MatrixCell__Matrix
        FOREIGN KEY (Period, MatrixID)
        REFERENCES Nada.Matrix (Period, MatrixID),
    CONSTRAINT FK_Nada_MatrixCell__Periods
        FOREIGN KEY (Period)
        REFERENCES Nada.Periods (Period),
    CONSTRAINT FK_Nada_MatrixCell__ValueTypes
        FOREIGN KEY (Period, ValueTypeId)
        REFERENCES Nada.ValueTypes (Period, ValueTypeId),
    CONSTRAINT FK_Nada_MatrixCell__Valuation
        FOREIGN KEY (ValuationID)
        REFERENCES Nada.Valuation (ValuationID)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Nada.VehicleConfiguration_History_Matrix (
    VehicleConfigurationHistoryID   INT NOT NULL,
    MatrixID                        INT NOT NULL,
    Period                          INT NOT NULL,
    CONSTRAINT PK_Nada_VehicleConfigurationHistory_Matrix
        PRIMARY KEY CLUSTERED (Period, VehicleConfigurationHistoryID),
    CONSTRAINT UK_Nada_VehicleConfigurationHistory_Matrix__Matrix
        UNIQUE NONCLUSTERED (Period, MatrixID),
    CONSTRAINT FK_Nada_VehicleConfiguration_History_Matrix__Periods
        FOREIGN KEY (Period)
        REFERENCES Nada.Periods (Period)
)
ON PS_Nada__Period (Period)
GO

CREATE TABLE Client.Vehicle_Nada (
    VehicleConfigurationID  INT NOT NULL,
    VehicleID               INT NOT NULL,
    CONSTRAINT PK_Client_Vehicle_Nada
        PRIMARY KEY CLUSTERED (VehicleConfigurationID, VehicleID),
    CONSTRAINT FK_Client_Vehicle_Nada__VehicleConfiguration
        FOREIGN KEY (VehicleConfigurationID)
        REFERENCES Nada.VehicleConfiguration (VehicleConfigurationID),
    CONSTRAINT FK_Client_Vehicle_Nada__Vehicle
        FOREIGN KEY (VehicleID)
        REFERENCES Client.Vehicle (VehicleID)
)
ON [DATA]
GO
