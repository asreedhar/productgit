
------------------------------------------------------------------------------------------------
-- Magic Database Settings
------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

------------------------------------------------------------------------------------------------
-- Partition Function
------------------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.partition_functions WHERE name = 'PF_Nada__Period')

	CREATE PARTITION FUNCTION PF_Nada__Period(int)
	    AS RANGE LEFT FOR VALUES (
	        201106, -- 2011Q2
	        201109) -- 2011Q3

GO

------------------------------------------------------------------------------------------------
-- Partition Scheme
------------------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.partition_schemes WHERE name = 'PS_Nada__Period')
	
 	CREATE PARTITION SCHEME PS_Nada__Period
		AS PARTITION PF_Nada__Period TO (
            [DATA_NADA_FG1],
            [DATA_NADA_FG2],
            [PRIMARY])

GO
