
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Client].[Vehicle_Nada]') AND type in (N'U'))
DROP TABLE [Client].[Vehicle_Nada]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[VehicleConfiguration_History_Matrix]') AND type in (N'U'))
DROP TABLE [Nada].[VehicleConfiguration_History_Matrix]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[MatrixCell]') AND type in (N'U'))
DROP TABLE [Nada].[MatrixCell]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[Matrix]') AND type in (N'U'))
DROP TABLE [Nada].[Matrix]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[Valuation]') AND type in (N'U'))
DROP TABLE [Nada].[Valuation]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[AccessoryState]') AND type in (N'U'))
DROP TABLE [Nada].[AccessoryState]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[AccessoryAction]') AND type in (N'U'))
DROP TABLE [Nada].[AccessoryAction]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[AccessoryActionType]') AND type in (N'U'))
DROP TABLE [Nada].[AccessoryActionType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[VehicleConfiguration_History]') AND type in (N'U'))
DROP TABLE [Nada].[VehicleConfiguration_History]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[VehicleConfiguration]') AND type in (N'U'))
DROP TABLE [Nada].[VehicleConfiguration]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[VinVehicleAttributes]') AND type in (N'U'))
DROP TABLE [Nada].[VinVehicleAttributes]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[VehicleAttributes]') AND type in (N'U'))
DROP TABLE [Nada].[VehicleAttributes]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[AttributeTypes]') AND type in (N'U'))
DROP TABLE [Nada].[AttributeTypes]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[ExclusiveAccessories]') AND type in (N'U'))
DROP TABLE [Nada].[ExclusiveAccessories]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[InclusiveAccessories]') AND type in (N'U'))
DROP TABLE [Nada].[InclusiveAccessories]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[VehicleAccessoryValues]') AND type in (N'U'))
DROP TABLE [Nada].[VehicleAccessoryValues]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[VinVehicleAccessories]') AND type in (N'U'))
DROP TABLE [Nada].[VinVehicleAccessories]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[VehicleAccessories]') AND type in (N'U'))
DROP TABLE [Nada].[VehicleAccessories]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[Accessories]') AND type in (N'U'))
DROP TABLE [Nada].[Accessories]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[AccessoryCategories]') AND type in (N'U'))
DROP TABLE [Nada].[AccessoryCategories]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[VinVehicleMaps]') AND type in (N'U'))
DROP TABLE [Nada].[VinVehicleMaps]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[VinVehicles]') AND type in (N'U'))
DROP TABLE [Nada].[VinVehicles]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[VehicleValues]') AND type in (N'U'))
DROP TABLE [Nada].[VehicleValues]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[Vehicles]') AND type in (N'U'))
DROP TABLE [Nada].[Vehicles]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[TotalAdjFloorValues]') AND type in (N'U'))
DROP TABLE [Nada].[TotalAdjFloorValues]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[ValueTypes]') AND type in (N'U'))
DROP TABLE [Nada].[ValueTypes]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[MileageValues]') AND type in (N'U'))
DROP TABLE [Nada].[MileageValues]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[Mileages]') AND type in (N'U'))
DROP TABLE [Nada].[Mileages]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[States]') AND type in (N'U'))
DROP TABLE [Nada].[States]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[StateCodes]') AND type in (N'U'))
DROP TABLE [Nada].[StateCodes]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[Regions]') AND type in (N'U'))
DROP TABLE [Nada].[Regions]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[Bodies]') AND type in (N'U'))
DROP TABLE [Nada].[Bodies]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[Series]') AND type in (N'U'))
DROP TABLE [Nada].[Series]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[Makes]') AND type in (N'U'))
DROP TABLE [Nada].[Makes]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[Years]') AND type in (N'U'))
DROP TABLE [Nada].[Years]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[Periods]') AND type in (N'U'))
DROP TABLE [Nada].[Periods]
GO
