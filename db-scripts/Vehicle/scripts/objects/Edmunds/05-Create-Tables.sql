
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET NOCOUNT ON
GO

-- our data-set key

CREATE TABLE Edmunds.FirstLook_DataLoad (
    DataLoadID    INT         NOT NULL,
    DataLoadTime  DATETIME    NOT NULL,
    CONSTRAINT PK_Edmunds_FirstLook_DataLoad
        PRIMARY KEY CLUSTERED (
            DataLoadID
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

-- the Edmunds data

CREATE TABLE [Edmunds].[TMV_VALID_CONDITION](
    [DataLoadID] INT NOT NULL,
    [TMV_CONDITION_ID] INT NOT NULL,
    [TMV_CONDITION] VARCHAR(20) NOT NULL,
    CONSTRAINT [PK_TMV_VALID_CONDITION]
        PRIMARY KEY CLUSTERED (
            [DataLoadID],
            [TMV_CONDITION_ID]
        ),
    CONSTRAINT FK_TMV_VALID_CONDITION__FirstLook_DataLoad
        FOREIGN KEY (
            [DataLoadID]
        )
        REFERENCES Edmunds.FirstLook_DataLoad (
            [DataLoadID]
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE [Edmunds].[TMV_VALID_GENERIC_COLOR](
    [DataLoadID] INT NOT NULL,
	[TMV_GENERIC_COLOR_ID] INT NOT NULL,
	[TMV_GENERIC_COLOR] VARCHAR(20) NOT NULL,
    CONSTRAINT [PK_TMV_GENERIC_COLOR]
        PRIMARY KEY CLUSTERED (
	        [DataLoadID],
            [TMV_GENERIC_COLOR_ID]
        ),
    CONSTRAINT FK_TMV_VALID_GENERIC_COLOR__FirstLook_DataLoad
        FOREIGN KEY (
            [DataLoadID]
        )
        REFERENCES Edmunds.FirstLook_DataLoad (
            [DataLoadID]
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE [Edmunds].[TMVU_VALID_OPTION](
    [DataLoadID] INT NOT NULL,
	[FEATURE_ID] BIGINT NOT NULL,
	[FEATURE_DESCRIPTION] VARCHAR(100) NULL,
    CONSTRAINT [PK_TMVU_VALID_OPTION]
        PRIMARY KEY CLUSTERED (
	        [DataLoadID],
            [FEATURE_ID]
        ),
    CONSTRAINT FK_TMV_VALID_OPTION__FirstLook_DataLoad
        FOREIGN KEY (
            [DataLoadID]
        )
        REFERENCES Edmunds.FirstLook_DataLoad (
            [DataLoadID]
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE [Edmunds].[TMV_VALID_REGION](
    [DataLoadID] INT NOT NULL,
	[TMV_REGION_ID] INT NOT NULL,
	[TMV_REGION] VARCHAR(20) NOT NULL,
    CONSTRAINT [PK_TMV_REGION_ID]
        PRIMARY KEY CLUSTERED (
	        [DataLoadID],
            [TMV_REGION_ID]
        ),
    CONSTRAINT FK_TMV_VALID_REGION__FirstLook_DataLoad
        FOREIGN KEY (
            [DataLoadID]
        )
        REFERENCES Edmunds.FirstLook_DataLoad (
            [DataLoadID]
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE [Edmunds].[TMVU_VEHICLE](
    [DataLoadID] INT NOT NULL,
	[ED_STYLE_ID] INT NOT NULL,
	[Year] INT NOT NULL,
	[MidYear] TINYINT NOT NULL,
	[MAKE] VARCHAR(30) NOT NULL,
	[MODEL] VARCHAR(30) NOT NULL,
	[STYLE_USED] VARCHAR(65) NOT NULL,
	[TMV_CATEGORY_ID] INT NOT NULL,
	[RETAIL_VALUE] INT NOT NULL,
	[PPARTY_VALUE] INT NOT NULL,
	[TRADEIN_VALUE] INT NOT NULL,
    CONSTRAINT [PK_TMVU_VEHICLE]
        PRIMARY KEY CLUSTERED (
	        [DataLoadID],
            [ED_STYLE_ID]
        ),
    CONSTRAINT FK_TMVU_VEHICLE__FirstLook_DataLoad
        FOREIGN KEY (
            [DataLoadID]
        )
        REFERENCES Edmunds.FirstLook_DataLoad (
            [DataLoadID]
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE [Edmunds].[TMVU_VEHICLE_ERRORS](
    [DataLoadID] INT NOT NULL,
	[ED_STYLE_ID] INT NOT NULL,
	[Year] INT NULL,
	[MidYear] TINYINT NULL,
	[MAKE] VARCHAR(30) NULL,
	[MODEL] VARCHAR(30) NULL,
	[STYLE_USED] VARCHAR(65) NULL,
	[TMV_CATEGORY_ID] INT NULL,
	[RETAIL_VALUE] INT NULL,
	[PPARTY_VALUE] INT NULL,
	[TRADEIN_VALUE] INT NULL,
	CONSTRAINT PK_TMVU_VEHICLE_ERRORS
	    PRIMARY KEY CLUSTERED (
	        [DataLoadID],
            [ED_STYLE_ID]
        ),
    CONSTRAINT FK_TMVU_VEHICLE_ERRORS__FirstLook_DataLoad
        FOREIGN KEY (
            [DataLoadID]
        )
        REFERENCES Edmunds.FirstLook_DataLoad (
            [DataLoadID]
        ),
    CONSTRAINT FK_TMVU_VEHICLE_ERRORS__TMVU_VEHICLE
        FOREIGN KEY (
            [DataLoadID],
            [ED_STYLE_ID]
        )
        REFERENCES [Edmunds].[TMVU_VEHICLE] (
            [DataLoadID],
            [ED_STYLE_ID]
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE [Edmunds].[TMVU_CPO_COST](
    [DataLoadID] INT NOT NULL,
	[ED_STYLE_ID] INT NOT NULL,
	[CPO_COST] INT NOT NULL,
	[CPO_PREMIUM_PERCENT] [real] NOT NULL,
    CONSTRAINT [PK_TMVU_CPO_COST]
        PRIMARY KEY CLUSTERED (
	        [DataLoadID],
            [ED_STYLE_ID]
        ),
    CONSTRAINT FK_TMVU_CPO_COST__FirstLook_DataLoad
        FOREIGN KEY (
            [DataLoadID]
        )
        REFERENCES Edmunds.FirstLook_DataLoad (
            [DataLoadID]
        ),
    CONSTRAINT FK_TMVU_CPO_COST__TMVU_VEHICLE
        FOREIGN KEY (
            [DataLoadID],
            [ED_STYLE_ID]
        )
        REFERENCES [Edmunds].[TMVU_VEHICLE] (
            [DataLoadID],
            [ED_STYLE_ID]
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE [Edmunds].[TMVU_CPO_MILEAGE](
    [DataLoadID] INT NOT NULL,
	[AGE] INT NOT NULL,
	[MAKE] VARCHAR(30) NOT NULL,
	[MAX_YEARS] INT NOT NULL,
	[MAX_MILEAGE] INT NOT NULL,
	[INTERVAL] INT NOT NULL,
	[INTERVAL_MIN] INT NOT NULL,
	[INTERVAL_MAX] INT NOT NULL,
	[CPO_MILEAGE_PERCENT] [real] NOT NULL,
    CONSTRAINT [PK_TMVU_CPO_MILEAGE]
        PRIMARY KEY CLUSTERED (
	        [DataLoadID],
            [MAKE],
	        [AGE],
	        [INTERVAL]
        ),
    CONSTRAINT FK_TMVU_CPO_MILEAGE__FirstLook_DataLoad
        FOREIGN KEY (
            [DataLoadID]
        )
        REFERENCES Edmunds.FirstLook_DataLoad (
            [DataLoadID]
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE [Edmunds].[TMVU_MILEAGE](
    [DataLoadID] INT NOT NULL,
	[Year] INT NOT NULL,
	[TMV_CATEGORY_ID] INT NOT NULL,
	[LOW_MILEAGE] INT NOT NULL,
	[HIGH_MILEAGE] INT NOT NULL,
	[ADD_FACTOR_1] [real] NOT NULL,
	[ADD_FACTOR_2] [real] NOT NULL,
	[ADD_FACTOR_3] [real] NOT NULL,
	[ADD_FACTOR_4] [real] NOT NULL,
	[DEDUCT_FACTOR_1] [real] NOT NULL,
	[DEDUCT_FACTOR_2] [real] NOT NULL,
	[DEDUCT_FACTOR_3] [real] NOT NULL,
	[DEDUCT_FACTOR_4] [real] NOT NULL,
	[HIGH_BOUND_1] INT NOT NULL,
	[HIGH_BOUND_2] INT NOT NULL,
	[HIGH_BOUND_3] INT NOT NULL,
	[HIGH_BOUND_4] INT NOT NULL,
	[LOW_BOUND_1] INT NOT NULL,
	[LOW_BOUND_2] INT NOT NULL,
	[LOW_BOUND_3] INT NOT NULL,
	[LOW_BOUND_4] INT NOT NULL,
    CONSTRAINT [PK_TMVU_MILEAGE]
        PRIMARY KEY CLUSTERED (
	        [DataLoadID],
            [TMV_CATEGORY_ID],
	        [Year]
        ),
    CONSTRAINT FK_TMVU_MILEAGE__FirstLook_DataLoad
        FOREIGN KEY (
            [DataLoadID]
        )
        REFERENCES Edmunds.FirstLook_DataLoad (
            [DataLoadID]
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE [Edmunds].[TMVU_OPTIONAL_FEATURE](
    [DataLoadID] INT NOT NULL,
	[ED_STYLE_ID] INT NOT NULL,
	[FEATURE_ID] BIGINT NOT NULL,
	[RETAIL_VALUE_FEATURES] INT NOT NULL,
	[PPARTY_VALUE_FEATURES] INT NOT NULL,
	[TRADEIN_VALUE_FEATURES] INT NOT NULL,
    CONSTRAINT [PK_TMVU_OPTIONAL_FEATURE]
        PRIMARY KEY CLUSTERED (
	        [DataLoadID],
            [ED_STYLE_ID],
	        [FEATURE_ID]
        ),
    CONSTRAINT FK_TMVU_OPTIONAL_FEATURE__FirstLook_DataLoad
        FOREIGN KEY (
            [DataLoadID]
        )
        REFERENCES Edmunds.FirstLook_DataLoad (
            [DataLoadID]
        ),
    CONSTRAINT FK_TMVU_OPTIONAL_FEATURE__TMVU_VEHICLE
        FOREIGN KEY (
            [DataLoadID],
            [ED_STYLE_ID]
        )
        REFERENCES [Edmunds].[TMVU_VEHICLE] (
            [DataLoadID],
            [ED_STYLE_ID]
        ),
    CONSTRAINT FK_TMVU_OPTIONAL_FEATURE__TMVU_VALID_OPTION
        FOREIGN KEY (
            [DataLoadID],
            [FEATURE_ID]
        )
        REFERENCES [Edmunds].[TMVU_VALID_OPTION] (
            [DataLoadID],
            [FEATURE_ID]
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE [Edmunds].[TMVU_OPTIONAL_FEATURE_ERRORS](
    [DataLoadID] INT NOT NULL,
	[ED_STYLE_ID] INT NOT NULL,
	[FEATURE_ID] BIGINT NOT NULL,
	[RETAIL_VALUE_FEATURES] INT NULL,
	[PPARTY_VALUE_FEATURES] INT NULL,
	[TRADEIN_VALUE_FEATURES] INT NULL,
    CONSTRAINT [PK_TMVU_OPTIONAL_FEATURE_ERRORS]
        PRIMARY KEY CLUSTERED (
	        [DataLoadID],
            [ED_STYLE_ID],
	        [FEATURE_ID]
        ),
    CONSTRAINT FK_TMVU_OPTIONAL_FEATURE_ERRORS__FirstLook_DataLoad
        FOREIGN KEY (
            [DataLoadID]
        )
        REFERENCES Edmunds.FirstLook_DataLoad (
            [DataLoadID]
        ),
    CONSTRAINT FK_TMVU_OPTIONAL_FEATURE_ERRORS__TMVU_VEHICLE
        FOREIGN KEY (
            [DataLoadID],
            [ED_STYLE_ID]
        )
        REFERENCES [Edmunds].[TMVU_VEHICLE] (
            [DataLoadID],
            [ED_STYLE_ID]
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE [Edmunds].[TMVU_CONDITION](
    [DataLoadID] INT NOT NULL,
	[TMV_CATEGORY_ID] INT NOT NULL,
	[Year] INT NOT NULL,
	[TMV_CONDITION_ID] INT NOT NULL,
	[RETAIL_PERCENT] [real] NOT NULL,
	[PPARTY_PERCENT] [real] NOT NULL,
	[TRADEIN_PERCENT] [real] NOT NULL,
    CONSTRAINT [PK_TMVU_CONDITION]
        PRIMARY KEY CLUSTERED (
	        [DataLoadID],
            [TMV_CATEGORY_ID],
	        [TMV_CONDITION_ID],
	        [Year]
        ),
    CONSTRAINT FK_TMVU_CONDITION__FirstLook_DataLoad
        FOREIGN KEY (
            [DataLoadID]
        )
        REFERENCES Edmunds.FirstLook_DataLoad (
            [DataLoadID]
        ),
    CONSTRAINT FK_TMVU_CONDITION__TMV_VALID_CONDITION
        FOREIGN KEY (
            [DataLoadID],
            [TMV_CONDITION_ID]
        )
        REFERENCES [Edmunds].[TMV_VALID_CONDITION] (
            [DataLoadID],
            [TMV_CONDITION_ID]
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE [Edmunds].[TMVU_CONDITION_EXCEPTION](
    [DataLoadID] INT NOT NULL,
	[MAKE] VARCHAR(30) NOT NULL,
	[TMV_CATEGORY_ID] INT NOT NULL,
	[TMV_CONDITION_ID] INT NOT NULL,
	[Year] INT NOT NULL,
	[RETAIL_PERCENT] [real] NOT NULL,
	[PPARTY_PERCENT] [real] NOT NULL,
	[TRADEIN_PERCENT] [real] NOT NULL,
    CONSTRAINT [PK_TMVU_CONDITION_EXCEPTION]
        PRIMARY KEY CLUSTERED (
	        [DataLoadID],
            [MAKE],
	        [TMV_CATEGORY_ID],
	        [TMV_CONDITION_ID],
	        [Year]
        ),
    CONSTRAINT FK_TMVU_CONDITION_EXCEPTION__FirstLook_DataLoad
        FOREIGN KEY (
            [DataLoadID]
        )
        REFERENCES Edmunds.FirstLook_DataLoad (
            [DataLoadID]
        ),
    CONSTRAINT FK_TMVU_CONDITION_EXCEPTION__TMV_VALID_CONDITION
        FOREIGN KEY (
            [DataLoadID],
            [TMV_CONDITION_ID]
        )
        REFERENCES [Edmunds].[TMV_VALID_CONDITION] (
            [DataLoadID],
            [TMV_CONDITION_ID]
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE [Edmunds].[TMVU_COLOR](
    [DataLoadID] INT NOT NULL,
	[TMV_CONDITION_ID] INT NOT NULL,
	[TMV_GENERIC_COLOR_ID] INT NOT NULL,
	[TMV_REGION_ID] INT NOT NULL,
	[TMV_CATEGORY_ID] INT NOT NULL,
	[YEAR] INT NOT NULL,
	[COLOR_PERCENT] [real] NOT NULL,
    CONSTRAINT [PK_TMVU_COLOR]
        PRIMARY KEY CLUSTERED (
	        [DataLoadID],
            [TMV_GENERIC_COLOR_ID],
	        [TMV_REGION_ID],
	        [TMV_CONDITION_ID],
	        [TMV_CATEGORY_ID],
	        [YEAR]
        ),
    CONSTRAINT FK_TMVU_COLOR__FirstLook_DataLoad
        FOREIGN KEY (
            [DataLoadID]
        )
        REFERENCES Edmunds.FirstLook_DataLoad (
            [DataLoadID]
        ),
    CONSTRAINT FK_TMVU_COLOR__TMV_VALID_CONDITION
        FOREIGN KEY (
            [DataLoadID],
            [TMV_CONDITION_ID]
        )
        REFERENCES [Edmunds].[TMV_VALID_CONDITION] (
            [DataLoadID],
            [TMV_CONDITION_ID]
        ),
    CONSTRAINT FK_TMVU_COLOR__TMV_VALID_GENERIC_COLOR
        FOREIGN KEY (
            [DataLoadID],
            [TMV_GENERIC_COLOR_ID]
        )
        REFERENCES [Edmunds].[TMV_VALID_GENERIC_COLOR] (
            [DataLoadID],
            [TMV_GENERIC_COLOR_ID]
        ),
    CONSTRAINT FK_TMVU_COLOR__TMV_VALID_REGION
        FOREIGN KEY (
            [DataLoadID],
            [TMV_REGION_ID]
        )
        REFERENCES [Edmunds].[TMV_VALID_REGION] (
            [DataLoadID],
            [TMV_REGION_ID]
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE [Edmunds].[TMVU_COLOR_EXCEPTION](
    [DataLoadID] INT NOT NULL,
	[ED_STYLE_ID] INT NOT NULL,
	[TMV_REGION_ID] INT NOT NULL,
	[TMV_CONDITION_ID] INT NOT NULL,
	[TMV_GENERIC_COLOR_ID] INT NOT NULL,
	[COLOR_PERCENT] [real] NOT NULL,
    CONSTRAINT [PK_TMVU_COLOR_EXCEPTION]
        PRIMARY KEY CLUSTERED (
	        [DataLoadID],
            [ED_STYLE_ID],
	        [TMV_GENERIC_COLOR_ID],
	        [TMV_REGION_ID],
	        [TMV_CONDITION_ID]
        ),
    CONSTRAINT FK_TMVU_COLOR_EXCEPTION__FirstLook_DataLoad
        FOREIGN KEY (
            [DataLoadID]
        )
        REFERENCES Edmunds.FirstLook_DataLoad (
            [DataLoadID]
        ),
    CONSTRAINT FK_TMVU_COLOR_EXCEPTION__TMVU_VEHICLE
        FOREIGN KEY (
            [DataLoadID],
            [ED_STYLE_ID]
        )
        REFERENCES [Edmunds].[TMVU_VEHICLE] (
            [DataLoadID],
            [ED_STYLE_ID]
        ),
    CONSTRAINT FK_TMVU_COLOR_EXCEPTION__TMV_VALID_CONDITION
        FOREIGN KEY (
            [DataLoadID],
            [TMV_CONDITION_ID]
        )
        REFERENCES [Edmunds].[TMV_VALID_CONDITION] (
            [DataLoadID],
            [TMV_CONDITION_ID]
        ),
    CONSTRAINT FK_TMVU_COLOR_EXCEPTION__TMV_VALID_GENERIC_COLOR
        FOREIGN KEY (
            [DataLoadID],
            [TMV_GENERIC_COLOR_ID]
        )
        REFERENCES [Edmunds].[TMV_VALID_GENERIC_COLOR] (
            [DataLoadID],
            [TMV_GENERIC_COLOR_ID]
        ),
    CONSTRAINT FK_TMVU_COLOR_EXCEPTION__TMV_VALID_REGION
        FOREIGN KEY (
            [DataLoadID],
            [TMV_REGION_ID]
        )
        REFERENCES [Edmunds].[TMV_VALID_REGION] (
            [DataLoadID],
            [TMV_REGION_ID]
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE [Edmunds].[TMVU_REGION](
    [DataLoadID] INT NOT NULL,
	[TMV_REGION_ID] INT NOT NULL,
	[TMV_CATEGORY_ID] INT NOT NULL,
	[Year] INT NOT NULL,
	[REGION_PERCENT] [real] NOT NULL,
    CONSTRAINT [PK_TMVU_Region]
        PRIMARY KEY CLUSTERED (
	        [DataLoadID],
            [TMV_REGION_ID],
	        [TMV_CATEGORY_ID],
	        [Year]
        ),
    CONSTRAINT FK_TMVU_REGION__FirstLook_DataLoad
        FOREIGN KEY (
            [DataLoadID]
        )
        REFERENCES Edmunds.FirstLook_DataLoad (
            [DataLoadID]
        ),
    CONSTRAINT FK_TMVU_REGION__TMV_VALID_REGION
        FOREIGN KEY (
            [DataLoadID],
            [TMV_REGION_ID]
        )
        REFERENCES [Edmunds].[TMV_VALID_REGION] (
            [DataLoadID],
            [TMV_REGION_ID]
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE [Edmunds].[TMVU_REGION_EXCEPTION](
    [DataLoadID] INT NOT NULL,
	[ED_STYLE_ID] INT NOT NULL,
	[TMV_REGION_ID] INT NOT NULL,
	[REGION_PERCENT] [real] NOT NULL,
    CONSTRAINT [PK_TMVU_REGION_EXCEPTION]
        PRIMARY KEY CLUSTERED (
	        [DataLoadID],
            [ED_STYLE_ID],
	        [TMV_REGION_ID]
        ),
    CONSTRAINT FK_TMVU_REGION_EXCEPTION__FirstLook_DataLoad
        FOREIGN KEY (
            [DataLoadID]
        )
        REFERENCES Edmunds.FirstLook_DataLoad (
            [DataLoadID]
        ),
    CONSTRAINT FK_TMVU_REGION_EXCEPTION__TMVU_VEHICLE
        FOREIGN KEY (
            [DataLoadID],
            [ED_STYLE_ID]
        )
        REFERENCES [Edmunds].[TMVU_VEHICLE] (
            [DataLoadID],
            [ED_STYLE_ID]
        ),
    CONSTRAINT FK_TMVU_REGION_EXCEPTION__TMV_VALID_REGION
        FOREIGN KEY (
            [DataLoadID],
            [TMV_REGION_ID]
        )
        REFERENCES [Edmunds].[TMV_VALID_REGION] (
            [DataLoadID],
            [TMV_REGION_ID]
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE [Edmunds].[TMVU_STATE_REGION](
    [DataLoadID] INT NOT NULL,
	[STATE_CODE] VARCHAR(4) NOT NULL,
	[TMV_REGION_ID] INT NOT NULL,
    CONSTRAINT [PK_TMVU_STATE_REGION]
        PRIMARY KEY CLUSTERED (
	        [DataLoadID],
            [STATE_CODE]
        ),
    CONSTRAINT FK_TMVU_STATE_REGION__FirstLook_DataLoad
        FOREIGN KEY (
            [DataLoadID]
        )
        REFERENCES Edmunds.FirstLook_DataLoad (
            [DataLoadID]
        ),
    CONSTRAINT FK_TMVU_STATE_REGION__TMV_VALID_REGION
        FOREIGN KEY (
            [DataLoadID],
            [TMV_REGION_ID]
        )
        REFERENCES [Edmunds].[TMV_VALID_REGION] (
            [DataLoadID],
            [TMV_REGION_ID]
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE [Edmunds].[FirstLook_COLOR_U](
    [DataLoadID] INT NOT NULL,
	[COLOR_ID] INT NOT NULL,
	[ED_STYLE_ID] INT NOT NULL,
	[BASIC_COLOR_NAME] NVARCHAR(255) NULL,
	[MFGR_COLOR_NAME] NVARCHAR(255) NULL,
	[MFGR_COLOR_CODE] NVARCHAR(255) NULL,
	[INTERIOR_EXTERIOR_FLAG] NVARCHAR(255) NULL,
	[R_CODE] NVARCHAR(255) NULL,
	[G_CODE] NVARCHAR(255) NULL,
	[B_CODE] NVARCHAR(255) NULL,
	[C_CODE] NVARCHAR(255) NULL,
	[M_CODE] NVARCHAR(255) NULL,
	[Y_CODE] NVARCHAR(255) NULL,
	[K_CODE] NVARCHAR(255) NULL,
	CONSTRAINT UK_Edmunds_FirstLook_COLOR_U
	    PRIMARY KEY CLUSTERED (
	        [DataLoadID],
            [ED_STYLE_ID],
	        [COLOR_ID]
        ),
    CONSTRAINT FK_Edmunds_FirstLook_COLOR_U__FirstLook_DataLoad
        FOREIGN KEY (
            [DataLoadID]
        )
        REFERENCES Edmunds.FirstLook_DataLoad (
            [DataLoadID]
        ),
    CONSTRAINT FK_Edmunds_FirstLook_COLOR_U__TMVU_VEHICLE
        FOREIGN KEY (
            [DataLoadID],
            [ED_STYLE_ID]
        )
        REFERENCES [Edmunds].[TMVU_VEHICLE] (
            [DataLoadID],
            [ED_STYLE_ID]
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE [Edmunds].[FirstLook_SQUISH_VIN_V2](
    [DataLoadID] INT NOT NULL,
    [SQUISH_VIN] NVARCHAR(255) NOT NULL,
    [ED_STYLE_ID] INT NOT NULL,
    [USED_ED_STYLE_ID] NVARCHAR(255) NULL,
    [ED_MODEL_ID] BIGINT NULL,
    [YEAR] INT NULL,
    [MAKE] NVARCHAR(255) NULL,
    [MODEL] NVARCHAR(255) NULL,
    [STYLE] NVARCHAR(255) NULL,
    [MFR_STYLE_CODE] NVARCHAR(255) NULL,
    [DOORS] TINYINT NULL,
    [BODY_TYPE] NVARCHAR(255) NULL,
    [DRIVE_TYPE_CODE] NVARCHAR(255) NULL,
    [STANDARD_FLAG] TINYINT NULL,
    [ENGINE_BLOCK_CONFIGURATION] NVARCHAR(255) NULL,
    [CYLINDER_QTY] TINYINT NULL,
    [ENGINE_DISPLACEMENT] DECIMAL(29, 10) NULL,
    [ENGINE_DISPLACEMENT_UOM] NVARCHAR(255) NULL,
    [CAM_TYPE] NVARCHAR(255) NULL,
    [FUEL_INDUCTION] NVARCHAR(255) NULL,
    [VALVES_CYLINDER] TINYINT NULL,
    [ASPIRATION] NVARCHAR(255) NULL,
    [FUEL_TYPE] NVARCHAR(255) NULL,
    [TRAN_TYPE] NVARCHAR(255) NULL,
    [TRAN_SPEED] NVARCHAR(255) NULL,
    [RESTRAINT_SYSTEM] NVARCHAR(255) NULL,
    [GVWR] NVARCHAR(255) NULL,
    [PLANT] NVARCHAR(255) NULL,
    CONSTRAINT PK_Edmunds_FirstLook_SQUISH_VIN_V2
        PRIMARY KEY CLUSTERED (
            [DataLoadID],
            [ED_STYLE_ID],
            [SQUISH_VIN]
        ),
    CONSTRAINT FK_Edmunds_FirstLook_SQUISH_VIN_V2__FirstLook_DataLoad
        FOREIGN KEY (
            [DataLoadID]
        )
        REFERENCES Edmunds.FirstLook_DataLoad (
            [DataLoadID]
        ),
    CONSTRAINT FK_Edmunds_FirstLook_SQUISH_VIN_V2__TMVU_VEHICLE
        FOREIGN KEY (
            [DataLoadID],
            [ED_STYLE_ID]
        )
        REFERENCES [Edmunds].[TMVU_VEHICLE] (
            [DataLoadID],
            [ED_STYLE_ID]
        )
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

-- vehicle configuration

CREATE TABLE Edmunds.VehicleConfiguration (
    VehicleConfigurationID  INT IDENTITY(1,1)   NOT NULL,
    VehicleID               INT                 NOT NULL,
    CONSTRAINT PK_Edmunds_VehicleConfiguration
        PRIMARY KEY CLUSTERED (VehicleConfigurationID),
    CONSTRAINT FK_Edmunds_VehicleConfiguration__Vehicle
        FOREIGN KEY (VehicleID)
        REFERENCES Reference.Vehicle (VehicleID)
)
ON [DATA]
GO

CREATE TABLE Edmunds.VehicleConfiguration_History (
    VehicleConfigurationHistoryID   INT IDENTITY(1,1)   NOT NULL,
    VehicleConfigurationID          INT                 NOT NULL,
    DataLoadID                      INT                 NOT NULL,
    StyleID                         INT                 NOT NULL,
    StateCode                       VARCHAR(4)          NOT NULL,
    Mileage                         INT                 NULL,
    ChangeTypeBitFlags              INT                 NOT NULL,
    AuditRowID                      INT                 NOT NULL,
    ChangeTypeBit_DataLoad          AS (CONVERT(BIT, ChangeTypeBitFlags&POWER(2,0))),
    ChangeTypeBit_State             AS (CONVERT(BIT, ChangeTypeBitFlags&POWER(2,1))),
    ChangeTypeBit_StyleId           AS (CONVERT(BIT, ChangeTypeBitFlags&POWER(2,2))),
    ChangeTypeBit_Mileage           AS (CONVERT(BIT, ChangeTypeBitFlags&POWER(2,3))),
    ChangeTypeBit_OptionActions     AS (CONVERT(BIT, ChangeTypeBitFlags&POWER(2,4))),
    ChangeTypeBit_OptionStates      AS (CONVERT(BIT, ChangeTypeBitFlags&POWER(2,5))),
    CONSTRAINT PK_Edmunds_VehicleConfiguration_History
        PRIMARY KEY CLUSTERED (DataLoadID, VehicleConfigurationHistoryID),
    CONSTRAINT FK_Edmunds_VehicleConfiguration_History__VehicleConfiguration
        FOREIGN KEY (VehicleConfigurationID)
        REFERENCES Edmunds.VehicleConfiguration (VehicleConfigurationID),
    CONSTRAINT FK_Edmunds_VehicleConfiguration_History__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES Edmunds.FirstLook_DataLoad (DataLoadID),
    CONSTRAINT FK_Edmunds_VehicleConfiguration_History__Vehicle
        FOREIGN KEY (DataLoadID, StyleID)
        REFERENCES Edmunds.TMVU_VEHICLE (DataLoadID, ED_STYLE_ID),
    CONSTRAINT FK_Edmunds_VehicleConfiguration_History__State
        FOREIGN KEY (DataLoadID, StateCode)
        REFERENCES Edmunds.TMVU_STATE_REGION (DataLoadID, STATE_CODE),
    CONSTRAINT FK_Edmunds_VehicleConfiguration_History__AuditRow
        FOREIGN KEY (AuditRowID)
        REFERENCES Audit.AuditRow (AuditRowID),
    CONSTRAINT CK_Edmunds_VehicleConfiguration_History__Mileage
        CHECK (Mileage IS NULL OR Mileage >= 0)
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE Edmunds.OptionActionType (
    OptionActionTypeID      TINYINT     NOT NULL,
    Name                    VARCHAR(32) NOT NULL,
    CONSTRAINT PK_Edmunds_OptionActionType
        PRIMARY KEY CLUSTERED (OptionActionTypeID),
    CONSTRAINT UK_Edmunds_OptionActionType
        UNIQUE NONCLUSTERED (Name),
    CONSTRAINT CK_Edmunds_OptionActionType__Name
        CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

INSERT INTO Edmunds.OptionActionType ( OptionActionTypeID, Name ) VALUES  ( 1, 'Add' )
INSERT INTO Edmunds.OptionActionType ( OptionActionTypeID, Name ) VALUES  ( 2, 'Remove' )
GO

CREATE TABLE Edmunds.OptionAction (
    VehicleConfigurationHistoryID   INT     NOT NULL,
    OptionActionTypeID              TINYINT NOT NULL,
    OptionActionIndex               TINYINT NOT NULL,
    DataLoadID                      INT     NOT NULL,
    OptionID                        BIGINT  NOT NULL,
    CONSTRAINT PK_Edmunds_OptionAction
        PRIMARY KEY CLUSTERED (DataLoadID, VehicleConfigurationHistoryID, OptionID),
    CONSTRAINT UK_Edmunds_OptionAction
        UNIQUE NONCLUSTERED (DataLoadID, VehicleConfigurationHistoryID, OptionActionIndex),
    CONSTRAINT FK_Edmunds_OptionAction__VehicleConfiguration_History
        FOREIGN KEY (DataLoadID, VehicleConfigurationHistoryID)
        REFERENCES Edmunds.VehicleConfiguration_History (DataLoadID, VehicleConfigurationHistoryID),
    CONSTRAINT FK_Edmunds_OptionAction__OptionActionType
        FOREIGN KEY (OptionActionTypeID)
        REFERENCES Edmunds.OptionActionType (OptionActionTypeID),
    CONSTRAINT FK_Edmunds_OptionAction__TMVU_VALID_OPTION
        FOREIGN KEY (DataLoadID, OptionID)
        REFERENCES Edmunds.TMVU_VALID_OPTION (DataLoadID, FEATURE_ID),
    CONSTRAINT FK_Edmunds_OptionAction__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES Edmunds.FirstLook_DataLoad (DataLoadID),
    CONSTRAINT CK_Edmunds_OptionAction__OptionActionType
        CHECK (OptionActionIndex >= 0)
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE Edmunds.OptionState (
    VehicleConfigurationHistoryID   INT     NOT NULL,
    DataLoadID                      INT     NOT NULL,
    OptionID                        BIGINT  NOT NULL,
    Selected                        BIT     NOT NULL,
    CONSTRAINT PK_Edmunds_OptionState
        PRIMARY KEY CLUSTERED (DataLoadID, VehicleConfigurationHistoryID, OptionID),
    CONSTRAINT FK_Edmunds_OptionState__VehicleConfiguration_History
        FOREIGN KEY (DataLoadID, VehicleConfigurationHistoryID)
        REFERENCES Edmunds.VehicleConfiguration_History (DataLoadID, VehicleConfigurationHistoryID),
    CONSTRAINT FK_Edmunds_OptionState__TMVU_VALID_OPTION
        FOREIGN KEY (DataLoadID, OptionID)
        REFERENCES Edmunds.TMVU_VALID_OPTION (DataLoadID, FEATURE_ID),
    CONSTRAINT FK_Edmunds_OptionState__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES Edmunds.FirstLook_DataLoad (DataLoadID)
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

-- price matrix

CREATE TABLE Edmunds.Valuation (
    ValuationID TINYINT     NOT NULL,
    Name        VARCHAR(32) NOT NULL,
    CONSTRAINT PK_Edmunds_Valuation
        PRIMARY KEY CLUSTERED (ValuationID),
    CONSTRAINT UK_Edmunds_Valuation
        UNIQUE NONCLUSTERED (Name),
    CONSTRAINT CK_Edmunds_Valuation_Name
        CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

INSERT INTO Edmunds.Valuation ( ValuationID, Name ) VALUES ( 1, 'Base' )
INSERT INTO Edmunds.Valuation ( ValuationID, Name ) VALUES ( 2, 'Option' )
INSERT INTO Edmunds.Valuation ( ValuationID, Name ) VALUES ( 3, 'Mileage' )
INSERT INTO Edmunds.Valuation ( ValuationID, Name ) VALUES ( 4, 'Color' )
INSERT INTO Edmunds.Valuation ( ValuationID, Name ) VALUES ( 5, 'Region' )
INSERT INTO Edmunds.Valuation ( ValuationID, Name ) VALUES ( 6, 'Condition' )
INSERT INTO Edmunds.Valuation ( ValuationID, Name ) VALUES ( 7, 'Final' )
GO

CREATE TABLE Edmunds.PriceType (
    PriceTypeID TINYINT     NOT NULL,
    Name        VARCHAR(32) NOT NULL,
    CONSTRAINT PK_Edmunds_PriceType
        PRIMARY KEY CLUSTERED (PriceTypeID),
    CONSTRAINT UK_Edmunds_PriceType
        UNIQUE NONCLUSTERED (Name),
    CONSTRAINT CK_Edmunds_PriceType_Name
        CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

INSERT INTO Edmunds.PriceType ( PriceTypeID, Name ) VALUES ( 1, 'Retail' )
INSERT INTO Edmunds.PriceType ( PriceTypeID, Name ) VALUES ( 2, 'Private Party' )
INSERT INTO Edmunds.PriceType ( PriceTypeID, Name ) VALUES ( 3, 'Trade In' )
GO

CREATE TABLE Edmunds.Matrix (
    MatrixID    INT IDENTITY(1,1)   NOT NULL,
    DataLoadID  INT                 NOT NULL,
    CONSTRAINT PK_Edmunds_Matrix
        PRIMARY KEY NONCLUSTERED (DataLoadID, MatrixID),
    CONSTRAINT FK_Edmunds_Matrix__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES Edmunds.FirstLook_DataLoad (DataLoadID)
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE Edmunds.MatrixCell (
    MatrixID    INT         NOT NULL,
    DataLoadID  INT         NOT NULL,
    PriceTypeID TINYINT     NOT NULL,
    ConditionID INT         NOT NULL,
    ValuationID TINYINT     NOT NULL,
    Visible     BIT         NOT NULL,
    Value       SMALLMONEY  NULL,
    CONSTRAINT PK_Edmunds_MatrixCell
        PRIMARY KEY CLUSTERED (DataLoadID, MatrixID, PriceTypeID, ConditionID, ValuationID),
    CONSTRAINT FK_Edmunds_MatrixCell__Matrix
        FOREIGN KEY (DataLoadID, MatrixID)
        REFERENCES Edmunds.Matrix (DataloadID, MatrixID),
    CONSTRAINT FK_Edmunds_MatrixCell__PriceType
        FOREIGN KEY (PriceTypeId)
        REFERENCES Edmunds.PriceType (PriceTypeId),
    CONSTRAINT FK_Edmunds_MatrixCell__Valuation
        FOREIGN KEY (ValuationID)
        REFERENCES Edmunds.Valuation (ValuationID),
    CONSTRAINT FK_Edmunds_MatrixCell__TMV_VALID_CONDITION
        FOREIGN KEY (DataLoadID, ConditionID)
        REFERENCES Edmunds.TMV_VALID_CONDITION (DataLoadID, TMV_CONDITION_ID),
    CONSTRAINT FK_Edmunds_MatrixCell__DataLoad
        FOREIGN KEY (DataLoadID)
        REFERENCES Edmunds.FirstLook_DataLoad (DataLoadID)
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE Edmunds.VehicleConfiguration_History_Matrix (
    VehicleConfigurationHistoryID   INT NOT NULL,
    MatrixID                        INT NOT NULL,
    DataLoadID                      INT NOT NULL,
    CONSTRAINT PK_Edmunds_VehicleConfigurationHistory_Matrix
        PRIMARY KEY CLUSTERED (DataLoadID, VehicleConfigurationHistoryID),
    CONSTRAINT UK_Edmunds_VehicleConfigurationHistory_Matrix__Matrix
        UNIQUE NONCLUSTERED (DataLoadID, MatrixID)
)
ON PS_Edmunds__DataLoad (DataLoadID)
GO

CREATE TABLE Client.Vehicle_Edmunds (
    VehicleConfigurationID  INT NOT NULL,
    VehicleID               INT NOT NULL,
    CONSTRAINT PK_Client_Vehicle_Edmunds
        PRIMARY KEY CLUSTERED (VehicleConfigurationID, VehicleID),
    CONSTRAINT FK_Client_Vehicle_Edmunds__VehicleConfiguration
        FOREIGN KEY (VehicleConfigurationID)
        REFERENCES Edmunds.VehicleConfiguration (VehicleConfigurationID),
    CONSTRAINT FK_Client_Vehicle_Edmunds__Vehicle
        FOREIGN KEY (VehicleID)
        REFERENCES Client.Vehicle (VehicleID)
)
ON [DATA]
GO
