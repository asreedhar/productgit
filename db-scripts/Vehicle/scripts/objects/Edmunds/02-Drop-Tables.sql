
-- client tables

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Client].[Vehicle_Edmunds]') AND type in (N'U'))
DROP TABLE [Client].[Vehicle_Edmunds]
GO

-- configuration + matrix tables

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[VehicleConfiguration_History_Matrix]') AND type in (N'U'))
DROP TABLE [Edmunds].[VehicleConfiguration_History_Matrix]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[MatrixCell]') AND type in (N'U'))
DROP TABLE [Edmunds].[MatrixCell]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[Matrix]') AND type in (N'U'))
DROP TABLE [Edmunds].[Matrix]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[PriceType]') AND type in (N'U'))
DROP TABLE [Edmunds].[PriceType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[Valuation]') AND type in (N'U'))
DROP TABLE [Edmunds].[Valuation]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[OptionState]') AND type in (N'U'))
DROP TABLE [Edmunds].[OptionState]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[OptionAction]') AND type in (N'U'))
DROP TABLE [Edmunds].[OptionAction]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[OptionActionType]') AND type in (N'U'))
DROP TABLE [Edmunds].[OptionActionType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[VehicleConfiguration_History]') AND type in (N'U'))
DROP TABLE [Edmunds].[VehicleConfiguration_History]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[VehicleConfiguration]') AND type in (N'U'))
DROP TABLE [Edmunds].[VehicleConfiguration]
GO

-- their tables

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[FirstLook_SQUISH_VIN_V2]') AND type in (N'U'))
DROP TABLE [Edmunds].[FirstLook_SQUISH_VIN_V2]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[FirstLook_COLOR_U]') AND type in (N'U'))
DROP TABLE [Edmunds].[FirstLook_COLOR_U]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[TMVU_STATE_REGION]') AND type in (N'U'))
DROP TABLE [Edmunds].[TMVU_STATE_REGION]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[TMVU_REGION_EXCEPTION]') AND type in (N'U'))
DROP TABLE [Edmunds].[TMVU_REGION_EXCEPTION]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[TMVU_REGION]') AND type in (N'U'))
DROP TABLE [Edmunds].[TMVU_REGION]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[TMVU_COLOR_EXCEPTION]') AND type in (N'U'))
DROP TABLE [Edmunds].[TMVU_COLOR_EXCEPTION]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[TMVU_COLOR]') AND type in (N'U'))
DROP TABLE [Edmunds].[TMVU_COLOR]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[TMVU_CONDITION_EXCEPTION]') AND type in (N'U'))
DROP TABLE [Edmunds].[TMVU_CONDITION_EXCEPTION]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[TMVU_CONDITION]') AND type in (N'U'))
DROP TABLE [Edmunds].[TMVU_CONDITION]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[TMVU_OPTIONAL_FEATURE_ERRORS]') AND type in (N'U'))
DROP TABLE [Edmunds].[TMVU_OPTIONAL_FEATURE_ERRORS]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[TMVU_OPTIONAL_FEATURE]') AND type in (N'U'))
DROP TABLE [Edmunds].[TMVU_OPTIONAL_FEATURE]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[TMVU_MILEAGE]') AND type in (N'U'))
DROP TABLE [Edmunds].[TMVU_MILEAGE]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[TMVU_CPO_MILEAGE]') AND type in (N'U'))
DROP TABLE [Edmunds].[TMVU_CPO_MILEAGE]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[TMVU_CPO_COST]') AND type in (N'U'))
DROP TABLE [Edmunds].[TMVU_CPO_COST]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[TMVU_VEHICLE_ERRORS]') AND type in (N'U'))
DROP TABLE [Edmunds].[TMVU_VEHICLE_ERRORS]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[TMVU_VEHICLE]') AND type in (N'U'))
DROP TABLE [Edmunds].[TMVU_VEHICLE]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[TMV_VALID_REGION]') AND type in (N'U'))
DROP TABLE [Edmunds].[TMV_VALID_REGION]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[TMVU_VALID_OPTION]') AND type in (N'U'))
DROP TABLE [Edmunds].[TMVU_VALID_OPTION]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[TMV_VALID_GENERIC_COLOR]') AND type in (N'U'))
DROP TABLE [Edmunds].[TMV_VALID_GENERIC_COLOR]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[TMV_VALID_CONDITION]') AND type in (N'U'))
DROP TABLE [Edmunds].[TMV_VALID_CONDITION]
GO

-- our partition

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[FirstLook_DataLoad]') AND type in (N'U'))
DROP TABLE [Edmunds].[FirstLook_DataLoad]
GO
