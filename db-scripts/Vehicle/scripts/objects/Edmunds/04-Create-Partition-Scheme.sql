
------------------------------------------------------------------------------------------------
-- Magic Database Settings
------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

------------------------------------------------------------------------------------------------
-- Partition Function
------------------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.partition_functions WHERE name = 'PF_Edmunds__DataLoad')

	CREATE PARTITION FUNCTION PF_Edmunds__DataLoad(int)
	    AS RANGE LEFT FOR VALUES (
	        2011060100, -- 2011Q2
	        2011090100) -- 2011Q3

GO

------------------------------------------------------------------------------------------------
-- Partition Scheme
------------------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.partition_schemes WHERE name = 'PS_Edmunds__DataLoad')
	
 	CREATE PARTITION SCHEME PS_Edmunds__DataLoad
		AS PARTITION PF_Edmunds__DataLoad TO (
            [DATA_Edmunds_FG1],
            [DATA_Edmunds_FG2],
            [PRIMARY])

GO
