
CREATE TABLE Audit.Audit (
        AuditID         INT IDENTITY(1,1) NOT NULL,
        AuditDate       DATETIME NOT NULL,
        UserID          INT NOT NULL,
        CONSTRAINT PK_Audit_Audit
                PRIMARY KEY CLUSTERED (
                        AuditID
                )
                ON [IDX],
        CONSTRAINT FK_Audit_Audit__AuditUser
                FOREIGN KEY (
                        UserID
                )
                REFERENCES Client.[User] (
                        UserID
                ),
        CONSTRAINT CK_Audit_Audit__AuditDate
                CHECK (
                        AuditDate BETWEEN '2010-11-01' AND '2038-01-01'
                )
)
ON [DATA]
GO

CREATE TABLE Audit.AuditRow (
        AuditRowID      INT IDENTITY(1,1) NOT NULL,
        AuditID         INT NOT NULL,
        ValidFrom       DATETIME NOT NULL,
        ValidUpTo       DATETIME NOT NULL,
        WasInsert       BIT NOT NULL,
        WasUpdate       BIT NOT NULL,
        WasDelete       BIT NOT NULL,
        CONSTRAINT PK_Audit_AuditRow
                PRIMARY KEY CLUSTERED (
                        AuditRowID
                )
                ON [IDX],
        CONSTRAINT FK_Audit_AuditRow__Audit
                FOREIGN KEY (
                        AuditID
                )
                REFERENCES Audit.Audit (
                        AuditID
                ),
        CONSTRAINT CK_Audit_AuditRow__Valid
                CHECK (
                        ValidFrom <= ValidUpTo
                ),
        CONSTRAINT CK_Audit_AuditRow__Was
                CHECK (
                        (WasInsert = 1 AND WasUpdate = 0 AND WasDelete = 0) OR
                        (WasInsert = 0 AND WasUpdate = 1 AND WasDelete = 0) OR
                        (WasInsert = 0 AND WasUpdate = 0 AND WasDelete = 1)
                )
)
ON [DATA]
GO

CREATE TABLE Audit.Association (
        AssociationID   INT IDENTITY (1,1) NOT NULL,
        CONSTRAINT PK_Audit_Association
                PRIMARY KEY (AssociationID)
)
ON [DATA]
GO

CREATE TABLE Audit.Association_History (
        AssociationID   INT NOT NULL,
        AuditRowID      INT NOT NULL,
        CONSTRAINT PK_Audit_Association_History
                PRIMARY KEY (AssociationID,AuditRowID),
        CONSTRAINT FK_Audit_Association_History__Association
                FOREIGN KEY (AssociationID)
                REFERENCES Audit.Association (AssociationID),
        CONSTRAINT FK_Audit_Association_History__AuditRow
                FOREIGN KEY (AuditRowID)
                REFERENCES Audit.AuditRow (AuditRowID)
)
ON [DATA]
GO
