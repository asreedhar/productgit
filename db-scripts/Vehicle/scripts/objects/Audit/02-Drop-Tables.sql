
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Audit].[Association_History]') AND type in (N'U'))
DROP TABLE [Audit].[Association_History]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Audit].[Association]') AND type in (N'U'))
DROP TABLE [Audit].[Association]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Audit].[AuditRow]') AND type in (N'U'))
DROP TABLE [Audit].[AuditRow]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Audit].[Audit]') AND type in (N'U'))
DROP TABLE [Audit].[Audit]
GO
