
-- LICENSE MODELS

CREATE TABLE License.LicenseModel 
(
        LicenseModelID  TINYINT NOT NULL,
        Name            VARCHAR(20) NOT NULL,
        
        CONSTRAINT PK_License_LicenseModel
        PRIMARY KEY CLUSTERED (LicenseModelID),
                
        CONSTRAINT UK_License_LicenseModel__Name
        UNIQUE (Name),
        
        CONSTRAINT CK_License_LicenseModel__Name
        CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

INSERT INTO License.LicenseModel (LicenseModelID, Name) VALUES (1, 'Site')
INSERT INTO License.LicenseModel (LicenseModelID, Name) VALUES (2, 'Floating')
INSERT INTO License.LicenseModel (LicenseModelID, Name) VALUES (3, 'Named')
GO

-- DEVICE TYPES

CREATE TABLE License.DeviceType
(
    DeviceTypeID    TINYINT NOT NULL,
    Name            VARCHAR(20) NOT NULL,
    
    CONSTRAINT PK_License_DeviceType
    PRIMARY KEY CLUSTERED (DeviceTypeID),
    
    CONSTRAINT UK_License_DeviceType__Name
    UNIQUE (Name),
    
    CONSTRAINT CK_License_DeviceType__Name
    CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

INSERT INTO License.DeviceType (DeviceTypeID, Name) VALUES (1, 'Desktop')
INSERT INTO License.DeviceType (DeviceTypeID, Name) VALUES (2, 'Mobile')
GO

-- PRODUCT

CREATE TABLE License.Product
(
    ProductID   SMALLINT NOT NULL,
    Name        VARCHAR(100) NOT NULL,
    
    CONSTRAINT PK_License_Product
    PRIMARY KEY CLUSTERED (ProductID),

    CONSTRAINT PK_License_Product__Name
    UNIQUE (Name),
    
    CONSTRAINT CK_License_Product__Name
    CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

INSERT INTO License.Product (ProductID, Name) VALUES (1, 'BlackBook')
INSERT INTO License.Product (ProductID, Name) VALUES (2, 'Edmunds')
INSERT INTO License.Product (ProductID, Name) VALUES (3, 'Galves')
INSERT INTO License.Product (ProductID, Name) VALUES (4, 'Kelley Blue Book')
INSERT INTO License.Product (ProductID, Name) VALUES (5, 'Manheim')
INSERT INTO License.Product (ProductID, Name) VALUES (6, 'NAAA')
INSERT INTO License.Product (ProductID, Name) VALUES (7, 'NADA')
GO

-- LICENSE

CREATE TABLE License.License
(
    LicenseID       INT IDENTITY(1,1) NOT NULL,
    Handle          UNIQUEIDENTIFIER NOT NULL CONSTRAINT DF_License_License__Handle DEFAULT NEWID(),
    LicenseModelID  TINYINT NOT NULL,
    ProductID       SMALLINT NOT NULL,
    DeviceTypeID    TINYINT NOT NULL,
    Uses            INT NOT NULL,
    AuditRowID      INT NOT NULL,
    
    CONSTRAINT PK_License_License
    PRIMARY KEY CLUSTERED (LicenseID),
    
    CONSTRAINT FK_License_License__LicenseModel
    FOREIGN KEY (LicenseModelID)
    REFERENCES License.LicenseModel (LicenseModelID),
    
    CONSTRAINT FK_License_License__Product
    FOREIGN KEY (ProductID)
    REFERENCES License.Product (ProductID),
    
    CONSTRAINT FK_License_License__DeviceType
    FOREIGN KEY (DeviceTypeID)
    REFERENCES License.DeviceType (DeviceTypeID),
    
    CONSTRAINT FK_License_License__AuditRow
    FOREIGN KEY (AuditRowID)
    REFERENCES Audit.AuditRow (AuditRowID),
    
    CONSTRAINT CK_License_License__Uses
    CHECK (Uses >= 0)
)
ON [DATA]
GO    
    
CREATE TABLE License.Client_License
(
    ClientID        INT NOT NULL,
    LicenseID       INT NOT NULL,
    AssociationID   INT NOT NULL,
    
    CONSTRAINT PK_License_Client_License
    PRIMARY KEY CLUSTERED (ClientID, LicenseID),
    
    CONSTRAINT FK_License_Client_License__Client
    FOREIGN KEY (ClientID)
    REFERENCES Client.Client (ClientID),
    
    CONSTRAINT FK_License_Client_License__License
    FOREIGN KEY (LicenseID)
    REFERENCES License.License (LicenseID),
    
    CONSTRAINT FK_License_Client_License__Association
    FOREIGN KEY (AssociationID)
    REFERENCES Audit.Association (AssociationID)
)    
ON [DATA]
GO

-- LICENSE PARAMETERS

CREATE TABLE License.LicenseParameterType
(
    LicenseParameterTypeID  TINYINT NOT NULL,
    Name                    VARCHAR(50) NOT NULL,
    
    CONSTRAINT PK_License_LicenseParameterType
    PRIMARY KEY CLUSTERED (LicenseParameterTypeID),
    
    CONSTRAINT UK_License_LicenseParameterType
    UNIQUE (Name),
    
    CONSTRAINT CK_License_LicenseParameterType
    CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

INSERT INTO License.LicenseParameterType (LicenseParameterTypeID, Name) VALUES (1, 'Seats')
INSERT INTO License.LicenseParameterType (LicenseParameterTypeID, Name) VALUES (2, 'Usage Limit')
INSERT INTO License.LicenseParameterType (LicenseParameterTypeID, Name) VALUES (3, 'Expiration Date')
GO
    
CREATE TABLE License.LicenseParameter
(
    LicenseParameterID      INT IDENTITY(1,1) NOT NULL,
    LicenseParameterTypeID  TINYINT NOT NULL,
    LicenseID               INT NOT NULL,
    
    CONSTRAINT PK_License_LicenseParameter
    PRIMARY KEY CLUSTERED (LicenseParameterID),
    
    CONSTRAINT FK_License_LicenseParameter__LicenseParameterType
    FOREIGN KEY (LicenseParameterTypeID)
    REFERENCES License.LicenseParameterType (LicenseParameterTypeID),
    
    CONSTRAINT FK_License_LicenseParameter__License
    FOREIGN KEY (LicenseID)
    REFERENCES License.License (LicenseID)
)    
ON [DATA]
GO
    
CREATE TABLE License.Seats
(
    LicenseParameterID  INT NOT NULL,
    AuditRowID          INT NOT NULL,
    Quantity            SMALLINT NOT NULL,
    
    CONSTRAINT PK_License_Seats
    PRIMARY KEY CLUSTERED (LicenseParameterID, AuditRowID),
    
    CONSTRAINT FK_License_Seats__LicenseParameter
    FOREIGN KEY (LicenseParameterID)
    REFERENCES License.LicenseParameter (LicenseParameterID),
    
    CONSTRAINT FK_License_Seats__AuditRow
    FOREIGN KEY (AuditRowID)
    REFERENCES Audit.AuditRow (AuditRowID),
    
    CONSTRAINT CK_License_Seats__Quantity
    CHECK (Quantity > 0)
)
ON [DATA]
GO
    
CREATE TABLE License.UsageLimit
(
    LicenseParameterID  INT NOT NULL,
    AuditRowID          INT NOT NULL,
    Uses                INT NOT NULL,
    
    CONSTRAINT PK_License_UsageLimit
    PRIMARY KEY CLUSTERED (LicenseParameterID, AuditRowID),
    
    CONSTRAINT FK_License_UsageLimit__LicenseParameter
    FOREIGN KEY (LicenseParameterID)
    REFERENCES License.LicenseParameter (LicenseParameterID),
    
    CONSTRAINT FK_License_UsageLimit__AuditRow
    FOREIGN KEY (AuditRowID)
    REFERENCES Audit.AuditRow (AuditRowID),
    
    CONSTRAINT CK_License_UsageLimit__Uses
    CHECK (Uses > 0)
)
ON [DATA]
GO    
    
CREATE TABLE License.ExpirationDate
(
    LicenseParameterID  INT NOT NULL,
    AuditRowID          INT NOT NULL,
    ExpirationDate      DATETIME NOT NULL,
    
    CONSTRAINT PK_License_ExpirationDate
    PRIMARY KEY CLUSTERED (LicenseParameterID, AuditRowID),
    
    CONSTRAINT FK_License_ExpirationDate__LicenseParameter
    FOREIGN KEY (LicenseParameterID)
    REFERENCES License.LicenseParameter (LicenseParameterID),
    
    CONSTRAINT FK_License_ExpirationDate__AuditRow
    FOREIGN KEY (AuditRowID)
    REFERENCES Audit.AuditRow (AuditRowID)
)
ON [DATA]
GO    
        
-- LEASE

CREATE TABLE License.Lease
(
    LeaseID INT IDENTITY(1,1) NOT NULL,
    
    CONSTRAINT PK_License_Lease
    PRIMARY KEY (LeaseID)
)        
ON [DATA]
GO

CREATE TABLE License.License_Lease
(
    LicenseID       INT NOT NULL,
    LeaseID         INT NOT NULL,
    AssociationID   INT NOT NULL,
    
    CONSTRAINT PK_License_License_Lease
    PRIMARY KEY CLUSTERED (LicenseID, LeaseID),
    
    CONSTRAINT FK_License_License_Lease__License
    FOREIGN KEY (LicenseID)
    REFERENCES License.License (LicenseID),
    
    CONSTRAINT FK_License_License_Lease__Lease
    FOREIGN KEY (LeaseID)
    REFERENCES License.Lease (LeaseID),
    
    CONSTRAINT FK_License_License_Lease__Association
    FOREIGN KEY (AssociationID)
    REFERENCES Audit.Association (AssociationID)
)
ON [DATA]
GO

CREATE TABLE License.Lease_User
(
    UserID          INT NOT NULL,
    LeaseID         INT NOT NULL,
    AssociationID   INT NOT NULL,
    
    CONSTRAINT PK_License_Lease_User
    PRIMARY KEY CLUSTERED (UserID, LeaseID),
    
    CONSTRAINT FK_License_Lease_User__User
    FOREIGN KEY (UserID)
    REFERENCES Client.[User] (UserID),
    
    CONSTRAINT FK_License_Lease_User__Lease
    FOREIGN KEY (LeaseID)
    REFERENCES License.Lease (LeaseID),
    
    CONSTRAINT FK_License_Lease_User__Association
    FOREIGN KEY (AssociationID)
    REFERENCES Audit.Association (AssociationID)    
)
ON [DATA]
GO

CREATE TABLE License.Lease_User_History
(
    UserID      INT NOT NULL,
    LeaseID     INT NOT NULL,
    Accessed    DATETIME NOT NULL,
    
    CONSTRAINT PK_License_Lease_User_History
    PRIMARY KEY CLUSTERED (UserID, LeaseID, Accessed)
)    
ON [DATA]
GO
        
-- DEVICE

CREATE TABLE License.Device
(
    DeviceID        INT IDENTITY(1,1) NOT NULL,
    DeviceTypeID    TINYINT NOT NULL,
    Handle          UNIQUEIDENTIFIER NOT NULL CONSTRAINT DF_License_Device__Handle DEFAULT NEWID(),
    
    CONSTRAINT PK_License_Device
    PRIMARY KEY CLUSTERED (DeviceID),
    
    CONSTRAINT FK_License_Device__DeviceType
    FOREIGN KEY (DeviceTypeID)
    REFERENCES License.DeviceType (DeviceTypeID)          
)    
ON [DATA]
GO
    
CREATE TABLE License.Lease_Device
(
    DeviceID        INT NOT NULL,
    UserID          INT NOT NULL,
    LeaseID         INT NOT NULL,
    Token           UNIQUEIDENTIFIER NOT NULL CONSTRAINT DF_License_Lease_Device__Token DEFAULT NEWID(),
    ExpiresOn       DATETIME NOT NULL,
    AssociationID   INT NOT NULL,
    
    CONSTRAINT PK_License_Lease_Device
    PRIMARY KEY CLUSTERED (DeviceID, UserID, LeaseID, AssociationID),
    
    CONSTRAINT FK_License_Lease_Device__Device
    FOREIGN KEY (DeviceID)
    REFERENCES License.Device (DeviceID),
    
    CONSTRAINT FK_License_Lease_Device__Lease_User
    FOREIGN KEY (UserID, LeaseID)
    REFERENCES License.Lease_User (UserID, LeaseID),
    
    CONSTRAINT FK_License_Lease_Device__Association
    FOREIGN KEY (AssociationID)
    REFERENCES Audit.Association (AssociationID)
)
ON [DATA]
GO    
    
