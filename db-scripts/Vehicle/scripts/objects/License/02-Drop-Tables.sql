IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[License].[Lease_Device]') AND type in (N'U'))
DROP TABLE [License].[Lease_Device]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[License].[Device]') AND type in (N'U'))
DROP TABLE [License].[Device]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[License].[Lease_User_History]') AND type in (N'U'))
DROP TABLE [License].[Lease_User_History]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[License].[Lease_User]') AND type in (N'U'))
DROP TABLE [License].[Lease_User]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[License].[License_Lease]') AND type in (N'U'))
DROP TABLE [License].[License_Lease]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[License].[Lease]') AND type in (N'U'))
DROP TABLE [License].[Lease]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[License].[ExpirationDate]') AND type in (N'U'))
DROP TABLE [License].[ExpirationDate]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[License].[UsageLimit]') AND type in (N'U'))
DROP TABLE [License].[UsageLimit]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[License].[Seats]') AND type in (N'U'))
DROP TABLE [License].[Seats]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[License].[LicenseParameter]') AND type in (N'U'))
DROP TABLE [License].[LicenseParameter]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[License].[LicenseParameterType]') AND type in (N'U'))
DROP TABLE [License].[LicenseParameterType]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[License].[Client_License]') AND type in (N'U'))
DROP TABLE [License].[Client_License]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[License].[License]') AND type in (N'U'))
DROP TABLE [License].[License]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[License].[Product]') AND type in (N'U'))
DROP TABLE [License].[Product]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[License].[DeviceType]') AND type in (N'U'))
DROP TABLE [License].[DeviceType]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[License].[LicenseModel]') AND type in (N'U'))
DROP TABLE [License].[LicenseModel]

