IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Party].[PersonParty]') AND type in (N'U'))
	DROP TABLE [Party].[PersonParty]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Party].[OrganizationParty]') AND type in (N'U'))
	DROP TABLE [Party].[OrganizationParty]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Party].[OrganizationContact]') AND type in (N'U'))
	DROP TABLE [Party].[OrganizationContact]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Party].[Party]') AND type in (N'U'))
	DROP TABLE [Party].[Party]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Party].[PartyType]') AND type in (N'U'))
	DROP TABLE [Party].[PartyType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Party].[Organization]') AND type in (N'U'))
	DROP TABLE [Party].[Organization]
GO








