USE [Vehicle]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [Party].[Organization](
	[OrganizationID] [int] IDENTITY(1,1) NOT NULL,
	[ClientID] [int] NOT NULL,
	[Name] [varchar](50) NULL,
	[AssociationID] [int] NOT NULL,
	[RevisionNumber] [smallint] NULL,
	CONSTRAINT [PK_Party_Organization] PRIMARY KEY CLUSTERED ([OrganizationID] ASC) ON [IDX]
) ON [DATA]
GO

CREATE TABLE [Party].[OrganizationContact](
	[ContactID] [int] NOT NULL,
	[OrganizationID] [int] NOT NULL,
	CONSTRAINT [PK_Party_OrganizationContact] PRIMARY KEY CLUSTERED ([ContactID] ASC,[OrganizationID] ASC) ON [IDX]
) ON [DATA]
GO

CREATE TABLE [Party].[OrganizationParty](
	[PartyID] [int] NOT NULL,
	[OrganizationID] [int] NOT NULL,
	CONSTRAINT [PK_Party_OrganizationParty] PRIMARY KEY CLUSTERED ([PartyID] ASC) ON [IDX]
) ON [DATA]
GO

CREATE TABLE [Party].[Party](
	[PartyID] [int] IDENTITY(1,1) NOT NULL,
	[PartyTypeID] [tinyint] NOT NULL,
	[ClientID] [int] NOT NULL,
	[AssociationID] [int] NOT NULL,
	CONSTRAINT [PK_Party_Party] PRIMARY KEY CLUSTERED ([PartyID] ASC) ON [IDX]
) ON [DATA]
GO

CREATE TABLE [Party].[PartyType](
	[PartyTypeID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	CONSTRAINT [PK_Party_PartyType] PRIMARY KEY CLUSTERED ([PartyTypeID] ASC) ON [IDX]
) ON [DATA]
GO

CREATE TABLE [Party].[PersonParty](
	[PartyID] [int] NOT NULL,
	[PersonID] [int] NOT NULL,
	CONSTRAINT [PK_Party_PersonParty] PRIMARY KEY CLUSTERED ([PartyID] ASC,[PersonID] ASC) ON [IDX]
) ON [DATA]
GO



ALTER TABLE [Party].[Organization]  WITH CHECK ADD  CONSTRAINT [FK_Organization_Association] FOREIGN KEY([AssociationID])
REFERENCES [Audit].[Association] ([AssociationID])
GO
ALTER TABLE [Party].[Organization] CHECK CONSTRAINT [FK_Organization_Association]
GO


ALTER TABLE [Party].[Organization]  WITH CHECK ADD  CONSTRAINT [FK_Organization_Client] FOREIGN KEY([ClientID])
REFERENCES [Client].[Client] ([ClientID])
GO
ALTER TABLE [Party].[Organization] CHECK CONSTRAINT [FK_Organization_Client]
GO


ALTER TABLE [Party].[OrganizationContact]  WITH CHECK ADD  CONSTRAINT [FK_OrganizationContact_Contact] FOREIGN KEY([ContactID])
REFERENCES [Contact].[Contact] ([ContactID])
GO
ALTER TABLE [Party].[OrganizationContact] CHECK CONSTRAINT [FK_OrganizationContact_Contact]
GO


ALTER TABLE [Party].[OrganizationContact]  WITH CHECK ADD  CONSTRAINT [FK_OrganizationContact_Organization] FOREIGN KEY([OrganizationID])
REFERENCES [Party].[Organization] ([OrganizationID])
GO
ALTER TABLE [Party].[OrganizationContact] CHECK CONSTRAINT [FK_OrganizationContact_Organization]
GO


ALTER TABLE [Party].[OrganizationParty]  WITH CHECK ADD  CONSTRAINT [FK_OrganizationParty_Organization] FOREIGN KEY([OrganizationID])
REFERENCES [Party].[Organization] ([OrganizationID])
GO
ALTER TABLE [Party].[OrganizationParty] CHECK CONSTRAINT [FK_OrganizationParty_Organization]
GO


ALTER TABLE [Party].[OrganizationParty]  WITH CHECK ADD  CONSTRAINT [FK_OrganizationParty_Party] FOREIGN KEY([PartyID])
REFERENCES [Party].[Party] ([PartyID])
GO
ALTER TABLE [Party].[OrganizationParty] CHECK CONSTRAINT [FK_OrganizationParty_Party]
GO


ALTER TABLE [Party].[Party]  WITH CHECK ADD  CONSTRAINT [FK_Party_Association] FOREIGN KEY([AssociationID])
REFERENCES [Audit].[Association] ([AssociationID])
GO
ALTER TABLE [Party].[Party] CHECK CONSTRAINT [FK_Party_Association]
GO


ALTER TABLE [Party].[Party]  WITH CHECK ADD  CONSTRAINT [FK_Party_Client] FOREIGN KEY([ClientID])
REFERENCES [Client].[Client] ([ClientID])
GO
ALTER TABLE [Party].[Party] CHECK CONSTRAINT [FK_Party_Client]
GO


ALTER TABLE [Party].[Party]  WITH CHECK ADD  CONSTRAINT [FK_Party_PartyType] FOREIGN KEY([PartyTypeID])
REFERENCES [Party].[PartyType] ([PartyTypeID])
GO
ALTER TABLE [Party].[Party] CHECK CONSTRAINT [FK_Party_PartyType]
GO

ALTER TABLE [Party].[PersonParty]  WITH CHECK ADD  CONSTRAINT [FK_PersonParty_Party] FOREIGN KEY([PartyID])
REFERENCES [Party].[Party] ([PartyID])
GO
ALTER TABLE [Party].[PersonParty] CHECK CONSTRAINT [FK_PersonParty_Party]
GO

ALTER TABLE [Party].[PersonParty]  WITH CHECK ADD  CONSTRAINT [FK_PersonParty_Person] FOREIGN KEY([PersonID])
REFERENCES [Person].[Person] ([PersonID])
GO
ALTER TABLE [Party].[PersonParty] CHECK CONSTRAINT [FK_PersonParty_Person]
GO


