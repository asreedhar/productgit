
CREATE TABLE Client.Tag (
        TagID           INT IDENTITY(1,1) NOT NULL,
        ClientID        INT NOT NULL,
        Handle          UNIQUEIDENTIFIER NOT NULL
                        CONSTRAINT DF_Client_Tag__Handle DEFAULT NEWID(),
        CONSTRAINT PK_Client_Tag
                PRIMARY KEY (TagID),
        CONSTRAINT UK_Client_Tag__Handle
                UNIQUE (Handle),
        CONSTRAINT FK_Client_Tag__Client
                FOREIGN KEY (ClientID)
                REFERENCES Client.Client (ClientID)
)
ON [DATA]
GO

CREATE TABLE Client.Tag_History (
        TagID           INT NOT NULL,
        Name            VARCHAR(128) NOT NULL,
        AuditRowID      INT NOT NULL,
        CONSTRAINT PK_Client_Tag_History
                PRIMARY KEY (TagID, AuditRowID),
        CONSTRAINT FK_Client_Tag_History__Tag
                FOREIGN KEY (TagID)
                REFERENCES Client.Tag (TagID),
        CONSTRAINT FK_Client_Tag_History__AuditRow
                FOREIGN KEY (AuditRowID)
                REFERENCES Audit.AuditRow (AuditRowID),
        CONSTRAINT CK_Client_Tag_History__Name
                CHECK (LEN(Name) > 0)
)
ON [DATA]
GO


CREATE TABLE Client.Vehicle_Tag (
        VehicleID       INT NOT NULL,
        TagID           INT NOT NULL,
        AssociationID   INT NOT NULL,
        CONSTRAINT PK_Client_Vehicle_Tag
                PRIMARY KEY (VehicleID, TagID),
        CONSTRAINT FK_Client_Vehicle_Tag__Vehicle
                FOREIGN KEY (VehicleID)
                REFERENCES Client.Vehicle (VehicleID),
        CONSTRAINT FK_Client_Vehicle_Tag__Tag
                FOREIGN KEY (TagID)
                REFERENCES Client.Tag (TagID),
        CONSTRAINT FK_Client_Vehicle_Tag__Association
                FOREIGN KEY (AssociationID)
                REFERENCES Audit.Association (AssociationID)
)
ON [DATA]
GO

