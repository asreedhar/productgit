
CREATE TABLE Client.User_System (
    UserID      INT         NOT NULL,
    UserName    VARCHAR(50) NOT NULL,
    DisplayName VARCHAR(50) NOT NULL,
    CONSTRAINT PK_Client_User_System
        PRIMARY KEY CLUSTERED (UserID),
    CONSTRAINT FK_Client_User_System__User
        FOREIGN KEY (UserID)
        REFERENCES Client.[User] (UserID),
    CONSTRAINT CK_Client_User_System__UserName
        CHECK (LEN(UserName) > 0),
    CONSTRAINT CK_Client_User_System__DisplayName
        CHECK (LEN(DisplayName) > 0)
)
ON [DATA]
GO

INSERT INTO Client.UserType ( UserTypeID, Name ) VALUES  ( 2, 'System Account' )

-- black book

INSERT INTO Client.[User] ( UserTypeID, Handle ) VALUES  ( 2, NEWID() )

INSERT INTO Client.User_System ( UserID, UserName, DisplayName ) VALUES  ( @@IDENTITY, 'blackbook', 'Black Book System Account' )

-- edmunds

INSERT INTO Client.[User] ( UserTypeID, Handle ) VALUES  ( 2, NEWID() )

INSERT INTO Client.User_System ( UserID, UserName, DisplayName ) VALUES  ( @@IDENTITY, 'edmunds', 'Edmunds System Account' )

-- galves

INSERT INTO Client.[User] ( UserTypeID, Handle ) VALUES  ( 2, NEWID() )

INSERT INTO Client.User_System ( UserID, UserName, DisplayName ) VALUES  ( @@IDENTITY, 'galves', 'Galves System Account' )

-- kelley blue book

INSERT INTO Client.[User] ( UserTypeID, Handle ) VALUES  ( 2, NEWID() )

INSERT INTO Client.User_System ( UserID, UserName, DisplayName ) VALUES  ( @@IDENTITY, 'kbb', 'Kelley Blue Book System Account' )

-- nada

INSERT INTO Client.[User] ( UserTypeID, Handle ) VALUES  ( 2, NEWID() )

INSERT INTO Client.User_System ( UserID, UserName, DisplayName ) VALUES  ( @@IDENTITY, 'nada', 'Galves System Account' )

