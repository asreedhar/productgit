IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Client].[List_Vehicle]') AND type in (N'U'))
DROP TABLE [Client].[List_Vehicle]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Client].[List]') AND type in (N'U'))
DROP TABLE [Client].[List]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Client].[ListType]') AND type in (N'U'))
DROP TABLE [Client].[ListType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Client].[Client_Seller]') AND type in (N'U'))
DROP TABLE [Client].[Client_Seller]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Client].[Client_Dealer]') AND type in (N'U'))
DROP TABLE [Client].[Client_Dealer]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Client].[Client]') AND type in (N'U'))
DROP TABLE [Client].[Client]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Client].[ClientType]') AND type in (N'U'))
DROP TABLE [Client].[ClientType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Client].[User_Member]') AND type in (N'U'))
DROP TABLE [Client].[User_Member]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Client].[User]') AND type in (N'U'))
DROP TABLE [Client].[User]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Client].[UserType]') AND type in (N'U'))
DROP TABLE [Client].[UserType]
GO
