
CREATE TABLE Client.ClientType (
        ClientTypeID    TINYINT NOT NULL,
        Name            VARCHAR(50) NOT NULL,
        CONSTRAINT PK_Client_ClientType
                PRIMARY KEY CLUSTERED (ClientTypeID),
        CONSTRAINT UK_Client_ClientType__Name
                UNIQUE (Name)
)
ON [DATA]
GO

INSERT INTO Client.ClientType (ClientTypeID,Name) VALUES (1, 'Dealer')
INSERT INTO Client.ClientType (ClientTypeID,Name) VALUES (2, 'Seller')
GO

CREATE TABLE Client.Client (
        ClientID        INT IDENTITY(1,1) NOT NULL,
        ClientTypeID    TINYINT NOT NULL,
        Handle          UNIQUEIDENTIFIER NOT NULL
                        CONSTRAINT DF_Client_Client__Handle DEFAULT NEWID()
        CONSTRAINT PK_Client_Client
                PRIMARY KEY CLUSTERED (ClientID),
        CONSTRAINT PK_Client_Client__ClientType
                FOREIGN KEY (ClientTypeID)
                REFERENCES Client.ClientType (ClientTypeID)
)
ON [DATA]
GO

CREATE TABLE Client.Client_Dealer (
        ClientID        INT NOT NULL,
        DealerID        INT NOT NULL,
        CONSTRAINT PK_Client_Client_Dealer
                PRIMARY KEY CLUSTERED (ClientID, DealerID),
        CONSTRAINT FK_Client_Client_Dealer__Client
                FOREIGN KEY (ClientID)
                REFERENCES Client.Client (ClientID),
        CONSTRAINT FK_Client_Client_Dealer__Dealer
                FOREIGN KEY (DealerID)
                REFERENCES Reference.Dealer (DealerID)
)
ON [DATA]
GO

CREATE TABLE Client.Client_Seller (
        ClientID        INT NOT NULL,
        SellerID        INT NOT NULL,
        CONSTRAINT PK_Client_Client_Seller
                PRIMARY KEY CLUSTERED (ClientID, SellerID),
        CONSTRAINT FK_Client_Client_Seller__Client
                FOREIGN KEY (ClientID)
                REFERENCES Client.Client (ClientID),
        CONSTRAINT FK_Client_Client_Seller__Seller
                FOREIGN KEY (SellerID)
                REFERENCES Reference.Seller (SellerID)
)
ON [DATA]
GO

CREATE TABLE Client.UserType (
        UserTypeID      TINYINT NOT NULL,
        Name            VARCHAR(50) NOT NULL,
        CONSTRAINT PK_Client_UserType
                PRIMARY KEY CLUSTERED (UserTypeID),
        CONSTRAINT UK_Client_UserType__Name
                UNIQUE (Name)
)
ON [DATA]
GO

INSERT INTO Client.UserType (UserTypeID,Name) VALUES (1, 'Member')
GO

CREATE TABLE Client.[User] (
        UserID          INT IDENTITY(1,1) NOT NULL,
        UserTypeID      TINYINT NOT NULL,
        Handle          UNIQUEIDENTIFIER NOT NULL
                        CONSTRAINT DF_Client_User__Handle DEFAULT NEWID()
        CONSTRAINT PK_Client_User
                PRIMARY KEY CLUSTERED (UserID),
        CONSTRAINT FK_Client_User__UserType
                FOREIGN KEY (UserTypeID)
                REFERENCES Client.UserType (UserTypeID)
)
ON [DATA]
GO

CREATE TABLE Client.User_Member (
        UserID          INT NOT NULL,
        MemberID        INT NOT NULL,
        CONSTRAINT PK_Client_User_Member
                PRIMARY KEY CLUSTERED (UserID, MemberID),
        CONSTRAINT FK_Client_User_Member__User
                FOREIGN KEY (UserID)
                REFERENCES Client.[User] (UserID),
        CONSTRAINT FK_Client_User_Member__Member
                FOREIGN KEY (MemberID)
                REFERENCES Reference.Member (MemberID)
)
ON [DATA]
GO


CREATE TABLE Client.Vehicle (
        VehicleID    INT IDENTITY(1,1) NOT NULL,
        Handle       UNIQUEIDENTIFIER NOT NULL
                     CONSTRAINT DF_Client_VehiclePerspective__Handle DEFAULT NEWID(),
        ClientID     INT NOT NULL,
        ReferenceID  INT NOT NULL,
        CONSTRAINT PK_Client_Vehicle
                PRIMARY KEY (VehicleID),
        CONSTRAINT UK_Client_Vehicle__Handle
                UNIQUE (Handle),
        CONSTRAINT UK_Client_Vehicle
                UNIQUE (ClientID, ReferenceID),
        CONSTRAINT FK_Client_Vehicle__Client
                FOREIGN KEY (ClientID)
                REFERENCES Client.Client (ClientID),
        CONSTRAINT FK_Client_Vehicle__Vehicle
                FOREIGN KEY (ReferenceID)
                REFERENCES Reference.Vehicle (VehicleID)
)
ON [DATA]
GO



