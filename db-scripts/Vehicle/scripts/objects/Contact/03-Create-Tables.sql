--------------ContactPointType
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contact].[ContactPointType]') AND type in (N'U'))
BEGIN
	
	CREATE TABLE [Contact].[ContactPointType](
		[ContactPointTypeID] 	[tinyint] 		NOT NULL,
		[Name] 					[varchar](50) 	NULL,
	 
		CONSTRAINT [PK_ContactPointType] 
			PRIMARY KEY CLUSTERED ( [ContactPointTypeID] )
	) 
	ON [DATA]
	
	INSERT INTO [Contact].[ContactPointType] ( [ContactPointTypeID], [Name] ) VALUES ( 1, 'Email' )
	INSERT INTO [Contact].[ContactPointType] ( [ContactPointTypeID], [Name] ) VALUES ( 2, 'Phone' )
	INSERT INTO [Contact].[ContactPointType] ( [ContactPointTypeID], [Name] ) VALUES ( 3, 'Postal Address' )

END
GO

--------------ContactPointPurpose
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contact].[ContactPointPurpose]') AND type in (N'U'))
BEGIN

	CREATE TABLE [Contact].[ContactPointPurpose](
		[ContactPointPurposeID] [tinyint] 		NOT NULL,
		[Name] 					[varchar](50) 	NULL,
	 
		CONSTRAINT [PK_ContactPointPurpose] 
			PRIMARY KEY CLUSTERED ( [ContactPointPurposeID] )
	)
	ON [DATA]
	
	INSERT INTO [Contact].[ContactPointPurpose] ( [ContactPointPurposeID], [Name] ) VALUES ( 1, 'Alternate' )
	INSERT INTO [Contact].[ContactPointPurpose] ( [ContactPointPurposeID], [Name] ) VALUES ( 2, 'Home' )
	INSERT INTO [Contact].[ContactPointPurpose] ( [ContactPointPurposeID], [Name] ) VALUES ( 3, 'General' )
	INSERT INTO [Contact].[ContactPointPurpose] ( [ContactPointPurposeID], [Name] ) VALUES ( 4, 'Work' )
	
END
GO

--------------PhoneLineType
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contact].[PhoneLineType]') AND type in (N'U'))
BEGIN

	CREATE TABLE [Contact].[PhoneLineType](
		[PhoneLineTypeID] 	[tinyint] 		NOT NULL,
		[Name] 				[varchar](50) 	NULL,
		
		CONSTRAINT [PK_PhoneLineType] 
			PRIMARY KEY CLUSTERED ( [PhoneLineTypeID] )
	)
	ON [DATA]
	
	INSERT INTO [Contact].[PhoneLineType] ( [PhoneLineTypeID], [Name] ) VALUES ( 1, 'Cell' )
	INSERT INTO [Contact].[PhoneLineType] ( [PhoneLineTypeID], [Name] ) VALUES ( 2, 'Fax' )
	INSERT INTO [Contact].[PhoneLineType] ( [PhoneLineTypeID], [Name] ) VALUES ( 3, 'Home' )
	INSERT INTO [Contact].[PhoneLineType] ( [PhoneLineTypeID], [Name] ) VALUES ( 4, 'Main' )
	INSERT INTO [Contact].[PhoneLineType] ( [PhoneLineTypeID], [Name] ) VALUES ( 5, 'Work' )

END
GO

--------------Contact
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contact].[Contact]') AND type in (N'U'))
BEGIN

	CREATE TABLE [Contact].[Contact](
		[ContactID] 		[int] IDENTITY(1,1)	NOT NULL,
		[ClientID] 			[int] 				NOT NULL,
		[RevisionNumber] 	[smallint] 			NOT NULL,
	 
		CONSTRAINT [PK_Contact] 
			PRIMARY KEY CLUSTERED ( [ContactID] ),	 

		CONSTRAINT [FK_Contact_Client] 
			FOREIGN KEY ( [ClientID] )
			REFERENCES [Client].[Client] ( [ClientID] )		
	) 
	ON [DATA]
	
END
GO

--------------ContactPoint
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contact].[ContactPoint]') AND type in (N'U'))
BEGIN

	CREATE TABLE [Contact].[ContactPoint](
		[ContactPointID] 		[int] IDENTITY(1,1) NOT NULL,
		[ContactPointTypeID] 	[tinyint] 			NOT NULL,
		[ContactID] 			[int] 				NOT NULL,
		[ContactPointPurposeID] [tinyint] 			NOT NULL,
		[IsPrimary] 			[bit] 				NULL,
		[RevisionNumber] 		[smallint] 			NULL,
	 
		CONSTRAINT [PK_ContactPoint] 
			PRIMARY KEY CLUSTERED ( [ContactPointID] ),

		CONSTRAINT [FK_ContactPoint_ContactPointType] 
			FOREIGN KEY ( [ContactPointTypeID] )
			REFERENCES [Contact].[ContactPointType] ( [ContactPointTypeID] ),

		CONSTRAINT [FK_ContactPoint_Contact] 
			FOREIGN KEY ( [ContactID] )
			REFERENCES [Contact].[Contact] ( [ContactID] ),
			
		CONSTRAINT [FK_ContactPoint_ContactPointPurpose] 
			FOREIGN KEY ( [ContactPointPurposeID] )
			REFERENCES [Contact].[ContactPointPurpose] ( [ContactPointPurposeID] )
		
	) 
	ON [DATA]
	
END
GO

--------------Email
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contact].[Email]') AND type in (N'U'))
BEGIN

	CREATE TABLE [Contact].[Email](
		[ContactPointID] 	[int] 			NOT NULL,
		[Value] 			[varchar](250) 	NULL,
		
		CONSTRAINT [PK_Email] 
			PRIMARY KEY CLUSTERED ( [ContactPointID] ),
			
		CONSTRAINT [FK_Email_ContactPoint] 
			FOREIGN KEY ( [ContactPointID] )
			REFERENCES [Contact].[ContactPoint] ( [ContactPointID] )
	) 
	ON [DATA]
	
END
GO

--------------Phone
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contact].[Phone]') AND type in (N'U'))
BEGIN

	CREATE TABLE [Contact].[Phone](
		[ContactPointID] 	[int] 			NOT NULL,
		[PhoneLineTypeID] 	[tinyint] 		NOT NULL,
		[CountryCode] 		[varchar](3) 	NULL,
		[AreaCode] 			[char](3) 		NULL,
		[CentralOfficeCode] [char](3) 		NULL,
		[SubscriberNumber] 	[char](3) 		NULL,
		[Extension] 		[char](50) 		NULL,
		
		CONSTRAINT [PK_Phone] 
			PRIMARY KEY CLUSTERED ( [ContactPointID] ),
		
		CONSTRAINT [FK_Phone_ContactPoint] 
			FOREIGN KEY ( [ContactPointID] )
			REFERENCES [Contact].[ContactPoint] ( [ContactPointID] ),
			
		CONSTRAINT [FK_Phone_PhoneLineType] 
			FOREIGN KEY ( [PhoneLineTypeID] )
			REFERENCES [Contact].[PhoneLineType] ( [PhoneLineTypeID] )
	) 
	ON [DATA]
	
END
GO

--------------PostalAddress
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contact].[PostalAddress]') AND type in (N'U'))
BEGIN

	CREATE TABLE [Contact].[PostalAddress](
		[ContactPointID] [int] NOT NULL,
		[Line1] [varchar](250) NULL,
		[Line2] [varchar](250) NULL,
		[City] [varchar](250) NULL,
		[State] [char](2) NULL,
		[Zip] [char](6) NULL,
		[ZipPlus4] [char](4) NULL,
		
		CONSTRAINT [PK_PostalAddress] 
			PRIMARY KEY CLUSTERED ( [ContactPointID] ),
			
		CONSTRAINT [FK_PostalAddress_ContactPoint] 
			FOREIGN KEY ( [ContactPointID] )
			REFERENCES [Contact].[ContactPoint] ( [ContactPointID] )
	) 
	ON [DATA]
	
END
GO