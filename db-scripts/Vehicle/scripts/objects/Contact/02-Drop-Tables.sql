IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contact].[PhoneLineType]') AND type in (N'U'))
DROP TABLE [Contact].[PhoneLineType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contact].[ContactPointType]') AND type in (N'U'))
DROP TABLE [Contact].[ContactPointType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contact].[ContactPointPurpose]') AND type in (N'U'))
DROP TABLE [Contact].[ContactPointPurpose]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contact].[PostalAddress]') AND type in (N'U'))
DROP TABLE [Contact].[PostalAddress]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contact].[Email]') AND type in (N'U'))
DROP TABLE [Contact].[Email]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contact].[Phone]') AND type in (N'U'))
DROP TABLE [Contact].[Phone]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contact].[ContactPoint]') AND type in (N'U'))
DROP TABLE [Contact].[ContactPoint]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Contact].[Contact]') AND type in (N'U'))
DROP TABLE [Contact].[Contact]
GO
