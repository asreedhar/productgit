-- Tables not copied:
--      Kbb.OptionRelationship
--      Kbb.OptionRelationshipSet
--      Kbb.ProgramContext
--      ******************************
--      Kbb.MakeAbbreviation
--      Kbb.ReleaseSchedule
--      Migration.ModelMapping
--      Migration.ModelTrimMapping#Raw
--      Migration.ModelYearMapping
--      Migration.OptionMapping
--      Migration.TrimMapping

-- ********************************** REFERENCE DATA ******************************************************************


-- DATALOAD


-----------------------------------------------------------------------------------------------------------------------
--  Kbb.DataLoadStatus
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.DataLoadStatus
(
    DataLoadStatusID    tinyint NOT NULL,
    Description         varchar(20) NOT NULL,
    UniqueDataLoad      tinyint NOT NULL CONSTRAINT DF_Kbb_DataLoadStatus__UniqueDataLoad  DEFAULT ((1)),
    
    CONSTRAINT PK_Kbb_DataLoadStatus 
    PRIMARY KEY CLUSTERED (DataLoadStatusID)
) 
ON [DATA]
GO

INSERT Kbb.DataLoadStatus (DataLoadStatusID, Description, UniqueDataLoad) VALUES (0, 'Newest', 1)
INSERT Kbb.DataLoadStatus (DataLoadStatusID, Description, UniqueDataLoad) VALUES (1, 'Current', 1)
INSERT Kbb.DataLoadStatus (DataLoadStatusID, Description, UniqueDataLoad) VALUES (2, 'Archive', 1)
INSERT Kbb.DataLoadStatus (DataLoadStatusID, Description, UniqueDataLoad) VALUES (3, 'Loading', 1)
INSERT Kbb.DataLoadStatus (DataLoadStatusID, Description, UniqueDataLoad) VALUES (4, 'History', 0)
GO


-----------------------------------------------------------------------------------------------------------------------
--  Kbb.DataLoadload
--    Foreign Keys: 
--      - Kbb.DataLoadStatus
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.DataLoad
(
    DataLoadID          int NOT NULL,
    DataVersionID       int NOT NULL,
    DataLoadStatusID    tinyint NOT NULL CONSTRAINT DF_Kbb_DataLoad__DataLoadStatusID  DEFAULT ((3)),
    DateLoaded          smalldatetime NOT NULL CONSTRAINT DF_Kbb_DataLoad__DateLoaded  DEFAULT (getdate()),
 
    CONSTRAINT PK_Kbb_DataLoad 
    PRIMARY KEY CLUSTERED (DataLoadID),
    
    CONSTRAINT FK_Kbb_DataLoad__DataLoadStatusID
    FOREIGN KEY (DataLoadStatusID)
    REFERENCES Kbb.DataLoadStatus (DataLoadStatusID),        
) 
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.DataVersion
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.DataVersion
(
    DataLoadID      int NOT NULL,
    DataVersionID   int NOT NULL,
    ReleaseVersion  varchar(25) NULL,
    SchemaVersion   varchar(25) NULL,
    VehicleTypeID   int NOT NULL CONSTRAINT DF_Kbb_DataVersion__VehicleTypeID  DEFAULT ((1)),
    StartDate       smalldatetime NULL,
    EndDate         smalldatetime NULL,
    Edition         varchar(20) NULL,

    CONSTRAINT PK_Kbb_DataVersion 
    PRIMARY KEY CLUSTERED (DataLoadID, DataVersionID, VehicleTypeID)    
)
ON PS_Kbb__DataLoad (DataLoadID)
GO


-- CATEGORY


-----------------------------------------------------------------------------------------------------------------------
--  Kbb.Category
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.Category
(
    DataLoadID              int NOT NULL,
    CategoryID              int NOT NULL,
    CategoryTypeID          int NOT NULL,
    CategoryTypeDisplayName varchar(25) NOT NULL,
    DisplayName             varchar(50) NOT NULL,
    SortOrder               int NOT NULL,
 
    CONSTRAINT PK_Kbb_Category 
    PRIMARY KEY CLUSTERED (DataLoadID, CategoryID)
) 
ON PS_Kbb__DataLoad (DataLoadID)
GO


-- YEAR


-----------------------------------------------------------------------------------------------------------------------
--  Kbb.Year
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.[Year]
(
    DataLoadID                  int NOT NULL,
    YearID                      int NOT NULL,
    DisplayName                 varchar(25) NULL,
    VinCode                     char(1) NULL,
    MileageZeroPoint            int NULL,
    MaximumDeductionPercentage  decimal(2, 1) NULL,
 
    CONSTRAINT PK_Kbb_Year 
    PRIMARY KEY CLUSTERED (DataLoadID, YearID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO


-- MAKE


-----------------------------------------------------------------------------------------------------------------------
--  Kbb.Make
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.Make
(
    DataLoadID  int NOT NULL,
    MakeID      int NOT NULL,
    DisplayName varchar(30) NULL,

    CONSTRAINT PK_Kbb_Make 
    PRIMARY KEY CLUSTERED (DataLoadID, MakeID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO


-- MILEAGE


-----------------------------------------------------------------------------------------------------------------------
--  Kbb.MileageGroupAdjustment
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.MileageGroupAdjustment
(
    DataLoadID                  int NOT NULL,
    MileageRangeID              int NOT NULL,
    MileageGroupID              int NOT NULL,
    MileageGroupDisplayName     varchar(255) NOT NULL,
    Adjustment                  float NOT NULL,
    AdjustmentTypeID            int NOT NULL,
    AdjustmentTypeDisplayName   varchar(25) NOT NULL,
 
    CONSTRAINT PK_Kbb_MileageGroupAdjustment 
    PRIMARY KEY CLUSTERED (DataLoadID, MileageRangeID, MileageGroupID)     
) 
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.MileageRange
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.MileageRange
(
    DataLoadID      int NOT NULL,
    MileageRangeID  int NOT NULL,
    YearID          int NULL,
    VehicleTypeID   int NULL,
    MileageMin      int NULL,
    MileageMax      int NULL,
 
    CONSTRAINT PK_Kbb_MileageRange 
    PRIMARY KEY CLUSTERED (DataLoadID, MileageRangeID) 
)
ON PS_Kbb__DataLoad (DataLoadID)
GO


-- MODEL


-----------------------------------------------------------------------------------------------------------------------
--  Kbb.Model
--    Foreign Keys:
--      - Kbb.Make
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.Model
(
    DataLoadID      int NOT NULL,
    ModelID         int NOT NULL,
    DisplayName     varchar(50) NULL,
    Description     varchar(50) NULL,
    MakeID          int NULL,
    SortOrder       int NULL,
    MarketName      varchar(50) NULL,
    ShortName       varchar(50) NULL,
 
    CONSTRAINT PK_Kbb_Model 
    PRIMARY KEY CLUSTERED (DataLoadID, ModelID),

    CONSTRAINT FK_Kbb_Model__Make 
    FOREIGN KEY (DataLoadID, MakeID)
    REFERENCES Kbb.Make (DataLoadID, MakeID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.ModelYear
--    Foreign Keys:
--      - Kbb.Model
--      - Kbb.Year
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.ModelYear
(
    DataLoadID                  int NOT NULL,
    ModelYearID                 int NOT NULL,
    ModelID                     int NULL,
    YearID                      int NULL,
    Note                        varchar(1000) NULL,
    EffectiveDate               smalldatetime NULL,
    EffectiveDateMissingReason  varchar(50) NULL,
    RevisedFlag                 bit NULL,
    
    CONSTRAINT PK_Kbb_ModelYear 
    PRIMARY KEY CLUSTERED (DataLoadID, ModelYearID),
    
    CONSTRAINT FK_Kbb_ModelYear__Model 
    FOREIGN KEY (DataLoadID, ModelID)
    REFERENCES Kbb.Model (DataLoadID, ModelID),
    
    CONSTRAINT FK_Kbb_ModelYear__Year 
    FOREIGN KEY (DataLoadID, YearID)
    REFERENCES Kbb.[Year] (DataLoadID, YearID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO


-- TRIM


-----------------------------------------------------------------------------------------------------------------------
--  Kbb.Trim
--    Foreign Keys:
--      - Kbb.Model
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.Trim
(
    DataLoadID  int NOT NULL,
    TrimID      int NOT NULL,
    ModelID     int NULL,
    YearID      int NULL,
    DisplayName varchar(255) NULL,
    SortOrder   int NULL,
    TrimName    varchar(255) NULL,
 
    CONSTRAINT PK_Kbb_Trim 
    PRIMARY KEY CLUSTERED (DataLoadID, TrimID),
    
    CONSTRAINT FK_Kbb_Trim__Model 
    FOREIGN KEY (DataLoadID, ModelID)
    REFERENCES Kbb.Model (DataLoadID, ModelID),
    
    CONSTRAINT FK_Kbb_Trim__Year
    FOREIGN KEY (DataLoadID, YearID)
    REFERENCES Kbb.Year (DataLoadID, YearID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO


-- VEHICLE


-----------------------------------------------------------------------------------------------------------------------
--  Kbb.Vehicle
--    Foreign Keys:
--      - Kbb.Trim
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.Vehicle
(
    DataLoadID                      int NOT NULL,
    VehicleID                       int NOT NULL,
    TrimID                          int NULL,
    ManufacturerAssignedModelCode   varchar(30) NULL,
    SubTrim                         varchar(30) NULL,
    DisplayName                     varchar(255) NULL,
    VehicleTypeID                   int NULL,
    VehicleTypeDisplayName          varchar(15) NULL,
    SortOrder                       int NULL,
    DisplayNameAdditionalData       varchar(255) NULL,
    KbbSortOrder                    int NULL,
    AvailabilityStatus              varchar(25) NULL,
    AvailabilityStatusEndDate       datetime NULL,
    MarketName                      varchar(50) NULL,
    RelatedVehicleID                int NULL,
    AvailabilityStatusStartDate     datetime NULL,
    CPOAdjustment                   bit NULL,
    MileageGroupID                  int NULL,
    BlueBookName                    varchar(100) NULL,
    ShortName                       varchar(50) NULL,
 
    CONSTRAINT PK_Kbb_Vehicle 
    PRIMARY KEY CLUSTERED (DataLoadID, VehicleID),
    
    CONSTRAINT FK_Kbb_Vehicle__Trim 
    FOREIGN KEY (DataLoadID, TrimID)
    REFERENCES Kbb.Trim (DataLoadID, TrimID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO


-- SPECIFICATION


-----------------------------------------------------------------------------------------------------------------------
--  Kbb.ModelYear
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.Specification
(
    DataLoadID                      int NOT NULL,
    SpecificationID                 int NOT NULL,
    SpecificationTypeID             int NOT NULL,
    SpecificationTypeDisplayName    varchar(50) NOT NULL,
    DisplayName                     varchar(50) NOT NULL,
    Units                           varchar(50) NULL,
 
    CONSTRAINT PK_Kbb_Specification 
    PRIMARY KEY CLUSTERED (DataLoadID, SpecificationID) 
)
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.SpecificationValue
--    Foreign Keys:
--      - Kbb.Specification
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.SpecificationValue
(
    DataLoadID      int NOT NULL,
    VehicleID       int NOT NULL,
    SpecificationID int NOT NULL,
    ValueIndex      int NOT NULL,
    [Value]         varchar(255) NULL,
 
    CONSTRAINT PK_Kbb_SpecificationValue 
    PRIMARY KEY CLUSTERED (DataLoadID, VehicleID, SpecificationID, ValueIndex),
    
    CONSTRAINT FK_Kbb_SepcificationValue__Vehicle
    FOREIGN KEY (DataLoadID, VehicleID)
    REFERENCES Kbb.Vehicle (DataLoadID, VehicleID),
    
    CONSTRAINT FK_Kbb_SpecificationValue__Specification 
    FOREIGN KEY (DataLoadID, SpecificationID)
    REFERENCES Kbb.Specification (DataLoadID, SpecificationID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO


-- PRICE


-----------------------------------------------------------------------------------------------------------------------
--  Kbb.PriceType
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.PriceType
(
    DataLoadID     int NOT NULL,
    PriceTypeID    int NOT NULL,
    DisplayName    varchar (50) NULL,
 
    CONSTRAINT PK_Kbb_PriceType 
    PRIMARY KEY CLUSTERED (DataLoadID, PriceTypeID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO


-- REGION


-----------------------------------------------------------------------------------------------------------------------
--  Kbb.VehicleRegion
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.VehicleRegion
(
    DataLoadID          int NOT NULL,
    VehicleTypeRegionID int NOT NULL,
    VehicleTypeID       int NULL,
    RegionID            int NULL,
 
    CONSTRAINT PK_Kbb_VehicleRegion 
    PRIMARY KEY CLUSTERED (DataLoadID, VehicleTypeRegionID) 
)
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.RegionBasePrice
--    Foreign Keys:
--      - Kbb.PriceType
--      - Kbb.Vehicle
--      - Kbb.VehicleRegion
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.RegionBasePrice
(
    DataLoadID          int NOT NULL,
    VehicleID           int NOT NULL,
    VehicleTypeRegionID int NOT NULL,
    PriceTypeID         int NOT NULL,
    Price               decimal(10, 2) NULL,
 
    CONSTRAINT PK_Kbb_RegionBasePrice 
    PRIMARY KEY CLUSTERED (DataLoadID, VehicleID, VehicleTypeRegionID, PriceTypeID),
    
    CONSTRAINT FK_Kbb_RegionBasePrice__PriceType 
    FOREIGN KEY (DataLoadID, PriceTypeID)
    REFERENCES Kbb.PriceType (DataLoadID, PriceTypeID),

    CONSTRAINT FK_Kbb_RegionBasePrice__Vehicle 
    FOREIGN KEY (DataLoadID, VehicleID)
    REFERENCES Kbb.Vehicle (DataLoadID, VehicleID),

    CONSTRAINT FK_Kbb_RegionBasePrice__VehicleRegion 
    FOREIGN KEY (DataLoadID, VehicleTypeRegionID)
    REFERENCES Kbb.VehicleRegion (DataLoadID, VehicleTypeRegionID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.RegionState
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.RegionState
(
    DataLoadID          int NOT NULL,
    RegionID            int NOT NULL,
    StateID             int NOT NULL,
    StateAbbreviation   varchar(3) NULL,
    StateDisplayName    varchar(30) NULL,
    RegionDisplayName   varchar(25) NULL,
 
    CONSTRAINT PK_Kbb_RegionState 
    PRIMARY KEY CLUSTERED (DataLoadID, RegionID, StateID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO


-- VEHICLE MAPPINGS


-----------------------------------------------------------------------------------------------------------------------
--  Kbb.VehicleCategory
--    Foreign Keys:
--      - Kbb.Category
--      - Kbb.Vehicle
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.VehicleCategory
(
    DataLoadID                  int NOT NULL,
    CategoryID                  int NOT NULL,
    VehicleID                   int NOT NULL,
    VehicleCategorySequence     int NULL,
 
    CONSTRAINT PK_Kbb_VehicleCategory 
    PRIMARY KEY CLUSTERED (DataLoadID, CategoryID, VehicleID),
    
    CONSTRAINT FK_Kbb_VehicleCategory__Category 
    FOREIGN KEY (DataLoadID, CategoryID)
    REFERENCES Kbb.Category (DataLoadID, CategoryID),
    
    CONSTRAINT FK_Kbb_VehicleCategory__Vehicle 
    FOREIGN KEY (DataLoadID, VehicleID)
    REFERENCES Kbb.Vehicle (DataLoadID, VehicleID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.VehicleOption
--    Foreign Keys:
--      - Kbb.Vehicle
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.VehicleOption
(
    DataLoadID                      int NOT NULL,
    VehicleOptionID                 int NOT NULL,
    OptionTypeID                    int NULL,
    OptionTypeDisplayName           varchar(50) NULL,
    DisplayName                     varchar(200) NULL,
    DisplayNameAdditionalData       varchar(2000) NULL,
    ManufacturerAssignedOptionCode  varchar(50) NULL,
    OptionAvailabilityID            int NULL,
    OptionAvailabilityDisplayName   varchar(15) NULL,
    OptionAvailabilityCode          char(1) NULL,
    IsDefaultConfiguration          bit NULL,
    VehicleID                       int NULL,
    DetailName                      varchar(300) NULL,
    NonBoldName                     varchar(300) NULL,
    Footer                          varchar(2600) NULL,
    SortOrder                       int NULL,
    NCBBAdjustmentTypeID            int NULL,
    NCBBAdjustmentTypeDisplayName   varchar(25) NULL,
    NCBBCanExceedMSRPFlag           bit NULL,
    NCBBAdjustmentValue             decimal(9, 3) NULL,
 
    CONSTRAINT PK_Kbb_VehicleOption 
    PRIMARY KEY CLUSTERED (DataLoadID, VehicleOptionID),
    
    CONSTRAINT FK_Kbb_VehicleOption__Vehicle 
    FOREIGN KEY (DataLoadID, VehicleID)
    REFERENCES Kbb.Vehicle (DataLoadID, VehicleID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.VehicleOptionCategory
--    Foreign Keys:
--      - Kbb.Category
--      - Kbb.VehicleOption
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.VehicleOptionCategory
(
    DataLoadID                      int NOT NULL,
    VehicleOptionID                 int NOT NULL,
    CategoryID                      int NOT NULL,
    VehicleOptionCategorySequence   int NOT NULL,
 
    CONSTRAINT PK_Kbb_VehicleOptionCategory 
    PRIMARY KEY CLUSTERED (DataLoadID, VehicleOptionID, CategoryID),
    
    CONSTRAINT FK_Kbb_VehicleOptionCategory__Category 
    FOREIGN KEY (DataLoadID, CategoryID)
    REFERENCES Kbb.Category (DataLoadID, CategoryID),
    
    CONSTRAINT FK_Kbb_VehicleOptionCategory__VehicleOption 
    FOREIGN KEY (DataLoadID, VehicleOptionID)
    REFERENCES Kbb.VehicleOption (DataLoadID, VehicleOptionID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO


-- OPTIONS


-----------------------------------------------------------------------------------------------------------------------
--  Kbb.OptionSpecificationValue
--    Foreign Keys:
--      - Kbb.Specification
--      - Kbb.VehicleOption
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.OptionSpecificationValue
(
    DataLoadID      int NOT NULL,
    VehicleOptionID int NOT NULL,
    SpecificationID int NOT NULL,
    ValueIndex      int NOT NULL,
    [Value]         varchar(255) NOT NULL,
 
    CONSTRAINT PK_Kbb_OptionSpecificationValue 
    PRIMARY KEY CLUSTERED (DataLoadID, VehicleOptionID, SpecificationID, ValueIndex),
    
    CONSTRAINT FK_Kbb_OptionSpecificationValue__Specification 
    FOREIGN KEY (DataLoadID, SpecificationID)
    REFERENCES Kbb.Specification (DataLoadID, SpecificationID),
    
    CONSTRAINT FK_Kbb_OptionSpecificationValue__VehicleOption 
    FOREIGN KEY (DataLoadID, VehicleOptionID)
    REFERENCES Kbb.VehicleOption (DataLoadID, VehicleOptionID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.OptionRegionPriceAdjustment
--    Foreign Keys:
--      - Kbb.PriceType
--      - Kbb.VehicleOption
--      - Kbb.VehicleRegion
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.OptionRegionPriceAdjustment
(
    DataLoadID              int NOT NULL,
    VehicleOptionID         int NOT NULL,
    VehicleTypeRegionID     int NOT NULL,
    VehicleID               int NOT NULL,
    PriceTypeID             int NOT NULL,
    PriceAdjustment         decimal(10, 2) NULL,
    ValueTypeID             int NULL,
 
    CONSTRAINT PK_Kbb_OptionRegionPriceAdjustment 
    PRIMARY KEY CLUSTERED (DataLoadID, VehicleOptionID, VehicleTypeRegionID, VehicleID, PriceTypeID),
    
    CONSTRAINT FK_Kbb_OptionRegionPriceAdjustment__PriceType 
    FOREIGN KEY (DataLoadID, PriceTypeID)
    REFERENCES Kbb.PriceType (DataLoadID, PriceTypeID),
    
    CONSTRAINT FK_Kbb_OptionRegionPriceAdjustment__VehicleOption 
    FOREIGN KEY (DataLoadID, VehicleOptionID)
    REFERENCES Kbb.VehicleOption (DataLoadID, VehicleOptionID),
    
    CONSTRAINT FK_Kbb_OptionRegionPriceAdjustment__VehicleRegion 
    FOREIGN KEY (DataLoadID, VehicleTypeRegionID)
    REFERENCES Kbb.VehicleRegion (DataLoadID, VehicleTypeRegionID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.OptionRegionPriceAdjustment
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.OptionRelationship
(
    DataLoadID                          int NOT NULL,
    VehicleOptionRelationshipID         int NOT NULL,
    ContextID                           int NULL,
    ContextDisplayName                  varchar(25) NULL,
    ContextValueID                      int NULL,
    OptionRelationshipTypeID            int NULL,
    OptionRelationshipTypeDisplayName   varchar(50) NULL,
    Sequence                            int NULL,
    GroupID                             int NULL,
 
    CONSTRAINT PK_Kbb_OptionRelationship 
    PRIMARY KEY CLUSTERED (DataLoadID, VehicleOptionRelationshipID)
) 
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.OptionRelationshipSet
--    Foreign Keys:
--      - Kbb.OptionRelationship
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.OptionRelationshipSet
(
    DataLoadID                  int NOT NULL,
    VehicleOptionRelationshipID int NOT NULL,
    ContextID                   int NOT NULL,
    ContextValueID              int NOT NULL,
    ContextDisplayName          varchar(25) NULL,
    ExcludeFlag                 bit NULL,
 
    CONSTRAINT PK_Kbb_OptionRelationshipSet 
    PRIMARY KEY CLUSTERED (DataLoadID, VehicleOptionRelationshipID, ContextID, ContextValueID),
    
    CONSTRAINT FK_Kbb_OptionRelationshipSet__OptionRelationship 
    FOREIGN KEY (DataLoadID, VehicleOptionRelationshipID)
    REFERENCES Kbb.OptionRelationship (DataLoadID, VehicleOptionRelationshipID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO


-- PROGRAM CONTEXT


-----------------------------------------------------------------------------------------------------------------------
--  Kbb.ProgramContext
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.ProgramContext
(
    DataLoadID          int NOT NULL,
    ContextID           int NOT NULL,
    ContextValueID      int NOT NULL,
    ContextDisplayName  varchar(25) NOT NULL,
    DisplayName         varchar(200) NOT NULL,
    Attribute           varchar(3000) NOT NULL,
    AttributeValue      varchar(3000) NOT NULL,
 
    CONSTRAINT PK_Kbb_ProgramContext 
    PRIMARY KEY CLUSTERED (DataLoadID, ContextID, ContextValueID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO


-- VIN


-----------------------------------------------------------------------------------------------------------------------
--  Kbb.VinOptionEquipmentPattern
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.VinOptionEquipmentPattern
(
    DataLoadID                  int NOT NULL,
    VinOptionEquipmentPatternID int NOT NULL,
    Pattern                     varchar(17) NULL,
 
    CONSTRAINT PK_Kbb_VinOptionEquipmentPattern 
    PRIMARY KEY CLUSTERED (DataLoadID, VinOptionEquipmentPatternID) 
)
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.VinOptionEquipment
--    Foreign Keys:
--      - Kbb.Vehicle
--      - Kbb.VehicleOption
--      - Kbb.VinOptionEquipmentPattern
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.VinOptionEquipment
(
    DataLoadID                  int NOT NULL,
    VinOptionEquipmentPatternID int NOT NULL,
    VehicleID                   int NOT NULL,
    VehicleOptionID             int NOT NULL,
 
    CONSTRAINT PK_Kbb_VinOptionEquipment 
    PRIMARY KEY CLUSTERED (DataLoadID, VinOptionEquipmentPatternID, VehicleID, VehicleOptionID),
    
    CONSTRAINT FK_Kbb_VinOptionEquipment__Vehicle 
    FOREIGN KEY (DataLoadID, VehicleID)
    REFERENCES Kbb.Vehicle (DataLoadID, VehicleID),
    
    CONSTRAINT FK_Kbb_VinOptionEquipment__VehicleOption 
    FOREIGN KEY (DataLoadID, VehicleOptionID)
    REFERENCES Kbb.VehicleOption (DataLoadID, VehicleOptionID),
    
    CONSTRAINT FK_Kbb_VinOptionEquipment__VinOptionEquipmentPattern 
    FOREIGN KEY (DataLoadID, VinOptionEquipmentPatternID)
    REFERENCES Kbb.VinOptionEquipmentPattern (DataLoadID, VinOptionEquipmentPatternID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.VinVehiclePattern
--    Foreign Keys:
--      - Kbb.Vehicle
--      - Kbb.Year
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.VinVehiclePattern
(
    DataLoadID  int NOT NULL,
    YearID      int NOT NULL,
    MakeID      int NOT NULL,
    Pattern     varchar(17) NOT NULL,
    VehicleID   int NOT NULL,
 
    CONSTRAINT PK_Kbb_VinVehiclePattern 
    PRIMARY KEY CLUSTERED (DataLoadID, YearID, MakeID, Pattern, VehicleID),
    
    CONSTRAINT FK_Kbb_VinVehiclePattern__Vehicle 
    FOREIGN KEY (DataLoadID, VehicleID)
    REFERENCES Kbb.Vehicle (DataLoadID, VehicleID),
    
    CONSTRAINT FK_Kbb_VinVehiclePattern__Year 
    FOREIGN KEY (DataLoadID, YearID)
    REFERENCES Kbb.[Year] (DataLoadID, YearID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO



-- ********************************** CONFIGURATIONS ******************************************************************



-----------------------------------------------------------------------------------------------------------------------
--  Kbb.VehicleConfiguration
--    Foreign Keys:
--      - Reference.Vehicle
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.VehicleConfiguration 
(
    VehicleConfigurationID  INT IDENTITY(1,1)   NOT NULL,
    VehicleID               INT                 NOT NULL,
    
    CONSTRAINT PK_Kbb_VehicleConfiguration
        PRIMARY KEY CLUSTERED (VehicleConfigurationID),
        
    CONSTRAINT FK_Kbb_VehicleConfiguration__Vehicle
        FOREIGN KEY (VehicleID)
        REFERENCES Reference.Vehicle (VehicleID)
)
ON [DATA]
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.VehicleConfiguration_History
--    Foreign Keys:
--      - Kbb.VehicleConfiguration
--      - Kbb.States
--      - Kbb.AuditRow
--      - Kbb.Vehicle
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.VehicleConfiguration_History 
(
    VehicleConfigurationHistoryID   INT IDENTITY(1,1)   NOT NULL,
    VehicleConfigurationID          INT                 NOT NULL,
    DataLoadID                      INT                 NOT NULL,
    VehicleID                       INT                 NOT NULL,
    RegionID                        INT                 NOT NULL,
    StateID                         INT                 NOT NULL,
    Mileage                         INT                 NULL,
    ChangeTypeBitFlags              INT                 NOT NULL,
    AuditRowID                      INT                 NOT NULL,
    
    ChangeTypeBit_BookDate          AS (CONVERT(BIT, ChangeTypeBitFlags & POWER(2,0))),
    ChangeTypeBit_State             AS (CONVERT(BIT, ChangeTypeBitFlags & POWER(2,1))),
    ChangeTypeBit_VehicleID         AS (CONVERT(BIT, ChangeTypeBitFlags & POWER(2,2))),
    ChangeTypeBit_Mileage           AS (CONVERT(BIT, ChangeTypeBitFlags & POWER(2,3))),
    ChangeTypeBit_OptionActions     AS (CONVERT(BIT, ChangeTypeBitFlags & POWER(2,4))),
    ChangeTypeBit_OptionStates      AS (CONVERT(BIT, ChangeTypeBitFlags & POWER(2,5))),
    
    CONSTRAINT PK_Kbb_VehicleConfiguration_History
        PRIMARY KEY CLUSTERED (DataLoadID, VehicleConfigurationHistoryID),
        
    CONSTRAINT FK_Kbb_VehicleConfiguration_History__VehicleConfiguration
        FOREIGN KEY (VehicleConfigurationID)
        REFERENCES Kbb.VehicleConfiguration (VehicleConfigurationID),       
        
    CONSTRAINT FK_Kbb_VehicleConfiguration_History__Vehicle
        FOREIGN KEY (DataLoadID, VehicleID)
        REFERENCES Kbb.Vehicle (DataLoadID, VehicleID),
        
    CONSTRAINT FK_Kbb_VehicleConfiguration_History__State
        FOREIGN KEY (DataLoadID, RegionID, StateID)
        REFERENCES Kbb.RegionState (DataLoadID, RegionID, StateID),
        
    CONSTRAINT FK_Kbb_VehicleConfiguration_History__AuditRow
        FOREIGN KEY (AuditRowID)
        REFERENCES Audit.AuditRow (AuditRowID),
        
    CONSTRAINT CK_Kbb_VehicleConfiguration_History__Mileage
        CHECK (Mileage IS NULL OR Mileage >= 0)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.OptionActionType
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.OptionActionType 
(
    OptionActionTypeID  TINYINT     NOT NULL,
    Name                VARCHAR(32) NOT NULL,
    
    CONSTRAINT PK_Kbb_OptionActionType
        PRIMARY KEY CLUSTERED (OptionActionTypeID),
        
    CONSTRAINT UK_Kbb_OptionActionType
        UNIQUE NONCLUSTERED (Name),
        
    CONSTRAINT CK_Kbb_OptionActionType__Name
        CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

INSERT INTO Kbb.OptionActionType ( OptionActionTypeID, Name ) VALUES  ( 1, 'Add' )
INSERT INTO Kbb.OptionActionType ( OptionActionTypeID, Name ) VALUES  ( 2, 'Remove' )
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.OptionAction
--    Foreign Keys:
--      - Kbb.VehicleConfiguration_History
--      - Kbb.OptionActionType
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.OptionAction 
(
    VehicleConfigurationHistoryID   INT                 NOT NULL,
    OptionActionTypeID              TINYINT             NOT NULL,
    OptionID                        INT                 NOT NULL,
    DataLoadID                      INT                 NOT NULL,
    
    CONSTRAINT PK_Kbb_OptionAction
        PRIMARY KEY CLUSTERED (DataLoadID, VehicleConfigurationHistoryID, OptionID),
        
    CONSTRAINT FK_Kbb_OptionAction__VehicleConfiguration_History
        FOREIGN KEY (DataLoadID, VehicleConfigurationHistoryID)
        REFERENCES Kbb.VehicleConfiguration_History (DataLoadID, VehicleConfigurationHistoryID),
        
    CONSTRAINT FK_Kbb_OptionAction__OptionActionType
        FOREIGN KEY (OptionActionTypeID)
        REFERENCES Kbb.OptionActionType (OptionActionTypeID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.OptionState
--    Foreign Keys:
--      - Kbb.VehicleConfiguration_History
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.OptionState 
(
    VehicleConfigurationHistoryID   INT         NOT NULL,
    DataLoadID                      INT         NOT NULL,
    OptionID                        INT         NOT NULL,    
    Selected                        BIT         NOT NULL,
    CONSTRAINT PK_Kbb_OptionState
        PRIMARY KEY CLUSTERED (DataLoadID, VehicleConfigurationHistoryID, OptionID),
        
    CONSTRAINT FK_Kbb_OptionState__VehicleConfiguration_History
        FOREIGN KEY (DataLoadID, VehicleConfigurationHistoryID)
        REFERENCES Kbb.VehicleConfiguration_History (DataLoadID, VehicleConfigurationHistoryID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.Valuation
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.Valuation 
(
    ValuationID TINYINT     NOT NULL,
    Name        VARCHAR(32) NOT NULL,
    
    CONSTRAINT PK_Kbb_Valuation
        PRIMARY KEY CLUSTERED (ValuationID),
        
    CONSTRAINT UK_Kbb_Valuation
        UNIQUE NONCLUSTERED (Name),
        
    CONSTRAINT CK_Kbb_Valuation_Name
        CHECK (LEN(Name) > 0)
)
ON [DATA]
GO

INSERT INTO Kbb.Valuation ( ValuationID, Name ) VALUES ( 1, 'Base' )
INSERT INTO Kbb.Valuation ( ValuationID, Name ) VALUES ( 2, 'Option' )
INSERT INTO Kbb.Valuation ( ValuationID, Name ) VALUES ( 3, 'Mileage' )
INSERT INTO Kbb.Valuation ( ValuationID, Name ) VALUES ( 4, 'Final' )
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.Matrix
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.Matrix 
(
    MatrixID    INT IDENTITY(1,1)   NOT NULL,
    DataLoadID  INT                 NOT NULL,
    
    CONSTRAINT PK_Kbb_Matrix
        PRIMARY KEY NONCLUSTERED (DataLoadID, MatrixID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO
                     
-----------------------------------------------------------------------------------------------------------------------
--  Kbb.MatrixCell
--    Foreign Keys:
--      - Kbb.Matrix
--      - Kbb.PriceType
--      - Kbb.Valuation
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.MatrixCell 
(
    MatrixID    INT         NOT NULL,
    DataLoadID  INT         NOT NULL,
    PriceTypeID INT         NOT NULL,
    ValuationID TINYINT     NOT NULL,
    Visible     BIT         NOT NULL,
    Value       SMALLMONEY  NULL,
    
    CONSTRAINT PK_Kbb_MatrixCell
        PRIMARY KEY CLUSTERED (DataLoadID, MatrixID, PriceTypeID, ValuationID),
        
    CONSTRAINT FK_Kbb_MatrixCell__Matrix
        FOREIGN KEY (DataLoadID, MatrixID)
        REFERENCES Kbb.Matrix (DataLoadID, MatrixID),
        
    CONSTRAINT FK_Kbb_MatrixCell__PriceType
        FOREIGN KEY (DataLoadID, PriceTypeID)
        REFERENCES Kbb.PriceType (DataLoadID, PriceTypeID),
        
    CONSTRAINT FK_Kbb_MatrixCell__Valuation
        FOREIGN KEY (ValuationID)
        REFERENCES Kbb.Valuation (ValuationID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.VehicleConfiguration_History_Matrix
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.VehicleConfiguration_History_Matrix 
(
    VehicleConfigurationHistoryID   INT NOT NULL,
    MatrixID                        INT NOT NULL,
    DataLoadID                      INT NOT NULL,
    
    CONSTRAINT PK_Kbb_VehicleConfigurationHistory_Matrix
        PRIMARY KEY CLUSTERED (DataLoadID, VehicleConfigurationHistoryID),
        
    CONSTRAINT UK_Kbb_VehicleConfigurationHistory_Matrix__Matrix
        UNIQUE NONCLUSTERED (DataLoadID, MatrixID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Client.Vehicle_Kbb
--    Foreign Keys:
--      - Kbb.VehicleConfiguration
--      - Client.Vehicle
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Client.Vehicle_Kbb (
    VehicleConfigurationID  INT NOT NULL,
    VehicleID               INT NOT NULL,
    
    CONSTRAINT PK_Client_Vehicle_Kbb
        PRIMARY KEY CLUSTERED (VehicleConfigurationID, VehicleID),
        
    CONSTRAINT FK_Client_Vehicle_Kbb__VehicleConfiguration
        FOREIGN KEY (VehicleConfigurationID)
        REFERENCES Kbb.VehicleConfiguration (VehicleConfigurationID),
        
    CONSTRAINT FK_Client_Vehicle_Kbb__Vehicle
        FOREIGN KEY (VehicleID)
        REFERENCES Client.Vehicle (VehicleID)
)
ON [DATA]
GO
