-- ********************************** CONFIGURATIONS ******************************************************************

-- Client.Vehicle_Kbb
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Client].[Vehicle_Kbb]') AND type in (N'U'))
DROP TABLE [Client].[Vehicle_Kbb]
GO

-- Kbb.VehicleConfiguration_History_Matrix
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[VehicleConfiguration_History_Matrix]') AND type in (N'U'))
DROP TABLE [Kbb].[VehicleConfiguration_History_Matrix]
GO

-- Kbb.MatrixCell
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[MatrixCell]') AND type in (N'U'))
DROP TABLE [Kbb].[MatrixCell]
GO

-- Kbb.Matrix
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[Matrix]') AND type in (N'U'))
DROP TABLE [Kbb].[Matrix]
GO

-- Kbb.Valuation
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[Valuation]') AND type in (N'U'))
DROP TABLE [Kbb].[Valuation]
GO

-- Kbb.OptionState
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[OptionState]') AND type in (N'U'))
DROP TABLE [Kbb].[OptionState]
GO

-- Kbb.OptionAction
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[OptionAction]') AND type in (N'U'))
DROP TABLE [Kbb].[OptionAction]
GO

-- Kbb.OptionActionType
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[OptionActionType]') AND type in (N'U'))
DROP TABLE [Kbb].[OptionActionType]
GO

-- Kbb.VehicleConfiguration_History
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[VehicleConfiguration_History]') AND type in (N'U'))
DROP TABLE [Kbb].[VehicleConfiguration_History]
GO

-- Kbb.VehicleConfiguration
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[VehicleConfiguration]') AND type in (N'U'))
DROP TABLE [Kbb].[VehicleConfiguration]
GO


-- ********************************** REFERENCE DATA ******************************************************************

-- Kbb.VinVehiclePattern
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[VinVehiclePattern]') AND type in (N'U'))
DROP TABLE [Kbb].[VinVehiclePattern]
GO

-- Kbb.VinOptionEquipment
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[VinOptionEquipment]') AND type in (N'U'))
DROP TABLE [Kbb].[VinOptionEquipment]
GO

-- Kbb.VinOptionEquipmentPattern
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[VinOptionEquipmentPattern]') AND type in (N'U'))
DROP TABLE [Kbb].[VinOptionEquipmentPattern]
GO

-- Kbb.ProgramContext
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[ProgramContext]') AND type in (N'U'))
DROP TABLE [Kbb].[ProgramContext]
GO

-- Kbb.OptionRelationshipSet
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[OptionRelationshipSet]') AND type in (N'U'))
DROP TABLE [Kbb].[OptionRelationshipSet]
GO

-- Kbb.OptionRelationship
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[OptionRelationship]') AND type in (N'U'))
DROP TABLE [Kbb].[OptionRelationship]
GO

-- Kbb.OptionRegionPriceAdjustment
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[OptionRegionPriceAdjustment]') AND type in (N'U'))
DROP TABLE [Kbb].[OptionRegionPriceAdjustment]
GO

-- Kbb.OptionSpecificationValue
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[OptionSpecificationValue]') AND type in (N'U'))
DROP TABLE [Kbb].[OptionSpecificationValue]
GO

-- Kbb.VehicleOptionCategory
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[VehicleOptionCategory]') AND type in (N'U'))
DROP TABLE [Kbb].[VehicleOptionCategory]
GO

-- Kbb.VehicleOption
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[VehicleOption]') AND type in (N'U'))
DROP TABLE [Kbb].[VehicleOption]
GO

-- Kbb.VehicleCategory
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[VehicleCategory]') AND type in (N'U'))
DROP TABLE [Kbb].[VehicleCategory]
GO

-- Kbb.RegionState
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[RegionState]') AND type in (N'U'))
DROP TABLE [Kbb].[RegionState]
GO

-- Kbb.RegionBasePrice
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[RegionBasePrice]') AND type in (N'U'))
DROP TABLE [Kbb].[RegionBasePrice]
GO

-- Kbb.VehicleRegion
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[VehicleRegion]') AND type in (N'U'))
DROP TABLE [Kbb].[VehicleRegion]
GO

-- Kbb.PriceType
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[PriceType]') AND type in (N'U'))
DROP TABLE [Kbb].[PriceType]
GO

-- Kbb.SpecificationValue
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[SpecificationValue]') AND type in (N'U'))
DROP TABLE [Kbb].[SpecificationValue]
GO

-- Kbb.Specification
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[Specification]') AND type in (N'U'))
DROP TABLE [Kbb].[Specification]
GO

-- Kbb.Vehicle
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[Vehicle]') AND type in (N'U'))
DROP TABLE [Kbb].[Vehicle]
GO

-- Kbb.Trim
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[Trim]') AND type in (N'U'))
DROP TABLE [Kbb].[Trim]
GO

-- Kbb.ModelYear
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[ModelYear]') AND type in (N'U'))
DROP TABLE [Kbb].[ModelYear]
GO

-- Kbb.Model
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[Model]') AND type in (N'U'))
DROP TABLE [Kbb].[Model]
GO

-- Kbb.MileageRange
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[MileageRange]') AND type in (N'U'))
DROP TABLE [Kbb].[MileageRange]
GO

-- Kbb.MileageGroupAdjustment
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[MileageGroupAdjustment]') AND type in (N'U'))
DROP TABLE [Kbb].[MileageGroupAdjustment]
GO

-- Kbb.MakeAbbreviation
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[MakeAbbreviation]') AND type in (N'U'))
DROP TABLE [Kbb].[MakeAbbreviation]
GO

-- Kbb.Make
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[Make]') AND type in (N'U'))
DROP TABLE [Kbb].[Make]
GO

-- Kbb.Year
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[Year]') AND type in (N'U'))
DROP TABLE [Kbb].[Year]
GO

-- Kbb.Category
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[Category]') AND type in (N'U'))
DROP TABLE [Kbb].[Category]
GO

-- Kbb.DataVersion
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[DataVersion]') AND type in (N'U'))
DROP TABLE [Kbb].[DataVersion]
GO

-- Kbb.DataLoad
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[DataLoad]') AND type in (N'U'))
DROP TABLE [Kbb].[DataLoad]
GO

-- Kbb.DataLoadStatus
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[DataLoadStatus]') AND type in (N'U'))
DROP TABLE [Kbb].[DataLoadStatus]
GO
