
------------------------------------------------------------------------------------------------
-- Magic Database Settings
------------------------------------------------------------------------------------------------

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

------------------------------------------------------------------------------------------------
-- Partition Function
------------------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.partition_functions WHERE name = 'PF_Kbb__DataLoad')

	CREATE PARTITION FUNCTION PF_Kbb__DataLoad(int)
	    AS RANGE LEFT FOR VALUES (
	        20,
	        30) 

GO

------------------------------------------------------------------------------------------------
-- Partition Scheme
------------------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.partition_schemes WHERE name = 'PS_Kbb__DataLoad')
	
 	CREATE PARTITION SCHEME PS_Kbb__DataLoad
		AS PARTITION PF_Kbb__DataLoad TO (
            [DATA_KBB_FG1],
            [DATA_KBB_FG2],
            [PRIMARY])

GO
