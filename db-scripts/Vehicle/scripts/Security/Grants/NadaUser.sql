
IF NOT EXISTS (	SELECT	1
		FROM	sys.database_role_members RM
			JOIN sys.database_principals P1 ON RM.member_principal_id = P1.principal_id
			JOIN sys.database_principals P2 ON RM.role_principal_id = P2.principal_id
		WHERE	P1.NAME = 'firstlook'
			AND P1.type_desc = 'SQL_USER'
			AND P2.NAME = 'NadaUser'
			AND P2.type_desc = 'DATABASE_ROLE'
			)

	EXEC sp_addrolemember 'NadaUser', 'firstlook'

GO

GRANT EXECUTE ON Nada.CloneData TO [NadaUser]
GRANT EXECUTE ON Nada.stp_Accessories_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_AttributeTypes_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_Bodies_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_debugExclusiveAccessories_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_debugInclusiveAccessories_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_dtproperties_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_ExclusiveAccessories_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_InclusiveAccessories_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_Makes_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_Mileages_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_MileageValues_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_Regions_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_Series_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_States_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_TotalAdjFloorValues_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_ValueTypes_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_VehicleAccessories_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_VehicleAccessoryValues_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_VehicleAttributes_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_Vehicles_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_VehicleValues_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_VinVehicleAccessories_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_VinVehicleAttributes_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_VinVehicleMaps_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_VinVehicles_get TO [NadaUser]
GRANT EXECUTE ON Nada.stp_Years_get TO [NadaUser]
GRANT EXECUTE ON Nada.udp_System_GenerateProc TO [NadaUser]
GRANT EXECUTE ON Nada.udp_VehicleAccessories_g TO [NadaUser]
GRANT EXECUTE ON Nada.udp_Vehicles_g TO [NadaUser]
GRANT EXECUTE ON Nada.udp_VehiclesBodies_g TO [NadaUser]
GRANT EXECUTE ON Nada.udp_VehiclesMakes_g TO [NadaUser]
GRANT EXECUTE ON Nada.udp_VehiclesSeries_g TO [NadaUser]
GRANT EXECUTE ON Nada.udp_VinVehicleAccessories_g TO [NadaUser]
GRANT EXECUTE ON Nada.udp_VinVehicleMaps_g TO [NadaUser]
GO
