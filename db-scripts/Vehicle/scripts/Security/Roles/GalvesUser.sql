IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'DATABASE_ROLE'
			AND NAME = 'GalvesUser'
		)

	CREATE ROLE [GalvesUser] AUTHORIZATION [dbo]
GO
