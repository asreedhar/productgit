IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'SQL_USER'
			AND name = 'firstlook'
		)	
	EXEC sp_grantdbaccess N'firstlook'
GO

EXEC sp_addrolemember N'db_datareader', N'firstlook'
GO

EXEC sp_addrolemember N'db_datawriter', N'firstlook'
GO
