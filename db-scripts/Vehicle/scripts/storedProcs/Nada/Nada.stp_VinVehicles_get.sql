SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_VinVehicles_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_VinVehicles_get]
GO

CREATE PROCEDURE [Nada].[stp_VinVehicles_get]
	@Period	INT = NULL
	,@VinVehicleId	INT = NULL
	,@Vin	VARCHAR(17) = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[VinVehicleId]
		,[Vin]
	FROM
		Nada.[VinVehicles] (nolock)
	WHERE
		((@Period IS NULL) OR (@Period = [Period]))
		AND ((@VinVehicleId IS NULL) OR (@VinVehicleId = [VinVehicleId]))
		AND ((@Vin IS NULL) OR (@Vin = [Vin]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
