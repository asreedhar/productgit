SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[udp_VehiclesSeries_g]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[udp_VehiclesSeries_g]
GO

CREATE PROCEDURE [Nada].[udp_VehiclesSeries_g]
	@Period	INT
	,@VehicleYear	INT = NULL
	,@MakeCode	INT = NULL
	,@SeriesCode	INT = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT 
		A.Period,
		A.SeriesCode,
		A.SeriesDescr
	FROM
		Nada.Series A
		JOIN
			(SELECT
				A.[Period]
				,A.[SeriesCode]
			FROM
				Nada.[Vehicles] A (nolock)
			WHERE
				@Period = A.[Period]
				AND ((@VehicleYear IS NULL) OR (@VehicleYear = [VehicleYear]))
				AND ((@MakeCode IS NULL) OR (@MakeCode = A.[MakeCode]))
				AND ((@SeriesCode IS NULL) OR (@SeriesCode = A.[SeriesCode]))
			GROUP BY 
				A.[Period]
				,A.[SeriesCode]) B 
			ON A.Period = B.Period AND A.SeriesCode = B.SeriesCode
	ORDER BY
		A.Period, A.SeriesDescr
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
