SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[udp_VehiclesBodies_g]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[udp_VehiclesBodies_g]
GO

CREATE  PROCEDURE [Nada].[udp_VehiclesBodies_g]
	@Period	INT
	,@VehicleYear	INT = NULL
	,@MakeCode	INT = NULL
	,@SeriesCode	INT = NULL
	,@BodyCode	INT = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT 
		B.Period,
		B.Uid AS BodyCode,
		A.BodyDescr
	FROM
		Nada.Bodies A
		JOIN
			(SELECT
						A.[Period]
						,A.[Uid]
						,A.[BodyCode]
					FROM
						Nada.[Vehicles] A (nolock)
					WHERE
						@Period = A.[Period]
						AND ((@VehicleYear IS NULL) OR (@VehicleYear = [VehicleYear]))
						AND ((@MakeCode IS NULL) OR (@MakeCode = A.[MakeCode]))
						AND ((@SeriesCode IS NULL) OR (@SeriesCode = A.[SeriesCode]))
						AND ((@BodyCode IS NULL) OR (@BodyCode = A.[BodyCode]))) B
			ON A.Period = B.Period AND A.BodyCode = B.BodyCode
	ORDER BY
		A.Period, A.BodyDescr
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
