SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_Years_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_Years_get]
GO

CREATE PROCEDURE [Nada].[stp_Years_get]
	@Period	INT
	,@VehicleYear	INT = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[VehicleYear]
	FROM
		Nada.[Years] (nolock)
	WHERE
		(@Period = [Period])
		AND ((@VehicleYear IS NULL) OR (@VehicleYear = [VehicleYear]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
