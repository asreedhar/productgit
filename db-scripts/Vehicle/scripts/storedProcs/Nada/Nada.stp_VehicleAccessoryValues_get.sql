SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_VehicleAccessoryValues_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_VehicleAccessoryValues_get]
GO

CREATE PROCEDURE  [Nada].[stp_VehicleAccessoryValues_get]
	@Period	INT
	,@Uid	INT
	,@AccCode	CHAR(3) = NULL
	,@RegionId	INT = NULL
	,@ValueTypeId	INT = NULL
	,@ValueAmount	INT = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[Uid]
		,[AccCode]
		,[RegionId]
		,[ValueTypeId]
		,[ValueAmount]
	FROM
		Nada.[VehicleAccessoryValues] (nolock)
	WHERE
		@Period = [Period]
		AND @Uid = [Uid]
		AND ((@AccCode IS NULL) OR (@AccCode = [AccCode]))
		AND ((@RegionId IS NULL) OR (@RegionId = [RegionId]))
		AND ((@ValueTypeId IS NULL) OR (@ValueTypeId = [ValueTypeId]))
		AND ((@ValueAmount IS NULL) OR (@ValueAmount = [ValueAmount]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
