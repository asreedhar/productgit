SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_Mileages_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_Mileages_get]
GO

CREATE PROCEDURE [Nada].[stp_Mileages_get]
	@Period	INT
	,@VehicleYear	INT
	,@MileageCode	CHAR(1) = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[VehicleYear]
		,[MileageCode]
	FROM
		Nada.[Mileages] (nolock)
	WHERE
		(@Period = [Period])
		AND (@VehicleYear = [VehicleYear])
		AND ((@MileageCode IS NULL) OR (@MileageCode = [MileageCode]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
