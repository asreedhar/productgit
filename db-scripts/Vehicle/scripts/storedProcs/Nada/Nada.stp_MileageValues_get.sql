SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_MileageValues_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_MileageValues_get]
GO

CREATE PROCEDURE [Nada].[stp_MileageValues_get]
	@Period	INT
	,@VehicleYear	INT = NULL
	,@MileageCode	CHAR(1) = NULL
	,@Low	INT = NULL
	,@High	INT = NULL
	,@Adjustment	INT = NULL
	,@AdjustmentRate	DECIMAL(18,0) = NULL
	,@AdjustmentPercent	DECIMAL(18,0) = NULL
	,@Descr	VARCHAR(50) = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[VehicleYear]
		,[MileageCode]
		,[Low]
		,[High]
		,[Adjustment]
		,[AdjustmentRate]
		,[AdjustmentPercent]
		,[Descr]
	FROM
		Nada.[MileageValues] (nolock)
	WHERE
		(@Period = [Period])
		AND ((@VehicleYear IS NULL) OR (@VehicleYear = [VehicleYear]))
		AND ((@MileageCode IS NULL) OR (@MileageCode = [MileageCode]))
		AND ((@Low IS NULL) OR (@Low = [Low]))
		AND ((@High IS NULL) OR (@High = [High]))
		AND ((@Adjustment IS NULL) OR (@Adjustment = [Adjustment]))
		AND ((@AdjustmentRate IS NULL) OR (@AdjustmentRate = [AdjustmentRate]))
		AND ((@AdjustmentPercent IS NULL) OR (@AdjustmentPercent = [AdjustmentPercent]))
		AND ((@Descr IS NULL) OR (@Descr = [Descr]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
