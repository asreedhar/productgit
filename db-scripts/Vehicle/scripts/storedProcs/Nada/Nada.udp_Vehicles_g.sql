SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[udp_Vehicles_g]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[udp_Vehicles_g]
GO

CREATE PROCEDURE  [Nada].[udp_Vehicles_g]
	@Period	INT
	,@Uid	INT = NULL
	,@VehicleYear	INT = NULL
	,@MakeCode	INT = NULL
	,@SeriesCode	INT = NULL
	,@BodyCode	INT = NULL
	,@MileageCode	CHAR(1) = NULL
	,@Vic	VARCHAR(15) = NULL
	,@CurbWeight	INT = NULL
	,@GVW	INT = NULL
	,@GCW	INT = NULL
	,@MSRP	INT = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		A.[Period]
		,A.[Uid]
		,A.[VehicleYear]
		,A.[MakeCode]
		,B.[MakeDescr]
		,A.[SeriesCode]
		,C.[SeriesDescr]
		,A.[BodyCode]
		,D.[BodyDescr]
		,A.[MileageCode]
		,A.[Vic]
		,A.[CurbWeight]
		,A.[GVW]
		,A.[GCW]
		,A.[MSRP]
	FROM
		Nada.[Vehicles] A (nolock)
		JOIN Nada.Makes B ON (A.Period = B.Period AND A.MakeCode = B.MakeCode)
		JOIN Nada.Series C ON (A.Period = C.Period AND A.SeriesCode = C.SeriesCode)
		JOIN Nada.Bodies D ON (A.Period = D.Period AND A.BodyCode = D.BodyCode)
	WHERE
		(@Period = A.[Period])
		AND ((@Uid IS NULL) OR (@Uid = A.[Uid]))
		AND ((@VehicleYear IS NULL) OR (@VehicleYear = A.[VehicleYear]))
		AND ((@MakeCode IS NULL) OR (@MakeCode = A.[MakeCode]))
		AND ((@SeriesCode IS NULL) OR (@SeriesCode = A.[SeriesCode]))
		AND ((@BodyCode IS NULL) OR (@BodyCode = A.[BodyCode]))
		AND ((@MileageCode IS NULL) OR (@MileageCode = A.[MileageCode]))
		AND ((@Vic IS NULL) OR (@Vic = A.[Vic]))
		AND ((@CurbWeight IS NULL) OR (@CurbWeight = A.[CurbWeight]))
		AND ((@GVW IS NULL) OR (@GVW = A.[GVW]))
		AND ((@GCW IS NULL) OR (@GCW = A.[GCW]))
		AND ((@MSRP IS NULL) OR (@MSRP = A.[MSRP]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
