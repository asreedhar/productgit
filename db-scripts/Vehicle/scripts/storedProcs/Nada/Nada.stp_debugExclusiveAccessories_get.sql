SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_debugExclusiveAccessories_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_debugExclusiveAccessories_get]
GO

CREATE PROCEDURE [Nada].[stp_debugExclusiveAccessories_get]
	@Period	INT = NULL
	,@Uid	INT = NULL
	,@AccCode	CHAR(3) = NULL
	,@AccExcludeCode	CHAR(3) = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[Uid]
		,[AccCode]
		,[AccExcludeCode]
	FROM
		Nada.[debugExclusiveAccessories] (nolock)
	WHERE
		((@Period IS NULL) OR (@Period = [Period]))
		AND ((@Uid IS NULL) OR (@Uid = [Uid]))
		AND ((@AccCode IS NULL) OR (@AccCode = [AccCode]))
		AND ((@AccExcludeCode IS NULL) OR (@AccExcludeCode = [AccExcludeCode]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
