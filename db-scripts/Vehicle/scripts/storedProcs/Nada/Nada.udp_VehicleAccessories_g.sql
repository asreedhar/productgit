SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[udp_VehicleAccessories_g]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[udp_VehicleAccessories_g]
GO

CREATE PROCEDURE  [Nada].[udp_VehicleAccessories_g]
	@Period	INT
	,@Uid		INT
	,@AccCode	CHAR(3) = NULL
	,@IsIncluded	BIT = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		A.[Period]
		,A.[Uid]
		,A.[AccCode]
		,A.[IsIncluded]
		,B.[AccDescr]
		,B.[AccCategoryCode]
	FROM
		Nada.[VehicleAccessories] A (nolock)
		JOIN Nada.[Accessories] B (nolock)
			ON A.Period = B.Period
				AND A.AccCode = B.AccCode
	WHERE
		(@Period = A.[Period])
		AND (@Uid = [Uid])
		AND ((@AccCode IS NULL) OR (@AccCode = A.[AccCode]))
		AND ((@IsIncluded IS NULL) OR (@IsIncluded = [IsIncluded]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
