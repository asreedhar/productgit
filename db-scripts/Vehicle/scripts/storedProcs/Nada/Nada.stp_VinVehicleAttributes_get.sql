SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_VinVehicleAttributes_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_VinVehicleAttributes_get]
GO

CREATE PROCEDURE [Nada].[stp_VinVehicleAttributes_get]
	@Period	INT = NULL
	,@VinVehicleId	INT = NULL
	,@AttributeTypeId	INT = NULL
	,@IntValue	INT = NULL
	,@VarcharValue	VARCHAR(50) = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[VinVehicleId]
		,[AttributeTypeId]
		,[IntValue]
		,[VarcharValue]
	FROM
		Nada.[VinVehicleAttributes] (nolock)
	WHERE
		((@Period IS NULL) OR (@Period = [Period]))
		AND ((@VinVehicleId IS NULL) OR (@VinVehicleId= [VinVehicleId]))
		AND ((@AttributeTypeId IS NULL) OR (@AttributeTypeId = [AttributeTypeId]))
		AND ((@IntValue IS NULL) OR (@IntValue = [IntValue]))
		AND ((@VarcharValue IS NULL) OR (@VarcharValue = [VarcharValue]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
