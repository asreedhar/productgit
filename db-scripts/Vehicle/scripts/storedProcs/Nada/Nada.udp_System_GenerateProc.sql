SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[udp_System_GenerateProc]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[udp_System_GenerateProc]
GO

CREATE PROCEDURE [Nada].[udp_System_GenerateProc] 
AS
SET NOCOUNT ON
	select 
		O.[NAME]	[O_NAME], 
		C.[NAME]	[C_NAME], 
		T.[NAME]	[T_NAME], 
		C.[LENGTH]	[LENGTH],
		C.[XPREC]	[XPREC],
		C.[XSCALE]	[XSCALE]
	from 
		sysobjects O
		join syscolumns C
			on O.[id] = C.[id]
		join systypes T
			on C.xtype = T.xtype 
	where
		O.xtype = 'U' AND
		T.status <> 1
GO
