SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_States_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_States_get]
GO

CREATE PROCEDURE [Nada].[stp_States_get]
	@Period	INT
	,@StateCode	CHAR(3) = NULL
	,@RegionId	INT = NULL
	,@StateDescr	VARCHAR(50) = NULL
	,@StateAbbr	CHAR(2) = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[StateCode]
		,[RegionId]
		,[StateDescr]
		,[StateAbbr]
	FROM
		Nada.[States] (nolock)
	WHERE
		(@Period = [Period])
		AND ((@StateCode IS NULL) OR (@StateCode = [StateCode]))
		AND ((@RegionId IS NULL) OR (@RegionId = [RegionId]))
		AND ((@StateDescr IS NULL) OR (@StateDescr = [StateDescr]))
		AND ((@StateAbbr IS NULL) OR (@StateAbbr = [StateAbbr]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
