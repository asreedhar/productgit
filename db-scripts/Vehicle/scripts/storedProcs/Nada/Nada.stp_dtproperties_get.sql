SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_dtproperties_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_dtproperties_get]
GO

CREATE PROCEDURE [Nada].[stp_dtproperties_get]
	@id	INT = NULL
	,@objectid	INT = NULL
	,@property	VARCHAR(64) = NULL
	,@value	VARCHAR(255) = NULL
	,@uvalue	NVARCHAR(255) = NULL
	,@version	INT = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[id]
		,[objectid]
		,[property]
		,[value]
		,[uvalue]
		,[lvalue]
		,[version]
	FROM
		[dtproperties] (nolock)
	WHERE
		((@id IS NULL) OR (@id = [id]))
		AND ((@objectid IS NULL) OR (@objectid = [objectid]))
		AND ((@property IS NULL) OR (@property = [property]))
		AND ((@value IS NULL) OR (@value = [value]))
		AND ((@uvalue IS NULL) OR (@uvalue = [uvalue]))
		AND ((@version IS NULL) OR (@version = [version]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
