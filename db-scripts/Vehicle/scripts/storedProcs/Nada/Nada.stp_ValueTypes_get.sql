SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_ValueTypes_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_ValueTypes_get]
GO

CREATE PROCEDURE [Nada].[stp_ValueTypes_get]
	@Period	INT = NULL
	,@ValueTypeId	INT = NULL
	,@ValueDescr	VARCHAR(20) = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[ValueTypeId]
		,[ValueDescr]
	FROM
		Nada.[ValueTypes] (nolock)
	WHERE
		((@Period IS NULL) OR (@Period = [Period]))
		AND ((@ValueTypeId IS NULL) OR (@ValueTypeId = [ValueTypeId]))
		AND ((@ValueDescr IS NULL) OR (@ValueDescr = [ValueDescr]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
