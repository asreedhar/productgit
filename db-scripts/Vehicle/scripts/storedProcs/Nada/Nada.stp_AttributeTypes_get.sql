SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_AttributeTypes_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_AttributeTypes_get]
GO

CREATE PROCEDURE [Nada].[stp_AttributeTypes_get]
	@Period	INT = NULL
	,@AttributeTypeId	INT = NULL
	,@AttributeTypeDescr	VARCHAR(50) = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[AttributeTypeId]
		,[AttributeTypeDescr]
	FROM
		Nada.[AttributeTypes] (nolock)
	WHERE
		((@Period IS NULL) OR (@Period = [Period]))
		AND ((@AttributeTypeId IS NULL) OR (@AttributeTypeId = [AttributeTypeId]))
		AND ((@AttributeTypeDescr IS NULL) OR (@AttributeTypeDescr = [AttributeTypeDescr]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
