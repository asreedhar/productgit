SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_Accessories_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_Accessories_get]
GO

CREATE PROCEDURE [Nada].[stp_Accessories_get]
	@Period	INT = NULL
	,@AccCode	CHAR(3) = NULL
	,@AccDescr	VARCHAR(50) = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[AccCode]
		,[AccDescr]
	FROM
		Nada.[Accessories] (nolock)
	WHERE
		((@Period IS NULL) OR (@Period = [Period]))
		AND ((@AccCode IS NULL) OR (@AccCode = [AccCode]))
		AND ((@AccDescr IS NULL) OR (@AccDescr = [AccDescr]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
