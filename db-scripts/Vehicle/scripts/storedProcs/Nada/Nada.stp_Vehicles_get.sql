SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_Vehicles_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_Vehicles_get]
GO

CREATE PROCEDURE [Nada].[stp_Vehicles_get]
	@Period	INT = NULL
	,@Uid	INT = NULL
	,@VehicleYear	INT = NULL
	,@MakeCode	INT = NULL
	,@SeriesCode	INT = NULL
	,@BodyCode	INT = NULL
	,@MileageCode	CHAR(1) = NULL
	,@Vic	VARCHAR(15) = NULL
	,@CurbWeight	INT = NULL
	,@GVW	INT = NULL
	,@GCW	INT = NULL
	,@MSRP	INT = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[Uid]
		,[VehicleYear]
		,[MakeCode]
		,[SeriesCode]
		,[BodyCode]
		,[MileageCode]
		,[Vic]
		,[CurbWeight]
		,[GVW]
		,[GCW]
		,[MSRP]
	FROM
		Nada.[Vehicles] (nolock)
	WHERE
		((@Period IS NULL) OR (@Period = [Period]))
		AND ((@Uid IS NULL) OR (@Uid = [Uid]))
		AND ((@VehicleYear IS NULL) OR (@VehicleYear = [VehicleYear]))
		AND ((@MakeCode IS NULL) OR (@MakeCode = [MakeCode]))
		AND ((@SeriesCode IS NULL) OR (@SeriesCode = [SeriesCode]))
		AND ((@BodyCode IS NULL) OR (@BodyCode = [BodyCode]))
		AND ((@MileageCode IS NULL) OR (@MileageCode = [MileageCode]))
		AND ((@Vic IS NULL) OR (@Vic = [Vic]))
		AND ((@CurbWeight IS NULL) OR (@CurbWeight = [CurbWeight]))
		AND ((@GVW IS NULL) OR (@GVW = [GVW]))
		AND ((@GCW IS NULL) OR (@GCW = [GCW]))
		AND ((@MSRP IS NULL) OR (@MSRP = [MSRP]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
