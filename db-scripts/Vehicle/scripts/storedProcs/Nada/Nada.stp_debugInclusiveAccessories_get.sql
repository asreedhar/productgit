SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_debugInclusiveAccessories_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_debugInclusiveAccessories_get]
GO

CREATE PROCEDURE [Nada].[stp_debugInclusiveAccessories_get]
	@Period	INT = NULL
	,@Uid	INT = NULL
	,@AccCode	CHAR(3) = NULL
	,@AccInclCode	CHAR(3) = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[Uid]
		,[AccCode]
		,[AccInclCode]
	FROM
		Nada.[debugInclusiveAccessories] (nolock)
	WHERE
		((@Period IS NULL) OR (@Period = [Period]))
		AND ((@Uid IS NULL) OR (@Uid = [Uid]))
		AND ((@AccCode IS NULL) OR (@AccCode = [AccCode]))
		AND ((@AccInclCode IS NULL) OR (@AccInclCode = [AccInclCode]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
