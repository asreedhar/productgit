SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_VinVehicleAccessories_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_VinVehicleAccessories_get]
GO

CREATE PROCEDURE [Nada].[stp_VinVehicleAccessories_get]
	@Period	INT
	,@VinVehicleId	INT
	,@Uid	INT = NULL
	,@AccCode	CHAR(3) = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[VinVehicleId]
		,[Uid]
		,[AccCode]
	FROM
		[VinVehicleAccessories] (nolock)
	WHERE
		(@Period = [Period])
		AND (@VinVehicleId = [VinVehicleId])
		AND ((@Uid IS NULL) OR (@Uid = [Uid]))
		AND ((@AccCode IS NULL) OR (@AccCode = [AccCode]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
