SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_TotalAdjFloorValues_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_TotalAdjFloorValues_get]
GO

CREATE  PROCEDURE [Nada].[stp_TotalAdjFloorValues_get]
	@Period	INT
	,@ValueTypeId	INT = NULL
	,@ValueAmount	INT = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[ValueTypeId]
		,[ValueAmount]
	FROM
		Nada.[TotalAdjFloorValues] (nolock)
	WHERE
		(@Period = [Period])
		AND ((@ValueTypeId IS NULL) OR (@ValueTypeId = [ValueTypeId]))
		AND ((@ValueAmount IS NULL) OR (@ValueAmount = [ValueAmount]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
