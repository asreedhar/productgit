SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_Regions_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_Regions_get]
GO

CREATE PROCEDURE [Nada].[stp_Regions_get]
	@Period	INT
	,@RegionId	INT = NULL
	,@RegionDescr	VARCHAR(50) = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[RegionId]
		,[RegionDescr]
	FROM
		Nada.[Regions] (nolock)
	WHERE
		(@Period = [Period])
		AND ((@RegionId IS NULL) OR (@RegionId = [RegionId]))
		AND ((@RegionDescr IS NULL) OR (@RegionDescr = [RegionDescr]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
