SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[CloneData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[CloneData]
GO

CREATE PROCEDURE Nada.CloneData
    @Period INT,
    @Uid INT,
    @Vin CHAR(17) = NULL
AS

SET NOCOUNT ON;

/* DECLARE @Uid INT, @Vin CHAR(17), @Period INT

SET @Uid = 1163992

SET @Vin = '1FTEX1EV5AFA23249'

SELECT @Period = MAX(Period) FROM VehicleUC_new.dbo.Vehicles */

INSERT INTO Nada.Periods ( Period )

SELECT  @Period
WHERE   NOT EXISTS (
            SELECT  1
            FROM    Nada.Periods
            WHERE   Period = @Period
        )

INSERT INTO Nada.Years ( Period, VehicleYear )
SELECT  Period, VehicleYear
FROM    VehicleUC_new.dbo.Vehicles V
WHERE   Uid = @Uid
AND     Period = @Period
AND     NOT EXISTS (
            SELECT  1
            FROM    Nada.Years Y
            WHERE   Y.Period = V.Period
            AND     Y.VehicleYear = V.VehicleYear
        )

INSERT INTO Nada.Makes ( Period, MakeCode, MakeDescr )
SELECT  M.Period, M.MakeCode, M.MakeDescr
FROM    VehicleUC_new.dbo.Vehicles V
JOIN    VehicleUC_new.dbo.Makes M ON M.Period = V.Period AND M.MakeCode = V.MakeCode
WHERE   V.Uid = @Uid
AND     V.Period = @Period
AND     NOT EXISTS (
            SELECT  1
            FROM    Nada.Makes X
            WHERE   X.Period = M.Period
            AND     X.MakeCode = M.MakeCode
        )

INSERT INTO Nada.Series ( Period, SeriesCode, SeriesDescr )
SELECT  S.Period, S.SeriesCode, S.SeriesDescr
FROM    VehicleUC_new.dbo.Vehicles V
JOIN    VehicleUC_new.dbo.Series S ON S.Period = V.Period AND S.SeriesCode = V.SeriesCode
WHERE   V.Uid = @Uid
AND     V.Period = @Period
AND     NOT EXISTS (
            SELECT  1
            FROM    Nada.Series X
            WHERE   X.Period = S.Period
            AND     X.SeriesCode = S.SeriesCode
        )

INSERT INTO Nada.Bodies ( Period, BodyCode, BodyDescr )
SELECT  B.Period, B.BodyCode, B.BodyDescr
FROM    VehicleUC_new.dbo.Vehicles V
JOIN    VehicleUC_new.dbo.Bodies B ON B.Period = V.Period AND B.BodyCode = V.BodyCode
WHERE   V.Uid = @Uid
AND     V.Period = @Period
AND     NOT EXISTS (
            SELECT  1
            FROM    Nada.Bodies X
            WHERE   X.Period = B.Period
            AND     X.BodyCode = B.BodyCode
        )

INSERT INTO Nada.Regions ( Period, RegionId, RegionDescr )
SELECT  R.Period, R.RegionId, R.RegionDescr
FROM    VehicleUC_new.dbo.Regions R
WHERE   R.Period = @Period
AND     NOT EXISTS (
            SELECT  1
            FROM    Nada.Regions X
            WHERE   X.Period = R.Period
            AND     X.RegionId = R.RegionId
        )

INSERT INTO Nada.States
        ( Period ,
          StateCode ,
          RegionId ,
          StateDescr ,
          StateAbbr
        )
SELECT  S.Period ,
        S.StateCode ,
        S.RegionId ,
        S.StateDescr ,
        S.StateAbbr
FROM    VehicleUC_new.dbo.States S
WHERE   S.Period = @Period
AND     NOT EXISTS (
            SELECT  1
            FROM    Nada.States X
            WHERE   X.Period = S.Period
            AND     X.StateCode = S.StateCode
        )

INSERT INTO Nada.Mileages ( Period, VehicleYear, MileageCode )
SELECT  M.Period ,
        M.VehicleYear ,
        M.MileageCode
FROM    VehicleUC_new.dbo.Mileages M
JOIN    VehicleUC_new.dbo.Vehicles V ON V.Period = M.Period AND V.VehicleYear = M.VehicleYear AND V.MileageCode = M.MileageCode
WHERE   V.Period = @Period
AND     V.Uid = @Uid
AND     NOT EXISTS (
            SELECT  1
            FROM    Nada.Mileages X
            WHERE   X.Period = M.Period
            AND     X.VehicleYear = M.VehicleYear
            AND     X.MileageCode = M.MileageCode
        )

INSERT INTO Nada.MileageValues
        ( Period ,
          VehicleYear ,
          MileageCode ,
          Low ,
          High ,
          Adjustment ,
          AdjustmentRate ,
          AdjustmentPercent ,
          Descr
        )
SELECT  M.Period ,
        M.VehicleYear ,
        M.MileageCode ,
        M.Low ,
        M.High ,
        M.Adjustment ,
        M.AdjustmentRate ,
        M.AdjustmentPercent ,
        M.Descr
FROM    VehicleUC_new.dbo.MileageValues M
JOIN    VehicleUC_new.dbo.Vehicles V ON V.Period = M.Period AND V.VehicleYear = M.VehicleYear AND V.MileageCode = M.MileageCode
WHERE   V.Period = @Period
AND     V.Uid = @Uid
AND     NOT EXISTS (
            SELECT  1
            FROM    Nada.MileageValues X
            WHERE   X.Period = M.Period
            AND     X.VehicleYear = M.VehicleYear
            AND     X.MileageCode = M.MileageCode
            AND     X.Low = M.Low
            AND     X.High = M.High
        )

INSERT INTO Nada.ValueTypes ( Period, ValueTypeId, ValueDescr )
SELECT  V.Period, V.ValueTypeId, V.ValueDescr
FROM    VehicleUC_new.dbo.ValueTypes V
WHERE   V.Period = @Period
AND     NOT EXISTS (
            SELECT  1
            FROM    Nada.ValueTypes X
            WHERE   X.Period = V.Period
            AND     X.ValueTypeId = V.ValueTypeId
        )

INSERT INTO Nada.TotalAdjFloorValues ( Period, ValueTypeId, ValueAmount )
SELECT  Period ,
        ValueTypeId ,
        ValueAmount
FROM    VehicleUC_new.dbo.TotalAdjFloorValues T
WHERE   T.Period = @Period
AND     NOT EXISTS (
            SELECT  1
            FROM    Nada.TotalAdjFloorValues X
            WHERE   X.Period = T.Period
            AND     X.ValueTypeId = T.ValueTypeId
        )

INSERT INTO Nada.Vehicles
        ( Period ,
          Uid ,
          VehicleYear ,
          MakeCode ,
          SeriesCode ,
          BodyCode ,
          MileageCode ,
          Vic ,
          CurbWeight ,
          GVW ,
          GCW ,
          MSRP
        )
SELECT  Period ,
        Uid ,
        VehicleYear ,
        MakeCode ,
        SeriesCode ,
        BodyCode ,
        MileageCode ,
        Vic ,
        CurbWeight ,
        GVW ,
        GCW ,
        MSRP
FROM    VehicleUC_new.dbo.Vehicles V
WHERE   V.Period = @Period
AND     V.Uid = @Uid
AND     NOT EXISTS (
            SELECT  1
            FROM    Nada.Vehicles X
            WHERE   X.Period = V.Period
            AND     X.Uid = V.Uid
        )

INSERT INTO Nada.VehicleValues
        ( Period ,
          Uid ,
          RegionId ,
          ValueTypeId ,
          ValueAmount
        )
SELECT  V.Period ,
        V.Uid ,
        V.RegionId ,
        V.ValueTypeId ,
        V.ValueAmount
FROM    VehicleUC_new.dbo.VehicleValues V
WHERE   V.Period = @Period
AND     V.Uid = @Uid
AND     NOT EXISTS (
            SELECT  1
            FROM    Nada.VehicleValues X
            WHERE   X.Period = V.Period
            AND     X.Uid = V.Uid
            AND     X.RegionId = V.RegionId
            AND     X.ValueTypeId = V.ValueTypeId
        )

IF (@Vin IS NOT NULL) BEGIN

    INSERT INTO Nada.VinVehicles ( Period, VinVehicleId, Vin )
    SELECT  Period ,
            VinVehicleId ,
            Vin
    FROM    VehicleUC_new.dbo.VinVehicles V
    WHERE   V.Period = @Period
    AND     V.Vin = SUBSTRING(@Vin, 0,9) + '0' + SUBSTRING(@VIN, 10, 1)
    AND     NOT EXISTS (
                SELECT  1
                FROM    Nada.VinVehicles X
                WHERE   X.Period = V.Period
                AND     X.VinVehicleId = V.VinVehicleId
            )

    INSERT INTO Nada.VinVehicleMaps ( Period, VinVehicleId, Uid, Priority )
    SELECT  M.Period ,
            M.VinVehicleId ,
            M.Uid ,
            M.Priority
    FROM    VehicleUC_new.dbo.VinVehicleMaps M
    JOIN    VehicleUC_new.dbo.VinVehicles V ON V.Period = M.Period AND M.VinVehicleId = V.VinVehicleId
    WHERE   V.Period = @Period
    AND     V.Vin = SUBSTRING(@Vin, 0,9) + '0' + SUBSTRING(@VIN, 10, 1)
    AND     M.Uid = @Uid
    AND     NOT EXISTS (
                SELECT  1
                FROM    Nada.VinVehicleMaps X
                WHERE   X.Period = M.Period
                AND     X.VinVehicleId = M.VinVehicleId
                AND     X.Uid = M.Uid
            )

END

INSERT INTO Nada.AccessoryCategories
        ( Period ,
          AccCategoryCode ,
          AccCategoryDescr
        )
SELECT  A.Period ,
        A.AccCategoryCode ,
        A.AccCategoryDescr
FROM    VehicleUC_new.dbo.AccessoryCategories A
WHERE   A.Period = @Period
AND     NOT EXISTS (
            SELECT  1
            FROM    Nada.AccessoryCategories X
            WHERE   X.Period = A.Period
            AND     X.AccCategoryCode = A.AccCategoryCode
        )

INSERT INTO Nada.Accessories
        ( Period ,
          AccCode ,
          AccDescr ,
          AccCategoryCode
        )

SELECT  A.Period ,
        A.AccCode ,
        A.AccDescr ,
        A.AccCategoryCode
FROM    VehicleUC_new.dbo.Accessories A
JOIN    VehicleUC_new.dbo.VehicleAccessories V ON V.Period = A.Period AND A.AccCode = V.AccCode
WHERE   V.Period = @Period
AND     V.Uid = @Uid
AND     NOT EXISTS (
            SELECT  1
            FROM    Nada.Accessories X
            WHERE   X.Period = A.Period
            AND     X.AccCode = A.AccCode
        )

INSERT INTO Nada.VehicleAccessories ( Period, Uid, AccCode, IsIncluded )
SELECT  V.Period ,
        V.Uid ,
        V.AccCode ,
        V.IsIncluded
FROM    VehicleUC_new.dbo.VehicleAccessories V
WHERE   V.Period = @Period
AND     V.Uid = @Uid
AND     NOT EXISTS (
            SELECT  1
            FROM    Nada.VehicleAccessories X
            WHERE   X.Period = V.Period
            AND     X.Uid = V.Uid
            AND     X.AccCode = V.AccCode
        )

IF (@Vin IS NOT NULL) BEGIN

    INSERT INTO Nada.VinVehicleAccessories ( Period, VinVehicleId, Uid, AccCode )
    SELECT  A.Period ,
            A.VinVehicleId ,
            A.Uid ,
            A.AccCode
    FROM    VehicleUC_new.dbo.VinVehicleAccessories A
    JOIN    VehicleUC_new.dbo.VinVehicles V ON A.Period = V.Period AND A.VinVehicleId = V.VinVehicleId
    WHERE   V.Period = @Period
    AND     V.Vin = SUBSTRING(@Vin, 0,9) + '0' + SUBSTRING(@VIN, 10, 1)
    AND     A.Uid = @Uid
    AND     NOT EXISTS (
                SELECT  1
                FROM    Nada.VinVehicleAccessories X
                WHERE   X.Period = A.Period
                AND     X.VinVehicleId = A.VinVehicleId
                AND     X.Uid = A.Uid
            )

END

INSERT INTO Nada.VehicleAccessoryValues
        ( Period ,
          Uid ,
          AccCode ,
          RegionId ,
          ValueTypeId ,
          ValueAmount
        )

SELECT  V.Period ,
        V.Uid ,
        V.AccCode ,
        V.RegionId ,
        V.ValueTypeId ,
        V.ValueAmount
FROM    VehicleUC_new.dbo.VehicleAccessoryValues V
WHERE   V.Period = @Period
AND     V.Uid = @Uid
AND     NOT EXISTS (
            SELECT  1
            FROM    Nada.VehicleAccessoryValues X
            WHERE   X.Period = V.Period
            AND     X.Uid = V.Uid
            AND     X.AccCode = V.AccCode
            AND     X.RegionId = V.RegionId
            AND     X.ValueTypeId = V.ValueTypeId
        )

INSERT INTO Nada.InclusiveAccessories ( Period, Uid, AccCode, AccInclCode )
SELECT  Period ,
        Uid ,
        AccCode ,
        AccInclCode
FROM    VehicleUC_new.dbo.InclusiveAccessories A
WHERE   A.Period = @Period
AND     A.Uid = @Uid
AND     NOT EXISTS (
            SELECT  1
            FROM    Nada.InclusiveAccessories X
            WHERE   X.Period = A.Period
            AND     X.Uid = A.Uid
            AND     X.AccCode = A.AccCode
            AND     X.AccInclCode = A.AccInclCode
        )

INSERT INTO Nada.ExclusiveAccessories
        ( Period ,
          Uid ,
          AccCode ,
          AccExcludeCode
        )
SELECT  Period ,
        Uid ,
        AccCode ,
        AccExcludeCode
FROM    VehicleUC_new.dbo.ExclusiveAccessories A
WHERE   A.Period = @Period
AND     A.Uid = @Uid
AND     NOT EXISTS (
            SELECT  1
            FROM    Nada.ExclusiveAccessories X
            WHERE   X.Period = A.Period
            AND     X.Uid = A.Uid
            AND     X.AccCode = A.AccCode
            AND     X.AccExcludeCode = A.AccExcludeCode
        )

INSERT INTO Nada.AttributeTypes
        ( Period ,
          AttributeTypeId ,
          AttributeTypeDescr
        )
SELECT  A.Period ,
        A.AttributeTypeId ,
        A.AttributeTypeDescr
FROM    VehicleUC_new.dbo.AttributeTypes A
WHERE   A.Period = @Period
AND     NOT EXISTS (
            SELECT  1
            FROM    Nada.AttributeTypes X
            WHERE   X.Period = A.Period
            AND     X.AttributeTypeId = A.AttributeTypeId
        )

INSERT INTO Nada.VehicleAttributes
        ( Period ,
          Uid ,
          AttributeTypeId ,
          IntValue ,
          VarcharValue
        )

SELECT  V.Period ,
        V.Uid ,
        V.AttributeTypeId ,
        V.IntValue ,
        V.VarcharValue
FROM    VehicleUC_new.dbo.VehicleAttributes V
WHERE   V.Period = @Period
AND     V.Uid = @Uid
AND     NOT EXISTS (
            SELECT  1
            FROM    Nada.VehicleAttributes X
            WHERE   X.Period = V.Period
            AND     X.Uid = V.Uid
            AND     X.AttributeTypeId = V.AttributeTypeId
        )

IF (@Vin IS NOT NULL) BEGIN

    INSERT INTO Nada.VinVehicleAttributes
            ( Period ,
              VinVehicleId ,
              AttributeTypeId ,
              IntValue ,
              VarcharValue
            )
    SELECT  A.Period ,
            A.VinVehicleId ,
            A.AttributeTypeId ,
            A.IntValue ,
            A.VarcharValue
    FROM    VehicleUC_new.dbo.VinVehicleAttributes A
    JOIN    VehicleUC_new.dbo.VinVehicles V ON A.Period = V.Period AND A.VinVehicleId = V.VinVehicleId
    WHERE   V.Period = @Period
    AND     V.Vin = SUBSTRING(@Vin, 0,9) + '0' + SUBSTRING(@VIN, 10, 1)
    AND     NOT EXISTS (
                SELECT  1
                FROM    Nada.VinVehicleAttributes X
                WHERE   X.Period = A.Period
                AND     X.VinVehicleId = A.VinVehicleId
                AND     X.AttributeTypeId = A.AttributeTypeId
            )

END

GO

