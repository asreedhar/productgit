SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[udp_VinVehicleMaps_g]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[udp_VinVehicleMaps_g]
GO

CREATE PROCEDURE [Nada].[udp_VinVehicleMaps_g]
	@Period	INT
	,@Vin		VARCHAR(17)
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
DECLARE @l_Vin VARCHAR(17)
SET @l_Vin =  SUBSTRING(@Vin, 0,9) + '0' + SUBSTRING(@VIN, 10, 1)
	SELECT
		A.[Period]
		,A.[Uid]
		,A.[VehicleYear]
		,A.[MakeCode]
		,B.[MakeDescr]
		,A.[SeriesCode]
		,C.[SeriesDescr]
		,A.[BodyCode]
		,D.[BodyDescr]
		,A.[MileageCode]
		,A.[Vic]
		,A.[CurbWeight]
		,A.[GVW]
		,A.[GCW]
		,A.[MSRP]
	FROM
		(SELECT
			A.[Period]
			,A.[Uid]
			,A.[VehicleYear]
			,A.[MakeCode]
			,A.[SeriesCode]
			,A.[BodyCode]
			,A.[MileageCode]
			,A.[Vic]
			,A.[CurbWeight]
			,A.[GVW]
			,A.[GCW]
			,A.[MSRP]
			,B.[Priority]
		FROM
			Nada.[Vehicles] A (nolock)
			JOIN Nada.VinVehicleMaps B ON (A.Period = B.Period AND A.Uid = B.Uid)
			JOIN Nada.VinVehicles C ON (B.Period = C.Period AND B.VinVehicleId = C.VinVehicleId)
		WHERE
			(@Period = A.[Period])
			AND (@l_Vin = Vin)) A
		JOIN Nada.Makes B ON (A.Period = B.Period AND A.MakeCode = B.MakeCode)
		JOIN Nada.Series C ON (A.Period = C.Period AND A.SeriesCode = C.SeriesCode)
		JOIN Nada.Bodies D ON (A.Period = D.Period AND A.BodyCode = D.BodyCode)
	ORDER BY
		Priority DESC 
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
