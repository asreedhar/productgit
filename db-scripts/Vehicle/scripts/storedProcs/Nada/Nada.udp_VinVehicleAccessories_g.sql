SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[udp_VinVehicleAccessories_g]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[udp_VinVehicleAccessories_g]
GO

CREATE PROCEDURE [Nada].[udp_VinVehicleAccessories_g]
	@Period	INT
	,@Vin VARCHAR(17)
	,@Uid	INT
AS
SET NOCOUNT ON
DECLARE @l_Vin		VARCHAR(17)
DECLARE @Rowcount	INT
SET @l_Vin =  SUBSTRING(@Vin, 0,9) + '0' + SUBSTRING(@Vin, 10, 1)
	SELECT
		A.[Period]
		,A.[VinVehicleId]
		,A.[Uid]
		,A.[AccCode]
	FROM
		Nada.VinVehicleAccessories A 
		JOIN
		(
			SELECT
				B.[Period]
				,B.[VinVehicleId]
				,B.[Uid]
			FROM
				Nada.VinVehicles A (nolock)
				JOIN Nada.VinVehicleMaps B (nolock) ON (A.Period = B.Period AND A.VinVehicleId = B.VinVehicleId)
			WHERE
				(@Period = B.[Period])
				AND (@l_Vin = Vin)
				AND (@Uid = B.[Uid])
		) B ON (A.Period = B.Period) AND (A.VinVehicleId = B.VinVehicleId) AND (A.Uid = B.Uid)
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
