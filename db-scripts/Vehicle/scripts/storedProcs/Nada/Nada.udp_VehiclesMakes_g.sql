SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[udp_VehiclesMakes_g]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[udp_VehiclesMakes_g]
GO

CREATE PROCEDURE [Nada].[udp_VehiclesMakes_g]
	@Period	INT
	,@VehicleYear	INT = NULL
	,@MakeCode	INT = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT 
		A.Period,
		A.MakeCode,
		A.MakeDescr
	FROM
		Nada.Makes A
		JOIN
			(SELECT
				A.[Period]
				,A.[MakeCode]
			FROM
				Nada.[Vehicles] A (nolock)
			WHERE
				@Period = A.[Period]
				AND ((@VehicleYear IS NULL) OR (@VehicleYear = [VehicleYear]))
				AND ((@MakeCode IS NULL) OR (@MakeCode = A.[MakeCode]))
			GROUP BY 
				A.[Period]
				,[MakeCode]) B ON A.Period = B.Period AND A.MakeCode = B.MakeCode
	ORDER BY
		A.Period, A.MakeDescr
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
