SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_VehicleAccessories_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_VehicleAccessories_get]
GO

CREATE PROCEDURE [Nada].[stp_VehicleAccessories_get]
	@Period	INT
	,@Uid	INT
	,@AccCode	CHAR(3) = NULL
	,@IsIncluded	BIT = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[Uid]
		,[AccCode]
		,[IsIncluded]
	FROM
		Nada.[VehicleAccessories] (nolock)
	WHERE
		(@Period = [Period])
		AND (@Uid = [Uid])
		AND ((@AccCode IS NULL) OR (@AccCode = [AccCode]))
		AND ((@IsIncluded IS NULL) OR (@IsIncluded = [IsIncluded]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
