SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_VehicleAttributes_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_VehicleAttributes_get]
GO

CREATE PROCEDURE [Nada].[stp_VehicleAttributes_get]
	@Period	INT = NULL
	,@Uid	INT = NULL
	,@AttributeTypeId	INT = NULL
	,@IntValue	INT = NULL
	,@VarcharValue	VARCHAR(50) = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[Uid]
		,[AttributeTypeId]
		,[IntValue]
		,[VarcharValue]
	FROM
		Nada.[VehicleAttributes] (nolock)
	WHERE
		((@Period IS NULL) OR (@Period = [Period]))
		AND ((@Uid IS NULL) OR (@Uid = [Uid]))
		AND ((@AttributeTypeId IS NULL) OR (@AttributeTypeId = [AttributeTypeId]))
		AND ((@IntValue IS NULL) OR (@IntValue = [IntValue]))
		AND ((@VarcharValue IS NULL) OR (@VarcharValue = [VarcharValue]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
