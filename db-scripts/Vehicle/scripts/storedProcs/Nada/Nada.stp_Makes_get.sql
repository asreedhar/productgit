SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_Makes_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_Makes_get]
GO

CREATE PROCEDURE [Nada].[stp_Makes_get]
	@Period	INT = NULL
	,@MakeCode	INT = NULL
	,@MakeDescr	VARCHAR(50) = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[MakeCode]
		,[MakeDescr]
	FROM
		Nada.[Makes] (nolock)
	WHERE
		((@Period IS NULL) OR (@Period = [Period]))
		AND ((@MakeCode IS NULL) OR (@MakeCode = [MakeCode]))
		AND ((@MakeDescr IS NULL) OR (@MakeDescr = [MakeDescr]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
