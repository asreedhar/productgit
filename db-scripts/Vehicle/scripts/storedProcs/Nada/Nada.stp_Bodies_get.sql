SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_Bodies_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_Bodies_get]
GO

CREATE PROCEDURE [Nada].[stp_Bodies_get]
	@Period	INT = NULL
	,@BodyCode	INT = NULL
	,@BodyDescr	VARCHAR(50) = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[BodyCode]
		,[BodyDescr]
	FROM
		Nada.[Bodies] (nolock)
	WHERE
		((@Period IS NULL) OR (@Period = [Period]))
		AND ((@BodyCode IS NULL) OR (@BodyCode = [BodyCode]))
		AND ((@BodyDescr IS NULL) OR (@BodyDescr = [BodyDescr]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
