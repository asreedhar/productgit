SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_Series_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_Series_get]
GO

CREATE PROCEDURE [Nada].[stp_Series_get]
	@Period	INT = NULL
	,@SeriesCode	INT = NULL
	,@SeriesDescr	VARCHAR(50) = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[SeriesCode]
		,[SeriesDescr]
	FROM
		Nada.[Series] (nolock)
	WHERE
		((@Period IS NULL) OR (@Period = [Period]))
		AND ((@SeriesCode IS NULL) OR (@SeriesCode = [SeriesCode]))
		AND ((@SeriesDescr IS NULL) OR (@SeriesDescr = [SeriesDescr]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
