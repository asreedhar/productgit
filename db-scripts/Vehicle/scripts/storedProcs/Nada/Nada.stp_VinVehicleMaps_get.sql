SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Nada].[stp_VinVehicleMaps_get]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Nada].[stp_VinVehicleMaps_get]
GO

CREATE PROCEDURE [Nada].[stp_VinVehicleMaps_get]
	@Period	INT = NULL
	,@VinVehicleId	INT = NULL
	,@Uid	INT = NULL
	,@Priority	INT = NULL
AS
SET NOCOUNT ON
DECLARE @Rowcount	INT
	SELECT
		[Period]
		,[VinVehicleId]
		,[Uid]
		,[Priority]
	FROM
		Nada.[VinVehicleMaps] (nolock)
	WHERE
		((@Period IS NULL) OR (@Period = [Period]))
		AND ((@VinVehicleId IS NULL) OR (@VinVehicleId = [VinVehicleId]))
		AND ((@Uid IS NULL) OR (@Uid = [Uid]))
		AND ((@Priority IS NULL) OR (@Priority = [Priority]))
	SELECT @Rowcount=@@Rowcount
	RETURN @Rowcount
GO
