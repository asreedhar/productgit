USE [Vehicle]
GO

/****** Object:  StoredProcedure [Client].[AppraisalsNonInv#Fetch]    Script Date: 01/24/2014 15:52:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Client].[AppraisalsNonInv#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Client].[AppraisalsNonInv#Fetch]
GO

USE [Vehicle]
GO

/****** Object:  StoredProcedure [Client].[AppraisalsNonInv#Fetch]    Script Date: 01/24/2014 15:52:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [Client].[AppraisalsNonInv#Fetch] 
	-- Add the parameters for the stored procedure here
	@DealerId int,
	@Days int,
	@ClientId int
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT  A.AppraisalID, A.ClientVehicleID, A.Created, A.[User], A.AppraisalTypeID, A.AppraisalAmount, A.TargetGrossProfit, A.EstimatedRecon, A.TargetSellingPrice
into #AppraisalResult
FROM Client.Appraisal A
JOIN Client.Vehicle V ON A.ClientVehicleID = V.VehicleID
JOIN Client.Client_Dealer CD ON CD.ClientID = V.ClientID
WHERE CD.DealerID = @DealerID
  AND (@Days IS NULL OR DATEDIFF(DAY,A.Created,GETDATE()) <= @Days) 
 
---------------------------------------------------------------------------------
SELECT  ROW_NUMBER() OVER(ORDER BY  RV.VIN) AS RowNum,V.ClientVehicleID, V.Color, V.Mileage, RV.VIN
into #VehicleAttributesResult
FROM Client.VehicleAttributes V
JOIN Client.Vehicle CV ON CV.VehicleID = V.ClientVehicleID
JOIN Reference.Vehicle RV ON RV.VehicleID = CV.ReferenceID
Join #AppraisalResult AR on AR.ClientVehicleID=V.ClientVehicleID 

CREATE TABLE #Results (        
        Id                      INT NOT NULL,
        Mileage                 INT NULL,
        VIN                     CHAR(17) NOT NULL,
        ModelYear               INT NOT NULL,
        MakeName                VARCHAR(255) NOT NULL,
        LineName                VARCHAR(255) NOT NULL,
        ModelFamilyName         VARCHAR(255) NOT NULL,
        SeriesName              VARCHAR(255) NULL,
        BodyTypeName            VARCHAR(255) NULL,
        SegmentName             VARCHAR(255) NULL,
        DriveTrainName          VARCHAR(255) NULL,
        EngineName              VARCHAR(255) NULL,
        FuelTypeName            VARCHAR(255) NULL,
        TransmissionName        VARCHAR(255) NULL,
        PassengerDoorName       VARCHAR(255) NULL
)
-----------------------------------------------------------------


Declare @Vin as varchar(17),
		@ModelYear as varchar(200),
		@MakeName as varchar(200),
		@ModelFamilyName as varchar(200),
		@SeriesName as varchar(200),
		@SegmentName as varchar(200),
		@BodyTypeName as varchar(200),
		@EngineName as varchar(200),
		@DriveTrainName as varchar(200),
		@TransmissionName as varchar(200),
		@FuelTypeName as varchar(200)


INSERT INTO #Results

SELECT  
        Id                      = V.VehicleID,
        Mileage                 = NULL,
        VIN                     = R.VIN,
        ModelYear               = CO.ModelYear,
        MakeName                = MA.Name,
        LineName                = LI.Name,
        ModelFamilyName         = MF.Name,
        SeriesName              = SR.Name,
        BodyTypeName            = BT.Name,
        SegmentName             = SG.Name,
        DriveTrainName          = DT.Name,
        EngineName              = EN.Name,
        FuelTypeName            = FT.Name,
        TransmissionName        = TR.Name,
        PassengerDoorName       = PD.Name
FROM    Reference.Vehicle R
JOIN    Client.Vehicle V ON R.VehicleID = V.ReferenceID
-- vin pattern mapping
JOIN    [VehicleCatalog].Categorization.VinPattern VP
        ON     SUBSTRING(R.VIN,1,8) + SUBSTRING(R.VIN,10,1)  = VP.SquishVin
        AND    R.VIN LIKE VP.VinPattern
JOIN    [VehicleCatalog].Categorization.VinPattern_ModelConfiguration_Lookup VPMC
        ON      VPMC.VinPatternID = VP.VinPatternID
        AND     VPMC.CountryID = 1
LEFT
JOIN    [VehicleCatalog].Firstlook.VINPatternPriority VPP
            ON      VP.VINPattern = VPP.VINPattern
            AND     VPP.CountryCode = 1
            AND     R.VIN LIKE VPP.PriorityVINPattern 
-- model configuration
JOIN    [VehicleCatalog].Categorization.ModelConfiguration MC ON VPMC.ModelConfigurationID = MC.ModelConfigurationID
JOIN    [VehicleCatalog].Categorization.Model MO ON MC.ModelID = MO.ModelID
JOIN    [VehicleCatalog].Categorization.Configuration CO ON MC.ConfigurationID = CO.ConfigurationID
-- model
JOIN    [VehicleCatalog].Categorization.Make MA ON MA.MakeID = MO.MakeID
JOIN    [VehicleCatalog].Categorization.Line LI ON LI.LineID = MO.LineID
JOIN	[VehicleCatalog].Categorization.ModelFamily MF ON MO.ModelFamilyID =MF.ModelFamilyID 
LEFT
JOIN    [VehicleCatalog].Categorization.Series SR ON SR.SeriesID = MO.SeriesID
LEFT
JOIN    [VehicleCatalog].Categorization.Segment SG ON SG.SegmentID = MO.SegmentID
LEFT
JOIN    [VehicleCatalog].Categorization.BodyType BT ON BT.BodyTypeID = MO.BodyTypeID
-- configuration
LEFT
JOIN    [VehicleCatalog].Categorization.DriveTrain DT ON DT.DriveTrainID = CO.DriveTrainID
LEFT
JOIN    [VehicleCatalog].Categorization.Engine EN ON EN.EngineID = Co.EngineID
LEFT
JOIN    [VehicleCatalog].Categorization.FuelType FT ON FT.FuelTypeID = Co.FuelTypeID
LEFT
JOIN    [VehicleCatalog].Categorization.Transmission TR ON TR.TransmissionID = CO.TransmissionID
LEFT
JOIN    [VehicleCatalog].Categorization.PassengerDoors PD ON PD.PassengerDoors = CO.PassengerDoors

JOIN    #VehicleAttributesResult  VR on VR.VIN=R.VIN
WHERE   V.ClientID = @ClientID
AND     VPP.PriorityVINPattern IS NULL
-- example
AND     (VR.VIN IS NULL OR (R.VIN LIKE '%' + VR.VIN + '%'))
AND     (@ModelYear IS NULL OR (CO.ModelYear = @ModelYear))
AND     (@MakeName IS NULL OR (MA.Name LIKE '%' + @MakeName + '%'))
AND     (@ModelFamilyName IS NULL OR (MF.Name LIKE '%' + @ModelFamilyName + '%'))
AND     (@SeriesName IS NULL OR (SR.Name LIKE '%' + @SeriesName + '%'))
AND     (@SegmentName IS NULL OR (SG.Name LIKE '%' + @SegmentName + '%'))
AND     (@BodyTypeName IS NULL OR (BT.Name LIKE '%' + @BodyTypeName + '%'))
AND     (@EngineName IS NULL OR (EN.Name LIKE '%' + @EngineName + '%'))
AND     (@DriveTrainName IS NULL OR (DT.Name LIKE '%' + @DriveTrainName + '%'))
AND     (@TransmissionName IS NULL OR (TR.Name LIKE '%' + @TransmissionName + '%'))
AND     (@FuelTypeName IS NULL OR (FT.Name LIKE '%' + @FuelTypeName + '%'))

select A.AppraisalId,A.Created,B.VIN,B.Color,A.AppraisalTypeId,A.AppraisalAmount,C.ModelYear,C.MakeName,C.ModelFamilyName,C.SeriesName
from #AppraisalResult A join #VehicleAttributesResult B on A.ClientVehicleId = B.ClientVehicleId
join #Results C on B.VIN = C.VIN 



drop table #VehicleAttributesResult
drop table #AppraisalResult
drop table #Results




END


GO



GRANT EXEC ON [Client].[AppraisalsNonInv#Fetch] TO firstlook 
GO


