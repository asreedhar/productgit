SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Galves].[CloneData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Galves].[CloneData]
GO

CREATE PROCEDURE Galves.CloneData
    @VehicleID INT
AS

SET NOCOUNT ON;

-- take a copy of the data load id

DECLARE @data_load_id INT

SELECT  @data_load_id = data_load_id
FROM    Galves.dbo.dataload

-- copy rows from each of the tables

INSERT INTO Galves.dataload
        ( data_load_id, data_load_time )
        
SELECT  s.data_load_id ,
        s.data_load_time
FROM    Galves.dbo.dataload s
WHERE   NOT EXISTS (
            SELECT  1
            FROM    Galves.dataload d
            WHERE   d.data_load_id = s.data_load_id
        )

INSERT INTO Galves.curryear
        ( data_load_id,
          curr_year,
          y1, y2, y3, y4, y5, y6, y7, y8, y9,
          y10, y11, y12, y13, y14, y15, y16, y17, y18, y19, y20 )

SELECT  @data_load_id,
        curr_year ,
        y1, y2, y3, y4, y5, y6, y7, y8, y9,
        y10, y11, y12, y13, y14, y15, y16, y17, y18, y19, y20
FROM    Galves.dbo.curryear s
WHERE   NOT EXISTS (
            SELECT  1
            FROM    Galves.curryear d
            WHERE   d.data_load_id = @data_load_id
        )

INSERT INTO Galves.exceptions
        ( data_load_id ,
          year ,
          manuf ,
          model ,
          bodystyle ,
          engine ,
          exception ,
          corrfeatur ,
          mf_type
        )

SELECT  DISTINCT
        @data_load_id,
        s.year ,
        s.manuf ,
        s.model ,
        s.bodystyle ,
        s.engine ,
        s.exception ,
        s.corrfeatur ,
        s.mf_type
FROM    Galves.dbo.exceptions s
JOIN    Galves.dbo.vehicles v
        ON      s.year = v.ph_year
        AND     s.manuf = v.ph_manuf
        AND     s.model = v.ph_model
        AND     s.bodystyle = v.ph_body
        AND     s.engine = v.ph_engtype
WHERE   v.ph_guid = @VehicleId
AND     NOT EXISTS (
            SELECT  1
            FROM    Galves.exceptions d
            WHERE   d.data_load_id = @data_load_id
            AND     d.year = s.year
            AND     d.manuf = s.manuf
            AND     d.model = s.model
            AND     d.bodystyle = s.bodystyle
            AND     d.engine = s.engine
            AND     d.exception = s.exception
            AND     d.corrfeatur = s.corrfeatur
            AND     d.mf_type = s.mf_type
        )

INSERT INTO Galves.region_milefactors
        ( data_load_id ,
          ph_mclass ,
          year ,
          adj_Region2 ,
          adj_Region3 ,
          adj_Region4 ,
          adj_Region5 ,
          adj_Region6
        )

SELECT  DISTINCT
        @data_load_id,
        s.ph_mclass ,
        s.year ,
        s.adj_Region2 ,
        s.adj_Region3 ,
        s.adj_Region4 ,
        s.adj_Region5 ,
        s.adj_Region6
FROM    Galves.dbo.vehicles v
JOIN    (
            SELECT  u.model_year, mileage_year = UPPER(u.year)
            FROM    Galves.dbo.curryear Y
                    UNPIVOT (
                        model_year FOR year IN (
                                y1, y2, y3, y4, y5, y6, y7, y8, y9, y10,
                                y11, y12, y13, y14, y15, y16, y17, y18, y19, y20)) U
        ) y ON y.model_year = v.ph_year
JOIN    Galves.dbo.region_milefactors s
        ON      v.ph_mclass = s.ph_mclass
        AND     y.mileage_year = s.year
WHERE   v.ph_guid = @VehicleId
AND     NOT EXISTS (
            SELECT  1
            FROM    Galves.region_milefactors d
            WHERE   d.data_load_id = @data_load_id
            AND     d.year = s.year
            AND     d.ph_mclass = s.ph_mclass
        )

INSERT INTO Galves.region_states
        ( data_load_id, state, region )
        
SELECT  @data_load_id,
        state ,
        region
FROM    Galves.dbo.region_states s
WHERE   NOT EXISTS (
            SELECT  1
            FROM    Galves.region_states d
            WHERE   d.data_load_id = @data_load_id
            AND     d.state = s.state
        )

INSERT INTO Galves.vehicle_mileage
        ( data_load_id ,
          class ,
          year ,
          miles ,
          abvfactor ,
          blwfactor
        )

SELECT  DISTINCT
        @data_load_id ,
        s.class ,
        s.year ,
        s.miles ,
        s.abvfactor ,
        s.blwfactor
FROM    Galves.dbo.vehicles v
JOIN    (
            SELECT  u.model_year, mileage_year = UPPER(u.year)
            FROM    Galves.dbo.curryear Y
                    UNPIVOT (
                        model_year FOR year IN (
                                y1, y2, y3, y4, y5, y6, y7, y8, y9, y10,
                                y11, y12, y13, y14, y15, y16, y17, y18, y19, y20)) U
        ) y ON y.model_year = v.ph_year
JOIN    Galves.dbo.cltrans c ON c.cl_code = v.ph_ccode
JOIN    Galves.dbo.vehicle_mileage s ON s.year = y.mileage_year AND s.class = c.s_code
WHERE   v.ph_guid = @VehicleID
AND     NOT EXISTS (
            SELECT  1
            FROM    Galves.vehicle_mileage d
            WHERE   d.data_load_id = @data_load_id
            AND     d.class = s.class
            AND     d.year = s.year
            AND     d.miles = s.miles
        )

INSERT INTO Galves.vehicles
        ( data_load_id ,
          ph_book ,
          ph_change ,
          ph_group ,
          ph_date ,
          ph_page ,
          ph_seq ,
          ph_keyseq ,
          ph_ccode ,
          ph_manuf ,
          ph_year ,
          ph_engtype ,
          ph_model ,
          ph_body ,
          ph_mclass ,
          ph_mcamt ,
          ph_mbase ,
          ph_lprice ,
          ph_madj ,
          ph_cadj ,
          ph_dadj ,
          ph_cprice ,
          ph_vincode ,
          ph_comm1 ,
          ph_comm2 ,
          ph_comm3 ,
          ph_comm4 ,
          ph_guid ,
          ph_price2 ,
          ph_cprice_region2 ,
          ph_cprice_region3 ,
          ph_cprice_region4 ,
          ph_cprice_region5 ,
          ph_cprice_region6 ,
          ph_price2_region2 ,
          ph_price2_region3 ,
          ph_price2_region4 ,
          ph_price2_region5 ,
          ph_price2_region6 ,
          ph_aft
        )

SELECT  @data_load_id ,
        s.ph_book ,
        s.ph_change ,
        s.ph_group ,
        s.ph_date ,
        s.ph_page ,
        s.ph_seq ,
        s.ph_keyseq ,
        s.ph_ccode ,
        s.ph_manuf ,
        s.ph_year ,
        s.ph_engtype ,
        s.ph_model ,
        s.ph_body ,
        s.ph_mclass ,
        s.ph_mcamt ,
        s.ph_mbase ,
        s.ph_lprice ,
        s.ph_madj ,
        s.ph_cadj ,
        s.ph_dadj ,
        s.ph_cprice ,
        s.ph_vincode ,
        s.ph_comm1 ,
        s.ph_comm2 ,
        s.ph_comm3 ,
        s.ph_comm4 ,
        s.ph_guid ,
        s.ph_price2 ,
        s.ph_cprice_region2 ,
        s.ph_cprice_region3 ,
        s.ph_cprice_region4 ,
        s.ph_cprice_region5 ,
        s.ph_cprice_region6 ,
        s.ph_price2_region2 ,
        s.ph_price2_region3 ,
        s.ph_price2_region4 ,
        s.ph_price2_region5 ,
        s.ph_price2_region6 ,
        s.ph_aft
FROM    Galves.dbo.vehicles s
WHERE   s.ph_guid = @VehicleID
AND     NOT EXISTS (
            SELECT  1
            FROM    Galves.vehicles d
            WHERE   d.data_load_id = @data_load_id
            AND     d.ph_guid = s.ph_guid
        )

INSERT INTO Galves.vindecoder
        ( data_load_id, ph_guid, VIN )

SELECT  @data_load_id,
        s.ph_guid ,
        s.VIN
FROM    Galves.dbo.vindecoder s
WHERE   s.ph_guid = @VehicleID
AND     NOT EXISTS (
            SELECT  1
            FROM    Galves.vehicles d
            WHERE   d.data_load_id = @data_load_id
            AND     d.ph_guid = s.ph_guid
        )

INSERT INTO Galves.addncost
        ( data_load_id ,
          year ,
          manuf ,
          description ,
          amt ,
          type ,
          model ,
          bodytype ,
          engine ,
          amt_code ,
          [except] ,
          mf_type ,
          amt_region2 ,
          amt_region3 ,
          amt_region4 ,
          amt_region5 ,
          amt_region6
        )

SELECT  DISTINCT
        @data_load_id ,
        s.year ,
        s.manuf ,
        s.description ,
        s.amt ,
        s.type ,
        s.model ,
        s.bodytype ,
        s.engine ,
        s.amt_code ,
        s.[except] ,
        s.mf_type ,
        s.amt_region2 ,
        s.amt_region3 ,
        s.amt_region4 ,
        s.amt_region5 ,
        s.amt_region6
FROM    Galves.dbo.vehicles v
JOIN    Galves.dbo.addncost s
        ON  ((s.type = 'GEN' AND s.year = v.ph_year AND s.manuf = v.ph_manuf) OR
             (s.type = 'SPC' AND s.year = v.ph_year AND s.manuf = v.ph_manuf AND s.model = v.ph_model AND s.bodytype = v.ph_body AND s.engine = v.ph_engtype))
WHERE   v.ph_guid = @VehicleID
AND     NOT EXISTS (
            SELECT  1
            FROM    Galves.addncost d
            WHERE   d.data_load_id = @data_load_id
            AND     d.type = s.type
            AND     d.year = s.year
            AND     d.manuf = s.manuf
            AND     ((d.type = 'GEN') OR (
                                d.type = 'SPC'
                        AND     d.model = s.model
                        AND     d.bodytype = s.bodytype
                        AND     d.engine = s.engine
                        AND     d.description = s.description))
        )


INSERT INTO Galves.bgmanuf
        ( data_load_id ,
          year ,
          mf_name ,
          mf_model ,
          mf_type ,
          webmanuf ,
          webmodel ,
          webbasemod
        )

SELECT  @data_load_id ,
        s.year ,
        s.mf_name ,
        s.mf_model ,
        s.mf_type ,
        s.webmanuf ,
        s.webmodel ,
        s.webbasemod
FROM    Galves.dbo.vehicles v
JOIN    Galves.dbo.bgmanuf s ON s.mf_name = v.ph_manuf and s.mf_type = v.ph_aft and s.mf_model = v.ph_model and s.year = v.ph_year
WHERE   v.ph_guid = @VehicleID
AND     NOT EXISTS (
            SELECT  1
            FROM    Galves.bgmanuf d
            WHERE   d.data_load_id = @data_load_id
            AND     s.year = d.year
            AND     s.mf_name = d.mf_name
            AND     s.mf_model = d.mf_model
            AND     s.mf_type = d.mf_type
        )

INSERT INTO Galves.bookdate
        ( data_load_id, book_date )

SELECT  @data_load_id, s.book_date
FROM    Galves.dbo.bookdate s
WHERE   NOT EXISTS (
            SELECT  1
            FROM    Galves.bookdate d
            WHERE   d.data_load_id = @data_load_id
        )

INSERT INTO Galves.cltrans
        ( data_load_id, cl_code, s_code )

SELECT  @data_load_id ,
        s.cl_code ,
        s.s_code
FROM    Galves.dbo.vehicles v
JOIN    Galves.dbo.cltrans s ON s.cl_code = v.ph_ccode
WHERE   v.ph_guid = @VehicleID
AND     NOT EXISTS (
            SELECT  1
            FROM    Galves.cltrans d
            WHERE   d.data_load_id = @data_load_id
            AND     s.cl_code = d.cl_code
            AND     s.s_code = d.s_code
        )

GO

