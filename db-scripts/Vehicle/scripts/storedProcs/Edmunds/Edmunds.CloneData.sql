SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Edmunds].[CloneData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Edmunds].[CloneData]
GO

CREATE PROCEDURE Edmunds.CloneData
    @ED_STYLE_ID INT
AS

SET NOCOUNT ON;

--DECLARE @ED_STYLE_ID INT
--
--SET @ED_STYLE_ID = 100299264 -- test data

-- take a copy of the data load id

DECLARE @DataLoadID INT

SELECT  @DataLoadID = DataLoadID
FROM    VehicleCatalog.Edmunds.FirstLook_DataLoad

-- copy rows from each of the tables

INSERT INTO Edmunds.FirstLook_DataLoad ( DataLoadID, DataLoadTime )
        
SELECT  s.DataLoadID ,
        s.DataLoadTime
FROM    VehicleCatalog.Edmunds.FirstLook_DataLoad s
WHERE   NOT EXISTS (
            SELECT  1
            FROM    Edmunds.FirstLook_DataLoad d
            WHERE   d.DataLoadID = s.DataLoadID
        )

INSERT INTO Edmunds.TMV_VALID_CONDITION
        ( DataLoadID ,
          TMV_CONDITION_ID ,
          TMV_CONDITION
        )
SELECT  @DataLoadID ,
        TMV_CONDITION_ID ,
        TMV_CONDITION
FROM    VehicleCatalog.Edmunds.TMV_VALID_CONDITION S
WHERE   NOT EXISTS (
            SELECT  1
            FROM    Edmunds.TMV_VALID_CONDITION D
            WHERE   DataLoadID = @DataLoadID
            AND     D.TMV_CONDITION_ID = S.TMV_CONDITION_ID
        )

INSERT INTO Edmunds.TMV_VALID_GENERIC_COLOR
        ( DataLoadID ,
          TMV_GENERIC_COLOR_ID ,
          TMV_GENERIC_COLOR
        )
SELECT  @DataLoadID,
        TMV_GENERIC_COLOR_ID,
        TMV_GENERIC_COLOR
FROM    VehicleCatalog.Edmunds.TMV_VALID_GENERIC_COLOR S
WHERE   NOT EXISTS (
            SELECT  1
            FROM    Edmunds.TMV_VALID_GENERIC_COLOR D
            WHERE   DataLoadID = @DataLoadID
            AND     D.TMV_GENERIC_COLOR_ID= S.TMV_GENERIC_COLOR_ID
        )

INSERT INTO Edmunds.TMVU_VALID_OPTION
        ( DataLoadID ,
          FEATURE_ID ,
          FEATURE_DESCRIPTION
        )
SELECT  @DataLoadID ,
        S.FEATURE_ID ,
        S.FEATURE_DESCRIPTION
FROM    VehicleCatalog.Edmunds.TMVU_VALID_OPTION S
WHERE   NOT EXISTS (
            SELECT  1
            FROM    Edmunds.TMVU_VALID_OPTION D
            WHERE   DataLoadID = @DataLoadID
            AND     D.FEATURE_ID= S.FEATURE_ID
        )

INSERT INTO Edmunds.TMV_VALID_REGION
        ( DataLoadID ,
          TMV_REGION_ID ,
          TMV_REGION
        )
SELECT  @DataLoadID ,
        S.TMV_REGION_ID ,
        S.TMV_REGION
FROM    VehicleCatalog.Edmunds.TMV_VALID_REGION S
WHERE   NOT EXISTS (
            SELECT  1
            FROM    Edmunds.TMV_VALID_REGION D
            WHERE   DataLoadID = @DataLoadID
            AND     D.TMV_REGION_ID = S.TMV_REGION_ID
        )

INSERT INTO Edmunds.TMVU_VEHICLE
        ( DataLoadID ,
          ED_STYLE_ID ,
          Year ,
          MidYear ,
          MAKE ,
          MODEL ,
          STYLE_USED ,
          TMV_CATEGORY_ID ,
          RETAIL_VALUE ,
          PPARTY_VALUE ,
          TRADEIN_VALUE
        )
SELECT  @DataLoadID ,
        S.ED_STYLE_ID ,
        S.Year ,
        S.MidYear ,
        S.MAKE ,
        S.MODEL ,
        S.STYLE_USED ,
        S.TMV_CATEGORY_ID ,
        S.RETAIL_VALUE ,
        S.PPARTY_VALUE ,
        S.TRADEIN_VALUE
FROM    VehicleCatalog.Edmunds.TMVU_VEHICLE S
WHERE   S.ED_STYLE_ID = @ED_STYLE_ID
AND     NOT EXISTS (
            SELECT  1
            FROM    Edmunds.TMVU_VEHICLE D
            WHERE   D.DataLoadID = @DataLoadID
            AND     D.ED_STYLE_ID = S.ED_STYLE_ID
        )

INSERT INTO Edmunds.TMVU_VEHICLE_ERRORS
        ( DataLoadID ,
          ED_STYLE_ID ,
          Year ,
          MidYear ,
          MAKE ,
          MODEL ,
          STYLE_USED ,
          TMV_CATEGORY_ID ,
          RETAIL_VALUE ,
          PPARTY_VALUE ,
          TRADEIN_VALUE
        )

SELECT  @DataLoadID,
        S.ED_STYLE_ID ,
        S.Year ,
        S.MidYear ,
        S.MAKE ,
        S.MODEL ,
        S.STYLE_USED ,
        S.TMV_CATEGORY_ID ,
        S.RETAIL_VALUE ,
        S.PPARTY_VALUE ,
        S.TRADEIN_VALUE
FROM    VehicleCatalog.Edmunds.TMVU_VEHICLE_ERRORS S
WHERE   S.ED_STYLE_ID = @ED_STYLE_ID
AND     NOT EXISTS (
            SELECT  1
            FROM    Edmunds.TMVU_VEHICLE_ERRORS D
            WHERE   D.DataLoadID = @DataLoadID
            AND     D.ED_STYLE_ID = S.ED_STYLE_ID
        )

INSERT INTO Edmunds.TMVU_CPO_COST
        ( DataLoadID ,
          ED_STYLE_ID ,
          CPO_COST ,
          CPO_PREMIUM_PERCENT
        )
SELECT  @DataLoadID,
        S.*
FROM    VehicleCatalog.Edmunds.TMVU_CPO_COST S
WHERE   S.ED_STYLE_ID = @ED_STYLE_ID
AND     NOT EXISTS (
            SELECT  1
            FROM    Edmunds.TMVU_CPO_COST D
            WHERE   D.DataLoadID = @DataLoadID
            AND     D.ED_STYLE_ID = S.ED_STYLE_ID
        )

INSERT INTO Edmunds.TMVU_CPO_MILEAGE
        ( DataLoadID ,
          AGE ,
          MAKE ,
          MAX_YEARS ,
          MAX_MILEAGE ,
          INTERVAL ,
          INTERVAL_MIN ,
          INTERVAL_MAX ,
          CPO_MILEAGE_PERCENT
        )
SELECT  @DataLoadID ,
        S.AGE ,
        S.MAKE ,
        S.MAX_YEARS ,
        S.MAX_MILEAGE ,
        S.INTERVAL ,
        S.INTERVAL_MIN ,
        S.INTERVAL_MAX ,
        S.CPO_MILEAGE_PERCENT
FROM    VehicleCatalog.Edmunds.TMVU_CPO_MILEAGE S
JOIN    VehicleCatalog.Edmunds.TMVU_VEHICLE V ON V.MAKE = S.MAKE
WHERE   V.ED_STYLE_ID = @ED_STYLE_ID
AND     S.AGE = COALESCE(NULLIF(YEAR(GETDATE()) - V.Year, 0), 1)
AND     NOT EXISTS (
            SELECT  1
            FROM    Edmunds.TMVU_CPO_MILEAGE D
            WHERE   D.DataLoadID = @DataLoadID
            AND     D.AGE = S.AGE
            AND     D.MAKE = S.MAKE
            AND     D.INTERVAL = S.INTERVAL
        )
        
INSERT INTO Edmunds.TMVU_MILEAGE
        ( DataLoadID ,
          Year ,
          TMV_CATEGORY_ID ,
          LOW_MILEAGE ,
          HIGH_MILEAGE ,
          ADD_FACTOR_1 ,
          ADD_FACTOR_2 ,
          ADD_FACTOR_3 ,
          ADD_FACTOR_4 ,
          DEDUCT_FACTOR_1 ,
          DEDUCT_FACTOR_2 ,
          DEDUCT_FACTOR_3 ,
          DEDUCT_FACTOR_4 ,
          HIGH_BOUND_1 ,
          HIGH_BOUND_2 ,
          HIGH_BOUND_3 ,
          HIGH_BOUND_4 ,
          LOW_BOUND_1 ,
          LOW_BOUND_2 ,
          LOW_BOUND_3 ,
          LOW_BOUND_4
        )
SELECT  @DataLoadID,
        S.Year ,
        S.TMV_CATEGORY_ID ,
        S.LOW_MILEAGE ,
        S.HIGH_MILEAGE ,
        S.ADD_FACTOR_1 ,
        S.ADD_FACTOR_2 ,
        S.ADD_FACTOR_3 ,
        S.ADD_FACTOR_4 ,
        S.DEDUCT_FACTOR_1 ,
        S.DEDUCT_FACTOR_2 ,
        S.DEDUCT_FACTOR_3 ,
        S.DEDUCT_FACTOR_4 ,
        S.HIGH_BOUND_1 ,
        S.HIGH_BOUND_2 ,
        S.HIGH_BOUND_3 ,
        S.HIGH_BOUND_4 ,
        S.LOW_BOUND_1 ,
        S.LOW_BOUND_2 ,
        S.LOW_BOUND_3 ,
        S.LOW_BOUND_4
FROM    VehicleCatalog.Edmunds.TMVU_MILEAGE S
JOIN    VehicleCatalog.Edmunds.TMVU_VEHICLE V ON V.Year = S.Year AND V.TMV_CATEGORY_ID = S.TMV_CATEGORY_ID
WHERE   V.ED_STYLE_ID = @ED_STYLE_ID
AND     NOT EXISTS (
            SELECT  1
            FROM    Edmunds.TMVU_MILEAGE D
            WHERE   D.DataLoadID = @DataLoadID
            AND     D.Year = S.Year
            AND     D.TMV_CATEGORY_ID = S.TMV_CATEGORY_ID
        )

INSERT INTO Edmunds.TMVU_OPTIONAL_FEATURE
        ( DataLoadID ,
          ED_STYLE_ID ,
          FEATURE_ID ,
          RETAIL_VALUE_FEATURES ,
          PPARTY_VALUE_FEATURES ,
          TRADEIN_VALUE_FEATURES
        )
SELECT  @DataLoadID,
        S.ED_STYLE_ID ,
        S.FEATURE_ID ,
        S.RETAIL_VALUE_FEATURES ,
        S.PPARTY_VALUE_FEATURES ,
        S.TRADEIN_VALUE_FEATURES
FROM    VehicleCatalog.Edmunds.TMVU_OPTIONAL_FEATURE S
WHERE   S.ED_STYLE_ID = @ED_STYLE_ID
AND     NOT EXISTS (
            SELECT  1
            FROM    Edmunds.TMVU_OPTIONAL_FEATURE D
            WHERE   D.DataLoadID = @DataLoadID
            AND     D.ED_STYLE_ID = S.ED_STYLE_ID
        )

INSERT INTO Edmunds.TMVU_OPTIONAL_FEATURE_ERRORS
        ( DataLoadID ,
          ED_STYLE_ID ,
          FEATURE_ID ,
          RETAIL_VALUE_FEATURES ,
          PPARTY_VALUE_FEATURES ,
          TRADEIN_VALUE_FEATURES
        )
SELECT  @DataLoadID ,
        S.ED_STYLE_ID ,
        S.FEATURE_ID ,
        S.RETAIL_VALUE_FEATURES ,
        S.PPARTY_VALUE_FEATURES ,
        S.TRADEIN_VALUE_FEATURES
FROM    VehicleCatalog.Edmunds.TMVU_OPTIONAL_FEATURE_ERRORS S
WHERE   S.ED_STYLE_ID = @ED_STYLE_ID
AND     NOT EXISTS (
            SELECT  1
            FROM    Edmunds.TMVU_OPTIONAL_FEATURE_ERRORS D
            WHERE   D.DataLoadID = @DataLoadID
            AND     D.ED_STYLE_ID = S.ED_STYLE_ID
            AND     D.FEATURE_ID = S.FEATURE_ID
        )

INSERT INTO Edmunds.TMVU_CONDITION
        ( DataLoadID ,
          TMV_CATEGORY_ID ,
          Year ,
          TMV_CONDITION_ID ,
          RETAIL_PERCENT ,
          PPARTY_PERCENT ,
          TRADEIN_PERCENT
        )
SELECT  @DataLoadID ,
        S.TMV_CATEGORY_ID ,
        S.Year ,
        S.TMV_CONDITION_ID ,
        S.RETAIL_PERCENT ,
        S.PPARTY_PERCENT ,
        S.TRADEIN_PERCENT
FROM    VehicleCatalog.Edmunds.TMVU_CONDITION S
JOIN    VehicleCatalog.Edmunds.TMVU_VEHICLE V ON V.Year = S.Year AND V.TMV_CATEGORY_ID = S.TMV_CATEGORY_ID
WHERE   V.ED_STYLE_ID = @ED_STYLE_ID
AND     NOT EXISTS (
            SELECT  1
            FROM    Edmunds.TMVU_CONDITION D
            WHERE   D.DataLoadID = @DataLoadID
            AND     D.TMV_CATEGORY_ID = S.TMV_CATEGORY_ID
            AND     D.TMV_CONDITION_ID = S.TMV_CONDITION_ID
            AND     D.Year = S.Year
        )

INSERT INTO Edmunds.TMVU_CONDITION_EXCEPTION
        ( DataLoadID ,
          MAKE ,
          TMV_CATEGORY_ID ,
          TMV_CONDITION_ID ,
          Year ,
          RETAIL_PERCENT ,
          PPARTY_PERCENT ,
          TRADEIN_PERCENT
        )
SELECT  @DataLoadID ,
        S.MAKE ,
        S.TMV_CATEGORY_ID ,
        S.TMV_CONDITION_ID ,
        S.Year ,
        S.RETAIL_PERCENT ,
        S.PPARTY_PERCENT ,
        S.TRADEIN_PERCENT
FROM    VehicleCatalog.Edmunds.TMVU_CONDITION_EXCEPTION S
JOIN    VehicleCatalog.Edmunds.TMVU_VEHICLE V ON V.Year = S.Year AND V.MAKE = S.MAKE AND V.TMV_CATEGORY_ID = S.TMV_CATEGORY_ID
WHERE   V.ED_STYLE_ID = @ED_STYLE_ID
AND     NOT EXISTS (
            SELECT  1
            FROM    Edmunds.TMVU_CONDITION_EXCEPTION D
            WHERE   D.DataLoadID = @DataLoadID
            AND     D.TMV_CATEGORY_ID = S.TMV_CATEGORY_ID
            AND     D.TMV_CONDITION_ID = S.TMV_CONDITION_ID
            AND     D.Year = S.Year
            AND     D.MAKE = S.MAKE
            
        )

INSERT INTO Edmunds.TMVU_COLOR
        ( DataLoadID ,
          TMV_CONDITION_ID ,
          TMV_GENERIC_COLOR_ID ,
          TMV_REGION_ID ,
          TMV_CATEGORY_ID ,
          YEAR ,
          COLOR_PERCENT
        )
SELECT  @DataLoadID ,
        S.TMV_CONDITION_ID ,
        S.TMV_GENERIC_COLOR_ID ,
        S.TMV_REGION_ID ,
        S.TMV_CATEGORY_ID ,
        S.YEAR ,
        S.COLOR_PERCENT
FROM    VehicleCatalog.Edmunds.TMVU_COLOR S
JOIN    VehicleCatalog.Edmunds.TMVU_VEHICLE V ON V.TMV_CATEGORY_ID = S.TMV_CATEGORY_ID AND V.Year = S.YEAR
WHERE   V.ED_STYLE_ID = @ED_STYLE_ID
AND     NOT EXISTS (
            SELECT  1
            FROM    Edmunds.TMVU_COLOR D
            WHERE   D.DataLoadID = @DataLoadID
            AND     D.TMV_CATEGORY_ID = S.TMV_CATEGORY_ID
            AND     D.TMV_CONDITION_ID = S.TMV_CONDITION_ID
            AND     D.TMV_REGION_ID = S.TMV_REGION_ID
            AND     D.TMV_GENERIC_COLOR_ID = S.TMV_GENERIC_COLOR_ID
            AND     D.YEAR = S.YEAR
        )

INSERT INTO Edmunds.TMVU_COLOR_EXCEPTION
        ( DataLoadID ,
          ED_STYLE_ID ,
          TMV_REGION_ID ,
          TMV_CONDITION_ID ,
          TMV_GENERIC_COLOR_ID ,
          COLOR_PERCENT
        )
SELECT  @DataLoadID ,
        S.ED_STYLE_ID ,
        S.TMV_REGION_ID ,
        S.TMV_CONDITION_ID ,
        S.TMV_GENERIC_COLOR_ID ,
        S.COLOR_PERCENT
FROM    VehicleCatalog.Edmunds.TMVU_COLOR_EXCEPTION S
WHERE   S.ED_STYLE_ID = @ED_STYLE_ID
AND     NOT EXISTS (
            SELECT  1
            FROM    Edmunds.TMVU_COLOR_EXCEPTION D
            WHERE   D.DataLoadID = @DataLoadID
            AND     D.ED_STYLE_ID = S.ED_STYLE_ID
            AND     D.TMV_CONDITION_ID = S.TMV_CONDITION_ID
            AND     D.TMV_REGION_ID = S.TMV_REGION_ID
            AND     D.TMV_GENERIC_COLOR_ID = S.TMV_GENERIC_COLOR_ID
        )

INSERT INTO Edmunds.TMVU_REGION
        ( DataLoadID ,
          TMV_REGION_ID ,
          TMV_CATEGORY_ID ,
          Year ,
          REGION_PERCENT
        )
SELECT  @DataLoadID ,
        S.TMV_REGION_ID ,
        S.TMV_CATEGORY_ID ,
        S.Year ,
        S.REGION_PERCENT
FROM    VehicleCatalog.Edmunds.TMVU_REGION S
JOIN    VehicleCatalog.Edmunds.TMVU_VEHICLE V ON S.TMV_CATEGORY_ID = V.TMV_CATEGORY_ID AND S.Year = V.Year
WHERE   V.ED_STYLE_ID = @ED_STYLE_ID
AND     NOT EXISTS (
            SELECT  1
            FROM    Edmunds.TMVU_REGION D
            WHERE   D.DataLoadID = @DataLoadID
            AND     D.TMV_CATEGORY_ID = S.TMV_CATEGORY_ID
            AND     D.Year = S.Year
            AND     D.TMV_REGION_ID = S.TMV_REGION_ID
        )

INSERT INTO Edmunds.TMVU_REGION_EXCEPTION
        ( DataLoadID ,
          ED_STYLE_ID ,
          TMV_REGION_ID ,
          REGION_PERCENT
        )
SELECT  @DataLoadID ,
        S.*
FROM    VehicleCatalog.Edmunds.TMVU_REGION_EXCEPTION S
WHERE   S.ED_STYLE_ID = @ED_STYLE_ID
AND     NOT EXISTS (
            SELECT  1
            FROM    Edmunds.TMVU_REGION_EXCEPTION D
            WHERE   D.DataLoadID = @DataLoadID
            AND     D.ED_STYLE_ID = S.ED_STYLE_ID
            AND     D.TMV_REGION_ID = S.TMV_REGION_ID
        )

INSERT INTO Edmunds.TMVU_STATE_REGION
        ( DataLoadID ,
          STATE_CODE ,
          TMV_REGION_ID
        )
SELECT  @DataLoadID ,
        S.STATE_CODE ,
        S.TMV_REGION_ID
FROM    VehicleCatalog.Edmunds.TMVU_STATE_REGION S
WHERE   NOT EXISTS (
            SELECT  1
            FROM    Edmunds.TMVU_STATE_REGION D
            WHERE   D.DataLoadID = @DataLoadID
            AND     D.STATE_CODE = S.STATE_CODE
            AND     D.TMV_REGION_ID = S.TMV_REGION_ID
        )

INSERT INTO Edmunds.FirstLook_COLOR_U
        ( DataLoadID ,
          COLOR_ID ,
          ED_STYLE_ID ,
          BASIC_COLOR_NAME ,
          MFGR_COLOR_NAME ,
          MFGR_COLOR_CODE ,
          INTERIOR_EXTERIOR_FLAG ,
          R_CODE ,
          G_CODE ,
          B_CODE ,
          C_CODE ,
          M_CODE ,
          Y_CODE ,
          K_CODE
        )

SELECT  @DataLoadID ,
        S.color_id ,
        S.ed_style_id ,
        S.basic_color_name ,
        S.mfgr_color_name ,
        S.mfgr_color_code ,
        S.interior_exterior_flag ,
        S.r_code ,
        S.g_code ,
        S.b_code ,
        S.c_code ,
        S.m_code ,
        S.y_code ,
        S.k_code
FROM    VehicleCatalog.Edmunds.FirstLook_COLOR_U S
WHERE   S.ed_style_id = @ED_STYLE_ID
AND     NOT EXISTS (
            SELECT  1
            FROM    Edmunds.FirstLook_COLOR_U D
            WHERE   D.DataLoadID = @DataLoadID
            AND     D.ED_STYLE_ID = S.ed_style_id
            AND     D.COLOR_ID = S.color_id
        )

INSERT INTO Edmunds.FirstLook_SQUISH_VIN_V2
        ( DataLoadID ,
          SQUISH_VIN ,
          ED_STYLE_ID ,
          USED_ED_STYLE_ID ,
          ED_MODEL_ID ,
          YEAR ,
          MAKE ,
          MODEL ,
          STYLE ,
          MFR_STYLE_CODE ,
          DOORS ,
          BODY_TYPE ,
          DRIVE_TYPE_CODE ,
          STANDARD_FLAG ,
          ENGINE_BLOCK_CONFIGURATION ,
          CYLINDER_QTY ,
          ENGINE_DISPLACEMENT ,
          ENGINE_DISPLACEMENT_UOM ,
          CAM_TYPE ,
          FUEL_INDUCTION ,
          VALVES_CYLINDER ,
          ASPIRATION ,
          FUEL_TYPE ,
          TRAN_TYPE ,
          TRAN_SPEED ,
          RESTRAINT_SYSTEM ,
          GVWR ,
          PLANT
        )

SELECT  @DataLoadID ,
        S.squish_vin ,
        S.ed_style_id ,
        S.used_ed_style_id ,
        S.ed_model_id ,
        S.year ,
        S.make ,
        S.model ,
        S.style ,
        S.mfr_style_code ,
        S.doors ,
        S.body_type ,
        S.drive_type_code ,
        S.standard_flag ,
        S.engine_block_configuration ,
        S.cylinder_qty ,
        S.engine_displacement ,
        S.engine_displacement_uom ,
        S.cam_type ,
        S.fuel_induction ,
        S.valves_cylinder ,
        S.aspiration ,
        S.fuel_type ,
        S.tran_type ,
        S.tran_speed ,
        S.restraint_system ,
        S.gvwr ,
        S.plant
FROM    VehicleCatalog.Edmunds.FirstLook_SQUISH_VIN_V2 S
WHERE   S.ed_style_id = @ED_STYLE_ID
AND     NOT EXISTS (
            SELECT  1
            FROM    Edmunds.FirstLook_SQUISH_VIN_V2 D
            WHERE   D.DataLoadID = @DataLoadID
            AND     D.ED_STYLE_ID = S.ed_style_id
            AND     D.SQUISH_VIN = S.squish_vin
        )

GO
