SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Kbb].[CloneData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Kbb].[CloneData]
GO

CREATE PROCEDURE [Kbb].[CloneData]
    @DataLoadID INT,
    @VehicleID INT   
AS

SET NOCOUNT ON;

-----------------------------------------------------------------------------------------------------------------------
--  Pull out identifiers tied to a vehicle.
-----------------------------------------------------------------------------------------------------------------------

DECLARE @MakeID INT
DECLARE @ModelID INT
DECLARE @TrimID INT
DECLARE @MileageGroupID INT
DECLARE @VehicleTypeID INT

SELECT DISTINCT
    @MakeID         = MA.MakeID,
    @ModelID        = MO.ModelID,    
    @TrimID         = T.TrimID,
    @MileageGroupID = V.MileageGroupID,
    @VehicleTypeID  = V.VehicleTypeId
FROM
    KBB.KBB.Vehicle V
LEFT JOIN
    KBB.KBB.Trim T ON T.TrimId = V.TrimID AND T.DataloadID = V.DataloadID
LEFT JOIN
    KBB.KBB.Model MO ON MO.ModelID = T.ModelID AND MO.DataloadID = T.DataloadID
LEFT JOIN
    KBB.KBB.Make MA ON MA.MakeID = MO.MakeID AND MA.DataloadID = MO.DataloadID
WHERE
    V.DataloadID = @DataLoadID
AND    
    V.VehicleId = @VehicleID
    
-----------------------------------------------------------------------------------------------------------------------
--  Pull out category identifiers.
-----------------------------------------------------------------------------------------------------------------------

DECLARE @Categories TABLE
(
    CategoryID  INT NOT NULL    
)
INSERT INTO 
    @Categories
SELECT 
    CategoryId
FROM
    KBB.KBB.VehicleCategory
WHERE
    DataloadID = @DataLoadID
AND
    VehicleID = @VehicleID
    
-----------------------------------------------------------------------------------------------------------------------
--  Pull out mileage range identifiers.
-----------------------------------------------------------------------------------------------------------------------

DECLARE @MileageRanges TABLE
(
    MileageRangeID INT NOT NULL
)
INSERT INTO 
    @MileageRanges
SELECT
    MileageRangeId
FROM
    KBB.KBB.MileageGroupAdjustment
WHERE
    DataloadID = @DataLoadID
AND
    MileageGroupId = @MileageGroupID
    
-----------------------------------------------------------------------------------------------------------------------
--  Pull out mileage range identifiers.
-----------------------------------------------------------------------------------------------------------------------
DECLARE @Specifications TABLE
(
    SpecificationID INT NULL
)
INSERT INTO 
    @Specifications
SELECT DISTINCT 
    SpecificationID 
FROM 
    KBB.KBB.SpecificationValue
WHERE 
    DataLoadID = @DataLoadID
AND 
    VehicleID = @VehicleID

-----------------------------------------------------------------------------------------------------------------------
--  Pull out vehicle option identifiers.
-----------------------------------------------------------------------------------------------------------------------        
DECLARE @VehicleOptions TABLE
(
    VehicleOptionID INT NULL
)
INSERT INTO 
    @VehicleOptions
SELECT 
    VehicleOptionID
FROM 
    KBB.KBB.VehicleOption
WHERE 
    DataloadID = @DataLoadID
AND 
    VehicleId = @VehicleID   

-----------------------------------------------------------------------------------------------------------------------
--  Pull out vehicle option identifiers.
-----------------------------------------------------------------------------------------------------------------------
DECLARE @OptionEquipment TABLE
(
    VinOptionEquipmentPatternID INT NULL
)
INSERT INTO 
    @OptionEquipment
SELECT 
    VinOptionEquipmentPatternID
FROM 
    KBB.KBB.VinOptionEquipment
WHERE 
    DataloadID = @DataLoadID
AND 
    VehicleId = @VehicleID
AND
    VehicleOptionId IN (SELECT VehicleOptionID FROM @VehicleOptions)
    
-----------------------------------------------------------------------------------------------------------------------
--  Kbb.DataLoad
-----------------------------------------------------------------------------------------------------------------------

INSERT INTO Kbb.DataLoad
(
    DataLoadID,
    DataVersionID,
    DataLoadStatusID,
    DateLoaded
)
SELECT 
    s.DataloadID,
    s.DataVersionID,
    s.DataloadStatusID,
    s.DateLoaded
FROM 
    KBB.KBB.Dataload s
WHERE
    s.DataloadID = @DataLoadID
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.DataLoad d
    WHERE d.DataLoadID = s.DataloadID
)

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.DataVersion
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.DataVersion
(
    DataLoadID,
    DataVersionID,
    ReleaseVersion,
    SchemaVersion,
    VehicleTypeID,
    StartDate,
    EndDate,
    Edition
)
SELECT
    s.DataloadID,
    s.DataVersionId,
    s.ReleaseVersion,
    s.SchemaVersion,
    s.VehicleTypeId,
    s.StartDate,
    s.EndDate,
    s.Edition
FROM 
    KBB.KBB.DataVersion s
WHERE 
    s.DataloadID = @DataLoadID
AND
    s.VehicleTypeId = @VehicleTypeID
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.DataVersion d
    WHERE d.DataLoadID = s.DataloadID
    AND d.DataVersionID = s.DataVersionId
    AND d.VehicleTypeID = s.VehicleTypeId
)

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.Category
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.Category
(
    DataLoadID,
    CategoryID,
    CategoryTypeID,
    CategoryTypeDisplayName,
    DisplayName,
    SortOrder
)
SELECT 
    s.DataloadID,
    s.CategoryId,
    s.CategoryTypeId,
    s.CategoryTypeDisplayName,
    s.DisplayName,
    s.SortOrder
FROM 
    KBB.KBB.Category s
WHERE
    s.DataloadID = @DataLoadID
AND
    s.CategoryId IN (SELECT CategoryID FROM @Categories)
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.Category d
    WHERE d.DataLoadID = s.DataloadID
    AND d.CategoryID = s.CategoryId
)
    
-----------------------------------------------------------------------------------------------------------------------
--  Kbb.Year
--      Copy over all years for a dataload.
-----------------------------------------------------------------------------------------------------------------------    
INSERT INTO Kbb.[Year]
(
    DataLoadID,
    YearID,
    DisplayName,
    VinCode,
    MileageZeroPoint,
    MaximumDeductionPercentage
)
SELECT
    s.DataloadID,
    s.YearId,
    s.DisplayName,
    s.VINCode,
    s.MileageZeroPoint,
    s.MaximumDeductionPercentage
FROM 
    KBB.KBB.[Year] s
WHERE
    s.DataloadID = @DataLoadID
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.[Year] d
    WHERE d.DataLoadID = s.DataloadID
    AND d.YearID = s.YearId
)    
    
-----------------------------------------------------------------------------------------------------------------------
--  Kbb.Make
--      Copy over makes used by a vehicle if they haven't already been copied.
-----------------------------------------------------------------------------------------------------------------------    
INSERT INTO Kbb.Make
(
    DataLoadID,
    MakeID,
    DisplayName
)
SELECT
    s.DataloadID,
    s.MakeId,
    s.DisplayName
FROM 
    KBB.KBB.Make s
WHERE
    s.DataloadID = @DataLoadID
AND
    s.MakeId = @MakeID
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.Make d
    WHERE d.DataLoadID = s.DataloadID
    AND d.MakeID = s.MakeId
)
       
-----------------------------------------------------------------------------------------------------------------------
--  Kbb.MileageGroupAdjustment
-----------------------------------------------------------------------------------------------------------------------    
INSERT INTO Kbb.MileageGroupAdjustment
(
    DataLoadID,
    MileageRangeID,
    MileageGroupID,
    MileageGroupDisplayName,
    Adjustment,
    AdjustmentTypeID,
    AdjustmentTypeDisplayName
)
SELECT 
    s.DataloadID,
    s.MileageRangeId,
    s.MileageGroupId,
    s.MileageGroupDisplayName,
    s.Adjustment,
    s.AdjustmentTypeId,
    s.AdjustmentTypeDisplayName
FROM 
    KBB.KBB.MileageGroupAdjustment s
WHERE
    s.DataloadID = @DataLoadID
AND
    s.MileageGroupId = @MileageGroupID
AND
    s.MileageRangeID IN (SELECT MileageRangeID FROM @MileageRanges)
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.MileageGroupAdjustment d
    WHERE d.DataLoadID = s.DataloadID
    AND d.MileageRangeID = s.MileageRangeId
    AND d.MileageGroupID = s.MileageGroupId
)

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.MileageRange
-----------------------------------------------------------------------------------------------------------------------    
INSERT INTO Kbb.MileageRange
(
    DataloadID,
    MileageRangeId,
    YearId,
    VehicleTypeId,
    MileageMin,
    MileageMax
)
SELECT
    s.DataloadID,
    s.MileageRangeId,
    s.YearId,
    s.VehicleTypeId,
    s.MileageMin,
    s.MileageMax
FROM 
    KBB.KBB.MileageRange s
WHERE
    s.DataloadID = @DataLoadID
AND
    s.MileageRangeId IN (SELECT MileageRangeID FROM @MileageRanges)
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.MileageRange d
    WHERE d.DataLoadID = s.DataloadID
    AND d.MileageRangeID = s.MileageRangeId    
)
    
    
-----------------------------------------------------------------------------------------------------------------------
--  Kbb.Model
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.Model
(
    DataLoadID,
    ModelID,
    DisplayName,
    Description,
    MakeID,
    SortOrder,
    MarketName,
    ShortName
)
SELECT
    s.DataloadID,
    s.ModelId,
    s.DisplayName,
    s.Description,
    s.MakeId,
    s.SortOrder,
    s.MarketName,
    s.ShortName
FROM 
    KBB.KBB.Model s
WHERE
    s.DataloadID = @DataLoadID
AND
    s.ModelId = @ModelID
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.Model d
    WHERE d.DataLoadID = s.DataloadID
    AND d.ModelID = s.ModelId
)

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.ModelYear
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.ModelYear
(
    DataLoadID,
    ModelYearID,
    ModelID,
    YearID,
    Note,
    EffectiveDate,
    EffectiveDateMissingReason,
    RevisedFlag
)
SELECT
    s.DataloadID,
    s.ModelYearId,
    s.ModelId,
    s.YearId,
    s.Note,
    s.EffectiveDate,
    s.EffectiveDateMissingReason,
    s.RevisedFlag
FROM 
    KBB.KBB.ModelYear s
WHERE
    s.DataloadID = @DataLoadID
AND
    s.ModelId = @ModelID
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.ModelYear d
    WHERE d.DataLoadID = s.DataloadID
    AND d.ModelID = s.ModelId
)
    
-----------------------------------------------------------------------------------------------------------------------
--  Kbb.Trim
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.Trim
(
    DataLoadID,
    TrimID,
    ModelID,
    YearID,
    DisplayName,
    SortOrder,
    TrimName
)
SELECT
    s.DataloadID,
    s.TrimId,
    s.ModelId,
    s.YearId,
    s.DisplayName,
    s.SortOrder,
    s.TrimName
FROM 
    KBB.KBB.Trim s
WHERE 
    s.DataloadID = @DataLoadID
AND
    s.TrimID =  @TrimID
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.Trim d
    WHERE d.DataLoadID = s.DataloadID
    AND d.TrimID = s.TrimId
)

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.Vehicle
-----------------------------------------------------------------------------------------------------------------------    
INSERT INTO Kbb.Vehicle
(
    DataLoadID,
    VehicleID,
    TrimID,
    ManufacturerAssignedModelCode,
    SubTrim,
    DisplayName,
    VehicleTypeID,
    VehicleTypeDisplayName,
    SortOrder,
    DisplayNameAdditionalData,
    KbbSortOrder,
    AvailabilityStatus,
    AvailabilityStatusEndDate,
    MarketName,
    RelatedVehicleID,
    AvailabilityStatusStartDate,
    CPOAdjustment,
    MileageGroupID,
    BlueBookName,
    ShortName
)
SELECT 
    s.DataloadID,
    s.VehicleId,
    s.TrimId,
    s.ManufacturerAssignedModelCode,
    s.SubTrim,
    s.DisplayName,
    s.VehicleTypeId,
    s.VehicleTypeDisplayName,
    s.SortOrder,
    s.DisplayNameAdditionalData,
    s.KBBSortOrder,
    s.AvailabilityStatus,
    s.AvailabilityStatusEndDate,
    s.MarketName,
    s.RelatedVehicleId,
    s.AvailabilityStatusStartDate,
    s.CPOAdjustment,
    s.MileageGroupId,
    s.BlueBookName,
    s.ShortName
FROM 
    KBB.KBB.Vehicle s
WHERE
    s.DataloadID = @DataLoadID
AND
    s.VehicleId = @VehicleID
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.Vehicle d
    WHERE d.DataLoadID = s.DataloadID
    AND d.VehicleID = s.VehicleId
)
    
-----------------------------------------------------------------------------------------------------------------------
--  Kbb.Specification
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.Specification
(
    DataLoadID,
    SpecificationID,
    SpecificationTypeID,
    SpecificationTypeDisplayName,
    DisplayName,
    Units
)
SELECT
    s.DataloadID,
    s.SpecificationId,
    s.SpecificationTypeId,
    s.SpecificationTypeDisplayName,
    s.DisplayName,
    s.Units
FROM 
    KBB.KBB.Specification s
WHERE
    s.DataloadID = @DataLoadID
AND
    s.SpecificationID IN (SELECT SpecificationID FROM @Specifications)
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.Specification d
    WHERE d.DataLoadID = s.DataloadID
    AND d.SpecificationID = s.SpecificationId
)
        
-----------------------------------------------------------------------------------------------------------------------
--  Kbb.SpecificationValue
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.SpecificationValue
(
    DataLoadID,
    VehicleID,
    SpecificationID,
    ValueIndex,
    Value
)
SELECT
    s.DataloadID,
    s.VehicleId,
    s.SpecificationId,
    s.ValueIndex,
    s.Value
FROM 
    KBB.KBB.SpecificationValue s
WHERE
    s.DataloadID = @DataLoadID
AND
    s.VehicleId = @VehicleID
AND
    s.SpecificationID IN (SELECT SpecificationID FROM @Specifications)
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.SpecificationValue d
    WHERE d.DataLoadID = s.DataloadID
    AND d.VehicleID = s.VehicleId
    AND d.SpecificationID = s.SpecificationId
    AND d.ValueIndex = s.ValueIndex
)
    
-----------------------------------------------------------------------------------------------------------------------
--  Kbb.PriceType
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.PriceType
(
    DataLoadID,
    PriceTypeID,
    DisplayName
)
SELECT
    s.DataloadID,
    s.PriceTypeId,
    s.DisplayName
FROM 
    KBB.KBB.PriceType s
WHERE
    s.DataloadID = @DataLoadID
AND NOT EXISTS
(   
    SELECT 1
    FROM Kbb.PriceType d
    WHERE d.DataLoadID = s.DataloadID
    AND d.PriceTypeID = s.PriceTypeId
)

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.VehicleRegion
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.VehicleRegion
(
    DataLoadID,
    VehicleTypeRegionID,
    VehicleTypeID,
    RegionID
)
SELECT
    s.DataloadID,
    s.VehicleTypeRegionId,
    s.VehicleTypeId,
    s.RegionId
FROM 
    KBB.KBB.VehicleRegion s
WHERE
    s.DataloadID = @DataLoadID
AND
    s.VehicleTypeID = @VehicleTypeID
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.VehicleRegion d
    WHERE d.DataLoadID = s.DataloadID
    AND d.VehicleTypeRegionID = s.VehicleTypeRegionId
)

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.RegionBasePrice
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.RegionBasePrice
(
    DataLoadID,
    VehicleID,
    VehicleTypeRegionID,
    PriceTypeID,
    Price
)
SELECT
    s.DataloadID,
    s.VehicleId,
    s.VehicleTypeRegionId,
    s.PriceTypeId,
    s.Price
FROM 
    KBB.KBB.RegionBasePrice s
JOIN
    KBB.KBB.VehicleRegion vr ON vr.DataloadID = s.DataloadID AND vr.VehicleTypeRegionId = s.VehicleTypeRegionId
WHERE
    s.DataloadID = @DataLoadID
AND
    s.VehicleId = @VehicleID
AND
    vr.VehicleTypeId = @VehicleTypeID
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.RegionBasePrice d
    WHERE d.DataLoadID = s.DataloadID
    AND d.VehicleID = s.VehicleId
    AND d.VehicleTypeRegionID = s.VehicleTypeRegionId
    AND d.PriceTypeID = s.PriceTypeId
)
    
-----------------------------------------------------------------------------------------------------------------------
--  Kbb.Region
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.Region
(
    DataLoadID,
    RegionID,
    RegionTypeID,
    DisplayName,
    RegionTypeDisplayName
)
SELECT
    r.DataLoadID,
    r.RegionID,
    r.RegionTypeID,
    r.DisplayName,
    r.RegionTypeDisplayName
FROM 
    KBB.KBB.Region r
WHERE
    r.DataloadID = @DataLoadID
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.Region d
    WHERE d.DataLoadID = r.DataloadID
    AND d.RegionID = r.RegionId
)

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.ZipCode
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.ZipCode
(
    DataLoadID,
    ZipCodeID,
    ZipCode
)
SELECT
    z.DataLoadID,
    z.ZipCodeID,
    z.ZipCode
FROM 
    KBB.KBB.ZipCode z
WHERE
    z.DataloadID = @DataLoadID
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.ZipCode d
    WHERE d.DataLoadID = z.DataloadID
    AND d.ZipCodeID = z.ZipCodeID
)

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.RegionAdjustmentTypePriceType
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.RegionAdjustmentTypePriceType
(
    DataLoadID,
    RegionAdjustmentTypeID,
    PriceTypeID,
	DisplayName
)
SELECT
    rat.DataLoadID,
    rat.RegionAdjustmentTypeID,
    rat.PriceTypeID,
	rat.DisplayName
FROM 
    KBB.KBB.RegionAdjustmentTypePriceType rat
WHERE
    rat.DataloadID = @DataLoadID
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.RegionAdjustmentTypePriceType d
    WHERE d.DataLoadID = rat.DataloadID
    AND d.RegionAdjustmentTypeID = rat.RegionAdjustmentTypeID
	AND d.PriceTypeID = rat.PriceTypeID
)
    
-----------------------------------------------------------------------------------------------------------------------
--  Kbb.RegionGroupAdjustment
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.RegionGroupAdjustment
(
    DataLoadID,
    RegionID,
	GroupID,
	RegionAdjustmentTypeID,
	AdjustmentTypeID,
	AdjustmentTypeRoundingTypeID,
	Adjustment,	
	AdjustmentTypeDisplayName
)
SELECT
    rga.DataLoadID,
    rga.RegionID,
	rga.GroupID,
	rga.RegionAdjustmentTypeID,
	rga.AdjustmentTypeID,
	rga.AdjustmentTypeRoundingTypeID,
	rga.Adjustment,	
	rga.AdjustmentTypeDisplayName
FROM 
    KBB.KBB.RegionGroupAdjustment rga
WHERE
    rga.DataloadID = @DataLoadID
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.RegionGroupAdjustment d
    WHERE d.DataLoadID = rga.DataloadID
    AND d.RegionID = rga.RegionID
	AND d.GroupID = rga.GroupID
	AND d.RegionAdjustmentTypeID = rga.RegionAdjustmentTypeID
)

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.RegionZipCode
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.RegionZipCode
(
    DataLoadID,
    RegionID,
	ZipCodeID 
)
SELECT
    rzc.DataLoadID,
    rzc.RegionID,
	rzc.ZipCodeID 
FROM 
    KBB.KBB.RegionZipCode rzc
WHERE
    rzc.DataloadID = @DataLoadID
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.RegionZipCode d
    WHERE d.DataLoadID = rzc.DataloadID
    AND d.RegionID = rzc.RegionID
	AND d.ZipCodeID = rzc.ZipCodeID
)

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.VehicleGroup
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.VehicleGroup
(
    DataLoadID,
    VehicleID,
    GroupID,
	GroupTypeID,
    DisplayName,
    GroupTypeDisplayName 
)
SELECT
    vg.DataLoadID,
    vg.VehicleID,
    vg.GroupID,
	vg.GroupTypeID,
    vg.DisplayName,
    vg.GroupTypeDisplayName 
FROM 
    KBB.KBB.VehicleGroup vg
WHERE
    vg.DataloadID = @DataLoadID
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.VehicleGroup d
    WHERE d.DataLoadID = vg.DataloadID
    AND d.VehicleID = vg.VehicleID
	AND d.GroupID = vg.GroupID
)

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.VehicleCategory
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.VehicleCategory
(
    DataLoadID,
    CategoryID,
    VehicleID,
    VehicleCategorySequence
)
SELECT
    s.DataloadID,
    s.CategoryId,
    s.VehicleId,
    s.VehicleCategorySequence
FROM 
    KBB.KBB.VehicleCategory s
WHERE
    s.DataloadID = @DataLoadID
AND
    s.VehicleID = @VehicleID
AND
    s.CategoryId IN (SELECT CategoryID FROM @Categories)
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.VehicleCategory d
    WHERE d.DataLoadID = s.DataloadID
    AND d.VehicleID = s.VehicleId
    AND d.CategoryID = s.CategoryId
)
    
-----------------------------------------------------------------------------------------------------------------------
--  Kbb.VehicleOption
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.VehicleOption
(
    DataLoadID,
    VehicleOptionID,
    OptionTypeID,
    OptionTypeDisplayName,
    DisplayName,
    DisplayNameAdditionalData,
    ManufacturerAssignedOptionCode,
    OptionAvailabilityID,
    OptionAvailabilityDisplayName,
    OptionAvailabilityCode,
    IsDefaultConfiguration,
    VehicleID,
    DetailName,
    NonBoldName,
    Footer,
    SortOrder,
    NCBBAdjustmentTypeID,
    NCBBAdjustmentTypeDisplayName,
    NCBBCanExceedMSRPFlag,
    NCBBAdjustmentValue
)
SELECT
    s.DataloadID,
    s.VehicleOptionId,
    s.OptionTypeId,
    s.OptionTypeDisplayName,
    s.DisplayName,
    s.DisplayNameAdditionalData,
    s.ManufacturerAssignedOptionCode,
    s.OptionAvailabilityId,
    s.OptionAvailabilityDisplayName,
    s.OptionAvailabilityCode,
    s.IsDefaultConfiguration,
    s.VehicleId,
    s.DetailName,
    s.NonBoldName,
    s.Footer,
    s.SortOrder,
    s.NCBBAdjustmentTypeId,
    s.NCBBAdjustmentTypeDisplayName,
    s.NCBBCanExceedMSRPFlag,
    s.NCBBAdjustmentValue
FROM 
    KBB.KBB.VehicleOption s
WHERE
    s.DataloadID = @DataLoadID
AND
    s.VehicleOptionId IN (SELECT VehicleOptionID FROM @VehicleOptions)
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.VehicleOption d
    WHERE d.DataLoadID = s.DataloadID
    AND d.VehicleOptionID = s.VehicleOptionId
)

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.VehicleOptionCategory
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.VehicleOptionCategory
(
    DataLoadID,
    VehicleOptionID,
    CategoryID,
    VehicleOptionCategorySequence
)
SELECT  s.DataloadID ,
                    s.VehicleOptionId ,
                    s.CategoryId ,
                    s.VehicleOptionCategorySequence
            FROM    KBB.KBB.VehicleOptionCategory s
            WHERE   s.DataloadID = @DataLoadID
                    AND s.VehicleOptionId IN ( SELECT   VehicleOptionID
                                               FROM     KBB.KBB.VehicleOption
                                               WHERE    DataloadID = @DataLoadID
                                                        AND VehicleId = @VehicleID )
                    AND s.CategoryId IN ( SELECT    CategoryId
                                          FROM      KBB.KBB.VehicleCategory
                                          WHERE     DataloadID = @DataLoadID
                                                    AND VehicleID = @VehicleID)
                    AND NOT EXISTS ( SELECT 1
                                     FROM   Kbb.VehicleOptionCategory d
                                     WHERE  d.DataLoadID = s.DataloadID
                                            AND d.VehicleOptionID = s.VehicleOptionId
                                            AND d.CategoryID = s.CategoryId )

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.OptionSpecificationValue
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.OptionSpecificationValue
(
    DataLoadID,
    VehicleOptionID,
    SpecificationID,
    ValueIndex,
    Value
)
SELECT 
    s.DataloadID,
    s.VehicleOptionId,
    s.SpecificationId,
    s.ValueIndex,
    s.Value
FROM 
    KBB.KBB.OptionSpecificationValue s
WHERE
    s.DataloadID = @DataLoadID
AND
    s.VehicleOptionId IN (SELECT   VehicleOptionID
                                               FROM     KBB.KBB.VehicleOption
                                               WHERE    DataloadID = @DataLoadID
                                                        AND VehicleId = @VehicleID)
AND
    s.SpecificationId IN (SELECT SpecificationID FROM @Specifications)
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.OptionSpecificationValue d
    WHERE d.DataLoadID = s.DataloadID
    AND d.VehicleOptionID = s.VehicleOptionId
    AND d.SpecificationID = s.SpecificationId
    AND d.ValueIndex = s.ValueIndex
)

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.OptionRegionPriceAdjustment
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.OptionRegionPriceAdjustment
(
    DataLoadID,
    VehicleOptionID,
    VehicleTypeRegionID,
    VehicleID,
    PriceTypeID,
    PriceAdjustment,
    ValueTypeID
)
SELECT
    s.DataloadID,
    s.VehicleOptionId,
    s.VehicleTypeRegionId,
    s.VehicleId,
    s.PriceTypeId,
    s.PriceAdjustment,
    s.ValueTypeId
FROM 
    KBB.KBB.OptionRegionPriceAdjustment s
JOIN
    KBB.KBB.VehicleRegion vr ON vr.DataloadID = s.DataloadID AND vr.VehicleTypeRegionId = s.VehicleTypeRegionID
WHERE
    s.DataloadID = @DataLoadID
AND
    s.VehicleId = @VehicleID
AND
    s.VehicleOptionId IN (SELECT   VehicleOptionID
                                               FROM     KBB.KBB.VehicleOption
                                               WHERE    DataloadID = @DataLoadID
                                                        AND VehicleId = @VehicleID)
AND
    vr.VehicleTypeId = @VehicleTypeID
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.OptionRegionPriceAdjustment d
    WHERE d.DataLoadID = s.DataloadID
    AND d.VehicleOptionID = s.VehicleOptionId
    AND d.VehicleTypeRegionID = s.VehicleTypeRegionId
    AND d.VehicleID = s.VehicleId
    AND d.PriceTypeID = s.PriceTypeId
)

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.VinOptionEquipmentPattern
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.VinOptionEquipmentPattern
(
    DataLoadID,
    VinOptionEquipmentPatternID,
    Pattern
)
SELECT
    s.DataloadID,
    s.VINOptionEquipmentPatternId,
    s.Pattern
FROM 
    KBB.KBB.VINOptionEquipmentPattern s
WHERE
    s.DataloadID = @DataLoadID
AND
    s.VINOptionEquipmentPatternId IN (SELECT VINOptionEquipmentPatternID FROM @OptionEquipment)
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.VinOptionEquipmentPattern d
    WHERE d.DataLoadID = s.DataloadID
    AND d.VINOptionEquipmentPatternId = s.VinOptionEquipmentPatternID
)

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.VinOptionEquipment
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.VinOptionEquipment
(
    DataLoadID,
    VinOptionEquipmentPatternID,
    VehicleID,
    VehicleOptionID
)
SELECT 
    s.DataloadID,
    s.VINOptionEquipmentPatternId,
    s.VehicleId,
    s.VehicleOptionId
FROM 
    KBB.KBB.VINOptionEquipment s
WHERE
    s.DataloadID = @DataLoadID
AND
    s.VehicleId = @VehicleID
AND
    s.VehicleOptionId IN (SELECT VehicleOptionID FROM @VehicleOptions)
AND
    s.VINOptionEquipmentPatternId IN (SELECT VinOptionEquipmentPatternID FROM @OptionEquipment)
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.VinOptionEquipment d
    WHERE d.DataLoadID = s.DataloadID
    AND d.VehicleID = s.VehicleID
    AND d.VinOptionEquipmentPatternID = s.VINOptionEquipmentPatternId
    AND d.VehicleOptionID = s.VehicleOptionId
)

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.VinOptionEquipment
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO Kbb.VinVehiclePattern
(
    DataLoadID,
    YearID,
    MakeID,
    Pattern,
    VehicleID
)
SELECT
    s.DataloadID,
    s.YearId,
    s.MakeId,
    s.Pattern,
    s.VehicleId
FROM 
    KBB.KBB.VINVehiclePattern s
WHERE
    s.DataloadID = @DataLoadID
AND
    s.VehicleId = @VehicleID
AND
    s.MakeId = @MakeID
AND NOT EXISTS
(
    SELECT 1
    FROM Kbb.VinVehiclePattern d
    WHERE d.DataLoadID = s.DataloadID
    AND d.VehicleID = s.VehicleId
    AND d.YearID = s.YearId
    AND d.MakeID = s.MakeId
    AND d.Pattern = s.Pattern
)


GO


