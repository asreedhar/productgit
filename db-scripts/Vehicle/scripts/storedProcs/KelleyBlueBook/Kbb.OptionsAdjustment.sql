SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[KBB].[OptionsAdjustment]') AND type in (N'P',N'PC'))
DROP PROCEDURE [KBB].[OptionsAdjustment]
GO

CREATE PROCEDURE KBB.OptionsAdjustment
	@DataLoadID INT,
	@VehicleID INT
AS
SET NOCOUNT ON

SELECT 
    OptionID = A.VehicleOptionID,
    PriceTypeID = A.PriceTypeID,
    Price = A.PriceAdjustment
FROM   
      KBB.KBB.Vehicle V
JOIN   
      KBB.KBB.VehicleRegion R ON V.DataloadID = R.DataloadID AND R.VehicleTypeId = V.VehicleTypeId
JOIN 
      KBB.KBB.VehicleOption VO ON V.DataloadID = VO.DataloadID AND V.VehicleID = VO.VehicleID     
JOIN
      KBB.KBB.OptionRegionPriceAdjustment A ON V.DataloadID = A.DataloadID AND V.VehicleId = A.VehicleId AND VO.VehicleOptionId = A.VehicleOptionId AND R.VehicleTypeRegionId = A.VehicleTypeRegionId
             
WHERE 
      V.DataloadID = @DataLoadID
AND   
      V.VehicleId = @VehicleID

GO
GRANT EXEC ON KBB.OptionsAdjustment TO KbbUser
GO