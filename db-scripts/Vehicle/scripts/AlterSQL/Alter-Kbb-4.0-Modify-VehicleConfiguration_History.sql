ALTER TABLE Kbb.VehicleConfiguration_History
DROP CONSTRAINT FK_Kbb_VehicleConfiguration_History__State

ALTER TABLE Kbb.VehicleConfiguration_History
ALTER COLUMN RegionID INT NULL 

UPDATE 	Kbb.VehicleConfiguration_History
SET  	RegionID = r.RegionID
FROM 	Kbb.VehicleConfiguration_History vch
JOIN 	Kbb.RegionState rs ON vch.DataLoadID = rs.DataLoadID AND vch.RegionID = rs.RegionID AND vch.StateID = rs.StateID
JOIN Kbb.Region r ON rs.DataLoadID = r.DataLoadID AND 'Region ' + LTRIM(RTRIM(rs.StateDisplayName)) = r.DisplayName

ALTER TABLE Kbb.VehicleConfiguration_History
ALTER COLUMN RegionID INT NOT NULL 

ALTER TABLE Kbb.VehicleConfiguration_History
DROP COLUMN StateID

ALTER TABLE Kbb.VehicleConfiguration_History
ADD ZipCodeID INT NULL
