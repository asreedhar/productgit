ALTER TABLE Vehicle.Nada.VehicleConfiguration_History ADD StateCode CHAR(3) NULL
GO
UPDATE Vehicle.Nada.VehicleConfiguration_History 
SET StateCode=SC.StateCode
FROM Vehicle.Nada.VehicleConfiguration_History VH 
	INNER JOIN Vehicle.Nada.StateCodes SC ON VH.StateID=SC.StateId
GO
ALTER TABLE Vehicle.Nada.VehicleConfiguration_History ALTER COLUMN StateCode CHAR(3) NOT NULL

