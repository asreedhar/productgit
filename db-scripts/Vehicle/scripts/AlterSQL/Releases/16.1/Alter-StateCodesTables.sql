 -- Drop the stateID foreign key and then drop the column
 ALTER TABLE Vehicle.Nada.VehicleConfiguration_History 
 DROP FK_Nada_VehicleConfiguration_History__State
 GO
 ALTER TABLE Vehicle.Nada.VehicleConfiguration_History
 DROP COLUMN StateID
 GO
 -- Add the foreign key for the new StateCode column
 ALTER TABLE Vehicle.Nada.VehicleConfiguration_History 
 ADD CONSTRAINT FK_Nada_VehicleConfiguration_History__StateCode 
 FOREIGN KEY (Period, StateCode) REFERENCES Vehicle.Nada.States(Period, StateCode)
 GO
 -- Remove the foreign key in the States table that points to the StateCodes table.
 ALTER TABLE Vehicle.Nada.States 
 DROP CONSTRAINT [FK_Nada_States__StateCodes]
 GO
 -- Finally, drop the stateCodes table
 DROP TABLE Vehicle.Nada.StateCodes
 GO
 DROP TABLE VehicleUC_New.dbo.StateCodes