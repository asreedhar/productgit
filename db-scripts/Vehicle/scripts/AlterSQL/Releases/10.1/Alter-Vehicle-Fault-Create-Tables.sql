/*
	Purpose:
	
	As part of addressing the problem that inner exception messages are not captured, 
	the following tables will be created:
	
	Fault.Message					- Recorded fault(exception) messages
	Fault.FaultEvent_Fault_Message	- Relationship between FaultEvent, Fault(s), MessageIDs
	
	Author:
	D. Reddy	02/03/2012		Initial Version
*/

CREATE TABLE Fault.Message (
        MessageID		INT IDENTITY		NOT NULL,
        [Message]		VARCHAR(1024),
        [HashCode]		AS (CAST(HashBytes('SHA1', Message) AS BINARY(20))),		
        CONSTRAINT PK_Fault_Message PRIMARY KEY (MessageID),
		CONSTRAINT UK_FaultMessage__HashCode UNIQUE (HashCode)
)
ON [DATA]
GO

CREATE TABLE Fault.FaultEvent_Fault_Message (
        FaultEventID	INT  				NOT NULL,
        FaultID			INT  				NOT NULL,
        MessageID       INT					NOT NULL,
        [Index]			INT					NOT NULL,
        CONSTRAINT PK_Fault_FaultEvent_Fault_Message PRIMARY KEY (FaultEventID, FaultID, MessageID, [Index]),
		CONSTRAINT FK_Fault_FaultEvent_Fault_Message__FaultEvent
			FOREIGN KEY (FaultEventID)
			REFERENCES Fault.FaultEvent (FaultEventID),      
		CONSTRAINT FK_Fault_FaultEvent_Fault__Fault
			FOREIGN KEY (FaultID)
			REFERENCES Fault.Fault (FaultID),
		CONSTRAINT FK_Fault_FaultEvent_Fault_Message__Message
			FOREIGN KEY (MessageID)
			REFERENCES Fault.Message (MessageID)			
)
ON [DATA]
GO