/*
	Purpose:
	
	As part of addressing the problem that inner exception messages are not captured, existing 
	faultevent 	messages need to be migrated to the updated Fault schema.
	
	This script will migrate messages as they currently exist (initial schema) in 
	Vehicle.Fault.FaultEvent to Vehile.Fault.Message.  It will also create ternary relationships 
	between a FaultEvent, each Fault associated with a FaultEvent, and the corresponding Message 
	associated with each Fault via the Vehicle.Fault.FaultEvent_Fault_Message table.
	
	After the migration completes, this script will drop the Message column from the
	Vehicle.Fault.FaultEvent table.
	
	Author:
	D. Reddy	02/03/2012		Initial Version
*/

DECLARE @TranName VARCHAR(30);
SET @TranName = 'MigrateFaultEventMessages'

BEGIN TRY

	BEGIN TRANSACTION @TranName

	--Declare variables
	DECLARE @FaultEventID INT;
	DECLARE @FaultID INT;
	DECLARE @Message VARCHAR(1024);
	DECLARE @StaticMsgID INT;

	INSERT INTO Vehicle.Fault.Message
			( [Message] )
	VALUES  ( NULL					-- Message - varchar(1024)
			  )

	SELECT @StaticMsgID = M.MessageID
	FROM Vehicle.Fault.Message M
	WHERE M.Message IS NULL

	--Declare cursor
	DECLARE faultEvent_cursor CURSOR FAST_FORWARD FOR
	SELECT	FEvent.FaultEventID, FEvent.FaultID, FEvent.Message 
	FROM	Vehicle.Fault.FaultEvent FEvent (NOLOCK)

	OPEN faultEvent_cursor;

	FETCH NEXT FROM faultEvent_cursor
	INTO @FaultEventID, @FaultID, @Message;

	WHILE @@FETCH_STATUS = 0
	BEGIN

		DECLARE @MessageID INT;
		DECLARE @Cause INT;
		
		SET @MessageID = NULL;
		SET @Cause = NULL;
		
		IF @Message IS NOT NULL
			BEGIN
				SELECT @MessageID = M.MessageID
				FROM   Vehicle.Fault.Message M
				WHERE  M.Message = @Message
			END
		ELSE	
			SET @MessageID = @StaticMsgID;
		
		IF @MessageID IS NULL
			BEGIN
				INSERT INTO Vehicle.Fault.Message
						( [Message] )
				VALUES  ( @Message	-- Message - varchar(1024)
						)
									
				SELECT @MessageID = M.MessageID
				FROM   Vehicle.Fault.Message M
				WHERE  M.Message = @Message
				
			END
		
		INSERT INTO Vehicle.Fault.FaultEvent_Fault_Message
				( FaultEventID ,
				  FaultID ,
				  MessageID ,
				  [Index]
				)
		VALUES  ( @FaultEventID ,	-- FaultEventID - int
				  @FaultID ,		-- FaultID		- int
				  @MessageID ,		-- MessageID	- int
				  0					-- Index		- int
				)

		SELECT @Cause = F.CauseID
		FROM Vehicle.Fault.Fault F (NOLOCK)
		WHERE F.FaultID = @FaultID
		
		DECLARE @Index INT;
		
		SET @Index = 0;
		
		WHILE (@Cause IS NOT NULL)
			BEGIN
				INSERT INTO Vehicle.Fault.FaultEvent_Fault_Message
						( FaultEventID ,
						  FaultID ,
						  MessageID ,
						  [Index]
						)
				VALUES  ( @FaultEventID ,		-- FaultEventID - int
						  @Cause ,				-- FaultID		- int
						  @StaticMsgID ,		-- MessageID	- int
						  @Index				-- Index		- int
						)
				
				SET @Index = @Index + 1;

				SELECT @Cause = F.CauseID
				FROM Vehicle.Fault.Fault F (NOLOCK)
				WHERE F.FaultID = @Cause						
			END	

		FETCH NEXT FROM faultEvent_cursor
		INTO @FaultEventID, @FaultID, @Message;

	END

	CLOSE faultEvent_cursor;
	DEALLOCATE faultEvent_cursor;

	ALTER TABLE Vehicle.Fault.FaultEvent
	DROP CONSTRAINT CK_Fault_FaultEvent__Message

	ALTER TABLE Vehicle.Fault.FaultEvent
	DROP COLUMN [Message]

	COMMIT TRANSACTION @TranName;

END TRY

BEGIN CATCH

	CLOSE faultEvent_cursor;
	DEALLOCATE faultEvent_cursor;
	
	IF @@TRANCOUNT > 0
		ROLLBACK
		
	DECLARE @ErrMsg nvarchar(4000), @ErrSeverity INT
	
	SELECT @ErrMsg = ERROR_MESSAGE(), @ErrSeverity = ERROR_SEVERITY()
	
	RAISERROR(@ErrMsg, @ErrSeverity, 1)	

END CATCH