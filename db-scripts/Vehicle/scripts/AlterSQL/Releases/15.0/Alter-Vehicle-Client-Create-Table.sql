CREATE TABLE Client.AppraisalType (
        AppraisalTypeID    TINYINT NOT NULL,
        [Name]              VARCHAR(50), 
        CONSTRAINT PK_Client_AppraisalType PRIMARY KEY CLUSTERED (AppraisalTypeID),
        CONSTRAINT UK_Client_AppraisalType UNIQUE NONCLUSTERED ([Name])
)
ON [DATA]
GO
 
INSERT INTO Client.AppraisalType (AppraisalTypeID, Name) VALUES (1, 'Trade')
INSERT INTO Client.AppraisalType (AppraisalTypeID, Name) VALUES (2, 'Purchase')
GO
 
CREATE TABLE Client.Appraisal (
              AppraisalID        INT IDENTITY(1,1) NOT NULL,
              ClientVehicleID    INT NOT NULL,
              Created            DATETIME NOT NULL,
              [User]             VARCHAR(50) NOT NULL,
              AppraisalDocument  VARCHAR(100),
              AppraisalTypeID    TINYINT NOT NULL,
              AppraisalAmount    INT NULL,
              TargetGrossProfit  INT NULL,
              EstimatedRecon     INT NULL,
              TargetSellingPrice INT NULL,
              CONSTRAINT PK_Client_Appraisal PRIMARY KEY NONCLUSTERED (AppraisalID),
              CONSTRAINT FK_Client_Appraisal__ClientVehicle FOREIGN KEY (ClientVehicleID) REFERENCES Client.[Vehicle] (VehicleID),
              CONSTRAINT FK_Client_Appraisal__AppraisalType FOREIGN KEY (AppraisalTypeID) REFERENCES Client.AppraisalType (AppraisalTypeID)                        
)
ON [DATA]
GO
CREATE CLUSTERED INDEX IXC_ClientAppraisal__VehicleAppraisalIDs ON Client.Appraisal(ClientVehicleID, AppraisalID) ON [DATA]
GO
 
CREATE TABLE Client.VehicleAttributes
(
       ClientVehicleID INT NOT NULL,
       Color           VARCHAR(50) NULL,
       Mileage         INT NULL,
       CONSTRAINT PK_Client_VehicleAttributes PRIMARY KEY (ClientVehicleID),
       CONSTRAINT FK_Client_VehicleAttributes_Vehicle FOREIGN KEY (ClientVehicleID) REFERENCES Client.Vehicle (VehicleID)      
)
ON [DATA]
GO