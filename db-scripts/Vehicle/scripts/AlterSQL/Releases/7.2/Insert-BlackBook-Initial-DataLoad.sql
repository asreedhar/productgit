-- Insert an initial value into BlackBook.DataLoad since there is no original table to copy the values from.

DECLARE @Now DATETIME
SET @Now = GETDATE()


-- Get initial value of the form YYYYMMDD00
DECLARE @InitialDataLoadID INT
SELECT  @InitialDataLoadID = CONVERT(INT, CONVERT(VARCHAR, @Now, 112)) * 100

INSERT INTO BlackBook.DataLoad
(
    DataLoadID,
    DataLoadTime
)
VALUES
(
    @InitialDataLoadID,
    @Now
)
