
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Client].[Vehicle_BlackBook]') AND type in (N'U'))
DROP TABLE [Client].[Vehicle_BlackBook]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[MatrixCell]') AND type in (N'U'))
DROP TABLE [BlackBook].[MatrixCell]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[Matrix]') AND type in (N'U'))
DROP TABLE [BlackBook].[Matrix]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[Condition]') AND type in (N'U'))
DROP TABLE [BlackBook].[Condition]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[Valuation]') AND type in (N'U'))
DROP TABLE [BlackBook].[Valuation]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[Market]') AND type in (N'U'))
DROP TABLE [BlackBook].[Market]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[OptionState]') AND type in (N'U'))
DROP TABLE [BlackBook].[OptionState]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[OptionAction]') AND type in (N'U'))
DROP TABLE [BlackBook].[OptionAction]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[OptionActionType]') AND type in (N'U'))
DROP TABLE [BlackBook].[OptionActionType]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[VehicleConfiguration_History]') AND type in (N'U'))
DROP TABLE [BlackBook].[VehicleConfiguration_History]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[ChangeAgent]') AND type in (N'U'))
DROP TABLE [BlackBook].[ChangeAgent]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[VehicleConfiguration]') AND type in (N'U'))
DROP TABLE [BlackBook].[VehicleConfiguration]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[StandardEquipmentRecord]') AND type in (N'U'))
DROP TABLE [BlackBook].[StandardEquipmentRecord]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[StandardEquipment_CachePolicy]') AND type in (N'U'))
DROP TABLE [BlackBook].[StandardEquipment_CachePolicy]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[StandardEquipment_History]') AND type in (N'U'))
DROP TABLE [BlackBook].[StandardEquipment_History]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[StandardEquipment_Cache]') AND type in (N'U'))
DROP TABLE [BlackBook].[StandardEquipment_Cache]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[StandardEquipment]') AND type in (N'U'))
DROP TABLE [BlackBook].[StandardEquipment]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[SubCategory]') AND type in (N'U'))
DROP TABLE [BlackBook].[SubCategory]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[Category]') AND type in (N'U'))
DROP TABLE [BlackBook].[Category]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[MileageAdjustment]') AND type in (N'U'))
DROP TABLE [BlackBook].[MileageAdjustment]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[Options]') AND type in (N'U'))
DROP TABLE [BlackBook].[Options]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[OptionAdjustmentSign]') AND type in (N'U'))
DROP TABLE [BlackBook].[OptionAdjustmentSign]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[Swatch]') AND type in (N'U'))
DROP TABLE [BlackBook].[Swatch]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[Color]') AND type in (N'U'))
DROP TABLE [BlackBook].[Color]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[Car_Attributes]') AND type in (N'U'))
DROP TABLE [BlackBook].[Car_Attributes]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[Car_Prices]') AND type in (N'U'))
DROP TABLE [BlackBook].[Car_Prices]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[Car_Description]') AND type in (N'U'))
DROP TABLE [BlackBook].[Car_Description]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[Value]') AND type in (N'U'))
DROP TABLE [BlackBook].[Value]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[ResidualValue]') AND type in (N'U'))
DROP TABLE [BlackBook].[ResidualValue]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[Car_History]') AND type in (N'U'))
DROP TABLE [BlackBook].[Car_History]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[Car_CachePolicy]') AND type in (N'U'))
DROP TABLE [BlackBook].[Car_CachePolicy]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[CachePolicy]') AND type in (N'U'))
DROP TABLE [BlackBook].[CachePolicy]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[Car]') AND type in (N'U'))
DROP TABLE [BlackBook].[Car]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[BlackBook].[State]') AND type in (N'U'))
DROP TABLE [BlackBook].[State]
GO
