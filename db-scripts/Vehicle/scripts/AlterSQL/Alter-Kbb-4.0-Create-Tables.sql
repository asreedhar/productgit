/*

drop table Kbb.ZipCode
drop table Kbb.Region
drop table Kbb.RegionAdjustmentTypePriceType
drop table Kbb.RegionGroupAdjustment
drop table Kbb.RegionZipCode
drop table Kbb.VehicleGroup

*/

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.Region
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.Region
(
    DataLoadID      int NOT NULL,
	RegionID		int NOT NULL,
	RegionTypeID	int NOT NULL,
    DisplayName  	varchar(50) NULL,
	RegionTypeDisplayName 	varchar(25) NULL,

    CONSTRAINT PK_Kbb_Region 
    PRIMARY KEY CLUSTERED (DataLoadID, RegionID)    
)
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.RegionAdjustmentTypePriceType
--    Foreign Keys: 
--		- Kbb.PriceType
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.RegionAdjustmentTypePriceType
(
    DataLoadID              	int NOT NULL,
    RegionAdjustmentTypeID      int NOT NULL,
	PriceTypeID 				int NOT NULL,
    DisplayName             	varchar(50) NOT NULL,
 
    CONSTRAINT PK_Kbb_RegionAdjustmentTypePriceType 
    PRIMARY KEY CLUSTERED (DataLoadID, RegionAdjustmentTypeID, PriceTypeID),
	
	CONSTRAINT FK_Kbb_RegionAdjustmentTypePriceType__PriceType
    FOREIGN KEY (DataLoadID, PriceTypeID) REFERENCES Kbb.PriceType (DataLoadID, PriceTypeID)
) 
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.RegionGroupAdjustment
--    Foreign Keys: 
--		- Kbb.Region
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.RegionGroupAdjustment
(
    DataLoadID                  	int NOT NULL,
    RegionID                    	int NOT NULL,
	GroupID							int NOT NULL,
	RegionAdjustmentTypeID			int NOT NULL,
	AdjustmentTypeID				int NOT NULL,
	AdjustmentTypeRoundingTypeID	int NOT NULL,
	Adjustment						float NOT NULL,		-- decimal(5, 4) NOT NULL,	
	AdjustmentTypeDisplayName		varchar(25) NOT NULL,
	
    CONSTRAINT PK_Kbb_RegionGroupAdjustment 
    PRIMARY KEY CLUSTERED (DataLoadID, RegionID, GroupID, RegionAdjustmentTypeID),
	
	CONSTRAINT FK_Kbb_RegionGroupAdjustment__Region
    FOREIGN KEY (DataLoadID, RegionID) REFERENCES Kbb.Region (DataLoadID, RegionID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.ZipCode
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.ZipCode
(
    DataLoadID      int NOT NULL,
    ZipCodeID  		int NOT NULL,
	ZipCode			char(5) NOT NULL,
 
    CONSTRAINT PK_Kbb_ZipCode 
    PRIMARY KEY CLUSTERED (DataLoadID, ZipCodeID) 
)
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.RegionZipCode
--    Foreign Keys: 
--		- Kbb.Region
--		- Kbb.ZipCode
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.RegionZipCode
(
    DataLoadID  	int NOT NULL,
    RegionID      	int NOT NULL,
	ZipCodeID      	int NOT NULL,

    CONSTRAINT PK_Kbb_RegionZipCode
    PRIMARY KEY CLUSTERED (DataLoadID, RegionID, ZipCodeID),
	
	CONSTRAINT FK_Kbb_RegionZipCode__Region
    FOREIGN KEY (DataLoadID, RegionID) REFERENCES Kbb.Region (DataLoadID, RegionID),
	
	CONSTRAINT FK_Kbb_RegionZipCode__ZipCode
    FOREIGN KEY (DataLoadID, ZipCodeID) REFERENCES Kbb.ZipCode (DataLoadID, ZipCodeID)
)
ON PS_Kbb__DataLoad (DataLoadID)
GO

-----------------------------------------------------------------------------------------------------------------------
--  Kbb.VehicleGroup
--    Foreign Keys: 
--		- Kbb.Vehicle
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE Kbb.VehicleGroup
(
    DataLoadID             	int NOT NULL,
    VehicleID              	int NOT NULL,
    GroupID              	int NOT NULL,
	GroupTypeID             int NOT NULL,
    DisplayName     		varchar(50) NOT NULL,
    GroupTypeDisplayName   	varchar(50) NOT NULL,
 
    CONSTRAINT PK_Kbb_VehicleGroup
    PRIMARY KEY CLUSTERED (DataLoadID, VehicleID, GroupID),   
	
	/*
	CONSTRAINT FK_Kbb_VehicleGroup__Vehicle
    FOREIGN KEY (DataLoadID, VehicleID) REFERENCES Kbb.Vehicle (DataLoadID, VehicleID)
	*/
) 
ON PS_Kbb__DataLoad (DataLoadID)
GO


