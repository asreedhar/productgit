ALTER TABLE Vehicle.Client.Vehicle_Kbb NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.Category NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.DataLoad NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.DataLoadStatus NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.DataVersion NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.Make NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.Matrix NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.MatrixCell NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.MileageGroupAdjustment NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.MileageRange NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.Model NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.ModelYear NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.OptionAction NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.OptionActionType NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.OptionRegionPriceAdjustment NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.OptionRelationship NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.OptionRelationshipSet NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.OptionSpecificationValue NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.OptionState NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.PriceType NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.ProgramContext NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.Region NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.RegionAdjustmentTypePriceType NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.RegionBasePrice NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.RegionGroupAdjustment NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.RegionZipCode NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.Specification NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.SpecificationValue NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.Trim NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.Valuation NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.Vehicle NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VehicleCategory NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VehicleConfiguration NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VehicleConfiguration_History NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VehicleConfiguration_History_Matrix NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VehicleGroup NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VehicleOption NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VehicleOptionCategory NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VehicleRegion NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VinOptionEquipment NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VinOptionEquipmentPattern NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VinVehiclePattern NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.[Year] NOCHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.ZipCode NOCHECK CONSTRAINT ALL


DELETE FROM Vehicle.Client.Vehicle_Kbb
DELETE FROM Vehicle.Kbb.Category
DELETE FROM Vehicle.Kbb.DataLoad
DELETE FROM Vehicle.Kbb.DataVersion
DELETE FROM Vehicle.Kbb.Make
DELETE FROM Vehicle.Kbb.Matrix
DELETE FROM Vehicle.Kbb.MatrixCell
DELETE FROM Vehicle.Kbb.MileageGroupAdjustment
DELETE FROM Vehicle.Kbb.MileageRange
DELETE FROM Vehicle.Kbb.Model
DELETE FROM Vehicle.Kbb.ModelYear
DELETE FROM Vehicle.Kbb.OptionAction
DELETE FROM Vehicle.Kbb.OptionRegionPriceAdjustment
DELETE FROM Vehicle.Kbb.OptionRelationship
DELETE FROM Vehicle.Kbb.OptionRelationshipSet
DELETE FROM Vehicle.Kbb.OptionSpecificationValue
DELETE FROM Vehicle.Kbb.OptionState
DELETE FROM Vehicle.Kbb.PriceType
DELETE FROM Vehicle.Kbb.ProgramContext
DELETE FROM Vehicle.Kbb.Region
DELETE FROM Vehicle.Kbb.RegionAdjustmentTypePriceType
DELETE FROM Vehicle.Kbb.RegionBasePrice
DELETE FROM Vehicle.Kbb.RegionGroupAdjustment
DELETE FROM Vehicle.Kbb.RegionZipCode
DELETE FROM Vehicle.Kbb.Specification
DELETE FROM Vehicle.Kbb.SpecificationValue
DELETE FROM Vehicle.Kbb.Trim
DELETE FROM Vehicle.Kbb.Vehicle
DELETE FROM Vehicle.Kbb.VehicleCategory
DELETE FROM Vehicle.Kbb.VehicleConfiguration
DELETE FROM Vehicle.Kbb.VehicleConfiguration_History
DELETE FROM Vehicle.Kbb.VehicleConfiguration_History_Matrix
DELETE FROM Vehicle.Kbb.VehicleGroup
DELETE FROM Vehicle.Kbb.VehicleOption
DELETE FROM Vehicle.Kbb.VehicleOptionCategory
DELETE FROM Vehicle.Kbb.VehicleRegion
DELETE FROM Vehicle.Kbb.VinOptionEquipment
DELETE FROM Vehicle.Kbb.VinOptionEquipmentPattern
DELETE FROM Vehicle.Kbb.VinVehiclePattern
DELETE FROM Vehicle.Kbb.[Year]
DELETE FROM Vehicle.Kbb.ZipCode



ALTER TABLE Vehicle.Client.Vehicle_Kbb CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.Category CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.DataLoad CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.DataLoadStatus CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.DataVersion CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.Make CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.Matrix CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.MatrixCell CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.MileageGroupAdjustment CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.MileageRange CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.Model CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.ModelYear CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.OptionAction CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.OptionActionType CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.OptionRegionPriceAdjustment CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.OptionRelationship CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.OptionRelationshipSet CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.OptionSpecificationValue CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.OptionState CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.PriceType CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.ProgramContext CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.Region CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.RegionAdjustmentTypePriceType CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.RegionBasePrice CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.RegionGroupAdjustment CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.RegionZipCode CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.Specification CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.SpecificationValue CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.Trim CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.Valuation CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.Vehicle CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VehicleCategory CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VehicleConfiguration CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VehicleConfiguration_History CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VehicleConfiguration_History_Matrix CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VehicleGroup CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VehicleOption CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VehicleOptionCategory CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VehicleRegion CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VinOptionEquipment CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VinOptionEquipmentPattern CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.VinVehiclePattern CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.[Year] CHECK CONSTRAINT ALL
ALTER TABLE Vehicle.Kbb.ZipCode CHECK CONSTRAINT ALL
