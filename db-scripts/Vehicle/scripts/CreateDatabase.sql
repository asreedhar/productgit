/*

Create a Vehicle database instance

*/

IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'__DATABASE__')
 DROP DATABASE [__DATABASE__]
GO

CREATE DATABASE __DATABASE__
ON PRIMARY
(       NAME = DATA1,
        FILENAME = N'__SQL_SERVER_DATA__\__DATABASE___Data1.mdf',
        SIZE = 10,
        MAXSIZE = 50,
        FILEGROWTH = 5 ),
FILEGROUP DATA DEFAULT 
(       NAME = DATA2,
        FILENAME = N'__SQL_SERVER_DATA__\__DATABASE___Data2.ndf',
        SIZE = 10,
        FILEGROWTH = 5 ),
FILEGROUP IDX
(       NAME = IDX,
        FILENAME = N'__SQL_SERVER_DATA__\__DATABASE___Idx.ndf',
        SIZE = 10,
        FILEGROWTH = 5 )
LOG ON
(       NAME = LOG,
        FILENAME = N'__SQL_SERVER_LOG__\__DATABASE___Log.ldf',
        SIZE = 5MB,
        FILEGROWTH = 5MB )
GO

--  A new SQL 2005 "feature" that I'll have to research better some day
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
    EXEC __DATABASE__.dbo.sp_fulltext_database @action = 'disable'
GO

CREATE TABLE __DATABASE__.dbo.Alter_Script
 (
   AlterScriptName varchar(255) not null
  ,ApplicationDate datetime    not null 
    constraint DF_Alter_Script__ApplicationDate
     default (CURRENT_TIMESTAMP)
 )
GO

INSERT __DATABASE__.dbo.Alter_Script (AlterScriptName) values ('Database Created')

--  Make sure these get set right!
ALTER DATABASE __DATABASE__ SET AUTO_CREATE_STATISTICS ON 
ALTER DATABASE __DATABASE__ SET AUTO_UPDATE_STATISTICS ON 
ALTER DATABASE __DATABASE__ SET RECOVERY FULL
GO
