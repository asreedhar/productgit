/*

Use this script as a template (by adjusting the database file locations) when
creating an archive database.

*/


IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'__DATABASE__')
	DROP DATABASE __DATABASE__
GO
 
 
--  Step 1: Create the database  ---------------------------------------------------
CREATE DATABASE __DATABASE__
  on primary
   (name = __DATABASE__,      filename = '__SQL_SERVER_DATA__\__DATABASE__.mdf'
    ,size = 2000MB, maxsize = unlimited, filegrowth = 500MB)
  log on
   (name = __DATABASE___log,  filename = '__SQL_SERVER_LOG__\__DATABASE___log.ldf'
    ,size = 500MB, maxsize = unlimited, filegrowth = 500MB)


ALTER DATABASE __DATABASE__
 set
   auto_create_statistics on
  ,auto_update_statistics on
  ,recovery simple
GO


--  Step 2: Create the "basic" stuff  ----------------------------------------------
USE __DATABASE__
GO

--  From IMT
CREATE TABLE [dbo].[Audit_Exceptions_Archive] (
	[Audit_ID] [int] NOT NULL ,
	[SourceTableID] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ExceptionRuleID] [int] NOT NULL ,
	[TimeStamp] [datetime] NOT NULL 
)
GO

CREATE TABLE [dbo].[Audit_Inventory_Archive] (
	[AUDIT_ID] [int] NOT NULL ,
	[STATUS] [tinyint] NOT NULL 
)
GO

CREATE TABLE [dbo].[Audit_Sales_Archive] (
	[AUDIT_ID] [int] NOT NULL ,
	[STATUS] [tinyint] NOT NULL ,
	[PACK_ADJUST] [decimal](8, 2) NULL 
)
GO


--  From DBASTAT
CREATE TABLE [dbo].[Dataload_Raw_History_Archive] (
	[DEALER_ID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DEALER_STATUS] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EXCEPTION_CODE] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DELTA_FLAG] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SALE_STATUS] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SALES_REF_NUM] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DEAL_NUM_FI] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DEAL_DT] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VIN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[STOCK_NUM] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VEH_YEAR] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MAKE] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MODEL] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MODEL_PKG] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BODY_TYPE] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DOOR_CT] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VEH_TYPE] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VEH_CLASS] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BASE_COLOR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EXT_COLOR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[INT_COLOR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[INT_DESC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ENGINE_DESC] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CYLINDER_CT] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FUEL_CODE] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TRANS_DESC] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DRIVE_TRAIN_DESC] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MILEAGE] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[USED_SELLING_PRICE] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DISPOSITION] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CERTIFIED] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ACQUISITION_PRICE] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PHOTO_FILE_NAME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PHOTO_AVAIL_FLAG] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RECON_COST] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PACK_AMOUNT] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[UNIT_COST] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SALE_PRICE] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FRONT_END_GROSS] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BACKEND_GROSS] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AFTERMARKET_GROSS] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TOTAL_GROSS] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ENTRY_DT] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MODIFIED_DT] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VEH_STATUS] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VEH_STATUS_CODE] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VEH_LOCATION] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VEH_SOURCE] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VEH_SOURCE_DTL] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SALESPERSON_ID] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SALESPERSON_LASTNAME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SALESPERSON_FIRSTNAME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SALES_COMMISSION] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[OTHER_COMMISSIONS] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PWR_WINDOWS] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PWR_LOCKS] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VEH_PROMOTION] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LEASE_FLAG] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AIR_BAGS] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AIR_CONDITIONING] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SALE_DESC] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CRUISE] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[REF_DT] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RECEIVED_DT] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ALARM_SYSTEM] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AGE_IN_DAYS] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CUSTOMER_TYPE] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CUSTOMER_ADDRESS1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CUSTOMER_ADDRESS2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CUSTOMER_CITY] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CUSTOMER_STATE] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CUSTOMER_ZIP] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CUSTOMER_ZIP_EXT] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SUN_ROOF] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TILT_STEERING] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PWR_STEERING] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CONVERTIBLE] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PWR_SEATS] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AUDIO_AM_FM] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AUDIO_CASS] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AUDIO_CD] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BRAKES_DISC] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BRAKES_FR_DISC_REAR_DRUM] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BRAKES_ABS] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TIRE_ALL_SEASON] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TIRE_ALL_TERR] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TIRE_OFF_ROAD] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TIRE_PERF] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TIRE_HWY] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TIRE_TOURING] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[WHEEL_ALLOY] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[WHEEL_ALUM] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[WHEEL_CHROME] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[WHEEL_SPECIAL] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILLER1] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILLER2] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILLER3] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILLER4] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILLER5] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILLER6] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILLER7] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILLER8] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILLER9] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILLER10] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILLER11] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILLER12] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILLER13] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILLER14] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILLER15] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILLER16] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILLER17] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILLER18] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILLER19] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FILLER20] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[AUDIT_ID] [int] NOT NULL ,
	[LOAD_ID] [int] NOT NULL ,
	[ORIGINAL_EXT_COLOR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ORIGINAL_INT_COLOR] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
)
GO


CREATE TABLE Alter_Script
 (
   AlterScriptName varchar(100) not null
    constraint PK_Alter_Script
     primary key clustered
  ,ApplicationDate datetime    not null 
    constraint DF_Alter_Script_ApplicationDate
     default (getdate())
 )

INSERT Alter_Script (AlterScriptName) values ('Database Created')
GO
