
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Audit_Exceptions_Archive]') AND type in (N'U'))
create index IX_Audit_Exceptions_Archive__Audit_ID on Audit_Exceptions_Archive(Audit_ID)
go

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Audit_Inventory_Archive]') AND type in (N'U'))
create index IX_Audit_Inventory_Archive__audit_id on Audit_Inventory_Archive(AUDIT_ID)
go

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Audit_Sales_Archive]') AND type in (N'U'))
create index IX_Audit_Sales_Archive__audit_id on Audit_Sales_Archive(AUDIT_ID)
go
