USE [master]
GO
ALTER DATABASE [Archive] ADD FILEGROUP [IMT]
GO
ALTER DATABASE [Archive] ADD FILE ( NAME = N'IMT', FILENAME = N'D:\SQL_Datafiles\IMT.ndf' , SIZE = 1024000KB , FILEGROWTH = 10%) TO FILEGROUP [IMT]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#InventoryTransferPriceHistory](
	[InventoryTransferHistoryID] [int] IDENTITY(1,1) NOT NULL,
	[InventoryID] [int] NOT NULL,
	[TransferPrice_Old] [decimal](9, 2) NULL,
	[TransferPrice_New] [decimal](9, 2) NULL,
	[Created] [datetime] NOT NULL,
	[CreatedBy] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#BusinessUnitType](
	[BusinessUnitTypeID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitType] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#PhotoStatus](
	[PhotoStatusCode] [tinyint] NOT NULL,
	[Description] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#InsightCategory](
	[InsightCategoryID] [int] NOT NULL,
	[Name] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#FLUSAN_EventType](
	[FLUSAN_EventTypeCode] [tinyint] NOT NULL,
	[Description] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#lu_InventoryVehicleLight](
	[InventoryVehicleLightCD] [tinyint] NOT NULL,
	[InventoryVehicleLightDesc] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#lu_AuditStatus](
	[AuditStatusCD] [tinyint] NOT NULL,
	[AuditStatusDESC] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ShortDesc] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#InsightAdjectival](
	[InsightAdjectivalID] [int] NOT NULL,
	[Name] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#Manufacturer](
	[ManufacturerID] [tinyint] NOT NULL,
	[Name] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ShortName] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#CIABucketGroups](
	[CIABucketGroupID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#PING_GroupingCodeMapping](
	[CodeID] [int] NULL,
	[GroupingDescription] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Make] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Model] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SegmentID] [tinyint] NULL,
	[Line] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FirstlookModel] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FirstlookLine] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FirstlookMake] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#SearchCandidateOptionAnnotationType](
	[SearchCandidateOptionAnnotationTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#InsightTargetType](
	[InsightTargetTypeID] [int] NOT NULL,
	[Name] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#tbl_tmpRunDayOfWeek](
	[businessunitid] [int] NULL,
	[rundayofweek] [int] NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#CIABucketRuleTypes](
	[CIABucketRuleTypeID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ColumnName] [varchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#DataSource](
	[DataSourceID] [int] IDENTITY(1,1) NOT NULL,
	[DataSource] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#lu_AccessGroupActor](
	[AccessGroupActorId] [int] NOT NULL,
	[Description] [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#map_BusinessUnitToMarketDealer](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[DealerNumber] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#ChangedVics](
	[ChangedVicID] [int] IDENTITY(100000,1) NOT NULL,
	[OldVic] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[NewVic] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#InsightReflection](
	[InsightReflectionID] [int] NOT NULL,
	[Name] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#tbl_VehicleSaleReversals](
	[InventoryID] [int] NOT NULL,
	[SalesReferenceNumber] [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VehicleMileage] [int] NULL,
	[DealDate] [smalldatetime] NOT NULL,
	[BackEndGross] [decimal](8, 2) NULL,
	[NetFinancialInsuranceIncome] [decimal](18, 0) NULL,
	[OriginalFrontEndGross] [decimal](8, 2) NULL,
	[FrontEndGross] [decimal](18, 0) NULL,
	[SalePrice] [decimal](9, 2) NULL,
	[SaleDescription] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FinanceInsuranceDealNumber] [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastPolledDate] [smalldatetime] NOT NULL,
	[AnalyticalEngineStatus] [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AnalyticalEngineStatus_CD] [int] NULL,
	[LeaseFlag] [tinyint] NULL,
	[Pack] [decimal](8, 2) NULL,
	[AfterMarketGross] [decimal](8, 2) NULL,
	[TotalGross] [decimal](8, 2) NULL,
	[VehiclePromotionAmount] [decimal](8, 2) NULL,
	[DealStatusCD] [tinyint] NULL,
	[PackAdjust] [decimal](10, 2) NULL,
	[AUDIT_ID] [int] NOT NULL,
	[valuation] [float] NULL,
	[CustomerZip] [varchar](9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#lu_AccessGroupPrice](
	[AccessGroupPriceId] [int] NOT NULL,
	[Description] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#InsightTargetValueType](
	[InsightTargetValueTypeID] [int] NOT NULL,
	[Name] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#DealerFranchise](
	[DealerFranchiseID] [int] IDENTITY(10000,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[FranchiseID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#tbl_MemberATCAccessGroupListType](
	[MemberATCAccessGroupListTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#lu_DecodedSquishVinSource](
	[dsvSourceID] [tinyint] NOT NULL,
	[dsvSourceDESC] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#FirstLookRegion](
	[FirstLookRegionID] [int] IDENTITY(1,1) NOT NULL,
	[FirstLookRegionName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#BookoutStatus](
	[BookoutStatusID] [int] NOT NULL,
	[Description] [varchar](45) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[IsAccurate] [tinyint] NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#SingleVINDecodeTarget](
	[VIN] [varchar](17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[VehicleTrim] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VehicleYear] [int] NULL,
	[Make] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Model] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MakeModelGroupingID] [int] NULL,
	[DoorCount] [int] NULL,
	[VehicleBodyStyleID] [int] NULL,
	[VehicleDriveTrain] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VehicleEngine] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CylinderCount] [int] NULL,
	[FuelType] [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VehicleTransmission] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InteriorColor] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OriginalBodyStyle] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InteriorDescription] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#Franchise](
	[FranchiseID] [int] IDENTITY(10000,1) NOT NULL,
	[FranchiseDescription] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ManufacturerID] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#PurchasingCenterChannels](
	[ChannelID] [int] IDENTITY(1,1) NOT NULL,
	[ChannelName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HasAccessGroups] [bit] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#GlobalParam](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [char](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[value] [varchar](256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#DealerTarget](
	[DealerTargetId] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitId] [int] NOT NULL,
	[TargetCD] [smallint] NOT NULL,
	[StartDT] [smalldatetime] NOT NULL,
	[TargetValue] [int] NOT NULL,
	[Active] [bit] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#tbl_GroupingDescription](
	[GroupingDescriptionID] [int] IDENTITY(100000,1) NOT NULL,
	[GroupingDescription] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SuppressMarketPerformer] [bit] NULL,
	[DateCreated] [smalldatetime] NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#InventoryStatusCodes](
	[InventoryStatusCD] [smallint] IDENTITY(1,1) NOT NULL,
	[ShortDescription] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#lu_RedistributionSource](
	[RedistributionSourceId] [tinyint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#VehicleSources](
	[VehicleSourceID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [char](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#BookOutTypes](
	[BookOutTypeId] [int] NOT NULL,
	[BookOutDescription] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#EdmundsBodyTypes](
	[EdmundsBodyType] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ExtendsVehicleBodyStyleDescription] [bit] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#KelleyBadVin](
	[KelleyBadVinId] [int] IDENTITY(10000,1) NOT NULL,
	[VIN] [varchar](17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#staging_messagecenter](
	[Ordering] [tinyint] NOT NULL,
	[Title] [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Body] [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HasPriority] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#AIP_EventCategory](
	[AIP_EventCategoryID] [tinyint] NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#InventoryOverrideHistory](
	[InventoryID] [int] NOT NULL,
	[StockNumber] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[VehicleID] [int] NOT NULL,
	[InventoryActive] [tinyint] NOT NULL,
	[InventoryReceivedDate] [smalldatetime] NOT NULL,
	[DeleteDt] [smalldatetime] NULL,
	[ModifiedDT] [smalldatetime] NOT NULL,
	[DMSReferenceDt] [smalldatetime] NULL,
	[UnitCost] [decimal](9, 2) NOT NULL,
	[AcquisitionPrice] [decimal](8, 2) NULL,
	[Pack] [decimal](8, 2) NULL,
	[MileageReceived] [int] NULL,
	[TradeOrPurchase] [tinyint] NOT NULL,
	[ListPrice] [decimal](8, 2) NULL,
	[InitialVehicleLight] [int] NULL,
	[Level4Analysis] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InventoryType] [tinyint] NOT NULL,
	[ReconditionCost] [decimal](8, 2) NULL,
	[UsedSellingPrice] [decimal](8, 2) NULL,
	[Certified] [tinyint] NOT NULL,
	[VehicleLocation] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CurrentVehicleLight] [tinyint] NULL,
	[FLRecFollowed] [tinyint] NOT NULL,
	[Audit_ID] [int] NOT NULL,
	[InventoryStatusCD] [smallint] NOT NULL,
	[ListPriceLock] [tinyint] NOT NULL,
	[SpecialFinance] [tinyint] NULL,
	[eStockCardLock] [tinyint] NOT NULL,
	[DateBookoutLocked] [smalldatetime] NULL,
	[PlanReminderDate] [smalldatetime] NULL,
	[NewStockNumber] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DateCreated] [smalldatetime] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#AppraisalActionTypes](
	[AppraisalActionTypeID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#rundayofweek](
	[businessunitid] [int] NOT NULL,
	[rundayofweek] [int] NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#Role](
	[RoleID] [int] NOT NULL,
	[Type] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Code] [char](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Name] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#lu_CIATimePeriod](
	[CIATimePeriodId] [int] IDENTITY(1,1) NOT NULL,
	[Days] [int] NOT NULL,
	[Forecast] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#MemberBusinessUnitSetType](
	[MemberBusinessUnitSetTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#PhotoTypes](
	[PhotoTypeID] [tinyint] NOT NULL,
	[Description] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#lu_DealerPackType](
	[PackTypeCD] [tinyint] IDENTITY(1,1) NOT NULL,
	[PackTypeDESC] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#OldBuyingAlertsSubscriptions](
	[SubscriptionID] [int] NOT NULL,
	[SubscriberID] [int] NOT NULL,
	[SubscriberTypeID] [tinyint] NOT NULL,
	[SubscriptionTypeID] [tinyint] NOT NULL,
	[SubscriptionDeliveryTypeID] [tinyint] NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[SubscriptionFrequencyDetailID] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#PhotosToBeDeleted](
	[PhotoID] [int] NOT NULL,
	[LoggedOn] [datetime] NOT NULL,
	[Reason] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#JobTitle](
	[JobTitleID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#VehicleAttributeSource](
	[VehicleAttributeSourceID] [tinyint] NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#SoftwareSystemComponent](
	[SoftwareSystemComponentID] [int] NOT NULL,
	[ParentID] [int] NULL,
	[Name] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Token] [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#PERL_DBD_TEST](
	[COL_A] [smallint] NOT NULL,
	[COL_B] [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[COL_C] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[COL_D] [datetime] NULL
) ON [IMT] TEXTIMAGE_ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#ThirdParties](
	[ThirdPartyID] [tinyint] NOT NULL,
	[Description] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Name] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DefaultFooter] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ImageName] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#lu_OptionType](
	[Id] [tinyint] NOT NULL,
	[Description] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#RepriceExportStatus](
	[RepriceExportStatusID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#lu_VehicleLight](
	[InventoryVehicleLightID] [tinyint] NOT NULL,
	[InventoryVehicleLightDESC] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#ReliableMessageQueue](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[QueueName] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Message] [varbinary](max) NULL,
	[Ready] [bit] NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#TargetSection](
	[SectionCD] [smallint] NOT NULL,
	[SectionName] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[InventoryVehicleTypeCD] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#TargetType](
	[TargetTypeCD] [tinyint] NOT NULL,
	[TargetTypeName] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#Segment](
	[SegmentID] [int] NOT NULL,
	[Segment] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#ThirdPartyOptionTypes](
	[ThirdPartyOptionTypeID] [tinyint] IDENTITY(1,1) NOT NULL,
	[OptionTypeName] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#MarketplaceSubmissionStatusCode](
	[MarketplaceSubmissionStatusCD] [tinyint] NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#HomePage](
	[HomePageID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#VehicleSubSegment](
	[VehicleSubSegmentID] [tinyint] IDENTITY(1,1) NOT NULL,
	[VehicleSubSegmentDESC] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#PING_Provider](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[code] [nvarchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[available] [bit] NOT NULL,
	[sortOrder] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#Position](
	[PositionID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#Marketplace_ThirdPartyEntity](
	[MarketplaceID] [int] NOT NULL,
	[Description] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#PING_System](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[active] [bit] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#WindowStickerTemplate](
	[WindowStickerTemplateId] [int] IDENTITY(1,1) NOT NULL,
	[WindowStickerTemplateName] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#PING_GroupingCodes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[CodeId] [int] NOT NULL,
	[MakeModelGroupingId] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#PING_Code](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[value] [nvarchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[providerId] [int] NOT NULL,
	[parentId] [int] NULL,
	[type] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#ThirdPartyEntityType](
	[ThirdPartyEntityTypeID] [tinyint] NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#AppraisalStatus](
	[AppraisalStatusID] [tinyint] NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#InventoryBookoutFailures](
	[InventoryID] [int] NULL,
	[BookoutID] [int] NULL,
	[DateCreate] [smalldatetime] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#ThirdPartyVehicleOptionValueTypes](
	[ThirdPartyOptionValueTypeID] [tinyint] IDENTITY(1,1) NOT NULL,
	[OptionValueTypeName] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#VehicleSalesHistoryAudit](
	[AuditID] [int] IDENTITY(100000,1) NOT NULL,
	[BusinessUnitID] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SaleRefNum] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DealerStatus] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VIN] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StockNum] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Year] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Make] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Model] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ModelPkg] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BodyType] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VehClass] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ExtColor] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Mileage] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DealDT] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NetProfit] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NetFIIncome] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AcvTrade1] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AcvTrade2] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AcvTrade3] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LeaseFlag] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[GrossMargin] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SalePrice] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SaleDesc] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DealNumFI] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RefDT] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ExceptionCode] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ReceivedDT] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RawText] [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ProcessDate] [datetime] NULL,
	[ExistedPreviously] [tinyint] NULL,
	[AcceptanceCode] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AcceptanceMessage] [varchar](3000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#MessageCenter](
	[Ordering] [tinyint] NOT NULL,
	[Title] [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Body] [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HasPriority] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#lu_AnalyticalEngineStatus](
	[analyticalenginestatus_CD] [tinyint] NOT NULL,
	[analyticalenginestatus_DESC] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#lu_CustomerType](
	[CustomerTypeCD] [tinyint] NOT NULL,
	[CustomerTypeDESC] [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#JDPower_PowerRegion](
	[PowerRegionID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](240) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#PrintAdvertiserTextOption](
	[PrintAdvertiserTextOptionID] [tinyint] NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#lu_Disposition](
	[DispositionCD] [tinyint] NOT NULL,
	[DispositionDESC] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#lu_DMIVehicleStatus](
	[DMIVehicleStatusCD] [smallint] IDENTITY(1,1) NOT NULL,
	[DMIVehicleStatusDESC] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#BuyingAlertsStatus](
	[BuyingAlertsStatusId] [tinyint] NOT NULL,
	[Description] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#BookoutSources](
	[BookoutSourceID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#Audit_Exceptions](
	[Audit_ID] [int] NOT NULL,
	[SourceTableID] [tinyint] NOT NULL,
	[ExceptionRuleID] [int] NOT NULL,
	[TimeStamp] [datetime] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#lu_InventoryVehicleType](
	[InventoryVehicleTypeCD] [tinyint] NOT NULL,
	[InventoryVehicleTypeDescription] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#AppraisalSource](
	[AppraisalSourceID] [tinyint] NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#lu_FuelType](
	[FuelTypeCD] [smallint] IDENTITY(1,1) NOT NULL,
	[FuelTypeDESC] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#lu_ProgramType](
	[ProgramType_CD] [tinyint] NOT NULL,
	[ProgramTypeName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#lu_SaleDescription](
	[SaleDescriptionCD] [tinyint] NOT NULL,
	[SaleDescription] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#MessageCenterAlert](
	[Ordering] [tinyint] NOT NULL,
	[Title] [varchar](300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Body] [varchar](4000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[HasPriority] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#lu_States](
	[StateID] [tinyint] NOT NULL,
	[StateShort] [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[StateLong] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#lu_TradeOrPurchase](
	[TradeOrPurchaseCD] [tinyint] NOT NULL,
	[TradeOrPurchaseDesc] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#License](
	[LicenseID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#lu_UserRole](
	[UserRoleCD] [tinyint] NOT NULL,
	[UserRoleDesc] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#lu_Status](
	[StatusId] [tinyint] NOT NULL,
	[StatusName] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#BookoutValueTypes](
	[BookoutValueTypeID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#InternetAdvertiserExportStatus](
	[ExportStatusCD] [tinyint] NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#Inventory_Level4Analysis](
	[InventoryID] [int] NOT NULL,
	[Level4Analysis] [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#CredentialType](
	[CredentialTypeID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#ModelBodyStyleOverrides](
	[Make] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Model] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[BodyType] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[EnhancedModel] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#Inventory_ListPriceHistory](
	[ListPriceHistoryID] [int] IDENTITY(1,1) NOT NULL,
	[InventoryID] [int] NOT NULL,
	[DMSReferenceDT] [smalldatetime] NOT NULL,
	[ListPrice] [decimal](8, 2) NULL,
	[UpdateLocked] [bit] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#PurgeManagement](
	[Subject] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Period] [tinyint] NOT NULL,
	[PeriodType] [varchar](6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Routine] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[LastCall] [smalldatetime] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#SubscriptionFrequency](
	[SubscriptionFrequencyID] [tinyint] NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#SubscriberTypes](
	[SubscriberTypeID] [tinyint] NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#TransferPriceUnitCostValue](
	[TransferPriceUnitCostValueID] [int] NOT NULL,
	[Description] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#DMSExportPriceMapping](
	[DMSExportPriceMappingID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#SubscriptionDeliveryTypes](
	[SubscriptionDeliveryTypeID] [tinyint] NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#DMSExportFrequency](
	[DMSExportFrequencyID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#TransferPriceInventoryAgeRange](
	[TransferPriceInventoryAgeRangeID] [int] IDENTITY(1,1) NOT NULL,
	[Low] [int] NOT NULL,
	[High] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#CIASummaryStatus](
	[CIASummaryStatusID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#SubscriptionTypes](
	[SubscriptionTypeID] [tinyint] NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
SET ANSI_PADDING ON
ALTER TABLE [SubscriptionTypes] ADD [Notes] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [SubscriptionTypes] ADD [UserSelectable] [tinyint] NOT NULL
ALTER TABLE [SubscriptionTypes] ADD [DefaultSubscriptionFrequencyDetailID] [tinyint] NOT NULL
ALTER TABLE [SubscriptionTypes] ADD [Active] [tinyint] NOT NULL
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#CIACategories](
	[CIACategoryID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#Alter_Script](
	[AlterScriptName] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ApplicationDate] [datetime] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#CIAGroupingItemDetailTypes](
	[CIAGroupingItemDetailTypeID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#InventoryBuckets](
	[InventoryBucketID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[InventoryType] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#CIAGroupingItemDetailLevels](
	[CIAGroupingItemDetailLevelID] [tinyint] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](45) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#SISVinAndStockMapping](
	[SISVin] [varchar](17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[SISStockNumber] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Vin] [varchar](17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[StockNumber] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[BusinessUnitID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#tbl_DealerATCAccessGroups](
	[DealerATCAccessGroupsId] [int] IDENTITY(10000,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[AccessGroupId] [int] NOT NULL,
	[AccessGroupPriceId] [int] NOT NULL,
	[AccessGroupActorId] [int] NOT NULL,
	[AccessGroupValue] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#ApplicationEvent](
	[ApplicationEventID] [int] IDENTITY(100000,1) NOT NULL,
	[EventType] [int] NOT NULL,
	[MemberID] [int] NOT NULL,
	[EventDescription] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreateTimestamp] [datetime] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#SearchCandidateListType](
	[SearchCandidateListTypeID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#CarfaxAvailability](
	[UserName] [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Vin] [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ReportType] [char](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Expires] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#AppraisalType](
	[AppraisalTypeID] [int] NOT NULL,
	[Description] [varchar](45) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#lu_DealStatus](
	[DealStatusCD] [tinyint] NOT NULL,
	[DealStatusDesc] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#BookoutProcessorMode](
	[BookoutProcessorModeId] [tinyint] NOT NULL,
	[Description] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#PhotoStorage](
	[PhotoStorageCode] [tinyint] NOT NULL,
	[Description] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#RepriceSource](
	[RepriceSourceID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#BookoutProcessorStatus](
	[BookoutProcessorStatusId] [tinyint] NOT NULL,
	[Description] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#NADARegions](
	[NADARegionCode] [tinyint] NOT NULL,
	[NADARegionName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#lu_DealerUpgrade](
	[DealerUpgradeCD] [tinyint] NOT NULL,
	[DealerUpgradeDESC] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#BookoutProcessorRunLog_Failures](
	[BookoutProcessorRunId] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitId] [int] NOT NULL,
	[ThirdPartyId] [tinyint] NOT NULL,
	[BookoutProcessorModeId] [tinyint] NOT NULL,
	[BookoutProcessorStatusId] [tinyint] NOT NULL,
	[LoadTime] [smalldatetime] NULL,
	[StartTime] [smalldatetime] NULL,
	[EndTime] [smalldatetime] NULL,
	[ServerName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SuccessfulBookouts] [int] NULL,
	[FailedBookouts] [int] NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#PhotoProviderDealership](
	[PhotoProviderID] [int] NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[PhotoProvidersDealershipCode] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[PhotoStorageCode_Override] [tinyint] NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#BookoutProcessorRunLog](
	[BookoutProcessorRunId] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitId] [int] NOT NULL,
	[ThirdPartyId] [tinyint] NOT NULL,
	[BookoutProcessorModeId] [tinyint] NOT NULL,
	[BookoutProcessorStatusId] [tinyint] NOT NULL,
	[LoadTime] [smalldatetime] NULL,
	[StartTime] [smalldatetime] NULL,
	[EndTime] [smalldatetime] NULL,
	[ServerName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SuccessfulBookouts] [int] NULL,
	[FailedBookouts] [int] NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#Inventory](
	[InventoryID] [int] IDENTITY(1,1) NOT NULL,
	[StockNumber] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[VehicleID] [int] NOT NULL,
	[InventoryActive] [tinyint] NOT NULL,
	[InventoryReceivedDate] [smalldatetime] NOT NULL,
	[DeleteDt] [smalldatetime] NULL,
	[ModifiedDT] [smalldatetime] NOT NULL,
	[DMSReferenceDt] [smalldatetime] NULL,
	[UnitCost] [decimal](9, 2) NOT NULL,
	[AcquisitionPrice] [decimal](8, 2) NULL,
	[Pack] [decimal](8, 2) NULL,
	[MileageReceived] [int] NULL,
	[TradeOrPurchase] [tinyint] NOT NULL,
	[ListPrice] [decimal](8, 2) NULL,
	[InitialVehicleLight] [int] NULL,
	[InventoryType] [tinyint] NOT NULL,
	[ReconditionCost] [decimal](8, 2) NULL,
	[UsedSellingPrice] [decimal](8, 2) NULL,
	[Certified] [tinyint] NOT NULL,
	[VehicleLocation] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CurrentVehicleLight] [tinyint] NULL,
	[FLRecFollowed] [tinyint] NOT NULL,
	[Audit_ID] [int] NOT NULL,
	[InventoryStatusCD] [smallint] NOT NULL,
	[ListPriceLock] [tinyint] NOT NULL,
	[SpecialFinance] [tinyint] NULL,
	[eStockCardLock] [tinyint] NOT NULL,
	[DateBookoutLocked] [smalldatetime] NULL,
	[PlanReminderDate] [smalldatetime] NULL,
	[VersionNumber] [int] NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
	[EdmundsTMV] [decimal](8, 2) NULL,
	[BookoutRequired] [tinyint] NOT NULL,
	[MileageReceivedLock] [tinyint] NOT NULL,
	[LotPrice] [int] NOT NULL,
	[TransferPrice] [decimal](9, 2) NULL,
	[TransferForRetailOnly] [bit] NOT NULL,
	[TransferForRetailOnlyEnabled] [bit] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#SearchCandidateList](
	[SearchCandidateListID] [int] IDENTITY(1,1) NOT NULL,
	[SearchCandidateListTypeID] [int] NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[MemberID] [int] NOT NULL,
	[Created] [datetime] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#DealerTransferAllowRetailOnlyRule](
	[DealerTransferAllowRetailOnlyRuleID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[TransferPriceInventoryAgeRangeID] [int] NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#DealerPreference_Pricing](
	[BusinessUnitID] [int] NOT NULL,
	[PingII_DefaultSearchRadius] [int] NULL,
	[PingII_MaxSearchRadius] [int] NULL,
	[PingII_SupressSellerName] [tinyint] NULL,
	[PingII_ExcludeNoPriceFromCalc] [tinyint] NULL,
	[PingII_ExcludeLowPriceOutliersMultiplier] [decimal](4, 2) NULL,
	[PingII_ExcludeHighPriceOutliersMultiplier] [decimal](4, 2) NULL,
	[PingII_GuideBookId] [tinyint] NULL,
	[MarketDaysSupplyBasePeriod] [int] NULL,
	[VehicleSalesBasePeriod] [int] NULL,
	[VehicleUnwindPeriod] [int] NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#DealerFacts](
	[DealerFactID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[LastPolledDate] [datetime] NULL,
	[LastDMSReferenceDateNew] [datetime] NULL,
	[LastDMSReferenceDateUsed] [datetime] NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#InventoryBucketRanges](
	[InventoryBucketRangeID] [int] IDENTITY(1,1) NOT NULL,
	[InventoryBucketID] [int] NOT NULL,
	[RangeID] [tinyint] NOT NULL,
	[Description] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Low] [int] NOT NULL,
	[High] [int] NULL,
	[Lights] [tinyint] NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[LongDescription] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SortOrder] [tinyint] NOT NULL,
	[Value1] [smallint] NULL,
	[Value2] [smallint] NULL,
	[Value3] [smallint] NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#DealerTransferPriceUnitCostRule](
	[DealerTransferPriceUnitCostRuleID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[TransferPriceUnitCostValueID] [int] NOT NULL,
	[TransferPriceInventoryAgeRangeID] [int] NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#CIASummary](
	[CIASummaryID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[CIASummaryStatusID] [tinyint] NOT NULL,
	[DateCreated] [smalldatetime] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#BusinessUnitPreference_Bookout](
	[BusinessUnitID] [int] NOT NULL,
	[ProcessorPriority] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#tbl_NewCarScoreCard](
	[NewCarScoreCardId] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitId] [int] NOT NULL,
	[Active] [tinyint] NOT NULL,
	[WeekEnding] [smalldatetime] NOT NULL,
	[WeekNumber] [int] NOT NULL,
	[OverallUnitSaleCurrent] [int] NOT NULL,
	[OverallUnitSalePrior] [int] NOT NULL,
	[OverallUnitSaleThreeMonth] [decimal](8, 2) NOT NULL,
	[OverallRetailAvgGrossProfitTrend] [decimal](8, 2) NOT NULL,
	[OverallRetailAvgGrossProfit12Week] [decimal](8, 2) NOT NULL,
	[OverallFAndIAvgGrossProfitTrend] [decimal](8, 2) NOT NULL,
	[OverallFAndIAvgGrossProfit12Week] [decimal](8, 2) NOT NULL,
	[OverallAvgDaysToSaleTrend] [decimal](8, 2) NOT NULL,
	[OverallAvgDaysToSale12Week] [decimal](8, 2) NOT NULL,
	[OverallAvgInventoryAgeCurrent] [decimal](8, 2) NOT NULL,
	[OverallAvgInventoryAgePrior] [decimal](8, 2) NOT NULL,
	[OverallCurrentDaysSupplyCurrent] [decimal](8, 2) NOT NULL,
	[OverallCurrentDaysSupplyPrior] [decimal](8, 2) NOT NULL,
	[AgingOver90Current] [decimal](8, 2) NOT NULL,
	[AgingOver90Prior] [decimal](8, 2) NOT NULL,
	[Aging76To90Current] [decimal](8, 2) NOT NULL,
	[Aging76To90Prior] [decimal](8, 2) NOT NULL,
	[Aging61To75Current] [decimal](8, 2) NOT NULL,
	[Aging61To75Prior] [decimal](8, 2) NOT NULL,
	[Aging31To60Current] [decimal](8, 2) NOT NULL,
	[Aging31To60Prior] [decimal](8, 2) NOT NULL,
	[Aging0To30Current] [decimal](8, 2) NOT NULL,
	[Aging0To30Prior] [decimal](8, 2) NOT NULL,
	[CoupePercentOfSalesTrend] [int] NOT NULL,
	[CoupePercentOfSales12Week] [int] NOT NULL,
	[CoupeRetailAvgGrossProfitTrend] [int] NOT NULL,
	[CoupeRetailAvgGrossProfit12Week] [int] NOT NULL,
	[CoupeRetailAvgDaysToSaleTrend] [int] NOT NULL,
	[CoupeRetailAvgDaysToSale12Week] [int] NOT NULL,
	[SedanPercentOfSalesTrend] [int] NOT NULL,
	[SedanPercentOfSales12Week] [int] NOT NULL,
	[SedanRetailAvgGrossProfitTrend] [int] NOT NULL,
	[SedanRetailAvgGrossProfit12Week] [int] NOT NULL,
	[SedanRetailAvgDaysToSaleTrend] [int] NOT NULL,
	[SedanRetailAvgDaysToSale12Week] [int] NOT NULL,
	[SUVPercentOfSalesTrend] [int] NOT NULL,
	[SUVPercentOfSales12Week] [int] NOT NULL,
	[SUVRetailAvgGrossProfitTrend] [int] NOT NULL,
	[SUVRetailAvgGrossProfit12Week] [int] NOT NULL,
	[SUVRetailAvgDaysToSaleTrend] [int] NOT NULL,
	[SUVRetailAvgDaysToSale12Week] [int] NOT NULL,
	[TruckPercentOfSalesTrend] [int] NOT NULL,
	[TruckPercentOfSales12Week] [int] NOT NULL,
	[TruckRetailAvgGrossProfitTrend] [int] NOT NULL,
	[TruckRetailAvgGrossProfit12Week] [int] NOT NULL,
	[TruckRetailAvgDaysToSaleTrend] [int] NOT NULL,
	[TruckRetailAvgDaysToSale12Week] [int] NOT NULL,
	[VanPercentOfSalesTrend] [int] NOT NULL,
	[VanPercentOfSales12Week] [int] NOT NULL,
	[VanRetailAvgGrossProfitTrend] [int] NOT NULL,
	[VanRetailAvgGrossProfit12Week] [int] NOT NULL,
	[VanRetailAvgDaysToSaleTrend] [int] NOT NULL,
	[VanRetailAvgDaysToSale12Week] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#tbl_UsedCarScoreCard](
	[UsedCarScoreCardId] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitId] [int] NOT NULL,
	[Active] [tinyint] NOT NULL,
	[WeekEnding] [smalldatetime] NOT NULL,
	[WeekNumber] [int] NOT NULL,
	[OverallUnitSaleCurrent] [int] NOT NULL,
	[OverallUnitSalePrior] [int] NOT NULL,
	[OverallUnitSaleThreeMonth] [decimal](8, 2) NOT NULL,
	[OverallRetailAvgGrossProfitTrend] [decimal](8, 2) NOT NULL,
	[OverallRetailAvgGrossProfit12Week] [decimal](8, 2) NOT NULL,
	[OverallFAndIAvgGrossProfitTrend] [decimal](8, 2) NOT NULL,
	[OverallFAndIAvgGrossProfit12Week] [decimal](8, 2) NOT NULL,
	[OverallPercentSellThroughTrend] [decimal](8, 2) NOT NULL,
	[OverallPercentSellThrough12Week] [decimal](8, 2) NOT NULL,
	[OverallAvgDaysToSaleTrend] [decimal](8, 2) NOT NULL,
	[OverallAvgDaysToSale12Week] [decimal](8, 2) NOT NULL,
	[OverallAvgInventoryAgeCurrent] [decimal](8, 2) NOT NULL,
	[OverallAvgInventoryAgePrior] [decimal](8, 2) NOT NULL,
	[OverallCurrentDaysSupplyCurrent] [decimal](8, 2) NOT NULL,
	[OverallCurrentDaysSupplyPrior] [decimal](8, 2) NOT NULL,
	[Aging60PlusCurrent] [decimal](8, 2) NOT NULL,
	[Aging60PlusPrior] [decimal](8, 2) NOT NULL,
	[Aging50To59Current] [decimal](8, 2) NOT NULL,
	[Aging50To59Prior] [decimal](8, 2) NOT NULL,
	[Aging40To49Current] [decimal](8, 2) NOT NULL,
	[Aging40To49Prior] [decimal](8, 2) NOT NULL,
	[Aging30To39Current] [decimal](8, 2) NOT NULL,
	[Aging30To39Prior] [decimal](8, 2) NOT NULL,
	[PurchasedPercentWinnersTrend] [decimal](8, 2) NOT NULL,
	[PurchasedPercentWinners12Week] [decimal](8, 2) NOT NULL,
	[PurchasedRetailAvgGrossProfitTrend] [decimal](8, 2) NOT NULL,
	[PurchasedRetailAvgGrossProfit12Week] [decimal](8, 2) NOT NULL,
	[PurchasedPercentSellThroughTrend] [decimal](8, 2) NOT NULL,
	[PurchasedPercentSellThrough12Week] [decimal](8, 2) NOT NULL,
	[PurchasedAvgDaysToSaleTrend] [decimal](8, 2) NOT NULL,
	[PurchasedAvgDaysToSale12Week] [decimal](8, 2) NOT NULL,
	[PurchasedAvgInventoryAgeCurrent] [decimal](8, 2) NOT NULL,
	[PurchasedAvgInventoryAgePrior] [decimal](8, 2) NOT NULL,
	[TradeInPercentNonWinnersWholesaledTrend] [decimal](8, 2) NOT NULL,
	[TradeInPercentNonWinnersWholesaled12Week] [decimal](8, 2) NOT NULL,
	[TradeInRetailAvgGrossProfitTrend] [decimal](8, 2) NOT NULL,
	[TradeInRetailAvgGrossProfit12Week] [decimal](8, 2) NOT NULL,
	[TradeInPercentSellThroughTrend] [decimal](8, 2) NOT NULL,
	[TradeInPercentSellThrough12Week] [decimal](8, 2) NOT NULL,
	[TradeInAvgDaysToSaleTrend] [decimal](8, 2) NOT NULL,
	[TradeInAvgDaysToSale12Week] [decimal](8, 2) NOT NULL,
	[TradeInAvgInventoryAgeCurrent] [decimal](8, 2) NOT NULL,
	[TradeInAvgInventoryAgePrior] [decimal](8, 2) NOT NULL,
	[WholesaleImmediateAvgGrossProfitTrend] [decimal](8, 2) NOT NULL,
	[WholesaleImmediateAvgGrossProfit12Week] [decimal](8, 2) NOT NULL,
	[WholesaleAgedInventoryAvgGrossProfitTrend] [decimal](8, 2) NOT NULL,
	[WholesaleAgedInventoryAvgGrossProfit12Week] [decimal](8, 2) NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#HalSessions](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[HalSessionID] [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[DateModified] [smalldatetime] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#PackAdjustRules](
	[PackAdjustRuleID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[SaleDescription] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VehicleSource] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UnitCostThresholdLow] [int] NULL,
	[UnitCostThresholdHigh] [int] NULL,
	[PackAmount] [decimal](9, 2) NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#DealerPreference](
	[DealerPreferenceID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[CustomHomePageMessage] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DashboardColumnDisplayPreference] [int] NOT NULL,
	[UnitsSoldThreshold4Wks] [int] NOT NULL,
	[UnitsSoldThreshold8Wks] [int] NOT NULL,
	[UnitsSoldThreshold13Wks] [int] NOT NULL,
	[UnitsSoldThreshold26Wks] [int] NOT NULL,
	[UnitsSoldThreshold52Wks] [int] NOT NULL,
	[DefaultTrendingView] [int] NULL,
	[DefaultTrendingWeeks] [int] NULL,
	[DefaultForecastingWeeks] [int] NULL,
	[GuideBookID] [tinyint] NOT NULL,
	[NADARegionCode] [int] NOT NULL,
	[InceptionDate] [datetime] NULL,
	[AgingInventoryTrackingDisplayPref] [tinyint] NOT NULL,
	[PackAmount] [decimal](8, 2) NOT NULL,
	[StockOrVinPreference] [tinyint] NOT NULL,
	[MaxSalesHistoryDate] [datetime] NULL,
	[UnitCostOrListPricePreference] [tinyint] NULL,
	[ListPricePreference] [tinyint] NULL,
	[AgeBandTarget1] [int] NULL,
	[AgeBandTarget2] [int] NULL,
	[AgeBandTarget3] [int] NULL,
	[AgeBandTarget4] [int] NULL,
	[AgeBandTarget5] [int] NULL,
	[AgeBandTarget6] [int] NULL,
	[DaysSupply12WeekWeight] [int] NULL,
	[DaysSupply26WeekWeight] [int] NULL,
	[BookOut] [tinyint] NOT NULL,
	[BookOutPreferenceId] [int] NOT NULL,
	[UnitCostThreshold] [decimal](10, 2) NOT NULL,
	[BookOutSecondPreferenceId] [int] NULL,
	[GuideBook2Id] [tinyint] NULL,
	[GuideBook2BookOutPreferenceId] [int] NULL,
	[GuideBook2SecondBookOutPreferenceId] [int] NULL,
	[UnitCostUpdateOnSale] [bit] NOT NULL,
	[UnwindDaysThreshold] [tinyint] NOT NULL,
	[AuctionAreaId] [int] NOT NULL,
	[showLotLocationStatus] [tinyint] NULL
) ON [IMT]
SET ANSI_PADDING OFF
ALTER TABLE [DealerPreference] ADD [PopulateClassName] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [DealerPreference] ADD [BookOutPreferenceSecondId] [int] NULL
ALTER TABLE [DealerPreference] ADD [RunDayOfWeek] [int] NOT NULL
ALTER TABLE [DealerPreference] ADD [UnitCostThresholdLower] [int] NULL
ALTER TABLE [DealerPreference] ADD [UnitCostThresholdUpper] [int] NULL
ALTER TABLE [DealerPreference] ADD [FEGrossProfitThreshold] [int] NULL
ALTER TABLE [DealerPreference] ADD [MarginPercentile] [smallint] NULL
ALTER TABLE [DealerPreference] ADD [DaysToSalePercentile] [smallint] NULL
ALTER TABLE [DealerPreference] ADD [ProgramType_CD] [tinyint] NULL
ALTER TABLE [DealerPreference] ADD [SellThroughRate] [int] NOT NULL
ALTER TABLE [DealerPreference] ADD [IncludeBackEndInValuation] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [UnitsSoldThresholdInvOverview] [int] NULL
ALTER TABLE [DealerPreference] ADD [CIATargetDaysSupply] [int] NULL
ALTER TABLE [DealerPreference] ADD [CIAMarketPerformerInStockThreshold] [int] NULL
ALTER TABLE [DealerPreference] ADD [GoLiveDate] [smalldatetime] NULL
ALTER TABLE [DealerPreference] ADD [VehicleSaleThresholdForCoreMarketPenetration] [float] NULL
ALTER TABLE [DealerPreference] ADD [ATCEnabled] [tinyint] NULL
ALTER TABLE [DealerPreference] ADD [ApplyPriorAgingNotes] [bit] NOT NULL
ALTER TABLE [DealerPreference] ADD [averageInventoryAgeRedThreshold] [int] NULL
ALTER TABLE [DealerPreference] ADD [averageDaysSupplyRedThreshold] [int] NULL
ALTER TABLE [DealerPreference] ADD [PurchasingDistanceFromDealer] [int] NOT NULL
ALTER TABLE [DealerPreference] ADD [DisplayUnitCostToDealerGroup] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [UnitsSoldThreshold12Wks] [int] NULL
ALTER TABLE [DealerPreference] ADD [SearchInactiveInventoryDaysBackThreshold] [int] NOT NULL
ALTER TABLE [DealerPreference] ADD [CalculateAverageBookValue] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [CarfaxListVINsOnWebsite] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [TFSEnabled] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [GMACEnabled] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [TradeManagerDaysFilter] [int] NOT NULL
ALTER TABLE [DealerPreference] ADD [ShowroomDaysFilter] [int] NOT NULL
ALTER TABLE [DealerPreference] ADD [bucketJumperAIPApproach] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [aipRunDayOfWeek] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [FlashLocatorHideUnitCostDays] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [AppraisalRequirementLevel] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [CheckAppraisalHistoryForIMPPlanning] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [ApplyDefaultPlanToGreenLightsInIMP] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [AppraisalFormShowOptions] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [AppraisalFormShowPhotos] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [AppraisalFormValidDate] [int] NOT NULL
ALTER TABLE [DealerPreference] ADD [AppraisalFormValidMileage] [int] NOT NULL
ALTER TABLE [DealerPreference] ADD [AppraisalFormDisclaimer] [varchar](180) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
ALTER TABLE [DealerPreference] ADD [AppraisalFormMemo] [varchar](45) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
ALTER TABLE [DealerPreference] ADD [ShowAppraisalForm] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [LiveAuctionDistanceFromDealer] [int] NOT NULL
ALTER TABLE [DealerPreference] ADD [RepricePercentChangeThreshold] [int] NOT NULL
ALTER TABLE [DealerPreference] ADD [RepriceConfirmation] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [InternetAdvertiser_AdPriceEnabled] [bit] NOT NULL
ALTER TABLE [DealerPreference] ADD [InternetAdvertiser_SendZeroesAsNull] [bit] NOT NULL
ALTER TABLE [DealerPreference] ADD [KBBInventoryBookoutDatasetPreference] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [KBBAppraisalBookoutDatasetPreference] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [KBBInventoryDefaultCondition] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [GroupAppraisalSearchWeeks] [int] NOT NULL
ALTER TABLE [DealerPreference] ADD [ShowAppraisalValueGroup] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [ShowAppraisalFormOfferGroup] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [RedistributionNumTopDealers] [int] NOT NULL
ALTER TABLE [DealerPreference] ADD [RedistributionDealerDistance] [int] NOT NULL
ALTER TABLE [DealerPreference] ADD [RedistributionROI] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [RedistributionUnderstock] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [AuctionTimePeriodID] [int] NOT NULL
ALTER TABLE [DealerPreference] ADD [OVEEnabled] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [ShowCheckOnAppraisalForm] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [ShowInactiveAppraisals] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [AppraisalFormValuesTPCBitMask] [int] NOT NULL
ALTER TABLE [DealerPreference] ADD [AppraisalFormIncludeMileageAdjustment] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [WindowStickerTemplate] [int] NOT NULL
ALTER TABLE [DealerPreference] ADD [DefaultLithiaCarCenterMemberID] [int] NULL
SET ANSI_PADDING ON
ALTER TABLE [DealerPreference] ADD [twixURL] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [DealerPreference] ADD [UseLotPrice] [tinyint] NOT NULL
ALTER TABLE [DealerPreference] ADD [SearchAppraisalDaysBackThreshold] [int] NOT NULL
ALTER TABLE [DealerPreference] ADD [AppraisalLookBackPeriod] [int] NOT NULL
ALTER TABLE [DealerPreference] ADD [AppraisalLookForwardPeriod] [int] NOT NULL
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#VehiclePlanTrackingHeader](
	[VehiclePlanTrackingHeaderID] [int] IDENTITY(10000,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[VehicleCount] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ModelCount] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TotalInventoryDollars] [int] NULL,
	[Highlights] [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Status] [tinyint] NOT NULL,
	[Date] [smalldatetime] NULL,
	[LastModifiedDate] [smalldatetime] NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#InternetAdvertiserDealership](
	[InternetAdvertiserID] [int] NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[InternetAdvertisersDealershipCode] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IsLive] [bit] NOT NULL,
	[Notes] [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ContactFirstName] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ContactLastName] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ContactPhoneNumber] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ContactEmail] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Active] [tinyint] NULL,
	[status] [tinyint] NULL,
	[VerifiedBy] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VerifiedDate] [smalldatetime] NULL,
	[IncrementalExport] [bit] NOT NULL,
	[ExportClientID] [int] NULL,
	[ExportGuid] [varchar](36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#EquityAnalyzerSearches](
	[EquityAnalyzerSearchID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[Name] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThirdPartyCategoryID] [int] NOT NULL,
	[PercentMultiplier] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#SystemErrors](
	[SystemErrorID] [int] IDENTITY(1,1) NOT NULL,
	[Trace] [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DateCreated] [smalldatetime] NOT NULL,
	[BusinessUnitID] [int] NULL,
	[MemberID] [int] NULL
) ON [IMT]
SET ANSI_PADDING ON
ALTER TABLE [SystemErrors] ADD [HTTPSessionID] [varchar](127) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [SystemErrors] ADD [Product] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [SystemErrors] ADD [Source] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [SystemErrors] ADD [HalSessionID] [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#DealerPreference_JDPowerSettings](
	[BusinessUnitID] [int] NOT NULL,
	[PowerRegionID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#BusinessUnitCredential](
	[BusinessUnitID] [int] NOT NULL,
	[CredentialTypeID] [tinyint] NOT NULL,
	[Username] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Password] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ReynoldsAreaNumber] [int] NULL
) ON [IMT]
SET ANSI_PADDING ON
ALTER TABLE [BusinessUnitCredential] ADD [ReynoldsDealerNumber] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [BusinessUnitCredential] ADD [ReynoldsStoreNumber] [int] NULL
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#VehiclePlanTracking](
	[VehiclePlanTrackingID] [int] IDENTITY(100000,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[InventoryID] [int] NOT NULL,
	[VehiclePlanTrackingHeaderID] [int] NOT NULL,
	[Approach] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Date] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Created] [datetime] NULL,
	[Notes] [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Spiffs] [tinyint] NULL,
	[Promos] [tinyint] NULL,
	[Advertise] [tinyint] NULL,
	[SalePending] [tinyint] NULL,
	[Other] [tinyint] NULL,
	[Wholesaler] [tinyint] NULL,
	[Auction] [tinyint] NULL,
	[NameText] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RetailDate] [datetime] NULL,
	[WholesaleDate] [datetime] NULL,
	[RangeID] [int] NULL,
	[CurrentPlanVehicleAge] [int] NULL,
	[Level4Analysis] [varchar](150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[RETAIL] [tinyint] NULL,
	[WHOLESALE] [tinyint] NULL,
	[Reprice] [decimal](8, 2) NULL
) ON [IMT]
SET ANSI_PADDING OFF
ALTER TABLE [VehiclePlanTracking] ADD [SpiffNotes] [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#Person](
	[PersonID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[MemberID] [int] NULL,
	[LastName] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
SET ANSI_PADDING OFF
ALTER TABLE [Person] ADD [MiddleInitial] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
SET ANSI_PADDING ON
ALTER TABLE [Person] ADD [FirstName] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
SET ANSI_PADDING OFF
ALTER TABLE [Person] ADD [NickName] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [Person] ADD [Salutation] [varchar](6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
SET ANSI_PADDING ON
ALTER TABLE [Person] ADD [PhoneNumber] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [Person] ADD [Email] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#GDLight](
	[GroupingDescriptionLightId] [int] IDENTITY(1,1) NOT NULL,
	[GroupingDescriptionId] [int] NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[InventoryVehicleLightID] [tinyint] NOT NULL,
	[GridCell] [int] NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#DealerGridValues](
	[DealerGridValuesId] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[LightValue] [int] NOT NULL,
	[TypeValue] [int] NOT NULL,
	[indexKey] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#DealerGridThresholds](
	[DealerGridThresholdsId] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[firstValuationThreshold] [int] NOT NULL,
	[secondValuationThreshold] [int] NOT NULL,
	[thirdValuationThreshold] [int] NOT NULL,
	[fourthValuationThreshold] [int] NOT NULL,
	[firstDealThreshold] [int] NOT NULL,
	[secondDealThreshold] [int] NOT NULL,
	[thirdDealThreshold] [int] NOT NULL,
	[fourthDealThreshold] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#SoftwareSystemComponentState](
	[SoftwareSystemComponentStateID] [int] IDENTITY(1,1) NOT NULL,
	[SoftwareSystemComponentID] [int] NOT NULL,
	[AuthorizedMemberID] [int] NOT NULL,
	[DealerGroupID] [int] NOT NULL,
	[DealerID] [int] NULL,
	[MemberID] [int] NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#AppraisalBusinessUnitGroupDemand](
	[AppraisalBusinessUnitGroupDemandID] [int] IDENTITY(1,1) NOT NULL,
	[AppraisalID] [int] NOT NULL,
	[BusinessUnitID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#DealerPack](
	[DealerPackID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[PackTypeCD] [tinyint] NOT NULL,
	[PackAmount] [decimal](8, 2) NULL,
	[PackJavaClass] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#MemberBusinessUnitSetItem](
	[MemberBusinessUnitSetID] [int] NOT NULL,
	[BusinessUnitID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#MemberBusinessUnitSet](
	[MemberBusinessUnitSetID] [int] IDENTITY(1,1) NOT NULL,
	[MemberBusinessUnitSetTypeID] [int] NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[MemberID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#ThirdPartyEntity](
	[ThirdPartyEntityID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[ThirdPartyEntityTypeID] [tinyint] NOT NULL,
	[Name] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#VehicleBookoutState](
	[VehicleBookoutStateID] [int] IDENTITY(1,1) NOT NULL,
	[VIN] [varchar](17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#MarketToZip](
	[ZipCode] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[BusinessUnitId] [int] NOT NULL,
	[Ring] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#tbl_DealerValuation](
	[DealerValuationId] [int] IDENTITY(1,1) NOT NULL,
	[DealerId] [int] NOT NULL,
	[DiscountRate] [decimal](6, 4) NULL,
	[TimeHorizon] [decimal](8, 4) NULL,
	[Date] [datetime] NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#MemberAccess](
	[MemberID] [int] NOT NULL,
	[BusinessUnitID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#MarketShare](
	[MarketShareID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[GroupingDescriptionID] [int] NOT NULL,
	[VehicleYear] [int] NOT NULL,
	[MarketShare] [decimal](8, 2) NOT NULL,
	[NumberOfWeeks] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#MemberAppraisalReviewPreferences](
	[MemberAppraisalReviewPreferenceID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[DaysBack] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#MemberInsightTypePreference](
	[MemberInsightTypePreference] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[MemberID] [int] NOT NULL,
	[InsightTypeID] [int] NOT NULL,
	[Active] [bit] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#KBBArchiveDatasetOverride](
	[KBBArchiveDatasetOverrideID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[MemberID] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#AuditBookOuts](
	[AuditId] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitId] [int] NOT NULL,
	[MemberId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[ReferenceId] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BookId] [tinyint] NOT NULL,
	[BookOutTimeStamp] [smalldatetime] NOT NULL,
	[BookOutId] [int] NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#Appraisals](
	[AppraisalID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[VehicleID] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[MemberID] [int] NULL,
	[DealCompleted] [tinyint] NULL,
	[Sold] [tinyint] NULL,
	[Color] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Mileage] [int] NOT NULL,
	[DealTrackStockNumber] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DealTrackNewOrUsed] [tinyint] NULL,
	[DateLocked] [smalldatetime] NULL,
	[DateModified] [smalldatetime] NOT NULL,
	[DealTrackSalespersonID] [int] NULL,
	[AppraisalSourceID] [tinyint] NOT NULL,
	[EdmundsTMV] [decimal](8, 2) NULL,
	[AppraisalStatusID] [tinyint] NOT NULL,
	[AppraisalTypeID] [int] NOT NULL,
	[SelectedBuyerId] [int] NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#tbl_RedistributionLog](
	[RedistributionLogId] [int] IDENTITY(1,1) NOT NULL,
	[VIN] [varchar](17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OriginatingDealerId] [int] NOT NULL,
	[DestinationDealerId] [int] NULL,
	[RedistributionSourceId] [tinyint] NOT NULL,
	[ReceivedDate] [smalldatetime] NULL,
	[OriginatingDealerStockNumber] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DestinationDealerStockNumber] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MemberId] [int] NOT NULL,
	[TransactionDate] [smalldatetime] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#tbl_GroupingPromotionPlan](
	[GroupingPromotionPlanId] [int] IDENTITY(1,1) NOT NULL,
	[GroupingDescriptionId] [int] NOT NULL,
	[PromotionStartDate] [smalldatetime] NOT NULL,
	[PromotionEndDate] [smalldatetime] NOT NULL,
	[Notes] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BusinessUnitID] [int] NOT NULL,
	[Created] [smalldatetime] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#DealerInsightTargetPreference](
	[DealerInsightTargetPreferenceID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[InsightTargetID] [int] NOT NULL,
	[Value] [int] NULL,
	[Min] [int] NULL,
	[Max] [int] NULL,
	[Threshold] [int] NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#tbl_MemberATCAccessGroupList](
	[MemberATCAccessGroupListID] [int] IDENTITY(1,1) NOT NULL,
	[MemberATCAccessGroupListTypeID] [int] NOT NULL,
	[MemberID] [int] NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[MaxDistanceFromDealer] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#BusinessUnitPhotos](
	[BusinessUnitID] [int] NOT NULL,
	[PhotoID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#DealerGroupPreference](
	[DealerGroupPreferenceID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[AgingPolicy] [int] NULL,
	[AgingPolicyAdjustment] [int] NULL,
	[IncludeRedistribution] [tinyint] NULL,
	[InventoryExchangeAgeLimit] [int] NULL,
	[IncludeRoundTable] [tinyint] NULL,
	[IncludeCIA] [tinyint] NULL,
	[IncludeRedLightAgingPlan] [tinyint] NULL,
	[PricePointDealsThreshold] [int] NULL,
	[LithiaStore] [tinyint] NOT NULL,
	[ShowAppraisalFormOffer] [tinyint] NULL,
	[ShowAppraisalValue] [tinyint] NULL,
	[IncludePerformanceManagementCenter] [bit] NOT NULL,
	[IncludeTransferPricing] [bit] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#DealerFinancialStatement](
	[DealerFinancialStatementID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[MonthNumber] [int] NOT NULL,
	[YearNumber] [int] NOT NULL,
	[RetailNumberOfDeals] [int] NULL,
	[RetailTotalGrossProfit] [decimal](9, 2) NULL,
	[RetailFrontEndGrossProfit] [decimal](9, 2) NULL,
	[RetailBackEndGrossProfit] [decimal](9, 2) NULL,
	[RetailAverageGrossProfit] [decimal](9, 2) NULL,
	[WholesaleNumberOfDeals] [int] NULL,
	[WholesaleTotalGrossProfit] [decimal](9, 2) NULL,
	[WholesaleFrontEndGrossProfit] [decimal](9, 2) NULL,
	[WholesaleBackEndGrossProfit] [decimal](9, 2) NULL,
	[WholesaleAverageGrossProfit] [decimal](9, 2) NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#DealerCIAPreferences](
	[DealerCIAPreferencesID] [int] IDENTITY(100000,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[ShortAgeBandID] [int] NULL,
	[LongAgeBandID] [int] NULL,
	[ForecastAgeBandID] [int] NULL,
	[ShortAgeBandWeight] [int] NULL,
	[LongAgeBandWeight] [int] NULL,
	[ForecastAgeBandWeight] [int] NULL,
	[NumberOfPowerZoneMakeModels] [int] NULL,
	[MinRecommendedDaysSupply] [int] NULL,
	[MaxRecommendedDaysSupply] [int] NULL,
	[ApplyNoSalePctThreshold] [tinyint] NULL,
	[NoSalePctThreshold] [int] NULL,
	[ThresholdOnDaysSupply] [decimal](8, 1) NULL,
	[MinRecommendedStockingLevel] [int] NULL,
	[MaxRecommendedStockingLevel] [int] NULL,
	[ShortAgeBandUnitsSoldThreshold] [int] NULL,
	[LongAgeBandUnitsSoldThreshold] [int] NULL,
	[ForecastAgeBandUnitsSoldThreshold] [int] NULL,
	[MetricAverageGrossProfit] [tinyint] NULL,
	[MetricAverageFrontEndGrossProfitPercentage] [int] NULL,
	[MetricAverageBackEndGrossProfitPercentage] [int] NULL,
	[MetricAverageCombinedGrossProfitPercentage] [int] NULL,
	[MetricAverageGrossProfitCombined] [tinyint] NULL,
	[MetricAverageUnitCost] [tinyint] NULL,
	[MetricAverageMileage] [tinyint] NULL,
	[MetricAverageSalePrice] [tinyint] NULL,
	[MetricAverageDaysToSale] [tinyint] NULL,
	[MetricAverageMargin] [tinyint] NULL,
	[MetricPercentOutOfStock] [tinyint] NULL,
	[MetricAdjustedGrossProfit] [tinyint] NULL,
	[MetricContribution] [tinyint] NULL,
	[MetricContributionFrontEndPercentage] [int] NULL,
	[MetricContributionBackEndPercentage] [int] NULL,
	[MetricContributionCombinedPercentage] [int] NULL,
	[MetricContributionCombined] [tinyint] NULL,
	[MetricContributionNoSaleLossFactor] [tinyint] NULL,
	[MetricAverageUnitCostPercentage] [int] NULL,
	[MetricAverageMileagePercentage] [int] NULL,
	[MetricAverageSalePricePercentage] [int] NULL,
	[MetricAverageDaysToSalePercentage] [int] NULL,
	[MetricAverageMarginPercentage] [int] NULL,
	[MetricPercentOutOfStockPercentage] [int] NULL,
	[MetricAdjustedGrossProfitPercentage] [int] NULL,
	[MetricInventoryHoldingAmount] [int] NULL,
	[WinnerScoreThreshold] [int] NULL,
	[MaxCIAVehicles] [int] NULL,
	[PowerZoneParentRankPercentage] [int] NULL,
	[MarketRankPercentage] [int] NULL,
	[ModelPerformance] [tinyint] NULL,
	[ModelPerformancePercentage] [int] NULL,
	[SalesPerMonthFirstLook] [tinyint] NULL,
	[SalesPerMonthFirstLookPercentage] [int] NULL,
	[SalesPerMonthSimilar] [tinyint] NULL,
	[SalesPerMonthSimilarPercentage] [int] NULL,
	[MinWeightVariable] [int] NULL,
	[MaxWeightVariable] [int] NULL,
	[DisplayGoodBets] [tinyint] NULL,
	[GoodBetScoreThreshold] [int] NULL,
	[ExcludeRedLightVehicle] [tinyint] NULL,
	[GoodBetMinTargetRange] [int] NULL,
	[GoodBetMaxTargetRange] [int] NULL,
	[UseInventoryHolding] [tinyint] NOT NULL,
	[RunDayOfWeek] [int] NOT NULL,
	[Modifiable] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#ReportCenterSessions](
	[ReportCenterSessionId] [int] IDENTITY(1,1) NOT NULL,
	[SessionId] [varchar](42) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[rcSessionId] [varchar](42) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BusinessUnitId] [int] NOT NULL,
	[MemberId] [int] NOT NULL,
	[AccessTime] [smalldatetime] NOT NULL,
	[DoAuthentication] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#DMSExportBusinessUnitPreference](
	[DMSExportBusinessUnitPreferenceID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[DMSExportID] [int] NOT NULL,
	[isActive] [tinyint] NULL,
	[ClientSystemID] [varchar](11) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DMSIPAddress] [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DMSDialUpPhone1] [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DMSDialUpPhone2] [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DMSLogin] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DMSPassword] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AccountLogon] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FinanceLogon] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[StoreNumber] [int] NOT NULL,
	[BranchNumber] [int] NULL,
	[DMSExportFrequencyID] [tinyint] NOT NULL,
	[DMSExportDailyHourOfDay] [int] NULL,
	[DealerName] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DealerPhoneOffice] [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DealerPhoneCell] [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DealerEmail] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DMSExportInternetPriceMappingID] [int] NULL,
	[DMSExportLotPriceMappingID] [int] NULL,
	[AreaNumber] [int] NULL,
	[ReynoldsDealerNo] [int] NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#tbl_CIAPreferences](
	[BusinessUnitId] [int] NOT NULL,
	[PowerZoneGroupingThreshold] [int] NOT NULL,
	[BucketAllocationMinimumThreshold] [int] NOT NULL,
	[TargetDaysSupply] [int] NOT NULL,
	[MarketPerformersDisplayThreshold] [int] NOT NULL,
	[MarketPerformersInStockThreshold] [int] NOT NULL,
	[MarketPerformersUnitsThreshold] [int] NOT NULL,
	[MarketPerformersZipCodeThreshold] [decimal](9, 2) NOT NULL,
	[GDLightProcessorTimePeriodId] [int] NOT NULL,
	[SalesHistoryDisplayTimePeriodId] [int] NOT NULL,
	[CiaStoreTargetInventoryBasisPeriodId] [int] NOT NULL,
	[CiaCoreModelDeterminationBasisPeriodId] [int] NOT NULL,
	[CiaPowerzoneModelTargetInventoryBasisPeriodId] [int] NOT NULL,
	[CiaCoreModelYearAllocationBasisPeriodId] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#FLUSAN_EventLog](
	[FLUSAN_EventLogID] [int] IDENTITY(1,1) NOT NULL,
	[FLUSAN_EventTypeCode] [tinyint] NOT NULL,
	[BusinessUnitID] [int] NULL,
	[MemberID] [int] NULL,
	[ChannelID] [int] NULL,
	[VIN] [varchar](17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[URL] [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DateCreated] [datetime] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#DealerAuctionPreference](
	[BusinessUnitID] [int] NOT NULL,
	[AuctionSearchEnabled] [tinyint] NOT NULL,
	[MaxMilesAway] [smallint] NOT NULL,
	[MaxDaysAhead] [smallint] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#BusinessUnitRelationship](
	[BusinessUnitID] [int] NOT NULL,
	[ParentID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#SearchCandidateCIACategoryAnnotation](
	[SearchCandidateID] [int] NOT NULL,
	[CIACategoryID] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#SearchCandidateOption](
	[SearchCandidateOptionID] [int] IDENTITY(1,1) NOT NULL,
	[SearchCandidateID] [int] NOT NULL,
	[ModelYear] [int] NULL,
	[Expires] [datetime] NULL,
	[Suppress] [bit] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#BusinessUnit](
	[BusinessUnitID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitTypeID] [int] NOT NULL,
	[BusinessUnit] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[BusinessUnitShortName] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BusinessUnitCode] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Address1] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Address2] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[City] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[State] [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ZipCode] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OfficePhone] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OfficeFax] [varchar](15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Active] [tinyint] NOT NULL,
	[OJBConcreteClass] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#Photos](
	[PhotoID] [int] IDENTITY(1,1) NOT NULL,
	[PhotoTypeID] [tinyint] NOT NULL,
	[PhotoProviderID] [int] NULL,
	[PhotoStatusCode] [tinyint] NOT NULL,
	[PhotoURL] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OriginalPhotoURL] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[PhotoUpdated] [datetime] NOT NULL,
	[IsPrimaryPhoto] [bit] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#InsightType](
	[InsightTypeID] [int] NOT NULL,
	[InsightCategoryID] [int] NOT NULL,
	[Name] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#InventoryPhotos](
	[InventoryID] [int] NOT NULL,
	[PhotoID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#AppraisalPhotos](
	[AppraisalID] [int] NOT NULL,
	[PhotoID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#InsightTarget](
	[InsightTargetID] [int] IDENTITY(1,1) NOT NULL,
	[InsightTargetTypeID] [int] NOT NULL,
	[InsightTargetValueTypeID] [int] NOT NULL,
	[InsightTypeID] [int] NOT NULL,
	[InsightContext] [int] NULL,
	[Name] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Value] [int] NULL,
	[Min] [int] NULL,
	[Max] [int] NULL,
	[Threshold] [int] NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#Audit_Sales](
	[AUDIT_ID] [int] NOT NULL,
	[STATUS] [tinyint] NOT NULL,
	[PACK_ADJUST] [decimal](8, 2) NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#Audit_Inventory](
	[AUDIT_ID] [int] NOT NULL,
	[STATUS] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#SearchCandidateOptionAnnotation](
	[SearchCandidateOptionID] [int] NOT NULL,
	[SearchCandidateOptionAnnotationTypeID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#CIABuckets](
	[CIABucketID] [int] IDENTITY(1,1) NOT NULL,
	[CIABucketGroupID] [tinyint] NOT NULL,
	[Rank] [tinyint] NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#CIABucketRules](
	[CIABucketRuleID] [int] IDENTITY(1,1) NOT NULL,
	[CIABucketID] [int] NOT NULL,
	[CIABucketRuleTypeID] [tinyint] NOT NULL,
	[Value] [int] NOT NULL,
	[Value2] [int] NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#OptionDetail](
	[OptionDetailID] [int] IDENTITY(1,1) NOT NULL,
	[OptionName] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OptionKey] [int] NOT NULL,
	[DataSourceID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#tbl_MemberATCAccessGroup](
	[MemberATCAccessGroupID] [int] IDENTITY(1,1) NOT NULL,
	[MemberATCAccessGroupListID] [int] NOT NULL,
	[MaxDistanceFromDealer] [int] NOT NULL,
	[Position] [int] NOT NULL,
	[Suppress] [bit] NOT NULL,
	[AccessGroupID] [int] NOT NULL,
	[MaxVehicleMileage] [int] NULL,
	[MinVehicleMileage] [int] NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#FirstLookRegionToZip](
	[FirstLookRegionID] [int] NOT NULL,
	[ZipCode] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#FranchiseAlias](
	[FranchiseAliasID] [int] IDENTITY(1,1) NOT NULL,
	[FranchiseID] [int] NOT NULL,
	[FranchiseAlias] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#MakeModelGrouping](
	[MakeModelGroupingID] [int] IDENTITY(100000,1) NOT NULL,
	[GroupingDescriptionID] [int] NOT NULL,
	[Make] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Model] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SegmentID] [int] NOT NULL,
	[Line] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ModelID] [int] NULL,
	[DateCreated] [smalldatetime] NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#CIAGroupingItems](
	[CIAGroupingItemID] [int] IDENTITY(1,1) NOT NULL,
	[CIABodyTypeDetailID] [int] NOT NULL,
	[GroupingDescriptionID] [int] NOT NULL,
	[CIACategoryID] [tinyint] NOT NULL,
	[MarketShare] [decimal](4, 4) NULL,
	[Valuation] [decimal](10, 2) NULL,
	[MarketPenetration] [decimal](10, 4) NULL,
	[Notes] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#map_MemberToInvStatusFilter](
	[MemberToInvStatusFilterId] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NOT NULL,
	[InventoryStatusCD] [smallint] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#tbl_Vehicle](
	[VehicleID] [int] IDENTITY(1,1) NOT NULL,
	[Vin] [varchar](17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[VehicleYear] [int] NOT NULL,
	[MakeModelGroupingID] [int] NOT NULL,
	[VehicleTrim] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[VehicleEngine] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[VehicleDriveTrain] [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[VehicleTransmission] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[CylinderCount] [int] NULL,
	[DoorCount] [int] NULL,
	[BaseColor] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[InteriorColor] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FuelType] [varchar](3) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[InteriorDescription] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CreateDt] [smalldatetime] NOT NULL,
	[OriginalMake] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OriginalModel] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OriginalYear] [smallint] NULL,
	[OriginalTrim] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OriginalBodyStyle] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Audit_ID] [int] NOT NULL,
	[VehicleSourceID] [int] NOT NULL
) ON [IMT]
SET ANSI_PADDING OFF
ALTER TABLE [tbl_Vehicle] ADD [CatalogKey] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [tbl_Vehicle] ADD [VIC] [char](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [tbl_Vehicle] ADD [VehicleCatalogID] [int] NULL
ALTER TABLE [tbl_Vehicle] ADD [BodyTypeID] [int] NOT NULL
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#AppraisalActions](
	[AppraisalActionID] [int] IDENTITY(1,1) NOT NULL,
	[AppraisalID] [int] NOT NULL,
	[AppraisalActionTypeID] [tinyint] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[WholesalePrice] [decimal](12, 2) NULL,
	[EstimatedReconditioningCost] [decimal](12, 2) NULL,
	[ConditionDescription] [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[TransferPrice] [decimal](9, 2) NULL,
	[TransferForRetailOnly] [bit] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#AppraisalValues](
	[AppraisalValueID] [int] IDENTITY(1,1) NOT NULL,
	[AppraisalID] [int] NULL,
	[SequenceNumber] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[Value] [int] NOT NULL,
	[AppraiserName] [varchar](35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#AppraisalCustomer](
	[AppraisalID] [int] NOT NULL,
	[Email] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
SET ANSI_PADDING ON
ALTER TABLE [AppraisalCustomer] ADD [FirstName] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [AppraisalCustomer] ADD [LastName] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
SET ANSI_PADDING OFF
ALTER TABLE [AppraisalCustomer] ADD [Gender] [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
SET ANSI_PADDING ON
ALTER TABLE [AppraisalCustomer] ADD [PhoneNumber] [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#Staging_VehicleHistoryReport](
	[Staging_VehicleHistoryReportId] [int] IDENTITY(1,1) NOT NULL,
	[AppraisalID] [int] NOT NULL,
	[CredentialTypeID] [tinyint] NOT NULL,
	[UserName] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#Staging_Bookouts](
	[Staging_BookoutID] [int] IDENTITY(1,1) NOT NULL,
	[AppraisalID] [int] NOT NULL,
	[ThirdPartyID] [tinyint] NOT NULL,
	[ThirdPartyVehicleCode] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#AppraisalBookouts](
	[BookoutID] [int] NOT NULL,
	[AppraisalID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#AppraisalWindowStickers](
	[AppraisalWindowStickerID] [int] IDENTITY(1,1) NOT NULL,
	[AppraisalID] [int] NOT NULL,
	[StockNumber] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SellingPrice] [int] NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#AppraisalFormOptions](
	[AppraisalFormID] [int] IDENTITY(1,1) NOT NULL,
	[AppraisalID] [int] NULL,
	[AppraisalFormOffer] [int] NULL,
	[AppraisalFormHistory] [int] NULL,
	[showEquipmentOptions] [tinyint] NULL,
	[showPhotos] [tinyint] NULL,
	[includeMileageAdjustment] [tinyint] NOT NULL,
	[showKbbConsumerValue] [tinyint] NOT NULL,
	[CategoriesToDisplayBitMask] [int] NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#BookoutValues](
	[BookoutValueID] [int] IDENTITY(1,1) NOT NULL,
	[BookoutThirdPartyCategoryID] [int] NOT NULL,
	[BookoutValueTypeID] [tinyint] NOT NULL,
	[ThirdPartySubCategoryID] [int] NULL,
	[Value] [int] NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#VehicleBookoutAdditionalState](
	[VehicleBookoutStateID] [int] NOT NULL,
	[ThirdPartySubCategoryID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#AIP_EventType](
	[AIP_EventTypeID] [tinyint] NOT NULL,
	[Description] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AIP_EventCategoryID] [tinyint] NOT NULL,
	[CurrentWhenSingleDayPlan] [bit] NOT NULL,
	[CurrentAtCreateDate] [bit] NOT NULL,
	[ThirdPartyEntityTypeID] [tinyint] NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#MemberRole](
	[MemberRoleID] [int] IDENTITY(1,1) NOT NULL,
	[MemberID] [int] NOT NULL,
	[RoleID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#MarketplaceSubmission](
	[MarketplaceSubmissionID] [int] IDENTITY(1,1) NOT NULL,
	[MarketplaceID] [int] NOT NULL,
	[InventoryID] [int] NOT NULL,
	[MemberID] [int] NOT NULL,
	[MarketplaceSubmissionStatusCD] [tinyint] NOT NULL,
	[URL] [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[BeginEffectiveDate] [datetime] NOT NULL,
	[EndEffectiveDate] [datetime] NOT NULL,
	[DateCreated] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#MemberLicenseAcceptance](
	[MemberID] [int] NOT NULL,
	[LicenseID] [int] NOT NULL,
	[DateCreated] [smalldatetime] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#MemberCredential](
	[MemberID] [int] NOT NULL,
	[CredentialTypeID] [tinyint] NOT NULL,
	[Username] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Password] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MemberCredentialID] [int] IDENTITY(1,1) NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#AIP_Event](
	[AIP_EventID] [int] IDENTITY(1,1) NOT NULL,
	[InventoryID] [int] NOT NULL,
	[AIP_EventTypeID] [tinyint] NOT NULL,
	[BeginDate] [smalldatetime] NOT NULL,
	[EndDate] [smalldatetime] NULL,
	[Notes] [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Notes2] [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ThirdPartyEntityID] [int] NULL,
	[Value] [decimal](9, 2) NULL,
	[Created] [smalldatetime] NOT NULL
) ON [IMT]
SET ANSI_PADDING OFF
ALTER TABLE [AIP_Event] ADD [CreatedBy] [varchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
ALTER TABLE [AIP_Event] ADD [LastModified] [smalldatetime] NOT NULL
ALTER TABLE [AIP_Event] ADD [LastModifiedBy] [varchar](128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
ALTER TABLE [AIP_Event] ADD [VersionNumber] [int] NOT NULL
ALTER TABLE [AIP_Event] ADD [UserBeginDate] [smalldatetime] NULL
ALTER TABLE [AIP_Event] ADD [UserEndDate] [smalldatetime] NULL
ALTER TABLE [AIP_Event] ADD [Notes3] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [AIP_Event] ADD [RepriceSourceID] [int] NULL
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#VehicleBookoutStateThirdPartyVehicles](
	[ThirdPartyVehicleID] [int] NOT NULL,
	[VehicleBookoutStateID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#tbl_CIACompositeTimePeriod](
	[CIACompositeTimePeriodId] [int] IDENTITY(1,1) NOT NULL,
	[PriorTimePeriodId] [int] NOT NULL,
	[ForecastTimePeriodId] [int] NOT NULL,
	[Description] [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#PrintAdvertiser_ThirdPartyEntity](
	[PrintAdvertiserID] [int] NOT NULL,
	[ContactEmail] [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FaxNumber] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DisplayPrice] [tinyint] NOT NULL,
	[VehicleOptionThirdPartyID] [tinyint] NOT NULL,
	[QuickAdvertiser] [tinyint] NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#InternetAdvertiser_ThirdPartyEntity](
	[InternetAdvertiserID] [int] NOT NULL,
	[DatafeedCode] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#DMSExport_ThirdPartyEntity](
	[DMSExportID] [int] NOT NULL,
	[Description] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#RepriceExport](
	[RepriceExportID] [int] IDENTITY(1,1) NOT NULL,
	[RepriceEventID] [int] NOT NULL,
	[RepriceExportStatusID] [int] NOT NULL,
	[ThirdPartyEntityID] [int] NOT NULL,
	[FailureReason] [varchar](2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FailureCount] [int] NOT NULL,
	[DateCreated] [datetime] NULL,
	[DateSent] [datetime] NULL,
	[StatusModifiedDate] [datetime] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#PhotoProvider_ThirdPartyEntity](
	[PhotoProviderID] [int] NOT NULL,
	[DatafeedCode] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[PhotoStorageCode] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#Member](
	[MemberID] [int] IDENTITY(1,1) NOT NULL,
	[Login] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Password] [varchar](80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Salutation] [varchar](6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[FirstName] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[PreferredFirstName] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MiddleInitial] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastName] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OfficePhoneNumber] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OfficePhoneExtension] [varchar](6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[OfficeFaxNumber] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[MobilePhoneNumber] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[PagerNumber] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[EmailAddress] [varchar](129) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[ReportMethod] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[Notes] [varchar](1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DashboardRowDisplay] [int] NOT NULL,
	[ReportPreference] [int] NULL,
	[LoginStatus] [int] NULL,
	[MemberType] [int] NULL,
	[CreateDate] [datetime] NOT NULL,
	[UserRoleCD] [tinyint] NOT NULL,
	[DefaultDealerGroupID] [int] NULL
) ON [IMT]
SET ANSI_PADDING OFF
ALTER TABLE [Member] ADD [sms_address] [varchar](129) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [Member] ADD [JobTitleID] [tinyint] NOT NULL
ALTER TABLE [Member] ADD [SearchHomePage] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [Member] ADD [InventoryOverviewSortOrderType] [tinyint] NOT NULL
ALTER TABLE [Member] ADD [GroupHomePageID] [tinyint] NOT NULL
ALTER TABLE [Member] ADD [ActiveInventoryLaunchTool] [tinyint] NOT NULL
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#ThirdPartyCategories](
	[ThirdPartyCategoryID] [int] IDENTITY(1,1) NOT NULL,
	[ThirdPartyID] [tinyint] NOT NULL,
	[Category] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#ThirdPartyVehicles](
	[ThirdPartyVehicleID] [int] IDENTITY(1,1) NOT NULL,
	[ThirdPartyID] [tinyint] NOT NULL,
	[ThirdPartyVehicleCode] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Description] [varchar](65) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
SET ANSI_PADDING ON
ALTER TABLE [ThirdPartyVehicles] ADD [MakeCode] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [ThirdPartyVehicles] ADD [ModelCode] [varchar](5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [ThirdPartyVehicles] ADD [Status] [tinyint] NOT NULL
ALTER TABLE [ThirdPartyVehicles] ADD [DateCreated] [datetime] NOT NULL
ALTER TABLE [ThirdPartyVehicles] ADD [DateModified] [datetime] NULL
SET ANSI_PADDING OFF
ALTER TABLE [ThirdPartyVehicles] ADD [EngineType] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [ThirdPartyVehicles] ADD [Make] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [ThirdPartyVehicles] ADD [Model] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
SET ANSI_PADDING ON
ALTER TABLE [ThirdPartyVehicles] ADD [Body] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#Bookouts](
	[BookoutID] [int] IDENTITY(1,1) NOT NULL,
	[BookoutSourceID] [tinyint] NOT NULL,
	[ThirdPartyID] [tinyint] NOT NULL,
	[DateCreated] [datetime] NULL,
	[Region] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[DatePublished] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[BookPrice] [int] NULL,
	[Weight] [int] NULL,
	[MileageCostAdjustment] [int] NULL,
	[MSRP] [int] NULL,
	[BookoutStatusID] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#Staging_BookoutValues](
	[Staging_BookoutValueID] [int] IDENTITY(1,1) NOT NULL,
	[Staging_BookoutID] [int] NOT NULL,
	[ThirdPartyCategoryID] [int] NOT NULL,
	[BaseValue] [int] NOT NULL,
	[MileageAdjustment] [int] NULL,
	[FinalValue] [int] NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#ThirdPartySubCategory](
	[ThirdPartySubCategoryID] [int] IDENTITY(1,1) NOT NULL,
	[ThirdPartyCategoryID] [int] NOT NULL,
	[Description] [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#BookoutThirdPartyCategories](
	[BookoutThirdPartyCategoryID] [int] IDENTITY(1,1) NOT NULL,
	[BookoutID] [int] NOT NULL,
	[ThirdPartyCategoryID] [int] NOT NULL,
	[IsValid] [tinyint] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#Staging_BookoutOptions](
	[Staging_BookoutOptionID] [int] IDENTITY(1,1) NOT NULL,
	[Staging_BookoutID] [int] NOT NULL,
	[ThirdPartyCategoryID] [int] NULL,
	[ThirdPartyOptionTypeID] [tinyint] NOT NULL,
	[OptionKey] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OptionName] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[isSelected] [tinyint] NOT NULL,
	[Value] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#SubscriptionTypeSubscriptionFrequencyDetail](
	[SubscriptionTypeSubscriptionFrequencyDetailID] [tinyint] IDENTITY(1,1) NOT NULL,
	[SubscriptionTypeID] [tinyint] NOT NULL,
	[SubscriptionFrequencyDetailID] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#Subscriptions](
	[SubscriptionID] [int] IDENTITY(1,1) NOT NULL,
	[SubscriberID] [int] NOT NULL,
	[SubscriberTypeID] [tinyint] NOT NULL,
	[SubscriptionTypeID] [tinyint] NOT NULL,
	[SubscriptionDeliveryTypeID] [tinyint] NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[SubscriptionFrequencyDetailID] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#Target](
	[TargetCD] [smallint] NOT NULL,
	[TargetName] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[TargetMin] [int] NULL,
	[TargetMax] [int] NULL,
	[SectionCD] [smallint] NOT NULL,
	[TargetTypeCD] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#CIABodyTypeDetails](
	[CIABodyTypeDetailID] [int] IDENTITY(1,1) NOT NULL,
	[SegmentID] [int] NOT NULL,
	[CIASummaryID] [int] NOT NULL,
	[NonCoreTargetUnits] [int] NOT NULL,
	[DisplaySortOrder] [int] NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#ThirdPartyOptions](
	[ThirdPartyOptionID] [int] IDENTITY(1,1) NOT NULL,
	[OptionName] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[OptionKey] [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[ThirdPartyOptionTypeID] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO

SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#ThirdPartyVehicleOptions](
	[ThirdPartyVehicleOptionID] [int] IDENTITY(1,1) NOT NULL,
	[ThirdPartyVehicleID] [int] NOT NULL,
	[ThirdPartyOptionID] [int] NOT NULL,
	[IsStandardOption] [tinyint] NOT NULL,
	[Status] [tinyint] NULL,
	[SortOrder] [tinyint] NULL,
	[DateCreated] [smalldatetime] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#PersonPosition](
	[PersonID] [int] NOT NULL,
	[PositionID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#BookoutThirdPartyVehicles](
	[ThirdPartyVehicleID] [int] NOT NULL,
	[BookoutID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#ThirdPartyVehicleOptionValues](
	[ThirdPartyVehicleOptionValueID] [int] IDENTITY(1,1) NOT NULL,
	[ThirdPartyVehicleOptionID] [int] NOT NULL,
	[ThirdPartyOptionValueTypeID] [tinyint] NULL,
	[Value] [int] NULL,
	[DateCreated] [smalldatetime] NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#PrintAdvertiserTextOptionDefault](
	[PrintAdvertiserTextOptionDefaultID] [int] IDENTITY(1,1) NOT NULL,
	[PrintAdvertiserID] [int] NOT NULL,
	[PrintAdvertiserTextOptionID] [tinyint] NOT NULL,
	[DisplayOrder] [tinyint] NOT NULL,
	[Status] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#JDPower_DealerMarketArea](
	[DealerMarketAreaID] [int] IDENTITY(1,1) NOT NULL,
	[PowerRegionID] [int] NOT NULL,
	[Name] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#BuyingAlertsRunList](
	[BuyingAlertsRunListId] [int] IDENTITY(1,1) NOT NULL,
	[SubscriptionId] [int] NOT NULL,
	[BuyingAlertsStatusId] [tinyint] NOT NULL,
	[SendDate] [smalldatetime] NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#InventoryBookouts](
	[BookoutID] [int] NOT NULL,
	[InventoryID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#InternetAdvertiserBuildLog](
	[InternetAdvertiserBuildLogID] [int] IDENTITY(1,1) NOT NULL,
	[InternetAdvertiserID] [int] NOT NULL,
	[Created] [datetime] NOT NULL,
	[TestForBusinessUnitID] [int] NULL,
	[ExceptionReportsSent] [datetime] NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#InternetAdvertiserBuildList](
	[InternetAdvertiserID] [int] NOT NULL,
	[InventoryID] [int] NOT NULL,
	[ExportStatusCD] [tinyint] NOT NULL,
	[Highlights] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[UseAdPrice] [bit] NOT NULL,
	[IncludePhotos] [bit] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#DealerRisk](
	[BusinessUnitId] [int] NOT NULL,
	[RiskLevelNumberOfWeeks] [int] NOT NULL,
	[RiskLevelDealsThreshold] [int] NOT NULL,
	[RiskLevelNumberOfContributors] [int] NOT NULL,
	[RedLightNoSaleThreshold] [int] NOT NULL,
	[RedLightGrossProfitThreshold] [int] NOT NULL,
	[GreenLightNoSaleThreshold] [int] NOT NULL,
	[GreenLightGrossProfitThreshold] [decimal](8, 2) NOT NULL,
	[GreenLightMarginThreshold] [decimal](8, 2) NOT NULL,
	[GreenLightDaysPercentage] [int] NOT NULL,
	[HighMileagePerYearThreshold] [int] NOT NULL,
	[HighMileageThreshold] [int] NOT NULL,
	[RiskLevelYearInitialTimePeriod] [int] NOT NULL,
	[RiskLevelYearSecondaryTimePeriod] [int] NOT NULL,
	[RiskLevelYearRollOverMonth] [int] NOT NULL,
	[RedLightTarget] [int] NOT NULL,
	[YellowLightTarget] [int] NOT NULL,
	[GreenLightTarget] [int] NOT NULL,
	[ExcessiveMileageThreshold] [int] NOT NULL,
	[HighAgeThreshold] [tinyint] NOT NULL,
	[ExcessiveAgeThreshold] [tinyint] NOT NULL,
	[ExcessiveMileagePerYearThreshold] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#DealerPreferenceDataload](
	[DealerPreferenceDataloadID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[TradeorPurchaseFromSales] [tinyint] NOT NULL,
	[TradeorPurchaseFromSalesDaysThreshold] [smallint] NOT NULL,
	[GenerateInventoryDeltas] [tinyint] NOT NULL,
	[CalculateUnitCost] [bit] NOT NULL,
	[VINDecodeCountryCodePrimary] [tinyint] NOT NULL,
	[VINDecodeCountryCodeSecondary] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#InternetAdvertiserBuildLogDetail](
	[InternetAdvertiserBuildLogID] [int] NOT NULL,
	[InventoryID] [int] NOT NULL,
	[ListedPrice] [int] NOT NULL,
	[ListedPriceSource] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Highlights] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[SendZeroesAsNull] [bit] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#PurgeLog](
	[Subject] [varchar](25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Executed] [datetime] NOT NULL,
	[ItemsDeleted] [int] NOT NULL,
	[First] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[Last] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#SubscriptionFrequencyDetail](
	[SubscriptionFrequencyDetailID] [tinyint] NOT NULL,
	[SubscriptionFrequencyID] [tinyint] NOT NULL,
	[Rank] [tinyint] NOT NULL,
	[Description] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#SubscriptionTypeSubscriptionDeliveryType](
	[SubscriptionTypeSubscriptionDeliveryTypeID] [tinyint] IDENTITY(1,1) NOT NULL,
	[SubscriptionTypeID] [tinyint] NOT NULL,
	[SubscriptionDeliveryTypeID] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#SubscriptionTypeDealerUpgrade](
	[SubscriptionTypeID] [tinyint] NOT NULL,
	[DealerUpgradeCD] [tinyint] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#CIAInventoryOverstocking](
	[CIAInventoryOverstockingID] [int] IDENTITY(1,1) NOT NULL,
	[CIASummaryID] [int] NOT NULL,
	[InventoryID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#CIAGroupingItemDetails](
	[CIAGroupingItemDetailID] [int] IDENTITY(1,1) NOT NULL,
	[CIAGroupingItemID] [int] NOT NULL,
	[CIAGroupingItemDetailLevelID] [tinyint] NOT NULL,
	[CIAGroupingItemDetailTypeID] [tinyint] NOT NULL,
	[Value] [varchar](45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LowRange] [int] NULL,
	[HighRange] [int] NULL,
	[Units] [int] NULL,
	[DateCreated] [smalldatetime] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#SearchCandidate](
	[SearchCandidateID] [int] IDENTITY(1,1) NOT NULL,
	[SearchCandidateListID] [int] NOT NULL,
	[Suppress] [bit] NOT NULL,
	[ModelID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#Inventory_Advertising](
	[InventoryID] [int] NOT NULL,
	[Description] [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AdPrice] [decimal](9, 2) NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#tbl_VehicleSale](
	[InventoryID] [int] NOT NULL,
	[SalesReferenceNumber] [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[VehicleMileage] [int] NULL,
	[DealDate] [smalldatetime] NOT NULL,
	[BackEndGross] [decimal](8, 2) NOT NULL,
	[NetFinancialInsuranceIncome] [decimal](8, 2) NULL,
	[OriginalFrontEndGross] [decimal](9, 2) NULL,
	[FrontEndGross] [decimal](9, 2) NOT NULL,
	[SalePrice] [decimal](9, 2) NOT NULL,
	[SaleDescription] [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[FinanceInsuranceDealNumber] [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastPolledDate] [smalldatetime] NOT NULL,
	[AnalyticalEngineStatus] [varchar](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[AnalyticalEngineStatus_CD] [int] NULL,
	[LeaseFlag] [tinyint] NULL,
	[Pack] [decimal](8, 2) NULL,
	[AfterMarketGross] [decimal](8, 2) NOT NULL,
	[TotalGross] [decimal](8, 2) NOT NULL,
	[VehiclePromotionAmount] [decimal](8, 2) NULL,
	[DealStatusCD] [tinyint] NOT NULL,
	[PackAdjust] [decimal](10, 2) NULL,
	[Audit_ID] [int] NOT NULL,
	[Valuation] [float] NULL,
	[CustomerZip] [varchar](9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IMT#dbo#Inventory_Tracking](
	[InventoryID] [int] NOT NULL,
	[Source] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[AuditID] [int] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#DemoDealerBaseline_Inventory](
	[InventoryID] [int] NOT NULL,
	[PlanReminderDate] [smalldatetime] NULL
) ON [IMT]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#Inventory_Insight](
	[InventoryID] [int] NOT NULL,
	[MileageInsightID] [tinyint] NULL,
	[AgeInsightID] [tinyint] NULL,
	[MileagePerYearInsightID] [tinyint] NULL,
	[WeakPerformingVehicleInsightID] [tinyint] NULL,
	[SalesHistoryInsightID] [tinyint] NULL
) ON [IMT]
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[IMT#dbo#NADARegionAreas](
	[NADARegionAreaID] [smallint] IDENTITY(1,1) NOT NULL,
	[NADARegionCode] [tinyint] NOT NULL,
	[State] [char](2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[County] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[IncludeOrExclude] [bit] NOT NULL
) ON [IMT]
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IMT#dbo#DealerUpgrade](
	[BusinessUnitID] [int] NOT NULL,
	[DealerUpgradeCD] [tinyint] NOT NULL,
	[StartDate] [smalldatetime] NULL,
	[EndDate] [smalldatetime] NULL,
	[Active] [tinyint] NOT NULL
) ON [IMT]
GO
