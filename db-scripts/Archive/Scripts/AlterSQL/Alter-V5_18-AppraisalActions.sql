ALTER TABLE dbo.AppraisalActions_Archive
ADD TransferPrice DECIMAL(9,2) NULL
GO

ALTER TABLE dbo.AppraisalActions_Archive
ADD TransferForRetailOnly BIT NULL 
GO
