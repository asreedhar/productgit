SET ANSI_NULLS OFF
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppraisalReviews_Archive](
	[AppraisalID] [int] NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[VehicleID] [int] NOT NULL,
	[MonthID] [int] NOT NULL,
	[DayID] [int] NOT NULL,
	[Rule1] [bit] NOT NULL,
	[Rule2] [bit] NOT NULL,
	[Rule3] [bit] NOT NULL,
	[Rule4] [bit] NOT NULL,
	[Rule5] [bit] NOT NULL,
	[Rule6] [bit] NOT NULL,
	[AppraisalValue] [int] NULL,
	[CustomerOffer] [int] NULL,
	[BookValue] [int] NULL,
	[BookThirdPartyID] [int] NULL,
	[BookThirdPartyCategoryID] [int] NULL,
	[NAAAValue] [int] NULL,
	[NADAValue] [int] NULL,
	[StockedUnitsModelYear] [int] NOT NULL,
	[StockedUnitsModel] [int] NOT NULL,
	[OptimalUnitsModelYear] [int] NULL,
	[OptimalUnitsModel] [int] NULL,
	[InventoryVehicleLightID] [int] NOT NULL,
	[Mileage] [int] NOT NULL,
	[Display] [bit] NOT NULL,
	[FollowUp] [bit] NOT NULL,
	[AppraisalDate] [datetime] NOT NULL,
 CONSTRAINT [PK_AppraisalReview_Archive] PRIMARY KEY CLUSTERED 
(
	[DayID] ASC,
	[BusinessUnitID] ASC,
	[AppraisalID] ASC
)
) ON [PRIMARY]
