CREATE TABLE [Merchandising].[Advertisement]
(
[AdvertisementID] [int] NOT NULL,
[RowTypeID] [tinyint] NOT NULL,
[Body] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Footer] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) 

CREATE TABLE [Merchandising].[Advertisement_Properties]
(
[AdvertisementID] [int] NOT NULL,
[RowTypeID] [tinyint] NOT NULL,
[Author] [int] NOT NULL,
[Created] [datetime] NOT NULL,
[Accessed] [datetime] NOT NULL,
[Modified] [datetime] NOT NULL,
[EditDuration] [int] NOT NULL,
[RevisionNumber] [int] NOT NULL,
[HasEditedBody] [bit] NOT NULL,
[HasEditedFooter] [bit] NOT NULL
) 

CREATE TABLE [Merchandising].[Advertisement_ExtendedProperties]
(
[AdvertisementID] [int] NOT NULL,
[RowTypeID] [tinyint] NOT NULL,
[AvailableCarfax] [bit] NOT NULL,
[AvailableCertified] [bit] NOT NULL,
[AvailableEdmundsTrueMarketValue] [bit] NOT NULL,
[AvailableKelleyBlueBook] [bit] NOT NULL,
[AvailableNada] [bit] NOT NULL,
[AvailableTagline] [bit] NOT NULL,
[AvailableHighValueOptions] [bit] NOT NULL,
[AvailableStandardOptions] [bit] NOT NULL,
[IncludedCarfax] [bit] NOT NULL,
[IncludedCertified] [bit] NOT NULL,
[IncludedEdmundsTrueMarketValue] [bit] NOT NULL,
[IncludedKelleyBlueBook] [bit] NOT NULL,
[IncludedNada] [bit] NOT NULL,
[IncludedTagline] [bit] NOT NULL,
[IncludedHighValueOptions] [bit] NOT NULL,
[IncludedStandardOptions] [bit] NOT NULL
) 

GO

CREATE TABLE [Merchandising].[Advertisement_VehicleInformation]
(
[AdvertisementID] [int] NOT NULL,
[RowTypeID] [tinyint] NOT NULL,
[ListPrice] [int] NOT NULL,
[Certified] [bit] NOT NULL,
[HasCarfaxReport] [bit] NOT NULL,
[HasCarfaxOneOwner] [bit] NOT NULL,
[KelleyBookoutID] [int] NULL,
[NadaBookoutID] [int] NULL,
[EdmundsStyleID] [int] NULL,
[EdmundsColorID] [int] NULL,
[EdmundsConditionId] [int] NULL,
[EdmundsOptionXML] [xml] NULL,
[EdmundsValuation] [int] NULL
)
GO

CREATE TABLE [Merchandising].[VehicleAdvertisement]
(
[OwnerEntityTypeID] [int] NOT NULL,
[OwnerEntityID] [int] NOT NULL,
[VehicleEntityTypeID] [int] NOT NULL,
[VehicleEntityID] [int] NOT NULL,
[AdvertisementID] [int] NOT NULL
)
GO

CREATE TABLE [Merchandising].[VehicleAdvertisement_Audit]
(
[OwnerEntityTypeID] [int] NOT NULL,
[OwnerEntityID] [int] NOT NULL,
[VehicleEntityTypeID] [int] NOT NULL,
[VehicleEntityID] [int] NOT NULL,
[AdvertisementID] [int] NOT NULL,
[ValidFrom] [datetime] NOT NULL,
[ValidUpTo] [datetime] NOT NULL,
[WasInsert] [bit] NOT NULL,
[WasUpdate] [bit] NOT NULL,
[WasDelete] [bit] NOT NULL
) 
GO