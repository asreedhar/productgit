SET ANSI_NULLS ON
GO

CREATE TABLE [dbo].[AppraisalBusinessUnitGroupDemand_Archive](
	[AppraisalBusinessUnitGroupDemandID] [int] NOT NULL,
	[AppraisalID] [int] NOT NULL,
	[BusinessUnitID] [int] NOT NULL
)
GO

CREATE TABLE [dbo].[Appraisals_Archive](
	[AppraisalID] [int] NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[VehicleID] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[MemberID] [int] NULL,
	[DealCompleted] [tinyint] NULL,
	[Sold] [tinyint] NULL,
	[Color] [varchar](50) NULL,
	[Mileage] [int] NOT NULL,
	[DealTrackStockNumber] [varchar](15) NULL,
	[DealTrackNewOrUsed] [tinyint] NULL,
	[DateLocked] [smalldatetime] NULL,
	[DateModified] [smalldatetime] NOT NULL,
	[DealTrackSalespersonID] [int] NULL,
	[AppraisalSourceID] [tinyint] NOT NULL,
	[EdmundsTMV] [decimal](8, 2) NULL,
	[AppraisalStatusID] [tinyint] NOT NULL
)
GO

CREATE TABLE [dbo].[AppraisalPhotos_Archive](
	[AppraisalID] [int] NOT NULL,
	[PhotoID] [int] NOT NULL
)
GO

CREATE TABLE [dbo].[AppraisalValues_Archive](
	[AppraisalValueID] [int] NOT NULL,
	[AppraisalID] [int] NULL,
	[SequenceNumber] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[Value] [int] NOT NULL,
	[AppraiserName] [varchar](35) NULL
)
GO

CREATE TABLE [dbo].[AppraisalActions_Archive](
	[AppraisalActionID] [int] NOT NULL,
	[AppraisalID] [int] NOT NULL,
	[AppraisalActionTypeID] [tinyint] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[WholesalePrice] [decimal](12, 2) NULL,
	[EstimatedReconditioningCost] [decimal](12, 2) NULL,
	[ConditionDescription] [varchar](2000) NULL
)
GO

CREATE TABLE [dbo].[AppraisalFormOptions_Archive](
	[AppraisalFormID] [int] NOT NULL,
	[AppraisalID] [int] NULL,
	[AppraisalFormOffer] [int] NULL,
	[AppraisalFormHistory] [int] NULL,
	[showEquipmentOptions] [tinyint] NULL,
	[showPhotos] [tinyint] NULL,
	[includeMileageAdjustment] [tinyint] NOT NULL,
	[showKbbConsumerValue] [tinyint] NOT NULL,
	[CategoriesToDisplayBitMask] [int] NULL
)
GO

CREATE TABLE [dbo].[AppraisalCustomer_Archive](
	[AppraisalID] [int] NOT NULL,
	[Email] [varchar](50) NULL,
	[FirstName] [varchar](40) NULL,
	[LastName] [varchar](40) NULL,
	[Gender] [varchar](1) NULL,
	[PhoneNumber] [varchar](40) NULL
)

GO

CREATE TABLE [dbo].[AppraisalBookouts_Archive](
	[BookoutID] [int] NOT NULL,
	[AppraisalID] [int] NOT NULL
)
GO

CREATE TABLE [dbo].[Staging_VehicleHistoryReport_Archive](
	[Staging_VehicleHistoryReportId] [int] NOT NULL,
	[AppraisalID] [int] NOT NULL,
	[CredentialTypeID] [tinyint] NOT NULL,
	[UserName] [varchar](50) NOT NULL
)
GO

CREATE TABLE [dbo].[Staging_Bookouts_Archive](
	[Staging_BookoutID] [int] NOT NULL,
	[AppraisalID] [int] NOT NULL,
	[ThirdPartyID] [tinyint] NOT NULL,
	[ThirdPartyVehicleCode] [varchar](20) NOT NULL
)
GO

CREATE TABLE [dbo].[AppraisalWindowStickers_Archive](
	[AppraisalWindowStickerID] [int] NOT NULL,
	[AppraisalID] [int] NOT NULL,
	[StockNumber] [varchar](25) NULL,
	[SellingPrice] [int] NULL
)
GO

CREATE TABLE [dbo].[Staging_BookoutOptions_Archive](
	[Staging_BookoutOptionID] [int] NOT NULL,
	[Staging_BookoutID] [int] NOT NULL,
	[ThirdPartyCategoryID] [int] NULL,
	[ThirdPartyOptionTypeID] [tinyint] NOT NULL,
	[OptionKey] [varchar](255) NOT NULL,
	[OptionName] [varchar](255) NULL,
	[isSelected] [tinyint] NOT NULL,
	[Value] [int] NOT NULL
)	
GO

CREATE TABLE [dbo].[Staging_BookoutValues_Archive](
	[Staging_BookoutValueID] [int] NOT NULL,
	[Staging_BookoutID] [int] NOT NULL,
	[ThirdPartyCategoryID] [int] NOT NULL,
	[BaseValue] [int] NOT NULL,
	[MileageAdjustment] [int] NULL,
	[FinalValue] [int] NULL
)
GO