CREATE TABLE [Marketing].[PrintAudit]
(
[PrintXML] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OwnerID] [int] NULL,
[VehicleEntityID] [int] NULL,
[VehicleEntityTypeID] [int] NULL,
[InsertUserID] [int] NULL,
[InsertDate] [datetime] NULL
)
GO


/*
DELETE	TOP (100) PA
OUTPUT	DELETED.* INTO Archive.Marketing.PrintAudit
FROM	IMT.Marketing.PrintAudit PA
	INNER JOIN Market.Pricing.Owner O WITH (NOLOCK) ON PA.OwnerID = O.OwnerID
	INNER JOIN IMT.dbo.Inventory I WITH (NOLOCK) ON O.OwnerEntityID = I.BusinessUnitID AND PA.VehicleEntityID = I.InventoryID
WHERE	PA.VehicleEntityTypeID = 1
	AND I.InventoryActive = 0
	AND PA.InsertDate < DATEADD(DAY, -60, GETDATE())


*/