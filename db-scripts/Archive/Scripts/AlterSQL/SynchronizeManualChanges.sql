/*

Scripting out modifications made who knows back when

*/

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Dataload_Raw_History_Archive]') AND type in (N'U'))
	GOTO EndOfAlters

ALTER TABLE Dataload_Raw_History_Archive
 alter column DEAL_NUM_FI varchar(16)

ALTER TABLE Dataload_Raw_History_Archive
 alter column DRIVE_TRAIN_DESC varchar(25)

ALTER TABLE Dataload_Raw_History_Archive
 alter column FILLER13 varchar(17)

ALTER TABLE Dataload_Raw_History_Archive
 alter column FILLER20 varchar(15)

ALTER TABLE Dataload_Raw_History_Archive
 add
   ACQUISITION_SOURCE varchar(50)  null
  ,WHSL_BUYER         varchar(50)  null
  ,ACCOUNTING_DT      varchar(25)  null
  ,REVERSAL_DT        varchar(25)  null

EndOfAlters:

go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Dataload_Raw_History_Archive]') AND type in (N'U'))
	GOTO EndOfIndexes

CREATE CLUSTERED INDEX IX_Dataload_Raw_History_Archive__audit_id
 on Dataload_Raw_History_Archive(AUDIT_ID)

CREATE INDEX IX_Dataload_Raw_History_Archive__load
 on Dataload_Raw_History_Archive(LOAD_ID)

EndOfIndexes:
