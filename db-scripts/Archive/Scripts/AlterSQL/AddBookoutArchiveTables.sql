/*

Create bookout archive tables:
 - AuditBookOuts_Archive
 - BookoutValues_Archive
 - BookoutThirdPartyCategories_Archive
 - InventoryBookouts_Archive
 - Bookouts_Archive


--  Undo script
DROP TABLE AuditBookOuts_Archive
DROP TABLE BookoutValues_Archive
DROP TABLE BookoutThirdPartyCategories_Archive
DROP TABLE InventoryBookouts_Archive
DROP TABLE Bookouts_Archive


*/

CREATE TABLE dbo.AuditBookOuts_Archive (
    AuditId int not null ,
    BusinessUnitId int not null ,
    MemberId int not null ,
    ProductId int not null ,
    ReferenceId varchar (25) null ,
    BookId tinyint not null ,
    BookOutTimeStamp smalldatetime not null ,
    BookOutId int not null 
)

CREATE TABLE dbo.BookoutValues_Archive (
    BookoutValueID int not null ,
    BookoutThirdPartyCategoryID int not null ,
    BookoutValueTypeID tinyint not null ,
    Value int null ,
    DateCreated datetime not null ,
    DateModified datetime null 
)

CREATE TABLE dbo.BookoutThirdPartyCategories_Archive (
    BookoutThirdPartyCategoryID int not null ,
    BookoutID int not null ,
    ThirdPartyCategoryID int not null ,
    IsValid tinyint not null ,
    DateCreated datetime not null ,
    DateModified datetime null ,
    LegacyID int null 
)

CREATE TABLE dbo.InventoryBookouts_Archive (
    BookoutID int not null ,
    InventoryID int not null 
)

CREATE TABLE dbo.Bookouts_Archive (
    BookoutID int not null ,
    BookoutSourceID tinyint not null ,
    ThirdPartyID tinyint not null ,
    DateCreated datetime null ,
    Region varchar (50) null ,
    DatePublished varchar (50) null ,
    BookPrice int null ,
    Weight int null ,
    MileageCostAdjustment int null ,
    MSRP int null ,
    IsAccurate tinyint not null ,
    LegacyID int not null 
)

CREATE nonclustered INDEX IX_Bookouts_Archive__BookoutID
 on dbo.Bookouts_Archive (BookoutID)
GO
