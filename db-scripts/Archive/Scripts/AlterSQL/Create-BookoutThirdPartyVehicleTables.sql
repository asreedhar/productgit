ALTER TABLE BookoutThirdPartyVehicles_Archive DROP COLUMN Legacy_object_id
ALTER TABLE BookoutThirdPartyVehicles_Archive DROP COLUMN Legacy_primary_key

GO

CREATE TABLE [dbo].[ThirdPartyVehicles_Archive] (
	[ThirdPartyVehicleID] [int] NOT NULL ,
	[ThirdPartyID] [tinyint] NOT NULL ,
	[ThirdPartyVehicleCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Description] [varchar] (65) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MakeCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ModelCode] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Status] [tinyint] NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[DateModified] [datetime] NULL ,
	[EngineType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Make] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Model] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Body] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)	
GO


CREATE TABLE [dbo].[ThirdPartyVehicleOptions_Archive] (
	[ThirdPartyVehicleOptionID] [int] NOT NULL ,
	[ThirdPartyVehicleID] [int] NOT NULL ,
	[ThirdPartyOptionID] [int] NOT NULL ,
	[IsStandardOption] [tinyint] NOT NULL,
	[Status] [tinyint] NULL,
	[SortOrder] [tinyint] NULL,
	[DateCreated] [datetime] NULL
)
GO

CREATE TABLE [dbo].[ThirdPartyVehicleOptionValues_Archive] (
	[ThirdPartyVehicleOptionValueID] [int] NOT NULL ,
	[ThirdPartyVehicleOptionID] [int] NOT NULL ,
	[ThirdPartyOptionValueTypeID] [tinyint] NULL,
	[Value] [int] NULL ,
	[DateCreated] [datetime] NULL
)
GO

CREATE TABLE [dbo].[VehicleBookoutState_Archive](
	[VehicleBookoutStateID] [int] NOT NULL,
	[VIN] [varchar](17) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[DateCreated] [datetime] NOT NULL 
) 
GO
CREATE TABLE [dbo].[VehicleBookoutAdditionalState_Archive](
	[VehicleBookoutStateID] [int] NOT NULL,
	[ThirdPartySubCategoryID] [int] NOT NULL
) 
GO

CREATE TABLE [dbo].[VehicleBookoutStateThirdPartyVehicles_Archive](
	[ThirdPartyVehicleID] [int] NOT NULL,
	[VehicleBookoutStateID] [int] NOT NULL
) 
GO

ALTER TABLE Bookouts_Archive ALTER COLUMN LegacyID INT NULL

UPDATE	Bookouts_Archive
SET	LegacyID = NULL

GO
EXEC SP_RENAME 'Bookouts_Archive.LegacyID','BookoutStatusID','COLUMN'
GO
ALTER TABLE Bookouts_Archive ALTER COLUMN BookoutStatusID TINYINT NULL
GO

