USE [Archive]
GO
/****** Object:  Schema [AutoCount]    Script Date: 01/06/2011 10:42:34 ******/
CREATE SCHEMA [AutoCount] AUTHORIZATION [dbo]
GO
/****** Object:  Table [AutoCount].[Datafeeds]    Script Date: 01/06/2011 10:42:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [AutoCount].[Datafeeds](
	[FileID] [int] IDENTITY(15,1) NOT NULL,
	[FileName] [varchar](100) NOT NULL,
	[Start_Date_ID] [int] NOT NULL CONSTRAINT [DF_Datafeeds__Start_Date_ID]  DEFAULT ((0)),
	[End_Date_ID] [int] NOT NULL CONSTRAINT [DF_Datafeeds__End_Date_ID]  DEFAULT ((0)),
	[Geo_ID] [int] NOT NULL CONSTRAINT [DF_Datafeeds__Geo_ID]  DEFAULT ((0)),
	[File_Type_CD] [tinyint] NOT NULL CONSTRAINT [DF_Datafeeds__File_Type_CD]  DEFAULT ((1)),
	[Status_CD] [tinyint] NOT NULL CONSTRAINT [DF_Datafeeds__Status_CD]  DEFAULT ((0)),
	[Source_CD] [tinyint] NOT NULL CONSTRAINT [DF_Datafeeds__Source_CD]  DEFAULT ((1)),
	[TimeStamp] [datetime] NOT NULL CONSTRAINT [DF_Datafeeds__TimeStamp]  DEFAULT (getdate()),
 CONSTRAINT [PK_Datafeeds] PRIMARY KEY CLUSTERED 
(
	[FileID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [AutoCount].[Market_AutoCount]    Script Date: 01/06/2011 10:42:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [AutoCount].[Market_AutoCount](
	[TITLENUMBER] [varchar](75) NULL,
	[TITLECOUNTY] [varchar](75) NULL,
	[TITLESTATE] [varchar](75) NULL,
	[REPORTINGPERIOD] [varchar](75) NULL,
	[VIN] [varchar](75) NULL,
	[VEHICLEMAKE] [varchar](75) NULL,
	[VEHICLEMODEL] [varchar](75) NULL,
	[VEHICLESEGMENT] [varchar](75) NULL,
	[VEHICLEBODY] [varchar](75) NULL,
	[VEHICLEYEAR] [varchar](75) NULL,
	[ODOMETERREADING] [float] NULL,
	[NEWUSEDINDICATOR] [varchar](75) NULL,
	[FLEETINDICATOR] [varchar](75) NULL,
	[LEASEINDICATOR] [varchar](75) NULL,
	[DEALERNUMBER] [varchar](75) NULL,
	[DEALERNAME] [varchar](75) NULL,
	[DEALERADDRESS] [varchar](75) NULL,
	[DEALERCITY] [varchar](75) NULL,
	[DEALERSTATE] [varchar](75) NULL,
	[DEALERZIPCODE] [varchar](75) NULL,
	[DEALERTYPE] [varchar](75) NULL,
	[DEALERCOUNTY] [varchar](75) NULL,
	[OWNERNAME] [varchar](75) NULL,
	[OWNERADDRESS] [varchar](75) NULL,
	[OWNERCITY] [varchar](75) NULL,
	[OWNERSTATE] [varchar](75) NULL,
	[OWNERZIPCODE] [varchar](75) NULL,
	[OWNERCOUNTY] [varchar](75) NULL,
	[LIENHOLDERNAMECORRECTED] [varchar](75) NULL,
	[LIENHOLDERADDRESS] [varchar](75) NULL,
	[LIENHOLDERCITY] [varchar](75) NULL,
	[LIENHOLDERSTATE] [varchar](75) NULL,
	[LIENHOLDERZIPCODE] [varchar](75) NULL,
	[FILEID] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [AutoCount].[Market_AutoCount_Staging_1]    Script Date: 01/06/2011 10:43:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [AutoCount].[Market_AutoCount_Staging_1](
	[VIN] [varchar](17) NULL,
	[Squish_vin] [varchar](17) NULL,
	[SegmentID] [tinyint] NULL,
	[SubSegmentID] [tinyint] NULL,
	[YR] [int] NULL,
	[Make] [varchar](50) NULL,
	[Model] [varchar](50) NULL,
	[MMGID] [int] NULL,
	[Zip] [varchar](5) NULL,
	[Odometer] [int] NULL,
	[OrigYear] [varchar](4) NULL,
	[OrigMake] [varchar](20) NULL,
	[OrigModel] [varchar](30) NULL,
	[DealerNumber] [int] NOT NULL,
	[DealerCounty] [varchar](50) NULL,
	[TitleState] [varchar](50) NULL,
	[ReportingPeriod] [varchar](6) NULL,
	[NewUsedIndicator] [varchar](1) NULL,
	[FileID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [AutoCount].[Market_AutoCount_Detail]    Script Date: 01/06/2011 10:42:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [AutoCount].[Market_AutoCount_Detail](
	[VIN] [varchar](17) NOT NULL,
	[Squish_vin] [varchar](17) NOT NULL,
	[SegmentID] [tinyint] NULL,
	[SubSegmentID] [tinyint] NULL,
	[YR] [int] NOT NULL,
	[Make] [varchar](50) NOT NULL,
	[Model] [varchar](50) NOT NULL,
	[MMGID] [int] NOT NULL,
	[Zip] [varchar](5) NOT NULL,
	[Odometer] [int] NULL,
	[OrigYear] [varchar](4) NULL,
	[OrigMake] [varchar](20) NULL,
	[OrigModel] [varchar](30) NULL,
	[DealerNumber] [int] NOT NULL,
	[DealerCounty] [varchar](50) NULL,
	[TitleState] [varchar](50) NULL,
	[ReportingPeriod] [varchar](6) NOT NULL,
	[NewUsedIndicator] [varchar](1) NULL,
	[FileID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [AutoCount].[Market_AutoCount_Exceptions]    Script Date: 01/06/2011 10:42:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [AutoCount].[Market_AutoCount_Exceptions](
	[VIN] [varchar](17) NULL,
	[Squish_vin] [varchar](17) NULL,
	[YR] [int] NULL,
	[Make] [varchar](50) NULL,
	[Model] [varchar](50) NULL,
	[MMGID] [int] NULL,
	[Zip] [varchar](5) NULL,
	[Odometer] [int] NULL,
	[OrigYear] [varchar](4) NULL,
	[OrigMake] [varchar](20) NULL,
	[OrigModel] [varchar](30) NULL,
	[DealerNumber] [int] NULL,
	[DealerCounty] [varchar](50) NULL,
	[TitleState] [varchar](50) NULL,
	[ReportingPeriod] [varchar](6) NULL,
	[NewUsedIndicator] [varchar](1) NULL,
	[FileID] [int] NOT NULL,
	[ExceptionDate] [datetime] NOT NULL,
	[ExceptionComment] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [AutoCount].[Market_CrossSell_Detail]    Script Date: 01/06/2011 10:43:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [AutoCount].[Market_CrossSell_Detail](
	[VIN] [varchar](17) NOT NULL,
	[Squish_vin] [varchar](17) NOT NULL,
	[SegmentID] [tinyint] NULL,
	[SubSegmentID] [tinyint] NULL,
	[YR] [int] NOT NULL,
	[Make] [varchar](20) NOT NULL,
	[Model] [varchar](20) NOT NULL,
	[MMGID] [int] NOT NULL,
	[Zip] [varchar](5) NOT NULL,
	[Odometer] [varchar](10) NULL,
	[OrigYear] [varchar](4) NULL,
	[OrigMake] [varchar](20) NULL,
	[OrigModel] [varchar](30) NULL,
	[DealerNumber] [int] NOT NULL,
	[MON] [varchar](6) NOT NULL,
	[S_T] [varchar](1) NULL,
	[Sprice] [varchar](6) NULL,
	[FileID] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [AutoCount].[AutoCountStaging_VINDecode]    Script Date: 01/06/2011 10:42:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------
--
--
--
---History----------------------------------------------------------------------------------
--	
--	WGH	 06/07/2007  Initial design/development (code extracted from DTS package)
--  PHK  12/11/2007  Removed GetSquishVIN call, as that's already been done by the package
--
------------------------------------------------------------------------------------------
CREATE PROC [AutoCount].[AutoCountStaging_VINDecode]
AS

UPDATE	AutoCount.Market_AutoCount_Staging_1
SET	YR		    = VC.ModelYear,
	Make		= VC.make,
	Model		= VC.Line,
	MMGID		= VC.MakeModelGroupingID,
	SegmentID	= VC.SegmentID,
	SubSegmentID	= 99

FROM	AutoCount.Market_AutoCount_Staging_1 CS
	JOIN dbo.VehicleCatalog VC
     ON VC.CountryCode = 1
      AND CS.Squish_VIN = VC.SquishVIN
      AND CS.VIN LIKE VC.VINPattern
	LEFT JOIN dbo.VehicleCatalogVINPatternPriority VPP
     ON VPP.CountryCode = 1
      AND VC.VINPattern = VPP.VINPattern
      AND CS.VIN LIKE VPP.PriorityVINPattern
      
WHERE 	VPP.PriorityVINPattern IS NULL
GO
/****** Object:  ForeignKey [FK_Market_AutoCount_Detail__Datafeeds]    Script Date: 01/06/2011 10:42:53 ******/
ALTER TABLE [AutoCount].[Market_AutoCount_Detail]  WITH NOCHECK ADD  CONSTRAINT [FK_Market_AutoCount_Detail__Datafeeds] FOREIGN KEY([FileID])
REFERENCES [AutoCount].[Datafeeds] ([FileID])
GO
ALTER TABLE [AutoCount].[Market_AutoCount_Detail] CHECK CONSTRAINT [FK_Market_AutoCount_Detail__Datafeeds]
GO
/****** Object:  ForeignKey [FK_Market_AutoCount_Exceptions__Datafeeds]    Script Date: 01/06/2011 10:42:58 ******/
ALTER TABLE [AutoCount].[Market_AutoCount_Exceptions]  WITH NOCHECK ADD  CONSTRAINT [FK_Market_AutoCount_Exceptions__Datafeeds] FOREIGN KEY([FileID])
REFERENCES [AutoCount].[Datafeeds] ([FileID])
GO
ALTER TABLE [AutoCount].[Market_AutoCount_Exceptions] CHECK CONSTRAINT [FK_Market_AutoCount_Exceptions__Datafeeds]
GO
/****** Object:  ForeignKey [FK_Market_AutoCount_Staging1__Datafeeds]    Script Date: 01/06/2011 10:43:04 ******/
ALTER TABLE [AutoCount].[Market_AutoCount_Staging_1]  WITH NOCHECK ADD  CONSTRAINT [FK_Market_AutoCount_Staging1__Datafeeds] FOREIGN KEY([FileID])
REFERENCES [AutoCount].[Datafeeds] ([FileID])
GO
ALTER TABLE [AutoCount].[Market_AutoCount_Staging_1] CHECK CONSTRAINT [FK_Market_AutoCount_Staging1__Datafeeds]
GO
/****** Object:  ForeignKey [FK_Market_CrossSell_Detail_Datafeeds]    Script Date: 01/06/2011 10:43:09 ******/
ALTER TABLE [AutoCount].[Market_CrossSell_Detail]  WITH CHECK ADD  CONSTRAINT [FK_Market_CrossSell_Detail_Datafeeds] FOREIGN KEY([FileID])
REFERENCES [AutoCount].[Datafeeds] ([FileID])
GO
ALTER TABLE [AutoCount].[Market_CrossSell_Detail] CHECK CONSTRAINT [FK_Market_CrossSell_Detail_Datafeeds]
GO

USE Archive

----------------------------------------------------------------------------------------------------------------------------------------------------
--	Market_AutoCount
----------------------------------------------------------------------------------------------------------------------------------------------------

INSERT
INTO	Archive.AutoCount.Market_AutoCount (TITLENUMBER, TITLECOUNTY, TITLESTATE, REPORTINGPERIOD, VIN, VEHICLEMAKE, VEHICLEMODEL, VEHICLESEGMENT, VEHICLEBODY, VEHICLEYEAR, ODOMETERREADING, NEWUSEDINDICATOR, FLEETINDICATOR, LEASEINDICATOR, DEALERNUMBER, DEALERNAME, DEALERADDRESS, DEALERCITY, DEALERSTATE, DEALERZIPCODE, DEALERTYPE, DEALERCOUNTY, OWNERNAME, OWNERADDRESS, OWNERCITY, OWNERSTATE, OWNERZIPCODE, OWNERCOUNTY, LIENHOLDERNAMECORRECTED, LIENHOLDERADDRESS, LIENHOLDERCITY, LIENHOLDERSTATE, LIENHOLDERZIPCODE, FILEID)

SELECT	TITLENUMBER, TITLECOUNTY, TITLESTATE, REPORTINGPERIOD, VIN, VEHICLEMAKE, VEHICLEMODEL, VEHICLESEGMENT, VEHICLEBODY, VEHICLEYEAR, ODOMETERREADING, NEWUSEDINDICATOR, FLEETINDICATOR, LEASEINDICATOR, DEALERNUMBER, DEALERNAME, DEALERADDRESS, DEALERCITY, DEALERSTATE, DEALERZIPCODE, DEALERTYPE, DEALERCOUNTY, OWNERNAME, OWNERADDRESS, OWNERCITY, OWNERSTATE, OWNERZIPCODE, OWNERCOUNTY, LIENHOLDERNAMECORRECTED, LIENHOLDERADDRESS, LIENHOLDERCITY, LIENHOLDERSTATE, LIENHOLDERZIPCODE, FILEID
FROM	MarketStaging.AutoCount.Market_AutoCount

----------------------------------------------------------------------------------------------------------------------------------------------------
--	Market_AutoCount_Staging_1
----------------------------------------------------------------------------------------------------------------------------------------------------
INSERT
INTO	Archive.AutoCount.Market_AutoCount_Staging_1 (VIN, Squish_vin, SegmentID, SubSegmentID, YR, Make, Model, MMGID, Zip, Odometer, OrigYear, OrigMake, OrigModel, DealerNumber, DealerCounty, TitleState, ReportingPeriod, NewUsedIndicator, FileID)

SELECT	VIN, Squish_vin, SegmentID, SubSegmentID, YR, Make, Model, MMGID, Zip, Odometer, OrigYear, OrigMake, OrigModel, DealerNumber, DealerCounty, TitleState, ReportingPeriod, NewUsedIndicator, FileID
FROM	MarketStaging.AutoCount.Market_AutoCount_Staging_1
----------------------------------------------------------------------------------------------------------------------------------------------------
--	Market_AutoCount_Detail
----------------------------------------------------------------------------------------------------------------------------------------------------
INSERT
INTO	Archive.AutoCount.Market_AutoCount_Detail (VIN, Squish_vin, SegmentID, SubSegmentID, YR, Make, Model, MMGID, Zip, Odometer, OrigYear, OrigMake, OrigModel, DealerNumber, DealerCounty, TitleState, ReportingPeriod, NewUsedIndicator, FileID)

SELECT	VIN, Squish_vin, SegmentID, SubSegmentID, YR, Make, Model, MMGID, Zip, Odometer, OrigYear, OrigMake, OrigModel, DealerNumber, DealerCounty, TitleState, ReportingPeriod, NewUsedIndicator, FileID
FROM	MarketStaging.AutoCount.Market_AutoCount_Detail
----------------------------------------------------------------------------------------------------------------------------------------------------
--	Market_AutoCount_Exceptions
----------------------------------------------------------------------------------------------------------------------------------------------------
INSERT
INTO	Archive.AutoCount.Market_AutoCount_Exceptions (VIN, Squish_vin, YR, Make, Model, MMGID, Zip, Odometer, OrigYear, OrigMake, OrigModel, DealerNumber, DealerCounty, TitleState, ReportingPeriod, NewUsedIndicator, FileID, ExceptionDate, ExceptionComment)

SELECT	VIN, Squish_vin, YR, Make, Model, MMGID, Zip, Odometer, OrigYear, OrigMake, OrigModel, DealerNumber, DealerCounty, TitleState, ReportingPeriod, NewUsedIndicator, FileID, ExceptionDate, ExceptionComment
FROM	MarketStaging.AutoCount.Market_AutoCount_Exceptions
----------------------------------------------------------------------------------------------------------------------------------------------------
--	Market_CrossSell_Detail
----------------------------------------------------------------------------------------------------------------------------------------------------
INSERT
INTO	Archive.AutoCount.Market_CrossSell_Detail (VIN, Squish_vin, SegmentID, SubSegmentID, YR, Make, Model, MMGID, Zip, Odometer, OrigYear, OrigMake, OrigModel, DealerNumber, MON, S_T, Sprice, FileID)

SELECT	VIN, Squish_vin, SegmentID, SubSegmentID, YR, Make, Model, MMGID, Zip, Odometer, OrigYear, OrigMake, OrigModel, DealerNumber, MON, S_T, Sprice, FileID
FROM	MarketStaging.AutoCount.Market_CrossSell_Detail
----------------------------------------------------------------------------------------------------------------------------------------------------
--	Datafeeds
----------------------------------------------------------------------------------------------------------------------------------------------------


SET IDENTITY_INSERT AutoCount.Datafeeds ON 
INSERT
INTO	Archive.AutoCount.Datafeeds (FileID, FileName, Start_Date_ID, End_Date_ID, Geo_ID, File_Type_CD, Status_CD, Source_CD, TimeStamp)

SELECT	FileID, FileName, Start_Date_ID, End_Date_ID, Geo_ID, File_Type_CD, Status_CD, Source_CD, TimeStamp
FROM	MarketStaging.AutoCount.Datafeeds

SET IDENTITY_INSERT AutoCount.Datafeeds OFF 

USE MarketStaging

DROP TABLE AutoCount.Market_AutoCount
DROP TABLE AutoCount.Market_AutoCount_Staging_1
DROP TABLE AutoCount.Market_AutoCount_Detail
DROP TABLE AutoCount.Market_AutoCount_Exceptions
DROP TABLE AutoCount.Market_CrossSell_Detail
DROP TABLE AutoCount.Datafeeds

DROP PROC AutoCount.AutoCountStaging_VINDecode

DROP SCHEMA AutoCount

