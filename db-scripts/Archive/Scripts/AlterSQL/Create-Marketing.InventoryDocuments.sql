CREATE SCHEMA Marketing
GO
CREATE TABLE [Marketing].[InventoryDocuments]
(
[DocumentID] [int] NOT NULL,
[OwnerID] [int] NOT NULL,
[VehicleEntityID] [int] NOT NULL,
[VehicleEntityTypeID] [int] NOT NULL,
[DocumentXml] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InsertUserID] [int] NOT NULL,
[InsertDate] [datetime] NOT NULL,
[UpdateUserID] [int] NOT NULL,
[UpdateDate] [datetime] NOT NULL
) 
GO

/*
DECLARE @BatchSize INT = 100;

DELETE	TOP (50) ID
OUTPUT	DELETED.* INTO Archive.Marketing.InventoryDocuments
FROM	IMT.dbo.Inventory I WITH (NOLOCK) 
	INNER JOIN Market.Pricing.Owner O WITH (NOLOCK) ON O.OwnerTypeID = 1 AND I.BusinessUnitID = O.OwnerEntityID
	INNER JOIN IMT.Marketing.InventoryDocuments ID ON O.OwnerID = ID.OwnerID AND I.InventoryID = ID.VehicleEntityID
WHERE	I.InventoryActive = 0
	AND ID.InsertDate < DATEADD(DAY, -60, GETDATE())

*/