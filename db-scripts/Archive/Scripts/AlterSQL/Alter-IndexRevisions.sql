/*

Do some Archive database modifications
 - Change Audit_Exceptions_Archive.SourceTableID from char(10) to tinyint
 - Change indexes on Audit_ID to clustered on three Audit_* tables

*/

--  Alter column
ALTER TABLE Audit_Exceptions_Archive
 alter column SourceTableID tinyint not null
GO


--  Revise index on Audit_Exceptions_Archive
DROP INDEX Audit_Exceptions_Archive.IX_Audit_Exceptions_Archive__Audit_ID
GO

CREATE clustered INDEX IX_Audit_Exceptions_Archive__Audit_ID
 on Audit_Exceptions_Archive (Audit_ID)
 with (sort_in_tempdb = on)
GO


--  Revise index on Audit_Inventory_Archive
DROP INDEX Audit_Inventory_Archive.IX_Audit_Inventory_Archive__Audit_ID
GO

CREATE clustered INDEX IX_Audit_Inventory_Archive__Audit_ID
 on Audit_Inventory_Archive (Audit_ID)
 with (sort_in_tempdb = on)
GO


--  Revise index on Audit_Sales_Archive
DROP INDEX Audit_Sales_Archive.IX_Audit_Sales_Archive__Audit_ID
GO

CREATE clustered INDEX IX_Audit_Sales_Archive__Audit_ID
 on Audit_Sales_Archive (Audit_ID)
 with (sort_in_tempdb = on)
GO
