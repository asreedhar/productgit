--*****************************************************
--Archive db
--*****************************************************
--add columns to archive table
ALTER TABLE dbo.AppraisalReviews_Archive
	ADD JDPowerACV DECIMAL(9,2) NULL
ALTER TABLE dbo.AppraisalReviews_Archive
	ADD JDPowerAvgSellingPrice DECIMAL(9,2) NULL
ALTER TABLE dbo.AppraisalReviews_Archive
	ADD PINGAvgMarketPrice DECIMAL(9,2) NULL
ALTER TABLE dbo.AppraisalReviews_Archive
	ADD Rule7 BIT
ALTER TABLE dbo.AppraisalReviews_Archive
	ADD Rule8 BIT
ALTER TABLE dbo.AppraisalReviews_Archive
	ADD Rule9 BIT
