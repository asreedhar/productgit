--Object:  Table [Listing].[Vehicle_History_Archive]    
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE dbo.Vehicle_History_Archive(
	[VehicleID] [int] NOT NULL,
	[ProviderID] [tinyint] NOT NULL,
	[SellerID] [int] NOT NULL,
	[SourceID] [tinyint] NOT NULL,
	[SourceRowID] [int] NOT NULL,
	[VehicleCatalogID] [int] NOT NULL,
	[ColorID] [int] NOT NULL,
	[MakeID] [int] NOT NULL,
	[ModelID] [int] NOT NULL,
	[ModelYear] [int] NOT NULL,
	[TrimID] [int] NOT NULL,
	[DriveTrainID] [tinyint] NOT NULL,
	[EngineID] [smallint] NOT NULL,
	[TransmissionID] [smallint] NOT NULL,
	[FuelTypeID] [tinyint] NOT NULL,
	[VIN] [varchar](17) NULL,
	[ListPrice] [int] NOT NULL,
	[ListingDate] [smalldatetime] NULL,
	[Mileage] [int] NOT NULL,
	[MSRP] [int] NULL,
	[StockNumber] [varchar](30) NULL,
	[Certified] [tinyint] NULL,
	[DateCreated] [smalldatetime] NOT NULL CONSTRAINT [DF_ListingVehicleHistory__DateCreated]  DEFAULT (dateadd(day,datediff(day,(0),getdate()),(0))),
	[SellerDescription] [varchar](2000) NULL,
 CONSTRAINT [PK_ListingVehicle_History] PRIMARY KEY CLUSTERED 
(
	[VehicleID] ASC
)
) ON [Primary]

GO
--Listing.Vehicle_Sales_Archive
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE dbo.Vehicle_Sales_Archive(
	[VehicleID] [int] NOT NULL,
	[DateSold] [datetime] NOT NULL CONSTRAINT [DF_Vehicle_Sales_DateSold1]  DEFAULT (getdate()),
	[DateStatusChanged] [datetime] NULL,
	[SaleType] [char](1) NOT NULL CONSTRAINT [DF_Vehicle_Sales_SaleType1]  DEFAULT ('R'),
	[SellerID] [int] NULL,
 CONSTRAINT [PK_Vehicle_Sales1] PRIMARY KEY CLUSTERED 
(
	[VehicleID] ASC,
	[DateSold] ASC
)
) ON [Primary]

GO
