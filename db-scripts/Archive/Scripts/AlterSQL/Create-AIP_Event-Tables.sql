SET ANSI_NULLS ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE dbo.AIP_Event_Archive
    (
      AIP_EventID	INT NOT NULL,
      InventoryID	INT NOT NULL,
      AIP_EventTypeID	TINYINT NOT NULL,
      BeginDate		SMALLDATETIME NOT NULL,
      EndDate		SMALLDATETIME NULL,
      Notes		VARCHAR(1000) NULL,
      Notes2		VARCHAR(250) NULL,
      ThirdPartyEntityID INT NULL,
      Value		DECIMAL(9, 2) NULL,
      Created		SMALLDATETIME NOT NULL,
      CreatedBy		VARCHAR(128) NOT NULL,
      LastModified	SMALLDATETIME NOT NULL,
      LastModifiedBy	VARCHAR(128) NOT NULL,
      VersionNumber	INT NOT NULL,
      UserBeginDate	SMALLDATETIME NULL,
      UserEndDate	SMALLDATETIME NULL,
      Notes3		VARCHAR(100) NULL,
      RepriceSourceID	INT NULL,
      DateArchived	SMALLDATETIME NOT NULL CONSTRAINT DF_AIP_Event_Archive__DateArchived DEFAULT (GETDATE())
    )

CREATE TABLE dbo.RepriceExport_Archive(
	RepriceExportID		INT NOT NULL,
	RepriceEventID		INT NOT NULL,
	RepriceExportStatusID	INT NOT NULL,
	ThirdPartyEntityID	INT NOT NULL,
	FailureReason		VARCHAR(2000) NULL,
	FailureCount		INT NOT NULL,
	DateCreated		DATETIME NULL,
	DateSent		DATETIME NULL,
	StatusModifiedDate	DATETIME NOT NULL,
 )

GO
