CREATE TABLE [dbo].[CIABodyTypeDetails_Archive] (
	[CIABodyTypeDetailID] [int] NOT NULL ,
	[DisplayBodyTypeID] [int] NOT NULL ,
	[CIASummaryID] [int] NOT NULL ,
	[NonCoreTargetUnits] [int] NOT NULL ,
	[DisplaySortOrder] [int] NULL 
)
GO

CREATE TABLE [dbo].[CIAGroupingItemDetails_Archive] (
	[CIAGroupingItemDetailID] [int] NOT NULL ,
	[CIAGroupingItemID] [int] NOT NULL ,
	[CIAGroupingItemDetailLevelID] [tinyint] NOT NULL ,
	[CIAGroupingItemDetailTypeID] [tinyint] NOT NULL ,
	[Value] [varchar] (45) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LowRange] [int] NULL ,
	[HighRange] [int] NULL ,
	[Units] [int] NULL ,
	[DateCreated] [smalldatetime] NOT NULL 
)
GO

CREATE TABLE [dbo].[CIAGroupingItems_Archive] (
	[CIAGroupingItemID] [int] NOT NULL ,
	[CIABodyTypeDetailID] [int] NOT NULL ,
	[GroupingDescriptionID] [int] NOT NULL ,
	[CIACategoryID] [tinyint] NOT NULL ,
	[MarketShare] [decimal](4, 4) NULL ,
	[Valuation] [decimal](10, 2) NULL ,
	[MarketPenetration] [decimal](10, 4) NULL ,
	[Notes] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
)
GO

CREATE TABLE [dbo].[CIASummary_Archive] (
	[CIASummaryID] [int] NOT NULL ,
	[BusinessUnitID] [int] NOT NULL ,
	[CIASummaryStatusID] [tinyint] NOT NULL ,
	[DateCreated] [smalldatetime] NOT NULL 
)
GO

CREATE TABLE [dbo].[CIAInventoryOverstocking_Archive] (
	[CIAInventoryOverstockingID] [int] NOT NULL ,
	[CIASummaryID] [int] NOT NULL ,
	[InventoryID] [int] NOT NULL 
) 
GO

