CREATE TABLE [dbo].[AuctionTransaction_F_Archive] (
	[AuctionTransactionID] [int] NOT NULL ,
	[TimeID] [int] NOT NULL ,
	[SeriesID] [smallint] NOT NULL ,
	[RegionID] [tinyint] NOT NULL ,
	[SaleTypeID] [tinyint] NOT NULL ,
	[DieselIdentifierID] [tinyint] NULL ,
	[BodyStyleID] [smallint] NULL ,
	[EngineID] [smallint] NULL ,
	[TransmissionID] [tinyint] NULL ,
	[SalePrice] [decimal](8, 2) NULL ,
	[Mileage] [int] NULL ,
	[VID] [int] NULL ,
	[VIC] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MultipleVICFlag] [bit] NULL ,
	[Make] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ModelYear] [smallint] NULL ,
	[Submake] [varchar] (24) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Bodystyle] [varchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VIN] [char] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LoadID] [int] NULL ,
	[OriginalBodyStyleID] [int] NOT NULL 
) ON [PRIMARY]
GO