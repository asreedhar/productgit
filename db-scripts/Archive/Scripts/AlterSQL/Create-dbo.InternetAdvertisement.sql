CREATE TABLE [dbo].[InternetAdvertisement]
(
[InventoryID] [int] NOT NULL,
[AdvertisementText] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateUser] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpdateDate] [datetime] NOT NULL
) 
GO


/*

DELETE	TOP (100) IA
OUTPUT	DELETED.* INTO Archive.dbo.InternetAdvertisement
FROM	IMT.dbo.InternetAdvertisement IA
	INNER JOIN IMT.dbo.Inventory I WITH (NOLOCK) ON IA.InventoryID = I.InventoryID
WHERE	I.InventoryActive = 0
	AND IA.UpdateDate < DATEADD(DAY, -60, GETDATE())
	
*/	