SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'dbo.ArchiveMarketMerchandising') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE dbo.ArchiveMarketMerchandising

GO

CREATE PROC dbo.ArchiveMarketMerchandising
@BatchSize INT = 1024
AS
SET NOCOUNT ON 

CREATE TABLE #AdvertisementIDs (AdvertisementID INT PRIMARY KEY)

INSERT
INTO	#AdvertisementIDs (AdvertisementID)
SELECT	TOP (@BatchSize) A.AdvertisementID
FROM	Market.Merchandising.Advertisement A
	LEFT JOIN (	Market.Merchandising.VehicleAdvertisement VA 
			INNER JOIN IMT.dbo.Inventory I ON VA.VehicleEntityTypeID = 1 AND VA.VehicleEntityID = I.InventoryID 
			) ON A.AdvertisementID = VA.AdvertisementID
WHERE	A.RowTypeID = 0							-- inactive
	AND (	VA.AdvertisementID IS NULL				-- orphaned
		OR (	I.InventoryActive = 0
			AND I.DeleteDt < DATEADD(DAY, -90, GETDATE())	-- inactive inventory, over 90 days old
			)	
		)	

DELETE	VAA
OUTPUT	DELETED.* INTO Archive.Merchandising.VehicleAdvertisement_Audit
FROM	Market.Merchandising.VehicleAdvertisement_Audit VAA
	INNER JOIN #AdvertisementIDs A ON VAA.AdvertisementID = A.AdvertisementID
	
DELETE	AXP
OUTPUT	DELETED.* INTO Archive.Merchandising.Advertisement_ExtendedProperties
FROM	Market.Merchandising.Advertisement_ExtendedProperties AXP
	INNER JOIN #AdvertisementIDs A ON AXP.AdvertisementID = A.AdvertisementID
	
DELETE	AP
OUTPUT	DELETED.* INTO Archive.Merchandising.Advertisement_Properties
FROM	Market.Merchandising.Advertisement_Properties AP
	INNER JOIN #AdvertisementIDs A ON AP.AdvertisementID = A.AdvertisementID	
	
DELETE	AVI
OUTPUT	DELETED.* INTO Archive.Merchandising.Advertisement_VehicleInformation
FROM	Market.Merchandising.Advertisement_VehicleInformation AVI
	INNER JOIN #AdvertisementIDs A ON AVI.AdvertisementID = A.AdvertisementID	
	
DELETE	VA
OUTPUT	DELETED.* INTO Archive.Merchandising.VehicleAdvertisement
FROM	Market.Merchandising.VehicleAdvertisement VA
	INNER JOIN #AdvertisementIDs A ON VA.AdvertisementID = A.AdvertisementID	

DELETE	A
OUTPUT	DELETED.* INTO Archive.Merchandising.Advertisement
FROM	Market.Merchandising.Advertisement A
	INNER JOIN #AdvertisementIDs AID ON A.AdvertisementID = AID.AdvertisementID	
WHERE	A.RowTypeID = 0			

GO