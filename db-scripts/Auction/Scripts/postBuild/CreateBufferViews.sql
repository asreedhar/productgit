   DECLARE @PresentState TINYINT
    
    SELECT  @PresentState=CurrentBuffer
    FROM    dbo.BufferState
  
    EXECUTE CreateBufferViews 'Sale', '*', '*', @PresentState ,0   ,0
  
    EXECUTE CreateBufferViews 'SaleVehicle', '*', '*', @PresentState ,0   ,0
  
    EXECUTE CreateBufferViews
          'SaleVehicleCompanyData_Adesa'
         ,'
	SaleVehicleID
	,VIN
	,OwnerID
	,ConsignorType
	,Consignor
	,right(replicate('' '', 20) + Lot, 20) Lot
	,right(replicate('' '', 20) + RunNumber, 20) RunNumber
	,Make
	,Model
	,Body
	,Series
	,Engine
	,Color
	,Doors
	,Odometer
	,M_K
	,TransmissionDescription
	 ,AMSOptions
	 ,AMSAnnouncements
	 ,UVCCode'
         ,'*' ,@PresentState ,NULL   ,0
         
    
    EXECUTE CreateBufferViews
          'SaleVehicleCompanyData_ColumbusFair'
              ,'
		SaleVehicleID ,
		Lane_Num,
		Lane_Descr,
		Seller,
		Seller_Descr,
		Access_Level,
		LOT,
		VIN,
		Year,
		Make,
		Model,
		Series,
		Body,
		Mileage,
		Engine,
		Color,
		Options,
		Condition,
		ImageURL,
		Online,
		VehicleURL,
		BuyNow,
		CurrentBid'
         ,'*'
         ,@PresentState
         ,NULL
         ,0

GO