if not exists (select * from dbo.sysusers where name = N'AuctionUser' and uid > 16399)
	EXEC sp_addrole N'AuctionUser'
GO

IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'Firstlook\Analytics'
		)
	CREATE USER [FIRSTLOOK\Analytics] FOR LOGIN [FIRSTLOOK\Analytics] 
go


