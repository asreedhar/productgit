IF OBJECTPROPERTY(OBJECT_ID('LoadData_Pipeline'), 'isProcedure') = 1 
    DROP PROCEDURE LoadData_Pipeline

GO
/******************************************************************************
**
**  Procedure: 
**  Description: Load data from the Pipeline staging table to the _TARGET tables
**
**
**  Return value:  Zero on success, non-zero otherwise
**
**  Input parameters:   See below
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  01/18/2007  PKelley   Copied and modified from the Pipeline version
**  03/29/2007  PKelley   Revised: load options from Options, not OptionList.  Duh.
**  07/10/2007  PKelley   Add in location name to properly set Sales data
**  06/05/2008  WGH       Changed isolation level to READ COMMITTED from 
**			  serializable       
**  02/10/2015	WGH	  Added AuctionDate ISDATE validation
***************************************************************************/
CREATE PROCEDURE dbo.LoadData_Pipeline
    @LogEventMasterID INT = NULL
      --  Top-level ID for DBASTAT..Log_Journal
    ,
    @Debug INT = 0
      --  Pass 1 to show what would happen if you hadn't passed 1.  Note that we
      --  load the _TARGET tables anyway, since they are only made live by other
      --  routines that (will need to) respect the state of @Debug.
AS 
    SET NOCOUNT ON

    DECLARE @ProcedureName VARCHAR(128),
        @LogMessage VARCHAR(255),
        @LongString VARCHAR(4000),
        @Error INT,
        @FullFileName VARCHAR(200),
        @DeleteFile BIT,
        @DataRows INT,
        @InvalidRows INT,
        @LowSaleID INT,
        @HighSaleID INT,
        @LowSaleVehicleID INT,
        @HighSaleVehicleID INT,
        @NextState TINYINT

    SET @ProcedureName = DB_NAME() + '..' + OBJECT_NAME(@@procid)
    SET @LogMessage = 'Beginning Pipeline data load process'
    SET @Error = 0


    EXECUTE DBASTAT..Log_Event 'I', @ProcedureName, @LogMessage,
        @LogEventMasterID OUTPUT  --  Sets it if not already set
        , @Debug

    --  We will load everything for which we can find a matching company

    --  Load Sale  --------------------------------------------------------------------------

    /*
    Being paranoid here, but who knows what we'll end up doing with the Auction
    system in the future.  (Jan 2007: Still looking good...)

    Determine the low and high identity values inserted by this batch, and feed them back
    into the staging table.
    */

    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    BEGIN TRANSACTION


    --  Determine the first new ID
    SELECT  @LowSaleID = MAX(SaleID)
    FROM    Sale_TARGET

    --  If null, set to 1, as we can safely assume the seed will always be 1
    SET @LowSaleID = ISNULL(@LowSaleID, 0) + 1


    --  Load Sale info
    INSERT  Sale_TARGET WITH ( TABLOCKX )
            (
              LocationID,
              DateHeld,
              Title,
              AuctionTypeCode,
              CompanySaleIdentifier,
              ProviderID
            )
            SELECT DISTINCT
                    lo.LocationID,
                    asp.AuctionDate,
                    asp.Sale,
                    CASE CAST(asp.AccessLevel AS INT)
                      WHEN 1 THEN 1  --  Open
                      WHEN 2 THEN 2  --  Closed
                      ELSE 0         --  Unknown
                    END,
                    '',
                    42
            FROM    AuctionStaging_Pipeline asp
                    INNER JOIN Company co ON co.Name = asp.AuctionName
                    INNER JOIN Location lo ON lo.CompanyID = co.CompanyID
            WHERE   asp.IsGood = 1
		AND ISDATE(asp.AuctionDate) = 1

    SET @Error = @@error

    IF @Error <> 0 
        BEGIN
            ROLLBACK
            SET TRANSACTION ISOLATION LEVEL READ COMMITTED

            SET @LogMessage = 'Failed on insert to Sale_TARGET'
            GOTO _Failed_
        END


    --  Determine the last new ID
    SELECT  @HighSaleID = MAX(SaleID)
    FROM    Sale_TARGET

    --  If null, the table is empty, so any value will work
    SET @HighSaleID = ISNULL(@HighSaleID, 0)


    --  Write the ID value back to the staging table.  The READ COMMITTED read ensures
    --  that nothing has been changed througout this transaction.  (Note that we're
    --  only locking up the TARGET tables.)
    UPDATE  AuctionStaging_Pipeline WITH ( TABLOCKX )
    SET     SaleID = sa.SaleID
    FROM    Sale_TARGET sa
            INNER JOIN Location lo ON lo.LocationID = sa.LocationID
            INNER JOIN Company co ON co.CompanyID = lo.CompanyID
            INNER JOIN AuctionStaging_Pipeline asp ON sa.DateHeld = asp.auctionDate
                                                      AND sa.Title = asp.Sale
                                                      AND sa.AuctionTypeCode = CASE CAST(asp.AccessLevel AS INT)
                                                                                 WHEN 1 THEN 1  --  Open
                                                                                 WHEN 2 THEN 2  --  Closed
                                                                                 ELSE 0         --  Unknown
                                                                               END
                                                      AND co.Name = asp.AuctionName
    WHERE   sa.SaleID BETWEEN @LowSaleID AND @HighSaleID
            AND asp.IsGood = 1

    COMMIT
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    --  And we're done with this tricky transaction


    --  Load SaleVehicle  -------------------------------------------------------------------

    /*

    Being paranoid again...

    Determine the low and high identity values inserted by this batch, and feed them back
    into the staging table.

    */

    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    BEGIN TRANSACTION


    --  Determine the first new ID
    SELECT  @LowSaleVehicleID = MAX(SaleVehicleID)
    FROM    SaleVehicle_TARGET

    --  If null, set to 1, as we can safely assume the seed will always be 1
    SET @LowSaleVehicleID = ISNULL(@LowSaleVehicleID, 0) + 1


    --  Load the data
    INSERT  SaleVehicle_TARGET
            (
              SaleID,
              Year,
              GroupingDescriptionID,
              MakeModelGroupingID,
              CompanyVehicleIdentifier,
              	VIN,
		Color,
		Series,
		Odometer,
		Transmission
            )
            SELECT  SaleID,
                    CAST(Year AS SMALLINT),
                    GroupingDescriptionID,
                    MakeModelGroupingID,
                    LaneNumber + ' -- ' + Lot,
			VIN,
			COALESCE(ExteriorColor, ''),	-- downstream assumptions, so this needs to be an empty string.  lame	
			COALESCE(Series, ''),	-- same
			CAST(Mileage AS INT),
			NULL                    
            FROM    AuctionStaging_Pipeline
            WHERE   IsGood = 1

    SET @Error = @@error

    IF @Error <> 0 
        BEGIN
            ROLLBACK
            SET TRANSACTION ISOLATION LEVEL READ COMMITTED

            SET @LogMessage = 'Failed on insert to SaleVehicle_TARGET'
            GOTO _Failed_
        END


    --  Determine the last new ID
    SELECT  @HighSaleVehicleID = MAX(SaleVehicleID)
    FROM    SaleVehicle_TARGET

    --  If null, the table is empty, so any value will work
    SET @HighSaleVehicleID = ISNULL(@HighSaleVehicleID, 0)


    --  Write the ID value back to the staging table.  The READ COMMITTED read ensures
    --  that nothing has been changed througout this transaction.  (Note that we're
    --  only locking the TARGET tables.
    UPDATE  AuctionStaging_Pipeline WITH ( TABLOCKX )
    SET     SaleVehicleID = sv.SaleVehicleID
    FROM    SaleVehicle_TARGET sv
            INNER JOIN AuctionStaging_Pipeline asp ON asp.SaleID = sv.SaleID
                                                      AND asp.LaneNumber
                                                      + ' -- ' + asp.Lot = sv.CompanyVehicleIdentifier
    WHERE   sv.SaleVehicleID BETWEEN @LowSaleVehicleID
                             AND     @HighSaleVehicleID
            AND asp.IsGood = 1

    COMMIT
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    --  And this one's done


    --  Load SaleVehicleCompanyData   --------------------------------------------------

    --  Don't need anything too fancy here, the ID values have already been set
    INSERT  SaleVehicleCompanyData_Target
            (
              SaleVehicleID,
              OwnerID,
              ConsignorType,
              Consignor,
              Lot,
              RunNumber,
              Make,
              Model,
              Body,
              Engine,
              Doors,
              M_K,
              AMSOptions,
              AMSAnnouncements,
              UVCCode
            )
            SELECT  SaleVehicleID,
                    -1    --  Data not available for Pipeline, use a dummy value
                    ,
                    0     --  Data not available for Pipeline, use a dummy value
                    ,
                    Seller,
                    LaneNumber,
                    Lot,
                    Make,
                    LEFT(Model, 50),
                    Body,
                    Engine,
                    0,     --  Data not available for Pipeline, use a dumb value
                    '',    --  Data not available for Pipeline
                    ISNULL(Options, ''),  --  Descended from DAA XML parsing, reserved for future use
                    Condition,
                    ''     --  Data not available for Pipeline
            FROM    AuctionStaging_Pipeline
            WHERE   IsGood = 1

    SET @Error = @@error

    IF @Error <> 0 
        BEGIN
            SET @LogMessage = 'Failed on insert to SaleVehicleCompanyData_Target'
            GOTO _Failed_
        END


    SET @LogMessage = 'Staged Pipeline data loaded into _TARGET tables'
    EXECUTE DBASTAT..Log_Event 'I', @ProcedureName, @LogMessage,
        @LogEventMasterID, @Debug

    RETURN 0


    --  ERROR HANDLING  ---------------------------------------------------------------------
    _Failed_:

    SET @LogMessage = 'ERROR# ' + CAST(@Error AS VARCHAR(10)) + ': '
        + @LogMessage

    --  Log the error
    EXECUTE DBASTAT..Log_Event 'E', @ProcedureName, @LogMessage,
        @LogEventMasterID, @Debug

    --  Configure error message
    SET @LongString = 'Pipeline Dataload failed.
Initial Log_Journal ID: ' + CAST(@LogEventMasterID AS VARCHAR(10)) + '
Log_Journal error message: ' + @LogMessage

    RAISERROR ( @LongString, 11, 1 )

    RETURN @Error

GO
