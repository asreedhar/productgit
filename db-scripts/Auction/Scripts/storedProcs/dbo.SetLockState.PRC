IF objectproperty(object_id('SetLockState'), 'isProcedure') = 1
    DROP PROCEDURE SetLockState

GO
/******************************************************************************
**
**  Procedure: SetLockState
**  Description: Use this procedure to lock and unlock the (future) Auction
**   dataload process.  The current state of the process is defined within
**   table DataLoadEngineStatus.  Note that if LockState is set to anything
**   besides "Locked" and "Unlocked", this process will return a failure code.
**
**
**  Return values:  none
**
**  Input parameters:   See below
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  02/15/2006  PKelley   Procedure created.
**  05/22/2006  PKelley   Copied to Auction from ATC database
**
*****************************************************************************/
CREATE PROCEDURE dbo.SetLockState

    @LockState         varchar(20)
   ,@LogEventMasterID  int  = NULL

AS

    SET NOCOUNT on

    DECLARE
      @ThisProcedure  varchar(128)
     ,@LogMessage     varchar(500)

    SET @ThisProcedure = db_name() + '..' + object_name(@@procid)  

    --  Validate parameter
    IF @LockState not in ('Locked', 'Unlocked')
     BEGIN
        SET @LogMessage = 'This procedure only accepts values of "Locked" and "Unlocked"'
        GOTO _Failed_
     END


    IF @LockState = 'Locked'
        --  Request to lock the process
        UPDATE DataLoadEngineStatus
         set LockState = 'Locked'
         where LockState = 'Unlocked'

    ELSE
        --  Request to unlock the process
        UPDATE DataLoadEngineStatus
         set LockState = 'Unlocked'
         where LockState = 'Locked'


    IF @@rowcount = 0
     BEGIN
        --  Could not alter the state
        SELECT @LogMessage = 'Could not set LockState to ' + @LockState + '; LockState was ' + LockState
         from DataLoadEngineStatus
        GOTO _Failed_
     END


    --  Log the (succesful) event
    SET @LogMessage = 'System state set to ' + @LockState
    EXECUTE DBASTAT..Log_Event 'I', @ThisProcedure, @LogMessage, @LogEventMasterID

RETURN 0

    --  ERROR HANDLING  ---------------------------------------------------------------------
    _Failed_:

    EXECUTE DBASTAT..Log_Event
      'E'
     ,@ThisProcedure
     ,@LogMessage
     ,@LogEventMasterID

    RAISERROR (@LogMessage, 11, 1)

RETURN 1
GO
