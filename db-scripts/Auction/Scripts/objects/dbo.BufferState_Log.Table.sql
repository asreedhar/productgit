/****** Object:  Table [dbo].[BufferState_Log]    Script Date: 01/22/2014 17:55:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BufferState_Log]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[BufferState_Log](
	[BufferSetAt] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
