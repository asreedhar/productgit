/****** Object:  Table [dbo].[ValidationCheck]    Script Date: 01/22/2014 17:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ValidationCheck]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ValidationCheck](
	[ValidationCheckID] [smallint] NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[Description] [varchar](500) NOT NULL,
 CONSTRAINT [PK_ValidationCheck] PRIMARY KEY CLUSTERED 
(
	[ValidationCheckID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ValidationCheck', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is used to list the check and validation routines used to “cleanse” the data. It’s intended for use if you need to follow up on why data did not load. (~2009)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ValidationCheck'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TableType' , N'SCHEMA',N'dbo', N'TABLE',N'ValidationCheck', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'TableType', @value=N'Reference' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ValidationCheck'
GO
