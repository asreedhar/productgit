/****** Object:  Table [dbo].[SaleVehicleCompanyData_Adesa#0]    Script Date: 01/22/2014 17:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SaleVehicleCompanyData_Adesa#0]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SaleVehicleCompanyData_Adesa#0](
	[SaleVehicleID] [int] NOT NULL,
	[OwnerID] [int] NOT NULL,
	[ConsignorType] [tinyint] NOT NULL,
	[Consignor] [varchar](100) NOT NULL,
	[Lot] [varchar](20) NOT NULL,
	[RunNumber] [varchar](20) NOT NULL,
	[Make] [varchar](50) NOT NULL,
	[Model] [varchar](50) NOT NULL,
	[Body] [varchar](50) NOT NULL,
	[Engine] [varchar](50) NOT NULL,
	[Doors] [tinyint] NOT NULL,
	[M_K] [char](1) NOT NULL,
	[AMSOptions] [varchar](500) NOT NULL,
	[AMSAnnouncements] [varchar](500) NOT NULL,
	[UVCCode] [varchar](10) NOT NULL,
 CONSTRAINT [PK_SaleVehicleCompanyData_Adesa#0] PRIMARY KEY NONCLUSTERED 
(
	[SaleVehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SaleVehicleCompanyData_Adesa#0', N'COLUMN',N'SaleVehicleID'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key, related to SaleVehicle(#0 or #1) (~2009)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SaleVehicleCompanyData_Adesa#0', @level2type=N'COLUMN',@level2name=N'SaleVehicleID'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SaleVehicleCompanyData_Adesa#0', N'COLUMN',N'ConsignorType'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Consignor code, interpreted as below. (This may have to be put in a lookup table some day…?)
<br>
<ol><li>Dealer</li><li>Captive Finance</li><li>Fleet/Lease
</li><li>Factory
</li><li>Federal Government
</li><li>Public Buyer
</li><li>Provincial/State Government
</li><li>Local Government
</li><li>Salvage
</li><li>Floor Plan Company</li></ol> (~2009)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SaleVehicleCompanyData_Adesa#0', @level2type=N'COLUMN',@level2name=N'ConsignorType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SaleVehicleCompanyData_Adesa#0', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Except for their names, SaleVehicleCompanyData_Adesa#0 and SaleVehicleCompanyData_Adesa#1 are identical. The associated views (SaleVehicleCompanyData_Adesa, SaleVehicleCompanyData_Adesa_TARGET) are simple “select *” queries against the appropriate tables, as described in table BufferState.
This is a “vertical partition” of the SaleVehicle table(s). These columns could be added on to the associated SaleVehicle table… but for performance reasons they are loaded into their own table and configured with a one-to-one relationship with that “parent” table.
This table is deleted and repopulated with every run. (It is not truncated, so the identity value is not reseeded.)
 (~2009)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SaleVehicleCompanyData_Adesa#0'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SaleVehicleCompanyData_Adesa#0__SaleVehicle#0]') AND parent_object_id = OBJECT_ID(N'[dbo].[SaleVehicleCompanyData_Adesa#0]'))
ALTER TABLE [dbo].[SaleVehicleCompanyData_Adesa#0]  WITH CHECK ADD  CONSTRAINT [FK_SaleVehicleCompanyData_Adesa#0__SaleVehicle#0] FOREIGN KEY([SaleVehicleID])
REFERENCES [dbo].[SaleVehicle#0] ([SaleVehicleID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SaleVehicleCompanyData_Adesa#0__SaleVehicle#0]') AND parent_object_id = OBJECT_ID(N'[dbo].[SaleVehicleCompanyData_Adesa#0]'))
ALTER TABLE [dbo].[SaleVehicleCompanyData_Adesa#0] CHECK CONSTRAINT [FK_SaleVehicleCompanyData_Adesa#0__SaleVehicle#0]
GO
