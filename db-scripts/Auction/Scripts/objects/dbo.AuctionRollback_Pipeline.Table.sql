/****** Object:  Table [dbo].[AuctionRollback_Pipeline]    Script Date: 01/22/2014 17:55:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuctionRollback_Pipeline]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AuctionRollback_Pipeline](
	[StagingID] [int] NOT NULL,
	[IsGood] [bit] NOT NULL,
	[SaleID] [int] NULL,
	[SaleVehicleID] [int] NULL,
	[GroupingDescriptionID] [int] NULL,
	[MakeModelGroupingID] [int] NULL,
	[OptionList] [varchar](500) NULL,
	[AuctionName] [varchar](250) NOT NULL,
	[AuctionState] [varchar](10) NOT NULL,
	[AuctionZip] [varchar](10) NOT NULL,
	[AuctionDate] [varchar](50) NOT NULL,
	[AuctionStartTime] [varchar](50) NOT NULL,
	[AuctionEndTime] [varchar](50) NOT NULL,
	[Sale] [varchar](100) NOT NULL,
	[LaneNumber] [varchar](50) NOT NULL,
	[LaneDescription] [varchar](500) NOT NULL,
	[Seller] [varchar](500) NOT NULL,
	[SellerDescription] [varchar](500) NOT NULL,
	[Accesslevel] [varchar](10) NOT NULL,
	[Lot] [varchar](50) NOT NULL,
	[VIN] [varchar](50) NOT NULL,
	[Year] [varchar](10) NOT NULL,
	[Make] [varchar](50) NOT NULL,
	[Model] [varchar](50) NOT NULL,
	[Series] [varchar](50) NOT NULL,
	[Body] [varchar](50) NOT NULL,
	[Mileage] [varchar](10) NOT NULL,
	[Engine] [varchar](50) NOT NULL,
	[ExteriorColor] [varchar](50) NOT NULL,
	[Options] [varchar](5000) NOT NULL,
	[Condition] [varchar](500) NOT NULL,
	[ImageURL] [varchar](500) NOT NULL,
	[Online] [varchar](10) NOT NULL,
	[VehicleURL] [varchar](500) NOT NULL,
	[BuyNow] [varchar](20) NOT NULL,
	[CurrentBid] [varchar](20) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
