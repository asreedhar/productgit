/****** Object:  Table [dbo].[SaleVehicleCompanyData_Adesa#1]    Script Date: 01/22/2014 17:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SaleVehicleCompanyData_Adesa#1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SaleVehicleCompanyData_Adesa#1](
	[SaleVehicleID] [int] NOT NULL,
	[OwnerID] [int] NOT NULL,
	[ConsignorType] [tinyint] NOT NULL,
	[Consignor] [varchar](100) NOT NULL,
	[Lot] [varchar](20) NOT NULL,
	[RunNumber] [varchar](20) NOT NULL,
	[Make] [varchar](50) NOT NULL,
	[Model] [varchar](50) NOT NULL,
	[Body] [varchar](50) NOT NULL,
	[Engine] [varchar](50) NOT NULL,
	[Doors] [tinyint] NOT NULL,
	[M_K] [char](1) NOT NULL,
	[AMSOptions] [varchar](500) NOT NULL,
	[AMSAnnouncements] [varchar](500) NOT NULL,
	[UVCCode] [varchar](10) NOT NULL,
 CONSTRAINT [PK_SaleVehicleCompanyData_Adesa#1] PRIMARY KEY NONCLUSTERED 
(
	[SaleVehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SaleVehicleCompanyData_Adesa#1', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'See <a href="http://qa.firstlook/docs/EntityProperties.asp?EntityID=15338&EntityTypeID=4&IsEntityType=False">SaleVehicleCompanyData_Adesa#0</a> (~2009)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SaleVehicleCompanyData_Adesa#1'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SaleVehicleCompanyData_Adesa#1__SaleVehicle#1]') AND parent_object_id = OBJECT_ID(N'[dbo].[SaleVehicleCompanyData_Adesa#1]'))
ALTER TABLE [dbo].[SaleVehicleCompanyData_Adesa#1]  WITH CHECK ADD  CONSTRAINT [FK_SaleVehicleCompanyData_Adesa#1__SaleVehicle#1] FOREIGN KEY([SaleVehicleID])
REFERENCES [dbo].[SaleVehicle#1] ([SaleVehicleID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SaleVehicleCompanyData_Adesa#1__SaleVehicle#1]') AND parent_object_id = OBJECT_ID(N'[dbo].[SaleVehicleCompanyData_Adesa#1]'))
ALTER TABLE [dbo].[SaleVehicleCompanyData_Adesa#1] CHECK CONSTRAINT [FK_SaleVehicleCompanyData_Adesa#1__SaleVehicle#1]
GO
