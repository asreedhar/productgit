/****** Object:  Table [dbo].[PurgeManagement]    Script Date: 01/22/2014 17:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PurgeManagement]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PurgeManagement](
	[Subject] [varchar](25) NOT NULL,
	[Description] [varchar](200) NOT NULL,
	[Period] [tinyint] NOT NULL,
	[PeriodType] [varchar](6) NOT NULL,
	[Routine] [varchar](100) NOT NULL,
	[LastCall] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_PurgeManagement] PRIMARY KEY CLUSTERED 
(
	[Subject] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'PurgeManagement', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table supports the “purge non-essential data” system, for which detailed documentation exists elsewhere.
As of Sept 29, the only purges performed are on AuctionLoadLog and on the purge log itself. The initial configuration will puring AuctionLoadLog data that is over six months old.
 (~2009)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PurgeManagement'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TableType' , N'SCHEMA',N'dbo', N'TABLE',N'PurgeManagement', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'TableType', @value=N'Reference' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PurgeManagement'
GO
IF NOT EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PurgeManagement__PeriodType]') AND parent_object_id = OBJECT_ID(N'[dbo].[PurgeManagement]'))
ALTER TABLE [dbo].[PurgeManagement]  WITH CHECK ADD  CONSTRAINT [CK_PurgeManagement__PeriodType] CHECK  (([PeriodType] = 'Years' or ([PeriodType] = 'Months' or ([PeriodType] = 'Weeks' or [PeriodType] = 'Days'))))
GO
IF  EXISTS (SELECT * FROM sys.check_constraints WHERE object_id = OBJECT_ID(N'[dbo].[CK_PurgeManagement__PeriodType]') AND parent_object_id = OBJECT_ID(N'[dbo].[PurgeManagement]'))
ALTER TABLE [dbo].[PurgeManagement] CHECK CONSTRAINT [CK_PurgeManagement__PeriodType]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_PurgeManagement]') AND parent_object_id = OBJECT_ID(N'[dbo].[PurgeManagement]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PurgeManagement]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PurgeManagement] ADD  CONSTRAINT [DF_PurgeManagement]  DEFAULT ('Jan 1, 1980') FOR [LastCall]
END


End
GO
