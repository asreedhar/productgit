/****** Object:  Table [dbo].[SaleVehicleCompanyData_ColumbusFair#0]    Script Date: 01/22/2014 17:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SaleVehicleCompanyData_ColumbusFair#0]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SaleVehicleCompanyData_ColumbusFair#0](
	[SaleVehicleID] [int] NOT NULL,
	[Lane_Num] [varchar](25) NOT NULL,
	[Lane_Descr] [varchar](250) NOT NULL,
	[Seller] [varchar](250) NOT NULL,
	[Seller_Descr] [varchar](250) NOT NULL,
	[Access_Level] [int] NOT NULL,
	[LOT] [varchar](10) NOT NULL,
	[Year] [int] NOT NULL,
	[Make] [varchar](50) NOT NULL,
	[Model] [varchar](50) NOT NULL,
	[Body] [varchar](50) NOT NULL,
	[Engine] [varchar](50) NOT NULL,
	[Options] [varchar](250) NOT NULL,
	[Condition] [varchar](250) NOT NULL,
	[ImageURL] [varchar](250) NOT NULL,
	[Online] [varchar](50) NOT NULL,
	[VehicleURL] [varchar](250) NOT NULL,
	[BuyNow] [varchar](50) NOT NULL,
	[CurrentBid] [varchar](50) NOT NULL,
 CONSTRAINT [PK_SaleVehicleCompanyData_ColumbusFair#0] PRIMARY KEY NONCLUSTERED 
(
	[SaleVehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SaleVehicleCompanyData_ColumbusFair#0__SaleVehicle#0]') AND parent_object_id = OBJECT_ID(N'[dbo].[SaleVehicleCompanyData_ColumbusFair#0]'))
ALTER TABLE [dbo].[SaleVehicleCompanyData_ColumbusFair#0]  WITH CHECK ADD  CONSTRAINT [FK_SaleVehicleCompanyData_ColumbusFair#0__SaleVehicle#0] FOREIGN KEY([SaleVehicleID])
REFERENCES [dbo].[SaleVehicle#0] ([SaleVehicleID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SaleVehicleCompanyData_ColumbusFair#0__SaleVehicle#0]') AND parent_object_id = OBJECT_ID(N'[dbo].[SaleVehicleCompanyData_ColumbusFair#0]'))
ALTER TABLE [dbo].[SaleVehicleCompanyData_ColumbusFair#0] CHECK CONSTRAINT [FK_SaleVehicleCompanyData_ColumbusFair#0__SaleVehicle#0]
GO
