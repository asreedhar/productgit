/****** Object:  Table [dbo].[SaleVehicle#0]    Script Date: 01/22/2014 17:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SaleVehicle#0]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SaleVehicle#0](
	[SaleVehicleID] [int] IDENTITY(1,1) NOT NULL,
	[SaleID] [int] NOT NULL,
	[Year] [smallint] NOT NULL,
	[GroupingDescriptionID] [int] NOT NULL,
	[CompanyVehicleIdentifier] [varchar](50) NOT NULL,
	[MakeModelGroupingID] [int] NOT NULL,
	[VIN] [char](17) NOT NULL,
	[Color] [varchar](50) NULL,
	[Series] [varchar](50) NULL,
	[Odometer] [int] NOT NULL,
	[Transmission] [varchar](50) NULL,
 CONSTRAINT [PK_SaleVehicle#0] PRIMARY KEY NONCLUSTERED 
(
	[SaleVehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SaleVehicle#0]') AND name = N'IX_SaleVehicle#0__SaleID')
CREATE CLUSTERED INDEX [IX_SaleVehicle#0__SaleID] ON [dbo].[SaleVehicle#0] 
(
	[SaleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SaleVehicle#0', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Except for their names, SaleVehicle#0 and SaleVehicle#1 are identical. The associated views (SaleVehicle, SaleVehicle_TARGET) are simple “select *” queries against the appropriate tables, as described in table BufferState.
An entry is made for those “good” vehicles that we can load.
This table contains only the information required to analyze auctions (that is, judging whether a dealer would be more or less likely to benefit from attending the auction). Information that is always retrieved for this vehicle could also be included here—but the table should be as narrow as possible, to optimize performance.
This table is deleted and repopulated with every run. (It is not truncated, so the identity value is not reseeded.)
 (~2009)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SaleVehicle#0'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SaleVehicle#0__Sale#0]') AND parent_object_id = OBJECT_ID(N'[dbo].[SaleVehicle#0]'))
ALTER TABLE [dbo].[SaleVehicle#0]  WITH CHECK ADD  CONSTRAINT [FK_SaleVehicle#0__Sale#0] FOREIGN KEY([SaleID])
REFERENCES [dbo].[Sale#0] ([SaleID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SaleVehicle#0__Sale#0]') AND parent_object_id = OBJECT_ID(N'[dbo].[SaleVehicle#0]'))
ALTER TABLE [dbo].[SaleVehicle#0] CHECK CONSTRAINT [FK_SaleVehicle#0__Sale#0]
GO
