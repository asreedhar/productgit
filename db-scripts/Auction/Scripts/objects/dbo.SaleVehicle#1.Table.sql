/****** Object:  Table [dbo].[SaleVehicle#1]    Script Date: 01/22/2014 17:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SaleVehicle#1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SaleVehicle#1](
	[SaleVehicleID] [int] IDENTITY(1,1) NOT NULL,
	[SaleID] [int] NOT NULL,
	[Year] [smallint] NOT NULL,
	[GroupingDescriptionID] [int] NOT NULL,
	[CompanyVehicleIdentifier] [varchar](50) NOT NULL,
	[MakeModelGroupingID] [int] NOT NULL,
	[VIN] [char](17) NOT NULL,
	[Color] [varchar](50) NULL,
	[Series] [varchar](50) NULL,
	[Odometer] [int] NOT NULL,
	[Transmission] [varchar](50) NULL,
 CONSTRAINT [PK_SaleVehicle#1] PRIMARY KEY NONCLUSTERED 
(
	[SaleVehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SaleVehicle#1]') AND name = N'IX_SaleVehicle#1__SaleID')
CREATE CLUSTERED INDEX [IX_SaleVehicle#1__SaleID] ON [dbo].[SaleVehicle#1] 
(
	[SaleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'SaleVehicle#1', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'See <a href="http://qa.firstlook/docs/EntityProperties.asp?EntityID=15334&EntityTypeID=4&IsEntityType=False">SaleVehicle#0</a> (~2009)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'SaleVehicle#1'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SaleVehicle#1__Sale#1]') AND parent_object_id = OBJECT_ID(N'[dbo].[SaleVehicle#1]'))
ALTER TABLE [dbo].[SaleVehicle#1]  WITH CHECK ADD  CONSTRAINT [FK_SaleVehicle#1__Sale#1] FOREIGN KEY([SaleID])
REFERENCES [dbo].[Sale#1] ([SaleID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SaleVehicle#1__Sale#1]') AND parent_object_id = OBJECT_ID(N'[dbo].[SaleVehicle#1]'))
ALTER TABLE [dbo].[SaleVehicle#1] CHECK CONSTRAINT [FK_SaleVehicle#1__Sale#1]
GO
