/****** Object:  Table [dbo].[DatafeedFile]    Script Date: 01/22/2014 17:55:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DatafeedFile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DatafeedFile](
	[DatafeedFileID] [int] IDENTITY(1,1) NOT NULL,
	[DatafeedID] [tinyint] NOT NULL,
	[Filename] [varchar](200) NOT NULL,
	[DatafeedStatusCode] [tinyint] NOT NULL,
	[StatusSetAt] [datetime] NOT NULL,
	[Comment] [varchar](500) NOT NULL,
 CONSTRAINT [PK_DatafeedFile] PRIMARY KEY CLUSTERED 
(
	[DatafeedFileID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DatafeedFile]') AND name = N'IX_DatafeedFile__DatafeedStatusCode')
CREATE NONCLUSTERED INDEX [IX_DatafeedFile__DatafeedStatusCode] ON [dbo].[DatafeedFile] 
(
	[DatafeedStatusCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DatafeedFile]') AND name = N'IX_DatafeedFile__Filename')
CREATE NONCLUSTERED INDEX [IX_DatafeedFile__Filename] ON [dbo].[DatafeedFile] 
(
	[Filename] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DatafeedFile__Datafeed]') AND parent_object_id = OBJECT_ID(N'[dbo].[DatafeedFile]'))
ALTER TABLE [dbo].[DatafeedFile]  WITH CHECK ADD  CONSTRAINT [FK_DatafeedFile__Datafeed] FOREIGN KEY([DatafeedID])
REFERENCES [dbo].[Datafeed] ([DatafeedID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DatafeedFile__Datafeed]') AND parent_object_id = OBJECT_ID(N'[dbo].[DatafeedFile]'))
ALTER TABLE [dbo].[DatafeedFile] CHECK CONSTRAINT [FK_DatafeedFile__Datafeed]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DatafeedFile__DatafeedStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[DatafeedFile]'))
ALTER TABLE [dbo].[DatafeedFile]  WITH CHECK ADD  CONSTRAINT [FK_DatafeedFile__DatafeedStatus] FOREIGN KEY([DatafeedStatusCode])
REFERENCES [dbo].[DatafeedStatus] ([DatafeedStatusCode])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DatafeedFile__DatafeedStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[DatafeedFile]'))
ALTER TABLE [dbo].[DatafeedFile] CHECK CONSTRAINT [FK_DatafeedFile__DatafeedStatus]
GO
