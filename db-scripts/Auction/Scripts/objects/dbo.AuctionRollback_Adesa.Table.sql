/****** Object:  Table [dbo].[AuctionRollback_Adesa]    Script Date: 01/22/2014 17:55:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuctionRollback_Adesa]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AuctionRollback_Adesa](
	[StagingID] [int] NOT NULL,
	[IsGood] [bit] NOT NULL,
	[SaleID] [int] NULL,
	[SaleVehicleID] [int] NULL,
	[GroupingDescriptionID] [int] NULL,
	[MakeModelGroupingID] [int] NULL,
	[AMSSaleID] [varchar](20) NOT NULL,
	[SaleDate] [varchar](20) NOT NULL,
	[SaleType] [varchar](50) NOT NULL,
	[OwnerID] [varchar](20) NOT NULL,
	[ConsignorType] [varchar](10) NOT NULL,
	[Consignor] [varchar](500) NOT NULL,
	[LocationID] [varchar](20) NOT NULL,
	[Location] [varchar](100) NOT NULL,
	[Lot] [varchar](10) NOT NULL,
	[RunNumber] [varchar](10) NOT NULL,
	[Year] [varchar](10) NOT NULL,
	[Make] [varchar](50) NOT NULL,
	[Model] [varchar](100) NOT NULL,
	[Body] [varchar](100) NOT NULL,
	[Series] [varchar](100) NOT NULL,
	[Engine] [varchar](100) NOT NULL,
	[Color] [varchar](100) NOT NULL,
	[Doors] [varchar](10) NOT NULL,
	[Odometer] [varchar](10) NOT NULL,
	[M_K] [varchar](10) NOT NULL,
	[VIN] [varchar](50) NOT NULL,
	[OpenSale] [varchar](10) NOT NULL,
	[TransmissionDescription] [varchar](100) NOT NULL,
	[AMSOptions] [varchar](2000) NOT NULL,
	[AMSAnnouncements] [varchar](2000) NOT NULL,
	[UVCCode] [varchar](20) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
