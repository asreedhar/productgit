/****** Object:  Table [dbo].[AuctionRollback_ColumbusFair]    Script Date: 01/22/2014 17:55:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuctionRollback_ColumbusFair]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AuctionRollback_ColumbusFair](
	[StagingID] [int] NOT NULL,
	[IsGood] [bit] NOT NULL,
	[SaleID] [int] NULL,
	[SaleVehicleID] [int] NULL,
	[GroupingDescriptionID] [int] NULL,
	[MakeModelGroupingID] [int] NULL,
	[Auction_Name] [varchar](250) NOT NULL,
	[Auction_State] [varchar](50) NOT NULL,
	[Auction_Zip] [varchar](50) NOT NULL,
	[Auction_Date] [varchar](50) NOT NULL,
	[Auction_Start_Time] [varchar](50) NOT NULL,
	[Auction_End_Time] [varchar](50) NOT NULL,
	[Sale] [varchar](50) NOT NULL,
	[Lane_Descr] [varchar](250) NOT NULL,
	[Seller] [varchar](250) NOT NULL,
	[Seller_Descr] [varchar](250) NOT NULL,
	[Access_Level] [varchar](50) NOT NULL,
	[LOT] [varchar](50) NOT NULL,
	[VIN] [varchar](50) NOT NULL,
	[Year] [varchar](50) NOT NULL,
	[Make] [varchar](50) NOT NULL,
	[Model] [varchar](50) NOT NULL,
	[Series] [varchar](50) NOT NULL,
	[Body] [varchar](50) NOT NULL,
	[Mileage] [varchar](50) NOT NULL,
	[Engine] [varchar](50) NOT NULL,
	[Color] [varchar](50) NOT NULL,
	[Options] [varchar](2000) NOT NULL,
	[Condition] [varchar](250) NOT NULL,
	[ImageURL] [varchar](250) NOT NULL,
	[Online] [varchar](50) NOT NULL,
	[VehicleURL] [varchar](250) NOT NULL,
	[BuyNow] [varchar](50) NOT NULL,
	[CurrentBid] [varchar](50) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
