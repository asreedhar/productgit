/****** Object:  Table [dbo].[AuctionStaging_ColumbusFair]    Script Date: 01/22/2014 17:55:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AuctionStaging_ColumbusFair](
	[StagingID] [int] IDENTITY(1,1) NOT NULL,
	[IsGood] [bit] NOT NULL,
	[SaleID] [int] NULL,
	[SaleVehicleID] [int] NULL,
	[GroupingDescriptionID] [int] NULL,
	[MakeModelGroupingID] [int] NULL,
	[Auction_Name] [varchar](250) NOT NULL,
	[Auction_State] [varchar](50) NOT NULL,
	[Auction_Zip] [varchar](50) NOT NULL,
	[Auction_Date] [varchar](50) NOT NULL,
	[Auction_Start_Time] [varchar](50) NOT NULL,
	[Auction_End_Time] [varchar](50) NOT NULL,
	[Sale] [varchar](50) NOT NULL,
	[Lane_Descr] [varchar](250) NOT NULL,
	[Seller] [varchar](250) NOT NULL,
	[Seller_Descr] [varchar](250) NOT NULL,
	[Access_Level] [varchar](50) NOT NULL,
	[LOT] [varchar](50) NOT NULL,
	[VIN] [varchar](50) NOT NULL,
	[Year] [varchar](50) NOT NULL,
	[Make] [varchar](50) NOT NULL,
	[Model] [varchar](50) NOT NULL,
	[Series] [varchar](50) NOT NULL,
	[Body] [varchar](50) NOT NULL,
	[Mileage] [varchar](50) NOT NULL,
	[Engine] [varchar](50) NOT NULL,
	[Color] [varchar](50) NOT NULL,
	[Options] [varchar](2000) NOT NULL,
	[Condition] [varchar](250) NOT NULL,
	[ImageURL] [varchar](250) NOT NULL,
	[Online] [varchar](50) NOT NULL,
	[VehicleURL] [varchar](250) NOT NULL,
	[BuyNow] [varchar](50) NOT NULL,
	[CurrentBid] [varchar](50) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__IsGood]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__IsGood]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__IsGood]  DEFAULT ((1)) FOR [IsGood]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Auction_Name]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Auction_Name]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Auction_Name]  DEFAULT ('') FOR [Auction_Name]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Auction_State]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Auction_State]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Auction_State]  DEFAULT ('') FOR [Auction_State]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Auction_Zip]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Auction_Zip]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Auction_Zip]  DEFAULT ('') FOR [Auction_Zip]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Auction_Date]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Auction_Date]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Auction_Date]  DEFAULT ('') FOR [Auction_Date]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Auction_Start_Time]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Auction_Start_Time]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Auction_Start_Time]  DEFAULT ('') FOR [Auction_Start_Time]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Auction_End_Time]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Auction_End_Time]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Auction_End_Time]  DEFAULT ('') FOR [Auction_End_Time]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Sale]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Sale]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Sale]  DEFAULT ('') FOR [Sale]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Lane_Descr]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Lane_Descr]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Lane_Descr]  DEFAULT ('') FOR [Lane_Descr]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Seller]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Seller]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Seller]  DEFAULT ('') FOR [Seller]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Seller_Descr]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Seller_Descr]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Seller_Descr]  DEFAULT ('') FOR [Seller_Descr]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Access_Level]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Access_Level]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Access_Level]  DEFAULT ('') FOR [Access_Level]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__LOT]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__LOT]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__LOT]  DEFAULT ('') FOR [LOT]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__VIN]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__VIN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__VIN]  DEFAULT ('') FOR [VIN]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Year]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Year]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Year]  DEFAULT ('') FOR [Year]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Make]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Make]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Make]  DEFAULT ('') FOR [Make]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Model]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Model]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Model]  DEFAULT ('') FOR [Model]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Series]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Series]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Series]  DEFAULT ('') FOR [Series]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Body]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Body]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Body]  DEFAULT ('') FOR [Body]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Mileage]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Mileage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Mileage]  DEFAULT ('') FOR [Mileage]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Engine]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Engine]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Engine]  DEFAULT ('') FOR [Engine]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Color]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Color]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Color]  DEFAULT ('') FOR [Color]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Options]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Options]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Options]  DEFAULT ('') FOR [Options]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Condition]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Condition]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Condition]  DEFAULT ('') FOR [Condition]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__ImageURL]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__ImageURL]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__ImageURL]  DEFAULT ('') FOR [ImageURL]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__Online]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__Online]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__Online]  DEFAULT ('') FOR [Online]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__VehicleURL]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__VehicleURL]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__VehicleURL]  DEFAULT ('') FOR [VehicleURL]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__BuyNow]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__BuyNow]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__BuyNow]  DEFAULT ('') FOR [BuyNow]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_ColumbusFair__CurrentBid]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_ColumbusFair]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_ColumbusFair__CurrentBid]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_ColumbusFair] ADD  CONSTRAINT [DF_AuctionStaging_ColumbusFair__CurrentBid]  DEFAULT ('') FOR [CurrentBid]
END


End
GO
