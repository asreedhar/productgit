/****** Object:  Table [dbo].[ValidationFailures]    Script Date: 01/22/2014 17:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ValidationFailures]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ValidationFailures](
	[DatafeedID] [tinyint] NOT NULL,
	[StagingID] [int] NOT NULL,
	[ValidationCheckID] [smallint] NOT NULL
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'ValidationFailures', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When “not good ” items are found, they and the validation check they fail are recorded in this table. If a row fails several checks, each will be logged here. This table is cleared at the beginning of every data load run.
There is no index and no primary key on this table (though there could be).
 (~2009)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ValidationFailures'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ValidationFailures__Datafeed]') AND parent_object_id = OBJECT_ID(N'[dbo].[ValidationFailures]'))
ALTER TABLE [dbo].[ValidationFailures]  WITH CHECK ADD  CONSTRAINT [FK_ValidationFailures__Datafeed] FOREIGN KEY([DatafeedID])
REFERENCES [dbo].[Datafeed] ([DatafeedID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ValidationFailures__Datafeed]') AND parent_object_id = OBJECT_ID(N'[dbo].[ValidationFailures]'))
ALTER TABLE [dbo].[ValidationFailures] CHECK CONSTRAINT [FK_ValidationFailures__Datafeed]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ValidationFailures__ValidationCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[ValidationFailures]'))
ALTER TABLE [dbo].[ValidationFailures]  WITH CHECK ADD  CONSTRAINT [FK_ValidationFailures__ValidationCheck] FOREIGN KEY([ValidationCheckID])
REFERENCES [dbo].[ValidationCheck] ([ValidationCheckID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ValidationFailures__ValidationCheck]') AND parent_object_id = OBJECT_ID(N'[dbo].[ValidationFailures]'))
ALTER TABLE [dbo].[ValidationFailures] CHECK CONSTRAINT [FK_ValidationFailures__ValidationCheck]
GO
