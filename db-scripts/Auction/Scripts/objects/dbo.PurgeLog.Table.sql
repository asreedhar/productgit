/****** Object:  Table [dbo].[PurgeLog]    Script Date: 01/22/2014 17:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PurgeLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PurgeLog](
	[Subject] [varchar](25) NOT NULL,
	[Executed] [datetime] NOT NULL,
	[ItemsDeleted] [int] NOT NULL,
	[First] [varchar](50) NOT NULL,
	[Last] [varchar](50) NOT NULL,
 CONSTRAINT [PK_PurgeLog] PRIMARY KEY NONCLUSTERED 
(
	[Subject] ASC,
	[Executed] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'PurgeLog', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This table supports the “purge non-essential data” system, for which detailed documentation exists elsewhere. (~2009)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'PurgeLog'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PurgeLog__PurgeManagement]') AND parent_object_id = OBJECT_ID(N'[dbo].[PurgeLog]'))
ALTER TABLE [dbo].[PurgeLog]  WITH CHECK ADD  CONSTRAINT [FK_PurgeLog__PurgeManagement] FOREIGN KEY([Subject])
REFERENCES [dbo].[PurgeManagement] ([Subject])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PurgeLog__PurgeManagement]') AND parent_object_id = OBJECT_ID(N'[dbo].[PurgeLog]'))
ALTER TABLE [dbo].[PurgeLog] CHECK CONSTRAINT [FK_PurgeLog__PurgeManagement]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_PurgeLog__Executed]') AND parent_object_id = OBJECT_ID(N'[dbo].[PurgeLog]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PurgeLog__Executed]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[PurgeLog] ADD  CONSTRAINT [DF_PurgeLog__Executed]  DEFAULT (getdate()) FOR [Executed]
END


End
GO
