/****** Object:  Table [dbo].[AuctionStaging_Pipeline]    Script Date: 01/22/2014 17:55:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AuctionStaging_Pipeline](
	[StagingID] [int] IDENTITY(1,1) NOT NULL,
	[IsGood] [bit] NOT NULL,
	[SaleID] [int] NULL,
	[SaleVehicleID] [int] NULL,
	[GroupingDescriptionID] [int] NULL,
	[MakeModelGroupingID] [int] NULL,
	[OptionList] [varchar](500) NULL,
	[AuctionName] [varchar](250) NOT NULL,
	[AuctionState] [varchar](10) NOT NULL,
	[AuctionZip] [varchar](10) NOT NULL,
	[AuctionDate] [varchar](50) NOT NULL,
	[AuctionStartTime] [varchar](50) NOT NULL,
	[AuctionEndTime] [varchar](50) NOT NULL,
	[Sale] [varchar](100) NOT NULL,
	[LaneNumber] [varchar](50) NOT NULL,
	[LaneDescription] [varchar](500) NOT NULL,
	[Seller] [varchar](500) NOT NULL,
	[SellerDescription] [varchar](500) NOT NULL,
	[Accesslevel] [varchar](10) NOT NULL,
	[Lot] [varchar](50) NOT NULL,
	[VIN] [varchar](50) NOT NULL,
	[Year] [varchar](10) NOT NULL,
	[Make] [varchar](50) NOT NULL,
	[Model] [varchar](100) NOT NULL,
	[Series] [varchar](50) NOT NULL,
	[Body] [varchar](50) NOT NULL,
	[Mileage] [varchar](10) NOT NULL,
	[Engine] [varchar](50) NOT NULL,
	[ExteriorColor] [varchar](50) NOT NULL,
	[Options] [varchar](5000) NOT NULL,
	[Condition] [varchar](500) NOT NULL,
	[ImageURL] [varchar](500) NOT NULL,
	[Online] [varchar](10) NOT NULL,
	[VehicleURL] [varchar](500) NOT NULL,
	[BuyNow] [varchar](20) NOT NULL,
	[CurrentBid] [varchar](20) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__IsGood]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__IsGood]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__IsGood]  DEFAULT ((1)) FOR [IsGood]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__AuctionName]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__AuctionName]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__AuctionName]  DEFAULT ('') FOR [AuctionName]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__AuctionState]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__AuctionState]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__AuctionState]  DEFAULT ('') FOR [AuctionState]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__AuctionZip]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__AuctionZip]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__AuctionZip]  DEFAULT ('') FOR [AuctionZip]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__AuctionDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__AuctionDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__AuctionDate]  DEFAULT ('') FOR [AuctionDate]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__AuctionStartTime]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__AuctionStartTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__AuctionStartTime]  DEFAULT ('') FOR [AuctionStartTime]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__AuctionEndTime]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__AuctionEndTime]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__AuctionEndTime]  DEFAULT ('') FOR [AuctionEndTime]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__Sale]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__Sale]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__Sale]  DEFAULT ('') FOR [Sale]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__LaneNumber]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__LaneNumber]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__LaneNumber]  DEFAULT ('') FOR [LaneNumber]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__LaneDescription]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__LaneDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__LaneDescription]  DEFAULT ('') FOR [LaneDescription]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__Seller]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__Seller]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__Seller]  DEFAULT ('') FOR [Seller]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__SellerDescription]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__SellerDescription]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__SellerDescription]  DEFAULT ('') FOR [SellerDescription]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__Accesslevel]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__Accesslevel]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__Accesslevel]  DEFAULT ('') FOR [Accesslevel]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__Lot]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__Lot]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__Lot]  DEFAULT ('') FOR [Lot]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__VIN]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__VIN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__VIN]  DEFAULT ('') FOR [VIN]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__Year]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__Year]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__Year]  DEFAULT ('') FOR [Year]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__Make]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__Make]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__Make]  DEFAULT ('') FOR [Make]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__Model]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__Model]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__Model]  DEFAULT ('') FOR [Model]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__Series]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__Series]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__Series]  DEFAULT ('') FOR [Series]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__Body]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__Body]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__Body]  DEFAULT ('') FOR [Body]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__Mileage]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__Mileage]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__Mileage]  DEFAULT ('') FOR [Mileage]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__Engine]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__Engine]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__Engine]  DEFAULT ('') FOR [Engine]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__ExteriorColor]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__ExteriorColor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__ExteriorColor]  DEFAULT ('') FOR [ExteriorColor]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__Options]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__Options]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__Options]  DEFAULT ('') FOR [Options]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__Condition]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__Condition]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__Condition]  DEFAULT ('') FOR [Condition]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__ImageURL]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__ImageURL]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__ImageURL]  DEFAULT ('') FOR [ImageURL]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__Online]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__Online]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__Online]  DEFAULT ('') FOR [Online]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__VehicleURL]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__VehicleURL]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__VehicleURL]  DEFAULT ('') FOR [VehicleURL]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__BuyNow]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__BuyNow]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__BuyNow]  DEFAULT ('') FOR [BuyNow]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Pipeline__CurrentBid]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Pipeline]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Pipeline__CurrentBid]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Pipeline] ADD  CONSTRAINT [DF_AuctionStaging_Pipeline__CurrentBid]  DEFAULT ('') FOR [CurrentBid]
END


End
GO
