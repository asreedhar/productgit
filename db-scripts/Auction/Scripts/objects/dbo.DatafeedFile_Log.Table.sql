/****** Object:  Table [dbo].[DatafeedFile_Log]    Script Date: 01/22/2014 17:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DatafeedFile_Log]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DatafeedFile_Log](
	[DatafeedFileID] [int] NOT NULL,
	[DatafeedID] [tinyint] NOT NULL,
	[Filename] [varchar](200) NOT NULL,
	[DatafeedStatusCode] [tinyint] NOT NULL,
	[StatusSetAt] [datetime] NOT NULL,
	[Comment] [varchar](500) NOT NULL,
	[LoggedAt] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DatafeedFile_Log]') AND name = N'IX_DatafeedFile_Log__DatafeedFileID')
CREATE NONCLUSTERED INDEX [IX_DatafeedFile_Log__DatafeedFileID] ON [dbo].[DatafeedFile_Log] 
(
	[DatafeedFileID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_DatafeedFile_Log__LoggedAt]') AND parent_object_id = OBJECT_ID(N'[dbo].[DatafeedFile_Log]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_DatafeedFile_Log__LoggedAt]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[DatafeedFile_Log] ADD  CONSTRAINT [DF_DatafeedFile_Log__LoggedAt]  DEFAULT (getdate()) FOR [LoggedAt]
END


End
GO
