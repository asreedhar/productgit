/****** Object:  Table [dbo].[BufferState]    Script Date: 01/22/2014 17:55:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[BufferState]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[BufferState](
	[CurrentBuffer] [tinyint] NOT NULL,
	[LastUpdated] [datetime] NOT NULL,
 CONSTRAINT [PK_BufferState] PRIMARY KEY CLUSTERED 
(
	[CurrentBuffer] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'BufferState', N'COLUMN',N'CurrentBuffer'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 or 1, indicating which set of tables is currently referenced by the “primary” views. There is a primary key constraint on this column, but again there should only ever be one row in this table. (~2009)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BufferState', @level2type=N'COLUMN',@level2name=N'CurrentBuffer'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'BufferState', N'COLUMN',N'LastUpdated'))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date that [Adesa] data was last loaded into the database. (~2009)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BufferState', @level2type=N'COLUMN',@level2name=N'LastUpdated'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'BufferState', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This database incorporates the architecture where there are two sets of tables with identical strucutres and names formatted like <name>#0 and <name>#1. There are also two views titled <name> and <name>_TARGET. Table BufferState is used to track which table set is referenced by (what I call) the “primary” views, and which is referenced by the “target” views.
[At this time, only Adesa data is loaded into the database. As a “fresh” set of data is loaded every time, what we essentially do is delete the old data from the “target” set, load it with fresh data, and then flip the views.]
This table also stores the most recently loaded set of information. [At this time it is keyed to track only Adesa data loads…]
There should only ever be one row present in this table.
An update trigger on this table populates AuctionLoadLog with historical information.
 (~2009)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BufferState'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TableType' , N'SCHEMA',N'dbo', N'TABLE',N'BufferState', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'TableType', @value=N'Reference' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BufferState'
GO
