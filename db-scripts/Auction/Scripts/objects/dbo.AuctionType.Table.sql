/****** Object:  Table [dbo].[AuctionType]    Script Date: 01/22/2014 17:55:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuctionType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AuctionType](
	[AuctionTypeCode] [tinyint] NOT NULL,
	[Description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_AuctionType] PRIMARY KEY CLUSTERED 
(
	[AuctionTypeCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'AuctionType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A lookup table for auction types. Currently we’ve got Open, Closed, and Unknown, but others could be added if and as necessary. [At this time it is not known how to associate closed auctions with dealers eligible to participate in them; depending, auction type may be one way to implement this.] (~2009)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuctionType'
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TableType' , N'SCHEMA',N'dbo', N'TABLE',N'AuctionType', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'TableType', @value=N'Reference' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuctionType'
GO
