/****** Object:  Table [dbo].[ExcludedConsignor_Adesa]    Script Date: 01/22/2014 17:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ExcludedConsignor_Adesa]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ExcludedConsignor_Adesa](
	[Consignor] [varchar](500) NOT NULL,
	[Category] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ExcludedConsignor_Adesa] PRIMARY KEY CLUSTERED 
(
	[Consignor] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TableType' , N'SCHEMA',N'dbo', N'TABLE',N'ExcludedConsignor_Adesa', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'TableType', @value=N'Reference' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExcludedConsignor_Adesa'
GO
