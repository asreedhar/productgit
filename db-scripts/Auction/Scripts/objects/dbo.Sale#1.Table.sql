/****** Object:  Table [dbo].[Sale#1]    Script Date: 01/22/2014 17:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Sale#1]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Sale#1](
	[SaleID] [int] IDENTITY(1,1) NOT NULL,
	[LocationID] [smallint] NOT NULL,
	[DateHeld] [smalldatetime] NOT NULL,
	[Title] [varchar](100) NOT NULL,
	[AuctionTypeCode] [tinyint] NOT NULL,
	[CompanySaleIdentifier] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Sale#1] PRIMARY KEY CLUSTERED 
(
	[SaleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Sale#1]') AND name = N'IX_Sale#1__DateHeld')
CREATE NONCLUSTERED INDEX [IX_Sale#1__DateHeld] ON [dbo].[Sale#1] 
(
	[DateHeld] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Sale#1]') AND name = N'IX_Sale#1__LocationID')
CREATE NONCLUSTERED INDEX [IX_Sale#1__LocationID] ON [dbo].[Sale#1] 
(
	[LocationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'Sale#1', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'See <a href="http://qa.firstlook/docs/EntityProperties.asp?EntityID=15330&EntityTypeID=4&IsEntityType=False">Sale#0</a> (~2009)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Sale#1'
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Sale#1__AuctionType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Sale#1]'))
ALTER TABLE [dbo].[Sale#1]  WITH CHECK ADD  CONSTRAINT [FK_Sale#1__AuctionType] FOREIGN KEY([AuctionTypeCode])
REFERENCES [dbo].[AuctionType] ([AuctionTypeCode])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Sale#1__AuctionType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Sale#1]'))
ALTER TABLE [dbo].[Sale#1] CHECK CONSTRAINT [FK_Sale#1__AuctionType]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Sale#1__Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[Sale#1]'))
ALTER TABLE [dbo].[Sale#1]  WITH CHECK ADD  CONSTRAINT [FK_Sale#1__Location] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Location] ([LocationID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Sale#1__Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[Sale#1]'))
ALTER TABLE [dbo].[Sale#1] CHECK CONSTRAINT [FK_Sale#1__Location]
GO
