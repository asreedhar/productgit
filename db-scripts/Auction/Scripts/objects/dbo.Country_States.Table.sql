/****** Object:  Table [dbo].[Country_States]    Script Date: 01/22/2014 17:55:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Country_States]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Country_States](
	[CountryCode] [char](2) NOT NULL,
	[Country] [varchar](50) NULL,
	[StateCode] [char](2) NOT NULL,
	[State] [varchar](50) NULL,
 CONSTRAINT [PK_Country_States] PRIMARY KEY CLUSTERED 
(
	[StateCode] ASC,
	[CountryCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'TableType' , N'SCHEMA',N'dbo', N'TABLE',N'Country_States', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'TableType', @value=N'Reference' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Country_States'
GO
