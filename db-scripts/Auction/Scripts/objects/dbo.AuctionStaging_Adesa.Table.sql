/****** Object:  Table [dbo].[AuctionStaging_Adesa]    Script Date: 01/22/2014 17:55:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AuctionStaging_Adesa](
	[StagingID] [int] IDENTITY(1,1) NOT NULL,
	[IsGood] [bit] NOT NULL,
	[SaleID] [int] NULL,
	[SaleVehicleID] [int] NULL,
	[GroupingDescriptionID] [int] NULL,
	[MakeModelGroupingID] [int] NULL,
	[AMSSaleID] [varchar](20) NOT NULL,
	[SaleDate] [varchar](20) NOT NULL,
	[SaleType] [varchar](50) NOT NULL,
	[OwnerID] [varchar](20) NOT NULL,
	[ConsignorType] [varchar](10) NOT NULL,
	[Consignor] [varchar](500) NOT NULL,
	[LocationID] [varchar](20) NOT NULL,
	[Location] [varchar](100) NOT NULL,
	[Lot] [varchar](10) NOT NULL,
	[RunNumber] [varchar](10) NOT NULL,
	[Year] [varchar](10) NOT NULL,
	[Make] [varchar](50) NOT NULL,
	[Model] [varchar](100) NOT NULL,
	[Body] [varchar](100) NOT NULL,
	[Series] [varchar](100) NOT NULL,
	[Engine] [varchar](100) NOT NULL,
	[Color] [varchar](100) NOT NULL,
	[Doors] [varchar](10) NOT NULL,
	[Odometer] [varchar](10) NOT NULL,
	[M_K] [varchar](10) NOT NULL,
	[VIN] [varchar](50) NOT NULL,
	[OpenSale] [varchar](10) NOT NULL,
	[TransmissionDescription] [varchar](100) NOT NULL,
	[AMSOptions] [varchar](2000) NOT NULL,
	[AMSAnnouncements] [varchar](2000) NOT NULL,
	[UVCCode] [varchar](20) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
IF NOT EXISTS (SELECT * FROM ::fn_listextendedproperty(N'MS_Description' , N'SCHEMA',N'dbo', N'TABLE',N'AuctionStaging_Adesa', NULL,NULL))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Data from Adesa files is loaded into this table using the bulk insert command. [Additional tables, in the format “AuctionStaging_<company>”, may be added if and when other auction data is loaded into this databse.]
The first few columns are for internal use; the rest are varchars intended to be more than long enough to accommodate any reasonable value that may be found within the file.
View vAdesaImport is defined on this table, covering only the Adesa rows, and is used by the bulk insert command.
There are no indexes or keys on this table.
 (~2009)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'AuctionStaging_Adesa'
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__IsGood]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__IsGood]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__IsGood]  DEFAULT (1) FOR [IsGood]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__AMSSaleID]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__AMSSaleID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__AMSSaleID]  DEFAULT ('') FOR [AMSSaleID]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__SaleDate]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__SaleDate]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__SaleDate]  DEFAULT ('') FOR [SaleDate]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__SaleType]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__SaleType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__SaleType]  DEFAULT ('') FOR [SaleType]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__OwnerID]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__OwnerID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__OwnerID]  DEFAULT ('') FOR [OwnerID]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__ConsignorType]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__ConsignorType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__ConsignorType]  DEFAULT ('') FOR [ConsignorType]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__Consignor]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__Consignor]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__Consignor]  DEFAULT ('') FOR [Consignor]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__LocationID]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__LocationID]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__LocationID]  DEFAULT ('') FOR [LocationID]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__Location]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__Location]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__Location]  DEFAULT ('') FOR [Location]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__Lot]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__Lot]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__Lot]  DEFAULT ('') FOR [Lot]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__RunNumber]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__RunNumber]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__RunNumber]  DEFAULT ('') FOR [RunNumber]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__Year]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__Year]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__Year]  DEFAULT ('') FOR [Year]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__Make]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__Make]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__Make]  DEFAULT ('') FOR [Make]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__Model]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__Model]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__Model]  DEFAULT ('') FOR [Model]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__Body]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__Body]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__Body]  DEFAULT ('') FOR [Body]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__Series]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__Series]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__Series]  DEFAULT ('') FOR [Series]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__Engine]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__Engine]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__Engine]  DEFAULT ('') FOR [Engine]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__Color]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__Color]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__Color]  DEFAULT ('') FOR [Color]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__Doors]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__Doors]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__Doors]  DEFAULT ('') FOR [Doors]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__Odometer]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__Odometer]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__Odometer]  DEFAULT ('') FOR [Odometer]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__M_K]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__M_K]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__M_K]  DEFAULT ('') FOR [M_K]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__VIN]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__VIN]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__VIN]  DEFAULT ('') FOR [VIN]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__OpenSale]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__OpenSale]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__OpenSale]  DEFAULT ('') FOR [OpenSale]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__TransmissionDesc]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__TransmissionDesc]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__TransmissionDesc]  DEFAULT ('') FOR [TransmissionDescription]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__AMSOptions]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__AMSOptions]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__AMSOptions]  DEFAULT ('') FOR [AMSOptions]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__AMSAnnouncements]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__AMSAnnouncements]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__AMSAnnouncements]  DEFAULT ('') FOR [AMSAnnouncements]
END


End
GO
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_AuctionStaging_Adesa__UVCCode]') AND parent_object_id = OBJECT_ID(N'[dbo].[AuctionStaging_Adesa]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_AuctionStaging_Adesa__UVCCode]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[AuctionStaging_Adesa] ADD  CONSTRAINT [DF_AuctionStaging_Adesa__UVCCode]  DEFAULT ('') FOR [UVCCode]
END


End
GO
