 
ALTER TABLE dbo.AuctionStaging_ColumbusFair DROP CONSTRAINT [DF_AuctionStaging_ColumbusFair__Lane_Num]
GO
ALTER TABLE dbo.AuctionStaging_ColumbusFair DROP COLUMN Lane_Num
GO
ALTER TABLE dbo.AuctionRollback_ColumbusFair DROP COLUMN Lane_Num
GO