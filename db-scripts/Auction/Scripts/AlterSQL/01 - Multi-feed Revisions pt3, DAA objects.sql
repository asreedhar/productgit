/*

Create the tables and static data required by DAA
 - Table AuctionStaging_DAA
 - Table AuctionRollback_DAA
 - Data in tables Datafeed, Company, and Location

*/


--  Create table AuctionStaging_DAA
CREATE TABLE AuctionStaging_DAA
 (
   StagingID              int           not null  identity(1,1)
  ,IsGood                 bit           not null
    constraint DF_AuctionStaging_DAA__IsGood
     default 1
  ,SaleID                 int           null
  ,SaleVehicleID          int           null
  ,GroupingDescriptionID  int           null
  ,OptionList             varchar(500)  null
   --  DAA import columns; the columns above are not included in the BULK INSERT view
  ,AuctionName            varchar(250)  not null
    constraint DF_AuctionStaging_DAA__AuctionName
     default ''
  ,AuctionState           varchar(10)   not null
    constraint DF_AuctionStaging_DAA__AuctionState
     default ''
  ,AuctionZip             varchar(10)   not null
    constraint DF_AuctionStaging_DAA__AuctionZip
     default ''
  ,AuctionDate            varchar(50)   not null
    constraint DF_AuctionStaging_DAA__AuctionDate
     default ''
  ,AuctionStartTime       varchar(50)   not null
    constraint DF_AuctionStaging_DAA__AuctionStartTime
     default ''
  ,AuctionEndTime         varchar(50)   not null
    constraint DF_AuctionStaging_DAA__AuctionEndTime
     default ''
  ,Sale                   varchar(100)  not null
    constraint DF_AuctionStaging_DAA__Sale
     default ''
  ,LaneNumber             varchar(50)   not null
    constraint DF_AuctionStaging_DAA__LaneNumber
     default ''
  ,LaneDescription        varchar(500)  not null
    constraint DF_AuctionStaging_DAA__LaneDescription
     default ''
  ,Seller                 varchar(500)  not null
    constraint DF_AuctionStaging_DAA__Seller
     default ''
  ,SellerDescription      varchar(500)  not null
    constraint DF_AuctionStaging_DAA__SellerDescription
     default ''
  ,Accesslevel            varchar(10)   not null
    constraint DF_AuctionStaging_DAA__Accesslevel
     default ''
  ,Lot                    varchar(20)   not null
    constraint DF_AuctionStaging_DAA__Lot
     default ''
  ,VIN                    varchar(50)   not null
    constraint DF_AuctionStaging_DAA__VIN
     default ''
  ,Year                   varchar(10)   not null
    constraint DF_AuctionStaging_DAA__Year
     default ''
  ,Make                   varchar(50)   not null
    constraint DF_AuctionStaging_DAA__Make
     default ''
  ,Model                  varchar(50)   not null
    constraint DF_AuctionStaging_DAA__Model
     default ''
  ,Series                 varchar(50)   not null
    constraint DF_AuctionStaging_DAA__Series
     default ''
  ,Body                   varchar(50)   not null
    constraint DF_AuctionStaging_DAA__Body
     default ''
  ,Mileage                varchar(10)   not null
    constraint DF_AuctionStaging_DAA__Mileage
     default ''
  ,Engine                 varchar(50)   not null
    constraint DF_AuctionStaging_DAA__Engine
     default ''
  ,ExteriorColor          varchar(50)   not null
    constraint DF_AuctionStaging_DAA__ExteriorColor
     default ''
  ,Options                varchar(5000)  not null
    constraint DF_AuctionStaging_DAA__Options
     default ''
  ,Condition              varchar(500)  not null
    constraint DF_AuctionStaging_DAA__Condition
     default ''
  ,ImageURL               varchar(500)  not null
    constraint DF_AuctionStaging_DAA__ImageURL
     default ''
  ,Online                 varchar(10)   not null
    constraint DF_AuctionStaging_DAA__Online
     default ''
  ,VehicleURL             varchar(500)  not null
    constraint DF_AuctionStaging_DAA__VehicleURL
     default ''
  ,BuyNow                 varchar(20)   not null
    constraint DF_AuctionStaging_DAA__BuyNow
     default ''
  ,CurrentBid             varchar(20)   not null
    constraint DF_AuctionStaging_DAA__CurrentBid
     default ''
 )


--  Create table AuctionRollback_DAA
CREATE TABLE AuctionRollback_DAA
 (
   StagingID              int           not null
  ,IsGood                 bit           not null
  ,SaleID                 int           null
  ,SaleVehicleID          int           null
  ,GroupingDescriptionID  int           null
  ,OptionList             varchar(500)  null
  ,AuctionName            varchar(250)  not null
  ,AuctionState           varchar(10)   not null
  ,AuctionZip             varchar(10)   not null
  ,AuctionDate            varchar(50)   not null
  ,AuctionStartTime       varchar(50)   not null
  ,AuctionEndTime         varchar(50)   not null
  ,Sale                   varchar(100)  not null
  ,LaneNumber             varchar(50)   not null
  ,LaneDescription        varchar(500)  not null
  ,Seller                 varchar(500)  not null
  ,SellerDescription      varchar(500)  not null
  ,Accesslevel            varchar(10)   not null
  ,Lot                    varchar(20)   not null
  ,VIN                    varchar(50)   not null
  ,Year                   varchar(10)   not null
  ,Make                   varchar(50)   not null
  ,Model                  varchar(50)   not null
  ,Series                 varchar(50)   not null
  ,Body                   varchar(50)   not null
  ,Mileage                varchar(10)   not null
  ,Engine                 varchar(50)   not null
  ,ExteriorColor          varchar(50)   not null
  ,Options                varchar(5000)  not null
  ,Condition              varchar(500)  not null
  ,ImageURL               varchar(500)  not null
  ,Online                 varchar(10)   not null
  ,VehicleURL             varchar(500)  not null
  ,BuyNow                 varchar(20)   not null
  ,CurrentBid             varchar(20)   not null
 )


/*

Load static data to (new) Auction tables

*/

SET NOCOUNT on

--  Configure known Auction datafeeds
INSERT Datafeed (DatafeedID, DatafeedCode, Name, LoadProcedure, RollbackProcedure, IsActive)
 values (21, 'DAA', 'Dealers Auto Auction', 'LoadData_DAA', 'RollbackStagedData_DAA', 1)


--  Add to Company table
INSERT Company (CompanyID, Name)
 values (2, 'Dealers Auto Auction')


--  Add all location for this company
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (2, 'Dealers Auto Auction of Oklahoma City', 'Oklahoma City', 'OK', '73108', 'OKC')
GO
