/*

Create the tables used for multiple feeds into the "Future Auctions" database
Add static data to tables marked with *

 - AuctionRollback_Adesa
 - Datafeed *
 - DatafeedStatus * 
 - DatafeedFile (with trigger)
 - DatafeedFile_Log * 
 - DataloadEngineStatus * (with trigger)

*/

SET NOCOUNT on

--  Table AuctionStaging_Adesa already exists


--  Create table AuctionRollback_Adesa
CREATE TABLE AuctionRollback_Adesa
 (
   StagingID         int            not null
  ,IsGood            bit            not null
  ,SaleID            int            null
  ,SaleVehicleID     int            null
  ,GroupingDescriptionID  int       null
  ,AMSSaleID         varchar(20)    not null
  ,SaleDate          varchar(20)    not null
  ,SaleType          varchar(50)    not null
  ,OwnerID           varchar(20)    not null
  ,ConsignorType     varchar(10)    not null
  ,Consignor         varchar(500)   not null
  ,LocationID        varchar(20)    not null
  ,Location          varchar(100)   not null
  ,Lot               varchar(10)    not null
  ,RunNumber         varchar(10)    not null
  ,Year              varchar(10)    not null
  ,Make              varchar(50)    not null
  ,Model             varchar(100)   not null
  ,Body              varchar(100)   not null
  ,Series            varchar(100)   not null
  ,Engine            varchar(100)   not null
  ,Color             varchar(100)   not null
  ,Doors             varchar(10)    not null
  ,Odometer          varchar(10)    not null
  ,M_K               varchar(10)    not null
  ,VIN               varchar(50)    not null
  ,OpenSale          varchar(10)    not null
  ,TransmissionDescription  varchar(100)   not null
  ,AMSOptions        varchar(2000)  not null
  ,AMSAnnouncements  varchar(2000)  not null
  ,UVCCode           varchar(20)    not null
 )


--  Create table Datafeed
CREATE TABLE Datafeed
 (
   DatafeedID         tinyint       not null
    constraint PK_Datafeed
     primary key clustered
  ,DatafeedCode       varchar(20)   not null
    constraint UQ_Datafeed__DatafeedCode
     unique nonclustered
  ,Name               varchar(100)  not null
  ,IsActive           bit           not null
  ,LoadProcedure      sysname       not null
  ,RollbackProcedure  sysname       not null
 )


--  Create table DatafeedStatus
CREATE TABLE DatafeedStatus
 (
   DatafeedStatusCode  tinyint       not null
    constraint PK_DatafeedStatus
     primary key clustered
  ,Description         varchar(200)  not null
 )


--  Create table DatafeedFile
CREATE TABLE DatafeedFile
 (
   DatafeedFileID      int           not null  identity(1,1)
    constraint PK_DatafeedFile
     primary key clustered
  ,DatafeedID          tinyint       not null
    constraint FK_DatafeedFile__Datafeed
     foreign key references Datafeed (DatafeedID)
  ,Filename            varchar(200)  not null
  ,DatafeedStatusCode  tinyint       not null
    constraint FK_DatafeedFile__DatafeedStatus
     foreign key references DatafeedStatus (DatafeedStatusCode)
  ,StatusSetAt         datetime      not null
  ,Comment             varchar(500)  not null
 )
CREATE nonclustered INDEX IX_DatafeedFile__DatafeedStatusCode
 on DatafeedFile (DatafeedStatusCode)
CREATE nonclustered INDEX IX_DatafeedFile__Filename
 on DatafeedFile (Filename)


--  Create table DatafeedFile_Log
CREATE TABLE DatafeedFile_Log
 (
   DatafeedFileID      int           not null
  ,DatafeedID          tinyint       not null
  ,Filename            varchar(200)  not null
  ,DatafeedStatusCode  tinyint       not null
  ,StatusSetAt         datetime      not null
  ,Comment             varchar(500)  not null
  ,LoggedAt            datetime      not null
    constraint DF_DatafeedFile_Log__LoggedAt
     default CURRENT_TIMESTAMP
 )
CREATE nonclustered INDEX IX_DatafeedFile_Log__DatafeedFileID
 on DatafeedFile_Log (DatafeedFileID)
CREATE nonclustered INDEX IX_DatafeedFile_Log__Filename
 on DatafeedFile_Log (Filename)


--  Create table DataloadEngineStatus
CREATE TABLE DataloadEngineStatus
 (
   LockState  varchar(20)  not null
    constraint PK_DataloadEngineStatus
     primary key clustered
 )

INSERT DataloadEngineStatus (LockState) values ('Unlocked')

GO
/******************************************************************************
**
**  Trigger: TR_iu_DatafeedFile
**  Description: All data written to the DatafeedFile table is simultaneously logged
**   in the DatafeedFile_Log table.
**        
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  05/23/2006  PKelley   Trigger created.
**  
*******************************************************************************/
CREATE TRIGGER TR_iu_DatafeedFile
 on DatafeedFile
 for insert, update
 as
    INSERT DatafeedFile_Log
      (
        DatafeedFileID
       ,DatafeedID
       ,FileName
       ,DatafeedStatusCode
       ,StatusSetAt
       ,Comment
      )
     select
        DatafeedFileID
       ,DatafeedID
       ,FileName
       ,DatafeedStatusCode
       ,StatusSetAt
       ,Comment
      from inserted

GO
/******************************************************************************
**
**  Trigger: TR_id_DataloadEngineStatus__OnlyOneRow
**  Description: There must be one and only one row in this table
**        
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  02/15/2006  PKelley   Trigger created.
**  
*******************************************************************************/
CREATE TRIGGER TR_id_DataLoadEngineStatus__OnlyOneRow
 on DataLoadEngineStatus
 for insert, delete

AS

    RAISERROR('There can be one and only one row in table DataLoadEngineStatus', 11, 1)

GO


/*

Load static data to (new) Auction tables

*/

SET NOCOUNT on

--  Configure known Auction datafeeds
INSERT Datafeed (DatafeedID, DatafeedCode, Name, LoadProcedure, RollbackProcedure, IsActive)
 values (14, 'Adesa', 'Adesa Auto Exchange', 'LoadData_Adesa', 'RollbackStagedData_Adesa', 1)


--  Status values are roughly similar to values in the ATC system
INSERT DatafeedStatus (DatafeedStatusCode, Description)
 values (10, 'New file to be staged')
INSERT DatafeedStatus (DatafeedStatusCode, Description)
 values (12, 'Old file to be restaged')
INSERT DatafeedStatus (DatafeedStatusCode, Description)
 values (20, 'Data has been staged')
INSERT DatafeedStatus (DatafeedStatusCode, Description)
 values (30, 'Data is active')
INSERT DatafeedStatus (DatafeedStatusCode, Description)
 values (32, 'Data rolled back but then reactivated')
INSERT DatafeedStatus (DatafeedStatusCode, Description)
 values (40, 'Data rolled back')
INSERT DatafeedStatus (DatafeedStatusCode, Description)
 values (50, 'Data deleted from rollback table')
INSERT DatafeedStatus (DatafeedStatusCode, Description)
 values (52, 'Data could not be staged')
INSERT DatafeedStatus (DatafeedStatusCode, Description)
 values (54, 'Data could not be loaded')


--  Load loading history from old table to new
INSERT DatafeedFile_Log
  (DatafeedFileID, DatafeedID, Filename, DatafeedStatusCode, StatusSetAt, Comment)
 select
   0
  ,14
  ,al.FileName
  ,50    --  Mark them all as archived
  ,al.LoadedAt
  ,cast(al.DataRows as varchar(10)) + ' rows loaded, ' + cast(al.ValidRows as varchar(10))+ ' rows validated'
 from AuctionLoadLog al
  left outer join BufferState bs
   on bs.filename = al.filename

GO
