/*

Remove DAA from system
 - Delete "short-term" data
 - Some data will age out of the system over the next few months
 - Down the road, someone gets to go through and purge whatever's left

*/

UPDATE Datafeed
 set IsActive = 0
 where DatafeedCode = 'DAA'


DELETE ValidationFailures
 where DatafeedID = 21

DELETE Location
 where CompanyID = 2

DELETE Company
 where CompanyID = 2

--  Mark files as "off the system"
UPDATE DatafeedFile
 set DatafeedStatusCode = 50
 where DatafeedID = 21
  and DatafeedStatusCode < 50


DROP PROCEDURE StageData_DAA
DROP PROCEDURE ValidateStagedData_DAA
DROP PROCEDURE RollbackStagedData_DAA
DROP PROCEDURE ParseDAAOptions
DROP PROCEDURE LoadData_DAA

DROP VIEW vDAAImport

DROP TABLE AuctionStaging_DAA
DROP TABLE AuctionRollback_DAA
