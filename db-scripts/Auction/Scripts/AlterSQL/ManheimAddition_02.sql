/*

Update ValidationCheck table for now-enabled Simulcast data

*/

SET NOCOUNT on

UPDATE ValidationCheck 
 set
   Description = '<deprecated> ' + Description
  ,Title = left('<deprecated> ' + Title, 50)
 where ValidationCheckID = 1003
