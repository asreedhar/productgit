/*

--  Undo script

DROP SYNONYM dbo.VehicleCatalog

DROP TABLE dbo.Country_States
--DROP TABLE dbo.ManheimLookups

DROP TABLE dbo.ManheimConsignor
DROP TABLE dbo.Stage_ManheimConsignor
DROP TABLE dbo.Stage_ManheimAuction
DROP TABLE dbo.AuctionRollback_Manheim
DROP TABLE dbo.AuctionStaging_Manheim

--  Yo, data's still in there...

*/

SET NOCOUNT on

IF not exists (select 1 from sys.Objects where name = 'VehicleCatalog')
    CREATE SYNONYM dbo.VehicleCatalog
     for IMT.dbo.VehicleCatalog


--  CREATE TABLE dbo.AuctionStaging_Manheim
--
--  Note: Originally there were two columns (FUTUREUSE1 and FUTUREUSE2) at the
--  end of the dataset.  Manheim added three colums in June of 2007 (SPRIMCD,
--  SLCAUSE, CLASS).  We will use none of these columns, and based on these,
--  it seems quite likely that we won't use any other colmns they may add in
--  the future.  So, I've just added column "ExtraFields" after the last used
--  column (Announce), and sized it to accomodate all the current garbage columns
--  (of widths 30, 30, 1, 1, and 1) and hopefully any further such columns.
--  Doing it this way, the schema supports the old, new, and most future file
--  formats.
CREATE TABLE dbo.AuctionStaging_Manheim
 (
   StagingID              int          not null  identity(1,1)
  ,IsGood                 int          not null
    constraint DF_AuctionStaging_Manheim__IsGood
     default (1)
  ,SaleID                 int          null
  ,SaleVehicleID          int          null
  ,GroupingDescriptionID  int          null
  ,MakeModelGroupingID    int          null
  ,DecodedEngine          varchar(50)  null
  ,DecodedTransmission    varchar(50)  null
  ,DecodedBodyType        varchar(50)  null
   --  Manheim import columns; the columns above are not included in the BULK INSERT view
  ,Channel                varchar(15)  not null
  ,Auction                varchar(4)   not null
  ,SaleDate               varchar(10)  not null
  ,SaleYear               varchar(6)   not null
  ,Consign                varchar(10)  not null
  ,ActLane                varchar(4)   not null
  ,Run                    varchar(6)   not null
  ,CarYear                varchar(6)   not null
  ,Make                   varchar(10)  not null
  ,Model                  varchar(15)  not null
  ,Subseries              varchar(10)  not null
  ,VIN                    varchar(17)  not null
  ,IntColor               varchar(30)  not null
  ,ExtColor               varchar(30)  not null
  ,Miles                  varchar(11)  not null
  ,Engine                 varchar(3)   not null
  ,Radio                  varchar(30)  not null
  ,Trans                  varchar(1)   not null
  ,PwrSteer               varchar(1)   not null
  ,PwrBrakes              varchar(1)   not null
  ,AC                     varchar(1)   not null
  ,ElecWndow              varchar(1)   not null
  ,ElecSeat               varchar(1)   not null
  ,ElecLock               varchar(1)   not null
  ,Cruise                 varchar(1)   not null
  ,Interior               varchar(1)   not null
  ,FourWheel              varchar(1)   not null
  ,TltSteer               varchar(1)   not null
  ,Airbag                 varchar(1)   not null
  ,TitleStat              varchar(1)   not null
  ,BodyDesc               varchar(4)   not null
  ,Announce               varchar(30)  not null
  ,ExtraColumns           varchar(200) not null
 )

--  AuctionRollback_Manheim
CREATE TABLE dbo.AuctionRollback_Manheim
 (
   StagingID              int          not null
  ,IsGood                 int          not null
  ,SaleID                 int          null
  ,SaleVehicleID          int          null
  ,GroupingDescriptionID  int          null
  ,MakeModelGroupingID    int          null
  ,DecodedEngine          varchar(50)  null
  ,DecodedTransmission    varchar(50)  null
  ,DecodedBodyType        varchar(50)  null
  ,Channel                varchar(15)  not null
  ,Auction                varchar(4)   not null
  ,SaleDate               varchar(10)  not null
  ,SaleYear               varchar(6)   not null
  ,Consign                varchar(10)  not null
  ,ActLane                varchar(4)   not null
  ,Run                    varchar(6)   not null
  ,CarYear                varchar(6)   not null
  ,Make                   varchar(10)  not null
  ,Model                  varchar(15)  not null
  ,Subseries              varchar(10)  not null
  ,VIN                    varchar(17)  not null
  ,IntColor               varchar(30)  not null
  ,ExtColor               varchar(30)  not null
  ,Miles                  varchar(11)  not null
  ,Engine                 varchar(3)   not null
  ,Radio                  varchar(30)  not null
  ,Trans                  varchar(1)   not null
  ,PwrSteer               varchar(1)   not null
  ,PwrBrakes              varchar(1)   not null
  ,AC                     varchar(1)   not null
  ,ElecWndow              varchar(1)   not null
  ,ElecSeat               varchar(1)   not null
  ,ElecLock               varchar(1)   not null
  ,Cruise                 varchar(1)   not null
  ,Interior               varchar(1)   not null
  ,FourWheel              varchar(1)   not null
  ,TltSteer               varchar(1)   not null
  ,Airbag                 varchar(1)   not null
  ,TitleStat              varchar(1)   not null
  ,BodyDesc               varchar(4)   not null
  ,Announce               varchar(30)  not null
  ,ExtraColumns           varchar(200) not null
 )


--  CREATE TABLE dbo.Stage_ManheimAuction
CREATE TABLE dbo.Stage_ManheimAuction
 (
   DNInit  varchar(4)   not null
  ,DNName  varchar(30)  not null
  ,DNAdd1  varchar(25)  not null
  ,DNCity  varchar(20)  not null
  ,DNStPr  varchar(4)   not null
  ,DNZip   varchar(9)   not null
 )


--  CREATE TABLE dbo.Stage_ManheimConsignor
CREATE TABLE dbo.Stage_ManheimConsignor
 (
   Consignor  varchar(10)   not null
  ,Name       varchar(255)  not null
  ,Zero       varchar(23)   not null
  ,Blank1     varchar(1)    not null
  ,Blank2     varchar(1)    not null
 )


--  ManheimConsignor
CREATE TABLE dbo.ManheimConsignor
 (
   ConsignorCode  varchar(10)   not null
    constraint PK_ManheimConsignor
     primary key clustered
  ,Name           varchar(255)  not null
 )


--  General purpose validation lookup table
CREATE TABLE Country_States
 (
   CountryCode    char(2)
  ,Country        varchar(50)
  ,StateCode      char(2)
  ,State          varchar(50)
  ,constraint PK_Country_States
    primary key clustered (StateCode, CountryCode)
 )


----  Manheim-specific validation lookup table
--CREATE TABLE dbo.ManheimLookups
-- (
--   Category     varchar(10)   not null
--  ,Code         varchar(10)   not null
--  ,Description  varchar(100)  not null
--  ,constraint PK_ManheimLookups
--    primary key clustered (Code, Category)
-- )


--  Configure static data for Manheim  ----------------------------------------

--  Add the datafeed
INSERT Datafeed (DatafeedID, DatafeedCode, Name, LoadProcedure, RollbackProcedure, IsActive)
 values (54, 'Manheim', 'Manheim', 'LoadData_Manheim', 'RollbackStagedData_Manheim', 1)


--  Add to Company table
INSERT Company (CompanyID, Name)
 values(21, 'Manheim')


--  Validation checks
INSERT ValidationCheck (ValidationCheckID, Title, Description)
 values (1003, 'Manheim Simulcast data does not get loaded', 'We do not load Manheim Simulcast data')

INSERT ValidationCheck (ValidationCheckID, Title, Description)
 values (1004, 'Manheim consignor not found', 'Manheim consignor (code) not found in table ManheimConsignor')


--  Populate lookup table
INSERT Country_States
 (CountryCode, Country, StateCode, State)
       select 'US', 'United States', 'AK', 'Alaska'
 union select 'US', 'United States', 'AL', 'Alabama'
 union select 'US', 'United States', 'AR', 'Arkansas'
 union select 'US', 'United States', 'AZ', 'Arizona'
 union select 'US', 'United States', 'CA', 'California'
 union select 'US', 'United States', 'CO', 'Colorado'
 union select 'US', 'United States', 'CT', 'Connecticut'
 union select 'US', 'United States', 'DE', 'Delaware'
 union select 'US', 'United States', 'FL', 'Florida'
 union select 'US', 'United States', 'GA', 'Georgia'
 union select 'US', 'United States', 'HI', 'Hawaii'
 union select 'US', 'United States', 'IA', 'Iowa'
 union select 'US', 'United States', 'ID', 'Idaho'
 union select 'US', 'United States', 'IL', 'Illinois'
 union select 'US', 'United States', 'IN', 'Indiana'
 union select 'US', 'United States', 'KS', 'Kansas'
 union select 'US', 'United States', 'KY', 'Kentucky'
 union select 'US', 'United States', 'LA', 'Louisiana'
 union select 'US', 'United States', 'MA', 'Massachusetts'
 union select 'US', 'United States', 'MD', 'Maryland'
 union select 'US', 'United States', 'ME', 'Maine'
 union select 'US', 'United States', 'MI', 'Michigan'
 union select 'US', 'United States', 'MN', 'Minnesota'
 union select 'US', 'United States', 'MO', 'Missouri'
 union select 'US', 'United States', 'MS', 'Mississippi'
 union select 'US', 'United States', 'MT', 'Montana'
 union select 'US', 'United States', 'NC', 'North Carolina'
 union select 'US', 'United States', 'ND', 'North Dakota'
 union select 'US', 'United States', 'NE', 'Nebraska'
 union select 'US', 'United States', 'NH', 'New Hampshire'
 union select 'US', 'United States', 'NJ', 'New Jersey'
 union select 'US', 'United States', 'NM', 'New Mexico'
 union select 'US', 'United States', 'NV', 'Nevada'
 union select 'US', 'United States', 'NY', 'New York'
 union select 'US', 'United States', 'OH', 'Ohio'
 union select 'US', 'United States', 'OK', 'Oklahoma'
 union select 'US', 'United States', 'OR', 'Oregon'
 union select 'US', 'United States', 'PA', 'Pennsylvania'
 union select 'US', 'United States', 'RI', 'Rhode Island'
 union select 'US', 'United States', 'SC', 'South Carolina'
 union select 'US', 'United States', 'SD', 'South Dakota'
 union select 'US', 'United States', 'TN', 'Tennessee'
 union select 'US', 'United States', 'TX', 'Texas'
 union select 'US', 'United States', 'UT', 'Utah'
 union select 'US', 'United States', 'VA', 'Virginia'
 union select 'US', 'United States', 'VT', 'Vermont'
 union select 'US', 'United States', 'WA', 'Washington'
 union select 'US', 'United States', 'WI', 'Wisconsin'
 union select 'US', 'United States', 'WV', 'West Virginia'
 union select 'US', 'United States', 'WY', 'Wyoming'


--INSERT ManheimLookups (Category, Code, Description)
--       select 'Engine', '4',   '4-Cylinder'
-- union select 'Engine', '8',   '8-Cylinder'
-- union select 'Engine', '10',  '10-Cylinder'
-- union select 'Engine', '12',  '12-Cylinder'
-- union select 'Engine', '10G', '10-Cylinder Gas'
-- union select 'Engine', '12G', '12-Cylinder Gas'
-- union select 'Engine', '1G',  '1-Cylinder Gas'
-- union select 'Engine', '2G',  '2-Cylinder Gas'
-- union select 'Engine', '3G',  '3-Cylinder Gas'
-- union select 'Engine', '3GT', '3-Cylinder Gas Turbo'
-- union select 'Engine', '3P',  '3-Cylinder Propane'
-- union select 'Engine', '4D',  '4-Cylinder Diesel'
-- union select 'Engine', '4DT', '4-Cylinder Diesel Turbo'
-- union select 'Engine', '4F',  '4-Cylinder Fossil Fuel'
-- union select 'Engine', '4G',  '4-Cylinder Gas'
-- union select 'Engine', '4GT', '4-Cylinder Gas Turbo'
-- union select 'Engine', '4P',  '4-Cylinder Propane'
-- union select 'Engine', '5D',  '5-Cylinder Diesel'
-- union select 'Engine', '5DT', '5-Cylinder Diesel Turbo'
-- union select 'Engine', '5F',  '5-Cylinder Fossil Fuel'
-- union select 'Engine', '5G',  '5-Cylinder Gas'
-- union select 'Engine', '5GT', '5-Cylinder Gas Turbo'
-- union select 'Engine', '6C',  '6-Cylinder'
-- union select 'Engine', '6D',  '6-Cylinder Diesel'
-- union select 'Engine', '6DT', '6-Cylinder Diesel Turbo'
-- union select 'Engine', '6F',  '6-Cylinder Fossil Fuel'
-- union select 'Engine', '6G',  '6-Cylinder Gas'
-- union select 'Engine', '6GT', '6-Cylinder Gas Turbo'
-- union select 'Engine', '6P',  '6-Cylinder Propane'
-- union select 'Engine', '8D',  '8-Cylinder Diesel'
-- union select 'Engine', '8DT', '8-Cylinder Diesel Turbo'
-- union select 'Engine', '8DT', '8-Cylinder Diesel Turbo'
-- union select 'Engine', '8F',  '8-Cylinder Fossil Fuel'
-- union select 'Engine', '8F',  '8-Cylinder Fossil Fuel'
-- union select 'Engine', '8G',  '8-Cylinder Gas'
-- union select 'Engine', '8G',  '8-Cylinder Gas'
-- union select 'Engine', '8GT', '8-Cylinder Gas Turbo'
-- union select 'Engine', '8GT', '8-Cylinder Gas Turbo'
-- union select 'Engine', '8P',  '8-Cylinder Propane'
-- union select 'Engine', '8P',  '8-Cylinder Propane'
-- union select 'Engine', 'EG',  'Electric/Gas'
-- union select 'Engine', 'EL',  'Electric'
-- union select 'Engine', 'G',   'Gas Engine'
-- union select 'Engine', 'G6',  '6-Cylinder Gas'
-- union select 'Engine', 'L3G', 'In-Line 3-Cylinder Gas'
-- union select 'Engine', 'L4G', 'In-Line 4-Cylinder Gas'
-- union select 'Engine', 'RT',  'Rotary Engine'
-- union select 'Engine', 'V4',  'V4'
-- union select 'Engine', 'V6',  'V6'
-- union select 'Engine', 'V6G', 'V6 Gas'
-- union select 'Engine', 'V8',  'V8'
-- union select 'Engine', 'V8D', 'V8 Diesel'

GO
