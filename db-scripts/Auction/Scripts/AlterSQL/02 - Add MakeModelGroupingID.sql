/*

IMPORTANT!  REVIEW ALL OF THIS BEFORE ROLLING OUT THE CODE!


1) Disable the load jobs while doing this work.


2) This process will wipe the staging and rollback tables of data.  This will cause the next LOAD run
to load data ONLY for the initiating datafeed, with no other data available for the other datafeeds.
The data will appear over time, as datafeeds are run.

To avoid this, AFTER deploying these changes but BEFORE the next load run, the most recently loaded
files should be retrieved from archive and re-staged (copy to staging folder, use this

    SELECT
       df.Name
      ,dff.FileName
      ,dff.DatafeedStatusCode Code
      ,ds.Description
     from datafeedFile dff
      inner join Datafeed df
       on df.DatafeedID = dff.DatafeedID
      inner join DatafeedStatus ds
       on ds.DatafeedStatusCode = dff.DatafeedStatusCode
     where dff.datafeedstatusCode = 30
     order by df.Name, dff.DatafeedStatusCode

to identify the files to load and run in this script to restage the data:

    USE Auction
    EXECUTE StageData_Adesa 'runlistca_whatever.csv', 14, null
    EXECUTE StageData_DAA   'DAAOKC_whatever.txt', 21, null

Don't forget that the Adesa file needs to be properly named.


3) If there is any existing data for which MakeModelGroupingID CANNOT be set, this process will FAIL.
Use the following to clear out any such entries... once you've figured out how and why there are any,
of course.

    DECLARE @WhackEm table (BadID  int  not null)
    
    INSERT @WhackEm (BadID)
     --  Run from here down to check the scope of the problem (if any)
     select sv.SaleVehicleID
      from SaleVehicle#0 sv
       inner join SaleVehicleCompanyData_Adesa#0 svx
        on svx.SaleVehicleID = sv.SaleVehicleID
       left outer join IMT..DecodedSquishVins dsv
        on dsv.squish_vin = IMT.dbo.fn_squishvin(svx.VIN)
       left outer join IMT..tbl_MakeModelGrouping mmg
        on mmg.MakeModelGroupingID = dsv.MakeModelGroupingID
      where mmg.MakeModelGroupingID is null
     union select sv.SaleVehicleID
      from SaleVehicle#1 sv
       inner join SaleVehicleCompanyData_Adesa#1 svx
        on svx.SaleVehicleID = sv.SaleVehicleID
       left outer join IMT..DecodedSquishVins dsv
        on dsv.squish_vin = IMT.dbo.fn_squishvin(svx.VIN)
       left outer join IMT..tbl_MakeModelGrouping mmg
        on mmg.MakeModelGroupingID = dsv.MakeModelGroupingID
      where mmg.MakeModelGroupingID is null
    
    
    DELETE SaleVehicleCompanyData_Adesa#0
     where SaleVehicleID in (select BadID from @WhackEm)

    DELETE SaleVehicleCompanyData_Adesa#1
     where SaleVehicleID in (select BadID from @WhackEm)

    DELETE SaleVehicle#0
     where SaleVehicleID in (select BadID from @WhackEm)

    DELETE SaleVehicle#1
     where SaleVehicleID in (select BadID from @WhackEm)

*/

SET NOCOUNT on

IF (select count(*)
     from SaleVehicle#0 sv
      inner join SaleVehicleCompanyData_Adesa#0 svx
       on svx.SaleVehicleID = sv.SaleVehicleID
      left outer join IMT..DecodedSquishVins dsv
       on dsv.squish_vin = IMT.dbo.fn_squishvin(svx.VIN)
      left outer join IMT..tbl_MakeModelGrouping mmg
       on mmg.MakeModelGroupingID = dsv.MakeModelGroupingID
     where mmg.MakeModelGroupingID is null) > 0
    RAISERROR('Unable to resolve MakeManagementGroupingIDs for existing data, manual fix required', 20, 1) with log

IF (select count(*)
     from SaleVehicle#1 sv
      inner join SaleVehicleCompanyData_Adesa#0 svx
       on svx.SaleVehicleID = sv.SaleVehicleID
      left outer join IMT..DecodedSquishVins dsv
       on dsv.squish_vin = IMT.dbo.fn_squishvin(svx.VIN)
      left outer join IMT..tbl_MakeModelGrouping mmg
       on mmg.MakeModelGroupingID = dsv.MakeModelGroupingID
     where mmg.MakeModelGroupingID is null) > 0
    RAISERROR('Unable to resolve MakeManagementGroupingIDs for existing data, manual fix required', 20, 1) with log



--  I don't know how this came about, but apparently it's off here and there...
UPDATE DatafeedStatus
 set DatafeedStatusCode = 11
 where DatafeedStatusCode = 12


--  Add column to "live" tables  --------------------------------------------------------
ALTER TABLE SaleVehicle#0
 add MakeModelGroupingID  int  null
GO

UPDATE SaleVehicle#0
 set MakeModelGroupingID = dsv.MakeModelGroupingID
 from SaleVehicle#0 sv
  inner join SaleVehicleCompanyData_Adesa#0 svx
   on svx.SaleVehicleID = sv.SaleVehicleID
  inner join IMT..DecodedSquishVins dsv
   on dsv.squish_vin = IMT.dbo.fn_squishvin(svx.VIN)

ALTER TABLE SaleVehicle#0
 alter column MakeModelGroupingID  int  not null

--  Second set
ALTER TABLE SaleVehicle#1
 add MakeModelGroupingID  int  null
GO

UPDATE SaleVehicle#1
 set MakeModelGroupingID = dsv.MakeModelGroupingID
 from SaleVehicle#1 sv
  inner join SaleVehicleCompanyData_Adesa#1 svx
   on svx.SaleVehicleID = sv.SaleVehicleID
  inner join IMT..DecodedSquishVins dsv
   on dsv.squish_vin = IMT.dbo.fn_squishvin(svx.VIN)

ALTER TABLE SaleVehicle#1
 alter column MakeModelGroupingID  int  not null

--  Recreate the views for these tables, as they reference the new column
DECLARE @CurrentBuffer tinyint

SELECT @CurrentBuffer = CurrentBuffer
 from BufferState

EXECUTE CreateBufferViews 'SaleVehicle', '*', @CurrentBuffer, null
GO


--  Add column to staging tables  -------------------------------------------------------

--  Avoid hypothetical problems by dropping and recreate the Bulk Insert views.  Note
--  that these views are not actually changing
DROP VIEW vAdesaImport
DROP VIEW vDAAImport

DROP TABLE AuctionStaging_Adesa
DROP TABLE AuctionRollback_Adesa
DROP TABLE AuctionStaging_DAA
DROP TABLE AuctionRollback_DAA

GO

CREATE TABLE AuctionStaging_Adesa (
	StagingID int IDENTITY (1, 1) NOT NULL ,
	IsGood bit NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__IsGood DEFAULT (1),
	SaleID int NULL ,
	SaleVehicleID int NULL ,
	GroupingDescriptionID int NULL ,
    MakeModelGroupingID int NULL ,
	AMSSaleID varchar (20) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__AMSSaleID DEFAULT (''),
	SaleDate varchar (20) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__SaleDate DEFAULT (''),
	SaleType varchar (50) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__SaleType DEFAULT (''),
	OwnerID varchar (20) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__OwnerID DEFAULT (''),
	ConsignorType varchar (10) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__ConsignorType DEFAULT (''),
	Consignor varchar (500) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__Consignor DEFAULT (''),
	LocationID varchar (20) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__LocationID DEFAULT (''),
	Location varchar (100) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__Location DEFAULT (''),
	Lot varchar (10) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__Lot DEFAULT (''),
	RunNumber varchar (10) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__RunNumber DEFAULT (''),
	Year varchar (10) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__Year DEFAULT (''),
	Make varchar (50) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__Make DEFAULT (''),
	Model varchar (100) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__Model DEFAULT (''),
	Body varchar (100) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__Body DEFAULT (''),
	Series varchar (100) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__Series DEFAULT (''),
	Engine varchar (100) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__Engine DEFAULT (''),
	Color varchar (100) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__Color DEFAULT (''),
	Doors varchar (10) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__Doors DEFAULT (''),
	Odometer varchar (10) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__Odometer DEFAULT (''),
	M_K varchar (10) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__M_K DEFAULT (''),
	VIN varchar (50) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__VIN DEFAULT (''),
	OpenSale varchar (10) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__OpenSale DEFAULT (''),
	TransmissionDescription varchar (100) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__TransmissionDesc DEFAULT (''),
	AMSOptions varchar (2000) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__AMSOptions DEFAULT (''),
	AMSAnnouncements varchar (2000) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__AMSAnnouncements DEFAULT (''),
	UVCCode varchar (20) NOT NULL CONSTRAINT DF_AuctionStaging_Adesa__UVCCode DEFAULT ('')
)


CREATE TABLE AuctionRollback_Adesa
 (
   StagingID         int            not null
  ,IsGood            bit            not null
  ,SaleID            int            null
  ,SaleVehicleID     int            null
  ,GroupingDescriptionID  int       null
  ,MakeModelGroupingID    int       null
  ,AMSSaleID         varchar(20)    not null
  ,SaleDate          varchar(20)    not null
  ,SaleType          varchar(50)    not null
  ,OwnerID           varchar(20)    not null
  ,ConsignorType     varchar(10)    not null
  ,Consignor         varchar(500)   not null
  ,LocationID        varchar(20)    not null
  ,Location          varchar(100)   not null
  ,Lot               varchar(10)    not null
  ,RunNumber         varchar(10)    not null
  ,Year              varchar(10)    not null
  ,Make              varchar(50)    not null
  ,Model             varchar(100)   not null
  ,Body              varchar(100)   not null
  ,Series            varchar(100)   not null
  ,Engine            varchar(100)   not null
  ,Color             varchar(100)   not null
  ,Doors             varchar(10)    not null
  ,Odometer          varchar(10)    not null
  ,M_K               varchar(10)    not null
  ,VIN               varchar(50)    not null
  ,OpenSale          varchar(10)    not null
  ,TransmissionDescription  varchar(100)   not null
  ,AMSOptions        varchar(2000)  not null
  ,AMSAnnouncements  varchar(2000)  not null
  ,UVCCode           varchar(20)    not null
 )


--  Create table AuctionStaging_DAA
CREATE TABLE AuctionStaging_DAA
 (
   StagingID              int           not null  identity(1,1)
  ,IsGood                 bit           not null
    constraint DF_AuctionStaging_DAA__IsGood
     default 1
  ,SaleID                 int           null
  ,SaleVehicleID          int           null
  ,GroupingDescriptionID  int           null
  ,MakeModelGroupingID    int           null
  ,OptionList             varchar(500)  null
   --  DAA import columns; the columns above are not included in the BULK INSERT view
  ,AuctionName            varchar(250)  not null
    constraint DF_AuctionStaging_DAA__AuctionName
     default ''
  ,AuctionState           varchar(10)   not null
    constraint DF_AuctionStaging_DAA__AuctionState
     default ''
  ,AuctionZip             varchar(10)   not null
    constraint DF_AuctionStaging_DAA__AuctionZip
     default ''
  ,AuctionDate            varchar(50)   not null
    constraint DF_AuctionStaging_DAA__AuctionDate
     default ''
  ,AuctionStartTime       varchar(50)   not null
    constraint DF_AuctionStaging_DAA__AuctionStartTime
     default ''
  ,AuctionEndTime         varchar(50)   not null
    constraint DF_AuctionStaging_DAA__AuctionEndTime
     default ''
  ,Sale                   varchar(100)  not null
    constraint DF_AuctionStaging_DAA__Sale
     default ''
  ,LaneNumber             varchar(50)   not null
    constraint DF_AuctionStaging_DAA__LaneNumber
     default ''
  ,LaneDescription        varchar(500)  not null
    constraint DF_AuctionStaging_DAA__LaneDescription
     default ''
  ,Seller                 varchar(500)  not null
    constraint DF_AuctionStaging_DAA__Seller
     default ''
  ,SellerDescription      varchar(500)  not null
    constraint DF_AuctionStaging_DAA__SellerDescription
     default ''
  ,Accesslevel            varchar(10)   not null
    constraint DF_AuctionStaging_DAA__Accesslevel
     default ''
  ,Lot                    varchar(20)   not null
    constraint DF_AuctionStaging_DAA__Lot
     default ''
  ,VIN                    varchar(50)   not null
    constraint DF_AuctionStaging_DAA__VIN
     default ''
  ,Year                   varchar(10)   not null
    constraint DF_AuctionStaging_DAA__Year
     default ''
  ,Make                   varchar(50)   not null
    constraint DF_AuctionStaging_DAA__Make
     default ''
  ,Model                  varchar(50)   not null
    constraint DF_AuctionStaging_DAA__Model
     default ''
  ,Series                 varchar(50)   not null
    constraint DF_AuctionStaging_DAA__Series
     default ''
  ,Body                   varchar(50)   not null
    constraint DF_AuctionStaging_DAA__Body
     default ''
  ,Mileage                varchar(10)   not null
    constraint DF_AuctionStaging_DAA__Mileage
     default ''
  ,Engine                 varchar(50)   not null
    constraint DF_AuctionStaging_DAA__Engine
     default ''
  ,ExteriorColor          varchar(50)   not null
    constraint DF_AuctionStaging_DAA__ExteriorColor
     default ''
  ,Options                varchar(5000)  not null
    constraint DF_AuctionStaging_DAA__Options
     default ''
  ,Condition              varchar(500)  not null
    constraint DF_AuctionStaging_DAA__Condition
     default ''
  ,ImageURL               varchar(500)  not null
    constraint DF_AuctionStaging_DAA__ImageURL
     default ''
  ,Online                 varchar(10)   not null
    constraint DF_AuctionStaging_DAA__Online
     default ''
  ,VehicleURL             varchar(500)  not null
    constraint DF_AuctionStaging_DAA__VehicleURL
     default ''
  ,BuyNow                 varchar(20)   not null
    constraint DF_AuctionStaging_DAA__BuyNow
     default ''
  ,CurrentBid             varchar(20)   not null
    constraint DF_AuctionStaging_DAA__CurrentBid
     default ''
 )


--  Create table AuctionRollback_DAA
CREATE TABLE AuctionRollback_DAA
 (
   StagingID              int           not null
  ,IsGood                 bit           not null
  ,SaleID                 int           null
  ,SaleVehicleID          int           null
  ,GroupingDescriptionID  int           null
  ,MakeModelGroupingID    int           null
  ,OptionList             varchar(500)  null
  ,AuctionName            varchar(250)  not null
  ,AuctionState           varchar(10)   not null
  ,AuctionZip             varchar(10)   not null
  ,AuctionDate            varchar(50)   not null
  ,AuctionStartTime       varchar(50)   not null
  ,AuctionEndTime         varchar(50)   not null
  ,Sale                   varchar(100)  not null
  ,LaneNumber             varchar(50)   not null
  ,LaneDescription        varchar(500)  not null
  ,Seller                 varchar(500)  not null
  ,SellerDescription      varchar(500)  not null
  ,Accesslevel            varchar(10)   not null
  ,Lot                    varchar(20)   not null
  ,VIN                    varchar(50)   not null
  ,Year                   varchar(10)   not null
  ,Make                   varchar(50)   not null
  ,Model                  varchar(50)   not null
  ,Series                 varchar(50)   not null
  ,Body                   varchar(50)   not null
  ,Mileage                varchar(10)   not null
  ,Engine                 varchar(50)   not null
  ,ExteriorColor          varchar(50)   not null
  ,Options                varchar(5000)  not null
  ,Condition              varchar(500)  not null
  ,ImageURL               varchar(500)  not null
  ,Online                 varchar(10)   not null
  ,VehicleURL             varchar(500)  not null
  ,BuyNow                 varchar(20)   not null
  ,CurrentBid             varchar(20)   not null
 )

GO
/*

Used by the BULK INSERT when loading Adesa data

*/
CREATE VIEW vAdesaImport
AS
 select
    AMSSaleID
   ,SaleDate
   ,SaleType
   ,OwnerID
   ,ConsignorType
   ,Consignor
   ,LocationID
   ,Location
   ,Lot
   ,RunNumber
   ,Year
   ,Make
   ,Model
   ,Body
   ,Series
   ,Engine
   ,Color
   ,Doors
   ,Odometer
   ,M_K
   ,VIN
   ,OpenSale
   ,TransmissionDescription
   ,AMSOptions
   ,AMSAnnouncements
   ,UVCCode
  from AuctionStaging_Adesa

GO
/*

Used by the BULK INSERT when loading DAA data

*/

CREATE VIEW vDAAImport
AS
 select
    AuctionName
   ,AuctionState
   ,AuctionZip
   ,AuctionDate
   ,AuctionStartTime
   ,AuctionEndTime
   ,Sale
   ,LaneNumber
   ,LaneDescription
   ,Seller
   ,SellerDescription
   ,Accesslevel
   ,Lot
   ,VIN
   ,Year
   ,Make
   ,Model
   ,Series
   ,Body
   ,Mileage
   ,Engine
   ,ExteriorColor
   ,Options
   ,Condition
   ,ImageURL
   ,Online
   ,VehicleURL
   ,BuyNow
   ,CurrentBid
  from AuctionStaging_DAA

GO

