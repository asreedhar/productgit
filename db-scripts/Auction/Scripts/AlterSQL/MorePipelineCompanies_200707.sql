/*

Additions to the AuctionPipeline list, created July 10, 2007
 - And a minor fix to some glitchy data

*/

SET NOCOUNT on

INSERT Company (CompanyID, Name)
       select 22, 'Rea Brothers'' Mid-South Auction - Pearl'
 union select 23, 'Missouri Auto Auction - Columbia'


INSERT Location
 (
   CompanyID
  ,Name
  ,City
  ,State
  ,ZipCode
  ,CompanyLocationIdentifier
 )
       select 22, 'Rea Brothers'' Mid-South Auction - Pearl', 'Pearl',    'MS', '39288', ''
 union select 23, 'Missouri Auto Auction - Columbia'        , 'Columbia', 'MO', '65201', ''


--  Set the identifier for an intentional (??) duplicate
UPDATE Location
 set CompanyLocationIdentifier = 'STLN'
 where CompanyID = 16

