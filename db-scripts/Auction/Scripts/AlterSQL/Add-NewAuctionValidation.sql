INSERT 
INTO	dbo.ValidationCheck
		(ValidationCheckID,Title,Description)
VALUES (2043,'Invalid Lane\Lot', 'The LOT format should be 6 characters long, with the last 4 digits being numeric.')

GO