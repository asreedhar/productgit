
/*********************************************************************************************************************************************
**
**	This script makes several assumptions.  It assumes that the @Datafeed code value is used in the names of all the procedures\paths.
**	It also assumes that the @CompanyName value is the same as the Location Name.
**
**	This script does the following:
**			1.	 Inserts row into the Auction.dbo.Company table.
**			2.  Inserts row into the Auction.dbo.Locations table.
**			3.  Inserts row into the Auction.dbo.Datafeed table.
**			4.	Creates the AuctionStaging__ColumbusFair table.
**			5.	Creates the AuctionRollback__ColumbusFair table
**
**
*************************************************************************************************************************************************/
DECLARE @DataSourceID INT
DECLARE @CompanyID INT
DECLARE @CompanyName VARCHAR(50)
DECLARE @AuctionName VARCHAR(100)
DECLARE @DatafeedCode VARCHAR(50)
DECLARE @City VARCHAR(50)
DECLARE @Zip	CHAR(5)
DECLARE @State CHAR(2)
DECLARE @CompanyLocationIdentifier VARCHAR(20)
DECLARE @FileExtension VARCHAR(3)
DECLARE @IsActive BIT

SET @DataSourceID =101
SET @CompanyID =27
SET @CompanyName ='Columbus Fair'
SET @DatafeedCode ='ColumbusFair'
SET @AuctionName ='Columbus Fair Auto Auction'
SET @City ='Columbus'
SET @State ='OH'
SET @Zip ='43207'
SET @FileExtension ='txt'
SET @CompanyLocationIdentifier =''
SET @IsActive =1

/*****
*  1  *
******/

     
INSERT        
INTO	dbo.Company
		(CompanyID,	Name)
VALUES	(@CompanyID, @CompanyName)


/*****
*  2  *
******/

INSERT
INTO	dbo.Location
		(CompanyID,
		Name,
		City,
		State,
		ZipCode,
		CompanyLocationIdentifier)
VALUES	( @CompanyID,
		@AuctionName,
		@City,
		@State,
		@Zip,
		@CompanyLocationIdentifier)
		
/*****
*  3  *
******/		
		
INSERT
INTO	dbo.Datafeed
		(DatafeedID,
		DatafeedCode,
		Name,
		IsActive,
		LoadProcedure,
		RollbackProcedure)
VALUES	(@DataSourceID,						
		@DatafeedCode,
		@CompanyName,
		@IsActive,
		'LoadData_'+@DatafeedCode,
		'RollbackStagedData_'+@DatafeedCode
		)
GO


/*****
*  4  *
******/
		CREATE TABLE [dbo].[AuctionStaging_ColumbusFair]
		(
		[StagingID] 			[int] IDENTITY(1,1) NOT NULL,
		[IsGood] 				[bit] NOT NULL 		CONSTRAINT [DF_AuctionStaging_ColumbusFair__IsGood]  DEFAULT ((1)),
		[SaleID] 				[int] NULL,
		[SaleVehicleID] 		[int] NULL,
		[GroupingDescriptionID] [int] NULL,
		[MakeModelGroupingID] 	[int] NULL,
		[Auction_Name] 			[varchar](250) NOT NULL CONSTRAINT [DF_AuctionStaging_ColumbusFair__Auction_Name]  DEFAULT (''),
		[Auction_State] 		[varchar](50) NOT NULL 	CONSTRAINT [DF_AuctionStaging_ColumbusFair__Auction_State]  DEFAULT (''),
		[Auction_Zip] 			[varchar](50) NOT NULL 	CONSTRAINT [DF_AuctionStaging_ColumbusFair__Auction_Zip]  DEFAULT (''),
		[Auction_Date] 			[varchar](50) NOT NULL 	CONSTRAINT [DF_AuctionStaging_ColumbusFair__Auction_Date]  DEFAULT (''),
		[Auction_Start_Time]	[varchar](50) NOT NULL	CONSTRAINT [DF_AuctionStaging_ColumbusFair__Auction_Start_Time]  DEFAULT (''),
		[Auction_End_Time] 		[varchar](50) NOT NULL 	CONSTRAINT [DF_AuctionStaging_ColumbusFair__Auction_End_Time]  DEFAULT (''),
		[Sale] 					[varchar](50) NOT NULL 	CONSTRAINT [DF_AuctionStaging_ColumbusFair__Sale]  DEFAULT (''),
		[Lane_Num] 				[varchar](50) NOT NULL 	CONSTRAINT [DF_AuctionStaging_ColumbusFair__Lane_Num]  DEFAULT (''),
		[Lane_Descr] 			[varchar](250) NOT NULL CONSTRAINT [DF_AuctionStaging_ColumbusFair__Lane_Descr]  DEFAULT (''),
		[Seller] 				[varchar](250) NOT NULL CONSTRAINT [DF_AuctionStaging_ColumbusFair__Seller]  DEFAULT (''),
		[Seller_Descr] 			[varchar](250) NOT NULL CONSTRAINT [DF_AuctionStaging_ColumbusFair__Seller_Descr]  DEFAULT (''),
		[Access_Level] 			[varchar](50) NOT NULL 	CONSTRAINT [DF_AuctionStaging_ColumbusFair__Access_Level]  DEFAULT (''),
		[LOT] 					[varchar](50) NOT NULL 	CONSTRAINT [DF_AuctionStaging_ColumbusFair__LOT]  DEFAULT (''),
		[VIN] 					[varchar](50) NOT NULL 	CONSTRAINT [DF_AuctionStaging_ColumbusFair__VIN]  DEFAULT (''),
		[Year] 					[varchar](50) NOT NULL 	CONSTRAINT [DF_AuctionStaging_ColumbusFair__Year]  DEFAULT (''),
		[Make] 					[varchar](50) NOT NULL 	CONSTRAINT [DF_AuctionStaging_ColumbusFair__Make]  DEFAULT (''),
		[Model] 				[varchar](50) NOT NULL 	CONSTRAINT [DF_AuctionStaging_ColumbusFair__Model]  DEFAULT (''),
		[Series] 				[varchar](50) NOT NULL 	CONSTRAINT [DF_AuctionStaging_ColumbusFair__Series]  DEFAULT (''),
		[Body] 					[varchar](50) NOT NULL 	CONSTRAINT [DF_AuctionStaging_ColumbusFair__Body]  DEFAULT (''),
		[Mileage] 				[varchar](50) NOT NULL 	CONSTRAINT [DF_AuctionStaging_ColumbusFair__Mileage]  DEFAULT (''),
		[Engine] 				[varchar](50) NOT NULL 	CONSTRAINT [DF_AuctionStaging_ColumbusFair__Engine]  DEFAULT (''),
		[Color] 				[varchar](50) NOT NULL 	CONSTRAINT [DF_AuctionStaging_ColumbusFair__Color]  DEFAULT (''),
		[Options] 				[varchar](2000) NOT NULL CONSTRAINT [DF_AuctionStaging_ColumbusFair__Options]  DEFAULT (''),
		[Condition] 			[varchar](250) NOT NULL CONSTRAINT [DF_AuctionStaging_ColumbusFair__Condition]  DEFAULT (''),
		[ImageURL] 				[varchar](250) NOT NULL CONSTRAINT [DF_AuctionStaging_ColumbusFair__ImageURL]  DEFAULT (''),
		[Online] 				[varchar](50) NOT NULL 	CONSTRAINT [DF_AuctionStaging_ColumbusFair__Online]  DEFAULT (''),
		[VehicleURL] 			[varchar](250) NOT NULL CONSTRAINT [DF_AuctionStaging_ColumbusFair__VehicleURL]  DEFAULT (''),
		[BuyNow] 				[varchar](50) NOT NULL 	CONSTRAINT [DF_AuctionStaging_ColumbusFair__BuyNow]  DEFAULT (''),
		[CurrentBid] 			[varchar](50) NOT NULL 	CONSTRAINT [DF_AuctionStaging_ColumbusFair__CurrentBid]  DEFAULT (''),
		[Blank] 				[varchar](2000) NOT NULL CONSTRAINT [DF_AuctionStaging_ColumbusFair__Blank]  DEFAULT ('')
)

/*****
*  5  *
******/
		CREATE TABLE [dbo].[AuctionRollback_ColumbusFair]
		(
		[StagingID] 			[int] NOT NULL,
		[IsGood] 				[bit] NOT NULL,
		[SaleID] 				[int] NULL,
		[SaleVehicleID] 		[int] NULL,
		[GroupingDescriptionID] [int] NULL,
		[MakeModelGroupingID] 	[int] NULL,
		[Auction_Name] 			[varchar](250) NOT NULL,
		[Auction_State] 		[varchar](50) NOT NULL,
		[Auction_Zip] 			[varchar](50) NOT NULL,
		[Auction_Date] 			[varchar](50) NOT NULL,
		[Auction_Start_Time]	[varchar](50) NOT NULL,
		[Auction_End_Time] 		[varchar](50) NOT NULL,
		[Sale] 					[varchar](50) NOT NULL,
		[Lane_Num] 				[varchar](50) NOT NULL,
		[Lane_Descr] 			[varchar](250) NOT NULL,
		[Seller] 				[varchar](250) NOT NULL,
		[Seller_Descr] 			[varchar](250) NOT NULL,
		[Access_Level] 			[varchar](50) NOT NULL,
		[LOT] 					[varchar](50) NOT NULL,
		[VIN] 					[varchar](50) NOT NULL,
		[Year] 					[varchar](50) NOT NULL,
		[Make] 					[varchar](50) NOT NULL,
		[Model] 				[varchar](50) NOT NULL,
		[Series] 				[varchar](50) NOT NULL,
		[Body] 					[varchar](50) NOT NULL,
		[Mileage] 				[varchar](50) NOT NULL,
		[Engine] 				[varchar](50) NOT NULL,
		[Color] 				[varchar](50) NOT NULL,
		[Options] 				[varchar](2000) NOT NULL,
		[Condition] 			[varchar](250) NOT NULL,
		[ImageURL] 				[varchar](250) NOT NULL,
		[Online] 				[varchar](50) NOT NULL,
		[VehicleURL] 			[varchar](250) NOT NULL,
		[BuyNow] 				[varchar](50) NOT NULL,
		[CurrentBid] 			[varchar](50) NOT NULL,
		[Blank] 				[varchar](2000) NOT NULL
)	
GO