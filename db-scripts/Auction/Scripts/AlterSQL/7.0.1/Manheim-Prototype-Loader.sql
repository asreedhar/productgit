CREATE VIEW dbo.SaleVehicle_Extract
AS
SELECT	DISTINCT 
	SaleID				= S2.SaleID, 
	Year				= [Vehicle.VehicleType.Year.Id], 
	GroupingDescriptionID		= NULL,	-- MMG.GroupingDescriptionID, 
	CompanyVehicleIdentifier	= CAST([SaleInfo.LaneNumber] AS VARCHAR) + ' - ' + CAST([SaleInfo.SaleNumber] AS VARCHAR) + ' - ' + CAST([SaleInfo.RunNumber] AS VARCHAR),
	MakeModelGroupingID		= NULL,		-- MMG.MakeModelGroupingID,	
	VIN				= [Vehicle.VehicleType.VinInfo.VIN],
	Color				= COALESCE(L.ExteriorColor, ''),
	Series				= [Vehicle.VehicleType.Body.Name],	
	Odometer			= [Vehicle.VehicleDetail.Mileage],
	Transmission			= COALESCE(L.Transmission, ''),
	OwnerID				= 0,
	ConsignorType			= 0,		--L.Channel,
	Consignor			= Consignor,
	Lot				= [SaleInfo.LaneNumber], 
	RunNumber			= [SaleInfo.RunNumber], 
	Make				= [Vehicle.VehicleType.Make.Name],	 
	Model				= [Vehicle.VehicleType.Model.Name], 
	Body				= [Vehicle.VehicleType.Body.Name], 
	Engine				= COALESCE(L.Engine, ''),
	Doors				= 0, 
	M_K				= '',	 
	AMSOptions			= COALESCE(L.OptionList,''), 
	AMSAnnouncements		= L.Comments, 
	UVCCode				= ''

FROM	Staging.Manheim.Sales  S
	INNER JOIN Staging.Manheim.Listings_Extract L ON S.[Channel.Code] = L.Channel
									AND S.[Auction.Id] = L.PhysicalLocation
									AND S.[Auction.Lane] = L.[SaleInfo.LaneNumber]
									AND S.[Auction.SaleNumber] = L.[SaleInfo.SaleNumber]
									AND S.[Auction.Consignor] = L.[Consignor]
									AND S.[Auction.SaleDate] = L.[SaleDate]
	INNER JOIN dbo.Location LC ON LC.CompanyID = 21 AND S.[Auction.Id] = LC.CompanyLocationIdentifier	
	INNER JOIN dbo.Sale_Target S2 ON S2.LocationID = LC.LocationID 
					AND S2.DateHeld = S.[Auction.SaleDate]
					AND S2.CompanySaleIdentifier = S.[Channel.Code] + '/' + S.[Auction.Id] + '/' + S.[Auction.Consignor]
						
GO	
	

CREATE PROC dbo.Manheim#PrototypeLoader
AS
SET NOCOUNT ON 
DECLARE @Step VARCHAR(200)

CREATE TABLE #Sale (	SaleID int NOT NULL,
			LocationID smallint NOT NULL,
			DateHeld smalldatetime NOT NULL,
			Title varchar (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
			AuctionTypeCode tinyint NOT NULL,
			CompanySaleIdentifier varchar (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
			)

CREATE TABLE #SaleVehicle (	SaleVehicleID			int NOT NULL,
				SaleID				INT NOT NULL,
				VIN				CHAR(17) NOT NULL,
				CompanyVehicleIdentifier	VARCHAR(100)
				)


INSERT
INTO	dbo.Sale_Target (LocationID, DateHeld, Title, AuctionTypeCode, CompanySaleIdentifier)
OUTPUT	INSERTED.SaleID, INSERTED.LocationID, INSERTED.DateHeld, INSERTED.Title, INSERTED.AuctionTypeCode, INSERTED.CompanySaleIdentifier INTO #Sale
SELECT	DISTINCT LC.LocationID, S.[Auction.SaleDate], COALESCE(NULLIF(S.[Auction.Description],''), S.[Auction.Consignor]), 0,  S.[Channel.Code] + '/' + S.[Auction.Id] + '/' + S.[Auction.Consignor]
FROM	Staging.Manheim.Sales  S
	INNER JOIN Staging.Manheim.Listings_Extract L ON S.[Channel.Code] = L.Channel
									AND S.[Auction.Id] = L.PhysicalLocation
									AND S.[Auction.Lane] = L.[SaleInfo.LaneNumber]
									AND S.[Auction.SaleNumber] = L.[SaleInfo.SaleNumber]
									AND S.[Auction.Consignor] = L.[Consignor]
									AND S.[Auction.SaleDate] = L.[SaleDate]
									
	INNER JOIN dbo.Location LC ON LC.CompanyID = 21 AND S.[Auction.Id] = LC.CompanyLocationIdentifier

SELECT	*
INTO	#SaleVehicle_Extract
FROM	dbo.SaleVehicle_Extract

------------------------------------------------------------------------------------------------
SET @Step = 'Update MMG/GD'
------------------------------------------------------------------------------------------------

UPDATE	X
SET	MakeModelGroupingID	= MMG.MakeModelGroupingID,
	GroupingDescriptionID	= MMG.GroupingDescriptionID
FROM	#SaleVehicle_Extract X	
	CROSS APPLY VehicleCatalog.Firstlook.GetVehicleCatalogByVINTrimTransmission(X.VIN, X.Series, X.Transmission) VC
	INNER JOIN IMT.dbo.MakeModelGrouping MMG ON VC.ModelID = MMG.ModelID

------------------------------------------------------------------------------------------------
SET @Step = 'Removed undecoded and duplicate VINs'
------------------------------------------------------------------------------------------------

DELETE	X
FROM	#SaleVehicle_Extract X
	LEFT JOIN (	SELECT	SaleID, VIN, CompanyVehicleIdentifier
			FROM	#SaleVehicle_Extract X
			GROUP
			BY	SaleID, VIN, CompanyVehicleIdentifier
			HAVING	COUNT(*) > 1
			) D ON X.SaleID = D.SaleID
				AND X.VIN = D.VIN
				AND X.CompanyVehicleIdentifier = D.CompanyVehicleIdentifier
				
WHERE	MakeModelGroupingID IS NULL	
	OR D.VIN IS NOT NULL		-- duplicate

INSERT
INTO	dbo.SaleVehicle_Target (SaleID, Year, GroupingDescriptionID, CompanyVehicleIdentifier, MakeModelGroupingID, VIN, Color, Series, Odometer, Transmission )
OUTPUT	INSERTED.SaleVehicleID, INSERTED.SaleID, INSERTED.VIN, INSERTED.CompanyVehicleIdentifier INTO #SaleVehicle

SELECT	SaleID,	Year, GroupingDescriptionID, CompanyVehicleIdentifier,	MakeModelGroupingID, VIN, Color, Series, Odometer, Transmission			
FROM	#SaleVehicle_Extract


CREATE CLUSTERED INDEX #IX_SaleVehicle ON #SaleVehicle(SaleVehicleID, SaleID, VIN, CompanyVehicleIdentifier)

INSERT
INTO	dbo.SaleVehicleCompanyData_Adesa_Target(SaleVehicleID, OwnerID, ConsignorType, Consignor, Lot, RunNumber, Make, Model, Body, Engine, Doors, M_K, AMSOptions, AMSAnnouncements, UVCCode)

SELECT	SaleVehicleID, OwnerID,	ConsignorType, Consignor,  Lot,	RunNumber, Make, Model, Body, COALESCE(Engine,''), Doors, M_K, AMSOptions, AMSAnnouncements, UVCCode			
FROM	#SaleVehicle_Extract X
	INNER JOIN #SaleVehicle SV ON X.SaleID = SV.SaleID AND X.VIN = SV.VIN AND X.CompanyVehicleIdentifier = SV.CompanyVehicleIdentifier

GO

/*

INSERT
INTO	dbo.Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier )

SELECT	DISTINCT C.CompanyID, L.[Auction.Name], City, LEFT(RTRIM(LTRIM(L.STATE)),2), LEFT(L.Zip,5), S.[Auction.Id]
FROM	Staging.Manheim.Locations L
	INNER JOIN dbo.Company C ON C.Name = 'Manheim'
	INNER JOIN Staging.Manheim.Sales S ON L.[Auction.Name] = S.[Auction.Name]
	
*/	

    