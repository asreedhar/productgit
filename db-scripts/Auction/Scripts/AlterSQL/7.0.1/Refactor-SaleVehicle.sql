DROP TABLE dbo.AdesaLocations
GO

/*

Move common attributes to dbo.SaleVehicle from the "CompanyData" subtype tables.  This allows us to remove downstream
references to the subtype tables in PING, for example.



Making the new attributes NULLABLE where applicable, and renaming TransmissionDescription to Transmission


*/

ALTER TABLE dbo.SaleVehicle#0 ADD VIN CHAR(17) NULL
ALTER TABLE dbo.SaleVehicle#0 ADD Color	VARCHAR(50) NULL
ALTER TABLE dbo.SaleVehicle#0 ADD Series VARCHAR(50) NULL
ALTER TABLE dbo.SaleVehicle#0 ADD Odometer INT NULL
ALTER TABLE dbo.SaleVehicle#0 ADD Transmission VARCHAR(50) NULL


ALTER TABLE dbo.SaleVehicle#1 ADD VIN CHAR(17) NULL
ALTER TABLE dbo.SaleVehicle#1 ADD Color	VARCHAR(50) NULL
ALTER TABLE dbo.SaleVehicle#1 ADD Series VARCHAR(50) NULL
ALTER TABLE dbo.SaleVehicle#1 ADD Odometer INT NULL
ALTER TABLE dbo.SaleVehicle#1 ADD Transmission VARCHAR(50) NULL


EXEC sp_refreshview 'dbo.SaleVehicle'
EXEC sp_refreshview 'dbo.SaleVehicle_TARGET'
GO

UPDATE	SV
SET	VIN		= COALESCE(A.VIN, CF.VIN),
	Color		= NULLIF(COALESCE(A.Color, CF.Color),''),
	Series		= NULLIF(COALESCE(A.Series, CF.Series),''),	
	Odometer	= COALESCE(A.Odometer, CF.Mileage),
	Transmission	= NULLIF(A.TransmissionDescription,'')		-- NO Transmission for CF

	
FROM	dbo.SaleVehicle SV
	LEFT JOIN dbo.SaleVehicleCompanyData_Adesa A ON SV.SaleVehicleID = A.SaleVehicleID
	LEFT JOIN dbo.SaleVehicleCompanyData_ColumbusFair CF ON SV.SaleVehicleID = CF.SaleVehicleID


ALTER TABLE dbo.SaleVehicle#0 ALTER COLUMN VIN CHAR(17) NOT NULL
ALTER TABLE dbo.SaleVehicle#0 ALTER COLUMN Odometer INT NOT NULL

ALTER TABLE dbo.SaleVehicle#1 ALTER COLUMN VIN CHAR(17) NOT NULL
ALTER TABLE dbo.SaleVehicle#1 ALTER COLUMN Odometer INT NOT NULL

ALTER TABLE dbo.SaleVehicleCompanyData_Adesa#0 DROP COLUMN VIN
ALTER TABLE dbo.SaleVehicleCompanyData_Adesa#0 DROP COLUMN Color
ALTER TABLE dbo.SaleVehicleCompanyData_Adesa#0 DROP COLUMN Series
ALTER TABLE dbo.SaleVehicleCompanyData_Adesa#0 DROP COLUMN Odometer
ALTER TABLE dbo.SaleVehicleCompanyData_Adesa#0 DROP COLUMN TransmissionDescription

ALTER TABLE dbo.SaleVehicleCompanyData_Adesa#1 DROP COLUMN VIN
ALTER TABLE dbo.SaleVehicleCompanyData_Adesa#1 DROP COLUMN Color
ALTER TABLE dbo.SaleVehicleCompanyData_Adesa#1 DROP COLUMN Series
ALTER TABLE dbo.SaleVehicleCompanyData_Adesa#1 DROP COLUMN Odometer
ALTER TABLE dbo.SaleVehicleCompanyData_Adesa#1 DROP COLUMN TransmissionDescription

ALTER TABLE dbo.SaleVehicleCompanyData_ColumbusFair#0 DROP COLUMN VIN
ALTER TABLE dbo.SaleVehicleCompanyData_ColumbusFair#0 DROP COLUMN Color
ALTER TABLE dbo.SaleVehicleCompanyData_ColumbusFair#0 DROP COLUMN Series
ALTER TABLE dbo.SaleVehicleCompanyData_ColumbusFair#0 DROP COLUMN Mileage

ALTER TABLE dbo.SaleVehicleCompanyData_ColumbusFair#1 DROP COLUMN VIN
ALTER TABLE dbo.SaleVehicleCompanyData_ColumbusFair#1 DROP COLUMN Color
ALTER TABLE dbo.SaleVehicleCompanyData_ColumbusFair#1 DROP COLUMN Series
ALTER TABLE dbo.SaleVehicleCompanyData_ColumbusFair#1 DROP COLUMN Mileage

GO

DECLARE @CurrentBuffer TINYINT

SELECT	@CurrentBuffer = CurrentBuffer
FROM	BufferState

EXECUTE CreateBufferViews
	'SaleVehicleCompanyData_Adesa',
'	SaleVehicleID,
	OwnerID,
	ConsignorType,
	Consignor,
	right(replicate('' '', 20) + Lot, 20) Lot,
	right(replicate('' '', 20) + RunNumber, 20) RunNumber,
	Make,
	Model,
	Body,
	Engine,
	Doors,
	M_K,
	AMSOptions,
	AMSAnnouncements,
	UVCCode',
	'*', 
	@CurrentBuffer,
        0
         
EXECUTE CreateBufferViews
	'SaleVehicleCompanyData_ColumbusFair',
'	SaleVehicleID ,
	Lane_Num,
	Lane_Descr,
	Seller,
	Seller_Descr,
	Access_Level,
	LOT,
	Year,
	Make,
	Model,
	Body,
	Engine,
	Options,
	Condition,
	ImageURL,
	Online,
	VehicleURL,
	BuyNow,
	CurrentBid',
        '*',
        @CurrentBuffer,
        0