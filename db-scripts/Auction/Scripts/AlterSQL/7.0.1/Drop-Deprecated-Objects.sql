

DROP TABLE dbo.AuctionRollback_Manheim
DROP TABLE dbo.AuctionStaging_Manheim
DROP TABLE dbo.ManheimConsignor
DROP TABLE dbo.Stage_ManheimAuction
DROP TABLE dbo.Stage_ManheimConsignor

DROP VIEW dbo.vManheimImport

DROP PROC dbo.LoadData_Manheim

DROP PROC dbo.RollbackStagedData_Manheim
DROP PROC dbo.StageData_Manheim
DROP PROC dbo.StageData_ManheimAuctions
DROP PROC dbo.StageData_ManheimConsignors
DROP PROC dbo.StageData_ManheimPresales
DROP PROC dbo.ValidateStagedData_Manheim