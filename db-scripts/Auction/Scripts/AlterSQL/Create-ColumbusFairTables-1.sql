CREATE TABLE [dbo].[SaleVehicleCompanyData_ColumbusFair#0](
	[SaleVehicleID] [int] NOT NULL,
	[VIN] [char](17) NOT NULL,
	[Lane_Num] [varchar](25) NOT NULL,
	[Lane_Descr] [varchar](250) NOT NULL,
	[Seller] [varchar](250) NOT NULL,
	[Seller_Descr] [varchar](250) NOT NULL,
	[Access_Level] [int] NOT NULL,
	[LOT] [varchar](10) NOT NULL,
	[Year] [int] NOT NULL,
	[Make] [varchar](50) NOT NULL,
	[Model] [varchar](50) NOT NULL,
	[Series] [varchar](50) NOT NULL,
	[Body] [varchar](50) NOT NULL,
	[Mileage] [int] NOT NULL,
	[Engine] [varchar](50) NOT NULL,
	[Color] [varchar](50) NOT NULL,
	[Options] [varchar](250) NOT NULL,
	[Condition] [varchar](250) NOT NULL,
	[ImageURL] [varchar](250) NOT NULL,
	[Online] [varchar](50) NOT NULL,
	[VehicleURL] [varchar](250) NOT NULL,
	[BuyNow] [varchar](50) NOT NULL,
	[CurrentBid] [varchar](50) NOT NULL,
 CONSTRAINT [PK_SaleVehicleCompanyData_ColumbusFair#0] PRIMARY KEY NONCLUSTERED 
(
	[SaleVehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[SaleVehicleCompanyData_ColumbusFair#0]  WITH CHECK ADD  CONSTRAINT [FK_SaleVehicleCompanyData_ColumbusFair#0__SaleVehicle#0] FOREIGN KEY([SaleVehicleID])
REFERENCES [dbo].[SaleVehicle#0] ([SaleVehicleID])
GO
ALTER TABLE [dbo].[SaleVehicleCompanyData_ColumbusFair#0] CHECK CONSTRAINT [FK_SaleVehicleCompanyData_ColumbusFair#0__SaleVehicle#0]
GO


CREATE TABLE [dbo].[SaleVehicleCompanyData_ColumbusFair#1](
	[SaleVehicleID] [int] NOT NULL,
	[VIN] [char](17) NOT NULL,
	[Lane_Num] [varchar](25) NOT NULL,
	[Lane_Descr] [varchar](250) NOT NULL,
	[Seller] [varchar](250) NOT NULL,
	[Seller_Descr] [varchar](250) NOT NULL,
	[Access_Level] [int] NOT NULL,
	[LOT] [varchar](10) NOT NULL,
	[Year] [int] NOT NULL,
	[Make] [varchar](50) NOT NULL,
	[Model] [varchar](50) NOT NULL,
	[Series] [varchar](50) NOT NULL,
	[Body] [varchar](50) NOT NULL,
	[Mileage] [int] NOT NULL,
	[Engine] [varchar](50) NOT NULL,
	[Color] [varchar](50) NOT NULL,
	[Options] [varchar](250) NOT NULL,
	[Condition] [varchar](250) NOT NULL,
	[ImageURL] [varchar](250) NOT NULL,
	[Online] [varchar](50) NOT NULL,
	[VehicleURL] [varchar](250) NOT NULL,
	[BuyNow] [varchar](50) NOT NULL,
	[CurrentBid] [varchar](50) NOT NULL,
 CONSTRAINT [PK_SaleVehicleCompanyData_ColumbusFair#1] PRIMARY KEY NONCLUSTERED 
(
	[SaleVehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[SaleVehicleCompanyData_ColumbusFair#1]  WITH CHECK ADD  CONSTRAINT [FK_SaleVehicleCompanyData_ColumbusFair#1__SaleVehicle#1] FOREIGN KEY([SaleVehicleID])
REFERENCES [dbo].[SaleVehicle#1] ([SaleVehicleID])
GO
ALTER TABLE [dbo].[SaleVehicleCompanyData_ColumbusFair#1] CHECK CONSTRAINT [FK_SaleVehicleCompanyData_ColumbusFair#1__SaleVehicle#1]


GO