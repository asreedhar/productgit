/*

Additions to the AuctionPipeline list, created April 9, 2007

*/

INSERT Company (CompanyID, Name)
       select 17, 'Columbus Fair Auto Auction - Columbus'
 union select 18, 'Sioux Falls Auto Auction - Tea'
 union select 19, 'State Line Auto Auction - Waverly'
 union select 20, 'Tri-State Auto Auction - Fargo - West Fargo'


INSERT Location
 (
   CompanyID
  ,Name
  ,City
  ,State
  ,ZipCode
  ,CompanyLocationIdentifier
 )
       select 17, 'Columbus Fair Auto Auction - Columbus'      , 'Columbus', 'OH', '43207', 'CFAA'
 union select 18, 'Sioux Falls Auto Auction - Tea'             , 'Sioux Falls, SD', 'SD', '57064', 'SFAA'
 union select 19, 'State Line Auto Auction - Waverly'          , 'Waverly, NY', 'NY', '14892', 'STLN'
 union select 20, 'Tri-State Auto Auction - Fargo - West Fargo', 'Fargo, ND', 'ND', '58078', 'TSFAA'

GO
