/*

Update existing Auction Pipeline companies and locations (now have city in with name)
Configure new Auction Pipeline company

*/

SET NOCOUNT on

UPDATE Company set Name = 'Brasher''s Cascade Auto Auction - Wood Village'     where CompanyID = 3
UPDATE Company set Name = 'Brasher''s Northwest Auto Auction - Eugene'         where CompanyID = 4
UPDATE Company set Name = 'Brasher''s Reno Auto Auction - Reno' where CompanyID = 5
UPDATE Company set Name = 'Brasher''s Sacramento Auto Auction - Rio Linda'     where CompanyID = 6
UPDATE Company set Name = 'Brasher''s Salt Lake Auto Auction - Salt Lake City' where CompanyID = 7
UPDATE Company set Name = 'Carolina Auto Auction - Williamston' where CompanyID = 8
UPDATE Company set Name = 'Charleston Auto Auction - Moncks Corner'            where CompanyID = 9
UPDATE Company set Name = 'DAA Northwest - Spokane'             where CompanyID = 10
UPDATE Company set Name = 'Dealers Auto Auction of OKC - Oklahoma City'        where CompanyID = 11
UPDATE Company set Name = 'Flint Auto Auction - Flint'          where CompanyID = 12
UPDATE Company set Name = 'Idaho Auto Auction - Boise'          where CompanyID = 13
UPDATE Company set Name = 'Kansas Auto Auction - Elwood'        where CompanyID = 14
UPDATE Company set Name = 'Tri-State Auto Auction - West Fargo' where CompanyID = 15

UPDATE Location set Name = 'Brasher''s Cascade Auto Auction - Wood Village'     where CompanyID = 3
UPDATE Location set Name = 'Brasher''s Northwest Auto Auction - Eugene'         where CompanyID = 4
UPDATE Location set Name = 'Brasher''s Reno Auto Auction - Reno' where CompanyID = 5
UPDATE Location set Name = 'Brasher''s Sacramento Auto Auction - Rio Linda'     where CompanyID = 6
UPDATE Location set Name = 'Brasher''s Salt Lake Auto Auction - Salt Lake City' where CompanyID = 7
UPDATE Location set Name = 'Carolina Auto Auction - Williamston' where CompanyID = 8
UPDATE Location set Name = 'Charleston Auto Auction - Moncks Corner'            where CompanyID = 9
UPDATE Location set Name = 'DAA Northwest - Spokane'             where CompanyID = 10
UPDATE Location set Name = 'Dealers Auto Auction of OKC - Oklahoma City'        where CompanyID = 11
UPDATE Location set Name = 'Flint Auto Auction - Flint'          where CompanyID = 12
UPDATE Location set Name = 'Idaho Auto Auction - Boise'          where CompanyID = 13
UPDATE Location set Name = 'Kansas Auto Auction - Elwood'        where CompanyID = 14
UPDATE Location set Name = 'Tri-State Auto Auction - West Fargo' where CompanyID = 15


--  And add a new one
INSERT Company (CompanyID, Name)
 select 16, 'State Line Auto Auction - Waverly'

INSERT Location
 (
   CompanyID
  ,Name
  ,City
  ,State
  ,ZipCode
  ,CompanyLocationIdentifier
 )
  select 16,  'State Line Auto Auction - Waverly',    'Waverly',   'NY', '14892', ''
