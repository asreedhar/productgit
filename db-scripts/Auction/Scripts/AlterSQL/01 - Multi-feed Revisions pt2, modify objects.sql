/*

Modify (or drop) existing objects:
 - Drop and recreate table ValidationFailures
 - Modify table BufferState
 - Create BufferState_Log, populate it based on contents of AuctionLoadLog
 - Drop table AuctionLoadLog
 - Added in the "DaimlerChrysler" validation check row
 - Drop some old routines:
      fn_squishVIN (function)
      ProcessDataFiles
      ValidateAdesaDataset
      PurgeAuctionLoadLog
 - Remove "Auction Load Log" from the Purge system
 - Tweak Alter_Script
 - Modify the auditing trigger on table BufferState
 - Convert column UVCCode in both SaleVehicleCompanyData_Adesa # tables from char(10) to varchar(10)

*/

SET NOCOUNT on

--  This table is regularly truncated, so we can drop and recreate it.  Its also
--  just a dumping ground without (currently) any built-in functionality, so we don't
--  bother much with integrity or indexes.
DROP TABLE ValidationFailures

CREATE TABLE ValidationFailures
 (
   DatafeedID         tinyint    not null
    constraint FK_ValidationFailures__Datafeed
     foreign key references Datafeed (DatafeedID)
  ,StagingID          int        not null
  ,ValidationCheckID  smallint   not null
    constraint FK_ValidationFailures__ValidationCheck
     foreign key references ValidationCheck (ValidationCheckID)
 )

/*

Mess with buffer state logging:
 - Drop the superfluous rows, keep the important data
 - Create new log table
 - Populate it with historical data
 - Drop old log table
 - Revise trigger

*/

ALTER TABLE BufferState
 drop column
   FileName
  ,DataRows
  ,ValidRows


CREATE TABLE BufferState_Log
 (
   BufferSetAt  datetime  not null
 )

INSERT BufferState_Log (BufferSetAt)
 select LoadedAt
  from AuctionLoadLog

DROP TABLE AuctionLoadLog


--  New validation check
INSERT ValidationCheck (ValidationCheckID, Title, Description)
 values (1001, 'No DaimlerChrysler from Adesa', 'Auctions where "DaimlerChrylser" is part of the Consignor name are not loaded for Adesa auctions')


--  No longer needed
DROP FUNCTION dbo.fn_squishVIN
DROP PROCEDURE ProcessDataFiles
DROP PROCEDURE ValidateAdesaDataset
DROP PROCEDURE PurgeAuctionLoadLog

DELETE PurgeLog
 where Subject = 'Auction Load Log'

DELETE PurgeManagement
 where Subject = 'Auction Load Log'

ALTER TABLE Alter_Script
 add constraint DF_Alter_Script__ApplicationDate
  default CURRENT_TIMESTAMP for ApplicationDate

ALTER TABLE Alter_Script
 add constraint PK_Alter_Script
  primary key clustered (AlterScriptName)

ALTER TABLE SaleVehicleCompanyData_Adesa#0
 alter column UVCCode varchar(10) not null

ALTER TABLE SaleVehicleCompanyData_Adesa#1
 alter column UVCCode varchar(10) not null

GO
/******************************************************************************
**
**  Trigger: TR_U_BufferState__Auditing
**  Description: When BufferState is updated at the end of a data load run, this
**   trigger stores that information in the AuctionLoadLog table.
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  09/21/2005  PKelley   Trigger created
**  05/26/2006  PKelley   Revised for new load process
**
*****************************************************************************/
ALTER TRIGGER TR_u_BufferState__Auditing
 on BufferState
 for update
AS

    INSERT BufferState_Log (BufferSetAt)
     select LastUpdated
      from inserted

GO
