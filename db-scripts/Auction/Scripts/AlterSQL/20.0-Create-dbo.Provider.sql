CREATE TABLE dbo.Provider (
	ProviderID		TINYINT NOT NULL CONSTRAINT PK_AuctionProvider PRIMARY KEY CLUSTERED,
	Description		VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,	
	Priority		tinyint NOT NULL CONSTRAINT DF_AuctioNProvider__Priority DEFAULT 4,
	ProviderStatusID	TINYINT NOT NULL,
	DataloadID		INT NULL,
	LastStatusChange	SMALLDATETIME NULL,
	ChangedByUser		VARCHAR (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	ClassicDatafeedID	TINYINT NULL
	
	)

INSERT
INTO	dbo.Provider (ProviderID,Description,Priority,ProviderStatusID,DataloadID,ClassicDatafeedID)
SELECT	28, 'ADESA AIF', 1, 0, 0, 14


INSERT
INTO	dbo.Provider (ProviderID,Description,Priority,ProviderStatusID,DataloadID,ClassicDatafeedID)
SELECT	42, 'Pipeline', 1, 0, 0, 42

GO
--------------------------------------------------------------------------------------------
-- 	Add provider to sale, with the idea that we would remove Location at some point
--	as we don't want to have to maintain dbo.Location -- if we don't, we can lose new
--	auctions as they appear in the feeds.
--------------------------------------------------------------------------------------------

ALTER TABLE dbo.Sale#0 ADD ProviderID INT NULL
ALTER TABLE dbo.Sale#1 ADD ProviderID INT NULL