/*

Create the tables and static data required by Pipeline
 - Change existing tables the to allow for non-numeric lanes and lots
 - Table AuctionStaging_Pipeline
 - Table AuctionRollback_Pipeline
 - Data in tables Datafeed, Company, and Location
 - New validation check codes


--  Partial undo
DROP TABLE AuctionStaging_Pipeline
DROP TABLE AuctionRollback_Pipeline

*/


--  Change existing tables to allow for non-numeric lanes and lots
ALTER TABLE SaleVehicleCompanyData_Adesa#0
 alter column Lot  varchar(20)  not null

ALTER TABLE SaleVehicleCompanyData_Adesa#0
 alter column RunNumber  varchar(20)  not null

ALTER TABLE SaleVehicleCompanyData_Adesa#1
 alter column Lot  varchar(20)  not null

ALTER TABLE SaleVehicleCompanyData_Adesa#1
 alter column RunNumber  varchar(20)  not null


UPDATE SaleVehicleCompanyData_Adesa#0
 set Lot = rtrim(Lot)

UPDATE SaleVehicleCompanyData_Adesa#1
 set Lot = rtrim(Lot)


--  Create table AuctionStaging_Pipeline
CREATE TABLE AuctionStaging_Pipeline
 (
   StagingID              int           not null  identity(1,1)
  ,IsGood                 bit           not null
    constraint DF_AuctionStaging_Pipeline__IsGood
     default 1
  ,SaleID                 int           null
  ,SaleVehicleID          int           null
  ,GroupingDescriptionID  int           null
  ,MakeModelGroupingID    int           null
  ,OptionList             varchar(500)  null
   --  Pipeline import columns; the columns above are not included in the BULK INSERT view
  ,AuctionName            varchar(250)  not null
    constraint DF_AuctionStaging_Pipeline__AuctionName
     default ''
  ,AuctionState           varchar(10)   not null
    constraint DF_AuctionStaging_Pipeline__AuctionState
     default ''
  ,AuctionZip             varchar(10)   not null
    constraint DF_AuctionStaging_Pipeline__AuctionZip
     default ''
  ,AuctionDate            varchar(50)   not null
    constraint DF_AuctionStaging_Pipeline__AuctionDate
     default ''
  ,AuctionStartTime       varchar(50)   not null
    constraint DF_AuctionStaging_Pipeline__AuctionStartTime
     default ''
  ,AuctionEndTime         varchar(50)   not null
    constraint DF_AuctionStaging_Pipeline__AuctionEndTime
     default ''
  ,Sale                   varchar(100)  not null
    constraint DF_AuctionStaging_Pipeline__Sale
     default ''
  ,LaneNumber             varchar(50)   not null
    constraint DF_AuctionStaging_Pipeline__LaneNumber
     default ''
  ,LaneDescription        varchar(500)  not null
    constraint DF_AuctionStaging_Pipeline__LaneDescription
     default ''
  ,Seller                 varchar(500)  not null
    constraint DF_AuctionStaging_Pipeline__Seller
     default ''
  ,SellerDescription      varchar(500)  not null
    constraint DF_AuctionStaging_Pipeline__SellerDescription
     default ''
  ,Accesslevel            varchar(10)   not null
    constraint DF_AuctionStaging_Pipeline__Accesslevel
     default ''
  ,Lot                    varchar(50)   not null
    constraint DF_AuctionStaging_Pipeline__Lot
     default ''
  ,VIN                    varchar(50)   not null
    constraint DF_AuctionStaging_Pipeline__VIN
     default ''
  ,Year                   varchar(10)   not null
    constraint DF_AuctionStaging_Pipeline__Year
     default ''
  ,Make                   varchar(50)   not null
    constraint DF_AuctionStaging_Pipeline__Make
     default ''
  ,Model                  varchar(50)   not null
    constraint DF_AuctionStaging_Pipeline__Model
     default ''
  ,Series                 varchar(50)   not null
    constraint DF_AuctionStaging_Pipeline__Series
     default ''
  ,Body                   varchar(50)   not null
    constraint DF_AuctionStaging_Pipeline__Body
     default ''
  ,Mileage                varchar(10)   not null
    constraint DF_AuctionStaging_Pipeline__Mileage
     default ''
  ,Engine                 varchar(50)   not null
    constraint DF_AuctionStaging_Pipeline__Engine
     default ''
  ,ExteriorColor          varchar(50)   not null
    constraint DF_AuctionStaging_Pipeline__ExteriorColor
     default ''
  ,Options                varchar(5000)  not null
    constraint DF_AuctionStaging_Pipeline__Options
     default ''
  ,Condition              varchar(500)  not null
    constraint DF_AuctionStaging_Pipeline__Condition
     default ''
  ,ImageURL               varchar(500)  not null
    constraint DF_AuctionStaging_Pipeline__ImageURL
     default ''
  ,Online                 varchar(10)   not null
    constraint DF_AuctionStaging_Pipeline__Online
     default ''
  ,VehicleURL             varchar(500)  not null
    constraint DF_AuctionStaging_Pipeline__VehicleURL
     default ''
  ,BuyNow                 varchar(20)   not null
    constraint DF_AuctionStaging_Pipeline__BuyNow
     default ''
  ,CurrentBid             varchar(20)   not null
    constraint DF_AuctionStaging_Pipeline__CurrentBid
     default ''
 )


--  Create table AuctionRollback_Pipeline
CREATE TABLE AuctionRollback_Pipeline
 (
   StagingID              int           not null
  ,IsGood                 bit           not null
  ,SaleID                 int           null
  ,SaleVehicleID          int           null
  ,GroupingDescriptionID  int           null
  ,MakeModelGroupingID    int           null
  ,OptionList             varchar(500)  null
  ,AuctionName            varchar(250)  not null
  ,AuctionState           varchar(10)   not null
  ,AuctionZip             varchar(10)   not null
  ,AuctionDate            varchar(50)   not null
  ,AuctionStartTime       varchar(50)   not null
  ,AuctionEndTime         varchar(50)   not null
  ,Sale                   varchar(100)  not null
  ,LaneNumber             varchar(50)   not null
  ,LaneDescription        varchar(500)  not null
  ,Seller                 varchar(500)  not null
  ,SellerDescription      varchar(500)  not null
  ,Accesslevel            varchar(10)   not null
  ,Lot                    varchar(50)   not null
  ,VIN                    varchar(50)   not null
  ,Year                   varchar(10)   not null
  ,Make                   varchar(50)   not null
  ,Model                  varchar(50)   not null
  ,Series                 varchar(50)   not null
  ,Body                   varchar(50)   not null
  ,Mileage                varchar(10)   not null
  ,Engine                 varchar(50)   not null
  ,ExteriorColor          varchar(50)   not null
  ,Options                varchar(5000)  not null
  ,Condition              varchar(500)  not null
  ,ImageURL               varchar(500)  not null
  ,Online                 varchar(10)   not null
  ,VehicleURL             varchar(500)  not null
  ,BuyNow                 varchar(20)   not null
  ,CurrentBid             varchar(20)   not null
 )


/*

Load static data for Pipeline

*/

SET NOCOUNT on

--  Configure known Auction datafeeds
INSERT Datafeed (DatafeedID, DatafeedCode, Name, LoadProcedure, RollbackProcedure, IsActive)
 values (42, 'Pipeline', 'Auction Pipeline', 'LoadData_Pipeline', 'RollbackStagedData_Pipeline', 1)


--  Add to Company table
INSERT Company (CompanyID, Name)
       select 3, 'Brasher''s Cascade Auto Auction'
 union select 4, 'Brasher''s Northwest Auto Auction'
 union select 5, 'Brasher''s Reno Auto Auction'
 union select 6, 'Brasher''s Sacramento Auto Auction'
 union select 7, 'Brasher''s Salt Lake Auto Auction'
 union select 8, 'Carolina Auto Auction'
 union select 9, 'Charleston Auto Auction'
 union select 10, 'DAA Northwest'
 union select 11, 'Dealers Auto Auction of OKC'
 union select 12, 'Flint Auto Auction'
 union select 13, 'Idaho Auto Auction'
 union select 14, 'Kansas Auto Auction'
 union select 15, 'Tri-State Auto Auction'


--  Add all locations for this company
INSERT Location
 (
   CompanyID
  ,Name
  ,City
  ,State
  ,ZipCode
  ,CompanyLocationIdentifier
 )
       select 3,  'Portland',    'Wood Village',   'OR', '97060', 'BCAA'
 union select 4,  'Eugene',      'Eugene',         'OR', '97402', 'BNWAA'
 union select 5,  'Reno',        'Reno',           'NV', '89506', 'BRAA'
 union select 6,  'Sacramento',  'Rio Linda',      'CA', '95673', 'BSAA'
 union select 7,  'Salt Lake',   'Salt Lake City', 'UT', '84104', 'BSLAA'
 union select 8,  'Anderson',    'Williamston',    'SC', '29697', 'CARAA'
 union select 9,  'Charleston',  'Moncks Corner',  'SC', '29461', 'CHRLS'
 union select 10, 'Spokane',     'Spokane',        'WA', '99224', 'DAANW'
 union select 11, 'OK City',     'Oklahoma City',  'OK', '73108', 'DAAOKC'
 union select 12, 'Flint',       'Flint',          'MI', '48506', 'FLINT'
 union select 13, 'Boise',       'Boise',          'ID', '83716', 'IDAA'
 union select 14, 'Kansas City', 'Elwood',         'KS', '66024', 'KSAA'
 union select 15, 'Fargo',       'West Fargo',     'ND', '58078', 'TSFAA'


--  Validation checks
INSERT ValidationCheck (ValidationCheckID, Title, Description)
 values (1002, 'Auction Pipeline company not found', 'A configured company was not found for this (Auction) Pipeline listing')
INSERT ValidationCheck (ValidationCheckID, Title, Description)
 values (2021, 'Invalid Lane/Lot', 'Neither the the lane nor the lot for this item are set')
INSERT ValidationCheck (ValidationCheckID, Title, Description)
 values (2040, 'Invalid AuctionDate', 'The auction date for this item is either invalid or not set')
INSERT ValidationCheck (ValidationCheckID, Title, Description)
 values (2041, 'Invalid AuctionStartTime', 'The auction start time for this item is not set')


GO
