IF  EXISTS (SELECT 1 
			FROM	INFORMATION_SCHEMA.COLUMNS 
			WHERE TABLE_NAME = 'AuctionStaging_ColumbusFair' AND COLUMN_NAME = 'Blank')
			BEGIN
				
			ALTER TABLE AuctionStaging_ColumbusFair DROP CONSTRAINT DF_AuctionStaging_ColumbusFair__Blank	
			ALTER TABLE AuctionStaging_ColumbusFair DROP COLUMN BLANK	

			END
IF  EXISTS (SELECT 1 
			FROM	INFORMATION_SCHEMA.COLUMNS 
			WHERE TABLE_NAME = 'AuctionRollback_ColumbusFair' AND COLUMN_NAME = 'Blank')
			
			ALTER TABLE AuctionRollback_ColumbusFair DROP COLUMN BLANK
						