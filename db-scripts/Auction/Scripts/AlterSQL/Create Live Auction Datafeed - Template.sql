/*********************************************************************************************************************************************
**
**	DO NOT RUN THIS SCRIPT!!!  IT IS JUST A TEMPLATE.
**
**********************************************************************************************************************************************/



/*********************************************************************************************************************************************
**
**	This script makes several assumptions.  It assumes that the @Datafeed code value is used in the names of all the procedures\paths.
**	It also assumes that the @CompanyName value is the same as the Location Name.
**
**	This script does the following:
**			1.	Inserts row into the DBASTAT.dbo.Datasources table.
**			2.	Inserts row into the Auction.dbo.Company table.
**			3.  Inserts row into the Auction.dbo.Locations table.
**			4.  Inserts row into the Auction.dbo.Datafeed table.
**			5.	Creates the AuctionStaging__  table.
**			6.	Creates the AuctionRollback__  table
**
**
*************************************************************************************************************************************************/
DECLARE @DataSourceID INT
DECLARE @LocationID INT
DECLARE @CompanyID INT
DECLARE @CompanyName VARCHAR(50)
DECLARE @AuctionName VARCHAR(100)
DECLARE @DatafeedCode VARCHAR(50)
DECLARE @City VARCHAR(50)
DECLARE @Zip	CHAR(5)
DECLARE @State CHAR(2)
DECLARE @CompanyLocationIdentifier VARCHAR(20)
DECLARE @FileExtension VARCHAR(3)



/*****
*  1  *
******/
/*  Put in SEPARATE SCRIPT 
INSERT
INTO	DBASTAT.dbo.Datasources
        ( DatasourceID ,
          Category ,
          Description ,
          Path ,
          ProcessorName ,
          StagingPath ,
          StagingProcedureName ,
          InventoryExtractProcedureName ,
          SalesExtractProcedureName ,
          ProcessorTypeCD ,
          LoadedResultCD ,
          FileExtension ,
          FilePrefixLength ,
          FilenameDecodeRuleset ,
          Partitioned ,
          ParseFileDate ,
          VerifyFileExists ,
          DeleteFileOnLoad ,
          DatafeedCode ,
          ParseDealerID ,
          ColumnNamesInFirstDataRow
        )
VALUES  ( @DataSourceID , -- DatasourceID - tinyint
          'Auction' , -- Category - varchar(20)
          @CompanyName, -- Description - varchar(50)
          '\\' + @@SERVERNAME +'\Datafeeds\' +@DatafeedCode , -- Path - varchar(100)
          'Auction.dbo.StageData_'+ @DatafeedCode , -- ProcessorName - varchar(128)
          '' , -- StagingPath - varchar(100)
          '' , -- StagingProcedureName - varchar(128)
          '' , -- InventoryExtractProcedureName - varchar(128)
          '' , -- SalesExtractProcedureName - varchar(128)
          0 , -- ProcessorTypeCD - tinyint
          0 , -- LoadedResultCD - tinyint
          @FileExtenstion , -- FileExtension - char(3)
          0 , -- FilePrefixLength - tinyint
          1 , -- FilenameDecodeRuleset - tinyint
          0 , -- Partitioned - bit
          0 , -- ParseFileDate - bit
          0 , -- VerifyFileExists - bit
          1 , -- DeleteFileOnLoad - bit
          NULL , -- DatafeedCode - varchar(20)
          0 , -- ParseDealerID - bit
          NULL  -- ColumnNamesInFirstDataRow - bit
        )
        */

/*****
*  2  *
******/
     
INSERT        
INTO	Auction.dbo.Company
		(CompanyID,	Name)
VALUES	(@CompanyID, @CompanyName)


/*****
*  3  *
******/

INSERT
INTO	Auction.dbo.Locations
		(LocationID,
		CompanyID,
		Name,
		City,
		State,
		ZipCode,
		CompanyLocationIdentifier)
VALUES	(@LocationID,
		@CompanyID,
		@AuctionName,
		@City,
		@State,
		@Zip)
		
/*****
*  4  *
******/		
		
INSERT
INTO	Auction.dbo.Datafeed
		(DatafeedID,
		DatafeedCode,
		Name,
		IsActive,
		LoadProcedure,
		RollbackProcedure)
VALUES	(@DataSourceID,						
		@DatafeedCode,
		@CompanyName,,
		@IsActive,
		'LoadData_'+@DatafeedCode,
		'RollbackStagedData_'+@DatafeedCode
		)
GO


/*****
*  5  *
******/
		CREATE TABLE [dbo].[AuctionStaging_ ]
		(
		[StagingID] 			[int] IDENTITY(1,1) NOT NULL,
		[IsGood] 				[bit] NOT NULL 		CONSTRAINT [DF_AuctionStaging_ColumbusFair__IsGood]  DEFAULT ((1)),
		[SaleID] 				[int] NULL,
		[SaleVehicleID] 		[int] NULL,
		[GroupingDescriptionID] [int] NULL,
		[MakeModelGroupingID] 	[int] NULL,
		Fields from the Datafeed file.
)

/*****
*  6  *
******/
		CREATE TABLE [dbo].[AuctionRollback_ ]
		(
		[StagingID] 			[int] NOT NULL,
		[IsGood] 				[bit] NOT NULL,
		[SaleID] 				[int] NULL,
		[SaleVehicleID] 		[int] NULL,
		[GroupingDescriptionID] [int] NULL,
		[MakeModelGroupingID] 	[int] NULL,
		 Fields from the Datafeed file.
)		

GO