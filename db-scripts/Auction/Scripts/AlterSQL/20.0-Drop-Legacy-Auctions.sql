------------------------------------------------------------------------------------------------
--	Adesa Legacy	
------------------------------------------------------------------------------------------------

IF OBJECTPROPERTY(OBJECT_ID('LoadData_Adesa'), 'isProcedure') = 1 
    DROP PROCEDURE LoadData_Adesa

IF objectproperty(object_id('RollbackStagedData_Adesa'), 'isProcedure') = 1
    DROP PROCEDURE RollbackStagedData_Adesa

IF OBJECTPROPERTY(OBJECT_ID('StageData_Adesa'), 'isProcedure') = 1 
        DROP PROCEDURE StageData_Adesa
            
IF objectproperty(object_id('ValidateStagedData_Adesa'), 'isProcedure') = 1
    DROP PROCEDURE ValidateStagedData_Adesa

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AuctionStaging_Adesa]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table [dbo].[AuctionStaging_Adesa]

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[vAdesaImport]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[vAdesaImport]
GO

------------------------------------------------------------------------------------------------
--	ColumbusFair	
------------------------------------------------------------------------------------------------

IF OBJECTPROPERTY(OBJECT_ID('LoadData_ColumbusFair'), 'isProcedure') = 1 
    DROP PROCEDURE LoadData_ColumbusFair

GO

IF objectproperty(object_id('RollbackStagedData_ColumbusFair'), 'isProcedure') = 1
    DROP PROCEDURE RollbackStagedData_ColumbusFair

GO

IF OBJECTPROPERTY(OBJECT_ID('StageData_ColumbusFair'), 'isProcedure') = 1 
        DROP PROCEDURE StageData_ColumbusFair

GO
         
IF OBJECTPROPERTY(OBJECT_ID('ValidateStagedData_ColumbusFair'), 'isProcedure') = 1 
    DROP PROCEDURE ValidateStagedData_ColumbusFair

GO
                    
IF EXISTS ( SELECT  1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleVehicleCompanyData_ColumbusFair]') AND OBJECTPROPERTY(id, N'IsView') = 1 ) 
    DROP VIEW [dbo].[SaleVehicleCompanyData_ColumbusFair]
                    
GO
IF EXISTS ( SELECT  1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleVehicleCompanyData_ColumbusFair_TARGET]') AND OBJECTPROPERTY(id, N'IsView') = 1 ) 
    DROP VIEW [dbo].[SaleVehicleCompanyData_ColumbusFair_TARGET]
                    
GO

IF EXISTS ( SELECT  1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[vColumbusFairImport]') AND OBJECTPROPERTY(id, N'IsView') = 1 ) 
    DROP VIEW [dbo].[vColumbusFairImport]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AuctionStaging_Adesa]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE dbo.AuctionRollback_ColumbusFair

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AuctionStaging_ColumbusFair]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)	
	DROP TABLE dbo.AuctionStaging_ColumbusFair

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SaleVehicleCompanyData_ColumbusFair#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)	
	DROP TABLE dbo.SaleVehicleCompanyData_ColumbusFair#0

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SaleVehicleCompanyData_ColumbusFair#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	DROP TABLE dbo.SaleVehicleCompanyData_ColumbusFair#1
                    
GO

EXEC sp_rename @objname = 'dbo.SaleVehicleCompanyData_Adesa#0', @newname = 'SaleVehicleCompanyData#0'
EXEC sp_rename @objname = 'dbo.SaleVehicleCompanyData_Adesa#1', @newname = 'SaleVehicleCompanyData#1'

GO

IF EXISTS ( SELECT  1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleVehicleCompanyData_Adesa]') AND OBJECTPROPERTY(id, N'IsView') = 1 ) 
    DROP VIEW [dbo].[SaleVehicleCompanyData_Adesa]
                    
GO

IF EXISTS ( SELECT  1 FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[SaleVehicleCompanyData_Adesa_TARGET]') AND OBJECTPROPERTY(id, N'IsView') = 1 ) 
    DROP VIEW [dbo].[SaleVehicleCompanyData_Adesa_TARGET]
                    
GO

------------------------------------------------------------------------------------------------
--	Rename constraints
------------------------------------------------------------------------------------------------

EXEC SP_RENAME 'FK_SaleVehicleCompanyData_Adesa#0__SaleVehicle#0', 'FK_SaleVehicleCompanyData#0__SaleVehicle#0'
EXEC SP_RENAME 'FK_SaleVehicleCompanyData_Adesa#1__SaleVehicle#1', 'FK_SaleVehicleCompanyData#1__SaleVehicle#1'

EXEC SP_RENAME 'PK_SaleVehicleCompanyData_Adesa#0', 'PK_SaleVehicleCompanyData#0'
EXEC SP_RENAME 'PK_SaleVehicleCompanyData_Adesa#1', 'PK_SaleVehicleCompanyData#1'

------------------------------------------------------------------------------------------------
--	Add some new columns
------------------------------------------------------------------------------------------------

ALTER TABLE SaleVehicleCompanyData#0 ADD DealerCodes VARCHAR(40) NULL
ALTER TABLE SaleVehicleCompanyData#1 ADD DealerCodes VARCHAR(40) NULL	

------------------------------------------------------------------------------------------------
--	Rebuild the legacy double-buffered views
------------------------------------------------------------------------------------------------


DECLARE @Buffer TINYINT

SELECT	@Buffer = CurrentBuffer
FROM	BufferState
         

EXECUTE CreateBufferViews 'Sale', '*', '*', @Buffer, 0
         
EXECUTE CreateBufferViews 'SaleVehicleCompanyData'
,'
   SaleVehicleID
  ,OwnerID
  ,ConsignorType
  ,Consignor
  ,right(replicate('' '', 20) + Lot, 20) Lot
  ,right(replicate('' '', 20) + RunNumber, 20) RunNumber
  ,Make
  ,Model
  ,Body
  ,Engine
  ,Doors
  ,M_K
  ,AMSOptions
  ,AMSAnnouncements
  ,UVCCode
  ,DealerCodes'
         ,'*'
         ,@Buffer
         ,0

------------------------------------------------------------------------------------------------
--	Disable 'em
------------------------------------------------------------------------------------------------
         
UPDATE	dbo.Datafeed
SET	IsActive = 0
WHERE	IsActive = 1
	AND DatafeedID IN (14,101)         

------------------------------------------------------------------------------------------------
--	Access Groups
------------------------------------------------------------------------------------------------
	
	
CREATE TABLE dbo.SaleVehicleAccessGroup (
	ProviderID	INT NOT NULL,				
	SaleVehicleID	INT NOT NULL,
	AccessGroupID	INT NOT NULL
	CONSTRAINT PK_SaleVehicleAccessGroup PRIMARY KEY CLUSTERED (ProviderID, SaleVehicleID, AccessGroupID)
	)
GO
CREATE INDEX IX_SaleVehicleAccessGroup_SaleVehicleID ON dbo.SaleVehicleAccessGroup(SaleVehicleID) INCLUDE (AccessGroupID)
	