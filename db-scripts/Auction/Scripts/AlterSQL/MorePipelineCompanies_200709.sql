/*

Update a company name

*/

SET NOCOUNT on

UPDATE Company
 set name = 'Brasher''s Idaho Auto Auction - Boise'
 where name = 'Idaho Auto Auction - Boise'

GO
