/*

Add another validation check and associated new table
 - Set up new "don't include these consignors" list
 - Note that DaimlerChrysler exclusion is not stored in here, as we do pattern
    matching on that one (e.g. " where Consignor like '%DaimlerChrysler%' ")

*/

INSERT ValidationCheck (ValidationCheckID, Title, Description)
 values (1005, 'Exclude named consignor from Adesa', 'Do not include any Adesa vehicles whose exact consignor name appears in the list found in table "ExcludedConsignor_Adesa"')


CREATE TABLE ExcludedConsignor_Adesa
 (
   Consignor  varchar(500)  not null
    constraint PK_ExcludedConsignor_Adesa
     primary key clustered
  ,Category   varchar(50)   not null
 )

INSERT ExcludedConsignor_Adesa (Consignor, Category)
       select 'ARS', 'Ford exclusion'
 union select 'ARS Kitchener O/A Carfinco Inc', 'Ford exclusion'
 union select 'ARS Kitchener OA ADESA Kitchener', 'Ford exclusion'
 union select 'ARS Kitchener OA Allstate Insurance', 'Ford exclusion'
 union select 'ARS Kitchener OA Maji Auto (2000) Inc', 'Ford exclusion'
 union select 'ARS Kitchener OA U Haul International', 'Ford exclusion'
 union select 'Ars/155587 Canada Inc OA Auto Vision', 'Ford exclusion'
 union select 'Ars/AFC Ottawa', 'Ford exclusion'
 union select 'Ars/Asset Recovery', 'Ford exclusion'
 union select 'Ars/Auto Collection J F M Inc', 'Ford exclusion'
 union select 'Ars/Automobiles 105', 'Ford exclusion'
 union select 'ARS/AVIS Budget Car Rental LLC', 'Ford exclusion'
 union select 'Ars/Caisse Populaire Pointe Gatineau', 'Ford exclusion'
 union select 'ARS/Carfinco', 'Ford exclusion'
 union select 'Ars/Casino Gatineau Acura', 'Ford exclusion'
 union select 'ARS/Deragon Location Inc', 'Ford exclusion'
 union select 'ARS/Discount Montreal Corporate', 'Ford exclusion'
 union select 'ARS/Donlen Corporation', 'Ford exclusion'
 union select 'ARS/Hertz Inc.', 'Ford exclusion'
 union select 'ARS/J W Zahr Enterprises', 'Ford exclusion'
 union select 'Ars/Kollbec Gatineau Chrysler Jeep Inc', 'Ford exclusion'
 union select 'Ars/Les Entreprises Yvon Roy', 'Ford exclusion'
 union select 'Ars/Location D''Autos Et Camions Alliance De Gatine', 'Ford exclusion'
 union select 'ARS/Location Jean Legare Ltee', 'Ford exclusion'
 union select 'Ars/Location Legare Ltee', 'Ford exclusion'
 union select 'ARS/Location Sauvageau Inc', 'Ford exclusion'
 union select 'ARS/Maji Auto (2000) Inc', 'Ford exclusion'
 union select 'Ars/Obsession Automobile Plus Inc', 'Ford exclusion'
 union select 'Ars/Richard Kelly Automobiles', 'Ford exclusion'
 union select 'Ars/Toit d`Auto Canadien Inc', 'Ford exclusion'
 union select 'Ars/Tom Woodford Limited(R)', 'Ford exclusion'
 union select 'Ars/Villa Toyota', 'Ford exclusion'
 union select 'Ford Credit', 'Ford exclusion'
 union select 'Ford Credit Can- AM', 'Ford exclusion'
 union select 'Ford Credit Canada Limited', 'Ford exclusion'
 union select 'Ford Credit Canada Limited - Oakville', 'Ford exclusion'
 union select 'Ford Credit Canada Limited (SP)', 'Ford exclusion'
 union select 'Ford Credit Canada Ltd (Commercial Lending)', 'Ford exclusion'
 union select 'Ford Motor Company', 'Ford exclusion'
 union select 'Ford Motor Company Of Canada (CSV)', 'Ford exclusion'

GO
