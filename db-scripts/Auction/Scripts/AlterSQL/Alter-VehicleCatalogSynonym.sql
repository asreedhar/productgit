IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'VehicleCatalog')
DROP SYNONYM [dbo].[VehicleCatalog]
GO

CREATE SYNONYM dbo.VehicleCatalog 
FOR [IMT].dbo.VehicleCatalog
GO