/*

Load the static data into the tables
 - BufferState
 - Company
 - AuctionType
 - Location
 - ValidationCheck


Create the "Buffer" views
 - Sale
 - SaleVehicle
 - SaleVehicleCompanyData_Adesa

*/

SET NOCOUNT on


--  Initialized views and BufferState table
INSERT BufferState (CurrentBuffer, LastUpdated, FileName, DataRows, ValidRows)
 values (0, getdate(), '<Initialized>', 0, 0)

EXECUTE CreateBufferViews 'Sale', '*', 0, null
EXECUTE CreateBufferViews 'SaleVehicle', '*', 0, null
EXECUTE CreateBufferViews 'SaleVehicleCompanyData_Adesa', '*', 0, null
GO


--  Load Company
INSERT Company (CompanyID, Name) values (1, 'Adesa')


--  Load AuctionType
INSERT AuctionType (AuctionTypeCode, Description) values (0, 'Unknown')
INSERT AuctionType (AuctionTypeCode, Description) values (1, 'Open')
INSERT AuctionType (AuctionTypeCode, Description) values (2, 'Closed')


--  Load Location
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Atlanta', 'Fairburn', 'GA', '30213', '237')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Austin', 'Austin', 'TX', '78754', '219')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Birmingham', 'Moody', 'AL', '35004', '203')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Boston', 'Framingham', 'MA', '01702', '218')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Buffalo', 'Akron', 'NY', '14001', '205')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Charlotte', 'Charlotte', 'NC', '28273', '206')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Cincinnati Dayton', 'Franklin', 'OH', '45005', '207')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Cleveland', 'Northfield', 'OH', '44067', '208')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Colorado Springs', 'Fountain', 'CO', '80817', '239')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Concord', 'Acton', 'MA', '01720', '232')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Dallas', 'Mesquite', 'TX', '75149', '220')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Des Moines', 'Grimes', 'IA', '50111', '233')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Golden Gate', 'Tracy', 'CA', '95377', '240')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Houston', 'Houston', 'TX', '77086', '225')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Indianapolis', 'Plainfield', 'IN', '46168', '202')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Jacksonville', 'Jacksonville', 'FL', '32219', '217')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Kansas City', 'Lee''s Summit', 'MO', '64081', '241')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Knoxville', 'Lenoir City', 'TN', '37771', '210')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Lansing', 'Dimondale', 'MI', '48821', '230')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Lexington', 'Lexington', 'KY', '40509', '211')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Little Rock', 'North Little Rock', 'AR', '72117', '249')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Long Island', 'Yaphank', 'NY', '11980', '209')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Los Angeles', 'Mira Loma', 'CA', '91752', '234')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Memphis', 'Memphis', 'TN', '38118', '212')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA New Jersey', 'Manville', 'NJ', '08835', '216')
--  Newnan is from Nicole's email...
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Newnan', 'Newnan', 'GA', '30213', '214')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Ocala', 'Ocala', 'FL', '34474', '248')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Orlando Sanford', 'Sanford', 'FL', '32773', '242')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Phoenix', 'Chandler', 'AZ', '85226', '243')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Pittsburgh', 'Mercer', 'PA', '16137', '221')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Sacramento', 'Sacramento', 'CA', '95826', '227')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA San Antonio', 'San Antonio', 'TX', '78227', '222')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA San Diego', 'San Diego', 'CA', '92154', '236')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Seattle', 'Auburn', 'WA', '98002', '245')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Shreveport', 'Shreveport', 'LA', '71119', '231')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Southern Indiana', 'Edinburgh', 'IN', '46124', '228')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA St. Louis', 'Barnhart', 'MO', '63012', '229')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Tampa', 'Tampa', 'FL', '33619', '244')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Tulsa', 'Tulsa', 'OK', '74116', '204')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Washington DC', 'Dulles', 'VA', '20166', '223')
INSERT Location (CompanyID, Name, City, State, ZipCode, CompanyLocationIdentifier)
 values (1, 'ADESA Wisconsin', 'Portage', 'WI', '53901', '224')

/*

Load ValidationCheck.  Code schema:
  1000's - data domain checks (is it integer, is it date, is string too long, etc.)
  2000's - relational integrity checks (table lookups work, set consistancy checks, etc.)

*/

SET NOCOUNT on

INSERT ValidationCheck (ValidationCheckID, Title, Description)
 values
  (
    2010
   ,'Unknown or Invalid Location'
   ,'Auction location is not found in the Location table.  Could be new, located out of the country, or otherwise invalid'
  )

INSERT ValidationCheck (ValidationCheckID, Title, Description)
 values
  (
    2020
   ,'Duplicate Entry'
   ,'Duplicate entry, as defined by {Location, saledate, saletype, lot, opensale, RunNumber}'
  )

INSERT ValidationCheck (ValidationCheckID, Title, Description)
 values
  (
    2030
   ,'Invalid VIN'
   ,'Could not determine make model grouping based on the VIN'
  )
GO
