
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_WholesaleInventorySalesFldwA]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_WholesaleInventorySalesFldwA] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_WholesaleInventorySalesFldwA] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_WholesaleInventorySalesFldwA]
	@PeriodID int,
	@DealerID varchar(40)
AS

SET NOCOUNT ON

DECLARE @Buckets TABLE (
	counter tinyint,
	daysLow int,
	daysHigh int,
	saleDesc varchar(1),
	bucketDesc varchar(20)
)

INSERT INTO @Buckets (counter, daysLow, daysHigh, saleDesc, bucketDesc) VALUES (0,0,29,'W','Wholesaled 0-29 Days')
INSERT INTO @Buckets (counter, daysLow, daysHigh, saleDesc, bucketDesc) VALUES (1,30,999,'W','Aged Wholesale')

SELECT	BU.BusinessUnit 'BusinessUnitName',
		B.counter,
		COUNT(1) DealCount,
		B.bucketDesc BucketDesc,
		B.SaleDesc,
		P.BeginDate 'FromDate',
		P.EndDate 'ToDate'

FROM	[FLDW].dbo.BusinessUnit BU
Join	[FLDW].dbo.Inventory I ON BU.BusinessUnitID = I.BusinessUnitID
Join	[FLDW].dbo.VehicleSale VS on I.InventoryID = VS.InventoryID
Join	[FLDW].dbo.Vehicle V on I.VehicleID = V.VehicleID And I.BusinessUnitID = V.BusinessUnitID
Join	@Buckets as B on ((I.DaysToSale between B.daysLow and B.daysHigh) AND B.SaleDesc = VS.SaleDescription)
CROSS JOIN [FLDW].dbo.Period_D P
WHERE 	BU.BusinessUnitID = @DealerID
And		I.InventoryType = 2
AND		P.PeriodID = @PeriodID
And		VS.DealDate Between P.BeginDate And P.EndDate

Group By BU.BusinessUnit, B.counter, B.SaleDesc, B.bucketDesc, P.BeginDate, P.EndDate 
GO
