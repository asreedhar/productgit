IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.DealerGroupCollection#Fetch') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE dbo.DealerGroupCollection#Fetch AS SELECT 1')
GO
 
ALTER PROCEDURE [dbo].[DealerGroupCollection#Fetch]

AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.DealerGroupCollection#Fetch.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns DealerGroupID and DealerGroupName from IMT..BusinessUnit
 * 
 * Parameters
 * ----------
 * 
 * Exceptions
 * ----------
 *
 * History
 * ----------
 * 
 * BYF	2009-09-29
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON;

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

-- Done this way to get dealer groups that actually have dealers in them!

SELECT G.BusinessunitID AS DealerGroupID,
	   G.BusinessUnit AS DealerGroup
FROM [IMT].dbo.BusinessUnit D
JOIN [IMT].dbo.BusinessUnitRelationship BR
	ON D.BusinessUnitID = BR.BusinessUnitID
JOIN [IMT].dbo.BusinessUnit G
	ON BR.ParentID = G.BusinessUnitID
WHERE D.Active = 1
AND D.BusinessUnitTypeID = 4 --Dealer
GROUP BY G.BusinessunitID, G.BusinessUnit
ORDER BY G.BusinessUnit, G.BusinessunitID

RETURN 0

GO