
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_Margin#Pricing]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_Margin#Pricing] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_Margin#Pricing] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_Margin#Pricing]
        @DealerID VARCHAR(6) ,
        @SegmentID INT = -1 ,
        @AgeBucketID INT = 0,
        @TargetThreshold INT,
        @SummaryInformation TINYINT =0,
        @StrategyID		TINYINT = NULL -- SEE FLDW.[dbo].[GetStockingReportCube] for documentation
AS 
/******************************************************************************************************
*	Routine:		dbo.rc_Margin#Pricing
*	Purpose:		Procedure for Summary Information for Margin Report
*
*	History:	 	MAK	05/18/2010	This procedure is currently used in the Margin report
*					to return summary information.  After QA and some tuning, will consider
*					using this procedure for the entire report.
*
*	History:	 	MAK	07/28/2010	This procedure is currently used in the Margin report
*					MAK	08/09/2010	Update Target Threshold to INT due to ReportMetadata.xml not liking floats.
*					MAK 	08/27/2010	Total market average has been fixed.
*					MAK	09/20/2010	Fixed MDS calculation.
*				    JMW 07/29/2011 BUGZID 15625 -- Added Over/Under for % of Market Avg Summary Columns
*					JMW 10/12/2011 BUGZID 15979
*					JMW 10/18/2011 BUGZID 17452
*					JMW 03/14/2012 BUGZID 18507 -- Add Precision Search Column 
*					JMW 03/14/2012 BUGZID 18671 - Changed where condition to count everything besides 
*												  Wholesale and Sold as Retail
*
*******************************************************************************************************/

IF (@StrategyID = 0) SELECT @StrategyID = NULL	

DECLARE @MarketDaysSupplyBasePeriod SMALLINT
DECLARE @Threshold DECIMAL(2,2)

SET @Threshold =CAST(@TargetThreshold AS DECIMAL(4,2))/100

SELECT  @MarketDaysSupplyBasePeriod = MarketDaysSupplyBasePeriod
FROM    IMT.dbo.DealerPreference_Pricing DPP
WHERE   DPP.BusinessUnitID = @DealerID       

SET @MarketDaysSupplyBasePeriod = COALESCE(@MarketDaysSupplyBasePeriod, 90)

DECLARE @HighDays INT
DECLARE @LowPrice INT
DECLARE @HighPrice INT
DECLARE @LowPriceLimit SMALLINT
DECLARE @HighPriceLimit INT
DECLARE @MaxBucketForAge SMALLINT
DECLARE @PingURLPrefix VARCHAR(200)


 		--  This is the percentage points +/- the Market Average % limit
SET @MaxBucketForAge =100			--	Buckets 1-6 are the actual Age Buckets; Anything >=100 is not.
SET @HighDays =99999				--	This is the Maximum age of a vehicle
SET @LowPrice=1000					--	This is the bottom threshhold of a mispriced vehicle.  Anything below this value is mispriced.	
SET @HighPrice =100000				--	This is the top threshhold of a mispriced vehicle.  Anything above this value is mispriced.	
SET @LowPriceLimit =0				--	This is the bottom of the range of a mispriced vehicle.  LowPriceLimit to LowPrice is mispriced.
SET @HighPriceLimit =999999			--	This is the top of the range of a mispriced vehicle.  HighPrice to HighpriceLimit is mispriced.

SET @PINGURLPrefix ='https://max.firstlook.biz/pricing/Pages/Internet/VehiclePricingAnalyzer.aspx?'
 
 
DECLARE @AgeBuckets TABLE (
	ColumnBucketName VARCHAR(20),
	BucketName VARCHAR(20),
	BucketNumber SMALLINT,
	MarketAverageTarget REAL,
	LowDays SMALLINT,
	HighDays INT,
	LowPrice INT,
	HighPrice INT)
	
/*	There are 3 types of Buckets that are used to describe summary information in the Pricing Margin Report.
	Priced Buckets		-	1,2,3,4,5,6		These are buckets for properly priced vehicles (vehicles with prices >=$1000 and<$=100,000) 
											within a given age range.
	Non-priced Buckets	-	101,102			These are buckets for improperly priced vehicles.  Vehicles with prices <$1000 or >$100,000
											are 'Mispriced' vehicles.  Vehicles with prices of '$0' or NULL are 'Unpriced' vehicles.
	Summary Buckets		-	100,103			These are buckets for summary vehicle information.  'Total Priced' (Bucket 100) is summary
											information for buckets 1-6. 'Total Inventory' (Bucket 103) is summary information for
											buckets 1-6 and 101 and 102.  These are not included in the AgeBucketTable										

*/
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('Col0_21','0-21',6,1.10,0,21,@LowPrice,@HighPrice)
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('Col22_29','22-29',5,1.05,22,29,@LowPrice,@HighPrice)
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('Col30_39','30-39',4,1.00,30,39,@LowPrice,@HighPrice)
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES ('Col40_49','40-49',3,.97,40,49,@LowPrice,@HighPrice)
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('Col50_59','50-59',2,.95,50,59,@LowPrice,@HighPrice)
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('Col60_','60+',1,.90,60,@HighDays,@LowPrice,@HighPrice)
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('ColUnpriced','Unpriced:',101,NULL,0,@HighDays,@LowPriceLimit,@LowPrice-1)
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('ColMispriced','Mispriced',102,NULL,0,@HighDays,@HighPrice+1,@HighPriceLimit)

/* The #TempSearches table collects Market information for a given piece of inventory usining it's coressponding Search from the Market.Pricing.Search table.
	If the Precisiion Search (SearchTypeID =4) has sufficient information (i.e. and Avg. ListPrice >0), use the information from the precision search.  If
	not, use the YMM Search (SearchTypeID =1) information.
*/
IF object_id('tempdb..#TempSearches')IS NOT NULL DROP TABLE #TempSearches
CREATE TABLE #TempSearches
    (
      SearchID INT ,
      OwnerID INT ,
      VehicleEntityID INT ,
      PINGURL VARCHAR(500) ,
      SearchTypeID TINYINT ,
      AvgListPrice REAL ,
      SumListPrice INT ,
      NumberOfListings INT ,
      NumberOFListingsWithPrice INT ,
      NumberOfSales INT
    )

INSERT  INTO #TempSearches
        ( SearchID ,
          OwnerID ,
          VehicleEntityID ,
          PINGURL
        )
        SELECT  S.SearchID ,
                S.OwnerID ,
                S.VehicleEntityID ,
                @PINGURLPrefix + 'oh=' + CAST(O.Handle AS VARCHAR(36))
                + '&vh=' + CAST(S.VehicleHandle AS VARCHAR(36)) + '&sh='
                + CAST(S.Handle AS VARCHAR(36))
        FROM    Market.Pricing.Search S
                JOIN Market.Pricing.Owner O ON S.OwnerID = O.OwnerID
        WHERE   O.OwnerEntityID = @DealerID
                AND O.OwnerTypeID = 1
                AND S.VehicleEntityTypeID = 1

UPDATE  TS
SET     SearchTypeID = 4 ,
        AvgListPrice = X.AvgListPrice ,
        SumListPrice = X.SumListPrice ,
        NumberOfListings = X.NumberOfListings ,
        NumberOfListingsWithPrice = X.NumberOfListingsWithPrice
FROM    ( SELECT    SearchID ,
                    SearchTypeID ,
                    CAST(SUM(SumListPrice) AS REAL) AS SumListPrice ,
                    CAST(SUM(Units) AS REAL) AS NumberOfListings ,
                    CAST(SUM(ComparableUnits) AS REAL) AS NumberOfListingsWithPrice ,
                    CAST(SUM(SumListPrice)
                    / COALESCE(NULLIF(SUM(ComparableUnits), 0), 1) AS INT) AS AvgListPrice
          FROM      Market.Pricing.SearchResult_A WITH ( NOLOCK )
          WHERE     SearchResultTypeID = 1 AND SearchTypeID=4
          GROUP BY  SearchID ,
                    SearchTypeID
          HAVING    CAST(SUM(SumListPrice)
                    / COALESCE(NULLIF(SUM(ComparableUnits), 0), 1) AS INT) > 0
        ) X
        JOIN #TempSearches TS ON X.SearchID = TS.SearchID


UPDATE  TS
SET     SearchTypeID = 1 ,
        AvgListPrice = X.AvgListPrice ,
        SumListPrice = X.SumListPrice ,
        NumberOfListings = X.NumberOfListings ,
        NumberOfListingsWithPrice = X.NumberOfListingsWithPrice
FROM    ( SELECT    SearchID ,
                    SearchTypeID ,
                    CAST(SUM(SumListPrice) AS REAL) AS SumListPrice ,
                    CAST(SUM(Units) AS REAL) AS NumberOfListings ,
                    CAST(SUM(ComparableUnits) AS REAL) AS NumberOfListingsWithPrice ,
                    CAST(SUM(SumListPrice)
                    / COALESCE(NULLIF(SUM(ComparableUnits), 0), 1) AS INT) AS AvgListPrice
          FROM      Market.Pricing.SearchResult_A WITH ( NOLOCK )
          WHERE     SearchResultTypeID = 1 AND SearchTypeID=1
          GROUP BY  SearchID ,
                    SearchTypeID
          HAVING    CAST(SUM(SumListPrice)
                    / COALESCE(NULLIF(SUM(ComparableUnits), 0), 1) AS INT) > 0
        ) X
        JOIN #TempSearches TS ON X.SearchID = TS.SearchID
WHERE   TS.SearchTypeID IS NULL
                         
UPDATE  TS
SET     NumberOfSales = XS.NumberOfSales
FROM    #TempSearches TS
        JOIN ( SELECT   SearchID ,
                        SearchTypeID ,
                        CAST(SUM(Units) AS REAL) AS NumberOfSales
               FROM     Market.Pricing.SearchResult_A A
                        JOIN Market.Pricing.Owner O ON A.OwnerID = O.OwnerID
               WHERE    SearchResultTypeID = 2
                        AND OwnerEntityID = @DealerID
                        AND OwnerTypeID = 1
               GROUP BY SearchID ,
                        SearchTypeID
             ) XS ON XS.SearchID = TS.SearchID
                     AND XS.SearchTypeID = TS.SearchTypeID	                   
                                  
	DECLARE @IBV table (InventoryID int, BookoutID int, ThirdPartyCategoryID int, ThirdPartyDescription varchar(30), ThirdPartyName varchar(30) null, ThirdPartyCategory varchar(30), IsValid tinyint, BookoutStatusID tinyint, CurrentValue int null, CurrentDT datetime, BookoutValueTypeID tinyint )

	-- USE THE FULL OUTER JOIN TO CHECK WHETHER THE PASSED STATUS CODES MATCH THE DEFAULT FILTER 
	-- IF SO, WE CAN USE FLDW'S PRE-BUILT VERSION

	INSERT
	INTO	@IBV	
	SELECT	InventoryID, BookoutID, ThirdPartyCategoryID, ThirdPartyDescription, ThirdPartyName, ThirdPartyCategory, IsValid, BookoutStatusID, CurrentValue, CurrentDT, BookoutValueTypeID 
	FROM	[FLDW]..GetActiveInventoryBookoutValues(@DealerID)

   
DECLARE @Results TABLE
    (
	  Chosen INT,
	  VehicleCount INT,
	  BucketName VARCHAR(2000),
      BusinessUnit VARCHAR(50) ,
      Age SMALLINT ,
      BucketNumber TINYINT ,
      AgeBucketName VARCHAR(20) ,
      SegmentID INT ,
      YEAR SMALLINT ,
      MakeModelTrim VARCHAR(100) ,
      Certified CHAR(1) ,
      StockNumber VARCHAR(20) ,
      PINGURL VARCHAR(500) ,
      Mileage INT ,
      UnitCost INT ,
      InternetPrice INT ,
      PercentofMarketAverage REAL ,
      InternetVsCost INT ,
      MarketAvgVsCost INT ,
      MktAvgListPrice INT ,
      MarketDaysSupply INT ,
      OverTarget SMALLINT ,
      UnderTarget SMALLINT ,
      WithinTarget SMALLINT ,
      SumListPrice REAL ,
      NumberOfListingsWithPrice REAL ,
      NumberOfSales REAL ,
      NumberOfListings REAL,
      CurrentValue REAL,
      BookoutStatusID INT,
      CurrentValue_2nd REAL,
      BookoutStatusID_2nd INT,
      SearchTypeId INT
    )
 
INSERT  INTO @Results
        ( BusinessUnit ,
          Age ,
          BucketNumber ,
          AgeBucketName ,
          SegmentID ,
          Year ,
          MakeModelTrim ,
          Certified ,
          StockNumber ,
          PINGURL ,
          Mileage ,
          UnitCost ,
          InternetPrice ,
          MktAvgListPrice ,
          InternetVsCost ,
          MarketAvgVsCost ,
          PercentofMarketAverage ,
          MarketDaysSupply ,
          OverTarget ,
          UnderTarget ,
          WithinTarget ,
          SumListPrice ,
          NumberOfListingsWithPrice ,
          NumberOfSales ,
          NumberOfListings,
          CurrentValue,
		  BookoutStatusID,
		  CurrentValue_2nd,
		  BookoutStatusID_2nd,
		  SearchTypeId
        )
        SELECT  BU.BusinessUnit ,
                I.AgeInDays Age ,
                AB.BucketNumber ,
                AB.BucketName ,
                V.SegmentID ,
                V.VehicleYear ,
                MMG.Make + ' ' + MMG.Model + ' ' + COALESCE(V.VehicleTRIM, '') ,
                CASE WHEN I.Certified = 1 THEN 'Y'
                     ELSE 'N'
                END ,
                I.StockNumber ,
                PINGURL ,
                I.MileageReceived ,
                CAST(I.UnitCost AS INT) UnitCost ,
                CAST(I.ListPrice AS INT) InternetPrice ,
                COALESCE(TS.AvgListPrice, 0) MktAvgListPrice ,
                CAST(I.ListPrice - I.UnitCost AS INT) InternetVsCost ,
                CAST(TS.AvgListPrice - I.UnitCost AS INT) MarketAvgVsCost ,
                CASE WHEN SumListPrice > 0
                          AND NumberOfListingsWithPrice > 0
                     THEN ListPrice / ( SumListPrice
                                        / NumberOfListingsWithPrice )
                     ELSE 0
                END AS PercentofMarketAverage ,
                CASE WHEN NumberOfSales > 0
                          AND NumberOfListings > 0
                     THEN ROUND(( NumberOfListings
                                  * @MarketDaysSupplyBasePeriod )
                                / NumberOfSales, 0)
                     ELSE 0
                END AS Market_Days_Supply ,
                CASE WHEN ListPrice / ( SumListPrice
                                        / NumberOfListingsWithPrice ) > @Threshold
                          + AB.MarketAverageTarget THEN 1
                     ELSE 0
                END ,
                CASE WHEN ListPrice / ( SumListPrice
                                        / NumberOfListingsWithPrice ) < ( AB.MarketAverageTarget
                                                              - @Threshold )
                     THEN 1
                     ELSE 0
                END ,
                CASE WHEN ListPrice / ( SumListPrice
                                        / NumberOfListingsWithPrice ) <= @Threshold
                          + AB.MarketAverageTarget
                          AND ListPrice / ( SumListPrice
                                            / NumberOfListingsWithPrice ) >= ( AB.MarketAverageTarget
                                                              - @Threshold )
                     THEN 1
                     ELSE 0
                END ,
                SumListPrice ,
                NumberOfListingsWithPrice ,
                NumberOfSales ,
                NumberOfListings,
                IBV1.CurrentValue, IBV1.BookoutStatusID, IBV2.CurrentValue CurrentValue_2nd, 
                IBV2.BookoutStatusID BookoutStatusID_2nd,
                TS.SearchTypeId
        FROM    FLDW.dbo.InventoryActive I WITH ( NOLOCK )
                JOIN IMT.dbo.BusinessUnit BU WITH ( NOLOCK ) ON I.BusinessUnitID = BU.BusinessUnitID
                JOIN IMT.dbo.DealerPreference DP WITH ( NOLOCK ) ON dp.BusinessUnitID = I.BusinessUnitID
                JOIN IMT.dbo.DealerPreference_Pricing AS DPP WITH ( NOLOCK ) ON DPP.BusinessUnitID = BU.BusinessUnitID
                JOIN Market.Pricing.DistanceBucket AS DB WITH ( NOLOCK ) ON DB.Distance = DPP.PingII_DefaultSearchRadius
                JOIN IMT.dbo.Vehicle V WITH ( NOLOCK ) ON I.VehicleID = V.VehicleID
                JOIN IMT.dbo.MakeModelGrouping MMG WITH ( NOLOCK ) ON MMG.MakeModelGroupingID = V.MakeModelGroupingID
                LEFT JOIN #TempSearches TS ON I.InventoryID = TS.VehicleEntityID
                LEFT JOIN IMT.dbo.Segment AS S2 WITH ( NOLOCK ) ON S2.SegmentID = V.SegmentID --@SegmentID
                LEFT JOIN FLDW.dbo.GetInventoryManagementPlanStrategy(@DealerID,GETDATE()) CS ON I.InventoryID = CS.InventoryID
                LEFT JOIN @IBV IBV1 on I.InventoryID = IBV1.InventoryID and DP.BookOutPreferenceID = IBV1.ThirdPartyCategoryID
				LEFT JOIN @IBV IBV2 on isnull(DP.BookOutPreferenceSecondID,0) <> 0 and  I.InventoryID = IBV2.InventoryID and isnull(DP.BookOutPreferenceSecondID,0) = IBV2.ThirdPartyCategoryID
                JOIN @AgeBuckets AB ON COALESCE(I.AgeInDays, 0) BETWEEN AB.LowDays
                                                              AND
                                                              AB.HighDays
                                       AND CAST(I.ListPrice AS INT) BETWEEN AB.LowPRice
                                                              AND
                                                              AB.HighPrice
        WHERE   I.BusinessUnitID = @DealerID
                AND I.InventoryActive = 1
                AND I.InventoryType = 2
                AND ( @StrategyID IS NULL -- ALL
					  OR ( @StrategyID = 1 AND  ( CS.AIP_EventCategoryID IS NULL OR CS.AIP_EventCategoryID IN (1,4) ) ) -- NULLs, Retail, and Other are all considered Retail (BUGZID: 18671)
					  OR ( @StrategyID = CS.AIP_EventCategoryID )  ) -- Wholesale, Sold

IF ( COALESCE(@SummaryInformation, 0) = 0 ) 
    BEGIN
        SELECT  *
        FROM    @Results
        WHERE   ( @SegmentID = -1
                  OR @SegmentID = SegmentID
                )
                AND ( @AgeBucketID = 0
                      OR @AgeBucketID = BucketNumber
                    )
        ORDER BY BucketNumber ,
                Age
    END
IF ( COALESCE(@SummaryInformation, 0) = 1 ) 
    BEGIN

	DECLARE @RetTable TABLE (
	Chosen int,
	BucketNumber int,
	BucketName varchar(50),
	VehicleCount int,
	OverCount int,
	UnderCount int,
	AverageAge int,
	PercentofMarketAverage float,
	MarketAverageTarget float,
	NumberOverTarget int,
	NumberUnderTarget int,
	NumberWithinTarget int,
	MarketDaysSupply int,
	PotentialGrossPerCar int,
	TotalUnitCost int,
	TotalInternetPrice int,
	TotalPotentialGross int)
		
	Insert into @RetTable

        SELECT  CASE WHEN B.BucketNumber = @AgeBucketID THEN 1
                     ELSE 0
                END AS Chosen ,
                B.BucketNumber ,
                B.BucketName ,
                COUNT(UnitCost) AS VehicleCount ,
				-1 AS OverCount,
				-1 AS UnderCount,
                AVG(Age) AS AverageAge ,
                AVG(PercentofMarketAverage) AS PercentofMarketAverage ,
                B.MarketAverageTarget AS MarketAverageTarget ,
                COALESCE(SUM(OverTarget), 0) AS NumberOverTarget ,
                COALESCE(SUM(UnderTarget), 0) AS NumberUnderTarget ,
                COALESCE(SUM(WithinTarget), 0) AS NumberWithinTarget ,
                CASE WHEN SUM(NumberOfSales) > 0
                          AND SUM(NumberOfListings) > 0
                     THEN ROUND(SUM(NumberOfListings) / ( SUM(NumberOfSales)
                                                          / CAST(@MarketDaysSupplyBasePeriod AS REAL) ),
                                0)
                     ELSE 0
                END AS MarketDaysSupply ,
                COALESCE(( SUM(InternetPrice) - SUM(UnitCost) ) / COUNT(*), 0) AS PotentialGrossPerCar ,
                COALESCE(SUM(UnitCost), 0) AS TotalUnitCost ,
                COALESCE(SUM(InternetPrice), 0) AS TotalInternetPrice ,
                COALESCE(SUM(InternetPrice) - SUM(UnitCost), 0) AS TotalPotentialGross
        FROM    @AgeBuckets B
                LEFT JOIN @Results R ON B.BucketNumber = R.BucketNumber
        GROUP BY B.BucketNumber ,
                B.BucketName ,
                B.MarketAverageTarget
        UNION
        SELECT  CASE WHEN @AgeBucketID = 0 THEN 1
                     ELSE 0
                END AS Chosen ,
                100 ,
                'Total Priced:' ,
                COUNT(*) AS VehicleCount ,
				-1 AS OverCount,
				-1 AS UnderCount,
                AVG(Age) AS AverageAge ,
                CASE WHEN SUM(SumListPrice) > 0
                     THEN ( CAST(SUM(InternetPrice) AS REAL)
                            / ( CAST(COUNT(*) AS REAL) ) )
                          / ( CAST(SUM(SumListPrice) AS REAL)
                              / ( CAST(SUM(NumberOfListingsWithPrice) AS REAL) ) )
                     ELSE 0
                END AS PercentofMarketAverage ,
                NULL AS MarketAverageTarget ,
                SUM(OverTarget) AS NumberOverTarget ,
                SUM(UnderTarget) AS NumberUnderTarget ,
                SUM(WithinTarget) AS NumberWithinTarget ,
                CASE WHEN SUM(NumberOfSales) > 0
                     THEN ROUND(SUM(NumberOfListings) / ( SUM(NumberOfSales)
                                                          / CAST(@MarketDaysSupplyBasePeriod AS REAL) ),
                                0)
                     ELSE 0
                END AS MarketDaysSupply ,
                ( SUM(InternetPrice) - SUM(UnitCost) ) / COUNT(*) AS PotentialGrossPerCar ,
                SUM(UnitCost) AS TotalUnitCost ,
                SUM(InternetPrice) AS TotalInternetCost ,
                SUM(InternetPrice) - SUM(UnitCost) AS TotalPotentialGross
        FROM    @Results
        WHERE   BucketNumber < @MaxBucketForAge
        UNION
        SELECT  0 AS Chosen ,
                103 ,
                'Total Inventory:' ,
                COUNT(*) AS VehicleCount ,
				-1 AS OverCount,
				-1 AS UnderCount,
                AVG(Age) AS AverageAge ,
                0 PercentofMarketAverage ,
                NULL AS MarketAverageTarget ,
                0 AS NumberOverTarget ,
                0 AS NumberUnderTarget ,
                0 AS NumberWithinTarget ,
                0 AS MarketDaysSupply ,
                0 AS PotentialGrossPerCar ,
                0 AS TotalUnitCost ,
                0 AS TotalInternetCost ,
                0 AS TotalPotentialGross
        FROM    @Results
        ORDER BY BucketNumber 

	UPDATE @retTable SET PercentofMarketAverage = (SELECT Avg(R.PercentofMarketAverage) FROM @retTable R WHERE R.BucketNumber < @MaxBucketForAge)
	WHERE BucketNumber = 100

	DECLARE @buckNum int
	DECLARE @percentMA float

	DECLARE overUnder CURSOR FOR
	select BucketNumber, PercentofMarketAverage from @RetTable
	
	OPEN overUnder
	FETCH NEXT FROM overUnder
	INTO @buckNum, @percentMA;


	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		UPDATE @retTable
		SET OverCount = (SELECT Count(UnitCost) FROM @Results R WHERE R.BucketNumber = @buckNum
															AND R.PercentofMarketAverage >= @percentMA)
		WHERE BucketNumber = @buckNum


		UPDATE @retTable
		SET UnderCount = (SELECT Count(UnitCost) FROM @Results R WHERE R.BucketNumber = @buckNum
															AND R.PercentofMarketAverage < @percentMA)
		WHERE BucketNumber = @buckNum
	
	FETCH NEXT FROM overUnder
    INTO @buckNum, @percentMA;
	END

	CLOSE overUnder;
	DEALLOCATE overUnder;

	DECLARE @sumOver INT, @sumUnder INT
	SELECT @sumOver = SUM(OverCount) FROM @retTable WHERE BucketNumber < 100
	SELECT @sumUnder = SUM(UnderCount) FROM @retTable WHERE BucketNumber < 100

	UPDATE @retTable 
	SET  OverCount = @sumOver
	    ,UnderCount = @sumUnder
	WHERE BucketNumber = 100			

	SELECT * from @retTable

    END
IF ( COALESCE(@SummaryInformation, 0) = 2 ) 
    BEGIN
        SELECT  *
        FROM    ( SELECT    ColumnBucketName ,
                            COUNT(UnitCost) AS UC
                  FROM      @AgeBuckets AG
                            LEFT JOIN @Results R ON AG.BucketNumber = R.BucketNumber
                  GROUP BY  AG.ColumnBucketName
                ) X PIVOT
( MAX(UC) FOR ColumnBucketName IN ( [Col60_], [Col50_59], [Col40_49],
                                    [Col30_39], [Col22_29], [Col0_21],
                                    [ColMispriced], [ColUnpriced] ) ) AS P
    END

GO