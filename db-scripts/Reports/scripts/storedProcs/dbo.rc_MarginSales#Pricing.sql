
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_MarginSales#Pricing]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[rc_MarginSales#Pricing]
GO

CREATE PROCEDURE [dbo].[rc_MarginSales#Pricing]
        @DealerID VARCHAR(6) ,
        @SegmentID INT = -1 ,
        @AgeBucketID INT = 0,
        @TargetThreshold INT,
        @SummaryInformation TINYINT =0,
        @StrategyID		TINYINT = NULL,	-- SEE FLDW.[dbo].[GetStockingReportCube] for documentation
        @PeriodID TINYINT
AS 
/******************************************************************************************************
*	Routine:		dbo.rc_MarginSales#Pricing
*	Purpose:		Procedure for Summary Information for Margin Sales Report
*
*	History:	 	JMW 10/18/2011 Created, BUGZID: 17387
*					JMW 02/29/2012 Added return of SearchTypeID, BUGZID: 19407
*						12/24/2013 Checked ISNULL for ListPrice in InventoryInActiveTable
*******************************************************************************************************/

DECLARE @saleDescription CHAR(1)

SELECT @saleDescription = NULL
IF (@StrategyID = 1) SELECT @saleDescription = 'R'
IF (@StrategyID = 2) SELECT @saleDescription = 'W'
	

DECLARE @MarketDaysSupplyBasePeriod SMALLINT
DECLARE @Threshold DECIMAL(2,2)

SET @Threshold =CAST(@TargetThreshold AS DECIMAL(4,2))/100

SELECT  @MarketDaysSupplyBasePeriod = MarketDaysSupplyBasePeriod
FROM    IMT.dbo.DealerPreference_Pricing DPP WITH(NOLOCK)
WHERE   DPP.BusinessUnitID = @DealerID       

SET @MarketDaysSupplyBasePeriod = COALESCE(@MarketDaysSupplyBasePeriod, 90)

DECLARE @HighDays INT
DECLARE @LowPrice INT
DECLARE @HighPrice INT
DECLARE @LowPriceLimit SMALLINT
DECLARE @HighPriceLimit INT
DECLARE @MaxBucketForAge SMALLINT
DECLARE @PingURLPrefix VARCHAR(200)


 		--  This is the percentage points +/- the Market Average % limit
SET @MaxBucketForAge =100			--	Buckets 1-6 are the actual Age Buckets; Anything >=100 is not.
SET @HighDays =99999				--	This is the Maximum age of a vehicle
SET @LowPrice=1000					--	This is the bottom threshhold of a mispriced vehicle.  Anything below this value is mispriced.	
SET @HighPrice =100000				--	This is the top threshhold of a mispriced vehicle.  Anything above this value is mispriced.	
SET @LowPriceLimit =0				--	This is the bottom of the range of a mispriced vehicle.  LowPriceLimit to LowPrice is mispriced.
SET @HighPriceLimit =999999			--	This is the top of the range of a mispriced vehicle.  HighPrice to HighpriceLimit is mispriced.

SET @PINGURLPrefix ='https://max.firstlook.biz/pricing/Pages/Internet/VehiclePricingAnalyzer.aspx?'
 
 
DECLARE @AgeBuckets TABLE (
	ColumnBucketName VARCHAR(20),
	BucketName VARCHAR(20),
	BucketNumber SMALLINT,
	MarketAverageTarget REAL,
	LowDays SMALLINT,
	HighDays INT,
	LowPrice INT,
	HighPrice INT)
	
/*	There are 3 types of Buckets that are used to describe summary information in the Pricing Margin Report.
	Priced Buckets		-	1,2,3,4,5,6		These are buckets for properly priced vehicles (vehicles with prices >=$1000 and<$=100,000) 
											within a given age range.
	Non-priced Buckets	-	101,102			These are buckets for improperly priced vehicles.  Vehicles with prices <$1000 or >$100,000
											are 'Mispriced' vehicles.  Vehicles with prices of '$0' or NULL are 'Unpriced' vehicles.
	Summary Buckets		-	100,103			These are buckets for summary vehicle information.  'Total Priced' (Bucket 100) is summary
											information for buckets 1-6. 'Total Inventory' (Bucket 103) is summary information for
											buckets 1-6 and 101 and 102.  These are not included in the AgeBucketTable										

*/
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('Col0_21','0-21',6,1.10,0,21,@LowPrice,@HighPrice)
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('Col22_29','22-29',5,1.05,22,29,@LowPrice,@HighPrice)
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('Col30_39','30-39',4,1.00,30,39,@LowPrice,@HighPrice)
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES ('Col40_49','40-49',3,.97,40,49,@LowPrice,@HighPrice)
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('Col50_59','50-59',2,.95,50,59,@LowPrice,@HighPrice)
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('Col60_','60+',1,.90,60,@HighDays,@LowPrice,@HighPrice)
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('ColUnpriced','Unpriced:',101,NULL,0,@HighDays,@LowPriceLimit,@LowPrice-1)
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('ColMispriced','Mispriced',102,NULL,0,@HighDays,@HighPrice+1,@HighPriceLimit)


/* The #TempSearches table collects Market information for a given piece of inventory usining it's coressponding Search from the Market.Pricing.Search table.
	If the Precisiion Search (SearchTypeID =4) has sufficient information (i.e. and Avg. ListPrice >0), use the information from the precision search.  If
	not, use the YMM Search (SearchTypeID =1) information.
*/
IF object_id('tempdb..#TempSearches')IS NOT NULL DROP TABLE #TempSearches
CREATE TABLE #TempSearches
    (
      OwnerEntityID INT ,
      VehicleEntityID INT ,
      SearchTypeID TINYINT ,
      AvgListPrice REAL ,
      SumListPrice BIGINT ,
      NumberOfListings BIGINT ,
      NumberOfListingsWithPrice BIGINT
    )

INSERT  INTO #TempSearches
        ( 
		  OwnerEntityID ,
          VehicleEntityID          
        )
        SELECT  DISTINCT
                V.OwnerEntityID ,
                V.VehicleEntityID
        FROM    Market.Pricing.VehicleMarketHistory V WITH ( NOLOCK )
                JOIN Market.Pricing.Owner O WITH ( NOLOCK ) ON V.OwnerEntityID = O.OwnerEntityID
        WHERE   O.OwnerEntityID = @DealerID
                AND O.OwnerTypeID = 1
                AND V.VehicleEntityTypeID = 1
                AND V.VehicleMarketHistoryEntryTypeID = 5

UPDATE  TS
SET     SearchTypeID = 4 ,
        AvgListPrice = X.AvgListPrice ,
        SumListPrice = (X.AvgListPrice * X.NumberOfListingsWithPrice),
        NumberOfListings = X.NumberOfListings,
        NumberOfListingsWithPrice = X.NumberOfListingsWithPrice
FROM    ( SELECT    VehicleEntityID ,
                    SearchTypeID,
                    CAST(SUM(Listing_TotalUnits) AS REAL) AS NumberOfListings,
                    CAST(SUM(Listing_ComparableUnits) AS REAL) AS NumberOfListingsWithPrice ,
                    CAST(SUM(Listing_AvgListPrice) AS INT) AS AvgListPrice
          FROM      Market.Pricing.VehicleMarketHistory V WITH ( NOLOCK )
          WHERE     SearchTypeID=4
					AND V.VehicleMarketHistoryEntryTypeID = 5
          GROUP BY  VehicleEntityID,
                    SearchTypeID
          HAVING    CAST(SUM(Listing_AvgListPrice) AS INT) > 0
        ) X
        JOIN #TempSearches TS ON X.VehicleEntityID = TS.VehicleEntityID


UPDATE  TS
SET     SearchTypeID = 1 ,
        AvgListPrice = X.AvgListPrice ,
        SumListPrice = (X.AvgListPrice * X.NumberOfListingsWithPrice),
        NumberOfListings = X.NumberOfListings,
        NumberOfListingsWithPrice = X.NumberOfListingsWithPrice
FROM    ( SELECT    VehicleEntityID ,
                    SearchTypeID,
                    CAST(SUM(Listing_TotalUnits) AS REAL) AS NumberOfListings,
                    CAST(SUM(Listing_ComparableUnits) AS REAL) AS NumberOfListingsWithPrice ,
                    CAST(SUM(Listing_AvgListPrice) AS INT) AS AvgListPrice
          FROM      Market.Pricing.VehicleMarketHistory V WITH ( NOLOCK )
          WHERE     SearchTypeID=1
					AND V.VehicleMarketHistoryEntryTypeID = 5
          GROUP BY  VehicleEntityID,
                    SearchTypeID
          HAVING    CAST(SUM(Listing_AvgListPrice) AS INT) > 0
        ) X
        JOIN #TempSearches TS ON X.VehicleEntityID = TS.VehicleEntityID
WHERE   TS.SearchTypeID IS NULL
   
DECLARE @Results TABLE
    (
	  Chosen INT,
	  VehicleCount INT,
	  BucketName VARCHAR(2000),
      BusinessUnit VARCHAR(50) ,
      Age SMALLINT ,
      BucketNumber TINYINT ,
      AgeBucketName VARCHAR(20) ,
      SegmentID INT ,
      [Year] SMALLINT ,
      MakeModelTrim VARCHAR(100) ,
      Certified CHAR(1) ,
      StockNumber VARCHAR(20),
      Mileage INT ,
      UnitCost INT ,
      InternetPrice INT ,
      PercentofMarketAverage REAL ,
      InternetVsCost INT ,
      MarketAvgVsCost INT ,
      MktAvgListPrice INT ,
      OverTarget SMALLINT ,
      UnderTarget SMALLINT ,
      WithinTarget SMALLINT ,
      SumListPrice REAL ,
      NumberOfListingsWithPrice REAL,
      NumberOfListings REAL,
      AverageAge INT,
      FrontEndGross DECIMAL,
      SalePrice DECIMAL,
      OverCount int,
	  UnderCount INT,
	  GrossPerCar INT,
	  TotalGross INT,
	  SearchTypeId INT,
	  TotalUnitCost INT
    )
 
INSERT  INTO @Results
        ( 
          Chosen,
		  VehicleCount,
		  BucketName,
		  BusinessUnit ,
          Age ,
          BucketNumber ,
          AgeBucketName ,
          SegmentID ,
          [Year] ,
          MakeModelTrim ,
          Certified ,
          StockNumber ,
          Mileage ,
          UnitCost ,
          InternetPrice ,
          MktAvgListPrice ,
          InternetVsCost ,
          MarketAvgVsCost ,
          PercentofMarketAverage ,
          OverTarget ,
          UnderTarget ,
          WithinTarget ,
          SumListPrice ,
          NumberOfListingsWithPrice ,
          NumberOfListings,
		  AverageAge,
		  FrontEndGross,
		  SalePrice,
		  OverCount,
		  UnderCount,
		  GrossPerCar,
		  TotalGross,
		  SearchTypeId,
		  TotalUnitCost
        )
        SELECT  
				NULL AS Chosen,
				NULL AS VehicleCount,
				NULL AS BucketName,
				BU.BusinessUnit ,
                I.DaysToSale Age ,
                AB.BucketNumber ,
                AB.BucketName ,
                V.SegmentID ,
                V.VehicleYear ,
                MMG.Make + ' ' + MMG.Model + ' ' + COALESCE(V.VehicleTRIM, '') ,
                CASE WHEN I.Certified = 1 THEN 'Y'
                     ELSE 'N'
                END ,
                I.StockNumber ,
                I.MileageReceived ,
                CAST(I.UnitCost AS INT) UnitCost ,
                CAST(I.ListPrice AS INT) InternetPrice ,
                COALESCE(TS.AvgListPrice, 0) MktAvgListPrice ,
                CAST(I.ListPrice - I.UnitCost AS INT) InternetVsCost ,
                CAST(TS.AvgListPrice - I.UnitCost AS INT) MarketAvgVsCost ,
                CASE WHEN SumListPrice > 0
                          AND NumberOfListingsWithPrice > 0
                     THEN ListPrice / ( SumListPrice
                                        / NumberOfListingsWithPrice )
                     ELSE 0
                END AS PercentofMarketAverage ,
                CASE WHEN ListPrice / ( SumListPrice
                                        / NumberOfListingsWithPrice ) > @Threshold
                          + AB.MarketAverageTarget THEN 1
                     ELSE 0
                END ,
                CASE WHEN ListPrice / ( SumListPrice
                                        / NumberOfListingsWithPrice ) < ( AB.MarketAverageTarget
                                                              - @Threshold )
                     THEN 1
                     ELSE 0
                END ,
                CASE WHEN ListPrice / ( SumListPrice
                                        / NumberOfListingsWithPrice ) <= @Threshold
                          + AB.MarketAverageTarget
                          AND ListPrice / ( SumListPrice
                                            / NumberOfListingsWithPrice ) >= ( AB.MarketAverageTarget
                                                              - @Threshold )
                     THEN 1
                     ELSE 0
                END ,
                SumListPrice ,
                NumberOfListingsWithPrice ,
                NumberOfListings,
                NULL AS AverageAge,
                VS.FrontEndGross AS [FrontEndGross],
                VS.SalePrice AS [SalePrice],
                NULL AS [OverCount],
                NULL AS [UnderCount],
                NULL AS GrossPerCar,
				NULL AS TotalGross,
				TS.SearchTypeID SearchTypeId,
				NULL TotalUnitCost
        FROM    FLDW.dbo.InventoryInActive I WITH ( NOLOCK )
                JOIN IMT.dbo.BusinessUnit BU WITH ( NOLOCK ) ON I.BusinessUnitID = BU.BusinessUnitID
                JOIN IMT.dbo.DealerPreference DP WITH ( NOLOCK ) ON dp.BusinessUnitID = I.BusinessUnitID
                JOIN IMT.dbo.DealerPreference_Pricing AS DPP WITH ( NOLOCK ) ON DPP.BusinessUnitID = BU.BusinessUnitID
                JOIN Market.Pricing.DistanceBucket AS DB WITH ( NOLOCK ) ON DB.Distance = DPP.PingII_DefaultSearchRadius
                JOIN IMT.dbo.Vehicle V WITH ( NOLOCK ) ON I.VehicleID = V.VehicleID
                JOIN FLDW.dbo.VehicleSale VS WITH ( NOLOCK ) ON I.InventoryID = VS.InventoryID
                JOIN FLDW.dbo.Period_D P ON VS.DealDate BETWEEN P.BeginDate AND P.EndDate
                JOIN IMT.dbo.MakeModelGrouping MMG WITH ( NOLOCK ) ON MMG.MakeModelGroupingID = V.MakeModelGroupingID                
                LEFT JOIN #TempSearches TS ON I.InventoryID = TS.VehicleEntityID
                LEFT JOIN IMT.dbo.Segment AS S2 WITH ( NOLOCK ) ON S2.SegmentID = V.SegmentID --@SegmentID
                JOIN @AgeBuckets AB ON COALESCE(I.DaysToSale, 0) BETWEEN AB.LowDays
                                                              AND
                                                              AB.HighDays
                                       AND CAST(ISNULL(I.ListPrice,0) AS INT) BETWEEN AB.LowPRice
                                                              AND
                                                              AB.HighPrice
        WHERE   I.BusinessUnitID = @DealerID
                AND I.InventoryType = 2
                AND (@saleDescription IS NULL OR VS.SaleDescription = @saleDescription)
                AND P.PeriodID = @PeriodID

IF ( COALESCE(@SummaryInformation, 0) = 0 ) 
    BEGIN
        SELECT  *
        FROM    @Results
        WHERE   ( @SegmentID = -1
                  OR @SegmentID = SegmentID
                )
                AND ( @AgeBucketID = 0
                      OR @AgeBucketID = BucketNumber
                    )
        ORDER BY BucketNumber ,
                Age
    END
IF ( COALESCE(@SummaryInformation, 0) = 1 ) 
    BEGIN

	DECLARE @RetTable TABLE (
	Chosen int,
	BucketNumber int,
	BucketName varchar(50),
	VehicleCount int,
	OverCount int,
	UnderCount int,
	AverageAge int,
	PercentofMarketAverage float,
	MarketAverageTarget float,
	NumberOverTarget int,
	NumberUnderTarget int,
	NumberWithinTarget int,
	GrossPerCar int,
	TotalUnitCost int,
	TotalInternetPrice int,
	TotalGross int)
		
	Insert into @RetTable

        SELECT  CASE WHEN B.BucketNumber = @AgeBucketID THEN 1
                     ELSE 0
                END AS Chosen ,
                B.BucketNumber ,
                B.BucketName ,
                COUNT(UnitCost) AS VehicleCount ,
				-1 AS OverCount,
				-1 AS UnderCount,
                AVG(Age) AS AverageAge ,
                AVG(PercentofMarketAverage) AS PercentofMarketAverage ,
                B.MarketAverageTarget AS MarketAverageTarget ,
                COALESCE(SUM(OverTarget), 0) AS NumberOverTarget ,
                COALESCE(SUM(UnderTarget), 0) AS NumberUnderTarget ,
                COALESCE(SUM(WithinTarget), 0) AS NumberWithinTarget ,
                COALESCE(SUM(FrontEndGross) / COUNT(*), 0) AS GrossPerCar ,
                COALESCE(SUM(UnitCost), 0) AS TotalUnitCost ,
                COALESCE(SUM(InternetPrice), 0) AS TotalInternetPrice ,
                COALESCE(SUM(FrontEndGross), 0) AS TotalGross
        FROM    @AgeBuckets B
                LEFT JOIN @Results R ON B.BucketNumber = R.BucketNumber
        GROUP BY B.BucketNumber ,
                B.BucketName ,
                B.MarketAverageTarget
        UNION
        SELECT  CASE WHEN @AgeBucketID = 0 THEN 1
                     ELSE 0
                END AS Chosen ,
                100 ,
                'Total Priced:' ,
                COUNT(*) AS VehicleCount ,
				-1 AS OverCount,
				-1 AS UnderCount,
                AVG(Age) AS AverageAge ,
                CASE WHEN SUM(SumListPrice) > 0
                     THEN ( CAST(SUM(InternetPrice) AS REAL)
                            / ( CAST(COUNT(*) AS REAL) ) )
                          / ( CAST(SUM(SumListPrice) AS REAL)
                              / ( CAST(SUM(NumberOfListingsWithPrice) AS REAL) ) )
                     ELSE 0
                END AS PercentofMarketAverage ,
                NULL AS MarketAverageTarget ,
                SUM(OverTarget) AS NumberOverTarget ,
                SUM(UnderTarget) AS NumberUnderTarget ,
                SUM(WithinTarget) AS NumberWithinTarget ,
                SUM(FrontEndGross) / COUNT(*) AS GrossPerCar ,
                SUM(UnitCost) AS TotalUnitCost ,
                SUM(InternetPrice) AS TotalInternetCost ,
                SUM(FrontEndGross) AS TotalGross
        FROM    @Results
        WHERE   BucketNumber < @MaxBucketForAge
        UNION
        SELECT  0 AS Chosen ,
                103 ,
                'Total Inventory:' ,
                COUNT(*) AS VehicleCount ,
				-1 AS OverCount,
				-1 AS UnderCount,
                AVG(Age) AS AverageAge ,
                0 PercentofMarketAverage ,
                NULL AS MarketAverageTarget ,
                0 AS NumberOverTarget ,
                0 AS NumberUnderTarget ,
                0 AS NumberWithinTarget ,
                0 AS GrossPerCar ,
                0 AS TotalUnitCost ,
                0 AS TotalInternetCost ,
                0 AS TotalGross
        FROM    @Results
        ORDER BY BucketNumber 

	UPDATE @retTable SET PercentofMarketAverage = (SELECT Avg(R.PercentofMarketAverage) FROM @retTable R WHERE R.BucketNumber < @MaxBucketForAge)
	WHERE BucketNumber = 100

	DECLARE @buckNum int
	DECLARE @percentMA float

	DECLARE overUnder CURSOR FOR
	select BucketNumber, PercentofMarketAverage from @RetTable
	
	OPEN overUnder
	FETCH NEXT FROM overUnder
	INTO @buckNum, @percentMA;


	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		UPDATE @retTable
		SET OverCount = (SELECT Count(UnitCost) FROM @Results R WHERE R.BucketNumber = @buckNum
															AND R.PercentofMarketAverage >= @percentMA)
		WHERE BucketNumber = @buckNum


		UPDATE @retTable
		SET UnderCount = (SELECT Count(UnitCost) FROM @Results R WHERE R.BucketNumber = @buckNum
															AND R.PercentofMarketAverage < @percentMA)
		WHERE BucketNumber = @buckNum
	
	FETCH NEXT FROM overUnder
    INTO @buckNum, @percentMA;
	END

	CLOSE overUnder;
	DEALLOCATE overUnder;

	DECLARE @sumOver INT, @sumUnder INT
	SELECT @sumOver = SUM(OverCount) FROM @retTable WHERE BucketNumber < 100
	SELECT @sumUnder = SUM(UnderCount) FROM @retTable WHERE BucketNumber < 100

	UPDATE @retTable 
	SET  OverCount = @sumOver
	    ,UnderCount = @sumUnder
	WHERE BucketNumber = 100			

	SELECT * from @retTable

    END
IF ( COALESCE(@SummaryInformation, 0) = 2 ) 
    BEGIN
        SELECT  *
        FROM    ( SELECT    ColumnBucketName ,
                            COUNT(UnitCost) AS UC
                  FROM      @AgeBuckets AG
                            LEFT JOIN @Results R ON AG.BucketNumber = R.BucketNumber
                  GROUP BY  AG.ColumnBucketName
                ) X PIVOT
( MAX(UC) FOR ColumnBucketName IN ( [Col60_], [Col50_59], [Col40_49],
                                    [Col30_39], [Col22_29], [Col0_21],
                                    [ColMispriced], [ColUnpriced] ) ) AS P
    END

GO

GRANT EXECUTE ON [dbo].[rc_MarginSales#Pricing] TO [StoredProcedureUser]
GO