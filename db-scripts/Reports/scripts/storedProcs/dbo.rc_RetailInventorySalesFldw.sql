IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_RetailInventorySalesFldw]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_RetailInventorySalesFldw] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_RetailInventorySalesFldw] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_RetailInventorySalesFldw]
	@PeriodID INT,
	@DealerID INT,
	@TradeOrPurchase VARCHAR(1) = 'A'
AS

SET NOCOUNT ON;

-- BEGIN T/P logic
DECLARE @TPFilterLow INT, @TPFilterHigh INT

IF @TradeOrPurchase = 'A'
BEGIN
	SELECT @TPFilterLow = 1, @TPFilterHigh = 2
END

IF @TradeOrPurchase = 'T'
BEGIN
	SELECT @TPFilterLow = 2, @TPFilterHigh = 2
END

IF @TradeOrPurchase = 'P'
BEGIN
	SELECT @TPFilterLow = 1, @TPFilterHigh = 1
END

IF @TradeOrPurchase <> 'A' AND @TradeOrPurchase <> 'T' AND @TradeOrPurchase <> 'P'
BEGIN 
	RAISERROR ('The @TradeOrPurchase parameter must be A, T, or P.',16,1)
	GOTO Failed
END

DECLARE @BeginDate SMALLDATETIME, @EndDate SMALLDATETIME

SELECT	@BeginDate = BeginDate, @EndDate = EndDate
FROM	[FLDW].dbo.Period_D P
WHERE	PeriodID = @PeriodID

SELECT	VS.DealDate,
		CAST(V.VehicleYear AS VARCHAR)
			+ ' ' + MMG.Make
			+ ' ' + MMG.[Model]
			+ ' ' + V.VehicleTrim
		AS VehicleDescription,
		V.BaseColor,
		I.DaysToSale,
		VS.FrontEndGross,
		VS.BackEndGross,
		VS.SalePrice,
		I.UnitCost,
		CASE WHEN I.TradeOrPurchase = 1
			THEN 'P'
			ELSE 'T'
		END AS TradeOrPurchase,
		I.MileageReceived,
		CASE I.InitialVehicleLight
			WHEN 1 THEN 'Red'
			WHEN 2 THEN 'Yellow'
			WHEN 3 THEN 'Green'
			ELSE 'UNKNOWN'
		END AS InitialVehicleLight,
		VS.SaleDescription,
		Case VS.SaleDescription When 'R' Then 1 Else 0 End as SaleDescriptionValue,
		I.StockNumber
FROM	[FLDW].dbo.Inventory I
JOIN	[FLDW].dbo.VehicleSale VS on I.InventoryID = VS.InventoryID
JOIN	[FLDW].dbo.Vehicle V on I.VehicleID = V.VehicleID And I.BusinessUnitID = V.BusinessUnitID
JOIN	[FLDW].dbo.Segment DBT on V.SegmentID = DBT.SegmentID
JOIN	[FLDW].dbo.MakeModelGrouping MMG on V.makemodelgroupingid = MMG.makemodelgroupingid
WHERE	I.BusinessUnitID = @DealerID
AND		I.InventoryType = 2
AND 	I.TradeOrPurchase BETWEEN @TPFilterLow AND @TPFilterHigh
AND		VS.DealDate Between @BeginDate And @EndDate
AND		(VS.saledescription = 'R' OR (VS.saledescription = 'W' and I.DaysToSale >= 30))

Failed:

GO
