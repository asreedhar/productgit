
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_FullOrUnplannedInternetAdvertising]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_FullOrUnplannedInternetAdvertising] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_FullOrUnplannedInternetAdvertising] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_FullOrUnplannedInternetAdvertising]
        @DealerID VARCHAR(6) ,
        @FullAdvertising SMALLINT = 1
AS 

/**************************************************************************************************************
*
*	Routine:	dbo.rc_FullOrUnplannedInternetAdvertising
*	Purpose:	Return the data for the New Full Internet Advertising Report (old one renamed)
*
*	Parameters:	@DealerID - Passed in through UI
*			@FullAdvertising - Flag for data filter (0=Blank, 1=Optimized, 2=All, 3=Not Optimized)
*
*	History:	12/17/2009	SVU	Initial Creation
*			12/21/2009	SVU	Added Non Optimized option.  Good Possibility that this will be removed in future.
*			01/18/2009	SVU	Added ISNULL(mem.MemberID, @NullMember) to account for Null Member ID's
*			01/21/2009	SVU	Added COALESCE(gs.EnableGeneration,0), changed ISNULL to COALESCE
***************************************************************************************************************/

        SET NOCOUNT ON
	
        DECLARE @NullMember INT
        DECLARE @ReportDate DATETIME

        SET @ReportDate = GETDATE()
        SELECT  @NullMember = MemberID
        FROM    IMT.dbo.Member
        WHERE   login = ''
        
        --
        -- If Null, we will use a hard coded default value (saves approx. 20% of all Ads across all Dealers)
        --
        SET @NullMember = COALESCE(@nullmember, 106917)
        

        SELECT  BusinessUnitID = I.BusinessUnitID ,
                BusinessUnit = BU.BusinessUnit ,
                StockNumber = I.StockNumber ,
                VehicleDescription = CAST(V.VehicleYear AS VARCHAR(4)) + ' '
                + MMG.Make + ' ' + MMG.Model + ' ' + CASE VehicleTrim
                                                       WHEN 'UNKNOWN' THEN ''
                                                       ELSE VehicleTrim
                                                     END + ' (' + BaseColor
                + ')' ,
                AdvertisementID = MV.AdvertisementID ,
                VehicleHighlights = CASE WHEN GS.EnableGeneration = 1
                                         THEN IAF.Body + ' ' + IAF.Footer
                                         ELSE MA.Body + ' ' + MA.Footer
                                    END ,
                LastChangedOn = MAP.Modified ,
                LastChangedBy = MEM.LastName + ', ' + MEM.FirstName ,
                IsCertified = CASE I.Certified
                                WHEN 1 THEN 'Yes'
                                ELSE 'No'
                              END
        --,gs.EnableGeneration
        --,ma.rowtypeid
        
	FROM    FLDW.dbo.InventoryActive I WITH (NOLOCK)
	        INNER JOIN IMT.dbo.BusinessUnit BU WITH (NOLOCK) ON BU.BusinessUnitID = I.BusinessUnitID
	        LEFT JOIN Market.Merchandising.VehicleAdvertisement MV WITH (NOLOCK) ON I.InventoryID = MV.VehicleEntityID
	        LEFT JOIN Market.Merchandising.Advertisement_Properties MAP WITH (NOLOCK) ON MAP.AdvertisementID = MV.AdvertisementID
	        LEFT JOIN Market.Merchandising.Advertisement MA WITH (NOLOCK) ON MA.AdvertisementID = MV.AdvertisementID
	        INNER JOIN FLDW.dbo.Vehicle V WITH (NOLOCK) ON I.VehicleID = V.VehicleID
	                                                 AND V.BusinessUnitID = @DealerID
	        LEFT JOIN IMT..MakeModelGrouping MMG WITH (NOLOCK) ON V.MakeModelGroupingID = MMG.MakeModelGroupingID
	        LEFT JOIN Market.Merchandising.VehicleAdvertisement_Audit MVA WITH (NOLOCK) ON MVA.AdvertisementID = MV.AdvertisementID
	        LEFT JOIN IMT.dbo.Member MEM WITH (NOLOCK) ON MAP.Author = MEM.MemberID
	        LEFT JOIN Market.Merchandising.AdvertisementGeneratorSetting AS GS WITH (NOLOCK) ON GS.DealerID = @DealerID
	        LEFT JOIN FLDW.dbo.InventoryAdvertisement_F IAF WITH (NOLOCK) ON IAF.BusinessUnitID = @DealerID
                                                                         AND IAF.InventoryID = I.InventoryID
        WHERE   I.BusinessUnitID = @dealerID
                AND I.InventoryType = 2
                AND I.InventoryActive = 1
                AND V.BusinessUnitID = @DealerID
                AND ( ( @FullAdvertising = 1
                        AND COALESCE(gs.EnableGeneration, 0) = 1
                        AND IAF.Body + IAF.Footer IS NOT NULL
                        AND COALESCE(mem.MemberID, @NullMember) <> @NullMember
                      ) -- Optimized, Auto Generated
                      OR ( @FullAdvertising = 1
                           AND COALESCE(gs.EnableGeneration, 0) = 0
                           AND MA.Body + MA.Footer IS NOT NULL
                           AND COALESCE(mem.MemberID, @NullMember) <> @NullMember
                         ) -- Optimized, Non Auto Generated
                      OR ( @FullAdvertising = 0
                           AND COALESCE(gs.EnableGeneration, 0) = 1
                           AND IAF.Body + IAF.Footer IS NULL
                         ) -- Blank, Auto Generated
                      OR ( @FullAdvertising = 0
                           AND COALESCE(gs.EnableGeneration, 0) = 0
                           AND MA.Body + MA.Footer IS NULL
                         ) -- Blank, Non Auto Generated
                      OR ( @FullAdvertising = 2 ) -- all Values
                      OR ( @FullAdvertising = 3
                           AND COALESCE(gs.EnableGeneration, 0) = 1
                           AND IAF.Body + IAF.Footer IS NOT NULL
                           AND COALESCE(mem.MemberID, @NullMember) = @NullMember
                         ) -- Non Optimized, Auto Generated
                      OR ( @FullAdvertising = 3
                           AND COALESCE(gs.EnableGeneration, 0) = 0
                           AND MA.Body + MA.Footer IS NOT NULL
                           AND COALESCE(mem.MemberID, @NullMember) = @NullMember
                         ) -- Non Optimized, Non Auto Generated
                      
                    )
        --
        -- These Last Two may present problems, due to NULL values
        --
                AND COALESCE(MA.RowTypeID, 1) = 1 -- Current Ad
                AND COALESCE(MVA.ValidUpTo, @ReportDate + 2) > DATEADD(dd, +1,
                                                              IMT.dbo.ToDate(CAST(@ReportDate AS DATETIME)))
        ORDER BY map.Modified DESC

GO