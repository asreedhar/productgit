 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_ClosedAppraisalsReport_Summary]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_ClosedAppraisalsReport_Summary] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_ClosedAppraisalsReport_Summary] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_ClosedAppraisalsReport_Summary]
        @DealerID AS INT ,
        @MemberOnly AS BIT = 0
AS 

/***************************************************************************************************
*	Procedure:	dbo.rc_ClosedAppraisalsReport_Summary
*	Purpose:	Provide the Summary Information for the Closed AppraisalsReports
*
*	Params:		@DealerID - Selected Business Unit
*			@MemberOnly - Flag to return appraiser summary data (0) or MemberID Summary Data (1)
*
*	2/5/2010	SVU	Initial Creation
*
***************************************************************************************************/
	
	--
	-- Main Query
	--
SELECT  BusinessUnitID ,
        CASE WHEN @MemberOnly=0 THEN NULLIF(AppraiserName,'') ELSE M.FirstName + ' ' + M.LastName END AppraiserName,
        SUM(DealClosedFlag) ClosedVehicles ,
        COUNT(*) TotalAppraisals ,
        CASE WHEN COUNT(*) <> 0 THEN CAST(SUM(DealClosedFlag) AS DECIMAL(5, 2)) / COUNT(*)
             ELSE 0
        END AppraisalClosingRate
FROM    [HAL].dbo.TradeClosingRateReport T
LEFT JOIN	IMT.dbo.Member M ON M.MemberID=T.MemberID
WHERE   AppraisalDate BETWEEN CONVERT(VARCHAR(10), DATEADD(wk, -6, GETDATE()), 101)
                      AND     CONVERT(VARCHAR(10), DATEADD(wk, -2, DATEADD(dd, -1, GETDATE())), 101)
        AND BusinessUnitID = @DealerID
GROUP BY BusinessUnitID ,
        CASE WHEN @MemberOnly=0 THEN NULLIF(AppraiserName,'') ELSE M.FirstName + ' ' + M.LastName END
ORDER BY BusinessUnitID ,
        CASE WHEN @MemberOnly=0 THEN NULLIF(AppraiserName,'') ELSE M.FirstName + ' ' + M.LastName END

GO