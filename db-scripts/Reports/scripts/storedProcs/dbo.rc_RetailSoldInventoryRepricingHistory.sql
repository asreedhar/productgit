
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_RetailSoldInventoryRepricingHistory]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_RetailSoldInventoryRepricingHistory] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_RetailSoldInventoryRepricingHistory] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_RetailSoldInventoryRepricingHistory]
	@PeriodID INT,
	@DealerID INT
AS

SET NOCOUNT ON

CREATE TABLE #SalesSnapshot (
	-- keys
	BusinessUnitID INT,
	InventoryID INT,
	VehicleID INT,
	-- inventory data
	StockNumber VARCHAR(15),
	InventoryReceivedDate SMALLDATETIME,
	UnitCost DECIMAL(9,2),
	MileageReceived INT,
	-- sales data
	DealDate SMALLDATETIME,
	SaleDescription CHAR(1),
	FrontEndGross DECIMAL(9,2)
)

INSERT INTO #SalesSnapshot
SELECT	I.BusinessUnitID, i.InventoryID, I.VehicleID,
		I.StockNumber, I.InventoryReceivedDate, I.UnitCost, I.MileageReceived,
		vs.DealDate, VS.SaleDescription, VS.FrontEndGross
FROM	[FLDW].dbo.Inventory I
JOIN	[FLDW].dbo.Vehicle v ON i.VehicleID = v.VehicleID AND i.BusinessUnitID = v.BusinessUnitID
JOIN	[FLDW].dbo.VehicleSale vs ON i.InventoryID = vs.InventoryID
CROSS JOIN [FLDW].dbo.Period_D P
WHERE	i.BusinessUnitID = @DealerID
AND		i.InventoryActive = 0
AND		i.InventoryType = 2
AND		P.PeriodID = @PeriodID
AND		VS.DealDate BETWEEN P.BeginDate AND P.EndDate
AND		VS.SaleDescription = 'R';

CREATE TABLE #InventoryPriceHistory (
	InventoryID INT,
	ListPrice DECIMAL(9,2),
	DMSReferenceDt SMALLDATETIME,
	idx INT
);

INSERT INTO #InventoryPriceHistory
SELECT	i.InventoryID, ilph1.ListPrice, ilph1.DMSReferenceDt, count(ilph2.InventoryID) idx
FROM	[IMT].dbo.Inventory I
JOIN	[IMT].dbo.Inventory_ListPriceHistory ilph1 ON ilph1.InventoryID = i.inventoryid
JOIN	[IMT].dbo.Inventory_ListPriceHistory ilph2 ON ilph2.InventoryID = i.InventoryID
JOIN	#SalesSnapshot s on s.InventoryID = i.InventoryID
WHERE	ilph1.DMSReferenceDt >= ilph2.DMSReferenceDt
AND		ilph2.ListPrice > 0
AND		ilph1.ListPrice > 0
AND		i.InventoryID = s.InventoryID
GROUP
BY		i.InventoryID, ilph1.ListPrice, ilph1.DMSReferenceDt
;

WITH InventoryPriceCount AS
(
	SELECT InventoryID, MAX(idx)-1 Counter FROM #InventoryPriceHistory GROUP BY InventoryID
),
InventoryPrices AS
(
	SELECT	InventoryID AS InventoryID, [1] AS [InitialPrice], [2] AS [Reprice1], [3] AS [Reprice2], [4] AS [Reprice3], [5] AS [Reprice4], [6] AS [Reprice5], [7] AS [Reprice6]
	FROM
	(SELECT	InventoryID, ListPrice, idx FROM #InventoryPriceHistory) As SourceTable
	PIVOT
	(MAX(ListPrice) FOR idx in ([1], [2], [3], [4], [5], [6], [7])) AS PivotTable
),
InventoryPriceDates AS
(
	SELECT	InventoryID AS InventoryID, [1] AS [DMSdt0], [2] AS [DMSdt1], [3] AS [DMSdt2], [4] AS [DMSdt3], [5] AS [DMSdt4], [6] AS [DMSdt5], [7] AS [DMSdt6]
	FROM
	(SELECT	InventoryID, DMSReferenceDt, idx FROM #InventoryPriceHistory) As SourceTable
	PIVOT
	(MAX(DMSReferenceDt) FOR idx in ([1], [2], [3], [4], [5], [6], [7])) AS PivotTable
)
SELECT	I.InventoryID,
		IC.Counter Counter,
		V.VehicleYear vYear,
		MMG.Make vMake,
		MMG.Model vModel,
		V.BaseColor,
		V.VehicleTrim,
		V.VIN,
		DBT.Segment DisplayBodyType,
		I.StockNumber,
		I.InventoryReceivedDate,
		I.UnitCost,
		I.MileageReceived,
		IP.InitialPrice, IP.Reprice1, IP.Reprice2, IP.Reprice3, IP.Reprice4, IP.Reprice5, IP.Reprice6,
		IPD.DMSdt0, IPD.DMSdt1, IPD.DMSdt2, IPD.DMSdt3, IPD.DMSdt4, IPD.DMSdt5,
		DATEDIFF(DD, i.InventoryReceivedDate, GETDATE()) 'Age',
		CASE
			WHEN DATEDIFF(DD, i.InventoryReceivedDate, GETDATE()) >=60 THEN 60
			WHEN DATEDIFF(DD, i.InventoryReceivedDate, GETDATE()) >=50 THEN 50
			WHEN DATEDIFF(DD, i.InventoryReceivedDate, GETDATE()) >=40 THEN 40
			WHEN DATEDIFF(DD, i.InventoryReceivedDate, GETDATE()) >=30 THEN 30
			WHEN DATEDIFF(DD, i.InventoryReceivedDate, GETDATE()) >=21 THEN 21
			ELSE 0
		END as 'Bucket',
		IB.BookValue BookValue,
		I.DealDate,
		I.SaleDescription,
		I.FrontEndGross,
		I.BusinessUnitID DealerID
FROM	#SalesSnapshot I
JOIN	[FLDW].dbo.Vehicle v ON i.VehicleID = v.VehicleID AND I.BusinessUnitID = V.BusinessUnitID
JOIN	[IMT].dbo.MakeModelGrouping MMG ON V.MakeModelGroupingid = MMG.MakeModelGroupingID
JOIN	[IMT].dbo.Segment DBT ON DBT.SegmentID = v.SegmentID
JOIN	[IMT].dbo.DealerPreference DP ON DP.BusinessUnitID = I.BusinessUnitID
LEFT JOIN	[FLDW].dbo.InventoryBookout_F IB ON (
				IB.InventoryID = I.InventoryID
			AND	IB.ThirdPartyCategoryID = DP.BookOutPreferenceId
			AND	IB.BookoutValueTypeID = 2
		)
JOIN	InventoryPriceCount IC ON IC.InventoryID = I.InventoryID
JOIN	InventoryPrices IP ON IP.InventoryID = I.InventoryID
JOIN	InventoryPriceDates IPD ON IPD.InventoryID = I.InventoryID

DROP TABLE #SalesSnapshot;
DROP TABLE #InventoryPriceHistory

GO
