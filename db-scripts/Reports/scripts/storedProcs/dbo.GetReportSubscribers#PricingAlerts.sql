
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetReportSubscribers#PricingAlerts]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[GetReportSubscribers#PricingAlerts] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[GetReportSubscribers#PricingAlerts] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[GetReportSubscribers#PricingAlerts]
	@SubscriptionTypeID            INT,
	@SubscriptionFrequencyDetailID INT = NULL
AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.GetReportSubscribers#PricingAlerts.sql,v 1.4 2009/03/16 19:27:05 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return a list of recipients for the supplied subscription type and frequency.
 * 
 * Parameters
 * ----------
 *
 * @SubscriptionTypeID            - the type of the report
 * @SubscriptionFrequencyDetailID - the frequency of the report
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - Invalid @SubscriptionTypeID
 * 50100 - Invalid @SubscriptionFrequencyDetailID
 *
 * MAK -01/29/2009  These should only be sent to Active users.
  * -------------------------------------------------------------------- */

SET NOCOUNT ON

IF @SubscriptionFrequencyDetailID IS NULL BEGIN

	SELECT	@SubscriptionFrequencyDetailID = SubscriptionFrequencyDetailID
	FROM	[IMT].dbo.SubscriptionFrequencyDetail
	WHERE	Description = DATENAME(DW,GETDATE())

END

/* --------------------------------------------------------------------
 *                          Validate Parameters
 * -------------------------------------------------------------------- */

IF NOT EXISTS (SELECT 1 FROM [IMT].dbo.SubscriptionTypes WHERE SubscriptionTypeID = @SubscriptionTypeID) BEGIN
	RAISERROR('Invalid SubscriptionType ''%d''.', 16, 1, @SubscriptionTypeID)
	RETURN 50100
END

IF NOT EXISTS (SELECT 1 FROM [IMT].dbo.SubscriptionFrequencyDetail WHERE SubscriptionFrequencyDetailID = @SubscriptionFrequencyDetailID AND SubscriptionFrequencyID <> 0) BEGIN
	RAISERROR('Invalid SubscriptionFrequencyDetail ''%d''.', 16, 1, @SubscriptionFrequencyDetailID)
	RETURN 50100
END

/* --------------------------------------------------------------------
 *                          Declare Result Schema
 * -------------------------------------------------------------------- */

DECLARE @Results TABLE (
	[TO]                VARCHAR(8000) NOT NULL,
	[RenderFormat]      VARCHAR(8000) NOT NULL,
	[Subject]           VARCHAR(8000) NOT NULL,
	[OwnerHandle]       VARCHAR(36) NOT NULL,
	[SaleStrategy]      CHAR(1) NOT NULL,
	[Mode]              CHAR(1) NOT NULL,
	[ColumnIndex]       INT NOT NULL,
	[FilterMode]        CHAR(1) NOT NULL,
	[FilterColumnIndex] INT NOT NULL
)

DECLARE @Recipients TABLE (
	[Idx]          INT NOT NULL,
	[TO]           VARCHAR(8000) NOT NULL,
	[Subject]      VARCHAR(8000) NOT NULL,
	[OwnerHandle]  VARCHAR(36) NOT NULL
)

/* --------------------------------------------------------------------
 *                             Collect Recipients
 * -------------------------------------------------------------------- */

INSERT INTO @Recipients (
	[Idx],
	[TO],
	[Subject],
	[OwnerHandle]
)

SELECT	[Idx]         = ROW_NUMBER() OVER (PARTITION BY O.Handle ORDER BY O.Handle),
	[TO]          = M.EmailAddress,
	[Subject]     = BusinessUnit + '''s ' + F.Description + ' ' + T.Description + ' for ' + D.Description + ', ' + CONVERT(VARCHAR,GETDATE(),107),
	[OwnerHandle] = O.Handle

FROM	[IMT].dbo.Subscriptions S
JOIN	[IMT].dbo.SubscriptionTypes T ON S.SubscriptionTypeID = T.SubscriptionTypeID
JOIN	[IMT].dbo.Member M ON S.SubscriberID = M.MemberID
JOIN	[IMT].dbo.BusinessUnit B ON S.BusinessUnitID = B.BusinessUnitID
CROSS JOIN [IMT].dbo.SubscriptionFrequencyDetail D
JOIN       [IMT].dbo.SubscriptionFrequency F ON D.SubscriptionFrequencyID = F.SubscriptionFrequencyID
JOIN	[Market].Pricing.Owner O ON O.OwnerEntityID = B.BusinessUnitID AND O.OwnerTypeID = 1

WHERE	D.SubscriptionFrequencyDetailID = @SubscriptionFrequencyDetailID
AND	S.SubscriptionTypeID = @SubscriptionTypeID
AND	S.SubscriptionFrequencyDetailID IN (@SubscriptionFrequencyDetailID,3)
AND	S.SubscriberTypeID = 1           -- Member
AND	S.SubscriptionDeliveryTypeID = 1 -- HTML email
AND	M.EmailAddress IS NOT NULL
AND	LEN(M.EmailAddress) > 5
AND	PATINDEX('%@%.%', M.EmailAddress) > 0
AND B.Active=1

/* --------------------------------------------------------------------
 *                           Combine Recipients
 * -------------------------------------------------------------------- */

; WITH Rec ([Idx], [To], [OwnerHandle], [Ctr]) AS
(
	SELECT	R1.Idx,
		R1.[TO],
		R1.[OwnerHandle],
		1 Ctr
	FROM	@Recipients R1
UNION ALL
	SELECT	R2.Idx,
		R2.[TO] + COALESCE(';' + R3.[TO],''),
		R2.[OwnerHandle],
		Ctr+1 Ctr
	FROM	@Recipients R2
	JOIN	Rec R3 ON R3.OwnerHandle = R2.OwnerHandle AND R3.Idx = R2.Idx + 1
),
Row (OwnerHandle,Subject,Ctr) AS
(
	SELECT	OwnerHandle, Subject, COUNT(*) Ctr
	FROM	@Recipients
	GROUP
	BY	OwnerHandle, Subject
)
INSERT INTO @Results (
	[TO],
	[RenderFormat],
	[Subject],
	[OwnerHandle],
	[SaleStrategy],
	[Mode],
	[ColumnIndex],
	[FilterMode],
	[FilterColumnIndex]
)
SELECT	[TO]                = R.[To],
	[RenderFormat]      = 'MHTML',
	[Subject]           = X.Subject,
	[OwnerHandle]       = R.OwnerHandle,
	[SaleStrategy]      = 'A',
	[Mode]              = 'R',
	[ColumnIndex]       = CASE WHEN @SubscriptionTypeID = 13 THEN 3 ELSE 1 END,
	[FilterMode]        = 'A',
	[FilterColumnIndex] = 0
FROM	Rec R
JOIN	Row X ON R.OwnerHandle = X.OwnerHandle AND R.Ctr = X.Ctr

/* --------------------------------------------------------------------
 *                           Return Results
 * -------------------------------------------------------------------- */

SELECT * FROM @Results

GO

