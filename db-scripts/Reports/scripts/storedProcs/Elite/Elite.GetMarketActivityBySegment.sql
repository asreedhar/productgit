IF (EXISTS(SELECT * FROM SYS.OBJECTS WHERE Name = 'GetMarketActivityBySegment' and Type = 'P'))
	DROP PROCEDURE Elite.GetMarketActivityBySegment
GO

CREATE PROCEDURE Elite.GetMarketActivityBySegment
@StartDate datetime,
@EndDate datetime,
@BusinessUnitId int = null,
@DebugFlag int = 0

AS
/******************************************************************************
**
**  Procedure: Elite.GetMarketActivityBySegment
**  Description: Returns Retail/Wholesale/Appraisal/Inventory data across the entire market,
**		Grouped By Vehicle Segment and CylinderQty
**		The group by will likely change
**        
**
**  Return values:  None
**  Input parameters:   see above
**
**  Output parameters:  none
**
**  Rows returned: RS1	SegmentName,
**			CylinderQty,
**			AppraisalCnt, 
**			TradeInCnt,
**			RetailCnt,
**			AgedWholesaleCnt,
**			RetailFrontEndGross,
**			RetailBackEndGross,
**			RetailAvgDaysToSale,
**			AgedWholesaleAvgDaysToSale,
**			AvgDaysToSale,
**			AgedWholesaleGiveBack
**			
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author: Description:
**  YYYY.MM.DD  ------- -------------------------------------------
**  2008.06.05  ffoiii	Procedure created.
**
**Comments
--Only tested against local data so far
--Avg run time about 10 seconds depending on period length
--Report request is for full market grouped by Segment and Engine Size
*******************************************************************************/

SET NOCOUNT ON
SET ANSI_WARNINGS ON

--Debug only controls whether timing messages are displayed as a secondary result set
SELECT @DebugFlag = 0 

--internal timing messages are logged to @Debug and printed if @DebugFlag = 1
DECLARE @Debug TABLE (MessageText varchar(1000))

--temp tables used for storing interim results
--the output of this report lends itself to vertical partitioning and recombining on VehicleCatalogId
CREATE TABLE #Dealers (BusinessUnitID INT PRIMARY KEY)
CREATE TABLE #ReportGroup (VehicleCatalogId INT PRIMARY KEY (VehicleCatalogId))
CREATE TABLE #Appraisal (VehicleCatalogId INT PRIMARY KEY, AppraisalCnt int, AppraisalClosedCnt int)
CREATE TABLE #Retail  (VehicleCatalogId INT PRIMARY KEY , RetailCnt INT, FrontEndGross DECIMAL(12,2), BackEndGross DECIMAL(12,2), CumulativeDaysToSale FLOAT, ExampleInventoryId INT)
CREATE TABLE #AgedWholesale (VehicleCatalogId INT PRIMARY KEY , AgedWholesaleCnt INT, AgedWholesaleGiveback DECIMAL(12,2), CumulativeDaysToSale Float, ExampleInventoryId INT)
CREATE TABLE #InventoryAging (VehicleCatalogId INT PRIMARY KEY , InventoryCnt INT, CumulativeInventoryAge INT)
CREATE TABLE #Output 
	(VehicleCatalogId INT,
	AppraisalCnt INT,
	AppraisalClosedCnt INT,
	RetailCnt INT,
	AgedWholesaleCnt INT,
	AgedWholesaleGiveBack FLOAT,
	AgedWholesaleCumulativeDaysToSale INT,
	RetailFrontEndGross DECIMAL(12,2),
	RetailBackEndGross DECIMAL(12,2),
	RetailCumulativeDaysToSale INT,
	ExampleRetailInventoryId INT, --Only used for debugging
	ExampleWholesaleInventoryId INT, --Only used for debugging
	TotalInventoryCnt INT,
	TotalInventoryCumulativeDays INT,
	AvgInventoryAge FLOAT,
)
insert into @Debug (MessageText)
select 'Temp Tables created: ' + convert(varchar,getdate(),108)


--*******************************************************************
--Only active dealers should be included
--*******************************************************************
INSERT 
INTO	#Dealers
SELECT	DS.BusinessUnitID
FROM	IMT.dbo.BusinessUnit DS
WHERE		DS.Active = 1
	AND	DS.BusinessUnitTypeID = 4
	AND	(@BusinessUnitId is null or DS.BusinessUnitId = @BusinessUnitId)

insert into @Debug (MessageText)
select '#Dealers populated: ' + convert(varchar,getdate(),108) + ' -- Rows: ' + convert(varchar, @@Rowcount)


--*******************************************************************
--All Inventory
--*******************************************************************
INSERT
INTO #InventoryAging (VehicleCatalogId, InventoryCnt, CumulativeInventoryAge)
SELECT	v.VehicleCatalogId,
	count(i.InventoryId) InventoryCnt,
	sum(datediff(dd,i.InventoryReceivedDate, @EndDate)) CumulativeInventoryAge
FROM FLDW.dbo.Inventory i
	INNER JOIN imt.dbo.Vehicle v on v.VehicleId = i.VehicleId
	INNER JOIN #Dealers d on d.BusinessUnitId = i.BusinessUnitId
WHERE	i.InventoryReceivedDate < @EndDate
	AND (i.DeleteDt is null or i.DeleteDt > @EndDate)
GROUP
BY	v.VehicleCatalogId
insert into @Debug (MessageText)
select '#InventoryAging populated: ' + convert(varchar,getdate(),108)


--*******************************************************************
--Appraisals
--*******************************************************************
INSERT
INTO	#Appraisal (VehicleCatalogId, AppraisalCnt, AppraisalClosedCnt)
SELECT	v.VehicleCatalogId,
	COUNT(distinct a.AppraisalId) AppraisalCnt,
	COUNT(distinct i.InventoryId) AppraisalClosedCnt
FROM	IMT.dbo.Appraisals a
	INNER JOIN IMT.dbo.Vehicle v on v.VehicleId = a.VehicleId
	INNER JOIN #Dealers d on d.BusinessUnitId = a.BusinessUnitId
	LEFT JOIN imt.dbo.inventory i on i.VehicleId = a.VehicleId 
				and i.BusinessUnitId = a.BusinessUnitId 
				and i.InventoryReceivedDate between a.DateCreated and DATEADD(DD,14,@EndDate)
WHERE	a.DateCreated BETWEEN @StartDate and @EndDate
GROUP
BY	v.VehicleCatalogId
insert into @Debug (MessageText)
select '#Appraisals populated: ' + convert(varchar,getdate(),108) + ' -- Rows: ' + convert(varchar, @@Rowcount)


--*******************************************************************
--Retail
--*******************************************************************
INSERT
INTO	#Retail (VehicleCatalogId, RetailCnt, FrontEndGross, BackEndGross, CumulativeDaysToSale, ExampleInventoryId)
SELECT	v.VehicleCatalogId,
	COUNT(i.InventoryId) RetailCnt, 
	SUM(vs.FrontEndGross) FrontEndGross,
	SUM(vs.BackEndGross) BackEndGross,
	SUM(DATEDIFF(dd, i.InventoryReceivedDate, vs.DealDate)) CumulativeDaysToSale,
	max(i.inventoryid) ExampleInventoryId
FROM	FLDW.dbo.Inventory i
	inner join IMT.dbo.Vehicle v on v.vehicleid = i.vehicleid
	INNER JOIN FLDW.dbo.VehicleSale vs on vs.InventoryId = i.InventoryId
	INNER JOIN #Dealers d on d.BusinessUnitId = i.BusinessUnitId
WHERE	i.InventoryType = 2  --Used Vehicles only
	AND	VS.DealDate BETWEEN @StartDate AND @EndDate
	AND	VS.SaleDescription = 'R'  --Retailed Vehicles Only
GROUP
BY	v.VehicleCatalogId
insert into @Debug (MessageText)
select '#Retail populated: ' + convert(varchar,getdate(),108) + ' -- Rows: ' + convert(varchar, @@Rowcount)


--*******************************************************************
--Aged Wholesale
--*******************************************************************
INSERT
INTO	#AgedWholesale (VehicleCatalogId, AgedWholesaleCnt, AgedWholesaleGiveBack, CumulativeDaysToSale, ExampleInventoryId)
SELECT	v.VehicleCatalogId,
	COUNT(v.VehicleId) AgedWholesaleCnt,
	SUM(isnull(i.UnitCost,0) - isnull(vs.SalePrice,0)) AgedWholesaleGiveBack,
	SUM(DATEDIFF(dd,i.InventoryReceivedDate, vs.DealDate)) CumulativeDaysToSale,
	MAX(i.InventoryId) ExampleInventoryId
FROM	FLDW.dbo.Inventory i
	INNER JOIN IMT.dbo.Vehicle v on v.VehicleId = i.VehicleId
	INNER JOIN FLDW.dbo.VehicleSale vs on vs.InventoryId = i.InventoryId
	INNER JOIN #Dealers d on d.BusinessUnitId = i.BusinessUnitId
WHERE	i.InventoryType = 2  --Used Vehicles only
	AND	VS.DealDate BETWEEN @StartDate AND @EndDate
	AND	VS.SaleDescription = 'W'  --Wholesaled Vehicles Only
	AND	DATEDIFF(dd, i.InventoryReceivedDate, vs.DealDate) > 30
GROUP
BY	v.VehicleCatalogId
insert into @Debug (MessageText)
select '#AgedWholesale populated: ' + convert(varchar,getdate(),108) + ' -- Rows: ' + convert(varchar, @@Rowcount)


--*******************************************************************
--FINAL AGGREGATION
--*******************************************************************
--Get distinct VehicleCategoryId list
INSERT
INTO	#ReportGroup (VehicleCatalogId)
SELECT VehicleCatalogId FROM #Retail
UNION SELECT VehicleCatalogId FROM #Appraisal
UNION SELECT VehicleCatalogId FROM #AgedWholesale
UNION SELECT VehicleCatalogId FROM #InventoryAging
insert into @Debug (MessageText)
select '#ReportGroup populated: ' + convert(varchar,getdate(),108) + ' -- Rows: ' + convert(varchar, @@Rowcount)

--merge vertical segments (retail, agedwholesale, etc.) into a common table based on the common key VehicleCatalogId
INSERT
INTO #Output (VehicleCatalogId, AppraisalCnt, AppraisalClosedCnt, RetailCnt, RetailFrontEndGross, RetailBackEndGross,
		 RetailCumulativeDaysToSale, AgedWholesaleCumulativeDaysToSale, AgedWholesaleCnt, AgedWholesaleGiveBack, ExampleRetailInventoryId, ExampleWholesaleInventoryId, TotalInventoryCnt, TotalInventoryCumulativeDays)
select	rg.VehicleCatalogId,
	isnull(a.AppraisalCnt, 0) AppraisalCnt, 
	isnull(a.AppraisalClosedCnt, 0) TradeInCnt,
	isnull(r.RetailCnt, 0) RetailCnt,
	isnull(r.FrontEndGross, 0) FrontEndGross,
	isnull(r.BackEndGross, 0) BackEndGross,
	isnull(r.CumulativeDaysToSale,0) RetailCumulativeDaysToSale,
	isnull(w.CumulativeDaysToSale,0) WholesaleCumulativeDaysToSale,
	isnull(w.AgedWholesaleCnt,0) AgedWholesaleCnt,
	isnull(w.AgedWholesaleGiveBack,0) AgedWholesaleGiveBack,
	r.ExampleInventoryId,
	w.ExampleInventoryId,
	isnull(ia.InventoryCnt,0) GeneralInventoryCnt,
	isnull(ia.CumulativeInventoryAge,0) CumulativeInventoryAge
FROM #ReportGroup rg
	LEFT JOIN #Appraisal a on a.VehicleCatalogId = rg.VehicleCatalogId
	LEFT JOIN #Retail r on r.VehicleCatalogId = rg.VehicleCatalogId
	LEFT JOIN #AgedWholesale w on w.VehicleCatalogId = rg.VehicleCatalogId
	LEFT JOIN #InventoryAging ia on ia.VehicleCatalogId = rg.VehicleCatalogId
insert into @Debug (MessageText)
select '#Output populated: ' + convert(varchar,getdate(),108) + ' -- Rows: ' + convert(varchar, @@Rowcount)

--aggregate to Segment and Cylinders
select  s.Segment,
	vc.CylinderQty,
	sum(isnull(o.AppraisalCnt,0)) AppraisalCnt, 
	sum(isnull(o.AppraisalClosedCnt,0)) AppraisalClosedCnt,
	sum(isnull(o.RetailCnt,0)) RetailCnt,
	sum(isnull(o.AgedWholesaleCnt,0)) AgedWholesaleCnt,
	sum(isnull(o.RetailFrontEndGross,0)) RetailFrontEndGross,
	sum(isnull(o.RetailBackEndGross,0)) RetailBackEndGross,
	case when sum(o.RetailCnt) = 0 then 0
		else sum(o.RetailCumulativeDaysToSale)/sum(o.RetailCnt)
	end RetailAvgDaysToSale,
	case when sum(o.AgedWholesaleCnt) = 0 then 0
		else sum(o.AgedWholesaleCumulativeDaysToSale)/sum(o.AgedWholesaleCnt) 
	end AgedWholesaleAvgDaysToSale,
	sum(isnull(o.AgedWholesaleGiveBack,0)) AgedWholesaleGiveBack,
	case when sum(o.TotalInventoryCnt) = 0 then 0
		else sum(o.TotalInventoryCumulativeDays)/sum(o.TotalInventoryCnt)
	end AvgInventoryDays
FROM #Output o
	INNER JOIN VehicleCatalog.Firstlook.VehicleCatalog vc on vc.VehicleCatalogId = o.VehicleCatalogId
	INNER JOIN VehicleCatalog.Firstlook.Segment s on s.SegmentId = vc.SegmentId
GROUP
BY	s.Segment, vc.CylinderQty
ORDER
BY	s.Segment, vc.CylinderQty


--*******************************************************************
--Cleanup
--*******************************************************************
DROP TABLE #Dealers 
DROP TABLE #Appraisal 
DROP TABLE #Retail  
DROP TABLE #AgedWholesale
DROP TABLE #Output
DROP TABLE #ReportGroup
DROP TABLE #InventoryAging


--Check Debug Mode
IF (@DebugFlag = 1)
select * from @Debug

GO
