SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Elite].[GetEliteGrpInventoryPerformReport]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Elite].[GetEliteGrpInventoryPerformReport]
GO

CREATE PROC [Elite].[GetEliteGrpInventoryPerformReport]
------------------------------------------------------------------------------------------------
--	               
--		ELITE Ops, SQL from SSRS Report
--		Internal INVENTORY PERFORMANCE Report BlackBerry friendly, for ELITE Groups.
--			Various Sales Performance metrics across 3 time periods:
--			1) The period defined between @BeginDate and @EndDate ('InPeriod')
--			2) 13 week period prior to @EndDate, 
--			3) Last month's volume (month prior to @EndDate)
--
---Parmeters------------------------------------------------------------------------------------
--
	@WaterLimit DECIMAL(9,2) = NULL
	,@BusinessUnitString VARCHAR(2000)	
          
--
---History--------------------------------------------------------------------------------------
--
--	09/09/08	JM	Initial design/development, with QA/improvements by Magda
--
------------------------------------------------------------------------------------------------
AS

SET NOCOUNT ON 

DECLARE 
	@BusinessUnits TABLE
		(BusinessUnitID INT)

INSERT 
	INTO	@BusinessUnits
		(BusinessUnitID)
	SELECT	value
	FROM	IMT.dbo.Split(@BusinessUnitString,',')

SELECT		
	Dealer.BusinessUnitID 
	--calculate inventory that is 60 Days +, in units and as % of total
	, SUM
		(CASE 
		 WHEN Inventory.AgeInDays >= 60 THEN 1
		 ELSE 0
		 END) Inventory60PlusUnits
	, CAST
		(
		SUM(CASE 
		 WHEN Inventory.AgeInDays >= 60 THEN 1
		 ELSE 0
		 END)AS FLOAT
		)/ 
		CAST
		(
		SUM(CASE 
		 WHEN Inventory.AgeInDays >= 0 THEN 1
		 ELSE 0
		 END)AS FLOAT
		) Inventory60PlusPercent

	--calculate inventory that is 50-59 Days, in units and as % of total
	, SUM
		(CASE 
		 WHEN Inventory.AgeInDays BETWEEN 50 AND 59 THEN 1
		 ELSE 0
		 END) Inventory50to59Units
	, CAST
		(
		SUM(CASE 
		 WHEN Inventory.AgeInDays BETWEEN 50 AND 59 THEN 1
		 ELSE 0
		 END)AS FLOAT
		)/ 
		CAST
		(
		SUM(CASE 
		 WHEN Inventory.AgeInDays >= 0 THEN 1
		 ELSE 0
		 END)AS FLOAT
		) Inventory50to59Percent

	--calculate inventory that is 40-49 Days, in units and as % of total
	, SUM
		(CASE 
		 WHEN Inventory.AgeInDays BETWEEN 40 AND 49 THEN 1
		 ELSE 0
		 END) Inventory40to49Units
	, CAST
		(
		SUM(CASE 
		 WHEN Inventory.AgeInDays BETWEEN 40 AND 49 THEN 1
		 ELSE 0
		 END)AS FLOAT
		)/ 
		CAST
		(
		SUM(CASE 
		 WHEN Inventory.AgeInDays >= 0 THEN 1
		 ELSE 0
		 END)AS FLOAT
		) Inventory40to49Percent

	--calculate total number of vehicles that are on the 'Water Hit List', which are vehicles underwater by -$2,000 vs. book value
	, SUM
		(CASE 
		 WHEN (InventoryBookout.BookValue - InventoryBookout.UnitCost <= @WaterLimit) THEN 1
		 ELSE 0
		 END) WaterHitListUnits

	--calculate total number of 'Water Hit List' vehicles that were Trade 'T', and those purchased, 'P'
	, SUM
		(CASE 
		 WHEN (InventoryBookout.BookValue - InventoryBookout.UnitCost <= @WaterLimit) AND Inventory.TradeOrPurchase = 1  THEN 1
		 ELSE 0
		 END) WaterHitListUnitsP
	, SUM
		(CASE 
		 WHEN (InventoryBookout.BookValue - InventoryBookout.UnitCost <= @WaterLimit) AND Inventory.TradeOrPurchase = 2  THEN 1
		 ELSE 0
		 END) WaterHitListUnitsT

	--calculate total number of 'Water Hit List'(WHL) vehicles that are Truck or SUV's 
	, SUM
		(CASE 
		 WHEN (InventoryBookout.BookValue - InventoryBookout.UnitCost <= @WaterLimit) 
			AND (Segment.SegmentID = 2 OR Segment.SegmentID = 6)  THEN 1
		 ELSE 0
		 END) WaterHitListUnitsTruckOrSUV

	--calculate total number of 'Water Hit List'(WHL) vehicles that are over 30 Days old 
	, SUM
		(CASE 
		 WHEN (InventoryBookout.BookValue - InventoryBookout.UnitCost <= @WaterLimit) 
			AND Inventory.AgeInDays >= 30  THEN 1
		 ELSE 0
		 END) WaterHitListUnits30DaysPlus

	--calculate total 'WHL' vehicles that are 'low mileage per year' (LUMPY)
	, SUM
		(CASE 
		 WHEN (InventoryBookout.BookValue - InventoryBookout.UnitCost <= @WaterLimit) 
--			AND (Inventory.MileageReceived / (DATEPART(YY,@InventorySnapshotDate) - Vehicle.VehicleYear +1)) <= 10000  THEN 1 --Rough estimate of vehicle's age
			AND (Inventory.MileageReceived / VV.VehicleAge) <= 10000  THEN 1 --Miles per year less <= 10,000 equals 'LuMPY'
		 ELSE 0
		 END) WaterHitListUnitsLUMPY

/*  THIS CODE IS REPLACED WITH THE CODE BELOW - REMOVE AFTER QA

FROM	[IMT].dbo.BusinessUnit Dealer
	JOIN	[IMT].dbo.BusinessUnitRelationship R ON R.BusinessUnitID = Dealer.BusinessUnitID
	JOIN	[FLDW].dbo.Inventory Inventory ON Inventory.BusinessUnitID = Dealer.BusinessUnitID
	JOIN	[FLDW].dbo.Vehicle Vehicle ON (Vehicle.VehicleID = Inventory.VehicleID AND Vehicle.BusinessUnitID = Inventory.BusinessUnitID)
	JOIN	[FLDW].dbo.Segment ON Segment.SegmentID = Vehicle.SegmentID
	JOIN	[FLDW].dbo.MakeModelGrouping MakeModel ON MakeModel.MakeModelGroupingID = Vehicle.MakeModelGroupingID
	JOIN	[FLDW].dbo.DealerPreference Preferences ON Inventory.BusinessUnitID = Preferences.BusinessUnitID
	LEFT JOIN [FLDW].dbo.InventoryBookout_F InventoryBookout ON (InventoryBookout.InventoryID = Inventory.InventoryID AND InventoryBookout.ThirdPartyCategoryID = Preferences.BookOutPreferenceID)
	JOIN @BusinessUnits B ON Dealer.BusinessUnitID =B.BusinessUnitID
*/


FROM	[IMT].dbo.BusinessUnit Dealer
	JOIN	[IMT].dbo.BusinessUnitRelationship R ON R.BusinessUnitID = Dealer.BusinessUnitID
	JOIN	[FLDW].dbo.Inventory Inventory ON Inventory.BusinessUnitID = Dealer.BusinessUnitID
	JOIN	( 	
			Select SegmentID,
					VehicleID,
					BusinessUnitID,
					MakeModelGroupingID
			,CASE 
					WHEN DATEPART (mm, GETDATE()) < 9  --Assume Sept model year for all cars
					THEN DATEPART(yy, GETDATE()) -  Vehicle.VehicleYear --before Sept
					ELSE (DATEPART(yy, GETDATE())+1) -  Vehicle.VehicleYear --after Sept
					END AS InterimVehicleAge

			--Remove zero and negative-value cases, set to one year old
			,CASE
						WHEN (Case
							WHEN DATEPART (mm, GETDATE()) < 9  --Assume Sept model year for all cars
							THEN DATEPART(yy, GETDATE()) -  Vehicle.VehicleYear --before Sept
							ELSE (DATEPART(yy, GETDATE())+1) -  Vehicle.VehicleYear --after Sept
						END )
					> 0 
						THEN (Case
							WHEN DATEPART (mm, GETDATE()) < 9  --Assume Sept model year for all cars
							THEN DATEPART(yy, GETDATE()) -  Vehicle.VehicleYear --before Sept
							ELSE (DATEPART(yy, GETDATE())+1) -  Vehicle.VehicleYear --after Sept
						END )
						ELSE 1
						END as VehicleAge
				FROM
				[FLDW].dbo.Vehicle Vehicle ) VV 
				ON (VV.VehicleID = Inventory.VehicleID AND VV.BusinessUnitID = Inventory.BusinessUnitID)
	JOIN	[FLDW].dbo.Segment ON Segment.SegmentID = VV.SegmentID
	JOIN	[FLDW].dbo.MakeModelGrouping MakeModel ON MakeModel.MakeModelGroupingID = VV.MakeModelGroupingID
	JOIN	[FLDW].dbo.DealerPreference Preferences ON Inventory.BusinessUnitID = Preferences.BusinessUnitID
	LEFT JOIN [FLDW].dbo.InventoryBookout_F InventoryBookout ON (InventoryBookout.InventoryID = Inventory.InventoryID AND InventoryBookout.ThirdPartyCategoryID = Preferences.BookOutPreferenceID)
	JOIN @BusinessUnits B ON Dealer.BusinessUnitID =B.BusinessUnitID

WHERE	
	Inventory.InventoryType = 2
	AND Inventory.InventoryActive = 1

GROUP BY
	Dealer.BusinessUnitID
GO               
               
GRANT EXECUTE ON [Elite].[GetEliteGrpInventoryPerformReport] TO [StoredProcedureUser]
GO