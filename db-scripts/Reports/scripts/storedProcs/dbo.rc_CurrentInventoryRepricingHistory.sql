
IF OBJECT_ID(N'[dbo].[rc_CurrentInventoryRepricingHistory]', 'P') IS NOT NULL
    DROP PROCEDURE [dbo].[rc_CurrentInventoryRepricingHistory] 
GO
CREATE PROCEDURE [dbo].[rc_CurrentInventoryRepricingHistory] @DealerID INT
AS
    SET NOCOUNT ON;
    WITH    InventoryPriceHistory
              AS ( SELECT   [i].[InventoryID]
                          , [ilph1].[ListPrice]
                          , [ilph1].[DMSReferenceDt]
                          , ROW_NUMBER() OVER ( PARTITION BY [ilph1].[InventoryID] ORDER BY [ilph1].[DMSReferenceDt] ASC ) AS Idx
                   FROM     [FLDW].[dbo].[InventoryActive] I WITH ( NOLOCK )
                            JOIN [IMT].[dbo].[Inventory_ListPriceHistory] ilph1 WITH ( NOLOCK ) ON [ilph1].[InventoryID] = [i].[inventoryid]
                   WHERE    [ilph1].[ListPrice] > 0
                            AND [i].[businessunitid] = @DealerID
                            AND [i].[InventoryType] = 2
                 )
        SELECT  [I].[InventoryID]
              , MAX([cte].[Idx]) - 1 AS [Counter]
              , [V].[VehicleYear] AS vYear
              , [MMG].[Make] vMake
              , [MMG].[Model] vModel
              , [V].[BaseColor]
              , [V].[VehicleTrim]
              , [V].[VIN]
              , [DBT].[Segment] DisplayBodyType
              , [I].[StockNumber]
              , [I].[InventoryReceivedDate]
              , [I].[UnitCost]
              , [I].[MileageReceived]
              , MAX(CASE [cte].[Idx]
                      WHEN 1 THEN [cte].[ListPrice]
                    END) AS InitialPrice
              , MAX(CASE [cte].[Idx]
                      WHEN 2 THEN [cte].[ListPrice]
                    END) AS Reprice1
              , MAX(CASE [cte].[Idx]
                      WHEN 3 THEN [cte].[ListPrice]
                    END) AS Reprice2
              , MAX(CASE [cte].[Idx]
                      WHEN 4 THEN [cte].[ListPrice]
                    END) AS Reprice3
              , MAX(CASE [cte].[Idx]
                      WHEN 5 THEN [cte].[ListPrice]
                    END) AS Reprice4
              , MAX(CASE [cte].[Idx]
                      WHEN 6 THEN [cte].[ListPrice]
                    END) AS Reprice5
              , MAX(CASE [cte].[Idx]
                      WHEN 7 THEN [cte].[ListPrice]
                    END) AS Reprice6
              , MAX(CASE [cte].[Idx]
                      WHEN 1 THEN [cte].[DMSReferenceDT]
                    END) AS DMSdt0
              , MAX(CASE [cte].[Idx]
                      WHEN 2 THEN [cte].[DMSReferenceDT]
                    END) AS DMSdt1
              , MAX(CASE [cte].[Idx]
                      WHEN 3 THEN [cte].[DMSReferenceDT]
                    END) AS DMSdt2
              , MAX(CASE [cte].[Idx]
                      WHEN 4 THEN [cte].[DMSReferenceDT]
                    END) AS DMSdt3
              , MAX(CASE [cte].[Idx]
                      WHEN 5 THEN [cte].[DMSReferenceDT]
                    END) AS DMSdt4
              , MAX(CASE [cte].[Idx]
                      WHEN 6 THEN [cte].[DMSReferenceDT]
                    END) AS DMSdt5
              , DATEDIFF(DD, [i].[InventoryReceivedDate], GETDATE()) 'Age'
              , CASE WHEN DATEDIFF(DD, [i].[InventoryReceivedDate], GETDATE()) >= 60 THEN 60
                     WHEN DATEDIFF(DD, [i].[InventoryReceivedDate], GETDATE()) >= 50 THEN 50
                     WHEN DATEDIFF(DD, [i].[InventoryReceivedDate], GETDATE()) >= 40 THEN 40
                     WHEN DATEDIFF(DD, [i].[InventoryReceivedDate], GETDATE()) >= 30 THEN 30
                     WHEN DATEDIFF(DD, [i].[InventoryReceivedDate], GETDATE()) >= 21 THEN 21
                     ELSE 0
                END AS 'Bucket'
              , [IB].[BookValue] BookValue
              , [TP].[Description] + COALESCE(' - ' + [TPC].[Category], '') Book
        FROM    [FLDW].[dbo].[InventoryActive] I WITH ( NOLOCK )
                JOIN [FLDW].[dbo].[Vehicle] v WITH ( NOLOCK ) ON [i].[VehicleID] = [v].[VehicleID]
                JOIN [FLDW].[dbo].[MakeModelGrouping] MMG WITH ( NOLOCK ) ON [V].[MakeModelGroupingid] = [MMG].[MakeModelGroupingID]
                JOIN [IMT].[dbo].[Segment] DBT WITH ( NOLOCK ) ON [DBT].[SegmentID] = [MMG].[SegmentID]
                JOIN [IMT].[dbo].[DealerPreference] DP WITH ( NOLOCK ) ON [DP].[BusinessUnitID] = [I].[BusinessUnitID]
                JOIN [IMT].[dbo].[ThirdParties] TP WITH ( NOLOCK ) ON [TP].[ThirdPartyID] = [DP].[GuideBookID]
                INNER JOIN InventoryPriceHistory cte ON [cte].[InventoryID] = [I].[InventoryID]
                                                        AND [cte].[Idx] <= 7
                LEFT JOIN [FLDW].[dbo].[ThirdPartyCategories] TPC WITH ( NOLOCK ) ON [TPC].[ThirdPartyID] = [TP].[ThirdPartyID]
                                                                                     AND [TPC].[ThirdPartyCategoryID] = [DP].[BookOutPreferenceId]
                LEFT JOIN [FLDW].[dbo].[InventoryBookout_F] IB WITH ( NOLOCK ) ON ( [IB].[InventoryID] = [I].[InventoryID]
                                                                                    AND [IB].[ThirdPartyCategoryID] = [DP].[BookOutPreferenceId]
                                                                                    AND [IB].[BookoutValueTypeID] = 2
                                                                                  )
        WHERE   [I].[BusinessUnitID] = @DealerID
        GROUP BY [I].[InventoryID]
              , [V].[VehicleYear]
              , [MMG].[Make]
              , [MMG].[Model]
              , [V].[BaseColor]
              , [V].[VehicleTrim]
              , [V].[VIN]
              , [DBT].[Segment]
              , [I].[StockNumber]
              , [I].[InventoryReceivedDate]
              , [I].[UnitCost]
              , [I].[MileageReceived]
              , [IB].[BookValue]
              , [TPC].[Category]
              , [TP].[Description]

GO
GRANT EXECUTE ON [dbo].[rc_CurrentInventoryRepricingHistory] TO [StoredProcedureUser]
GO
