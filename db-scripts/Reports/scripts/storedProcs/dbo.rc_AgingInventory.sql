
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_AgingInventory]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_AgingInventory] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_AgingInventory] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_AgingInventory]
	@DealerGroupID INT,
	@MemberID      INT
AS

SET NOCOUNT ON;


DECLARE @AgeBuckets TABLE (Low INT, High INT, BucketNumber TINYINT)

INSERT INTO @AgeBuckets (Low, High, BucketNumber) VALUES (-2147483647, 30, 1)
INSERT INTO @AgeBuckets (Low, High, BucketNumber) VALUES (31, 45, 2)
INSERT INTO @AgeBuckets (Low, High, BucketNumber) VALUES (46, 60, 3)
INSERT INTO @AgeBuckets (Low, High, BucketNumber) VALUES (61, 75, 4)
INSERT INTO @AgeBuckets (Low, High, BucketNumber) VALUES (76, 2147483647, 5)


SELECT	Dealer.BusinessUnitID,
		Dealer.BusinessUnit BusinessUnitName,
		Inventory.CurrentVehicleLight,
		Inventory.AgeInDays,
		COALESCE(AB.BucketNumber, 0) 'Bucket Value',
		Vehicle.VehicleYear,
		MakeModel.MAKE,
		MakeModel.MODEL,
		Segment.Segment DisplayBodyType,
		CASE WHEN Vehicle.VehicleTrim = 'N/A' THEN ''
			WHEN Vehicle.VehicleTrim = 'UNKNOWN' THEN ''
			ELSE Vehicle.VehicleTrim
		END VehicleTrim,
		Vehicle.BaseColor,
		Inventory.UnitCost,
		Inventory.MileageReceived,
		Inventory.TradeOrPurchase,
		Inventory.ListPrice,
		Vehicle.VIN,
		InventoryBookout.UnitCost UnitCostIB,
		InventoryBookout.BookValue BookValueIB,
		InventoryBookout.ThirdPartyCategoryID ThirdPartyCategoryIDIB,
		Inventory.StockNumber
FROM	[IMT].dbo.BusinessUnit Dealer WITH(NOLOCK)
JOIN	dbo.MemberAccess(@DealerGroupID, @MemberID) MA ON MA.BusinessUnitID = Dealer.BusinessUnitID
JOIN	[IMT].dbo.BusinessUnitRelationship R ON R.BusinessUnitID = Dealer.BusinessUnitID
JOIN	[FLDW].dbo.Inventory Inventory ON Inventory.BusinessUnitID = Dealer.BusinessUnitID
JOIN	[FLDW].dbo.Vehicle Vehicle ON (Vehicle.VehicleID = Inventory.VehicleID And Vehicle.BusinessUnitID = Inventory.BusinessUnitID)
JOIN	[FLDW].dbo.Segment ON Segment.SegmentID = Vehicle.SegmentID
JOIN	[FLDW].dbo.MakeModelGrouping MakeModel ON MakeModel.MakeModelGroupingID = Vehicle.MakeModelGroupingID
JOIN	[FLDW].dbo.DealerPreference Preferences ON Inventory.BusinessUnitID = Preferences.BusinessUnitID
LEFT JOIN [FLDW].dbo.InventoryBookout_F InventoryBookout ON (InventoryBookout.InventoryID = Inventory.InventoryID AND InventoryBookout.ThirdPartyCategoryID = Preferences.BookOutPreferenceID)
CROSS JOIN @AgeBuckets AB
WHERE	R.ParentID = @DealerGroupID
AND		Inventory.InventoryType = 2
AND		Inventory.InventoryActive = 1
AND		Inventory.CurrentVehicleLight != 0
AND		Inventory.AgeInDays BETWEEN AB.Low AND AB.High
ORDER
BY		Dealer.BusinessUnit
GO
