IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ValidateParameter_TradeOrPurchase]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[ValidateParameter_TradeOrPurchase] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[ValidateParameter_TradeOrPurchase] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[ValidateParameter_TradeOrPurchase]
	@TradeOrPurchase VARCHAR(1),
	@TPFilterLow INT OUT,
	@TPFilterHigh INT OUT
AS
/* --------------------------------------------------------------------
 * 
 * $Id: dbo.ValidateParameter_TradeOrPurchase.sql,v 1.2 2008/08/07 21:00:18 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Validate the TradeOrPurchase parameter filter
 * 
 * Parameters
 * ----------
 *
 * @TradeOrPurchase	
 *	'A' - ALL - Filters nothing
 *	'T' - Trades - Filters where InventoryType is Trade
 *	'P' - Purchase - Filters where InventoryType is Purchase
 * 
 * Return Values
 * -------------
 * Since TradeOrPurchase is an enum value (Purchase = 1, Trade = 2),
 * two values are used as return values to accomadate the 'All' option.
 *
 * @TPFilterLow - The low value
 * @TPFilterHigh - The high value
 * 
 * When @TradeOrPurchase passed in is 
 *  'P', @TPFilterLow and @TPFilterHigh are both 1
 *  'T', @TPFilterLow and @TPFilterHigh are both 2
 *  'A', @TPFilterLow is 1 and @TPFilterHigh is 2
 * 
 * Usage
 * -----
 * 
 * DECLARE @err INT, @TradeOrPurchase VARCHAR(1), @TPFilterLow INT, @TPFilterHigh INT
 * SET @TradeOrPurchase = 'A'
 * EXEC @err = [dbo].[ValidateParameter_TradeOrPurchase] @TradeOrPurchase, @TPFilterLow out, @TPFilterHigh out
 * IF @err <> 0 THEN GOTO Failed
 * Failed:
 * -------------------------------------------------------------------- */	

SET NOCOUNT ON

IF @TradeOrPurchase = 'A'
BEGIN
	SELECT @TPFilterLow = 1, @TPFilterHigh = 2
END

IF @TradeOrPurchase = 'T'
BEGIN
	SELECT @TPFilterLow = 2, @TPFilterHigh = 2
END

IF @TradeOrPurchase = 'P'
BEGIN
	SELECT @TPFilterLow = 1, @TPFilterHigh = 1
END

IF	@TradeOrPurchase <> 'A' 
	AND @TradeOrPurchase <> 'T' 
	AND @TradeOrPurchase <> 'P'
BEGIN 
	RAISERROR ('The @TradeOrPurchase parameter must be A, T, or P.',16,1)
	RETURN 50000
END

RETURN 0
