 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_SalesByAppraiser_Wholesale]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_SalesByAppraiser_Wholesale] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_SalesByAppraiser_Wholesale] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_SalesByAppraiser_Wholesale]
	@PeriodID INT,
	@DealerID INT,
	@AppraisalTypeID INT
AS

SET NOCOUNT ON;

SET ANSI_WARNINGS OFF;

DECLARE @BeginDate SMALLDATETIME
DECLARE @EndDate SMALLDATETIME

SELECT	@BeginDate = BeginDate, @EndDate = EndDate
FROM	[FLDW].dbo.Period_D
WHERE	PeriodID = @PeriodID

DECLARE @TradeOrPurchase TINYINT

/**
 *  Behold some great design:
 *
 *	SELECT * FROM IMT.dbo.AppraisalType
 *	SELECT * FROM IMT.dbo.lu_TradeOrPurchase
 */
 
SELECT @TradeOrPurchase = 
	CASE @AppraisalTypeID
		WHEN 1 THEN 2
		WHEN 2 THEN 1
		ELSE 3
	END
	
SELECT	VS.FrontEndGross, VS.BackEndGross, VS.DealDate, VS.FinanceInsuranceDealNumber, VS.SaleDescription, 
		VS.VehicleMileage, v.BaseColor, v.VehicleYear, v.VehicleTrim, VBS.Segment AS Descriptions, VS.SalePrice,
		MMG.Line AS Model, MMG.Make, MMG.GroupingDescriptionID, I.InventoryReceivedDate AS InventoryRecievedDate, 
		I.TradeOrPurchase, I.UnitCost, I.BusinessUnitID, I.InventoryType, I.InventoryID, I.DaysToSale, I.StockNumber,
		I.InitialVehicleLight, A.Value as AppraisalValue, AV.AppraiserName 

FROM	[FLDW].dbo.Inventory AS I
JOIN	[FLDW].dbo.VehicleSale AS VS ON I.InventoryID = VS.InventoryID
JOIN	[FLDW].dbo.Vehicle AS v ON I.BusinessUnitID = v.BusinessUnitID AND I.VehicleID = v.VehicleID
JOIN	[FLDW].dbo.MakeModelGrouping AS MMG ON v.MakeModelGroupingID = MMG.MakeModelGroupingID
JOIN	[FLDW].dbo.Segment AS VBS ON v.SegmentID = VBS.SegmentID
LEFT JOIN	[FLDW].dbo.Appraisal_F A ON I.BusinessUnitID = A.BusinessUnitID AND V.vehicleId = A.vehicleId 
LEFT JOIN	[FLDW].dbo.AppraisalValues AV on A.Value = AV.Value and A.AppraisalId = AV.AppraisalID AND A.AppraisalValueID=AV.AppraisalValueID

WHERE	I.BusinessUnitID = @DealerID
AND		VS.SaleDescription = 'W'
AND		VS.DealDate BETWEEN @BeginDate AND @EndDate
AND		I.DaysToSale < 30
AND		I.TradeOrPurchase = @TradeOrPurchase
--AND 	A.AppraisalTypeID = @AppraisalTypeID -- users may forget to mark appraisal as trade or purchase! 
ORDER BY DealDate DESC



GO


