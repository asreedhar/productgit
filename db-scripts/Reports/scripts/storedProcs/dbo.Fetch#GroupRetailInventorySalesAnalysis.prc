

--/****** Object:  StoredProcedure [dbo].[Fetch#GroupRetailInventorySalesAnalysis]    Script Date: 08/28/2014 10:42:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Fetch#GroupRetailInventorySalesAnalysis]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Fetch#GroupRetailInventorySalesAnalysis]
GO



/****** Object:  StoredProcedure [dbo].[Fetch#GroupRetailInventorySalesAnalysis]    Script Date: 08/28/2014 10:42:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Pradnya Sawant
-- Create date: 2014-08-11
-- Last Modified By : Pradnya Sawant
-- Last Modified Date : 2014-08-11
-- Description:	This stored procedure returns the data fro Group Level Retail Inventory Sales Analysis 
-- =============================================


CREATE PROCEDURE [dbo].[Fetch#GroupRetailInventorySalesAnalysis]
	@PeriodID INT,
	@DealerGroupID INT,
	@TradeInType VARCHAR(1),
	@AccessibleDealerID VARCHAR(1000),
	@SelectedDealerID VARCHAR(1000)
	
AS

BEGIN

SET NOCOUNT ON

--DECLARE
--	@PeriodID INT=1,
--	@DealerGroupID INT=100068,
--	@TradeInType VARCHAR(1) = 'A',
--	@AccessibleDealerID VARCHAR(1000)='106800,101929,100652,100857,101219,102820,102821,102822,102854,104790,105062,105063,105121,105443,105444,105445,105446',
--	@SELECTedDealerID VARCHAR(1000)='106800,100147,100148,100590,100650,100659,100857,101219,102820,102821,102822'
	
DECLARE
	@LocalPeriodID INT,
	@LocalDealerID INT,
	@LocalTradeOrPurchase VARCHAR(1),
	@RetailCount INT,
	@WholesaleCount INT,
	@LocalAccessibleDealerID VARCHAR(1000),
	@LocalSelectedDealerID VARCHAR(1000)
	
	
	SELECT @LocalPeriodID=@PeriodID, @LocalDealerID=@DealerGroupID, @LocalTradeOrPurchase=@TradeInType,
		   @LocalAccessibleDealerID=@AccessibleDealerID,@LocalSelectedDealerID=@SELECTedDealerID

	DECLARE @err INT, @TPFilterLow INT, @TPFilterHigh INT
	EXEC @err = Reports.[dbo].[ValidateParameter_TradeOrPurchase] @LocalTradeOrPurchase, @TPFilterLow out, @TPFilterHigh out
	IF @err <> 0 GOTO Failed
	
	IF @LocalTradeOrPurchase='A' 
    SET @TPFilterHigh=3 --Including 'Unknown' trade purchase type

DECLARE @Buckets Table (
	counter tinyint,
	daysLow int,
	daysHigh int,
	saleDesc varchar(1),
	bucketDesc varchar(20)
)

INSERT INTO @Buckets (counter, daysLow, daysHigh, saleDesc, bucketDesc) VALUES (0,0,29,'R','Retailed 0-29 Days')
INSERT INTO @Buckets (counter, daysLow, daysHigh, saleDesc, bucketDesc) VALUES (1,30,59,'R','Retailed 30-59 Days')
INSERT INTO @Buckets (counter, daysLow, daysHigh, saleDesc, bucketDesc) VALUES (2,60,2147483647,'R','Retailed 60+ Days')
INSERT INTO @Buckets (counter, daysLow, daysHigh, saleDesc, bucketDesc) VALUES (3,30,2147483647,'W','Aged Wholesale')

DECLARE @BeginDate SMALLDATETIME, @EndDate SMALLDATETIME


Select @BeginDate=StartDate,@EndDate=EndDate FROM IMT.dbo.GetDatesFromPeriodId(@LocalPeriodID,1);



SELECT B.BusinessUnitID,B.BusinessUnit 
INTO #Dealers
FROM IMT.DBO.BusinessUnit B
INNER JOIN IMT.dbo.BusinessUnitRelatiONship rel ON B.BusinessUnitID=rel.BusinessUnitID
INNER JOIN IMT.dbo.BusinessUnit B2 ON B2.BusinessUnitID = rel.ParentID
WHERE B2.BusinessUnitID=@LocalDealerID and B.Active=1;

WITH TEMP AS
(
SELECT I.InventoryID,VS.SaleDescriptiON, I.BusinessUnitID
FROM	[FLDW].dbo.Inventory I
JOIN	[FLDW].dbo.VehicleSale VS ON I.InventoryID = VS.InventoryID
JOIN	@Buckets as B ON ((I.DaysToSale between B.daysLow and B.daysHigh) AND B.SaleDesc = VS.SaleDescriptiON)
JOIN    #Dealers D ON D.BusinessUnitID =I.BusinessUnitID
WHERE	I.InventoryType = 2
AND		I.TradeOrPurchase BETWEEN @TPFilterLow AND @TPFilterHigh
AND		VS.DealDate Between @BeginDate And @EndDate
AND		I.UnitCost>0
AND		VS.SalePrice>0
AND		I.MileageReceived>0
)
--AttemptedRetail
,AttemptedRetailCount  AS

(
SELECT COUNT(T.InventoryID) AS AttemptedRetail, T.BusinessUnitID AS BusinessUnitID
FROM	Temp T

JOIN    #Dealers D ON D.BusinessUnitID=T.BusinessUnitID
GROUP BY T.BusinessUnitID

)
--RetailCount
,Retailed AS
(
SELECT COUNT(T.InventoryID) AS RetailCount, T.BusinessUnitID AS BusinessUnitID
FROM	Temp T
JOIN    #Dealers D ON D.BusinessUnitID=T.BusinessUnitID
WHERE	T.SaleDescriptiON='R'
GROUP BY T.BusinessUnitID
)

SELECT	I.BusinessUnitID AS BusinessUnitID,
		B.counter,
		COUNT(counter) DealCount,
		MAX(CAST(T.AttemptedRetail AS FLOAT)) AS PercentSaleDescriptiON,
		MAX(bucketDesc) BucketDesc,
		B.SaleDesc,
		SUM(CAST(VS.FrontEndGross AS FLOAT)) TotalFrONtEndGross,
		AVG(CAST(VS.FrontEndGross AS FLOAT)) AvgFrONtEndGross,
	   CASE WHEN CAST (R.RetailCount AS FLOAT)>0 THEN(SUM(CAST(VS.FrontEndGross AS FLOAT))/CAST (R.RetailCount AS FLOAT)) ELSE 0 END AS TrueAverageGross
INTO #FinalTable
FROM	[FLDW].dbo.Inventory I
JOIN	[FLDW].dbo.VehicleSale VS ON I.InventoryID = VS.InventoryID
JOIN	@Buckets AS B ON ((I.DaysToSale between B.daysLow and B.daysHigh) AND B.SaleDesc = VS.SaleDescriptiON)
JOIN    #Dealers D ON D.BusinessUnitID=I.BusinessUnitID
JOIN    AttemptedRetailCount T ON T.BusinessUnitID=I.BusinessUnitID
JOIN    Retailed R ON R.BusinessUnitID=I.BusinessUnitID

WHERE	I.InventoryType = 2
AND		I.TradeOrPurchase BETWEEN @TPFilterLow AND @TPFilterHigh
AND		VS.DealDate Between @BeginDate And @EndDate
AND		I.UnitCost>0
AND		VS.SalePrice>0
AND		I.MileageReceived>0
GROUP BY B.counter, B.SaleDesc,i.BusinessUnitID, R.RetailCount
Order By B.counter;


--RetailCount
WITH RetailCount as
(

SELECT sum(DealCount) as RetailCounter, F.BusinessUnitID as BusinessUnitID
FROM #FinalTable F
JOIN #Dealers D ON D.BusinessUnitID=F.BusinessUnitID 
WHERE SaleDesc='R'
GROUP BY F.BusinessUnitID
)
--WholesaleCount

,WholesaleCount AS
(
SELECT sum(DealCount) as WHolesaleCounter, F.BusinessUnitID  as BusinessUnitID

FROM #FinalTable F
JOIN    #Dealers D ON D.BusinessUnitID=F.BusinessUnitID
WHERE SaleDesc='W'
GROUP BY F.BusinessUnitID
)


SELECT  F.BusinessUnitID AS BusinessUnitID, D.BusinessUnit as BusinessUnit,
		Avg(F.PercentSaleDescriptiON)PercentSaleDescriptiON,
		CASE WHEN CAST(Avg(F.PercentSaleDescriptiON)AS FLOAT)>0
		THEN (MAX(CAST (R.RetailCounter AS FLOAT))/CAST(Avg(F.PercentSaleDescriptiON)AS FLOAT)) 
		ELSE 0 END RetailPercent,
		--,CASE WHEN CAST(Avg(F.PercentSaleDescriptiON)AS FLOAT)>0 THEN (CAST(MAX(W.WholesaleCounter)AS FLOAT)/CAST(Avg(F.PercentSaleDescriptiON)AS FLOAT)) ELSE 0 END WholesalePercent,
		CAST(MAX(W.WholesaleCounter)AS FLOAT)AS  WholesalePercent,
		CASE WHEN R.RetailCounter>0 
		THEN SUM(TotalFrONtEndGross)/MAX(R.RetailCounter) 
		ELSE 0 END TrueFrONtEnd,
		SUM(TotalFrONtEndGross) TotalFrONtEndGross

INTO #Res
FROM #FinalTable F
JOIN    #Dealers D ON D.BusinessUnitID=F.BusinessUnitID
LEFT JOIN RetailCount R ON R.BusinessUnitID=F.BusinessUnitID
LEFT JOIN WholesaleCount W ON W.BusinessUnitID=F.BusinessUnitID

GROUP BY F.BusinessUnitID,R.RetailCounter, D.BusinessUnit


 CREATE TABLE #Result
 (
Dealership INT,
BusinessUnit VARCHAR(100),
AttempedRetail int,
Retailed029 float,
Retailed3059 float,
Retailed60plus float,
AgedWholesale float,
TotalFrONtEndGross float,
TrueAverageGross float
 )
 
 INSERT INTO #Result 
 SELECT 
 R.businessUnitID,
 R.BusinessUnit,
 R.PercentSaleDescriptiON,
 0,
 0,
 0,
 R.WholesalePercent,
 R.TotalFrONtEndGross,
 R.TrueFrONtEnd
 FROM #Res R

--Bucket 0-29

UPDATE #Result
	SET Retailed029 = F.DealCount
	FROM #FinalTable F
	JOIN    #Dealers D ON D.BusinessUnitID=F.BusinessUnitID
	WHERE Dealership=F.BusinessUnitID
	AND F.counter=0    
	         
--Bucket 30-59
  
UPDATE #Result 
	SET Retailed3059= F.DealCount
	FROM #FinalTable F 
	JOIN    #Dealers D ON D.BusinessUnitID=F.BusinessUnitID
	WHERE Dealership=F.BusinessUnitID
	AND F.counter=1 
	        
--Bucket 60+
	
UPDATE #Result 
	SET Retailed60plus= F.DealCount
	FROM #FinalTable F
	JOIN #Dealers D ON D.BusinessUnitID=F.BusinessUnitID
	WHERE Dealership=F.BusinessUnitID
	AND F.counter=2			  


		 --Get Selected Dealers
 		DECLARE @SELECTed table(Id int)
		INSERT INTO @SELECTed
		SELECT	CAST(NULLIF(S.Value,'') AS INT) FROM	imt.dbo.Split(@SELECTedDealerID,',') S 
		WHERE	NULLIF(@SELECTedDealerID,'') IS NOT NULL AND LTRIM(RTRIM(S.Value))<>''
		
		--Get Accessible Dealers		
		DECLARE @Accessible table(Id int)
		INSERT INTO @Accessible
		SELECT	CAST(NULLIF(S.Value,'') AS INT) FROM	imt.dbo.Split(@AccessibleDealerID,',') S 
		WHERE	NULLIF(@AccessibleDealerID,'') IS NOT NULL  AND LTRIM(RTRIM(S.Value))<>''
		
		--SHOW SELECTED DEALERS
		
		SELECT  s.Id ,
				BusinessUnit,
				SUM(AttempedRetail) AS AttemptedRetail ,
				SUM(Retailed029) AS Retail029 ,
				SUM(Retailed3059) AS Retailed3059,
				SUM(Retailed60plus) AS Retailed60plus,
				SUM(AgedWholesale) AS AgedWholesale,
				SUM(TotalFrONtEndGross) AS TotalFrontEndGross,
				SUM(TotalFrONtEndGross)/ (SUM(Retailed029) +SUM(Retailed3059) + SUM(Retailed60plus)) AS TrueAverageGross
		FROM #Result R									
		JOIN @SELECTed s ON s.Id=R.Dealership
		GROUP BY s.Id,BusinessUnit
		--ORDER BY s.Id
		
		UNION ALL
		
		--SHOW NOT SELECTED BUT ACCESSIBLE DEALERS(IN OTHER DEALERSHIP U MANAGE)
		
		SELECT  null,
		        'Other Dealerships You Manage' ,
				SUM(AttempedRetail) AS AttemptedRetail ,
				SUM(Retailed029) AS Retail029 ,
				SUM(Retailed3059) AS Retailed3059,
				SUM(Retailed60plus) AS Retailed60plus,
				SUM(AgedWholesale) AS AgedWholesale,
				SUM(TotalFrONtEndGross) AS TotalFrontEndGross,
				SUM(TotalFrONtEndGross)/ (SUM(Retailed029) +SUM(Retailed3059) + SUM(Retailed60plus)) AS TrueAverageGross
		FROM #Result R							
		JOIN @Accessible A ON A.Id=R.Dealership
		WHERE  A.Id NOT IN (SELECT S.Id FROM @SELECTed S)
		
		UNION ALL
		
		--SHOW NOT ACCESSIBLE(IN OTHER GROUP DEALERSHIP)
		
		SELECT  null,
				'In Group Dealerships' ,
				SUM(AttempedRetail) AS AttemptedRetail ,
				SUM(Retailed029) AS Retail029 ,
				SUM(Retailed3059) AS Retailed3059,
				SUM(Retailed60plus) AS Retailed60plus,
				SUM(AgedWholesale) AS AgedWholesale,
				SUM(TotalFrONtEndGross) AS TotalFrontEndGross,
				SUM(TotalFrONtEndGross)/ (SUM(Retailed029) +SUM(Retailed3059) + SUM(Retailed60plus)) AS TrueAverageGross
		FROM #Result R							
		WHERE R.Dealership NOT IN (SELECT S.Id FROM @Accessible S)
		
		UNION ALL
		
		--WHOLE GROUP
		
		SELECT  Null,
				'Whole Group' ,
				SUM(AttempedRetail) AS AttemptedRetail ,
				SUM(Retailed029) AS Retail029 ,
				SUM(Retailed3059) AS Retailed3059,
				SUM(Retailed60plus) AS Retailed60plus,
				SUM(AgedWholesale) AS AgedWholesale,
				SUM(TotalFrONtEndGross) AS TotalFrontEndGross,
				SUM(TotalFrONtEndGross)/ (SUM(Retailed029) +SUM(Retailed3059) + SUM(Retailed60plus)) AS TrueAverageGross

		FROM #Result R		
												

DROP TABLE #FinalTable
DROP TABLE #Res
DROP TABLE #result
DROP TABLE #Dealers

Failed:

SET NOCOUNT OFF
END

GO
GRANT EXEC ON [Reports].[dbo].[Fetch#GroupRetailInventorySalesAnalysis] TO firstlook 
GO

