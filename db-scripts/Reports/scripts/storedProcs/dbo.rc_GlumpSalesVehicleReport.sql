SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_GlumpSalesVehicleReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[rc_GlumpSalesVehicleReport]
GO

CREATE PROCEDURE [dbo].[rc_GlumpSalesVehicleReport]
---Parameters-------------------------------------------------------------------------------------
--
	@segment		INT,
	@highBucket		INT,
	@lowBucket		INT,
	@periodId		INT,
	@businessUnitId		INT
--
---History--------------------------------------------------------------------------------------
--
--	JMW	07/16/2012	Created -- BUGZID: 15978
--      DR      09/11/2012      Remove retailWholesale flag. Glump is USED RETAIL ONLY (FB: 23047)
--
---Notes----------------------------------------------------------------------------------------
--	
------------------------------------------------------------------------------------------------
AS 

SET NOCOUNT ON


DECLARE @beginDate DATETIME, @endDate DATETIME

SELECT	@beginDate=BeginDate,
		@endDate=EndDate 
FROM	FLDW.dbo.Period_D
WHERE	PeriodID = @periodId

SELECT VS.DealDate AS [RDealDate]
	  ,V.VehicleYear AS [RYear] 
	  ,MMG.Model AS [RModel]
	  ,V.VehicleTrim AS [RTrim]
	  ,S.Segment AS [RSegment]
	  ,COALESCE(V.BaseColor, V.ExtColor, 'UNKNOWN') AS [RColor]
	  ,VS.VehicleMileage AS [RMileage]
	  ,I.UnitCost AS [RUnitCost]
	  ,VS.BackEndGross AS [RBackEndGross]
	  ,VS.FrontEndGross AS [RFrontEndGross]
	  ,DATEDIFF("d", I.InventoryReceivedDate, VS.DealDate) AS [RDaysToSale]
	  ,VS.FinanceInsuranceDealNumber AS [RDealNumber]
	  ,CASE WHEN VS.SaleDescription = 'R' THEN 'Retail' WHEN VS.SaleDescription = 'W' THEN 'Wholesale' ELSE 'Unknown' END AS [RetailOrWholesale],
	  @segment SEGMENT
FROM FLDW.dbo.VehicleSale VS
JOIN FLDW.dbo.Inventory I ON VS.InventoryID = I.InventoryID
JOIN IMT.dbo.tbl_Vehicle V ON I.VehicleID = V.VehicleID
JOIN IMT.dbo.MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID
JOIN FLDW.dbo.Segment S ON MMG.SegmentID = S.SegmentID
WHERE I.BusinessUnitID = @businessUnitId AND
	  VS.DealDate BETWEEN @beginDate AND @endDate AND
	  I.ListPrice BETWEEN @lowBucket AND @highBucket AND
	  (@segment = -1 OR S.SegmentID = @segment) AND
	  VS.SaleDescription = 'R' AND
	  I.InventoryType = 2  AND
	  (I.ListPrice > 0 AND I.UnitCost > 0)
