SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[dbo].[rc_GrossProfitByDaysInStock#Pricing]')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE [dbo].[rc_GrossProfitByDaysInStock#Pricing]
GO


CREATE PROCEDURE [dbo].[rc_GrossProfitByDaysInStock#Pricing]
    @PeriodID INT,
    @DealerID INT
AS 


/****************************************************************************************
**
**	This procedure is used in the report GrossProfitByDaysInStock.rdl
**	This procedure calculates pricing information for a given dealer/period and is displayed in
**	the second table of the report..
**
********************************************************************************************
**
**	05/04/2010	MAK		Create procedure
**
*******************************************************************************************/

    DECLARE @BottomRightLatitude DECIMAL(7, 4),
        @TopLeftLatitude DECIMAL(7, 4),
        @TopLeftLongitude DECIMAL(7, 4),
        @BottomRightLongitude DECIMAL(7, 4),
        @ZipCode VARCHAR(5),
        @SearchRadius SMALLINT,
        @DistanceBucketID TINYINT,
        @ExcludeNoPriceFromCalc BIT,
        @ExcludeLowPriceOutliersMultiplier DECIMAL(4, 2),
        @ExcludeHighPriceOutliersMultiplier DECIMAL(4, 2),
        @MarketDaysSupplyBasePeriod TINYINT,
        @BasePeriodThresholddate DATETIME

    SELECT  @ZipCode = PO.ZipCode,
            @SearchRadius = PingII_DefaultSearchRadius,
            @ExcludeNoPriceFromCalc = PingII_ExcludeNoPriceFromCalc,
            @ExcludeLowPriceOutliersMultiplier = PingII_ExcludeLowPriceOutliersMultiplier,
            @ExcludeLowPriceOutliersMultiplier = PingII_ExcludeHighPriceOutliersMultiplier,
            @MarketDaysSupplyBasePeriod = MarketDaysSupplyBasePeriod,
            @BasePeriodThresholdDate = DATEADD(DD,
                                               -1
                                               * ( MarketDaysSupplyBasePeriod ),
                                               DATEADD(DD,
                                                       DATEDIFF(DD, 0, GETDATE()),
                                                       0)),
            @DistanceBucketID = DB.DistanceBucketID
    FROM    [Market].Pricing.Owner PO
            JOIN [IMT]..DealerPreference_Pricing DPP ON PO.OwnerEntityID = DPP.BusinessUnitID
            JOIN [Market].Pricing.DistanceBucket DB ON DB.Distance = DPP.PingII_DefaultSearchRadius
    WHERE   PO.OwnerEntityID = @DealerID

    IF ( @SearchRadius IS NULL ) 
        BEGIN
            SELECT  @ZipCode = BU.ZipCode,
                    @SearchRadius = PingII_DefaultSearchRadius,
                    @ExcludeNoPriceFromCalc = PingII_ExcludeNoPriceFromCalc,
                    @ExcludeLowPriceOutliersMultiplier = PingII_ExcludeLowPriceOutliersMultiplier,
                    @ExcludeLowPriceOutliersMultiplier = PingII_ExcludeHighPriceOutliersMultiplier,
                    @MarketDaysSupplyBasePeriod = MarketDaysSupplyBasePeriod,
                    @BasePeriodThresholdDate = DATEADD(DD,
                                                       -1
                                                       * ( MarketDaysSupplyBasePeriod ),
                                                       DATEADD(DD, DATEDIFF(DD, 0, GETDATE()), 0)),
                    @DistanceBucketID = DB.DistanceBucketID
            FROM    [IMT]..DealerPreference_Pricing DPP
                    JOIN [Market].Pricing.DistanceBucket DB ON DB.Distance = DPP.PingII_DefaultSearchRadius
                    CROSS JOIN [IMT]..BusinessUnit BU
            WHERE   DPP.BusinessUnitID = 100150
                    AND BU.BusinessUnitID = @DealerID
		
 
        END
        
    SET @MarketDaysSupplyBasePeriod = COALESCE(@MarketDaysSupplyBasePeriod, 90)
    SET @BasePeriodThresholddate = COALESCE(@BasePeriodThresholdDate,
                                            DATEADD(DD,
                                                    -1
                                                    * @MarketDaysSupplyBasePeriod,
                                                    DATEADD(DD,
                                                            DATEDIFF(DD, 0, GETDATE()),
                                                            0)))
    SELECT  @BottomRightLatitude = X.BottomRightLatitude,
            @TopLeftLatitude = X.TopLeftLatitude,
            @TopLeftLongitude = X.TopLeftLongitude,
            @BottomRightLongitude = X.BottomRightLongitude
    FROM    [Market].dbo.BoundingRectangle(@ZipCode, @SearchRadius * 1.03) X


 
    CREATE TABLE #Sales
        (
          InventoryID INT,
          AgeDesc VARCHAR(50),
          UnitCost INT,
          SalePrice INT,
          FrontEndGross DECIMAL(12, 2),
          BackEndGross DECIMAL(12, 2),
          DaysToSale SMALLINT,
          VehicleCatalogID INT,
          ModelConfigurationID INT,
          NumberOfListingsWithPrice INT,
          NumberOfListings INT,
          NumberOfSales INT,
          MarketPrice BIGINT
        )

    INSERT  INTO #Sales
            (
              InventoryID,
              AgeDesc,
              UnitCost,
              SalePrice,
              FrontEndGross,
              BackEndGross,
              DaysToSale,
              VehicleCatalogID,
              ModelConfigurationID
            )
            SELECT  IA.InventoryID,
                    CASE WHEN IA.DaysToSale < 30
                              AND S.SaleDescription = 'R' THEN '< 30 Days'
                         WHEN IA.DaysToSale >= 30
                              AND IA.DaysToSale < 60
                              AND S.SaleDescription = 'R' THEN '30 - 60 Days'
                         WHEN IA.DaysToSale >= 60
                              AND S.SaleDescription = 'R' THEN '60+ Days'
                         ELSE 'NA'
                    END,
                    IA.UnitCost,
                    S.SalePrice,
                    S.FrontEndGross,
                    S.BackEndGross,
                    IA.DaysToSale,
                    V.VehicleCatalogID,
                    MCVC.ModelConfigurationID
            FROM    [FLDW].dbo.Inventory IA
                    JOIN [FLDW].dbo.VehicleSale S ON IA.InventoryID = S.InventoryID
                    JOIN [FLDW].dbo.Vehicle V ON IA.VehicleID = V.VehicleID
                                                 AND IA.BusinessUnitID = V.BusinessUnitID
                    JOIN [VehicleCatalog].Categorization.ModelConfiguration_VehicleCatalog MCVC ON V.VehicleCatalogID = MCVC.VehicleCatalogID
                    CROSS JOIN [FLDW].dbo.Period_D Period
            WHERE   IA.BusinessUnitID = @DealerID
                    AND IA.InventoryType = 2
                    AND Period.PeriodID = @PeriodID
                    AND S.DealDate BETWEEN Period.BeginDate
                                   AND     Period.EndDate
                    AND ( S.SaleDescription = 'R' ) 


    UPDATE  SS
    SET     NumberOfListings = X.NumberOfListings,
            NumberOfListingsWithPrice = X.NumberOfListingsWithPrice,
            MarketPrice = X.MarketPrice
    FROM    ( SELECT    COUNT(*) AS NumberOfListings,
                        SUM(CASE WHEN V.ListPrice IS NULL
                                      OR V.ListPrice = 0 THEN 0
                                 ELSE 1
                            END) AS NumberOfListingsWithPrice,
                        SUM(CASE WHEN @ExcludeNoPriceFromCalc = 1
                                 THEN NULLIF(V.ListPrice, 0)
                                 ELSE V.ListPrice
                            END) AS MarketPrice,
                        SS.InventoryID
              FROM      #Sales SS
                        JOIN VehicleCatalog.Categorization.ModelConfiguration#MakeLineModelYearMatch MM ON SS.ModelConfigurationID = MM.ReferenceModelConfigurationID
                        JOIN Market.Listing.VehicleDecoded_F V WITH ( NOLOCK ) ON V.ModelConfigurationID = MM.ModelConfigurationID
                        JOIN [Market].Pricing.DistanceBucketBoundingRectangles BR
                        WITH ( NOLOCK ) ON BR.ZipCode = @ZipCode
                                           AND BR.DistanceBucketID = @DistanceBucketID
                                           AND V.Latitude BETWEEN BR.BottomRightLatitude
                                                          AND     BR.TopLeftLatitude
                                           AND V.Longitude BETWEEN BR.TopLeftLongitude
                                                           AND     BR.BottomRightLongitude
              WHERE     V.Latitude BETWEEN @BottomRightLatitude
                                   AND     @TopLeftLatitude
                        AND V.Longitude BETWEEN @TopLeftLongitude
                                        AND     @BottomRightLongitude
              GROUP BY  SS.InventoryID
            ) X
            JOIN #Sales SS ON X.InventoryID = SS.InventoryID		

    UPDATE  SS
    SET     NumberOfSales = XX.NumberOfSales
    FROM    (  SELECT   COUNT (DISTINCT (V.VIN)) AS NumberOfSales,
                                    SS.InventoryID
                          FROM      #Sales SS
                                    JOIN VehicleCatalog.Categorization.ModelConfiguration#MakeLineModelYearMatch MM ON SS.ModelConfigurationID = MM.ReferenceModelConfigurationID
                                    JOIN Market.Listing.Vehicle_Sales V WITH ( NOLOCK ) ON V.ModelConfigurationID = MM.ModelConfigurationID
                                    JOIN [Market].Pricing.DistanceBucketBoundingRectangles BR
                                    WITH ( NOLOCK ) ON BR.ZipCode = @ZipCode
                                                       AND BR.DistanceBucketID = @DistanceBucketID
                                                       AND V.Latitude BETWEEN BR.BottomRightLatitude
                                                                      AND     BR.TopLeftLatitude
                                                       AND V.Longitude BETWEEN BR.TopLeftLongitude
                                                                       AND     BR.BottomRightLongitude
                          WHERE     V.Latitude BETWEEN @BottomRightLatitude
                                               AND     @TopLeftLatitude
                                    AND V.Longitude BETWEEN @TopLeftLongitude
                                                    AND     @BottomRightLongitude
                                    AND V.SaleType = 'R'
                                    AND V.DateSold >= @BasePeriodThresholdDate
                          GROUP BY SS.InventoryID
                           ) XX
            JOIN #Sales SS ON XX.InventoryID = SS.InventoryID				
		    
     
    SELECT  AgeDesc,
            COUNT(*) AS Vehicle_Count,
            SUM(UnitCost) AS Unit_Cost,
            SUM(SalePrice) AS Internet_Price,
            SUM(MarketPrice) AS Market_Price,
            SUM(NumberOfListings) AS NumberOfListings,
            SUM(NumberOfListingsWithPrice) AS NumberOfListingsWIthPrice,
            CASE WHEN SUM(MarketPrice) > 0
                 THEN ( CAST(SUM(SalePrice) AS REAL)
                        / ( CAST(COUNT(*) AS REAL) ) )
                      / ( CAST(SUM(MarketPrice) AS REAL)
                          / ( CAST(SUM(NumberOfListingsWithPrice) AS REAL) ) )
                 ELSE 0
            END AS Percent_of_Market_Average,
            CASE WHEN SUM(NumberOfSales) > 0
                 THEN ROUND(SUM(NumberOfListings) / ( SUM(NumberOfSales)
                                                      / CAST(@MarketDaysSupplyBasePeriod AS REAL) ),
                            0)
                 ELSE 0
            END AS Market_Days_Supply,
            CAST(ROUND(SUM(FrontEndGross + BackEndGross), 0) AS INT) AS Total_Potential_Gross,
            CAST(ROUND(SUM(FrontEndGross + BackEndGross) / COUNT(*), 0) AS INT) AS Potential_Gross_Per_Vehicle
    FROM    #Sales
    GROUP BY AgeDesc

GO