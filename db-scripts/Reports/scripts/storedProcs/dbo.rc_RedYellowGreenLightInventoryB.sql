
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_RedYellowGreenLightInventoryB]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_RedYellowGreenLightInventoryB] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_RedYellowGreenLightInventoryB] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_RedYellowGreenLightInventoryB]
	@DealerID INT
AS

SET NOCOUNT ON;

SELECT	CASE I.CurrentVehicleLight WHEN 1 THEN 'Red' WHEN 2 THEN 'Yellow' WHEN 3 THEN 'Green' ELSE 'Undetermined' END CurrentVehicleLight,
		COUNT( * ) CurrentVehicleLightCount,
		CASE WHEN (SUM( CASE WHEN I.ListPrice <> 0 THEN 1 ELSE 0 END)) > 0 THEN (SUM( CASE WHEN I.ListPrice <> 0 THEN I.ListPrice ELSE 0 END)) / (SUM( CASE WHEN I.ListPrice <> 0 THEN 1 ELSE 0 END)) ELSE NULL END 'ListPrice',
		AVG( I.AgeInDays ) AgeInDays,
		I.CurrentVehicleLight 'CurrentVehicleLightNum',
		SUM(CASE WHEN IB.BookValue IS NOT NULL AND IB.BookValue > 0 Then IB.BookValue - IB.UnitCost ELSE 0 END ) 'Water',
		SUM(I.UnitCost) 'UnitCost',
		AVG(I.UnitCost) 'AvgUnitCost',
		SUM(CASE WHEN IB.BookValue IS NOT NULL AND IB.BookValue > 0 THEN 1 ELSE 0 END ) 'UnitsWithBook',
		SUM(CASE WHEN I.ListPrice <> 0 THEN 1 ELSE 0 END ) 'ListPriceUnits'

FROM	[FLDW].dbo.InventoryActive I
JOIN	[FLDW].dbo.DealerPreference DP ON DP.BusinessUnitID = I.BusinessUnitID
LEFT JOIN	[FLDW].dbo.InventoryBookout_F IB ON (
					IB.InventoryID = I.InventoryID
				AND	IB.BusinessUnitID = I.BusinessUnitID
				AND	IB.ThirdPartyCategoryID = DP.BookOutPreferenceID
				AND IB.BookoutValueTypeID = 2
				AND	IB.InventoryTypeID = 2
			)

WHERE	I.BusinessUnitID = @DealerID
AND		I.InventoryType = 2
AND		I.InventoryActive = 1

GROUP BY I.CurrentVehicleLight
ORDER BY I.CurrentVehicleLight

GO
