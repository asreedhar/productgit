IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.UpdateUsageMetricsForSalesForce1Day') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE dbo.UpdateUsageMetricsForSalesForce1Day AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on dbo.UpdateUsageMetricsForSalesForce1Day to [CustomerOperations]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO
 
ALTER PROCEDURE [dbo].[UpdateUsageMetricsForSalesForce1Day] 
	(@ScheduledRun BIT = 0,
	 @DaysBack SMALLINT = NULL,
	 @BackFromDate DATETIME = NULL)
AS

/*	
JDK 03/23/2012  - Proc originally written by SW.
	This procedure gathers metrics regarding PING tool usage for the 
	selected period.  The procedure returns, by Business Unit, the # and % of
	Number Of Retail InventoryItems 
	Number of Items that were 'PINGed'
	Number of Items that were repriced.
	Number of Items with MysteryShop  
	Number of Items with JDPower process
	Number of Items with TMV process 
	
	This entire proc is almost entirely copied from UpdateUsageMetricsForSalesForce so 
	it can use a different number of days back (1 instead of 14) and aggregate into 
	different tables. 
	
	Though there is a @DaysBack parameter, since the data are aggregated in tables and 
	reports run in SalesForce, that proc and the data in the tables need to be 
	maintained as they are.
*/
 
SET NOCOUNT ON

DECLARE @StartDate DATETIME, 
	@EndDate DATETIME,
	@DefaultDaysBack SMALLINT

SET	@DefaultDaysBack = 1 -- Current Default as defined by Laura G.
SET 	@DaysBack =Coalesce(@DaysBack, @DefaultDaysBack)

SELECT	@EndDate = Coalesce(@BackFromDate, GETDATE())
SELECT	@StartDate = DATEADD(dd, DATEDIFF(dd, 0, DATEADD(DD, -1*@DaysBack, @EndDate)), 0)
DECLARE @DateString VARCHAR(50)
DECLARE @RunID INT

BEGIN TRY

SET @DateString = CONVERT(VARCHAR(10),@StartDate,101)+ ' and '+CONVERT(VARCHAR(10),@EndDate,101)

--  This table holds all the results
INSERT
INTO SystemUsageMetricsRuns1Day
	(DateCreated,
	StartDate,
	EndDate,
	IsScheduledRun)
Values (GETDATE(),@StartDate,@EndDate,@ScheduledRun)

SET @RunID = SCOPE_IDENTITY()

CREATE 
TABLE	#PINGIIUsageResults
	(BusinessUnitID INT,
	BusinessUnit	VARCHAR(50),
	NumberOfInventoryItems	INT,
	NumberOfRetailInventoryItems	INT,
	HasPINGII	TINYINT,
	HasAcceleratorPackage VARCHAR(50),
	NumberOfItemsPINGED	INT,
	TotalDMSChangeCount	INT,
	TotalRepriceCount	INT,
	NumberRepricedIMP	INT,
	NumberRepricedeStock	INT,
	NumberRepricedPING	INT,
	NumberRepricedWriteBack	INT,
	PriceConfirmCount	INT,
	NumberInventoryItemsShopd	INT,
	NumberInventoryItemsPowered	INT,
	NumberInventoryItemsTMVed	INT,
	WBTurnedOn	TINYINT,
	MADTurnedOn	TINYINT,
	AcceleratorStore	TINYINT,
	HasEDTDescriptionProvider	TINYINT,
	EDTDescriptionProviderName	VARCHAR(50),
	NumberInternetDescriptions	INT,
	NumberInternetDescriptionsCreated INT,
	NumberInternetDescriptionsEdited INT,
	NumberClosingNotes	INT,
	NumberAppraisals INT,
	NumberAppraisalValues INT,
	NumberAppraisalForms INT,
	NumberAppraisalsPINGed INT,
	NumberMADSubscriptions INT,
	NumberLogins INT)


-- #Resources contains all the URLS From the VehiclePricingAnalyzer in the defined period.
CREATE 
TABLE	#Resources (
		Name VARCHAR(1024) NOT NULL
	)

-- #Tracking contains the Resource information broken down into readable columns.
CREATE 
TABLE	#Tracking (
		OwnerHandle VARCHAR(36) NOT NULL,
		VehicleHandle VARCHAR(36) NOT NULL,
		SearchHandle VARCHAR(36) NOT NULL,
		WasMysteryShop BIT NOT NULL,
		WasJDPower BIT NOT NULL,
		WasTMV BIT NOT NULL)

-- #TrackingInventory translates the Handles from #Tracking into DB values.
CREATE 
TABLE	#TrackingInventory (
		BusinessUnitID INT NOT NULL,
		InventoryID INT NOT NULL,
		HasMysteryShop BIT NOT NULL,
		HasJDPower BIT NOT NULL,
		HasTMV BIT NOT NULL,
		PRIMARY KEY (BusinessUnitID, InventoryID)
)

-- This is the output from the Market.Pricing.Load#InventoryStrategy 'R' procedure.
--	This returns only inventory with RETAIL strategies.
CREATE 
TABLE	#InventoryStrategy (
		InventoryID INT,
		BusinessUnitID INT,
		VehicleID INT,
		ListPrice decimal(12,2),
		InventoryActive   INT,
		InventoryType INT,
		StockNumber VARCHAR(30),
		PlanReminderDate SMALLDATETIME,
		InventoryReceivedDate SMALLDATETIME,
		AIP_EventCategoryID INT,
		HasActivePlan BIT,
		EverBeenPlanned BIT
		PRIMARY KEY (BusinessUnitID,InventoryID))

-- This table sees if the individual Inventory unit was reviewed or repriced within
-- this given time period.
CREATE 
TABLE	#InventoryAction (
		BusinessUnitID INT NOT NULL,
		InventoryID INT NOT NULL,
		HasReview BIT NOT NULL,
		HasReprice BIT NOT NULL,
		PRIMARY KEY (BusinessUnitID,InventoryID))


CREATE 
TABLE #Reprices
	(BusinessUnitID INT,
	InventoryID	INT,
	RepriceSourceID TINYINT)

---  The following code populates the various temp tables.


 
INSERT 
INTO	#Resources 
		(Name)
SELECT  DISTINCT r.Name
FROM	InteractionLog.Track.Resource R
JOIN	InteractionLog.Track.Action A 
ON		A.ResourceId = R.Id
WHERE	r.Name LIKE '/pricing/VehiclePricingAnalyzer.aspx%'
AND		A.Timestamp BETWEEN @StartDate AND @EndDate



; WITH vehicle_pricing_analyzer AS
(
      select      name,
            type = case
                  -- ?oh=x&sh=y&vh=z
                  when patindex('%?oh=%',name) < patindex('%&sh=%',name) and patindex('%&sh=%',name) < patindex('%&vh=%',name) then
                        1
                  -- ?oh=x&vh=y&sh=z
                  when patindex('%?oh=%',name) < patindex('%&vh=%',name) and patindex('%&vh=%',name) < patindex('%&sh=%',name) then
                        2
                  -- ?vh=x&oh=y&sh=z
                  when patindex('%?vh=%',name) < patindex('%&oh=%',name) and patindex('%&oh=%',name) < patindex('%&sh=%',name) then
                        3
                  -- ?vh=x&sh=y&oh=z
                  when patindex('%?vh=%',name) < patindex('%&sh=%',name) and patindex('%&sh=%',name) < patindex('%&oh=%',name) then
                        4
                  -- ?sh=x&oh=y&vh=z
                  when patindex('%?sh=%',name) < patindex('%&oh=%',name) and patindex('%&oh=%',name) < patindex('%&vh=%',name) then
                        5
                  -- ?sh=x&vh=y&oh=z
                  when patindex('%?sh=%',name) < patindex('%&vh=%',name) and patindex('%&vh=%',name) < patindex('%&oh=%',name) then
                        6
                  else
                        0
            end
      from  #Resources
),
vehicle_pricing_analyzer_params as
(
      select      name,
            oh = case
                  when type in (1,2) then
                        substring(name, patindex('%?oh=%',name)+4, 36)
                  else
                        substring(name, patindex('%&oh=%',name)+4, 36)
            end,
            vh = case
                  when type = 3 then
                        substring(name, patindex('%?vh=%',name)+4, patindex('%&oh=%',name)-patindex('%?vh=%',name)-4)
                  when type = 4 then
                        substring(name, patindex('%?vh=%',name)+4, patindex('%&sh=%',name)-patindex('%?vh=%',name)-4)
                  when type in (2,6) then
                        substring(name, patindex('%&vh=%',name)+4, patindex('%&sh=%',name)-patindex('%&vh=%',name)-4)
                  when type in (1,5) and charindex('&',name,patindex('%&vh=%',name)+4) <> 0 then
                        substring(name, patindex('%&vh=%',name)+4, charindex('&',name,patindex('%&vh=%',name)+4)-patindex('%&vh=%',name)-4)
                  else
                        substring(name, patindex('%&vh=%',name)+4, len(name)-patindex('%&vh=%',name)-4)
            end,
            sh = case
                  when type in (5,6) then
                        substring(name, patindex('%?sh=%',name)+4, 36)
                  else
                        substring(name, patindex('%&sh=%',name)+4, 36)
            end
      from  vehicle_pricing_analyzer
)

INSERT 
INTO	#Tracking (
		OwnerHandle,
		VehicleHandle,
		SearchHandle,
		WasMysteryShop,
		WasJDPower,
		WasTMV)

SELECT  oh, 
		vh, 
		sh,
		case when name like '%id=%AutoTrader%' or name like '%id=%Cars%' then 1 else 0 end,
		case when name like '%id=%JDPower%' then 1 else 0 end,
		case when name like '%id=%TMV%' then 1 else 0 end
FROM	vehicle_pricing_analyzer_params 
WHERE	SUBSTRING(vh, 1, 1) = '1'

INSERT 
INTO	#TrackingInventory (
		BusinessUnitID,
		InventoryID,
		HasMysteryShop,
		HasJDPower,
		HasTMV)

SELECT	O.OwnerEntityID,
		CAST(SUBSTRING(T.VehicleHandle, 2, LEN(VehicleHandle)-1) AS INT),
		CAST(MAX(CAST(WasMysteryShop AS TINYINT)) AS BIT),
		CAST(MAX(CAST(WasJDPower AS TINYINT)) AS BIT),
		CAST(MAX(CAST(WasTMV AS TINYINT)) AS BIT)
FROM	#Tracking T
JOIN	Market.Pricing.Owner O 
ON		O.Handle = T.OwnerHandle
WHERE	O.OwnerTypeID = 1
GROUP
BY    O.OwnerEntityID,
      CAST(SUBSTRING(T.VehicleHandle, 2, LEN(VehicleHandle)-1) AS INT)



-- Loads #SaleStrategy
EXEC Market.Pricing.Load#InventoryStrategy @SaleStrategy = 'R'


INSERT 
INTO	#InventoryAction (
		BusinessUnitID, 
		InventoryID,
		HasReview, 
		HasReprice)

SELECT  OwnerEntityID, 
		VehicleEntityID,
		CAST(MAX(CASE WHEN VehicleMarketHistoryEntryTypeID = 3 THEN 1 ELSE 0 END) AS BIT),
		CAST(MAX(CASE WHEN VehicleMarketHistoryEntryTypeID = 4 THEN 1 ELSE 0 END) AS BIT)
FROM	Market.Pricing.VehicleMarketHistory
WHERE	Created BETWEEN @StartDate AND @EndDate
AND		OwnerEntityTypeID = 1
AND		VehicleEntityTypeID = 1
AND		VehicleMarketHistoryEntryTypeID IN (3,4)
GROUP
BY		OwnerEntityID, 
		VehicleEntityID

--*************************************************-

INSERT
INTO	#PINGIIUsageResults
		(BusinessUnitID,
		BusinessUnit)
SELECT	B.BusinessUnitID,
	B.BusinessUnit
FROM	IMT..BusinessUnit B 
JOIN	IMT..DealerPreference P
ON	B.BusinessUnitID =P.BusinessUnitID
WHERE	B.Active =1
AND	B.BusinessUnitTypeID =4
AND	P.GoLiveDate IS NOT NULL
GROUP	
BY	B.BusinessUnitID,
	B.BusinessUnit

UPDATE	PG
SET	WBTurnedOn = 1
FROM	#PINGIIUsageResults PG
JOIN	IMT..DMSExportBusinessUnitPreference WB 
ON		PG.BusinessUnitID =WB.BusinessUnitID
WHERE	WB.IsActive =1

UPDATE	PG
SET		MADTurnedOn = 1
FROM	#PINGIIUsageResults PG
JOIN	IMT..DealerUpgrade U 
ON		PG.BusinessUnitID =U.BusinessUnitID
WHERE	U.DealerUpgradeCD = 21
        AND	U.EffectiveDateActive < @EndDate
        AND	U.Active = 1 

UPDATE	PG
SET	HASPINGII = 1
FROM	#PINGIIUsageResults PG
JOIN	IMT..DealerUpgrade U 
ON		PG.BusinessUnitID =U.BusinessUnitID
WHERE	U.DealerUpgradeCD = 19
        AND	U.EffectiveDateActive < @EndDate
        AND	U.Active = 1 
 
UPDATE	PG
SET		HasAcceleratorPackage = 
	CASE WHEN DP.PackageID =1 then 'PINGII Only'
	WHEN DP.PackageID =2 then 'PINGII w/Mystery Shopping'
	WHEN DP.PackageID =3 then '360 Pricing'
	WHEN DP.PackageID =4 then '360 Pricing w/Mystery Shopping'
	WHEN DP.PackageID =5 then 'Advertising Accelerator, No 1-Click'
	WHEN DP.PackageID =6 then 'Advertising Accelerator, Has 1-Click'
	ELSE	'No Accelerator' END
	FROM	#PINGIIUsageResults PG
LEFT
JOIN	IMT.dbo.DealerPreference_Pricing DP
ON	PG.BusinessUnitID =DP.BusinessUnitID


UPDATE	PG
SET		AcceleratorStore =1
FROM	#PINGIIUsageResults PG
JOIN	IMT.dbo.DealerPreference_Pricing DP
ON		PG.BusinessUnitID =DP.BusinessUnitID
WHERE	PackageID =5 or PackageID =6

 
UPDATE  PG
SET		HasEDTDescriptionProvider =1,
		EDTDescriptionProviderName =D.DatafeedCode
FROM	#PINGIIUsageResults PG
JOIN	IMT..InternetAdvertiserDealership IAD 
ON		PG.BusinessUnitID =IAD.BusinessUnitID
JOIN	dbo.InternetAdvertiser#HasDescription D
ON		IAD.InternetAdvertiserID = D.InternetAdvertiserID
	
UPDATE	PG	
SET	NumberOfInventoryItems=  CNT 
FROM
		(SELECT	Count(distinct(I.InventoryID)) as CNT,
				B.BusinessUnitID
		FROM	IMT..BusinessUnit B
		JOIN	FLDW..InventoryActive I 
		ON		I.BusinessUnitID = B.BusinessUnitID
		JOIN	IMT..DealerUpgrade U 
		ON		B.BusinessUnitID = U.BusinessUnitID 
		WHERE	I.InventoryType = 2 
		AND		I.InventoryActive = 1
		GROUP
		BY		B.BusinessUnitID) Inv
JOIN	#PINGIIUsageResults PG
ON		inv.BusinessUnitID =PG.BusinessUnitID 

UPDATE	PG	
SET	NumberOfRetailInventoryItems= 	 CNT 
FROM
		(SELECT	Count(distinct(I.InventoryID)) as CNT,
				B.BusinessUnitID
		FROM	IMT..BusinessUnit B
		JOIN	#InventoryStrategy I
		ON		I.BusinessUnitID = B.BusinessUnitID
		GROUP
		BY		B.BusinessUnitID) Inv
JOIN	#PINGIIUsageResults PG
ON		inv.BusinessUnitID =PG.BusinessUnitID 


UPDATE PG
SET	NumberOfItemsPINGed = CNT 
FROM
			(SELECT	Count(distinct(H.InventoryID)) as CNT,
					B.BusinessUnitID
			FROM	IMT..BusinessUnit B
			JOIN 	#InventoryAction H 
			ON		H.BusinessUnitID = B.BusinessUnitID
			JOIN	#InventoryStrategy I
			ON		H.BusinessUnitID =I.BusinessUnitID
			AND		H.InventoryID =I.InventoryID
			WHERE	H.HasReview=1
			GROUP
			BY	B.BusinessUnitID) Inv
JOIN	#PINGIIUsageResults PG
ON	inv.BusinessUnitID =PG.BusinessUnitID 



UPDATE PG
SET		NumberInventoryItemsShopd = TT.HasMysteryShop,
		NumberInventoryItemsPowered = TT.HasJDPower,
		NumberInventoryItemsTMVed = TT.HasTMV 
FROM	(SELECT SUM(CASE WHEN T.HasMysteryShop = 1 THEN 1 ELSE 0 END) as HasMysteryShop ,
				SUM(CASE WHEN T.HasJDPower = 1 THEN 1 ELSE 0 END) HasJDPower,
				SUM(CASE WHEN T.HasTMV = 1 THEN 1 ELSE 0 END)  as HasTMV,
				I.BusinessUnitID
		FROM	#TrackingInventory T
		JOIN	#InventoryStrategy I
		ON		T.BusinessUnitID =I.BusinessUnitID
		AND		T.InventoryID =I.InventoryID
		group
		by 		I.BusinessUnitID) TT
JOIN	#PINGIIUsageResults PG
ON		TT.BusinessUnitID = PG.BusinessUnitID


UPDATE PG
SET		PriceConfirmCount= 
	 	Icount
FROM	
		(SELECT count(Distinct(Ae.InventoryID)) as Icount,
				B.BusinessUnitID as BusinessUnitID
		FROM	#InventoryStrategy I  
		JOIN	IMT..BusinessUnit B
		ON		I.BusinessUnitID = B.BusinessUnitID
		LEFT 
		JOIN	IMT..AIP_Event AE 
		ON		I.InventoryID = AE.InventoryID
		JOIN	IMT..DealerUpgrade U 
		ON		B.BusinessUnitID = U.BusinessUnitID 
		WHERE	AE.BeginDate BETWEEN @StartDate AND @EndDate
		AND		AE.aip_eventtypeid = 5
		AND		cast(cast(ae.notes2 as float) as int) = ae.[value]
		AND		U.dealerupgradecd = 19
		AND		U.effectivedateactive = 1
		GROUP
		BY		B.businessunitid )A	
	JOIN	#PINGIIUsageResults PG
	ON	A.BusinessUnitID = PG.BusinessUnitID


UPDATE PG
SET	TotalDMSChangeCount= 
	 Icount
FROM	(
	SELECT	count(Distinct(InventoryID)) as Icount,
			BusinessUnitID as BusinessUnitID
	FROM
			(SELECT I.InventoryID,
					B.BusinessUnitID as BusinessUnitID
			FROM	#InventoryStrategy I
			JOIN	IMT..Inventory_ListPriceHistory  ILH
			ON		I.InventoryID =ILH.InventoryID
			JOIN	IMT..BusinessUnit B
			ON		I.BusinessUnitID = B.BusinessUnitID
			JOIN	IMT..DealerUpgrade U 
			ON		B.BusinessUnitID = U.BusinessUnitID	
			WHERE	ILH.DMSReferenceDT BETWEEN @StartDate AND @EndDate
			AND		U.dealerupgradecd = 19
			AND		U.effectivedateactive = 1
			GROUP
			BY		I.InventoryID,
					B.businessunitid
			HAVING Count(*)>1 )A
	GROUP 
	BY	BusinessUnitID	
		)TT	
	JOIN	#PINGIIUsageResults PG
	ON	TT.BusinessUnitID = PG.BusinessUnitID




	INSERT
	INTO	#Reprices
			(BusinessUnitID,
			InventoryID,
			RepriceSourceID)
	SELECT 	B.BusinessUnitID as BusinessUnitID,
			I.InventoryID,
			AE.RepriceSourceID
	FROM	#InventoryStrategy I
	JOIN	IMT..BusinessUnit B
	ON		I.BusinessUnitID = B.BusinessUnitID
	JOIN	IMT..AIP_Event AE 
	ON		I.InventoryID = AE.InventoryID
	JOIN	IMT..DealerUpgrade U 
	ON		B.BusinessUnitID = U.BusinessUnitID
	LEFT 
	JOIN	IMT..Member M
	ON		AE.LastModifiedBy=M.Login	
	WHERE	AE.BeginDate BETWEEN @StartDate AND @EndDate
	AND		AE.aip_eventtypeid = 5
	AND		U.dealerupgradecd = 19
	AND		U.effectivedateactive = 1
	AND		cast(cast(AE.notes2 as float) as int) <> AE.[value]
	AND		RepriceSourceID is not null
	GROUP
	BY		I.InventoryID,
			B.businessunitid,
			AE.RepriceSourceID


UPDATE 	PG
SET		TotalRepriceCount= 
			Icount
FROM		(SELECT count(Distinct(InventoryID)) as Icount,
					BusinessUnitID as BusinessUnitID
			FROM	#Reprices
			GROUP BY BusinessUnitID)TT
	JOIN	#PINGIIUsageResults PG
	ON	TT.BusinessUnitID = PG.BusinessUnitID

		
UPDATE 	PG
SET		NumberRepricedIMP= 
	 	Icount
FROM	
		(SELECT count(Distinct( InventoryID)) as Icount,
				BusinessUnitID as BusinessUnitID
		FROM	#Reprices I
		WHERE 	RepriceSourceID =1
		GROUP 
		BY		I.BusinessUnitID)TT	
	JOIN	#PINGIIUsageResults PG
	ON	TT.BusinessUnitID = PG.BusinessUnitID

UPDATE 	PG
SET		NumberRepricedeStock= 
	 	Icount
FROM	
		(SELECT count(Distinct(InventoryID)) as Icount,
				BusinessUnitID as BusinessUnitID
		FROM	#Reprices I
		WHERE 	RepriceSourceID =2	--eStock
		GROUP
		BY 		Businessunitid 	)TT	
	JOIN	#PINGIIUsageResults PG
	ON	TT.BusinessUnitID = PG.BusinessUnitID

UPDATE 	PG
SET		NumberRepricedPING= 
	 	Icount
FROM	
		(SELECT count(Distinct(InventoryID)) as Icount,
				BusinessUnitID as BusinessUnitID
		FROM	#Reprices I
		WHERE 	RepriceSourceID =3	--eStock
		GROUP
		BY 		Businessunitid 	)TT	
	JOIN	#PINGIIUsageResults PG
	ON	TT.BusinessUnitID = PG.BusinessUnitID

UPDATE 	PG
SET		NumberRepricedPING= 
	 	Icount
FROM	
		(SELECT count(Distinct(InventoryID)) as Icount,
				BusinessUnitID as BusinessUnitID
		FROM	#Reprices I
		WHERE 	RepriceSourceID =4	--eStock
		GROUP
		BY 		Businessunitid 	)TT	
	JOIN	#PINGIIUsageResults PG
	ON	TT.BusinessUnitID = PG.BusinessUnitID



UPDATE PG 
SET		NumberInternetDescriptions=TT.NumberInternetDescriptions,
		NumberInternetDescriptionsCreated =TT.NumberInternetDescriptionsCreated,
		NumberInternetDescriptionsEdited=TT.NumberInternetDescriptionsEdited
		FROM		(SELECT	NumberInternetDescriptions=COUNT(*),
					NumberInternetDescriptionsCreated =SUM(HasCreate),
					NumberInternetDescriptionsEdited=SUM(HasEdit),
					BusinessUnitID
				FROM	(SELECT InventoryID ,
								CASE WHEN Created between @StartDate and @EndDate then 1
								ELSE 0
								END HasCreate,
								CASE WHEN Modified between @StartDate and @EndDate then 1
								ELSE 0
								END HasEdit,
								BusinessUnitID as BusinessUnitID
						FROM	Market.Merchandising.VehicleAdvertisement VA
						JOIN	#InventoryStrategy I
						ON		I.InventoryID =VA.VehicleEntityID
						JOIN	Market.Merchandising.Advertisement_Properties AP
					ON	VA.AdvertisementID =AP.AdvertisementID
				JOIN	IMT..Member M
				ON		M.MemberID =AP.Author
				WHERE	AP.RowTypeID =1
				AND		M.MemberType =2)T
		GROUP
		BY	BusinessUnitID)TT	
	JOIN	#PINGIIUsageResults PG
	ON	TT.BusinessUnitID = PG.BusinessUnitID

UPDATE PG	
	SET	NumberClosingNotes=Icount
	FROM		(SELECT Count(Distinct(InventoryID)) as Icount,
						I.BusinessUnitID as BusinessUnitID
				FROM	Market.Pricing.VehiclePricingDecisionInput VPDI
				JOIN	#InventoryStrategy I
				ON		I.InventoryID =VPDI.VehicleEntityID
				AND		VPDI.Notes IS NOT NULL
				GROUP
				BY		I.BusinessUnitID)TT	
	JOIN	#PINGIIUsageResults PG
	ON	TT.BusinessUnitID = PG.BusinessUnitID



UPDATE PG	
	SET	NumberAppraisals =TT.NumberAppraisals,
		NumberAppraisalValues=TT.NumberAppraisalValues,
		NumberAppraisalForms =TT.NumberAppraisalForms,
		NumberAppraisalsPINGED=TT.NumberAppraisalsPINGED 
	FROM
		(SELECT	NumberAppraisals =COUNT(*),
				NumberAppraisalValues	 =SUM(HasAppraisalValue),
				NumberAppraisalForms =SUM(HasAppraisalForm),
				NumberAppraisalsPINGED =SUM(AppraisalPINGED),
				BusinessUnitID
		FROM
				(SELECT		A.AppraisalID,
							CASE WHEN AV.AppraisalID IS NOT NULL then 1
							ELSE 0
							END HasAppraisalValue,
							CASE WHEN S.SearchID IS NOT NULL then 1
							ELSE 0
							END AppraisalPINGed,
							CASE WHEN AF.AppraisalFormID IS NOT NULL then 1
							ELSE 0
							END HasAppraisalForm,
							A.BusinessUnitID AS BusinessUnitID
				FROM		IMT..Appraisals A
				JOIN		IMT..Member M 
				ON		M.MemberID = A.MemberID
				LEFT
				JOIN		IMT..AppraisalValues AV
				ON		A.AppraisalID =AV.AppraisalID
				LEFT
				JOIN		Market.Pricing.Search S
				ON		S.VehicleHandle= '2'+CAST(A.AppraisalID as Varchar(20))
				LEFT
				JOIN		IMT..AppraisalFormOptions AF
				ON		A.AppraisalID =AF.AppraisalID
				AND		AF.AppraisalFormOffer>0
				WHERE		A.AppraisalStatusID = 1
				AND		A.DateCreated BETWEEN @StartDate AND @EndDate
				AND		A.AppraisalTypeID = 1 
				AND		M.MemberType = 2
							)T
		GROUP 
		BY	BusinessUnitID)TT	
	JOIN	#PINGIIUsageResults PG
	ON	TT.BusinessUnitID = PG.BusinessUnitID


UPDATE	PG
	SET         NumberMADSubscriptions = ICOUNT
	FROM	
	(SELECT COUNT(*) AS Icount,
			BusinessUnitID
			FROM	HAL.dbo.AppraisalReviewBookletPreference
			WHERE	EmailNotification = 1
			AND	EmailAddress IS NOT NULL
			GROUP 
			BY	BusinessUnitID)TT
	JOIN	#PINGIIUsageResults PG
	ON	TT.BusinessUnitID = PG.BusinessUnitID

UPDATE	PG
	SET         NumberLogins = LoginCount
	FROM	
	(SELECT ma.BusinessUnitID AS BusinessUnitID, COUNT(ae.ApplicationEventID) AS LoginCount
         FROM IMT.dbo.MemberAccess ma
         JOIN IMT.dbo.Member m
         ON ma.MemberID = m.MemberID
         JOIN IMT.dbo.ApplicationEvent ae
         ON m.MemberID = ae.MemberID
         WHERE ae.EventType = 1 AND
         ae.CreateTimeStamp > @StartDate AND
         ae.CreateTimestamp < @EndDate AND
         m.MemberType > 1
         GROUP BY ma.BusinessUnitID) Logins
	JOIN	#PINGIIUsageResults PG
	ON	Logins.BusinessUnitID = PG.BusinessUnitID

INSERT
INTO	dbo.SystemUsageMetricsForSalesForce1Day(
	SystemUsageMetricRunID,
	BusinessUnitID,
	BusinessUnit,
	NumberOfInventoryItems,
	NumberOfRetailInventoryItems ,
	
	HasPINGII,
	HasAcceleratorPackage,
	NumberOfItemsPINGED,
	PriceConfirmCount,
	TotalDMSChangeCount,
	
	TotalRepriceCount,
	NumberRepricedIMP,
	NumberRepricedeStock,
	NumberRepricedPING,
	NumberInventoryItemsShopped,
	
	NumberInventoryItemsPowered,
	NumberInventoryItemsTMVed,
	WBTurnedOn,
	MADTurnedOn,
	AcceleratorStore,
	
	HasEDTDescriptionProvider,
	EDTDescripton,
	NumberInternetDescriptions,
	NumberInternetDescriptionsCreated,
	NumberInternetDescriptionsEdited,
	
	NumberClosingNotes,
	NumberAppraisals,
	NumberAppraisalValues,
	NumberAppraisalForms,
	NumberAppraisalsPINGed,
	
	NumberMADSubscriptions,
	NumberLogins
)
	
SELECT 		@RunID,
 		BusinessUnitID,
		BusinessUnit,
		COALESCE(NumberOfInventoryItems,0),
		COALESCE(NumberOfRetailInventoryItems,0),
		
		COALESCE(HasPINGII,0)  ,
		HasAcceleratorPackage,
		COALESCE(NumberOfItemsPINGED,0)	,
		COALESCE(PriceConfirmCount,0),
		COALESCE(TotalDMSChangeCount,0),
		
		COALESCE(TotalRepriceCount,0),
		COALESCE(NumberRepricedIMP,0)	,
		COALESCE(NumberRepricedeStock,0),
		COALESCE(NumberRepricedPING,0),
		COALESCE(NumberInventoryItemsShopd,0),
		
		COALESCE(NumberInventoryItemsPowered,0)	,
		COALESCE(NumberInventoryItemsTMVed,0),
		COALESCE(WBTurnedOn,0),
		COALESCE(MADTurnedOn,0),
		COALESCE(AcceleratorStore,0),
		
		COALESCE(HasEDTDescriptionProvider,0),
		EDTDescriptionProviderName,
		COALESCE(NumberInternetDescriptions,0),
		COALESCE(NumberInternetDescriptionsCreated,0),
		COALESCE(NumberInternetDescriptionsEdited,0),
		
		COALESCE(NumberClosingNotes,0),
		COALESCE(NumberAppraisals,0),
		COALESCE(NumberAppraisalValues,0),
		COALESCE(NumberAppraisalForms,0),
		COALESCE(NumberAppraisalsPINGed ,0),
	 	
		COALESCE(NumberMADSubscriptions,0),
		COALESCE(NumberLogins,0)
FROM		#PINGIIUsageResults


DROP TABLE #Resources
DROP TABLE #TrackingInventory
DROP TABLE #InventoryStrategy
DROP TABLE #InventoryAction
DROP TABLE #Reprices
DROP TABLE #PINGIIUsageResults
DROP TABLE #Tracking

Update	dbo.SystemUsageMetricsRuns1Day
SET		IsSuccessful =1 
Where	SystemUsageMetricsRunID =@RunID

END TRY
BEGIN CATCH
	Update dbo.SystemUsageMetricsRuns1Day
	SET	IsSuccessful =0,
		ErrorID =ERROR_NUMBER(),
		ErrorMessage =error_message()
	Where SystemUsageMetricsRunID =@RunID

END CATCH


