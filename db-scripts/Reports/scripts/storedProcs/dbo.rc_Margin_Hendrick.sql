
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_Margin_Hendrick]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_Margin_Hendrick] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_Margin_Hendrick] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_Margin_Hendrick]
        @DealerID VARCHAR(6) ,
        @SegmentID INT = -1 ,
        @AgeBucketID INT = 0
AS 
/******************************************************************************************************
*	Routine:		dbo.rc_Margin_Hendrick
*	Purpose:		Source procedure for Margin Report
*
*	History:		SVU	12/24/2009	Initial Creation
*				SVU	01/14/2010	Fixed Performance Problem & Multiple Rows per vehicle
*				SVU	01/18/2010	Added Segment Parameter
*				SVU	01/19/2010	Used Auction Tables (A1 & A2) with VIC instead of MMG & Model Year
*				SVU	01/22/2010	Added Aging Bucket Parameter
*				SVU	02/03/2010	Changed Aging Bucket Parameters
*				SVU	03/08/2010	Fixed Market Search
*				SVU	04/16/2010	Forked this proc from dbo.rc_Margin
*******************************************************************************************************/
DECLARE @AgeBuckets TABLE (Low INT ,
				High INT ,
				BucketNumber TINYINT)

INSERT  INTO @AgeBuckets (Low, High, BucketNumber) VALUES  (-2147483647, 2147483647, 0) -- All
INSERT  INTO @AgeBuckets (Low, High, BucketNumber) VALUES  (-2147483647, 21, 1)
INSERT  INTO @AgeBuckets (Low, High, BucketNumber) VALUES  (22, 29, 2)
INSERT  INTO @AgeBuckets (Low, High, BucketNumber) VALUES  (30, 39, 3)
INSERT  INTO @AgeBuckets (Low, High, BucketNumber) VALUES  (40, 49, 4)
INSERT  INTO @AgeBuckets (Low, High, BucketNumber) VALUES  (50, 59, 5)
INSERT  INTO @AgeBuckets (Low, High, BucketNumber) VALUES  (60, 2147483647, 6)

SELECT  BU.BusinessUnit ,
	I.AgeInDays Age ,
	V.VehicleYear Year ,
	MMG.Make ,
	MMG.Model ,
	V.VehicleTrim ,
	I.MileageReceived Mileage ,
	CAST(I.UnitCost AS INT) UnitCost ,
	CAST(I.ListPrice AS INT) InternetPrice ,
	I.UsedSellingPrice ,
	CAST(B.BookValue - I.UnitCost AS INT) BookVsCost ,
	CAST(I.ListPrice - I.UnitCost AS INT) InternetVsCost ,
	CAST(CASE WHEN S.DefaultSearchTypeID=4 THEN PrecisionSR.AvgListPrice ELSE ConsumerSR.AvgListPrice END - I.UnitCost AS INT) MarketAvgVsCost,
--	CAST(COALESCE(PrecisionSR.AvgListPrice, ConsumerSR.AvgListPrice, 0) - I.UnitCost AS INT) MarketAvgVsCost_Orig ,
	CAST(COALESCE(AucSim.AveSalePrice, AucAll.AveSalePrice, AucSim2.AveSalePrice, AucAll2.AveSalePrice, 0) - I.UnitCost AS INT) AuctionVsCost ,
	I.StockNumber ,
	V.Vin ,
	CASE I.Certified
		WHEN 1 THEN 'Y'
		ELSE 'N'
	END Certified ,
	AucAll.AveSalePrice AuctionAllAvgPrice ,
	AucSim.AveSalePrice AuctionSimilarAvgPrice ,
	COALESCE(PrecisionSR.AvgListPrice, ConsumerSR.AvgListPrice, 0) MktAvgListPrice ,
	TP.NAME BookName ,
	S2.SEGMENT ,
	AB.LOW ,
	AB.High,
	I.TransferPrice
FROM    FLDW.dbo.InventoryActive I WITH (NOLOCK)
JOIN    IMT.dbo.BusinessUnit BU WITH (NOLOCK) ON I.BusinessUnitID = BU.BusinessUnitID
JOIN    IMT.dbo.DealerPreference DP WITH (NOLOCK) ON dp.BusinessUnitID = I.BusinessUnitID
JOIN    IMT.dbo.DealerPreference_Pricing AS DPP WITH (NOLOCK) ON DPP.BusinessUnitID = BU.BusinessUnitID
JOIN    Market.Pricing.DistanceBucket AS DB WITH (NOLOCK) ON DB.Distance = DPP.PingII_DefaultSearchRadius
JOIN    FLDW.dbo.InventoryBookout_F B WITH (NOLOCK) ON I.InventoryID = B.InventoryID
                                                       AND B.ThirdPartyCategoryID = DP.BookOutPreferenceId
JOIN    IMT.dbo.Vehicle V WITH (NOLOCK) ON I.VehicleID = V.VehicleID
JOIN    IMT.dbo.MakeModelGrouping MMG WITH (NOLOCK) ON MMG.MakeModelGroupingID = V.MakeModelGroupingID
	 -- Auction By VIC
LEFT JOIN [AuctionNet].dbo.AuctionTransaction_A1 AS AucALL WITH (NOLOCK) ON AucAll.VIC = V.VIC
									AND AucAll.AreaID = DP.AuctionAreaId
									AND AucAll.PeriodID = DP.AuctionTimePeriodID
	-- Auction By VIC & Mileage Range
LEFT JOIN [AuctionNet].dbo.AuctionTransaction_A2 AS AucSim WITH (NOLOCK) ON AucSim.VIC = V.VIC
									AND AucSim.LowMileageRange = CAST(I.MileageReceived / 10000 AS INT) * 10000
									AND AucSim.HighMileageRange = CAST(I.MileageReceived / 10000 AS INT) * 10000 + 9999
									AND AucSim.AreaID = DP.AuctionAreaId
									AND AucSim.PeriodID = DP.AuctionTimePeriodID
	-- Auction By MMG & Year
LEFT JOIN [AuctionNet].dbo.AuctionTransaction_A3 AS AucALL2 WITH (NOLOCK) ON AucAll2.MakeModelGroupingID = MMG.MakeModelGroupingID
									AND AucAll2.ModelYear = v.VehicleYear
									AND AucAll2.AreaID = DP.AuctionAreaId
									AND AucAll2.PeriodID = DP.AuctionTimePeriodID
	-- Auction By MMG & Year & Mileage Range
LEFT JOIN [AuctionNet].dbo.AuctionTransaction_A4 AS AucSim2 WITH (NOLOCK) ON AucSim2.MakeModelGroupingID = MMG.MakeModelGroupingID
									AND AucSim2.ModelYear = V.VehicleYear
									AND AucSim2.LowMileageRange = CAST(I.MileageReceived / 10000 AS INT) * 10000
									AND AucSim2.HighMileageRange = CAST(I.MileageReceived / 10000 AS INT) * 10000
										+ 9999
									AND AucSim2.AreaID = DP.AuctionAreaId
									AND AucSim2.PeriodID = DP.AuctionTimePeriodID
LEFT JOIN Market.Pricing.Search AS S WITH (NOLOCK) ON I.InventoryID = S.VehicleEntityID
							AND S.VehicleEntityTypeID = 1
LEFT JOIN Market.Pricing.SearchResult_F AS PrecisionSR WITH (NOLOCK) ON S.SearchID = PrecisionSR.SearchID
								AND S.OwnerID=PrecisionSR.OwnerID
								AND PrecisionSR.SearchTypeID = 4		-- Precision Market Search Results
LEFT JOIN Market.Pricing.SearchResult_F AS ConsumerSR WITH (NOLOCK) ON S.SearchID = ConsumerSR.SearchID
								AND S.OwnerID=ConsumerSR.OwnerID
								AND ConsumerSR.SearchTypeID = 1 -- Consumer Search Results
LEFT JOIN IMT.dbo.ThirdParties AS TP WITH (NOLOCK) ON TP.ThirdPartyID = DP.GuideBookID
LEFT JOIN IMT.dbo.Segment AS S2 WITH (NOLOCK) ON S2.SegmentID = V.SegmentID --@SegmentID
JOIN    @AgeBuckets AB ON COALESCE(I.AgeInDays, 0) BETWEEN AB.LOW AND AB.HIGH
			AND AB.BucketNumber = @AgeBucketID
WHERE   I.BusinessUnitID = @DealerID
	AND I.InventoryActive = 1
	AND I.InventoryType = 2
	AND (@SegmentID = -1
		OR V.SegmentID = @SegmentID)
ORDER BY I.AgeInDays

GO