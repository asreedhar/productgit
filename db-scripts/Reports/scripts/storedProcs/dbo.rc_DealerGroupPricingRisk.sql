IF OBJECT_ID(N'[dbo].[rc_DealerGroupPricingRisk]', 'P') IS NOT NULL
	DROP PROCEDURE [dbo].[rc_DealerGroupPricingRisk]
GO
CREATE PROCEDURE [dbo].[rc_DealerGroupPricingRisk]
	@DealerGroupID INT,
	@Mode INT,
	@RiskIndex TINYINT = NULL,
	@SaleStrategy CHAR = 'A',
	@Debug		BIT = 0
AS

BEGIN

DECLARE @T0 DATETIME,@MaxDateConstant SMALLDATETIME, 
	@CurrentDate	SMALLDATETIME

SET	@MaxDateConstant =cast('06/06/2079' as SMALLDATETIME)-- MAK setting constants in procedure
SET	@CurrentDate =cast(GETDATE() as SMALLDATETIME)

SET NOCOUNT ON;
set transaction isolation level read uncommitted


	
	SET	@MaxDateConstant =cast('06/06/2079' as SMALLDATETIME)-- MAK setting constants in procedure
SET	@CurrentDate =cast(GETDATE() as SMALLDATETIME)

CREATE TABLE #Preferences (
	Dealer_Group                       VARCHAR(255) NULL,
	Dealer_Group_ID                    INT          NOT NULL,
	Dealer                             VARCHAR(255) NOT NULL,
	Dealer_ID                          INT          NOT NULL,
	ThirdPartyCategoryID               INT          NOT NULL,
	HasUpgrade                         BIT          NULL,
	ExcludeNoPriceFromCalc             BIT          NOT NULL,
	ExcludeLowPriceOutliersMultiplier  DECIMAL(4,2) NOT NULL,
	ExcludeHighPriceOutliersMultiplier DECIMAL(4,2) NOT NULL,
	PRIMARY KEY (Dealer_ID)
)

SELECT @T0 = GETDATE()

INSERT INTO #Preferences (
	Dealer_Group,
	Dealer_Group_ID,
	Dealer,
	Dealer_ID,
	ThirdPartyCategoryID,
	HasUpgrade,
	ExcludeNoPriceFromCalc,
	ExcludeLowPriceOutliersMultiplier,
	ExcludeHighPriceOutliersMultiplier
)
SELECT	Dealer_Group                       = G.BusinessUnit,
	Dealer_Group_ID                    = G.BusinessUnitID,
	Dealer                             = D.BusinessUnit,
	Dealer_ID                          = D.BusinessUnitID,
	ThirdPartyCategoryID               = P.BookOutPreferenceId,
	HasUpgrade                         = CASE WHEN U.BusinessUnitID IS NULL THEN 0 ELSE 1 END,
	ExcludeNoPriceFromCalc             = COALESCE(DPP.PingII_ExcludeNoPriceFromCalc, DPS.PingII_ExcludeNoPriceFromCalc),
	ExcludeLowPriceOutliersMultiplier  = COALESCE(DPP.PingII_ExcludeLowPriceOutliersMultiplier, DPS.PingII_ExcludeLowPriceOutliersMultiplier),
	ExcludeHighPriceOutliersMultiplier = COALESCE(DPP.PingII_ExcludeHighPriceOutliersMultiplier, DPS.PingII_ExcludeHighPriceOutliersMultiplier)

FROM	IMT.dbo.BusinessUnit D
JOIN	IMT.dbo.BusinessUnitRelationship R ON R.BusinessUnitID = D.BusinessUnitID
JOIN	IMT.dbo.BusinessUnit G ON R.ParentID = G.BusinessUnitID
JOIN	IMT.dbo.DealerPreference P ON P.BusinessUnitID = D.BusinessUnitID
LEFT JOIN IMT.dbo.DealerUpgrade U ON U.BusinessUnitID = D.BusinessUnitID AND U.DealerUpgradeCD = 19 AND U.EffectiveDateActive = 1
LEFT JOIN IMT.dbo.DealerPreference_Pricing DPP ON D.BusinessUnitID = DPP.BusinessUnitID
LEFT JOIN IMT.dbo.DealerPreference_Pricing DPS ON DPS.BusinessUnitID = 100150 --First Look business unit

WHERE	G.BusinessUnitID = @DealerGroupID
AND	D.Active = 1
AND	D.BusinessUnitTypeID = 4
AND	P.GoLiveDate IS NOT NULL

IF @Debug = 1 PRINT '0. Preferences = ' + CONVERT(VARCHAR,DATEDIFF(MS,@T0,GETDATE()))

SELECT  @T0 = GETDATE()

CREATE TABLE #BookVersusCost (
        DealerID INT NOT NULL,
        InventoryID INT NOT NULL,
        UnitCost INT NULL,
        BookValue INT NULL,
        PRIMARY KEY (DealerID,InventoryID)
)

IF @Mode = 3 BEGIN


INSERT INTO #BookVersusCost (
        DealerID,
        InventoryID,
        UnitCost,
        BookValue
)
SELECT  I.BusinessUnitID,
        I.InventoryID,
        COALESCE(B.UnitCost,0),
        B.BookValue
FROM    #Preferences P
JOIN    FLDW.dbo.InventoryActive I ON I.BusinessUnitID = P.Dealer_ID
JOIN    FLDW.dbo.InventoryBookout_F B ON B.InventoryID = I.InventoryID AND B.BusinessUnitID = I.BusinessUnitID AND P.ThirdPartyCategoryID = B.ThirdPartyCategoryID AND B.BookoutValueTypeID = 2
WHERE   I.InventoryType = 2
AND     I.InventoryActive = 1

IF @Debug = 1 PRINT '1. Book Versus Cost = ' + CONVERT(VARCHAR,DATEDIFF(MS,@T0,GETDATE()))

SELECT  @T0 = GETDATE()

END

CREATE TABLE #MarketPricingBucket (
	DealerID              int     not null,
	MarketPricingBucketID tinyint not null,
	LowAge                tinyint null,
	HighAge               int null,
	MinMarketPct          tinyint null,
	MaxMarketPct          tinyint null,
	MinGross              int     null,
	PRIMARY KEY (DealerID,MarketPricingBucketID)
)

INSERT
INTO	#MarketPricingBucket
SELECT	DealerID		= P.Dealer_ID,
	MarketPricingBucketID	= IBR.RangeID,
	LowAge			= COALESCE(IBR.Low,0),
	HighAge			= COALESCE(IBR.High,9999),
	MinMarketPct		= IBR.Value1,
	MaxMarketPct		= IBR.Value2,
	MinGross		= IBR.Value3 

FROM	#Preferences P CROSS APPLY [IMT]..GetInventoryBucketRanges(P.Dealer_ID,7) IBR

IF @Debug = 1 PRINT '2. Market Pricing Bucket = ' + CONVERT(VARCHAR,DATEDIFF(MS,@T0,GETDATE()))

SELECT  @T0 = GETDATE()

/* this is just temporary until we release the index change on market.pricing.search */

CREATE TABLE #Search (
        OwnerID INT,
        DealerID INT,
        InventoryID INT,
        SearchID INT,
        SearchTypeID INT,
        PRIMARY KEY (OwnerID, SearchID, SearchTypeID, DealerID, InventoryID))

INSERT INTO #Search (OwnerID, DealerID, SearchID, SearchTypeID, InventoryID)

SELECT	O.OwnerID, P.Dealer_ID, S.SearchID, S.DefaultSearchTypeID, S.VehicleEntityID
FROM	#Preferences P
JOIN	Market.Pricing.Owner O ON O.OwnerEntityID = P.Dealer_ID
JOIN	Market.Pricing.Search S ON S.OwnerID = O.OwnerID
WHERE	P.HasUpgrade = 1
AND	O.OwnerTypeID = 1
AND	S.VehicleEntityTypeID = 1

IF @Debug = 1 PRINT '3. Search = ' + CONVERT(VARCHAR,DATEDIFF(MS,@T0,GETDATE()))

SELECT  @T0 = GETDATE()

CREATE TABLE #Stock (
	DealerID INT NOT NULL,
	InventoryID INT NOT NULL,
	RiskIndex TINYINT NOT NULL,
	AIP_EventCategoryID INT NULL,
	PRIMARY KEY(DealerID,InventoryID)--,AIP_EventCategoryID)
)

INSERT INTO #Stock (DealerID, InventoryID, RiskIndex)--,AIP_EventCategoryID)

SELECT	DISTINCT Dealer_ID   = P.Dealer_ID,
	InventoryID = I.InventoryID,
	RiskIndex   = MAX(CASE WHEN DI.CategorizationOverrideExpiryDate > GETDATE() THEN 2 ELSE
		CASE WHEN COALESCE(I.ListPrice,0.0) = 0.0 THEN 4
			WHEN SRF.SearchID IS NULL OR SRF.Units < 5 OR I.ListPrice > SRF.AvgListPrice * P.ExcludeHighPriceOutliersMultiplier OR I.ListPrice < SRF.AvgListPrice * P.ExcludeLowPriceOutliersMultiplier THEN 5
			WHEN ROUND(I.ListPrice / SRF.AvgListPrice * 100,0) < MPB.MinMarketPct THEN 1
			WHEN ROUND(I.ListPrice / SRF.AvgListPrice * 100,0) > MPB.MaxMarketPct THEN 3
			ELSE 2
		END
	END)
	--,AET.AIP_EventCategoryID 
	
FROM	#Preferences P

--JOIN [IMT].dbo.AIP_EventType AET ON AE.AIP_EventTypeID = AET.AIP_EventTypeID
JOIN	FLDW.dbo.InventoryActive I ON I.BusinessUnitID = P.Dealer_ID AND I.InventoryType = 2 AND I.InventoryActive = 1
JOIN	#MarketPricingBucket MPB ON MPB.DealerID = P.Dealer_ID AND (CASE WHEN DATEDIFF(DD, InventoryReceivedDate, GETDATE()) > 0 THEN DATEDIFF(DD, InventoryReceivedDate, GETDATE()) ELSE 1 END) BETWEEN MPB.LowAge AND MPB.HighAge
LEFT JOIN #Search S ON S.DealerID = P.Dealer_ID AND I.InventoryID = S.InventoryID

LEFT JOIN Market.Pricing.SearchResult_F SRF ON S.OwnerID = SRF.OwnerID AND S.SearchID = SRF.SearchID AND S.SearchTypeID = SRF.SearchTypeID
LEFT JOIN Market.Pricing.VehiclePricingDecisionInput DI ON DI.OwnerID = S.OwnerID AND DI.VehicleEntityTypeID = 1 AND DI.VehicleEntityID = S.InventoryID
--JOIN[IMT].dbo.AIP_Event AE ON AE.InventoryID = I.INVENTORYID
--JOIN [IMT].dbo.AIP_EventType AET ON AE.AIP_EventTypeID = AET.AIP_EventTypeID

WHERE	P.HasUpgrade = 1
GROUP BY  P.Dealer_ID, i.[InventoryID]

IF @Debug = 1 PRINT '4. Stock = ' + CONVERT(VARCHAR,DATEDIFF(MS,@T0,GETDATE()))

SELECT  @T0 = GETDATE()



IF @SaleStrategy = 'R'	BEGIN 

--Added for 29801
CREATE TABLE #AIP_Event (
		InventoryID	INT NOT NULL,
		AIP_EventID	INT NOT NULL,
		AIP_EventTypeID TINYINT NOT NULL,
		AIP_EventCategoryID TINYINT NOT NULL,
		StartOn		SMALLDATETIME NOT NULL,
		Status	TINYINT NOT NULL,
		PRIMARY KEY (InventoryID,AIP_EventID,AIP_EventTypeID,Status)
		);
		
		INSERT INTO #AIP_Event SELECT AE.InventoryID,AE.AIP_EventID,AE.AIP_EventTypeID,AET.AIP_EventCategoryID,
			CASE
				WHEN AET.CurrentAtCreateDate = 1 THEN
					CAST(CONVERT(VARCHAR(10),AE.Created,101) AS DATETIME)
				ELSE
					AE.BeginDate
			END,
			CASE WHEN (CASE
				WHEN AET.CurrentAtCreateDate = 1 THEN
					CAST(CONVERT(VARCHAR(10),AE.Created,101) AS DATETIME)
				ELSE
					AE.BeginDate
			END <= @CurrentDate AND ISNULL(AE.EndDate, @MaxDateConstant)>= @CurrentDate ) THEN 1 ELSE 2 END
		
		FROM	[IMT].dbo.AIP_Event AE 
					JOIN [IMT].dbo.AIP_EventType AET ON AE.AIP_EventTypeID = AET.AIP_EventTypeID
					JOIN #Stock S on AE.InventoryID = S.InventoryID
				WHERE AET.AIP_EventCategoryID in (1,2,3,4) 
				--GETDATE() BETWEEN AE.BeginDate AND COALESCE(AE.EndDate, '6/6/2079')
 

	 -- remove previous plans where there is a current plan
	DELETE 	E2
	FROM 	#AIP_Event E2
	JOIN	#AIP_Event E1 
	ON	E2.InventoryID = E1.InventoryID
	WHERE	E2.Status = 2
	AND	E1.Status = 1


	-- remove all but the last strategy for inventory with expired plans only
	DELETE AE
	FROM	 #AIP_Event AE
	LEFT JOIN(
		SELECT	X1.AIP_EventID
		FROM	#AIP_Event X1
		JOIN	(
			SELECT	InventoryID,
				MAX(StartOn)StartOn
			FROM	#AIP_Event
			WHERE	Status = 2
			GROUP
			BY	InventoryID
			) X2 
			ON  X1.InventoryID = X2.InventoryID 
			AND X1.StartOn = X2.StartOn
		) XX
		ON AE.AIP_EventID =XX.AIP_EventID
	WHERE	Status = 2 
		AND XX.AIP_EventID is null 
				
	
	UPDATE A
	SET AIP_EventCategoryID =COALESCE(AE.MaxEventCat, 1)-- MK  Unplanned plans count as retail
	FROM	#Stock A
	LEFT JOIN (	SELECT	InventoryID,
				MAX(Status)as MaxStatus,
				MAX(AIP_EventCategoryID)  As MaxEventCat
			FROM	#AIP_Event 
			GROUP 
			BY	InventoryID
		) AE
		ON A.InventoryID =AE.InventoryID	
		
		--select distinct InventoryID from #Stock where dealerid=102008 and riskindex=2
			DELETE	S
		FROM	#Stock S
		WHERE S.AIP_EventCategoryID <> 1        -- Retail
			
		drop table #AIP_Event		
--END Added for 29801 ------
		


                 
			
			
			
	--JOIN [IMT].dbo.AIP_Event AE ON AE.AIP_EventTypeID = S.AIP_EventCategoryID
	--JOIN [IMT].dbo.AIP_EventType AET ON AE.AIP_EventTypeID = AET.AIP_EventTypeID
	
--WHERE S.AIP_EventCategoryID IN (5,6,7,8) AND GETDATE() BETWEEN AE.BeginDate AND COALESCE(AE.EndDate, '6/6/2079' )


        IF @Debug = 1 PRINT '5. Retail Only = ' + CONVERT(VARCHAR,DATEDIFF(MS,@T0,GETDATE()))

        SELECT  @T0 = GETDATE()
        

END

DECLARE @RiskValues TABLE (
	RiskIndex TINYINT      NOT NULL,
	RiskName  VARCHAR(255) NOT NULL
)

INSERT INTO @RiskValues (RiskIndex, RiskName) VALUES (1, 'Underpricing Risk')
INSERT INTO @RiskValues (RiskIndex, RiskName) VALUES (2, 'Priced at Market')
INSERT INTO @RiskValues (RiskIndex, RiskName) VALUES (3, 'Overpricing Risk')
INSERT INTO @RiskValues (RiskIndex, RiskName) VALUES (4, 'Zero/No Price')
INSERT INTO @RiskValues (RiskIndex, RiskName) VALUES (5, 'Not Analyzed')

DECLARE @Units INT

--SELECT @Units = COUNT(*) FROM #Stock

IF @Mode = 1 /* home page */

SELECT	P.Dealer_Group,
	P.Dealer_Group_ID,
	P.Dealer,
	P.Dealer_ID,
	P.HasUpgrade,
	Units                  = COUNT(*),
	Units_UnderpricingRisk = SUM(CASE WHEN S.RiskIndex = 1 THEN 1 ELSE 0 END),
	Units_PricedToMarket   = SUM(CASE WHEN S.RiskIndex = 2 THEN 1 ELSE 0 END),
	Units_OverpricingRisk  = SUM(CASE WHEN S.RiskIndex = 3 THEN 1 ELSE 0 END),
	Units_NoPrice          = SUM(CASE WHEN S.RiskIndex = 4 THEN 1 ELSE 0 END),
	Units_NotAnalyzed      = SUM(CASE WHEN S.RiskIndex = 5 THEN 1 ELSE 0 END)
	
FROM	#Preferences P LEFT JOIN #Stock S ON P.Dealer_ID = S.DealerID

WHERE	P.HasUpgrade = 1

GROUP
BY	P.Dealer_Group,
	P.Dealer_Group_ID,
	P.Dealer,
	P.Dealer_ID,
	P.HasUpgrade

IF @Mode = 2 /* graph */

SELECT	P.Dealer_Group,
	P.Dealer_Group_ID,
	P.Dealer,
	P.Dealer_ID,
	R.RiskIndex,
	R.RiskName,
	Units = COALESCE(S.Units,0),
	Total = COALESCE(T.Units,0)

FROM	#Preferences P CROSS JOIN @RiskValues R
	LEFT JOIN (
		SELECT	S.DealerID,
			S.RiskIndex,
			COUNT(*) Units
		FROM	#Stock S
		GROUP
		BY	S.DealerID,
			S.RiskIndex
	)
	 S ON S.DealerID = P.Dealer_ID AND R.RiskIndex = S.RiskIndex
	LEFT JOIN (
		SELECT	S.DealerID,
			COUNT(*) Units
		FROM	#Stock S
		GROUP
		BY	S.DealerID
	) T ON T.DealerID = P.Dealer_ID

WHERE	P.HasUpgrade = 1

IF @Mode = 3 /* table */

SELECT	P.Dealer_Group,
	P.Dealer_Group_ID,
	P.Dealer,
	P.Dealer_ID,
	P.HasUpgrade,
	Rank            = RANK() OVER (ORDER BY
				ROUND(
					CAST(SUM(CASE WHEN S.RiskIndex = @RiskIndex THEN 1 ELSE 0 END) AS REAL)
					/ CAST(COALESCE(NULLIF(COUNT(*),0),1) AS REAL)
					* 100.0
				,0)
			DESC),
	UnitCost        = SUM(CASE WHEN S.RiskIndex = @RiskIndex THEN B.UnitCost ELSE 0 END),
	BookVersusCost  = SUM(CASE WHEN S.RiskIndex = @RiskIndex THEN B.BookValue-B.UnitCost ELSE 0 END),
	Units_Total     = COUNT(*),
	Units_Risk      = SUM(CASE WHEN S.RiskIndex = @RiskIndex THEN 1 ELSE 0 END),
	Percentage      = ROUND(
				CAST(SUM(CASE WHEN S.RiskIndex = @RiskIndex THEN 1 ELSE 0 END) AS REAL)
				/ CAST(COALESCE(NULLIF(COUNT(*),0),1) AS REAL)
				* 100.0
			,0)

FROM	#Preferences P
LEFT
JOIN    #Stock S ON P.Dealer_ID = S.DealerID
LEFT
JOIN    #BookVersusCost B ON S.DealerID = B.DealerID AND S.InventoryID = B.InventoryID
WHERE	P.HasUpgrade = 1

GROUP
BY	P.Dealer_Group,
	P.Dealer_Group_ID,
	P.Dealer,
	P.Dealer_ID,
	P.HasUpgrade

IF @Debug = 1 PRINT '6. Results = ' + CONVERT(VARCHAR,DATEDIFF(MS,@T0,GETDATE()))


DROP TABLE #Preferences

DROP TABLE #BookVersusCost

DROP TABLE #MarketPricingBucket 

DROP TABLE #Search

DROP TABLE #Stock

END



GO


GRANT EXEC ON [dbo].[rc_DealerGroupPricingRisk] to firstlook
GO
