 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_ClosedAppraisalsReport_Closed]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_ClosedAppraisalsReport_Closed] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_ClosedAppraisalsReport_Closed] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_ClosedAppraisalsReport_Closed]
	@DealerID INT
AS

SET NOCOUNT ON;

SET ANSI_WARNINGS OFF;

SELECT	AppraisalID, VehicleID, AppraisalDate,
		InventoryReceivedDate, InventoryVehicleLight,
		ModelYear, VehicleMake, VehicleLine, VehicleSeries, VIN,
		AppraisalValue, CustomerOffer, BookName, BookCategoryName, BookValue, AppraiserName
		,M.FirstName + ' ' + M.LastName LoggedInAppraiser

FROM	[HAL].dbo.TradeClosingRateReport tcr
LEFT JOIN imt.dbo.Member M ON tcr.MemberID=M.MemberID

WHERE   AppraisalDate BETWEEN 
				CONVERT(VARCHAR(10), DATEADD(wk, - 6, GETDATE()), 101)
			AND	CONVERT(VARCHAR(10), DATEADD(wk, - 2, DATEADD(dd,-1,GETDATE())), 101)
AND		InventoryReceivedDate IS NOT NULL
AND		BusinessUnitID = @DealerID

ORDER
BY		AppraisalDate ASC
GO
