 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_RedistributionOverview_Main]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_RedistributionOverview_Main] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_RedistributionOverview_Main] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_RedistributionOverview_Main]
	@DealerGroupID INT,
	@DealerIDs VARCHAR(500),
	@MemberID INT
AS

SET NOCOUNT ON;

SET ANSI_WARNINGS OFF;

DECLARE @Dealers TABLE (
	BusinessUnitID INT
)

IF UPPER(@DealerIDs) = 'ALL'
	INSERT INTO @Dealers (BusinessUnitID)
	SELECT	DS.BusinessUnitID
	FROM	[IMT].dbo.BusinessUnit DS
	JOIN	[IMT].dbo.BusinessUnitRelationship DR ON DR.BusinessUnitID = DS.BusinessUnitID
	JOIN	[IMT].dbo.BusinessUnit DG ON DG.BusinessUnitID = DR.ParentID
	JOIN	[IMT].dbo.DealerPreference DP ON DP.BusinessUnitID = DS.BusinessUnitID
	JOIN	dbo.MemberAccess(@DealerGroupID,@MemberID) MA ON MA.BusinessUnitID = DS.BusinessUnitID
	JOIN	(
				SELECT	BusinessUnitID, SUM(Active) NumerOfActiveUpgrades
				FROM	[IMT].dbo.DealerUpgrade
				GROUP
				BY		BusinessUnitID
			) DU ON DU.BusinessUnitID = DS.BusinessUnitID
	WHERE	DS.Active = 1
	AND		DP.GoLiveDate IS NOT NULL
	AND		DS.BusinessUnitTypeID = 4
	AND		DU.NumerOfActiveUpgrades > 0
	AND		DG.BusinessUnitID = @DealerGroupID
ELSE
	INSERT INTO @Dealers (BusinessUnitID)
	SELECT	CAST(S.Value AS INT)
	FROM	[IMT].dbo.Split(@DealerIDs, ',') S

SELECT	dense_rank() over ( order by V.[ParentID], V.[VehicleID]) AS [VehicleRank],
        V.[RowNum],
        V.[NumOfRecentTrades],
        V.[ParentID],
        V.[StockNumber],
        V.[InventoryID],
        V.[UnitCost],
        V.[UnitCostBucket],
        V.[BusinessUnitID],
        V.[VehicleID],
        V.[VehicleDescription],
        V.[PriorBusinessUnitID],
        V.[CurrentLocationBusinessUnitID],
        V.[CurrentUnitCost],
        V.[CurrentUnitCostBucket],
        V.[CurrentMileageReceived],
        V.[CurrentMileageReceivedBucket],
        V.[CurrentVehicleLight],
        [Dealership] = LTRIM(V.[Dealership]),
        V.[AgeInDays],
        V.[DaysToSale],
        V.[DaysInStock],
        V.[VehicleLight],
        V.[MileageReceived],
        V.[MileageReceivedBucket],
        V.[Color],
        V.[InventoryReceivedDate],
        V.[DeleteDt],
        V.[InventoryActive],
        V.[PriorRowNum],
        V.[PriorInventoryID],
        V.[PriorUnitCost],
        [PriorDealership] = LTRIM(V.[PriorDealership]),
        V.[PriorAgeInDays],
        V.[PriorDaysToSale],
        V.[PriorDaysInStock],
        V.[PriorInventoryReceivedDate],
        V.[PriorDeleteDt],
        V.[PriorInventoryActive]
FROM	dbo.VehicleRedistribution V
JOIN	@Dealers D ON D.BusinessUnitID = V.CurrentLocationBusinessUnitID
WHERE   V.[ParentID] = @DealerGroupID
ORDER
BY		V.[ParentID],
        V.[VehicleID],
        V.[RowNum]
GO
