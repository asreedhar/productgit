
/****** Object:  StoredProcedure [dbo].[Fetch#RetailInventorySalesAnalysisSummary]    Script Date: 08/28/2014 10:45:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Fetch#RetailInventorySalesAnalysisSummary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Fetch#RetailInventorySalesAnalysisSummary]
GO


/****** Object:  StoredProcedure [dbo].[Fetch#RetailInventorySalesAnalysisSummary]    Script Date: 08/28/2014 10:45:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		Pradnya Sawant
-- Create date: 2014-08-11
-- Last Modified By : Pradnya Sawant
-- Last Modified Date : 2014-08-11
-- Description:	This stored procedure returns the data fro Store Level Retail Inventory Sales Analysis -Summary Data
-- =============================================


CREATE PROCEDURE [dbo].[Fetch#RetailInventorySalesAnalysisSummary]
	@PeriodID INT,
	@SelectedDealerId INT,
	@TradeInType VARCHAR(1)
	
AS

BEGIN

--DECLARE
--	@PeriodID INT=4,
--	@SelectedDealerId INT=100147,
--	@TradeInType VARCHAR(1) = 'A'
DECLARE
	@LocalPeriodID INT,
	@LocalDealerID INT,
	@LocalTradeOrPurchase VARCHAR(1)
	
	SELECT @LocalPeriodID=@PeriodID, @LocalDealerID=@SelectedDealerId, @LocalTradeOrPurchase=@TradeInType
SET NOCOUNT ON

DECLARE @err INT, @TPFilterLow INT, @TPFilterHigh INT
EXEC @err = Reports.[dbo].[ValidateParameter_TradeOrPurchase] @LocalTradeOrPurchase, @TPFilterLow out, @TPFilterHigh out
IF @err <> 0 GOTO Failed

IF @LocalTradeOrPurchase='A' 
SET @TPFilterHigh=3 --Including 'Unknown' trade purchase type

Declare @Buckets Table (
	counter tinyint,
	daysLow int,
	daysHigh int,
	saleDesc varchar(1),
	bucketDesc varchar(20)
)

Insert Into @Buckets (counter, daysLow, daysHigh, saleDesc, bucketDesc) VALUES (0,0,29,'R','Retailed 0-29 Days')
Insert Into @Buckets (counter, daysLow, daysHigh, saleDesc, bucketDesc) VALUES (1,30,59,'R','Retailed 30-59 Days')
Insert Into @Buckets (counter, daysLow, daysHigh, saleDesc, bucketDesc) VALUES (2,60,2147483647,'R','Retailed 60+ Days')
Insert Into @Buckets (counter, daysLow, daysHigh, saleDesc, bucketDesc) VALUES (3,30,2147483647,'W','Aged Wholesale')

DECLARE @BeginDate SMALLDATETIME, @EndDate SMALLDATETIME


Select @BeginDate=StartDate,@EndDate=EndDate FROM IMT.dbo.GetDatesFromPeriodId(@LocalPeriodID,1);



SELECT COUNT(I.InventoryID) AS AttemptedRetail
INTO #Temp
FROM	[FLDW].dbo.Inventory I
JOIN	[FLDW].dbo.VehicleSale VS on I.InventoryID = VS.InventoryID
JOIN	@Buckets as B on ((I.DaysToSale between B.daysLow and B.daysHigh) AND B.SaleDesc = VS.SaleDescription)
WHERE	I.BusinessUnitID = @LocalDealerID
AND		I.InventoryType = 2
AND		I.TradeOrPurchase BETWEEN @TPFilterLow AND @TPFilterHigh
AND		VS.DealDate Between @BeginDate And @EndDate
AND		I.UnitCost>0
AND		VS.SalePrice>0
AND		I.MileageReceived>0




SELECT	B.counter,
		COUNT(counter) DealCount,
		MAX(T.AttemptedRetail) AS PercentSaleDescription,
		MAX(bucketDesc) BucketDesc,
		B.SaleDesc,
		AVG(CAST(VS.SalePrice AS FLOAT)) AvgSalePrice,
		AVG(CAST(I.DaysToSale AS FLOAT)) AvgDaysToSale,
		SUM(CAST(VS.FrontEndGross AS FLOAT)) TotalFrontEndGross,
		AVG(CAST(VS.FrontEndGross AS FLOAT)) AvgFrontEndGross,
		AVG(CAST(I.UnitCost AS FLOAT)) AvgUnitCost,
		AVG(CAST(I.MileageReceived AS FLOAT)) AvgMileageReceived,
		Cast(SUM(Case When CurrentVehicleLight = 1 Then 1 Else 0 End) as real) / Cast(COUNT(1) as real) PctRedLights,
		Cast(SUM(Case When CurrentVehicleLight = 2 Then 1 Else 0 End) as real) / Cast(COUNT(1) as real) PctYellowLights,
		Cast(SUM(Case When CurrentVehicleLight = 3 Then 1 Else 0 End) as real) / Cast(COUNT(1) as real) PctGreenLights
INTO #Final
FROM	[FLDW].dbo.Inventory I
JOIN	[FLDW].dbo.VehicleSale VS on I.InventoryID = VS.InventoryID
RIGHT JOIN	@Buckets as B on ((I.DaysToSale between B.daysLow and B.daysHigh) AND B.SaleDesc = VS.SaleDescription)
CROSS JOIN #Temp T 
WHERE	I.BusinessUnitID = @LocalDealerID
AND		I.InventoryType = 2
AND		I.TradeOrPurchase BETWEEN @TPFilterLow AND @TPFilterHigh
AND		VS.DealDate Between @BeginDate And @EndDate
AND		I.UnitCost>0
AND		VS.SalePrice>0
AND		I.MileageReceived>0
Group By B.counter, B.SaleDesc
Order By B.counter


INSERT INTO #Final(counter)
SELECT B.counter AS counter
FROM @Buckets B 
WHERE B.counter NOT IN (SELECT [counter] FROM #Final)
 
SELECT * FROM #Final
Order By [counter]

DROP TABLE #Temp
DROP TABLE #Final
Failed:
SET NOCOUNT OFF
END

GO

GRANT EXEC ON [Reports].[dbo].[Fetch#RetailInventorySalesAnalysisSummary] TO firstlook 
GO

