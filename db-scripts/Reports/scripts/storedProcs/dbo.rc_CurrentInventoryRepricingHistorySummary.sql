
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_CurrentInventoryRepricingHistorySummary]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_CurrentInventoryRepricingHistorySummary] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_CurrentInventoryRepricingHistorySummary] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_CurrentInventoryRepricingHistorySummary]
	@DealerID INT
AS

SET NOCOUNT ON;

DECLARE @Buckets TABLE (
	counter int,
	lowAge int,
	highAge int
)

INSERT INTO @Buckets (counter, lowAge, highAge) VALUES (0 , 21 , 29)
INSERT INTO @Buckets (counter, lowAge, highAge) VALUES (1 , 30 , 39)
INSERT INTO @Buckets (counter, lowAge, highAge) VALUES (2 , 40 , 49)
INSERT INTO @Buckets (counter, lowAge, highAge) VALUES (3 , 50 , 59)
INSERT INTO @Buckets (counter, lowAge, highAge) VALUES (4 , 60 , 999)

CREATE TABLE #InventorySnapshot (
	BusinessUnitID INT,
	InventoryID INT,
	VehicleID INT,
	InventoryReceivedDate SMALLDATETIME
)

INSERT INTO #InventorySnapshot (BusinessUnitID, InventoryID, VehicleID, InventoryReceivedDate)
SELECT	I.BusinessUnitID, I.InventoryID, I.VehicleID, I.InventoryReceivedDate
FROM	[FLDW].dbo.InventoryActive I
WHERE	I.InventoryActive = 1
AND		I.InventoryType = 2
AND		I.BusinessUnitID = @DealerID

SELECT	b.counter,
		MAX( b.lowAge ) as 'lowAge',
		MAX( b.highAge ) as 'highAge',
		AVG( lph.[Difference] ) as 'AvgRepriceValue',
		COUNT( DISTINCT( lph.InventoryID ) )  as 'VehiclesRepriced',
		MAX( i.Vehicles ) as 'Vehicles'

FROM	@Buckets b

LEFT JOIN
	(
		SELECT	i.InventoryReceivedDate,
				DATEDIFF(DD, i.InventoryReceivedDate, GETDATE()) as 'Age',
				( LPH2.ListPrice - LPH1.ListPrice ) as 'Difference',
				i.InventoryID

		FROM	#InventorySnapshot I
		JOIN	(
					SELECT	LPH1.InventoryID, LPH1.ListPrice, LPH1.DMSReferenceDT, LPH2.NextDMSReferenceDT
					FROM 	[IMT].dbo.Inventory_ListPriceHistory LPH1
					LEFT JOIN
						(
							SELECT	L1.InventoryID, L1.DMSReferenceDT, MIN(L2.DMSReferenceDT) NextDMSReferenceDT
							FROM	[IMT].dbo.Inventory_ListPriceHistory L1
							JOIN	[IMT].dbo.Inventory_ListPriceHistory L2 ON L1.InventoryID = L2.InventoryID
							WHERE	L1.ListPrice > 0
							AND		L2.ListPrice > 0
							AND		L1.DMSReferenceDT < L2.DMSReferenceDT
							GROUP
							BY		L1.InventoryID, L1.DMSReferenceDT
						) LPH2 ON LPH2.InventoryID = LPH1.InventoryID AND LPH2.DMSReferenceDT = LPH1.DMSReferenceDT
					WHERE	LPH1.ListPrice > 0
				) LPH1 ON LPH1.InventoryID = I.InventoryID
		JOIN	[IMT].dbo.Inventory_ListPriceHistory LPH2 ON LPH1.InventoryID = LPH2.InventoryID AND LPH1.NextDMSReferenceDT = LPH2.DMSReferenceDT
		WHERE	LPH2.ListPrice > 0
		AND		LPH1.ListPrice > 0
	) lph ON lph.Age Between b.lowAge And b.highAge

LEFT JOIN
	(
		SELECT	bc.counter,
				bc.lowAge,
				bc.highAge,
				COUNT(ic.InventoryID) as 'Vehicles'

		FROM	@Buckets bc

		LEFT JOIN
			(
				SELECT	I.InventoryID, DATEDIFF(DD, I.InventoryReceivedDate, GETDATE()) as 'Age'
				FROM	#InventorySnapshot I
			) ic ON ic.Age Between bc.lowAge And bc.highAge
		
		GROUP BY bc.Counter , bc.lowAge , bc.highAge
		
	) i ON b.counter = i.counter

GROUP BY b.counter

DROP TABLE #InventorySnapshot

GO
