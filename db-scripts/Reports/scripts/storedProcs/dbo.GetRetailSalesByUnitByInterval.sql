
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetRetailSalesByUnitByInterval]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[GetRetailSalesByUnitByInterval]  AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[GetRetailSalesByUnitByInterval] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[GetRetailSalesByUnitByInterval]
(
	 @BusinessUnitId INT= 100068 /*Param???*/
	 ,@Interval INT= -90 /*Param???*/
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @dt SMALLDATETIME = DATEADD(day, @Interval, GETDATE())

	/*
		Create Window from interval period to today
	*/
	DECLARE @DateID90Days INT= DATEPART(YEAR, @dt) * 10000 + DATEPART(MONTH, @dt)
		* 100 + DATEPART(DAY, @dt)

	SET @dt = GETDATE()
	DECLARE @Today INT= DATEPART(YEAR, @dt) * 10000 + DATEPART(MONTH, @dt) * 100
		+ DATEPART(DAY, @dt);

	/*
		Recursive CTE for Children Account
		Current, at least for Hendricks, only one level has Sales data, 
		but the data model supports sales at any level, not just the leaf node

	*/    
	WITH    relations
			  AS ( SELECT   bu.BusinessUnitID
						   ,bur.ParentID, BusinessUnitShortName
						   ,bu.Active
				   FROM     IMT.dbo.BusinessUnit bu
							LEFT JOIN imt.dbo.BusinessUnitRelationship bur ON bur.BusinessUnitID = bu.BusinessUnitId
				 ),   GetChildren
			  AS ( SELECT   r.BusinessUnitID
						   ,r.ParentID
						   ,0 [lvl]
						   ,r.BusinessUnitShortName
						   ,0 RelationTypeId
						   ,r.Active
				   FROM     relations r
				   WHERE    r.BusinessUnitID = @BusinessUnitId
	      
				   UNION ALL
				   SELECT   r.businessunitid
						   ,r.parentid
						   ,c.lvl - 1
						   ,r.BusinessUnitShortName
						   ,1 RelationTypeId
						   ,r.Active
				   FROM     relations r
							INNER JOIN GetChildren c ON c.BusinessUnitID = r.ParentID
							--AND r.Active=1
	                        
				 ) 
	SELECT bu.ParentID
		   ,bu.BusinessUnitShortName
	       
		   ,[AvgDaysToSaleRetail] = AVG(CASE WHEN s.SaleTypeID = 1 THEN DaysToSale
											 ELSE NULL
										END)
		
		   ,RetailSales = SUM(CASE WHEN s.SaleTypeID = 1 THEN 1
								   ELSE 0
							  END)
		
		   ,AvgFrontEndGrossRetail = AVG(CASE WHEN s.SaleTypeID = 1
											  THEN FrontEndGross
											  ELSE NULL
										 END)

		   ,AvgBackEndGrossRetail = AVG(CASE WHEN s.SaleTypeID = 1
											 THEN BackEndGross
											 ELSE NULL
										END)

	FROM   GetChildren bu
			inner  JOIN FLDW.dbo.InventorySales_F s  ON s.BusinessUnitID = bu.BusinessUnitID AND s.inventorytypeid=2 /* Added Only Used Vehicles!*/
				AND s.DealDateTimeID BETWEEN @DateID90Days AND @Today
				AND s.SaleTypeID IN ( 1, 2 ) /*1=Retail,2=Wholesale,2=No Sale,4=Other,5=Unsold*/
	WHERE 	BU.Active=1
	GROUP BY GROUPING SETS (bu.ParentID,bu.BusinessUnitShortName)-- WITH rollup
	      
	ORDER BY bu.BusinessUnitShortName
END
GO


