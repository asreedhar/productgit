
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_ProfitByAppraiser]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_ProfitByAppraiser] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_ProfitByAppraiser] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_ProfitByAppraiser]
	@PeriodID INT,
	@DealerID INT,
	@AppraisalTypeID INT
AS

SET NOCOUNT ON;

DECLARE @BeginDate SMALLDATETIME
DECLARE @EndDate SMALLDATETIME

SELECT	@BeginDate = BeginDate, @EndDate  = EndDate
FROM	[FLDW].dbo.Period_D
WHERE	PeriodID = @PeriodID

DECLARE @TradeOrPurchase TINYINT

/**
 *  Behold some great design:
 *
 *	SELECT * FROM IMT.dbo.AppraisalType
 *	SELECT * FROM IMT.dbo.lu_TradeOrPurchase
 */
 
SELECT @TradeOrPurchase = 
	CASE @AppraisalTypeID
		WHEN 1 THEN 2
		WHEN 2 THEN 1
		ELSE 3
	END

SELECT
	NULLIF(AV.AppraiserName,'') AppraiserName, 
	--Retail Sales
	SUM(CASE WHEN VS.SaleDescription = 'R' THEN 1 ELSE 0 END) as RetailSold, 
	AVG(CASE WHEN VS.SaleDescription = 'R' THEN FrontEndGross ELSE NULL END ) as RetailAvgGross,
	AVG(CASE WHEN VS.SaleDescription = 'R' THEN I.daysToSale ELSE NULL END ) as RetailAvgSaleDays,
	--Wholesale Sales
	SUM(CASE WHEN VS.SaleDescription = 'W' AND I.DaysToSale < 30 THEN 1 ELSE 0 END) as WholesaleSold, 
	AVG(CASE WHEN VS.SaleDescription = 'W' AND I.DaysToSale < 30 THEN FrontEndGross ELSE NULL END ) as WholesaleAvgGross, 
	AVG(CASE WHEN VS.SaleDescription = 'W' AND I.DaysToSale < 30 THEN I.daysToSale ELSE NULL END ) as WholesaleAvgSaleDays,
	--No Sales
	SUM(CASE WHEN VS.SaleDescription = 'W' AND I.DaysToSale > 29 THEN 1 ELSE 0 END) as NoSales, 
	AVG(CASE WHEN VS.SaleDescription = 'W' AND I.DaysToSale > 29 THEN FrontEndGross ELSE NULL END ) as NoSaleAvgLoss

FROM	
	[FLDW].dbo.Inventory AS I 
	JOIN [FLDW].dbo.VehicleSale AS VS ON I.InventoryID = VS.InventoryID
	JOIN [FLDW].dbo.Vehicle AS V ON I.BusinessUnitID = V.BusinessUnitID AND I.VehicleID = V.VehicleID 
	LEFT JOIN [FLDW].dbo.Appraisal_F A ON I.BusinessUnitID = A.BusinessUnitID AND V.VehicleID = A.VehicleID
	LEFT JOIN [FLDW].dbo.AppraisalValues AV on A.Value = AV.Value and A.AppraisalId = AV.AppraisalID AND A.AppraisalValueID=AV.AppraisalValueID
WHERE
	I.BusinessUnitID = @DealerID
	AND VS.DealDate BETWEEN @BeginDate and @EndDate
	AND I.TradeOrPurchase = @TradeOrPurchase
	AND I.InventoryType = 2	-- Used Vehicles only
GROUP 
BY	NULLIF(AV.AppraiserName,'') 
ORDER
BY	RetailSold DESC, NULLIF(AV.AppraiserName,'') DESC


GO


