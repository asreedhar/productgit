 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_RedistributionOverview_Name]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_RedistributionOverview_Name] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_RedistributionOverview_Name] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_RedistributionOverview_Name]
	@DealerGroupID INT,
	@DealerIDs VARCHAR(500),
	@MemberID INT
AS

SET NOCOUNT ON;

SET ANSI_WARNINGS OFF;

DECLARE @Dealers TABLE (
	BusinessUnitID INT
)

IF @DealerIDs = 'All'
	INSERT INTO @Dealers (BusinessUnitID)
	SELECT	DS.BusinessUnitID
	FROM	[IMT].dbo.BusinessUnit DS
	JOIN	[IMT].dbo.BusinessUnitRelationship DR ON DR.BusinessUnitID = DS.BusinessUnitID
	JOIN	[IMT].dbo.BusinessUnit DG ON DG.BusinessUnitID = DR.ParentID
	JOIN	[IMT].dbo.DealerPreference DP ON DP.BusinessUnitID = DS.BusinessUnitID
	JOIN	dbo.MemberAccess(@DealerGroupID, @MemberID) MA ON MA.BusinessUnitID = DS.BusinessUnitID
	JOIN	(
				SELECT	BusinessUnitID, SUM(Active) NumerOfActiveUpgrades
				FROM	[IMT].dbo.DealerUpgrade
				GROUP
				BY		BusinessUnitID
			) DU ON DU.BusinessUnitID = DS.BusinessUnitID
	WHERE	DS.Active = 1
	AND		DP.GoLiveDate IS NOT NULL
	AND		DS.BusinessUnitTypeID = 4
	AND		DU.NumerOfActiveUpgrades > 0
	AND		DG.BusinessUnitID = @DealerGroupID
ELSE
	INSERT INTO @Dealers (BusinessUnitID)
	SELECT	CAST(S.Value AS INT)
	FROM	[IMT].dbo.Split(@DealerIDs, ',') S

DECLARE @NumberOfDealers INT

SELECT @NumberOfDealers = COUNT(*) FROM @Dealers

IF @NumberOfDealers = 1
	SELECT	DG.BusinessUnit GroupName, DS.BusinessUnit DealerName
	FROM	[IMT].dbo.BusinessUnit DS
	JOIN	[IMT].dbo.BusinessUnitRelationship DR ON DR.BusinessUnitID = DS.BusinessUnitID
	JOIN	[IMT].dbo.BusinessUnit DG ON DG.BusinessUnitID = DR.ParentID
	JOIN	@Dealers D ON DS.BusinessUnitID = D.BusinessUnitID
	WHERE	DS.Active = 1
	AND		DS.BusinessUnitTypeID = 4
	AND		DG.BusinessUnitID = @DealerGroupID
ELSE
	SELECT	DG.BusinessUnit GroupName, NULL DealerName
	FROM	[IMT].dbo.BusinessUnit DG
	WHERE	DG.BusinessUnitTypeID = 3
	AND		DG.BusinessUnitID = @DealerGroupID

GO
