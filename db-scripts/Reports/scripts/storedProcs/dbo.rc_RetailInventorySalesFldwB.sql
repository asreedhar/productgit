
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_RetailInventorySalesFldwB]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_RetailInventorySalesFldwB] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_RetailInventorySalesFldwB] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_RetailInventorySalesFldwB]
	@PeriodID INT,
	@DealerID INT,
	@TradeOrPurchase VARCHAR(1) = 'A'
AS

SET NOCOUNT ON

DECLARE @err INT, @TPFilterLow INT, @TPFilterHigh INT
EXEC @err = [dbo].[ValidateParameter_TradeOrPurchase] @TradeOrPurchase, @TPFilterLow out, @TPFilterHigh out
IF @err <> 0 GOTO Failed

Declare @Buckets Table (
	counter tinyint,
	daysLow int,
	daysHigh int,
	saleDesc varchar(1),
	bucketDesc varchar(20)
)

Insert Into @Buckets (counter, daysLow, daysHigh, saleDesc, bucketDesc) VALUES (0,0,29,'R','Retailed 0-29 Days')
Insert Into @Buckets (counter, daysLow, daysHigh, saleDesc, bucketDesc) VALUES (1,30,59,'R','Retailed 30-59 Days')
Insert Into @Buckets (counter, daysLow, daysHigh, saleDesc, bucketDesc) VALUES (2,60,2147483647,'R','Retailed 60+ Days')
Insert Into @Buckets (counter, daysLow, daysHigh, saleDesc, bucketDesc) VALUES (3,30,2147483647,'W','Aged Wholesale')

DECLARE @BeginDate SMALLDATETIME, @EndDate SMALLDATETIME

SELECT	@BeginDate = BeginDate, @EndDate = EndDate
FROM	[FLDW].dbo.Period_D P
WHERE	PeriodID = @PeriodID

SELECT	B.counter,
		COUNT(counter) DealCount,
		MAX(bucketDesc) BucketDesc,
		B.SaleDesc,
		AVG(VS.SalePrice) AvgSalePrice,
		AVG(I.DaysToSale) AvgDaysToSale,
		SUM(VS.FrontEndGross) TotalFrontEndGross,
		AVG(VS.FrontEndGross) AvgFrontEndGross,
		AVG(I.UnitCost) AvgUnitCost,
		AVG(I.MileageReceived) AvgMileageReceived,
		Cast(SUM(Case When InitialVehicleLight = 1 Then 1 Else 0 End) as real) / Cast(COUNT(1) as real) PctRedLights,
		Cast(SUM(Case When InitialVehicleLight = 2 Then 1 Else 0 End) as real) / Cast(COUNT(1) as real) PctYellowLights,
		Cast(SUM(Case When InitialVehicleLight = 3 Then 1 Else 0 End) as real) / Cast(COUNT(1) as real) PctGreenLights
FROM	[FLDW].dbo.Inventory I
JOIN	[FLDW].dbo.VehicleSale VS on I.InventoryID = VS.InventoryID
JOIN	@Buckets as B on ((I.DaysToSale between B.daysLow and B.daysHigh) AND B.SaleDesc = VS.SaleDescription)
WHERE	I.BusinessUnitID = @DealerID
AND		I.InventoryType = 2
AND		I.TradeOrPurchase BETWEEN @TPFilterLow AND @TPFilterHigh
AND		VS.DealDate Between @BeginDate And @EndDate
Group By B.counter, B.SaleDesc
Order By B.counter

Failed:

GO
