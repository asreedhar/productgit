IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetDealerWithHierarchy]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[GetDealerWithHierarchy]  AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[GetDealerWithHierarchy] TO [StoredProcedureUser]
GO

ALTER PROCEDURE dbo.GetDealerWithHierarchy 
AS
BEGIN
;WITH    relations
          AS ( SELECT   bu.BusinessUnitID
                       ,bur.ParentID, LTrim(BusinessUnitShortName) BusinessUnitShortName
                       ,bu.Active
               FROM     IMT.dbo.BusinessUnit bu
                        LEFT JOIN imt.dbo.BusinessUnitRelationship bur ON bur.BusinessUnitID = bu.BusinessUnitId
             ),   GetChildren
          AS ( SELECT   r.BusinessUnitID
                       ,r.ParentID
                       ,0 [lvl]
                       ,r.BusinessUnitShortName
                       ,0 RelationTypeId
                       ,r.Active
                       ,CAST(r.BusinessUnitShortName  AS VARCHAR(MAX))+'.'  FullPath 
                       ,CAST(r.BusinessUnitShortName  AS VARCHAR(MAX))+'.' ParentPath 
               FROM     relations r
               WHERE    r.ParentID = 100150 
      
               UNION ALL
               SELECT   r.businessunitid
                       ,r.parentid
                       ,c.lvl + 1
                       ,r.BusinessUnitShortName
                       ,1 RelationTypeId
                       ,r.Active
                       ,CAST(c.FullPath + '' + CAST(r.BusinessUnitShortName AS VARCHAR(MAX)) AS VARCHAR(MAX))+'.'
                       ,c.FullPath AS ParentPath
               FROM     relations r
                        INNER JOIN GetChildren c ON c.BusinessUnitID = r.ParentID
                        --AND r.Active=1
                        
             ) 
SELECT BusinessUnitID,Parentid,
	ISNULL(replicate('    ',lvl),'') +BusinessUnitShortName [DisplayHierarchy],FullPath,ParentPath,lvl,gc.Active
FROM  GetChildren gc 
WHERE LEN(LTRIM(BusinessUnitShortName))>0  --AND Active=1
ORDER BY 
	 FullPath
END