IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_CurrentInventoryRepricingHistoryListEDGE]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_CurrentInventoryRepricingHistoryListEDGE] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_CurrentInventoryRepricingHistoryListEDGE] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_CurrentInventoryRepricingHistoryListEDGE]
	@InventoryID INT
AS
/*	MAK	05/18/2009	Wants price changes not all repricing events.
				Included inventory table for consistency.*/

SELECT	ilph.Created, 
		ilph.Notes2, 
		ilph.Value
FROM	[IMT].dbo.Inventory i 
JOIN	(SELECT	ilph.inventoryID,
				ilph.Created,
				ilph.Notes2,
				ilph.Value
		FROM	[IMT].dbo.AIP_Event ilph
		WHERE	ilph.InventoryID =@InventoryID
		AND	ilph.Notes2 != '0.0'
		AND	IsNumeric(ilph.Notes2)=1) ilph
		
ON		i.InventoryID =ilph.inventoryID
AND		CAST(ilph.Notes2 as Decimal (9,2)) <>ilph.value
ORDER
BY		ilph.Created DESC

GO

