
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_RetailInventorySalesFldwA]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_RetailInventorySalesFldwA] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_RetailInventorySalesFldwA] TO [StoredProcedureUser]
GO


---Parmeters--------------------------------------------------------------------
-- @PeriodID		PeriodID from FLDW.dbo.Period_D
-- @DealerID		DealerID
-- @TradeOrPurchase	'A' - ALL - Filters nothing
--				'T' - Trades - Filters where InventoryType is Trade
--				'P' - Purchase - Filters where InventoryType is Purchase
--------------------------------------------------------------------------------
ALTER PROCEDURE [dbo].[rc_RetailInventorySalesFldwA]
	@PeriodID INT,
	@DealerID INT,
	@TradeOrPurchase VARCHAR(1) = 'A'
AS

SET NOCOUNT ON;

DECLARE @err INT, @TPFilterLow INT, @TPFilterHigh INT
EXEC @err = [dbo].[ValidateParameter_TradeOrPurchase] @TradeOrPurchase, @TPFilterLow out, @TPFilterHigh out
IF @err <> 0 GOTO Failed

DECLARE @BeginDate SMALLDATETIME, @EndDate SMALLDATETIME

SELECT	@BeginDate = BeginDate, @EndDate = EndDate
FROM	[FLDW].dbo.Period_D P
WHERE	PeriodID = @PeriodID;

WITH RetailSales AS
(
	SELECT COUNT(VS.InventoryID) AS Units
	FROM [FLDW].dbo.Inventory I
	JOIN [FLDW].dbo.VehicleSale VS on I.InventoryID = VS.InventoryID
	WHERE I.BusinessUnitID = @DealerID
	AND I.InventoryType = 2
	AND VS.DealDate BETWEEN @BeginDate AND @EndDate
	AND	VS.saledescription = 'R'
	AND I.TradeOrPurchase BETWEEN @TPFilterLow AND @TPFilterHigh
), AgedWholesaleSales AS
(
	SELECT COUNT(VS.InventoryID) AS Units
	FROM [FLDW].dbo.Inventory I
	JOIN [FLDW].dbo.VehicleSale VS on I.InventoryID = VS.InventoryID
	WHERE I.BusinessUnitID = @DealerID
	AND I.InventoryType = 2
	AND VS.DealDate BETWEEN @BeginDate AND @EndDate
	AND	VS.saledescription = 'W' 
	AND I.DaysToSale >= 30
	AND I.TradeOrPurchase BETWEEN @TPFilterLow AND @TPFilterHigh
)
SELECT
	B.BusinessUnit AS BusinessUnitID
	, CASE @TradeOrPurchase
		WHEN 'A' THEN 'Trades And Purchases'
		WHEN 'T' THEN 'Trades Only'
		WHEN 'P' THEN 'Purchases Only'
	  END AcquisitionType
	, P.PeriodID
	, P.BeginDate AS FromDate
	, P.EndDate AS ToDate
	, R.Units + AW.Units AS TotalUnitsSold
	, R.Units AS RetailUnitsSold
	, AW.Units AS AgedWholesaleUnitsSold
	, CASE WHEN (R.Units + AW.Units) = 0 
		THEN NULL 
		ELSE R.Units / CAST((R.Units + AW.Units) AS FLOAT) 
	  END RetailPercent
	, CASE WHEN (R.Units + AW.Units) = 0 
		THEN NULL
		ELSE AW.Units / CAST((R.Units + AW.Units) AS FLOAT) 
	  END AgedWholesalePercent
FROM [FLDW].dbo.BusinessUnit B
CROSS JOIN [FLDW].dbo.Period_D P
CROSS JOIN RetailSales R
CROSS JOIN AgedWholesaleSales AW
WHERE B.BusinessUnitID = @DealerID
AND P.PeriodID = @PeriodID

Failed:

GO