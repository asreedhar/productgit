
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_RedYellowGreenLightInventory]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_RedYellowGreenLightInventory] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_RedYellowGreenLightInventory] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_RedYellowGreenLightInventory]
	@DealerID INT
AS

SET NOCOUNT ON;

SELECT	Inventory.CurrentVehicleLight,
		Inventory.AgeInDays,
		Vehicle.VehicleYear,
		MakeModel.MAKE,
		MakeModel.MODEL,
		Segment.Segment DisplayBodyType,
		Vehicle.VehicleTrim,
		Vehicle.BaseColor,
		Inventory.UnitCost,
		Inventory.MileageReceived,
		Inventory.TradeOrPurchase,
		Inventory.ListPrice,
		Vehicle.VIN,
		Bookout.UnitCost UnitCostIB,
		Bookout.BookValue BookValueIB,
		Bookout.ThirdPartyCategoryID ThirdPartyCategoryIDIB,
		Inventory.StockNumber
FROM 	[FLDW].dbo.Inventory Inventory
JOIN	[FLDW].dbo.Vehicle Vehicle on Inventory.VehicleID = Vehicle.VehicleID And Inventory.BusinessUnitID = Vehicle.BusinessUnitID
JOIN	[FLDW].dbo.Segment on Vehicle.SegmentID = Segment.SegmentID
JOIN	[FLDW].dbo.MakeModelGrouping MakeModel on Vehicle.makemodelgroupingid = MakeModel.makemodelgroupingid
JOIN	[FLDW].dbo.DealerPreference Preference ON Inventory.BusinessUnitID = Preference.BusinessUnitID
LEFT JOIN [FLDW].dbo.InventoryBookout_F Bookout on Inventory.InventoryID = Bookout.InventoryID and Bookout.ThirdPartyCategoryID = Preference.BookOutPreferenceID
WHERE	Inventory.BusinessUnitID = @DealerID
AND		Inventory.InventoryType = 2
AND		Inventory.InventoryActive = 1
AND		Inventory.CurrentVehicleLight != 0
GO
