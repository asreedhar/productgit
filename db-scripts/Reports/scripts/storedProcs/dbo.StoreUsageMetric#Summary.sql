
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StoreUsageMetric#Summary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[StoreUsageMetric#Summary]
GO

SET NUMERIC_ROUNDABORT OFF;

SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT ON;

SET QUOTED_IDENTIFIER, ANSI_NULLS ON;

GO

CREATE PROCEDURE [dbo].[StoreUsageMetric#Summary]
	-- no parameters
AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.StoreUsageMetric#Summary.sql,v 1.2 2009/03/16 19:27:05 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * This is a internal report used by Pat that summarizes (rather crudely)
 * the usage of our system by our dealers.  It replaces the stored procedure
 * ReportHost.dbo.GetTCBCC (Tom Bergerson's Command Center).
 * 
 * Parameters
 * ----------
 *
 * None
 * 
 * Exceptions
 * ----------
 * 
 * None
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

-- None

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

CREATE TABLE #Detail (
	BusinessUnitID              INT         NOT NULL,
	DealerGroup                 VARCHAR(40) NOT NULL,
	Dealer                      VARCHAR(40) NOT NULL,
	TradeAnalyzerPct            INT         NULL,
	TradeAnalyzerScore          CHAR(1)     NOT NULL,
	InventoryPlanningPct        INT         NULL,
	InventoryPlanningScore      CHAR(1)     NOT NULL,
	OverallPct                  INT         NULL,
	OverallScore                CHAR(1)     NOT NULL
)

DECLARE @Results TABLE (
	RowType           VARCHAR(64) NOT NULL,
	TradeAnalyzer     INT NOT NULL,
	InventoryPlanning INT NOT NULL,
	Overall           INT NOT NULL
)

--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

INSERT INTO #Detail EXEC [dbo].[StoreUsageMetric#Detail]

INSERT INTO @Results (RowType, TradeAnalyzer, InventoryPlanning, Overall)

SELECT	'Total Green' RowType,
	SUM(CASE WHEN TradeAnalyzerScore = 'G' THEN 1 ELSE 0 END) TradeAnalyzer,
	SUM(CASE WHEN InventoryPlanningScore = 'G' THEN 1 ELSE 0 END) InventoryPlanning,
	SUM(CASE WHEN OverallScore = 'G' THEN 1 ELSE 0 END) Overall
FROM	#Detail

INSERT INTO @Results (RowType, TradeAnalyzer, InventoryPlanning, Overall)

SELECT	'Total Yellow' RowType,
	SUM(CASE WHEN TradeAnalyzerScore = 'Y' THEN 1 ELSE 0 END) TradeAnalyzer,
	SUM(CASE WHEN InventoryPlanningScore = 'Y' THEN 1 ELSE 0 END) InventoryPlanning,
	SUM(CASE WHEN OverallScore = 'Y' THEN 1 ELSE 0 END) Overall
FROM	#Detail

INSERT INTO @Results (RowType, TradeAnalyzer, InventoryPlanning, Overall)

SELECT	'Total Red' RowType,
	SUM(CASE WHEN TradeAnalyzerScore = 'R' THEN 1 ELSE 0 END) TradeAnalyzer,
	SUM(CASE WHEN InventoryPlanningScore = 'R' THEN 1 ELSE 0 END) InventoryPlanning,
	SUM(CASE WHEN OverallScore = 'R' THEN 1 ELSE 0 END) Overall
FROM	#Detail

INSERT INTO @Results (RowType, TradeAnalyzer, InventoryPlanning, Overall)

SELECT	'Total Unknown' RowType,
	SUM(CASE WHEN TradeAnalyzerScore = 'U' THEN 1 ELSE 0 END) TradeAnalyzer,
	SUM(CASE WHEN InventoryPlanningScore = 'U' THEN 1 ELSE 0 END) InventoryPlanning,
	SUM(CASE WHEN OverallScore = 'U' THEN 1 ELSE 0 END) Overall
FROM	#Detail

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @Results

DROP TABLE #Detail

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO

GRANT EXECUTE, VIEW DEFINITION on [dbo].[StoreUsageMetric#Summary] to [StoredProcedureUser]
GO

EXEC [dbo].[StoreUsageMetric#Summary]
