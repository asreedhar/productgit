
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Dashboard].[Search#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Dashboard].[Search#Fetch]
GO

CREATE PROCEDURE Dashboard.Search#Fetch
	@Date DATETIME
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Dashboard.Search#Fetch.sql,v 1.6 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return a per-dealer daily report on usage in the search engine (FLUSAN).
 * 
 * Notes
 * -----
 * 
 * This procedure is expected to run only once daily.  It is used to
 * populate a fact table in the dashboard database.
 * 
 * Parameters
 * ----------
 * 
 * @Date - The date beyond which the procedure is expected to return data.
 * 
 * Exceptions
 * ----------
 * 
 * NONE
 *
 * History
 * ----------
 * 
 * SBW	04/20/2009	First revision (CVS)
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @Today DATETIME

SET @Today = DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()),0)

SET @Date = DATEADD(DAY, DATEDIFF(DAY, 0, @Date),0)

SELECT	P.Date,
	P.DealerID,
	P.[1] AS InternetCount,
	P.[2] AS InGroupCount,
	P.[3] AS LiveCount
FROM
(
	SELECT	[Date]=DATEADD(DAY, DATEDIFF(DAY, 0, L.DateCreated), 0),
		[DealerID]=L.BusinessUnitID,
		[ChannelID]=L.ChannelID,
		[ID]=FLUSAN_EventLogID
	FROM	[IMT].dbo.FLUSAN_EventLog L WITH (NOLOCK)
	JOIN	[IMT].dbo.Member M WITH (NOLOCK) ON M.MemberID = L.MemberID
	JOIN	[IMT].dbo.BusinessUnit B WITH (NOLOCK) ON B.BusinessUnitID = L.BusinessUnitID
	WHERE	DATEADD(DAY, DATEDIFF(DAY, 0, L.DateCreated), 0) > @Date
	AND	DATEADD(DAY, DATEDIFF(DAY, 0, L.DateCreated), 0) < @Today
	AND	M.MemberType = 2
	AND	M.Login NOT LIKE '%keynote%'
	AND	B.BusinessUnitTypeID = 4
	AND	B.BusinessUnitID NOT IN (101590,101619,101620,101621) -- WINDY CITY STORES
	AND	B.BusinessUnit NOT LIKE '%DO NOT USE%' -- NON DATA
	AND	B.BusinessUnit NOT LIKE '%INVALID STORE%'
	AND	B.BusinessUnit NOT LIKE '%ADD ME%'
	AND	B.BusinessUnit NOT LIKE '%INACTIVE STORE%'
	AND	B.BusinessUnit NOT LIKE '%Duplicate Store%'
	AND	B.BusinessUnit NOT LIKE '%Invalid Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Invalid Client%'
	AND	B.BusinessUnit NOT LIKE '%Dealer Test%'
	AND	B.BusinessUnit NOT LIKE '%DONT USE%'
	AND	B.BusinessUnit NOT LIKE '%Duplicate Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Test Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Testing Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Testing Dealer%'
) T
PIVOT
(
	COUNT(ID) FOR ChannelID IN ([1], [2], [3])
) P

GO

GRANT EXECUTE, VIEW DEFINITION on Dashboard.Search#Fetch to [DashboardUser]
GO
