
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Dashboard].[Pricing#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Dashboard].[Pricing#Fetch]
GO

CREATE PROCEDURE Dashboard.Pricing#Fetch
	@Date DATETIME
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Dashboard.Pricing#Fetch.sql,v 1.6 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return a per-dealer daily report on usage in the pricing tool (PING II).
 * 
 * Notes
 * -----
 * 
 * This procedure is expected to run only once daily.  It is used to
 * populate a fact table in the dashboard database.
 * 
 * Parameters
 * ----------
 * 
 * @Date - The date beyond which the procedure is expected to return data.
 * 
 * Exceptions
 * ----------
 * 
 * NONE
 *
 * History
 * ----------
 * 
 * SBW	04/20/2009	First revision (CVS)
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @Today DATETIME

SET @Today = DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()),0)

SET @Date = DATEADD(DAY, DATEDIFF(DAY, 0, @Date),0)

SELECT	[Date], [DealerID], P.[3] AS [PriceReview], P.[4] AS [PriceChange]
FROM
(
	SELECT	DISTINCT
		[Date]=DATEADD(DAY, DATEDIFF(DAY, 0, H.Created), 0),
		[DealerID]=H.OwnerEntityID,
		[InventoryID]=H.VehicleEntityID,
		[ActionID]=VehicleMarketHistoryEntryTypeID
	FROM	[Market].Pricing.VehicleMarketHistory H WITH (NOLOCK)
	JOIN	[IMT].dbo.BusinessUnit B WITH (NOLOCK) ON B.BusinessUnitID = H.OwnerEntityID
	WHERE	DATEADD(DAY, DATEDIFF(DAY, 0, H.Created), 0) > @Date
	AND	DATEADD(DAY, DATEDIFF(DAY, 0, H.Created), 0) < @Today
	AND	H.OwnerEntityTypeID = 1
	AND	H.VehicleEntityTypeID = 1
	AND	B.BusinessUnitTypeID = 4
	AND	B.BusinessUnitID NOT IN (101590,101619,101620,101621) -- WINDY CITY STORES
	AND	B.BusinessUnit NOT LIKE '%DO NOT USE%' -- NON DATA
	AND	B.BusinessUnit NOT LIKE '%INVALID STORE%'
	AND	B.BusinessUnit NOT LIKE '%ADD ME%'
	AND	B.BusinessUnit NOT LIKE '%INACTIVE STORE%'
	AND	B.BusinessUnit NOT LIKE '%Duplicate Store%'
	AND	B.BusinessUnit NOT LIKE '%Invalid Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Invalid Client%'
	AND	B.BusinessUnit NOT LIKE '%Dealer Test%'
	AND	B.BusinessUnit NOT LIKE '%DONT USE%'
	AND	B.BusinessUnit NOT LIKE '%Duplicate Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Test Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Testing Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Testing Dealer%'
) X
PIVOT
(
	COUNT(InventoryID) FOR ActionID IN ([3], [4])
) P

GO

GRANT EXECUTE, VIEW DEFINITION on Dashboard.Pricing#Fetch to [DashboardUser]
GO
