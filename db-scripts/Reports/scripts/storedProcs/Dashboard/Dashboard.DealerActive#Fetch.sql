
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Dashboard].[DealerActive#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Dashboard].[DealerActive#Fetch]
GO

CREATE PROCEDURE Dashboard.DealerActive#Fetch
	-- no arguments
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Dashboard.DealerActive#Fetch.sql,v 1.5 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return dealers and their status (active, inactive) in the system.
 * 
 * Notes
 * -----
 * 
 * This procedure is expected to run only once daily.  It is used to
 * maintain a slowly changing dimension (historic) in the dashboard
 * database.
 * 
 * Parameters
 * ----------
 * 
 * NONE
 * 
 * Exceptions
 * ----------
 * 
 * NONE
 *
 * History
 * ----------
 * 
 * SBW	04/20/2009	First revision (CVS)
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON;

SELECT	DealerID=B.BusinessUnitID,
	Active=CAST(B.Active AS BIT)
FROM	[IMT].dbo.BusinessUnit B WITH (NOLOCK)
WHERE	B.BusinessUnitTypeID = 4
AND	B.BusinessUnitID NOT IN (101590,101619,101620,101621) -- WINDY CITY STORES
AND	B.BusinessUnit NOT LIKE '%DO NOT USE%'
AND	B.BusinessUnit NOT LIKE '%INVALID STORE%'
AND	B.BusinessUnit NOT LIKE '%ADD ME%'
AND	B.BusinessUnit NOT LIKE '%INACTIVE STORE%'
AND	B.BusinessUnit NOT LIKE '%Duplicate Store%'
AND	B.BusinessUnit NOT LIKE '%Invalid Dealer%'
AND	B.BusinessUnit NOT LIKE '%Invalid Client%'
AND	B.BusinessUnit NOT LIKE '%Dealer Test%'
AND	B.BusinessUnit NOT LIKE '%DONT USE%'
AND	B.BusinessUnit NOT LIKE '%Duplicate Dealer%'
AND	B.BusinessUnit NOT LIKE '%Test Dealer%'
AND	B.BusinessUnit NOT LIKE '%Testing Dealer%'
AND	B.BusinessUnit NOT LIKE '%Testing Dealer%'

GO

GRANT EXECUTE, VIEW DEFINITION on Dashboard.DealerActive#Fetch to [DashboardUser]
GO
