
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Dashboard].[PriceChange#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Dashboard].[PriceChange#Fetch]
GO

CREATE PROCEDURE Dashboard.PriceChange#Fetch
	@Date DATETIME
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Dashboard.PriceChange#Fetch.sql,v 1.6 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return a per-dealer daily price-change report for the period after the supplied date.
 * 
 * Notes
 * -----
 * 
 * This procedure is expected to run only once daily.  It is used to
 * populate a fact table in the dashboard database.
 * 
 * Parameters
 * ----------
 * 
 * @Date - The date beyond which the procedure is expected to return data.
 * 
 * Exceptions
 * ----------
 * 
 * NONE
 *
 * History
 * ----------
 * 
 * SBW	04/20/2009	First revision (CVS)
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @Today DATETIME

SET @Today = DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()),0)

SET @Date = DATEADD(DAY, DATEDIFF(DAY, 0, @Date),0)

SELECT	P.[Date],
	P.[DealerID],
	P.[1] AS [RS1],
	P.[2] AS [RS2],
	P.[3] AS [RS3],
	P.[4] AS [RS4]
FROM
(
	SELECT	DISTINCT
		[Date]=DATEADD(DAY, DATEDIFF(DAY, 0, E.Created), 0),
		[DealerID]=I.BusinessUnitID,
		[InventoryID]=I.InventoryID,
		[RepriceSourceID]=E.RepriceSourceID
	FROM	[IMT].dbo.AIP_Event E WITH (NOLOCK)
	JOIN	[IMT].dbo.Inventory I WITH (NOLOCK) ON I.InventoryID = E.InventoryID
	JOIN	[IMT].dbo.BusinessUnit B WITH (NOLOCK) ON B.BusinessUnitID = I.BusinessUnitID
	WHERE	DATEADD(DAY, DATEDIFF(DAY, 0, E.Created), 0) > @Date
	AND	DATEADD(DAY, DATEDIFF(DAY, 0, E.Created), 0) < @Today
	AND	E.AIP_EventTypeID = 5
	AND	CAST(CAST(E.Notes2 AS FLOAT) AS INT) != E.Value
	AND	B.BusinessUnitTypeID = 4
	AND	B.BusinessUnitID NOT IN (101590,101619,101620,101621) -- WINDY CITY STORES
	AND	B.BusinessUnit NOT LIKE '%DO NOT USE%' -- NON DATA
	AND	B.BusinessUnit NOT LIKE '%INVALID STORE%'
	AND	B.BusinessUnit NOT LIKE '%ADD ME%'
	AND	B.BusinessUnit NOT LIKE '%INACTIVE STORE%'
	AND	B.BusinessUnit NOT LIKE '%Duplicate Store%'
	AND	B.BusinessUnit NOT LIKE '%Invalid Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Invalid Client%'
	AND	B.BusinessUnit NOT LIKE '%Dealer Test%'
	AND	B.BusinessUnit NOT LIKE '%DONT USE%'
	AND	B.BusinessUnit NOT LIKE '%Duplicate Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Test Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Testing Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Testing Dealer%'
) X
PIVOT
(
	COUNT(InventoryID) FOR RepriceSourceID IN ([1], [2], [3], [4])
) P

GO

GRANT EXECUTE, VIEW DEFINITION on Dashboard.PriceChange#Fetch to [DashboardUser]
GO
