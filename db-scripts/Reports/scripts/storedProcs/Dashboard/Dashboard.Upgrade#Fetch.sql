 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Dashboard].[Upgrade#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Dashboard].[Upgrade#Fetch]
GO

CREATE PROCEDURE Dashboard.Upgrade#Fetch
	-- no arguments
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Dashboard.Upgrade#Fetch.sql,v 1.5 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Fetch a list of upgrades.
 * 
 * Notes
 * -----
 * 
 * This procedure is expected to run only once daily.  It is used to
 * maintain a dimension in the dashboard database (insert new rows
 * and update changes to upgrades).
 * 
 * Parameters
 * ----------
 * 
 * NONE
 * 
 * Exceptions
 * ----------
 * 
 * NONE
 *
 * History
 * ----------
 * 
 * SBW	04/20/2009	First revision (CVS)
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

SELECT	DealerUpgradeCD,
	DealerUpgradeDESC
FROM	[IMT].dbo.lu_DealerUpgrade

GO

GRANT EXECUTE, VIEW DEFINITION on Dashboard.Upgrade#Fetch to [DashboardUser]
GO
