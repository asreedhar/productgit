 
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Dashboard].[DealerActive#Import]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Dashboard].[DealerActive#Import]
GO

CREATE PROCEDURE Dashboard.DealerActive#Import
	-- no arguments
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Dashboard.DealerActive#Import.sql,v 1.5 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return a list of dealers and their status (active, inactive) over time.
 * 
 * Notes
 * -----
 * 
 * This procedure is expected to run only once (EVER) in order to initialize
 * a slowly changing dimension table in the dashboard database.
 * 
 * Parameters
 * ----------
 * 
 * NONE
 * 
 * Exceptions
 * ----------
 * 
 * NONE
 *
 * History
 * ----------
 * 
 * SBW	04/20/2009	First revision (CVS)
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON;

WITH InventoryLoadDates (DealerID, StartDate, EndDate) AS (
	SELECT	BusinessUnitID,
		StartDate=MIN(DATEADD(DAY, DATEDIFF(DAY, 0, DMSReferenceDt), 0)),
		EndDate=MAX(DATEADD(DAY, DATEDIFF(DAY, 0, DMSReferenceDt), 0))
	FROM	[IMT].dbo.Inventory I WITH (NOLOCK)
	WHERE	BusinessUnitID NOT IN (101590,101619,101620,101621) -- WINDY CITY STORES
	GROUP
	BY	BusinessUnitID
),
MemberDates (DealerID, StartDate, EndDate) AS (
	SELECT	MA.BusinessUnitID,
		StartDate=MIN(DATEADD(DAY, DATEDIFF(DAY, 0, M.CreateDate), 0)),
		EndDate=MAX(DATEADD(DAY, DATEDIFF(DAY, 0, AE.CreateTimestamp), 0))
	FROM	[IMT].dbo.Member m WITH (NOLOCK)
	JOIN	[IMT].dbo.MemberAccess ma WITH (NOLOCK) on ma.MemberID = m.MemberID
	LEFT
	JOIN	[IMT].dbo.ApplicationEvent AE WITH (NOLOCK) ON AE.MemberID = MA.MemberID AND AE.CreateTimestamp >= M.CreateDate
	WHERE	MA.BusinessUnitID NOT IN (101590,101619,101620,101621) -- WINDY CITY STORES
	GROUP
	BY	MA.BusinessUnitID
),
DealerDates (DealerID, Name, State, Active, StartDate, EndDate) AS (
	SELECT	DealerID=B.BusinessUnitID,
		Name=B.BusinessUnit,
		State=B.State,
		Active=CAST(B.Active AS BIT),
		StartDate=COALESCE(I.StartDate,P.GoLiveDate,M.StartDate,'2000-01-01'),
		EndDate=CASE WHEN B.Active = 1 THEN NULL ELSE COALESCE(I.EndDate,M.EndDate,I.StartDate,P.GoLiveDate,M.StartDate,'2000-01-01') END
	FROM	[IMT].dbo.BusinessUnit B WITH (NOLOCK)
	LEFT
	JOIN	[IMT].dbo.DealerPreference P WITH (NOLOCK)
		ON	P.BusinessUnitID = B.BusinessUnitID
		AND	P.GoLiveDate > '2001-09-11'
	LEFT
	JOIN	InventoryLoadDates I ON I.DealerID = B.BusinessUnitID
	LEFT
	JOIN	MemberDates M ON M.DealerID = B.BusinessUnitID
	WHERE	B.BusinessUnitTypeID = 4
	AND	B.BusinessUnitID NOT IN (101590,101619,101620,101621) -- WINDY CITY STORES
	AND	B.BusinessUnit NOT LIKE '%DO NOT USE%'
	AND	B.BusinessUnit NOT LIKE '%INVALID STORE%'
	AND	B.BusinessUnit NOT LIKE '%ADD ME%'
	AND	B.BusinessUnit NOT LIKE '%INACTIVE STORE%'
	AND	B.BusinessUnit NOT LIKE '%Duplicate Store%'
	AND	B.BusinessUnit NOT LIKE '%Invalid Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Invalid Client%'
	AND	B.BusinessUnit NOT LIKE '%Dealer Test%'
	AND	B.BusinessUnit NOT LIKE '%DONT USE%'
	AND	B.BusinessUnit NOT LIKE '%Duplicate Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Test Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Testing Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Testing Dealer%'
)

SELECT	DealerID,
	Active=CAST(1 AS BIT),
	StartDate,
	EndDate
FROM	DealerDates
WHERE	Active = 0
AND	EndDate > StartDate

UNION ALL -- inactive records for the inactive dealers

SELECT	DealerID,
	Active=CAST(Active AS BIT),
	StartDate=EndDate,
	EndDate=CAST(NULL AS SMALLDATETIME)
FROM	DealerDates
WHERE	Active = 0

UNION ALL -- active dealers

SELECT	DealerID,
	Active=CAST(Active AS BIT),
	StartDate,
	EndDate=CAST(NULL AS SMALLDATETIME)
FROM	DealerDates
WHERE	Active = 1

GO

GRANT EXECUTE, VIEW DEFINITION on Dashboard.DealerActive#Import to [DashboardUser]
GO
