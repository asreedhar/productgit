  
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Dashboard].[DealerUpgrade#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Dashboard].[DealerUpgrade#Fetch]
GO

CREATE PROCEDURE Dashboard.DealerUpgrade#Fetch
	-- no arguments
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Dashboard.DealerUpgrade#Fetch.sql,v 1.5 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return dealers and their upgrades.
 * 
 * Notes
 * -----
 * 
 * This procedure is expected to run only once daily.  It is used to
 * maintain a slowly changing dimension (historic) in the dashboard
 * database.
 * 
 * Parameters
 * ----------
 * 
 * NONE
 * 
 * Exceptions
 * ----------
 * 
 * NONE
 *
 * History
 * ----------
 * 
 * SBW	04/20/2009	First revision (CVS)
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON;

WITH InventoryLoadDates (DealerID, StartDate, EndDate) AS (
	SELECT	BusinessUnitID,
		StartDate=MIN(DATEADD(DAY, DATEDIFF(DAY, 0, DMSReferenceDt), 0)),
		EndDate=MAX(DATEADD(DAY, DATEDIFF(DAY, 0, DMSReferenceDt), 0))
	FROM	[IMT].dbo.Inventory I WITH (NOLOCK)
	WHERE	BusinessUnitID NOT IN (101590,101619,101620,101621) -- WINDY CITY STORES
	GROUP
	BY	BusinessUnitID
),
MemberDates (DealerID, StartDate, EndDate) AS (
	SELECT	MA.BusinessUnitID,
		StartDate=MIN(DATEADD(DAY, DATEDIFF(DAY, 0, M.CreateDate), 0)),
		EndDate=MAX(DATEADD(DAY, DATEDIFF(DAY, 0, AE.CreateTimestamp), 0))
	FROM	[IMT].dbo.Member m WITH (NOLOCK)
	JOIN	[IMT].dbo.MemberAccess ma WITH (NOLOCK) on ma.MemberID = m.MemberID
	LEFT
	JOIN	[IMT].dbo.ApplicationEvent AE ON AE.MemberID = MA.MemberID AND AE.CreateTimestamp >= M.CreateDate
	WHERE	MA.BusinessUnitID NOT IN (101590,101619,101620,101621) -- WINDY CITY STORES
	GROUP
	BY	MA.BusinessUnitID
),
DealerDates (DealerID, Active, StartDate, EndDate) AS (
	SELECT	DealerID=B.BusinessUnitID,
		Active=CAST(B.Active AS BIT),
		StartDate=COALESCE(I.StartDate,P.GoLiveDate,M.StartDate,'2000-01-01'),
		EndDate=CASE WHEN B.Active = 1 THEN NULL ELSE COALESCE(I.EndDate,M.EndDate,I.StartDate,P.GoLiveDate,M.StartDate,'2000-01-01') END
	FROM	[IMT].dbo.BusinessUnit B WITH (NOLOCK)
	LEFT
	JOIN	[IMT].dbo.DealerPreference P WITH (NOLOCK)
		ON	P.BusinessUnitID = B.BusinessUnitID
		AND	P.GoLiveDate > '2001-09-11'
	LEFT
	JOIN	InventoryLoadDates I ON I.DealerID = B.BusinessUnitID
	LEFT
	JOIN	MemberDates M ON M.DealerID = B.BusinessUnitID
	WHERE	B.BusinessUnitTypeID = 4
	AND	B.BusinessUnitID NOT IN (101590,101619,101620,101621) -- WINDY CITY STORES
	AND	B.BusinessUnit NOT LIKE '%DO NOT USE%'
	AND	B.BusinessUnit NOT LIKE '%INVALID STORE%'
	AND	B.BusinessUnit NOT LIKE '%ADD ME%'
	AND	B.BusinessUnit NOT LIKE '%INACTIVE STORE%'
	AND	B.BusinessUnit NOT LIKE '%Duplicate Store%'
	AND	B.BusinessUnit NOT LIKE '%Invalid Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Invalid Client%'
	AND	B.BusinessUnit NOT LIKE '%Dealer Test%'
	AND	B.BusinessUnit NOT LIKE '%DONT USE%'
	AND	B.BusinessUnit NOT LIKE '%Duplicate Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Test Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Testing Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Testing Dealer%'
),
UpgradeDates (DealerID, UpgradeID, Active, StartDate, EndDate) AS (
	SELECT	BusinessUnitID,
		DealerUpgradeCD,
		EffectiveDateActive,
		CASE	WHEN StartDate IS NOT NULL AND (EndDate IS NULL OR EndDate >= StartDate) THEN DATEADD(DAY, DATEDIFF(DAY, 0, StartDate), 0)
			ELSE NULL
		END,
		CASE	WHEN EndDate IS NOT NULL AND StartDate IS NOT NULL AND EndDate >= StartDate THEN DATEADD(DAY, DATEDIFF(DAY, 0, EndDate), 0)
			WHEN EndDate IS NULL AND Active = 0 THEN DATEADD(DAY, DATEDIFF(DAY, 0, StartDate), 0)
			ELSE NULL
		END
	FROM	[IMT].dbo.DealerUpgrade
	WHERE	CASE	WHEN (Active = 0 AND ((StartDate IS NULL AND EndDate IS NULL) OR (DATEDIFF(DD,StartDate,EndDate) = 0))) THEN 0
			WHEN (Active = 1 AND DATEDIFF(DD,StartDate,EndDate) = 0) THEN 0
			ELSE 1
		END = 1
)
SELECT	D.DealerID,
	U.UpgradeID,
	Active=CAST(CASE WHEN (D.Active = 1 AND U.Active = 1) THEN 1 ELSE 0 END AS BIT)
FROM	DealerDates D
JOIN	UpgradeDates U ON U.DealerID = D.DealerID
WHERE	(D.Active = 1 AND U.Active = 1)
OR	(
		(D.Active = 0 OR U.Active = 0) AND
		/* start date */ CASE
			WHEN U.StartDate IS NULL THEN D.StartDate
			WHEN U.StartDate < D.StartDate THEN D.StartDate
			WHEN U.StartDate > D.EndDate THEN D.EndDate
			ELSE U.StartDate
		END
	<>	/* end date */ CASE
			WHEN U.EndDate IS NULL		THEN CASE WHEN U.StartDate < D.StartDate THEN COALESCE(D.EndDate,D.StartDate) ELSE COALESCE(D.EndDate,U.StartDate,D.StartDate) END
			WHEN U.EndDate < D.StartDate	THEN CASE WHEN U.StartDate < D.StartDate THEN COALESCE(D.EndDate,D.StartDate) ELSE COALESCE(D.EndDate,U.StartDate,D.StartDate) END
			WHEN U.EndDate > D.EndDate	THEN CASE WHEN U.StartDate < D.StartDate THEN COALESCE(D.EndDate,D.StartDate) ELSE COALESCE(D.EndDate,U.StartDate,D.StartDate) END
			ELSE U.EndDate
		END
	)

GO

GRANT EXECUTE, VIEW DEFINITION on Dashboard.DealerUpgrade#Fetch to [DashboardUser]
GO
