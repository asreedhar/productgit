
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Dashboard].[Appraisal#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Dashboard].[Appraisal#Fetch]
GO

CREATE PROCEDURE Dashboard.Appraisal#Fetch
	@Date DATETIME
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Dashboard.Appraisal#Fetch.sql,v 1.6 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Fetch a per-dealer count of appraisals after the argument date and before
 * today-midnight.
 * 
 * Notes
 * -----
 * 
 * This procedure is expected to run only once daily.  It is used to
 * populate a fact table in the dashboard database.  The data is used
 * for charting purposes.
 * 
 * Parameters
 * ----------
 * 
 * @Date - The date beyond which the procedure is expected to return data.
 * 
 * Exceptions
 * ----------
 * 
 * NONE
 *
 * History
 * ----------
 * 
 * SBW	04/20/2009	First revision (CVS)
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @Today DATETIME

SET @Today = DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()),0)

SET @Date = DATEADD(DAY, DATEDIFF(DAY, 0, @Date),0)

SELECT	[Date]=DATEADD(DAY, DATEDIFF(DAY, 0, A.DateCreated), 0),
	[DealerID]=A.BusinessUnitID,
	[Count]=COUNT(DISTINCT(AppraisalID))
FROM	[IMT].dbo.Appraisals A WITH (NOLOCK)
JOIN	[IMT].dbo.BusinessUnit B WITH (NOLOCK) ON B.BusinessUnitID = A.BusinessUnitID
LEFT
JOIN	[IMT].dbo.DealerPreference P WITH (NOLOCK)
	ON	P.BusinessUnitID = B.BusinessUnitID
	AND	P.GoLiveDate > '2001-09-11'
WHERE	DATEADD(DAY, DATEDIFF(DAY, 0, A.DateCreated), 0) > @Date
AND	DATEADD(DAY, DATEDIFF(DAY, 0, A.DateCreated), 0) < @Today
AND	B.BusinessUnitTypeID = 4
AND	(B.Active = 0 OR P.GoLiveDate IS NOT NULL)
AND	B.BusinessUnitID NOT IN (101590,101619,101620,101621) -- WINDY CITY STORES
AND	B.BusinessUnit NOT LIKE '%DO NOT USE%' -- NON DATA
AND	B.BusinessUnit NOT LIKE '%INVALID STORE%'
AND	B.BusinessUnit NOT LIKE '%ADD ME%'
AND	B.BusinessUnit NOT LIKE '%INACTIVE STORE%'
AND	B.BusinessUnit NOT LIKE '%Duplicate Store%'
AND	B.BusinessUnit NOT LIKE '%Invalid Dealer%'
AND	B.BusinessUnit NOT LIKE '%Invalid Client%'
AND	B.BusinessUnit NOT LIKE '%Dealer Test%'
AND	B.BusinessUnit NOT LIKE '%DONT USE%'
AND	B.BusinessUnit NOT LIKE '%Duplicate Dealer%'
AND	B.BusinessUnit NOT LIKE '%Test Dealer%'
AND	B.BusinessUnit NOT LIKE '%Testing Dealer%'
AND	B.BusinessUnit NOT LIKE '%Testing Dealer%'
GROUP
BY	DATEADD(DAY, DATEDIFF(DAY, 0, A.DateCreated), 0),
	A.BusinessUnitID
	
GO

GRANT EXECUTE, VIEW DEFINITION on Dashboard.Appraisal#Fetch to [DashboardUser]
GO
