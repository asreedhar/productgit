
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Dashboard].[Planning#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Dashboard].[Planning#Fetch]
GO

CREATE PROCEDURE Dashboard.Planning#Fetch
	@Date DATETIME
AS

/* --------------------------------------------------------------------
 * 
 * $Id: Dashboard.Planning#Fetch.sql,v 1.6 2010/02/10 21:54:22 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return a per-dealer report on daily planning activity after the supplied
 * date.
 * 
 * Notes
 * -----
 * 
 * This procedure is expected to run only once daily.  It is used to
 * populate a fact table in the dashboard database.  The data is used
 * for charting purposes.
 * 
 * Parameters
 * ----------
 * 
 * @Date - The date beyond which the procedure is expected to return data.
 * 
 * Exceptions
 * ----------
 * 
 * NONE
 *
 * History
 * ----------
 * 
 * SBW	04/20/2009	First revision (CVS)
 * 
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @Today DATETIME

SET @Today = DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()),0)

SET @Date = DATEADD(DAY, DATEDIFF(DAY, 0, @Date),0)

SELECT	P.[Date],
	P.[DealerID],
	P.[1] AS [PT01],
	P.[2] AS [PT02],
	P.[3] AS [PT03],
	P.[4] AS [PT04],
	P.[5] AS [PT05],
	P.[6] AS [PT06],
	P.[7] AS [PT07],
	P.[8] AS [PT08],
	P.[9] AS [PT09],
	P.[10] AS [PT10],
	P.[11] AS [PT11],
	P.[12] AS [PT12],
	P.[13] AS [PT13],
	P.[14] AS [PT14],
	P.[15] AS [PT15],
	P.[16] AS [PT16],
	P.[17] AS [PT17],
	P.[18] AS [PT18]
FROM
(
	SELECT	[Date]=DATEADD(DAY, DATEDIFF(DAY, 0, E.Created), 0),
		[DealerID]=I.BusinessUnitID,
		[PlanTypeID]=E.AIP_EventTypeID,
		[PlanID]=E.AIP_EventID
	FROM	[IMT].dbo.AIP_Event E WITH (NOLOCK)
	JOIN	[IMT].dbo.Inventory I WITH (NOLOCK) ON I.InventoryID = E.InventoryID
	JOIN	[IMT].dbo.BusinessUnit B WITH (NOLOCK) ON B.BusinessUnitID = I.BusinessUnitID
	WHERE	DATEADD(DAY, DATEDIFF(DAY, 0, E.Created), 0) > @Date
	AND	DATEADD(DAY, DATEDIFF(DAY, 0, E.Created), 0) < @Today
	AND	B.BusinessUnitTypeID = 4
	AND	B.BusinessUnitID NOT IN (101590,101619,101620,101621) -- WINDY CITY STORES
	AND	B.BusinessUnit NOT LIKE '%DO NOT USE%' -- NON DATA
	AND	B.BusinessUnit NOT LIKE '%INVALID STORE%'
	AND	B.BusinessUnit NOT LIKE '%ADD ME%'
	AND	B.BusinessUnit NOT LIKE '%INACTIVE STORE%'
	AND	B.BusinessUnit NOT LIKE '%Duplicate Store%'
	AND	B.BusinessUnit NOT LIKE '%Invalid Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Invalid Client%'
	AND	B.BusinessUnit NOT LIKE '%Dealer Test%'
	AND	B.BusinessUnit NOT LIKE '%DONT USE%'
	AND	B.BusinessUnit NOT LIKE '%Duplicate Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Test Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Testing Dealer%'
	AND	B.BusinessUnit NOT LIKE '%Testing Dealer%'
) X
PIVOT
(
	COUNT(PlanID) FOR PlanTypeID IN (
		[1], [2], [3], [4], [5], [6], [7], [8], [9], [10],
		[11], [12], [13], [14], [15], [16], [17], [18]
	)
) P

GO

GRANT EXECUTE, VIEW DEFINITION on Dashboard.Planning#Fetch to [DashboardUser]
GO
