
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_GrossProfitByDaysInStock#SummarySales]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_GrossProfitByDaysInStock#SummarySales] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_GrossProfitByDaysInStock#SummarySales] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_GrossProfitByDaysInStock#SummarySales]
	@PeriodID INT,
	@DealerGroupID INT,
	@MemberID INT
AS

/******************************************************************************************************************
*	Routine:	[dbo].[rc_GrossProfitByDaysInStock#SummarySales]
*	Purpose:	Driver routine for Gross Profit by Days in Stock Summary Sales Table
*
*	History
*	2/26/2010	SVU	Initial Creation
*	05/20/2010	MAK	Update to work by BusinessUnit.
*
*******************************************************************************************************************/
SET NOCOUNT ON ;

DECLARE @DealerGroupName VARCHAR(50)

SELECT @DealerGroupName =BusinessUnit
FROM	IMT.dbo.BusinessUnit
WHERE	BusinessUnitID=@DealerGroupID

DECLARE @MemberAccessTable TABLE (BusinessUnitID INT)
                
;
WITH    MemberBusinessUnitSet
          AS (SELECT    I.BusinessUnitID
              FROM      [IMT].dbo.MemberBusinessUnitSet S
              JOIN      [IMT].dbo.MemberBusinessUnitSetItem I ON S.MemberBusinessUnitSetID = I.MemberBusinessUnitSetID
              WHERE     S.MemberID = @MemberID
                        AND S.MemberBusinessUnitSetTypeID = 1 --Command Center Dealer Group Set
                        
             )
        INSERT  INTO @MemberAccessTable
                ( BusinessUnitID)
                SELECT  DISTINCT  MBUS.BusinessUnitID
                FROM    dbo.MemberAccess(@DealerGroupID, @MemberID) MA
                JOIN    MemberBusinessUnitSet MBUS ON MA.BusinessUnitID = MBUS.BusinessUnitID

SELECT  @DealerGroupName AS DealerGroupName,
		@DealerGroupID AS DealerGroupID,
		BU.BusinessUnit,
		BU.BusinessUnitID,
		COUNT(*) RetailUnitsSold,
		SUM(S.FrontEndGross) SumGrossProfit,
		SUM(I.DaysToSale) SUMDaysToSale,
		SUM(S.FrontEndGross + S.BackEndGross) Profit
FROM    [FLDW].dbo.Inventory I
JOIN    [FLDW].dbo.VehicleSale S ON I.InventoryID = S.InventoryID
JOIN    [FLDW].dbo.Vehicle V ON I.VehicleID = V.VehicleID
                                      AND I.BusinessUnitID = V.BusinessUnitID
JOIN    [IMT].dbo.MakeModelGrouping MakeModel ON V.MakeModelGroupingID = MakeModel.MakeModelGroupingID
JOIN    [IMT].dbo.Segment ON MakeModel.SegmentID = Segment.SegmentID
JOIN    @MemberAccessTable MAT ON MAT.BusinessUnitID = I.BusinessUnitID
JOIN	IMT.dbo.BusinessUnit BU ON BU.BusinessUnitID = I.BusinessUnitID

CROSS JOIN [FLDW].dbo.Period_D Period
WHERE   I.InventoryType = 2
	AND Period.PeriodID = @PeriodID
	AND S.DealDate BETWEEN Period.BeginDate AND Period.EndDate
	AND (S.SaleDescription = 'R')   /*OR (S.SaleDescription = 'W' AND I.DaysToSale >= 30))*/
GROUP BY BU.BusinessUnit,
		BU.BusinessUnitID	

GO

GO