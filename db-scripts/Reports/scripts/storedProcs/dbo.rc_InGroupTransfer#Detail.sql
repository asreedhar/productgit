
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_InGroupTransfer#Detail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[rc_InGroupTransfer#Detail]
GO

CREATE PROCEDURE [dbo].[rc_InGroupTransfer#Detail]
	@DealerGroupID      INT,
	@MemberID           INT,
	@PeriodID           INT,
	@NowBusinessUnitID  INT,
    @WasBusinessUnitID  INT
AS

SET NOCOUNT ON

DECLARE @DateFrom DATETIME, @DateUpTo DATETIME

SELECT  @DateFrom = BeginDate,
        @DateUpTo = EndDate
FROM    FLDW.dbo.Period_D
WHERE   PeriodID = @PeriodID

SELECT  DISTINCT
        DstDealerID =   BNow.BusinessUnitID,
        DstDealer   =   BNow.BusinessUnit,
        SrcDealerID =   BWas.BusinessUnitID,
        SrcDealer   =   BWas.BusinessUnit,
        -- vehicle information
        V.VehicleID,
        V.Vin,
        Description = CONVERT(VARCHAR,C.ModelYear) + ' ' + M.Make + ' ' + L.Line + ' ' + C.Series,
        -- src
        SrcReceivedDate   = IWas.InventoryReceivedDate,
        SrcStockNumber    = IWas.StockNumber,
        SrcUnitCost       = NULLIF(IWas.UnitCost,0),
        SrcListPrice      = NULLIF(IWas.ListPrice,0),
        SrcSalePrice      = SWas.SalePrice,
        SrcSaleDate       = SWas.DealDate,
        SrcSaleType       = SWas.SaleDescription,
        SrcFrontEndGross  = CONVERT(INT,SWas.FrontEndGross),
        SrcDaysInStock    = DATEDIFF(DD,IWas.InventoryReceivedDate,IWas.DeleteDt),
        -- dst
        DstReceivedDate   = INow.InventoryReceivedDate,
        DstStockNumber    = INow.StockNumber,
        DstUnitCost       = NULLIF(INow.UnitCost,0),
        DstListPrice      = NULLIF(INow.ListPrice,0),
        DstSalePrice      = SNow.SalePrice,
        DstSaleDate       = SNow.DealDate,
        DstSaleType       = SNow.SaleDescription,
        DstFrontEndGross  = CONVERT(INT,SNow.FrontEndGross),
        DstDaysInStock    = DATEDIFF(DD,INow.InventoryReceivedDate,COALESCE(INow.DeleteDt,GETDATE()))
        
FROM    IMT.Transfers.Transfer R
JOIN    IMT.dbo.BusinessUnit BNow ON R.DstDealerID = BNow.BusinessUnitID
JOIN    IMT.dbo.BusinessUnit BWas ON R.SrcDealerID = BWas.BusinessUnitID
JOIN    dbo.MemberAccess(@DealerGroupID, @MemberID) D ON R.DstDealerID = D.BusinessUnitID

JOIN    IMT.dbo.tbl_Vehicle V ON V.VehicleID = R.VehicleID

JOIN    IMT.dbo.Inventory INow ON INow.VehicleID = R.VehicleID AND INow.BusinessUnitID = R.DstDealerID AND INow.InventoryReceivedDate = R.DstDate
JOIN    IMT.dbo.Inventory IWas ON IWas.VehicleID = R.VehicleID AND IWas.BusinessUnitID = R.SrcDealerID

JOIN    IMT.dbo.tbl_VehicleSale SWas ON SWas.InventoryID = IWas.InventoryID

LEFT
JOIN    IMT.dbo.tbl_VehicleSale SNow ON SNow.InventoryID = INow.InventoryID

JOIN    VehicleCatalog.Firstlook.VehicleCatalog C ON C.VehicleCatalogID = V.VehicleCatalogID
JOIN    VehicleCatalog.Firstlook.Line L ON L.LineID = C.LineID
JOIN    VehicleCatalog.Firstlook.Make M ON M.MakeID = L.MakeID

WHERE   R.DstDealerID = COALESCE(@NowBusinessUnitID,R.DstDealerID)
AND     R.SrcDealerID = COALESCE(@WasBusinessUnitID,R.SrcDealerID)
AND     R.DstDealerID <> R.SrcDealerID
AND     R.SrcDate BETWEEN @DateFrom AND @DateUpTo
AND     BNow.Active = 1
AND     BWas.Active = 1
AND     INow.InventoryType = 2
AND     IWas.InventoryType = 2

AND     SWas.DealDate = R.SrcDate -- this makes it correct but *very* slow (sigh)

GO

GRANT EXECUTE ON [dbo].[rc_InGroupTransfer#Detail] TO [StoredProcedureUser]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_InGroupTransfer#Detail_Taken]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[rc_InGroupTransfer#Detail_Taken]
GO

CREATE PROCEDURE [dbo].[rc_InGroupTransfer#Detail_Taken]
	@DealerGroupID      INT,
	@MemberID           INT,
	@PeriodID           INT,
	@NowBusinessUnitID  INT,
    @WasBusinessUnitID  INT
AS

EXEC dbo.rc_InGroupTransfer#Detail
    @DealerGroupID = @DealerGroupID,
    @MemberID = @MemberID,
    @PeriodID = @PeriodID,
    @NowBusinessUnitID = @NowBusinessUnitID,
    @WasBusinessUnitID = @WasBusinessUnitID

GO

GRANT EXECUTE ON [dbo].[rc_InGroupTransfer#Detail_Taken] TO [StoredProcedureUser]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_InGroupTransfer#Detail_Given]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[rc_InGroupTransfer#Detail_Given]
GO

CREATE PROCEDURE [dbo].[rc_InGroupTransfer#Detail_Given]
	@DealerGroupID      INT,
	@MemberID           INT,
	@PeriodID           INT,
	@NowBusinessUnitID  INT,
    @WasBusinessUnitID  INT
AS

EXEC dbo.rc_InGroupTransfer#Detail
    @DealerGroupID = @DealerGroupID,
    @MemberID = @MemberID,
    @PeriodID = @PeriodID,
    @NowBusinessUnitID = @WasBusinessUnitID,
    @WasBusinessUnitID = @NowBusinessUnitID
    
GO

GRANT EXECUTE ON [dbo].[rc_InGroupTransfer#Detail_Given] TO [StoredProcedureUser]
GO
