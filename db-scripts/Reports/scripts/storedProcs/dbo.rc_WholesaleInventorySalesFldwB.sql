
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_WholesaleInventorySalesFldwB]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_WholesaleInventorySalesFldwB] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_WholesaleInventorySalesFldwB] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_WholesaleInventorySalesFldwB]
	@PeriodID INT,
	@DealerID INT
AS

SET NOCOUNT ON

Declare @Buckets Table (
	counter tinyint,
	daysLow int,
	daysHigh int,
	saleDesc varchar(1),
	bucketDesc varchar(20)
)

Insert Into @Buckets (counter, daysLow, daysHigh, saleDesc, bucketDesc) VALUES (0,0,29,'W','Immediate Wholesale')
Insert Into @Buckets (counter, daysLow, daysHigh, saleDesc, bucketDesc) VALUES (1,30,999,'W','Aged Wholesale')
Insert Into @Buckets (counter, daysLow, daysHigh, saleDesc, bucketDesc) VALUES (2,0,999,'W','All Wholesale')

SELECT	B.counter,
		COUNT(counter) DealCount,
		MAX(bucketDesc) BucketDesc,
		B.SaleDesc,
		AVG(VS.SalePrice) AGP,
		AVG(I.DaysToSale) AvgDaysToSale,
		AVG(VS.FrontEndGross) AvgFrontEndGross,
		AVG(I.UnitCost) AvgUnitCost,
		AVG(VS.SalePrice) AvgSalePrice,
		AVG(I.MileageReceived) AvgMileageReceived,
		Cast(SUM(Case When CurrentVehicleLight = 1 Then 1 Else 0 End) as real) / Cast(COUNT(counter) as real) PctRedLights,
		Cast(SUM(Case When CurrentVehicleLight = 2 Then 1 Else 0 End) as real) / Cast(COUNT(counter) as real) PctYellowLights,
		Cast(SUM(Case When CurrentVehicleLight = 3 Then 1 Else 0 End) as real) / Cast(COUNT(counter) as real) PctGreenLights,
		SUM(VS.FrontEndGross) TotalFrontEndGross

FROM	[FLDW].dbo.Inventory I
Join	[FLDW].dbo.VehicleSale VS on I.InventoryID = VS.InventoryID
Join	[FLDW].dbo.Vehicle V on I.VehicleID = V.VehicleID And I.BusinessUnitID = V.BusinessUnitID
Join	@Buckets as B on ((I.DaysToSale between B.daysLow and B.daysHigh) AND B.SaleDesc = VS.SaleDescription)
CROSS JOIN [FLDW].dbo.Period_D P

WHERE	I.BusinessUnitID = @DealerID
And		I.InventoryType = 2
AND		P.PeriodID = @PeriodID
And		VS.DealDate Between P.BeginDate And P.EndDate

Group By B.counter, B.SaleDesc

GO
