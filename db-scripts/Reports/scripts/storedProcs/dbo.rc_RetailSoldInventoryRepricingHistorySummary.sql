
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_RetailSoldInventoryRepricingHistorySummary]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_RetailSoldInventoryRepricingHistorySummary] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_RetailSoldInventoryRepricingHistorySummary] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_RetailSoldInventoryRepricingHistorySummary]
	@PeriodID INT,
	@DealerID INT
AS

SET NOCOUNT ON

DECLARE @Buckets TABLE (
	counter int,
	lowAge int,
	highAge int
)

INSERT INTO @Buckets (counter, lowAge, highAge) VALUES (0 , 21 , 29)
INSERT INTO @Buckets (counter, lowAge, highAge) VALUES (1 , 30 , 39)
INSERT INTO @Buckets (counter, lowAge, highAge) VALUES (2 , 40 , 49)
INSERT INTO @Buckets (counter, lowAge, highAge) VALUES (3 , 50 , 59)
INSERT INTO @Buckets (counter, lowAge, highAge) VALUES (4 , 60 , 999)

CREATE TABLE #SalesSnapshot (
	BusinessUnitID INT,
	InventoryID INT,
	VehicleID INT,
	InventoryReceivedDate SMALLDATETIME,
	DealDate SMALLDATETIME
)

INSERT INTO #SalesSnapshot
SELECT	I.BusinessUnitID, i.InventoryID, I.VehicleID, I.InventoryReceivedDate, vs.DealDate
FROM	[FLDW].dbo.Inventory I
JOIN	[FLDW].dbo.Vehicle v ON i.VehicleID = v.VehicleID AND i.BusinessUnitID = v.BusinessUnitID
JOIN	[FLDW].dbo.VehicleSale vs ON i.InventoryID = vs.InventoryID
CROSS JOIN [FLDW].dbo.Period_D P
WHERE	i.BusinessUnitID = @DealerID
AND		i.InventoryActive = 0
AND		i.InventoryType = 2
AND		P.PeriodID = @PeriodID
AND		VS.DealDate BETWEEN P.BeginDate AND P.EndDate
AND		VS.SaleDescription = 'R'

SELECT	b.counter,
		b.lowAge,
		b.highAge,
		AVG( lph.[Difference] ) as 'AvgRepriceValue',
		COUNT( DISTINCT( lph.InventoryID ) )  as 'VehiclesRepriced',
		i.Vehicles as 'Vehicles'

FROM	@Buckets b

LEFT JOIN
	(
		SELECT	I.InventoryReceivedDate,
				DATEDIFF(DD, I.InventoryReceivedDate, I.DealDate) as 'Age',
				(LPH2.ListPrice - LPH1.ListPrice) as 'Difference',
				I.InventoryID
		FROM	#SalesSnapshot I
		JOIN	(
					SELECT	LPH1.InventoryID, LPH1.ListPrice, LPH1.DMSReferenceDT, LPH2.NextDMSReferenceDT
					FROM 	[IMT].dbo.Inventory_ListPriceHistory LPH1
					LEFT JOIN
						(
							SELECT	L1.InventoryID, L1.DMSReferenceDT, MIN(L2.DMSReferenceDT) NextDMSReferenceDT
							FROM	[IMT].dbo.Inventory_ListPriceHistory L1
							JOIN	[IMT].dbo.Inventory_ListPriceHistory L2 ON L1.InventoryID = L2.InventoryID
							WHERE	L1.ListPrice > 0
							AND		L2.ListPrice > 0
							AND		L1.DMSReferenceDT < L2.DMSReferenceDT
							GROUP
							BY		L1.InventoryID, L1.DMSReferenceDT
						) LPH2 ON LPH2.InventoryID = LPH1.InventoryID AND LPH2.DMSReferenceDT = LPH1.DMSReferenceDT
					WHERE	LPH1.ListPrice > 0
				) LPH1 ON LPH1.InventoryID = I.InventoryID
		JOIN	[IMT].dbo.Inventory_ListPriceHistory LPH2 ON LPH1.InventoryID = LPH2.InventoryID AND LPH1.NextDMSReferenceDT = LPH2.DMSReferenceDT
		WHERE	LPH2.ListPrice > 0
		AND		LPH1.ListPrice > 0
	) lph ON lph.Age BETWEEN b.lowAge And b.highAge

LEFT JOIN
	(
		SELECT	bc.counter, bc.lowAge, bc.highAge, COUNT(ic.InventoryID) as 'Vehicles'
		FROM	@Buckets bc
		LEFT JOIN
		(
			SELECT	I.InventoryID, DATEDIFF(DD, I.InventoryReceivedDate, GETDATE()) as 'Age'
			FROM	#SalesSnapshot I
		) ic ON ic.Age Between bc.lowAge And bc.highAge
		GROUP
		BY		bc.Counter, bc.lowAge, bc.highAge
	) i ON b.counter = i.counter

GROUP
BY		b.counter,
		b.lowAge,
		b.highAge,
		i.Vehicles

DROP TABLE #SalesSnapshot

GO
