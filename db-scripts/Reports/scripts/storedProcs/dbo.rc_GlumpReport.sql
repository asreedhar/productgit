SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_GlumpReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[rc_GlumpReport]
GO

CREATE PROCEDURE [dbo].[rc_GlumpReport]
---Parameters-------------------------------------------------------------------------------------
--
	@segmentUnknown         INT,
	@segmentTruck		INT,
	@segmentSedan		INT,
	@segmentCoupe		INT,
	@segmentVan		INT,
	@segmentSUV		INT,
	@segmentConvertible	INT,
	@segmentWagon		INT,
	@periodId		INT,
	@DealerID		INT

--
---History--------------------------------------------------------------------------------------
--
--	JMW	07/16/2012	Created -- BUGZID: 15978
--
---Notes----------------------------------------------------------------------------------------
--		
--	Bucket calculation was formulated from talking to business. There was no clear cut
--	already existing formula. The goal was to drive home for the consumer what cars are best to
--	sell. From that goal and the spec I will implement the following formula. Feel free to
--	change as necesary.
--
--	Bucket 1/7 = -/+ 1.5 the standard deviation. i.e. bucket 1 = -4*stdDev to -1.5*stdDev
--	Bucket 2-6 = 3/5 minus the previous mark. i.e bucket 6 = 1.5*stdDev to 1.5*stdDev-((3/5)*1.5*stdDev)
--
--	For segments with no data we'll just use hard coded data. For segments with very small
--	data we'll just cut up the min and max into 7 evenly spaced buckets
------------------------------------------------------------------------------------------------
AS

SET NOCOUNT ON

DECLARE @beginDate DATETIME, @endDate DATETIME

SELECT	@beginDate=BeginDate,
		@endDate=EndDate 
FROM	FLDW.dbo.Period_D
WHERE	PeriodID = @periodId

CREATE TABLE #results
(
	 NumberOfSales INT
	,PercentageOfSales FLOAT
	,AverageSalePrice FLOAT
	,AverageListPrice FLOAT
	,AverageUnitCost FLOAT
	,AverageTimeToSale FLOAT
	,AverageROI FLOAT
	,AverageProfitPerVehicle FLOAT
	,TotalProfit FLOAT
	,Segment VARCHAR(2000)
	,SegmentID INT
	,LowBucket INT
	,HighBucket INT
	,BucketID INT
	,SalesData INT -- use this to determine if it is for 
				   -- the sales portion or inventory portion of report
)

CREATE TABLE #tmpSegment (RSEGMENT INT)

IF @segmentUnknown = 1 INSERT INTO #tmpSegment VALUES (1)
IF @segmentTruck = 1 INSERT INTO #tmpSegment VALUES (2)
IF @segmentSedan = 1 INSERT INTO #tmpSegment VALUES (3)
IF @segmentCoupe = 1 INSERT INTO #tmpSegment VALUES (4)
IF @segmentVan = 1 INSERT INTO #tmpSegment VALUES (5)
IF @segmentSUV = 1 INSERT INTO #tmpSegment VALUES (6)
IF @segmentConvertible = 1 INSERT INTO #tmpSegment VALUES (7)
IF @segmentWagon = 1 INSERT INTO #tmpSegment VALUES (8)

----------------------------------------------------------------------------------------
-- Bucket Calculation - Stats
----------------------------------------------------------------------------------------

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

CREATE TABLE #stats (
	SegmentID INT,
	BucketFourLow INT, 
	BucketFourHigh INT, 
	MinSale INT, 
	MaxSale INT,
	MainBucketLength INT)

INSERT INTO #stats(SegmentID, BucketFourLow, BucketFourHigh, MinSale, MaxSale)
SELECT  -1 SegmentID,
	CEILING((AVG(VS.SalePrice) - 0.3*STDEV(VS.SalePrice))/1000)*1000 BucketFourLow,
	FLOOR((AVG(VS.SalePrice) + 0.3*STDEV(VS.SalePrice))/1000)*1000 BucketFourHigh,
	FLOOR(MIN(VS.SalePrice)/1000)*1000 MinSale,
	CEILING(MAX(VS.SalePrice)/1000)*1000 MaxSale
FROM    FLDW.dbo.VehicleSale VS
JOIN    FLDW.dbo.Inventory I ON VS.InventoryID = I.InventoryID
WHERE   I.BusinessUnitID = @DealerID 
    AND VS.DealDate BETWEEN @beginDate AND @endDate
    AND VS.SaleDescription = 'R'
    AND I.InventoryType = 2
    AND (I.ListPrice > 0 AND I.UnitCost > 0)

INSERT INTO #stats(SegmentID, BucketFourLow, BucketFourHigh, MinSale, MaxSale)
SELECT	MMG.SegmentID SegmentID,
	CEILING((AVG(VS.SalePrice) - 0.3*STDEV(VS.SalePrice))/1000)*1000 BucketFourLow,
	FLOOR((AVG(VS.SalePrice) + 0.3*STDEV(VS.SalePrice))/1000)*1000 BucketFourHigh,
	FLOOR(MIN(VS.SalePrice)/1000)*1000 MinSale,
	CEILING(MAX(VS.SalePrice)/1000)*1000 MaxSale
FROM	FLDW.dbo.VehicleSale VS
JOIN	FLDW.dbo.Inventory I ON VS.InventoryID = I.InventoryID
JOIN	IMT.dbo.tbl_Vehicle V ON I.VehicleID = V.VehicleID
JOIN	IMT.dbo.MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID
JOIN	FLDW.dbo.Segment S ON MMG.SegmentID = S.SegmentID
WHERE	I.BusinessUnitID = @DealerID 
    AND VS.DealDate BETWEEN @beginDate AND @endDate
    AND VS.SaleDescription = 'R'
    AND I.InventoryType = 2
    AND (I.ListPrice > 0 AND I.UnitCost > 0)
GROUP BY MMG.SegmentID

----------------------------------------------------------------------------------------
-- Stats Calculation - No data for segment
----------------------------------------------------------------------------------------

INSERT INTO #stats (SegmentID,BucketFourLow,BucketFourHigh,MinSale,MaxSale,MainBucketLength)
SELECT	SG.SegmentID SegmentID,
        16000 BucketFourLow,
	20000 BucketFourHigh,
	0 MinSale,
	40000 MaxSale,
	NULL MainBucketLength
FROM	FLDW.dbo.Segment SG
WHERE NOT EXISTS (SELECT 1 FROM #stats ST WHERE SG.SegmentID = ST.SegmentID)

----------------------------------------------------------------------------------------
-- Stats Calculation - No data for overall segment
----------------------------------------------------------------------------------------

UPDATE	#stats
SET     BucketFourLow=16000,
	BucketFourHigh=20000,
	MinSale=0,
	MaxSale=40000,
	MainBucketLength=NULL
WHERE	BucketFourHigh IS NULL 
    AND SegmentID = -1

----------------------------------------------------------------------------------------
-- Stats Calculation - At least one sale but not a lot of other data
----------------------------------------------------------------------------------------

UPDATE	#stats 
SET	BucketFourLow = FLOOR((((MaxSale-MinSale)/7.0)*3+MinSale)/1000)*1000,
	BucketFourHigh = CEILING((((MaxSale-MinSale)/7.0)*4+MinSale)/1000)*1000
WHERE	BucketFourHigh IS NULL 
    AND BucketFourLow IS NULL 
    AND MaxSale IS NOT NULL 
    AND MinSale IS NOT NULL		

----------------------------------------------------------------------------------------
-- Stats Calculation - High bucket is greater than low bucket because floor/ceiling
----------------------------------------------------------------------------------------

UPDATE	#stats 
SET	BucketFourLow = BucketFourHigh,
	BucketFourHigh = BucketFourLow
WHERE	BucketFourHigh < BucketFourLow		

----------------------------------------------------------------------------------------
-- Stats Calculation - High bucket is same as low bucket because of few data
----------------------------------------------------------------------------------------
								
UPDATE	#stats 
SET	BucketFourHigh = BucketFourHigh+1000
WHERE	BucketFourHigh = BucketFourLow

----------------------------------------------------------------------------------------
-- Stats Calculation - Max sale is under 7000
----------------------------------------------------------------------------------------

UPDATE	#stats
SET	BucketFourHigh = 4000,
	BucketFourLow = 3000,
	MinSale = 0,
	MaxSale = 7000
WHERE	MaxSale <= 7000

UPDATE #stats SET MainBucketLength = BucketFourHigh - BucketFourLow

----------------------------------------------------------------------------------------
-- Stats Calculation - BucketOne is under 0
----------------------------------------------------------------------------------------

UPDATE	#stats 
SET	BucketFourHigh = BucketFourHigh - CEILING((0-(BucketFourLow-2*MainBucketLength-1))/1000.0)*1000,
	MainBucketLength = MainBucketLength - CEILING((0-(BucketFourLow-2*MainBucketLength-1))/1000.0)*1000
WHERE	BucketFourLow-2*MainBucketLength-1 <= 0

----------------------------------------------------------------------------------------
-- Bucket Calculation - Buckets
----------------------------------------------------------------------------------------

CREATE TABLE #buckets(BucketID INT, LowBucket INT, HighBucket INT, SegmentID INT)

INSERT INTO #buckets (BucketID, LowBucket, HighBucket, SegmentID)
SELECT		1 BucketID,
			CASE WHEN MinSale >= (BucketFourLow-2*MainBucketLength) THEN 0 ELSE MinSale END LowBucket,
			BucketFourLow-2*MainBucketLength HighBucket,
			SegmentID SegmentID			
FROM		#stats
GROUP BY	SegmentID, BucketFourLow, BucketFourHigh, MinSale, MainBucketLength
UNION ALL 
SELECT		2 BucketID,
			BucketFourLow-2*MainBucketLength+1 LowBucket,
			BucketFourLow-MainBucketLength HighBucket,
			SegmentID SegmentID			
FROM		#stats
GROUP BY	SegmentID, BucketFourLow, BucketFourHigh, MainBucketLength
UNION ALL 
SELECT		3 BucketID,
			BucketFourLow-MainBucketLength+1 LowBucket,
			BucketFourLow HighBucket,
			SegmentID SegmentID			
FROM		#stats
GROUP BY	SegmentID, BucketFourLow, BucketFourHigh, MainBucketLength
UNION ALL 
SELECT		4 BucketID,
			BucketFourLow+1 LowBucket,
			BucketFourHigh HighBucket,
			SegmentID SegmentID			
FROM		#stats
GROUP BY	SegmentID, BucketFourLow, BucketFourHigh
UNION ALL 
SELECT		5 BucketID,
			BucketFourHigh+1 LowBucket,
			BucketFourHigh+MainBucketLength HighBucket,
			SegmentID SegmentID			
FROM		#stats
GROUP BY	SegmentID, BucketFourLow, BucketFourHigh, MainBucketLength
UNION ALL 
SELECT		6 BucketID,
			BucketFourHigh+MainBucketLength+1 LowBucket,
			BucketFourHigh+2*MainBucketLength HighBucket,
			SegmentID SegmentID			
FROM		#stats
GROUP BY	SegmentID, BucketFourLow, BucketFourHigh, MainBucketLength
UNION ALL 
SELECT		7 BucketID,
			BucketFourHigh+2*MainBucketLength+1 LowBucket,
			CASE WHEN (BucketFourHigh+2*MainBucketLength+1) >= MaxSale THEN (BucketFourHigh+2*MainBucketLength+4000) ELSE MaxSale END HighBucket,
			SegmentID SegmentID			
FROM		#stats
GROUP BY	SegmentID, BucketFourLow, BucketFourHigh, MaxSale, MainBucketLength

DROP TABLE #stats

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 

----------------------------------------------------------------------------------------
-- Grab All Segment Sales Data
----------------------------------------------------------------------------------------
INSERT INTO #results
SELECT COUNT(VS.InventoryID) AS NumberOfSales
	   ,NULL AS PercentageOfSales
	   ,AVG(VS.SalePrice) AS AverageSalePrice
	   ,AVG(I.ListPrice) AS AverageListPrice
	   ,AVG(I.UnitCost) AS AverageUnitCost
	   ,AVG(DATEDIFF("d", I.InventoryReceivedDate,I.DeleteDt)) AS AverageTimeToSale
	   ,CASE SUM(I.UnitCost) WHEN 0 THEN NULL ELSE ((CAST(SUM(VS.SalePrice) AS DECIMAL) - CAST(SUM(I.UnitCost) AS DECIMAL)) / CAST(SUM(I.UnitCost) AS DECIMAL)) END AS AverageROI
	   ,AVG(VS.FrontEndGross) AS AverageProfitPerVehicle
	   ,SUM(VS.SalePrice-I.UnitCost) AS TotalProfit
	   ,'S_' + S.Segment + 's' AS [Segment]
	   ,S.SegmentID AS [SegmentID]
	   ,B.LowBucket
	   ,B.HighBucket
	   ,B.BucketID
	   ,1 AS SalesData
FROM FLDW.dbo.VehicleSale VS
JOIN FLDW.dbo.Inventory I ON VS.InventoryID = I.InventoryID
JOIN IMT.dbo.tbl_Vehicle V ON I.VehicleID = V.VehicleID
JOIN IMT.dbo.MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID
JOIN FLDW.dbo.Segment S ON MMG.SegmentID = S.SegmentID
JOIN #tmpSegment TS ON S.SegmentID = TS.RSEGMENT
JOIN #buckets B ON I.ListPrice BETWEEN B.LowBucket AND B.HighBucket AND S.SegmentID = B.SegmentID
WHERE I.BusinessUnitID = @DealerID AND
	  VS.DealDate BETWEEN @beginDate AND @endDate
	  AND VS.SaleDescription = 'R'
	  AND I.InventoryType = 2
          AND (I.ListPrice > 0 AND I.UnitCost > 0)
GROUP BY B.LowBucket, B.HighBucket, S.Segment, B.BucketID, S.SegmentID
ORDER BY S.Segment, B.LowBucket


SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

----------------------------------------------------------------------------------------
-- Grab All Segment Inventory Data
----------------------------------------------------------------------------------------
INSERT INTO #results
SELECT COUNT(I.InventoryID) AS NumberOfSales -- Number Vehicles
	   ,0 AS PercentageOfSales -- Percentage of Inventory
	   ,AVG(I.ListPrice) AS AverageSalePrice -- Average Price
	   ,0 AS AverageListPrice
	   ,0 AS AverageUnitCost
	   ,AVG(DATEDIFF("d", I.InventoryReceivedDate,GETDATE())) AS AverageTimeToSale -- Days in Stock
	   ,0 AS AverageROI
	   ,AVG(i.ListPrice-I.UnitCost) AS AverageProfitPerVehicle -- Average Potential Profit Per Vehicle
	   ,SUM(I.ListPrice-I.UnitCost) AS TotalProfit -- Total Profit
	   ,'I_' + S.Segment AS [Segment]
	   ,S.SegmentID AS [SegmengID]
	   ,B.LowBucket
	   ,B.HighBucket
	   ,B.BucketID
	   ,0 AS SalesData
FROM FLDW.dbo.Inventory I 
JOIN IMT.dbo.tbl_Vehicle V ON I.VehicleID = V.VehicleID
JOIN FLDW.dbo.InventorySales_F ISF ON ISF.BusinessUnitID = @DealerID AND ISF.InventoryID = I.InventoryID
JOIN FLDW.dbo.InventoryEventCategory_D IECD ON IECD.InventoryEventCategoryID = ISF.InventoryEventCategoryID
JOIN IMT.dbo.MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID
JOIN FLDW.dbo.Segment S ON MMG.SegmentID = S.SegmentID
JOIN #tmpSegment TS ON S.SegmentID = TS.RSEGMENT
JOIN #buckets B ON I.ListPrice BETWEEN B.LowBucket AND B.HighBucket AND S.SegmentID = B.SegmentID
WHERE I.BusinessUnitID = @DealerID 
        AND I.InventoryActive = 1
        AND I.InventoryType = 2
	AND IECD.IsRetail = 1
        AND (I.ListPrice > 0 AND I.UnitCost > 0)
GROUP BY B.LowBucket, B.HighBucket, S.Segment, B.BucketID, S.SegmentID
ORDER BY S.Segment, B.LowBucket

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

----------------------------------------------------------------------------------------
-- Grab Overall Category Sales Data
----------------------------------------------------------------------------------------
INSERT INTO #results
SELECT COUNT(VS.InventoryID) AS NumberOfSales
	   ,NULL AS PercentageOfSales
	   ,AVG(VS.SalePrice) AS AverageSalePrice
	   ,AVG(I.ListPrice) AS AverageListPrice
	   ,AVG(I.UnitCost) AS AverageUnitCost
	   ,AVG(DATEDIFF("d", I.InventoryReceivedDate,I.DeleteDt)) AS AverageTimeToSale
	   ,CASE SUM(I.UnitCost) WHEN 0 THEN NULL ELSE ((CAST(SUM(VS.SalePrice) AS DECIMAL) - CAST(SUM(I.UnitCost) AS DECIMAL)) / CAST(SUM(I.UnitCost) AS DECIMAL)) END AS AverageROI
	   ,AVG(VS.FrontEndGross) AS AverageProfitPerVehicle
	   ,SUM(VS.SalePrice-I.UnitCost) AS TotalProfit
	   ,'S_Overall Category' AS [Segment]
	   ,-1 AS [SegmentID]
	   ,B.LowBucket
	   ,B.HighBucket
	   ,B.BucketID
	   ,1 AS SalesData
FROM FLDW.dbo.VehicleSale VS
JOIN FLDW.dbo.Inventory I ON VS.InventoryID = I.InventoryID
JOIN #buckets B ON I.ListPrice BETWEEN B.LowBucket AND B.HighBucket
WHERE I.BusinessUnitID = @DealerID 
  AND VS.DealDate BETWEEN @beginDate AND @endDate 
  AND B.SegmentID = -1
  AND VS.SaleDescription = 'R'
  AND I.InventoryType = 2
  AND (I.ListPrice > 0 AND I.UnitCost > 0)
GROUP BY B.LowBucket, B.HighBucket, B.BucketID
ORDER BY B.LowBucket

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

----------------------------------------------------------------------------------------
-- Grab Overall Category Inventory Data
----------------------------------------------------------------------------------------
INSERT INTO #results
SELECT COUNT(I.InventoryID) AS NumberOfSales -- Number Vehicles
	   ,0 AS PercentageOfSales -- Percentage of Inventory
	   ,AVG(I.ListPrice) AS AverageSalePrice -- Average Price
	   ,0 AS AverageListPrice
	   ,0 AS AverageUnitCost
	   ,AVG(DATEDIFF("d", I.InventoryReceivedDate,GETDATE())) AS AverageTimeToSale -- Days in Stock
	   ,0 AS AverageROI
	   ,AVG(I.ListPrice-I.UnitCost) AS AverageProfitPerVehicle -- Average Potential Profit Per Vehicle
	   ,SUM(I.ListPrice-I.UnitCost) AS TotalProfit -- Total Profit
	   ,'I_Overall Category' AS [Segment]
	   ,-2 AS [SegmentID]
	   ,B.LowBucket
	   ,B.HighBucket
	   ,B.BucketID
	   ,0 AS SalesData
FROM FLDW.dbo.Inventory I 
JOIN FLDW.dbo.InventorySales_F ISF ON ISF.BusinessUnitID = @DealerID AND ISF.InventoryID = I.InventoryID
JOIN FLDW.dbo.InventoryEventCategory_D IECD ON IECD.InventoryEventCategoryID = ISF.InventoryEventCategoryID
JOIN #buckets B ON I.ListPrice BETWEEN B.LowBucket AND B.HighBucket
WHERE I.BusinessUnitID = @DealerID 
  AND I.InventoryActive = 1 
  AND B.SegmentID = -1
  AND I.InventoryType = 2
  AND IECD.IsRetail = 1
  AND (I.ListPrice > 0 AND I.UnitCost > 0)
GROUP BY B.LowBucket, B.HighBucket, B.BucketID
ORDER BY B.LowBucket

----------------------------------------------------------------------------------------
-- Calculate Percentage of Sales values
----------------------------------------------------------------------------------------

UPDATE R
SET R.PercentageOfSales = (R.NumberOfSales / CAST(T.TotalSales AS FLOAT))
FROM #results R
JOIN 
( 
	SELECT SUM(NumberOfSales) AS TotalSales, Segment  
	FROM #results
	WHERE SalesData = 1 
	GROUP BY Segment 
) T ON R.Segment = T.Segment

----------------------------------------------------------------------------------------
-- Calculate Percentage of Inventory values
----------------------------------------------------------------------------------------

UPDATE R
SET R.PercentageOfSales = (R.NumberOfSales / CAST(T.TotalSales AS FLOAT))
FROM #results R
JOIN 
( 
	SELECT SUM(NumberOfSales) AS TotalSales, Segment  
	FROM #results
	WHERE SalesData = 0 
	GROUP BY Segment
) T ON R.Segment = T.Segment

----------------------------------------------------------------------------------------
-- Add null rows where a vehicles list price is not in a bucket
----------------------------------------------------------------------------------------

INSERT INTO #results
SELECT  0 AS NumberOfSales
	   ,0 AS PercentageOfSales
	   ,0 AS AverageSalePrice
	   ,0 AS AverageListPrice
	   ,0 AS AverageUnitCost
	   ,0 AS AverageTimeToSale
	   ,0 AS AverageROI
	   ,0 AS AverageProfitPerVehicle
	   ,0 AS TotalProfit
	   ,S.Segment AS [Segment]
	   ,S.SegmentID AS [SegmentID]
	   ,B.LowBucket
	   ,B.HighBucket
	   ,B.BucketID
	   ,NULL AS SalesData
FROM	(SELECT DISTINCT 'I_' + Segment Segment, SegmentID SegmentID FROM FLDW.dbo.Segment UNION ALL 
		 SELECT DISTINCT 'S_' + Segment + 's' Segment, SegmentID SegmentID FROM FLDW.dbo.Segment UNION ALL
		 SELECT DISTINCT 'I_Overall Category' Segment, -2 SegmentID UNION ALL
		 SELECT DISTINCT 'S_Overall Category' Segment, -1 SegmentID) S JOIN
		#buckets B ON (S.SegmentID = B.SegmentID OR (S.SegmentID = -2 AND B.BucketID = -1)) LEFT JOIN
		(SELECT DISTINCT RSEGMENT FROM #tmpSegment) TS ON S.SegmentID = TS.RSEGMENT
WHERE NOT EXISTS
	(
		SELECT 1 FROM #results R WHERE B.LowBucket = R.LowBucket AND 
									   B.HighBucket = R.HighBucket AND
									   S.Segment = R.Segment
	) AND
	(S.SegmentID < 0 OR TS.RSEGMENT IS NOT NULL)


DROP TABLE #tmpSegment

----------------------------------------------------------------------------------------
-- Pivot on each calculation and join everything together to produce final unorganized results
----------------------------------------------------------------------------------------
;WITH UnorganizedResults(Segment, Bucket1, Bucket2, Bucket3, Bucket4, Bucket5, Bucket6, Bucket7,
Calculation, OrderBySegment, OrderByCalculation, LowBucket1, HighBucket1, LowBucket2, HighBucket2,
LowBucket3, HighBucket3, LowBucket4, HighBucket4, LowBucket5, HighBucket5, LowBucket6, HighBucket6,
LowBucket7, HighBucket7) AS (
SELECT COMBINED.*
	  ,B1.LowBucket AS LowBucket1
	  ,B1.HighBucket AS HighBucket1 
	  ,B2.LowBucket AS LowBucket2
	  ,B2.HighBucket AS HighBucket2
	  ,B3.LowBucket AS LowBucket3
	  ,B3.HighBucket AS HighBucket3
	  ,B4.LowBucket AS LowBucket4
	  ,B4.HighBucket AS HighBucket4
	  ,B5.LowBucket AS LowBucket5
	  ,B5.HighBucket AS HighBucket5
	  ,B6.LowBucket AS LowBucket6
	  ,B6.HighBucket AS HighBucket6
	  ,B7.LowBucket AS LowBucket7
	  ,B7.HighBucket AS HighBucket7
FROM (
	--------------------------
	-- NumberOfSales
	--------------------------
	SELECT T.*
		   ,'Number of Sales' AS Calculation
		   ,CASE T.Segment WHEN 'S_Overall Category' THEN 0 WHEN 'I_Overall Category' THEN 0 ELSE 1 END AS OrderBySegment
		   ,1 as OrderByCalculation
	FROM (
		SELECT Segment, [1] AS Bucket1, [2] AS Bucket2, 
			   [3] AS Bucket3, [4] AS Bucket4, [5] AS Bucket5,
			   [6] AS Bucket6, [7] AS Bucket7
		FROM (SELECT NumberOfSales, BucketID, Segment FROM #results) AS Results
		PIVOT ( MAX(NumberOfSales) FOR BucketID IN ([1], [2], [3], [4], [5], [6], [7]) ) AS PivotTable 
	) T
	UNION ALL
	--------------------------
	-- PercentageOfSales
	--------------------------
	SELECT T.*
		   ,'Percentage of Sales' AS Calculation
		   ,CASE T.Segment WHEN 'S_Overall Category' THEN 0 WHEN 'I_Overall Category' THEN 0 ELSE 1 END AS OrderBySegment
		   ,2 as OrderByCalculation
	FROM (
		SELECT Segment, [1] AS Bucket1, [2] AS Bucket2, 
			   [3] AS Bucket3, [4] AS Bucket4, [5] AS Bucket5,
			   [6] AS Bucket6, [7] AS Bucket7
		FROM (SELECT PercentageOfSales, BucketID, Segment FROM #results) AS Results
		PIVOT ( MAX(PercentageOfSales) FOR BucketID IN ([1], [2], [3], [4], [5], [6], [7]) ) AS PivotTable 
	) T
	UNION ALL
	--------------------------
	-- AverageSalePrice
	--------------------------
	SELECT T.*
		   ,'Average Price' AS Calculation
		   ,CASE T.Segment WHEN 'S_Overall Category' THEN 0 WHEN 'I_Overall Category' THEN 0 ELSE 1 END AS OrderBySegment
		   ,3 as OrderByCalculation
	FROM (
		SELECT Segment, [1] AS Bucket1, [2] AS Bucket2, 
			   [3] AS Bucket3, [4] AS Bucket4, [5] AS Bucket5,
			   [6] AS Bucket6, [7] AS Bucket7
		FROM (SELECT AverageSalePrice, BucketID, Segment FROM #results) AS Results
		PIVOT ( MAX(AverageSalePrice) FOR BucketID IN ([1], [2], [3], [4], [5], [6], [7]) ) AS PivotTable 
	) T
	UNION ALL
	--------------------------
	-- AverageListPrice
	--------------------------
	SELECT T.*
		   ,'AverageListPrice' AS Calculation
		   ,CASE T.Segment WHEN 'S_Overall Category' THEN 0 WHEN 'I_Overall Category' THEN 0 ELSE 1 END AS OrderBySegment
		   ,4 as OrderByCalculation
	FROM (
		SELECT Segment, [1] AS Bucket1, [2] AS Bucket2, 
			   [3] AS Bucket3, [4] AS Bucket4, [5] AS Bucket5,
			   [6] AS Bucket6, [7] AS Bucket7
		FROM (SELECT AverageListPrice, BucketID, Segment FROM #results) AS Results
		PIVOT ( MAX(AverageListPrice) FOR BucketID IN ([1], [2], [3], [4], [5], [6], [7]) ) AS PivotTable 
	) T
	UNION ALL
	--------------------------
	-- AverageUnitCost
	--------------------------
	SELECT T.*
		   ,'AverageUnitCost' AS Calculation
		   ,CASE T.Segment WHEN 'S_Overall Category' THEN 0 WHEN 'I_Overall Category' THEN 0 ELSE 1 END AS OrderBySegment
		   ,5 as OrderByCalculation
	FROM (
		SELECT Segment, [1] AS Bucket1, [2] AS Bucket2, 
			   [3] AS Bucket3, [4] AS Bucket4, [5] AS Bucket5,
			   [6] AS Bucket6, [7] AS Bucket7
		FROM (SELECT AverageUnitCost, BucketID, Segment FROM #results) AS Results
		PIVOT ( MAX(AverageUnitCost) FOR BucketID IN ([1], [2], [3], [4], [5], [6], [7]) ) AS PivotTable 
	) T
	UNION ALL
	--------------------------
	-- AverageTimeToSale
	--------------------------
	SELECT T.*
		   ,'Time to Sale' AS Calculation
		   ,CASE T.Segment WHEN 'S_Overall Category' THEN 0 WHEN 'I_Overall Category' THEN 0 ELSE 1 END AS OrderBySegment
		   ,6 as OrderByCalculation
	FROM (
		SELECT Segment, [1] AS Bucket1, [2] AS Bucket2, 
			   [3] AS Bucket3, [4] AS Bucket4, [5] AS Bucket5,
			   [6] AS Bucket6, [7] AS Bucket7
		FROM (SELECT AverageTimeToSale, BucketID, Segment FROM #results) AS Results
		PIVOT ( MAX(AverageTimeToSale) FOR BucketID IN ([1], [2], [3], [4], [5], [6], [7]) ) AS PivotTable 
	) T
	UNION ALL
	--------------------------
	-- AverageROI
	--------------------------
	SELECT T.*
		   ,'ROI' AS Calculation
		   ,CASE T.Segment WHEN 'S_Overall Category' THEN 0 WHEN 'I_Overall Category' THEN 0 ELSE 1 END AS OrderBySegment
		   ,7 as OrderByCalculation
	FROM (
		SELECT Segment, [1] AS Bucket1, [2] AS Bucket2, 
			   [3] AS Bucket3, [4] AS Bucket4, [5] AS Bucket5,
			   [6] AS Bucket6, [7] AS Bucket7
		FROM (SELECT AverageROI, BucketID, Segment FROM #results) AS Results
		PIVOT ( MAX(AverageROI) FOR BucketID IN ([1], [2], [3], [4], [5], [6], [7]) ) AS PivotTable 
	) T
	UNION ALL
	--------------------------
	-- AverageProfitPerVehicle
	--------------------------
	SELECT T.*
		   ,'Average Profit Per Vehicle' AS Calculation
		   ,CASE T.Segment WHEN 'S_Overall Category' THEN 0 WHEN 'I_Overall Category' THEN 0 ELSE 1 END AS OrderBySegment
		   ,8 as OrderByCalculation
	FROM (
		SELECT Segment, [1] AS Bucket1, [2] AS Bucket2, 
			   [3] AS Bucket3, [4] AS Bucket4, [5] AS Bucket5,
			   [6] AS Bucket6, [7] AS Bucket7
		FROM (SELECT AverageProfitPerVehicle, BucketID, Segment FROM #results) AS Results
		PIVOT ( MAX(AverageProfitPerVehicle) FOR BucketID IN ([1], [2], [3], [4], [5], [6], [7]) ) AS PivotTable 
	) T
	UNION ALL
	--------------------------
	-- TotalProfit
	--------------------------
	SELECT T.*
		   ,'Total Profit' AS Calculation
		   ,CASE T.Segment WHEN 'S_Overall Category' THEN 0 WHEN 'I_Overall Category' THEN 0 ELSE 1 END AS OrderBySegment
		   ,9 as OrderByCalculation
	FROM (
		SELECT Segment, [1] AS Bucket1, [2] AS Bucket2, 
			   [3] AS Bucket3, [4] AS Bucket4, [5] AS Bucket5,
			   [6] AS Bucket6, [7] AS Bucket7
		FROM (SELECT TotalProfit, BucketID, Segment FROM #results) AS Results
		PIVOT ( MAX(TotalProfit) FOR BucketID IN ([1], [2], [3], [4], [5], [6], [7]) ) AS PivotTable 
	) T
) COMBINED
JOIN (SELECT DISTINCT Segment, SegmentID FROM #results) S ON S.Segment = COMBINED.Segment
JOIN (SELECT LowBucket, HighBucket, SegmentID FROM #buckets WHERE BucketID = 1) B1 ON (B1.SegmentID = S.SegmentID OR B1.SegmentID = -1 AND S.SegmentID = -2) 
JOIN (SELECT LowBucket, HighBucket, SegmentID FROM #buckets WHERE BucketID = 2) B2 ON (B2.SegmentID = S.SegmentID OR B2.SegmentID = -1 AND S.SegmentID = -2)
JOIN (SELECT LowBucket, HighBucket, SegmentID FROM #buckets WHERE BucketID = 3) B3 ON (B3.SegmentID = S.SegmentID OR B3.SegmentID = -1 AND S.SegmentID = -2)
JOIN (SELECT LowBucket, HighBucket, SegmentID FROM #buckets WHERE BucketID = 4) B4 ON (B4.SegmentID = S.SegmentID OR B4.SegmentID = -1 AND S.SegmentID = -2)
JOIN (SELECT LowBucket, HighBucket, SegmentID FROM #buckets WHERE BucketID = 5) B5 ON (B5.SegmentID = S.SegmentID OR B5.SegmentID = -1 AND S.SegmentID = -2)
JOIN (SELECT LowBucket, HighBucket, SegmentID FROM #buckets WHERE BucketID = 6) B6 ON (B6.SegmentID = S.SegmentID OR B6.SegmentID = -1 AND S.SegmentID = -2)
JOIN (SELECT LowBucket, HighBucket, SegmentID FROM #buckets WHERE BucketID = 7) B7 ON (B7.SegmentID = S.SegmentID OR B7.SegmentID = -1 AND S.SegmentID = -2)
)

----------------------------------------------------------------------------------------
-- Order and select final results
----------------------------------------------------------------------------------------

SELECT T.*, S.SegmentID AS [SegmentID] FROM (
SELECT RANK() OVER (PARTITION BY LEFT(Segment, 1) ORDER BY OrderBySegment, Segment) AS Ranking, * FROM UnorganizedResults
WHERE Calculation NOT IN ('AverageUnitCost', 'AverageListPrice') ) T
JOIN (SELECT DISTINCT Segment, SegmentID FROM #results) S ON S.Segment = T.Segment
ORDER BY T.Ranking, LEFT(T.Segment, 1) DESC, T.OrderByCalculation

DROP TABLE #buckets
DROP TABLE #results

GO

GRANT EXECUTE ON dbo.rc_GlumpReport TO [StoredProcedureUser]
GO