
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetReportSubscribers#PlatinumReports]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[GetReportSubscribers#PlatinumReports] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[GetReportSubscribers#PlatinumReports] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[GetReportSubscribers#PlatinumReports]
	@SubscriptionTypeID            INT,
	@SubscriptionFrequencyDetailID INT = NULL
AS


/* --------------------------------------------------------------------
 * 
 * $Id: dbo.GetReportsSubscribers#PlatinumReports.sql,v 1.5 2009/03/16 19:27:05 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return a list of recipients for the supplied subscription type and frequency.
 * This SPROC is expected to run on subscriptin types 2-5
 * 
 * Parameters
 * ----------
 *
 * @SubscriptionTypeID            - the type of the report
 * @SubscriptionFrequencyDetailID - the frequency of the report
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - Invalid @SubscriptionTypeID
 * 50100 - Invalid @SubscriptionFrequencyDetailID
 *
 * MAK -01/29/2009  These should only be sent to Active users.
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

IF @SubscriptionFrequencyDetailID IS NULL BEGIN

	SELECT	@SubscriptionFrequencyDetailID = SubscriptionFrequencyDetailID
	FROM	[IMT].dbo.SubscriptionFrequencyDetail
	WHERE	Description = DATENAME(DW,GETDATE())

END

/* --------------------------------------------------------------------
 *                          Validate Parameters
 * -------------------------------------------------------------------- */

IF NOT EXISTS (SELECT 1 FROM [IMT].dbo.SubscriptionTypes WHERE SubscriptionTypeID = @SubscriptionTypeID) BEGIN
	RAISERROR('Invalid SubscriptionType ''%d''.', 16, 1, @SubscriptionTypeID)
	RETURN 50100
END

IF @SubscriptionTypeID not in (2,3,4,5) BEGIN
	RAISERROR('SubscriptionType Out Of Range [2,3,4,5] ''%d''.', 16, 1, @SubscriptionTypeID)
	RETURN 50100
END

IF NOT EXISTS (SELECT 1 FROM [IMT].dbo.SubscriptionFrequencyDetail WHERE SubscriptionFrequencyDetailID = @SubscriptionFrequencyDetailID AND SubscriptionFrequencyID <> 0) BEGIN
	RAISERROR('Invalid SubscriptionFrequencyDetail ''%d''.', 16, 1, @SubscriptionFrequencyDetailID)
	RETURN 50100
END

/* --------------------------------------------------------------------
 *                          Declare Result Schema
 * -------------------------------------------------------------------- */

DECLARE @Results TABLE (
	[TO]                VARCHAR(8000) NOT NULL,
	[RenderFormat]      VARCHAR(8000) NOT NULL,
	[Subject]           VARCHAR(8000) NOT NULL,
	[DealerID]			INT NOT NULL
)

DECLARE @Recipients TABLE (
	[Idx]			INT NOT NULL,
	[TO]			VARCHAR(8000) NOT NULL,
	[Subject]		VARCHAR(8000) NOT NULL,
	[DealerID]		INT NOT NULL
)

/* --------------------------------------------------------------------
 *                             Collect Recipients
 * -------------------------------------------------------------------- */

INSERT INTO @Recipients (
	[Idx],
	[TO],
	[Subject],
	[DealerID]
)

SELECT	[Idx]         = ROW_NUMBER() OVER (PARTITION BY B.BusinessUnitID ORDER BY B.BusinessUnitID),
	[TO]          = M.EmailAddress,
	[Subject]     = BusinessUnit + '''s ' + F.Description + ' ' + REPLACE(T.Description, 'Weekly ', '') + ' for ' + D.Description + ', ' + CONVERT(VARCHAR,GETDATE(),107),
	[DealerID] = B.BusinessUnitID

FROM	[IMT].dbo.Subscriptions S
JOIN	[IMT].dbo.SubscriptionTypes T ON S.SubscriptionTypeID = T.SubscriptionTypeID
JOIN	[IMT].dbo.Member M ON S.SubscriberID = M.MemberID
JOIN	[IMT].dbo.BusinessUnit B ON S.BusinessUnitID = B.BusinessUnitID
CROSS JOIN [IMT].dbo.SubscriptionFrequencyDetail D
JOIN       [IMT].dbo.SubscriptionFrequency F ON D.SubscriptionFrequencyID = F.SubscriptionFrequencyID


WHERE	D.SubscriptionFrequencyDetailID = @SubscriptionFrequencyDetailID
AND	S.SubscriptionTypeID = @SubscriptionTypeID
AND	S.SubscriptionFrequencyDetailID IN (@SubscriptionFrequencyDetailID,3)
AND	S.SubscriberTypeID = 1           -- Member
AND	S.SubscriptionDeliveryTypeID = 1 -- HTML email
AND	M.EmailAddress IS NOT NULL
AND	LEN(M.EmailAddress) > 5
AND	PATINDEX('%@%.%', M.EmailAddress) > 0
AND	B.Active =1

/* --------------------------------------------------------------------
 *                           Combine Recipients
 * -------------------------------------------------------------------- */

; WITH Rec ([Idx], [To], [DealerID], [Ctr]) AS
(
	SELECT	R1.Idx,
		R1.[TO],
		R1.[DealerID],
		1 Ctr
	FROM	@Recipients R1
UNION ALL
	SELECT	R2.Idx,
		R2.[TO] + COALESCE(';' + R3.[TO],''),
		R2.[DealerID],
		Ctr+1 Ctr
	FROM	@Recipients R2
	JOIN	Rec R3 ON R3.DealerID = R2.DealerID AND R3.Idx = R2.Idx + 1
),
Row (DealerID,Subject,Ctr) AS
(
	SELECT	DealerID, Subject, COUNT(*) Ctr
	FROM	@Recipients
	GROUP
	BY	DealerID, Subject
)
INSERT INTO @Results (
	[TO],
	[RenderFormat],
	[Subject],
	[DealerID]
)
SELECT	[TO]                = R.[To],
	[RenderFormat]      = 'MHTML',
	[Subject]           = X.Subject,
	[DealerID]       = R.DealerID
FROM	Rec R
JOIN	Row X ON R.DealerID = X.DealerID AND R.Ctr = X.Ctr

/* --------------------------------------------------------------------
 *                           Return Results
 * -------------------------------------------------------------------- */

SELECT * FROM @Results

GO