
/****** Object:  StoredProcedure [dbo].[rc_GrossProfitByDaysInStockDealLog]    Script Date: 05/06/2014 04:40:58 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_GrossProfitByDaysInStockDealLog]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[rc_GrossProfitByDaysInStockDealLog]
GO

USE [Reports]
GO

/****** Object:  StoredProcedure [dbo].[rc_GrossProfitByDaysInStockDealLog]    Script Date: 05/06/2014 04:40:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[rc_GrossProfitByDaysInStockDealLog]
	@PeriodID INT,
	@DealerID INT,
    @StrategyID	TINYINT = NULL -- SEE FLDW.[dbo].[GetStockingReportCube] for documentation
AS

IF (@StrategyID = 0) SELECT @StrategyID = NULL

DECLARE @StrategyDescription VARCHAR(2000)
SELECT @StrategyDescription = NULL
IF (@StrategyID = 1) SELECT @StrategyDescription = 'R'
IF (@StrategyID = 2) SELECT @StrategyDescription = 'W'

SET NOCOUNT ON;

SELECT 
		Sale.DealDate,
		Vehicle.VehicleYear,
		MakeModel.[Make],
		MakeModel.[Model],
		Segment.Segment DisplayBodyType,
		Vehicle.VehicleTrim,
		Vehicle.BaseColor,
		Inventory.DaysToSale,
		CASE
			WHEN Inventory.DaysToSale < 22 AND Sale.SaleDescription = 'R' Then 'Retailed 0 - 21 Days'
			WHEN Inventory.DaysToSale < 30 AND Sale.SaleDescription = 'R' Then 'Retailed 22 - 29 Days'
			WHEN Inventory.DaysToSale < 40 AND Sale.SaleDescription = 'R' Then 'Retailed 30 - 39 Days'
			WHEN Inventory.DaysToSale < 50 AND Sale.SaleDescription = 'R' Then 'Retailed 40 - 49 Days'
			WHEN Inventory.DaysToSale < 60 AND Sale.SaleDescription = 'R' Then 'Retailed 50 - 59 Days'
			WHEN Inventory.DaysToSale > 59 AND Sale.SaleDescription = 'R' Then 'Retailed 60+ Days'
			WHEN Inventory.DaysToSale > 29 AND Sale.SaleDescription = 'W' Then 'Wholesaled 30+ Days'
			ELSE 'NA'
		End 'DaysToSaleGrouping',
		Sale.FrontEndGross,
		Sale.BackEndGross,
		Sale.FrontEndGross + Sale.BackEndGross as 'Gross',
		Sale.SalePrice,
		Inventory.UnitCost,
		Inventory.TradeOrPurchase,
		Inventory.MileageReceived,
		Inventory.InitialVehicleLight,
		Sale.SaleDescription,
		Case Sale.SaleDescription When 'R' Then 1 Else 0 End as SaleDescriptionValue,
		Inventory.StockNumber,
		Inventory.InventoryID
FROM	[FLDW].dbo.Inventory Inventory
JOIN	[FLDW].dbo.VehicleSale Sale on Inventory.InventoryID = Sale.InventoryID
JOIN	[FLDW].dbo.Vehicle Vehicle on Inventory.VehicleID = Vehicle.VehicleID AND Inventory.BusinessUnitID = Vehicle.BusinessUnitID
JOIN	[IMT].dbo.MakeModelGrouping MakeModel on Vehicle.MakeModelGroupingID = MakeModel.MakeModelGroupingID
JOIN	[IMT].dbo.Segment on MakeModel.SegmentID = Segment.SegmentID
CROSS JOIN	[FLDW].dbo.Period_D Period
WHERE	Inventory.BusinessUnitID = @DealerID
AND		Inventory.InventoryType = 2
AND		Period.PeriodID = @PeriodID
AND		Sale.DealDate Between Period.BeginDate And Period.EndDate
AND		(Sale.SaleDescription = 'R' or (Sale.SaleDescription = 'W' and Inventory.DaysToSale >= 30))
AND	    (@StrategyDescription IS NULL OR Sale.SaleDescription = @StrategyDescription)
ORDER BY DaysToSaleGrouping,Inventory.DaysToSale


GO




GRANT EXEC ON [dbo].[rc_GrossProfitByDaysInStockDealLog] TO firstlook 
GO


