

/****** Object:  StoredProcedure [dbo].[Fetch#RetailInventorySalesAnalysisOverview]    Script Date: 08/28/2014 10:44:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Fetch#RetailInventorySalesAnalysisOverview]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Fetch#RetailInventorySalesAnalysisOverview]
GO


/****** Object:  StoredProcedure [dbo].[Fetch#RetailInventorySalesAnalysisOverview]    Script Date: 08/28/2014 10:44:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		Pradnya Sawant
-- Create date: 2014-08-11
-- Last Modified By : Pradnya Sawant
-- Last Modified Date : 2014-08-11
-- Description:	This stored procedure returns the data fro Store Level Retail Inventory Sales Analysis -Overview Data
-- =============================================


CREATE PROCEDURE [dbo].[Fetch#RetailInventorySalesAnalysisOverview]
	@PeriodID INT,
	@SelectedDealerId INT,
	@TradeInType VARCHAR(1)
	
AS

BEGIN



--DECLARE
--	@PeriodID INT=1,
--	@SelectedDealerId INT=106800,
--	@TradeInType VARCHAR(1) = 'A'
DECLARE
	@LocalPeriodID INT,
	@LocalDealerID INT,
	@LocalTradeOrPurchase VARCHAR(1)
	
	SELECT @LocalPeriodID=@PeriodID, @LocalDealerID=@SelectedDealerId, @LocalTradeOrPurchase=@TradeInType

SET NOCOUNT ON;

DECLARE @err INT, @TPFilterLow INT, @TPFilterHigh INT
EXEC @err = Reports.[dbo].[ValidateParameter_TradeOrPurchase] @LocalTradeOrPurchase, @TPFilterLow out, @TPFilterHigh out
IF @err <> 0 GOTO Failed

IF @LocalTradeOrPurchase='A' 
SET @TPFilterHigh=3 --Including 'Unknown' trade purchase type

DECLARE @BeginDate SMALLDATETIME, @EndDate SMALLDATETIME


Select @BeginDate=StartDate,@EndDate=EndDate FROM IMT.dbo.GetDatesFromPeriodId(@LocalPeriodID,1);


SELECT VS.InventoryID AS InventoryID, (CAST(VS.FrontEndGross as float)) AS FrontEndGross, I.DaysToSale AS DaysToSale, VS.SaleDescription INTO #Temp
	FROM [FLDW].dbo.Inventory I
	JOIN [FLDW].dbo.VehicleSale VS on I.InventoryID = VS.InventoryID
	WHERE I.BusinessUnitID = @LocalDealerID
	AND I.InventoryType = 2
	AND VS.DealDate BETWEEN @BeginDate AND @EndDate
	AND     I.TradeOrPurchase BETWEEN @TPFilterLow AND @TPFilterHigh
	AND		I.UnitCost>0
	AND		VS.SalePrice>0
	AND		I.MileageReceived>0;



WITH RetailSales AS
(
	SELECT COUNT(T.InventoryID) AS Units, SUM(CAST(T.FrontEndGross AS FLOAT)) AS RetailFrontEndGross
	FROM #Temp T
	WHERE 
	T.saledescription = 'R'

), AgedWholesaleSales AS
(
	SELECT COUNT(T.InventoryID) AS Units ,SUM(CAST( T.FrontEndGross AS FLOAT)) AS WholesaleFrontEndGross
	FROM #Temp T
	WHERE 
	T.saledescription = 'W' 
	AND T.DaysToSale >= 30
	
)

SELECT
	B.BusinessUnit AS BusinessUnitID
	, CASE @LocalTradeOrPurchase
		WHEN 'A' THEN 'Trades, Purchases And Unknowns'
		WHEN 'T' THEN 'Trades Only'
		WHEN 'P' THEN 'Purchases Only'
	  END AcquisitionType
	--, P.PeriodID
	--, P.BeginDate AS FromDate
	--, P.EndDate AS ToDate
	, ISNULL(R.Units,0) + ISNULL(AW.Units,0) AS TotalUnitsSold-- For us it is Attempted Retail Sales
	, R.Units AS RetailUnitsSold
	, AW.Units AS AgedWholesaleUnitsSold
	, CASE WHEN (ISNULL(R.Units,0) + ISNULL(AW.Units,0)) = 0 
		THEN NULL 
		ELSE R.Units / CAST((R.Units + AW.Units) AS FLOAT) 
	  END RetailPercent                             -- Retailed%
	, CASE WHEN (ISNULL(R.Units,0) + ISNULL(AW.Units,0)) = 0 
		THEN NULL
		ELSE AW.Units / CAST((R.Units + AW.Units) AS FLOAT) 
	  END AgedWholesalePercent                       --Aged Wholesale%
	, (CAST(ISNULL(RetailFrontEndGross,0) AS FLOAT)+ CAST(ISNULL(WholesaleFrontEndGross,0) AS FLOAT)) AS TotalFrontEndGross  
	, CASE WHEN(R.Units = 0) 
		THEN NULL
		ELSE CAST(RetailFrontEndGross AS FLOAT)/CAST(R.Units AS FLOAT)
	  END AS AverageFrontEndGross
	, CASE WHEN(R.Units = 0) 
		THEN NULL
		ELSE (CAST(ISNULL(RetailFrontEndGross,0) AS FLOAT) + CAST(ISNULL(WholesaleFrontEndGross,0) AS FLOAT))/CAST(R.Units AS FLOAT)
	  END AS TrueAverageGross
	, RetailFrontEndGross
	, WholesaleFrontEndGross
FROM [FLDW].dbo.BusinessUnit B
--CROSS JOIN [FLDW].dbo.Period_D P
CROSS JOIN RetailSales R
CROSS JOIN AgedWholesaleSales AW
WHERE B.BusinessUnitID = @LocalDealerID
--AND P.PeriodID = @LocalPeriodID


DROP TABLE #Temp
Failed:
SET NOCOUNT OFF
END

GO

GRANT EXEC ON [Reports].[dbo].[Fetch#RetailInventorySalesAnalysisOverview] TO firstlook 
GO
