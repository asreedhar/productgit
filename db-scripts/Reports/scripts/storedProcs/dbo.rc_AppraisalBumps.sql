
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_AppraisalBumps]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_AppraisalBumps] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_AppraisalBumps] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_AppraisalBumps]
	@PeriodID INT,
	@DealerID INT
AS

SET NOCOUNT ON

SELECT	Appraisal.BusinessUnitID,
		Member.MemberID,
		Member.LastName,
		Member.FirstName,
		Vehicle.VehicleYear,
		MakeModel.Make,
		MakeModel.Model,
		Vehicle.VehicleTrim,
		Appraisal.Color,
		Vehicle.VIN,
		Appraisal.DateCreated              InitialAppraisalDate,
		InitialValuation.[Value]           InitialValue,
		CurrentValuationRecord.DateCreated LastAppraisalDate,
		CurrentValuation.[Value]           LastValue,
		null                               Initials,
		BeginDate                          FromDate,
		EndDate                            ToDate
FROM	[IMT].dbo.Appraisals Appraisal
JOIN	[IMT].dbo.Member Member ON Member.MemberID = Appraisal.MemberID
JOIN	[IMT].dbo.tbl_Vehicle Vehicle ON Vehicle.VehicleID = Appraisal.VehicleID
JOIN	[IMT].dbo.MakeModelGrouping MakeModel ON MakeModel.MakeModelGroupingID = Vehicle.MakeModelGroupingID
LEFT JOIN	[IMT].dbo.AppraisalValues InitialValuation ON InitialValuation.AppraisalID = Appraisal.AppraisalID
LEFT JOIN	[FLDW].dbo.Appraisal_F CurrentValuation on CurrentValuation.AppraisalID = Appraisal.AppraisalID AND Appraisal.AppraisalTypeID = CurrentValuation.AppraisalTypeID 
LEFT JOIN	[IMT].dbo.AppraisalValues CurrentValuationRecord ON CurrentValuationRecord.AppraisalValueID = CurrentValuation.AppraisalValueID
CROSS JOIN	[FLDW].dbo.Period_D Period
WHERE	Appraisal.BusinessUnitID = @DealerID
AND		Member.MemberType != 1
AND		DATEADD(DD, DATEDIFF(DD, 0, Appraisal.DateCreated), 0) BETWEEN Period.BeginDate AND Period.EndDate
AND		(InitialValuation.SequenceNumber IS NULL OR InitialValuation.SequenceNumber = 0)
AND		Period.PeriodID = @PeriodID
AND 	Appraisal.AppraisalTypeID = 1
ORDER
BY		Member.MemberID, Appraisal.DateCreated DESC
GO