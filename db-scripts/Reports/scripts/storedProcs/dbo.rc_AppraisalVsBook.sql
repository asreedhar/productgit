
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_AppraisalVsBook]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_AppraisalVsBook] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_AppraisalVsBook] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_AppraisalVsBook]
	@PeriodID INT,
	@DealerID INT
AS

SET NOCOUNT ON

SELECT	A.AppraisalID,
	B.BusinessUnit 'BusinessUnitID',
	Cast( A.DateCreated as smalldatetime ) AS 'DateCreatedA',
	Case When AV.SequenceNumber is not null Then 0 Else null End AS 'SequenceNumberALV',
	Cast( AV.DateCreated as smalldatetime ) AS 'DateCreatedALV', 
	AV.[Value] AS 'ValueALV',
	'--' InitialsALV,
	BB.[Value] AS 'ValueB',
	M.MemberID, M.LastName, M.FirstName,
	UPPER( V.VIN ) VIN,
	V.VehicleYear,
	V.VehicleTrim,
	V.BaseColor,
	DB.Segment DisplayBodyType,
	MM.Make,
	MM.Model,
	AA.EstimatedReconditioningCost,
	T.Description 'TimePeriodDescription'
	
FROM		[IMT].dbo.Appraisals A
JOIN		[IMT].dbo.BusinessUnit B ON B.BusinessUnitID = A.BusinessUnitID
LEFT JOIN	[FLDW].dbo.Appraisal_F ALV ON ALV.AppraisalID = A.AppraisalID AND ALV.BusinessUnitID = A.BusinessUnitID
LEFT JOIN	[IMT].dbo.AppraisalValues AV ON AV.AppraisalValueID = ALV.AppraisalValueID AND AV.AppraisalID = ALV.AppraisalID
JOIN		[IMT].dbo.DealerPreference P ON P.BusinessUnitID = A.BusinessUnitID
JOIN		[FLDW].dbo.AppraisalBookout_F BB ON (
						BB.AppraisalID = A.AppraisalID
						AND	BB.BusinessUnitID = A.BusinessUnitID
						AND	BB.BookoutValueTypeID = 2
						AND	BB.ThirdPartyCategoryID = P.BookoutPreferenceID)
JOIN		[IMT].dbo.Member M ON M.MemberID = A.MemberID
JOIN		[IMT].dbo.tbl_Vehicle V on V.VehicleID = A.VehicleID
JOIN		[IMT].dbo.MakeModelGrouping MM on MM.MakeModelGroupingID = V.MakeModelGroupingID
JOIN		[IMT].dbo.Segment DB on DB.SegmentID = MM.SegmentID
CROSS JOIN	[FLDW].dbo.Period_D T
JOIN  		[IMT].dbo.AppraisalActions AA ON AA.AppraisalID = A.AppraisalID
WHERE	A.BusinessUnitID = @DealerID
AND	M.MemberType != 1
AND	T.PeriodID = @PeriodID
AND	A.DateCreated BETWEEN T.BeginDate and T.EndDate
AND	A.AppraisalTypeID = 1

GO