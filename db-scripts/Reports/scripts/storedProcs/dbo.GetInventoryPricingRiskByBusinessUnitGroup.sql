IF objectproperty(object_id('dbo.GetInventoryPricingRiskByBusinessUnitGroup'), 'isProcedure') = 1
    DROP PROCEDURE dbo.GetInventoryPricingRiskByBusinessUnitGroup

GO

CREATE PROCEDURE dbo.GetInventoryPricingRiskByBusinessUnitGroup
@BusinessUnitGroupId INT,  --BusinessUnitId of the BusinessUnitGroup
@SaleStrategy CHAR, --'A' for All, 'R' for Retail
@DebugMode bit = 0 --In Debug Mode additional recordsets will be returned

AS
/******************************************************************************
**
**  Procedure: dbo.GetInventoryPricingRiskByBusinessUnitGroup
**  Description: For a provided BusinessUnitGroup, returns Inventory by Pricing Risk 
**  for both the BusinessUnitGroup and individual child BusinessUnits
**        
**
**  Return values:  0 on Success, -2 on Error
** 
**  Input parameters:
**		@BusinessUnitId INT,  --BusinessUnitId of the BusinessUnitGroup
**		@SaleStrategy CHAR, --'A' for All, 'R' for Retail
**		@DebugMode bit = 0 --In Debug Mode additional recordsets will be returned
**
**  Output parameters:  none
**
**  Rows returned: none
**	RS 1	BusinessUnitId			INT
**		TotalInventoryCnt		INT
**		TotalUnitCost			INT
**		AvgPctMarketPrice		INT
**		AvgMarketSalePrice		INT
**
**		UnderpricedUnitCost		INT
**		UnderpricedPctAvgMarketPrice	INT
**		UnderpricedAvgMarketSalePrice	INT
**		UnderpricedInventoryCnt		INT
**
**		OverpricedUnitCost		INT
**		OverpricedPctAvgMarketPrice	INT
**		OverpricedAvgMarketSalePrice	INT
**		OverrpricedInventoryCnt		INT
**
**		MarketpricedUnitCost		INT
**		MarketpricedPctAvgMarketPrice	INT
**		MarketpricedAvgMarketSalePrice	INT
**		MarketpricedInventoryCnt	INT
**
**		ZeropricedUnitCost		INT
**		ZeropricedPctAvgMarketPrice	INT
**		ZeropricedAvgMarketSalePrice	INT
**		ZeropricedInventoryCnt		INT
**
**		NotAnalyzedUnitCost		INT
**		NotAnalyzedPctAvgMarketPrice	INT
**		NotAnalyzedAvgMarketSalePrice	INT
**		NotAnalyzedInventoryCnt		INT
**
**	RS 2	BusinessUnitID		INT
**		RiskDescription		VARCHAR(100)
**		RiskBucketId		INT
**		InventoryCnt		INT
**		AvgPctMarketPrice	INT
**		TotalUnitCost		INT
**		AvgMarketSalePrice	INT
**
**	RS 3-6  Only Present when DebugMode = 1
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  2008.06.19  ffoiii    Procedure created.
**
*******************************************************************************/
--DEFAULT ENVIRONMENT AND VARIABLE SETTINGS
SET NOCOUNT ON
SET ANSI_NULLS ON 

DECLARE @sErrMsg VARCHAR(255)
DECLARE	@bLocalTran bit
DECLARE	@sprocname sysname
DECLARE	@bTrue bit
DECLARE	@bFalse bit
SELECT	@sprocname = object_name(@@procid),
	@bTrue = 1,
	@bFalse = 0,
	@sErrMsg = ''
SET	@bLocalTran = @bFalse
--END DEFAULT ENVIRONMENT AND VARIABLE SETTINGS

DECLARE @BusinessUnitId INT

--create temp tables used in processing
CREATE TABLE #Inventory (
	BusinessUnitId		INT,
	AgeIndex		TINYINT,
	RiskIndex		TINYINT,
	IsOverridden		BIT,
	VehicleEntityID		INT,
	ListPrice		INT,
	UnitCost		INT,
	PctAvgMarketPrice	INT,
	AvgMarketSalePrice	INT,
	MarketDaysSupply	INT,
)
CREATE TABLE #Children (BusinessUnitId int)
CREATE TABLE #PowerRegion (PowerRegionId INT)
CREATE TABLE #MarketPricingBucket (
	BusinessUnitId INT,
	MarketPricingBucketID tinyint, 
	LowAge tinyint, 
	HighAge tinyint null, 
	MinMarketPct tinyint null, 
	MaxMarketPct tinyint null, 
	MinGross int NULL
)
--end temp tables used in processing


--Get all child BusinessUnits for the current BusinessUnitGroup
INSERT INTO #Children (BusinessUnitId)
SELECT child.BusinessUnitId
FROM IMT.dbo.BusinessUnit parent 
	INNER JOIN IMT.dbo.BusinessUnitRelationship bur on bur.ParentId = Parent.BusinessUnitId
	INNER JOIN IMT.dbo.BusinessUnit child on child.BusinessUnitId = bur.BusinessUnitId
WHERE parent.Active = 1
	AND child.Active = 1
	AND parent.BusinessUnitId = @BusinessUnitGroupId

--JDPower data is aggregated and multiplied by a constant in order to create "reasonable" numbers for the DaysSupply metric.
--As such, I use a derived table within the main query in order to include JDPower data
--Market.JdPower.UsedSales_A1#[0,1] contains 1M+ records, but after filtering by PowerRegionId and PeriodId (maybe restructure to key off PeriodId first?)
--only 60k or so records are read
INSERT INTO #PowerRegion (PowerRegionId)
SELECT DISTINCT op.PowerRegionId
FROM #Children c inner join Market.Pricing.Owner o on o.OwnerEntityId = c.BusinessUnitId
	INNER JOIN Market.Pricing.OwnerPreference op on op.OwnerId = o.OwnerId
WHERE o.OwnerTypeId = 1 --Dealer

--initialize @BusinessUnitId
SET @BusinessUnitId = 0

--for all child BusinessUnits, execute Market.Pricing.Load#Inventory
WHILE (1=1)
	BEGIN
		--get next businessunit to process
		SELECT @BusinessUnitId = MIN(child.BusinessUnitId)
		FROM #Children child
		WHERE child.BusinessUnitId > @BusinessUnitId
		
		--If no additional BusinessUnitIds to process, break loop
		IF (@BusinessUnitId is null) BREAK
		
		--Min/Max MarketPct are a function of BusinessUnit and AgeBucket and both the buckets
		--and thresholds can be customized for each BusinessUnit
		INSERT
		INTO	#MarketPricingBucket (BusinessUnitId, MarketPricingBucketId, LowAge, HighAge, MinMarketPct, MaxMarketPct, MinGross)
		SELECT	BusinessUnitId		= @BusinessUnitId,
			MarketPricingBucketID	= IBR.RangeID,
			LowAge			= IBR.Low,
			HighAge			= IBR.High,
			MinMarketPct		= IBR.Value1,
			MaxMarketPct		= IBR.Value2,
			MinGross		= IBR.Value3 
		FROM	IMT.dbo.GetInventoryBucketRanges(@BusinessUnitId, 7) IBR  --2nd Parameter is @InventoryBucketId reference IMT.dbo.InventoryBuckets
	END

--load Inventory data for all BusinessUnits
INSERT INTO #Inventory (BusinessUnitId, AgeIndex, RiskIndex, VehicleEntityId, ListPrice, UnitCost, PctAvgMarketPrice, AvgMarketSalePrice, MarketDaysSupply)
SELECT	BusinessUnitId = i.BusinessUnitId,
	AgeIndex  = MPB.MarketPricingBucketID, 
	RiskIndex = CASE WHEN DI.CategorizationOverrideExpiryDate > GETDATE() THEN 2 ELSE 
			CASE WHEN COALESCE(I.ListPrice,0.0) = 0.0 THEN 4 --Zero/NoPrice
				WHEN ((SRF.SearchID IS NULL) OR (SRF.Units < 5) OR (I.ListPrice > SRF.AvgListPrice * OP.ExcludeHighPriceOutliersMultiplier) OR (I.ListPrice < SRF.AvgListPrice * OP.ExcludeLowPriceOutliersMultiplier)) THEN 5 --Not Analyzed
				WHEN ROUND(I.ListPrice/SRF.AvgListPrice * 100,0) < MPB.MinMarketPct THEN 1 --Underpriced
				WHEN ROUND(I.ListPrice/SRF.AvgListPrice * 100,0) > MPB.MaxMarketPct THEN 3 --Overpriced
				ELSE 2 --MarketPriced
			END END,
	--IsOverridden      = CASE WHEN DI.CategorizationOverrideExpiryDate > GETDATE() THEN 1 ELSE 0 END,
	InventoryID       = I.InventoryID,
	ListPrice         = ROUND(I.ListPrice,0),
	UnitCost	= ROUND(I.UnitCost,0),
	PctAvgMarketPrice = CASE WHEN ((SRF.SearchID IS NULL) OR (SRF.Units < 5)  OR (I.ListPrice > SRF.AvgListPrice * OP.ExcludeHighPriceOutliersMultiplier) OR (I.ListPrice < SRF.AvgListPrice * OP.ExcludeLowPriceOutliersMultiplier)) THEN NULL
				 ELSE ROUND(COALESCE(I.ListPrice,0.0) / SRF.AvgListPrice * 100,0)
				END,
	AvgMarketSalePrice = SRF.AvgListPrice,
	MarketDaysSupply = CASE WHEN NULLIF(JSR.Rate,0) IS NULL THEN NULL ELSE CAST((CAST(SRF.Units AS REAL) / CAST(JSR.Rate AS REAL)) AS INT) END
FROM	#Children c 
	INNER JOIN FLDW.dbo.InventoryActive I ON i.BusinessUnitId = c.BusinessUnitId
	INNER JOIN FLDW.dbo.Vehicle V ON I.BusinessUnitID = V.BusinessUnitID AND I.VehicleID = V.VehicleID
	INNER JOIN IMT.dbo.MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID
	INNER JOIN #MarketPricingBucket MPB ON mpb.BusinessUnitId = c.BusinessUnitId and (CASE WHEN DATEDIFF(DD, InventoryReceivedDate, GETDATE()) > 0 THEN DATEDIFF(DD, InventoryReceivedDate, GETDATE()) ELSE 1 END) BETWEEN MPB.LowAge AND COALESCE(MPB.HighAge,9999)
	INNER JOIN Market.Pricing.Owner o ON o.OwnerEntityId = c.BusinessUnitId and o.OwnerTypeId = 1 -- Dealer
	LEFT JOIN Market.Pricing.Search S ON --S.OwnerId = @OwnerId
		S.OwnerId = o.OwnerId
		 AND I.InventoryID = S.VehicleEntityID AND S.VehicleEntityTypeID = 1
	LEFT JOIN Market.Pricing.SearchResult_F SRF ON S.SearchID = SRF.SearchID AND S.SearchTypeID = SRF.SearchTypeID
	LEFT JOIN Market.Pricing.OwnerPreference OP ON S.OwnerID = OP.OwnerID
	-- note: jd power covers 20% of dealers. we need to expand sales by 5 to get 100% of sold units. yup, it's a fudge.
	LEFT JOIN (
		SELECT	s.PowerRegionId, S.ModelID, S.ModelYear, AvgSellingPrice = AVG(S.AvgSellingPrice), Rate = CAST(5*SUM(S.Units) AS DECIMAL(9,2)) / CAST(P.DaysInPeriod AS DECIMAL(9,2))
		FROM	Market.JDPower.UsedSalesByModelYear S
			INNER JOIN Market.JDPower.Period_D P ON S.PeriodID = P.PeriodID
			INNER JOIN #PowerRegion pr ON pr.PowerRegionId = s.PowerRegionId --performance optimization
		WHERE	P.PeriodID = 3  --Previous six months
		GROUP
		BY	s.PowerRegionId, S.ModelID, S.ModelYear, P.DaysInPeriod
	) JSR ON JSR.ModelYear = V.VehicleYear AND JSR.ModelID = MMG.ModelID and jsr.PowerRegionId = op.PowerRegionId

	LEFT JOIN Market.Pricing.VehiclePricingDecisionInput DI
		ON O.OwnerID = DI.OwnerID AND DI.VehicleEntityTypeID = 1 AND DI.VehicleEntityID = I.InventoryID
WHERE	I.InventoryType = 2	-- USED

--if SaleStrategey is retail, remove non-retail vehicles
IF (@SaleStrategy = 'R')
	DELETE	I
	FROM	#Inventory i
	WHERE	NOT EXISTS (	SELECT	1 
		FROM	[IMT]..AIP_Event AE 
			JOIN [IMT]..AIP_EventType AET ON AE.AIP_EventTypeID = AET.AIP_EventTypeID
		WHERE	AE.InventoryID = I.VehicleEntityID
			AND AET.AIP_EventCategoryID = 1		-- Retail
			AND GETDATE() BETWEEN AE.BeginDate AND COALESCE(AE.EndDate, '6/6/2079')
	)


--aggregate by BusinessUnitId
--RS 1
SELECT	c.BusinessUnitId,
	
	SUM(CASE WHEN I.BusinessUnitId IS NULL THEN 0 ELSE 1 end) InventoryCnt,
	AVG(PctAvgMarketPrice) AvgPctMarketPrice,
	SUM(UnitCost) TotalUnitCost,
	AVG(AvgMarketSalePrice) AvgMarketSalePrice,
	
	SUM(CASE WHEN rb.Description = 'Underpricing Risk' then UnitCost ELSE 0 END) UnderpricedUnitCost,
	AVG(CASE WHEN rb.Description = 'Underpricing Risk' then PctAvgMarketPrice ELSE NULL END) UnderpricedPctAvgMarketPrice,
	AVG(CASE WHEN rb.Description = 'Underpricing Risk' then AvgMarketSalePrice ELSE NULL END) UnderpricedAvgMarketSalePrice,
	SUM(CASE WHEN rb.Description = 'Underpricing Risk' then 1 ELSE 0 END) UnderpricedInventoryCnt,

	SUM(CASE WHEN rb.Description = 'Overpricing Risk' then UnitCost ELSE 0 END) OverpricedUnitCost,
	AVG(CASE WHEN rb.Description = 'Overpricing Risk' then PctAvgMarketPrice ELSE NULL END) OverpricedPctAvgMarketPrice,
	AVG(CASE WHEN rb.Description = 'Overpricing Risk' then AvgMarketSalePrice ELSE NULL END) OverpricedAvgMarketSalePrice,
	SUM(CASE WHEN rb.Description = 'Overpricing Risk' then 1 ELSE 0 END) OverpricedInventoryCnt,

	SUM(CASE WHEN rb.Description = 'Priced at Market' then UnitCost ELSE 0 END) MarketpricedUnitCost,
	AVG(CASE WHEN rb.Description = 'Priced at Market' then PctAvgMarketPrice ELSE NULL END) MarketpricedPctAvgMarketPrice,
	AVG(CASE WHEN rb.Description = 'Priced at Market' then AvgMarketSalePrice ELSE NULL END) MarketpricedAvgMarketSalePrice,
	SUM(CASE WHEN rb.Description = 'Priced at Market' then 1 ELSE 0 END) MarketpricedInventoryCnt,

	SUM(CASE WHEN rb.Description = 'Zero/No Price' then UnitCost ELSE 0 END) ZeropricedUnitCost,
	AVG(CASE WHEN rb.Description = 'Zero/No Price' then PctAvgMarketPrice ELSE NULL END) ZeropricedPctAvgMarketPrice,
	AVG(CASE WHEN rb.Description = 'Zero/No Price' then AvgMarketSalePrice ELSE NULL END) ZeropricedAvgMarketSalePrice,
	SUM(CASE WHEN rb.Description = 'Zero/No Price' then 1 ELSE 0 END) ZeropricedInventoryCnt,

	SUM(CASE WHEN rb.Description = 'Not Analyzed' then UnitCost ELSE 0 END) NotAnalyzedUnitCost,
	AVG(CASE WHEN rb.Description = 'Not Analyzed' then PctAvgMarketPrice ELSE NULL END) NotAnalyzedPctAvgMarketPrice,
	AVG(CASE WHEN rb.Description = 'Not Analyzed'  then AvgMarketSalePrice ELSE NULL END) NotAnalyzedAvgMarketSalePrice,
	SUM(CASE WHEN rb.Description = 'Not Analyzed' then 1 ELSE 0 END) NotAnalyzedInventoryCnt
	
FROM #Children c 
	LEFT JOIN #Inventory i on i.BusinessUnitId = c.BusinessUnitId
	LEFT JOIN Market.Pricing.RiskBucket rb ON rb.RiskBucketId = i.RiskIndex
GROUP BY c.BusinessUnitId
ORDER BY c.BusinessUnitId

--Aggregate by BusinessUnitId and Risk Index
--RS 2
SELECT	c.BusinessUnitId, 
	rb.Description RiskDescription,
	rb.RiskBucketId,
	sum(case when i.BusinessUnitId is null then 0 else 1 end ) InventoryCnt,
	avg(i.PctAvgMarketPrice) AvgPctMarketPrice,
	sum(UnitCost) TotalUnitCost,
	avg(AvgMarketSalePrice) AvgMarketSalePrice
FROM #Children c cross join Market.Pricing.RiskBucket rb
	left join #Inventory i on i.BusinessUnitId = c.BusinessUnitId and i.RiskIndex = rb.RiskBucketId
GROUP BY c.BusinessUnitId, rb.Description, rb.RiskBucketId
ORDER BY c.BusinessUnitId, rb.Description


--cleanup
DROP TABLE #Inventory
DROP TABLE #Children
DROP TABLE #MarketPricingBucket
DROP TABLE #PowerRegion

--debug
IF (@DebugMode = @bTrue)
	BEGIN
		SELECT * FROM #Children
		SELECT * FROM #PowerRegion
		SELECT * FROM #MarketPricingBucket
		SELECT * FROM #Inventory
	END
--exit the procedure
RETURN 0

--Error encountered
Failed:
IF (@bLocalTran = @bTrue)
	BEGIN
		ROLLBACK TRAN @sprocname
	END
IF (@sErrMsg = '')
	BEGIN
		SET @sErrMsg = 'Unspecified Error.'
	END
EXEC DBASTAT.dbo.Log_Event @Log_Code = 'E', @sp_name = @sprocname, @Log_Text = @sErrMsg, @Originating_Log_ID = null
RETURN -2

GO

