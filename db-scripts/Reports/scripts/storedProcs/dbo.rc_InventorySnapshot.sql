IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_InventorySnapshot]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_InventorySnapshot] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_InventorySnapshot] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_InventorySnapshot]
---Parameters-------------------------------------------------------------------------------------
--
	@DealerGroupID INT,
	@DealerID INT,
	@MemberID INT,
	@Mode VARCHAR(12)
--
---History--------------------------------------------------------------------------------------
--
--	JMW	08/08/2012	Added history section...Modified days supply -- BUGZID: 22331
--
---Notes----------------------------------------------------------------------------------------
--	BusinessUnitID = 0 --> All
--	VehicleSegmentID = 0 --> All
--	VehicleGroupingID = -1 --> All
------------------------------------------------------------------------------------------------
AS

SET NOCOUNT ON;

DECLARE @MemberAccessTable TABLE (
	BusinessUnitID INT
)

IF(@Mode = 'DealerGroup')
BEGIN
	WITH MemberBusinessUnitSet AS (
		SELECT I.BusinessUnitID
		FROM [IMT].dbo.MemberBusinessUnitSet S
		JOIN [IMT].dbo.MemberBusinessUnitSetItem I
			ON S.MemberBusinessUnitSetID = I.MemberBusinessUnitSetID
		WHERE S.MemberID = @MemberID
		AND S.MemberBusinessUnitSetTypeID = 1 --Command Center Dealer Group Set
	)
	INSERT INTO @MemberAccessTable (BusinessUnitID)
	SELECT MBUS.BusinessUnitID
	FROM dbo.MemberAccess(@DealerGroupID, @MemberID) MA
	JOIN MemberBusinessUnitSet MBUS ON MA.BusinessUnitID = MBUS.BusinessUnitID
END
ELSE
BEGIN
	INSERT INTO @MemberAccessTable (BusinessUnitID) VALUES (@DealerID)
END


SELECT
	-- a
	a.BusinessUnitID BusinessUnitIdA,
	DS.BusinessUnit,
	a.MetricID MetricIdA,
	Cast( a.Measure as int ) MeasureA,
	a.[Desc] DescA,
	-- b
	b.BusinessUnitID BusinessUnitIdB,
	b.MetricID MetricIdB ,
	Cast( b.Measure as int ) MeasureB,
	b.[Desc] DescB,
	( a.Measure / Case When b.Measure = 0 Then null Else b.Measure End ) 'Pct',
	( 1 - ( a.Measure / Case When b.Measure = 0 Then null Else b.Measure End )  ) 'Pct2',
	-- c
	c.Measure Measure244 ,
	d.Measure Measure245 ,
	(c.Measure / Case When (c.Measure + d.Measure ) = 0 Then null Else (c.Measure + d.Measure ) End ) 'PctDue',
	e.Measure 'Measure299',
	f.Measure 'Measure301',
	g.Measure 'Measure303',
	h.Measure 'Measure305',
	i.Measure 'Measure307',
	k.Measure 'Measure313',
	l.Measure 'Measure314',
	m.Measure 'Measure306',
	n.Measure 'Measure304',
	o.Measure 'Measure302',
	p.Measure 'Measure300',
	j.DaysSupply,
	DG.BusinessUnit 'Parent'

FROM	[IMT].dbo.BusinessUnit DS
JOIN	@MemberAccessTable MA ON MA.BusinessUnitID = DS.BusinessUnitID
JOIN	[IMT].dbo.BusinessUnitRelationship DR ON DR.BusinessUnitID = DS.BusinessUnitID
JOIN	[IMT].dbo.BusinessUnit DG ON DG.BusinessUnitID = DR.ParentID
JOIN	[IMT].dbo.DealerPreference DP ON DP.BusinessUnitID = DS.BusinessUnitID
JOIN	(
			SELECT	BusinessUnitID, SUM(Active) NumerOfActiveUpgrades
			FROM	[IMT].dbo.DealerUpgrade
			GROUP
			BY		BusinessUnitID
		) DU ON DU.BusinessUnitID = DS.BusinessUnitID	

LEFT JOIN
	(
	SELECT	BusinessUnitID, MetricID, Measure, 'Pricing Evaluation' 'Desc'
	FROM	[EdgeScorecard].dbo.Measures
	WHERE	PeriodID = 7
	AND		SetID = 126
	AND		MetricID = 248
	) a ON ( DS.BusinessUnitID = a.BusinessUnitID )

LEFT JOIN
	(
	SELECT	BusinessUnitID, MetricID, Measure, 'Retailed Pricing Evaluation' 'Desc'
	FROM	[EdgeScorecard].dbo.Measures
	WHERE	PeriodID = 7
	AND		SetID = 126
	AND		MetricID = 247
	) b ON ( DS.BusinessUnitID = b.BusinessUnitID )

LEFT JOIN
	(
	SELECT	BusinessUnitID, Measure
	FROM	[EdgeScorecard].dbo.Measures
	WHERE	PeriodID = 7 
	AND		SetID = 125
	AND		MetricID = 244
	) c ON ( DS.BusinessUnitID = c.BusinessUnitID )

LEFT JOIN
	(
	SELECT	BusinessUnitID, Measure
	FROM	[EdgeScorecard].dbo.Measures
	WHERE	PeriodID = 7 
	AND		SetID = 125
	AND		MetricID = 245
	) d ON ( DS.BusinessUnitID = d.BusinessUnitID )

LEFT JOIN
	(
	SELECT	BusinessUnitID, Measure
	FROM	[EdgeScorecard].dbo.Measures
	WHERE	PeriodID = 7 
	AND		SetID = 144
	AND		MetricID = 299
	) e ON ( DS.BusinessUnitID = e.BusinessUnitID )

LEFT JOIN
	(
	SELECT	BusinessUnitID, Measure
	FROM	[EdgeScorecard].dbo.Measures
	WHERE	PeriodID = 7 
	AND		SetID = 145
	AND		MetricID = 301
	) f ON ( DS.BusinessUnitID = f.BusinessUnitID )

LEFT JOIN
	(
	SELECT	BusinessUnitID, Measure
	FROM	[EdgeScorecard].dbo.Measures
	WHERE	PeriodID = 7 
	AND		SetID = 146
	AND		MetricID = 303
	) g ON ( DS.BusinessUnitID = g.BusinessUnitID )

LEFT JOIN
	(
	SELECT	BusinessUnitID, Measure
	FROM	[EdgeScorecard].dbo.Measures
	WHERE	PeriodID = 7 
	AND		SetID = 147
	AND		MetricID = 305
	) h ON ( DS.BusinessUnitID = h.BusinessUnitID )

LEFT JOIN
	(
	SELECT	BusinessUnitID, Measure
	FROM	[EdgeScorecard].dbo.Measures
	WHERE	PeriodID = 7 
	AND		SetID = 148
	AND		MetricID = 307
	) i ON ( DS.BusinessUnitID = i.BusinessUnitID )

LEFT JOIN
	(
	SELECT	BusinessUnitID, DaysSupply
	FROM	[FLDW].dbo.DaysSupply_A
	WHERE	VehicleSegmentID = 0 AND
			VehicleGroupingID = -1 
	) j on ( DS.BusinessUnitID = j.BusinessUnitID )

LEFT JOIN
	(
	SELECT	BusinessUnitID, Measure
	FROM	[EdgeScorecard].dbo.Measures
	WHERE	PeriodID = 7 
	AND		SetID = 151
	AND		MetricID = 313
	) k ON ( DS.BusinessUnitID = k.BusinessUnitID )

LEFT JOIN
	(
	SELECT	BusinessUnitID, Measure
	FROM	[EdgeScorecard].dbo.Measures
	WHERE	PeriodID = 7 
	AND		SetID = 151
	AND		MetricID = 314
	) l ON ( DS.BusinessUnitID = l.BusinessUnitID )

LEFT JOIN
	(
	SELECT	BusinessUnitID, Measure
	FROM	[EdgeScorecard].dbo.Measures
	WHERE	PeriodID = 7 
	AND		SetID = 147
	AND		MetricID = 306
	) m ON ( DS.BusinessUnitID = m.BusinessUnitID )

LEFT JOIN
	(
	SELECT	BusinessUnitID, Measure
	FROM	[EdgeScorecard].dbo.Measures
	WHERE	PeriodID = 7
	AND		SetID = 146
	AND		MetricID = 304
	) n ON ( DS.BusinessUnitID = n.BusinessUnitID )

LEFT JOIN
	(
	SELECT	BusinessUnitID, Measure
	FROM	[EdgeScorecard].dbo.Measures
	WHERE	PeriodID = 7 
	AND		SetID = 145
	AND		MetricID = 302
	) o ON ( DS.BusinessUnitID = o.BusinessUnitID )

LEFT JOIN
	(
	SELECT	BusinessUnitID, Measure
	FROM	EdgeScorecard..Measures
	WHERE	PeriodID = 7 
	AND		SetID = 144
	AND		MetricID = 300
	) p ON ( DS.BusinessUnitID = p.BusinessUnitID )

WHERE	DS.Active = 1
AND		DP.GoLiveDate IS NOT NULL
AND		DS.BusinessUnitTypeID = 4
AND		DU.NumerOfActiveUpgrades > 0

ORDER BY DS.BusinessUnit
GO
