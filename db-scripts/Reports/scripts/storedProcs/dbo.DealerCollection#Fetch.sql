IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.DealerCollection#Fetch') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE dbo.DealerCollection#Fetch AS SELECT 1')
GO
 
ALTER PROCEDURE [dbo].[DealerCollection#Fetch]

AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.DealerCollection#Fetch.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns DealerID and DealerName from IMT..BusinessUnit
 * 
 * Parameters
 * ----------
 * 
 * Exceptions
 * ----------
 *
 * History
 * ----------
 * 
 * BYF	2009-09-29
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON;

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT 
	BusinessUnitID AS DealerID, 
	BusinessUnit AS Dealer
FROM [IMT].dbo.BusinessUnit
WHERE BusinessUnitTypeID = 4 --dealer
AND Active = 1
ORDER BY BusinessUnit ASC

RETURN 0

GO