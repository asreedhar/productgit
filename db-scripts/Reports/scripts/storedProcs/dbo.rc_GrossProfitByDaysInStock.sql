
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_GrossProfitByDaysInStock]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_GrossProfitByDaysInStock] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_GrossProfitByDaysInStock] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_GrossProfitByDaysInStock]
	@PeriodID INT,
	@DealerGroupID INT,
	@MemberID INT
AS

/******************************************************************************************************************
*	Routine:	dbo.GrossProfitByDaysInStock
*	Purpose:	Driver routine for Gross Profit by Days in Stock (DealerGroup level)
*
*	History
*	2/22/2010	SVU	Initial Creation
*******************************************************************************************************************/
SET NOCOUNT ON ;

DECLARE @MemberAccessTable TABLE (BusinessUnitID INT)
                
;
WITH    MemberBusinessUnitSet
          AS (SELECT    I.BusinessUnitID
              FROM      [IMT].dbo.MemberBusinessUnitSet S
              JOIN      [IMT].dbo.MemberBusinessUnitSetItem I ON S.MemberBusinessUnitSetID = I.MemberBusinessUnitSetID
              WHERE     S.MemberID = @MemberID
                        AND S.MemberBusinessUnitSetTypeID = 1 --Command Center Dealer Group Set
                        
             )
        INSERT  INTO @MemberAccessTable
                (BusinessUnitID)
                SELECT  DISTINCT MBUS.BusinessUnitID
                FROM    dbo.MemberAccess(@DealerGroupID, @MemberID) MA
                JOIN    MemberBusinessUnitSet MBUS ON MA.BusinessUnitID = MBUS.BusinessUnitID

SELECT  S.DealDate ,
	V.VehicleYear ,
	MakeModel.[Make] ,
	MakeModel.[Model] ,
	Segment.Segment DisplayBodyType ,
	V.VehicleTrim ,
	V.BaseColor ,
	I.DaysToSale ,
	CASE	WHEN I.DaysToSale < 22 AND S.SaleDescription = 'R' THEN 'Retailed 0 - 21 Days'
		WHEN I.DaysToSale < 30 AND S.SaleDescription = 'R' THEN 'Retailed 22 - 29 Days'
		WHEN I.DaysToSale < 40 AND S.SaleDescription = 'R' THEN 'Retailed 30 - 39 Days'
		WHEN I.DaysToSale < 50 AND S.SaleDescription = 'R' THEN 'Retailed 40 - 49 Days'
		WHEN I.DaysToSale < 60 AND S.SaleDescription = 'R' THEN 'Retailed 50 - 59 Days'
		WHEN I.DaysToSale > 59 AND S.SaleDescription = 'R' THEN 'Retailed 60+ Days'
		WHEN I.DaysToSale > 29 AND S.SaleDescription = 'W' THEN 'Wholesaled 30+ Days'
		ELSE 'NA'
	END 'DaysToSaleGrouping' ,
	S.FrontEndGross ,
	S.BackEndGross ,
	S.FrontEndGross + S.BackEndGross AS 'Gross' ,
	S.SalePrice ,
	I.UnitCost ,
	I.TradeOrPurchase ,
	I.MileageReceived ,
	I.InitialVehicleLight ,
	S.SaleDescription ,
	CASE S.SaleDescription
		WHEN 'R' THEN 1
		ELSE 0
	END AS SaleDescriptionValue ,
	I.StockNumber ,
	I.InventoryID,
	BU.BusinessUnit
FROM    [FLDW].dbo.Inventory I WITH (NOLOCK)
JOIN    [FLDW].dbo.VehicleSale S WITH (NOLOCK) ON I.InventoryID = S.InventoryID
JOIN    [FLDW].dbo.Vehicle V WITH (NOLOCK) ON I.VehicleID = V.VehicleID
                                      AND I.BusinessUnitID = V.BusinessUnitID
JOIN    [IMT].dbo.MakeModelGrouping MakeModel WITH (NOLOCK) ON V.MakeModelGroupingID = MakeModel.MakeModelGroupingID
JOIN    [IMT].dbo.Segment WITH (NOLOCK) ON MakeModel.SegmentID = Segment.SegmentID
JOIN    @MemberAccessTable MAT ON MAT.BusinessUnitID = I.BusinessUnitID
JOIN	IMT.dbo.BusinessUnit BU WITH (NOLOCK) ON BU.BusinessUnitID = I.BusinessUnitID
CROSS JOIN [FLDW].dbo.Period_D Period WITH (NOLOCK)
WHERE   I.InventoryType = 2
	AND Period.PeriodID = @PeriodID
	AND S.DealDate BETWEEN Period.BeginDate AND Period.EndDate
	AND (S.SaleDescription = 'R' OR (S.SaleDescription = 'W' AND I.DaysToSale >= 30))

GO
