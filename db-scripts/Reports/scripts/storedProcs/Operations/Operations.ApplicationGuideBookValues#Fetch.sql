IF objectproperty(object_id('[Operations].[ApplicationGuideBookValues#Fetch]'), 'isProcedure') = 1
    DROP PROCEDURE [Operations].[ApplicationGuideBookValues#Fetch]

GO
 
CREATE PROCEDURE [Operations].[ApplicationGuideBookValues#Fetch]
--------------------------------------------------------------------------------
--	Routine:	[Operations].[ApplicationGuideBookValues#Fetch]
--	Purpose:	Provide data for the Application Guide Book Values Report
--
---History----------------------------------------------------------------------
--	
--	SVU	11/12/2009	Initial Creation
--	SVU	12/14/2009	Revised to use the View, which was created for SalesForce
--------------------------------------------------------------------------------

AS

SET NOCOUNT ON

SELECT * FROM [Operations].[ApplicationGuideBookValues]

GO

GRANT EXECUTE ON [Operations].[ApplicationGuideBookValues#Fetch] TO [SalesForceExtractorService]
GRANT EXECUTE ON [Operations].[ApplicationGuideBookValues#Fetch] TO [CustomerOperations]