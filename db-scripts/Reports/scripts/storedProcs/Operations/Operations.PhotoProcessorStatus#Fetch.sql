
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operations].[PhotoProcessorStatus#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Operations].[PhotoProcessorStatus#Fetch] AS SELECT 1')
GO

GRANT EXECUTE ON [Operations].[PhotoProcessorStatus#Fetch] TO [StoredProcedureUser]
GO
GRANT EXECUTE ON [Operations].[PhotoProcessorStatus#Fetch] TO [CustomerOperations]
GO

ALTER PROCEDURE [Operations].[PhotoProcessorStatus#Fetch]
AS

/* --------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * Status of the Photo Processor queues
 *
 * Powers a SSRS report for QA and OPs purposes
 *
 *
 * Parameters
 * ----------
 * 
 * Exceptions
 * ----------
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

SELECT 
	PSC.PhotoStatusCode, 
	PSC.Description, 
	MAX(P.DateCreated) LatestDateCreated, 
	CAST(NULL AS DATETIME) LatestDateUpdated, 
	COUNT(*) NumInStatus,
	CASE	WHEN PSC.PhotoStatusCode IN (1, 3, 7) THEN 'Scraping' 
			WHEN PSC.PhotoStatusCode IN (6, 8) THEN 'Prune (delete)' 
			ELSE '' 
	END AS PhotoProcessorOperation
FROM IMT.dbo.Photos P WITH(NOLOCK)
JOIN IMT.dbo.PhotoStatus PSC WITH(NOLOCK)
	ON P.PhotoStatusCode = PSC.PhotoStatusCode
GROUP BY PSC.PhotoStatusCode, PSC.Description
ORDER BY PSC.PhotoStatusCode

RETURN 0

GO
