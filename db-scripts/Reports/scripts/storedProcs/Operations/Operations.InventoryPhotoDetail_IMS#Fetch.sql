
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Operations].[InventoryPhotoDetail_IMS#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Operations].[InventoryPhotoDetail_IMS#Fetch] AS SELECT 1')
GO

GRANT EXECUTE ON [Operations].[InventoryPhotoDetail_IMS#Fetch] TO [StoredProcedureUser]
GO
GRANT EXECUTE ON [Operations].[InventoryPhotoDetail_IMS#Fetch] TO [CustomerOperations]
GO

ALTER PROCEDURE [Operations].[InventoryPhotoDetail_IMS#Fetch]
	@InventoryID INT
AS

/* --------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * Details about photos for a particular inventory item in the IMS system
 *
 * Powers a SSRS report for QA and OPs purposes
 *
 * Parameters
 * ----------
 *
 * @InventoryID - the InventoryID
 * 
 * Exceptions
 * ----------
 *
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

SELECT 
	IP.InventoryID, 
	P.PhotoID,
	PCS.PhotoStatusCode,
	PCS.Description AS PhotoStatus,
	CASE WHEN PATINDEX('http%', PhotoURL) > 0 
		THEN PhotoURL 
		ELSE 'https://www.firstlook.biz/digitalimages/' + PhotoURL 
		END AS PhotoURL,
	CASE WHEN PATINDEX('http%', OriginalPhotoURL) > 0 
		THEN OriginalPhotoURL 
		ELSE 'https://www.firstlook.biz/digitalimages/' + OriginalPhotoURL 
		END AS OriginalPhotoURL,
	OriginalPhotoURL,
	P.DateCreated,
	CAST(NULL AS DATETIME) AS DateUpdated
FROM IMT.dbo.InventoryPhotos IP WITH(NOLOCK)
JOIN IMT.dbo.Photos P WITH(NOLOCK)
	ON IP.PhotoID = P.PhotoID
JOIN IMT.dbo.PhotoStatus PCS WITH(NOLOCK)
	ON PCS.PhotoStatusCode = P.PhotoStatusCode
WHERE IP.InventoryID = @InventoryID

RETURN 0

GO
