
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_YesterdaysAppraisals]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_YesterdaysAppraisals] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_YesterdaysAppraisals] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_YesterdaysAppraisals]
	@DealerID INT
AS

SET NOCOUNT ON

SELECT  convert(varchar(20), A.DateCreated, 101) TradeAnalyzerDate, 
	AAT.Description, 
	B.BusinessUnit 'BusinessUnitID',
	V.VIN, 
	V.VehicleYear Year, 
	V.Make, 
	V.Model, 
	V.VehicleTrim Trim, 
	CASE V.BodyType WHEN 'UNKNOWN' THEN '' ELSE V.BodyType END AS Body, 
	COALESCE(A.Color, V.BaseColor) Color, 
	A.Mileage,  
	COALESCE(AV2.Value, 0.0) AppraisalValue, 
	AC.FirstName + ' ' + AC.LastName Customer, 
	AA.WholesalePrice, 
	AA.EstimatedReconditioningCost,
	M.MemberID,
	COALESCE(AV2.AppraiserName, M.FirstName + ' ' + M.LastName + '*') Appraiser
FROM 	[IMT].dbo.Appraisals AS A INNER JOIN
	[IMT].dbo.Member M ON M.MemberID = A.MemberID INNER JOIN
	[IMT].dbo.BusinessUnit B ON B.BusinessUnitID = A.BusinessUnitID INNER JOIN
      [IMT].dbo.Vehicle AS V ON A.VehicleID = V.VehicleID LEFT OUTER JOIN 
	[IMT].dbo.AppraisalCustomer AS AC ON A.AppraisalID = AC.AppraisalID INNER JOIN 
	[IMT].dbo.AppraisalActions AS AA ON A.AppraisalID = AA.AppraisalID INNER JOIN
     	[IMT].dbo.AppraisalActionTypes AS AAT ON AA.AppraisalActionTypeID = AAT.AppraisalActionTypeID LEFT OUTER JOIN
	(SELECT AV1.AppraisalValueID, 			
		AV1.AppraisalID, 
		AV1.SequenceNumber, 
		AV1.DateCreated, 
		AV1.Value, 
		AV1.AppraiserName, 
		CASE WHEN AV1.SequenceNumber > 1 THEN 1 ELSE 0 END AS Bumped
	FROM	[IMT].dbo.Appraisals AS A INNER JOIN
		[IMT].dbo.AppraisalValues AS AV1 ON A.AppraisalID = AV1.AppraisalID INNER JOIN
		(SELECT AppraisalID, MAX(SequenceNumber) AS SequenceNumber FROM [IMT].dbo.AppraisalValues GROUP BY AppraisalID) AS AC ON AV1.AppraisalID = AC.AppraisalID AND AV1.SequenceNumber = AC.SequenceNumber
	WHERE   (A.BusinessUnitID = @DealerID) AND (CONVERT(varchar(20), A.DateCreated, 101) = CONVERT(varchar(20), DATEADD(dd, - 1, 
		CONVERT(varchar(20), GETDATE(), 101)), 101))) AS AV2 ON A.AppraisalID = AV2.AppraisalID
WHERE   (A.BusinessUnitID = @DealerID) AND (CONVERT(varchar(20), A.DateCreated, 101) = CONVERT(varchar(20), DATEADD(dd, - 1, 
	CONVERT(varchar(20), GETDATE(), 101)), 101)) AND (A.AppraisalStatusID = 1)
ORDER BY TradeAnalyzerDate, AA.AppraisalActionTypeID
GO