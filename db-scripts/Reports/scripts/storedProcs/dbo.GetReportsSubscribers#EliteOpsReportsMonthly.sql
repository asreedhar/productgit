
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetReportSubscribers#EliteOpsReportsMonthly]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[GetReportSubscribers#EliteOpsReportsMonthly] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[GetReportSubscribers#EliteOpsReportsMonthly] TO [StoredProcedureUser]
GO
GRANT EXECUTE ON [dbo].[GetReportSubscribers#EliteOpsReportsMonthly] TO [FirstlookReports]
GO

ALTER PROCEDURE [dbo].[GetReportSubscribers#EliteOpsReportsMonthly]
	@SubscriptionTypeID            INT,
	@SubscriptionFrequencyDetailID INT = 11
AS


/* --------------------------------------------------------------------
 * 
 * $Id: dbo.GetReportsSubscribers#EliteOpsReportsMonthly.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return a list of recipients for the supplied subscription type and frequency.
 * This SPROC is expected to run on subscription types 16-19
 * 
 * Parameters
 * ----------
 *
 * @SubscriptionTypeID            - the type of the report
 * @SubscriptionFrequencyDetailID - the frequency of the report
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - Invalid @SubscriptionTypeID
 * 50100 - Invalid @SubscriptionFrequencyDetailID
 *
 * MAK -01/29/2009  These should only be sent to Active users.
 * SVU - 12/3/2009  Copied from #PlatinumReports to #ElitEOpsReportsMonthly
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE		@WeekDay	bit
DECLARE		@Year		INT
DECLARE		@Month		INT
DECLARE		@MonthRange	VARCHAR(20)
DECLARE		@ReportMonth	VARCHAR(100)
DECLARE		@RunDay		INT
DECLARE		@RunFlag	bit

--
-- Initial Values
--
SET @Year=YEAR(GETDATE())
SET @MONTH=MONTH(GETDATE())
SET @RunDay=DAY(GETDATE())
SET @RunFlag=0

IF @Month=1
BEGIN
	SET @Month=12
	SET @Year=@Year-1
END
ELSE
BEGIN
	SET @Month=@Month-1
END

SET @MonthRange=CAST(@Year AS VARCHAR(4)) + RIGHT('00' + CAST(@MONTH AS VARCHAR(2)),2)
SET @ReportMonth=DateName( month , DateAdd( month , @Month , 0 ) - 1 ) + ' ' + CAST(@Year AS VARCHAR(4))

--
-- The report needs to run on the first weekday of the 11th, 12th or 13th.
--
IF @RunDay=11 AND DATEPART(weekday,GETDATE()) BETWEEN 2 AND 6	-- Today is the 11th on a weekday
	BEGIN
		SET @RunFlag=1
	END
IF @RunDay=12 AND DATEPART(weekday,GETDATE()) = 2	-- Today is Monday the 12th, 11th was weekend
	BEGIN
		SET @RunFlag=1
	END
IF @RunDay=13 AND DATEPART(weekday,GETDATE()) = 2	-- Today is Monday the 13th, 11th was weekend
	BEGIN
		SET @RunFlag=1
	END


--IF @SubscriptionFrequencyDetailID IS NULL BEGIN
--
--	SELECT	@SubscriptionFrequencyDetailID = SubscriptionFrequencyDetailID
--	FROM	[IMT].dbo.SubscriptionFrequencyDetail
--	WHERE	Description = DATENAME(DW,GETDATE())
--
--END

/* --------------------------------------------------------------------
 *                          Validate Parameters
 * -------------------------------------------------------------------- */

IF NOT EXISTS (SELECT 1 FROM [IMT].dbo.SubscriptionTypes WHERE SubscriptionTypeID = @SubscriptionTypeID) BEGIN
	RAISERROR('Invalid SubscriptionType ''%d''.', 16, 1, @SubscriptionTypeID)
	RETURN 50100
END

IF @SubscriptionTypeID not in (20) BEGIN
	RAISERROR('SubscriptionType Out Of Range [20] ''%d''.', 16, 1, @SubscriptionTypeID)
	RETURN 50100
END

IF NOT EXISTS (SELECT 1 FROM [IMT].dbo.SubscriptionFrequencyDetail WHERE SubscriptionFrequencyDetailID = @SubscriptionFrequencyDetailID) BEGIN
	RAISERROR('Invalid SubscriptionFrequencyDetail ''%d''.', 16, 1, @SubscriptionFrequencyDetailID)
	RETURN 50100
END

/* --------------------------------------------------------------------
 *                          Declare Result Schema
 * -------------------------------------------------------------------- */
DECLARE @Results TABLE (
	[TO]		VARCHAR(8000) NOT NULL,
	[RenderFormat]	VARCHAR(8000) NOT NULL,
	[Subject]	VARCHAR(8000) NOT NULL,
	[Year]		INT,
	[Month]		INT,
	[MonthRange]	VARCHAR(20),
	[MonthStart]	VARCHAR(20),
	[MonthEnd]	VARCHAR(20),
	[ReportMonth]	VARCHAR(100),
	[DealerID]	INT NOT NULL
)

DECLARE @Recipients TABLE (
	[Idx]			INT NOT NULL,
	[TO]			VARCHAR(8000) NOT NULL,
	[Subject]		VARCHAR(8000) NOT NULL,
	[DealerID]		INT NOT NULL
)

/* --------------------------------------------------------------------
 *                             Collect Recipients
 * -------------------------------------------------------------------- */

--
-- In the future, We need to account for user selectable RenderFormat
--

INSERT INTO @Recipients (
	[Idx],
	[TO],
	[Subject],
	[DealerID]
)

SELECT	[Idx]         = ROW_NUMBER() OVER (PARTITION BY B.BusinessUnitID ORDER BY B.BusinessUnitID),
	[TO]          = M.EmailAddress,
	[Subject]     = BusinessUnit + '''s ' + F.Description + ' ' + REPLACE(T.Description, 'Weekly ', '') + ' for ' + D.Description + ', ' + CONVERT(VARCHAR,GETDATE(),107),
	[DealerID]    = B.BusinessUnitID

FROM	[IMT].dbo.Subscriptions S
JOIN	[IMT].dbo.SubscriptionTypes T ON S.SubscriptionTypeID = T.SubscriptionTypeID
JOIN	[IMT].dbo.Member M ON S.SubscriberID = M.MemberID
JOIN	[IMT].dbo.BusinessUnit B ON S.BusinessUnitID = B.BusinessUnitID
CROSS JOIN [IMT].dbo.SubscriptionFrequencyDetail D
JOIN       [IMT].dbo.SubscriptionFrequency F ON D.SubscriptionFrequencyID = F.SubscriptionFrequencyID


WHERE	D.SubscriptionFrequencyDetailID = @SubscriptionFrequencyDetailID
AND	S.SubscriptionTypeID = @SubscriptionTypeID
AND	S.SubscriptionFrequencyDetailID IN (@SubscriptionFrequencyDetailID,3)
AND	S.SubscriberTypeID = 1           -- Member
AND	S.SubscriptionDeliveryTypeID = 1 -- HTML email
AND	M.EmailAddress IS NOT NULL
AND	LEN(M.EmailAddress) > 5
AND	PATINDEX('%@%.%', M.EmailAddress) > 0
AND	B.Active =1

/* --------------------------------------------------------------------
 *                           Combine Recipients
 * -------------------------------------------------------------------- */

; WITH Rec ([Idx], [To], [DealerID], [Ctr]) AS
(
	SELECT	R1.Idx,
		R1.[TO],
		R1.[DealerID],
		1 Ctr
	FROM	@Recipients R1
UNION ALL
	SELECT	R2.Idx,
		R2.[TO] + COALESCE(';' + R3.[TO],''),
		R2.[DealerID],
		Ctr+1 Ctr
	FROM	@Recipients R2
	JOIN	Rec R3 ON R3.DealerID = R2.DealerID AND R3.Idx = R2.Idx + 1
),
Row (DealerID,Subject,Ctr) AS
(
	SELECT	DealerID, Subject, COUNT(*) Ctr
	FROM	@Recipients
	GROUP
	BY	DealerID, Subject
)
INSERT INTO @Results (
	[TO],
	[RenderFormat],
	[Subject],
	[Year],
	[Month],
	[MonthRange],
	[MonthStart],
	[MonthEnd],
	[ReportMonth],
	[DealerID]
)
SELECT	[TO]		= R.[To],
	[RenderFormat]	= 'PDF',
	[Subject]	= X.Subject,
	[Year]		= @Year,
	[Month]		= @Month,
	[MonthRange]	= @MonthRange,
	[MonthStart]	= @MonthRange,
	[MonthEnd]	= @MonthRange,
	[ReportMonth]	= @ReportMonth,
	[DealerID]	= R.DealerID
FROM	Rec R
JOIN	Row X ON R.DealerID = X.DealerID AND R.Ctr = X.Ctr
WHERE	@RunFlag=1

/* --------------------------------------------------------------------
 *                           Return Results
 * -------------------------------------------------------------------- */

SELECT * FROM @Results

GO