
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_RepriceAudit]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_RepriceAudit] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_RepriceAudit] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_RepriceAudit]
	@PeriodID INT,
	@DealerID INT
AS

SET NOCOUNT ON;

SELECT	B.BusinessUnit 'BusinessUnitName',
		I.InventoryID,
		I.StockNumber ,
		Cast( aip.Value as int ) as NewListPrice,
		Cast( aip.notes2 as dec(10,2) ) as oldListPrice,
		aip.notes3 as wasConfirmed,
		aip.createdBy as userWhoDidRepricesLoginName,
		aip.created as timeRepriceWasDone,
		V.[VehicleYear],
		MMG.[Make],
		MMG.[Model],
		V.[VehicleTrim],
		DBT.Segment DisplayBodyType,
		M.[FirstName],
		M.[LastName]
FROM	[IMT].dbo.BusinessUnit B
JOIN	[IMT].dbo.Inventory I ON I.BusinessUnitID = B.BusinessUnitID
JOIN	[IMT].dbo.AIP_Event aip ON aip.InventoryID = I.InventoryID
JOIN	[IMT].dbo.Member M ON aip.CreatedBy = M.login
JOIN	[IMT].dbo.tbl_Vehicle V ON I.VehicleID = V.VehicleID
JOIN	[IMT].dbo.MakeModelGrouping MMG ON MMG.MakeModelGroupingID = V.MakeModelGroupingID
JOIN	[IMT].dbo.Segment DBT ON DBT.SegmentID = MMG.SegmentID
CROSS JOIN	[FLDW].dbo.Period_D P
WHERE	aip.AIP_EventTypeID = 5
And		B.BusinessUnitID = @DealerID
AND		P.PeriodID = @PeriodID
AND		DATEADD(DD, -1, aip.Created) BETWEEN
			CASE	WHEN @PeriodID = 1 THEN DATEADD(WW, -1, GETDATE())
					WHEN @PeriodID = 2 THEN DATEADD(WW, -2, GETDATE())
					ELSE P.BeginDate
			END
		AND	DATEADD(DD, 1, P.EndDate)
GO
