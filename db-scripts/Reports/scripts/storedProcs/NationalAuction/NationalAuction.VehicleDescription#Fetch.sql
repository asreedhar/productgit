
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[NationalAuction].[VehicleDescription#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [NationalAuction].[VehicleDescription#Fetch] AS SELECT 1')
GO

GRANT EXECUTE ON [NationalAuction].[VehicleDescription#Fetch] TO [NationalAuctionUser]
GO

ALTER PROCEDURE [NationalAuction].[VehicleDescription#Fetch]
	@Vin           CHAR(17)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: NationalAuction.VehicleDescription#Fetch.sql,v 1.2 2008/10/27 20:18:14 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Give the generic description for the supplied VIN.
 *
 * Parameters
 * ----------
 *
 * @Vin		- The VIN (not null).
 * 
 * Exceptions
 * ----------
 *
 * (Vin)
 * 50100 - VIN is null
 * 50107 - VIN has bad format
 * 50200 - VIN is unknown
 * (Vic)
 * 50100 - VIC is null
 * 50200 - VIC is invalid (does not exist)
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

-- validate parameters

DECLARE @MakeModelGroupingID INT, @ModelYear INT, @AltVic CHAR(10)

EXEC @rc = NationalAuction.ValidateParameter#Vin @Vin, @MakeModelGroupingID out, @ModelYear out, @Vic=@AltVic out

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

-- get data

DECLARE @Results TABLE (
	Id INT NOT NULL,
	ModelYear INT NOT NULL,
	Make VARCHAR(50) NOT NULL,
	Model VARCHAR(100) NOT NULL
)

INSERT INTO @Results(Id,ModelYear, Make, Model)
SELECT	MakeModelGroupingID,@ModelYear, Make, Model
FROM	[IMT].dbo.MakeModelGrouping
WHERE	MakeModelGroupingID = @MakeModelGroupingID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

SELECT * FROM @Results

RETURN 0

Failed:

RETURN @err

GO
