
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[NationalAuction].[SeriesList#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [NationalAuction].[SeriesList#Fetch] AS SELECT 1')
GO

GRANT EXECUTE ON [NationalAuction].[SeriesList#Fetch] TO [NationalAuctionUser]
GO

ALTER PROCEDURE [NationalAuction].[SeriesList#Fetch]
	@Vin VARCHAR(17)
AS

/* --------------------------------------------------------------------
 * 
 * $Id: NationalAuction.SeriesList#Fetch.sql,v 1.2.42.2 2010/06/02 18:40:42 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Return a list of NAAA series.
 * 
 * Parameters
 * ----------
 *
 * NONE
 * 
 * Exceptions
 * ----------
 *
 * 50204 - No NAAA series!
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

-- validate parameters

DECLARE @MakeModelGroupingID INT, @ModelYear INT

EXEC @rc = NationalAuction.ValidateParameter#Vin @Vin, @MakeModelGroupingID out, @ModelYear out

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

-- get list of NAAA series

DECLARE @Results TABLE (
	VIC CHAR(10) NOT NULL,
	ModelYear INT NOT NULL,
	Make VARCHAR(50) NOT NULL,
	Series VARCHAR(50) NOT NULL,
	Body VARCHAR(50) NOT NULL
)

INSERT INTO @Results (VIC, ModelYear, Make, Series, Body)

SELECT	VIC.VIC, VIC.ModelYear, VIC.Make, VIC.Series, VIC.Body
FROM	[VehicleUC].dbo.VIC_Catalog VIC
JOIN	[VehicleUC].dbo.VINPrefix_VIC PFX ON VIC.VIC = PFX.VIC
WHERE	PFX.VINPrefix = Left(@Vin,8) + '0' + SUBSTRING(@Vin, 10, 1)

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

--We allow for 0 returns as well
IF @rc > 1 OR @rc = 0 BEGIN

INSERT INTO @Results (VIC, ModelYear, Make, Series, Body)

SELECT	'MMG' + CAST(MMG.MakeModelGroupingID AS VARCHAR) + 'X', VC.ModelYear, MMG.Make, HAL.dbo.initcap(MMG.OriginalModel), S.Segment
FROM	VehicleCatalog.Firstlook.VehicleCatalog VC
JOIN	VehicleCatalog.Firstlook.GetVehicleCatalogByVIN(@Vin, NULL, 1) DC ON DC.VehicleCatalogID = VC.VehicleCatalogID
JOIN	IMT.dbo.MakeModelGrouping MMG ON MMG.ModelID = VC.ModelID
JOIN	IMT.dbo.Segment S ON S.SegmentID = MMG.SegmentID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

END

-- test results (need one or more rows)

IF @rc < 1
BEGIN
	RAISERROR (50204,16,1,'>=',1,@rc)
	RETURN @@ERROR
END

-- success!

SELECT * FROM @Results ORDER BY Make, Series, Body

Failed:

RETURN @err

GO
