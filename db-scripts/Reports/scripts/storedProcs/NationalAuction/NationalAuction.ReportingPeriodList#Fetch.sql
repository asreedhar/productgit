
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[NationalAuction].[ReportingPeriodList#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [NationalAuction].[ReportingPeriodList#Fetch] AS SELECT 1')
GO

GRANT EXECUTE ON [NationalAuction].[ReportingPeriodList#Fetch] TO [NationalAuctionUser]
GO

ALTER PROCEDURE [NationalAuction].[ReportingPeriodList#Fetch]
AS

/* --------------------------------------------------------------------
 * 
 * $Id: NationalAuction.ReportingPeriodList#Fetch.sql,v 1.2 2008/10/27 20:18:14 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Return a list of NAAA reporting periods.
 * 
 * Parameters
 * ----------
 *
 * NONE
 * 
 * Exceptions
 * ----------
 *
 * 50204 - No NAAA reporting periods!
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

DECLARE @Results TABLE (
	Id INT NOT NULL,
	Name VARCHAR(255) NOT NULL
)

INSERT INTO @Results (Id,Name)

SELECT	PeriodID, Description
FROM	[AuctionNet].[dbo].[Period_D]
WHERE	TimeTypeCD = 6
ORDER
BY	Rank

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF @rc < 1
BEGIN
	RAISERROR (50204,16,1,'>=',1,@rc)
	RETURN @@ERROR
END

DECLARE @Year VARCHAR(4), @YearAgo VARCHAR(4), @Month VARCHAR(3), @MonthAgo VARCHAR(3)

SET @Year = CONVERT(VARCHAR(4),GETDATE(),120)
SET @Month = CONVERT(VARCHAR(3),GETDATE(),109)
SET @YearAgo = CONVERT(VARCHAR(4),DATEADD(YY,-1,GETDATE()),120)
SET @MonthAgo = CONVERT(VARCHAR(3),DATEADD(MM,-1,GETDATE()),109)

UPDATE @Results SET Name = @Year + ' YTD' WHERE Id = 10
UPDATE @Results SET Name = @Month + ' ' + @Year + ' MTD' WHERE Id = 8
UPDATE @Results SET Name = @Month + ' ' + @YearAgo + ' MTD' WHERE Id = 9
UPDATE @Results SET Name = 'Prior Month (' + @MonthAgo + ')' WHERE Id = 14

SELECT * FROM @Results

RETURN 0

Failed:

RETURN @err

GO
