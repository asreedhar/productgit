
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[NationalAuction].[ValidateParameter#AreaId]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [NationalAuction].[ValidateParameter#AreaId] AS SELECT 1')
GO

GRANT EXECUTE ON [NationalAuction].[ValidateParameter#AreaId] TO [NationalAuctionUser]
GO

ALTER PROCEDURE [NationalAuction].[ValidateParameter#AreaId]
	@AreaId INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: NationalAuction.ValidateParameter#AreaId.sql,v 1.2 2008/10/27 20:18:14 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Validates the supplied NAAA Area Id (not null and exists).
 *
 * Parameters
 * ----------
 *
 * @AreaId	- The NAAA Area Id to validate.
 * 
 * Exceptions
 * ----------
 *
 * 50100 - Area Id is null
 * 50200 - Area Id is invalid
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

-- null check parameters

IF @AreaId IS NULL
BEGIN
	RAISERROR (50100,16,1,'AreaId')
	RETURN 1
END

IF NOT EXISTS (SELECT 1 FROM [AuctionNet].dbo.Area_D WHERE AreaID = @AreaID)
BEGIN
	RAISERROR (50200,16,1,0)
	RETURN 1
END

RETURN 0

GO
