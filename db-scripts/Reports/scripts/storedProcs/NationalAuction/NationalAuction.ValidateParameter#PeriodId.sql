
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[NationalAuction].[ValidateParameter#PeriodId]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [NationalAuction].[ValidateParameter#PeriodId] AS SELECT 1')
GO

GRANT EXECUTE ON [NationalAuction].[ValidateParameter#PeriodId] TO [NationalAuctionUser]
GO

ALTER PROCEDURE [NationalAuction].[ValidateParameter#PeriodId]
	@PeriodId INT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: NationalAuction.ValidateParameter#PeriodId.sql,v 1.2 2008/10/27 20:18:14 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Validates the supplied NAAA Period Id (not null and exists).
 *
 * Parameters
 * ----------
 *
 * @AreaId	- The NAAA Period Id to validate.
 * 
 * Exceptions
 * ----------
 *
 * 50100 - @PeriodId is null
 * 50200 - @PeriodId is invalid
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

-- null check parameters

IF @PeriodId IS NULL
BEGIN
	RAISERROR (50100,16,1,'PeriodId')
	RETURN 1
END

IF NOT EXISTS (SELECT 1 FROM [AuctionNet].dbo.Period_D WHERE PeriodID = @PeriodID)
BEGIN
	RAISERROR (50200,16,1,0)
	RETURN 1
END

RETURN 0

GO
