
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[NationalAuction].[OverallReport#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [NationalAuction].[OverallReport#Fetch] AS SELECT 1')
GO

GRANT EXECUTE ON [NationalAuction].[OverallReport#Fetch] TO [NationalAuctionUser]
GO

ALTER PROCEDURE [NationalAuction].[OverallReport#Fetch]
	@Vin           CHAR(17),
	@Vic           CHAR(10) = NULL,
	@AreaID        INT = NULL,
	@PeriodID      INT = NULL
AS

/* --------------------------------------------------------------------
 * 
 * $Id: NationalAuction.OverallReport#Fetch.sql,v 1.2 2008/10/27 20:18:14 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Reports summary information (min, avg and max) on the price and mileage of vehicles sold
 * with a given series in a region over a time period.
 * 
 * When the VIC of a vehicle is unknown we fallback to collecting statistics for a given MMG
 * and model year.
 *
 * Parameters
 * ----------
 *
 * @Vin		- The VIN (not null).
 * @Vic		- The vehicle identification code (VIC), possibly null.
 * @AreaId	- The reporting geographic region (not null).
 * @PeriodId	- The reporting time frame (not null).
 * 
 * Exceptions
 * ----------
 *
 * (Vin)
 * 50100 - VIN is null
 * 50107 - VIN has bad format
 * 50200 - VIN is unknown
 * (Vic)
 * 50100 - VIC is null
 * 50200 - VIC is invalid (does not exist)
 * (PeriodId)
 * 50100 - Period Id is null
 * 50200 - Period Id is invalid
 * (AreaId)
 * 50100 - Area Id is null
 * 50200 - Area Id is invalid
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

-- validate parameters

DECLARE @MakeModelGroupingID INT, @ModelYear INT, @AltVic CHAR(10)

EXEC @rc = NationalAuction.ValidateParameter#Vin @Vin, @MakeModelGroupingID out, @ModelYear out, @Vic=@AltVic out

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF @Vic IS NOT NULL AND @Vic LIKE 'MMG[0-9][0-9][0-9][0-9][0-9][0-9]X' SET @Vic = NULL

IF @Vic IS NULL AND @AltVic IS NOT NULL SET @Vic = @AltVic

EXEC @rc = NationalAuction.ValidateParameter#Vic @Vic

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

EXEC @rc = NationalAuction.ValidateParameter#AreaId @AreaId

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

EXEC @rc = NationalAuction.ValidateParameter#PeriodId @PeriodID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

-- get results

DECLARE @Results TABLE (
	MaxSalePrice DECIMAL(8,2) NOT NULL,
	AvgSalePrice DECIMAL(8,2) NOT NULL,
	MinSalePrice DECIMAL(8,2) NOT NULL,
	MaxMileage INT NOT NULL,
	AvgMileage INT NOT NULL,
	MinMileage INT NOT NULL,
	VehicleCount INT NOT NULL
)

IF @VIC IS NOT NULL AND LEN(LTRIM(@VIC)) = 10

	INSERT INTO @Results (
		MaxSalePrice,
		AvgSalePrice,
		MinSalePrice,
		MaxMileage,
		AvgMileage,
		MinMileage,
		VehicleCount
	)
	SELECT	HighSalePrice,
		AveSalePrice,
		LowSalePrice,
		HighMileage,
		AveMileage,
		LowMileage,
		TransactionCount
	FROM	[AuctionNet].dbo.AuctionTransaction_A1
	WHERE	VIC = @VIC
	AND	AreaID = @AreaId
	AND	PeriodID = @PeriodId

ELSE

	INSERT INTO @Results (
		MaxSalePrice,
		AvgSalePrice,
		MinSalePrice,
		MaxMileage,
		AvgMileage,
		MinMileage,
		VehicleCount
	)
	SELECT	HighSalePrice,
		AveSalePrice,
		LowSalePrice,
		HighMileage,
		AveMileage,
		LowMileage,
		TransactionCount
	FROM	[AuctionNet].dbo.AuctionTransaction_A3
	WHERE	MakeModelGroupingID = @MakeModelGroupingID
	AND	ModelYear = @ModelYear
	AND	AreaID = @AreaId
	AND	PeriodID = @PeriodId

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

-- No results is OK

SELECT * FROM @Results

RETURN 0

Failed:

RETURN @err

GO
