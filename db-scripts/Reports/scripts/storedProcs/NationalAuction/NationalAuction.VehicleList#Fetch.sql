
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[NationalAuction].[VehicleList#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [NationalAuction].[VehicleList#Fetch] AS SELECT 1')
GO

GRANT EXECUTE ON [NationalAuction].[VehicleList#Fetch] TO [NationalAuctionUser]
GO

ALTER PROCEDURE [NationalAuction].[VehicleList#Fetch]
	@Vin           CHAR(17),
	@Vic           CHAR(10) = NULL,
	@AreaID        INT = NULL,
	@PeriodID      INT = NULL,
	@SortColumns   VARCHAR(128) = 'RowNumber',
	@MaximumRows   INT = 100,
	@StartRowIndex INT = 0
AS

/* --------------------------------------------------------------------
 * 
 * $Id: NationalAuction.VehicleList#Fetch.sql,v 1.2 2008/10/27 20:18:14 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * List the vehicles for the supplied criteria on the given page.
 * 
 * Parameters
 * ----------
 *
 * @PeriodId		- The reporting time frame.
 * @AreaId		- The reporting geographic region.
 * @Vic			- The vehicle identification code (VIC), possibly null.
 * @MakeModelGroupingID	- The vehicle MMG (when VIC is null).
 * @ModelYear		- The vehicle model year (when VIC is null).
 * 
 * Exceptions
 * ----------
 *
 * 50204 - No NAAA reporting periods!
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

-- validate parameters

DECLARE @MakeModelGroupingID INT, @ModelYear INT, @AltVic CHAR(10)

EXEC @rc = NationalAuction.ValidateParameter#Vin @Vin, @MakeModelGroupingID out, @ModelYear out, @Vic = @AltVic out

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF @Vic IS NOT NULL AND @Vic LIKE 'MMG[0-9][0-9][0-9][0-9][0-9][0-9]X' SET @Vic = NULL

IF @Vic IS NULL AND @AltVic IS NOT NULL SET @Vic = @AltVic

EXEC @rc = NationalAuction.ValidateParameter#Vic @Vic

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

EXEC @rc = NationalAuction.ValidateParameter#AreaId @AreaId

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

EXEC @rc = NationalAuction.ValidateParameter#PeriodId @PeriodID

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

-- table result set

CREATE TABLE #Results (
	RowNumber INT IDENTITY(1,1) NOT NULL,
	SaleDate SMALLDATETIME,
	RegionName VARCHAR(100),
	SaleTypeName VARCHAR(100),
	Series VARCHAR(100),
	SalePrice DECIMAL(8,2),
	Mileage INT,
	Engine VARCHAR(100),
	Transmission VARCHAR(100)
)

IF @VIC IS NOT NULL

	INSERT INTO #Results (SaleDate, RegionName, SaleTypeName, Series, SalePrice, Mileage, Engine, Transmission)
	SELECT	SaleDate, RegionName, SaleTypeName, Series, SalePrice, Mileage, Engine, Transmission
	FROM	[AuctionNet].dbo.AuctionDetailReport ADR
	JOIN	[AuctionNet].dbo.Region_D R ON R.regionId = ADR.regionId
	WHERE	ADR.VIC = @VIC
	AND	ADR.AreaID = @AreaID
	AND	ADR.PeriodID = @PeriodID
	ORDER
	BY	ADR.SaleDate DESC

ELSE
	INSERT INTO #Results (SaleDate, RegionName, SaleTypeName, Series, SalePrice, Mileage, Engine, Transmission)
	SELECT	SaleDate, RegionName, SaleTypeName, Series, SalePrice, Mileage, Engine, Transmission
	FROM	[AuctionNet].dbo.AuctionDetailReport ADR
	JOIN	[AuctionNet].dbo.Region_D R ON R.regionId = ADR.regionId
	WHERE	ADR.MakeModelGroupingId = @MakeModelGroupingID
	AND	ADR.AreaId = @AreaID
	AND	ADR.PeriodID = @PeriodID
	AND	ADR.ModelYear = @ModelYear
	ORDER
	BY	ADR.SaleDate desc

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

-- Bill's (Magic) Paging Code

DECLARE @ColumnList	NVARCHAR(500),
	@sql		NVARCHAR(4000)

SELECT	@ColumnList = COALESCE(@ColumnList + ',','') + name 
FROM	tempdb.sys.columns 
WHERE 	object_id = OBJECT_ID('tempdb.dbo.#Results') 
ORDER 
BY	column_id

SET @sql = '
SELECT	' + @ColumnList + '
FROM	(SELECT *, ROW_NUMBER() OVER(ORDER BY ' + @SortColumns + ') AS DisplayRowNumber
	FROM	#Results 
	) R
WHERE	DisplayRowNumber BETWEEN @StartRowIndex+1 AND @StartRowIndex + @MaximumRows
ORDER
BY	DisplayRowNumber'

EXEC @err = sp_executesql @sql, N'@StartRowIndex INT, @MaximumRows INT', @StartRowIndex = @StartRowIndex, @MaximumRows = @MaximumRows

SELECT TotalRowCount = COUNT(*) FROM #Results

DROP TABLE #Results

RETURN 0

Failed:

IF OBJECT_ID('tempdb.dbo.#Results') IS NOT NULL
	DROP TABLE #Results

RETURN @err

GO
