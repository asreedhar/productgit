
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[NationalAuction].[ValidateParameter#Vin]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [NationalAuction].[ValidateParameter#Vin] AS SELECT 1')
GO

GRANT EXECUTE ON [NationalAuction].[ValidateParameter#Vin] TO [NationalAuctionUser]
GO

ALTER PROCEDURE [NationalAuction].[ValidateParameter#Vin]
	@Vin VARCHAR(17),
	@MakeModelGroupingID INT OUT,
	@ModelYear INT OUT,
	@Vic CHAR(10) = NULL OUT
AS

/* --------------------------------------------------------------------
 * 
 * $Id: NationalAuction.ValidateParameter#Vin.sql,v 1.2 2008/10/27 20:18:14 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Validates the supplied VIN (not null, length 17 characters) and returns some useful
 * decoded information.
 *
 * Parameters
 * ----------
 *
 * @Vin	- The VIN to validate and decode.
 * 
 * Exceptions
 * ----------
 *
 * 50100 - VIN is null
 * 50107 - VIN has bad format
 * 50200 - VIN is unknown
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

-- null check parameters

IF @Vin IS NULL
BEGIN
	RAISERROR (50100,16,1,'Vin')
	RETURN 1
END

IF LEN(@Vin) <> 17
BEGIN
	RAISERROR (50107,16,1,@Vin)
	RETURN 1
END

-- get make/model/grouping and model-year

SELECT	@MakeModelGroupingID = MMG.MakeModelGroupingID,
	@ModelYear = VC.ModelYear
FROM	[VehicleCatalog].Firstlook.GetVehicleCatalogByVIN(@VIN, NULL, 1) LU
JOIN	[VehicleCatalog].Firstlook.VehicleCatalog VC ON VC.VehicleCatalogID = LU.VehicleCatalogID
JOIN	[IMT].dbo.MakeModelGrouping MMG ON MMG.ModelID = VC.ModelID
WHERE	MMG.ModelID <> 0

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF @rc <> 1
BEGIN
	RAISERROR (50200,16,1,@rc)
	RETURN @@ERROR
END

-- get unique VIC

SELECT	@Vic = V.VIC
FROM	[VehicleUC].dbo.VINPrefix_VIC V
JOIN	(
		SELECT	VINPrefix, COUNT(*) NumberOfVICs
		FROM	[VehicleUC].dbo.VINPrefix_VIC
		WHERE	VINPrefix = Left(@Vin,8) + '0' + SUBSTRING(@Vin, 10, 1)
		GROUP
		BY	VINPrefix
		HAVING	COUNT(*) = 1
	) R ON V.VINPrefix = R.VINPrefix

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

Failed:

RETURN @err

GO
