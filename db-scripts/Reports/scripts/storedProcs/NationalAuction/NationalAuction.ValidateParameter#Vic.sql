
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[NationalAuction].[ValidateParameter#Vic]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [NationalAuction].[ValidateParameter#Vic] AS SELECT 1')
GO

GRANT EXECUTE ON [NationalAuction].[ValidateParameter#Vic] TO [NationalAuctionUser]
GO

ALTER PROCEDURE [NationalAuction].[ValidateParameter#Vic]
	@Vic VARCHAR(10),
	@Opt BIT = 1
AS

/* --------------------------------------------------------------------
 * 
 * $Id: NationalAuction.ValidateParameter#Vic.sql,v 1.2 2008/10/27 20:18:14 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Validates the supplied VIC (not null, length 10 characters) and is in the lookup table.
 *
 * Parameters
 * ----------
 *
 * @Vic	- The VIC to validate.
 * 
 * Exceptions
 * ----------
 *
 * 50100 - VIC is null
 * 50200 - VIC is invalid (does not exist)
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

-- check parameters

IF @Opt = 0 AND @Vic IS NULL
BEGIN
	RAISERROR (50100,16,1,'Vic')
	RETURN 1
END

IF @Vic IS NOT NULL AND NOT EXISTS (SELECT 1 FROM [VehicleUC].dbo.VIC_Catalog WHERE VIC = @Vic)
BEGIN
	RAISERROR (50200,16,1,0)
	RETURN 1
END

-- success

RETURN 0

GO
