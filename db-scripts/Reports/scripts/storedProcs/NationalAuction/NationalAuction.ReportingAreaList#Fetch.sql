
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[NationalAuction].[ReportingAreaList#Fetch]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [NationalAuction].[ReportingAreaList#Fetch] AS SELECT 1')
GO

GRANT EXECUTE ON [NationalAuction].[ReportingAreaList#Fetch] TO [NationalAuctionUser]
GO

ALTER PROCEDURE [NationalAuction].[ReportingAreaList#Fetch]
AS

/* --------------------------------------------------------------------
 * 
 * $Id: NationalAuction.ReportingAreaList#Fetch.sql,v 1.2 2008/10/27 20:18:14 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Return a list of NAAA reporting regions.
 * 
 * Parameters
 * ----------
 *
 * NONE
 * 
 * Exceptions
 * ----------
 *
 * 50204 - No NAAA reporting regions!
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

DECLARE @Results TABLE (
	Id INT NOT NULL,
	Name VARCHAR(255) NOT NULL
)

INSERT INTO @Results (Id,Name)

SELECT	AreaID, AreaName
FROM	[AuctionNet].[dbo].[Area_D]

SELECT @rc = @@ROWCOUNT, @err = @@ERROR
IF @err <> 0 GOTO Failed

IF @rc < 1
BEGIN
	RAISERROR (50204,16,1,'>=',1,@rc)
	RETURN @@ERROR
END

SELECT * FROM @Results

RETURN 0

Failed:

RETURN @err

GO
