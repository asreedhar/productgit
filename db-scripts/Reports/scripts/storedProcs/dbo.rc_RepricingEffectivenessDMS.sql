IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_RepricingEffectivenessDMS]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_RepricingEffectivenessDMS] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_RepricingEffectivenessDMS] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_RepricingEffectivenessDMS]
---Parameters-------------------------------------------------------------------------------------
--
	@DealerGroupID INT,
	@DealerID INT,
	@MemberID INT,
	@Mode VARCHAR(12)
--
---History--------------------------------------------------------------------------------------
--
--	JMW	08/08/2012	Added history section...Modified days supply -- BUGZID: 22331
--
---Notes----------------------------------------------------------------------------------------
--	BusinessUnitID = 0 --> All
--	VehicleSegmentID = 0 --> All
--	VehicleGroupingID = -1 --> All
------------------------------------------------------------------------------------------------
AS

SET NOCOUNT ON

CREATE TABLE #Dealers (
	BusinessUnitID INT,
	BusinessUnit   VARCHAR(50),
	DealerGroup    VARCHAR(50),
	CONSTRAINT PK_Dealers_Temp PRIMARY KEY CLUSTERED (BusinessUnitID)
);

INSERT INTO #Dealers
SELECT	DS.BusinessUnitID,
		DS.BusinessUnit,
		DG.BusinessUnit
FROM	[IMT].dbo.BusinessUnit DS
JOIN	[IMT].dbo.BusinessUnitRelationship DR ON DR.BusinessUnitID = DS.BusinessUnitID
JOIN	[IMT].dbo.BusinessUnit DG ON DG.BusinessUnitID = DR.ParentID
JOIN	dbo.MemberAccess(@DealerGroupID,@MemberID) MA ON MA.BusinessUnitID = DS.BusinessUnitID
JOIN	[IMT].dbo.DealerPreference DP ON DP.BusinessUnitID = DS.BusinessUnitID
JOIN	(
			SELECT	BusinessUnitID, SUM(Active) NumerOfActiveUpgrades
			FROM	[IMT].dbo.DealerUpgrade
			GROUP
			BY		BusinessUnitID
		) DU ON DU.BusinessUnitID = DS.BusinessUnitID
WHERE	DS.Active = 1
AND		DP.GoLiveDate IS NOT NULL
AND		DS.BusinessUnitTypeID = 4
AND		DU.NumerOfActiveUpgrades > 0
AND		(@Mode = 'DealerGroup' AND DG.BusinessUnitID = @DealerGroupID
OR		 @Mode = 'Dealer' AND DS.BusinessUnitID = @DealerID)

CREATE TABLE #Sales (
	BusinessUnitID INT,
	InventoryID INT,
	DealDate SMALLDATETIME
);

INSERT INTO #Sales
SELECT	i.BusinessUnitID,
		i.InventoryID,
		vs.DealDate
FROM	#Dealers DS
JOIN	[FLDW].dbo.Inventory i on i.BusinessUnitID = DS.BusinessUnitID
JOIN	[FLDW].dbo.Vehicle v ON i.VehicleID = v.VehicleID AND i.BusinessUnitID = v.BusinessUnitID
JOIN	[FLDW].dbo.VehicleSale vs ON i.InventoryID = vs.InventoryID
CROSS JOIN [FLDW].dbo.Period_D P
WHERE	i.InventoryType = 2
AND		vs.DealDate BETWEEN P.BeginDate AND P.EndDate
AND		P.PeriodID = 4
ORDER
BY		i.BusinessUnitID, i.InventoryID

SELECT	DISTINCT
		--a
		DS.BusinessUnitID BusinessUnitIdA,
		DS.BusinessUnit,
		Cast( a.Measure as int ) MeasureA,
		-- b
		b.BusinessUnitID BusinessUnitIdB,
		Cast( b.Measure as int ) MeasureB,
		( a.Measure / Case When b.Measure = 0 Then null Else b.Measure End ) 'Pct',
		( 1 - ( a.Measure / Case When b.Measure = 0 Then null Else b.Measure End )  ) 'Pct2',
		-- c
		0 Measure244,
		0 Measure245,
		NULL 'PctDue',
		e.Measure 'Measure299',
		f.Measure 'Measure301',
		g.Measure 'Measure303',
		h.Measure 'Measure305',
		i.Measure 'Measure307',
		j.DaysSupply,
		DS.DealerGroup 'Parent'

FROM	#Dealers DS

LEFT JOIN
	(
		SELECT	i.BusinessUnitID,
				Cast(COUNT(DISTINCT(i.InventoryID)) as real) 'Measure'
		FROM	#Sales i
		JOIN	(
					SELECT	InventoryID, MAX(DMSreferenceDT) 'MaxDmsRefDT'
					FROM	[IMT].dbo.Inventory_ListPriceHistory ilph
					WHERE	ListPrice > 0
					GROUP
					BY		InventoryID
				) ilph ON ilph.InventoryID = i.InventoryID
		WHERE	DATEDIFF(dd, ilph.MaxDmsRefDT, i.DealDate) < 14
		GROUP
		BY		i.BusinessUnitID
	) a ON DS.BusinessUnitID = a.BusinessUnitID

LEFT JOIN
	(
		SELECT	i.BusinessUnitID,
				Cast(COUNT(DISTINCT(i.InventoryID)) as real) 'Measure'
		FROM	#Sales i
		GROUP
		BY		i.BusinessUnitID
	) b ON DS.BusinessUnitID = b.BusinessUnitID

LEFT JOIN
	(
		SELECT	BusinessUnitID, Measure
		FROM	[EdgeScorecard].dbo.Measures
		WHERE	PeriodID = 7 
		AND		SetID = 144
		AND		MetricID = 299
	) e ON DS.BusinessUnitID = e.BusinessUnitID

LEFT JOIN
	(
		SELECT	BusinessUnitID, Measure
		FROM	[EdgeScorecard].dbo.Measures
		WHERE	PeriodID = 7 
		AND		SetID = 145
		AND		MetricID = 301
	) f ON DS.BusinessUnitID = f.BusinessUnitID

LEFT JOIN
	(
		SELECT	BusinessUnitID, Measure
		FROM	[EdgeScorecard].dbo.Measures
		WHERE	PeriodID = 7 
		AND		SetID = 146
		AND		MetricID = 303
	) g ON DS.BusinessUnitID = g.BusinessUnitID

LEFT JOIN
	(
		SELECT	BusinessUnitID, Measure
		FROM	[EdgeScorecard].dbo.Measures
		WHERE	PeriodID = 7 
		AND		SetID = 147
		AND		MetricID = 305
	) h ON DS.BusinessUnitID = h.BusinessUnitID

LEFT JOIN
	(
		SELECT	BusinessUnitID, Measure
		FROM	[EdgeScorecard].dbo.Measures
		WHERE	PeriodID = 7 
		AND		SetID = 148
		AND		MetricID = 307
	) i ON DS.BusinessUnitID = i.BusinessUnitID

LEFT JOIN FLDW.dbo.DaysSupply_A j ON DS.BusinessUnitID = j.BusinessUnitID AND j.AllVehicleSegments =1 AND j.AllVehicleGroups = 1

ORDER BY DS.BusinessUnit;

DROP TABLE #Dealers;
DROP TABLE #Sales;

GO
