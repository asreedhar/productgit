

/****** Object:  StoredProcedure [dbo].[Fetch#RetailInventorySalesAnalysisDetail]    Script Date: 08/28/2014 10:44:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Fetch#RetailInventorySalesAnalysisDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Fetch#RetailInventorySalesAnalysisDetail]
GO


/****** Object:  StoredProcedure [dbo].[Fetch#RetailInventorySalesAnalysisDetail]    Script Date: 08/28/2014 10:44:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Pradnya Sawant
-- Create date: 2014-08-11
-- Last Modified By : Pradnya Sawant
-- Last Modified Date : 2014-08-11
-- Description:	This stored procedure returns the data fro Store Level Retail Inventory Sales Analysis -Transaction Details Data
-- =============================================


CREATE PROCEDURE [dbo].[Fetch#RetailInventorySalesAnalysisDetail]
	@PeriodID INT,
	@SelectedDealerId INT,
	@TradeInType VARCHAR(1)
	
AS

BEGIN

--DECLARE
--	@PeriodID INT=4,
--	@SelectedDealerId INT=100147,
--	@TradeInType VARCHAR(1) = 'A'
DECLARE
	@LocalPeriodID INT,
	@LocalDealerID INT,
	@LocalTradeOrPurchase VARCHAR(1)
	SELECT @LocalPeriodID=@PeriodID, @LocalDealerID=@SelectedDealerId, @LocalTradeOrPurchase=@TradeInType

SET NOCOUNT ON;

DECLARE @err INT, @TPFilterLow INT, @TPFilterHigh INT
EXEC @err = Reports.[dbo].[ValidateParameter_TradeOrPurchase] @LocalTradeOrPurchase, @TPFilterLow out, @TPFilterHigh out
IF @err <> 0 GOTO Failed

IF @LocalTradeOrPurchase='A' 
 SET @TPFilterHigh=3 --Including 'Unknown' trade purchase type

DECLARE @BeginDate SMALLDATETIME, @EndDate SMALLDATETIME


Select @BeginDate=StartDate,@EndDate=EndDate FROM IMT.dbo.GetDatesFromPeriodId(@LocalPeriodID,1);


SELECT	CAST(VS.DealDate AS Date)AS DealDate,
		CAST(V.VehicleYear AS VARCHAR)
			+ ' ' + MMG.Make
			+ ' ' + MMG.[Model]
			+ ' ' + V.VehicleTrim
		AS VehicleDescription,
		V.BaseColor,
		I.DaysToSale,
	    VS.FrontEndGross AS FrontEndGross,
		VS.BackEndGross,
		VS.SalePrice,
		I.UnitCost,
		CASE WHEN I.TradeOrPurchase = 1
			THEN 'P'
			WHEN I.TradeOrPurchase = 2
			THEN 'T'
			ELSE 'UNKNOWN'
		END AS TradeOrPurchase,
		I.MileageReceived,
		CASE I.CurrentVehicleLight
			WHEN 1 THEN 'Red'
			WHEN 2 THEN 'Yellow'
			WHEN 3 THEN 'Green'
			ELSE 'UNKNOWN'
		END AS CurrentVehicleLight,
		VS.SaleDescription,
		Case VS.SaleDescription When 'R' Then 1 Else 0 End as SaleDescriptionValue,
		I.StockNumber
FROM	[FLDW].dbo.Inventory I
JOIN	[FLDW].dbo.VehicleSale VS on I.InventoryID = VS.InventoryID
JOIN	[FLDW].dbo.Vehicle V on I.VehicleID = V.VehicleID And I.BusinessUnitID = V.BusinessUnitID
JOIN	[FLDW].dbo.Segment DBT on V.SegmentID = DBT.SegmentID
--JOIN IMT.dbo.Vehicle IV on IV.VehicleID = V.VehicleID
JOIN    [FLDW].dbo.MakeModelGrouping MMG ON MMG.MakeModelGroupingID=V.MakeModelGroupingID

WHERE	I.BusinessUnitID = @LocalDealerID
AND		I.InventoryType = 2
AND 	I.TradeOrPurchase BETWEEN @TPFilterLow AND @TPFilterHigh
AND		VS.DealDate Between @BeginDate And @EndDate
AND	   (VS.saledescription = 'R' OR (VS.saledescription = 'W' and I.DaysToSale >= 30))

Failed:
SET NOCOUNT OFF
END

GO

GRANT EXEC ON [Reports].[dbo].[Fetch#RetailInventorySalesAnalysisDetail] TO firstlook 
GO
