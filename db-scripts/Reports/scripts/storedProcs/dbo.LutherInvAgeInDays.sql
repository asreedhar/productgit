SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[LutherInvAgeInDays]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[LutherInvAgeInDays]
GO


/******************************************************************************
**
**  Procedure:[dbo].[LutherInvAgeInDays]
**  Description: query for Luther(101676) to get snapshot of data
**   
**        
**
**  Return values:  

** 
**  Input parameters:   none
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  10.11.12     EWimmer   Procedure created.
**
*******************************************************************************/



Create PROCEDURE [dbo].[LutherInvAgeInDays] AS
BEGIN
SET NOCOUNT ON



IF OBJECT_ID('tempdb.. #Parent') IS NOT NULL DROP TABLE  #Parent

CREATE TABLE #Parent(BusinessUnitName VARCHAR(100), businessunitid INT, RetailUnitsSold INT)

--ewimmer 
INSERT INTO #Parent
SELECT c.businessunit, c.businessunitid,
COUNT(1) AS RetailUnitsSold
FROM FLDW.dbo.InventorySales_F AS a
INNER JOIN FLDW.dbo.VehicleSale AS b WITH (NOLOCK) ON b.inventoryid = a.InventoryID
INNER JOIN [FLDW].dbo.BusinessUnit AS c WITH (NOLOCK) ON c.BusinessUnitID = a.BusinessUnitID
INNER JOIN [FLDW].dbo.BusinessUnitRelationship AS d  WITH (NOLOCK) ON d.BusinessUnitID = c.BusinessUnitID
WHERE d.ParentID = 101676  AND c.active = 1 AND a.InventoryTypeID = 2 AND b.SaleDescription = 'R' 
AND b.dealdate BETWEEN DATEADD(DD,-31,GETDATE()) AND GETDATE()
GROUP BY c.businessunit,c.businessunitid
ORDER BY c.businessunit



IF OBJECT_ID('tempdb..#Dealers1') IS NOT NULL DROP TABLE #Dealers1

CREATE TABLE #Dealers1 (ParentID INT, BusinessUnitID INT,BusinessUnit VARCHAR(50))

INSERT INTO #Dealers1
SELECT	DR.ParentID, 
	DS.BusinessUnitID,
	DS.BusinessUnit
FROM	[IMT].dbo.BusinessUnit DS WITH(NOLOCK)
INNER JOIN	[IMT].dbo.BusinessUnitRelationship DR WITH(NOLOCK) ON DR.BusinessUnitID = DS.BusinessUnitID
INNER JOIN	[IMT].dbo.DealerPreference DP WITH(NOLOCK) ON DP.BusinessUnitID = DS.BusinessUnitID
INNER JOIN	(	SELECT	BusinessUnitID, SUM(Active) NumerOfActiveUpgrades
			FROM	[IMT].dbo.DealerUpgrade WITH(NOLOCK)
			GROUP
			BY		BusinessUnitID
		) DU ON DU.BusinessUnitID = DS.BusinessUnitID
WHERE	DS.Active = 1
AND		DP.GoLiveDate IS NOT NULL
AND		DS.BusinessUnitTypeID = 4
AND		DU.NumerOfActiveUpgrades > 0
--for Luther only 
AND             DR.ParentID = 101676

IF OBJECT_ID('tempdb..#InventorySnapshot1') IS NOT NULL DROP TABLE #InventorySnapshot1

CREATE TABLE #InventorySnapshot1  (ParentID INT, BusinessUnitID INT, InventoryID INT, AgeInDays INT,InventoryReceivedDate  SMALLDATETIME,TradeOrPurchase INT)

INSERT INTO #InventorySnapshot1
SELECT	DS.ParentID AS ParentID,
	I.BusinessUnitID AS BusinessUnitID,
	I.InventoryID AS InventoryID,
	I.AgeInDays AS AgeInDays,
	CONVERT(datetime, CAST(ReceivedDateTimeID AS CHAR(12)), 112) AS InventoryReceivedDate,
	AcquisitionTypeID AS TradeOrPurchase
FROM	#Dealers1 DS
INNER JOIN	FLDW.dbo.InventorySales_F I WITH(NOLOCK)  ON I.BusinessUnitID = DS.BusinessUnitID
INNER JOIN	FLDW.dbo.InventoryActive IA WITH(NOLOCK)  ON IA.BusinessUnitID = I.BusinessUnitID AND IA.InventoryID = I.InventoryID
--INNER JOIN FLDW.dbo.InventoryBucketRanges AS IR ON IR.BusinessUnitID = DS.ParentID AND I.AgeInDays BETWEEN IR.Low AND IR.High
WHERE	I.InventoryTypeID = 2  --used
AND	IA.InventoryActive = 1  --active
AND             DS.ParentID = 101676

IF OBJECT_ID('tempdb..#SalesInPeriod1') IS NOT NULL DROP TABLE #SalesInPeriod1

CREATE TABLE  #SalesInPeriod1   (BusinessUnitID  INT, InventoryID INT, DealDate SMALLDATETIME, SaleDescription VARCHAR(20),FrontEndGross DECIMAL(9,2),BackEndGross DECIMAL(8,2))

INSERT INTO #SalesInPeriod1
SELECT	VS.BusinessUnitID,
		VS.InventoryID,
		VS.DealDate,
		VS.SaleDescription,	
		VS.FrontEndGross,
		VS.BackEndGross 

FROM	#Dealers1 DS
INNER JOIN FLDW.dbo.InventorySales_F I WITH(NOLOCK) ON I.BusinessUnitID = DS.BusinessUnitID
INNER JOIN [FLDW].dbo.VehicleSale vs WITH(NOLOCK) ON i.inventoryID = vs.InventoryID
WHERE	VS.SaleDescription = 'R'
AND VS.DealDate BETWEEN DATEADD(DD,-31,GETDATE()) AND GETDATE()
AND  DS.ParentID = 101676


--results
SELECT	SnapshotDt = CONVERT(DATETIME, CONVERT(INT, GETDATE())),
	DS.BusinessUnitID,
	DS.BusinessUnit businessunit,
	P.RetailUnitsSold,
	INV.Inventory0to30Days,
	INV.InventoryBetween31and45Days,
	INV.InventoryBetween46and60Days,
	INV.InventoryOver60Days
FROM	#Dealers1 DS
LEFT JOIN
	(SELECT	BusinessUnitID,
				(
					COUNT(DISTINCT(InventoryID))
				) InventoryCount,
				(
					SUM(CASE WHEN AgeInDays < 31 THEN 1 ELSE 0 END)
				) Inventory0to30Days,
				
				(
					SUM(CASE WHEN AgeInDays BETWEEN 31 AND 45 THEN 1 ELSE 0 END)
				) InventoryBetween31and45Days,

				(
					SUM(CASE WHEN AgeInDays BETWEEN 46 AND 60 THEN 1 ELSE 0 END)
				) InventoryBetween46and60Days,

				(
					SUM(CASE WHEN AgeInDays > 60 THEN 1 ELSE 0 END)
				) InventoryOver60Days
		
FROM	#InventorySnapshot1
GROUP 	BY BusinessUnitID
	 ) INV on DS.BusinessUnitID = INV.BusinessUnitID 
LEFT JOIN
	(SELECT	BusinessUnitID,
				(
					COUNT(DISTINCT(InventoryID))
				) RetailSales,				
				(
					COUNT(DISTINCT(InventoryID))*1.5
				) TargetStock45Day	
		FROM	#SalesInPeriod1
		GROUP
		BY		BusinessUnitID
	) S on DS.BusinessUnitID = S.BusinessUnitID  --Sales Prior 30 Days
LEFT JOIN #Parent AS P ON P.businessunitid = DS.BusinessUnitID
ORDER BY DS.BusinessUnit ASC


IF OBJECT_ID('tempdb..#InventorySnapshot1') IS NOT NULL DROP TABLE #InventorySnapshot1

IF OBJECT_ID('tempdb..#Dealers1') IS NOT NULL DROP TABLE #Dealers1
 
IF OBJECT_ID('tempdb..#SalesInPeriod1') IS NOT NULL DROP TABLE #SalesInPeriod1

IF OBJECT_ID('tempdb..#Parent') IS NOT NULL DROP TABLE #Parent



END



