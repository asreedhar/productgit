 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_SalesByAppraiser_NoSale]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_SalesByAppraiser_NoSale] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_SalesByAppraiser_NoSale] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_SalesByAppraiser_NoSale]
	@PeriodID INT,
	@DealerID INT,
	@AppraisalTypeID INT
AS

SET NOCOUNT ON;

SET ANSI_WARNINGS OFF;

DECLARE @BeginDate SMALLDATETIME
DECLARE @EndDate SMALLDATETIME

SELECT	@BeginDate = BeginDate, @EndDate = EndDate
FROM	[FLDW].dbo.Period_D
WHERE	PeriodID = @PeriodID

DECLARE @TradeOrPurchase TINYINT

/**
 *  Behold some great design:
 *
 *	SELECT * FROM IMT.dbo.AppraisalType
 *	SELECT * FROM IMT.dbo.lu_TradeOrPurchase
 */
 
SELECT @TradeOrPurchase = 
	CASE @AppraisalTypeID
		WHEN 1 THEN 2
		WHEN 2 THEN 1
		ELSE 3
	END
	
SELECT	count(*) as NoSales, avg(FrontEndGross) as NoSaleAvgLoss

FROM	[FLDW].dbo.Inventory AS I 
JOIN	[FLDW].dbo.VehicleSale AS VS ON I.InventoryID = VS.InventoryID
JOIN	[FLDW].dbo.Vehicle AS V ON I.BusinessUnitID = V.BusinessUnitID AND I.VehicleID = V.VehicleID 
LEFT JOIN	[FLDW].dbo.Appraisal_F A ON I.BusinessUnitID = A.BusinessUnitID AND V.vehicleId = A.vehicleId 
LEFT JOIN	[FLDW].dbo.AppraisalValues AV on A.Value = AV.Value and A.AppraisalId = AV.AppraisalID AND A.AppraisalValueID=AV.AppraisalValueID

WHERE	I.BusinessUnitID  = @DealerID
AND		VS.SaleDescription = 'W'
AND		VS.DealDate BETWEEN @BeginDate AND @EndDate
AND		I.DaysToSale > 29
AND		I.TradeOrPurchase = @TradeOrPurchase
--AND 	A.AppraisalTypeID = @AppraisalTypeID -- users may forget to mark appraisal as trade or purchase! 
	


GO


