
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_GrossProfitByDaysInStock#SummarySalesAged]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_GrossProfitByDaysInStock#SummarySalesAged] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_GrossProfitByDaysInStock#SummarySalesAged] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_GrossProfitByDaysInStock#SummarySalesAged]
	@PeriodID INT,
	@DealerGroupID INT,
	@MemberID INT

AS

/******************************************************************************************************************
*	Routine:	dbo.rc_GrossProfitByDaysInStock#SummarySalesAgedByBusinessUnitID
*	Purpose:	Driver routine for Gross Profit by Days in Stock Summary Sales Aged table
*				Based on dbo.rc_GrossProfitByDaysInStock#SummarySalesAged.
*
*	History
*	2/26/2010	SVU	Initial Creation
*	05/24/2010	MAK	Updated to display by BusinessUnitID.
*					DON'T BE LIKE THAT BECAUSE I PIVOTED A TABLE.
*					I switched this matrix to a table because the Business wanted to be able to drill down into a group
*					and I was not able to do this with a matrix.  This data set is NOT Ideal for a matrix anyway due to
*					the variations of datatypes and due to the fact that we want to save the raw data anyway as not to
*					calculate the average of an average.  Certain design decisions are due to the evolution of this 
*					report and procedure.
*
*
*******************************************************************************************************************/

SET NOCOUNT ON ;

DECLARE @MemberAccessTable TABLE (BusinessUnitID INT)

/* The @Results table holds the data in it's final form.*/
DECLARE @Results TABLE
    (
      DataType VARCHAR(50) ,			-- This becomes the name of the row in the report (i.e. 'Units Sold','% Profit')
      DataTypeID TINYINT ,				-- This is a # 1-6 coressponding to a datatype and identifies formatting in the report.
      BusinessUnit VARCHAR(50) ,		-- Dealer Name
      BusinessUnitID INT ,
      DataValueR021_1 DECIMAL(9, 2) ,	--  Each of the following 7 column coressponds to an Age Bucket.
      DataValueR2229_1 DECIMAL(9, 2) ,  --	The 'R' is for retail and the next numbers are the age range. 
      DataValueR3039_1 DECIMAL(9, 2) ,	--  The  'W' is for wholesale.  An extra '_' signifies a '+'.  The '1' is not used, but I don't want to redo the report.
      DataValueR4049_1 DECIMAL(9, 2) ,
      DataValueR5059_1 DECIMAL(9, 2) ,
      DataValueR60__1 DECIMAL(9, 2) ,
      DataValueW30__1 DECIMAL(9, 2) ,
      TotalField DECIMAL(9, 2)			--  This total field is dependent on the DataType.  This Data point is used to help the rollups
	 )									--	It is equal to Units Sold when calculating Avg Gross or % Units.  It is Total Gross when calculating % Profit.
										--	It is not used when calculating total Gross so I use a filler of 1

DECLARE @DGData TABLE
    (
      BusinessUnit VARCHAR(50) ,
      BusinessUnitID INT ,
      DaysToSaleGrouping VARCHAR(50) ,
      GroupTypeID TINYINT ,
      FrontEndGross DECIMAL(9, 2) ,
	  BackEndGross DECIMAL(9, 2) ,
      Gross DECIMAL(9, 2) ,
      UnitsSold INT
    )

;
WITH    MemberBusinessUnitSet
          AS (SELECT    I.BusinessUnitID
              FROM      [IMT].dbo.MemberBusinessUnitSet S
              JOIN      [IMT].dbo.MemberBusinessUnitSetItem I ON S.MemberBusinessUnitSetID = I.MemberBusinessUnitSetID
              WHERE     S.MemberID = @MemberID
                        AND S.MemberBusinessUnitSetTypeID = 1 --Command Center Dealer Group Set
                        
             )
        INSERT  INTO @MemberAccessTable
                (BusinessUnitID)
                SELECT  DISTINCT MBUS.BusinessUnitID
                FROM    dbo.MemberAccess(@DealerGroupID, @MemberID) MA
                JOIN    MemberBusinessUnitSet MBUS ON MA.BusinessUnitID = MBUS.BusinessUnitID

INSERT INTO @DGData ( BusinessUnit,BusinessUnitID,GroupTypeID, UnitsSold, FrontEndGross, BackEndGross, Gross)
SELECT  	BU.BusinessUnit,BU.BusinessUnitID,
	CASE	WHEN I.DaysToSale < 22 AND S.SaleDescription = 'R' THEN 1
		WHEN I.DaysToSale < 30 AND S.SaleDescription = 'R' THEN 2
		WHEN I.DaysToSale < 40 AND S.SaleDescription = 'R' THEN 3
		WHEN I.DaysToSale < 50 AND S.SaleDescription = 'R' THEN 4
		WHEN I.DaysToSale < 60 AND S.SaleDescription = 'R' THEN 5
		WHEN I.DaysToSale > 59 AND S.SaleDescription = 'R' THEN 6
		WHEN I.DaysToSale > 29 AND S.SaleDescription = 'W' THEN 7
		ELSE 0
	END  ,
	COUNT(*) UnitsSold,
	SUM(S.FrontEndGross) FrontEndGross ,
	SUM(S.BackEndGross) BackEndGross ,
	SUM(S.FrontEndGross + S.BackEndGross) 'Gross' 
FROM    [FLDW].dbo.Inventory I
JOIN    [FLDW].dbo.VehicleSale S ON I.InventoryID = S.InventoryID
JOIN    [FLDW].dbo.Vehicle V ON I.VehicleID = V.VehicleID
                                      AND I.BusinessUnitID = V.BusinessUnitID
JOIN    [IMT].dbo.MakeModelGrouping MakeModel ON V.MakeModelGroupingID = MakeModel.MakeModelGroupingID
JOIN    [IMT].dbo.Segment ON MakeModel.SegmentID = Segment.SegmentID
JOIN    @MemberAccessTable MAT ON MAT.BusinessUnitID = I.BusinessUnitID
JOIN	IMT.dbo.BusinessUnit BU ON BU.BusinessUnitID = I.BusinessUnitID
CROSS JOIN [FLDW].dbo.Period_D Period
WHERE   I.InventoryType = 2
	AND Period.PeriodID = @PeriodID
	AND S.DealDate BETWEEN Period.BeginDate AND Period.EndDate
	AND (S.SaleDescription = 'R' OR (S.SaleDescription = 'W' AND I.DaysToSale > 29))
GROUP BY	BU.BusinessUnit,BU.BusinessUnitID,
				CASE	WHEN I.DaysToSale < 22 AND S.SaleDescription = 'R' THEN 1
		WHEN I.DaysToSale < 30 AND S.SaleDescription = 'R' THEN 2
		WHEN I.DaysToSale < 40 AND S.SaleDescription = 'R' THEN 3
		WHEN I.DaysToSale < 50 AND S.SaleDescription = 'R' THEN 4
		WHEN I.DaysToSale < 60 AND S.SaleDescription = 'R' THEN 5
		WHEN I.DaysToSale > 59 AND S.SaleDescription = 'R' THEN 6
		WHEN I.DaysToSale > 29 AND S.SaleDescription = 'W' THEN 7
		ELSE 0
		END WITH ROLLUP

/*  The following CTE formats the data into a table by BusinessUnit.
	The Union statement below it turns it into a table by BusinessUnit, DataType.
*/
 ;
WITH DGData_byAgeBucket
	(BusinessUnit,BusinessUnitID,TotalUnitsSold,TotalGross,
	D1UnitsSold,D2UnitsSold,D3UnitsSold,D4UnitsSold,D5UnitsSold,D6UnitsSold,D7UnitsSold,
	D1FrontEndGross,D2FrontEndGross,D3FrontEndGross,D4FrontEndGross,D5FrontEndGross,D6FrontEndGross,D7FrontEndGross,
	D1BackEndGross,D2BackEndGross,D3BackEndGross,D4BackEndGross,D5BackEndGross,D6BackEndGross,D7BackEndGross)
AS	
(
SELECT	DD0.BusinessUnit,DD0.BusinessUnitID,DD0.UnitsSold,DD0.Gross,
		DD1.UnitsSold,DD2.UnitsSold,DD3.UnitsSold,DD4.UnitsSold,DD5.UnitsSold,DD6.UnitsSold,DD7.UnitsSold,
		DD1.FrontEndGross,DD2.FrontEndGross,DD3.FrontEndGross,DD4.FrontEndGross,DD5.FrontEndGross,DD6.FrontEndGross,DD7.FrontEndGross,
		DD1.BackEndGross,DD2.BackEndGross,DD3.BackEndGross,DD4.BackEndGross,DD5.BackEndGross,DD6.BackEndGross,DD7.BackEndGross
FROM	@DGData DD1
JOIN	@DGData DD2 ON DD1.BusinessUnitID=DD2.BusinessUnitID
JOIN	@DGData DD3 ON DD1.BusinessUnitID=DD3.BusinessUnitID
JOIN	@DGData DD4 ON DD1.BusinessUnitID=DD4.BusinessUnitID
JOIN	@DGData DD5 ON DD1.BusinessUnitID=DD5.BusinessUnitID
JOIN	@DGData DD6 ON DD1.BusinessUnitID=DD6.BusinessUnitID
JOIN	@DGData DD7 ON DD1.BusinessUnitID=DD7.BusinessUnitID
JOIN	@DGData DD0 ON DD1.BusinessUnitID =DD0.BusinessUnitID
WHERE	DD1.GroupTYpeID =1
	AND DD2.GroupTYpeID =2
	AND DD3.GroupTYpeID =3
	AND DD4.GroupTYpeID =4
	AND DD5.GroupTYpeID =5
	AND DD6.GroupTYpeID =6
	AND DD7.GroupTYpeID =7
	AND DD0.GroupTYpeID IS NULL
)	 

INSERT 
INTO  
	@Results
	(DataType,DataTypeID,BusinessUnit,BusinessUnitID,
	DataValueR021_1	,DataValueR2229_1,DataValueR3039_1	,DataValueR4049_1,
	DataValueR5059_1,DataValueR60__1,DataValueW30__1	,TotalField 
)
SELECT 'Units Sold',1,BusinessUnit,BusinessUnitID,
		D1UnitsSold,D2UnitsSold ,D3UnitsSold,D4UnitsSold,
		D5UnitsSold,D6UnitsSold,D7UnitsSold,TotalUnitsSold
FROM	DGData_byAgeBucket
UNION 
SELECT '% Profit',2,BusinessUnit,BusinessUnitID,
		D1FrontEndGross+D1BackEndGross,D2FrontEndGross+D2BackEndGross,D3FrontEndGross+D3BackEndGross,D4FrontEndGross+D4BackEndGross,
		D5FrontEndGross+D5BackEndGross,D6FrontEndGross+D6BackEndGross,D7FrontEndGross+D7BackEndGross,TotalGross 
FROM	DGData_byAgeBucket
UNION
SELECT 'Avg Front End Gross',3,BusinessUnit,BusinessUnitID,
		D1FrontEndGross,D2FrontEndGross,D3FrontEndGross,D4FrontEndGross,
		D5FrontEndGross,D6FrontEndGross,D7FrontEndGross,TotalUnitsSold
FROM	DGData_byAgeBucket
UNION
SELECT 'Total Front End Gross',4,BusinessUnit,BusinessUnitID,
		D1FrontEndGross,D2FrontEndGross,D3FrontEndGross,D4FrontEndGross,
		D5FrontEndGross,D6FrontEndGross,D7FrontEndGross,1
FROM	DGData_byAgeBucket
UNION
SELECT 'Avg Back End Gross',5,BusinessUnit,BusinessUnitID,
		D1BackEndGross,D2BackEndGross,D3BackEndGross,D4BackEndGross,
		D5BackEndGross,D6BackEndGross,D7BackEndGross,TotalUnitsSold
FROM	DGData_byAgeBucket
UNION 
SELECT 'Total Back End Gross',6,BusinessUnit,BusinessUnitID,
		D1BackEndGross,D2BackEndGross,D3BackEndGross,D4BackEndGross,
		D5BackEndGross,D6BackEndGross,D7BackEndGross,1
FROM	DGData_byAgeBucket
		
		
SELECT * FROM @Results		 

GO