
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_DealerGroupSystemUsageSummary]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_DealerGroupSystemUsageSummary] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_DealerGroupSystemUsageSummary] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_DealerGroupSystemUsageSummary]
	@DealerGroupID INT
	,@MemberID INT
	
AS

DECLARE @M INT
SET @M = @MemberID

DECLARE @DGID INT
SET @DGID=@DealerGroupID

SET NOCOUNT ON ;

WITH    TradeInAnalyzed_FoursWeeks
          AS (SELECT    M.BusinessUnitID ,
                        M.Measure
              FROM      [EdgeScorecard].dbo.Measures M
              WHERE     M.MetricID = 144
                        AND M.PeriodID = 2
                        AND M.SetID = 2
             ),
        TradeIns_FoursWeeks
          AS (SELECT    M.BusinessUnitID ,
                        M.Measure
              FROM      [EdgeScorecard].dbo.Measures M
              WHERE     M.MetricID = 145
                        AND M.PeriodID = 2
                        AND M.SetID = 2
             ),
        A2
          AS (SELECT    T1.BusinessUnitID ,
                        CAST(CASE WHEN T2.Measure < 1 THEN NULL
                                  ELSE T1.Measure / T2.Measure
                             END AS dec(5, 2)) 'Measure'
              FROM      TradeInAnalyzed_FoursWeeks T1
              LEFT JOIN TradeIns_FoursWeeks T2 ON T2.BusinessUnitID = T1.BusinessUnitID
             ),
        A6
          AS -- AgedUnitsWithValidPlan_Current
(SELECT M.BusinessUnitID ,
        M.Measure
 FROM   [EdgeScorecard].dbo.Measures M
 WHERE  M.MetricID = 284
        AND M.PeriodID = 7
        AND M.SetID = 142
),      A7
          AS -- AgedUnitsInStock_Current
(SELECT M.BusinessUnitID ,
        M.Measure
 FROM   [EdgeScorecard].dbo.Measures M
 WHERE  M.MetricID = 282
        AND M.PeriodID = 7
        AND M.SetID = 142
),      A8
          AS -- WatchListUnitsWithValidPlan_Current
(SELECT M.BusinessUnitID ,
        M.Measure
 FROM   [EdgeScorecard].dbo.Measures M
 WHERE  M.MetricID = 290
        AND M.PeriodID = 7
        AND M.SetID = 142
),      A9
          AS -- WatchListUnitsInStock_Current
(SELECT M.BusinessUnitID ,
        M.Measure
 FROM   [EdgeScorecard].dbo.Measures M
 WHERE  M.MetricID = 288
        AND M.PeriodID = 7
        AND M.SetID = 142
),      InventoryPlanned
          AS (SELECT    A7.BusinessUnitID ,
                        CASE WHEN CAST(COALESCE(A9.Measure, 0) AS INT) + CAST(COALESCE(A7.Measure, 0) AS INT) <> 0
                             THEN (CAST(A6.Measure AS DECIMAL(12, 2)) + CAST(A8.Measure AS INT))
                                  / (CAST(COALESCE(A9.Measure, 0) AS INT) + CAST(COALESCE(A7.Measure, 0) AS INT))
                             ELSE CAST(0 AS DECIMAL(12, 2))
                        END [Inventory Planned]
              FROM      A7
              LEFT JOIN A6 ON A7.BusinessUnitID = A6.BusinessUnitID
              LEFT JOIN A8 ON A6.BusinessUnitID = A8.BusinessUnitID
              LEFT JOIN A9 ON A8.BusinessUnitID = A9.BusinessUnitID
             )
        SELECT  DG.BusinessUnit [Dealer Group] ,
                DS.BusinessUnitShortName Dealer ,
                DS.BusinessUnitID [Dealer ID] ,
                a2.Measure [Trade-In Inventory Analyzed] ,
                IP.[Inventory Planned] ,
                CAST((COALESCE(A2.Measure,0.00) + COALESCE(IP.[Inventory Planned],0.00)) / 2 AS DECIMAL(12, 2)) [Usage Score] ,
                CASE WHEN CAST((COALESCE(A2.Measure,0.00) + COALESCE(IP.[Inventory Planned],0.00)) / 2 AS DECIMAL(12, 2)) < .25 THEN 1
                     ELSE 0
                END RedCount ,
                CASE WHEN CAST((COALESCE(A2.Measure,0.00) + COALESCE(IP.[Inventory Planned],0.00)) / 2 AS DECIMAL(12, 2)) >= .25
                          AND CAST((COALESCE(A2.Measure,0.00) + COALESCE(IP.[Inventory Planned],0.00)) / 2 AS DECIMAL(12, 2)) < .75 THEN 1
                     ELSE 0
                END YellowCount ,
                CASE WHEN CAST((COALESCE(A2.Measure,0.00) + COALESCE(IP.[Inventory Planned],0.00)) / 2 AS DECIMAL(12, 2)) >= .75 THEN 1
                     ELSE 0
                END GreenCount
        FROM    [IMT].dbo.BusinessUnit DS
        JOIN    [IMT].dbo.BusinessUnitRelationship DR ON DR.BusinessUnitID = DS.BusinessUnitID
        JOIN    [IMT].dbo.BusinessUnit DG ON DG.BusinessUnitID = DR.ParentID
        JOIN    [IMT].dbo.DealerPreference DP ON DP.BusinessUnitID = DS.BusinessUnitID
        JOIN    dbo.MemberAccess(@DGID, @M) MA ON MA.BusinessUnitID = DS.BusinessUnitID
        JOIN    (SELECT BusinessUnitID ,
                        SUM(Active) NumerOfActiveUpgrades
                 FROM   [IMT].dbo.DealerUpgrade
                 GROUP BY BusinessUnitID
                ) DU ON DU.BusinessUnitID = DS.BusinessUnitID
        LEFT JOIN A2 ON DS.BusinessUnitID = A2.BusinessUnitID
        LEFT JOIN A6 ON DS.BusinessUnitID = A6.BusinessUnitID
        LEFT JOIN A7 ON DS.BusinessUnitID = A7.BusinessUnitID
        LEFT JOIN A8 ON DS.BusinessUnitID = A8.BusinessUnitID
        LEFT JOIN A9 ON DS.BusinessUnitID = A9.BusinessUnitID
        LEFT JOIN InventoryPlanned IP ON DS.BusinessUnitID = IP.BusinessUnitID
        WHERE   DS.Active = 1
                AND DP.GoLiveDate IS NOT NULL
                AND DS.BusinessUnitTypeID = 4
                AND DU.NumerOfActiveUpgrades > 0
                AND DG.BusinessUnitID = @DGID

GO
