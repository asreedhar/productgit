
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_CurrentInventoryRepricingHistoryEDGE]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_CurrentInventoryRepricingHistoryEDGE] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_CurrentInventoryRepricingHistoryEDGE] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_CurrentInventoryRepricingHistoryEDGE]
	@DealerID INT
AS

/************************************************************************************************
**	
**	MAK 05/26/2009	Updated so that only reprices with actual priceing changes display.  This is
**				 	related to the change made in rc_CurrentInventeoryRepricingHistoryListEdge. 
**
*************************************************************************************************/

SET NOCOUNT ON;

WITH InventoryPriceHistory AS
(
	SELECT	ilph1.InventoryID,
			Cast(Cast(ilph1.Notes2 as float) as int) ListPrice,
			ilph1.Created DMSReferenceDt,
			RANK() OVER (PARTITION BY ilph1.InventoryID order by ilph1.Created) idx
	FROM	(
				/*The price when the item came into our system*/
				SELECT	E1.InventoryID, I.InventoryReceivedDate AS Created, Notes2
				FROM [IMT].dbo.AIP_Event E1 WITH(NOLOCK)
				JOIN [IMT].dbo.Inventory I WITH(NOLOCK) ON E1.InventoryID = I.InventoryID
				JOIN (
					SELECT	E.InventoryID, MIN(AIP_EventID) AIP_EventID
					FROM	[IMT].dbo.AIP_Event E WITH(NOLOCK)
					JOIN	[IMT].dbo.Inventory I WITH(NOLOCK) ON E.InventoryID = I.InventoryID
					WHERE	E.AIP_EventTypeID = 5
					AND		E.Value != 0
					AND		I.BusinessUnitID = @DealerID
					AND		I.InventoryActive = 1
					AND		I.InventoryType = 2
					GROUP
					BY		E.InventoryID
				) E2 ON E1.InventoryID = E2.InventoryID AND E1.AIP_EventID = E2.AIP_EventID		
			UNION ALL
				/* Reprices */
				SELECT	I.InventoryID, E.Created, E.Value AS Notes2
				FROM	[IMT].dbo.AIP_Event E WITH(NOLOCK)
				JOIN	[IMT].dbo.Inventory I WITH(NOLOCK) ON E.InventoryID = I.InventoryID
				WHERE	E.AIP_EventTypeID = 5
				AND		E.Notes2 != '0.0'
				AND		IsNumeric(E.Notes2)=1
				AND		CAST(E.Notes2 as Decimal(9,2)) <> e.Value
				AND		I.BusinessUnitID = @DealerID
				AND		I.InventoryActive = 1
				AND		I.InventoryType = 2	
			) ilph1
	JOIN	[IMT].dbo.Inventory i WITH(NOLOCK) ON ilph1.InventoryID = i.InventoryID

	GROUP
	BY		ilph1.InventoryID, ilph1.Notes2, ilph1.Created
),
InventoryPriceCount AS
(
	SELECT InventoryID, MAX(idx)-1 Counter FROM InventoryPriceHistory GROUP BY InventoryID
),
InventoryPrices AS
(
	SELECT	InventoryID AS InventoryID, [1] AS [InitialPrice], [2] AS [Reprice1], [3] AS [Reprice2], [4] AS [Reprice3], [5] AS [Reprice4], [6] AS [Reprice5], [7] AS [Reprice6]
	FROM
	(SELECT	InventoryID, ListPrice, idx FROM InventoryPriceHistory) As SourceTable
	PIVOT
	(MAX(ListPrice) FOR idx in ([1], [2], [3], [4], [5], [6], [7])) AS PivotTable
),
InventoryPriceDates AS
(
	SELECT	InventoryID AS InventoryID, [1] AS [DMSdt0], [2] AS [DMSdt1], [3] AS [DMSdt2], [4] AS [DMSdt3], [5] AS [DMSdt4], [6] AS [DMSdt5], [7] AS [DMSdt6]
	FROM
	(SELECT	InventoryID, DMSReferenceDt, idx FROM InventoryPriceHistory) As SourceTable
	PIVOT
	(MAX(DMSReferenceDt) FOR idx in ([1], [2], [3], [4], [5], [6], [7])) AS PivotTable
)
SELECT	I.InventoryID,
		IC.Counter Counter,
		V.VehicleYear vYear,
		MMG.Make vMake,
		MMG.Model vModel,
		V.BaseColor,
		V.VehicleTrim,
		V.VIN,
		DBT.Segment DisplayBodyType,
		I.StockNumber,
		I.InventoryReceivedDate,
		I.UnitCost,
		I.MileageReceived,
		IP.InitialPrice, IP.Reprice1, IP.Reprice2, IP.Reprice3, IP.Reprice4, IP.Reprice5, IP.Reprice6,
		IPD.DMSdt0, IPD.DMSdt1, IPD.DMSdt2, IPD.DMSdt3, IPD.DMSdt4, IPD.DMSdt5,
		DATEDIFF(DD, i.InventoryReceivedDate, GETDATE()) 'Age',
		CASE
			WHEN DATEDIFF(DD, i.InventoryReceivedDate, GETDATE()) >=60 THEN 60
			WHEN DATEDIFF(DD, i.InventoryReceivedDate, GETDATE()) >=50 THEN 50
			WHEN DATEDIFF(DD, i.InventoryReceivedDate, GETDATE()) >=40 THEN 40
			WHEN DATEDIFF(DD, i.InventoryReceivedDate, GETDATE()) >=30 THEN 30
			WHEN DATEDIFF(DD, i.InventoryReceivedDate, GETDATE()) >=21 THEN 21
			ELSE 0
		END as 'Bucket',
		IB.BookValue BookValue,
		TP.Description + COALESCE(' - ' + TPC.Category, '') Book
FROM	[IMT].dbo.Inventory I WITH(NOLOCK)
JOIN	[IMT].dbo.tbl_Vehicle v WITH(NOLOCK) ON i.VehicleID = v.VehicleID
JOIN	[IMT].dbo.MakeModelGrouping MMG ON V.MakeModelGroupingid = MMG.MakeModelGroupingID
JOIN	[IMT].dbo.Segment DBT ON DBT.SegmentID = MMG.SegmentID
JOIN	[IMT].dbo.DealerPreference DP WITH(NOLOCK) ON DP.BusinessUnitID = I.BusinessUnitID
JOIN	[IMT].dbo.ThirdParties TP ON TP.ThirdPartyID = DP.GuideBookID
LEFT JOIN FLDW.dbo.ThirdPartyCategories TPC ON TPC.ThirdPartyID = TP.ThirdPartyID AND TPC.ThirdPartyCategoryID = DP.BookOutPreferenceId
LEFT JOIN	[FLDW].dbo.InventoryBookout_F IB ON (
				IB.InventoryID = I.InventoryID
			AND	IB.ThirdPartyCategoryID = DP.BookOutPreferenceId
			AND	IB.BookoutValueTypeID = 2
		)
JOIN	InventoryPriceCount IC ON IC.InventoryID = I.InventoryID
JOIN	InventoryPrices IP ON IP.InventoryID = I.InventoryID
JOIN	InventoryPriceDates IPD ON IPD.InventoryID = I.InventoryID
WHERE	I.BusinessUnitID = @DealerID
	AND IC.Counter > 0
GO
