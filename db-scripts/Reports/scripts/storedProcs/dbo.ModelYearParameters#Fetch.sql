IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.ModelYearParameters#Fetch') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE dbo.ModelYearParameters#Fetch AS SELECT 1')
GO
 
ALTER PROCEDURE [dbo].[ModelYearParameters#Fetch]
	@MinModelYear INT = NULL,
	@MaxModelYear INT = NULL
AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.ModelYearParameters#Fetch.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns a list of vehicle ModelYears.  
 * 
 * Parameters
 * ----------
 * 
 * Exceptions
 * ----------
 *
 * History
 * ----------
 * 
 * BYF	2009-09-30
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON;

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

IF(@MinModelYear IS NULL)
BEGIN
	SET @MinModelYear = 1981
END

IF(@MinModelYear < 1981)
BEGIN
	RAISERROR ('MinModelYear must be 1981 or greater',16,1)
	RETURN @@ERROR
END

IF(@MaxModelYear < @MinModelYear)
BEGIN
	RAISERROR ('MaxModelYear must be greater than the MinModelYear %d',16,1, @MinModelYear)
	RETURN @@ERROR
END
------------------------------------------------------------------------------------------------
-- Declare Result Set
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	ModelYear INT
)

------------------------------------------------------------------------------------------------
-- Populate Result Set
------------------------------------------------------------------------------------------------
IF(@MaxModelYear IS NULL)
BEGIN
	SELECT 	@MaxModelYear = 
				CASE WHEN DATEPART(MONTH, GETDATE()) > 6 
					 THEN DATEPART(YEAR, GETDATE()) + 1 --make the next ModelYear available after june.
					 ELSE DATEPART(YEAR, GETDATE())
				END
END

DECLARE @TheModelYear INT;
SET @TheModelYear = @MinModelYear;

WHILE (@TheModelYear <= @MaxModelYear)
BEGIN

	INSERT INTO @Results(ModelYear)
	VALUES (@TheModelYear)

	SET @TheModelYear = @TheModelYear + 1;
END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT 	ModelYear
FROM @Results

RETURN 0

GO