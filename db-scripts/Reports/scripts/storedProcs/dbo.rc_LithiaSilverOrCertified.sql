
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_LithiaSilverOrCertified]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_LithiaSilverOrCertified] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_LithiaSilverOrCertified] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_LithiaSilverOrCertified]
	@DealerID        INT,
	@NumberOfWeeks   INT,
	@InventoryTypeID INT,
	@Certified       INT
AS

SET NOCOUNT ON

SET ANSI_WARNINGS OFF

DECLARE @BeginTimeID INT, @EndTimeID INT

IF SIGN(@NumberOfWeeks) = 1
	SELECT	@BeginTimeID = CAST(CONVERT(CHAR(8), DATEADD(YY, -1, GETDATE()), 112) AS INT),
			@EndTimeID = CAST(CONVERT(CHAR(8), DATEADD(WW, @NumberOfWeeks, DATEADD(YY, -1, GETDATE())), 112) AS INT)
ELSE
	SELECT	@BeginTimeID = CAST(CONVERT(CHAR(8), DATEADD(WW, @NumberOfWeeks, GETDATE()), 112) AS INT),
			@EndTimeID = CAST(CONVERT(CHAR(8), GETDATE(), 112) AS INT)

DECLARE @MileageLower INT, @MileageUpper INT

IF @Certified = 1
	SELECT	@MileageLower = 0, @MileageUpper = 2147483647
ELSE
	SELECT	@MileageLower = 75000, @MileageUpper = 2147483647

IF @InventoryTypeID = 2

	SELECT	VehicleGroupingDescription	= VH.Description,
		GroupingID			= VH.VehicleGroupingID,
		UnitsInStock			= CAST(SUM(CASE WHEN SaleTypeID = 5 THEN 1 ELSE 0 END) AS INT),
		UnitsSold			= CAST(SUM(CASE WHEN SaleTypeID = 1 THEN 1 ELSE 0 END) AS INT),
		AverageGrossProfit		= CAST(AVG(CASE WHEN SaleTypeID = 1 AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID THEN F.FrontEndGross+F.BackEndGross ELSE NULL END) AS INT),
		AverageBackEnd		= CAST(AVG(CASE WHEN SaleTypeID = 1 AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID THEN F.BackEndGross ELSE NULL END) AS INT),
		AverageFrontEnd		= CAST(AVG(CASE WHEN SaleTypeID = 1 AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID THEN F.FrontEndGross ELSE NULL END) AS INT),
		AverageDays			= AVG(CASE WHEN SaleTypeID = 1 AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID THEN F.DaysToSale ELSE NULL END),
		AverageMileage			= CAST(AVG(CASE WHEN SaleTypeID = 1 AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID THEN F.VehicleMileage ELSE NULL END) AS INT),
		TotalRevenue			= CAST(SUM(CASE WHEN SaleTypeID = 1 AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID THEN F.SalePrice ELSE NULL END) AS INT),
		TotalGrossMargin		= CAST(SUM(CASE WHEN SaleTypeID = 1 AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID THEN F.FrontEndGross ELSE NULL END) AS INT),
		TotalInventoryDollars 		= CAST(SUM(CASE WHEN SaleTypeID = 5 THEN F.UnitCost ELSE 0 END) AS INT),
		No_Sales			= SUM(CASE WHEN SaleTypeID = 2 AND F.DaysToSale >= 30 AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID THEN 1 ELSE 0 END),
		TotalBackEnd			= CAST(SUM(CASE WHEN SaleTypeID = 1 AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID THEN F.BackEndGross ELSE NULL END) AS INT), 
		TotalFrontEnd			= CAST(SUM(CASE WHEN SaleTypeID = 1 AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID THEN F.FrontEndGross ELSE NULL END) AS INT),
		AverageInvAge			= CAST(AVG(CASE WHEN SaleTypeID = 5 THEN F.AgeInDays ELSE NULL END) AS INT),
		Threshold			=MAX(CASE ABS(@NumberOfWeeks) When 4 Then dp.UnitsSoldThreshold4Wks When 8 Then dp.UnitsSoldThreshold8Wks When 13 Then dp.UnitsSoldThreshold13Wks When 26 Then dp.UnitsSoldThreshold26Wks Else dp.UnitsSoldThreshold52Wks End)

	FROM	[FLDW].dbo.InventorySales_F F
		JOIN [FLDW].dbo.Inventory I ON F.InventoryID = I.InventoryID AND F.BusinessUnitID = I.BusinessUnitID
		JOIN [FLDW].dbo.Vehicle V ON I.VehicleID = V.VehicleID AND I.BusinessUnitID = V.BusinessUnitID
		JOIN [FLDW].dbo.VehicleHierarchy_D VH ON F.VehicleHierarchyID = VH.VehicleHierarchyID
		JOIN [IMT].dbo.DealerPreference dp ON F.BusinessUnitID = dp.BusinessUnitID
	
	WHERE	F.BusinessUnitID = @DealerID
	 	AND F.InventoryTypeID = @InventoryTypeID
		AND I.MileageReceived BETWEEN @MileageLower AND @MileageUpper
		AND (F.SaleTypeID = 5 OR ((F.SaleTypeID = 1 OR (F.SaleTypeID = 2 AND F.DaysToSale >= 30)) AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID))
		AND I.Certified = @Certified
	GROUP
	BY	VH.Description, VH.VehicleGroupingID

ELSE IF @InventoryTypeID = 1

	SELECT	Make				= MMG.Make,
		Model				= MMG.Model,
		VehicleTrim			= V.VehicleTrim,
		Description			= NULL,
		VehicleBodyStyleID		= NULL,
		UnitsInStock			= CAST(SUM(CASE WHEN SaleTypeID = 5 THEN 1 ELSE 0 END) AS INT),
		UnitsSold			= CAST(SUM(CASE WHEN SaleTypeID = 1 THEN 1 ELSE 0 END) AS INT),
		AverageGrossProfit		= ISNULL(CAST(AVG(CASE WHEN SaleTypeID = 1 AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID THEN F.FrontEndGross ELSE NULL END) AS INT), 0),
		AverageBackEnd			= ISNULL(CAST(AVG(CASE WHEN SaleTypeID = 1 AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID THEN F.BackEndGross ELSE NULL END) AS INT), 0),
		AverageFrontEnd			= ISNULL(CAST(AVG(CASE WHEN SaleTypeID = 1 AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID THEN F.FrontEndGross ELSE NULL END) AS INT), 0),
		AverageDays			= ISNULL(AVG(CASE WHEN SaleTypeID = 1 AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID THEN F.DaysToSale ELSE NULL END), 0),
		AverageMileage			= ISNULL(CAST(AVG(CASE WHEN SaleTypeID = 1 AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID THEN F.VehicleMileage ELSE NULL END) AS INT), 0),
		TotalRevenue			= ISNULL(CAST(SUM(CASE WHEN SaleTypeID = 1 AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID THEN F.SalePrice ELSE NULL END) AS INT), 0),
		TotalGrossMargin		= ISNULL(CAST(SUM(CASE WHEN SaleTypeID = 1 AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID THEN F.FrontEndGross ELSE NULL END) AS INT), 0),
		TotalInventoryDollars 		= ISNULL(CAST(SUM(CASE WHEN SaleTypeID = 5 THEN F.UnitCost ELSE 0 END) AS INT), 0),
		No_Sales			= ISNULL(SUM(CASE WHEN SaleTypeID = 2 AND F.DaysToSale >= 30 AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID THEN 1 ELSE 0 END), 0),
		TotalBackEnd			= ISNULL(CAST(SUM(CASE WHEN SaleTypeID = 1 AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID THEN F.BackEndGross ELSE NULL END) AS INT),	0), 
		TotalFrontEnd			= ISNULL(CAST(SUM(CASE WHEN SaleTypeID = 1 AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID THEN F.FrontEndGross ELSE NULL END) AS INT), 0),
		AverageInvAge			= ISNULL(CAST(AVG(CASE WHEN SaleTypeID = 5 THEN F.AgeInDays ELSE NULL END) AS INT), 0)
	
	FROM	[FLDW].dbo.InventorySales_F F
		JOIN [FLDW].dbo.Inventory I ON F.InventoryID = I.InventoryID AND F.BusinessUnitID = I.BusinessUnitID
		JOIN [FLDW].dbo.Vehicle V ON I.VehicleID = V.VehicleID AND I.BusinessUnitID = V.BusinessUnitID
		JOIN [FLDW].dbo.MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID
	
	WHERE	F.BusinessUnitID = @DealerID
	 	AND F.InventoryTypeID = @InventoryTypeID
		AND I.MileageReceived BETWEEN @MileageLower AND @MileageUpper
		AND (F.SaleTypeID = 5 OR ((F.SaleTypeID = 1 OR (F.SaleTypeID = 2 AND F.DaysToSale >= 30)) AND F.DealDateTimeID BETWEEN @BeginTimeID AND @EndTimeID))
		AND I.Certified = @Certified
	GROUP
	BY	MMG.Make, MMG.Model, V.VehicleTrim
	
GO
