
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_WholesaleInventorySalesFldw]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_WholesaleInventorySalesFldw] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_WholesaleInventorySalesFldw] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_WholesaleInventorySalesFldw]
	@PeriodID INT,
	@DealerID INT
AS

SET NOCOUNT ON;

DECLARE @Buckets TABLE (
	counter tinyint,
	daysLow int,
	daysHigh int,
	saleDesc varchar(1),
	bucketDesc varchar(20)
)

INSERT INTO @Buckets (counter, daysLow, daysHigh, saleDesc, bucketDesc) VALUES (0,0,29,'W','Wholesaled 0-29 Days')
INSERT INTO @Buckets (counter, daysLow, daysHigh, saleDesc, bucketDesc) VALUES (1,30,999,'W','Aged Wholesale')

SELECT	b.counter,
		VS.DealDate,
		V.VehicleYear,
		MMG.[Make],
		MMG.[Model],
		ludbt.Segment DisplayBodyType,
		V.VehicleTrim,
		V.BaseColor,
		I.DaysToSale,
		VS.FrontEndGross,
		VS.BackEndGross,
		VS.SalePrice,
		I.UnitCost,
		I.TradeOrPurchase,
		I.MileageReceived,
		I.InitialVehicleLight,
		VS.SaleDescription,
		Case VS.SaleDescription When 'R' Then 1 Else 0 End as SaleDescriptionValue,
		I.StockNumber

FROM	[FLDW].dbo.Inventory I
JOIN	[FLDW].dbo.VehicleSale VS on I.InventoryID = VS.InventoryID
JOIN	[FLDW].dbo.Vehicle V on I.VehicleID = V.VehicleID AND I.BusinessUnitID = V.BusinessUnitID
JOIN	[FLDW].dbo.Segment ludbt on V.SegmentID = ludbt.SegmentID
JOIN	[FLDW].dbo.MakeModelGrouping mmg on V.makemodelgroupingid = MMG.makemodelgroupingid
JOIN	@Buckets as B on ((I.DaysToSale between B.daysLow and B.daysHigh) AND B.SaleDesc = VS.SaleDescription)
CROSS JOIN [FLDW].dbo.Period_D P

WHERE	I.BusinessUnitID = @DealerID
AND		I.InventoryType = 2
AND		VS.DealDate Between P.BeginDate and P.EndDate
AND		VS.saledescription = 'W'
AND		P.PeriodID = @PeriodID
GO
