
SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

IF (EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_DriveYourNumbersUnweighted]') AND type in (N'P', N'PC')))
	DROP PROCEDURE [dbo].[rc_DriveYourNumbersUnweighted]
GO

CREATE PROCEDURE dbo.rc_DriveYourNumbersUnweighted
	@DealerGroupID INT,
	@PeriodStartDate DATETIME,
	@PeriodEndDate DATETIME,
	@DealerHierarchy XML  
/*
Example DealerHierarchy
@DealerHierarchy = '<data>
<Child><Parent>Team Ramonat</Parent><BusinessUnitId>100898</BusinessUnitId></Child>
<Child><Parent>Team Ramonat</Parent><BusinessUnitId>100885</BusinessUnitId></Child>
</data>'
*/

AS
/******************************************************************************
**
**  Procedure: rc_DriveYourNumbersUnweighted
**  Description: 
**        
**  Input parameters:   see below
**
**  Output parameters:  none
**
**  Rows returned: RS1:	DealerGroup		VARCHAR(100)
			BusinessUnitId		INT
			BusinessUnit		VARCHAR(100)
			BeginDate		DATETIME
			EndDate			DATETIME
			OverallRank		INT
			OverallRankInGroup	INT
			Retail0To30Pct		FLOAT
			Retail0to30RankOverall	INT
			Retail0to30RankInGroup	INT
			Retail30To60Pct		FLOAT
			Retail30to60RankOverall	INT
			Retail30to60RankInGroup	INT
			GiveBackPct		FLOAT
			GiveBankRankOverall	INT
			GiveBankRankInGroup	INT
			TotalPct		FLOAT
			
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  2008.06.08  ffoiii    Procedure created.
**  2009.12.10	svu	  Modified GiveBackPCT & TotalPct - Added -1 * ABS(
**  2010.01.07	svu	  Modified Main Select statement to match another report's logic
*******************************************************************************/

--Standard Environment Settings
SET NOCOUNT ON
SET ANSI_WARNINGS ON

--temp table declaration
CREATE TABLE #Dealers (
	BusinessUnitID INT,
	BusinessUnit   VARCHAR(50),
	DealerGroup    VARCHAR(50)
)
CREATE TABLE #SalesSnapshot (
	BusinessUnitID         INT,
	SaleDescription        VARCHAR(1),
	InventoryReceivedDate  SMALLDATETIME,
	DealDate               SMALLDATETIME,
	TradeOrPurchase        INT,
	FrontEndGross          REAL
)


--variable declarations
DECLARE @BeginDate SMALLDATETIME
DECLARE @EndDate SMALLDATETIME

SELECT @BeginDate = CONVERT(DATETIME,CONVERT(VARCHAR(30),@PeriodStartDate,101))
	,@EndDate = CONVERT(DATETIME,CONVERT(VARCHAR(30),@PeriodEndDate,101))


--populate #Dealers with the dealer hierarchy
INSERT INTO #Dealers
SELECT	DS.BusinessUnitID,
		DS.BusinessUnit,
		DG.BusinessUnit
FROM	[IMT].dbo.BusinessUnit DS
JOIN	[IMT].dbo.BusinessUnitRelationship DR ON DR.BusinessUnitID = DS.BusinessUnitID
JOIN	[IMT].dbo.BusinessUnit DG ON DG.BusinessUnitID = DR.ParentID
JOIN	[IMT].dbo.DealerPreference DP ON DP.BusinessUnitID = DS.BusinessUnitID
JOIN	(
			SELECT	BusinessUnitID, SUM(Active) NumerOfActiveUpgrades
			FROM	[IMT].dbo.DealerUpgrade
			GROUP
			BY		BusinessUnitID
		) DU ON DU.BusinessUnitID = DS.BusinessUnitID
WHERE	DS.Active = 1
AND		DP.GoLiveDate IS NOT NULL
AND		DS.BusinessUnitTypeID = 4
AND		DU.NumerOfActiveUpgrades > 0
AND		DG.BusinessUnitID = @DealerGroupID

--@DealerHierarchy allows a user to pass in their own BusinessUnitId hierarchy in XML
IF (@DealerHierarchy is not null)
	BEGIN
		DELETE #dealers
		
		INSERT INTO #Dealers (BusinessUnitId, BusinessUnit, DealerGroup)
		SELECT Input.BusinessUnitId, bu.BusinessUnit, Input.ParentName
		FROM	(SELECT T.Hierarchy.query('Parent').value('.','varchar(100)') ParentName
				,T.Hierarchy.query('BUID').value('.','int') BusinessUnitId
			 FROM @DealerHierarchy.nodes('data/Rec') T(Hierarchy)
			) Input
			inner join imt.dbo.BusinessUnit bu on bu.BusinessUnitId = Input.BusinessUnitId
	END


--grab relevant sales data
INSERT INTO #SalesSnapshot
SELECT	I.BusinessUnitID,
		VS.SaleDescription,
		I.InventoryReceivedDate,
		VS.DealDate,
		I.TradeOrPurchase,
		VS.FrontEndGross
FROM	#Dealers DS
JOIN	[FLDW].dbo.Inventory I ON I.BusinessUnitID = DS.BusinessUnitID
JOIN	[FLDW].dbo.VehicleSale VS ON VS.InventoryID = I.InventoryID
WHERE	I.InventoryType = 2
AND		VS.DealDate BETWEEN @BeginDate AND @EndDate
AND		(VS.SaleDescription = 'R' OR (VS.SaleDescription = 'W' AND DATEDIFF(DD, I.InventoryReceivedDate, VS.DealDate) >= 30))



--group by BusinessUnitId, rank over relevant criteria
SELECT	
		DS.DealerGroup,
		DS.BusinessUnitID buid,
		DS.BusinessUnit businessunit,
		@BeginDate 'beginDate',
		@EndDate 'endDate',
		RANK() OVER(ORDER BY (((SSR.SweetSpotPctValue + NSR.RetailEfficiencyPctValue + GBR.GiveBackPctValue) * 100) / 1.55) DESC) AS OverallRank,
		RANK() OVER(PARTITION BY ds.DealerGroup ORDER BY (((SSR.SweetSpotPctValue + NSR.RetailEfficiencyPctValue + GBR.GiveBackPctValue) * 100) / 1.55) DESC) AS OverallRankInGroup,
		ISNULL(SSR.SweetSpotPctValue,0) Retail0to30Pct,   --Sweet Spot, Sold "R" < 30 Days
		SSR.SweetSpotRankID Retail0to30RankOverall,
		RANK () OVER (PARTITION BY ds.DealerGroup ORDER BY SSR.SweetSpotPctValue DESC) Retail0to30RankInGroup,
		ISNULL(NSR.RetailEfficiencyPctValue,0) Retail30tp60Pct,  --Retail Efficiency, Sold "R" < 60 Days
		NSR.RetailEfficiencyRankID Retail30to60RankOverall,
		RANK () OVER (PARTITION BY ds.DealerGroup ORDER BY NSR.RetailEfficiencyPctValue DESC) Retail30to60RankInGroup,
		ISNULL(GBR.GiveBackPctValue,0) GiveBackPct,   -- Wholesale Giveback, in dollars
		GBR.GiveBackRankID GiveBackRankOverall,
		RANK () OVER (PARTITION BY ds.DealerGroup ORDER BY GBR.GiveBackPctValue DESC) GiveBackRankInGroup,
		ISNULL(SSR.SweetSpotPctValue,0)+ISNULL(NSR.RetailEfficiencyPctValue,0)+ISNULL(GBR.GiveBackPctValue,0) TotalPct
--		, GBR.WholesaleFEG
--		, GBR.RetailFEG
FROM	#Dealers DS

LEFT JOIN
	(
	SELECT	BusinessUnitID,	
		(SUM(CASE WHEN SaleDescription = 'R' AND DATEDIFF(DD, InventoryReceivedDate, DealDate) < 30 THEN 1 ELSE 0 END)/CAST(COUNT(*) AS REAL)) SweetSpotPctValue,
		RANK() OVER (ORDER BY (SUM(CASE WHEN SaleDescription = 'R' AND DATEDIFF(DD, InventoryReceivedDate, DealDate) < 30 THEN 1 ELSE 0 END)/CAST(COUNT(*) AS REAL)) DESC) SweetSpotRankID
	FROM	#SalesSnapshot
	GROUP BY	BusinessUnitID
	) SSR on DS.BusinessUnitID = SSR.BusinessUnitID  --Sweet Spot Rank

LEFT JOIN
	(
	SELECT	BusinessUnitID,
		(SUM(CASE WHEN SaleDescription = 'R' AND DATEDIFF(DD, InventoryReceivedDate, DealDate) < 60 THEN 1 ELSE 0 END)/CAST(COUNT(*) AS REAL)) RetailEfficiencyPctValue,  --Percent Sold Retail within 60 days (Retail Efficiency)
		RANK() OVER (ORDER BY(SUM(CASE WHEN SaleDescription = 'R' AND DATEDIFF(DD, InventoryReceivedDate, DealDate) < 60 THEN 1 ELSE 0 END)/CAST(COUNT(*) AS REAL)) DESC) RetailEfficiencyRankID
	FROM	#SalesSnapshot
	GROUP BY	BusinessUnitID
	) NSR on DS.BusinessUnitID = NSR.businessunitid	-- Retail Efficiency Rank

LEFT JOIN
	(
	SELECT	BusinessUnitID,	
		(SUM(CASE WHEN SaleDescription = 'W' THEN FrontEndGross ELSE 0 END)   /	ABS(SUM(CASE WHEN SaleDescription = 'R' THEN FrontEndGross ELSE 0 END))) GiveBackPctValue,
		RANK() OVER(ORDER BY(SUM(CASE WHEN SaleDescription = 'W' THEN FrontEndGross ELSE 0 END)	/ ABS(SUM(CASE WHEN SaleDescription = 'R' THEN FrontEndGross ELSE 0 END))) DESC) GiveBackRankID
--		, SUM(CASE WHEN SaleDescription = 'W' THEN FrontEndGross ELSE 0 END) WholesaleFEG
--		, SUM(CASE WHEN SaleDescription = 'R' THEN FrontEndGross ELSE 0 END) RetailFEG
	FROM	#SalesSnapshot
	GROUP BY	BusinessUnitID
	) GBR on DS.BusinessUnitID = GBR.BusinessUnitID	-- GiveBack / Aged Wholesale

WHERE	(SSR.SweetSpotPctValue + NSR.RetailEfficiencyPctValue + GBR.GiveBackPctValue) IS NOT null
ORDER BY DealerGroup, OverallRankInGroup

DROP TABLE #SalesSnapshot;
DROP TABLE #Dealers;

GO
GRANT EXECUTE ON [dbo].[rc_DriveYourNumbersUnweighted] TO [StoredProcedureUser]
