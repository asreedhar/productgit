

USE Reports
go

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_VipStockingReportUsedCar]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_VipStockingReportUsedCar] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_VipStockingReportUsedCar] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_VipStockingReportUsedCar]
	@DealerID INT,
	@NumberOfWeeks INT
AS

SET NOCOUNT ON;
--------------------------------------------------------

CREATE TABLE #t(
	[VehicleGroupingDescription] [varchar](50) NOT NULL,
	[GroupingID] [int] NOT NULL,
	[UnitsInStock] [int] NULL,
	[UnitsSold] [int] NULL,
	[AverageGrossProfit] [int] NOT NULL,
	[AverageBackEnd] [int] NOT NULL,
	[AverageFrontEnd] [int] NOT NULL,
	[AverageDays] [int] NOT NULL,
	[AverageMileage] [int] NOT NULL,
	[TotalRevenue] [int] NOT NULL,
	[TotalGrossMargin] [int] NOT NULL,
	[TotalInventoryDollars] [int] NOT NULL,
	[No_Sales] [int] NOT NULL,
	[TotalBackEnd] [int] NOT NULL,
	[TotalFrontEnd] [int] NOT NULL,
	[AverageInvAge] [int] NOT NULL
) 
  
DECLARE @Forecast INT, @Mileage INT, @Weeks INT  
  
SELECT @Mileage = 2147483647, @Forecast = CASE WHEN SIGN(@NumberOfWeeks) = 1 THEN 1 ELSE 0 END, @Weeks = ABS(@NumberOfWeeks)  


INSERT #t
Exec [IMT].[dbo].[GetSalesHistoryReport] @DealerID, @Weeks, @Forecast, @Mileage, 2

ALTER TABLE #t ADD SalesRate DECIMAL(20,5)

UPDATE t
SET SalesRate = a.SalesRate
FROM #t t
JOIN fldw.dbo.daysSupply_a A ON t.GroupingID = a.VehicleGroupingID AND 0 = A.[VehicleSegmentID] AND @DealerID = [BusinessUnitID]

SELECT * FROM #t

GO


