
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_DealerGroupSystemUsage]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_DealerGroupSystemUsage] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_DealerGroupSystemUsage] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_DealerGroupSystemUsage]
	@DealerGroupID INT,
	@MemberID INT
AS

SET NOCOUNT ON;

WITH TradeInAnalyzed_ThisWeek AS 
(
	SELECT	M.BusinessUnitID, M.Measure
	FROM	[EdgeScorecard].dbo.Measures M
	WHERE	M.MetricID = 144
	AND		M.PeriodID = 1
	AND		M.SetID = 2
),
TradeIns_ThisWeek AS
(
	SELECT	M.BusinessUnitID, M.Measure
	FROM	[EdgeScorecard].dbo.Measures M
	WHERE	M.MetricID = 145
	AND		M.PeriodID = 1
	AND		M.SetID = 2
),
A1 AS
(
	SELECT	T1.BusinessUnitID,
			Cast( Case When T2.Measure < 1 Then null Else T1.Measure / T2.Measure End as dec(5,2) ) 'Measure'
	FROM	TradeInAnalyzed_ThisWeek T1
	LEFT JOIN TradeIns_ThisWeek T2 ON T2.BusinessUnitID = T1.BusinessUnitID
),
TradeInAnalyzed_FoursWeeks AS 
(
	SELECT	M.BusinessUnitID, M.Measure
	FROM	[EdgeScorecard].dbo.Measures M
	WHERE	M.MetricID = 144
	AND		M.PeriodID = 2
	AND		M.SetID = 2
),
TradeIns_FoursWeeks AS
(
	SELECT	M.BusinessUnitID, M.Measure
	FROM	[EdgeScorecard].dbo.Measures M
	WHERE	M.MetricID = 145
	AND		M.PeriodID = 2
	AND		M.SetID = 2
),
A2 AS
(
	SELECT	T1.BusinessUnitID,
			Cast( Case When T2.Measure < 1 Then null Else T1.Measure / T2.Measure End as dec(5,2) ) 'Measure'
	FROM	TradeInAnalyzed_FoursWeeks T1
	LEFT JOIN TradeIns_FoursWeeks T2 ON T2.BusinessUnitID = T1.BusinessUnitID
),
TradeInAnalyzed_LastWeek AS 
(
	SELECT	M.BusinessUnitID, M.Measure
	FROM	[EdgeScorecard].dbo.Measures M
	WHERE	M.MetricID = 144
	AND		M.PeriodID = 9
	AND		M.SetID = 2
),
TradeIns_LastWeek AS
(
	SELECT	M.BusinessUnitID, M.Measure
	FROM	[EdgeScorecard].dbo.Measures M
	WHERE	M.MetricID = 145
	AND		M.PeriodID = 9
	AND		M.SetID = 2
),
A3 AS
(
	SELECT	T1.BusinessUnitID,
			Cast( Case When T2.Measure < 1 Then null Else T1.Measure / T2.Measure End as dec(5,2) ) 'Measure'
	FROM	TradeInAnalyzed_LastWeek T1
	LEFT JOIN TradeIns_LastWeek T2 ON T2.BusinessUnitID = T1.BusinessUnitID
),
A4 AS -- UnitsWithValidPlan_Current
(
	SELECT	M.BusinessUnitID, M.Measure
	FROM	[EdgeScorecard].dbo.Measures M
	WHERE	M.MetricID = 281
	AND		M.PeriodID = 7
	AND		M.SetID = 142
),
A5 AS -- UnitsInStock_Current
(
	SELECT	M.BusinessUnitID, M.Measure
	FROM	[EdgeScorecard].dbo.Measures M
	WHERE	M.MetricID = 279
	AND		M.PeriodID = 7
	AND		M.SetID = 142
),
A6 AS -- AgedUnitsWithValidPlan_Current
(
	SELECT	M.BusinessUnitID, M.Measure
	FROM	[EdgeScorecard].dbo.Measures M
	WHERE	M.MetricID = 284
	AND		M.PeriodID = 7
	AND		M.SetID = 142
),
A7 AS -- AgedUnitsInStock_Current
(
	SELECT	M.BusinessUnitID, M.Measure
	FROM	[EdgeScorecard].dbo.Measures M
	WHERE	M.MetricID = 282
	AND		M.PeriodID = 7
	AND		M.SetID = 142
),
A8 AS -- WatchListUnitsWithValidPlan_Current
(
	SELECT	M.BusinessUnitID, M.Measure
	FROM	[EdgeScorecard].dbo.Measures M
	WHERE	M.MetricID = 290
	AND		M.PeriodID = 7
	AND		M.SetID = 142
),
A9 AS -- WatchListUnitsInStock_Current
(
	SELECT	M.BusinessUnitID, M.Measure
	FROM	[EdgeScorecard].dbo.Measures M
	WHERE	M.MetricID = 288
	AND		M.PeriodID = 7
	AND		M.SetID = 142
),
A10 AS -- PriceUpToDate_Current
(
	SELECT	M.BusinessUnitID, M.Measure
	FROM	[EdgeScorecard].dbo.Measures M
	WHERE	M.MetricID = 244
	AND		M.PeriodID = 7
	AND		M.SetID = 125
),
A11 AS -- PriceOutOfDate_Current
(
	SELECT	M.BusinessUnitID, M.Measure
	FROM	[EdgeScorecard].dbo.Measures M
	WHERE	M.MetricID = 245
	AND		M.PeriodID = 7
	AND		M.SetID = 125
)
, A12 AS -- AgeBucketThreshold_Current
(
	SELECT	M.BusinessUnitID, M.Measure
	FROM	[EdgeScorecard].dbo.Measures M
	WHERE	M.MetricID = 246
	AND		M.PeriodID = 7
	AND		M.SetID = 125
)
SELECT	DS.BusinessUnitID,
		DS.BusinessUnitShortName BusinessUnitName,
		a1.Measure TradesAnalyzedThisWeek,
		a2.Measure TradesAnalyzedFourWeeks,
		a3.Measure 'TradesAnalyzedLastWeek',
		ISNULL( Cast( a4.Measure as int ), 0 ) metric281,
		ISNULL( Cast( a5.Measure as int ), 0 ) metric279,
		Case When a5.Measure < 1 Then null Else a4.Measure / a5.Measure End 'allInventory',
		ISNULL( Cast( a6.Measure as int ) , 0 ) metric284,
		ISNULL( Cast( a7.Measure as int ) , 0 ) metric282,
		Case When a7.Measure < 1 Then null Else a6.Measure / a7.Measure End 'agingInventory',
		ISNULL( Cast( a8.Measure as int ) , 0 ) metric290,
		ISNULL( Cast( a9.Measure as int ) , 0 ) metric288,
		Case When a9.Measure < 1 Then null Else a8.Measure / a9.Measure End  'watchList',
		ISNULL( Cast( a10.Measure as int ) , 0 ) metric244,
		ISNULL( Cast( a11.Measure as int ) , 0 ) metric245,
		Case When a10.Measure + a11.Measure < 1 Then null Else a10.Measure / ( a10.Measure + a11.Measure ) End 'agingRetailUnits',
		ISNULL( Cast( a12.Measure as int ) , 0 ) metric246,
		ISNULL( Cast (
			(a5.Measure + a7.Measure + a9.Measure + a11.Measure  )
			/ 
			(Case When ( a4.Measure + a6.Measure + a8.Measure + a10.Measure + a12.Measure ) < 1 Then 1 Else ( a4.Measure + a6.Measure + a8.Measure + a10.Measure + a12.Measure ) End ) as dec(9,2) ),
			0 ) WeightingAip,
		DG.BusinessUnitID ParentID,
		DG.BusinessUnit Parent,
		DP.GoLiveDate

FROM	[IMT].dbo.BusinessUnit DS
JOIN	[IMT].dbo.BusinessUnitRelationship DR ON DR.BusinessUnitID = DS.BusinessUnitID
JOIN	[IMT].dbo.BusinessUnit DG ON DG.BusinessUnitID = DR.ParentID
JOIN	[IMT].dbo.DealerPreference DP ON DP.BusinessUnitID = DS.BusinessUnitID
JOIN	dbo.MemberAccess(@DealerGroupID, @MemberID) MA ON MA.BusinessUnitID = DS.BusinessUnitID
JOIN	(
			SELECT	BusinessUnitID, SUM(Active) NumerOfActiveUpgrades
			FROM	[IMT].dbo.DealerUpgrade
			GROUP
			BY		BusinessUnitID
		) DU ON DU.BusinessUnitID = DS.BusinessUnitID

LEFT JOIN	A1 ON DS.BusinessUnitID = A1.BusinessUnitID
LEFT JOIN	A2 ON DS.BusinessUnitID = A2.BusinessUnitID
LEFT JOIN	A3 ON DS.BusinessUnitID = A3.BusinessUnitID
LEFT JOIN	A4 ON DS.BusinessUnitID = A4.BusinessUnitID
LEFT JOIN	A5 ON DS.BusinessUnitID = A5.BusinessUnitID
LEFT JOIN	A6 ON DS.BusinessUnitID = A6.BusinessUnitID
LEFT JOIN	A7 ON DS.BusinessUnitID = A7.BusinessUnitID
LEFT JOIN	A8 ON DS.BusinessUnitID = A8.BusinessUnitID
LEFT JOIN	A9 ON DS.BusinessUnitID = A9.BusinessUnitID
LEFT JOIN	A10 ON DS.BusinessUnitID = A10.BusinessUnitID
LEFT JOIN	A11 ON DS.BusinessUnitID = A11.BusinessUnitID
LEFT JOIN	A12 ON DS.BusinessUnitID = A12.BusinessUnitID

WHERE	DS.Active = 1
AND		DP.GoLiveDate IS NOT NULL
AND		DS.BusinessUnitTypeID = 4
AND		DU.NumerOfActiveUpgrades > 0
AND		DG.BusinessUnitID = @DealerGroupID
GO
