IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_PerformanceByAuctionOrWholesalerDrillThrough_Inventory]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_PerformanceByAuctionOrWholesalerDrillThrough_Inventory] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_PerformanceByAuctionOrWholesalerDrillThrough_Inventory] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_PerformanceByAuctionOrWholesalerDrillThrough_Inventory]
	@DealerID		INT,
	@InventoryTypeID    INT,
	@WholesalerID		INT,
	@UnitCostBucket		VARCHAR(7),
	@MileageBucket		VARCHAR(7),
	@VehicleLight		VARCHAR(7)
AS

SET NOCOUNT ON

--Build Unit Cost Bucket
DECLARE @UnitCostBuckets TABLE(
	BucketName varchar(7)
	, LowValue int
	, HighValue int
)

INSERT INTO @UnitCostBuckets
SELECT 'All', -2147483648, 2147483647
UNION ALL
SELECT '1', -2147483648, 10000
UNION ALL
SELECT '2', 10000, 19999
UNION ALL
SELECT '3', 20000, 29999
UNION ALL
SELECT '4', 30000, 39999
UNION ALL
SELECT '5', 40000, 49999
UNION ALL
SELECT '6', 50000, 59999
UNION ALL
SELECT '7', 60000, 69999
UNION ALL
SELECT '8', 70000, 79999
UNION ALL
SELECT '9', 80000, 89999
UNION ALL
SELECT '10', 90000, 99999
UNION ALL
SELECT '11', 100000, 2147483647

IF @UnitCostBucket = 'UNKNOWN'
SET @UnitCostBucket = NULL

--Build Mileage Bucket
DECLARE @MileageBuckets TABLE(
	BucketName varchar(7)
	, LowValue int
	, HighValue int
)

INSERT INTO @MileageBuckets
SELECT 'All', -2147483648, 2147483647
UNION ALL
SELECT '1', -2147483648, 25000
UNION ALL
SELECT '2', 25000, 49000
UNION ALL
SELECT '3', 50000, 74000
UNION ALL
SELECT '4', 75000, 99000
UNION ALL
SELECT '5', 100000, 124000
UNION ALL
SELECT '6', 125000, 149000
UNION ALL
SELECT '7', 150000, 2147483647

IF @MileageBucket = 'UNKNOWN'
SET @MileageBucket = NULL

--Process VehicleLight Parameter
DECLARE @VehicleLights TABLE(
	Label varchar(7)
	, LightID int
)

INSERT INTO @VehicleLights
SELECT 'All', 1
UNION ALL
SELECT 'All', 2
UNION ALL
SELECT 'All', 3
UNION ALL
SELECT '1', 1
UNION ALL
SELECT '2', 2
UNION ALL
SELECT '3', 3
UNION ALL
SELECT 'Not Set', NULL

SELECT 
	I.AgeInDays AS InventoryAge
	, V.VehicleYear AS VehicleYear
	, CAST(V.VehicleYear AS VARCHAR(4)) + ' ' + V.Description + ' - ' + V.Trim AS VehicleDescription
	, Color.Color
	, I.StockNumber
	, I.UnitCost
	, I.MileageReceived AS Mileage
	, I.InitialVehicleLight
	, I.InventoryReceivedDate AS ReceivedDate
FROM [FLDW].dbo.Inventory I WITH (NOLOCK)
JOIN [FLDW].dbo.InventorySales_F ISales
	ON I.InventoryID = ISales.InventoryID
JOIN [FLDW].dbo.InventorySource_D ISource
	ON ISales.InventorySourceID = ISource.InventorySourceID
JOIN [FLDW].dbo.VehicleHierarchy_D V
	ON ISales.VehicleHierarchyID = V.VehicleHierarchyID
JOIN [FLDW].dbo.Color_D Color
	ON ISales.ColorID = Color.ColorID
JOIN @UnitCostBuckets UCB
	ON ((@UnitCostBucket IS NOT NULL AND I.UnitCost BETWEEN UCB.LowValue AND UCB.HighValue)
		OR (@UnitCostBucket IS NULL AND I.UnitCost IS NULL) )
JOIN @MileageBuckets MB
	ON ((@MileageBucket IS NOT NULL AND I.MileageReceived BETWEEN MB.LowValue AND MB.HighValue)
		OR (@MileageBucket IS NULL AND I.MileageReceived IS NULL) )
JOIN @VehicleLights Lights
	ON I.InitialVehicleLight = Lights.LightID
WHERE I.InventoryActive = 1
AND ISource.InventorySourceID = @WholesalerID
AND I.InventoryType = @InventoryTypeID
AND I.BusinessUnitID = @DealerID
AND UCB.BucketName = @UnitCostBucket
AND MB.BucketName = @MileageBucket
AND Lights.Label = @VehicleLight
ORDER BY 
	I.AgeInDays DESC
	, V.Description + ' - ' + V.Trim
