IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.MonthParameters#Fetch') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE dbo.MonthParameters#Fetch AS SELECT 1')
GO
 
ALTER PROCEDURE [dbo].[MonthParameters#Fetch]

AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.MonthParameters#Fetch.sql,v 1.4 2010/02/10 21:54:15 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns a fudge mappging for displaying the month name and input for a Year-Month format of yyyymm
 * 
 * Parameters
 * ----------
 * 
 * Exceptions
 * ----------
 *
 * History
 * ----------
 * 
 * BYF	2009-09-29
 * 						
 * -------------------------------------------------------------------- */

SET NOCOUNT ON;

------------------------------------------------------------------------------------------------
-- Validate Parameters
------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------
-- Declare Result Set
------------------------------------------------------------------------------------------------

DECLARE @Results TABLE (
	YearMonthValue 	INT, --yyyymm format
	YearMonthLabel VARCHAR(25) --ex: Sept. 28, 2009
)

------------------------------------------------------------------------------------------------
-- Populate Result Set
------------------------------------------------------------------------------------------------

DECLARE @Now SMALLDATETIME, @StartDate SMALLDATETIME, @CurrentDate SMALLDATETIME;

SET @Now = getdate();
SET @StartDate = DATEADD(MONTH, -5, @Now);
SET @CurrentDate = @Now;

--loop in reverse order to avoid sorting afterwards
WHILE (@CurrentDate >= @StartDate)
BEGIN

	INSERT INTO @Results(YearMonthValue, YearMonthLabel)
--yes, this will break after the year 9999
	SELECT 	CAST(
				CAST(DATEPART(YEAR, @CurrentDate) AS VARCHAR(4)) + 
				'' + 
				CASE WHEN DATEPART(MONTH, @CurrentDate) < 10 
					 THEN '0' + CAST(DATEPART(MONTH, @CurrentDate) AS VARCHAR(1))
					 ELSE CAST(DATEPART(MONTH, @CurrentDate) AS VARCHAR(2))
				END 
		   	AS INT ) 
		   AS Value,
		   CAST(DATEPART(YEAR, @CurrentDate) AS VARCHAR) + ' ' + DATENAME(MONTH, @CurrentDate) AS Label

	   
SET @CurrentDate = DATEADD(MONTH, -1, @CurrentDate);

END

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT 
	YearMonthValue,
	YearMonthLabel
FROM @Results

RETURN 0

GO