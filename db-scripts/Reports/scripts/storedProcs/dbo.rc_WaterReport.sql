
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_WaterReport]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_WaterReport] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_WaterReport] TO [StoredProcedureUser]
GO

---History--------------------------------------------------------------------------------------
--
--  JMW 01/16/2012  BUGZID: 18671 - Changed where condition to count everything besides 
--					Wholesale and Sold as Retail
--
------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[rc_WaterReport]
	@DealerID INT,
	@InventoryBucketID INT,
    @StrategyID		TINYINT = NULL -- SEE FLDW.[dbo].[GetStockingReportCube] for documentation
	
AS

	IF (@StrategyID = 0) SELECT @StrategyID = NULL	
		
	--MUST MATCH IMT.[dbo].[GetAgingInventoryPlan] = IMT.[dbo].[GetAgingInventoryPlanDetail] + IMT.[dbo].Segment
DECLARE @Results TABLE
    (
      RangeID TINYINT ,
      InventoryID INT ,
      StockNumber VARCHAR(15) ,
      BusinessUnitID INT ,
      VehicleID INT ,
      InventoryActive TINYINT ,
      InventoryReceivedDate SMALLDATETIME ,
      DeleteDt SMALLDATETIME ,
      ModifiedDT SMALLDATETIME ,
      DMSReferenceDt SMALLDATETIME ,
      UnitCost DECIMAL ,
      AcquisitionPrice DECIMAL ,
      Pack DECIMAL ,
      MileageReceived INT ,
      TradeOrPurchase TINYINT ,
      ListPrice DECIMAL ,
      InitialVehicleLight INT ,
      Level4Analysis VARCHAR(150) ,
      InventoryType TINYINT ,
      ReconditionCost DECIMAL ,
      UsedSellingPrice DECIMAL ,
      Certified TINYINT ,
      VehicleLocation VARCHAR(50) ,
      CurrentVehicleLight TINYINT ,
      FLRecFollowed TINYINT ,
      Audit_ID INT ,
      InventoryStatusCD SMALLINT ,
      ListPriceLock TINYINT ,
      SpecialFinance TINYINT ,
      Vin VARCHAR(17) ,
      VehicleYear INT ,
      Make VARCHAR(20) ,
      Model VARCHAR(101) ,
      VehicleTrim VARCHAR(50) ,
      BodyTypeID INT ,
      BodyType VARCHAR(50) ,
      BaseColor VARCHAR(50) ,
      VehicleTransmission VARCHAR(20) ,
      GroupingDescriptionID INT ,
      GroupingDescriptionStr VARCHAR(80) ,
      CurrentValue INT ,
      BookoutID INT ,
      ThirdPartyCategoryID INT ,
      CurrentDT DATETIME ,
      BookoutValueTypeID TINYINT ,
      IsValid TINYINT ,
      BookoutStatusID TINYINT ,
      ThirdPartyName VARCHAR(30) ,
      ThirdPartyCategory VARCHAR(30) ,
      CurrentValue_2nd INT ,
      BookoutID_2nd INT ,
      ThirdPartyCategoryID_2nd INT ,
      CurrentDT_2nd DATETIME ,
      BookoutValueTypeID_2nd TINYINT ,
      IsValid_2nd TINYINT ,
      BookoutStatusID_2nd TINYINT ,
      ThirdPartyName_2nd VARCHAR(30) ,
      ThirdPartyCategory_2nd VARCHAR(30) ,
      Overstocked INT ,
      AgeInDays INT ,
      SegmentID INT ,
      Segment VARCHAR(50)
    )

INSERT  INTO @Results
        EXEC [IMT].[dbo].[GetAgingInventoryPlan] @BusinessUnitID = @DealerID,
            @InventoryBucketID = @InventoryBucketID

SELECT R.* 
FROM @Results R 
LEFT JOIN FLDW.dbo.GetInventoryManagementPlanStrategy(@DealerID,GETDATE()) CS ON R.InventoryID = CS.InventoryID
WHERE @StrategyID IS NULL -- ALL
	  OR ( @StrategyID = 1 AND  ( CS.AIP_EventCategoryID IS NULL OR CS.AIP_EventCategoryID IN (1,4) ) ) -- NULLs, Retail, and Other are all considered Retail (BUGZID: 18671)
	  OR ( @StrategyID = CS.AIP_EventCategoryID ) -- Wholesale, Sold
	  
GO
