
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_RedYellowGreenLightInventoryA]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_RedYellowGreenLightInventoryA] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_RedYellowGreenLightInventoryA] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_RedYellowGreenLightInventoryA]
	@DealerID INT
AS

SET NOCOUNT ON;

SELECT	B.BusinessUnit BusinessUnitName,
		SUM(CASE WHEN I.CurrentVehicleLight = 1 THEN 1 ELSE 0 END) Red,
		SUM(CASE WHEN I.CurrentVehicleLight = 2 THEN 1 ELSE 0 END) Ylw,
		SUM(CASE WHEN I.CurrentVehicleLight = 3 THEN 1 ELSE 0 END) Grn,
		SUM(CASE WHEN I.CurrentVehicleLight = 0 THEN 1 ELSE 0 END) NaC,
		SUM(CASE WHEN I.CurrentVehicleLight = 1 and datediff(dd, I.InventoryReceivedDate, GETDATE()) > 15 THEN 1 ELSE 0 END) RedOver15,
		SUM(CASE WHEN I.CurrentVehicleLight = 2 and datediff(dd, I.InventoryReceivedDate, GETDATE()) > 30 THEN 1 ELSE 0 END) YlwOver30,
		SUM(CASE WHEN I.CurrentVehicleLight = 3 and datediff(dd, I.InventoryReceivedDate, GETDATE()) > 60 THEN 1 ELSE 0 END) GrnOver60,
		SUM(CASE WHEN I.CurrentVehicleLight = 0 and datediff(dd, I.InventoryReceivedDate, GETDATE()) > 15 THEN 1 ELSE 0 END) NacOver15,
		COUNT( I.InventoryID ) TotalCount,
		TP.Description + COALESCE(' - ' + TPC.Category, '') Book

FROM	[FLDW].dbo.BusinessUnit B
JOIN	[FLDW].dbo.DealerPreference P ON P.BusinessUnitID = B.BusinessUnitID
JOIN	[FLDW].dbo.ThirdParties TP ON TP.ThirdPartyID = P.GuideBookID
LEFT JOIN FLDW.dbo.ThirdPartyCategories TPC ON TPC.ThirdPartyID = TP.ThirdPartyID AND TPC.ThirdPartyCategoryID = P.BookOutPreferenceId
JOIN	[FLDW].dbo.InventoryActive I ON I.BusinessUnitID = B.BusinessUnitID
LEFT JOIN	[FLDW].dbo.InventoryBookout_F IB on I.InventoryID = IB.InventoryID AND IB.ThirdPartyCategoryID = P.BookOutPreferenceID

WHERE	B.BusinessUnitID = @DealerID
AND		I.InventoryType = 2
AND		I.InventoryActive = 1

GROUP
BY	B.BusinessUnit, TP.Description + COALESCE(' - ' + TPC.Category, '')


GO