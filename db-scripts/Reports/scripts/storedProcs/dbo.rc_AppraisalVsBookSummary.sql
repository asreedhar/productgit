
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_AppraisalVsBookSummary]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_AppraisalVsBookSummary] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_AppraisalVsBookSummary] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_AppraisalVsBookSummary]
	@PeriodID INT,
	@DealerID INT
AS

SET NOCOUNT ON;

WITH SummaryData AS
(
	SELECT	ISNULL((MAX(m.FirstName) + ' ' + m.Lastname), 'ALL') AS 'Member',
			Cast(
				sum( CASE WHEN alv.AppraisalValueID IS NOT NULL THEN ALV.[Value] ELSE 0 end ) 
				/
				cast( ( Case When sum( CASE WHEN alv.AppraisalValueID IS NOT NULL THEN 1 ELSE 0 end ) = 0 Then -1 Else sum( CASE WHEN alv.AppraisalValueID IS NOT NULL THEN 1 ELSE 0 end ) End ) AS real) as Int
			) AS 'ValueALV',  
			Cast( 
				sum( CASE WHEN alv.AppraisalValueID IS NOT NULL THEN BB.[Value] ELSE 0 end ) 
				/
				cast( ( Case When sum( CASE WHEN alv.AppraisalValueID IS NOT NULL THEN 1 ELSE 0 end ) = 0 Then -1 Else sum( CASE WHEN alv.AppraisalValueID IS NOT NULL THEN 1 ELSE 0 end) End ) AS real) as Int
			) 'ValueB',
			Cast( AVG( ALV.[Value] - BB.[Value] ) as Int ) 'AppBookDiff',
			sum(case WHEN a.datecreated IS NOT NULL THEN 1 ELSE 0 end) AS 'VehiclesAppraised',
			sum( CASE WHEN ALV.Value IS NOT NULL THEN 1 ELSE 0 end ) as 'AppraisalRecorded'
	FROM	[IMT].dbo.Appraisals A
	LEFT JOIN	[FLDW].dbo.Appraisal_F ALV ON ALV.AppraisalID = A.AppraisalID AND ALV.BusinessUnitID = A.BusinessUnitID
	JOIN	[IMT].dbo.DealerPreference P ON P.BusinessUnitID = A.BusinessUnitID
	LEFT JOIN	[FLDW].dbo.AppraisalBookout_F BB ON (
					BB.AppraisalID = A.AppraisalID
				AND	BB.BusinessUnitID = A.BusinessUnitID
				AND	BB.BookoutValueTypeID = 2
				AND	BB.ThirdPartyCategoryID = P.BookoutPreferenceID
			)
	JOIN	[IMT].dbo.Member M ON M.MemberID = A.MemberID
	CROSS JOIN	[FLDW].dbo.Period_D T
	WHERE	A.BusinessUnitID = @DealerID
	AND		M.MemberType != 1
	AND		T.PeriodID = @PeriodID
	AND		A.DateCreated BETWEEN T.BeginDate and T.EndDate
	AND		A.AppraisalTypeID = 1
	GROUP
	BY		M.Lastname, M.FirstName
	UNION ALL
	SELECT	'ALL' as 'Member',
			Cast(
				sum( CASE WHEN alv.AppraisalValueID IS NOT NULL THEN ALV.[Value] ELSE 0 end ) 
				/
				cast( ( Case When sum( CASE WHEN alv.AppraisalValueID IS NOT NULL THEN 1 ELSE 0 end ) = 0 Then -1 Else sum( CASE WHEN alv.AppraisalValueID IS NOT NULL THEN 1 ELSE 0 end ) End ) AS real) as Int
			) AS 'ValueALV',  
			Cast( 
				sum( CASE WHEN alv.AppraisalValueID IS NOT NULL THEN BB.[Value] ELSE 0 end ) 
				/
				cast( ( Case When sum( CASE WHEN alv.AppraisalValueID IS NOT NULL THEN 1 ELSE 0 end ) = 0 Then -1 Else sum( CASE WHEN alv.AppraisalValueID IS NOT NULL THEN 1 ELSE 0 end) End ) AS real) as Int
			) 'ValueB',
			Cast( AVG( ALV.[Value] - BB.[Value] ) as Int ) 'AppBookDiff',
			sum(case WHEN a.datecreated IS NOT NULL THEN 1 ELSE 0 end) AS 'VehiclesAppraised',
			sum( CASE WHEN ALV.Value IS NOT NULL THEN 1 ELSE 0 end ) as 'AppraisalRecorded'
	FROM	[IMT].dbo.Appraisals A
	LEFT JOIN	[FLDW].dbo.Appraisal_F ALV ON ALV.AppraisalID = A.AppraisalID AND ALV.BusinessUnitID = A.BusinessUnitID
	JOIN	[IMT].dbo.DealerPreference P ON P.BusinessUnitID = A.BusinessUnitID
	LEFT JOIN	[FLDW].dbo.AppraisalBookout_F BB ON (
					BB.AppraisalID = A.AppraisalID
				AND	BB.BusinessUnitID = A.BusinessUnitID
				AND	BB.BookoutValueTypeID = 2
				AND	BB.ThirdPartyCategoryID = P.BookoutPreferenceID
			)
	JOIN	[IMT].dbo.Member M ON M.MemberID = A.MemberID
	CROSS JOIN	[FLDW].dbo.Period_D T
	WHERE	A.BusinessUnitID = @DealerID
	AND		M.MemberType != 1
	AND		T.PeriodID = @PeriodID
	AND		A.DateCreated BETWEEN T.BeginDate and T.EndDate
	AND		A.AppraisalTypeID = 1
)
SELECT	D.*,
		TP.Description 'Book',
		TPC.Category 'BookPref',
		T.BeginDate 'FromDate',
		Case
			When DATEDIFF( dd , T.EndDate , getdate() ) < 0 Then
				Cast( DATEPART( mm , getdate() ) as varchar(2) ) + '/' +  Cast( DATEPART( dd , getdate() ) as varchar(2) )+ '/' +  Cast( DATEPART( yy , getdate() ) as varchar(4) )
			Else
				T.EndDate
		End 'ToDate'
FROM	[IMT].dbo.DealerPreference P
JOIN	[IMT].dbo.ThirdParties TP ON TP.ThirdPartyID = P.GuideBookID
JOIN	[IMT].dbo.ThirdPartyCategories TPC ON TPC.ThirdPartyCategoryID = P.BookoutPreferenceID
CROSS JOIN	SummaryData D
CROSS JOIN	[FLDW].dbo.Period_D T
WHERE	P.BusinessUnitID = @DealerID
AND		T.PeriodID = @PeriodID
GO
