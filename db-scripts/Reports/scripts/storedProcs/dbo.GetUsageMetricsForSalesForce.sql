IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.GetUsageMetricsForSalesForce') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE dbo.GetUsageMetricsForSalesForce AS SELECT 1')
GO

GRANT EXECUTE, VIEW DEFINITION on dbo.GetUsageMetricsForSalesForce to [CustomerOperations]

GO
 
ALTER PROCEDURE [dbo].[GetUsageMetricsForSalesForce] (@DaysBack SMALLINT=NULL)
AS
/* 
******* I promise, with complete understanding, that I will not
******* run this query before 1pm on any given day  Otherwise, 
******* Engineering, Bill and Frank in particular, will break 
******* keyboards and yell a whole lot  -- Simon or Vince.
*/

/* MAK 12/12/2008  - Proc originally written by SW.
	This procedure gathers metrics regarding PING tool usage for the 
	selected period.  The procedure returns, by Business Unit, the # and % of
	Number Of Retail InventoryItems 
	Number of Items that were 'PINGed'
	Number of Items that were repriced.
	Number of Items with MysteryShop  
	Number of Items with JDPower process
	Number of Items with TMV process */
/* MAK 01/07/2009 - 
	Per LG added Total Inventory  and Price Confirm Count 
	It should be noted that the startdate has time stripped out in order  for
there to be consistency with Laura G data.*/
/* MAK 02/11/2009 - 
	Added all reprice events and DMS change.*/
/* MAK 03/26/2009 - 
	Added all reprice events by type.*/
/* MAK	04/08/2009 -
	Added metrics as further defined in FB4965.
	WB turned on
	MAD turned on,
	Full File EDT defined as Pricing/Description.
	Descriptions Saved
	ClosingNotes Saved
	Appraisal Stats.

*/
/*	MAK 04/16/2009 -Changed From Yes\No to True\False.
	Added additional descriptive columns.

	MAK 04/25/2009  Updating table, reading from view
*/
 --DECLARE @DaysBack INT
 --SET	@DaysBack=14

EXEC [dbo].[UpdateUsageMetricsForSalesForce]0,@DaysBack
SELECT * FROM  SystemUsageMetricsForSalesForceView 


GO
