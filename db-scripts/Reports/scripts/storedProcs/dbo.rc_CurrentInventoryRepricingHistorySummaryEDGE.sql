
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_CurrentInventoryRepricingHistorySummaryEDGE]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_CurrentInventoryRepricingHistorySummaryEDGE] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_CurrentInventoryRepricingHistorySummaryEDGE] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_CurrentInventoryRepricingHistorySummaryEDGE]
	@DealerID INT
AS

SET NOCOUNT ON;

DECLARE @Buckets TABLE (
	counter int,
	lowAge int,
	highAge int
)

INSERT INTO @Buckets (counter, lowAge, highAge) VALUES (0 , 21 , 29)
INSERT INTO @Buckets (counter, lowAge, highAge) VALUES (1 , 30 , 39)
INSERT INTO @Buckets (counter, lowAge, highAge) VALUES (2 , 40 , 49)
INSERT INTO @Buckets (counter, lowAge, highAge) VALUES (3 , 50 , 59)
INSERT INTO @Buckets (counter, lowAge, highAge) VALUES (4 , 60 , 999)

CREATE TABLE #InventorySnapshot (
	BusinessUnitID INT,
	InventoryID INT,
	VehicleID INT,
	InventoryReceivedDate SMALLDATETIME
)

INSERT INTO #InventorySnapshot (BusinessUnitID, InventoryID, VehicleID, InventoryReceivedDate)
SELECT	I.BusinessUnitID, I.InventoryID, I.VehicleID, I.InventoryReceivedDate
FROM	[FLDW].dbo.InventoryActive I
WHERE	I.InventoryActive = 1
AND		I.InventoryType = 2
AND		I.BusinessUnitID = @DealerID

CREATE TABLE #Inventory_ListPriceHistory (
	InventoryID INT,
	ListPrice dec(8,2),
	DMSReferenceDT SMALLDATETIME
)

INSERT INTO #Inventory_ListPriceHistory (InventoryID, ListPrice, DMSReferenceDT)
-- all the original prices
SELECT	I.InventoryID, E.Notes2, E.Created
FROM	[IMT].dbo.AIP_Event E
		JOIN	#InventorySnapshot I ON E.InventoryID = I.InventoryID
WHERE	E.AIP_EventTypeID = 5
		AND		E.Notes2 != '0.0'
		AND     Cast(Cast(E.Notes2 as float) as int) != E.Value
UNION ALL
-- the last list price (add 1 minute to the created field so it does not get lost in the group by)
SELECT	E1.InventoryID, Value Notes2, DATEADD(s,60,Created)
FROM	[IMT].dbo.AIP_Event E1
		JOIN	(
			SELECT	E.InventoryID, MAX(AIP_EventID) AIP_EventID
			FROM	[IMT].dbo.AIP_Event E
					JOIN	#InventorySnapshot I ON E.InventoryID = I.InventoryID
			WHERE	E.AIP_EventTypeID = 5
					AND		E.Value != '0.0'
					AND     Cast(Cast(E.Notes2 as float) as int) != E.Value
			GROUP
			BY		E.InventoryID
		) E2 ON E1.InventoryID = E2.InventoryID AND E1.AIP_EventID = E2.AIP_EventID	

SELECT	b.counter,
		b.lowAge,
		b.highAge,
		AVG( lph.[Difference] ) as 'AvgRepriceValue',
		COUNT(DISTINCT(lph.InventoryID)) as 'VehiclesRepriced',
		i.Vehicles as 'Vehicles'

FROM	@Buckets b

LEFT JOIN
	(
		SELECT	i.InventoryReceivedDate,
				DATEDIFF(DD, i.InventoryReceivedDate, GETDATE()) as 'Age',
				( LPH2.ListPrice - LPH1.ListPrice ) as 'Difference',
				i.InventoryID

		FROM	#InventorySnapshot I
		JOIN	(
					SELECT	LPH1.InventoryID, LPH1.ListPrice, LPH1.DMSReferenceDT, LPH2.NextDMSReferenceDT
					FROM 	#Inventory_ListPriceHistory LPH1
					LEFT JOIN
						(
							SELECT	L1.InventoryID, L1.DMSReferenceDT, MIN(L2.DMSReferenceDT) NextDMSReferenceDT
							FROM	#Inventory_ListPriceHistory L1
							JOIN	#Inventory_ListPriceHistory L2 ON L1.InventoryID = L2.InventoryID
							WHERE	L1.ListPrice > 0
							AND		L2.ListPrice > 0
							AND		L1.DMSReferenceDT < L2.DMSReferenceDT
							GROUP
							BY		L1.InventoryID, L1.DMSReferenceDT
						) LPH2 ON LPH2.InventoryID = LPH1.InventoryID AND LPH2.DMSReferenceDT = LPH1.DMSReferenceDT
					WHERE	LPH1.ListPrice > 0
				) LPH1 ON LPH1.InventoryID = I.InventoryID
		JOIN	#Inventory_ListPriceHistory LPH2 ON LPH1.InventoryID = LPH2.InventoryID AND LPH1.NextDMSReferenceDT = LPH2.DMSReferenceDT
		WHERE	LPH2.ListPrice > 0
		AND		LPH1.ListPrice > 0
	) lph ON lph.Age Between b.lowAge And b.highAge

LEFT JOIN
	(
		SELECT	bc.counter,
				bc.lowAge,
				bc.highAge,
				COUNT(ic.InventoryID) as 'Vehicles'

		FROM	@Buckets bc
		
		LEFT JOIN
		(
			SELECT	inv.InventoryID,
					DATEDIFF(dd, inv.InventoryReceivedDate, GETDATE()) as 'Age'
			FROM	#InventorySnapshot inv
		) ic ON ic.Age Between bc.lowAge And bc.highAge
		
		GROUP BY bc.Counter, bc.lowAge, bc.highAge
		
	) i ON b.counter = i.counter

GROUP BY b.counter, b.lowAge, b.highAge, i.vehicles

DROP TABLE #InventorySnapshot

DROP TABLE #Inventory_ListPriceHistory

GO