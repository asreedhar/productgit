
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_FullInternetAdvertising]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_FullInternetAdvertising] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_FullInternetAdvertising] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_FullInternetAdvertising]
	@DealerID INT,
	@PeriodID INT
AS
/**************************************************************************************
**						 
**	History
**
**	MAK	10/39/2009 Updated procedure to eliminate Cartesian join.
**						 
***************************************************************************************/

SET NOCOUNT ON

SELECT 	I.BusinessUnitID,
		B.BusinessUnit,
		I.StockNumber,
		CAST(V.VehicleYear as varChar(4)) + ' ' + MMG.Make + ' ' + MMG.Model + ' ' + CASE VehicleTrim WHEN 'UNKNOWN' THEN '' ELSE VehicleTrim END + ' (' + BaseColor + ')' as VehicleDescription,
		MV.AdvertisementID,
		MA.Body + ' ' + MA.Footer as VehicleHighlights,
		MAP.Modified as LastChangedOn,
		MEM.LastName + ', ' + MEM.FirstName as LastChangedBy,
		T.Description 'TimePeriodDescription',
		CONVERT(varchar, T.BeginDate, 101) as FromDate,
		CONVERT(varchar, T.EndDate, 101) as ToDate,
		CASE I.Certified WHEN 1 THEN 'Yes' ELSE 'No' END as IsCertified
FROM	[Market].Merchandising.VehicleAdvertisement MV
JOIN	[Market].Merchandising.Advertisement_Properties MAP ON MAP.AdvertisementID = MV.AdvertisementID
JOIN	[Market].Merchandising.Advertisement MA ON MA.AdvertisementID = MV.AdvertisementID
JOIN	[FLDW].dbo.InventoryActive I ON I.InventoryID = MV.VehicleEntityID
JOIN	[FLDW].dbo.Vehicle V ON I.VehicleID = V.VehicleID AND V.BusinessUnitID =I.BusinessUnitID
JOIN	[IMT].dbo.MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID
JOIN	[Market].Merchandising.VehicleAdvertisement_Audit MVA ON MVA.AdvertisementID = MV.AdvertisementID
JOIN	[IMT].dbo.Member MEM ON MAP.Author = MEM.MemberID
JOIN  	[IMT].dbo.BusinessUnit B ON B.BusinessUnitID = I.BusinessUnitID
CROSS JOIN	[FLDW].dbo.Period_D T
WHERE	MA.RowTypeID = 1 -- Current Ad
AND		MVA.ValidUpTo > DATEADD(dd, +1, GETDATE())
AND		MVA.WasDelete = 0
AND		I.InventoryActive = 1
AND		I.InventoryType = 2
AND		I.BusinessUnitID = @DealerID
AND 	T.PeriodID = @PeriodID
AND		(MAP.Created BETWEEN T.BeginDate AND T.EndDate OR MAP.Modified BETWEEN T.BeginDate AND T.EndDate)

GO