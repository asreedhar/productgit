IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_PerformanceByAuctionOrWholesalerDrillThrough_Sales]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_PerformanceByAuctionOrWholesalerDrillThrough_Sales] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_PerformanceByAuctionOrWholesalerDrillThrough_Sales] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_PerformanceByAuctionOrWholesalerDrillThrough_Sales]
--------------------------------------------------------------------------------
--	
-- Drives the Sales table in Deal Log.rdl	
--
---History----------------------------------------------------------------------
--	
--	BYF	2008-07-29	Put documentation header in.
--					Added @WholesaleOnly flag.  The Deal Log.rdl is referenced
--					from two different contexts.  From the "Purchased Vehicle
--					Performance by Auction or Wholesaler" context, this bit 
--					should be 0, and from "Wholesale Performance by Auction 
--					or Wholesaler" this bit should be 1.  The first report is a
--					reception report while the second is a sales report.
--
--------------------------------------------------------------------------------
	@DealerID		INT,
	@InventoryTypeID    INT,
	@WholesalerID		INT,
	@TimePeriod			VARCHAR(25),
	@UnitCostBucket		VARCHAR(7),
	@MileageBucket		VARCHAR(7),
	@VehicleLight		VARCHAR(7),
	@WholesaleOnly		BIT = 0
AS

SET NOCOUNT ON

--Build TimePeriod Table
DECLARE @DateRange TABLE(
	Label varchar(25)
	, StartDate smalldatetime
	, EndDate smalldatetime
)

DECLARE @NOW datetime
SET @NOW = CAST(CONVERT(varchar(10),getdate(),101) AS DATETIME)

INSERT INTO @DateRange
SELECT 'CurrentMonthCurrentYear', DATEADD(dd,-DAY(@NOW) + 1, @NOW),DATEADD(dd,-DAY(@NOW), DATEADD(mm, 1, @NOW))
UNION ALL
SELECT 'PriorMonthCurrentYear', DATEADD(mm, -1, DATEADD(dd,-DAY(@NOW) + 1, @NOW)), DATEADD(dd,-DAY(@NOW), @NOW)
UNION ALL
SELECT 'MonthToDateCurrentYear', DATEADD(dd,-DAY(@NOW) + 1, @NOW),@NOW
UNION ALL
SELECT 'YearToDateCurrentYear', CAST(YEAR(@NOW) AS VARCHAR(4)) + '-01-01', @NOW

INSERT INTO @DateRange -- Using reference dates from above INSERTs
SELECT 'CurrentMonthLastYear', DATEADD(yy, -1, StartDate), DATEADD(yy, -1, EndDate)
FROM @DateRange
WHERE Label = 'CurrentMonthCurrentYear'
UNION ALL
SELECT 'PriorMonthLastYear', DATEADD(yy, -1, StartDate), DATEADD(yy, -1, EndDate)
FROM @DateRange
WHERE Label = 'PriorMonthCurrentYear'

INSERT INTO @DateRange
SELECT 'NextMonthLastYear', DATEADD(mm, 1, StartDate), DATEADD(dd, -1, DATEADD(mm, 2, StartDate))
FROM @DateRange
WHERE Label = 'CurrentMonthLastYear' -- From INSERTs set #2
UNION ALL
SELECT 'MonthToDateLastYear', DATEADD(yy, -1, StartDate), DATEADD(yy, -1, EndDate)
FROM @DateRange
WHERE Label = 'MonthToDateCurrentYear'
UNION ALL
SELECT 'YearToDateLastYear', DATEADD(yy, -1, StartDate), DATEADD(yy, -1, EndDate)
FROM @DateRange
WHERE Label = 'YearToDateCurrentYear'

--Build Unit Cost Bucket
DECLARE @UnitCostBuckets TABLE(
	BucketName varchar(7)
	, LowValue int
	, HighValue int
)

INSERT INTO @UnitCostBuckets
SELECT 'All', -2147483648, 2147483647
UNION ALL
SELECT '1', -2147483648, 10000
UNION ALL
SELECT '2', 10000, 19999
UNION ALL
SELECT '3', 20000, 29999
UNION ALL
SELECT '4', 30000, 39999
UNION ALL
SELECT '5', 40000, 49999
UNION ALL
SELECT '6', 50000, 59999
UNION ALL
SELECT '7', 60000, 69999
UNION ALL
SELECT '8', 70000, 79999
UNION ALL
SELECT '9', 80000, 89999
UNION ALL
SELECT '10', 90000, 99999
UNION ALL
SELECT '11', 100000, 2147483647

IF @UnitCostBucket = 'UNKNOWN'
SET @UnitCostBucket = NULL

--Build Mileage Bucket
DECLARE @MileageBuckets TABLE(
	BucketName varchar(7)
	, LowValue int
	, HighValue int
)

INSERT INTO @MileageBuckets
SELECT 'All', -2147483648, 2147483647
UNION ALL
SELECT '1', -2147483648, 25000
UNION ALL
SELECT '2', 25000, 49000
UNION ALL
SELECT '3', 50000, 74000
UNION ALL
SELECT '4', 75000, 99000
UNION ALL
SELECT '5', 100000, 124000
UNION ALL
SELECT '6', 125000, 149000
UNION ALL
SELECT '7', 150000, 2147483647

IF @MileageBucket = 'UNKNOWN'
SET @MileageBucket = NULL

--Process VehicleLight Parameter
DECLARE @VehicleLights TABLE(
	Label varchar(7)
	, LightID int
)

INSERT INTO @VehicleLights
SELECT 'All', 1
UNION ALL
SELECT 'All', 2
UNION ALL
SELECT 'All', 3
UNION ALL
SELECT '1', 1
UNION ALL
SELECT '2', 2
UNION ALL
SELECT '3', 3
UNION ALL
SELECT 'Not Set', NULL

DECLARE @SaleTypes TABLE (
	SaleTypeID INT
	, Description VARCHAR(20)
)

IF (@WholesaleOnly = 1)
BEGIN
	INSERT INTO @SaleTypes (SaleTypeID, Description)
	SELECT SaleTypeID, Description
	FROM [FLDW].dbo.SaleType_D
	WHERE SaleTypeID = 2
END
ELSE
BEGIN
	INSERT INTO @SaleTypes (SaleTypeID, Description)
	SELECT SaleTypeID, Description
	FROM [FLDW].dbo.SaleType_D
END

-- Do the query
SELECT 
	D.CalendarDateValue AS DealDate
	, V.Description + ' - ' + V.Trim AS VehicleDescription
	, Color.Color
	, ISales.FinanceInsuranceDealNumber AS DealNumber
	, I.StockNumber
	, ISales.DaysToSale
	, ISales.TotalGross
	, ISales.BackEndGross AS FIGross
	, ISales.SalePrice
	, I.UnitCost
	, I.MileageReceived AS Mileage
	, I.InitialVehicleLight
	, ST.Description AS SaleType
	, ISource.Description AS Wholesaler
	, BU.businessUnit AS Dealer
FROM [FLDW].dbo.Inventory I WITH (NOLOCK)
JOIN [FLDW].dbo.InventorySales_F ISales
	ON I.InventoryID = ISales.InventoryID
JOIN @SaleTypes ST
	ON ISales.SaleTypeID = ST.SaleTypeID
JOIN [FLDW].dbo.InventorySource_D ISource
	ON ISales.InventorySourceID = ISource.InventorySourceID
JOIN [FLDW].dbo.VehicleHierarchy_D V
	ON ISales.VehicleHierarchyID = V.VehicleHierarchyID
JOIN [FLDW].dbo.Color_D Color
	ON ISales.ColorID = Color.ColorID
JOIN [FLDW].dbo.DateDimension D
	ON ISales.DealDateTimeID = D.CalendarDateID
JOIN [FLDW].dbo.BusinessUnit BU
	ON I.businessUnitID = BU.businessUnitID
JOIN @DateRange DR
	ON D.CalendarDateValue BETWEEN DR.StartDate and DR.EndDate
JOIN @UnitCostBuckets UCB
	ON ((@UnitCostBucket IS NOT NULL AND I.UnitCost BETWEEN UCB.LowValue AND UCB.HighValue)
		OR (@UnitCostBucket IS NULL AND I.UnitCost IS NULL) )
JOIN @MileageBuckets MB
	ON ((@MileageBucket IS NOT NULL AND I.MileageReceived BETWEEN MB.LowValue AND MB.HighValue)
		OR (@MileageBucket IS NULL AND I.MileageReceived IS NULL) )
JOIN @VehicleLights Lights
	ON I.InitialVehicleLight = Lights.LightID
WHERE ISource.InventorySourceID = @WholesalerID
AND I.InventoryType = @InventoryTypeID
AND I.BusinessUnitID = @DealerID
AND DR.Label = @TimePeriod
AND UCB.BucketName = @UnitCostBucket
AND MB.BucketName = @MileageBucket
AND Lights.Label = @VehicleLight
ORDER BY 
	D.CalendarDateValue DESC
	, V.Description + ' - ' + V.Trim
