
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_VipStockingReportNewCar]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_VipStockingReportNewCar] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_VipStockingReportNewCar] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_VipStockingReportNewCar]
	@DealerID INT,
	@NumberOfWeeks INT,
	@InventoryTypeID INT
AS

SET NOCOUNT ON;

DECLARE @Forecast INT, @Mileage INT, @Weeks INT

SELECT @Mileage = 2147483647, @Forecast = CASE WHEN SIGN(@NumberOfWeeks) = 1 THEN 1 ELSE 0 END, @Weeks = ABS(@NumberOfWeeks)

Exec [IMT].[dbo].[GetSalesHistoryReport] @DealerID, @Weeks, @Forecast, @Mileage, @InventoryTypeID

GO
