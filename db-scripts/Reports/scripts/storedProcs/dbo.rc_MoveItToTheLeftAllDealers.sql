
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_MoveItToTheLeftAllDealers]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_MoveItToTheLeftAllDealers] AS SELECT 1')
GO
GRANT EXECUTE ON [dbo].[rc_MoveItToTheLeftAllDealers] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_MoveItToTheLeftAllDealers]
	@DealerGroupID INT,
	@MonthStart INT,
	@MonthEnd INT
AS

SET NOCOUNT ON;
SET ANSI_WARNINGS OFF;

DECLARE @BeginDate SMALLDATETIME, @EndDate SMALLDATETIME

SELECT	@BeginDate = CONVERT(DATETIME, CAST(@MonthStart*100+1 AS VARCHAR(8)), 112),
		@EndDate = DATEADD(MI, -1, DATEADD(MM, 1, CONVERT(DATETIME, CAST(@MonthEnd*100+1 AS VARCHAR(8)), 112)))

CREATE TABLE #Results (
	buid INT NOT NULL
	, businessunit VARCHAR(40) NOT NULL
	, SweetSpotPctValue FLOAT
	, SweetSpotRankID INT 
	, NonUnderperformingPctValue FLOAT
	, NonUnderperformingRankID INT
	, GiveBackPctValue FLOAT
	, GiveBackRankID INT
	, SweetSpotUnits INT
	, DealerGroup VARCHAR(40)
	, ClassGroup INT
	, Zero60RetailUnits INT
	, BeginDate SMALLDATETIME
	, EndDate SMALLDATETIME
	, PowerRanking FLOAT
	, OverallRank INT
);

CREATE TABLE #Dealers (
	DealerID INT NOT NULL,
	Dealer   VARCHAR(50) NOT NULL,
	DealerGroup    VARCHAR(50) NOT NULL
);

INSERT INTO #Dealers
SELECT	DS.BusinessUnitID DealerID,
		DS.BusinessUnit Dealer,
		DG.BusinessUnit DealerGroup
FROM	[IMT].dbo.BusinessUnit DS
JOIN	[IMT].dbo.BusinessUnitRelationship DR ON DR.BusinessUnitID = DS.BusinessUnitID
JOIN	[IMT].dbo.BusinessUnit DG ON DG.BusinessUnitID = DR.ParentID
JOIN	[IMT].dbo.DealerPreference DP ON DP.BusinessUnitID = DS.BusinessUnitID
JOIN	(
			SELECT	BusinessUnitID, SUM(Active) NumerOfActiveUpgrades
			FROM	[IMT].dbo.DealerUpgrade
			GROUP
			BY		BusinessUnitID
		) DU ON DU.BusinessUnitID = DS.BusinessUnitID
WHERE	DS.Active = 1
AND		DP.GoLiveDate IS NOT NULL
AND		DS.BusinessUnitTypeID = 4
AND		DU.NumerOfActiveUpgrades > 0
AND		DG.BusinessUnitID = @DealerGroupID;

IF @@ERROR <> 0
	GOTO Finished

INSERT INTO #Results (
	buid
	, businessunit
	, SweetSpotPctValue
	, SweetSpotRankID 
	, NonUnderperformingPctValue
	, NonUnderperformingRankID
	, GiveBackPctValue
	, GiveBackRankID
	, SweetSpotUnits
	, DealerGroup
	, Zero60RetailUnits
)
SELECT	BusinessUnitID			= D.DealerID
		,BusinessUnit			= D.Dealer
		,SweetSpotPctValue = 
			SUM(CASE WHEN VS.SaleDescription = 'R' AND I.DaysToSale BETWEEN 0 AND 29 THEN 1 ELSE 0 END)
			/ CAST(COUNT(VS.InventoryId) AS FLOAT)
		,SweetSpotRankID = 
			RANK() OVER(
				ORDER BY (
					SUM(CASE WHEN VS.SaleDescription = 'R' AND I.DaysToSale BETWEEN 0 AND 29 THEN 1 ELSE 0 END) 
					/ CAST(COUNT(VS.InventoryId) AS FLOAT)
				) DESC
			)
		,NonUnderperformingPctValue = --SalesEfficiencyPctValue
			SUM(CASE WHEN VS.SaleDescription = 'R' AND I.DaysToSale BETWEEN 0 AND 59 THEN 1 ELSE 0 END)
			/ CAST(COUNT(VS.InventoryId) AS FLOAT)
		,NonUnderperformingRankID = --SalesEfficiencyRankID
			RANK() OVER(
				ORDER BY (
					SUM(CASE WHEN VS.SaleDescription = 'R' AND I.DaysToSale BETWEEN 0 AND 59 THEN 1 ELSE 0 END) 
					/ CAST(COUNT(VS.InventoryId) AS FLOAT)
				) DESC
			)
		,GiveBackPctValue = --Wholesale loss in dollars
			-1 * ABS(SUM(CASE WHEN VS.SaleDescription = 'W' THEN VS.FrontEndGross ELSE 0 END)
				/ CAST(SUM(CASE WHEN VS.SaleDescription = 'R' THEN VS.FrontEndGross ELSE 0 END) AS FLOAT))
		,GiveBackRankID =
			RANK() OVER (
				ORDER BY (
					-1 * ABS(SUM(CASE WHEN VS.SaleDescription = 'W' THEN VS.FrontEndGross ELSE 0 END)
						/ CAST(SUM(CASE WHEN VS.SaleDescription = 'R' THEN VS.FrontEndGross ELSE 0 END) AS FLOAT))
				) DESC
			)
		,SweetSpotUnits = SUM(CASE WHEN VS.SaleDescription = 'R' AND I.DaysToSale BETWEEN 0 AND 29 THEN 1 ELSE 0 END)
		,DealerGroup = D.DealerGroup
		,Zero60RetailUnits = SUM(CASE WHEN VS.SaleDescription = 'R' AND I.DaysToSale BETWEEN 0 AND 59 THEN 1 ELSE 0 END)
FROM	[FLDW].dbo.Inventory I
JOIN	[FLDW].dbo.VehicleSale VS on I.InventoryID = VS.InventoryID
JOIN	#Dealers D ON I.BusinessUnitID = D.DealerID
WHERE	I.InventoryType = 2
AND		VS.DealDate Between @BeginDate And @EndDate
AND		(VS.SaleDescription = 'R' OR (VS.SaleDescription = 'W' AND I.DaysToSale >= 30))
Group By D.DealerGroup, D.DealerID, D.Dealer
Order By D.Dealer;

IF @@ERROR <> 0
	GOTO Finished;

WITH Constants AS (
	SELECT
		buid = buid 
		, ClassGroup = 0
		, BeginDate = @BeginDate
		, EndDate = @EndDate
		, PowerRanking = ((SweetSpotPctValue 
				+ NonUnderperformingPctValue 
				+ GiveBackPctValue) * 100) / 1.55
		, OverallRank = RANK() OVER(
			ORDER BY (
				((SweetSpotPctValue 
					+ NonUnderperformingPctValue 
					+ GiveBackPctValue) * 100) / 1.55
				) DESC
			)
	FROM #Results
)
UPDATE R
SET	R.ClassGroup = 0
	, R.BeginDate = @BeginDate
	, R.EndDate = @EndDate
	, R.PowerRanking = C.PowerRanking
	, R.OverallRank = C.OverallRank
FROM Constants C
JOIN #Results R ON C.buid = R.buid

IF @@ERROR <> 0
	GOTO Finished

SELECT *
FROM #Results

IF @@ERROR <> 0
	GOTO Finished

Finished:

DROP TABLE #Dealers;
DROP TABLE #Results;

GO
