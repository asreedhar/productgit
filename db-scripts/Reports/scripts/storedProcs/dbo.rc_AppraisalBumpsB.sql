
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_AppraisalBumpsB]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_AppraisalBumpsB] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_AppraisalBumpsB] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_AppraisalBumpsB]
	@PeriodID INT,
	@DealerID INT
AS

SET NOCOUNT ON;

WITH AppraisalStatistics AS
(
	SELECT	A.BusinessUnitID,
			A.MemberID,
			AVG(AV2.Value - AV1.Value) AvgBump,
			Count(distinct(A.VehicleID)) 'Bumped',
			Count(A.VehicleID ) 'Bumps',
			SUM( AV2.Value - AV1.Value ) 'BumpSUM'
	FROM	[IMT].dbo.Appraisals A
	JOIN	[IMT].dbo.AppraisalValues AV1 ON A.AppraisalID = AV1.AppraisalID
	LEFT JOIN [IMT].dbo.AppraisalValues AV2 ON (
					AV2.SequenceNumber > 0
				AND	AV1.AppraisalID = AV2.AppraisalID
				AND	AV1.SequenceNumber = AV2.SequenceNumber - 1
				AND	DATEDIFF(DD, AV1.DateCreated, AV2.DateCreated) < 60
			)
	CROSS JOIN [FLDW].dbo.Period_D P
	WHERE	A.BusinessUnitID = @DealerID
	AND		P.PeriodID = @PeriodID
	AND 	A.AppraisalTypeID = 1
	AND		DATEADD(DD, DATEDIFF(DD, 0, A.DateCreated), 0) BETWEEN P.BeginDate and P.EndDate
	AND		AV1.Value > 0.0 			    -- DON'T INCLUDE INITIAL VALUES OF ZERO, THEY SKEW THE AVERAGE
	AND		(AV2.Value - AV1.Value) > 0.0	-- DON'T COUNT NULL BUMPS
	GROUP
	BY	 A.BusinessUnitID, A.MemberID
)
SELECT	B.BusinessUnit 'BusinessUnitID',
		M.MemberID,
		M.LastName,
		M.FirstName,
		COUNT(A.AppraisalID) as 'VehiclesAppraised',
		SUM( Case When AV.AppraisalID is null Then 0 Else 1 End ) 'VehicleAppraisalValueRecorded',
		CAST( AB.Bumps as int ) 'Bumps',
		CAST( AB.Bumped as int ) 'Bumped',
		AB.Bumped / CAST( (SUM( Case When AV.AppraisalID is null Then 0 Else 1 End )) as decimal(9,2)) as 'VehiclesBumpedPct',
		AB.AvgBump * CAST( (AB.Bumps) as decimal(9,2)) as 'TotalBumpDollars',
		CAST( AB.AvgBump as int ) 'AvgBump',
		CAST( CAST(AB.BumpSUM as dec(9,2)) / CAST(AB.Bumped as dec(9,2)) as int ) 'AvgChange',
		P.BeginDate 'FromDate',
		P.EndDate   'ToDate'
FROM	[IMT].dbo.Appraisals A
JOIN	[IMT].dbo.BusinessUnit B ON B.BusinessUnitID = A.BusinessUnitID
JOIN	[IMT].dbo.Member M ON A.MemberID = M.MemberID
LEFT JOIN	[IMT].dbo.AppraisalValues AV ON AV.AppraisalID = A.AppraisalID
LEFT JOIN	[FLDW].dbo.Appraisal_F ACV ON ACV.AppraisalID = A.AppraisalID AND ACV.BusinessUnitID = A.BusinessUnitID AND ACV.AppraisalTypeID=A.AppraisalTypeID
LEFT JOIN	AppraisalStatistics AB ON A.BusinessUnitID = AB.BusinessUnitID AND A.MemberID = AB.MemberID
CROSS JOIN	[FLDW].dbo.Period_D P
WHERE 	A.BusinessUnitID = @DealerID
AND		P.PeriodID = @PeriodID
AND		M.MemberType != 1
AND		DATEADD(DD, DATEDIFF(DD, 0, A.DateCreated), 0) BETWEEN P.BeginDate and P.EndDate
AND		(AV.SequenceNumber IS NULL OR AV.SequenceNumber = 0)
AND		A.AppraisalTypeID = 1
GROUP
BY		B.BusinessUnit,
		M.MemberID,
		M.LastName,
		M.FirstName,
		AB.AvgBump,
		AB.Bumped,
		AB.Bumps,
		AB.BumpSUM,
		P.BeginDate,
		P.EndDate
GO
