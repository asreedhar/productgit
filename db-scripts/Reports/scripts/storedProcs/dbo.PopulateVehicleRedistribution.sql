
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PopulateVehicleRedistribution]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[PopulateVehicleRedistribution] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[PopulateVehicleRedistribution] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[PopulateVehicleRedistribution]
----Description-----------------------------------------------------------------
--
--	This script populates the VehicleRedistribution table and tracks vehicle
--  transfers relative to vehicles currently in active inventory.
--
--------------------------------------------------------------------------------
AS

SET NOCOUNT ON

--Table variable used to store resultset from recursive data generation
DECLARE @table1 TABLE
    (
      [RowNum] [int] NULL,
      [ParentID] [int] NULL,
      [StockNumber] [varchar](15) NULL,
      [InventoryID] [int] NULL,
      [UnitCost] [decimal](9, 2) NULL,
      [UnitCostBucket] [int] NULL,
      [BusinessUnitID] [int] NULL,
      [VehicleID] [int] NULL,
      [VehicleDescription] [varchar](50) NULL,
      [RedistributionFlag] [int] NULL,
      [PriorBusinessUnitID] [int] NULL,
      [CurrentLocationBusinessUnitID] [int] NULL,
      [CurrentActiveStatus] [tinyint] NULL,
      [CurrentUnitCost] [decimal](9, 2) NULL,
      [CurrentUnitCostBucket] [int] NULL,
      [CurrentMileageReceived] [int] NULL,
      [CurrentMileageReceivedBucket] [int] NULL,
      [CurrentVehicleLight] [varchar](10) NULL,
      [NextBusinessUnitID] [int] NULL,
      [Dealership] [varchar](50) NULL,
      [AgeInDays] [int] NULL,
      [DaysToSale] [int] NULL,
      [DaysInStock] [int] NULL,
      [VehicleLight] [varchar](10) NULL,
      [MileageReceived] [int] NULL,
      [MileageReceivedBucket] [int] NULL,
      [Color] [varchar](50) NULL,
      [InventoryReceivedDate] [smalldatetime] NULL,
      [DeleteDt] [smalldatetime] NULL,
      [InventoryActive] [tinyint] NULL,
      [PriorRowNum] [int] NULL,
      [PriorInventoryID] [int] NULL,
      [PriorUnitCost] [decimal](9, 2) NULL,
      [PriorDealership] [varchar](50) NULL,
      [PriorAgeInDays] [int] NULL,
      [PriorDaysToSale] [int] NULL,
      [PriorDaysInStock] [int] NULL,
      [PriorInventoryReceivedDate] [smalldatetime] NULL,
      [PriorDeleteDt] [smalldatetime] NULL,
      [PriorInventoryActive] [tinyint] NULL
    )

--Table variable used to remove vehicles with no distribution after further logic applied
DECLARE @table2 TABLE
    (
      [RowNum] [int] NULL,
      [ParentID] [int] NULL,
      [StockNumber] [varchar](15) NULL,
      [InventoryID] [int] NULL,
      [UnitCost] [decimal](9, 2) NULL,
      [UnitCostBucket] [int] NULL,
      [BusinessUnitID] [int] NULL,
      [VehicleID] [int] NULL,
      [VehicleDescription] [varchar](50) NULL,
      [RedistributionFlag] [int] NULL,
      [RedistributionFlagTotal] [INT] NULL,
      [PriorBusinessUnitID] [int] NULL,
      [CurrentLocationBusinessUnitID] [int] NULL,
      [CurrentActiveStatus] [tinyint] NULL,
      [CurrentUnitCost] [decimal](9, 2) NULL,
      [CurrentUnitCostBucket] [int] NULL,
      [CurrentMileageReceived] [int] NULL,
      [CurrentMileageReceivedBucket] [int] NULL,
      [CurrentVehicleLight] [varchar](10) NULL,
      [NextBusinessUnitID] [int] NULL,
      [Dealership] [varchar](50) NULL,
      [AgeInDays] [int] NULL,
      [DaysToSale] [int] NULL,
      [DaysInStock] [int] NULL,
      [VehicleLight] [varchar](10) NULL,
      [MileageReceived] [int] NULL,
      [MileageReceivedBucket] [int] NULL,
      [Color] [varchar](50) NULL,
      [InventoryReceivedDate] [smalldatetime] NULL,
      [DeleteDt] [smalldatetime] NULL,
      [InventoryActive] [tinyint] NULL,
      [PriorRowNum] [int] NULL,
      [PriorInventoryID] [int] NULL,
      [PriorUnitCost] [decimal](9, 2) NULL,
      [PriorDealership] [varchar](50) NULL,
      [PriorAgeInDays] [int] NULL,
      [PriorDaysToSale] [int] NULL,
      [PriorDaysInStock] [int] NULL,
      [PriorInventoryReceivedDate] [smalldatetime] NULL,
      [PriorDeleteDt] [smalldatetime] NULL,
      [PriorInventoryActive] [tinyint] NULL
    ) ;

---------------------------------------------------------------------------------------------------------
WITH    RedistributionCTE ( RowNum, ParentID, InventoryID, BusinessUnitId, VehicleID, StockNumber, UnitCost, UnitCostBucket, [Description], AgeInDays, DaysToSale, MileageReceived, MileageReceivedBucket, VehicleLight, Color, VehicleDescription, InventoryReceivedDate, DeleteDt, InventoryActive )
          AS ( SELECT   ROW_NUMBER() OVER ( PARTITION BY bur.[ParentId],
                                            I.[VehicleID] ORDER BY I.[InventoryReceivedDate] DESC ) AS RowNum,
                        BUR.[ParentId],
                        I.[InventoryID],
                        I.[BusinessUnitId],
                        I.[Vehicleid],
                        I.[StockNumber],
                        I.[UnitCost],
                        [UnitCostBucket] = CASE WHEN i.UnitCost >= 100000
                                                THEN 11
                                                WHEN I.UnitCost BETWEEN 90000 AND 99999
                                                THEN 10
                                                WHEN I.UnitCost BETWEEN 80000 AND 89999
                                                THEN 9
                                                WHEN I.UnitCost BETWEEN 70000 AND 79999
                                                THEN 8
                                                WHEN I.UnitCost BETWEEN 60000 AND 69999
                                                THEN 7
                                                WHEN I.UnitCost BETWEEN 50000 AND 59999
                                                THEN 6
                                                WHEN I.UnitCost BETWEEN 40000 AND 49999
                                                THEN 5
                                                WHEN I.UnitCost BETWEEN 30000 AND 39999
                                                THEN 4
                                                WHEN I.UnitCost BETWEEN 20000 AND 29999
                                                THEN 3
                                                WHEN I.UnitCost BETWEEN 10000 AND 19999
                                                THEN 2
                                                WHEN I.unitcost < 99999 THEN 1
                                                ELSE NULL
                                           END,
                        B.[Description],
                        I.[AgeInDays],
                        I.[DaysToSale],
                        I.[MileageReceived],
                        [MileageReceivedBucket] = CASE WHEN I.MileageReceived >= 150000
                                                       THEN 7
                                                       WHEN I.MileageReceived BETWEEN 125000 AND 14999
                                                       THEN 6
                                                       WHEN I.MileageReceived BETWEEN 100000 AND 124999
                                                       THEN 5
                                                       WHEN I.MileageReceived BETWEEN 75000 AND 99999
                                                       THEN 4
                                                       WHEN I.MileageReceived BETWEEN 50000 AND 74999
                                                       THEN 3
                                                       WHEN I.MileageReceived BETWEEN 25000 AND 49999
                                                       THEN 2
                                                       WHEN I.Mileagereceived < 25000
                                                       THEN 1
                                                       ELSE NULL
                                                  END,
                        VL.[Description] AS VehicleLight,
                        V.[BaseColor],
                        VG.[Description] AS VehicleDescription,
                        I.[InventoryReceivedDate],
                        I.[DeleteDt],
                        I.[InventoryActive]
               FROM     [FLDW].dbo.[InventoryPartitioned] I WITH ( NOLOCK )
                        JOIN [FLDW].dbo.[BusinessUnitRelationship] BUR WITH ( NOLOCK ) ON BUR.[BusinessUnitID] = I.[BusinessUnitID]
                        JOIN [FLDW].dbo.[BusinessUnit_D] B WITH ( NOLOCK ) ON B.[BusinessUnitID] = I.[BusinessUnitID]
                        JOIN [FLDW].dbo.[VehicleLight_D] VL WITH ( NOLOCK ) ON VL.[VehicleLightID] = I.[CurrentVehicleLight]
                        JOIN [FLDW].dbo.Vehicle V WITH ( NOLOCK ) ON I.Businessunitid = V.Businessunitid
                                                                     AND I.Vehicleid = V.Vehicleid
                        JOIN [FLDW].dbo.VehicleGrouping_D VG WITH ( NOLOCK ) ON V.GroupingDescriptionID = VG.[VehicleGroupingID]
                        LEFT JOIN [FLDW].dbo.VehicleSale VS WITH ( NOLOCK ) ON I.InventoryID = VS.InventoryID
               WHERE    ISNULL(VS.SaleDescription, '') <> 'R'
			   AND		I.InventoryType = 2 -- redistribute used car only
             )
    INSERT  INTO @table1
            SELECT  C.[RowNum],
                    C.[ParentID],
                    C.[StockNumber],
                    C.[InventoryID],
                    C.[UnitCost],
                    C.[UnitCostBucket],
                    C.[BusinessUnitID],
                    C.[VehicleID],
                    C.[VehicleDescription],
                    CASE WHEN DATEDIFF(DD, C.[DeleteDt],
                                       N.[InventoryReceivedDate]) BETWEEN -30
                                                                  AND     30
                         THEN NULL
                         ELSE DATEDIFF(DD, C.[DeleteDt],
                                       N.[InventoryReceivedDate])
                    END AS RedistributionFlag,
                    P.[BusinessUnitID] AS PriorBusinessUnitID,
                    CL.[BusinessUnitID] AS CurrentLocationBusinessUnitID,
                    CL.[InventoryActive] AS [CurrentActiveStatus],
                    CL.[UnitCost] AS [CurrentUnitCost],
                    CL.[UnitCostBucket] AS [CurrentUnitCostBucket],
                    CL.[MileageReceived] AS [CurrentMileageReceived],
                    CL.[MileageReceivedBucket] AS [CurrentMileageReceivedBucket],
                    CL.[VehicleLight] AS [CurrentVehicleLight],
                    N.[BusinessUnitID] AS NextBusinessUnitID,
                    C.[Description] AS Dealership,
                    C.[AgeInDays],
                    C.[DaysToSale],
                    ISNULL(C.[AgeInDays], 0) + ISNULL(C.DaysToSale, 0) AS DaysInStock,
                    C.VehicleLight,
                    C.[MileageReceived],
                    C.[MileageReceivedBucket],
                    C.Color,
                    C.[InventoryReceivedDate],
                    C.[DeleteDt],
                    C.[InventoryActive],
                    P.[RowNum] AS PriorRowNum,
                    P.[InventoryID] AS PriorInventoryID,
                    P.[UnitCost] AS PriorUnitCost,
                    P.[Description] AS PriorDealership,
                    P.[AgeInDays] AS PriorAgeInDays,
                    P.[DaysToSale] AS PriorDaysToSale,
                    ISNULL(P.[AgeInDays], 0) + ISNULL(P.DaysToSale, 0) AS PriorDaysInStock,
                    P.[InventoryReceivedDate] AS PriorInventoryReceivedDate,
                    P.[DeleteDt] AS PriorDeleteDt,
                    P.[InventoryActive] AS PriorInventoryActive
            FROM    RedistributionCTE C --Current
                    LEFT JOIN RedistributionCTE P ON C.RowNum = P.RowNum - 1
                                                     AND C.VehicleID = P.VehicleID--To get the previous dealer
                    LEFT JOIN RedistributionCTE N ON C.RowNUM = N.RowNum + 1
                                                     AND C.VehicleID = N.VehicleID --The next dealer, to eliminate dealer redistributions to itself
                    LEFT JOIN ( SELECT  [ParentID],
                                        [VehicleID],
                                        MIN([RowNum]) AS MaxRowNum
                                FROM    RedistributionCTE
                                GROUP BY [ParentID],
                                        [VehicleID]
                              ) CR ON CR.[ParentID] = C.[ParentID]
                                      AND CR.[VehicleID] = C.[VehicleID]
                    LEFT JOIN ( SELECT  [RowNum],
                                        [ParentID],
                                        [BusinessUnitID],
                                        [VehicleID],
                                        [UnitCost],
                                        [UnitCostBucket],
                                        [MileageReceived],
                                        [MileageReceivedBucket],
                                        [VehicleLight],
                                        [InventoryActive]
                                FROM    RedistributionCTE
                              ) CL ON CL.[ParentID] = C.[ParentID]
                                      AND CR.[MaxRowNum] = CL.[RowNum]
                                      AND CR.[VehicleID] = CL.[VehicleID]
            WHERE   NOT ( P.[BusinessUnitID] IS NULL
                          AND N.[BusinessUnitID] IS NULL
                        ) -- Filters vehicle not re-distributed	
                    AND CL.[InventoryActive] = 1
            ORDER BY [ParentID],
                    [VehicleID],
                    [RowNum]

---------------------------------------------------------------------------------------------------------

INSERT  INTO @table2
        SELECT  [RowNum],
                [ParentID],
                [StockNumber],
                [InventoryID],
                [UnitCost],
                [UnitCostBucket],
                [BusinessUnitID],
                [VehicleID],
                [VehicleDescription],
                [RedistributionFlag],
                ( SELECT    SUM([RedistributionFlag])
                  FROM      @table1 t2
                  WHERE     t1.[ParentID] = t2.[ParentID]
                            AND t1.[VehicleID] = t2.[VehicleID]
                            AND t2.RowNum <= t1.RowNum
                ) AS [RedistributionFlagTotal],
                [PriorBusinessUnitID],
                [CurrentLocationBusinessUnitID],
                [CurrentActiveStatus],
                [CurrentUnitCost],
                [CurrentUnitCostBucket],
                [CurrentMileageReceived],
                [CurrentMileageReceivedBucket],
                [CurrentVehicleLight],
                [NextBusinessUnitID],
                [Dealership],
                [AgeInDays],
                [DaysToSale],
                [DaysInStock],
                [VehicleLight],
                [MileageReceived],
                [MileageReceivedBucket],
                [Color],
                [InventoryReceivedDate],
                [DeleteDt],
                [InventoryActive],
                [PriorRowNum],
                [PriorInventoryID],
                [PriorUnitCost],
                [PriorDealership],
                [PriorAgeInDays],
                [PriorDaysToSale],
                [PriorDaysInStock],
                [PriorInventoryReceivedDate],
                [PriorDeleteDt],
                [PriorInventoryActive]
        FROM    @table1 t1

---------------------------------------------------------------------------------------------------------

TRUNCATE TABLE  dbo.VehicleRedistribution

---------------------------------------------------------------------------------------------------------

INSERT  INTO dbo.VehicleRedistribution
        SELECT  x1.[RowNum],
                z1.[NumOfRecentTrades],
                x1.[ParentID],
                x1.[StockNumber],
                x1.[InventoryID],
                x1.[UnitCost],
                x1.[UnitCostBucket],
                x1.[BusinessUnitID],
                x1.[VehicleID],
                x1.[VehicleDescription],
                x1.[PriorBusinessUnitID],
                x1.[CurrentLocationBusinessUnitID],
                x1.[CurrentUnitCost],
                x1.[CurrentUnitCostBucket],
                x1.[CurrentMileageReceived],
                x1.[CurrentMileageReceivedBucket],
                x1.[CurrentVehicleLight],
                x1.[Dealership],
                x1.[AgeInDays],
                x1.[DaysToSale],
                x1.[DaysInStock],
                x1.[VehicleLight],
                x1.[MileageReceived],
                x1.[MileageReceivedBucket],
                x1.[Color],
                x1.[InventoryReceivedDate],
                x1.[DeleteDt],
                x1.[InventoryActive],
                x1.[PriorRowNum],
                x1.[PriorInventoryID],
                x1.[PriorUnitCost],
                x1.[PriorDealership],
                x1.[PriorAgeInDays],
                x1.[PriorDaysToSale],
                x1.[PriorDaysInStock],
                x1.[PriorInventoryReceivedDate],
                x1.[PriorDeleteDt],
                x1.[PriorInventoryActive]
        FROM    @table2 x1
                JOIN ( SELECT   [ParentID],
                                [VehicleID],
                                COUNT([RowNum]) AS NumOfRecentTrades
                       FROM     @table2
                       WHERE    [RedistributionFlagTotal] IS NULL
                       GROUP BY [ParentID],
                                [VehicleID]
                     ) z1 ON x1.[ParentID] = z1.[ParentID]
                             AND x1.[VehicleID] = z1.VehicleID
        WHERE   x1.[RedistributionFlagTotal] IS NULL
                AND z1.[NumOfRecentTrades] > 1
GO