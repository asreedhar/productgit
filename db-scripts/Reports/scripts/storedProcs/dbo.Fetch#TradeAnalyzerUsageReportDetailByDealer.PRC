
/****** Object:  StoredProcedure [dbo].[Fetch#TradeAnalyzerUsageReportDetailByDealer]    Script Date: 08/01/2014 10:34:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Fetch#TradeAnalyzerUsageReportDetailByDealer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Fetch#TradeAnalyzerUsageReportDetailByDealer]
GO

/****** Object:  StoredProcedure [dbo].[Fetch#TradeAnalyzerUsageReportDetailByDealer]    Script Date: 08/01/2014 10:34:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		Vivek Rane
-- Create date: 2014-07-24
-- Last Modified By : Vivek Rane
-- Last Modified Date : 2014-07-24
-- Description:	This stored procedure returns the Data for Average Immidiate Wholesale Profit and Loss
-- =============================================


CREATE PROCEDURE [dbo].[Fetch#TradeAnalyzerUsageReportDetailByDealer]
	@Period INT,
    @SelectedDealerId VARCHAR(1000)=NULL,      
    @MemberID INT = NULL,
    @TradeInType TINYINT = 0,   -- 0 For all 1 for Analyzed and 2 for Not Analyzed
    @IsSummary BIT = 0    
	
AS
BEGIN

-- SET NOCOUNT ON added to prevent extra result sets FROM	
	SET NOCOUNT ON;
	
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
	DECLARE @DateField date
	,@EndDate date
	,@LocalPeriod INT 
	,@LocalSelectedDealerId VARCHAR(1000)   	
	,@LocalDealerGroupID INT 
	,@LocalMemberID INT
	,@LocalTradeInType TINYINT
	,@IsLocalSummary BIT
	,@WholeGroup VARCHAR(100)='Whole Group'
	,@OtherDealerships VARCHAR(100)='Other Dealerships You Manage'
	,@InGroupDealers VARCHAR(100)='In-Group Dealerships'
	,@TradeAnalyzed INT
	,@MaxGroupRank INT
	Select @LocalPeriod=@Period,@LocalSelectedDealerId=@SelectedDealerId,
	@LocalMemberID=@MemberID,@IsLocalSummary=@IsSummary,@LocalTradeInType=@TradeInType;
	
	Select @DateField=StartDate,@EndDate=EndDate FROM IMT.dbo.GetDatesFromPeriodId(@LocalPeriod,1);
	
	DECLARE  @BusinessUnit TABLE (BusinessUnitId INT,BusinessUnit varchar(200), Active tinyint)
	
	INSERT into @BusinessUnit SELECT CAST(NULLIF(S.Value,'') AS INT),BU.BusinessUnit,BU.Active FROM IMT.dbo.Split(@LocalSelectedDealerId,',') S  
	INNER JOIN IMT.dbo.BusinessUnit BU ON BU.BusinessUnitID=S.Value
	WHERE NULLIF(@LocalSelectedDealerId,'') IS NOT NULL AND LTRIM(RTRIM(S.Value))<>'';	
	
	DECLARE  @WholeGroupBusinessUnit TABLE (BusinessUnitId INT,BusinessUnit varchar(200))
	
	
	INSERT into @WholeGroupBusinessUnit SELECT BU.BusinessUnitID,BU.BusinessUnit from @BusinessUnit BU
	
		
	CREATE TABLE #TradeAnalyzed(AppraisalID INT, InventoryID INT,InventoryReceivedDate Date , MileageReceived INT, UnitCost Decimal(9,2),CurrentVehicleLight TINYINT,StockNumber VARCHAR(15),AppraisalDateModified DATE, VehicleID INT, BusinessUnitID INT)
	
	
		INSERT INTO #TradeAnalyzed 
			Select A.AppraisalID,I.InventoryID,I.InventoryReceivedDate,I.MileageReceived,I.UnitCost,I.CurrentVehicleLight,I.StockNumber,A.DateCreated,I.VehicleID,I.BusinessUnitID 
			from 
			IMT.dbo.Appraisals A
				INNER JOIN IMT.dbo.Inventory I on I.VehicleID=A.VehicleID And I.BusinessUnitID=A.BusinessUnitID
				INNER JOIN IMT.dbo.DealerPreference D on D.BusinessUnitID=I.BusinessUnitID
				INNER JOIN @WholeGroupBusinessUnit BU on BU.BusinessUnitId=I.BusinessUnitID 
				WHERE I.InventoryType=2
				  AND A.AppraisalSourceID=1
				  AND CAST(A.DateCreated AS DATE)<=CAST(I.InventoryReceivedDate AS DATE) 
				  AND I.TradeOrPurchase=2 AND A.AppraisalTypeID=1 
				 -- AND CAST(A.DateCreated AS DATE) between CAST(DATEADD(DD,-D.AppraisalLookBackPeriod,I.InventoryReceivedDate) AS DATE) and CAST(I.InventoryReceivedDate AS DATE)
				 AND cast(A.datemodified AS date) >= cast(DATEADD(DD,-D.AppraisalLookBackPeriod,I.InventoryReceivedDate) AS date ) -- and I.InventoryReceivedDate
				  AND CAST(I.InventoryReceivedDate AS DATE) BETWEEN @DateField AND @EndDate
		
		  
	CREATE TABLE #TradeNotAnalyzed(InventoryID INT,InventoryReceivedDate Date,MileageReceived INT, UnitCost Decimal(9,2),CurrentVehicleLight TINYINT,StockNumber VARCHAR(15),VehicleID INT, BusinessUnitID INT)
			
	
		INSERT INTO #TradeNotAnalyzed 
			Select I.InventoryID,I.InventoryReceivedDate,I.MileageReceived,I.UnitCost,I.CurrentVehicleLight,I.StockNumber,I.VehicleID,I.BusinessUnitID from IMT.dbo.Inventory I 		
				INNER JOIN @WholeGroupBusinessUnit BU on BU.BusinessUnitId=I.BusinessUnitID 				
				WHERE I.InventoryType=2				  				  
				  AND I.TradeOrPurchase=2 				  
				  AND CAST(I.InventoryReceivedDate AS DATE) BETWEEN @DateField AND @EndDate
				  AND NOT EXISTS (Select 1 from #TradeAnalyzed TA WHERE TA.InventoryID=I.InventoryID)
						
		IF @LocalTradeInType=0		
		BEGIN	
				SELECT  BU.BusinessUnitID,Bu.BusinessUnit
						,TA.AppraisalID
						,TA.InventoryID
						,TA.StockNumber
						,TA.InventoryReceivedDate
						,TA.AppraisalDateModified
						,V.VIN					
						,TA.CurrentVehicleLight					
						,V.VehicleYear					
						,V.MAKE+' '+V.Model+' ' + V.VehicleTrim AS ModelTrimBodyStyle 
						,TA.MileageReceived
						,TA.UnitCost										
					 FROM 
						#TradeAnalyzed TA						
						INNER JOIN  IMT.dbo.Vehicle V on V.vehicleID=TA.VehicleID											
						INNER JOIN IMT.dbo.DealerPreference DP on DP.BusinessUnitID =TA.BusinessUnitId
						INNER JOIN @BusinessUnit BU on BU.BusinessUnitId=TA.BusinessUnitID
				UNION  		
				SELECT  BU.BusinessUnitID,Bu.BusinessUnit
						,NULL
						,TA.InventoryID
						,TA.StockNumber
						,TA.InventoryReceivedDate
						,NULL
						,V.VIN					
						,TA.CurrentVehicleLight					
						,V.VehicleYear					
						,V.MAKE+' '+V.Model+' ' + V.VehicleTrim AS ModelTrimBodyStyle 
						,TA.MileageReceived
						,TA.UnitCost										
					 FROM 
						#TradeNotAnalyzed TA						
						INNER JOIN  IMT.dbo.Vehicle V on V.vehicleID=TA.VehicleID											
						INNER JOIN IMT.dbo.DealerPreference DP on DP.BusinessUnitID =TA.BusinessUnitId
						INNER JOIN @BusinessUnit BU on BU.BusinessUnitId=TA.BusinessUnitID		
						
		END
		ELSE IF @LocalTradeInType=1 
			BEGIN
				SELECT BU.BusinessUnitID,Bu.BusinessUnit
						,TA.AppraisalID
						,TA.InventoryID
						,TA.StockNumber
						,TA.InventoryReceivedDate
						,TA.AppraisalDateModified
						,V.VIN					
						,TA.CurrentVehicleLight					
						,V.VehicleYear					
						,V.MAKE+' '+V.Model+' ' + V.VehicleTrim AS ModelTrimBodyStyle 
						,TA.MileageReceived
						,TA.UnitCost										
					 FROM 
						#TradeAnalyzed TA						
						INNER JOIN  IMT.dbo.Vehicle V on V.vehicleID=TA.VehicleID											
						INNER JOIN IMT.dbo.DealerPreference DP on DP.BusinessUnitID =TA.BusinessUnitId
						INNER JOIN @BusinessUnit BU on BU.BusinessUnitId=TA.BusinessUnitID;
			END
			ELSE IF @LocalTradeInType=2			
			BEGIN					
					SELECT BU.BusinessUnitID,Bu.BusinessUnit
						,NULL AS AppraisalID
						,TA.InventoryID
						,TA.StockNumber
						,TA.InventoryReceivedDate
						,NULL AS AppraisalDateModified
						,V.VIN					
						,TA.CurrentVehicleLight					
						,V.VehicleYear					
						,V.MAKE+' '+V.Model+' ' + V.VehicleTrim AS ModelTrimBodyStyle 
						,TA.MileageReceived
						,TA.UnitCost										
					 FROM 
						#TradeNotAnalyzed TA						
						INNER JOIN  IMT.dbo.Vehicle V on V.vehicleID=TA.VehicleID											
						INNER JOIN IMT.dbo.DealerPreference DP on DP.BusinessUnitID =TA.BusinessUnitId
						INNER JOIN @BusinessUnit BU on BU.BusinessUnitId=TA.BusinessUnitID;
			END	
			
		DROP TABLE #TradeAnalyzed
		DROP TABLE  #TradeNotAnalyzed

END






GO



grant EXECUTE on Reports.dbo.Fetch#TradeAnalyzerUsageReportDetailByDealer to firstlook


GO 

