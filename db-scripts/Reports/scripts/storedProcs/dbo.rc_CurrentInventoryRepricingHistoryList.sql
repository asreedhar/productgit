
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_CurrentInventoryRepricingHistoryList]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_CurrentInventoryRepricingHistoryList] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_CurrentInventoryRepricingHistoryList] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_CurrentInventoryRepricingHistoryList]
	@InventoryID int
AS
SELECT	v.VehicleID,
		v.Vin,
		v.VehicleYear,
		mmg.Make,
		mmg.Model,
		v.VehicleTrim,
		v.BaseColor,
		lubt.Segment DisplayBodyType,
		i.InventoryReceivedDate,
		i.MileageReceived,
		i.UnitCost,
		i.StockNumber,
		lph.ListPrice,
		lph.DMSReferenceDT
FROM	[IMT].dbo.Inventory_ListPriceHistory lph
JOIN	[IMT].dbo.Inventory i ON lph.InventoryID = i.InventoryID
JOIN	[IMT].dbo.tbl_Vehicle v ON i.VehicleID = v.VehicleID
JOIN	[IMT].dbo.MakeModelGrouping mmg ON mmg.MakeModelGroupingID = v.MakeModelGroupingID
JOIN	[IMT].dbo.Segment lubt ON lubt.SegmentID = mmg.SegmentID
WHERE 	lph.InventoryID = @InventoryID
ORDER
BY		lph.DMSReferenceDT DESC
GO


