
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_InGroupTransfer#Summary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[rc_InGroupTransfer#Summary]
GO

CREATE PROCEDURE [dbo].[rc_InGroupTransfer#Summary]
	@DealerGroupID INT,
	@MemberID INT,
	@PeriodID INT
AS

SET NOCOUNT ON

DECLARE @DateFrom DATETIME, @DateUpTo DATETIME

SELECT  @DateFrom = BeginDate,
        @DateUpTo = EndDate
FROM    FLDW.dbo.Period_D
WHERE   PeriodID = @PeriodID

DECLARE @Dealer TABLE (DealerID INT NOT NULL, Dealer VARCHAR(40) NOT NULL, PRIMARY KEY (DealerID))

INSERT INTO @Dealer ( DealerID, Dealer )
    SELECT  B.BusinessUnitID, B.BusinessUnit
    FROM    dbo.MemberAccess(@DealerGroupID, @MemberID) A
    JOIN    IMT.dbo.BusinessUnit B ON B.BusinessUnitID = A.BusinessUnitID
    WHERE   B.Active = 1
    AND     B.BusinessUnitTypeID = 4

DECLARE @Dealers TABLE (NowDealerID INT NOT NULL, NowDealer VARCHAR(40) NOT NULL, WasDealerID INT NOT NULL, WasDealer VARCHAR(40) NOT NULL, PRIMARY KEY (NowDealerID, WasDealerID))

INSERT INTO @Dealers (NowDealerID, NowDealer, WasDealerID, WasDealer)
    SELECT  NowDealerID =   BNow.DealerID,
            NowDealer   =   BNow.Dealer,
            WasDealerID =   BWas.DealerID,
            WasDealer   =   BWas.Dealer
    FROM    @Dealer BNow CROSS JOIN @Dealer BWas
    WHERE   BNow.DealerID <> BWas.DealerID

DECLARE @Transfer TABLE (
    NowDealerID INT NOT NULL, NowDealer VARCHAR(40) NOT NULL, -- dst
    WasDealerID INT NOT NULL, WasDealer VARCHAR(40) NOT NULL, -- src
    WasFrontEndGross    INT NULL,
    NowFrontEndGross    INT NULL,
    NowSold             INT NOT NULL,
    NowInventory        INT NOT NULL,
    NowTotal            INT NOT NULL, -- was "Number"; Sold+Inventory == Total
    PRIMARY KEY (NowDealerID, WasDealerID))

INSERT INTO @Transfer (
    NowDealerID, NowDealer,
    WasDealerID, WasDealer,
    WasFrontEndGross,
    NowFrontEndGross,
    NowSold,
    NowInventory,
    NowTotal)

    SELECT  P.NowDealerID, P.NowDealer,
            P.WasDealerID, P.WasDealer,
            SUM(SWas.FrontEndGross),
            SUM(SNow.FrontEndGross),
            COUNT(SNow.InventoryID),
            COUNT(INow.InventoryID) - COUNT(SNow.InventoryID),
            COUNT(R.VehicleID)
    FROM    @Dealers P
    JOIN    IMT.Transfers.Transfer R
            ON  R.DstDealerID = P.NowDealerID
            AND R.SrcDealerID = P.WasDealerID
    JOIN    IMT.dbo.Inventory INow
            ON  INow.VehicleID = R.VehicleID
            AND INow.BusinessUnitID = R.DstDealerID
            AND INow.InventoryReceivedDate = R.DstDate
            AND INow.InventoryType = 2
    LEFT
    JOIN    IMT.dbo.tbl_VehicleSale SNow
            ON  SNow.InventoryID = INow.InventoryID
    JOIN    IMT.dbo.Inventory IWas
            ON  IWas.VehicleID = R.VehicleID
            AND IWas.BusinessUnitID = R.SrcDealerID
            AND IWas.InventoryType = 2
    JOIN    IMT.dbo.tbl_VehicleSale SWas
            ON  SWas.InventoryID = IWas.InventoryID
            AND SWas.DealDate = R.SrcDate
    WHERE   R.SrcDate BETWEEN @DateFrom AND @DateUpTo
    GROUP
    BY      P.NowDealerID,
            P.NowDealer,
            P.WasDealerID,
            P.WasDealer

SELECT  NowDealerID = COALESCE(T1.NowDealerID, T2.WasDealerID),
        NowDealer   = COALESCE(T1.NowDealer, T2.WasDealer),
        WasDealerID = COALESCE(T1.WasDealerID, T2.NowDealerID),
        WasDealer   = COALESCE(T1.WasDealer, T2.NowDealer),
        -- given == now -> was
        Given           = COALESCE(T2.NowTotal,0),
        GivenFEG        = COALESCE(T2.WasFrontEndGross,0),
        -- taken == was -> now
        TakenInventory  = COALESCE(T1.NowInventory,0),
        TakenSold       = COALESCE(T1.NowSold,0),
        TakenFEG        = COALESCE(T1.NowFrontEndGross,0)
--      T1 = was to now (taken)
FROM    @Transfer T1 
--      T2 = now to was (given)
FULL OUTER JOIN @Transfer T2 ON T1.NowDealerID = T2.WasDealerID AND T1.WasDealerID = T2.NowDealerID
WHERE   (T1.NowTotal IS NOT NULL AND T2.NowTotal IS NULL)
OR      (T1.NowTotal IS NULL AND T2.NowTotal IS NOT NULL)
ORDER
BY      COALESCE(T1.NowDealer, T2.WasDealer),
        COALESCE(T1.WasDealer, T2.NowDealer)

GO

GRANT EXECUTE ON [dbo].[rc_InGroupTransfer#Summary] TO [StoredProcedureUser]
GO
