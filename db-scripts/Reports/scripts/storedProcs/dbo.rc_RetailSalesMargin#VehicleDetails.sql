

/****** Object:  StoredProcedure [dbo].[rc_RetailSalesMargin#VehicleDetails]    Script Date: 07/28/2014 05:39:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_RetailSalesMargin#VehicleDetails]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[rc_RetailSalesMargin#VehicleDetails]
GO


/****** Object:  StoredProcedure [dbo].[rc_RetailSalesMargin#VehicleDetails]    Script Date: 07/28/2014 05:39:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


    
CREATE PROCEDURE [dbo].[rc_RetailSalesMargin#VehicleDetails]    
        @DealerID VARCHAR(6) ,        
        @AgeBucketID INT = 0 ,    
        @TargetThreshold INT=0 ,     
        @PeriodID TINYINT    
AS     
/******************************************************************************************************    
* Routine:  [dbo].[rc_RetailSalesMargin#VehicleDetails]  101126,0,0,13  
* Purpose:  Procedure for Summary Information for Retail Sales Margin Report    
* BUGZID :  30403   
*   
*******************************************************************************************************/    
    
--declare   @DealerID VARCHAR(6) ,    
--@AgeBucketID INT = 0,    
--@TargetThreshold INT = 0,  
--@PeriodID TINYINT    
  
--set @DealerID =104767  
--set @AgeBucketID =0  
--set @PeriodID =4 
Begin    
DECLARE  @LocalDealerID VARCHAR(6) ,  
        @LocalAgeBucketID INT = 0,  
        @LocalTargetThreshold INT=0,  
        @LocalPeriodID TINYINT  
		
Select @LocalDealerID=@DealerID, @LocalAgeBucketID=@AgeBucketID, @LocalTargetThreshold=@TargetThreshold, @LocalPeriodID=@PeriodID

DECLARE @saleDescription CHAR(1)    
DECLARE @BeginDate date,@EndDate Date
set @saleDescription = 'R'      
--SELECT @saleDescription = NULL    
--IF (@StrategyID = 1) SELECT @saleDescription = 'R'    
--IF (@StrategyID = 2) SELECT @saleDescription = 'W'    
     
    
DECLARE @MarketDaysSupplyBasePeriod SMALLINT    
DECLARE @Threshold DECIMAL(2,2)    
    
SET @Threshold =CAST(@LocalTargetThreshold AS DECIMAL(4,2))/100    
Select @BeginDate=StartDate,@EndDate=EndDate FROM IMT.dbo.GetDatesFromPeriodId(@LocalPeriodID,1);
    
SELECT  @MarketDaysSupplyBasePeriod = MarketDaysSupplyBasePeriod    
FROM    IMT.dbo.DealerPreference_Pricing DPP WITH(NOLOCK)    
WHERE   DPP.BusinessUnitID = @LocalDealerID           
    
SET @MarketDaysSupplyBasePeriod = COALESCE(@MarketDaysSupplyBasePeriod, 90)    
    
DECLARE @HighDays INT    
DECLARE @LowPrice INT    
DECLARE @HighPrice INT    
DECLARE @LowPriceLimit SMALLINT    
DECLARE @HighPriceLimit INT    
DECLARE @MaxBucketForAge SMALLINT    
--DECLARE @PingURLPrefix VARCHAR(200)    
    
    
--  This is the percentage points +/- the Market Average % limit    
SET @MaxBucketForAge =100   -- Buckets 1-6 are the actual Age Buckets; Anything >=100 is not.    
SET @HighDays =99999    -- This is the Maximum age of a vehicle    
SET @LowPrice=0     -- This is the bottom threshhold of a mispriced vehicle.  Anything below this value is mispriced.     
SET @HighPrice =100000    -- This is the top threshhold of a mispriced vehicle.  Anything above this value is mispriced.     
SET @LowPriceLimit =0    -- This is the bottom of the range of a mispriced vehicle.  LowPriceLimit to LowPrice is mispriced.    
SET @HighPriceLimit =999999   -- This is the top of the range of a mispriced vehicle.  HighPrice to HighpriceLimit is mispriced.    
    
--SET @PINGURLPrefix ='https://max.firstlook.biz/pricing/Pages/Internet/VehiclePricingAnalyzer.aspx?'    
     
     
DECLARE @AgeBuckets TABLE (    
 ColumnBucketName VARCHAR(20),    
 BucketName VARCHAR(20),    
 BucketNumber SMALLINT,    
 MarketAverageTarget REAL,    
 LowDays SMALLINT,    
 HighDays INT,    
 LowPrice INT,    
 HighPrice INT)    
     
/* There are 3 types of Buckets that are used to describe summary information in the Pricing Margin Report.    
 Priced Buckets  - 1,2,3,4,5,6  These are buckets for properly priced vehicles (vehicles with prices >=$1000 and<$=100,000)     
           within a given age range.    
 Non-priced Buckets - 101,102   These are buckets for improperly priced vehicles.  Vehicles with prices <$1000 or >$100,000    
           are 'Mispriced' vehicles.  Vehicles with prices of '$0' or NULL are 'Unpriced' vehicles.    
 Summary Buckets  - 100,103   These are buckets for summary vehicle information.  'Total Priced' (Bucket 100) is summary    
           information for buckets 1-6. 'Total Inventory' (Bucket 103) is summary information for    
           buckets 1-6 and 101 and 102.  These are not included in the AgeBucketTable              
    
*/    
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('Col0_21','0-21',6,1.10,0,21,@LowPrice,@HighPrice)    
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('Col22_29','22-29',5,1.05,22,29,@LowPrice,@HighPrice)    
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('Col30_39','30-39',4,1.00,30,39,@LowPrice,@HighPrice)    
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES ('Col40_49','40-49',3,.97,40,49,@LowPrice,@HighPrice)    
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('Col50_59','50-59',2,.95,50,59,@LowPrice,@HighPrice)    
INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('Col60_','60+',1,.90,60,@HighDays,@LowPrice,@HighPrice)    
--INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('ColUnpriced','Unpriced:',101,NULL,0,@HighDays,@LowPriceLimit,@LowPrice-1)    
--INSERT  INTO @AgeBuckets (ColumnBucketName,BucketName,BucketNumber,MarketAverageTarget,LowDays,HighDays,LowPrice,HighPrice) VALUES  ('ColMispriced','Mispriced',102,NULL,0,@HighDays,@HighPrice+1,@HighPriceLimit)    
    
    
/* The #TempSearches table collects Market information for a given piece of inventory usining it's coressponding Search from the Market.Pricing.Search table.    
 If the Precisiion Search (SearchTypeID =4) has sufficient information (i.e. and Avg. ListPrice >0), use the information from the precision search.  If    
 not, use the YMM Search (SearchTypeID =1) information.    
*/    
IF object_id('tempdb..#TempSearches')IS NOT NULL DROP TABLE #TempSearches    
CREATE TABLE #TempSearches    
    (    
      OwnerEntityID INT ,    
      VehicleEntityID INT ,    
      SearchTypeID TINYINT ,    
      AvgListPrice REAL ,    
      SumListPrice BIGINT ,    
      NumberOfListings BIGINT ,    
      NumberOfListingsWithPrice BIGINT    
    )    
    
INSERT  INTO #TempSearches    
        (     
    OwnerEntityID ,    
          VehicleEntityID              
        )    
        SELECT  DISTINCT    
                V.OwnerEntityID ,    
                V.VehicleEntityID    
        FROM    Market.Pricing.VehicleMarketHistory V WITH ( NOLOCK )    
                JOIN Market.Pricing.Owner O WITH ( NOLOCK ) ON V.OwnerEntityID = O.OwnerEntityID    
        WHERE   O.OwnerEntityID = @LocalDealerID    
                AND O.OwnerTypeID = 1    
                AND V.VehicleEntityTypeID = 1  -- Inventory  
                AND V.VehicleMarketHistoryEntryTypeID = 5  -- Vehicle Sale  
    
UPDATE  TS    
SET     SearchTypeID = 4 ,    
        AvgListPrice = X.AvgListPrice ,    
        SumListPrice = (X.AvgListPrice * X.NumberOfListingsWithPrice),    
        NumberOfListings = X.NumberOfListings,    
        NumberOfListingsWithPrice = X.NumberOfListingsWithPrice    
FROM    ( SELECT    VehicleEntityID ,    
                    SearchTypeID,    
                    CAST(SUM(Listing_TotalUnits) AS REAL) AS NumberOfListings,    
                    CAST(SUM(Listing_ComparableUnits) AS REAL) AS NumberOfListingsWithPrice ,    
                    CAST(SUM(ISNULL(Listing_AvgListPrice,0)) AS INT) AS AvgListPrice    
          FROM      Market.Pricing.VehicleMarketHistory V WITH ( NOLOCK )    
          WHERE     SearchTypeID=4  -- Precision Search  
     AND V.VehicleMarketHistoryEntryTypeID = 5  -- Vehicle Sale   
          GROUP BY  VehicleEntityID,  -- Same as InventoryID  
                    SearchTypeID    
          HAVING    CAST(SUM(ISNULL(Listing_AvgListPrice,0)) AS INT) > 0    
        ) X    
        JOIN #TempSearches TS ON X.VehicleEntityID = TS.VehicleEntityID    
    
UPDATE  TS    
SET     SearchTypeID = 1 ,    
        AvgListPrice = X.AvgListPrice ,    
        SumListPrice = (X.AvgListPrice * X.NumberOfListingsWithPrice),    
        NumberOfListings = X.NumberOfListings,    
        NumberOfListingsWithPrice = X.NumberOfListingsWithPrice    
FROM    ( SELECT    VehicleEntityID ,    
                    SearchTypeID,    
                    CAST(SUM(Listing_TotalUnits) AS REAL) AS NumberOfListings,    
                    CAST(SUM(Listing_ComparableUnits) AS REAL) AS NumberOfListingsWithPrice ,    
                    CAST(SUM(ISNULL(Listing_AvgListPrice,0)) AS INT) AS AvgListPrice    
          FROM      Market.Pricing.VehicleMarketHistory V WITH ( NOLOCK )    
          WHERE     SearchTypeID=1  -- Year, Make and Model  
     AND V.VehicleMarketHistoryEntryTypeID = 5    
        GROUP BY  VehicleEntityID,    
                    SearchTypeID    
          HAVING    CAST(SUM(ISNULL(Listing_AvgListPrice,0)) AS INT) > 0    
        ) X    
        JOIN #TempSearches TS ON X.VehicleEntityID = TS.VehicleEntityID    
WHERE   TS.SearchTypeID IS NULL    
     --select * from #TempSearches   
       
DECLARE @Results TABLE    
    ( Risk INT,  
	  Age SMALLINT ,    
      BucketNumber TINYINT ,    
      AgeBucketName VARCHAR(20) ,    
      [Year] SMALLINT ,    
      MakeModelTrim VARCHAR(100) ,    
      Certified CHAR(1) ,    
      StockNumber VARCHAR(20),    
      Mileage INT ,    
      UnitCost FLOAT ,    
      InternetPrice INT ,    
      PercentofMarketAverage REAL ,  
      InternetVsSale INT,  
      FrontEndGross DECIMAL, 
      BackEndGross DECIMAL,   
      SalePrice DECIMAL,    
	  PrecisionSearch CHAR(1) 
    )    
     
INSERT  INTO @Results    
        ( 
   Risk,    
   Age ,    
   BucketNumber ,    
   AgeBucketName ,    
   [Year] ,    
   MakeModelTrim ,    
   Certified ,    
   StockNumber ,    
   Mileage ,    
   UnitCost ,    
   InternetPrice ,    
   PercentofMarketAverage ,    
   InternetVsSale,
   FrontEndGross,  
   BackEndGross,  
   SalePrice, 
   PrecisionSearch   
        )    
   
   SELECT    
   I.CurrentVehicleLight,  
   I.DaysToSale Age ,    
   AB.BucketNumber ,    
   AB.BucketName ,    
   V.VehicleYear ,    
   MMG.Make + ' ' + MMG.Model + ' ' + COALESCE(V.VehicleTRIM, '') ,    
   CASE WHEN I.Certified = 1 THEN 'Y'    
     ELSE 'N'    
   END ,    
   I.StockNumber ,    
   I.MileageReceived ,   
   CAST(I.UnitCost AS FLOAT) UnitCost ,    
   CAST(ISNULL(I.ListPrice,0) AS INT) InternetPrice ,    
   CASE WHEN SumListPrice > 0    
       AND NumberOfListingsWithPrice > 0    
     THEN ISNULL(ListPrice,0) / ( SumListPrice    
         / NumberOfListingsWithPrice )    
     ELSE 0    
   END AS PercentofMarketAverage ,     
   (VS.SalePrice - ISNULL(I.ListPrice,0)) as InternetVsSale,
   VS.FrontEndGross AS [FrontEndGross],    
   VS.BackEndGross AS [BackEndGross],   
   VS.SalePrice AS [SalePrice],    
   case when TS.SearchTypeID=4 then 'Y' else 'N' end 
FROM    
FLDW.dbo.InventoryInActive I WITH ( NOLOCK )    
JOIN IMT.dbo.BusinessUnit BU WITH ( NOLOCK ) ON I.BusinessUnitID = BU.BusinessUnitID    
JOIN FLDW.dbo.DealerPreference DP WITH ( NOLOCK ) ON dp.BusinessUnitID = I.BusinessUnitID    
JOIN IMT.dbo.DealerPreference_Pricing AS DPP WITH ( NOLOCK ) ON DPP.BusinessUnitID = BU.BusinessUnitID    
JOIN Market.Pricing.DistanceBucket AS DB WITH ( NOLOCK ) ON DB.Distance = DPP.PingII_DefaultSearchRadius    
JOIN FLDW.dbo.Vehicle V WITH ( NOLOCK ) ON I.VehicleID = V.VehicleID  and I.BusinessUnitID = V.BusinessUnitID     
JOIN FLDW.dbo.VehicleSale VS WITH ( NOLOCK ) ON I.InventoryID = VS.InventoryID    
--JOIN FLDW.dbo.Period_D P ON VS.DealDate BETWEEN P.BeginDate AND P.EndDate    
JOIN FLDW.dbo.MakeModelGrouping MMG WITH ( NOLOCK ) ON MMG.MakeModelGroupingID = V.MakeModelGroupingID                    
LEFT JOIN #TempSearches TS ON I.InventoryID = TS.VehicleEntityID    
LEFT JOIN FLDW.dbo.Segment AS S2 WITH ( NOLOCK ) ON S2.SegmentID = V.SegmentID --@SegmentID    
JOIN @AgeBuckets AB ON COALESCE(I.DaysToSale, 0) BETWEEN AB.LowDays    
								  AND    
								  AB.HighDays    
		   AND CAST(ISNULL(I.ListPrice,0) AS INT) BETWEEN AB.LowPRice    
								  AND    
								  AB.HighPrice    
        WHERE   I.BusinessUnitID = @LocalDealerID  
                AND I.InventoryType = 2  
                AND VS.SaleDescription = @saleDescription
                --AND P.PeriodID = @LocalPeriodID  
                --AND ISNULL(ListPrice,0) > 0 
                AND VS.DealDate BETWEEN @BeginDate AND @EndDate      
                          
SELECT  *
FROM    @Results
WHERE    ( @LocalAgeBucketID = 0
              OR @LocalAgeBucketID = BucketNumber 
            )
ORDER BY BucketNumber ,
        Age
   
END 

GO
GRANT EXEC ON [Reports].[dbo].[rc_RetailSalesMargin#VehicleDetails] TO firstlook 
GO


