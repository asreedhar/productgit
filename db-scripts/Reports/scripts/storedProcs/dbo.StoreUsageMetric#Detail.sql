
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StoreUsageMetric#Detail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[StoreUsageMetric#Detail]
GO

SET NUMERIC_ROUNDABORT OFF;

SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT ON;

SET QUOTED_IDENTIFIER, ANSI_NULLS ON;

GO

CREATE PROCEDURE [dbo].[StoreUsageMetric#Detail]
	-- no parameters
AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.StoreUsageMetric#Detail.sql,v 1.2 2009/03/16 19:27:05 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * This is a internal report used by Pat that summarizes (rather crudely)
 * the usage of our system by our dealers.  It replaces the stored procedure
 * ReportHost.dbo.GetTCBCC (Tom Bergerson's Command Center).
 * 
 * Parameters
 * ----------
 *
 * None
 * 
 * Exceptions
 * ----------
 * 
 * None
 *
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

DECLARE @rc INT, @err INT

------------------------------------------------------------------------------------------------
-- Validate Parameter Values
------------------------------------------------------------------------------------------------

-- None

------------------------------------------------------------------------------------------------
-- API Output Schema
------------------------------------------------------------------------------------------------

DECLARE @ResultSummary TABLE (
	BusinessUnitID              INT         NOT NULL,
	DealerGroup                 VARCHAR(40) NOT NULL,
	Dealer                      VARCHAR(40) NOT NULL,
	TradeAnalyzerPct            INT         NULL,
	TradeAnalyzerScore          CHAR(1)     NOT NULL,
	InventoryPlanningPct        INT         NULL,
	InventoryPlanningScore      CHAR(1)     NOT NULL,
	OverallPct                  INT         NULL,
	OverallScore                CHAR(1)     NOT NULL
)

DECLARE @Results TABLE (
	BusinessUnitID              INT         NOT NULL,
	DealerGroup                 VARCHAR(40) NOT NULL,
	Dealer                      VARCHAR(40) NOT NULL,
	TradeInAnalyzed             INT         NULL,
	TradeIn                     INT         NULL,
	AgedInventoryWithValidPlan  INT         NULL,
	AgedInventory               INT         NULL,
	WatchInventoryWithValidPlan INT         NULL,
	WatchInventory              INT         NULL
)


--------------------------------------------------------------------------------------------
-- Begin
--------------------------------------------------------------------------------------------

INSERT INTO @Results (
	BusinessUnitID              ,
	DealerGroup                 ,
	Dealer                      ,
	TradeInAnalyzed             ,
	TradeIn                     ,
	AgedInventoryWithValidPlan  ,
	AgedInventory               ,
	WatchInventoryWithValidPlan ,
	WatchInventory              
)

SELECT	DISTINCT
	D.BusinessUnitID,
	G.BusinessUnit,
	D.BusinessUnit,
	TradeInAnalyzed.Measure,
	TradeIn.Measure,
	AgedInventoryWithValidPlan.Measure,
	AgedInventory.Measure,
	WatchInventoryWithValidPlan.Measure,
	WatchInventory.Measure

FROM	[IMT].dbo.BusinessUnit D
JOIN	[IMT].dbo.BusinessUnitRelationship R on D.BusinessUnitID = R.BusinessUnitID
JOIN	[IMT].dbo.BusinessUnit G on G.BusinessUnitID = R.ParentID
JOIN	[IMT].dbo.Dealerpreference P on D.BusinessUnitID = P.BusinessUnitID
LEFT JOIN [IMT].dbo.DealerUpgrade U on U.BusinessUnitID = D.BusinessUnitID 

JOIN	EdgeScorecard.dbo.Measures TradeInAnalyzed ON TradeInAnalyzed.BusinessUnitID = D.BusinessUnitID AND TradeInAnalyzed.MetricID = 144 AND TradeInAnalyzed.PeriodID = 22
JOIN	EdgeScorecard.dbo.Measures TradeIn ON TradeIn.BusinessUnitID = D.BusinessUnitID AND TradeIn.MetricID = 145 AND TradeIn.PeriodID = 22

JOIN	EdgeScorecard.dbo.Measures AgedInventoryWithValidPlan ON AgedInventoryWithValidPlan.BusinessUnitID = D.BusinessUnitID AND AgedInventoryWithValidPlan.MetricID = 284 AND AgedInventoryWithValidPlan.PeriodID = 7
JOIN	EdgeScorecard.dbo.Measures AgedInventory ON AgedInventory.BusinessUnitID = D.BusinessUnitID AND AgedInventory.MetricID = 282 AND AgedInventory.PeriodID = 7

JOIN	EdgeScorecard.dbo.Measures WatchInventoryWithValidPlan ON WatchInventoryWithValidPlan.BusinessUnitID = D.BusinessUnitID AND WatchInventoryWithValidPlan.MetricID = 290 AND WatchInventoryWithValidPlan.PeriodID = 7
JOIN	EdgeScorecard.dbo.Measures WatchInventory ON WatchInventory.BusinessUnitID = D.BusinessUnitID AND WatchInventory.MetricID = 288 AND WatchInventory.PeriodID = 7

WHERE	D.Active = 1
AND	D.BusinessUnitTypeID = 4  
AND	P.GoLiveDate IS NOT NULL 

ORDER
BY	G.BusinessUnit, D.BusinessUnit

; WITH Results (BusinessUnitID, DealerGroup, Dealer, TradeAnalyzerPct, InventoryPlanningPct) AS (
	SELECT	BusinessUnitID,
		DealerGroup,
		Dealer,
		CASE	WHEN TradeIn = 0 THEN NULL
			ELSE ROUND((TradeInAnalyzed/CAST(TradeIn AS REAL)*100.0),0) END,
		CASE	WHEN AgedInventory+WatchInventory = 0 THEN NULL
			ELSE ROUND((AgedInventoryWithValidPlan+WatchInventoryWithValidPlan)/CAST((AgedInventory+WatchInventory) AS REAL)*100.0,0) END
	FROM	@Results
)

INSERT INTO @ResultSummary (
	BusinessUnitID              ,
	DealerGroup                 ,
	Dealer                      ,
	TradeAnalyzerPct            ,
	TradeAnalyzerScore          ,
	InventoryPlanningPct        ,
	InventoryPlanningScore      ,
	OverallPct                  ,
	OverallScore                
)

SELECT	BusinessUnitID,
	DealerGroup,
	Dealer,
	-- trade analyzer usage
	TradeAnalyzerPct,
	CASE	WHEN TradeAnalyzerPct IS NULL            THEN 'U'
		WHEN TradeAnalyzerPct BETWEEN 75 AND 100 THEN 'G'
		WHEN TradeAnalyzerPct BETWEEN 25 AND  74 THEN 'Y' ELSE 'R' END,
	-- inventory planning usage
	InventoryPlanningPct,
	CASE	WHEN InventoryPlanningPct IS NULL            THEN 'U'
		WHEN InventoryPlanningPct BETWEEN 75 AND 100 THEN 'G'
		WHEN InventoryPlanningPct BETWEEN 25 AND  74 THEN 'Y' ELSE 'R' END,
	-- overall usage
	ROUND((TradeAnalyzerPct + InventoryPlanningPct)/2,0) OverallPct,
	CASE	WHEN ROUND((TradeAnalyzerPct + InventoryPlanningPct)/2,0) IS NULL THEN 'U'
		WHEN ROUND((TradeAnalyzerPct + InventoryPlanningPct)/2,0) BETWEEN 75 AND 100 THEN 'G'
		WHEN ROUND((TradeAnalyzerPct + InventoryPlanningPct)/2,0) BETWEEN 25 AND  74 THEN 'Y' ELSE 'R' END
FROM	Results

------------------------------------------------------------------------------------------------
-- Success
------------------------------------------------------------------------------------------------

SELECT * FROM @ResultSummary

RETURN 0

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

Failed:

RETURN @err

GO

GRANT EXECUTE, VIEW DEFINITION on [dbo].[StoreUsageMetric#Detail] to [StoredProcedureUser]
GO
