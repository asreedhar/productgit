
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_GrossProfitByDaysInStock#SummaryActiveAged]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_GrossProfitByDaysInStock#SummaryActiveAged] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_GrossProfitByDaysInStock#SummaryActiveAged] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[rc_GrossProfitByDaysInStock#SummaryActiveAged]
@DealerGroupID AS INT
,@MemberID AS INT

AS

/***************************************************************************************************************************
*	Routine:	[dbo].[rc_GrossProfitByDaysInStock#SummaryActiveAgedByBusinessUnitID]
*	
*	2/23/2010	SVU	Initial Creation
*	05/24/2010	MAK	Updated This procedure is like dbo.rc_GrossProfitByDaysInStock#SummaryActiveAged
*					but but includes the BusinessUnitID and additional datapoints so that MDS\% Mkt average 
*					and other averages can be correctly calculated in the aggregates on the reports.
*
****************************************************************************************************************************/
--
-- Gross Profit by Days in Stock (Dealer Group) Summary
--

SET NOCOUNT ON ;

DECLARE @HardLimit INT ,
        @PercentageLimit INT
DECLARE @MemberAccessTable TABLE (BusinessUnitID INT)
DECLARE @DealerGroupName VARCHAR(50)

SELECT	@DealerGroupName =BusinessUnit
FROM	IMT.dbo.BusinessUnit
WHERE	BusinessUnitID=@DealerGroupID

SET @HardLimit = 3
SET @PercentageLimit = 10

;
WITH    MemberBusinessUnitSet
          AS (SELECT    I.BusinessUnitID
              FROM      [IMT].dbo.MemberBusinessUnitSet S
              JOIN      [IMT].dbo.MemberBusinessUnitSetItem I ON S.MemberBusinessUnitSetID = I.MemberBusinessUnitSetID
              WHERE     S.MemberID = @MemberID
                        AND S.MemberBusinessUnitSetTypeID = 1 --Command Center Dealer Group Set
                        
             )
        INSERT  INTO @MemberAccessTable
                (BusinessUnitID)
                SELECT DISTINCT MBUS.BusinessUnitID
                FROM    Reports.dbo.MemberAccess(@DealerGroupID, @MemberID) MA
                JOIN    MemberBusinessUnitSet MBUS ON MA.BusinessUnitID = MBUS.BusinessUnitID


SELECT  @DealerGroupID AS DealerGroupID,
		@DealerGroupName AS DealerGroupName,
		Q.AgingBucket ,
		Q.BusinessUnitID,
		Q.BusinessUnit,
		MAX(MarketDaysSupplyBasePeriod) AS MarketDaysSupplyBasePeriod,
        SUM(q.VehicleCount) VehicleCount ,
        SUM(Q.UnitCost) UnitCost ,
        SUM(Q.InternetPrice) InternetPrice ,
        SUM(SumListPrice) SumListPrice,
        SUM(NumberOFListingsWithPrice) NumberOfListingsWithPrice,
        SUM (NumberOfSales) NumberOfSales,
        SUM (NumberOfListings) NumberOfListings,
        SUM(TotalPotentialGross) TotalPotentialGross  
FROM    (SELECT 1 AS VehicleCount ,
                UnitCost ,
                IA.BusinessUnitID AS BusinessUnitID,
                BU.BusinessUnit AS BusinessUnit,
                NULLIF(ia.ListPrice, 0) InternetPrice ,
                SumListPrice,
				NumberOfListingsWithPrice,
				NumberOfSales,
				NumberOfListings,
				COALESCE(DPP.MarketDaysSupplyBasePeriod,DPP.MarketDaysSupplyBasePeriod,90) AS MarketDaysSupplyBasePeriod,
                NULLIF(ia.ListPrice, 0) - NULLIF(ia.UnitCost, 0) TotalPotentialGross ,
                CASE WHEN IA.AgeInDays < 30 THEN '<30 Days'
                     WHEN IA.AgeInDays BETWEEN 30 AND 59 THEN '30 - 60 Days'
                     ELSE '60+ Days'
                END AgingBucket
         FROM   FLDW.dbo.InventoryActive IA WITH (NOLOCK)
         JOIN   FLDW.dbo.Vehicle V WITH (NOLOCK) ON ia.VehicleID = v.VehicleID
                                                    AND ia.BusinessUnitID = v.BusinessUnitID
         JOIN   @MemberAccessTable MAT ON MAT.BusinessUnitID = IA.BusinessUnitID
         JOIN	IMT.dbo.BusinessUnit BU ON IA.BusinessUnitID=BU.BusinessUnitID
         LEFT JOIN [IMT]..DealerPreference_Pricing DPP WITH (NOLOCK) ON IA.BusinessUnitID = DPP.BusinessUnitID
         LEFT JOIN [IMT]..DealerPreference_Pricing DPS WITH (NOLOCK) ON DPS.BusinessUnitID = 100150	-- Default Value
         LEFT JOIN Market.Pricing.Owner O WITH (NOLOCK) ON O.OwnerEntityID = IA.BusinessUnitID
                                                           AND O.OwnerTypeID = 1
         LEFT JOIN Market.Pricing.OwnerPreference OP WITH (NOLOCK) ON op.OwnerID = O.OwnerID
         LEFT JOIN imt.dbo.BodyType AS BT WITH (NOLOCK) ON BT.BodyType = v.BodyType
         LEFT JOIN Market.Pricing.Search AS S WITH (NOLOCK) ON IA.InventoryID = S.VehicleEntityID
                                                               AND S.VehicleEntityTypeID = 1
                                                               AND S.DistanceBucketID = OP.DistanceBucketID
                                                               AND S.OwnerID = O.OwnerID
          LEFT JOIN ( SELECT  SearchID,
                            SearchTypeID,
                            CAST(SUM(SumListPrice) AS REAL) AS SumListPrice,
                            CAST(SUM(Units) AS REAL) AS NumberOfListings,
                            CAST(SUM(ComparableUnits) AS REAL) AS NumberOfListingsWithPrice
                    FROM    Market.Pricing.SearchResult_A
                    WHERE   SearchResultTypeID = 1
                    GROUP BY SearchID,
                            SearchTypeID
                  ) LST ON S.SearchID = LST.SearchID
                           AND S.DefaultSearchTypeID = LST.SearchTypeID
        LEFT JOIN ( SELECT  SearchID,
                                    SearchTypeID,
                                    CAST(SUM(Units) AS REAL) AS NumberOfSales
                            FROM    Market.Pricing.SearchResult_A
                            WHERE   SearchResultTypeID = 2
                            GROUP BY SearchID,
                                    SearchTypeID
                          ) SLS ON S.SearchID = SLS.SearchID		
                          	AND S.DefaultSearchTypeID =SLS.SearchTypeID	 	
         WHERE  ia.InventoryType = 2
                AND ia.InventoryActive = 1)Q
    GROUP BY Q.AgingBucket ,
		Q.BusinessUnitID,
		Q.BusinessUnit     
		
GO		