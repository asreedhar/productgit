
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_InventoryAgingByAgeBucketSummary]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[rc_InventoryAgingByAgeBucketSummary] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[rc_InventoryAgingByAgeBucketSummary] TO [StoredProcedureUser]
GO

ALTER PROCEDURE dbo.rc_InventoryAgingByAgeBucketSummary
@DealerGroupID AS INT
,@MemberID AS INT

AS

/***************************************************************************************************************************
*	Routine:	dbo.rc_InventoryAgingByAgeBucketSummary
*	
*	2/23/2010	SVU	Initial Creation
****************************************************************************************************************************/

/*Testing Only*/
--SET @DealerGroupID = 101676
--SET @MemberID = 103184

SET NOCOUNT ON ;

DECLARE @HardLimit INT ,
        @PercentageLimit INT
        
DECLARE @MemberAccessTable TABLE (BusinessUnitID INT)
DECLARE @DealerData TABLE (BusinessUnit VARCHAR(50) ,
                           BusinessUnitID INT,
                           SortOrder INT ,
                           AgingBucket INT ,
                           VehicleCount INT ,
                           UnitCost DECIMAL(9,2) ,
                           InternetPrice DECIMAL(9,2) ,
                           MarketPrice DECIMAL(9,2) ,
                           MarketDays INT ,
                           TotalPotentialGross DECIMAL(11,2) ,
                           MarketDaySupplyCount INT,
                           InventoryPCT DECIMAL(5,2),
                           MarketAveragePCT DECIMAL(5,2),
                           PotentialGrossPerVehicle DECIMAL(9,2),
                           VehicleSalesPCT DECIMAL(5,2),
                           AverageVehicleGross DECIMAL(9,2),
                           MarketDaysSupply INT, 
                           SalesInBasePeriod DECIMAL(9,2), 
                           SalesUnits INT, 
                           MarketDaysSupplyBasePeriod INT)

SET @HardLimit = 3
SET @PercentageLimit = 10


  INSERT        INTO @MemberAccessTable
                (BusinessUnitID)
                SELECT  DISTINCT MBUS.BusinessUnitID
                FROM    Reports.dbo.MemberAccess(@DealerGroupID, @MemberID) MA
                JOIN    (SELECT I.BusinessUnitID
                         FROM   [IMT].dbo.MemberBusinessUnitSet S
                         JOIN   [IMT].dbo.MemberBusinessUnitSetItem I ON S.MemberBusinessUnitSetID = I.MemberBusinessUnitSetID
                         WHERE  S.MemberID = @MemberID
                                AND S.MemberBusinessUnitSetTypeID = 1 --Command Center Dealer Group Set
                         
                        ) MBUS ON MA.BusinessUnitID = MBUS.BusinessUnitID
                
                
;WITH MainDS AS
(                           
SELECT 	BU.BusinessUnit,
		BU.BusinessUnitID ,
		1 VehicleCount ,
                UnitCost ,
                NULLIF(ia.ListPrice, 0) InternetPrice ,
                COALESCE(SRA.AvgListPrice, SRA2.AvgListPrice) MarketPrice ,
                CASE WHEN COALESCE(SSF.SalesInBasePeriod, 0) < @HardLimit THEN NULL
                     WHEN ((SSF.SalesInBasePeriod / CAST(SRF.Units AS REAL)) * 100.0) < @PercentageLimit THEN NULL
                     ELSE ROUND(SRF.Units / (SSF.SalesInBasePeriod / CAST(COALESCE(DPP.MarketDaysSupplyBasePeriod, DPS.MarketDaysSupplyBasePeriod) AS REAL)), 0)
                END MarketDaySupply ,
                NULLIF(ia.ListPrice, 0) - NULLIF(ia.UnitCost, 0) TotalPotentialGross ,
                CASE WHEN IA.AgeInDays < 30 THEN 0
                     WHEN IA.AgeInDays BETWEEN 30 AND 59 THEN 30
                     ELSE 60
                END AgingBucket ,
                CASE WHEN COALESCE(SSF.SalesInBasePeriod, 0) < @HardLimit THEN 0
                     WHEN ((SSF.SalesInBasePeriod / CAST(SRF.Units AS REAL)) * 100.0) < @PercentageLimit THEN 0
                     ELSE 1
                END MarketDaySupplyCount, 
                SSF.SalesInBasePeriod,
                SRF.Units SalesUnits,
                CAST(COALESCE(DPP.MarketDaysSupplyBasePeriod, DPS.MarketDaysSupplyBasePeriod) AS REAL) MarketDaysSupplyBasePeriod
         FROM   FLDW.dbo.InventoryActive IA WITH (NOLOCK)
         JOIN	IMT.dbo.BusinessUnit BU WITH (NOLOCK) ON IA.BusinessUnitID=BU.BusinessUnitID
         JOIN   FLDW.dbo.Vehicle V WITH (NOLOCK) ON ia.VehicleID = v.VehicleID
                                      AND ia.BusinessUnitID = v.BusinessUnitID
         JOIN	Reports.dbo.MemberAccess(@DealerGroupID, @MemberID) MA ON MA.BusinessUnitID=BU.BusinessUnitID
         JOIN   @MemberAccessTable MAT ON MAT.BusinessUnitID = IA.BusinessUnitID
         LEFT JOIN [IMT]..DealerPreference_Pricing DPP WITH (NOLOCK) ON IA.BusinessUnitID = DPP.BusinessUnitID
         LEFT JOIN [IMT]..DealerPreference_Pricing DPS WITH (NOLOCK) ON DPS.BusinessUnitID = 100150	-- Default Value
         LEFT JOIN Market.Pricing.DistanceBucket DB WITH (NOLOCK) ON db.Distance=COALESCE(DPP.PingII_DefaultSearchRadius, DPS.PingII_DefaultSearchRadius)
         LEFT JOIN Market.Pricing.Owner O WITH (NOLOCK) ON O.OwnerEntityID = IA.BusinessUnitID AND O.OwnerTypeID=1
         LEFT JOIN Market.Pricing.OwnerPreference OP WITH (NOLOCK) ON op.OwnerID = O.OwnerID
         LEFT JOIN imt.dbo.BodyType AS BT WITH (NOLOCK) ON BT.BodyType = v.BodyType
         LEFT JOIN Market.Pricing.Search AS S WITH (NOLOCK) ON IA.InventoryID = S.VehicleEntityID
                                                               AND S.DefaultSearchTypeID = 4	-- Precision Market Search
                                                               AND S.VehicleEntityTypeID = 1
                                                               AND S.DistanceBucketID = COALESCE(OP.DistanceBucketID,db.DistanceBucketID)
         LEFT JOIN VehicleCatalog.Categorization.ModelConfiguration AS MC WITH (NOLOCK) ON MC.ModelConfigurationID = S.ModelConfigurationID
         LEFT JOIN VehicleCatalog.Categorization.Model AS M2 WITH (NOLOCK) ON M2.ModelID = mc.ModelID
         LEFT JOIN Market.Pricing.SearchResult_A AS SRA WITH (NOLOCK) ON S.SearchID = SRA.SearchID
                                                                         AND BT.BodyTypeID = SRA.BodyTypeID
                                                                         AND SRA.SearchResultTypeID = 1
                                                                         AND SRA.SearchTypeID = 4		-- Precision Market Search Results
                                                                         AND SRA.ModelFamilyID = M2.ModelFamilyID
                                                                         AND SRA.OwnerID = S.OwnerID
         LEFT JOIN Market.Pricing.Search AS SE2 WITH (NOLOCK) ON IA.InventoryID = SE2.VehicleEntityID
                                                                 AND SE2.DefaultSearchTypeID = 1	-- Year Make Model Search
                                                                 AND SE2.VehicleEntityTypeID = 1
                                                                 AND SE2.DistanceBucketID = COALESCE(OP.DistanceBucketID,db.DistanceBucketID)
         LEFT JOIN VehicleCatalog.Categorization.ModelConfiguration AS MC2 WITH (NOLOCK) ON MC2.ModelConfigurationID = SE2.ModelConfigurationID
         LEFT JOIN VehicleCatalog.Categorization.Model AS MO2 WITH (NOLOCK) ON MO2.ModelID = mc2.ModelID
         LEFT JOIN Market.Pricing.SearchResult_A AS SRA2 WITH (NOLOCK) ON SE2.SearchID = SRA2.SearchID
                                                                          AND BT.BodyTypeID = SRA2.BodyTypeID
                                                                          AND SRA2.SearchResultTypeID = 1
                                                                          AND SRA2.SearchTypeID = 1 -- Year Make Model Search Results
                                                                          AND SRA2.ModelFamilyID = MO2.ModelFamilyID
                                                                          AND SRA2.OwnerID = SE2.OwnerID
         LEFT JOIN Market.Pricing.SearchResult_F SRF WITH (NOLOCK) ON S.OwnerID = SRF.OwnerID
                                                        AND S.SearchID = SRF.SearchID
                                                        AND SRF.SearchTypeID = 4
         LEFT JOIN Market.Pricing.SearchSales_F SSF WITH (NOLOCK) ON SSF.SearchID = S.SearchID
                                                       AND SSF.SearchTypeID = 4
         WHERE  ia.InventoryType = 2
                AND ia.InventoryActive = 1

)

--
-- Base Insert of Active Inventory
--
INSERT INTO @DealerData (BusinessUnit, SortOrder, AgingBucket, VehicleCount, UnitCost, InternetPrice, MarketPrice, MarketDays, TotalPotentialGross, MarketDaySupplyCount
, SalesInBasePeriod, SalesUnits, MarketDaysSupplyBasePeriod)
SELECT COALESCE(D1.BusinessUnit, 'Total') BusinessUnit
, CASE WHEN D1.BusinessUnit IS NULL THEN 999 ELSE 1 END SortOrder
, COALESCE(D1.AgingBucket,999) AgingBucket
, SUM(D1.VehicleCount) VehicleCount
, AVG(D1.UnitCost) UnitCost
, AVG(D1.InternetPrice) InternetPrice
, AVG(D1.MarketPrice) MarketPrice
, SUM(D1.MarketDaySupply) MarketDays
, SUM(D1.TotalPotentialGross) TotalPotentialGross
, SUM(D1.MarketDaySupplyCount) MarketDaySupplyCount
, SUM(D1.SalesInBasePeriod) SalesInBasePeriod
, SUM(D1.SalesUnits) SalesUnits
, AVG(MarketDaysSupplyBasePeriod) MarketDaysSupplyBasePeriod
FROM 
(SELECT BusinessUnit, VehicleCount, UnitCost, InternetPrice, MarketPrice, MarketDaySupply, TotalPotentialGross, AgingBucket, MarketDaySupplyCount 
, SalesInBasePeriod, SalesUnits, MarketDaysSupplyBasePeriod, BusinessUnitID
FROM MainDS m
) D1
GROUP BY D1.BusinessUnit, D1.AgingBucket WITH CUBE
ORDER BY CASE WHEN D1.BusinessUnit IS NULL THEN 999 ELSE 1 END, D1.BusinessUnit, COALESCE(D1.AgingBucket, 999)


--
-- Insert Sales Data
--
UPDATE @DealerData
SET AverageVehicleGross=S.AvgFrontEndGross
FROM @DealerData D 
JOIN (SELECT  BU.BusinessUnit ,
        COUNT(*) Sales ,
        CASE WHEN I.DaysToSale < 30 THEN 0
             WHEN I.DaysToSale BETWEEN 30 AND 59 THEN 30
             WHEN I.DaysToSale >= 60 THEN 60
        END AgingBucket ,
        AVG(FrontEndGross) AvgFrontEndGross
FROM    FLDW.dbo.VehicleSale VS WITH (NOLOCK)
JOIN    FLDW.dbo.Inventory I WITH (NOLOCK) ON VS.BusinessUnitId = I.BusinessUnitID
                                AND VS.InventoryID = I.InventoryID
JOIN    @MemberAccessTable MAT ON VS.BusinessUnitId = MAT.BusinessUnitID
JOIN	IMT.dbo.BusinessUnit BU WITH (NOLOCK) ON BU.BusinessUnitID=VS.BusinessUnitId
LEFT JOIN [IMT]..DealerPreference_Pricing DPP WITH (NOLOCK) ON I.BusinessUnitID = DPP.BusinessUnitID
LEFT JOIN [IMT]..DealerPreference_Pricing DPS WITH (NOLOCK) ON DPS.BusinessUnitID = 100150	-- Default Value
WHERE   DealDate >= GETDATE() - COALESCE(DPP.MarketDaysSupplyBasePeriod, DPS.MarketDaysSupplyBasePeriod, 91)
GROUP BY Bu.BusinessUnit ,
	VS.BusinessUnitId ,
        CASE WHEN I.DaysToSale < 30 THEN 0
             WHEN I.DaysToSale BETWEEN 30 AND 59 THEN 30
             WHEN I.DaysToSale >= 60 THEN 60
        END
) S ON S.BusinessUnit=D.BusinessUnit AND S.AgingBucket=D.AgingBucket

--
-- Update Totals
--
UPDATE @DealerData
SET AverageVehicleGross=(SELECT SUM(DD.AverageVehicleGross * DD.VehicleCount) FROM @DealerData DD WHERE D.SortOrder=DD.SortOrder AND DD.BusinessUnit=D.BusinessUnit AND DD.SortOrder<>999 AND DD.AgingBucket<>999 )/D.VehicleCount
FROM @DealerData D
WHERE SortOrder=999 OR AgingBucket=999

UPDATE @DealerData
SET AverageVehicleGross=(SELECT SUM(DD.AverageVehicleGross * DD.VehicleCount) FROM @DealerData DD WHERE DD.AgingBucket=D.AgingBucket AND DD.SortOrder<>999)/D.VehicleCount
FROM @DealerData D
WHERE SortOrder=999

--SELECT * FROM @dealerdata

--
-- Update Additional Columns by comparing to Totals
--
UPDATE @DealerData
SET d.InventoryPCT =CAST(d.VehicleCount AS decimal(9,1)) / (SELECT NULLIF(DTOT.VehicleCount,0) FROM @DealerData DTOT WHERE DTOT.BusinessUnit=D.BusinessUnit AND DTOT.AgingBucket=999)
, MarketAveragePCT = 1.00-(MarketPrice - NULLIF(InternetPrice,0)) / NULLIF(MarketPrice,0)
, MarketDaysSupply = MarketDays / NULLIF(MarketDaySupplyCount,0)
, PotentialGrossPerVehicle = TotalPotentialGross / NULLIF(VehicleCount,0)
, VehicleSalesPct = CAST(d.SalesUnits AS decimal(9,1)) / (SELECT NULLIF(DTOT.SalesUnits,0) FROM @DealerData DTOT WHERE DTOT.BusinessUnit=D.BusinessUnit AND DTOT.AgingBucket=999)
FROM @DealerData d 


-- 
-- Retrieve data
--
SELECT * FROM @DealerData


GO
