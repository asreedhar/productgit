SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_GlumpInventoryVehicleReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[rc_GlumpInventoryVehicleReport]
GO

CREATE PROCEDURE [dbo].[rc_GlumpInventoryVehicleReport]
---Parameters-------------------------------------------------------------------------------------
--
	@segment		INT,
	@highBucket		INT,
	@lowBucket		INT,
	@periodId		INT,
	@businessUnitId		INT
--
---History--------------------------------------------------------------------------------------
--
--	JMW	07/16/2012	Created -- BUGZID: 15978
--      DR      09/13/2012      Remove retail/wholesale filter, this report is retail only
--
---Notes----------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------
AS 
    
SET NOCOUNT ON

DECLARE @beginDate DATETIME, @endDate DATETIME


SELECT	@beginDate=BeginDate,
		@endDate=EndDate 
FROM	FLDW.dbo.Period_D
WHERE	PeriodID = @periodId


SELECT I.StockNumber AS [RStockNumber]
	  ,V.VehicleYear AS [RYear] 
	  ,MMG.Model AS [RModel]
	  ,V.VehicleTrim AS [RTrim]
	  ,S.Segment AS [RSegment]
	  ,COALESCE(V.BaseColor, V.ExtColor, 'UNKNOWN') AS [RColor]
	  ,I.MileageReceived AS [RMileage]
	  ,I.UnitCost AS [RUnitCost]
	  ,I.ListPrice AS [RInternetPrice]
	  ,I.ListPrice-I.UnitCost AS [RPotentialGrossProfit]
	  ,DATEDIFF("d", I.InventoryReceivedDate, GETDATE()) AS [RDaysOld]	  
	  ,@segment SEGMENT
FROM FLDW.dbo.Inventory I
JOIN IMT.dbo.tbl_Vehicle V ON I.VehicleID = V.VehicleID
JOIN FLDW.dbo.InventorySales_F ISF ON ISF.BusinessUnitID = @businessUnitId AND ISF.InventoryID = I.InventoryID
JOIN FLDW.dbo.InventoryEventCategory_D IECD ON IECD.InventoryEventCategoryID = ISF.InventoryEventCategoryID
JOIN IMT.dbo.MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID
JOIN FLDW.dbo.Segment S ON MMG.SegmentID = S.SegmentID
WHERE I.BusinessUnitID = @businessUnitId AND
	  I.InventoryActive = 1 AND
	  I.ListPrice BETWEEN @lowBucket AND @highBucket AND
	  (@segment = -2 OR S.SegmentID = @segment) AND
	  IECD.IsRetail = 1 AND
	  I.InventoryType = 2 AND
          (I.ListPrice > 0 AND I.UnitCost > 0)

          

	  