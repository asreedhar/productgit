
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetReportSubscribers#GroupMakeADealAlerts]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [dbo].[GetReportSubscribers#GroupMakeADealAlerts] AS SELECT 1')
GO

GRANT EXECUTE ON [dbo].[GetReportSubscribers#GroupMakeADealAlerts] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [dbo].[GetReportSubscribers#GroupMakeADealAlerts]
	@SubscriptionFrequencyDetailID INT = NULL
AS

/* --------------------------------------------------------------------
 * 
 * $Id: dbo.GetReportSubscribers#GroupMakeADealAlerts.sql,v 1.4 2009/03/16 19:27:05 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Return a list of recipients for Group Make-A-Deal (AppraisalReviews) Report and frequency.
 * 
 * Parameters
 * ----------
 *
 * @SubscriptionFrequencyDetailID - the frequency of the report
 * 
 * Exceptions
 * ----------
 * 
 * 50100 - Invalid @SubscriptionFrequencyDetailID
 *
 * MAK -01/29/2009  These should only be sent to Active users.
 * -------------------------------------------------------------------- */

SET NOCOUNT ON

IF @SubscriptionFrequencyDetailID IS NULL BEGIN

	SELECT	@SubscriptionFrequencyDetailID = SubscriptionFrequencyDetailID
	FROM	[IMT].dbo.SubscriptionFrequencyDetail
	WHERE	Description = DATENAME(DW,GETDATE())

END

/* --------------------------------------------------------------------
 *                          Validate Parameters
 * -------------------------------------------------------------------- */

IF NOT EXISTS (SELECT 1 FROM [IMT].dbo.SubscriptionFrequencyDetail WHERE SubscriptionFrequencyDetailID = @SubscriptionFrequencyDetailID AND SubscriptionFrequencyID <> 0) BEGIN
	RAISERROR('Invalid SubscriptionFrequencyDetail ''%d''.', 16, 1, @SubscriptionFrequencyDetailID)
	RETURN 50100
END

/* --------------------------------------------------------------------
 *                          Declare Result Schema
 * -------------------------------------------------------------------- */

DECLARE @Results TABLE (
	[TO]                VARCHAR(8000) NOT NULL,
	[RenderFormat]      VARCHAR(8000) NOT NULL,
	[Subject]           VARCHAR(8000) NOT NULL,
	[DealerGroupID]		INT NOT NULL,
	[DaysBack]			INT NOT NULL,
	[MemberID]          INT NOT NULL
)

/* --------------------------------------------------------------------
 *                             Collect Recipients
 * -------------------------------------------------------------------- */
INSERT INTO @Results (
	[TO],
	[RenderFormat],
	[Subject],
	[DealerGroupID],
	[DaysBack],
	[MemberID]
)

SELECT	[TO]          = M.EmailAddress,
	[RenderFormat]	= 'MHTML',
	[Subject]     = BusinessUnit + '''s ' + F.Description + ' ' + T.Description + ' for ' + D.Description + ', ' + CONVERT(VARCHAR,GETDATE(),107),
	[DealerGroupID] = MARP.BusinessUnitID,
	[DaysBack]	= MARP.DaysBack,
	[MemberID] = M.MemberID

FROM	[IMT].dbo.Subscriptions S
JOIN	[IMT].dbo.SubscriptionTypes T ON S.SubscriptionTypeID = T.SubscriptionTypeID
JOIN	[IMT].dbo.Member M ON S.SubscriberID = M.MemberID
JOIN	[IMT].dbo.MemberAppraisalReviewPreferences MARP ON M.MemberID = MARP.MemberID
JOIN	[IMT].dbo.BusinessUnit B ON MARP.BusinessUnitID = B.BusinessUnitID
CROSS JOIN [IMT].dbo.SubscriptionFrequencyDetail D
JOIN       [IMT].dbo.SubscriptionFrequency F ON D.SubscriptionFrequencyID = F.SubscriptionFrequencyID

WHERE	D.SubscriptionFrequencyDetailID = @SubscriptionFrequencyDetailID
AND	S.SubscriptionTypeID = 15 -- The only Group Make-A-Deal Subscription
AND	S.SubscriptionFrequencyDetailID IN (@SubscriptionFrequencyDetailID,3)
AND	S.SubscriberTypeID = 1           -- Member
AND	S.SubscriptionDeliveryTypeID = 1 -- HTML email
AND	M.EmailAddress IS NOT NULL
AND	LEN(M.EmailAddress) > 5
AND	PATINDEX('%@%.%', M.EmailAddress) > 0
AND	B.Active =1

/* --------------------------------------------------------------------
 *                           Return Results
 * -------------------------------------------------------------------- */

SELECT * FROM @Results

GO


