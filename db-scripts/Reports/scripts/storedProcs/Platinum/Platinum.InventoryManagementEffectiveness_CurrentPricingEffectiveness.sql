IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[InventoryManagementEffectiveness#CurrentPricingEffectiveness]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[InventoryManagementEffectiveness#CurrentPricingEffectiveness] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[InventoryManagementEffectiveness#CurrentPricingEffectiveness] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[InventoryManagementEffectiveness#CurrentPricingEffectiveness]
	@DealerID INT
AS

SET NOCOUNT ON

/* --------------------------------------------------------------------
 * 
 * $Id: Platinum.InventoryManagementEffectiveness_CurrentPricingEffectiveness.sql,v 1.7 2010/02/10 21:54:24 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns metrics for CurrentPricingEffectiveness
 * 
 * Parameters
 * ----------
 *
 * @DealerID
 * 
 * Exceptions
 * ----------
 *
 *	MAK 05/11/2009  I don't know how we missed this previously, but % Sold should be Sold/Total NOT
 *			Sold/(Sold+Total).
 *
 *  BYF Aug. 26, 2009 Update to proc to add more metrics for Pricing Ad Appraisal Effectiveness report.
 * -------------------------------------------------------------------- */

DECLARE @Results TABLE (
	Sold int,
	Total int,
	PercentSold int,
	SoldStatus varchar(20),
	PercentSoldOrNot int,
	PercentUnsold int
)

DECLARE 
	@Sold int,
	@Total int,
	@PercentSold int,
	@PercentUnsold int
	
SELECT @Sold = Cast( Measure as int ) 
	FROM EdgeScorecard..Measures 
	WHERE BusinessUnitID = @DealerID And PeriodID = 7 And SetID = 126 And MetricID = 248

SELECT @Total = Cast( Measure as int ) 
	FROM EdgeScorecard..Measures 
	WHERE BusinessUnitID = @DealerID And PeriodID = 7 And SetID = 126 And MetricID = 247

SET @PercentSold = 
	CASE WHEN @Sold > 0 or @Total > 0 
		THEN CAST (( ROUND( @Sold / CAST((@Total ) as decimal(12,2)) , 2 ) ) * 100  as int ) 
		ELSE 0
	END

SET @PercentUnsold = (100-@PercentSold)

INSERT INTO @Results (
	Sold,
	Total,
	PercentSold,
	SoldStatus,
	PercentSoldOrNot,
	PercentUnsold
) VALUES (
	@Sold,
	@Total,
	@PercentSold,
	'Sold',
	@PercentSold,
	@PercentUnsold
)

INSERT INTO @Results (
	Sold,
	Total,
	PercentSold,
	SoldStatus,
	PercentSoldOrNot,
	PercentUnsold
) VALUES (
	@Sold,
	@Total,
	@PercentSold,
	'UnSold',
	@PercentUnsold,
	@PercentUnsold
)

SELECT 
	Sold,
	Total,
	PercentSold,
	SoldStatus,
	PercentSoldOrNot,
	PercentUnsold 
FROM @Results