
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[WeeklyTradeInAppraisal#ImmediateWholesaleChart]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[WeeklyTradeInAppraisal#ImmediateWholesaleChart] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[WeeklyTradeInAppraisal#ImmediateWholesaleChart] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[WeeklyTradeInAppraisal#ImmediateWholesaleChart]
	@DealerID INT
AS

SET NOCOUNT ON


DECLARE @Results TABLE (
	NumberOfWeeks INT,
	ImmediateWholesaleProfit DECIMAL(12,2)
)

INSERT INTO @Results (NumberOfWeeks, ImmediateWholesaleProfit)
SELECT	4, Measure
FROM	[EdgeScorecard].dbo.Measures
WHERE	BusinessUnitID = @DealerID
	AND PeriodID = 2
	AND SetID = 7
	AND MetricID = 18

INSERT INTO @Results (NumberOfWeeks, ImmediateWholesaleProfit)
SELECT	8, Measure
FROM	[EdgeScorecard].dbo.Measures
WHERE	BusinessUnitID = @DealerID
	AND PeriodID = 26
	AND SetID = 7
	AND MetricID = 18

SELECT * FROM @Results

GO
