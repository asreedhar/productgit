
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[UnderstockingOverstocking]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[UnderstockingOverstocking] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[UnderstockingOverstocking] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[UnderstockingOverstocking]
	@DealerID INT
AS

SET NOCOUNT ON



DECLARE @Results TABLE
(
	BusinessUnit varchar(40),
	HasExtraSection bit,
	RedLight int
)

DECLARE 
	@BusinessUnit varchar(40),
	@HasExtraSection bit,
	@RedLight int

SELECT  @BusinessUnit = BusinessUnit FROM [IMT].dbo.BusinessUnit WHERE BusinessUnitID = @DealerID
SELECT  @HasExtraSection =  CASE WHEN B.BusinessUnitID =  @DealerID and R.parentid = 100205 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END FROM [IMT].dbo.BusinessUnit B JOIN [IMT].dbo.BusinessUnitRelationship R ON R.BusinessUnitID = B.BusinessUnitID WHERE 	B.BusinessUnitID = @DealerID 
select @RedLight = CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 22) AND (SetID = 39) AND (MetricID = 99)


INSERT INTO @Results VALUES 
(
	@BusinessUnit,
	@HasExtraSection,
	@RedLight
)


SELECT * FROM @Results