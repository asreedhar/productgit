
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[UnderstockingOverstocking#Understocking]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[UnderstockingOverstocking#Understocking] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[UnderstockingOverstocking#Understocking] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[UnderstockingOverstocking#Understocking]
	@DealerID INT
AS

SET NOCOUNT ON

select 	F.BusinessUnitID, 
	GD.GroupingDescription, 
	DaysSupply, 
	AverageInventoryAge,
	GD.GroupingDescriptionWithoutSegment,
	GD.Segment
FROM 	[EdgeScorecard].dbo.BusinessUnitGroupingDescriptionFacts F
	join [EdgeScorecard].dbo.GroupingDescription GD 
ON	F.GroupingDescriptionID = GD.GroupingDescriptionID
WHERE	OverUnderStockFlag = 2
	and PeriodID = 27
	and BusinessUnitID = @DealerID
