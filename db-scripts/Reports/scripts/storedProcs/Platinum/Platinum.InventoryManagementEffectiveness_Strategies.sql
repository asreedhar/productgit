IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[InventoryManagementEffectiveness#Strategies]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[InventoryManagementEffectiveness#Strategies] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[InventoryManagementEffectiveness#Strategies] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[InventoryManagementEffectiveness#Strategies]
	@DealerID INT
AS

SET NOCOUNT ON
--  MAK 11/03/2008  Added more information to make formatting easier.

DECLARE @endDate smalldatetime, @startDate smalldatetime, @now smalldatetime, @buid int
DECLARE @CountTotal int

SET @now = getDate()
SET @endDate = IMT.dbo.ToDate(getDate())
SET @startDate = dateadd(ww,-4,@endDate)

SELECT @CountTotal =Count(*)
FROM IMT.dbo.AIP_Event aie
  JOIN IMT.dbo.Inventory iu ON aie.InventoryID = iu.InventoryID
  JOIN IMT.dbo.AIP_EventType aiet ON aie.AIP_EventTypeID = aiet.AIP_EventTypeID
  JOIN IMT.dbo.AIP_EventCategory aiec ON aiet.AIP_EventCategoryID = aiec.AIP_EventCategoryID AND aiec.AIP_EventCategoryID in (1,2)
WHERE iu.BusinessUnitID = @DealerID
  And aie.BeginDate Between @startDate And @endDate
 

SELECT 
aiec.[Description] AS 'Objective',
aiet.[Description] AS 'Strategy',
aiec.[Description]  + ' - '+ aiet.[Description] AS 'ObjectiveAndStrategy',
count(*) AS 'Number',
Round(cast(count(*) as real)/cast(@CountTotal as real)*100,1)  as 'Percentage',
Cast(Round(cast(count(*) as real)/cast(@CountTotal as real)*100,1)as varchar(5))+'%  ('+cast(count(*) as varchar(10)) + ') - ' + aiec.[Description]  + ' - '+ aiet.[Description] as Label
FROM IMT.dbo.AIP_Event aie
  JOIN IMT.dbo.Inventory iu ON aie.InventoryID = iu.InventoryID
  JOIN IMT.dbo.AIP_EventType aiet ON aie.AIP_EventTypeID = aiet.AIP_EventTypeID
  JOIN IMT.dbo.AIP_EventCategory aiec ON aiet.AIP_EventCategoryID = aiec.AIP_EventCategoryID AND aiec.AIP_EventCategoryID in (1,2)
WHERE iu.BusinessUnitID = @DealerID
  And aie.BeginDate Between @startDate And @endDate
 
GROUP BY aiec.[Description], aiet.[Description]
ORDER BY aiec.[Description], aiet.[Description]

