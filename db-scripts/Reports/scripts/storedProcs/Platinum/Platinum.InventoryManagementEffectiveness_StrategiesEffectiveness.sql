IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[InventoryManagementEffectiveness#StrategiesEffectiveness]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[InventoryManagementEffectiveness#StrategiesEffectiveness] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[InventoryManagementEffectiveness#StrategiesEffectiveness] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[InventoryManagementEffectiveness#StrategiesEffectiveness]
	@DealerID INT
AS

SET NOCOUNT ON


DECLARE @Results TABLE
(
	Advertise int,
	AdvertiseTotal int,
	LotPromote int,
	LotPromoteTotal int,
	Spiff int,
	SpiffTotal int,
	Other int,
	OtherTotal int,
	Wholesaler int,
	WholesalerTotal int,
	Auction int,
	AuctionTotal int,
	ThirtyRetailBy60 int,
	ThirtyRetailBy60Total int
)

DECLARE 
	@AdvertiseCount int,
	@AdvertiseTotalCount int,
	@LotPromoteCount int,
	@LotPromoteTotalCount int,
	@SpiffCount int,
	@SpiffTotalCount int,
	@OtherCount int,
	@OtherTotalCount int,
	@WholesalerCount int,
	@WholesalerTotalCount int,
	@AuctionCount int,
	@AuctionTotalCount int,
	@ThirtyRetailBy60Count int,
	@ThirtyRetailBy60TotalCount int


SELECT	@AdvertiseCount = Cast( Measure as int ) From EdgeScorecard..Measures Where BusinessUnitID = @DealerID And PeriodID = 7 And SetID = 128 And MetricID = 254
SELECT	@AdvertiseTotalCount = Cast( Measure as int ) From EdgeScorecard..Measures Where BusinessUnitID = @DealerID And PeriodID = 7 And SetID = 128 And MetricID = 253

SELECT @LotPromoteCount = Cast( Measure as int ) FROM EdgeScorecard..Measures Where BusinessUnitID = @DealerID And PeriodID = 7 And SetID = 129 And MetricID = 257
SELECT @LotPromoteTotalCount = Cast( Measure as int ) From EdgeScorecard..Measures Where BusinessUnitID = @DealerID And PeriodID = 7 And SetID = 129 And MetricID = 256

SELECT @SpiffCount = Cast( Measure as int ) From EdgeScorecard..Measures Where BusinessUnitID = @DealerID And PeriodID = 7 And SetID = 131 And MetricID = 263
SELECT @SpiffTotalCount = Cast( Measure as int ) From EdgeScorecard..Measures Where BusinessUnitID = @DealerID And PeriodID = 7 And SetID = 131 And MetricID = 262

SELECT @OtherCount = Cast( Measure as int ) From EdgeScorecard..Measures Where BusinessUnitID = @DealerID And PeriodID = 7  And SetID = 130 And MetricID = 260
SELECT @OtherTotalCount = Cast( Measure as int ) From EdgeScorecard..Measures Where BusinessUnitID = @DealerID And PeriodID = 7 And SetID = 130 And MetricID = 259

SELECT @WholesalerCount = Cast( Measure as int ) From EdgeScorecard..Measures Where BusinessUnitID = @DealerID And PeriodID = 7 And SetID = 132 And MetricID = 266
SELECT @WholesalerTotalCount = Cast( Measure as int ) From EdgeScorecard..Measures Where BusinessUnitID = @DealerID And PeriodID = 7 And SetID = 132 And MetricID = 265

SELECT @AuctionCount = Cast( Measure as int ) From EdgeScorecard..Measures Where BusinessUnitID = @DealerID And PeriodID = 7 And SetID = 133 And MetricID = 269
SELECT @AuctionTotalCount = Cast( Measure as int ) From EdgeScorecard..Measures Where BusinessUnitID = @DealerID And PeriodID = 7   And SetID = 133 And MetricID = 268

Select @ThirtyRetailBy60Count  = Cast( Measure as int ) From EdgeScorecard..Measures Where BusinessUnitID = @DealerID And PeriodID = 26 And MetricID = ( 20 )
Select @ThirtyRetailBy60TotalCount  = Cast( Measure as int ) From EdgeScorecard..Measures Where BusinessUnitID = @DealerID And PeriodID = 26 And MetricID = ( 21 )



INSERT INTO @Results VALUES 
(
	@AdvertiseCount,
	@AdvertiseTotalCount,
	@LotPromoteCount,
	@LotPromoteTotalCount,
	@SpiffCount,
	@SpiffTotalCount,
	@OtherCount,
	@OtherTotalCount,
	@WholesalerCount,
	@WholesalerTotalCount,
	@AuctionCount,
	@AuctionTotalCount,
	@ThirtyRetailBy60Count,
	@ThirtyRetailBy60TotalCount
)


SELECT * FROM @Results