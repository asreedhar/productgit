IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[InventoryManagementEffectiveness]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[InventoryManagementEffectiveness] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[InventoryManagementEffectiveness] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[InventoryManagementEffectiveness]
	@DealerID INT
AS


SELECT BusinessUnit FROM [IMT].dbo.BusinessUnit WHERE BusinessUnitID = @DealerID
