IF NOT EXISTS
	(SELECT *
	FROM sys.objects
	WHERE object_id = OBJECT_ID(N'[Platinum].[UnderstockingOverstocking#ExtraCurrentInventory]')
	AND type in (N'P', N'PC'))

EXECUTE('CREATE PROCEDURE [Platinum].[UnderstockingOverstocking#ExtraCurrentInventory] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[UnderstockingOverstocking#ExtraCurrentInventory] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[UnderstockingOverstocking#ExtraCurrentInventory]
	@DealerID INT
AS

SET NOCOUNT ON


DECLARE @Results TABLE
(
	DaysSupply int,
	Suv INT NULL,
	Sedan INT NULL,
	Truck INT NULL,
	Van INT NULL,
	Coupe INT NULL,
	Wagon INT NULL,
	Convertible INT NULL
)

DECLARE 
	@DaysSupplyCount int,
	@SuvCount INT,
	@SedanCount INT,
	@TruckCount INT,
	@VanCount INT,
	@CoupeCount INT,
	@WagonCount INT,
	@ConvertibleCount INT

SELECT	@DaysSupplyCount =  CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 3) AND (SetID = 14) AND (MetricID = 34)

SELECT	@SuvCount	=		CASE WHEN comments is null THEN null ELSE CAST(Measure AS int) END FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 3) AND (SetID = 17) AND (MetricID = 36)
SELECT	@SedanCount =		CASE WHEN comments is null THEN null ELSE CAST(Measure AS int) END FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 3) AND (SetID = 17) AND (MetricID = 35)
SELECT	@TruckCount =		CASE WHEN comments is null THEN null ELSE CAST(Measure AS int) END FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 3) AND (SetID = 17) AND (MetricID = 37)
SELECT	@VanCount	=		CASE WHEN comments is null THEN null ELSE CAST(Measure AS int) END FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 3) AND (SetID = 17) AND (MetricID = 39)
SELECT	@CoupeCount =		CASE WHEN comments is null THEN null ELSE CAST(Measure AS int) END FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 3) AND (SetID = 17) AND (MetricID = 38)
SELECT	@WagonCount =       CASE WHEN comments is null THEN null ELSE CAST(Measure AS int) END FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 3) AND (SetID = 17) AND (MetricID = 40)
SELECT	@ConvertibleCount = CASE WHEN comments is null THEN null ELSE CAST(Measure AS int) END FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 3) AND (SetID = 17) AND (MetricID = 41)


INSERT INTO @Results VALUES 
(
	@DaysSupplyCount,
	@SuvCount,
	@SedanCount,
	@TruckCount,
	@VanCount,
	@CoupeCount,
	@WagonCount,
	@ConvertibleCount
)

SELECT * FROM @Results