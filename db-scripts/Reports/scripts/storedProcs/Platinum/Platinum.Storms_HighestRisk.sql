
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[Storms#HighestRisk]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[Storms#HighestRisk] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[Storms#HighestRisk] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[Storms#HighestRisk]
	@DealerID INT
AS

SET NOCOUNT ON

DECLARE @Results TABLE
(
	RedOver15Units INT,
	RedOver15Inv INT,
	RedOver15Water INT,
	YellowOver30Units INT,
	YellowOver30Inv INT,
	YellowOver30Water INT, 
	AllUnits INT,
	RedPercent INT,
	YellowPrecent INT
)

DECLARE 
	@RedOver15Units INT,
	@RedOver15Inv INT,
	@RedOver15Water INT,
	@YellowOver30Units INT,
	@YellowOver30Inv INT,
	@YellowOver30Water INT, 
	@AllUnits INT,
	@RedPercent INT,
	@YellowPrecent INT


SELECT	@RedOver15Units =  CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 9) AND (MetricID = 22)
SELECT	@RedOver15Inv =  CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 9) AND (MetricID = 23)
SELECT	@RedOver15Water =  CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 9) AND (MetricID = 25)
SELECT	@YellowOver30Units =  CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 10) AND (MetricID = 26)
SELECT	@YellowOver30Inv =  CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 10) AND (MetricID = 27)
SELECT	@YellowOver30Water =  CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 10) AND (MetricID = 29)
SELECT	@AllUnits =  CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 20) AND (MetricID = 30)

Set @RedPercent    = CASE WHEN @AllUnits > 0 THEN Cast (( ROUND( @RedOver15Units    / cast(@AllUnits as decimal(12,2)) , 2 ) ) * 100  as int ) ELSE 0 END
Set @YellowPrecent = CASE WHEN @AllUnits > 0 THEN Cast (( ROUND( @YellowOver30Units / cast(@AllUnits as decimal(12,2)) , 2 ) ) * 100  as int ) ELSE 0 END


INSERT INTO @Results VALUES 
(
	@RedOver15Units,
	@RedOver15Inv,
	@RedOver15Water,
	@YellowOver30Units,
	@YellowOver30Inv,
	@YellowOver30Water, 
	@AllUnits,
	@RedPercent,
	@YellowPrecent
)

SELECT * FROM @Results