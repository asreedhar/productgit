
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[UnderstockingOverstocking#DaysSupply]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[UnderstockingOverstocking#DaysSupply] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[UnderstockingOverstocking#DaysSupply] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[UnderstockingOverstocking#DaysSupply]
	@DealerID INT
AS

SET NOCOUNT ON


DECLARE @Results TABLE (
	metric nvarchar(20),
	total INT
)


INSERT INTO @Results (metric, total)
select 'Suv',	formattedmeasure from	dbo.GetMeasureResults(100147, 'Composite Basis Period', 'Purchasing', 'Current Inventory', 'Days Supply by Segment', 'suv')

INSERT INTO @Results (metric, total)
select 'Sedan',		formattedmeasure from	dbo.GetMeasureResults(100147, 'Composite Basis Period', 'Purchasing', 'Current Inventory', 'Days Supply by Segment', 'sedan')

INSERT INTO @Results (metric, total)
select 'Truck',		formattedmeasure from	dbo.GetMeasureResults(100147, 'Composite Basis Period', 'Purchasing', 'Current Inventory', 'Days Supply by Segment', 'truck')

INSERT INTO @Results (metric, total)
select 'Van',		formattedmeasure from	dbo.GetMeasureResults(100147, 'Composite Basis Period', 'Purchasing', 'Current Inventory', 'Days Supply by Segment', 'van')

INSERT INTO @Results (metric, total)
select 'Coupe',		formattedmeasure from	dbo.GetMeasureResults(100147, 'Composite Basis Period', 'Purchasing', 'Current Inventory', 'Days Supply by Segment', 'coupe')

INSERT INTO @Results (metric, total)
select 'Wagon',		formattedmeasure from	dbo.GetMeasureResults(100147, 'Composite Basis Period', 'Purchasing', 'Current Inventory', 'Days Supply by Segment', 'wagon')

INSERT INTO @Results (metric, total)
select 'Convertible',		formattedmeasure from	dbo.GetMeasureResults(100147, 'Composite Basis Period', 'Purchasing', 'Current Inventory', 'Days Supply by Segment', 'convertible')


SELECT * FROM @Results


