
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[UnderstockingOverstocking#Overstocking]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[UnderstockingOverstocking#Overstocking] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[UnderstockingOverstocking#Overstocking] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[UnderstockingOverstocking#Overstocking]
	@DealerID INT
AS

SET NOCOUNT ON

select 	F.BusinessUnitID, 
	GD.GroupingDescription, 
	DaysSupply, 
	AverageInventoryAge,
	GD.GroupingDescriptionWithoutSegment,
	GD.Segment
from 	[EdgeScorecard].dbo.BusinessUnitGroupingDescriptionFacts F
	JOIN [EdgeScorecard].dbo.GroupingDescription GD 
	ON F.GroupingDescriptionID = GD.GroupingDescriptionID
where	OverUnderStockFlag = 1
	and PeriodID = 27
	and BusinessUnitID = @DealerID
