IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[InventoryMerchandisingEffectiveness#CurrentMerchandisingAverages]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[InventoryMerchandisingEffectiveness#CurrentMerchandisingAverages] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[InventoryMerchandisingEffectiveness#CurrentMerchandisingAverages] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[InventoryMerchandisingEffectiveness#CurrentMerchandisingAverages]
	@DealerID INT
AS

SET NOCOUNT ON

/* --------------------------------------------------------------------
 * 
 * $Id: Platinum.InventoryMerchandisingEffectiveness#CurrentMerchandisingAverages.sql,v 1.5 2010/02/10 21:54:24 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns summary metrics (averages) for "Merchandising Effectiveness"
 * 
 * Parameters
 * ----------
 *
 * @DealerID
 * 
 * Exceptions
 * ----------
 *
 *  JTM 10 Aug '09 Calculates the Avg number of days from InventoryReceivedDate to ad merchandized date (ValidFrom), for all inventory that was merchandized.  Snapshot, as of "today" (run date).
 *  For "Platinum Redux" report - Merchandising section.
 * 
 * -------------------------------------------------------------------- */

DECLARE @Results TABLE (
 	NumMerchandised INT,
 	SumDaysUntilAdCreated DECIMAL(20,1),
 	AvgDaysToCreateAd DECIMAL(20,1) 
)

DECLARE 
	@NumMerchandised INT
	, @SumDaysUntilAdCreated DECIMAL(20,1) 
	, @AvgDaysToCreateAd DECIMAL(20,1)

DECLARE @InventoryMerchandised TABLE ( --All inventory w/ Merchandised Ad as of today 
	InventoryID INT, 
	AdCreateDate DATETIME, 
	ReceivedDate SMALLDATETIME
)

INSERT INTO @InventoryMerchandised (InventoryID, AdCreateDate, ReceivedDate)   
SELECT DISTINCT I.InventoryID
	, MVA.ValidFrom
	, I.InventoryReceivedDate
FROM  FLDW..Inventory I WITH(NOLOCK)
JOIN Market.Merchandising.VehicleAdvertisement_Audit MVA WITH(NOLOCK) ON MVA.VehicleEntityID = I.InventoryID
                                                                    AND [MVA].[OwnerEntityID] = [I].[BusinessUnitID]
                                                                    AND [MVA].[VehicleEntityTypeID] = 1
                                                                    AND [MVA].[OwnerEntityTypeID] = 1
JOIN Market.Merchandising.Advertisement_Properties MAP WITH(NOLOCK) ON MAP.AdvertisementID = MVA.AdvertisementID
WHERE I.BusinessUnitID = @DealerID
AND I.InventoryType = 2  --used
AND I.InventoryActive = 1
AND MAP.Created IS NOT NULL
AND MAP.RowTypeID = 1
AND MVA.WasDelete = 0

SELECT @NumMerchandised = ISNULL(COUNT(IM.InventoryID), 0)
FROM  @InventoryMerchandised IM 

SELECT @SumDaysUntilAdCreated = ISNULL(SUM(DATEDIFF(DD,IM.ReceivedDate,IM.AdCreateDate)), 0)
FROM   @InventoryMerchandised IM 

IF(@NumMerchandised > 0)
	BEGIN
		SET @AvgDaysToCreateAd = ROUND((CAST(@SumDaysUntilAdCreated AS FLOAT)/ CAST(@NumMerchandised AS FLOAT)),1)
	END
ELSE
	BEGIN
		SET @AvgDaysToCreateAd = 0;
	END 

 
INSERT INTO @Results (
 	NumMerchandised,
 	SumDaysUntilAdCreated,
 	AvgDaysToCreateAd 
) VALUES (
	@NumMerchandised
	, @SumDaysUntilAdCreated
	, @AvgDaysToCreateAd
)

SELECT 
	NumMerchandised,
 	SumDaysUntilAdCreated,
 	AvgDaysToCreateAd 
FROM @Results

GO