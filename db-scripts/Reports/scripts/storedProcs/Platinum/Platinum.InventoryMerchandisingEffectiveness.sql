IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[InventoryMerchandisingEffectiveness]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[InventoryMerchandisingEffectiveness] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[InventoryMerchandisingEffectiveness] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[InventoryMerchandisingEffectiveness]
	@DealerID INT
AS

SET NOCOUNT ON

/* --------------------------------------------------------------------
 * 
 * $Id: Platinum.InventoryMerchandisingEffectiveness.sql,v 1.5 2010/02/10 21:54:24 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns simple metrics for "Merchandising Effectiveness".  Middle of the page on the PricingAdAppraisalEffectivenessReport.
 * 
 * Parameters
 * ----------
 *
 * @DealerID
 * 
 * Exceptions
 * ----------
 *
 *
 * JTM 7 Aug '09	Merchandising information for all reprice events during the period.
 * 
 * -------------------------------------------------------------------- */

DECLARE @Results TABLE (
	SoldStatusWithAd VARCHAR(20),
	PercentSoldOrNotWithAd FLOAT,
	PercentSoldWithAd FLOAT,
	PercentNotSoldWithAd FLOAT
)

DECLARE 
     @PeriodStart DATETIME
    ,@PeriodEnd DATETIME
    ,@Today DATETIME
	,@NumMerchandised INT
	,@NumSoldWithAd INT
	,@PercentSoldWithAd FLOAT
	,@PercentNotSoldWithAd FLOAT
		
SELECT  
	@Today = CAST(CONVERT(VARCHAR(10), GETDATE(), 101) AS DATETIME)    
	, @PeriodEnd = DATEADD(ww,-2,@Today)	--2 weeks ago
	, @PeriodStart = DATEADD(ww,-4,@PeriodEnd)  --4 weeks prior to PeriodEnd


DECLARE @InventoryMerchandisedOver21 TABLE (
	InventoryID INT NOT NULL,
	DealerID INT NOT NULL,
	AdCreateDate DATETIME,
	DealDate DATETIME 
)
	
INSERT INTO @InventoryMerchandisedOver21 (
	InventoryID, 
	DealerID, 
	AdCreateDate, 
	DealDate
)   
SELECT DISTINCT 
	I.InventoryID, 
	I.BusinessUnitID, 
	MVA.ValidFrom, 
	I.DeleteDt
FROM FLDW..Inventory I WITH(NOLOCK)
JOIN Market.Merchandising.VehicleAdvertisement_Audit MVA WITH(NOLOCK) ON MVA.VehicleEntityID = I.InventoryID
JOIN Market.Merchandising.Advertisement_Properties MAP WITH(NOLOCK) ON MAP.AdvertisementID = MVA.AdvertisementID
WHERE I.BusinessUnitID = @DealerID
AND I.InventoryType = 2  --used
AND MAP.Created IS NOT NULL
AND MAP.RowTypeID = 1
AND MVA.ValidFrom >= @PeriodStart
--	AND MVA.ValidUpTo > @PeriodEnd
AND MVA.WasDelete = 0
AND ((I.DeleteDt IS NULL AND I.AgeInDays > 21) 
	OR (I.DeleteDt IS NOT NULL AND DATEDIFF(DD, I.InventoryReceivedDate, I.DeleteDt) > 21))

SELECT @NumMerchandised = 
		COUNT(IM.InventoryID)
		FROM  @InventoryMerchandisedOver21 IM 

SELECT @NumSoldWithAd = (
	COUNT(IM.InventoryID)  )
	FROM   @InventoryMerchandisedOver21 IM 
	WHERE  IM.DealDate IS NOT NULL
	AND IM.AdCreateDate <= IM.DealDate
	AND DATEDIFF(DD,IM.AdCreateDate,IM.DealDate) <= 15	

IF (@NumMerchandised > 0)
	BEGIN
		SET @PercentSoldWithAd = ROUND((CAST(@NumSoldWithAd AS FLOAT)/ CAST(@NumMerchandised AS FLOAT))*100,0)
	END
ELSE
	BEGIN
		SET @PercentSoldWithAd = 0;
	END 
 
SET @PercentNotSoldWithAd = 100 - @PercentSoldWithAd


INSERT INTO @Results (
	SoldStatusWithAd,
	PercentSoldOrNotWithAd,
	PercentSoldWithAd,
	PercentNotSoldWithAd
) VALUES (
	'SoldWithAd'
	,@PercentSoldWithAd
	,@PercentSoldWithAd
	,@PercentNotSoldWithAd
)

INSERT INTO @Results (
	SoldStatusWithAd,
	PercentSoldOrNotWithAd,
	PercentSoldWithAd,
	PercentNotSoldWithAd
) VALUES (
	'UnsoldWithAd' 
	,@PercentNotSoldWithAd
	,@PercentSoldWithAd
	,@PercentNotSoldWithAd
)

SELECT 
	SoldStatusWithAd,
	PercentSoldOrNotWithAd,
	PercentSoldWithAd,
	PercentNotSoldWithAd
FROM @Results

GO