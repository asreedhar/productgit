IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[InventoryRepricingEffectiveness#Totals]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[InventoryRepricingEffectiveness#Totals] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[InventoryRepricingEffectiveness#Totals] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[InventoryRepricingEffectiveness#Totals]
	@DealerID INT
AS

SET NOCOUNT ON

/* --------------------------------------------------------------------
 * 
 * $Id: Platinum.InventoryRepricingEffectiveness#Totals.sql,v 1.5 2010/02/10 21:54:24 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns simple metrics for "Repricing Effectiveness".  Middle of the page on the PricingAdAppraisalEffectivenessReport.
 * 
 * Parameters
 * ----------
 *
 * @DealerID
 * 
 * Exceptions
 * ----------
 *
 * JTM 7 Aug '09	Repricing information for all reprice events during the period.
 * 
 * -------------------------------------------------------------------- */

DECLARE 
	@PeriodStart DATETIME
	, @PeriodEnd DATETIME

SELECT      
	@PeriodEnd = DATEADD(WW,-2,GETDATE())		--2 weeks ago
	, @PeriodStart = DATEADD(WW,-4,@PeriodEnd)  --4 weeks prior to PeriodEnd

DECLARE @Results TABLE (
	BusinessUnit VARCHAR(40) NOT NULL,
	PeriodStart DATETIME NOT NULL,
	PeriodEnd DATETIME NOT NULL,
	today DATETIME NOT NULL,
	AvgRepriceAmount DECIMAL(8,0) NULL,
	NumReprices INT NULL,
	RepricedUnits INT NULL,
	AvgNumRepricesUnit DECIMAL(8,2) NULL
);

WITH Header(BusinessUnitID, BusinessUnit, PeriodStart, PeriodEnd, today) AS (
	SELECT BU.BusinessUnitID
		, BU.BusinessUnit
		, CONVERT(VARCHAR(10), @PeriodStart,101)
		, CONVERT(VARCHAR(10), @PeriodEnd,101)
		, CONVERT(VARCHAR(10), GETDATE(),101)
	FROM IMT..BusinessUnit BU WITH(NOLOCK)
	WHERE BU.Active = 1
	AND BU.BusinessUnitTypeID = 4 --dealer
	AND BU.BusinessUnitID = @DealerID
		
), Metrics(BusinessUnitID, AvgRepriceAmount, NumReprices, RepricedUnits, AvgNumRepricesUnit) AS (
	SELECT 	
		I.BusinessUnitID
		, CAST(AVG((AE.[Value] - CAST(AE.Notes2 as int))) AS DECIMAL(8,0)) 'AvgRepriceAmount'
		, COUNT(*) 'NumReprices'
		, COUNT(DISTINCT I.InventoryID) 'RepricedUnits'
		, CAST(
			(
			CAST(COUNT(*)AS FLOAT)
			/
			CAST((COUNT(DISTINCT I.InventoryID))AS FLOAT)
			)
			AS DECIMAL(8,2)) 'AvgNumRepricesUnit'
	FROM IMT..Inventory I WITH(NOLOCK)
	JOIN IMT..VehicleSale vs WITH(NOLOCK) ON VS.InventoryID = I.InventoryID
	JOIN IMT..AIP_Event AE WITH(NOLOCK) ON AE.InventoryID = I.InventoryID
	JOIN IMT..Member M WITH(NOLOCK) ON M.[Login] = AE.CreatedBy
	WHERE I.BusinessUnitID = @DealerID
	AND (VS.DealDate IS NOT NULL AND VS.DealDate BETWEEN @PeriodStart AND @PeriodEnd)
	AND I.InventoryType = 2
	AND AE.AIP_EventTypeID = 5  --Reprice Event
	AND AE.RepriceSourceID IN (1,2,3)  -- 1=IMP, 2=eStock, 3=PINGII
	AND AE.AIP_EventTypeID IS NOT NULL 
	AND AE.[Value] <> AE.Notes2
	AND AE.Notes2 > '0'
	AND M.MemberType = 2
	AND (vs.SaleDescription = 'R'
		OR (vs.SaleDescription = 'W' AND (DATEDIFF(DD, I.InventoryReceivedDate, vs.DealDate) > 29 )))
	GROUP BY I.BusinessUnitID
)

INSERT INTO @Results (
	BusinessUnit,
	PeriodStart,
	PeriodEnd,
	today,
	AvgRepriceAmount,
	NumReprices,
	RepricedUnits,
	AvgNumRepricesUnit
)
SELECT H.BusinessUnit,
	H.PeriodStart,
	H.PeriodEnd,
	H.today,
	M.AvgRepriceAmount,
	M.NumReprices,
	M.RepricedUnits,
	M.AvgNumRepricesUnit
FROM Header H
LEFT JOIN Metrics M ON H.BusinessUnitID = M.BusinessUnitID

SELECT 
	BusinessUnit,
	PeriodStart AS '4wk_start',
	PeriodEnd AS '4wk_end',
	today,
	AvgRepriceAmount,
	NumReprices,
	RepricedUnits,
	AvgNumRepricesUnit
FROM @Results

GO