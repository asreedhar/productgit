
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[Storms]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[Storms] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[Storms] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[Storms]
	@DealerID INT
AS

SET NOCOUNT ON


declare @Extra BIT

SET @Extra = CASE WHEN @DealerID =  100215 THEN CAST(1 AS BIT) ELSE CAST(0 AS BIT) END 

SELECT  BusinessUnit, @Extra as 'Extra' FROM [IMT].dbo.BusinessUnit WHERE BusinessUnitID = @DealerID

