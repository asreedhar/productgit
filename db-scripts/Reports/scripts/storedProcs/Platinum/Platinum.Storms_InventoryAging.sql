
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[Storms#InventoryAging]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[Storms#InventoryAging] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[Storms#InventoryAging] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[Storms#InventoryAging]
	@DealerID INT
AS

-- MAK Only allow Sum the difference (difference between Book Value and Unit Cost) when the Book Value >0

SET NOCOUNT ON


DECLARE @InventoryBuckets TABLE (
	RangeID		INT NOT NULL,
	BucketName	VARCHAR(255) NOT NULL,
	Low			INT NOT NULL,
	High		INT NULL,
	Lights		INT NOT NULL,
	Target		INT NULL
)
IF @DealerID = 100215 BEGIN
INSERT INTO @InventoryBuckets (RangeID, BucketName, Low, High, Lights, Target) VALUES
	(1, 'Sweet Spot / Watch List (0-30 Days)', 0, 30, 7, 70)
INSERT INTO @InventoryBuckets (RangeID, BucketName, Low, High, Lights, Target) VALUES
	(2, 'Approaching the Retail Cliff (31-45 Days)', 31, 45, 7, 15)
INSERT INTO @InventoryBuckets (RangeID, BucketName, Low, High, Lights, Target) VALUES
	(3, 'On the Retail Cliff (46-60 Days)', 46, 60, 7, 10)
INSERT INTO @InventoryBuckets (RangeID, BucketName, Low, High, Lights, Target) VALUES
	(4, 'Off the Retail Cliff (61-75 Days)', 61, 75, 7, 5)
INSERT INTO @InventoryBuckets (RangeID, BucketName, Low, High, Lights, Target) VALUES
	(5, 'Off the Wholesale Cliff (75-90 Days)', 76, 90, 7, 0)
INSERT INTO @InventoryBuckets (RangeID, BucketName, Low, High, Lights, Target) VALUES
	(6, 'Antique! (91+ Days)', 90, NULL, 7, 0)
END ELSE BEGIN
INSERT INTO @InventoryBuckets (RangeID, BucketName, Low, High, Lights, Target) 
SELECT	RangeID,
		CASE	WHEN Low >= 60 THEN 'Off the Wholesale Cliff ('+Description+')'
				WHEN Low >= 50 THEN 'Off the Retail Cliff ('+Description+')'
				WHEN Low >= 40 THEN 'On the Retail Cliff ('+Description+')'
				WHEN Low >= 30 THEN 'Approaching the Retail Cliff ('+Description+')'
				WHEN Low <= 29 AND Lights = 3 THEN 'Watch List (Higher Risk Under '+CONVERT(VARCHAR, High+1)+' Days)'
				ELSE 'Sweet Spot ('+Description+' Green)'
		END,
		Low, High, Lights,
		CASE WHEN Low >= 60 THEN 0 WHEN Low >= 50 THEN 5 WHEN Low >= 40 THEN 10 WHEN Low >= 30 THEN 15 ELSE NULL END
FROM	IMT.dbo.GetInventoryBucketRanges(@DealerID, 4)
END
DECLARE @Inventory TABLE (
	InventoryID				INT,
	CurrentVehicleLight		INT,
	InventoryReceivedDate	DATETIME,
	UnitCost				INT,
	BookValue				INT,
	BookValueMinusUnitCost	INT
)
DECLARE @InventoryCount REAL
INSERT INTO @Inventory (InventoryID, CurrentVehicleLight, InventoryReceivedDate, UnitCost, BookValue,BookValueMinusUnitCost  )
SELECT	I.InventoryID, I.CurrentVehicleLight, I.InventoryReceivedDate, UnitCost, ISNULL(IBCV.Value,0.0),Case when ISNULL(IBCV.Value,0.0) >0 then ISNULL(IBCV.Value,0.0)-I.UnitCost else 0 end
FROM	FLDW.dbo.InventoryActive I
JOIN	IMT.dbo.DealerPreference DP on I.BusinessUnitID = DP.BusinessUnitID
LEFT JOIN IMT.dbo.InventoryBookoutCurrentValue IBCV on I.InventoryID = IBCV.InventoryID and DP.BookoutPreferenceId = IBCV.ThirdPartyCategoryID
WHERE	I.InventoryType = 2
AND		I.InventoryActive = 1
AND		I.BusinessUnitID = @DealerID
SELECT @InventoryCount = COALESCE(NULLIF(@@ROWCOUNT,0),1)
SELECT	IBR.RangeID,
		IBR.BucketName,
		TargetPercentage = IBR.Target,
		ActualPercentage = COUNT(*) / @InventoryCount,
		Units = COUNT(*),
		BookVersusCost =  SUM(BookValueMinusUnitCost)
FROM	@InventoryBuckets IBR
LEFT JOIN	@Inventory I ON
		CASE	WHEN DATEDIFF(DD,I.InventoryReceivedDate,GETDATE()) < 1 THEN 1 
				ELSE DATEDIFF(DD,I.InventoryReceivedDate,GETDATE())
		END BETWEEN IBR.Low AND isnull(IBR.High,9999) AND IBR.Lights & POWER(2,I.CurrentVehicleLight-1) = POWER(2,I.CurrentVehicleLight-1)
GROUP
BY		IBR.RangeID, IBR.BucketName, IBR.Target
ORDER
BY		IBR.RangeID desc