IF NOT EXISTS
	(SELECT *
	FROM sys.objects
	WHERE object_id = OBJECT_ID(N'[Platinum].[UnderstockingOverstocking#CurrentInventory]')
	AND type in (N'P', N'PC'))

EXECUTE('CREATE PROCEDURE [Platinum].[UnderstockingOverstocking#CurrentInventory] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[UnderstockingOverstocking#CurrentInventory] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[UnderstockingOverstocking#CurrentInventory]
	@DealerID INT
AS

SET NOCOUNT ON


DECLARE @Results TABLE
(
	LowerUnitCost int,
	CurrentInventory int,
	Units int,
	DaysSupply int,
	Suv INT,
	Sedan INT,
	Truck INT,
	Van INT,
	Coupe INT,
	Wagon INT,
	Convertible INT
)

DECLARE 
	@LowerUnitCostCount int,
	@CurrentInventoryCount int,
	@UnitsCount decimal(12,2),
	@DaysSupplyCount int,
	@SuvCount INT,
	@SedanCount INT,
	@TruckCount INT,
	@VanCount INT,
	@CoupeCount INT,
	@WagonCount INT,
	@ConvertibleCount INT

SELECT	@LowerUnitCostCount = CAST(LTRIM(RTRIM(SUBSTRING(Comments, CHARINDEX('UnitCostThresholdLower:', Comments) + 23, LEN(Comments)))) AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 5) AND (SetID = 14) AND (MetricID = 33)
SELECT	@UnitsCount			= CAST(LTRIM(RTRIM(SUBSTRING(Comments, CHARINDEX('Units:', Comments) + 6, CHARINDEX(',', Comments) - 7))) AS  decimal(12,2)) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 5) AND (SetID = 14) AND (MetricID = 33)
SELECT	@CurrentInventoryCount	= CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 5) AND (SetID = 14) AND (MetricID = 33)
SELECT	@DaysSupplyCount		= CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 27) AND (SetID = 14) AND (MetricID = 34)

SELECT	@SuvCount	=		CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 6) AND (SetID = 17) AND (MetricID = 36)
SELECT	@SedanCount =		CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 6) AND (SetID = 17) AND (MetricID = 35)
SELECT	@TruckCount =		CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 6) AND (SetID = 17) AND (MetricID = 37)
SELECT	@VanCount	=		CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 6) AND (SetID = 17) AND (MetricID = 39)
SELECT	@CoupeCount =		CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 6) AND (SetID = 17) AND (MetricID = 38)
SELECT	@WagonCount =       CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 6) AND (SetID = 17) AND (MetricID = 40)
SELECT	@ConvertibleCount = CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 6) AND (SetID = 17) AND (MetricID = 41)


INSERT INTO @Results VALUES 
(
	@LowerUnitCostCount,
	@CurrentInventoryCount,
	@UnitsCount,
	@DaysSupplyCount,
	@SuvCount,
	@SedanCount,
	@TruckCount,
	@VanCount,
	@CoupeCount,
	@WagonCount,
	@ConvertibleCount
)

SELECT * FROM @Results