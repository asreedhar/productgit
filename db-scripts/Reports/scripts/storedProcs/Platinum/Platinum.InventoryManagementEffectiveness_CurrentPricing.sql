IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[InventoryManagementEffectiveness#CurrentPricing]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[InventoryManagementEffectiveness#CurrentPricing] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[InventoryManagementEffectiveness#CurrentPricing] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[InventoryManagementEffectiveness#CurrentPricing]
	@DealerID INT
AS

SET NOCOUNT ON





Select 
	Cast(Measure as int) as Measure,'Pricing Not Updated' as Description
From
	EdgeScorecard..Measures
Where
	BusinessUnitID = @DealerID
	  And PeriodID = 7 
	  And SetID = 125
	  And MetricID = 245

UNION ALL

Select 
	Cast(Measure as int) as Measure,'Pricing Updated' as Description
From
	EdgeScorecard..Measures
Where
	BusinessUnitID = @DealerID
	  And PeriodID = 7 
	  And SetID = 125
	  And MetricID = 244
