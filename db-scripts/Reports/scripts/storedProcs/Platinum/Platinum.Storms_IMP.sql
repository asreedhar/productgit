
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[Storms#IMP]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[Storms#IMP] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[Storms#IMP] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[Storms#IMP]
	@DealerID INT
AS

SET NOCOUNT ON




DECLARE @Results TABLE
(
	DayThreshold int,
	TotalUnits INT,
	UnitsWithValidPlan INT,
	TotalAgedUnits INT,
	AgedUnitsWithValidPlan INT,
	WatchListUnitsWithValidPlan INT, 
	TotalWatchListUnits INT, 
	TotalPriced int,
	TotalNonPriced int
)

DECLARE 
	@DayThresholdCount int,
	@TotalUnitsCount INT,
	@UnitsWithValidPlanCount INT,
	@TotalAgedUnitsCount INT,
	@AgedUnitsWithValidPlanCount INT,
	@WatchListUnitsWithValidPlanCount INT, 
	@TotalWatchListUnitsCount INT, 
	@TotalPricedCount int,
	@TotalNonPricedCount int



SELECT	@DayThresholdCount =  CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 125) AND (MetricID = 246)
SELECT	@TotalUnitsCount =  CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 142) AND (MetricID = 279)
SELECT	@UnitsWithValidPlanCount =  CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 142) AND (MetricID = 281)
SELECT	@TotalAgedUnitsCount =  CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 142) AND (MetricID = 282)
SELECT	@AgedUnitsWithValidPlanCount =  CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 142) AND (MetricID = 283)
SELECT	@WatchListUnitsWithValidPlanCount =  CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 142) AND (MetricID = 290)
SELECT	@TotalWatchListUnitsCount =  CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 142) AND (MetricID = 288)

SELECT	@TotalPricedCount =  CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 125) AND (MetricID = 244)
SELECT	@TotalNonPricedCount =  CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 125) AND (MetricID = 245)




INSERT INTO @Results VALUES 
(
	@DayThresholdCount,
	@TotalUnitsCount,
	@UnitsWithValidPlanCount,
	@TotalAgedUnitsCount,
	@AgedUnitsWithValidPlanCount,
	@WatchListUnitsWithValidPlanCount, 
	@TotalWatchListUnitsCount, 
	@TotalPricedCount,
	@TotalNonPricedCount
)

SELECT * FROM @Results