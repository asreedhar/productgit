IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[InventoryManagementEffectiveness#CurrentPricingTotals]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[InventoryManagementEffectiveness#CurrentPricingTotals] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[InventoryManagementEffectiveness#CurrentPricingTotals] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[InventoryManagementEffectiveness#CurrentPricingTotals]
	@DealerID INT
AS

SET NOCOUNT ON

/* --------------------------------------------------------------------
 * 
 * $Id: Platinum.InventoryManagementEffectiveness_CurrentPricingTotals.sql,v 1.6 2010/02/10 21:54:24 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns summary metrics for CurrentPricingEffectiveness
 * 
 * Parameters
 * ----------
 *
 * @DealerID
 * 
 * Exceptions
 * ----------
 *
 *  BYF Aug. 26, 2009 Update to proc to add more metrics for Pricing Ad Appraisal Effectiveness report.
 * -------------------------------------------------------------------- */

DECLARE @Results TABLE (
	PercentUpdated int,
	DayThreshold int,
	PriceStatus varchar(20),
	PercentageUpdatedOrNot int,
	PercentNotUpdated int
)

DECLARE 
	@Updated int,
	@NotUpdated int,
	@PercentUpdated int,
	@DayThreshold int,
	@PercentNotUpdated int

SELECT
	@Updated = Cast(Measure as int) 
FROM EdgeScorecard..Measures 
WHERE BusinessUnitID = @DealerID And PeriodID = 7 And SetID = 125 And MetricID = 244

SELECT @NotUpdated = Cast(Measure as int) 
FROM EdgeScorecard..Measures 
WHERE BusinessUnitID = @DealerID And PeriodID = 7 And SetID = 125 And MetricID = 245

SET @PercentUpdated = 
	CASE WHEN @Updated > 0 or @NotUpdated > 0 
		THEN Cast (( ROUND( @Updated / cast((@Updated + @NotUpdated ) as decimal(12,2)) , 2 ) ) * 100  as int ) 
		ELSE 0 
	END

SET @PercentNotUpdated = 100 - @PercentUpdated

SELECT 
	@DayThreshold = Cast(Measure as int) 
	FROM EdgeScorecard..Measures 
	WHERE BusinessUnitID = @DealerID And PeriodID = 7 And SetID = 125 And MetricID = 246

INSERT INTO @Results (
	PercentUpdated,
	DayThreshold,
	PriceStatus,
	PercentageUpdatedOrNot,
	PercentNotUpdated
) VALUES (
	@PercentUpdated,
	@DayThreshold,
	'PriceUpdated',
	@PercentUpdated,
	@PercentNotUpdated
)

INSERT INTO @Results (
	PercentUpdated,
	DayThreshold,
	PriceStatus,
	PercentageUpdatedOrNot,
	PercentNotUpdated
) VALUES (
	@PercentUpdated,
	@DayThreshold,
	'PriceNotUpdated',
	@PercentNotUpdated,
	@PercentNotUpdated
)

SELECT 
	PercentUpdated,
	DayThreshold,
	PriceStatus,
	PercentageUpdatedOrNot,
	PercentNotUpdated
FROM @Results