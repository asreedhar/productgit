IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[TradeInAppraisalEffectiveness]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[TradeInAppraisalEffectiveness] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[TradeInAppraisalEffectiveness] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[TradeInAppraisalEffectiveness]
	@DealerID INT
AS

SET NOCOUNT ON


/* --------------------------------------------------------------------
 * 
 * Platinum.TradeInAppraisalEffectiveness.sql
 * 
 * Summary
 * -------
 * 
 * For "Trade-in Appraisal Effectiveness" section of the new "Platinum Redux" reports.
 * 
 * Parameters
 * ----------
 *
 * @DealerID
 * 
 * Exceptions
 * ----------
 *
 * JTM August 6, 2009  
 * SVU April 23, 2010
 * -------------------------------------------------------------------- */

DECLARE @Results TABLE (
	TimePeriod VARCHAR(20) NOT NULL,
	DealerName VARCHAR(255) NOT NULL,
	NumCompletedAppraisals INT NULL,  --Appraisals completed using FirstLook  
	TradedUnits DECIMAL(12,0) NULL,  
	AnalyzedUnits DECIMAL(12,0) NULL,
	PercentTradesAnalyzed INT NULL,
	ClosingRate DECIMAL(12,2) NULL,
	NonClosingRate DECIMAL(12,2) NULL,
	ImmedWhsaleGross DECIMAL(12,2) NULL,  --same as 'flip profit'
	MoreProfitable BIT NULL,
	HowMuchMoreProfitable DECIMAL(12,2) NULL,
	SellsFaster BIT NULL,
	HowMuchFaster DECIMAL(12,2) NULL,
	NumCreatedAppraisals INT NULL
)

DECLARE
	@DealerName VARCHAR(255),
	@TradeInPercent4Weeks DECIMAL(12,2),  --% of traded units that were analyzed w/ FL
	@OneWeekCompletedAppraisals INT,  --Appraisals completed using FirstLook  
	@FourWeekUnits DECIMAL(12,0),  --Units traded in
	@FourWeekAnalyzedUnits DECIMAL(12,0),  --Units analysed in FirstLook
	@FourWeekClosingRate DECIMAL(12,2),
	@EightWeekClosingRate DECIMAL(12,2),
	@FourWeekNonClosingRate DECIMAL(12,2),
	@EightWeekNonClosingRate DECIMAL(12,2),
	@FourWeekFlipProfit DECIMAL(12,2),
	@EightWeekFlipProfit DECIMAL(12,2),
	@FourWeekAnalyzedGross DECIMAL(12,2),
	@FourWeekNonAnalyzedGross DECIMAL(12,2),
	@FourWeekAnalyzedDays DECIMAL(12,2),
	@FourWeekNonAnalyzedDays  DECIMAL(12,2),
	@FourWeekMoreProfitable BIT,
	@FourWeekHowMuchMoreProfitable DECIMAL(12,2),
	@FourWeekSellsFaster BIT,
	@FourWeekHowMuchFaster DECIMAL(12,2),
	@OneWeekCreatedAppraisals INT

SET @DealerName = (
		SELECT BU.BusinessUnit
		FROM    [IMT].dbo.BusinessUnit BU WITH(NOLOCK)
		WHERE 	BusinessUnitID = @DealerID)

SET @OneWeekCompletedAppraisals = (
	SELECT	CAST(SUBSTRING(Comments,1,PATINDEX('% of %',Comments)) AS INT)
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 1
		AND SetID = 4
		AND MetricID = 11)
		
SET @OneWeekCreatedAppraisals = (
	SELECT	CAST(SUBSTRING(Comments, PATINDEX('% of %', Comments)+3, LEN(Comments)-(PATINDEX('% of %', Comments)+2)) AS INT) CreatedAppraisals
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 1
		AND SetID = 4
		AND MetricID = 11)
		
SET @TradeInPercent4Weeks = (
	SELECT	Measure
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 2
		AND SetID = 2
		AND MetricID = 2)

SET @FourWeekUnits = (
	SELECT	Measure
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 2
		AND SetID = 2
		AND MetricID = 145)

SET @FourWeekAnalyzedUnits = (
	SELECT	Measure
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 2
		AND SetID = 2
		AND MetricID = 144)

SET @FourWeekClosingRate = (
	SELECT	Measure
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 2
		AND SetID = 1
		AND MetricID = 1)

SET @FourWeekNonClosingRate = (1 - @FourWeekClosingRate)

SET @EightWeekClosingRate = (
	SELECT	Measure
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 26
		AND SetID = 1
		AND MetricID = 1)

SET @EightWeekNonClosingRate = (1 - @EightWeekClosingRate)

SET @FourWeekFlipProfit = (
	SELECT	Measure
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 2
		AND SetID = 7
		AND MetricID = 18)

SET @EightWeekFlipProfit = (
	SELECT	Measure
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 26
		AND SetID = 7
		AND MetricID = 18)

SET		@FourWeekAnalyzedGross = (
	SELECT	Measure
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 2
		AND SetID = 2
		AND MetricID = 3)

SET		@FourWeekNonAnalyzedGross = (
	SELECT	Measure
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 2
		AND SetID = 2
		AND MetricID = 6)

SET		@FourWeekAnalyzedDays = (
	SELECT	Measure
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 2
		AND SetID = 2
		AND MetricID = 4)

SET		@FourWeekNonAnalyzedDays = (
	SELECT	Measure
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 2
		AND SetID = 2
		AND MetricID = 7)

SET	@FourWeekMoreProfitable = (CASE WHEN (@FourWeekAnalyzedGross - @FourWeekNonAnalyzedGross) > 100 THEN 1 ELSE 0 END)

SET	@FourWeekHowMuchMoreProfitable = (@FourWeekAnalyzedGross - @FourWeekNonAnalyzedGross)

SET	@FourWeekSellsFaster = (CASE WHEN @FourWeekAnalyzedDays = 0 THEN 0 WHEN ((@FourWeekNonAnalyzedDays/@FourWeekAnalyzedDays)*100) > 110 THEN 1 ELSE 0 END)

SET	@FourWeekHowMuchFaster = (@FourWeekNonAnalyzedDays-@FourWeekAnalyzedDays)

INSERT INTO @Results (
	TimePeriod,
	DealerName,
	NumCompletedAppraisals,  --Appraisals completed using FirstLook  
	TradedUnits,  
	AnalyzedUnits,
	PercentTradesAnalyzed,
	ClosingRate,
	NonClosingRate,
	ImmedWhsaleGross,  --same as 'flip profit'
	MoreProfitable,
	HowMuchMoreProfitable,
	SellsFaster,
	HowMuchFaster,
	NumCreatedAppraisals
) VALUES (
	'OneWeek',
	@DealerName,
	@OneWeekCompletedAppraisals,    
	NULL,  
	NULL,
	NULL,
	NULL,
	NULL,  
	NULL,  
	NULL,  
	NULL,  
	NULL,
	NULL,
	@OneWeekCreatedAppraisals
)

INSERT INTO @Results (
	TimePeriod,
	DealerName,
	NumCompletedAppraisals,  --Appraisals completed using FirstLook  
	TradedUnits,  
	AnalyzedUnits,
	PercentTradesAnalyzed,
	ClosingRate,
	NonClosingRate,
	ImmedWhsaleGross,  --same as 'flip profit'
	MoreProfitable,
	HowMuchMoreProfitable,
	SellsFaster,
	HowMuchFaster
) VALUES (
	'FourWeeks',
	@DealerName,
	NULL,
	@FourWeekUnits,  --Units traded in
	@FourWeekAnalyzedUnits,  --Of units traded in, those analyzed in FL
	CAST(@TradeInPercent4Weeks*100 AS INT), --% of traded units that were analyzed w/ FL
	@FourWeekClosingRate,
	@FourWeekNonClosingRate,
	@FourWeekFlipProfit,  --same as 'ImmedWhlsaleGross'
	@FourWeekMoreProfitable,
	@FourWeekHowMuchMoreProfitable,
	@FourWeekSellsFaster,
	@FourWeekHowMuchFaster
)

INSERT INTO @Results (
	TimePeriod,
	DealerName,
	NumCompletedAppraisals,  --Appraisals completed using FirstLook  
	TradedUnits,  
	AnalyzedUnits,
	PercentTradesAnalyzed,
	ClosingRate,
	NonClosingRate,
	ImmedWhsaleGross,  --same as 'flip profit'
	MoreProfitable,
	HowMuchMoreProfitable,
	SellsFaster,
	HowMuchFaster
) VALUES (
	'EightWeeks',
	@DealerName,
	NULL,
	NULL,
	NULL,
	NULL,
	@EightWeekClosingRate,
	@EightWeekNonClosingRate,
	@EightWeekFlipProfit,  --same as 'ImmedWhlsaleGross'
	NULL,
	NULL,
	NULL,
	NULL
)

SELECT TimePeriod,
	DealerName,
	NumCompletedAppraisals,  --Appraisals completed using FirstLook  
	TradedUnits,  
	AnalyzedUnits,
	PercentTradesAnalyzed,
	ClosingRate,
	NonClosingRate,
	ImmedWhsaleGross,  --same as 'flip profit'
	MoreProfitable,
	HowMuchMoreProfitable,
	SellsFaster,
	HowMuchFaster,
	NumCreatedAppraisals
FROM @Results


GO
