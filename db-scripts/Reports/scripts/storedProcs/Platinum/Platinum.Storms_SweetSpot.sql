
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[Storms#SweetSpot]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[Storms#SweetSpot] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[Storms#SweetSpot] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[Storms#SweetSpot]
	@DealerID INT
AS

SET NOCOUNT ON

DECLARE @Results TABLE
(
	DaysInInventory varchar(100),
	Target INT,
	Actual INT,
	ActualUnits INT,
	UnderAboveWater INT
)


DECLARE
	@DaysInInventory varchar(100),
	@Target INT ,
	@Actual INT,
	@ActualUnits INT,
	@UnderAboveWater INT,
	@TotalUnits int
	
select @TotalUnits =  CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 148) AND (MetricID = 307)


set @DaysInInventory = 'Watch List (Higher Risk Under 30 Days)'
set @Target = NULL
select @Actual = 	CASE WHEN @TotalUnits > 0 THEN Cast (( ROUND( CAST(Measure AS INT)   / cast(@TotalUnits as decimal(12,2)) , 2 ) ) * 100  as int ) ELSE 0 END FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 149) AND (MetricID = 309) 
select @ActualUnits = CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 149) AND (MetricID = 309)
select @UnderAboveWater = CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 149) AND (MetricID = 310)
INSERT INTO @Results VALUES (@DaysInInventory,@Target,@Actual,@ActualUnits,@UnderAboveWater)


set @DaysInInventory = 'Sweet Spot (0-29 Days Green)'
set @Target = NULL
select @Actual = 	CASE WHEN @TotalUnits > 0 THEN Cast (( ROUND( CAST(Measure AS INT)   / cast(@TotalUnits as decimal(12,2)) , 2 ) ) * 100  as int ) ELSE 0 END FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 150) AND (MetricID = 311) 
select @ActualUnits = CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 150) AND (MetricID = 311)
select @UnderAboveWater = CAST(Measure AS INT) FROM EdgeScorecard.dbo.Measures WHERE (BusinessUnitID = @DealerID) AND (PeriodID = 7) AND (SetID = 150) AND (MetricID = 312)
INSERT INTO @Results VALUES (@DaysInInventory,@Target,@Actual,@ActualUnits,@UnderAboveWater)



SELECT * FROM @Results