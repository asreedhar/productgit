
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[WeeklyTradeInAppraisal]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[WeeklyTradeInAppraisal] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[WeeklyTradeInAppraisal] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[WeeklyTradeInAppraisal]
	@DealerID INT
AS

SET NOCOUNT ON

DECLARE @Results TABLE (
	DealerName VARCHAR(255) NOT NULL,
	OneWeekAppraisals INT NULL,
	TradeInPercent4Weeks DECIMAL(12,2),
	FourWeekUnits DECIMAL(12,2),
	FourWeekAnalyzedUnits DECIMAL(12,2),
	FourWeekClosingRate DECIMAL(12,2),
	EightWeekClosingRate DECIMAL(12,2),
	FourWeekFlipProfit DECIMAL(12,2),
	EightWeekFlipProfit DECIMAL(12,2),
	MoreProfitable BIT,
	HowMuchMoreProfitable DECIMAL(12,2),
	SellsFaster BIT,
	HowMuchFaster DECIMAL(12,2)
)

INSERT INTO @Results (DealerName) 
SELECT  BusinessUnit
FROM    [IMT].dbo.BusinessUnit WITH(NOLOCK)
WHERE 	BusinessUnitID = @DealerID

UPDATE @Results SET OneWeekAppraisals = (
	SELECT	CAST(SUBSTRING(Comments,1,PATINDEX('% of %',Comments)) AS INT)
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 1
		AND SetID = 4
		AND MetricID = 11
)

UPDATE @Results SET TradeInPercent4Weeks = (
	SELECT	Measure
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 2
		AND SetID = 2
		AND MetricID = 2
)

UPDATE @Results SET FourWeekUnits = (
	SELECT	Measure
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 2
		AND SetID = 2
		AND MetricID = 145
)

UPDATE @Results SET FourWeekAnalyzedUnits = (
	SELECT	Measure
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 2
		AND SetID = 2
		AND MetricID = 144
)

UPDATE @Results SET FourWeekClosingRate = (
	SELECT	Measure
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 2
		AND SetID = 1
		AND MetricID = 1
)

UPDATE @Results SET EightWeekClosingRate = (
	SELECT	Measure
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 26
		AND SetID = 1
		AND MetricID = 1
)

UPDATE @Results SET FourWeekFlipProfit = (
	SELECT	Measure
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 2
		AND SetID = 7
		AND MetricID = 18
)

UPDATE @Results SET EightWeekFlipProfit = (
	SELECT	Measure
	FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
	WHERE	BusinessUnitID = @DealerID
		AND PeriodID = 26
		AND SetID = 7
		AND MetricID = 18
)

DECLARE @FourWeekAnalyzedGross DECIMAL(12,2),
	@FourWeekNonAnalyzedGross DECIMAL(12,2),
	@FourWeekAnalyzedDays DECIMAL(12,2),
	@FourWeekNonAnalyzedDays  DECIMAL(12,2)

SELECT	@FourWeekAnalyzedGross = Measure
FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
WHERE	BusinessUnitID = @DealerID
	AND PeriodID = 2
	AND SetID = 2
	AND MetricID = 3

SELECT	@FourWeekNonAnalyzedGross = Measure
FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
WHERE	BusinessUnitID = @DealerID
	AND PeriodID = 2
	AND SetID = 2
	AND MetricID = 6

SELECT	@FourWeekAnalyzedDays = Measure
FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
WHERE	BusinessUnitID = @DealerID
	AND PeriodID = 2
	AND SetID = 2
	AND MetricID = 4

SELECT	@FourWeekNonAnalyzedDays = Measure
FROM	[EdgeScorecard].dbo.Measures WITH(NOLOCK)
WHERE	BusinessUnitID = @DealerID
	AND PeriodID = 2
	AND SetID = 2
	AND MetricID = 7

UPDATE @Results SET
	MoreProfitable = CASE WHEN (@FourWeekAnalyzedGross - @FourWeekNonAnalyzedGross) > 100 THEN 1 ELSE 0 END,
	HowMuchMoreProfitable = @FourWeekAnalyzedGross - @FourWeekNonAnalyzedGross,
	SellsFaster = CASE WHEN @FourWeekAnalyzedDays = 0 THEN 0 WHEN ((@FourWeekNonAnalyzedDays/@FourWeekAnalyzedDays)*100) > 110 THEN 1 ELSE 0 END,
	HowMuchFaster = @FourWeekNonAnalyzedDays-@FourWeekAnalyzedDays

SELECT
	DealerName,
	OneWeekAppraisals,
	TradeInPercent4Weeks,
	FourWeekUnits,
	FourWeekAnalyzedUnits,
	FourWeekClosingRate,
	EightWeekClosingRate,
	FourWeekFlipProfit,
	EightWeekFlipProfit,
	MoreProfitable,
	HowMuchMoreProfitable,
	SellsFaster,
	HowMuchFaster
FROM @Results

GO
