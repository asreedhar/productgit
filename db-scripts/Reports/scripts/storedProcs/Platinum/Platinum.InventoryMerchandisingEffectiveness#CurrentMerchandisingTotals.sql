IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[InventoryMerchandisingEffectiveness#CurrentMerchandisingTotals]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[InventoryMerchandisingEffectiveness#CurrentMerchandisingTotals] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[InventoryMerchandisingEffectiveness#CurrentMerchandisingTotals] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[InventoryMerchandisingEffectiveness#CurrentMerchandisingTotals]
	@DealerID INT
	--, @LowerInventoryAgeLimit INT = 21
AS

SET NOCOUNT ON

/* --------------------------------------------------------------------
 * 
 * $Id: Platinum.InventoryMerchandisingEffectiveness#CurrentMerchandisingTotals.sql,v 1.5 2010/02/10 21:54:24 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Returns summary metrics for "Merchandising Effectiveness"
 * 
 * Parameters
 * ----------
 *
 * @DealerID
 * 
 * Exceptions
 * ----------
 *
 *  BYF Aug. 26, 2009 	Create Proc, extracted and cleaned sql from attachement in Case 6667
 *  					Not using @LowerInventoryAgeLimit because InventoryManagementEffectiveness#CurrentPricingTotals
 * 						Numbers are (hard)coded in EdgeScorecard
 * -------------------------------------------------------------------- */

DECLARE @Results TABLE (
	MerchandisedStatus varchar(20),
	PercentageMerchandisedOrNot int,
	PercentMerchandised int,
	PercentNotMerchandised int
)

DECLARE 
	@Today DATETIME,
	@PercentMerchandised INT,
	@PercentNotMerchandised INT

SET @Today = CAST(CONVERT(VARCHAR(10), GETDATE(), 101) AS DATETIME)

/*
IF (@LowerInventoryAgeLimit < 0)
BEGIN
	SET @LowerInventoryAgeLimit = 0
END
*/

SELECT @PercentMerchandised = 
	CASE 
		WHEN COUNT(I.InventoryID) = 0 THEN 0
		ELSE ROUND(
			CAST(COUNT(A.InventoryID) AS FLOAT) / CAST(COUNT(I.InventoryID) AS FLOAT)
			, 2
		) * 100
	END
FROM 	[FLDW]..InventoryActive I WITH(NOLOCK)
	LEFT JOIN (
		SELECT 	MVA.VehicleEntityID AS InventoryID,
			MVA.OwnerEntityID	
		FROM 	[Market].Merchandising.VehicleAdvertisement_Audit MVA WITH(NOLOCK)
		JOIN 	[Market].Merchandising.Advertisement_Properties MAP WITH(NOLOCK)
				ON MAP.AdvertisementID = MVA.AdvertisementID
		WHERE 	MVA.ValidUpTo > @Today
			AND MVA.WasDelete = 0
			AND MAP.Created IS NOT NULL
			AND MAP.RowTypeID = 1
			AND MVA.VehicleEntityTypeID = 1 --Inventory
			AND MVA.OwnerEntityTypeID = 1	-- Dealer
			AND MVA.OwnerEntityID = @DealerID
	) A ON I.BusinessUnitID = A.OwnerEntityID
		AND I.InventoryID = A.InventoryID
	
WHERE 	I.BusinessUnitID = @DealerID
	AND I.InventoryType = 2  --used
	AND I.AgeInDays > 21 --@LowerInventoryAgeLimit
	AND I.DeleteDt IS NULL

SET @PercentNotMerchandised = 100 - @PercentMerchandised

INSERT INTO @Results (
	MerchandisedStatus,
	PercentageMerchandisedOrNot,
	PercentMerchandised,
	PercentNotMerchandised
) VALUES (
	'AdUpdated'
	, @PercentMerchandised
	, @PercentMerchandised
	, @PercentNotMerchandised
)

INSERT INTO @Results (
	MerchandisedStatus,
	PercentageMerchandisedOrNot,
	PercentMerchandised,
	PercentNotMerchandised
) VALUES (
	'AdNotUpdated' 
	, @PercentNotMerchandised
	, @PercentMerchandised
	, @PercentNotMerchandised
)

SELECT 
	MerchandisedStatus,
	PercentageMerchandisedOrNot,
	PercentMerchandised,
	PercentNotMerchandised
FROM @Results

