
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Platinum].[WeeklyTradeInAppraisal#ClosingRateChart]') AND type in (N'P', N'PC'))
EXECUTE('CREATE PROCEDURE [Platinum].[WeeklyTradeInAppraisal#ClosingRateChart] AS SELECT 1')
GO

GRANT EXECUTE ON [Platinum].[WeeklyTradeInAppraisal#ClosingRateChart] TO [StoredProcedureUser]
GO

ALTER PROCEDURE [Platinum].[WeeklyTradeInAppraisal#ClosingRateChart]
	@DealerID INT
AS

SET NOCOUNT ON


DECLARE @Results TABLE (
	NumberOfWeeks INT,
	ClosingRatePct DECIMAL(12,2)
)

INSERT INTO @Results (NumberOfWeeks, ClosingRatePct)
SELECT	4, Measure
FROM	[EdgeScorecard].dbo.Measures
WHERE	BusinessUnitID = @DealerID
	AND PeriodID = 2
	AND SetID = 1
	AND MetricID = 1

INSERT INTO @Results (NumberOfWeeks, ClosingRatePct)
SELECT	8, Measure
FROM	[EdgeScorecard].dbo.Measures
WHERE	BusinessUnitID = @DealerID
	AND PeriodID = 26
	AND SetID = 1
	AND MetricID = 1

SELECT * FROM @Results

GO
