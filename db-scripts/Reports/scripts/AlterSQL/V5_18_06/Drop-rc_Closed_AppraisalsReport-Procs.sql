IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_ClosedAppraisalsReport_Open]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[rc_ClosedAppraisalsReport_Open]

GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[rc_ClosedAppraisalsReport_Closed]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[rc_ClosedAppraisalsReport_Closed]

GO