
CREATE CLUSTERED INDEX [IX_dbo_VehicleRedistribution__BusinessUnitID_PriorBusinessUnitID] ON [dbo].[VehicleRedistribution] 
(
	[BusinessUnitID] ASC,
	[PriorBusinessUnitID] ASC
)
