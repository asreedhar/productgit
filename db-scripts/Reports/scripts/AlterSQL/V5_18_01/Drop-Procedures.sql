
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Support].[AuctionTransactions#Data]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Support].[AuctionTransactions#Data]
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Support].[AuctionTransactions#Summary]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Support].[AuctionTransactions#Summary]
GO
