
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SystemUsageMetricsRuns]') 
and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE  [dbo].[SystemUsageMetricsRuns] 
GO
 
CREATE TABLE [dbo].[SystemUsageMetricsRuns](
	[SystemUsageMetricsRunID] [int] IDENTITY(1,1) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[StartDate] [smalldatetime] NOT NULL,
	[EndDate] [smalldatetime] NOT NULL,
	[IsScheduledRun] [bit] NOT NULL CONSTRAINT [DF_SystemUsageMetricsRuns_IsScheduledRun]  DEFAULT ((0)),
	[IsSuccessful] [bit] NULL,
	[ErrorID] [int] NULL,
	[ErrorMessage] [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_SystemUsageMetricsRuns] PRIMARY KEY CLUSTERED 
(
	[SystemUsageMetricsRunID] ASC
) 
)  

GO
 