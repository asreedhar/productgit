 

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SystemUsageMetricsForSalesForce]') 
and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE  [dbo].[SystemUsageMetricsForSalesForce] 
GO
CREATE TABLE [dbo].[SystemUsageMetricsForSalesForce](
	[SystemUsageMetricDataID] [int] IDENTITY(1,1) NOT NULL,
	[SystemUsageMetricRunID] [int] NULL,
	[BusinessUnitID] [int] NOT NULL,
	[BusinessUnit] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	[NumberOfInventoryItems] [int] NULL,
	[NumberOfRetailInventoryItems] [int] NULL,
	[HasPINGII] [bit] NOT NULL CONSTRAINT [DF__SystemUsa__HasPI__6FB49575]  DEFAULT ((0)),
	[HasAcceleratorPackage] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__SystemUsa__HasAc__70A8B9AE]  DEFAULT ((0)),
	[NumberOfItemsPINGED] [int] NULL,
	[PriceConfirmCount] [int] NULL,
	[TotalDMSChangeCount] [int] NULL,
	[TotalRepriceCount] [int] NULL,
	[NumberRepricedIMP] [int] NULL,
	[NumberRepricedeStock] [int] NULL,
	[NumberRepricedPING] [int] NULL,
	[NumberInventoryItemsShopped] [int] NULL,
	[NumberInventoryItemsPowered] [int] NULL,
	[NumberInventoryItemsTMVed] [int] NULL,
	[WBTurnedOn] [bit] NOT NULL CONSTRAINT [DF__SystemUsa__WBTur__719CDDE7]  DEFAULT ((0)),
	[MADTurnedOn] [bit] NOT NULL CONSTRAINT [DF__SystemUsa__MADTu__72910220]  DEFAULT ((0)),
	[AcceleratorStore] [bit] NOT NULL CONSTRAINT [DF__SystemUsa__Accel__73852659]  DEFAULT ((0)),
	[HasEDTDescriptionProvider] [bit] NOT NULL CONSTRAINT [DF__SystemUsa__HasED__74794A92]  DEFAULT ((0)),
	[EDTDescripton] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[NumberInternetDescriptions] [int] NULL,
	[NumberInternetDescriptionsCreated] [int] NULL,
	[NumberInternetDescriptionsEdited] [int] NULL,
	[NumberClosingNotes] [int] NULL,
	[NumberAppraisals] [int] NULL,
	[NumberAppraisalValues] [int] NULL,
	[NumberAppraisalForms] [int] NULL,
	[NumberAppraisalsPINGed] [int] NULL,
	[NumberMADSubscriptions] [int] NULL,
 CONSTRAINT [PK_SystemUsageMetricsForSalesForce] PRIMARY KEY CLUSTERED 
(
	[SystemUsageMetricDataID] ASC
) 
) ON [PRIMARY]

GO
 
ALTER TABLE [dbo].[SystemUsageMetricsForSalesForce]  WITH CHECK ADD  CONSTRAINT [FK_SystemUsageMetricsForSalesForce_SystemUsageMetricsRuns] FOREIGN KEY([SystemUsageMetricRunID])
REFERENCES [dbo].[SystemUsageMetricsRuns] ([SystemUsageMetricsRunID])
GO