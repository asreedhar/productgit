
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].VehicleRedistribution') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.VehicleRedistribution
GO

CREATE TABLE [dbo].[VehicleRedistribution](
	[RowNum] [int] NULL,
	[NumOfRecentTrades] [int] NULL,
	[ParentID] [int] NULL,
	[StockNumber] [varchar](15) NULL,
	[InventoryID] [int] NULL,
	[UnitCost] [decimal](9, 2) NULL,
	[UnitCostBucket] [int] NULL,
	[BusinessUnitID] [int] NULL,
	[VehicleID] [int] NULL,
	[VehicleDescription] [varchar](50) NULL,
	[PriorBusinessUnitID] [int] NULL,
	[CurrentLocationBusinessUnitID] [int] NULL,
	[CurrentUnitCost] [decimal](9, 2) NULL,
	[CurrentUnitCostBucket] [int] NULL,
	[CurrentMileageReceived] [int] NULL,
	[CurrentMileageReceivedBucket] [int] NULL,
	[CurrentVehicleLight] [varchar](10) NULL,
	[Dealership] [varchar](50) NULL,
	[AgeInDays] [int] NULL,
	[DaysToSale] [int] NULL,
	[DaysInStock] [int] NULL,
	[VehicleLight] [varchar](10) NULL,
	[MileageReceived] [int] NULL,
	[MileageReceivedBucket] [int] NULL,
	[Color] [varchar](50) NULL,
	[InventoryReceivedDate] [smalldatetime] NULL,
	[DeleteDt] [smalldatetime] NULL,
	[InventoryActive] [tinyint] NULL,
	[PriorRowNum] [int] NULL,
	[PriorInventoryID] [int] NULL,
	[PriorUnitCost] [decimal](9, 2) NULL,
	[PriorDealership] [varchar](50) NULL,
	[PriorAgeInDays] [int] NULL,
	[PriorDaysToSale] [int] NULL,
	[PriorDaysInStock] [int] NULL,
	[PriorInventoryReceivedDate] [smalldatetime] NULL,
	[PriorDeleteDt] [smalldatetime] NULL,
	[PriorInventoryActive] [tinyint] NULL
)
GO

GRANT INSERT, SELECT, UPDATE, DELETE ON [dbo].[VehicleRedistribution] TO [StoredProcedureUser]
GO
