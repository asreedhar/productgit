
IF EXISTS (select * from sys.messages where message_id = 50204)
	EXEC sp_dropmessage @msgnum = 50204
GO
 
EXEC sp_addmessage 50204, 16,
   N'Data Error! 
   Expected %s %d row(s) but got %d rows!';
GO
