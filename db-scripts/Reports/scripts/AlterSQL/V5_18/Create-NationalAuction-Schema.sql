
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME='NationalAuction')
EXEC('CREATE SCHEMA NationalAuction')
GO

CREATE ROLE [NationalAuctionUser]
GO

-- exec sp_addrolemember @rolename='NationalAuctionUser', @membername='firstlook'
