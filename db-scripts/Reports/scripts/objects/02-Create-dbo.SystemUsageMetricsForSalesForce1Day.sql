if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SystemUsageMetricsForSalesForce1Day]') 
and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE  [dbo].[SystemUsageMetricsForSalesForce1Day] 
GO

/****** Object:  Table [dbo].[SystemUsageMetricsForSalesForce1Day]    Script Date: 03/23/2012 11:17:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SystemUsageMetricsForSalesForce1Day](
	[SystemUsageMetricDataID] [int] IDENTITY(1,1) NOT NULL,
	[SystemUsageMetricRunID] [int] NULL,
	[BusinessUnitID] [int] NOT NULL,
	[BusinessUnit] [varchar](50) NOT NULL,
	[NumberOfInventoryItems] [int] NULL,
	[NumberOfRetailInventoryItems] [int] NULL,
	[HasPINGII] [bit] NOT NULL CONSTRAINT [DF__SystemUsa__HasPI__6FB49576]  DEFAULT ((0)),
	[HasAcceleratorPackage] [varchar](50) NOT NULL CONSTRAINT [DF__SystemUsa__HasAc__70A8B9AF]  DEFAULT ((0)),
	[NumberOfItemsPINGED] [int] NULL,
	[PriceConfirmCount] [int] NULL,
	[TotalDMSChangeCount] [int] NULL,
	[TotalRepriceCount] [int] NULL,
	[NumberRepricedIMP] [int] NULL,
	[NumberRepricedeStock] [int] NULL,
	[NumberRepricedPING] [int] NULL,
	[NumberInventoryItemsShopped] [int] NULL,
	[NumberInventoryItemsPowered] [int] NULL,
	[NumberInventoryItemsTMVed] [int] NULL,
	[WBTurnedOn] [bit] NOT NULL CONSTRAINT [DF__SystemUsa__WBTur__719CDDE8]  DEFAULT ((0)),
	[MADTurnedOn] [bit] NOT NULL CONSTRAINT [DF__SystemUsa__MADTu__72910221]  DEFAULT ((0)),
	[AcceleratorStore] [bit] NOT NULL CONSTRAINT [DF__SystemUsa__Accel__73852660]  DEFAULT ((0)),
	[HasEDTDescriptionProvider] [bit] NOT NULL CONSTRAINT [DF__SystemUsa__HasED__74794A93]  DEFAULT ((0)),
	[EDTDescripton] [varchar](50) NULL,
	[NumberInternetDescriptions] [int] NULL,
	[NumberInternetDescriptionsCreated] [int] NULL,
	[NumberInternetDescriptionsEdited] [int] NULL,
	[NumberClosingNotes] [int] NULL,
	[NumberAppraisals] [int] NULL,
	[NumberAppraisalValues] [int] NULL,
	[NumberAppraisalForms] [int] NULL,
	[NumberAppraisalsPINGed] [int] NULL,
	[NumberMADSubscriptions] [int] NULL,
	[NumberLogins] [int] NULL,
 CONSTRAINT [PK_SystemUsageMetricsForSalesForce1Day] PRIMARY KEY CLUSTERED 
(
	[SystemUsageMetricDataID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[SystemUsageMetricsForSalesForce1Day]  WITH CHECK ADD  CONSTRAINT [FK_SystemUsageMetricsForSalesForce1Day_SystemUsageMetricsRuns1Day] FOREIGN KEY([SystemUsageMetricRunID])
REFERENCES [dbo].[SystemUsageMetricsRuns1Day] ([SystemUsageMetricsRunID])
GO
ALTER TABLE [dbo].[SystemUsageMetricsForSalesForce1Day] CHECK CONSTRAINT [FK_SystemUsageMetricsForSalesForce1Day_SystemUsageMetricsRuns1Day]