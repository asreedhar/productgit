if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SystemUsageMetricsRuns1Day]') 
and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE  [dbo].[SystemUsageMetricsRuns1Day] 
GO

/****** Object:  Table [dbo].[SystemUsageMetricsRuns1Day]    Script Date: 03/23/2012 11:46:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SystemUsageMetricsRuns1Day](
	[SystemUsageMetricsRunID] [int] IDENTITY(1,1) NOT NULL,
	[DateCreated] [datetime] NOT NULL,
	[StartDate] [smalldatetime] NOT NULL,
	[EndDate] [smalldatetime] NOT NULL,
	[IsScheduledRun] [bit] NOT NULL CONSTRAINT [DF_SystemUsageMetricsRuns1Day_IsScheduledRun]  DEFAULT ((0)),
	[IsSuccessful] [bit] NULL,
	[ErrorID] [int] NULL,
	[ErrorMessage] [varchar](200) NULL,
 CONSTRAINT [PK_SystemUsageMetricsRuns1Day] PRIMARY KEY CLUSTERED 
(
	[SystemUsageMetricsRunID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF