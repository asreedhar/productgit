SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAppraisalReviewDaysBackReportDealerGroup]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetAppraisalReviewDaysBackReportDealerGroup]
GO

CREATE FUNCTION [dbo].[GetAppraisalReviewDaysBackReportDealerGroup](@DealerGroupID INT, @DaysBack INT)
RETURNS @Report TABLE (
	InGroupRank INT
	, DealerGroupID INT 
	, Dealer_Group VARCHAR(100)
	, Dealer_ID INT
	, Dealer VARCHAR(100)
	, DaysBack INT
	, NumberOfAppraisalReviews INT
	, NumberOfAppraisals INT
	, NumberOfAppraisalReviewsPrevious INT
	, NumberOfAppraisalsPrevious INT
	, AvgAppraisalVsBookValue FLOAT
	, AvgAppraisalVsNaaaValue FLOAT
)
AS
BEGIN
	WITH MaxDealers AS (
		SELECT B.BusinessUnitID, G.BusinessUnitID Dealer_Group
		FROM [IMT].dbo.BusinessUnit B
		JOIN [IMT].dbo.BusinessUnitRelationship R ON R.BusinessUnitID = B.BusinessUnitID
		JOIN [IMT].dbo.BusinessUnit G ON G.BusinessUnitID = R.ParentID
		JOIN [IMT].dbo.DealerUpgrade D
			ON B.BusinessUnitID = D.BusinessUnitID
		WHERE D.DealerUpgradeCD IN (15, 16) --MAX and MAX-Test
		AND D.EffectiveDateActive = 1
		AND B.Active = 1
		AND B.BusinessUnitTypeID = 4
	), Results AS (
		SELECT C.DealerGroupID
			, D.Dealer_Group
			, C.DealerID Dealer_ID
			, C.Dealer
			, C.DaysBack
			, CASE WHEN D.BusinessUnitID IS NOT NULL 
				THEN C.NumberOfAppraisalReviews
				ELSE NULL
				END NumberOfAppraisalReviews
			, CASE WHEN D.BusinessUnitID IS NOT NULL 
				THEN C.NumberOfAppraisals
				ELSE NULL
				END NumberOfAppraisals
			, CASE WHEN D.BusinessUnitID IS NOT NULL 
				THEN P.NumberOfAppraisalReviews
				ELSE NULL
				END NumberOfAppraisalReviewsPrevious
			, CASE WHEN D.BusinessUnitID IS NOT NULL 
				THEN P.NumberOfAppraisals
				ELSE NULL
				END NumberOfAppraisalsPrevious
			, CASE WHEN C.NumberOfValidAppraisalVsBookValues > 0 
				THEN CAST(C.TotalAppraisalVsBookValue AS FLOAT)/ CAST(C.NumberOfValidAppraisalVsBookValues AS FLOAT)
				ELSE NULL
				END AvgAppraisalVsBookValue
			, CASE WHEN C.NumberOfValidAppraisalVsNaaaValues > 0
				THEN CAST(C.TotalAppraisalVsNaaaValue AS FLOAT) / CAST(C.NumberOfValidAppraisalVsNaaaValues AS FLOAT)
				ELSE NULL
				END AvgAppraisalVsNaaaValue
		FROM [HAL].dbo.GetAppraisalReviewsDaysBackReport C
		LEFT JOIN [HAL].dbo.GetAppraisalReviewsDaysBackReport P
		  ON C.DealerID = P.DealerID AND C.DaysBack + 1 = P.DaysBack
		LEFT JOIN MaxDealers D
			ON C.DealerID = D.BusinessUnitID
		WHERE C.DealerGroupID = @DealerGroupID
		AND C.DaysBack = @DaysBack
	)
	INSERT INTO @Report (
		InGroupRank
		, DealerGroupID
		, Dealer_Group
		, Dealer_ID
		, Dealer
		, DaysBack
		, NumberOfAppraisalReviews
		, NumberOfAppraisals
		, NumberOfAppraisalReviewsPrevious
		, NumberOfAppraisalsPrevious
		, AvgAppraisalVsBookValue
		, AvgAppraisalVsNaaaValue
	)
	SELECT
		RANK() OVER(ORDER BY R.NumberOfAppraisalReviews DESC) AS InGroupRank
		, R.*
	FROM Results R;
	RETURN
END
