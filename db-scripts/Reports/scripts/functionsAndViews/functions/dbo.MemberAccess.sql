 
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MemberAccess]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[MemberAccess]
GO

CREATE FUNCTION dbo.MemberAccess(@DealerGroupID INT, @MemberID INT)
RETURNS @Access TABLE (BusinessUnitID INT)
AS
BEGIN
	INSERT INTO @Access (BusinessUnitID)
	SELECT T.BusinessUnitID
	FROM
	(
		SELECT	BusinessUnitID
		FROM	[IMT].dbo.MemberAccess
		WHERE	MemberID = @MemberID
	UNION ALL
		SELECT	B.BusinessUnitID
		FROM	[IMT].dbo.BusinessUnitRelationship B CROSS JOIN [IMT].dbo.Member M
		WHERE	B.ParentID = @DealerGroupID
		AND		M.MemberID = @MemberID
		AND		M.MemberType = 1
	) T
	RETURN
END
GO
