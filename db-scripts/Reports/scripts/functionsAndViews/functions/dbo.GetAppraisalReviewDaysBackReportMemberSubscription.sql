IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAppraisalReviewDaysBackReportMemberSubscription]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetAppraisalReviewDaysBackReportMemberSubscription]
GO

CREATE FUNCTION [dbo].[GetAppraisalReviewDaysBackReportMemberSubscription](@DealerGroupID INT, @MemberID INT, @DaysBack INT)
RETURNS @Report TABLE (
	InGroupRank INT
	, DealerGroupID INT 
	, Dealer_ID INT
	, Dealer VARCHAR(100)
	, DaysBack INT
	, NumberOfAppraisalReviews INT
	, NumberOfAppraisals INT
	, NumberOfAppraisalReviewsPrevious INT
	, NumberOfAppraisalsPrevious INT
	, AvgAppraisalVsBookValue FLOAT
	, AvgAppraisalVsNaaaValue FLOAT
	, HasAccess BIT
)
AS
BEGIN
	INSERT INTO @Report (
		InGroupRank
		, DealerGroupID
		, Dealer_ID
		, Dealer
		, DaysBack
		, NumberOfAppraisalReviews
		, NumberOfAppraisals
		, NumberOfAppraisalReviewsPrevious
		, NumberOfAppraisalsPrevious
		, AvgAppraisalVsBookValue
		, AvgAppraisalVsNaaaValue
		, HasAccess
	) 
	SELECT 
		R.InGroupRank
		, R.DealerGroupID
		, R.Dealer_ID
		, R.Dealer
		, R.DaysBack
		, R.NumberOfAppraisalReviews
		, R.NumberOfAppraisals
		, R.NumberOfAppraisalReviewsPrevious
		, R.NumberOfAppraisalsPrevious
		, R.AvgAppraisalVsBookValue
		, R.AvgAppraisalVsNaaaValue
		, CASE WHEN MA.BusinessUnitID IS NULL
			THEN 0
			ELSE 1
			END HasAccess
	FROM dbo.GetAppraisalReviewDaysBackReportDealerGroup(@DealerGroupID, @DaysBack) R
	LEFT JOIN dbo.MemberAccess(@DealerGroupID, @MemberID) MA
		ON R.Dealer_ID = MA.BusinessUnitID
	RETURN
END
GO
