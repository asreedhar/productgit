IF NOT EXISTS (SELECT * FROM sys.server_principals WHERE name = N'FIRSTLOOK\CustomerOperations')
CREATE LOGIN [FIRSTLOOK\CustomerOperations] FROM WINDOWS WITH DEFAULT_DATABASE=[Reports]
GO

IF  NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'CustomerOperations' AND type = 'R')
CREATE ROLE [CustomerOperations]
GO

IF  NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'FIRSTLOOK\CustomerOperations')
CREATE USER [FIRSTLOOK\CustomerOperations] FOR LOGIN [FIRSTLOOK\CustomerOperations] WITH DEFAULT_SCHEMA=[dbo]
GO
