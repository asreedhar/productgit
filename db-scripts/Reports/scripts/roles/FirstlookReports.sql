IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'SQL_USER'
			AND name = 'FirstlookReports'
		)

	CREATE USER [FirstlookReports] FOR LOGIN [FirstlookReports] WITH DEFAULT_SCHEMA=[dbo]
GO
sp_addrolemember @rolename='db_datareader', @membername='FirstlookReports'
GO


USE [IMT]
GO

GRANT EXECUTE ON dbo.GetSalesHistoryReport TO FirstlookReports
GO

USE [FLDW]
GO

GRANT EXECUTE ON dbo.GetSalesHistoryReport TO FirstlookReports
GO
