/*

Routine to create linked server "ReportHostLink".  The idea is, this is used
by the Report Center application to link to the ReportHost database, which may
or may not be on the same server as the originating database.  Configure the
one parameter below (@ServerToLink) with the server hosting the desired instance
of ReportHost.

When deploying stored procedures (e.g. rc_GetReports) that use this definition,
it will probably be necessary to drop and recreate it in stages.  [It'd take a
page or more to describe why in enough detail, and I'm not doing that here.]
Briefly:
 - Run it with @RunPhaseTwo=0 to configure the linked server definition for stored procedure deployment
 - Run it with @RunPhaseTwo=1 to configure the linked server definition for standard operation


  Philip Kelley
  Aug 2006
  Dec 2006  (Some optimization)


--  Show all current linked server entries
EXECUTE sp_linkedServers

--  Check the configured local logins for it
EXECUTE sp_helpLinkedSrvLogin 'ReportHostLink'

--  Drop the linked server entry
EXECUTE sp_dropServer 'ReportHostLink', 'droplogins'

*/

SET NOCOUNT on

DECLARE
  @ServerToLink  varchar(128)
 ,@RunPhaseTwo   bit

SET @ServerToLink    = 'xxx'
  --  Server (instance) to link to

SET @RunPhaseTwo = 0
  --  Set to 0 to only run the first part
  --  Set to 1 for the full implementation

--  First Part  -------------------------------------------------------------------------

IF exists (select 1 from master.dbo.sysServers where srvname = 'ReportHostLink')
    --  It LOOKS like the linked server definition alread exists, so drop it
    EXECUTE sp_dropServer 'ReportHostLink', 'droplogins'


--  Create the database link
EXECUTE sp_addLinkedServer
  @server = 'ReportHostLink'
 ,@srvproduct = ''
 ,@provider = 'SQLOLEDB'
 ,@datasrc = @ServerToLink
 ,@catalog = 'ReportHost'


--  Set up appropriate options for the link
EXECUTE sp_serverOption 'ReportHostLink', 'Data Access',          'true'
EXECUTE sp_serverOption 'ReportHostLink', 'rpc',                  'true'
EXECUTE sp_serverOption 'ReportHostLink', 'rpc out',              'true'
EXECUTE sp_serverOption 'ReportHostLink', 'Collation Compatible', 'true'


IF @RunPhaseTwo = 1
 BEGIN
    --  Second Part  ------------------------------------------------------------------------
    
    --  Set it so that all unnamed logins have no rights.  (Ya drop the null entry
    --  to do this.  Presumably, it's created when the link def is first made.  This
    --  is hard to determine, as the proc to list linked server logins doesn't work.)
    EXECUTE sp_dropLinkedSrvLogin 'ReportHostLink', null
    
    --  Set up the FirstlookReports link to connect through using the same name AND
    --  password (all servers for a given environment should have the same password;
    --  servers of different environments should have different passwords)
    EXECUTE sp_addLinkedSrvLogin 'ReportHostLink', 'true', 'FirstlookReports', '', ''
 END

GO
