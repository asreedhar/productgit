The ReportServer database is created, managed, and used by Microsoft Reporting Services.
For assorted reasons, it has been found necessary to populate it with at least one of
our own stored procedures.  Moreover, we have had to "empower" it with a linked server,
defined on the hosting SQL Server instance.

The code for this database is not managed per se within CVS.  This project is solely
here to host the code necessary to (re)create the stored procedure and the related
linked server.  If you have to work with these objects, read the notes in the files
carefully--this is a tricky subject to get right.

		Philip Kelley
		September 2006
