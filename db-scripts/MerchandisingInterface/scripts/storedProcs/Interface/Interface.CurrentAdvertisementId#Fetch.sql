if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[CurrentAdvertisementId#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Interface].[CurrentAdvertisementId#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <3/10/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Interface.CurrentAdvertisementId#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Get the latest advertisement id from the advertising accelerator tables (Market.Merchandising.VehicleAdvertisement
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId,
 * @InventoryId,
 * @MerchandisingDescription,
 * @PhotoUrls - comma separated list of urls,
 * @OptionsList - comma separated list of options
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Interface].[CurrentAdvertisementId#Fetch]
	@OwnerEntityId int,
	@OwnerEntityTypeId int,
	@VehicleEntityId int, 
	@VehicleEntityTypeId int, 
	@AdvertisementId int output
	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @OwnerEntityId IS NULL BEGIN
		RAISERROR('Please supply OwnerEntityId',16,1)
		RETURN @@ERROR
	END
	
	IF @OwnerEntityTypeId IS NULL BEGIN
		RAISERROR('Please supply OwnerEntityTypeId',16,1)
		RETURN @@ERROR
	END
	
	IF @VehicleEntityId IS NULL BEGIN
		RAISERROR('Please supply VehicleEntityId',16,1)
		RETURN @@ERROR
	END
	
	IF @VehicleEntityTypeId IS NULL BEGIN
		RAISERROR('Please supply VehicleEntityTypeId',16,1)
		RETURN @@ERROR
	END	
	
	DECLARE @err INT	

	SELECT @AdvertisementId = max(advertisementId) FROM market.merchandising.vehicleadvertisement
	WHERE 
	vehicleEntityId = @VehicleEntityId
	AND vehicleEntityTypeId = @VehicleEntityTypeId
	AND ownerEntityId = @OwnerEntityId
	AND ownerEntityTypeId = @OwnerEntityTypeId
    
    RETURN 0
    
END


GO

GRANT EXECUTE ON [Interface].[CurrentAdvertisementId#Fetch] TO InterfaceUser 
GO

