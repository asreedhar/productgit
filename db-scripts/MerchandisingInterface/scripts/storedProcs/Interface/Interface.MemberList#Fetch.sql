if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[MemberList#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Interface].[MemberList#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/18/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Interface.MemberList#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Fetch all members for a business unit
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the member to fetch
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Interface].[MemberList#Fetch]
	@BusinessUnitId int
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	set transaction isolation level read uncommitted

	IF @BusinessUnitId IS NULL BEGIN
		RAISERROR('Please supply BusinessUnitId',16,1)
		RETURN @@ERROR
	END
	
	
	IF NOT EXISTS (SELECT 1 FROM [IMT].dbo.BusinessUnit WHERE BusinessUnitID = @BusinessUnitId) BEGIN
		RAISERROR('Not a BusinessUnitId %d',16,1,@BusinessUnitID)
		RETURN @@ERROR
	END
	

	SELECT m.MemberId,
		m.FirstName,
		m.LastName,
		m.Login,
		m.EmailAddress,
		m.MemberType,
		m.LoginStatus
	FROM imt.dbo.MemberAccess ma
						
	INNER JOIN imt.dbo.Member m
		ON ma.memberID = m.memberID
	WHERE ma.businessUnitId = @BusinessUnitID
				
END


GO

GRANT EXECUTE ON [Interface].[MemberList#Fetch] TO InterfaceUser 
GO
