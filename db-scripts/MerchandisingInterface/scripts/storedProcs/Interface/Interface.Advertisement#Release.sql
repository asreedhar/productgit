if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Advertisement#Release]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Interface].[Advertisement#Release]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/18/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Interface.Advertisement#Release.sql,v 1.8.4.2 2010/06/02 18:40:29 whummel Exp $
 * 
 * Summary
 * -------
 * 
 * Releases an advertisement to primary firstlook system
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId,
 * @InventoryId,
 * @MerchandisingDescription,
 * @OptionsList - comma separated list of options
	@ExteriorColor varchar(50) = NULL,
	@InteriorColor varchar(50) = NULL,
	@InteriorType varchar(50) = NULL,
	@MappedOptionIds varchar(1000) = NULL,
	@ChromeStyleId int = NULL,
	@OptionCodes varchar(300) = NULL,
	@HtmlMerchandisingDescription varchar(max) = NULL,
	@HighlightText varchar(200) = NULL,
	@Keywords varchar(200) = NULL,
	@ModelCode varchar(20) = NULL,
	@Trim varchar(35) = NULL,
	@ExteriorColorCode varchar(50) = NULL,
	@InteriorColorCode varchar(50) = NULL,
	@BedLength varchar(100) = NULL,
	@WheelBase varchar(12) = NULL,
	@GasMileageCity int = NULL,
	@GasMileageHighway int = NULL
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Interface].[Advertisement#Release]
	@BusinessUnitId int
	, @InventoryId int
	, @MerchandisingDescription varchar(5000)
	, @OptionsList varchar(5000) = NULL
	, @ExteriorColor varchar(50) = NULL
	, @InteriorColor varchar(50) = NULL
	, @InteriorType varchar(50) = NULL
	, @MappedOptionIds varchar(1000) = NULL
	, @ChromeStyleId int = NULL
	, @OptionCodes varchar(300) = NULL
	, @HtmlMerchandisingDescription varchar(max) = NULL
	, @HighlightText varchar(200) = NULL
	, @Keywords varchar(200) = NULL
	, @ModelCode varchar(20) = NULL
	, @Trim varchar(35) = NULL
	, @ExteriorColorCode varchar(50) = NULL
	, @InteriorColorCode varchar(50) = NULL
	, @BedLength varchar(100) = NULL
	, @WheelBase varchar(12) = NULL
	, @GasMileageCity int = NULL
	, @GasMileageHighway int = NULL
	, @VehicleCondition varchar(50) = NULL
	, @TireTread int = NULL
	, @StandardFeaturesList varchar(500) = NULL
	, @SpecialPrice int
	, @MSRP int = NULL
	, @ManufacturerRebate int = NULL
	, @DealerDiscount int = NULL

	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	IF @BusinessUnitId IS NULL BEGIN
		RAISERROR('Please supply BusinessUnitId',16,1)
		RETURN @@ERROR
	END
	
	IF @InventoryId IS NULL BEGIN
		RAISERROR('Please supply Inventory',16,1)
		RETURN @@ERROR
	END
	
	IF @MerchandisingDescription IS NULL BEGIN
		RAISERROR('Please supply MerchandisingDescription',16,1)
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM [IMT].dbo.BusinessUnit WHERE BusinessUnitID = @BusinessUnitId) BEGIN
		RAISERROR('Not a BusinessUnitId %d',16,1,@BusinessUnitID)
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM [IMT].dbo.Inventory WHERE BusinessUnitID = @BusinessUnitId AND InventoryID = @InventoryId) BEGIN
		RAISERROR('InventoryId %d does not belong to BusinessUnitId %d',16,1,@InventoryId,@BusinessUnitID)
		RETURN @@ERROR
	END
	
	DECLARE @rc INT, @err INT
	
	-- Transaction moved to middle tier
	
	
	declare @HighInternetPrice int
	
	--get the historically high internet price
	if EXISTS(SELECT 1 FROM Interface.ExportSettings
		where businessUnitId = @BusinessUnitId
		and useHighPrice = 1)
	BEGIN
		
		EXEC interface.highInternetPrice#Fetch @InventoryId, @HighInternetPrice output
	END
	
	IF EXISTS (SELECT 1 FROM Interface.AdReleases 
				WHERE inventoryId = @InventoryId
				AND businessUnitId = @BusinessUnitId)
	BEGIN
		UPDATE Interface.AdReleases
		SET  merchandisingDescription = @MerchandisingDescription
			,optionsList = @OptionsList
			,exteriorColor = @ExteriorColor
			,interiorColor = @InteriorColor
			,interiorType = @InteriorType
			,mappedOptionIds = @MappedOptionIds
			,chromeStyleId = @ChromeStyleId
			,OptionCodes = @OptionCodes
			,htmlMerchandisingDescription = @HtmlMerchandisingDescription
			,keywords = @Keywords
			,highlightText = @HighlightText
			,modelCode = @ModelCode
			,trim = @Trim
			,exteriorColorCode = @ExteriorColorCode
			,interiorColorCode = @InteriorColorCode
			,bedLength = @BedLength
			,wheelBase = @WheelBase
			,gasMileageCity = @GasMileageCity
			,gasMileageHighway = @GasMileageHighway
			,HighInternetPrice = @HighInternetPrice
			,VehicleCondition = @VehicleCondition
			,TireTread = @TireTread
			,standardFeaturesList = @StandardFeaturesList
			,SpecialPrice = @SpecialPrice
			,MSRP = @MSRP
			,ManufacturerRebate = @ManufacturerRebate
			,DealerDiscount = @DealerDiscount
		WHERE inventoryId = @InventoryId
			AND businessUnitId = @BusinessUnitId  
	END
	ELSE
	BEGIN
		INSERT INTO Interface.AdReleases
			(BusinessUnitId
			,InventoryID
			,MerchandisingDescription
			,OptionsList
			,ExteriorColor
			,InteriorColor
			,InteriorType
			,MappedOptionIds
			,chromeStyleId
			,OptionCodes
			,htmlMerchandisingDescription
			,keywords
			,highlightText
			,modelCode
			,trim
			,exteriorColorCode
			,interiorColorCode
			,bedLength
			,wheelBase
			,gasMileageCity
			,gasMileageHighway
			,highInternetPrice
			,VehicleCondition
			,TireTread
			,standardFeaturesList
			,SpecialPrice
			,MSRP
			,ManufacturerRebate
			,DealerDiscount)
		VALUES
			(@BusinessUnitId
			,@InventoryId
			,@MerchandisingDescription
			,@OptionsList
			,@ExteriorColor
			,@InteriorColor
			,@InteriorType
			,@MappedOptionIds
			,@ChromeStyleId
			,@OptionCodes
			,@HtmlMerchandisingDescription
			,@Keywords
			,@HighlightText
			,@ModelCode
			,@Trim
			,@ExteriorColorCode
			,@InteriorColorCode
			,@BedLength
			,@WheelBase
			,@GasMileageCity
			,@GasMileageHighway
			,@HighInternetPrice
			,@VehicleCondition
			,@TireTread
			,@StandardFeaturesList
			,@SpecialPrice
			,@MSRP
			,@ManufacturerRebate
			,@DealerDiscount)
			
	END
    SELECT @rc = @@ROWCOUNT, @err = @@ERROR
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    IF @err <> 0 BEGIN
				
		RAISERROR('Error %d!',16,1,@err)
		RETURN @@ERROR
    END    
    
    IF @rc <> 1 BEGIN
    		
		RAISERROR('You updated %d rows (expected 1 update) for BusinessUnitID %d and InventoryId %d',16,1,@rc,@BusinessUnitId,@InventoryId)
		RETURN @@ERROR
    END
        
    RETURN 1
    
END


GO

GRANT EXECUTE ON [Interface].[Advertisement#Release] TO InterfaceUser 
GO

