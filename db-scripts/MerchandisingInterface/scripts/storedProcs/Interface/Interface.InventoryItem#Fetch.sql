if exists (select * from dbo.sysobjects where id = object_id(N'Interface.InventoryItem#Fetch') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Interface.InventoryItem#Fetch
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/18/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * Fetch a single inventory item for a business unit
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId - id of inventory item
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE Interface.InventoryItem#Fetch
	@BusinessUnitId int,
	@InventoryId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	set transaction isolation level read uncommitted

	IF @BusinessUnitId IS NULL BEGIN
		RAISERROR('Please supply BusinessUnitId',16,1)
		RETURN @@ERROR
	END
	IF @InventoryId IS NULL BEGIN
		RAISERROR('Please supply InventoryId',16,1)
		RETURN @@ERROR
	END
	
	
	IF NOT EXISTS (SELECT 1 FROM IMT.dbo.BusinessUnit WHERE BusinessUnitID = @BusinessUnitId) BEGIN
		RAISERROR('Not a BusinessUnitId %d',16,1,@BusinessUnitID)
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM FLDW.dbo.InventoryActive WHERE InventoryID = @InventoryId) BEGIN
		RAISERROR('Not an InventoryId %d',16,1,@InventoryId)
		RETURN @@ERROR
	END
	

	SELECT 
		inv.businessUnitId,
		inv.Vin, 
		inv.StockNumber, 
		InventoryReceivedDate, 
		vehicleYear, 
		UnitCost, 
		AcquisitionPrice, 
		Certified, 
		Make, 
		model,
		v.vehicleTrim,
		inventoryID,
		MileageReceived, 
		TradeOrPurchase, 
		ListPrice,
		inventoryType,
		inv.InventoryStatusCD,
		v.BaseColor,
		inv.VehicleLocation
		,vpsm.ChromeStyleId
		,inv.LotPrice
		,inv.MSRP
		,v.VehicleCatalogId
		
	from
		FLDW.dbo.InventoryActive inv
		join FLDW.dbo.Vehicle v
			on inv.vehicleid = v.vehicleid
			and inv.businessUnitId = v.businessUnitId
		left join VehicleCatalog.Firstlook.VehicleCatalog_ChromeMapping vccm
			on vccm.vehicleCatalogId = v.vehicleCatalogId
		left join VehicleCatalog.Chrome.VinPatternStyleMapping vpsm
			on VPSM.CountryCode = 1
			and vpsm.VinMappingId = vccm.Chrome_VINMappingID
		join IMT.dbo.MakeModelGrouping mmg
			on mmg.MakeModelGroupingId = v.makeModelGroupingId
	
	where
		inv.BusinessUnitId = @BusinessUnitId
		and inventoryId = @InventoryId
			
END

GO

GRANT EXECUTE ON Interface.InventoryItem#Fetch TO InterfaceUser 
GO
