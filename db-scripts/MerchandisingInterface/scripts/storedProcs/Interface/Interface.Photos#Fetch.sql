if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Photos#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Interface].[Photos#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/18/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Interface.Photos#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Fetches an inventory item's photos
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businessUnit to fetch defaults to all dealers
 * @InventoryId - integer id of the inventory item
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Interface].[Photos#Fetch]
	@BusinessUnitID int,
	@InventoryId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT p.[PhotoID]
		  ,[PhotoTypeID]
		  ,[PhotoProviderID]
		  ,[PhotoStatusCode]
		  ,[PhotoURL]
		  ,[OriginalPhotoURL]
		  ,[DateCreated]
		  --,[PhotoUpdated]
		  ,[IsPrimaryPhoto]
		  
	FROM [IMT].[dbo].[Photos] p
	INNER JOIN imt.dbo.inventoryphotos ip 
		ON ip.photoId = p.photoId
	INNER JOIN imt.dbo.inventory i
		ON i.inventoryid = ip.inventoryid
	WHERE i.businessUnitId = @BusinessUnitId
	AND i.inventoryId = @InventoryId
	AND p.photoStatusCode = 1
		
END



GO

GRANT EXECUTE ON [Interface].[Photos#Fetch] TO InterfaceUser 
GO