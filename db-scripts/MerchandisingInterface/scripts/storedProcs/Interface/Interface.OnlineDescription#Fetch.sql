if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[OnlineDescription#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Interface].[OnlineDescription#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Interface.OnlineDescription#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * fetch the current sellers description from our recovered listings table
 * 
 * Parameters
 * ----------
 * 
 * @VIN
 * @ProviderId - check market.providers (autotrader=1, getauto=2 google base=3)
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Interface].[OnlineDescription#Fetch]
	-- Add the parameters for the stored procedure here
	@VIN varchar(17),
	@ProviderId int = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    select ISNULL(sellerDescription,'') as sellerDescription, sourceRowId from market.listing.vehicle vh
		LEFT join market.listing.vehicle_sellerDescription sd
		on sd.vehicleId = vh.vehicleId
		where VIN = @VIN
		AND (@ProviderId IS NULL OR providerId = @ProviderId)
END

GO

GRANT EXECUTE ON [Interface].[OnlineDescription#Fetch] TO InterfaceUser
GO