if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[MemberLogin#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Interface].[MemberLogin#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/18/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Interface.MemberLogin#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Fetch a member by login 
 * 
 * 
 * Parameters
 * ----------
 * 
 * @MemberLogin     - varchar login of the member to fetch
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Interface].[MemberLogin#Fetch]
	@MemberLogin varchar(80)
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	set transaction isolation level read uncommitted
	
	
	IF @MemberLogin IS NULL BEGIN
		RAISERROR('Please supply MemberLogin',16,1)
		RETURN @@ERROR
	END
	
	
	IF NOT EXISTS (SELECT 1 FROM [IMT].dbo.Member WHERE Login = @MemberLogin) BEGIN
		RAISERROR('Not a MemberLogin %s',16,1,@MemberLogin)
		RETURN @@ERROR
	END
	

	SELECT m.MemberId,
		m.FirstName,
		m.LastName,
		m.Login,
		m.EmailAddress,
		m.MemberType,
		m.LoginStatus
			
	FROM imt.dbo.Member m
	WHERE [Login] = @MemberLogin
END


GO

GRANT EXECUTE ON [Interface].[MemberLogin#Fetch] TO InterfaceUser 
GO
