if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Photo#Publish]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Interface].[Photo#Publish]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/18/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Interface.Photo#Publish.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Publishes an inventory item's photos - if photo exists, does nothing
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businessUnit to fetch defaults to all dealers
 * @InventoryId - integer id of the inventory item
 * @PhotoUrl - url to the photo
 * @IsPrimary - bit flag to indicate if this photo is the vehicle's primary photo
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Interface].[Photo#Publish]
	@BusinessUnitID int,
	@InventoryId int,
	@PhotoUrl varchar(200),
	@IsPrimary bit = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	IF @BusinessUnitId IS NULL BEGIN
		RAISERROR('Please supply BusinessUnitId',16,1)
		RETURN @@ERROR
	END
		
	IF @InventoryId IS NULL BEGIN
		RAISERROR('Please supply InventoryId',16,1)
		RETURN @@ERROR
	END
	
	IF @PhotoUrl IS NULL OR len(@PhotoUrl) <= 0 BEGIN
		RAISERROR('Please supply photoUrl',16,1)
		RETURN @@ERROR
	END
	
		
	IF NOT EXISTS (SELECT 1 FROM [IMT].dbo.Inventory WHERE BusinessUnitID = @BusinessUnitId AND InventoryID = @InventoryId) BEGIN
		RAISERROR('InventoryId %d does not belong to BusinessUnitId %d',16,1,@InventoryId,@BusinessUnitID)
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM IMT.dbo.Photos WHERE OriginalPhotoUrl = @PhotoUrl)
	BEGIN
		INSERT INTO IMT.dbo.Photos 
		(photoTypeId
		,photoProviderId
		,photoStatusCode
		,photoUrl
		,originalPhotoUrl
		,dateCreated
		--,photoUpdated
		,isPrimaryPhoto)
		VALUES
		(2			--inventory photo
		,NULL		--no third party provider (dealer provided)
		,2			--stored remotely
		,@PhotoUrl
		,@PhotoUrl
		,getdate()
		--,getdate()
		,@IsPrimary)			--NOT the primary photo by default
		
		--insert a row into the inventoryPhotos table to link the photo to the inventory item
		INSERT INTO IMT.dbo.InventoryPhotos
		(inventoryId
		,photoId)
		VALUES
		(@InventoryId
		,SCOPE_IDENTITY())
		
	END
	
END



GO

GRANT EXECUTE ON [Interface].[Photo#Publish] TO InterfaceUser 
GO