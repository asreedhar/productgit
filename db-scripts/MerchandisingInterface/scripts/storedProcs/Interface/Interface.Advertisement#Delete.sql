if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Advertisement#Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Interface].[Advertisement#Delete]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/18/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Interface.Advertisement#Delete.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Deletes an advertisement from primary firstlook system
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId,
 * @InventoryId,
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Interface].[Advertisement#Delete]
	@BusinessUnitId int,
	@InventoryId int
	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @BusinessUnitId IS NULL BEGIN
		RAISERROR('Please supply BusinessUnitId',16,1)
		RETURN @@ERROR
	END
	
	IF @InventoryId IS NULL BEGIN
		RAISERROR('Please supply Inventory',16,1)
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM [IMT].dbo.BusinessUnit WHERE BusinessUnitID = @BusinessUnitId) BEGIN
		RAISERROR('Not a BusinessUnitId %d',16,1,@BusinessUnitID)
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM [IMT].dbo.Inventory WHERE BusinessUnitID = @BusinessUnitId AND InventoryID = @InventoryId) BEGIN
		RAISERROR('InventoryId %d does not belong to BusinessUnitId %d',16,1,@InventoryId,@BusinessUnitID)
		RETURN @@ERROR
	END
	
	DECLARE @rc INT, @err INT
	
	--begin upsert transaction
	BEGIN TRANSACTION
	
	DELETE FROM Interface.AdReleases 
				WHERE inventoryId = @InventoryId
				AND businessUnitId = @BusinessUnitId
				
	-- NEEDS RULES ENGINE IMPLEMENTATION TO TRULY STOP SENDING----------------
	
    SELECT @rc = @@ROWCOUNT, @err = @@ERROR
    
    IF @err <> 0 BEGIN
		
		--ROLLBACK DELETE
		ROLLBACK
		
		RAISERROR('Error %d!',16,1,@err)
		RETURN @@ERROR
    END    
    
    IF @rc > 1 BEGIN
    
		--TOO MANY ROWS UPDATED
		ROLLBACK 
		
		RAISERROR('You deleted %d rows (expected 0 or 1 delete) for BusinessUnitID %d and InventoryId %d',16,1,@rc,@BusinessUnitId,@InventoryId)
		RETURN @@ERROR
    END
    
    --MADE IT PAST ERROR CASES, COMMIT
    COMMIT
    
    RETURN 1
    
END


GO

GRANT EXECUTE ON [Interface].[Advertisement#Delete] TO InterfaceUser 
GO

