if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[ConditionInformation#Release]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Interface].[ConditionInformation#Release]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/18/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Interface.ConditionInformation#Release.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Releases an advertisement to primary firstlook system
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId,
 * @InventoryId,
 * @exteriorCondition varchar(1),
	@paintCondition varchar(1),
	@trimCondition varchar(1),
	@glassCondition varchar(1),
	@interiorCondition varchar(1),
	@carpetCondition varchar(1),
	@seatsCondition varchar(1),
	@headlinerCondition varchar(1),
	@dashboardCondition varchar(1),
	@isDealerMaintained bit,
	@isFullyDetailed bit,
	@hasNoPanelScratches bit,
	@hasNoVisibleDents bit,
	@hasNoVisibleRust bit,
	@hasNoKnownAccidents bit,
	@hasNoKnownBodyWork bit,
	@hasPassedDealerInspection bit,
	@haveAllKeys bit,
	@noKnownMechanicalProblems bit,
	@hadAllRequiredScheduledMaintenancePerformed bit,
	@haveServiceRecords bit,
	@haveOriginalManuals bit	
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Interface].[ConditionInformation#Release]
	@BusinessUnitId int,
	@InventoryId int,
	@exteriorCondition varchar(1),
	@paintCondition varchar(1),
	@trimCondition varchar(1),
	@glassCondition varchar(1),
	@interiorCondition varchar(1),
	@carpetCondition varchar(1),
	@seatsCondition varchar(1),
	@headlinerCondition varchar(1),
	@dashboardCondition varchar(1),
	@isDealerMaintained bit,
	@isFullyDetailed bit,
	@hasNoPanelScratches bit,
	@hasNoVisibleDents bit,
	@hasNoVisibleRust bit,
	@hasNoKnownAccidents bit,
	@hasNoKnownBodyWork bit,
	@hasPassedDealerInspection bit,
	@haveAllKeys bit,
	@noKnownMechanicalProblems bit,
	@hadAllRequiredScheduledMaintenancePerformed bit,
	@haveServiceRecords bit,
	@haveOriginalManuals bit	
	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @BusinessUnitId IS NULL BEGIN
		RAISERROR('Please supply BusinessUnitId',16,1)
		RETURN @@ERROR
	END
	
	IF @InventoryId IS NULL BEGIN
		RAISERROR('Please supply Inventory',16,1)
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM [IMT].dbo.BusinessUnit WHERE BusinessUnitID = @BusinessUnitId) BEGIN
		RAISERROR('Not a BusinessUnitId %d',16,1,@BusinessUnitID)
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM [IMT].dbo.Inventory WHERE BusinessUnitID = @BusinessUnitId AND InventoryID = @InventoryId) BEGIN
		RAISERROR('InventoryId %d does not belong to BusinessUnitId %d',16,1,@InventoryId,@BusinessUnitID)
		RETURN @@ERROR
	END
	
	DECLARE @rc INT, @err INT
	
	-- transaction moved to middle tier
	
	--insert condition checklist
	IF EXISTS (SELECT 1 FROM Interface.ConditionChecklist 
				WHERE inventoryId = @InventoryId
				AND businessUnitId = @BusinessUnitId)
	BEGIN
		UPDATE Interface.ConditionChecklist
		SET 
			isDealerMaintained = @isDealerMaintained,
			isFullyDetailed = @isFullyDetailed,
			hasNoPanelScratches = @hasNoPanelScratches,
			hasNoVisibleDents = @hasNoVisibleDents,
			hasNoVisibleRust = @hasNoVisibleRust,
			hasNoKnownAccidents = @hasNoKnownAccidents,
			hasNoKnownBodyWork = @hasNoKnownBodyWork,
			hasPassedDealerInspection = @hasPassedDealerInspection,
			haveAllKeys = @haveAllKeys,
			noKnownMechanicalProblems = @noKnownMechanicalProblems,
			hadAllRequiredScheduledMaintenancePerformed = @hadAllRequiredScheduledMaintenancePerformed,
			haveServiceRecords = @haveServiceRecords,
			haveOriginalManuals = @haveOriginalManuals
		WHERE inventoryId = @InventoryId
			AND businessUnitId = @BusinessUnitId  
	END
	ELSE
	BEGIN
		INSERT INTO Interface.ConditionChecklist
			(BusinessUnitId, 
			InventoryID, 
			isDealerMaintained,
			isFullyDetailed,
			hasNoPanelScratches,
			hasNoVisibleDents,
			hasNoVisibleRust,
			hasNoKnownAccidents,
			hasNoKnownBodyWork,
			hasPassedDealerInspection,
			haveAllKeys,
			noKnownMechanicalProblems,
			hadAllRequiredScheduledMaintenancePerformed,
			haveServiceRecords,
			haveOriginalManuals
			)
		VALUES
			(@BusinessUnitId,
			@InventoryId,
			@isDealerMaintained,
			@isFullyDetailed,
			@hasNoPanelScratches,
			@hasNoVisibleDents,
			@hasNoVisibleRust,
			@hasNoKnownAccidents,
			@hasNoKnownBodyWork,
			@hasPassedDealerInspection,
			@haveAllKeys,
			@noKnownMechanicalProblems,
			@hadAllRequiredScheduledMaintenancePerformed,
			@haveServiceRecords,
			@haveOriginalManuals			
			)
	END
	
	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
    
   
    IF @err <> 0 BEGIN
				
		RAISERROR('Error %d!',16,1,@err)
		RETURN @@ERROR
    END    
    
    IF @rc <> 1 BEGIN
		
		RAISERROR('You updated %d rows (expected 1 update) for BusinessUnitID %d and InventoryId %d',16,1,@rc,@BusinessUnitId,@InventoryId)
		RETURN @@ERROR
    END
    
    
	--insert condition report
	IF EXISTS (SELECT 1 FROM Interface.ConditionReport
				WHERE inventoryId = @InventoryId
				AND businessUnitId = @BusinessUnitId)
	BEGIN
		UPDATE Interface.ConditionReport
		SET 
		exteriorCondition = @exteriorCondition,
		paintCondition = @paintCondition,
		trimCondition = @trimCondition,
		glassCondition = @glassCondition,
		interiorCondition = @interiorCondition,
		carpetCondition = @carpetCondition,
		seatsCondition = @seatsCondition,
		headlinerCondition = @headlinerCondition,
		dashboardCondition = @dashboardCondition
		WHERE inventoryId = @InventoryId
			AND businessUnitId = @BusinessUnitId  
	END
	ELSE
	BEGIN
		INSERT INTO Interface.ConditionReport
			(BusinessUnitId, 
			InventoryID, 
			exteriorCondition,
			paintCondition,
			trimCondition,
			glassCondition,
			interiorCondition,
			carpetCondition,
			seatsCondition,
			headlinerCondition,
			dashboardCondition
			)
		VALUES
			(@BusinessUnitId,
			@InventoryId,
			@exteriorCondition,
			@paintCondition,
			@trimCondition,
			@glassCondition,
			@interiorCondition,
			@carpetCondition,
			@seatsCondition,
			@headlinerCondition,
			@dashboardCondition			
			)
	END
	
	SELECT @rc = @@ROWCOUNT, @err = @@ERROR
    
   
    IF @err <> 0 BEGIN
				
		RAISERROR('Error %d!',16,1,@err)
		RETURN @@ERROR
    END    
    
    IF @rc <> 1 BEGIN
    		
		RAISERROR('You updated %d rows (expected 1 update) for BusinessUnitID %d and InventoryId %d',16,1,@rc,@BusinessUnitId,@InventoryId)
		RETURN @@ERROR
    END
        
    RETURN 1
    
END


GO

GRANT EXECUTE ON [Interface].[ConditionInformation#Release] TO InterfaceUser 
GO

