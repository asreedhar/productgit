if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[VehicleListingOptions#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Interface].[VehicleListingOptions#Fetch]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <1/16/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Interface.VehicleListingOptions#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Get options for a vehicle in market.listings
 * 
 * 
 * Parameters
 * ----------
 * 
 * @InventoryId     - inventoryId of vehicle
 * @ProviderId		- providerId of options to fetch - if null, gets any...
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Interface].[VehicleListingOptions#Fetch]
	@InventoryId int,
	@ProviderId int = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	set transaction isolation level read uncommitted

	IF @InventoryId IS NULL BEGIN
		RAISERROR('Please supply an inventoryId',16,1)
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM [FLDW].dbo.InventoryActive WHERE InventoryID = @InventoryId) BEGIN
		RAISERROR('Not an InventoryId %d',16,1,@InventoryId)
		RETURN @@ERROR
	END
	
	--not checking for existence of sellerid in pricing.owner because some sellers have listings with no entry in owner table
	
	SELECT 
		ol.optionID
		,ol.Description
	FROM 
	Market.Pricing.Search se
	INNER JOIN [Market].Listing.Vehicle ve 
		ON se.ListingVehicleId = ve.VehicleId
	INNER JOIN [Market].Listing.VehicleOption op 
		ON ve.vehicleid = op.vehicleid
	INNER JOIN [Market].Listing.OptionsList ol 
		ON ol.optionid = op.optionid
	WHERE 
		se.vehicleEntityId = @InventoryId
		AND vehicleEntityTypeId = 1
		AND (@ProviderId IS NULL OR ve.providerId = @ProviderId)
	

	
END

GO

GRANT EXECUTE ON [Interface].[VehicleListingOptions#Fetch] TO InterfaceUser 
GO