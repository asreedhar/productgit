if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[HistoricalInventoryList#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Interface].[HistoricalInventoryList#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/18/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Interface.HistoricalInventoryList#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Fetch a list of inventory items for a business unit - active and inactive inventory
 * Only return inventory received after a certain date - allowing us to constrain the 
 * slice of history returned
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @EarliestReceivedDate - the earliest inventory received date to return in the set
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Interface].[HistoricalInventoryList#Fetch]
	@BusinessUnitId int,
	@EarliestReceivedDate datetime,
	@UsedOrNew int = 2
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	set transaction isolation level read uncommitted

	IF @BusinessUnitId IS NULL BEGIN
		RAISERROR('Please supply BusinessUnitId',16,1)
		RETURN @@ERROR
	END
	
	if @EarliestReceivedDate IS NULL BEGIN
		RAISERROR('Please supply an earliest received date to confine the results set',16,1)
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 FROM [IMT].dbo.BusinessUnit WHERE BusinessUnitID = @BusinessUnitId) BEGIN
		RAISERROR('Not a BusinessUnitId %d',16,1,@BusinessUnitID)
		RETURN @@ERROR
	END

	SELECT 
		inv.businessUnitId,
		Vin, 
		inv.StockNumber, 
		InventoryReceivedDate, 
		vehicleYear, 
		UnitCost, 
		AcquisitionPrice, 
		Certified, 
		Make, 
		model,
		v.vehicleTrim,
		v.segmentId,
		seg.segment,
		inventoryID,
		MileageReceived, 
		TradeOrPurchase, 
		ListPrice,
		inventoryType,
		inv.InventoryStatusCD,
		v.BaseColor,
		inv.VehicleLocation
		FROM [FLDW].dbo.Inventory inv
			INNER JOIN [FLDW].dbo.Vehicle v 
				ON inv.vehicleid = v.vehicleid
				AND inv.businessUnitId = v.businessUnitId
			INNER JOIN [IMT].dbo.MakeModelGrouping mmg
				ON mmg.MakeModelGroupingId = v.makeModelGroupingId
			INNER JOIN [IMT].dbo.Segment seg
				ON seg.segmentId = v.segmentId
		WHERE inv.BusinessUnitId = @BusinessUnitId
			and (@UsedOrNew = 0 OR inv.inventoryType = @UsedOrNew)
			and inventoryReceivedDate >= @EarliestReceivedDate
	
END

GO

GRANT EXECUTE ON [Interface].[HistoricalInventoryList#Fetch] TO InterfaceUser 
GO