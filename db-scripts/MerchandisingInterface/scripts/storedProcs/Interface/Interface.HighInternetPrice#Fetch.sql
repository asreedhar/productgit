if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[HighInternetPrice#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Interface].[HighInternetPrice#Fetch]
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/12/09>
-- Description:	Return the high price for a vehicle from its pricing history
-- =============================================
CREATE PROCEDURE [Interface].[HighInternetPrice#Fetch] 
	@InventoryId int,
	@HighInternetPrice decimal(8,2) output
   
AS
BEGIN
   
   select @HighInternetPrice = case when maxHistory > maxValue 
				then maxHistory
				else maxValue end
		FROM (
			SELECT MAX(AE.Notes2) as maxHistory, max([Value]) as maxValue
			FROM	IMT.dbo.AIP_Event AE
			JOIN FLDW.dbo.InventoryActive I ON I.InventoryID = AE.InventoryID
			WHERE	I.InventoryID = @InventoryId
			AND AE.AIP_EventTypeID = 5
			group by I.inventoryId
		) as priceHistory   
  
END

GO

GRANT EXECUTE ON [Interface].[HighInternetPrice#Fetch] TO InterfaceUser
GO