if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[InventoryList#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Interface].[InventoryList#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/18/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Interface.InventoryList#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Fetch a list of inventory items for a business unit
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Interface].[InventoryList#Fetch]
	@BusinessUnitId int,
	@UsedOrNew int = 2
AS
set nocount on
set transaction isolation level read uncommitted

IF @BusinessUnitId IS NULL BEGIN
	RAISERROR('Please supply BusinessUnitId',16,1)
	RETURN @@ERROR
END


IF NOT EXISTS (SELECT 1 FROM [IMT].dbo.BusinessUnit WHERE BusinessUnitID = @BusinessUnitId) BEGIN
	RAISERROR('Not a BusinessUnitId %d',16,1,@BusinessUnitID)
	RETURN @@ERROR
END
	

	


SELECT 
	inv.businessUnitId,
	inv.Vin, 
	inv.StockNumber, 
	InventoryReceivedDate, 
	vehicleYear, 
	UnitCost, 
	AcquisitionPrice, 
	Certified, 
	Make, 
	model,
	v.vehicleTrim,
	inv.inventoryID,
	MileageReceived, 
	TradeOrPurchase, 
	ListPrice,
	inventoryType,
	inv.InventoryStatusCD,
	COALESCE(oc.extColor1, v.BaseColor),
	inv.VehicleLocation,
	inv.LotPrice,
	inv.MSRP,
	v.VehicleCatalogId
from
	[FLDW].dbo.InventoryActive inv
	INNER JOIN [FLDW].dbo.Vehicle v 
		ON inv.vehicleid = v.vehicleid
		AND inv.businessUnitId = v.businessUnitId
	INNER JOIN [IMT].dbo.MakeModelGrouping mmg
		ON mmg.MakeModelGroupingId = v.makeModelGroupingId
	LEFT JOIN [Merchandising].[builder].[OptionsConfiguration] oc 
		ON oc.inventoryId = inv.InventoryID AND inv.BusinessUnitID = oc.businessUnitID
where
	inv.BusinessUnitId = @BusinessUnitId
	AND inventoryActive = 1
	and (@UsedOrNew = 0 OR inv.inventoryType = @UsedOrNew)



GO

GRANT EXECUTE ON [Interface].[InventoryList#Fetch] TO InterfaceUser 
GO