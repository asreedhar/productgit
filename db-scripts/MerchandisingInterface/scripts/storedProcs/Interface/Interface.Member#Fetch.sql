if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Member#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Interface].[Member#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/18/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Interface.Member#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Fetch a member by id 
 * 
 * 
 * Parameters
 * ----------
 * 
 * @MemberId     - integer id of the member to fetch
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Interface].[Member#Fetch]
	@MemberId int
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	set transaction isolation level read uncommitted
	
	IF @MemberId IS NULL BEGIN
		RAISERROR('Please supply MemberId',16,1)
		RETURN @@ERROR
	END
	
	
	IF NOT EXISTS (SELECT 1 FROM [IMT].dbo.Member WHERE MemberID = @MemberId) BEGIN
		RAISERROR('Not a MemberId %d',16,1,@MemberID)
		RETURN @@ERROR
	END
	

	SELECT m.MemberId,
		m.FirstName,
		m.LastName,
		m.Login,
		m.EmailAddress,
		m.MemberType,
		m.LoginStatus
			
	FROM imt.dbo.Member m
	WHERE memberID = @MemberId
	
END


GO

GRANT EXECUTE ON [Interface].[Member#Fetch] TO InterfaceUser 
GO
