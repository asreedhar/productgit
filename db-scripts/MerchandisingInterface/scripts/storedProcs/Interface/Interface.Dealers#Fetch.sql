if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Dealers#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Interface].[Dealers#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/18/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Interface.Dealers#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Fetches a dealer by business unit id or a list of active dealers when input is null
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businessUnit to fetch defaults to all dealers
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Interface].[Dealers#Fetch]
	@BusinessUnitID int = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	set transaction isolation level read uncommitted
	
	--DO NOT INLINE OR CHANGE THIS TABLE - CONTRACTUAL INTERFACE TO CODE
		
	SELECT 
		b.businessUnitID,
		b.businessUnit,
		b.businessUnitTypeID
			
		FROM imt.dbo.BusinessUnit b
		INNER JOIN imt.dbo.DealerPreference p
			ON p.businessUnitId = b.businessUnitId
		WHERE (b.businessUnitID = @BusinessUnitID
			OR @BusinessUnitID IS NULL)
			AND b.businessUnitTypeID = 4
			AND b.active = 1
			AND p.goLiveDate IS NOT NULL
				
END



GO

GRANT EXECUTE ON [Interface].[Dealers#Fetch] TO InterfaceUser 
GO