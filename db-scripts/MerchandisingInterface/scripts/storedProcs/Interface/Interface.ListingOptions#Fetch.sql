if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[ListingOptions#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Interface].[ListingOptions#Fetch]
GO

/****** Object:  StoredProcedure [Interface].[ListingOptions#Fetch]    Script Date: 02/09/2009 14:59:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <1/16/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Interface.ListingOptions#Fetch.sql,v 1.4 2010/02/10 21:54:16 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Get options for a seller's listings
 * 
 * 
 * Parameters
 * ----------
 * 
 * @MarketSellerId     - integer id of the seller in market database
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Interface].[ListingOptions#Fetch]
	@MarketSellerId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	set transaction isolation level read uncommitted

	IF @MarketSellerId IS NULL BEGIN
		RAISERROR('Please supply a Market SellerID',16,1)
		RETURN @@ERROR
	END
	
	--not checking for existence of sellerid in pricing.owner because some sellers have listings with no entry in owner table
	

	select veh.vehicleId, optionId
	FROM [Market].Listing.Vehicle veh
	INNER JOIN 
		[Market].Listing.VehicleOption vehOpt
	ON veh.vehicleId = vehOpt.vehicleId
	WHERE sellerId = @MarketSellerId
	
END

GO

GRANT EXECUTE ON [Interface].[ListingOptions#Fetch] TO InterfaceUser 
GO