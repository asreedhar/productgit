IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'WINDOWS_USER'
			AND name = 'FIRSTLOOK\InsightReports'
		) BEGIN
		
	CREATE USER [Firstlook\InsightReports] FOR LOGIN [Firstlook\InsightReports]
	ALTER USER [Firstlook\InsightReports] WITH DEFAULT_SCHEMA=[dbo]
END	
	
EXEC sp_addrolemember N'db_datareader', N'Firstlook\InsightReports'
