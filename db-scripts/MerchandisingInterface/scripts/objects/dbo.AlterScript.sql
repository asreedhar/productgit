
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Alter_Script]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Alter_Script]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Alter_Script] (
	[AlterScriptName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ApplicationDate] [datetime] NOT NULL 
) 
GO