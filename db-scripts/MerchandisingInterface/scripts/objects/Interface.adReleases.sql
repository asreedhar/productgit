if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[AdReleases]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [Interface].[AdReleases]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Interface].[AdReleases](
	
	[businessUnitId] [int] NOT NULL,
	[inventoryId] [int] NOT NULL,
	[merchandisingDescription] [varchar](5000) NOT NULL,
	[optionsList] [varchar](5000) NOT NULL,
	[photoUrls] [varchar] (5000) NOT NULL

	
)

GO
SET ANSI_PADDING OFF

GRANT SELECT ON [Interface].[AdReleases] TO [InterfaceUser]
GO