
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Advertisement#Fetch]'))
DROP SYNONYM [Interface].[Advertisement#Fetch]
GO

CREATE SYNONYM [Interface].[Advertisement#Fetch] FOR [Market].Merchandising.[Advertisement#Fetch]
GO
--------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Advertisement#Insert]'))
DROP SYNONYM [Interface].[Advertisement#Insert]
GO

CREATE SYNONYM [Interface].[Advertisement#Insert] FOR [Market].Merchandising.[Advertisement#Insert]
GO
--------------------


if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Advertisement_Properties#Insert]'))
DROP SYNONYM [Interface].[Advertisement_Properties#Insert]
GO

CREATE SYNONYM [Interface].[Advertisement_Properties#Insert] FOR [Market].Merchandising.[Advertisement_Properties#Insert]
GO
--------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Advertisement_ExtendedProperties#Insert]'))
DROP SYNONYM [Interface].[Advertisement_ExtendedProperties#Insert]
GO

CREATE SYNONYM [Interface].[Advertisement_ExtendedProperties#Insert] FOR [Market].Merchandising.[Advertisement_ExtendedProperties#Insert]
GO
--------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[VehicleAdvertisement#Assign]'))
DROP SYNONYM [Interface].[VehicleAdvertisement#Assign]
GO

CREATE SYNONYM [Interface].[VehicleAdvertisement#Assign] FOR [Market].Merchandising.[VehicleAdvertisement#Assign]
GO
--------------------


-----------INITIAL LISTING INTERFACES-----------
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[ListingProviderMakeModel]'))
DROP SYNONYM [Interface].[ListingProviderMakeModel]
GO

CREATE SYNONYM [Interface].[ListingProviderMakeModel] FOR [Market].Pricing.[ListingProviderMakeModel]
GO
--------------------
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[HandleLookup_Vehicle]'))
DROP SYNONYM [Interface].[HandleLookup_Vehicle]
GO

CREATE SYNONYM [Interface].[HandleLookup_Vehicle] FOR [Market].Pricing.[HandleLookup_Vehicle]
GO
--------------------
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[OwnerHandle]'))
DROP SYNONYM [Interface].[OwnerHandle]
GO

CREATE SYNONYM [Interface].[OwnerHandle] FOR [Market].Pricing.[OwnerHandle]
GO
--------------------
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[MarketPricing]'))
DROP SYNONYM [Interface].[MarketPricing]
GO

CREATE SYNONYM [Interface].[MarketPricing] FOR [Market].Pricing.[MarketPricing]
GO
--------------------