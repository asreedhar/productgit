IF EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'RebateAmount' AND Object_ID = Object_ID(N'Interface.AdReleases'))
BEGIN
	exec sp_rename 'MerchandisingInterface.Interface.AdReleases.RebateAmount', 'ManufacturerRebate', 'COLUMN'
END
GO


IF EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'IncentiveAmount' AND Object_ID = Object_ID(N'Interface.AdReleases'))
BEGIN
	exec sp_rename 'MerchandisingInterface.Interface.AdReleases.IncentiveAmount', 'DealerDiscount', 'COLUMN'
END
GO

