if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[ConditionReport]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [Interface].[ConditionReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Interface].[ConditionReport](
	
	[businessUnitId] [int] NOT NULL,
	[inventoryId] [int] NOT NULL,
	[exteriorCondition] [varchar](1) NOT NULL,
	[paintCondition] [varchar](1) NOT NULL,
	[trimCondition] [varchar](1) NOT NULL,
	[glassCondition] [varchar](1) NOT NULL,
	[interiorCondition] [varchar](1) NOT NULL,
	[carpetCondition] [varchar](1) NOT NULL,
	[seatsCondition] [varchar](1) NOT NULL,
	[headlinerCondition] [varchar](1) NOT NULL,
	[dashboardCondition] [varchar](1) NOT NULL,
	CONSTRAINT [PK_ConditionReport] PRIMARY KEY CLUSTERED 
	(
		[businessUnitID] ASC,
		[inventoryID] ASC
	)

	
)

GO
SET ANSI_PADDING OFF

if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[ConditionChecklist]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [Interface].[ConditionChecklist]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Interface].[ConditionChecklist](
	
	[businessUnitId] [int] NOT NULL,
	[inventoryId] [int] NOT NULL,
	isDealerMaintained [bit] NOT NULL,
	isFullyDetailed [bit] NOT NULL,
	hasNoPanelScratches [bit] NOT NULL,
	hasNoVisibleDents [bit] NOT NULL,
	hasNoVisibleRust [bit] NOT NULL,
	hasNoKnownAccidents [bit] NOT NULL,
	hasNoKnownBodyWork [bit] NOT NULL,
	hasPassedDealerInspection [bit] NOT NULL,
	haveAllKeys [bit] NOT NULL,
	noKnownMechanicalProblems [bit] NOT NULL,
	hadAllRequiredScheduledMaintenancePerformed [bit] NOT NULL,
	haveServiceRecords [bit] NOT NULL,
	haveOriginalManuals [bit] NOT NULL,
	CONSTRAINT [PK_Interface_ConditionChecklist] PRIMARY KEY CLUSTERED 
	(
		[businessUnitID] ASC,
		[inventoryID] ASC
	)

	
)

GO
SET ANSI_PADDING OFF