ALTER TABLE Interface.AdReleases 
ADD ExteriorColor VARCHAR(50) NULL
GO

ALTER TABLE Interface.AdReleases 
ADD InteriorColor VARCHAR(50) NULL
GO

ALTER TABLE Interface.AdReleases 
ADD InteriorType VARCHAR(50) NULL
GO

ALTER TABLE Interface.AdReleases 
ADD MappedOptionIds VARCHAR(1000) NULL
GO


ALTER TABLE Interface.AdReleases 
ALTER COLUMN OptionsList VARCHAR(5000) NULL
GO


ALTER TABLE Interface.AdReleases 
ALTER COLUMN MerchandisingDescription VARCHAR(5000) NULL
GO
