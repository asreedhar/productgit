ALTER TABLE Interface.AdReleases 
ADD ModelCode varchar(20) NULL
GO

ALTER TABLE Interface.AdReleases
ADD Trim varchar(35) NULL
GO

ALTER TABLE Interface.AdReleases
ADD ExteriorColorCode varchar(50) NULL
GO

ALTER TABLE Interface.AdReleases
ADD InteriorColorCode varchar(50) NULL
GO

ALTER TABLE Interface.AdReleases
ADD BedLength varchar(100) NULL
GO

ALTER TABLE Interface.AdReleases
ADD WheelBase varchar(12) NULL
GO

ALTER TABLE Interface.AdReleases
ADD GasMileageCity int NULL
GO

ALTER TABLE Interface.AdReleases
ADD GasMileageHighway int NULL
GO