--
-- Remove unsupported HTML tags
--
UPDATE Interface.AdReleases
SET HtmlMerchandisingDescription=REPLACE(REPLACE(REPLACE(REPLACE(HtmlMerchandisingDescription, '<p>', ''), '</p>', ''), '<b>', ''), '</b>', '')
