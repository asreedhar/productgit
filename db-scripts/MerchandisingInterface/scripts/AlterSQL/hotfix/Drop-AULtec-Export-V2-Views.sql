------------------------------------------------------------------------------------------------
--	Remove V2 views
------------------------------------------------------------------------------------------------


if exists (select * from dbo.sysobjects where id = object_id(N'[Extract].[Merchandising#AULtec2]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view Extract.Merchandising#AULtec2
