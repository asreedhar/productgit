--
-- Modify the HTMLMerchandisingDescription column per Zac's specifications
--
UPDATE Interface.AdReleases
SET HtmlMerchandisingDescription=
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(HtmlMerchandisingDescription, '~~~', '==='), '~~', '==' ),'<br />','<br /><br />' ),'===', '<br /><br />'), '==', ''), '<br /><br /><br /><br />', '<br /><br />')
FROM Interface.AdReleases AR
LEFT JOIN Merchandising.builder.Advertisement A ON AR.businessUnitId = A.businessUnitId AND AR.inventoryId = A.inventoryId
WHERE AR.HtmlMerchandisingDescription IS NOT NULL
AND A.businessUnitId IS NULL