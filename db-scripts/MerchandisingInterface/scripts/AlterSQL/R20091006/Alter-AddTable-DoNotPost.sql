if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[ExportSettings_Inventory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [Interface].[ExportSettings_Vehicle]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE Interface.ExportSettings_Vehicle (
	businessUnitId int,
	inventoryId int,
	doNotPostFlag bit,
	CONSTRAINT PK_ExportSettings_Vehicle PRIMARY KEY (businessUnitId, inventoryId)
) 


GO
SET ANSI_PADDING OFF