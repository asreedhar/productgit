ALTER TABLE Interface.AdReleases 
ADD VehicleCondition varchar(50) NULL
GO 

ALTER TABLE Interface.AdReleases 
ADD TireTread int NULL
GO 

ALTER TABLE Interface.AdReleases
ADD VideoUrl varchar(100) NULL
GO

ALTER TABLE Interface.AdReleases
ADD standardFeaturesList varchar(500) NULL
GO

