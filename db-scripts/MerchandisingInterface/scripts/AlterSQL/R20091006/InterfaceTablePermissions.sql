 GRANT SELECT ON [Interface].[ExportSettings] TO InterfaceUser
 GO
 
 GRANT SELECT ON [Interface].[ExportSettings_Vehicle] TO InterfaceUser
 GO
 
 GRANT SELECT ON [Interface].[ConditionReport] TO InterfaceUser
 GO
 
 GRANT SELECT ON [Interface].[ConditionChecklist] TO InterfaceUser
 GO
 