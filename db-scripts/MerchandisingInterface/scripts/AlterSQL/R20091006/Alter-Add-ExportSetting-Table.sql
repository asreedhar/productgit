if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[ExportSettings]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [Interface].[ExportSettings]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Interface].[ExportSettings](
	
	[businessUnitId] [int] NOT NULL,
	[useHighPrice] [bit] NOT NULL,
	CONSTRAINT [PK_ExportSettings] PRIMARY KEY CLUSTERED 
	(
		[businessUnitID] ASC		
	)

	
)

GO
SET ANSI_PADDING OFF
