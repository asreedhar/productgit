ALTER TABLE Interface.AdReleases 
ADD HtmlMerchandisingDescription varchar(max)
GO

ALTER TABLE Interface.AdReleases 
ADD HighlightText varchar(200)
GO

ALTER TABLE Interface.AdReleases 
ADD Keywords VARCHAR(200) NULL
GO