ALTER TABLE dbo.AuctionTransaction_A2 ADD TransactionCount INT NULL
ALTER TABLE dbo.AuctionTransaction_A4 ADD TransactionCount INT NULL
GO

UPDATE	dbo.AuctionTransaction_A2
SET	TransactionCount = 0
GO

UPDATE	dbo.AuctionTransaction_A4
SET	TransactionCount = 0
GO


ALTER TABLE dbo.AuctionTransaction_A2 ALTER COLUMN TransactionCount INT NOT NULL
ALTER TABLE dbo.AuctionTransaction_A4 ALTER COLUMN TransactionCount INT NOT NULL
GO


--------------------------------------------------------------------------------
--	MAKE SURE THE TABLES ARE REAGG'D BEFORE USING
--------------------------------------------------------------------------------