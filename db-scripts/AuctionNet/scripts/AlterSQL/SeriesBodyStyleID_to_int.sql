/*

Modify column SeriesBodyStyleID from smallint to int in the following tables:
 - SeriesBodyStyle_D
 - AuctionTransaction_A1
 - AuctionTransaction_A2
 - SeriesBodyStyle_DXM

This will take some time to run.  Before running, access to the database should be
disabled; after running, it should be restored.  The following should work for the
appropriate servers:

--  HYPERION  -----------------------------------------------------

--  Disable
EXECUTE sp_dropRoleMember 'db_owner'     , 'firstlook'
EXECUTE sp_dropRoleMember 'db_dataReader', 'Firstlook\Analytics'
EXECUTE sp_dropRoleMember 'db_owner'     , 'uat'

--  Enable
EXECUTE sp_AddRoleMember 'db_owner'     , 'firstlook'
EXECUTE sp_AddRoleMember 'db_dataReader', 'Firstlook\Analytics'
EXECUTE sp_AddRoleMember 'db_owner'     , 'uat'


--  TITAN  --------------------------------------------------------

--  Disable
EXECUTE sp_dropRoleMember 'db_owner'     , 'firstlook'
EXECUTE sp_dropRoleMember 'db_dataReader', 'Firstlook\Analytics'
EXECUTE sp_dropRoleMember 'db_owner'     , 'uat'

--  Enable
EXECUTE sp_AddRoleMember 'db_owner'     , 'firstlook'
EXECUTE sp_AddRoleMember 'db_dataReader', 'Firstlook\Analytics'
EXECUTE sp_AddRoleMember 'db_owner'     , 'Readerlink'


--  Adjust ALTER_SCRIPT after running IF run by hand
select * from alter_script

INSERT Alter_script (AlterScriptName) values ('SeriesBodyStyleID_to_int.sql')

*/

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_AuctionTransaction_A1_SeriesBodyStyle_D]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[AuctionTransaction_A1] DROP CONSTRAINT FK_AuctionTransaction_A1_SeriesBodyStyle_D
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_AuctionTransaction_A2_SeriesBodyStyle_D]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[AuctionTransaction_A2] DROP CONSTRAINT FK_AuctionTransaction_A2_SeriesBodyStyle_D
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_SeriesBodyStyle_DXM_SeriesBodyStyle_D]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[SeriesBodyStyle_DXM] DROP CONSTRAINT FK_SeriesBodyStyle_DXM_SeriesBodyStyle_D
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PK_SeriesBodyStyle_D]') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[SeriesBodyStyle_D] DROP CONSTRAINT PK_SeriesBodyStyle_D
GO


ALTER TABLE [dbo].[SeriesBodyStyle_D] ALTER COLUMN [SeriesBodyStyleID] [INT]


ALTER TABLE [dbo].[SeriesBodyStyle_D] WITH NOCHECK ADD 
	CONSTRAINT [PK_SeriesBodyStyle_D] PRIMARY KEY  CLUSTERED 
	(
		[SeriesBodyStyleID]
	)  ON [PRIMARY] 
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PK_AuctionTransaction_A1]') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[AuctionTransaction_A1] DROP CONSTRAINT PK_AuctionTransaction_A1
GO

ALTER TABLE [dbo].[AuctionTransaction_A1] ALTER COLUMN [SeriesBodyStyleID] [INT] NOT NULL
GO
ALTER TABLE [dbo].[AuctionTransaction_A1] WITH NOCHECK ADD 
	CONSTRAINT [PK_AuctionTransaction_A1] PRIMARY KEY  CLUSTERED 
	(
		[PeriodID],
		[SeriesBodyStyleID],
		[ModelYear],
		[AreaID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[AuctionTransaction_A1] ADD 
	CONSTRAINT [FK_AuctionTransaction_A1_SeriesBodyStyle_D] FOREIGN KEY 
	(
		[SeriesBodyStyleID]
	) REFERENCES [dbo].[SeriesBodyStyle_D] (
		[SeriesBodyStyleID]
	)

GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[PK_AuctionTransaction_A2]') and OBJECTPROPERTY(id, N'IsPrimaryKey') = 1)
ALTER TABLE [dbo].[AuctionTransaction_A2] DROP CONSTRAINT PK_AuctionTransaction_A2
GO

ALTER TABLE [dbo].[AuctionTransaction_A2] ALTER COLUMN [SeriesBodyStyleID] [INT] NOT NULL
GO

ALTER TABLE [dbo].[AuctionTransaction_A2] WITH NOCHECK ADD 
	CONSTRAINT [PK_AuctionTransaction_A2] PRIMARY KEY  CLUSTERED 
	(
		[PeriodID],
		[SeriesBodyStyleID],
		[ModelYear],
		[AreaID],
		[LowMileageRange],
		[HighMileageRange]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO


ALTER TABLE [dbo].[AuctionTransaction_A2] ADD 
	CONSTRAINT [FK_AuctionTransaction_A2_SeriesBodyStyle_D] FOREIGN KEY 
	(
		[SeriesBodyStyleID]
	) REFERENCES [dbo].[SeriesBodyStyle_D] (
		[SeriesBodyStyleID]
	)
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------

DROP INDEX  [SeriesBodyStyle_DXM].[IX_SeriesID]
GO

ALTER TABLE [dbo].[SeriesBodyStyle_DXM] ALTER COLUMN [SeriesBodyStyleID] [INT] NOT NULL
GO

 CREATE  INDEX [IX_SeriesID] ON [dbo].[SeriesBodyStyle_DXM]([SeriesBodyStyleID]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO


ALTER TABLE [dbo].[SeriesBodyStyle_DXM] ADD 
	CONSTRAINT [FK_SeriesBodyStyle_DXM_SeriesBodyStyle_D] FOREIGN KEY 
	(
		[SeriesBodyStyleID]
	) REFERENCES [dbo].[SeriesBodyStyle_D] (
		[SeriesBodyStyleID]
	)
GO
