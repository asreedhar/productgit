----------------------------------------------------
-- Auction Build process, alters only.
-- BF 03/24/05 - Birth
----------------------------------------------------

----------------------------------------------------
-- Blow away Unused tables
----------------------------------------------------

----------------------------------------------------
-- Create dbo.Area_D
----------------------------------------------------
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Area_D]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Area_D]
GO

CREATE TABLE [dbo].[Area_D] (
	[AreaID] [tinyint] NOT NULL ,
	[AreaName] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	CONSTRAINT [PK_Area_D] PRIMARY KEY  CLUSTERED 
	(
		[AreaID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO

----------------------------------------------------
-- Create dbo.AreaRegion_X
----------------------------------------------------
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AreaRegion_X]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AreaRegion_X]
GO

CREATE TABLE [dbo].[AreaRegion_X] (
	[AreaID] [tinyint] NOT NULL ,
	[RegionID] [tinyint] NOT NULL ,
	CONSTRAINT [PK_AreaRegion_X] PRIMARY KEY  CLUSTERED 
	(
		[AreaID],
		[RegionID]
	)  ON [PRIMARY] ,
	CONSTRAINT [FK_AreaRegion_X_Area_D] FOREIGN KEY 
	(
		[AreaID]
	) REFERENCES [dbo].[Area_D] (
		[AreaID]
	),
	CONSTRAINT [FK_AreaRegion_X_Region_D] FOREIGN KEY 
	(
		[RegionID]
	) REFERENCES [dbo].[Region_D] (
		[RegionID]
	)
) ON [PRIMARY]
GO

----------------------------------------------------
-- 'Alter' a composite key from RegionId to AreaId 
-- apply ModelYear fix
----------------------------------------------------
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AuctionTransaction_A1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AuctionTransaction_A1]
GO

CREATE TABLE [dbo].[AuctionTransaction_A1] (
	[PeriodID] [tinyint] NOT NULL ,
	[SeriesBodyStyleID] [smallint] NOT NULL ,
	[ModelYear] [smallint] NOT NULL CONSTRAINT [DF_AuctionTransaction_A1_ModelYear] DEFAULT (0),
	[AreaID] [tinyint] NOT NULL ,
	[HighSalePrice] [decimal](8, 2) NOT NULL ,
	[AveSalePrice] [decimal](8, 2) NOT NULL ,
	[LowSalePrice] [decimal](8, 2) NOT NULL ,
	[HighMileage] [decimal](18, 0) NOT NULL ,
	[AveMileage] [decimal](18, 0) NOT NULL ,
	[LowMileage] [decimal](18, 0) NOT NULL ,
	[TransactionCount] [int] NOT NULL CONSTRAINT [DF_AuctionTransaction_A1_TransactionCount] DEFAULT (0),
	CONSTRAINT [PK_AuctionTransaction_A1] PRIMARY KEY  CLUSTERED 
	(
		[PeriodID],
		[SeriesBodyStyleID],
		[ModelYear],
		[AreaID]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	CONSTRAINT [FK_AuctionTransaction_A1_SeriesBodyStyle_D] FOREIGN KEY 
	(
		[SeriesBodyStyleID]
	) REFERENCES [dbo].[SeriesBodyStyle_D] (
		[SeriesBodyStyleID]
	),
	CONSTRAINT [FK_AuctionTransaction_F_Period_D] FOREIGN KEY 
	(
		[PeriodID]
	) REFERENCES [dbo].[Period_D] (
		[PeriodID]
	),
	CONSTRAINT [FK_AuctionTransaction_F_Region_D] FOREIGN KEY 
	(
		[AreaID]
	) REFERENCES [dbo].[Region_D] (
		[RegionID]
	)
) ON [PRIMARY]
GO

----------------------------------------------------
-- 'Alter' a composite key from RegionId to AreaId 
-- apply ModelYear fix
----------------------------------------------------
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AuctionTransaction_A2]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[AuctionTransaction_A2]
GO

CREATE TABLE [dbo].[AuctionTransaction_A2] (
	[PeriodID] [tinyint] NOT NULL ,
	[SeriesBodyStyleID] [smallint] NOT NULL ,
	[ModelYear] [smallint] NOT NULL CONSTRAINT [DF_AuctionTransaction_A2_ModelYear] DEFAULT (0),
	[AreaID] [tinyint] NOT NULL ,
	[LowMileageRange] [int] NOT NULL ,
	[HighMileageRange] [int] NOT NULL ,
	[AveSalePrice] [decimal](8, 2) NULL ,
	CONSTRAINT [PK_AuctionTransaction_A2] PRIMARY KEY  CLUSTERED 
	(
		[PeriodID],
		[SeriesBodyStyleID],
		[ModelYear],
		[AreaID],
		[LowMileageRange],
		[HighMileageRange]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	CONSTRAINT [FK_AuctionTransaction_A2_Period_D] FOREIGN KEY 
	(
		[PeriodID]
	) REFERENCES [dbo].[Period_D] (
		[PeriodID]
	),
	CONSTRAINT [FK_AuctionTransaction_A2_Region_D] FOREIGN KEY 
	(
		[AreaID]
	) REFERENCES [dbo].[Region_D] (
		[RegionID]
	),
	CONSTRAINT [FK_AuctionTransaction_A2_SeriesBodyStyle_D] FOREIGN KEY 
	(
		[SeriesBodyStyleID]
	) REFERENCES [dbo].[SeriesBodyStyle_D] (
		[SeriesBodyStyleID]
	)
) ON [PRIMARY]
GO

----------------------------------------------------
-- Alter Period_D table
----------------------------------------------------
ALTER TABLE dbo.Period_D
	ADD Rank tinyint NOT NULL CONSTRAINT [DF_Period_D_Rank] DEFAULT (0)
GO

----------------------------------------------------
-- Create PeriodDates Table
----------------------------------------------------
CREATE TABLE [dbo].[PeriodDates] (
	[PeriodID] [tinyint] NOT NULL ,
	[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[BeginDate] [datetime] NULL ,
	[EndDate] [datetime] NULL ,
	[Rank] [int] NULL ,
	CONSTRAINT [PK_PeriodDates] PRIMARY KEY  CLUSTERED 
	(
		[PeriodID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO	


insert
into	Area_D
select 1,'National'
union select 2,'Southeast'
union select 3,'Northeast'
union select 4,'Midwest'
union select 5,'Southwest'
union select 6,'West Coast'


insert
into	AreaRegion_X
select 1,0
union select 1,1
union select 1,2
union select 1,3
union select 1,4
union select 1,5
union select 1,6
union select 1,7
union select 1,8
union select 1,9
union select 1,10
union select 1,11
union select 1,12
union select 1,13
union select 1,14
union select 1,15
union select 1,16
union select 1,17
union select 1,18
union select 1,19
union select 1,20
union select 2,7
union select 2,8
union select 2,9
union select 2,18
union select 3,1
union select 3,10
union select 3,15
union select 3,17
union select 4,3
union select 4,4
union select 4,11
union select 4,12
union select 4,13
union select 4,14
union select 4,16
union select 5,2
union select 5,6
union select 5,19
union select 6,5
union select 6,20
GO

alter view dbo.AuctionDetailReport
as

select 	A.AuctionTransactionID, PD.PeriodID, SBS.SeriesBodyStyleID, AR.AreaID, A.RegionID, ModelYear, cast(T.BeginDate as smalldatetime) SaleDate, SaleTypeName, SalePrice, Mileage, E.Engine, TR.Transmission, SBSV.SeriesBodyStyle, A.VIN
from 	AuctionTransaction_F A
	join Time_D T on A.TimeID = T.TimeID
	join PeriodDates PD on T.BeginDate between PD.BeginDate and PD.EndDate
	join SaleType_D ST on A.SaleTypeID = ST.SaleTypeID
	join SeriesBodyStyle_D SBS on A.SeriesID = SBS.SeriesID and A.BodyStyleID = SBS.BodyStyleID
	join SeriesBodyStyle SBSV on SBS.SeriesBodyStyleID = SBSV.SeriesBodyStyleID
	join Engine_D E on A.EngineID = E.EngineID
	join Transmission_D TR on A.TransmissionID = TR.TransmissionID
	join AreaRegion_X AR on A.RegionID = AR.RegionID 	-- EXCLUDE NATIONAL???




GO


ALTER view dbo.AuctionTransaction_AV1
as
select 	PeriodID, SeriesBodyStyleID, AreaID, ModelYear, HighSalePrice, AveSalePrice, LowSalePrice, HighMileage, AveMileage, LowMileage, TransactionCount
from	AuctionTransaction_A1

GO

ALTER view dbo.AuctionTransaction_AV2
as
select 	PeriodID, SeriesBodyStyleID, AreaID, ModelYear, LowMileageRange, HighMileageRange, AveSalePrice 
from	AuctionTransaction_A2

GO

-- Stored procedure
CREATE proc dbo.GetAuctionDetailReport
@PeriodID 		tinyint = null,
@SeriesBodyStyleID 	smallint = null,
@AreaID 		tinyint = null,
@RegionID		tinyint = null,
@ModelYear		int = null,
@RunDate 		datetime = null
as

set nocount on 

set @RunDate = isnull(@RunDate, dbo.ToDate(getdate()))

select 	A.AuctionTransactionID, PD.PeriodID, SBS.SeriesBodyStyleID, AR.AreaID, A.RegionID, ModelYear, cast(T.BeginDate as smalldatetime) SaleDate, SaleTypeName, SalePrice, Mileage, E.Engine, TR.Transmission, SBSV.SeriesBodyStyle, A.VIN
from 	AuctionTransaction_F A
	join Time_D T on A.TimeID = T.TimeID
	join dbo.GetPeriodDates(@RunDate) PD on T.BeginDate between PD.BeginDate and PD.EndDate
	join SaleType_D ST on A.SaleTypeID = ST.SaleTypeID
	join SeriesBodyStyle_D SBS on A.SeriesID = SBS.SeriesID and A.BodyStyleID = SBS.BodyStyleID
	join SeriesBodyStyle SBSV on SBS.SeriesBodyStyleID = SBSV.SeriesBodyStyleID
	join Engine_D E on A.EngineID = E.EngineID
	join Transmission_D TR on A.TransmissionID = TR.TransmissionID
	join AreaRegion_X AR on A.RegionID = AR.RegionID 	-- EXCLUDE NATIONAL???
where	(@PeriodID is null or PD.PeriodID = @PeriodID)
	and (@SeriesBodyStyleID is null or SBS.SeriesBodyStyleID = @SeriesBodyStyleID)
	and (@AreaID is null or AR.AreaID = @AreaID)
	and (@RegionID is null or A.RegionID = @RegionID)
	and (@ModelYear is null or A.ModelYear = @ModelYear)
GO


ALTER proc dbo.Rebuild_Aggregates
--------------------------------------------------------------------------------
--
--	Loads the AuctionNet data and associated dimensions in the AuctionNet
--	star schema.
--	
---History----------------------------------------------------------------------
--	
--	WGH	09/02/2004	Initial design/development
--		02/25/2005	Added materialized view PeriodDates
--				Replace RegionID with AreaID (FL Region) 
--				Restored (?) ModelYear aggregation updates
--				from 01/12/2005
--
--------------------------------------------------------------------------------	
as

--------------------------------------------------------------------------------
--	INITIALIZATION
--------------------------------------------------------------------------------

set nocount on 

declare @sql 	varchar(1000),
	@rc		int,
	@err 		int,
	@msg		varchar(255),
	@proc_name	varchar(128),
	@path		varchar(100),
	@RunDate	datetime

set @proc_name = 'Rebuild_Aggregates'
exec DBASTAT..Log_Event 'I', @proc_name, 'Started'

--------------------------------------------------------------------------------
--	REBUILD MATERIALIZED VIEWS
--------------------------------------------------------------------------------

set @RunDate = isnull(@RunDate, dbo.ToDate(getdate()))

--------------------------------------------------------------------------------
set @msg = 'Truncate PeriodDates'
--------------------------------------------------------------------------------

truncate table dbo.PeriodDates

select @err = @@error, @rc = @@error
if @err <> 0 goto Failed

--------------------------------------------------------------------------------
set @msg = 'Populate PeriodDates'
--------------------------------------------------------------------------------

insert
into	PeriodDates (PeriodID, Description, BeginDate, EndDate, Rank)
select	PeriodID, Description, BeginDate, EndDate, Rank
from	dbo.GetPeriodDates(@RunDate)

select @err = @@error, @rc = @@rowcount
if @err <> 0 goto Failed

set @msg = cast(@rc as varchar) + ' rows inserted into PeriodDates'
exec DBASTAT..Log_Event 'I', @proc_name, @msg

--------------------------------------------------------------------------------
--	REBUILD AGGREGATE TABLES'
--	SHOULD DROP KEYS AND RESTORE AFTER COMPLETELY POP'D
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
set @msg = 'TRUNCATE AuctionTransaction_A1'
--------------------------------------------------------------------------------

truncate table AuctionTransaction_A1

select @err = @@error, @rc = @@error
if @err <> 0 goto Failed

--------------------------------------------------------------------------------
set @msg = 'Populate AuctionTransaction_A1'
--------------------------------------------------------------------------------

insert
into	AuctionTransaction_A1 (PeriodID, SeriesBodyStyleID, ModelYear, AreaID, HighSalePrice, AveSalePrice, LowSalePrice, HighMileage, AveMileage, LowMileage, TransactionCount)
select 	PD.PeriodID, SBS.SeriesBodyStyleID, A.ModelYear, ARX.AreaID, max(SalePrice), avg(SalePrice), min(SalePrice), 0, avg(cast(Mileage as decimal(10,2))), 0, count(*)
from 	AuctionTransaction_F A
	join Time_D T on A.TimeID = T.TimeID
	join PeriodDates PD on T.BeginDate between PD.BeginDate and PD.EndDate
	join SeriesBodyStyle_D SBS on A.SeriesID = SBS.SeriesID and A.BodyStyleID = SBS.BodyStyleID
	join AreaRegion_X ARX on A.RegionID = ARX.RegionID

group
by	PD.PeriodID, SBS.SeriesBodyStyleID, ModelYear, ARX.AreaID, SBS.SeriesBodyStyleID

select @err = @@error, @rc = @@rowcount
if @err <> 0 goto Failed

set @msg = cast(@rc as varchar) + ' rows inserted into AuctionTransaction_A1'
exec DBASTAT..Log_Event 'I', @proc_name, @msg

--------------------------------------------------------------------------------
set @msg = 'Update AuctionTransaction_A1 (HighMileage)'
--------------------------------------------------------------------------------

update	A
set	HighMileage = Mileage
from	AuctionTransaction_A1 A
	join AreaRegion_X ARX on A.AreaID = ARX.AreaID
	join SeriesBodyStyle_D SBS on A.SeriesBodyStyleID = SBS.SeriesBodyStyleID
	join AuctionTransaction_F F on SBS.SeriesID = F.SeriesID and SBS.BodyStyleID = F.BodyStyleID and ARX.RegionID = F.RegionID and F.SalePrice = A.HighSalePrice
	join Time_D T on F.TimeID = T.TimeID
	join PeriodDates PD on T.BeginDate between PD.BeginDate and PD.EndDate and A.PeriodID = PD.PeriodID

select @err = @@error, @rc = @@rowcount
if @err <> 0 goto Failed

set @msg = cast(@rc as varchar) + ' rows updated in AuctionTransaction_A1 (HighMileage)'
exec DBASTAT..Log_Event 'I', @proc_name, @msg

--------------------------------------------------------------------------------
set @msg = 'Update AuctionTransaction_A1 (LowMileage)'
--------------------------------------------------------------------------------

update	A
set	LowMileage = Mileage
from	AuctionTransaction_A1 A
	join AreaRegion_X ARX on A.AreaID = ARX.AreaID
	join SeriesBodyStyle_D SBS on A.SeriesBodyStyleID = SBS.SeriesBodyStyleID
	join AuctionTransaction_F F on SBS.SeriesID = F.SeriesID and SBS.BodyStyleID = F.BodyStyleID and ARX.RegionID = F.RegionID and F.SalePrice = A.LowSalePrice
	join Time_D T on F.TimeID = T.TimeID
	join PeriodDates PD on T.BeginDate between PD.BeginDate and PD.EndDate and A.PeriodID = PD.PeriodID


select @err = @@error, @rc = @@rowcount
if @err <> 0 goto Failed

set @msg = cast(@rc as varchar) + ' rows updated in AuctionTransaction_A1 (LowMileage)'
exec DBASTAT..Log_Event 'I', @proc_name, @msg

--------------------------------------------------------------------------------
set @msg = 'TRUNCATE AuctionTransaction_A2'
--------------------------------------------------------------------------------

truncate table AuctionTransaction_A2

select @err = @@error, @rc = @@error
if @err <> 0 goto Failed

--------------------------------------------------------------------------------
set @msg = 'Populate AuctionTransaction_A2'
--------------------------------------------------------------------------------

insert
into	AuctionTransaction_A2 (PeriodID, SeriesBodyStyleID, ModelYear, AreaID, LowMileageRange, HighMileageRange, AveSalePrice)
select	PeriodID, SeriesBodyStyleID, ModelYear, ARX.AreaID, R.Low, R.High, avg(SalePrice)
from	AuctionTransaction_F A
	join AreaRegion_X ARX on A.RegionID = ARX.RegionID
	join Time_D T on A.TimeID = T.TimeID
	join PeriodDates PD on T.BeginDate between PD.BeginDate and PD.EndDate
	join SeriesBodyStyle_D SBS on A.SeriesID = SBS.SeriesID and A.BodyStyleID = SBS.BodyStyleID
	join (	select 	distinct cast(Mileage / 10000 as int) * 10000 Low, cast(Mileage / 10000 as int) * 10000 + 9999 High
		from 	AuctionTransaction_F
		) R on A.Mileage between R.Low and R.High
group
by	PeriodID, SeriesBodyStyleID, ModelYear, ARX.AreaID, R.Low, R.High

select @err = @@error, @rc = @@rowcount
if @err <> 0 goto Failed

set @msg = cast(@rc as varchar) + ' rows updated in AuctionTransaction_A2'
exec DBASTAT..Log_Event 'I', @proc_name, @msg


Finished:

exec DBASTAT..Log_Event 'I', @proc_name, 'Completed without error'
return 0

Failed:

set @msg = 'Failed at step "' + @msg + '"'
exec DBASTAT..Log_Event 'I', @proc_name, @msg
return @err

GO
create function dbo.BuildDate(@month tinyint, @day tinyint, @year int)
----------------------------------------------------------------------------
--	IS THERE A BETTER WAY?
----------------------------------------------------------------------------
returns datetime
as
begin
	return(cast(cast(@month as varchar) + '/' + cast(@day as varchar) +  '/' + cast(@year as varchar) as datetime))
end	


GO


CREATE function dbo.GetPeriodDates(@RunDate datetime)
returns @results table (PeriodID int, Description varchar(50), BeginDate datetime, EndDate datetime, Rank tinyint)
as
begin

	insert
	into	@results (PeriodID, Description, BeginDate, EndDate, Rank)
--	select 	PeriodID, Description, dateadd(dd,1,dateadd(ww,-DurationCount,@RunDate)) BeginDate, @RunDate EndDate, Rank
--	from 	Period_D
--	where	TimeTypeCD = 2
--	union all
	select	PeriodID, dbo.PeriodDescriptionParse(Description, @RunDate), dbo.BuildDate(1,1,year(@RunDate)), @RunDate, Rank
	from	Period_D
	where	PeriodID = 10
	union all
	select	PeriodID, dbo.PeriodDescriptionParse(Description, @RunDate), dbo.BuildDate(month(@RunDate),1,year(@RunDate)), @RunDate, Rank
	from	Period_D
	where	PeriodID = 8
	union all
	select	PeriodID, dbo.PeriodDescriptionParse(Description, @RunDate), dbo.BuildDate(month(@RunDate),1,year(@RunDate)-1), dateadd(year,-1,@RunDate), Rank
	from	Period_D
	where	PeriodID = 9
	union all
	select	PeriodID, dbo.PeriodDescriptionParse(Description, @RunDate), dateadd(month,-1,dbo.BuildDate(month(@RunDate),1,year(@RunDate))), dateadd(day,-1,dbo.BuildDate(month(@RunDate),1,year(@RunDate))), Rank
	from	Period_D
	where	PeriodID = 14
	union	all
	select	PeriodID, dbo.PeriodDescriptionParse(Description, @RunDate), dateadd(day,1,dateadd(week,-DurationCount, @RunDate)), @RunDate, Rank
	from	Period_D
	where	PeriodID in (11,12,13)
	return
end



GO

create function dbo.PeriodDescriptionParse(@Description varchar(50), @RunDate datetime )
returns varchar(50)
as
begin
	declare @result varchar(50)

	set @result = replace(@Description,'<CY>',cast(year(@RunDate) as varchar))
	set @result = replace(@result,'<PY>',cast(year(@RunDate)-1 as varchar))
	set @result = replace(@result,'<CM>',left(datename(mm,@RunDate),3))
	set @result = replace(@result,'<PM>',left(datename(mm,dateadd(mm,-1,@RunDate)),3))
	set @result = replace(@result,'<RD>',convert(varchar(10), @RunDate, 101))
	return(@result)
end	

GO





insert
into	TimeTypes
select	6, 'Custom'
go

delete from Period_D
go

insert
into	Period_D (PeriodID, Description, TimeTypeCD, DurationCount, Rank)
select 0 ,'N/A', NULL,NULL,0
union select 1 ,'1 Week', 2,1,0
union select 2 ,'2 Weeks', 2,2,0
union select 3 ,'4 Weeks', 2,4,0
union select 4 ,'6 Weeks', 2,6,0
union select 5 ,'8 Weeks', 2,8,0
union select 6 ,'12 Weeks', 2,12,0
union select 7 ,'26 Weeks', 2,26,0
union select 8 ,'<CM> <CY> MTD', 6,NULL,5
union select 9 ,'<CM> <PY> MTD', 6,NULL,6
union select 10 ,'<CY> YTD', 6,NULL,4
union select 11 ,'2 week ', 6,2,1
union select 12 ,'4 week', 6,4,2
union select 13 ,'6 week', 6,6,3
union select 14 ,'Prior Month (<PM>)', 6,NULL,7

go


exec Rebuild_Aggregates