/*

Adding this index should speed up a recurring nightmare query

*/

CREATE nonclustered INDEX IX_AuctionTransaction_F__BodyStyleID_SeriesID
 on AuctionTransaction_F (BodyStyleID, SeriesID)
 with fillfactor = 95
