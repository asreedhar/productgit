TRUNCATE TABLE AuctionTransaction_A1

ALTER TABLE AuctionTransaction_A1 DROP CONSTRAINT PK_AuctionTransaction_A1
ALTER TABLE AuctionTransaction_A1 DROP CONSTRAINT DF_AuctionTransaction_A1_ModelYear
ALTER TABLE AuctionTransaction_A1 DROP CONSTRAINT FK_AuctionTransaction_A1_SeriesBodyStyle_D

ALTER TABLE AuctionTransaction_A1 DROP COLUMN SeriesBodyStyleID

EXEC sp_rename 'AuctionTransaction_A1.ModelYear', 'VIC','COLUMN'
ALTER TABLE AuctionTransaction_A1 ALTER COLUMN VIC CHAR(10) NOT NULL

ALTER TABLE AuctionTransaction_A1 ADD CONSTRAINT PK_AuctionTransaction_A1 PRIMARY KEY CLUSTERED (PeriodID, VIC, AreaID)

TRUNCATE TABLE AuctionTransaction_A2

ALTER TABLE AuctionTransaction_A2 DROP CONSTRAINT PK_AuctionTransaction_A2
ALTER TABLE AuctionTransaction_A2 DROP CONSTRAINT FK_AuctionTransaction_A2_SeriesBodyStyle_D
ALTER TABLE AuctionTransaction_A2 DROP CONSTRAINT DF_AuctionTransaction_A2_ModelYear
ALTER TABLE AuctionTransaction_A2 DROP COLUMN SeriesBodyStyleID

EXEC sp_rename 'AuctionTransaction_A2.ModelYear', 'VIC','COLUMN'
ALTER TABLE AuctionTransaction_A2 ALTER COLUMN VIC CHAR(10) NOT NULL

ALTER TABLE AuctionTransaction_A2 ADD CONSTRAINT PK_AuctionTransaction_A2 PRIMARY KEY CLUSTERED (PeriodID, VIC, AreaID, LowMileageRange, HighMileageRange)

GO


if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AuctionTransaction_A3]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].AuctionTransaction_A3
GO

CREATE TABLE dbo.AuctionTransaction_A3 (
	PeriodID 		TINYINT NOT NULL ,
	MakeModelGroupingID	INT NOT NULL ,
	ModelYear 		SMALLINT NOT NULL,
	AreaID 			TINYINT NOT NULL ,
	HighSalePrice		DECIMAL(8, 2) NOT NULL ,
	AveSalePrice		DECIMAL(8, 2) NOT NULL ,
	LowSalePrice		DECIMAL(8, 2) NOT NULL ,
	HighMileage		DECIMAL(18, 0) NOT NULL ,
	AveMileage		DECIMAL(18, 0) NOT NULL ,
	LowMileage		DECIMAL(18, 0) NOT NULL ,
	TransactionCount 	INT NOT NULL,
	CONSTRAINT PK_AuctionTransaction_A3 PRIMARY KEY CLUSTERED 
	(
		PeriodID,
		MakeModelGroupingID,
		ModelYear,
		AreaID
	) WITH  FILLFACTOR = 100,
) 
GO
if exists (select * from dbo.sysobjects where id = object_id(N'dbo.AuctionTransaction_A4') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.AuctionTransaction_A4
GO

CREATE TABLE dbo.AuctionTransaction_A4 (
	PeriodID 		TINYINT NOT NULL ,
	MakeModelGroupingID	INT NOT NULL ,
	ModelYear 		SMALLINT NOT NULL,
	AreaID 			TINYINT NOT NULL ,
	LowMileageRange 	INT NOT NULL ,
	HighMileageRange 	INT NOT NULL ,
	AveSalePrice 		DECIMAL(8, 2) NULL ,
	CONSTRAINT PK_AuctionTransaction_A4 PRIMARY KEY  CLUSTERED 
	(
		PeriodID,
		MakeModelGroupingID,
		ModelYear,
		AreaID,
		LowMileageRange,
		HighMileageRange
	) WITH  FILLFACTOR = 100,
) 
GO

CREATE INDEX IX_AuctionTransaction_F__VICRegionTimeID ON AuctionTransaction_F(VIC, RegionID, TimeID)
GO