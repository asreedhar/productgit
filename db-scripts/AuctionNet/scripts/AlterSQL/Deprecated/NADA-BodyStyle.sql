----------------------------------------------------------------------------------------
--	Add OriginalBodyStyleID to the staging and fact tables; this will store 
--	the nAAA BodyStyle.  "BodyStyle" is now sourced from NADA or nAAA, with
--	priority to NADA
----------------------------------------------------------------------------------------

ALTER TABLE AuctionTransaction_F ADD OriginalBodyStyleID INT NOT NULL CONSTRAINT DF_AuctionTransaction_F_OriginalBodyStyleID DEFAULT (0)
GO

ALTER TABLE AuctionTransaction_FS ADD OriginalBodystyle VARCHAR(50) NULL
GO

----------------------------------------------------------------------------------------
--	Set the "Original" (nAAA) BodyStyle ID in the fact table
----------------------------------------------------------------------------------------

UPDATE	AuctionTransaction_F
SET	OriginalBodyStyleID = BodyStyleID

GO
----------------------------------------------------------------------------------------
--	Create the NADA lookup table
----------------------------------------------------------------------------------------

CREATE TABLE #NADA (VIC CHAR(10), Body_Desc VARCHAR(50), BodyStyleID INT NULL)

INSERT
INTO	#NADA
SELECT	DISTINCT VIC, body_desc, NULL
FROM	VehicleUC..vin_vinprefix VP
  	JOIN VehicleUC..vehicle_desc_codes VC ON VP.VIC_Make + VP.VIC_Year + VP.VIC_Series + VP.VIC_Body = VC.vic
	JOIN VehicleUC..vehicle_make M ON VC.make_code = M.make_code
	JOIN VehicleUC..vehicle_series S on VC.series_code = S.series_code
	JOIN VehicleUC..vehicle_bodies B on VC.body_code = B.body_code

CREATE INDEX #IX_NADA ON #NADA(VIC)	-- probably not necessary since only 15k rows

----------------------------------------------------------------------------------------
--	We will need to make sure that BodyStyle_D is fully populated from NADA
--	for all facts before updating BodyStyleID
----------------------------------------------------------------------------------------

INSERT
INTO	BodyStyle_D (BodyStyle)

SELECT	DISTINCT NADA.Body_Desc
FROM	AuctionTransaction_F F
	JOIN #NADA NADA ON F.VIC = NADA.VIC
WHERE	NOT EXISTS (	SELECT	1
			FROM	BodyStyle_D S
			WHERE	isnull(nullif(NADA.Body_Desc,''), 'UNKNOWN')  = S.BodyStyle
			)
UPDATE 	NADA	
SET	BodyStyleID = BS.BodyStyleID
FROM	#NADA NADA
	JOIN BodyStyle_D BS ON NADA.Body_Desc = BS.BodyStyle


ALTER TABLE dbo.AuctionTransaction_F DROP CONSTRAINT FK_AuctionTransaction_F_BodyStyle_D 	
GO

DROP INDEX AuctionTransaction_F.IX_AuctionTransaction_F__BodyStyleID_SeriesID
GO

UPDATE	F
SET	BodyStyleID = NADA.BodyStyleID
FROM	AuctionTransaction_F F
	JOIN #NADA NADA ON F.VIC = NADA.VIC


ALTER TABLE dbo.AuctionTransaction_F ADD CONSTRAINT FK_AuctionTransaction_F_BodyStyle_D FOREIGN KEY (BodyStyleID) REFERENCES dbo.BodyStyle_D(BodyStyleID)
GO

CREATE  INDEX IX_AuctionTransaction_F__BodyStyleID_SeriesID ON dbo.AuctionTransaction_F(BodyStyleID, SeriesID) WITH  FILLFACTOR = 100 
GO

--------------------------------------------------------------------------------------------
--	WE ARE IN FULL CONTROL OF THE AGGREGATE LOADING, SO FK'S ARE NOT NECESSARY
--------------------------------------------------------------------------------------------

ALTER TABLE dbo.AuctionTransaction_A1 DROP CONSTRAINT FK_AuctionTransaction_A1_SeriesBodyStyle_D 
ALTER TABLE dbo.AuctionTransaction_A1 DROP CONSTRAINT FK_AuctionTransaction_F_Period_D 
ALTER TABLE dbo.AuctionTransaction_A1 DROP CONSTRAINT FK_AuctionTransaction_F_Region_D 

ALTER TABLE dbo.AuctionTransaction_A2 DROP CONSTRAINT FK_AuctionTransaction_A2_Period_D 
ALTER TABLE dbo.AuctionTransaction_A2 DROP CONSTRAINT FK_AuctionTransaction_A2_Region_D 
ALTER TABLE dbo.AuctionTransaction_A2 DROP CONSTRAINT FK_AuctionTransaction_A2_SeriesBodyStyle_D 
GO

----------------------------------------------------------------------------------------
--	Now that we have BodyStyle_D updated, we need to rebuild any related 
--	dimensions
----------------------------------------------------------------------------------------

TRUNCATE TABLE SeriesBodyStyle_DXM

DELETE FROM SeriesBodyStyle_D

INSERT
INTO	SeriesBodyStyle_D (SeriesID, BodyStyleID)
SELECT 	DISTINCT  F.SeriesID, F.BodyStyleID
FROM 	AuctionTransaction_F F

--------------------------------------------------------------------------------

INSERT
INTO	SeriesBodyStyle_DXM (SeriesBodyStyleID, MakeModelGroupingID, ModelYear)
SELECT 	DISTINCT SBS.SeriesBodyStyleID, ISNULL(DSV.MakeModelGroupingID,0), ISNULL(DSV.Year, 0)
FROM 	AuctionTransaction_F F
	JOIN SeriesBodyStyle_D SBS on F.SeriesID = SBS.SeriesID and F.BodyStyleID = SBS.BodyStyleID
	LEFT JOIN IMT..DecodedSquishVins DSV on IMT.dbo.fn_SquishVIN(VIN) = DSV.Squish_VIN

--------------------------------------------------------------------------------

EXEC Rebuild_Aggregates
