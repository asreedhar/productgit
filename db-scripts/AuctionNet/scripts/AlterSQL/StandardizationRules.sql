if exists (select * from dbo.sysobjects where id = object_id(N'dbo.StandardizationRules') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table dbo.StandardizationRules
GO
CREATE TABLE dbo.StandardizationRules (
	RuleID int, 
	Search varchar(50), 
	Replacement varchar(50),
	Delim varchar(1)
) 
GO
