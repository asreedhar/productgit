--------------------------------------------------------------------------------
--	REMOVE ANY OUTDATED ENTRIES STILL IN 'DXM DUE TO THE INC. LOAD STRATEGY
--	AND THE FACT THAT THE VC CAN DECODE TO DIFFERENT MMGs FROM ONE LOAD TO
--	THE NEXT (STANDARDIZATION RULES, ERROR CORRECTIONS, ETC.)		
--
--	NOTE WE NEED TO UPDATE THE LOADER IN dbo.Process_Datafile TO REFLECT
--	THIS REALITY.  NEXT RELEASE :-)
--
--------------------------------------------------------------------------------

SELECT	DISTINCT
        SBS.SeriesBodyStyleID,
        ISNULL(VC.MakeModelGroupingID, 0) MakeModelGroupingID,
        ISNULL(VC.ModelYear, 0) ModelYear
INTO    #SeriesBodyStyle_DXM
FROM    AuctionTransaction_F T
        JOIN SeriesBodyStyle_D SBS ON T.SeriesID = SBS.SeriesID
                                      AND T.BodyStyleID = SBS.BodyStyleID
        LEFT JOIN VehicleCatalog VC ON VC.CountryCode = 1
                                       AND IMT.dbo.GetSquishVIN(T.VIN) = VC.SquishVIN
                                       AND T.VIN LIKE VC.VINPattern
        LEFT JOIN VehicleCatalogVINPatternPriority VPP ON VPP.CountryCode = 1
                                                          AND VC.VINPattern = VPP.VINPattern
                                                          AND T.VIN LIKE VPP.PriorityVINPattern
WHERE   VPP.PriorityVINPattern IS NULL


INSERT
INTO	dbo.SeriesBodyStyle_DXM (SeriesBodyStyleID, MakeModelGroupingID, ModelYear)
SELECT	SeriesBodyStyleID, MakeModelGroupingID, ModelYear
FROM	#SeriesBodyStyle_DXM X
WHERE	NOT EXISTS (	SELECT	1
			FROM	SeriesBodyStyle_DXM T
			WHERE	X.SeriesBodyStyleID = T.SeriesBodyStyleID
				AND X.ModelYear = T.ModelYear
				AND x.MakeModelGroupingID = T.MakeModelGroupingID
				)

DELETE	X
FROM	SeriesBodyStyle_DXM X
WHERE	NOT EXISTS (	SELECT	1
			FROM	#SeriesBodyStyle_DXM T
			WHERE	X.SeriesBodyStyleID = T.SeriesBodyStyleID
				AND X.ModelYear = T.ModelYear
				AND X.MakeModelGroupingID = T.MakeModelGroupingID
				)