
insert
into	StandardizationRules (RuleID, Search, Replacement, Delim)
select	1,'CONV','CONVERTIBLE',' ' 
union
select	2,'CPE','COUPE',''
union
select	3,'RDSTR','ROADSTER',''
union
select	4,'EXT.','EXTENDED',''
union
select	5,'EXT','EXTENDED',' '
union
select	6,'DLX','DELUXE',''
union
select	7,'HBK','HATCHBACK',''
union
select	8,'HTOP','HARDTOP',''
union
select	9,'LBK','LIFTBACK',''
union
select	10,'LTD.','LIMITED',''
union
select	11,'LTD','LIMITED',''
union
select	12,'PKP','PICKUP',''
union
select	13,'SPRCHR','SUPERCHARGER',''
union
select	14,'SED','SEDAN',' '
union
select	15,'SILV.','SILVERADO',''
union
select	16,'UTIL','UTILITY',' '
union
select	17,'WGN','WAGON',''
union	
select	18,'DSL','DIESEL',''

--------------------------------------------------------------------------------
--	ALTER TABLES SINCE BODY STYLE MAY TAKE UP MORE SPACE
--------------------------------------------------------------------------------

alter table BodyStyle_D alter column BodyStyle varchar(50)
go

alter table AuctionTransaction_FS alter column BodyStyle varchar(50)
go

create table #StandardizedBodyStyle_D (BodyStyleID int, StandardizedBodyStyle varchar(50))

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

insert
into	#StandardizedBodyStyle_D
select	distinct BodyStyleID,
	dbo.StandardizeValue(BS.BodyStyle) StandardizedBodyStyle	
from	BodyStyle_D BS

--------------------------------------------------------------------------------
--	UPDATE BODY STYLE DIMENSION WITH NEW STANDARDIZED VALUES
--------------------------------------------------------------------------------

insert
into	BodyStyle_D (BodyStyle)
select 	distinct left(SBS.StandardizedBodyStyle,50)
from 	#StandardizedBodyStyle_D SBS
where	not exists (	select	1
			from	BodyStyle_D S
			where	SBS.StandardizedBodyStyle = S.BodyStyle
			)

------------------------------------------------------------------------------------------------
--
------------------------------------------------------------------------------------------------

create table  #BodyStyleMapping (BodyStyleID int, NewBodyStyleID int)

insert
into	#BodyStyleMapping 

select	distinct BS.BodyStyleID OldBodyStyleID, NBS.BodyStyleID NewBodyStyleID
from	BodyStyle_D BS
	join #StandardizedBodyStyle_D SBS on SBS.BodyStyleID = BS.BodyStyleID
	join BodyStyle_D NBS on SBS.StandardizedBodyStyle = NBS.BodyStyle
where	BS.BodyStyleID <> NBS.BodyStyleID

--------------------------------------------------------------------------------
--	UPDATE THE FACT TABLE
--------------------------------------------------------------------------------

update	T
set	T.BodyStyleID = BSM.NewBodyStyleID
from 	AuctionTransaction_F T
	join #BodyStyleMapping BSM on T.BodyStyleID = BSM.BodyStyleID

--------------------------------------------------------------------------------
--	REBUILD THE SERIES-BODYSTYLE INTERSECTION DIMENSION
--------------------------------------------------------------------------------
truncate table AuctionTransaction_A1
truncate table AuctionTransaction_A2
truncate table SeriesBodyStyle_DXM

delete from  SeriesBodyStyle_D

insert
into	SeriesBodyStyle_D (SeriesID, BodyStyleID)
select 	distinct  SeriesID, BodyStyleID
from 	AuctionTransaction_F T

--------------------------------------------------------------------------------
--	REBUILD THE SBS->MMG MAPPING TABLE
--------------------------------------------------------------------------------

insert
into	SeriesBodyStyle_DXM (SeriesBodyStyleID, MakeModelGroupingID, ModelYear)

select 	distinct SBS.SeriesBodyStyleID, isnull(DSV.MakeModelGroupingID,0), isnull(DSV.Year, 0)
from 	AuctionTransaction_F T
	join SeriesBodyStyle_D SBS on T.SeriesID = SBS.SeriesID and T.BodyStyleID = SBS.BodyStyleID
	left join IMT..DecodedSquishVins DSV on IMT.dbo.fn_SquishVIN(VIN) = DSV.Squish_VIN

------------------------------------------------------------------------------------------------
--	NOW REBUILD THE AGGREGATES
------------------------------------------------------------------------------------------------

exec Rebuild_Aggregates