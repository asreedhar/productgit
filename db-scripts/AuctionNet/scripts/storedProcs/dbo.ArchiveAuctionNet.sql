SET ANSI_NULLS ON
GO

IF objectproperty(object_id('ArchiveAuctionNet'), 'isProcedure') = 1
    DROP PROCEDURE ArchiveAuctionNet

GO
CREATE PROC dbo.ArchiveAuctionNet
----------------------------------------------------------------------------------------------------
--
--	Moves AuctionNet (nAAA) transaction history to the archive database.  We don't need to 
--	worry about the dimensions as they are relatively small.
--
---Parameters---------------------------------------------------------------------------------------
--
@AuctionTransactionID	INT = NULL,
@MaxToArchive   	INT = 0
--
---History------------------------------------------------------------------------------------------
--	
--	WGH	02/13/2007	Initial design/development (as part of HAL rollout)
	
----------------------------------------------------------------------------------------------------
AS
SET NOCOUNT ON

DECLARE @err INT

DECLARE @AuctionTransactionIDs TABLE (AuctionTransactionID INT)


SET ROWCOUNT @MaxToArchive

INSERT
INTO	@AuctionTransactionIDs
SELECT	AuctionTransactionID
FROM	AuctionTransaction_F F
	JOIN Time_D T ON F.TimeID = T.TimeID
WHERE	T.BeginDate < (SELECT MIN(BeginDate) FROM PeriodDates)
	AND @AuctionTransactionID IS NULL
UNION
SELECT	@AuctionTransactionID
WHERE	@AuctionTransactionID IS NOT NULL

SET ROWCOUNT 0

----------------------------------------------------------------------------------------------------

INSERT
INTO	Archive..AuctionTransaction_F_Archive (AuctionTransactionID, TimeID, SeriesID, RegionID, SaleTypeID, DieselIdentifierID, BodyStyleID, EngineID, TransmissionID, SalePrice, Mileage, VID, VIC, MultipleVICFlag, Make, ModelYear, Submake, Bodystyle, VIN, LoadID, OriginalBodyStyleID)

SELECT	F.AuctionTransactionID, TimeID, SeriesID, RegionID, SaleTypeID, DieselIdentifierID, BodyStyleID, EngineID, TransmissionID, SalePrice, Mileage, VID, VIC, MultipleVICFlag, Make, ModelYear, Submake, Bodystyle, VIN, LoadID, 0
FROM	dbo.AuctionTransaction_F F
	JOIN @AuctionTransactionIDs FID ON F.AuctionTransactionID = FID.AuctionTransactionID	

SET @err = @@ERROR
IF @err <> 0 GOTO Failed

DELETE	F
FROM	dbo.AuctionTransaction_F F
	JOIN @AuctionTransactionIDs FID ON F.AuctionTransactionID = FID.AuctionTransactionID


SET @err = @@ERROR
IF @err <> 0 GOTO Failed


RETURN 0

Failed:

RETURN @err
GO

