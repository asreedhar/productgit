
IF NOT EXISTS (SELECT 1 FROM sys.server_principals WHERE type_desc = 'SQL_LOGIN' AND name = N'MerchandisingWebsite')
	CREATE LOGIN [MerchandisingWebsite] WITH PASSWORD = 'XAw3r62#CREs', DEFAULT_DATABASE= Merchandising

USE Merchandising 

EXEC sp_change_users_login @Action = "Auto_Fix", @UserNamePattern = 'MerchandisingWebsite'
GO

USE [VehicleCatalog]
GO
CREATE USER [MerchandisingWebsite] FOR LOGIN [MerchandisingWebsite]
ALTER USER [MerchandisingWebsite] WITH DEFAULT_SCHEMA=[Merchandising]

EXEC sp_addrolemember N'db_datareader', N'MerchandisingWebsite'
GO
