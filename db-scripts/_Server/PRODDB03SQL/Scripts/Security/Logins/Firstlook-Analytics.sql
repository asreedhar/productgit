IF NOT EXISTS (	SELECT	1
		FROM	sys.server_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'Firstlook\Analytics'
		)

	CREATE LOGIN [FIRSTLOOK\Analytics] FROM WINDOWS WITH DEFAULT_DATABASE=Datafeeds

GO

USE Datafeeds
GO

IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'Firstlook\Analytics'
		)
	CREATE USER [FIRSTLOOK\Analytics] FOR LOGIN [FIRSTLOOK\Analytics] 
	
GO


EXEC sp_addrolemember N'db_datareader', N'FIRSTLOOK\Analytics'
GO
	