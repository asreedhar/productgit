IF NOT EXISTS (	SELECT	1
		FROM	sys.server_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'Firstlook\DMSi#DataloadAdmin'
		)

	CREATE LOGIN [FIRSTLOOK\DMSi#DataloadAdmin] FROM WINDOWS WITH DEFAULT_DATABASE=[Datafeeds]

GO


EXEC sp_map_exec

'USE [?]

IF NOT EXISTS (	SELECT	*
		FROM	sys.database_principals
		WHERE	type_desc = "WINDOWS_GROUP"
			AND name = "Firstlook\DMSi#DataloadAdmin"
		)
			
	CREATE USER [FIRSTLOOK\DMSi#DataloadAdmin] FOR LOGIN [FIRSTLOOK\DMSi#DataloadAdmin]
	
EXEC sp_addrolemember N"db_datawriter", N"FIRSTLOOK\DMSi#DataloadAdmin"
EXEC sp_addrolemember N'db_datareader', N'FIRSTLOOK\DMSi#DataloadAdmin'
',
'
SELECT	name
FROM	sys.databases
WHERE	name not in ("master","tempdb","model","msdb") and is_read_only = 0
'

GO

USE master
GRANT VIEW ANY DEFINITION TO [Firstlook\DMSi#DataloadAdmin]
GO

USE [msdb]
EXEC sp_addrolemember N'SQLAgentReaderRole', N'FIRSTLOOK\DMSi#DataloadAdmin'
EXEC sp_addrolemember N'SQLAgentUserRole', N'FIRSTLOOK\DMSi#DataloadAdmin'
GO
GO

