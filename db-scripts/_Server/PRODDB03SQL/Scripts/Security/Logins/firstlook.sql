--
-- Script Login, If Necessary
--

IF NOT EXISTS
(
SELECT	1
FROM	sys.server_principals
WHERE	type_desc = 'SQL_LOGIN'
AND	NAME='firstlook'
)

CREATE LOGIN [firstlook] WITH PASSWORD='7Tk#uD{qM]28', DEFAULT_DATABASE=[Lot], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
