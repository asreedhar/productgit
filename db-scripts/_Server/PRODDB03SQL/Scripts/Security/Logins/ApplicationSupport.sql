----------------------------------------------------------------------------------------
--	CREATE LOGIN
----------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.server_principals WHERE name = N'FIRSTLOOK\ApplicationSupport')

	CREATE LOGIN [FIRSTLOOK\ApplicationSupport] FROM WINDOWS WITH DEFAULT_DATABASE=[Datafeeds]


----------------------------------------------------------------------------------------
--	CREATE USER, ROLE, AND ASSIGN db_datareader, DataManagement (if available)
----------------------------------------------------------------------------------------


EXEC sp_map_exec 
'
USE [?]

IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = "WINDOWS_GROUP"
			AND name = "Firstlook\ApplicationSupport"
		)			
	CREATE USER [FIRSTLOOK\ApplicationSupport] FOR LOGIN [FIRSTLOOK\ApplicationSupport]


IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = "DATABASE_ROLE"
			AND name = "ApplicationSupportManager"
		)

	CREATE ROLE [ApplicationSupportManager] AUTHORIZATION [dbo]

EXEC sp_addrolemember N"db_datareader", N"FIRSTLOOK\ApplicationSupport"

',
'SELECT	name
FROM	master.sys.databases
WHERE	name not in ("IMT_Archive", "master","tempdb","model","msdb") 
	and is_read_only = 0
	
',0


USE master

GRANT VIEW ANY DEFINITION TO [FIRSTLOOK\ApplicationSupport]

GO	
