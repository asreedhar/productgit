DECLARE @RestorePoint VARCHAR(12)

SET @RestorePoint = '201001110735'		-- select a restore point so there is at least one unapplied t-log file
						-- so the call to dbo.RunInformalLogShipping properly sets things.

EXEC master..sp_ComprehensiveRestore_SLS

@SourceDatabase = 'IMT', 
@TargetDatabase = 'IMT_Copy', 
@FinalState = 2, 
@CompleteBackupFolder = '\\FIRSTLOOK.BIZ\DataArchive\DatabaseBackups\PRODDB01SQL\IMT', 
@DifferentialBackupFolder = '\\FIRSTLOOK.BIZ\DataArchive\DatabaseBackups\PRODDB01SQL\IMT',
@TransactionBackupFolder  = '\\FIRSTLOOK.BIZ\DataArchive\DatabaseBackups\PRODDB01SQL\IMT',
@StandbyFile = 'H:\SQL_DataFiles\IMT_copy_standby.stb',
@FileLocations =  
', move "Data1" to "H:\SQL_Datafiles\IMT_Data1.mdf"
, move "Log" to "I:\SQL_Datafiles\IMT_Log.ldf"
, move "Log2" to "I:\SQL_Datafiles\IMT_Log2.ldf"
, move "Data2" to "H:\SQL_Datafiles\IMT_Data2.ndf"
, move "IDX1" to "H:\SQL_Datafiles\IMT_IDX1.ndf"
',
@Replace = 1,
@Live = 1,
@Latest =  @RestorePoint

UPDATE	InformalLogShipping
SET	LastLogFileDateTime =  @RestorePoint
WHERE	DBName = 'IMT_Copy'
GO

EXECUTE dbo.RunInformalLogShipping 'IMT_copy', 2, 1

GO