/*

Add database mail profile for SQL Agent

Got two different "models" in here:
 - One for the 815 VanBuren, with one profile account
 - One for the colo site, with two profile accounts



Yo, something out there enables/disables SQL Agent's use of SQL Mail.  Dunno what nor
where; check by right-clicking the SQL Agent node, look under "Alert System", and mess
with the "Mail session" settings.


--  Undo
EXECUTE msdb.dbo.sysmail_delete_profile_sp
 @profile_name = 'SQL Agent Email Profile'

*/

DECLARE
  @ReturnValue   int
 ,@TheirID       int
 ,@Location     tinyint

SET @Location = 1
  --  1 = 815 Van Buren
  --  2 = Colo site

-------------------------------------------------------------------------------


EXECUTE @ReturnValue = msdb.dbo.sysmail_add_profile_sp
  @profile_name = 'SQL Agent Email Profile'
 ,@description  = 'Database mail profile used by SQL Agent'
 ,@profile_id   = @TheirID OUTPUT


PRINT '--  SQL Agent Email Profile  -------------------------------------'
PRINT @ReturnValue
PRINT @TheirID


PRINT '--  Configuring accounts in profile  -----------------------------'

IF @Location = 1
    --  815 Van Buren
    EXECUTE @ReturnValue = msdb.dbo.sysmail_add_profileaccount_sp
      @profile_name = 'SQL Agent Email Profile'
     ,@account_name = 'SQL Agent/Office SMTP'
     ,@sequence_number = 1
    PRINT @ReturnValue

IF @Location = 2
 BEGIN
    --  Colo
    EXECUTE @ReturnValue = msdb.dbo.sysmail_add_profileaccount_sp
      @profile_name = 'SQL Agent Email Profile'
     ,@account_name = 'SQL Agent/CoLo SMTP (primary)'
     ,@sequence_number = 1
    PRINT @ReturnValue

    EXECUTE @ReturnValue = msdb.dbo.sysmail_add_profileaccount_sp
      @profile_name = 'SQL Agent Email Profile'
     ,@account_name = 'SQL Agent/CoLo SMTP (secondary)'
     ,@sequence_number = 2
    PRINT @ReturnValue
 END


PRINT '--  Grant access rights  -------------------------------------'

EXECUTE @ReturnValue = msdb.dbo.sysmail_add_principalprofile_sp
  @principal_name = 'dbo'
 ,@profile_name   = 'SQL Agent Email Profile'
 ,@is_default     = 1

PRINT @ReturnValue

PRINT 'Stop and restart SQL Agent for changes to take effect'
