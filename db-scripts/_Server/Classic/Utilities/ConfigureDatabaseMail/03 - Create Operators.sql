/*

Set up default operators on a box

*/

IF (SELECT COUNT(*) FROM msdb.dbo.sysoperators WHERE name = N'DBA - Pager') < 1 
    EXECUTE msdb.dbo.sp_add_operator
      @name                      = N'DBA - Pager'
     ,@enabled                   = 1
     ,@email_address             = N'1073725@skytel.com'
     ,@category_name             = N'[Uncategorized]'
     --  Every day, all day long
     ,@weekday_pager_start_time  = 0
     ,@weekday_pager_end_time    = 0
     ,@saturday_pager_start_time = 0
     ,@saturday_pager_end_time   = 0
     ,@sunday_pager_start_time   = 0
     ,@sunday_pager_end_time     = 0
     ,@pager_days                = 127
ELSE
    PRINT 'Operator "DBA - Pager" already present on server'
