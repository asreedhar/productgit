/*

Create database mail accounts:
 - SQL Agent/Office SMTP
 - SQL Agent/CoLo SMTP (primary)
 - SQL Agent/CoLo SMTP (secondary)

Got two different "models" in here:
 - One for the 815 VanBuren, using webmail.incisent.com as the mail server
 - One for the colo site, using ord4mail01.firstlook.biz as the primary
    and ord1util01.firstlook.biz as the secondary mail server

*/

DECLARE
  @ReturnValue  int
 ,@TheirID      int
 ,@Location     tinyint


SET @Location = 1
  --  1 = 815 Van Buren
  --  2 = Colo site

-------------------------------------------------------------------------------

IF @Location = 1
 BEGIN
    --  815 Van Buren
    EXECUTE @ReturnValue = msdb.dbo.sysmail_add_account_sp
      @account_name  =  'SQL Agent/Office SMTP'
     ,@email_address =  'DummyAccount_Prod01@Incisent.com'
     ,@display_name  =  'SQLAgent'
     ,@replyto_address =  'DummyAccount_Prod_Prod02@Incisent.com'
     ,@description =  'SMTP Server at 815 Van Buren'
     ,@mailserver_name =  'webmail.incisent.com' 
     ,@port = 25
     ,@enable_ssl = 0
     ,@account_id = @TheirID  OUTPUT

    PRINT '--  SQL Agent/Office SMTP  -------------------------------------'
    PRINT @ReturnValue
    PRINT @TheirID
 END

IF @Location = 2
 BEGIN
    --  Colo site
    EXECUTE @ReturnValue = msdb.dbo.sysmail_add_account_sp
      @account_name  =  'SQL Agent/CoLo SMTP (primary)'
     ,@email_address =  'DummyAccount_Prod01@Incisent.com'
     ,@display_name  =  'SQLAgent'
     ,@replyto_address =  'DummyAccount_PreProd@Incisent.com'
     ,@description =  'Primary SMTP Server at the CoLo site'
     ,@mailserver_name =  'ord4mail01.firstlook.biz'
     ,@port = 25
     ,@enable_ssl = 0
     ,@account_id = @TheirID  OUTPUT

    PRINT '--  SQL Agent/CoLo SMTP (primary)  -----------------------------'
    PRINT @ReturnValue
    PRINT @TheirID

    EXECUTE @ReturnValue = msdb.dbo.sysmail_add_account_sp
      @account_name  =  'SQL Agent/CoLo SMTP (secondary)'
     ,@email_address =  'DummyAccount_PreProd@Incisent.com'
     ,@display_name  =  'SQLAgent'
     ,@replyto_address =  'DummyAccount_Prod01@Incisent.com'
     ,@description =  'Secondary SMTP Server at the CoLo site'
     ,@mailserver_name =  'ord1util01.firstlook.biz'
     ,@port = 25
     ,@enable_ssl = 0
     ,@account_id = @TheirID  OUTPUT

    PRINT '--  SQL Agent/CoLo SMTP (secondary)  ---------------------------'
    PRINT @ReturnValue
    PRINT @TheirID
 END
