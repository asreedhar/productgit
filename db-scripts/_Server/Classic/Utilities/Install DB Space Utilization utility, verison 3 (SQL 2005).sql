/*

Code used to create a system to track the size and internal space utilization of
the databases and tables on a server.  Objects created:
 - Table master.dbo.DatabaseSizeLog_2005
     (Will not create if already exists)
 - Table master.dbo.DatabaseTableSizeLog_2005
     (Will not create if already exists)
 - Stored Procedure master.dbo.sp_CalcDatabaseSpaceUsed_2005
     (Will recreate if already exists)
 - Stored Procedure master.dbo.sp_CalcDatabaseSpaceUsed_Tables_2005
     (Will recreate if already exists)
 - SQL Agent job "_Update Database Sizing Log"
     (Will not create if already exists)
     (Created disabled)
     (Set to run daily at 5:01am)


--  Uninstall script (clean out everything):
EXECUTE msdb.dbo.sp_delete_job @job_name = '_Update Database Sizing Log'

USE master
DROP PROCEDURE sp_CalcDatabaseSpaceUsed_2005
DROP PROCEDURE sp_CalcDatabaseSpaceUsed_Tables_2005
 
DROP TABLE dbo.DatabaseSizeLog_2005
DROP TABLE dbo.DatabaseTableSizeLog_2005


-----------------------------------------------------------------------------------------
Version 1.0 released October 20, 2003  Philip Kelley
Updated May 21, 2004: Set job step to retry 7 times ever 15 minutes
Updated May 17, 2005: Revised to "generic" object names

Version 2.0 released September 13, 2006  Philip Kelley  (Added table space tracking)

Version 3.0 released June 2007  Philip Kelley  (Revised for SQL 2005)

*/

--  Step 1: Create the tables  ----------------------------------------------------------
USE master

IF object_id('dbo.DatabaseSizeLog_2005') is null
 BEGIN
    --  Create the tables, but only if they doen't already exist
    CREATE TABLE dbo.DatabaseSizeLog_2005
     (
       DBName        sysname        not null
      ,AsOf          smalldatetime  not null
      ,FileType      varchar(4)     not null
      ,LogicalName   sysname        not null
      ,PhysicalName  varchar(200)   not null
      ,FileSize      bigint         not null
      ,Unallocated  as (FileSize - Reserved)
      ,Reserved      bigint         not null
      ,TotalUsed    as (DataUsed + LobUsed + IndexUsed)
      ,DataUsed      bigint         not null
      ,LobUsed       bigint         not null
      ,IndexUsed     bigint         not null
      ,Unused       as (Reserved - DataUsed - LobUsed - IndexUsed)
      ,constraint PK_DatabaseSizeLog_2005
        primary key nonclustered (DBName, AsOf, LogicalName)
     )

    CREATE clustered INDEX IX_DatabaseSizeLog_2005__AsOf
     on DatabaseSizeLog_2005 (AsOf)
 END


IF object_id('dbo.DatabaseTableSizeLog_2005') is null
 BEGIN
    CREATE TABLE dbo.DatabaseTableSizeLog_2005
     (
       DBName      sysname        not null
      ,AsOf        smalldatetime  not null
      ,TableName   sysname        not null
      ,Rows        bigint         not null
      ,AllPages   as (DataPages + LobPages + IndexPages)
      ,DataPages   bigint         not null
      ,LobPages    bigint         not null
      ,IndexPages  bigint         not null
      ,constraint PK_DatabaseTableSizeLog_2005
        primary key nonclustered (DBName, TableName, AsOf)
     )

    CREATE clustered INDEX IX_DatabaseTableSizeLog_2005__AsOf
     on DatabaseTableSizeLog_2005 (AsOf)
 END


--  Step 2: Create the stored procedure  ------------------------------------------------

--  Create the stored procedures.  If they already exist, delete them, on the assumption
--  that we're updating the code.


IF objectproperty(object_id('sp_CalcDatabaseSpaceUsed_2005'), 'isProcedure') = 1
    DROP PROCEDURE dbo.sp_CalcDatabaseSpaceUsed_2005
IF objectproperty(object_id('sp_CalcDatabaseSpaceUsed_Tables_2005'), 'isProcedure') = 1
    DROP PROCEDURE dbo.sp_CalcDatabaseSpaceUsed_Tables_2005

GO
/******************************************************************************
**
**  Procedure: sp_CalcDatabaseSpaceUsed_2005
**
**  Description: Return a data set with one row for every file (data or log)
**   for every database attached to this SQL instance, containing information
**   on how hard drive space is allocated and used.
**
**
**  Original Description: I took procedure sp_spaceUsed and revised it to produce
**   one data set.  I then crammed it into a "sp_msForEachDB" call, and wrapped
**   that with functionality to load the results from each call into a ##temp
**   table.  The data loaded into this table is returned by this procedure.
**
**   Years later, this was overhauled to work with SQL Server 2005.  It's easier
**   now, you just use the system views for everything... though figuring them
**   out was not simple.  Where originally we returned one row per database, we
**   now return one row per database file (data and log).
**
**
**  Return values:  0
**
**  Input parameters: None
**
**  Output parameters:  None
**
**  Rows returned:
**
**   Column          Datatype        Description
**   --------------  --------------  -------------------------------
**   DBName          sysname         Database name
**   As_Of           smalldatetime   Date and time results are for
**   FileType        varchar(4)      Type of file, either "Data" or "Log"
**   LogicalName     sysname         Logical (internal) file name
**   PhysicalName    sysname         Physical (external) file name
**   FileSize        bigint          Size of the file, in (8k) pages
**   Reserved        bigint          Number of pages allocated for use
**   DataUsed        bigint          Number of pages holding data
**   LobUsed         bigint          Number of pages holding text, ntext,
**                                    image, XML, "large value types",
**                                    and CLR user-defined data types
**   IndexUsed       bigint          Number of pages of index data
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  ??/??/2003  PKelley   Procedure created (as stand alone script)
**  xx/xx/2003  PKelley   Made it a System Procedure (this file)
**  06/05/2007  PKelley   Overhauled and adapted for SQL Server 2005
**
*************************************************************************/
CREATE PROCEDURE dbo.sp_CalcDatabaseSpaceUsed_2005

AS

    --  The results of running the dynamic code against every database are
    --  stored in this table
    IF object_id('tempdb.dbo.##DatabaseSizeLog') is not null
        DROP TABLE ##DatabaseSizeLog
    
    CREATE TABLE ##DatabaseSizeLog
     (
       DBName        sysname        not null
      ,AsOf          smalldatetime  not null
      ,FileType      varchar(4)     not null
      ,LogicalName   sysname        not null
      ,PhysicalName  varchar(200)   not null
      ,FileSize      bigint         not null
      ,Reserved      bigint         not null
      ,DataUsed      bigint         not null
      ,LobUsed       bigint         not null
      ,IndexUsed     bigint         not null
     )

    --  Get database sizing and space usage information for all databases
    EXECUTE sp_msForEachDB '
USE ?

INSERT ##DatabaseSizeLog
  (
    DBName
   ,AsOf
   ,FileType
   ,LogicalName
   ,PhysicalName
   ,FileSize
   ,Reserved
   ,DataUsed
   ,LobUsed
   ,IndexUsed
  )
 select
    ''?''
   ,getdate()
   ,''Data''
   ,df.name
   ,df.physical_name
   ,df.Size
   ,sum(au.total_pages)
   ,sum(case when pa.index_id < 2 and au.type <> 2 then au.used_pages else 0 end)
   ,sum(case when pa.index_id < 2 and au.type = 2 then au.used_pages else 0 end)
   ,sum(case when pa.index_id > 1 then au.used_pages else 0 end)
  from sys.allocation_units au
   inner join sys.partitions pa
    on pa.partition_id = au.container_id
   inner join sys.database_files df
    on df.data_space_id = au.data_space_id
  group by
    df.name
   ,df.physical_name
   ,df.type_desc
   ,df.Size
 UNION SELECT
    ''?''
   ,getdate()
   ,''Log''
   ,df.name
   ,df.physical_name
   ,df.Size
   ,0,0,0,0
  from sys.database_files df
  where df.type_desc = ''LOG''
'
    
    --  Return the results
    SELECT *
     from ##DatabaseSizeLog
     order by DBName

    DROP TABLE ##DatabaseSizeLog

RETURN 0
GO
/******************************************************************************
**
**  Procedure: sp_CalcDatabaseSpaceUsed_Tables_2005
**
**  Description: Return a data set containing space utilization information
**   for the tables in a database.  Data is returned either for one database,
**   or for all database except for tempdb on the SQL instance.  Only tables
**   where sys.tables.is_ms_shipped = 0 (user defined tables, with some
**   exceptions) will be listed.
**
**
**  Original Description: This was slapped together after numerous reviews of SysIndexes.
**   It returns space allocated to a table, including any blob data but NOT index
**   space, for all user tables in all databases (NOT including tempdb).  This is
**   a pretty accurate estimate, but I'm disinclined to fully trust it--I just
**   don't think SysIndexes is 100% accurate, and that's even after DBCC UPDATEUSAGE
**   is run.
**
**
**  Return values:  0
**
**  Input parameters:  See below
**
**  Output parameters:  None
**
**  Rows returned: 
**
**   Column      Datatype       Description
**   ----------  -------------  -------------------------------
**   DBName      sysname        Database name
**   AsOf        smalldatetime  Date and teim results are for
**   TableName   sysname        Table name
**   Rows        bigint         Rows in the table
**   DataPages   bigint         Data pages used by the table
**   LobPages    bigint         Pages used for text, ntext, image, XML,
**                               "large value types", and CLR user-defined
**                               data types
**   IndexPages  bigint         Index pages used by the table
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  09/13/2006  PKelley   Procedure created
**  01/26/2007  PKelley   Added @TargetDB parameter
**  06/06/2007  PKelley   Overhauled and adapted for SQL Server 2005
**
*************************************************************************/
CREATE PROCEDURE dbo.sp_CalcDatabaseSpaceUsed_Tables_2005

    @TargetDB  sysname = null
    --  Pass the database to analyze, or NULL for all databases

AS

    --  The results of running the dynamic code are stored in this table
    IF object_id('tempdb.dbo.##DatabaseTableSizeLog_2005') is not null
        DROP TABLE ##DatabaseTableSizeLog_2005

    CREATE TABLE ##DatabaseTableSizeLog_2005
     (
       DBName      sysname        not null
      ,AsOf        smalldatetime  not null
      ,TableName   sysname        not null
      ,Rows        bigint         not null
      ,DataPages   bigint         not null
      ,LobPages    bigint         not null
      ,IndexPages  bigint         not null
     )


    --  This table is used to pass parameters to the "hit every database" code
    IF object_id('tempdb.dbo.##TargetDBs') is not null
        DROP TABLE ##TargetDBs
    
    CREATE TABLE ##TargetDBs
     (TargetDB  sysname  null)

    INSERT ##TargetDBs (TargetDB)
     values (@TargetDB)


    --  Do the work
    EXECUTE sp_msForEachDB '
IF ''?'' <> ''tempdb''
 BEGIN
    USE ?
    SET NOCOUNT on

    DECLARE @TargetDB sysname

    SELECT @TargetDB = TargetDB
     from ##TargetDBs

    IF ''?'' = isnull(@TargetDB, ''?'')
        --  Load data for this DB
        INSERT ##DatabaseTableSizeLog_2005
          (
            DBName
           ,AsOf
           ,TableName
           ,Rows
           ,DataPages
           ,LobPages
           ,IndexPages
          )
         select
            ''?''
           ,getdate()
           ,ta.name
           ,avg(pa.rows)  --  Approximate value, oh well
           ,sum(case when ind.index_id < 2 and au.type <> 2 then au.total_pages else 0 end)  Data_Pg
           ,sum(case when ind.index_id < 2 and au.type = 2 then au.total_pages else 0 end)   Lob_Pg
           ,sum(case when pa.index_id > 1 then au.total_pages else 0 end)                    Index_Pg
          from sys.tables ta
           inner join sys.indexes ind
            on ind.object_id = ta.object_id
           inner join sys.partitions pa
            on pa.object_id = ta.object_id
             and pa.index_id = ind.index_id
           inner join sys.allocation_units au
            on au.container_id = pa.partition_id
          where ta.is_ms_shipped = 0
          group by
            ta.name
 END
'

    --  Return the results
    SELECT *
     from ##DatabaseTableSizeLog_2005
     order by DBName, TableName

    DROP TABLE ##DatabaseTableSizeLog_2005
    DROP TABLE ##TargetDBs

RETURN 0
GO


--  Step 3: Create the SQL Agent Job  ---------------------------------------------------
BEGIN TRANSACTION            
  DECLARE @JobID BINARY(16)  
  DECLARE @ReturnCode INT    
  SELECT @ReturnCode = 0
IF (SELECT COUNT(*) FROM msdb.dbo.syscategories WHERE name = N'Database Maintenance') < 1 
  EXECUTE msdb.dbo.sp_add_category @name = N'Database Maintenance'
IF (SELECT COUNT(*) FROM msdb.dbo.sysjobs WHERE name = N'_Update Database Sizing Log') > 0 
  PRINT N'The job "_Update Database Sizing Log" already exists so will not be replaced.'
ELSE
BEGIN 

  -- Add the job
  EXECUTE @ReturnCode = msdb.dbo.sp_add_job
    @job_id = @JobID OUTPUT
   ,@job_name = N'_Update Database Sizing Log'
   ,@owner_login_name = N'sa'
   ,@description = N'Jun 2007: SQL 2005, version 3.0
 - Daily database size and space check loaded into master.dbo.DatabaseSizeLog_2005
 - Weekly table row and space check loaded into master.dbo.DatabaseTableSizeLog_2005'
   ,@category_name = N'Database Maintenance'
   ,@enabled = 0
   ,@notify_level_email = 0
   ,@notify_level_page = 0
   ,@notify_level_netsend = 0
   ,@notify_level_eventlog = 2
   ,@delete_level= 0
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

  -- Add the job steps
  EXECUTE @ReturnCode = msdb.dbo.sp_add_jobstep
    @job_id = @JobID
   ,@step_id = 1
   ,@step_name = N'Do the work'
   ,@command = N'--  Scan and load database file utilization data
INSERT master.dbo.DatabaseSizeLog_2005
 (
   DBName
  ,AsOf
  ,FileType
  ,LogicalName
  ,PhysicalName
  ,FileSize
  ,Reserved
  ,DataUsed
  ,LobUsed
  ,IndexUsed
 )
 execute master.dbo.sp_CalcDatabaseSpaceUsed_2005

IF datename(dw, getdate()) = ''Sunday''
    --  On Sundays (only), Scan and load table utilization data
    INSERT master.dbo.DatabaseTableSizeLog_2005
     (
       DBName
      ,AsOf
      ,TableName
      ,Rows
      ,DataPages
      ,LobPages
      ,IndexPages
     )
     execute master.dbo.sp_CalcDatabaseSpaceUsed_Tables_2005
'  ,@database_name = N'master'
   ,@server = N''
   ,@database_user_name = N''
   ,@subsystem = N'TSQL'
   ,@cmdexec_success_code = 0
   ,@flags = 0
   ,@retry_attempts = 3
   ,@retry_interval = 15
   ,@output_file_name = N''
   ,@on_success_step_id = 0
   ,@on_success_action = 1
   ,@on_fail_step_id = 0
   ,@on_fail_action = 2
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

  --  Make the first step the starting step
  EXECUTE @ReturnCode = msdb.dbo.sp_update_job @job_id = @JobID, @start_step_id = 1 
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

  -- Add the job schedules
  EXECUTE @ReturnCode = msdb.dbo.sp_add_jobschedule
    @job_id = @JobID
   ,@name = N'Once a day at 5:01am'
   ,@enabled = 1
   ,@freq_type = 4
   ,@active_start_date = 20031021
   ,@active_start_time = 050100
   ,@freq_interval = 1
   ,@freq_subday_type = 1
   ,@freq_subday_interval = 0
   ,@freq_relative_interval = 0
   ,@freq_recurrence_factor = 0
   ,@active_end_date = 99991231
   ,@active_end_time = 235959
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

  -- Add the Target Servers
  EXECUTE @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @JobID, @server_name = N'(local)' 
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

END
COMMIT TRANSACTION          
GOTO   EndSave              
QuitWithRollback:
  IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION 
EndSave: 
GO
