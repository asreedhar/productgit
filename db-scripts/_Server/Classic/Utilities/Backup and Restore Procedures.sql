/*

Set up PHK backup and restore procedures

This code includes references to both the SQL LiteSpeed product and to
assorted tools and systems devised to support it.  However, the code
does NOT require that this tool be present in order to work, and has
been written accordingly.


Dec 15, 2003  Created.  (Moved several backup and restore routines from other
               installation files to this one.)
Jun  9, 2005  Edited procedure names
Jul 27, 2005  Inserted parameter in sp_differentialBackup
Dec 27, 2006  Added output parameter to sp_ComprehensiveBackup



Creates the following procedures (see their comments for details):
 - sp_ComprehensiveBackup  (11/07/2003)
 - sp_DifferentialBackup   (11/07/2003)
 - sp_ComprehensiveRestore (10/26/2003)

*/


USE master

--  Drop (and then recreate) the procedures if they exist.
IF objectproperty(object_id('sp_ComprehensiveBackup'), 'isProcedure') = 1
    DROP PROCEDURE sp_ComprehensiveBackup
IF objectproperty(object_id('sp_DifferentialBackup'), 'isProcedure') = 1
    DROP PROCEDURE sp_DifferentialBackup
IF objectproperty(object_id('sp_ComprehensiveRestore'), 'isProcedure') = 1
    DROP PROCEDURE sp_ComprehensiveRestore

GO
/******************************************************************************
**
**  Procedure: sp_ComprehensiveBackup
**  Description: Perform a database backup based on the passed parameters.
**   This was designed for use with the sp_HijackDBMaintPlan routine, but
**   it can also be used as a stand-alone utility.
**
**   Depending on the parameters passed and the presence of an installed licensed
**   copy of SQL LiteSpeed, it can be used to create any kind and type of backup
**   on a suitable configured database:
**     - Complete, differential, or transaction
**     - "Standard", SQL LiteSpeed compressed, or compressed and encrypted
**
**   Detailed information on the resulting activity is returned in the output parameters.
**
**   A few key assumptions and limitations have been made:
**     - Backups will always be sent to DISK (no tape support)
**     - It uses the default number of threads when performing an SQL LiteSpeed backup
**     - This is designed for use on SQL Server 2000 only
**
**
**  INPUT PARAMETERS:
**
**    @Database      varchar(128)   REQUIRED
**      The name of the database to be backed up
**
**    @Folder        varchar(200)   REQUIRED
**      The (local machine) folder in which to create the backup.  Must include
**       drive, trailing backslash is optional.
**
**    @Type          tinyint        REQUIRED
**      Value indicating kind of backup to be made:
**        1 - Complete
**        2 - Differential
**        3 - Transaction
**
**    @Cutoff_Period  tinyint       Defaults to null
**      If not null, the procedure will delete all backups of this precise type made
**       earlier than this number of days ago.
**
**    @Verify         bit           Defaults to 1
**      Do the "verify backup" routine?
**        0 - No
**        1 - Yes
**
**    @Extension       varchar(50)  Defaults to null
**      The extension to assign to the backup.  If not provided, the appropriate
**      "standard" default is used.
**
**    @Compressed      tinyint      Defaults to 1
**      Code indicating whether or not the backup should be a (SQL LiteSpeed)
**      compressed backup:
**        0 - No (use standard SQL Server backups)
**        1 - Yes (use SQL LiteSpeed backups if available)
**      The default is to always create compressed backups--but if SQL LiteSpeed is not
**      found on the box, we revert to a conventional backup.
**
**    @Encrypt_Lookup  varchar(25)  Defaults to null
**      Encryption lookup value.  If this explicit value is found in MASTER.dbo.CRYPT,
**      then the accompanying "Value" is used as the key and the backups will be encrypted
**      ...if SQL LiteSpeed is installed on the box, of course.
**
**    @Threads         int          Defaults to 1
**      If SQL LiteSpeed is used, how many threads are used to crate the backup.  NULL or
**      zero gets reset to one.  As of Nov 2003, no other checking is done on this value.
**
**
**  OUTPUT PARAMETERS:
**    (All output values are set, if only to null, by this procedure; all passed in values
**     get overwritten.)
**
**    @Backup_File            varchar(500)
**      Drive/patch/file name of the file that we attempted to create.
**
**    @Backup_Type_Attempted  varchar(50)
**      Set with a textual descriptiong of the backup type attempted; should be useful when
**      interpreting error messages.  Will be in the form '{Compressed|Standard} [ Encrypted]
**      { Complete| Differential| Transaction}}'
**
**    @Backup_Started         datetime
**      When the backup was started (if done)
**
**    @Backup_Elapsed         int
**      Number of seconds elapsed during the backup (if done)
**
**    @Verify_Started         datetime
**      When the backup verification was started (if idone)
**
**    @Verify_Elapsed         int
**      Number of seconds elapsed during the verify (if done)
**
**    @Files_Deleted          int
**      Number of files deleted (if "cleanup" requested and done)
**
**
**  RETURN VALUES:
**     0 - Backup created
**    -1 - Database not found
**    -2 - Target folder not found
**    -3 - Invalid backup type code
**   <other non-zero> - Code returned by failed backup (or verify) call
**
**
**  ROWS RETURNED:  None
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  06/20/2001  PKelley   Procedure created.
**  07/25/2002  PKelley   Revised differential backup extension, added parameter
**                         allowing control over that extension
**  11/07/2003  PKelley   Added parameter and processing for SLS thread count
**
*****************************************************************************/
CREATE PROCEDURE dbo.sp_ComprehensiveBackup

    @Database               varchar(128)
   ,@Folder                 varchar(200)
   ,@Type                   tinyint
   ,@Cutoff_Period          tinyint      = null
   ,@Verify                 bit          = 1
   ,@Extension              varchar(50)  = null
   ,@Compressed             tinyint      = 1    --  Always assume they always want compression
   ,@Encrypt_Lookup         varchar(25)  = null
   ,@Threads                tinyint      = null  --  Use default value (currently hard-coded to 1)

    --  Optional output values indicate how things went
   ,@Backup_File            varchar(500) = null  OUTPUT
   ,@Backup_Type_Attempted  varchar(50)  = null  OUTPUT  --  Useful for interpreting error codes...
   ,@Backup_Started         datetime     = null  OUTPUT
   ,@Backup_Elapsed         int          = null  OUTPUT
   ,@Verify_Started         datetime     = null  OUTPUT
   ,@Verify_Elapsed         int          = null  OUTPUT
   ,@Files_Deleted          int          = null  OUTPUT

AS

    SET NOCOUNT on

    DECLARE
      @Checkit             int
     ,@Backup_Enabled      bit
     ,@Encryption_Enabled  varchar(10)  --  September 26, 2003: data type BIT for SLS 2.6
     ,@File                varchar(500)
     ,@FileSaveDate        varchar(16)
     ,@DeleteBeforeDate    varchar(16)
     ,@Command             varchar(300)
     ,@Type_Label          varchar(10)
     ,@Encryption_Key      varchar(255)
     ,@Started             datetime
     ,@Return_Value        int


    --  Set defaults for output parameters
    SET @Backup_File           = null
    SET @Backup_Type_Attempted = null
    SET @Backup_Started        = null
    SET @Backup_Elapsed        = null
    SET @Verify_Started        = null
    SET @Verify_Elapsed        = null
    SET @Files_Deleted         = null

    SET @Return_Value = 0


    --  Validate the parameters  --------------------------------------------------------

    --  Database Ok?
    IF db_id(@Database) is null
     BEGIN
        RAISERROR('Database %s not found', 11, 1, @Database)
        SET @Return_Value = -1
        GOTO _Done_
     END

    --  Target folder Ok?
    CREATE TABLE #foo
     (
       File_Exists        int  not null
      ,Is_Directory       int  not null
      ,Parent_Dir_Exists  int  not null
     )

    INSERT #foo
     execute master.dbo.xp_fileExist @Folder

    IF not exists (select 'x' from #foo where Is_Directory = 1)
     BEGIN
        --  Cannot determine that specified backup folder exists
        RAISERROR('Folder %s not found', 11, 2, @Folder)
        SET @Return_Value = -2
        GOTO _Done_
     END
    DROP TABLE #foo

    --  Make sure the folder string ends with a backslash
    IF right(@Folder,1) <> '\'
        SET @Folder = @Folder + '\'

    --  Backup type Ok?
    IF @Type not between 1 and 3
     BEGIN
        RAISERROR('Invalid backup type code, must be 1 (Complete), 2 (Differential), or 3 (Transaction)', 11, 3)
        SET @Return_Value = -3
        GOTO _Done_
     END


    --  Pick over the optional parameters  ----------------------------------------------

    --  Determine backup style: standard, compressed, or encrypted compressed
    IF @Compressed <> 0 and object_id('master.dbo.xp_sqllitespeed_version') is not null
     BEGIN
        --  First, make sure a backup-enabled version of SQL LiteSpeed is installed
        --  Assumes SLS xp routines are "online", I don't know how to check for otherwise...

        --  September 26, 2003: the commented-out section works for SLS 2.6, the "live"
        --  code is for 3.0 [which documentation indicates the same structure as 2.6, but
        --  it ain't so.  Going to have to assume that if properly installed a compressed
        --  backup can be performed, as I can't find that "restore only" setting.]

        CREATE TABLE #SLS_Version
         (
           Name   nvarchar(128)  --  Item label (don't know data type)
          ,Value  nvarchar(128)  --  Item value (don't know data type)
         )

        INSERT #SLS_Version
          (
            Name
           ,Value
          )
         execute @Checkit = master.dbo.xp_sqllitespeed_version
        
        IF @Checkit = 0
         BEGIN
            --  XP_ ran without errors
            SET @Backup_Enabled = 1

            SELECT @Encryption_Enabled = Value    --  Should be "Yes" or "No"
             from #SLS_Version
             where Name = 'Encryption Support'
         END

        ELSE
            --  Errors were encountered
            SET @Backup_Enabled = 0
        
        DROP TABLE #sls_version


/*  SQL LiteSpeed 2.6

        CREATE TABLE #SLS_Version
         (
           LiteSpeedVersion  nvarchar(128)  --  Version of SQL LiteSpeed installed
          ,RegName           nvarchar(128)  --  Registered Company Name for SQL LiteSpeed License
          ,RegNumber         nvarchar(128)  --  Serial Number for SQL LiteSpeed License
          ,Processors        int            --  Number of Processors Licensed for SQL LiteSpeed on the system
          ,Encryption        bit            --  1 = Encryption Enabled,   0 = Encryption Disabled
          ,RestoreOnly       bit            --  1 = Restore Only Version, 0 = Fully Functional Version
         )

        INSERT #SLS_Version
          (
            LiteSpeedVersion
           ,RegName
           ,RegNumber
           ,Processors
           ,Encryption
           ,RestoreOnly
          )
         execute @Checkit = master.dbo.xp_sqllitespeed_version
        
        IF @Checkit = 0
            --  XP_ ran without errors
            SELECT 
               @Backup_Enabled     = case RestoreOnly when 1 then 0 else 1 end
              ,@Encryption_Enabled = Encryption
             from #SLS_Version

        ELSE
            --  Errors were encountered
            SET @Backup_Enabled = 0
        
        DROP TABLE #sls_version
*/

        --  Check over the data we just got
        IF @Checkit <> 0 or @Backup_Enabled = 0
         BEGIN
            --  Compressed backups not enabled, do standard
            SET @Compressed = 0
            SET @Backup_Type_Attempted = 'Standard'
         END

        ELSE
         BEGIN
            SET @Backup_Type_Attempted = 'Compressed'

            --  Check over thread count (for now, make sure it's not zero.  Some day,
            --  make sure it's less than 2xCPUCount - 1)
            IF isnull(@Threads, 0) = 0
                SET @Threads = 1

            --  Can do a compressed backup, now check for encryption options
            IF @Encryption_Enabled = 'Yes' and object_id('CRYPT') is not null    --  Note September 26 2003 mods  --
                --  Encryption enabled on this server and lookup table exists
                IF exists (select 'x' from CRYPT where Lookup = @Encrypt_Lookup)
                 BEGIN
                     --  *Force* an encrypted compressed backup.  (If you don't want
                     --  it encrypted, don't specify a lookup value.)
                    SET @Backup_Type_Attempted = @Backup_Type_Attempted + ' Encrypted'
                    SELECT
                       @Encryption_Key = Value
                      ,@Compressed     = 2    --  Perform a compressed encrypted backup
                     from CRYPT
                     where Lookup = @Encrypt_Lookup
                 END
         END
     END 
   
    ELSE
     BEGIN
        --  Perform conventional SQL Server backup
        SET @Compressed = 0
        SET @Backup_Type_Attempted = 'Standard'
     END

    --  Finish setting the "backup type attempted" output parameter
    SET @Backup_Type_Attempted = @Backup_Type_Attempted
                              + case @Type
                                 when 1 then ' Complete'
                                 when 2 then ' Differential'
                                 when 3 then ' Transaction'
                                end

    --  Set backup file extension
    IF @Extension is null or len(@Extension) = 0
        --  Extension not specified, use our default values
        IF @Compressed = 0
            --  Conventional backup
            SET @Extension = case @Type
                              when 1 then 'BAK'
                              when 2 then 'FBK'
                              when 3 then 'TRN'
                             end
        ELSE
            --  Compressed backup
            SET @Extension = case @Type
                              when 1 then 'BKC'
                              when 2 then 'FBC'
                              when 3 then 'TRC'
                             end


    --  Do some final configuration routines  -------------------------------------------

    --  Set backup file type label
    IF @Compressed = 2
        --  Encrypted
        SET @Type_Label = case @Type
                           when 1 then '_dbE_'
                           when 2 then '_diffE_'
                           when 3 then '_tlogE_'
                          end

    ELSE
        --  Standard
        SET @Type_Label = case @Type
                           when 1 then '_db_'
                           when 2 then '_diff_'
                           when 3 then '_tlog_'
                          end

    --  Get and format the date and time to be used in this backup's file name.  Note that
    --  converting to varchar(16) chops off the seconds, and the subsequent lines just drop
    --  the formatting characters.
    SET @FileSaveDate = convert(varchar(16), getdate(), 120)
    SET @FileSaveDate = replace(@FileSaveDate, '-', '')
    SET @FileSaveDate = replace(@FileSaveDate, ':', '')
    SET @FileSaveDate = replace(@FileSaveDate, ' ', '')

    IF @Cutoff_Period is not null
     BEGIN
        --  Get the cut-off date for file deletions in part 2.  (Done here, so that the
        --  getdate() values are highly synchronized.  We check for null values again
        --  later on.)
        SET @DeleteBeforeDate = convert(varchar(16), dateadd(dd, -@Cutoff_Period, getdate()), 120)
        SET @DeleteBeforeDate = replace(@DeleteBeforeDate, '-', '')
        SET @DeleteBeforeDate = replace(@DeleteBeforeDate, ':', '')
        SET @DeleteBeforeDate = replace(@DeleteBeforeDate, ' ', '')
     END

    --  Determine the backup file name
    SET @Backup_File = @Folder + @Database + @Type_Label + @FileSaveDate + '.' + @Extension


    --  Perform the actual backup  ------------------------------------------------------

    --  Conventional backups
    SET @Backup_Started = getdate()

    IF @Compressed = 0
     BEGIN
        IF @Type = 1
            --  Complete
            BACKUP DATABASE @Database
             to disk = @Backup_File
             with init
        IF @Type = 2
            --  Differential
            BACKUP DATABASE @Database
             to disk = @Backup_File
             with
               differential
              ,init
        IF @Type = 3
            --  Transaction
            BACKUP LOG @Database
             to disk = @Backup_File
             with init
     END
    SET @Return_Value = @@error
    
    --  SQL LiteSpeed compressed backups
    IF @Compressed = 1
     BEGIN
        IF @Type = 1
            --  Complete
            EXECUTE @Return_Value = master.dbo.xp_backup_database
              @database = @Database
             ,@filename = @Backup_File
             ,@threads  = @Threads
             ,@init = 1
             ,@logging = 1    --  Log generated if problems encountered.  (0=noLog, 2=logEverything)
        IF @Type = 2
            --  Differential
            EXECUTE @Return_Value = master.dbo.xp_backup_database
              @database = @Database
             ,@filename = @Backup_File
             ,@with = 'differential'
             ,@threads  = @Threads
             ,@init = 1
             ,@logging = 1    --  Log generated if problems encountered.  (0=noLog, 2=logEverything)
        IF @Type = 3
            --  Transaction
            EXECUTE @Return_Value = master.dbo.xp_backup_log
              @database = @Database
             ,@filename = @Backup_File
             ,@threads  = @Threads
             ,@init = 1
             ,@logging = 1    --  Log generated if problems encountered.  (0=noLog, 2=logEverything)
     END
    
    --  SQL LiteSpeed compressed and encrypted backups
    IF @Compressed = 2
     BEGIN
        IF @Type = 1
            --  Complete
            EXECUTE @Return_Value = master.dbo.xp_backup_database
              @database = @Database
             ,@filename = @Backup_File
             ,@threads  = @Threads
             ,@init = 1
             ,@logging = 1    --  Log generated if problems encountered.  (0=noLog, 2=logEverything)
             ,@encryptionkey = @Encryption_Key
        IF @Type = 2
            --  Differential
            EXECUTE @Return_Value = master.dbo.xp_backup_database
              @database = @Database
             ,@filename = @Backup_File
             ,@with = 'differential'
             ,@threads  = @Threads
             ,@init = 1
             ,@logging = 1    --  Log generated if problems encountered.  (0=noLog, 2=logEverything)
             ,@encryptionkey = @Encryption_Key
        IF @Type = 3
            --  Transaction
            EXECUTE @Return_Value = master.dbo.xp_backup_log
              @database = @Database
             ,@filename = @Backup_File
             ,@threads  = @Threads
             ,@init = 1
             ,@logging = 1    --  Log generated if problems encountered.  (0=noLog, 2=logEverything)
             ,@encryptionkey = @Encryption_Key
     END

    --  If the backup failed, bomb out
    IF @Return_Value <> 0 GOTO _BackupFailed_
    SET @Backup_Elapsed = datediff(ss, @Backup_Started, getdate())


    --  Verify backup  ------------------------------------------------------------------
    IF @Verify = 1
     BEGIN
        SET @Verify_Started = getdate()

        IF @Compressed = 0
         BEGIN
            --  Conventional backups
            RESTORE VERIFYONLY
             from disk = @Backup_File

            SET @Return_Value = @@error
         END

        IF @Compressed = 1
            --  SQL LiteSpeed compressed backups
            EXECUTE @Return_Value = master.dbo.xp_restore_verifyonly
              @filename = @Backup_File
             ,@logging = 1    --  Log generated if problems encountered.  (0=noLog, 2=logEverything)

        IF @Compressed = 2
            --  SQL LiteSpeed compressed and encrypted backups
            EXECUTE @Return_Value = master.dbo.xp_restore_verifyonly
              @filename = @Backup_File
             ,@logging = 1    --  Log generated if problems encountered.  (0=noLog, 2=logEverything)
             ,@encryptionkey = @Encryption_Key

        --  If problems were encountered, bomb out
        IF @Return_Value <> 0 GOTO _VerifyFailed_
        SET @Verify_Elapsed = datediff(ss, @Verify_Started, getdate())
     END


    IF @Cutoff_Period is not null
     BEGIN
        --  Delete the suitably aged prior backups of the same type  ------------------------
        SET @Files_Deleted = 0
        CREATE TABLE #DROPFILES (data varchar(200) null)
    
        --  Get a list of all backups simlar in structure to this one
        SET @Command = 'DIR "' + @Folder + @Database + @Type_Label + '*.' + @Extension + '" /b'
        INSERT INTO #DROPFILES 
         execute master..xp_cmdshell @Command
    
        --  Now walk through and check them one by one, identifying and deleting the old ones
        DECLARE c_DUMPDIFFS CURSOR
         local fast_forward
         for select data from #DROPFILES
              where data like @Database + @Type_Label + '%'

        OPEN c_DUMPDIFFS

        FETCH next from c_DUMPDIFFS into @File

        WHILE @@Fetch_Status = 0
         BEGIN
            --  Check each file found
            IF substring(@File, len(@Database) + len(@Type_Label) + 1, 12) < @DeleteBeforeDate
             BEGIN
                --  This file was created before the cutoff period, time to delete!
                SET @Command = 'DEL "' + @Folder + @File + '"'
                EXECUTE master..xp_cmdShell @Command, no_output
                SET @Files_Deleted = @Files_Deleted + 1
             END
            FETCH next from c_DUMPDIFFS into @File
         END

        CLOSE c_DUMPDIFFS
        DEALLOCATE c_DUMPDIFFS
        DROP TABLE #DROPFILES
     END

    GOTO _Done_

    --  Error handling ------------------------------------------------------------------
    --  At this time, I see no need for special code or handling on either of these erros,
    --  but I'm leaving this section in just in case.

    _BackupFailed_:

    _VerifyFailed_:

    _Done_:

RETURN @Return_Value
GO
/******************************************************************************
**
**  Procedure: master.dbo.sp_DifferentialBackup
**  Description: Perform a differential backup based on the passed parameters.
**   This orginally stand-alone procedure has been modified to use sp_ComprehensiveBackup,
**   and to create log entries in msdb.dbo.sysDBMaintplan_History, to bring it
**    in line with the SQL LiteSpeed modifications.
**
**  Return values:  None
**
**  Input parameters:
**
**    @Database       varchar(128)  REQUIRED
**      The name of the database to perform a differential backup on
**
**   ,@Folder         varchar(200)  REQUIRED
**      What folder to create the backup in
**
**   ,@Cutoff_Period  tinyint       Defaults to null
**      If not null, the procedure will delete all backups of this precise type made
**       earlier than this number of days ago.
**
**   ,@Compressed     tinyint       Defaults to 1
**      If not 0, the backup will be compressed (if SQL LiteSpeed is detected)
**
**   ,@Extension      varchar(50)   Defaults to null
**      If not null, override the default extension used in differential backups
**
**   ,@Key_Lookup     varchar(25)   Defaults to null
**      Encryption lookup value.  If this explicit value is found in MASTER.dbo.CRYPT,
**      then the accompanying "Value" is used as the key and the backups will be encrypted
**      ...if SQL LiteSpeed is installed on the box, of course.
**
**   ,@Log_Activity   bit           Defaults to 1
**      If 1, activity is logged in msdb..sysDBMaintplan_history in a fashion similar to
**      that generated by msdb..sp_HijackDBMaintPlan.  Job_ID is zeroes and plan_name
**      is set to 'sp_DifferentialBackup'
**
**
**  Output parameters:  None
**
**  Rows returned:  None
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  06/20/2001  PKelley   Procedure created.
**  07/25/2002  PKelley   Revised differential backup extension, added parameter
**                         allowing control over that extension
**  09/03/2003  PKelley   Revised to use sp_ComprehensiveBackup, which does all
**                         of the original functionality and more.  Added last
**                         two parameters and msdb logging.
**  11/07/2003  PKelley   Added parameter for SLS thread count
**  07/28/2005  PKelley   >>Inserted<< parameter @Compressed
**
***************************************************************************/
CREATE PROCEDURE dbo.sp_DifferentialBackup

    @Database       varchar(128)
   ,@Folder         varchar(200)
   ,@Cutoff_Period  tinyint      = null
   ,@Compressed     tinyint      = 1
   ,@Extension      varchar(50)  = null
   ,@Key_Lookup     varchar(25)  = null
   ,@Log_Activity   bit          = 1

AS

    SET NOCOUNT on

    DECLARE
      @Backup_File            varchar(500)
     ,@Backup_Type_Attempted  varchar(50)
     ,@Backup_Started         datetime
     ,@Backup_Elapsed         int
     ,@Verify_Started         datetime
     ,@Verify_Elapsed         int
     ,@Files_Deleted          int
     ,@Return_Value           int


    --  When returned not null, these indicate that the actions were performed
    SET @Backup_Elapsed = null
    SET @Verify_Elapsed = null
    SET @Files_Deleted  = null


    --  Call the routine that does all the work
    EXECUTE @Return_Value = sp_ComprehensiveBackup
      @Database
     ,@Folder
     ,2      --  Perform a differential backup
     ,@Cutoff_Period
     ,1      --  Validate the backup
     ,@Extension
     ,@Compressed
     ,@Key_Lookup
     ,null   --  Defaqult number of threads
     --  Output parameters
     ,@Backup_File            OUTPUT
     ,@Backup_Type_Attempted  OUTPUT
     ,@Backup_Started         OUTPUT
     ,@Backup_Elapsed         OUTPUT
     ,@Verify_Started         OUTPUT
     ,@Verify_Elapsed         OUTPUT
     ,@Files_Deleted          OUTPUT


    IF @Log_Activity = 1
     BEGIN
        --  Log all activity, otherwise we don't much care what happens at this point
        IF @Backup_Elapsed is not null
            --  Backup performed
            INSERT msdb.dbo.sysDBMaintplan_history
              (
                 plan_name
                ,database_name
                ,activity
                ,succeeded
               ,end_time
                ,duration
                ,message
              )
             values
              (
                 'sp_DifferentialBackup'
                ,@Database
                ,'Backup Database / ' + @Backup_Type_Attempted
                ,1
               ,dateadd(ss, @Backup_Elapsed, @Backup_Started)
                ,@Backup_Elapsed
                ,'Backup Destination: [' + @Backup_File + ']'
              )

        IF @Verify_Elapsed is not null
            --  Backup verification done
            INSERT msdb.dbo.sysDBMaintplan_history
              (
                 plan_name
                ,database_name
                ,activity
                ,succeeded
               ,end_time
                ,duration
                ,message
              )
             values
              (
                 'sp_DifferentialBackup'
                ,@Database
                ,'Backup Database / Verify'
                ,1
               ,dateadd(ss, @Verify_Elapsed, @Verify_Started)
                ,@Verify_Elapsed
                ,'Backup Destination: [' + @Backup_File + ']'
              )

        IF @Files_Deleted is not null
            --  Old files deleted (could be zero)
            INSERT msdb.dbo.sysDBMaintplan_history
              (
                 plan_name
                ,database_name
                ,activity
                ,succeeded
                ,message
              )
             values
              (
                 'sp_DifferentialBackup'
                ,@Database
                ,'Backup Database / Delete old backups'
                ,1
                ,cast(@Files_Deleted as varchar(13)) + ' file(s) deleted'
              )

        IF @Return_Value <> 0
            --  Backup failed for some reason
            INSERT msdb.dbo.sysDBMaintplan_history
              (
                 plan_name
                ,database_name
                ,activity
                ,succeeded
               ,error_number
                ,message
              )
             values
              (
                 'sp_DifferentialBackup'
                ,@Database
                ,'Backup Database / Database error'
                ,0
               ,@Return_Value
                ,'Error raised by sp_ComprehensiveBackup'
              )
     END


    --  This is the brain-destroying correlated subquery adapted over from sp_HijackDBMaintPlan.
    --  For now, we will arbitrarily and summarily keep 500 entries per database called by
    --  sp_DifferentialBackup.
    DELETE msdb.dbo.sysDBMaintplan_history
      where plan_name = 'sp_DifferentialBackup'
       and database_name = @Database
       and (select count(*)
              from msdb.dbo.sysDBMaintplan_history mh_sub
              where mh_sub.sequence_id > msdb.dbo.sysDBMaintplan_history.sequence_id
               and plan_name = 'sp_DifferentialBackup'
               and database_name = @Database
            ) >= 500

RETURN 0
GO
/******************************************************************************
**
**  Procedure: sp_ComprehensiveRestore
**  Description: A magnificent simplification of the database restore process
**
**
**  Return values:
**     0 - Completed successfully
**     1 - Database already present (complete only -- not instructed to overwrite)
**     2 - Database does not exist (only diff and/or trans restores)
**     3 - Database is fully recovered (only diff and/or trans restores)
**     4 - Standby file not specified (only if told to do standby mode)
**     5 - Can't find a complete backup file
**    11 - Complete restore failed
**    12 - Differential restore failed
**    13 - Transaction restore failed
**
**  Input parameters:   See parameter list
**
**  Output parameters:  See parameter list
**
**  Rows returned:  None
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  08/15/2002  PKelley   Stand and gape in awe at the wonder I have created.
**  10/26/2003  PKelley   Minor cleanup in conjunction with _SLS version.
**  05/04/2004  PKelley   Tightened default values on "earliest" and "latest" values.
**  12/27/2006  PKelley   Added the output parameter, made complete backup folder
**                         parameter optional, tweaked some layout details
**
***************************************************************************/
CREATE PROCEDURE dbo.sp_ComprehensiveRestore

    @SourceDatabase            varchar(128)
       --  The name of the database that was backed up (i.e., the name used to create
       --  the backup files)

   ,@TargetDatabase            varchar(128)   = ''
       --  The name of the database to be restored (i.e., what do you want this one called?)
       --  If not set, @SourceDatabase is used

   ,@FinalState                tinyint        = 3
       --  What is the state of this database upon conclusion
       --    1 = norecovery (cannot be read)
       --    2 = standby    (read-only)
       --    3 = recovery   (ready for use)

   ,@StandbyFile               varchar(200)   = ''
       --  Location of the standby file--must specify if needed!

   ,@CompleteBackupFolder      varchar(200)   = ''
       --  Full path to the folder containing the complete backup.  Most recent backup will
       --  be used.  If left empty, no attempt will be made to do a complete restore.
       --  Do not include final backslash!

   ,@DifferentialBackupFolder  varchar(200)   = ''
       --  Full path to the folder containing the differential backup.  Most recent
       --  backup will be used.  If left empty, no attempt will be made to do a differential
       --  restore.  Do not include final backslash!

   ,@TransactionBackupFolder   varchar(200)   = ''
       --  Full path to the folder containing the transaction backups.  All present since
       --  most recent of (either complete or differential) will be applied.  If left
       --  empty, no attempt will be made to do a transaction restore.  Do not include
       --  final backslash!

   ,@FileLocations             varchar(1000)  = ''
       --  This one's tricky.  Leave it blank if not needed.  As per prior research:
       --    Insert the full path and file names to create the database (mdf and ndf)
       --    and log (ldf) files.  Note that you must know the logical file names to
       --    do this; these can be determined by doing "SELECT * FROM SYSFILES" on
       --    the original database, or configure and run:
       --    RESTORE FILELISTONLY from disk = '<backupfile>'
       --
       --  A sample:
       --    ,move ''MyDatabase''     to ''M:\SQL_DataFiles\MyDatabase.mdf''
       --    ,move ''MyDatabase_log'' to ''M:\SQL_DataFiles\MyDatabase_log.ldf''

   ,@Replace               tinyint  = 0
      --  Set to 0 to cause failure if a database by this name already exists.
      --  Set to 1 to restore over an existing database of the same name, if one exists
      --  (Note that this is only relevant if a complete restore is to be done)

   ,@Live                  tinyint  = 1
      --  Set to 0 to just print the RESTORE commands generated
      --  Set to 1 to execute them (they will not be displayed)

   ,@Earliest              char(12) = null
      --  Besides all other considerations, only attempt to use (any kind of) backup
      --  after (and including) this date/time

   ,@Latest                char(12) = null
      --  Besides all other considerations, only attempt to use (any kind of) backup
      --  before (and including) this date/time

   ,@CompleteExtension     char(3)  = 'BAK'
      --  Extension used by complete backups

   ,@DifferentialExtension char(3)  = 'FBK'
      --  Extension used by differential backups

   ,@TransactionExtension  char(3)  = 'TRN'
      --  Extension used by transaction backups

   ,@LastDateTimeRestored  char(12) = null  OUTPUT
      --  Input ignored and overwritten with YYYYMMDDhhmm datetime string of last
      --  file restored (and the empty string if nothing is restored)

AS

    DECLARE
      @DBState       tinyint
     ,@Command       varchar(2000)
     ,@RestoreFile   varchar(200)
     ,@StartDate     char(12)
     ,@CompareDate   char(12)
     ,@CurrentTrans  int
     ,@LastTrans     int
     ,@ReplaceCmd    varchar(10)

    SET NOCOUNT ON

    --  Parameter validation checks  ----------------------------------------------------

    SET @LastDateTimeRestored = ''

    --  If they want it in standby mode, did they set a standby file?
    IF @FinalState = 2 and @StandbyFile = ''
     BEGIN
        PRINT 'To restore in standby mode, you have to specify a standby file'
        RETURN 4
     END

    --  Make sure the range dates are properly defaulted
    IF @Earliest is null or @Earliest = ''
        SET @Earliest = '000000000000'
    IF @Latest is null or @Earliest = ''
        SET @Latest = '999999999999'

    --  Set up variables and stuff  -----------------------------------------------------

    --  Mess with defaults
    SET @StartDate = null

    IF @Replace = 0
        SET @ReplaceCmd = ''
    ELSE
        SET @ReplaceCmd = ', replace'

    IF @TargetDatabase = ''
        SET @TargetDaTabase = @SourceDatabase

    --  Determine the state of the database.  Values are:
    --    0 - Does not exist
    --    1 - Norecovery
    --    2 - Standby
    --    3 - Online
    SELECT @DBState =
     case
      --  Standby
      when cast(databasepropertyex(@TargetDatabase, N'isInStandby') as int) = 1 then 2
      --  Does not exist
      when databasepropertyex(@TargetDatabase, N'status') is null then 0
      --  Norecovery
      when cast(databasepropertyex(@TargetDatabase, N'status') as varchar(20)) = 'RESTORING' then 1
      --  Online
      else 3
     end

    --  This table is used to process files found in the various folders
    CREATE TABLE #BACKUP_FILES
     (
       Ordering   int           not null  identity(1,1)
      ,File_Name  varchar(200)  null
      ,File_Type  tinyint       null
     )
    
    
    --  Restore Complete Backup  ------------------------------------------------------------
    IF @CompleteBackupFolder <> ''
     BEGIN
        --  We have been instructed to restore a complete backup
        IF @DBState <> 0 and @Replace = 0
         BEGIN
            SET @Command = 'Cannot restore database ' + @TargetDatabase + ', as one by that name already exists on this server.  Set the "replace" parameter to overwrite it.'
            PRINT @Command
            RETURN 1
         END

        --  List complete backup files in short form order by name (and thus, date) ascending
        SET @Command = 'DIR ' + @CompleteBackupFolder + '\' + @SourceDatabase + '*.' + @CompleteExtension + ' /on /b'

        --  Load these into the temp table
        INSERT INTO #BACKUP_FILES (File_Name)
         execute master.dbo.xp_cmdShell @Command

        --  Set all completed files found to type 1
        UPDATE #BACKUP_FILES
         set File_Type = 1
         where File_Type is null
          and File_Name is not null

        --  Get the most recently desired complete backup in the folder
        SELECT @RestoreFile = File_Name
         from #BACKUP_FILES
         where Ordering = (select max(Ordering)
                            from #BACKUP_FILES
                            where File_Type = 1
                             and File_Name like @SourceDatabase + '_db_%'
                             and substring(File_Name, len(@SourceDatabase)+ 5, 12)
                              between @Earliest and @Latest)

        --  Did we find a complete backup file?
        IF @RestoreFile is null or @RestoreFile = ''
         BEGIN
            SET @Command = 'Unable to find complete backup for database ' + @SourceDatabase
             + ' in folder ' + @CompleteBackupFolder
            PRINT @Command
            DROP TABLE #BACKUP_FILES
            RETURN 5
         END

        --  Track the date stored in the file name.  Any subsequent incremental restores
        --  must be for after this date.
        SET @StartDate = substring(@RestoreFile, len(@SourceDatabase)+ 5, 12)

        --  Prepare the restore command.  If we want standby, make it standby, otherwise leave
        --  it at norecovery for now.  Formatted for legible testing output.
        SET @Command = 'RESTORE DATABASE ' + @TargetDatabase + '
 from disk = ''' + @CompleteBackupFolder + '\' + @RestoreFile + '''' + '
 with stats ' + @FileLocations
         + @ReplaceCmd
         + case @FinalState
            when 2 then ', standby = ''' + @StandbyFile + ''''
            else ', norecovery'
           end

        PRINT '--  Complete Restore  -------------------------------'
        PRINT @RestoreFile
        IF @Live = 0
         BEGIN
            PRINT @Command
            PRINT ''
         END
        ELSE
            EXECUTE (@Command)

        IF @@error <> 0
         BEGIN
            PRINT '-----------------------------------------------------'
            PRINT 'Error encountered while restoring complete backup'
            DROP TABLE #BACKUP_FILES
            RETURN 11
         END

        --  Presumably, we now have a database by this name on the server
        SET @LastDateTimeRestored = @StartDAte
        SET @DBState = 1  --  Indicate that there is now a database out there,
                          --  in either norecovery or standby mode
     END


    --  Restore Differential Backup  --------------------------------------------------------
    IF @DifferentialBackupFolder <> ''
     BEGIN
        --  We have been instructed to restore a differential backup
        IF @DBState = 0
         BEGIN
            SET @Command = 'Cannot perform differential restore on database ' + @TargetDatabase + ', as there is no such database currently on this server!'
            PRINT @Command
            RETURN 2
         END

        IF @DBState = 3
         BEGIN
            SET @Command = 'Cannot perform differential restore on database ' + @TargetDatabase + ', as this database has already been fully recovered'
            PRINT @Command
            RETURN 3
         END

        --  List differential files in short form order by name (and thus, date) ascending
        SET @Command = 'DIR ' + @DifferentialBackupFolder + '\' + @SourceDatabase + '*.' + @DifferentialExtension + ' /on /b'

        --  Load these into the temp table
        INSERT INTO #BACKUP_FILES (File_Name)
         execute master.dbo.xp_cmdShell @Command

        --  Set all differential files found to type 2
        UPDATE #BACKUP_FILES
         set File_Type = 2
         where File_Type is null
          and File_Name is not null

        --  Get the most recent desired differential backup in the folder
        SET @RestoreFile = ''
        SELECT @RestoreFile = File_Name
         from #BACKUP_FILES
         where Ordering = (select max(Ordering)
                            from #BACKUP_FILES
                            where File_Type = 2
                             and File_Name like @SourceDatabase + '_diff_%'
                             and substring(File_Name, len(@SourceDatabase)+ 7, 12)
                              between @Earliest and @Latest)

        IF @RestoreFile is not null and @RestoreFile <> ''
         BEGIN
            --  Differential backup file found, proceed (otherwise skip to transaction section)

            --  Get the date of the differential backup
            SET @CompareDate = substring(@RestoreFile, len(@SourceDatabase)+ 7, 12)

            IF @StartDate is not null and @CompareDate <= @StartDate
             BEGIN
                --  We can tell that the latest differential backup is from before the complete backup
                SET @Command = 'Differential backup ' + @RestoreFile + ' is dated before the complete backup (dated ' + @StartDate + ')'
                PRINT '-----------------------------------------------------'
                PRINT @Command
                PRINT 'Differential backup not applied'
             END

            ELSE
             BEGIN
                --  Attempt to apply this differential backup.  Formatted for legible testing output.
                SET @Command = 'RESTORE DATABASE ' + @TargetDatabase + '
 from disk = ''' + @DifferentialBackupFolder + '\' + @RestoreFile + '''' + '
 with stats ' + @ReplaceCmd
                 + case @FinalState
                    when 2 then ', standby = ''' + @StandbyFile + ''''
                    else ', norecovery'
                   end

                PRINT ''
                PRINT '--  Differential Restore  ---------------------------'
                PRINT @RestoreFile
                IF @Live = 0
                 BEGIN
                    PRINT @Command
                    PRINT ''
                 END
                ELSE
                    EXECUTE (@Command)

                IF @@error <> 0
                 BEGIN
                    PRINT '-----------------------------------------------------'
                    PRINT 'Error encountered while restoring differential backup'
                    DROP TABLE #BACKUP_FILES
                    RETURN 12
                 END

                --  Differential restore succesfully applied
                SET @LastDateTimeRestored = @CompareDate
             END

            --  Reset for transaction log checking
            SET @StartDate = @CompareDate
         END
     END


    --  Restore Transaction Backup  ---------------------------------------------------------
    IF @TransactionBackupFolder <> ''
     BEGIN
        --  We have been instructed to restore transaciton backups
        IF @DBState = 0
         BEGIN
            SET @Command = 'Cannot perform transaction restore(s) on database ' + @TargetDatabase + ', as there is no such database currently on this server!'
            PRINT @Command
            RETURN 2
         END

        IF @DBState = 3
         BEGIN
            SET @Command = 'Cannot perform transaction restore(s) on database ' + @TargetDatabase + ', as this database has already been fully recovered'
            PRINT @Command
            RETURN 3
         END

        --  List transaction files in short form order by name (and thus, date) ascending
        SET @Command = 'DIR ' + @TransactionBackupFolder + '\' + @SourceDatabase + '*.' + @TransactionExtension + ' /on /b'
        
        --  Load these into the temp table
        INSERT INTO #BACKUP_FILES (File_Name)
         execute master.dbo.xp_cmdShell @Command
        
        --  Set all transaction files found to type 3
        UPDATE #BACKUP_FILES
         set File_Type = 3
         where File_Type is null
          and File_Name is not null

        SELECT
           @CurrentTrans = min(Ordering)
          ,@LastTrans = max(Ordering)
         from #BACKUP_FILES
         where File_Type = 3
          and File_Name like @SourceDatabase + '_tlog_%'
          and substring(File_Name, len(@SourceDatabase)+ 7, 12)
           between @Earliest and @Latest
        
        WHILE @CurrentTrans <= @LastTrans
         BEGIN
        
            --  Get information for the "next" transaction backup
            SELECT @RestoreFile = File_Name
             from #BACKUP_FILES
             where Ordering = @CurrentTrans
        
            SET @CompareDate = substring(@RestoreFile, len(@SourceDatabase)+ 7, 12)
        
            IF @StartDate is null or @CompareDate > @StartDate
             BEGIN
                --  Attempt to apply this transaction backup.  Formatted for legible testing
                --  output.  Add the "stopat = 'XXX' option some day...
                SET @Command = 'RESTORE LOG ' + @TargetDatabase + '
 from disk = ''' + @TransactionBackupFolder + '\' + @RestoreFile + '''' + '
 with stats '    + @ReplaceCmd
                 + case @FinalState
                    when 2 then ', standby = ''' + @StandbyFile + ''''
                    else ', norecovery'
                   end

                PRINT ''
                PRINT '--  Transaction Restore  ----------------------------'
                PRINT @RestoreFile
                IF @Live = 0
                 BEGIN
                    PRINT @Command
                    PRINT ''
                 END
                ELSE
                    EXECUTE (@Command)

                IF @@error <> 0
                 BEGIN
                    PRINT '-----------------------------------------------------'
                    PRINT 'Error encountered while restoring transaction backup'
                    DROP TABLE #BACKUP_FILES
                    RETURN 13
                 END

                --  Transaction restore(s) succesfully applied
                SET @LastDateTimeRestored = @CompareDate
             END
        
            --  Increment counter for next t-log
            SET @CurrentTrans = @CurrentTrans + 1
         END
     END


    --  Final Steps  -----------------------------------------------
    
    IF @FinalState = 3
     BEGIN
        --  Set the database for active use
        SET @Command = 'RESTORE DATABASE ' + @TargetDatabase + ' with recovery'
    
        PRINT ''
        PRINT '--  Final Restore  ----------------------------------'
        IF @Live = 0
         BEGIN
            PRINT @Command
            PRINT ''
         END
        ELSE
            EXECUTE (@Command)
     END

    --  Clean up
    DROP TABLE #BACKUP_FILES

RETURN 0
GO
