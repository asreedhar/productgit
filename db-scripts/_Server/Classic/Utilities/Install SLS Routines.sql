/*

Install database components that support my adaptation of SQL LiteSpeed 3.0.122.0
 - Revised July 27, 2005 for SQL LiteSpeed 2005 (4.0.8.5)


Sep  3, 2003  Created (finalized)
Sep 26, 2003  Updated for SQL LiteSpeed 3.0.122.0 from 2.6.09
Oct  6, 2003  Added quotes to cmdshell calls to account for embedded spaces (I hate windows 3.0!)
Nov  6, 2003  Accounted for threads in SLS backup calls.  Default is now 1, with room for
               change if/when we get past 2 CPUs.
Dec 15, 2003  All backup and restore routines moved to installation file
               "CAT Backup and Restore Procedures.sql".  THIS MEANS THAT SCRIPT MUST BE
               RUN ON A COMPUTER BEFORE THIS ONE!  (We check for this in this script...)
Jan  7, 2004  sp_ModifyDBMaintplanJobs modified to only update >>backup<< jobs
May  4, 2004  Tightened up sp_ComprehensiveRestore_SLS date range parameters
Jul 27, 2005  Reviewed naming conventions, factored in SLS v4.0.8.5
Dec 27, 2006  Added output parameter to sp_ComprehensiveRestore_SLS

---------------------------------------

Creates the following objects.  Tables and jobs will be created ONLY if they do
not yet exist; stored procedures will be replaced, if present.

MASTER
 - Table Crypt (and deny access to role Public)
 - Procedure sp_ComprehensiveRestore_SLS
     (This is based on "sp_ComprehensiveRestore", but requires SLS to be loaded, so
      I am not making it part of the "backup and restore" installation file.
      Procedures sp_ComprehensiveBackup and sp_DifferentialBackup are stored elsewhere.)

MSDB
 - Table MODIFY_DBMAINTPLAN
 - Procedure sp_HijackDBMaintPlan
 - Procedure sp_ModifyDBMaintplanJobs

SQL Agent Job
 - _Refresh SLS Settings, scheduled to run at 3:30pm and 9:30pm (should catch changes impacting
    most off-hour backups)

----------------------------
For quick reference, the following shows the significant SLS registry keys

DECLARE @RegData  nvarchar(200)

--  Where the error log gets written:  SOFTWARE\Imceda\SQLLiteSpeed\Engine\ErrorLogPath
EXECUTE master.dbo.xp_instance_regread
  N'HKEY_LOCAL_MACHINE'
 ,'SOFTWARE\Imceda\SQLLiteSpeed\Engine'
 ,'ErrorLogPath'
 ,@RegData OUTPUT
PRINT @RegData

-----------------------------
Run this to uninstall all (and only) these components:

USE MASTER
DROP TABLE Crypt
DROP PROCEDURE sp_ComprehensiveRestore_SLS

USE MSDB
DROP TABLE MODIFY_DBMAINTPLAN
DROP PROCEDURE sp_HijackDBMaintPlan
DROP PROCEDURE sp_ModifyDBMaintplanJobs

EXECUTE sp_delete_job @job_name = '_Refresh SLS Settings'

*/


--  MASTER database modifications  ------------------------------------------------------
USE master

/*

First, have the backup and restore objects been installed?  We determine this by checking
for the one these routines depends on.

*/

IF object_id('sp_ComprehensiveBackup') is null
    --  This crashes the routine; nothing else will run
    RAISERROR ('Required "standard" backup and restore routines not present on the server', 20, 1) with log


/*

Create table master.dbo.Crypt, if it does not exist

*/

IF object_id('Crypt') is null
 BEGIN
    CREATE TABLE Crypt
     (
       Lookup  varchar(25)   not null
        constraint PK_Crypt
         primary key nonclustered
      ,Value   varchar(255)  not null
     )

    DENY all on Crypt to public
 END
ELSE
    PRINT N'The table "master.dbo.Crypt" already exists so will not be replaced.'


GO

--  Drop it if it exists, then recreate
IF objectproperty(object_id('sp_ComprehensiveRestore_SLS'), 'isProcedure') = 1
    DROP PROCEDURE sp_ComprehensiveRestore_SLS

GO
/******************************************************************************
**
**  Procedure: sp_ComprehensiveRestore_SLS
**  Description: sp_ComprehensiveRestore adpated for use by SQL LiteSpeed.
**   No attempt has made to try and write a "merged" routine that accounts for
**   backups made using both conventional and compressed backup technologies.
**
**  This routine assumes that SLS backups are stored as individual files.
**
**
**  Return values:
**     0 - Completed successfully
**     1 - Database already present (complete only -- not instructed to overwrite)
**     2 - Database does not exist (only diff and/or trans restores)
**     3 - Database is fully recovered (only diff and/or trans restores)
**     4 - Standby file not specified (only if told to do standby mode)
**     5 - Can't find a complete backup file
**    11 - Complete restore failed
**    12 - Differential restore failed
**    13 - Transaction restore failed
**
**  Input parameters:   See parameter list
**
**  Output parameters:  See parameter list
**
**  Rows returned:  None
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  08/15/2002  PKelley   Stand and gape in awe at the wonder I have created.
**  10/26/2003  PKelley   Minor cleanup in conjunction with (this) _SLS version.
**  12/15/2003  PKelley   _SLS "branched" version created, based on the standard
**                         version (first two entries).  Note comment in header
**                         above.
**  05/04/2004  PKelley   Tightened default values on "earliest" and "latest" values.
**  07/27/2005  PKelley   Revised naming conventions, reviewed for SLS 4.0.8.5 compatibility
**  12/27/2006  PKelley   Added the output parameter, made complete backup folder
**                         parameter optional
**
***************************************************************************/
CREATE PROCEDURE dbo.sp_ComprehensiveRestore_SLS

    @SourceDatabase            varchar(128)
       --  The name of the database that was backed up (i.e., the name used to create
       --  the backup files)

   ,@TargetDatabase            varchar(128)   = ''
       --  The name of the database to be restored (i.e., what do you want this one called?)
       --  If not set, @SourceDatabase is used

   ,@FinalState                tinyint        = 3
       --  What is the state of this database upon conclusion
       --    1 = norecovery (cannot be read)
       --    2 = standby    (read-only)
       --    3 = recovery   (ready for use)

   ,@StandbyFile               varchar(200)   = ''
       --  Location of the standby file--must specify if needed!

   ,@CompleteBackupFolder      varchar(200)   = ''
       --  Full path to the folder containing the complete backup.  Most recent backup will
       --  be used.  If left empty, no attempt will be made to do a complete restore.
       --  Do not include final backslash!

   ,@DifferentialBackupFolder  varchar(200)   = ''
       --  Full path to the folder containing the differential backup.  Most recent
       --  backup will be used.  If left empty, no attempt will be made to do a differential
       --  restore.  Do not include final backslash!

   ,@TransactionBackupFolder   varchar(200)   = ''
       --  Full path to the folder containing the transaction backups.  All present since
       --  most recent of (either complete or differential) will be applied.  If left
       --  empty, no attempt will be made to do a transaction restore.  Do not include
       --  final backslash!

   ,@FileLocations             varchar(1000)  = ''
       --  This one's tricky.  Leave it blank if not needed.  As per prior research:
       --    Insert the full path and file names to create the database (mdf and ndf)
       --    and log (ldf) files.  Note that you must know the logical file names to
       --    do this.  These can be determined by doing "SELECT * FROM SYSFILES" on
       --    the original database, or configure and run:
       --    EXECUTE master..xp_restore_filelistonly @filename = '<compressedBackupFile>'
       --
       --  A sample:
       --    ,move "MyDatabase"     to "M:\SQL_DataFiles\MyDatabase.mdf"
       --    ,move "MyDatabase_log" to "M:\SQL_DataFiles\MyDatabase_log.ldf"

----@Decrypt  varchar(255)  = ''
      --  Work this in if and when encrypted backups become a factor

   ,@Replace               tinyint  = 0
      --  Set to 0 to cause failure if a database by this name already exists.
      --  Set to 1 to restore over an existing database of the same name, if one exists
      --  (Note that this is only relevant if a complete restore is to be done)

   ,@Live                  tinyint  = 1
      --  Set to 0 to just print the RESTORE commands generated
      --  Set to 1 to execute them (they will not be displayed)

   ,@Earliest              char(12) = null
      --  Besides all other considerations, only attempt to use backup files labeled for
      --  on or AFTER this date/time

   ,@Latest                char(12) = null
      --  Besides all other considerations, only attempt to use backup files labeled for
      --  on or BEFORE this date/time

   ,@CompleteExtension     char(3) = 'BKC'
      --  Identify the extension used by complete compressed backups

   ,@DifferentialExtension char(3) = 'FBC'
      --  Identify the extension used by differential compressed backups

   ,@TransactionExtension  char(3) = 'TRC'
      --  Identify the extension used by transaction compressed backups

   ,@LastDateTimeRestored  char(12) = null  OUTPUT
      --  Input ignored and overwritten with YYYYMMDDhhmm datetime string of last
      --  file restored (and the empty string if nothing is restored)

AS

    DECLARE
      @DBState       tinyint
     ,@Command       varchar(2000)
     ,@RestoreFile   varchar(200)
     ,@StartDate     char(12)
     ,@CompareDate   char(12)
     ,@CurrentTrans  int
     ,@LastTrans     int
     ,@ReplaceCmd    varchar(10)
     ,@WorkString    varchar(500)

    SET NOCOUNT ON

    --  Parameter validation checks  ----------------------------------------------------

    SET @LastDateTimeRestored = ''

    --  If they want it in standby mode, did they set a standby file?
    IF @FinalState = 2 and @StandbyFile = ''
     BEGIN
        PRINT 'To restore in standby mode, you have to specify a standby file'
        RETURN 4
     END

    --  Make sure the range dates are properly defaulted
    IF @Earliest is null or @Earliest = ''
        SET @Earliest = '000000000000'
    IF @Latest is null or @Earliest = ''
        SET @Latest = '999999999999'

    --  Set up variables and stuff  -----------------------------------------------------

    --  Mess with defaults
    SET @StartDate = null

    IF @Replace = 0
        SET @ReplaceCmd = ''
    ELSE
        SET @ReplaceCmd = ', replace'

    IF @TargetDatabase = ''
        SET @TargetDaTabase = @SourceDatabase

    --  Determine the state of the database.  Values are:
    --    0 - Does not exist
    --    1 - Norecovery
    --    2 - Standby
    --    3 - Online
    SELECT @DBState =
     case
      --  Standby
      when cast(databasepropertyex(@TargetDatabase, N'isInStandby') as int) = 1 then 2
      --  Does not exist
      when databasepropertyex(@TargetDatabase, N'status') is null then 0
      --  Norecovery
      when cast(databasepropertyex(@TargetDatabase, N'status') as varchar(20)) = 'RESTORING' then 1
      --  Online
      else 3
     end

    --  This table is used to process files found in the various folders
    CREATE TABLE #BACKUP_FILES
     (
       Ordering   int           not null  identity(1,1)
      ,File_Name  varchar(200)  null
      ,File_Type  tinyint       null
     )
    
    
    --  Restore Complete Backup  ------------------------------------------------------------
    IF @CompleteBackupFolder <> ''
     BEGIN
        --  We have been instructed to restore a complete backup
        IF @DBState <> 0 and @Replace = 0
         BEGIN
            SET @Command = 'Cannot restore database ' + @TargetDatabase
             + ', as one by that name already exists on this server.  Set the "replace" parameter to overwrite it.'
            PRINT @Command
            RETURN 1
         END

        --  List complete backup files in short form order by name (and thus, date) ascending
        SET @Command = 'DIR ' + @CompleteBackupFolder + '\' + @SourceDatabase + '*.' + @CompleteExtension + ' /on /b'

        --  Load these into the temp table
        INSERT INTO #BACKUP_FILES (File_Name)
         execute master.dbo.xp_cmdShell @Command

        --  Set all completed files found to type 1
        UPDATE #BACKUP_FILES
         set File_Type = 1
         where File_Type is null
          and File_Name is not null

        --  Get the most recently desired complete backup in the folder
        SELECT @RestoreFile = File_Name
         from #BACKUP_FILES
         where Ordering = (select max(Ordering)
                            from #BACKUP_FILES
                            where File_Type = 1
                             and File_Name like @SourceDatabase + '_db_%'
                             and substring(File_Name, len(@SourceDatabase)+ 5, 12)
                              between @Earliest and @Latest)

        --  Did we find a complete backup file?
        IF @RestoreFile is null or @RestoreFile = ''
         BEGIN
            SET @Command = 'Unable to find complete backup for database ' + @SourceDatabase
             + ' in folder ' + @CompleteBackupFolder
            PRINT @Command
            DROP TABLE #BACKUP_FILES
            RETURN 5
         END

        --  If there are Move or Replace values, configure the "with" parameter,
        --  making sure there is no leading comma
        SET @WorkString = LTRIM(@FileLocations + @ReplaceCmd)
        SET @WorkString = case
                           when @WorkString = '' then ''
                           when left(@WorkString,1) = ',' then ' ,@With = ''' + substring(@WorkString, 2, 500) + '''
' --  here for proper spacing 
                           else ' ,@With = ''' + @WorkString + '''
' --  here for proper spacing 
                          end

        --  Track the date stored in the file name.  Any subsequent incremental restores
        --  must be for after this date.
        SET @StartDate = substring(@RestoreFile, len(@SourceDatabase)+ 5, 12)

        --  Prepare the restore command.  If we want standby, make it standby, otherwise leave
        --  it at norecovery for now.  Formatted for legible testing output.
        SET @Command = 'EXECUTE master.dbo.xp_restore_database
  @Database = ''' + @TargetDatabase + '''
 ,@Filename = ''' + @CompleteBackupFolder + '\' + @RestoreFile + '''
' + @WorkString +
' ,@With = ''' + case @FinalState
                  when 2 then 'standby = "' + @StandbyFile + '"'''
                  else 'norecovery'''
                 end + '
 ,@Logging = 1'

        PRINT '--  Complete Restore  -------------------------------'
        PRINT @RestoreFile
        IF @Live = 0
         BEGIN
            PRINT @Command
            PRINT ''
         END
        ELSE
            EXECUTE (@Command)

        IF @@error <> 0
         BEGIN
            PRINT '-----------------------------------------------------'
            PRINT 'Error encountered while restoring complete backup'
            DROP TABLE #BACKUP_FILES
            RETURN 11
         END

        --  Presumably, we now have a database by this name on the server
        SET @LastDateTimeRestored = @StartDAte
        SET @DBState = 1  --  Indicate that there is now a database out there,
                          --  in either norecovery or standby mode
     END


    --  Restore Differential Backup  --------------------------------------------------------
    IF @DifferentialBackupFolder <> ''
     BEGIN
        --  We have been instructed to restore a differential backup
        IF @DBState = 0
         BEGIN
            SET @Command = 'Cannot perform differential restore on database ' + @TargetDatabase
             + ', as there is no such database currently on this server!'
            PRINT @Command
            RETURN 2
         END

        IF @DBState = 3
         BEGIN
            SET @Command = 'Cannot perform differential restore on database ' + @TargetDatabase
             + ', as this database has already been fully recovered'
            PRINT @Command
            RETURN 3
         END

        --  List differential files in short form order by name (and thus, date) ascending
        SET @Command = 'DIR ' + @DifferentialBackupFolder + '\' + @SourceDatabase + '*.' + @DifferentialExtension + ' /on /b'

        --  Load these into the temp table
        INSERT INTO #BACKUP_FILES (File_Name)
         execute master.dbo.xp_cmdShell @Command

        --  Set all differential files found to type 2
        UPDATE #BACKUP_FILES
         set File_Type = 2
         where File_Type is null
          and File_Name is not null

        --  Get the most recent desired differential backup in the folder
        SET @RestoreFile = ''
        SELECT @RestoreFile = File_Name
         from #BACKUP_FILES
         where Ordering = (select max(Ordering)
                            from #BACKUP_FILES
                            where File_Type = 2
                             and File_Name like @SourceDatabase + '_diff_%'
                             and substring(File_Name, len(@SourceDatabase)+ 7, 12)
                              between @Earliest and @Latest)

        IF @RestoreFile is not null and @RestoreFile <> ''
         BEGIN
            --  Differential backup file found, proceed (otherwise skip to transaction section)

            --  Get the date of the differential backup
            SET @CompareDate = substring(@RestoreFile, len(@SourceDatabase)+ 7, 12)

            IF @StartDate is not null and @CompareDate <= @StartDate
             BEGIN
                --  We can tell that the latest differential backup is from before the complete backup
                SET @Command = 'Differential backup ' + @RestoreFile + ' is dated before the complete backup (dated ' + @StartDate + ')'
                PRINT '-----------------------------------------------------'
                PRINT @Command
                PRINT 'Differential backup not applied'
             END

            ELSE
             BEGIN
                --  Prepare the differential restore command.  If we want standby, make it standby,
                --  otherwise leave it at norecovery for now.  Formatted for legible testing output.
                SET @Command = 'EXECUTE master.dbo.xp_restore_database
  @Database = ''' + @TargetDatabase + '''
 ,@Filename = ''' + @DifferentialBackupFolder + '\' + @RestoreFile + '''
 ,@With = ''' + case @FinalState
                  when 2 then 'standby = "' + @StandbyFile + '"'
                  else 'norecovery'
                 end
              + '''
 ,@Logging = 1'

                PRINT ''
                PRINT '--  Differential Restore  ---------------------------'
                PRINT @RestoreFile
                IF @Live = 0
                 BEGIN
                    PRINT @Command
                    PRINT ''
                 END
                ELSE
                    EXECUTE (@Command)

                IF @@error <> 0
                 BEGIN
                    PRINT '-----------------------------------------------------'
                    PRINT 'Error encountered while restoring differential backup'
                    DROP TABLE #BACKUP_FILES
                    RETURN 12
                 END

                --  Differential restore succesfully applied
                SET @LastDateTimeRestored = @CompareDate
             END

            --  Reset for transaction log checking
            SET @StartDate = @CompareDate
         END
     END


    --  Restore Transaction Backup  ---------------------------------------------------------
    IF @TransactionBackupFolder <> ''
     BEGIN
        --  We have been instructed to restore transaciton backups
        IF @DBState = 0
         BEGIN
            SET @Command = 'Cannot perform transaction restore(s) on database ' + @TargetDatabase
             + ', as there is no such database currently on this server!'
            PRINT @Command
            RETURN 2
         END

        IF @DBState = 3
         BEGIN
            SET @Command = 'Cannot perform transaction restore(s) on database ' + @TargetDatabase
             + ', as this database has already been fully recovered'
            PRINT @Command
            RETURN 3
         END

        --  List transaction files in short form order by name (and thus date) ascending
        SET @Command = 'DIR ' + @TransactionBackupFolder + '\' + @SourceDatabase + '*.' + @TransactionExtension + ' /on /b'
        
        --  Load these into the temp table
        INSERT INTO #BACKUP_FILES (File_Name)
         execute master.dbo.xp_cmdShell @Command
        
        --  Set all transaction files found to type 3
        UPDATE #BACKUP_FILES
         set File_Type = 3
         where File_Type is null
          and File_Name is not null

        SELECT
           @CurrentTrans = min(Ordering)
          ,@LastTrans = max(Ordering)
         from #BACKUP_FILES
         where File_Type = 3
          and File_Name like @SourceDatabase + '_tlog_%'
          and substring(File_Name, len(@SourceDatabase)+ 7, 12)
           between @Earliest and @Latest
        
        WHILE @CurrentTrans <= @LastTrans
         BEGIN
        
            --  Get information for the "next" transaction backup
            SELECT @RestoreFile = File_Name
             from #BACKUP_FILES
             where Ordering = @CurrentTrans
        
            SET @CompareDate = substring(@RestoreFile, len(@SourceDatabase)+ 7, 12)
        
            IF @StartDate is null or @CompareDate > @StartDate
             BEGIN
                --  Prepare the transaction restore command.  If we want standby, make it standby,
                --  otherwise leave it at norecovery for now.  Formatted for legible testing output.
                --  Some day, the STOPAT options could be worked in...
                SET @Command = 'EXECUTE master.dbo.xp_restore_log
  @Database = ''' + @TargetDatabase + '''
 ,@Filename = ''' + @TransactionBackupFolder + '\' + @RestoreFile + '''
 ,@With = ''' + case @FinalState
                  when 2 then 'standby = "' + @StandbyFile + '"'
                  else 'norecovery'
                 end
              + '''
 ,@Logging = 1'

                PRINT ''
                PRINT '--  Transaction Restore  ----------------------------'
                PRINT @RestoreFile
                IF @Live = 0
                 BEGIN
                    PRINT @Command
                    PRINT ''
                 END
                ELSE
                    EXECUTE (@Command)
        
                IF @@error <> 0
                 BEGIN
                    PRINT '-----------------------------------------------------'
                    PRINT 'Error encountered while restoring transaction backup'
                    DROP TABLE #BACKUP_FILES
                    RETURN 13
                 END

                --  Transaction restore(s) succesfully applied
                SET @LastDateTimeRestored = @CompareDate
             END
        
            --  Increment counter for next t-log
            SET @CurrentTrans = @CurrentTrans + 1
         END
     END


    --  Final Steps  -----------------------------------------------
    
    IF @FinalState = 3
     BEGIN
        --  Set the database for active use
        SET @Command = 'RESTORE DATABASE ' + @TargetDatabase + ' with recovery'
    
        PRINT ''
        PRINT '--  Final Restore  ----------------------------------'
        IF @Live = 0
         BEGIN
            PRINT @Command
            PRINT ''
         END
        ELSE
            EXECUTE (@Command)
     END

    --  Clean up
    DROP TABLE #BACKUP_FILES

RETURN 0
GO


--  MSDB database modifications  --------------------------------------------------------
USE msdb


--  Set up table, if it doesn't already exist
IF object_ID('MODIFY_DBMAINTPLAN') is null

    CREATE TABLE MODIFY_DBMAINTPLAN
     (
       Plan_Name                    sysname  not null
        constraint PK_MODIFY_DBMAINTPLAN
         primary key clustered
      ,Compress_Complete_Backup     bit      not null
      ,Compress_Transaction_Backup  bit      not null
     )


IF objectproperty(object_id('sp_HijackDBMaintPlan'), 'isProcedure') = 1
    DROP PROCEDURE sp_HijackDBMaintPlan

IF objectproperty(object_id('sp_ModifyDBMaintplanJobs'), 'isProcedure') = 1
    DROP PROCEDURE sp_ModifyDBMaintplanJobs

GO
/*******************************************************************************
**
**  Procedure: sp_HijackDBMaintPlan
**  Description: This procedure integrates as much as the *backup* functionality
**   of a standard database maintenance plan as possible with the compressed (and
**   possibly encrypted) backups of SQL LiteSpeed.  It uses stored procedure
**   master.dbo.sp_ComprehensiveBackup to perform the actual backups.  It is
**   intended for use on systems where SQL LiteSpeed has been installed.
**
**   If directed to "write history" in the maintenance plan, all significant actions
**   will be logged in msdb..sysdbMaintplan_history.
**
**   A few key assumptions and limitations have been made:
**     - Remote backup history logging is not supported
**     - Backup report files are not generated -- but extensive information is loaded
**        for each run in msdb..sysMaintplan_history
**     - The various backup file extensions I came up with will always be used
**     - If replication creates a system database named "distribution", we are
**        completely ignorant about it
**     - The job step command and sysdbMainplans.user_defined_2 are converted from
**        nvarchar to varchar without checking for loss of information
**     - This is designed for use on SQL Server 2000 only
**
**
**  Return values:
**    0  This procedure ran with no serious problems (though backups may have failed)
**   -1  '-plan_id' not found in parameter
**   -2  Plan ID not found in sysDBMaintPlans
**   -3  Backup type could not be determined
**   -4  "Main" target backup folder was not found
**   -5  Deleting aged files measured by hours or minutes not currently supported
**
**  Input parameters:   See parameter list
**
**  Output parameters:  None
**
**  Rows returned:      None
**
********************************************************************************
**  Version History
********************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  08/27/2003  PKelley   Procedure created.
**  11/07/2003  PKelley   Added parameter for SLS thread count
**  07/27/2005  PKelley   Reviewed for SLS 4.0.8.5 compatibility.  (I wonder where
**                         that second parameter went?)
**
******************************************************************************/
CREATE PROCEDURE dbo.sp_HijackDBMaintPlan

    @DBMaint_Param  varchar(3200)

AS

    SET NOCOUNT on

    DECLARE
      @Backup_Database        varchar(128)
     ,@Target_Folder          varchar(200)
     ,@Backup_Type            tinyint
     ,@Retain_Days            smallint
     ,@Verify_Backup          bit
     ,@Position               smallint
     ,@Position2              smallint
     ,@Findit                 varchar(20)
     ,@Log_Activity           bit
     ,@Base_Folder            varchar(500)
     ,@Create_DB_SubFolder    bit
     ,@Instance               sysname
     ,@Build_String           varchar(1000)
     ,@Plan_ID                uniqueidentifier
     ,@Plan_ID_Char           char(36)
     ,@Plan_Name              sysname
     ,@Max_History            int
     ,@Key_Lookup             varchar(25)
     ,@MaintScope             char(1)
     ,@Value                  varchar(500)
     ,@Multiplier             int
     ,@Count                  int
     ,@Loop                   int
     ,@TLog_Class             tinyint
     ,@Backup_File            varchar(500)
     ,@Backup_Type_Attempted  varchar(50)
     ,@Backup_Started         datetime
     ,@Backup_Elapsed         int
     ,@Verify_Started         datetime
     ,@Verify_Elapsed         int
     ,@Files_Deleted          int
     ,@Alt_Return_Value       int
     ,@Return_Value           int


    --  Preliminary initializations
    SET @Return_Value = 0
    SET @Max_History = 0

    --  Do this to avoid potential if obscure problems later on
    SET @DBMaint_Param = @DBMaint_Param + ' -'


    --  PART 0: DETERMINE IF LOGGING HAS BEEN ASKED FOR  ------------------------------------

    --  Are we to log activity to the sysDBMaintplan_History table?
    SET @Findit = '-WriteHistory'
    SET @Position = charindex(@Findit, @DBMaint_Param)

    IF @Position = 0
        --  No logging
        SET @Log_Activity = 0
    ELSE
        --  Logging active
        SET @Log_Activity = 1


    --  PART 1: INFORMATION FROM THE DATABASE MAINTENANCE PLAN  -----------------------------

    --  Get Plan_ID and related information
    SET @Findit = '-PlanID'
    SET @Position = charindex(@Findit, @DBMaint_Param)
        
    IF @Position = 0
     BEGIN
        --  Could not find -PlanID in the parameter passed
        SET @Return_Value = -1
        IF @Log_Activity = 1
            INSERT msdb.dbo.sysDBMaintplan_history
              (
                plan_name
               ,activity
               ,succeeded
               ,error_number
               ,message
              )
             values
              (
                '<not found>'
               ,'Backup Database / Procedure error'
               ,0
               ,-1
               ,'"-plan_id" not found in parameter'
              )

        RAISERROR('"-plan_id" not found in parameter', 11, 1) with log
        GOTO _Done_
     END
    SET @Plan_ID = substring(@DBMaint_Param, @Position + 8, 36)  --  Always the same
    SET @Plan_ID_Char = @Plan_ID


    --  Get important data
    SELECT 
       @Plan_Name   = plan_name
      ,@Max_History = max_history_rows
      ,@Key_Lookup  = user_defined_2    --  Part of our perhaps overly-comlpex sceurty routine
     from msdb.dbo.sysDBMaintPlans
     where plan_id = @Plan_ID

    IF @@rowcount <> 1
     BEGIN
        --  Could not find this plan_id in the system tables
        SET @Return_Value = -2
        IF @Log_Activity = 1
            --  plan_id defaults to 00000000-00<etc>
            INSERT msdb.dbo.sysDBMaintplan_history
              (
                plan_id
               ,plan_name
               ,activity
               ,succeeded
               ,error_number
               ,message
              )
             values
              (
                @Plan_ID
               ,'<not found>'
               ,'Backup Database / Procedure error'
               ,0
               ,-2
               ,'Plan ID ' + @Plan_ID_Char + ' not found in sysDBMaintPlans'
              )

        RAISERROR('Plan ID not found in msdb..sysDBMaintPlans', 11, 2, @Plan_ID_Char) with log
        GOTO _Done_
     END


    --  Create list of databases to be backed up
    DECLARE @Target_DB table
     (
       Target_DB_ID    int      not null  identity(1,1)
      ,Database_Name   sysname  not null
      ,TLog_Class      tinyint  not null
         --  0, User defined, not simple recovery mode (transaction backups enabled)
         --  1, User defined, simple recovery mode (transaction backups disabled)
         --  2, System database (no transaction backups)
     )

    SET @MaintScope = 'N'  --  As in "N number of selected databases to work over"

    IF (select count(*)
         from msdb.dbo.sysDBMaintplan_databases
         where plan_id = @Plan_ID) = 1

        --  Only the one item, check for special value
        SELECT @MaintScope = case database_name
                              when 'All Databases'        then 'A'
                              when 'All System Databases' then 'S'
                              when 'All User Databases'   then 'U'
                              else 'N'
                             end
         from msdb.dbo.sysDBMaintplan_databases
         where Plan_ID = @Plan_ID

    IF @MaintScope = 'A'
        --  All databases
        INSERT @Target_DB (Database_Name, TLog_Class)
         select
            name
           ,case
             when dbid <=4 then 2           --  System database
             when (Status & 8) <> 0 then 1  --  Simple recover mode [actually, truncate log on checkpoint]
             else 0
            end
          from master.dbo.sysdatabases
          where name <> 'tempdb'
          order by name

    IF @MaintScope = 'S'
        --  All system databases
        INSERT @Target_DB (Database_Name, TLog_Class)
         select
            name
           ,2
          from master..sysdatabases
          where name in ('master','msdb','model','distribution')
          order by dbid

    IF @MaintScope = 'U'
        --  All user databases
        INSERT @Target_DB (Database_Name, TLog_Class)
         select
            name
           ,case
             when (Status & 8) <> 0 then 1  --  Simple recover mode [actually, truncate log on checkpoint]
             else 0
            end
          from master..sysdatabases
          where name not in ('tempdb','master','msdb','model','distribution') 
          order by name

    IF @MaintScope = 'N'
        --  Specific list of databases (in dbMaintplan)
        INSERT @Target_DB (Database_Name, TLog_Class)
         select
            name
           ,case
             when sysdb.dbid <=4 then 2             --  System database
             when (sysdb.Status & 8) <> 0 then 1  --  Simple recover mode [actually, truncate log on checkpoint]
             else 0
            end
          from msdb.dbo.sysDBMaintplan_databases msdb
           inner join master.dbo.sysDatabases sysdb
            on sysdb.name = msdb.database_name
          where msdb.plan_id = @Plan_ID
          order by sysdb.name

    --  Number of databases to loop through
    SELECT @Count = max(Target_DB_ID)
     from @Target_DB


    --  PART 2: BACKUP TYPE AND TARGET FOLDER  ----------------------------------------------

    --  Determine type of backup to make (differential backups are not available through DBMaintplans)
    SET @Findit = '-BkUpDB'
    SET @Position = charindex(@Findit, @DBMaint_Param)

    IF @Position > 0
        --  Complete backup
        SET @Backup_Type = 1  --  Complete backup
    ELSE
     BEGIN
        --  Not complete, check if transaction
        SET @Findit = '-BkUpLog'
        SET @Position = charindex(@Findit, @DBMaint_Param)
        
        IF @Position > 0
            --  Transaction backup
            SET @Backup_Type = 3  --  Transaction backup
     END

    IF @Position = 0
     BEGIN
        --  Neither complete nor transaction backup specified, crash out
        SET @Return_Value = -3
        IF @Log_Activity = 1
            INSERT msdb.dbo.sysDBMaintplan_history
              (
                plan_id
               ,plan_name
               ,activity
               ,succeeded
               ,error_number
               ,message
              )
             values
              (
                @Plan_ID
               ,@Plan_Name
               ,'Backup Database / Procedure error'
               ,0
               ,-3
               ,'Backup type could not be determined'
              )

        RAISERROR('Backup type could not be determined', 11, 3) with log
        GOTO _Done_
     END


    --  Backup type known, determine target folder
    --  (Interestingly enough, this would seem to be stored with the job, not in the dbmaintplan)
    SET @Position2 = charindex('-', substring(@DBMaint_Param, @Position + len(@Findit), 1000)) - 1
    SET @Base_Folder = ltrim(rtrim(substring(@DBMaint_Param, @Position + len(@Findit), @Position2)))
    SET @Base_Folder = replace(@Base_Folder, '"', '')

    IF @Base_Folder = ''
     BEGIN
        --  Target folder not specified, so use SQL Server default backup folder
        SET @Instance = convert(sysname, serverproperty('InstanceName'))
        
        IF @Instance is null
            --  SQL Server 2000 default instance
            EXECUTE master.dbo.xp_instance_regread
              @rootkey     = 'HKEY_LOCAL_MACHINE'
             ,@key         = 'SOFTWARE\Microsoft\MSSQLServer\MSSQLServer'
             ,@value_name  = 'BackupDirectory'
             ,@Base_Folder = @Base_Folder  OUTPUT
        ELSE
         BEGIN
            --  SQL Server 2000 named instance
            SET @Build_String = 'SOFTWARE\Microsoft\Microsoft SQL Server\' + @Instance + '\MSSQLServer'
            EXECUTE master.dbo.xp_instance_regread
              @rootkey     = 'HKEY_LOCAL_MACHINE'
             ,@key         = @Build_String
             ,@value_name  = 'BackupDirectory'
             ,@Base_Folder = @Base_Folder  OUTPUT
         END
     END


    --  Determine if the target folder actually exists
    CREATE TABLE #Folder_Check
     (
       File_Exists        int  not null
      ,Is_Directory       int  not null
      ,Parent_Dir_Exists  int  not null
     )

    INSERT #Folder_Check EXECUTE master.dbo.xp_fileExist @Base_Folder

    IF not exists (select 'x' from #Folder_Check where Is_Directory = 1)
     BEGIN
        --  No such folder, and (just like Enterprise Manger) we're not going to try and create it
        SET @Return_Value = -4
        IF @Log_Activity = 1
            INSERT msdb.dbo.sysDBMaintplan_history
              (
                plan_id
               ,plan_name
               ,activity
               ,succeeded
               ,error_number
               ,message
              )
             values
              (
                @Plan_ID
               ,@Plan_Name
               ,'Backup Database / Procedure error'
               ,0
               ,-4
               ,'Backup folder "' + @Base_Folder + '" was not found'
              )

        RAISERROR('Backup folder "%s" was not found', 11, 4, @Base_Folder) with log
        GOTO _Done_
     END
    --  Table is not dropped since we may use it later on


    --  Determine if subfolders need to be made
    SET @Findit = '-CrBkSubDir'
    SET @Position = charindex(@Findit, @DBMaint_Param)

    IF @Position > 0
        SET @Create_DB_SubFolder = 1
    ELSE
        SET @Create_DB_SubFolder = 0


    --  PART 3: GET THE REMAING BITS AND PIECES  --------------------------------------------

    --  Verify backup?
    SET @Findit = '-VrfyBackup'
    SET @Position = charindex(@Findit, @DBMaint_Param)

    IF @Position = 0
        SET @Verify_Backup = 0
    ELSE
        SET @Verify_Backup = 1


    --  Delete backups?
    SET @Findit = '-DelBkUps'
    SET @Position = charindex(@Findit, @DBMaint_Param)

    IF @Position = 0
        --  Do not delete prior backups
        SET @Retain_Days = null
    ELSE
     BEGIN
        --  Determine retention period in days
        SET @Position2 = charindex('-', substring(@DBMaint_Param, @Position + len(@Findit), 1000)) - 1
        SET @Value = ltrim(rtrim(substring(@DBMaint_Param, @Position + len(@Findit), @Position2)))
        SET @Multiplier = 0

        IF @Value like '%WEEKS%'
         BEGIN
            SET @Value = replace(@Value, 'WEEKS', '')
            SET @Multiplier = 7
         END
        
        IF @Value like '%DAYS%'
         BEGIN
            SET @Value = replace(@Value, 'DAYS', '')
            SET @Multiplier = 1
         END

        IF @Value like '%MONTHS%'
         BEGIN
            SET @Value = replace(@Value, 'MONTHS', '')
            SET @Multiplier = 30  --  Loses a bit of accuracy, oh well
         END

        IF @Multiplier = 0
         BEGIN
            --  Currently cannot support deleting aged files measured by hours or minutes
            SET @Return_Value = -5
            IF @Log_Activity = 1
                INSERT msdb.dbo.sysDBMaintplan_history
                  (
                    plan_id
                   ,plan_name
                   ,activity
                   ,succeeded
                   ,error_number
                   ,message
                  )
                 values
                  (
                    @Plan_ID
                   ,@Plan_Name
                   ,'Backup Database / Procedure error'
                   ,0
                   ,-5
                   ,'Aged deletion interval (' + @Value + ') not currently supported'
                  )

            RAISERROR('Aged deletion interval (%s) not currently supported', 11, 5, @Value) with log
            GOTO _Done_
         END

        SET @Retain_Days =  cast(@Value as int) * @Multiplier
     END


    --  PART 4: LOOP THROUGH AND PROCESS THE DATABASE LIST  ---------------------------------

    --  Log the "official" start
    IF @Log_Activity = 1
        INSERT msdb.dbo.sysDBMaintplan_history
          (
            plan_id
           ,plan_name
           ,activity
           ,succeeded
          )
         values
          (
            @Plan_ID
           ,@Plan_Name
           ,'Backup Database / ** Procedure start **'
           ,1
          )

    --  Gets reset inside loop only if subfolders required
    SET @Target_Folder = @Base_Folder

    SET @Loop = 0

    WHILE @Loop < @Count
     BEGIN
        SET @Loop = @Loop + 1

        --  Get information for this database
        SELECT
           @Backup_Database = Database_Name
          ,@TLog_Class      = TLog_Class
         from @Target_DB
         where Target_DB_ID = @Loop

        IF @Backup_Type = 3 and @TLog_Class <> 0
         BEGIN
            --  Transaction backup request cannot be done
            IF @Log_Activity = 1
                INSERT msdb.dbo.sysDBMaintplan_history
                  (
                     plan_id
                    ,plan_name
                    ,database_name
                    ,activity
                   ,succeeded
                    ,error_number
                    ,message
                  )
                 values
                  (
                     @Plan_ID
                    ,@Plan_Name
                    ,@Backup_Database
                    ,'Backup Database / Database error'
                   ,0
                    ,-11
                    ,'Unable to perform transaction backup'
                  )

            GOTO _Loop_
         END

        IF @Create_DB_SubFolder = 1
         BEGIN
            SET @Target_Folder = @Base_Folder + '\' + @Backup_Database

            --  Check if subfolder exists; if not, try and create it
            DELETE #Folder_Check
            INSERT #Folder_Check EXECUTE master.dbo.xp_fileExist @Target_Folder
            IF not exists (select 'x' from #Folder_Check where Is_Directory = 1)
             BEGIN
                --  No such subfolder--but we know the parent folder exists, so try and make it
                SET @Build_String = 'md "' + @Target_Folder + '"'
            	EXECUTE @Alt_Return_Value = master.dbo.xp_cmdshell @Build_String, NO_OUTPUT
            
            	IF @Alt_Return_Value <> 0
                 BEGIN
                    --  Could not create folder
                    IF @Log_Activity = 1
                        INSERT msdb.dbo.sysDBMaintplan_history
                          (
                             plan_id
                            ,plan_name
                            ,database_name
                            ,activity
                           ,succeeded
                            ,error_number
                            ,message
                          )
                         values
                          (
                             @Plan_ID
                            ,@Plan_Name
                            ,@Backup_Database
                            ,'Backup Database / Database error'
                           ,0
                            ,-12
                            ,'Could not create subdirectory "' + @Target_Folder + '"'
                          )
                    GOTO _Loop_
                 END
             END
         END

        --  When returned not null, these indicate that the actions were performed
        SET @Backup_Elapsed = null
        SET @Verify_Elapsed = null
        SET @Files_Deleted  = null


        --  Call the routine that actually performs the backup
        EXECUTE @Return_Value = sp_ComprehensiveBackup
          @Backup_Database
         ,@Target_Folder
         ,@Backup_Type
         ,@Retain_Days
         ,@Verify_Backup
         ,null              --  Always use our defaults
         ,1                 --  Always use compression, if available
         ,@Key_Lookup
         ,null              --  Default number of threads
         --  Output parameters
         ,@Backup_File            OUTPUT
         ,@Backup_Type_Attempted  OUTPUT
         ,@Backup_Started         OUTPUT
         ,@Backup_Elapsed         OUTPUT
         ,@Verify_Started         OUTPUT
         ,@Verify_Elapsed         OUTPUT
         ,@Files_Deleted          OUTPUT


        IF @Log_Activity = 1
         BEGIN
            --  Log all activity, otherwise we don't much care what happens at this point
            IF @Backup_Elapsed is not null
                --  Backup performed
                INSERT msdb.dbo.sysDBMaintplan_history
                  (
                     plan_id
                    ,plan_name
                    ,database_name
                    ,activity
                   ,succeeded
                    ,end_time
                    ,duration
                    ,message
                  )
                 values
                  (
                     @Plan_ID
                    ,@Plan_Name
                    ,@Backup_Database
                    ,'Backup Database / ' + @Backup_Type_Attempted
                   ,1
                    ,dateadd(ss, @Backup_Elapsed, @Backup_Started)
                    ,@Backup_Elapsed
                    ,'Backup Destination: [' + @Backup_File + ']'
                  )

            IF @Verify_Elapsed is not null
                --  Backup verification done
                INSERT msdb.dbo.sysDBMaintplan_history
                  (
                     plan_id
                    ,plan_name
                    ,database_name
                    ,activity
                   ,succeeded
                    ,end_time
                    ,duration
                    ,message
                  )
                 values
                  (
                     @Plan_ID
                    ,@Plan_Name
                    ,@Backup_Database
                    ,'Backup Database / Verify'
                   ,1
                    ,dateadd(ss, @Verify_Elapsed, @Verify_Started)
                    ,@Verify_Elapsed
                    ,'Backup Destination: [' + @Backup_File + ']'
                  )

            IF @Files_Deleted is not null
                --  Old files deleted (could be zero)
                INSERT msdb.dbo.sysDBMaintplan_history
                  (
                     plan_id
                    ,plan_name
                    ,database_name
                    ,activity
                   ,succeeded
                    ,message
                  )
                 values
                  (
                     @Plan_ID
                    ,@Plan_Name
                    ,@Backup_Database
                    ,'Backup Database / Delete old backups'
                   ,1
                    ,cast(@Files_Deleted as varchar(13)) + ' file(s) deleted'
                  )

            IF @Return_Value <> 0
                --  Backup failed for some reason
                INSERT msdb.dbo.sysDBMaintplan_history
                  (
                     plan_id
                    ,plan_name
                    ,database_name
                    ,activity
                   ,succeeded
                    ,error_number
                    ,message
                  )
                 values
                  (
                     @Plan_ID
                    ,@Plan_Name
                    ,@Backup_Database
                    ,'Backup Database / Database error'
                   ,0
                    ,@Return_Value
                    ,'Error raised by sp_ComprehensiveBackup'
                  )
         END

        --  On to the next database to process
        _Loop_:

     END


    IF @Log_Activity = 1
        --  Final log entry
        INSERT msdb.dbo.sysDBMaintplan_history
          (
            plan_id
           ,plan_name
           ,activity
           ,succeeded
          )
         values
          (
            @Plan_ID
           ,@Plan_Name
           ,'Backup Database / ** Procedure completed **'
           ,1
          )

    _Done_:


    --  Clean out old history if and as necessary
    IF @Max_History > 0
        --  Looks like 0 means keep it all and anything else is a retention factor.
        --  Now this, this is a brain-destroying correlated subquery.  I don't know
        --  that I can explain this in plain English... so I'll let you figure it
        --  out on your own.  Don't worry, it's tested and proven.
        DELETE msdb.dbo.sysDBMaintplan_history
          where plan_id = @Plan_ID
           and (select count(*)
                  from msdb.dbo.sysDBMaintplan_history mh_sub
                  where mh_sub.sequence_id > msdb.dbo.sysDBMaintplan_history.sequence_id
                   and mh_sub.plan_id = @Plan_ID
                ) >= @Max_History

RETURN @Return_Value
GO
/*******************************************************************************
**
**  Procedure: sp_ModifyDBMaintplanJobs
**  Description: For every database maintenance plan backup specified in table
**   MODIFY_DBMAINTPLAN, go through and adjust the companion jobs to call
**   sp_HijackDBMaintplan instead.
**
**   This routine was originally provided by dbAssociates (the original sellers of SLS)
**    for use with SQL LiteSpeed.  I adapted it for use my way.
**
**  Return values:  None
**
**  Input parameters:   None
**
**  Output parameters:  None
**
**  Rows returned:  None
**
********************************************************************************
**  Version History
********************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  07/21/2003  PKelley   Adapted from original code
**  01/08/2004  PKelley   Only update DBMaintPlan-generated >>backup<< jobs
**  07/27/2005  PKelley   Reviewed for SLS 4.0.8.5 compatibility.
**
********************************************************************************/
CREATE PROCEDURE sp_ModifyDBMaintplanJobs

AS

    SET NOCOUNT on

    DECLARE
      @Job_ID                uniqueidentifier
     ,@Job_Name              sysname
     ,@Job_Step_ID           int
     ,@Job_Command           nvarchar(3200)
     ,@Compress_Complete     bit
     ,@Compress_Transaction  bit

     ,@Does_Job_Compress     bit
     ,@Should_Job_Compress   bit
     ,@Position              tinyint
     ,@Argument              nvarchar(3200)


    SET @Position = 32
      --  As of SQL 2000 sp3, the db maintencance plan-generated jobs all start with the
      --  following format:  EXECUTE master.dbo.xp_sqlmaint N'-PlanID <etc.>
      --  That is, the command followed by a (large) parameter.  This value is used to
      --  trim the command, leaving only the parameter.

    --  All db valid maintenance plan backup jobs that we wish to "hijack" that have not
    --  already been processed.  Messy code, but it's run against a very small table.
    DECLARE c_Jobs CURSOR fast_forward for
     select
        jo.job_id, jo.Name, js.step_id, js.command
       ,db.Compress_Complete_Backup, db.Compress_Transaction_Backup
      from sysdbmaintplans mp
       inner join sysdbmaintplan_jobs mj
        on mj.plan_id = mp.plan_id
       inner join sysjobs jo
        on jo.job_id = mj.job_id
       inner join sysjobsteps js
        on js.job_id = jo.job_id
       left outer join MODIFY_DBMAINTPLAN db
        on db.plan_name = mp.Plan_Name
       where js.command like '%xp_sqlmaint%'                     --  Should only be the one step
        and jo.name like '%Backup Job for DB Maintenance Plan%'  --  Only process the backup jobs!

    OPEN c_Jobs
    FETCH NEXT FROM c_Jobs
     into @Job_ID, @Job_Name, @Job_Step_ID, @Job_Command
      ,@Compress_Complete, @Compress_Transaction

    --  Set the desired jobs to run our code instead of the standard system code
    WHILE @@FETCH_STATUS = 0
     BEGIN
        --  Determine if the current job will attempt to make compressed backups
        SET @Does_Job_Compress = case charindex('sp_HijackDBMaintplan', @Job_Command)
                                  when 0 then 0 else 1
                                 end

        --  Determine if this job should attempt to create compressed backups
        IF @Job_Name like 'DB Backup Job%'
            SET @Should_Job_Compress = isnull(@Compress_Complete, 0)

        IF @Job_Name like 'Transaction Log Backup Job%'
            SET @Should_Job_Compress = isnull(@Compress_Transaction, 0)


        --  Check if we need to set a standard job to compressed
        IF @Does_Job_Compress = 0 and @Should_Job_Compress = 1
         BEGIN
            --  Identify the parameter being passed
            SET @Argument = substring(@Job_Command, @Position, len(@Job_Command))

            --  Comment out the system command, add our command
            SET @Job_Command = '--' + @Job_Command
             + char(13) + char(10)
             + 'EXECUTE msdb.dbo.sp_HijackDBMaintplan ' + @Argument

            --  Update the job step
            EXECUTE msdb.dbo.sp_update_jobstep
              @job_id  = @Job_ID
             ,@step_id = @Job_Step_ID
             ,@command = @Job_Command
             ,@database_name = 'msdb'    --  Hijack default
         END


        --  Check if we need to reset a compressed job back to standard
        IF @Does_Job_Compress = 1 and @Should_Job_Compress = 0
         BEGIN
            SET @Job_Command = substring(@Job_Command, 3, charindex(char(13) + char(10), @Job_Command) - 3)

            --  Update the job step
            EXECUTE msdb.dbo.sp_update_jobstep
              @job_id  = @Job_ID
             ,@step_id = @Job_Step_ID
             ,@command = @Job_Command
             ,@database_name = 'master'  --  dbmaintplan default
         END

        FETCH NEXT FROM c_Jobs
         into @Job_ID, @Job_Name, @Job_Step_ID, @Job_Command
          ,@Compress_Complete, @Compress_Transaction
     END

    CLOSE c_Jobs
    DEALLOCATE c_Jobs

RETURN 0
GO


/*

Create SQL Agent job that can be run to make sure backup jobs are properly configured

*/

BEGIN TRANSACTION            
  DECLARE @JobID BINARY(16)  
  DECLARE @ReturnCode INT    
  SELECT @ReturnCode = 0     
IF (SELECT COUNT(*) FROM msdb.dbo.syscategories WHERE name = N'Database Maintenance') < 1 
  EXECUTE msdb.dbo.sp_add_category @name = N'Database Maintenance'
IF (SELECT COUNT(*) FROM msdb.dbo.sysjobs WHERE name = N'_Refresh SLS Settings') > 0 
  PRINT N'The job "_Refresh SLS Settings" already exists so will not be replaced.'
ELSE
BEGIN 

  -- Add the job
  EXECUTE @ReturnCode = msdb.dbo.sp_add_job @job_id = @JobID OUTPUT , @job_name = N'_Refresh SLS Settings', @owner_login_name = N'sa', @description = N'Sept 3, 2003: Run to make sure SQL LiteSpeed modifications are incorporated into the dbMaintPlan-generated jobs. (Updates at 3:30pm and 9:30pm.)', @category_name = N'Database Maintenance', @enabled = 0, @notify_level_email = 0, @notify_level_page = 0, @notify_level_netsend = 0, @notify_level_eventlog = 2, @delete_level= 0
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

  -- Add the job steps
  EXECUTE @ReturnCode = msdb.dbo.sp_add_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'Routine to configure SLS', @command = N'EXECUTE sp_ModifyDBMaintplanJobs
', @database_name = N'msdb', @server = N'', @database_user_name = N'', @subsystem = N'TSQL', @cmdexec_success_code = 0, @flags = 0, @retry_attempts = 0, @retry_interval = 1, @output_file_name = N'', @on_success_step_id = 0, @on_success_action = 1, @on_fail_step_id = 0, @on_fail_action = 2
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 
  EXECUTE @ReturnCode = msdb.dbo.sp_update_job @job_id = @JobID, @start_step_id = 1 

  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

  -- Add the job schedules
  EXECUTE @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id = @JobID, @name = N'Update at 3:30pm and 9:30pm', @enabled = 1, @freq_type = 4, @active_start_date = 20031006, @active_start_time = 153000, @freq_interval = 1, @freq_subday_type = 8, @freq_subday_interval = 6, @freq_relative_interval = 0, @freq_recurrence_factor = 0, @active_end_date = 99991231, @active_end_time = 235959
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

  -- Add the Target Servers
  EXECUTE @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @JobID, @server_name = N'(local)' 
  IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

END
COMMIT TRANSACTION          
GOTO   EndSave              
QuitWithRollback:
  IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION 
EndSave: 
GO
