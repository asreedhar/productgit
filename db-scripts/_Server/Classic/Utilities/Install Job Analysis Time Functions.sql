/*

Functions to convert the msdb job date and time columns to actual datetime datatypes, so that
they can actually be USED.

FixJobTime: convert run_date and run_time to a datetime
FixJobDuration: convert run_duration to number of seconds


So why didn't I write these years ago?

*/

USE msdb

IF objectproperty(object_id('FixJobTime'), 'isScalarFunction') = 1
    DROP FUNCTION dbo.FixJobTime
IF objectproperty(object_id('FixJobDuration'), 'isScalarFunction') = 1
    DROP FUNCTION dbo.FixJobDuration

GO
/*

Version 1: Function to convert crappy msdb job dates and times to something that can be used
(in this case, a datetime value)

PHK, Mar 10 2006

*/

CREATE FUNCTION dbo.FixJobTime
 (
   @JobDate  int
  ,@JobTime  int = 0
 )
RETURNS datetime
AS BEGIN

    RETURN cast( cast((@JobDate % 10000) / 100 as varchar(10))
                  + '/' + cast(@JobDate % 100 as varchar(10))
                  + '/' + cast(@JobDate / 10000 as varchar(10))
                  + ' ' + cast(@JobTime / 10000 as varchar(10))
                  + ':' + cast((@JobTime % 10000) / 100 as varchar(10))
                  + ':' + cast(@JobTime % 100 as varchar(10))
            as datetime)
END
GO
/*

Version 1: Function to convert crappy msdb job durations into something that can be used
(in this case, total seconds as an integer)

PHK, Mar 10 2006

*/

CREATE FUNCTION dbo.FixJobDuration
 (
   @JobDuration  int
 )
RETURNS int
AS BEGIN

    RETURN (@JobDuration/10000)*3600 + ((@JobDuration%10000)/100)*60 + (@JobDuration%100)
END
GO
