/*

Add a purge routine to a database configured with the purge system.  This procedure will fail
if the database has not been configured; it will also fail if this purge routine has already
been configured in the database.

First, earch and replace on:
  xxx = Purge routine name/title/identifier
  yyy = Name of new purge stored procedure
  aaa = Table being purged
  zzz = [small]datetime column in aaa to base the purge criteria upon

Next, configure the parameters below

Lastly, review and configure the stored procedure as necessary.  Not all purges will be simple...


--  Review
SELECT * from PurgeManagement where Subject = 'xxx'

*/


DECLARE
  @PurgeTarget   varchar(25)
 ,@Description   varchar(200)
 ,@PeriodType    varchar(6)
 ,@PeriodLength  tinyint
 ,@Routine       varchar(100)


SET @PurgeTarget = 'xxx'
  --  Name/title/identifier of the purge routine

SET @Description  = ''
  --  Description of what's being purged

SET @PeriodType = ''
  --  Retention period type; we're keeping data X many of these units.
  --  Valid values are 'Days', 'Weeks', 'Months', or 'Years'

SET @PeriodLength = X
  --  How many period units we're keeping data for (e.g. 3 months, 8 weeks)

SET @Routine = 'EXECUTE yyy'
  --  The stored procedure to call.  Include the "EXECUTE" statement.


--  Error checking  -----------------------------------------------------------

IF object_id('PurgeManagement') is null
    --  Main purge table not present in this database
    RAISERROR ('This database has not been configured with the purge management components', 20, 1) with log

IF exists (select 1 from PurgeManagement where Subject = @PurgeTarget)
    --  Purge subject has already been configured in this database
    RAISERROR ('This database has already been configured with this purge item', 20, 1) with log


INSERT PurgeManagement (Subject, Description, Period, PeriodType, Routine)
 values
 (
   @PurgeTarget
  ,@Description
  ,@PeriodLength
  ,@PeriodType
  ,@Routine
 )


IF objectproperty(object_id('yyy'), 'isProcedure') = 1
    DROP PROCEDURE yyy

GO
/******************************************************************************
**
**  Procedure: yyy
**  Description: This routine will purge (delete) all rows from...
**  ...that were entered prior to the passed-in retention date.
**        
**
**  Return values:  none
** 
**  Input parameters:   Once done, we should have no data from before this
**                       date stored within the table.
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  xx/xx/2006  PKelley   Procedure created
**  
*******************************************************************************/
CREATE PROCEDURE dbo.yyy

    @RetainThru  datetime

AS

    SET NOCOUNT on

    DECLARE
      @HowMany   int
     ,@Earliest  datetime
     ,@Latest    datetime

    --  Figure out how much there is to delete
    SELECT
       @HowMany  = count(*)
      ,@Earliest = min(zzz)
      ,@Latest   = max(zzz)
     from aaa
     where zzz < @RetainThru

    IF isnull(@HowMany, 0) > 0
     BEGIN
        --  There's stuff to delete.  (If there's too much to delete all at once,
        --  you'll need to break the delete into several pieces.  "Too much"
        --  is, of course, extremely subjective.)
        DELETE aaa
         where zzz < @RetainThru

        --  Log the fact that it's been deleted
        INSERT PurgeLog (Subject, ItemsDeleted, First, Last)
         values ('xxx', @HowMany, @Earliest, @Latest)
     END

    --  Log the most recent run in the parent table
    UPDATE PurgeManagement
     set LastCall = getdate()
     where Subject = 'xxx'

RETURN
GO
