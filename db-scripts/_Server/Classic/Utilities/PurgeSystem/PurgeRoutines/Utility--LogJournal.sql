/*

Add a purge routine to a database configured with the purge system.  This procedure will fail
if the database has not been configured; it will also fail if this purge routine has already
been configured in the database.

First, earch and replace on:
  Log Journal = Purge routine name/title/identifier
  PurgeLogJournal = Name of new purge stored procedure
  LogJournal = Table being purged
  Logged = [small]datetime column in LogJournal to base the purge criteria upon

Next, configure the parameters below

Lastly, review and configure the stored procedure as necessary.  Not all purges will be simple...


--  Review
SELECT * from PurgeManagement where Subject = 'Log Journal'

*/


DECLARE
  @PurgeTarget   varchar(25)
 ,@Description   varchar(200)
 ,@PeriodType    varchar(6)
 ,@PeriodLength  tinyint
 ,@Routine       varchar(100)


SET @PurgeTarget = 'Log Journal'
  --  Name/title/identifier of the purge routine

SET @Description  = 'Clear out old Log Journal rows'
  --  Description of what's being purged

SET @PeriodType = 'Months'
  --  Retention period type; we're keeping data X many of these units.
  --  Valid values are 'Days', 'Weeks', 'Months', or 'Years'

SET @PeriodLength = 6
  --  How many period units we're keeping data for (e.g. 3 months, 8 weeks)

SET @Routine = 'EXECUTE PurgeLogJournal'
  --  The stored procedure to call.  Include the "EXECUTE" statement.


--  Error checking  -----------------------------------------------------------

IF object_id('PurgeManagement') is null
    --  Main purge table not present in this database
    RAISERROR ('This database has not been configured with the purge management components', 20, 1) with log

IF exists (select 1 from PurgeManagement where Subject = @PurgeTarget)
    --  Purge subject has already been configured in this database
    RAISERROR ('This database has already been configured with this purge item', 20, 1) with log


INSERT PurgeManagement (Subject, Description, Period, PeriodType, Routine)
 values
 (
   @PurgeTarget
  ,@Description
  ,@PeriodLength
  ,@PeriodType
  ,@Routine
 )


IF objectproperty(object_id('PurgeLogJournal'), 'isProcedure') = 1
    DROP PROCEDURE PurgeLogJournal

GO
/******************************************************************************
**
**  Procedure: PurgeLogJournal
**  Description: This routine will purge (delete) all rows from the
**   LogJournal table that were entered prior to the passed-in
**   retention date.
**        
**
**  Return values:  none
** 
**  Input parameters:   Once done, we should have no data from before this
**                       date stored within the table.
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  09/27/2005  PKelley   Applied to this table
**  01/03/2006  PKelley   Factored in log chaining -- delete entire sets of log entries
**                         based on OriginatingLogID, but only if the latest entry is
**                         within the deletion period
**  04/26/2006  PKelley   Adapted over to the Utility database version of LogJournal
**
***************************************************************************/
CREATE PROCEDURE PurgeLogJournal

    @RetainThru  datetime

AS

    SET NOCOUNT on

    DECLARE
      @HowMany   int
     ,@Earliest  datetime
     ,@Latest    datetime


    --  Figure out how much there is to delete
    SELECT
       @HowMany  = count(*)
      ,@Earliest = min(Logged)
      ,@Latest   = max(Logged)
     from LogJournal
     where OriginatingLogID in (select OriginatingLogID
                                   from LogJournal
                                   group by OriginatingLogID
                                   having max(Logged) < @RetainThru)

    IF isnull(@HowMany, 0) > 0
     BEGIN
        --  There's stuff to delete.  If there's too much to delete all at once,
        --  future generations will need to break the delete into several pieces.
        DELETE LogJournal
         where OriginatingLogID in (select OriginatingLogID
                                       from LogJournal
                                       group by OriginatingLogID
                                       having max(Logged) < @RetainThru)

        --  Log the fact that it's been deleted
        INSERT PurgeLog (Subject, ItemsDeleted, First, Last)
         values ('Log Journal', @HowMany, @Earliest, @Latest)
     END

    --  Log the most recent run in the parent table
    UPDATE PurgeManagement
     set LastCall = getdate()
     where Subject = 'Log Journal'

RETURN
