/*

Add a purge routine to a database configured with the purge system.  This procedure will fail
if the database has not been configured; it will also fail if this purge routine has already
been configured in the database.

First, earch and replace on:
  System Errors = Purge routine name/title/identifier
  dbo.PurgeSystemErrors = Name of new purge stored procedure
  SystemErrors = Table being purged
  DateCreated = [small]datetime column in SystemErrors to base the purge criteria upon

Next, configure the parameters below

Lastly, review and configure the stored procedure as necessary.  Not all purges will be simple...


--  Review
SELECT * from PurgeManagement where Subject = 'System Errors'

*/


DECLARE
  @PurgeTarget   varchar(25)
 ,@Description   varchar(200)
 ,@PeriodType    varchar(6)
 ,@PeriodLength  tinyint
 ,@Routine       varchar(100)


SET @PurgeTarget = 'System Errors'
  --  Name/title/identifier of the purge routine

SET @Description  = 'Clear out data from the SystemErrors table'
  --  Description of what's being purged

SET @PeriodType = 'Weeks'
  --  Retention period type; we're keeping data X many of these units.
  --  Valid values are 'Days', 'Weeks', 'Months', or 'Years'

SET @PeriodLength = 6
  --  How many period units we're keeping data for (e.g. 3 months, 8 weeks)

SET @Routine = 'EXECUTE dbo.PurgeSystemErrors'
  --  The stored procedure to call.  Include the "EXECUTE" statement.


--  Error checking  -----------------------------------------------------------

IF object_id('PurgeManagement') is null
    --  Main purge table not present in this database
    RAISERROR ('This database has not been configured with the purge management components', 20, 1) with log

IF exists (select 1 from PurgeManagement where Subject = @PurgeTarget)
    --  Purge subject has already been configured in this database
    RAISERROR ('This database has already been configured with this purge item', 20, 1) with log


INSERT PurgeManagement (Subject, Description, Period, PeriodType, Routine)
 values
 (
   @PurgeTarget
  ,@Description
  ,@PeriodLength
  ,@PeriodType
  ,@Routine
 )


IF objectproperty(object_id('dbo.PurgeSystemErrors'), 'isProcedure') = 1
    DROP PROCEDURE dbo.PurgeSystemErrors

GO
/******************************************************************************
**
**  Procedure: dbo.PurgeSystemErrors
**  Description: This routine will purge (delete) all rows from the SystemErros
**  table that were entered prior to the passed-in retention date.
**        
**
**  Return values:  none
** 
**  Input parameters:   Once done, we should have no data from before this
**                       date stored within the table.
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  11/06/2007  PKelley   Procedure created
**  
*******************************************************************************/
CREATE PROCEDURE dbo.PurgeSystemErrors

    @RetainThru  datetime

AS

    SET NOCOUNT on

    DECLARE
      @HowMany   int
     ,@Earliest  datetime
     ,@Latest    datetime

    --  Figure out how much there is to delete
    SELECT
       @HowMany  = count(*)
      ,@Earliest = min(DateCreated)
      ,@Latest   = max(DateCreated)
     from SystemErrors
     where DateCreated < @RetainThru

    IF isnull(@HowMany, 0) > 0
     BEGIN
        --  There's stuff to delete.  (If there's too much to delete all at once,
        --  you'll need to break the delete into several pieces.  "Too much"
        --  is, of course, extremely subjective.)
        DELETE SystemErrors
         where DateCreated < @RetainThru

        --  Log the fact that it's been deleted
        INSERT PurgeLog (Subject, ItemsDeleted, First, Last)
         values ('System Errors', @HowMany, @Earliest, @Latest)
     END

    --  Log the most recent run in the parent table
    UPDATE PurgeManagement
     set LastCall = getdate()
     where Subject = 'System Errors'

RETURN
GO
