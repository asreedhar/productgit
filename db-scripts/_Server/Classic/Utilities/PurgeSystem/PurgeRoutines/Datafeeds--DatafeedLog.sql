/*

Add a purge routine to a database configured with the purge system.  This procedure will fail
if the database has not been configured; it will also fail if this purge routine has already
been configured in the database.

First, earch and replace on:
  Datafeed Log = Purge routine name/title/identifier
  PurgeDatafeedLog = Name of new purge stored procedure
  DatafeedActivityLog = Table being purged
  StartedAt = [small]datetime column in DatafeedActivityLog to base the purge criteria upon

Next, configure the parameters below

Lastly, review and configure the stored procedure as necessary.  Not all purges will be simple...


--  Review
SELECT * from PurgeManagement where Subject = 'Datafeed Log'

*/


DECLARE
  @PurgeTarget   varchar(25)
 ,@Description   varchar(200)
 ,@PeriodType    varchar(6)
 ,@PeriodLength  tinyint
 ,@Routine       varchar(100)


SET @PurgeTarget = 'Datafeed Log'
  --  Name/title/identifier of the purge routine

SET @Description  = 'Purge old datafeed log activity'
  --  Description of what's being purged

SET @PeriodType = 'Months'
  --  Retention period type; we're keeping data X many of these units.
  --  Valid values are 'Days', 'Weeks', 'Months', or 'Years'

SET @PeriodLength = 3
  --  How many period units we're keeping data for (e.g. 3 months, 8 weeks)

SET @Routine = 'EXECUTE PurgeDatafeedLog'
  --  The stored procedure to call.  Include the "EXECUTE" statement.


--  Error checking  -----------------------------------------------------------

IF object_id('PurgeManagement') is null
    --  Main purge table not present in this database
    RAISERROR ('This database has not been configured with the purge management components', 20, 1) with log

IF exists (select 1 from PurgeManagement where Subject = @PurgeTarget)
    --  Purge subject has already been configured in this database
    RAISERROR ('This database has already been configured with this purge item', 20, 1) with log


INSERT PurgeManagement (Subject, Description, Period, PeriodType, Routine)
 values
 (
   @PurgeTarget
  ,@Description
  ,@PeriodLength
  ,@PeriodType
  ,@Routine
 )


IF objectproperty(object_id('PurgeDatafeedLog'), 'isProcedure') = 1
    DROP PROCEDURE PurgeDatafeedLog

GO
/******************************************************************************
**
**  Procedure: PurgeDatafeedLog
**  Description: This routine will purge (delete) all rows from the datafeed
**  activity log that were entered prior to the passed-in retention date.
**        
**
**  Return values:  none
** 
**  Input parameters:   Once done, we should have no data from before this
**                       date stored within the table.
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  04/26/2006  PKelley   Procedure created
**  
*******************************************************************************/
CREATE PROCEDURE dbo.PurgeDatafeedLog

    @RetainThru  datetime

AS

    SET NOCOUNT on

    DECLARE
      @HowMany   int
     ,@Earliest  datetime
     ,@Latest    datetime

    --  Figure out how much there is to delete
    SELECT
       @HowMany  = count(*)
      ,@Earliest = min(StartedAt)
      ,@Latest   = max(StartedAt)
     from DatafeedActivityLog
     where StartedAt < @RetainThru

    IF isnull(@HowMany, 0) > 0
     BEGIN
        --  There's stuff to delete.  (If there's too much to delete all at once,
        --  you'll need to break the delete into several pieces.  "Too much"
        --  is, of course, extremely subjective.)
        DELETE DatafeedActivityLog
         where StartedAt < @RetainThru

        --  Log the fact that it's been deleted
        INSERT PurgeLog (Subject, ItemsDeleted, First, Last)
         values ('Datafeed Log', @HowMany, @Earliest, @Latest)
     END

    --  Log the most recent run in the parent table
    UPDATE PurgeManagement
     set LastCall = getdate()
     where Subject = 'Datafeed Log'

RETURN
GO
