/*

Add a purge routine to a database configured with the purge system.  This procedure will fail
if the database has not been configured; it will also fail if this purge routine has already
been configured in the database.

First, earch and replace on:
  Inventory Dataload = Purge routine name/title/identifier
  PurgeInventoryDataloadArchive = Name of new purge stored procedure

NOTE: this routine hits four biiiig tables in the Archive database, and the "basis" date
is found in the DBASTAT database


--  Review
SELECT * from PurgeManagement where Subject = 'Inventory Dataload'

*/


DECLARE
  @PurgeTarget   varchar(25)
 ,@Description   varchar(200)
 ,@PeriodType    varchar(6)
 ,@PeriodLength  tinyint
 ,@Routine       varchar(100)


SET @PurgeTarget = 'Inventory Dataload'
  --  Name/title/identifier of the purge routine

SET @Description  = 'Delete Inventory Dataload archive data from the system'
  --  Description of what's being purged

SET @PeriodType = 'Months'
  --  Retention period type; we're keeping data X many of these units.
  --  Valid values are 'Days', 'Weeks', 'Months', or 'Years'

SET @PeriodLength = 6
  --  How many period units we're keeping data for (e.g. 3 months, 8 weeks)

SET @Routine = 'EXECUTE PurgeInventoryDataloadArchive'
  --  The stored procedure to call.  Include the "EXECUTE" statement.


--  Error checking  -----------------------------------------------------------

IF object_id('PurgeManagement') is null
    --  Main purge table not present in this database
    RAISERROR ('This database has not been configured with the purge management components', 20, 1) with log

IF exists (select 1 from PurgeManagement where Subject = @PurgeTarget)
    --  Purge subject has already been configured in this database
    RAISERROR ('This database has already been configured with this purge item', 20, 1) with log


INSERT PurgeManagement (Subject, Description, Period, PeriodType, Routine)
 values
 (
   @PurgeTarget
  ,@Description
  ,@PeriodLength
  ,@PeriodType
  ,@Routine
 )


IF objectproperty(object_id('PurgeInventoryDataloadArchive'), 'isProcedure') = 1
    DROP PROCEDURE PurgeInventoryDataloadArchive

GO
/******************************************************************************
**
**  Procedure: PurgeInventoryDataloadArchive
**  Description: This routine will purge (delete) all rows from these tables:
**    - Dataload_Raw_History_Archive
**    - Audit_Inventory
**    - Audit_Sales
**    - Audit_Exceptions
**  Deletion criteria are: delete where the entry date of the load_id that
**  originated the data is earlier than the value in parameter @RetainThru.
**
**
**  Return values:  none
** 
**  Input parameters:   Once done, we should have no data from before this
**                       date stored within the table.
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  06/06/2006  PKelley   Procedure created
**  
*******************************************************************************/
CREATE PROCEDURE dbo.PurgeInventoryDataloadArchive

    @RetainThru  datetime

AS

    SET NOCOUNT on

    DECLARE
      @HowMany   int
     ,@Earliest  datetime
     ,@Latest    datetime

    --  Get a list of Load_IDs whose data can be deleted
    DECLARE @Target table
     (Load_ID  int  not null  primary key)
    
    INSERT @Target (Load_ID)
     select distinct drha.Load_ID
      from Dataload_Raw_History_Archive drha
       inner join DBASTAT..Dataload_History dh
        on dh.Load_ID = drha.Load_ID
      where dh.EntryDT < @RetainThru

    IF @@rowcount > 0
     BEGIN
        --  There's stuff to delete.  (Volume may some day require this section
        --  to be chunkified.)

        --  First, get the date range of the data being deleted
        SELECT
           @Earliest = min(EntryDT)
          ,@Latest   = max(EntryDT)
         from DBASTAT..Dataload_History
         where Load_ID in (select Load_ID from @Target)


        --  Data is deleted from four different tables; we add all rows deleted to get the final count
        DELETE Audit_Inventory_Archive with (tablockx)
         from Audit_Inventory_Archive arc
          inner join Dataload_Raw_History_Archive drha
           on drha.Audit_id = arc.Audit_ID
          inner join @Target xx
           on xx.Load_id = drha.Load_ID
         option (maxdop 1)  --  Stick with one CPU, don't clog a server with this process

        SET @HowMany = @@rowcount

        DELETE Audit_Sales_Archive with (tablockx)
         from Audit_Sales_Archive arc
          inner join Dataload_Raw_History_Archive drha
           on drha.Audit_id = arc.Audit_ID
          inner join @Target xx
           on xx.Load_id = drha.Load_ID
         option (maxdop 1)  --  Stick with one CPU, don't clog a server with this process

        SET @HowMany = @HowMany + @@rowcount

        DELETE Audit_Exceptions_Archive with (tablockx)
         from Audit_Exceptions_Archive arc
          inner join Dataload_Raw_History_Archive drha
           on drha.Audit_id = arc.Audit_ID
          inner join @Target xx
           on xx.Load_id = drha.Load_ID
         option (maxdop 1)  --  Stick with one CPU, don't clog a server with this process

        SET @HowMany = @HowMany + @@rowcount

        DELETE Dataload_Raw_History_Archive with (tablockx)
         from Dataload_Raw_History_Archive drha
          inner join @Target xx
           on drha.load_id = xx.load_id
         option (maxdop 1)  --  Stick with one CPU, don't clog a server with this process

        SET @HowMany = @HowMany + @@rowcount

        --  Log the event
        INSERT PurgeLog (Subject, ItemsDeleted, First, Last)
         values ('Inventory Dataload', @HowMany, @Earliest, @Latest)
     END

    --  Log the most recent run in the parent table
 UPDATE PurgeManagement
     set LastCall = getdate()
     where Subject = 'Inventory Dataload'

RETURN
GO
