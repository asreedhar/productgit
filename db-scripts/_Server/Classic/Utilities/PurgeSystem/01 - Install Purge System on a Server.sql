/*

Install the components and basic configuration for the "Purge Non-essential Data" system.
This includes:
 - SQL Agent job '_Purge Non-essential Data', configured to call msdb..PurgeManager
 - Table msdb..PurgeManagement
 - Table msdb..PurgeLog
 - Procedure msdb..PurgeManager
 - Procedure msdb..PurgePurgeLog
 - Settings in PurgeManagement to call PurgeManager

NOTE: The job is installed disabled.  Test it before enabling it!

*/

BEGIN TRANSACTION            

DECLARE @JobID BINARY(16)  
DECLARE @ReturnCode INT    
SELECT @ReturnCode = 0     

IF (SELECT COUNT(*) FROM msdb.dbo.syscategories WHERE name = N'Database Maintenance') < 1 
    EXECUTE msdb.dbo.sp_add_category @name = N'Database Maintenance'

IF (SELECT COUNT(*) FROM msdb.dbo.sysjobs WHERE name = N'_Purge Non-essential Data') > 0 
    PRINT N'The job "_Purge Non-essential Data" already exists so will not be replaced.'

ELSE
 BEGIN 

    -- Add the job
    EXECUTE @ReturnCode = msdb.dbo.sp_add_job
      @job_id = @JobID OUTPUT 
     ,@job_name = N'_Purge Non-essential Data'
     ,@owner_login_name = N'sa'
     ,@description = N'Call the routines to purge sufficiently aged non-essential data from specified databases.'
     ,@category_name = N'Database Maintenance'
     ,@enabled = 0
     ,@notify_level_email = 0
     ,@notify_level_page = 0
     ,@notify_level_netsend = 0
     ,@notify_level_eventlog = 2
     ,@delete_level= 0
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

    -- Add the job steps
    EXECUTE @ReturnCode = msdb.dbo.sp_add_jobstep
      @job_id = @JobID
     ,@step_id = 1
     ,@step_name = N'Purge MSDB'
     ,@command = N'EXECUTE PurgeManager'
     ,@database_name = N'msdb'
     ,@server = N''
     ,@database_user_name = N''
     ,@subsystem = N'TSQL'
     ,@cmdexec_success_code = 0
     ,@flags = 0
     ,@retry_attempts = 0
     ,@retry_interval = 1
     ,@output_file_name = N''
     ,@on_success_step_id = 0
     ,@on_success_action = 1
     ,@on_fail_step_id = 0
     ,@on_fail_action = 2
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

    --  Set the first job step
    EXECUTE @ReturnCode = msdb.dbo.sp_update_job
      @job_id = @JobID
     ,@start_step_id = 1 
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

    -- Add the job schedules
    EXECUTE @ReturnCode = msdb.dbo.sp_add_jobschedule
      @job_id = @JobID
     ,@name = N'Weekly, Friday evening at 8:00'
     ,@enabled = 1
     ,@freq_type = 8
     ,@active_start_date = 20050923
     ,@active_start_time = 200000
     ,@freq_interval = 32
     ,@freq_subday_type = 1
     ,@freq_subday_interval = 0
     ,@freq_relative_interval = 0
     ,@freq_recurrence_factor = 1
     ,@active_end_date = 99991231
     ,@active_end_time = 235959
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 

    -- Add the Target Servers
    EXECUTE @ReturnCode = msdb.dbo.sp_add_jobserver
      @job_id = @JobID
     ,@server_name = N'(local)' 
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback 
 END

COMMIT TRANSACTION          
GOTO   EndSave              

QuitWithRollback:
IF (@@TRANCOUNT > 0)
    ROLLBACK TRANSACTION 

RAISERROR ('Unable to install Purge job', 20, 1) with log

EndSave: 
GO

USE msdb

/*

* * * * *
*
*  NOTE: The following is a copied and lightly edited version of the purge
*  component installation routine.
*

Install the baseline PURGE objects for the target database.  This will fail if the main
Purge job has not been configured on the server.

Assumptions:
 - Adding objects to the currently selected database

Objects created:
 - Table PurgeManagement
 - Table PurgeLog
 - Procedure PurgeManager
 - Procedure PurgePurgeLog  [ Procedure naming convention: Purge<purgeTarget> ]
 - Adds a step to the purge job
 - Settings to configures the system to call PurgePurgeLog

*/

SET NOCOUNT on

--  Create tables
CREATE TABLE PurgeManagement
 (
   Subject      varchar(25)    not null
    constraint PK_PurgeManagement
     primary key  clustered
  ,Description  varchar(200)   not null
  ,Period       tinyint        not null
  ,PeriodType   varchar(6)     not null  --  Should be 'Days', 'Weeks', 'Months', or 'Years'
  ,Routine      varchar(100)   not null
  ,LastCall     smalldatetime  not null
    constraint DF_PurgeManagement
     default 'Jan 1, 1980'
 )


CREATE TABLE PurgeLog
 (
   Subject       varchar(25)  not null
    constraint FK_PurgeLog__PurgeManagement
     foreign key references PurgeManagement (Subject)
  ,Executed      datetime     not null
    constraint DF_PurgeLog__Executed
     default CURRENT_TIMESTAMP
  ,ItemsDeleted  int          not null
  ,First         varchar(50)  not null
  ,Last          varchar(50)  not null
  ,constraint PK_PurgeLog
    primary key  nonclustered (Subject, Executed)
    with fillfactor = 90
 )

GO

--  Set up a purge routine to purge the purge system
INSERT PurgeManagement (Subject, Description, Period, PeriodType, Routine)
 values
 (
   'Purge Log'
  ,'Clear out records of having purged data'
  ,6
  ,'Months'
  ,'EXECUTE PurgePurgeLog'
 )

GO
/******************************************************************************
**
**  Procedure: PurgeManager
**  Description: This routine walks through table PurgeManagement, calculates
**   the date before which we do not wish to retain data, and runs the routine
**   defined in the table, passing that date as a parameter.
**        
**
**  Return values:  none
** 
**  Input parameters:   none
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  08/30/2005  PKelley   More robust incarnation
**  
*******************************************************************************/
CREATE PROCEDURE dbo.PurgeManager
AS

    SET NOCOUNT on

    DECLARE
      @Subject     varchar(25)
     ,@Period      tinyint
     ,@PeriodType  varchar(6)
     ,@Routine     varchar(100)
     ,@Command     varchar(1000)
     ,@RetainThru  smalldatetime

    
    --  Go through every item (row) in the table and attempt to process it
    DECLARE cPurgeList CURSOR fast_forward for
     select Subject, Period, PeriodType, Routine
      from PurgeManagement

    OPEN cPurgeList
    FETCH next from cPurgeList into @Subject, @Period, @PeriodType, @Routine

    WHILE @@fetch_status = 0
     BEGIN
        --  Determine the desired cut-off date
        SET @RetainThru = case @PeriodType
                           when 'Days'   then dateadd(dd, -@Period, getdate())
                           when 'Weeks'  then dateadd(ww, -@Period, getdate())
                           when 'Months' then dateadd(mm, -@Period, getdate())
                           when 'Years'  then dateadd(yy, -@Period, getdate())
                           else null
                          end

        IF @RetainThru is not null
         BEGIN
            --  Build a string like: EXECUTE myProc 'Aug 30, 2005 11:16AM'
            --  ...but actually, all we're doing is appending the retain through
            --  date (preceded by a space) to the command stored in the table.
            SET @Command = @Routine + ' ''' + convert(char(19), @RetainThru, 100) + ''''

            --  And run it
            EXECUTE (@Command)
         END

        FETCH next from cPurgeList into @Subject, @Period, @PeriodType, @Routine
     END

    CLOSE cPurgeList
    DEALLOCATE cPurgeList

RETURN
GO
/******************************************************************************
**
**  Procedure: PurgePurgeLog
**  Description: This routine will purge (delete) all rows from the PurgeLog
**   table that were entered prior to the passed-in retention date.  (This
**   can be used as a template for other purge routines.)
**        
**
**  Return values:  none
** 
**  Input parameters:   Once done, we should have no data from before this
**                       date stored within the table.
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  08/30/2005  PKelley   More robust incarnation
**  
*******************************************************************************/
CREATE PROCEDURE PurgePurgeLog

    @RetainThru  datetime

AS

    SET NOCOUNT on

    DECLARE
      @HowMany   int
     ,@Earliest  datetime
     ,@Latest    datetime

    --  Figure out how much there is to delete
    SELECT
       @HowMany  = count(*)
      ,@Earliest = min(Executed)
      ,@Latest   = max(Executed)
     from PurgeLog
     where Executed < @RetainThru

    IF isnull(@HowMany, 0) > 0
     BEGIN
        --  There's stuff to delete.  If there's too much to delete all at once,
        --  you'll need to break the delete into several pieces.  ("Too much"
        --  is, of course, extremely subjective.)
        DELETE PurgeLog
         where Executed < @RetainThru

        --  Log the fact that it's been deleted
        INSERT PurgeLog (Subject, ItemsDeleted, First, Last)
         values ('Purge Log', @HowMany, @Earliest, @Latest)
     END

    --  Log the most recent run in the parent table
    UPDATE PurgeManagement
     set LastCall = getdate()
     where Subject = 'Purge Log'

RETURN
GO
