/*

Install the baseline PURGE objects for the target database.  This routine will error
out if the main Purge job has not been configured on the server.

Assumptions:
 - Objects are to be added to the currently selected database

Objects created:
 - Table PurgeManagement
 - Table PurgeLog
 - Procedure PurgeManager
 - Procedure PurgePurgeLog  [ Procedure naming convention: Purge<purgeTarget> ]
 - Adds a step to the master purge job
 - Settings to configure the system to call PurgePurgeLog in this database


Note: if the top-level job has been renamed from "_Purge Non-essential Data", search
and replace on that string within this script with the new job name, and this script
should work.


--  Quick uninstall routine.  Note that this will NOT remove the job step!
DROP PROCEDURE PurgePurgeLog
DROP PROCEDURE PurgeManager
DROP TABLE PurgeLog
DROP TABLE PurgeManagement

*/

SET NOCOUNT on


IF not exists(select 1 from msdb..sysJobs where name = '_Purge Non-essential Data')
    --  If the purge job is not present on server, you can't load these routines.
    --  Severity 20 errors will stop the script.
    RAISERROR('The purge system has not been properly installed on this server', 20, 1) with log

IF exists (select 1
            from msdb..sysJobs sj
             inner join msdb..sysJobSteps sjs
              on sjs.job_id = sj.job_id
            where sj.name = '_Purge Non-essential Data'
             and sjs.database_name = db_name())
    --  A step already exists in the job for this database
    RAISERROR ('This database is already configured with the purge components', 20, 1) with log


--  Create tables
CREATE TABLE PurgeManagement
 (
   Subject      varchar(25)    not null
    constraint PK_PurgeManagement
     primary key  clustered
  ,Description  varchar(200)   not null
  ,Period       tinyint        not null
  ,PeriodType   varchar(6)     not null  --  Should be 'Days', 'Weeks', 'Months', or 'Years'
  ,Routine      varchar(100)   not null
  ,LastCall     smalldatetime  not null
    constraint DF_PurgeManagement
     default 'Jan 1, 1980'
 )


CREATE TABLE PurgeLog
 (
   Subject       varchar(25)  not null
    constraint FK_PurgeLog__PurgeManagement
     foreign key references PurgeManagement (Subject)
  ,Executed      datetime     not null
    constraint DF_PurgeLog__Executed
     default CURRENT_TIMESTAMP
  ,ItemsDeleted  int          not null
  ,First         varchar(50)  not null
  ,Last          varchar(50)  not null
  ,constraint PK_PurgeLog
    primary key  nonclustered (Subject, Executed)
    with fillfactor = 90
 )

GO

--  Set up a purge routine to purge the purge system
INSERT PurgeManagement (Subject, Description, Period, PeriodType, Routine)
 values
 (
   'Purge Log'
  ,'Clear out records of having purged data'
  ,6
  ,'Months'
  ,'EXECUTE PurgePurgeLog'
 )

GO
/******************************************************************************
**
**  Procedure: PurgeManager
**  Description: This routine walks through table PurgeManagement, calculates
**   the date before which we do not wish to retain data, and runs the routine
**   defined in the table, passing that date as a parameter.
**        
**
**  Return values:  none
** 
**  Input parameters:   none
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  08/30/2005  PKelley   More robust incarnation
**  
*******************************************************************************/
CREATE PROCEDURE dbo.PurgeManager
AS

    SET NOCOUNT on

    DECLARE
      @Subject     varchar(25)
     ,@Period      tinyint
     ,@PeriodType  varchar(6)
     ,@Routine     varchar(100)
     ,@Command     varchar(1000)
     ,@RetainThru  smalldatetime

    
    --  Go through every item (row) in the table and attempt to process it
    DECLARE cPurgeList CURSOR fast_forward for
     select Subject, Period, PeriodType, Routine
      from PurgeManagement

    OPEN cPurgeList
    FETCH next from cPurgeList into @Subject, @Period, @PeriodType, @Routine

    WHILE @@fetch_status = 0
     BEGIN
        --  Determine the desired cut-off date
        SET @RetainThru = case @PeriodType
                           when 'Days'   then dateadd(dd, -@Period, getdate())
                           when 'Weeks'  then dateadd(ww, -@Period, getdate())
                           when 'Months' then dateadd(mm, -@Period, getdate())
                           when 'Years'  then dateadd(yy, -@Period, getdate())
                           else null
                          end

        IF @RetainThru is not null
         BEGIN
            --  Build a string like: EXECUTE myProc 'Aug 30, 2005 11:16AM'
            --  ...but actually, all we're doing is appending the retain through
            --  date (preceded by a space) to the command stored in the table.
            SET @Command = @Routine + ' ''' + convert(char(19), @RetainThru, 100) + ''''

            --  And run it
            EXECUTE (@Command)
         END

        FETCH next from cPurgeList into @Subject, @Period, @PeriodType, @Routine
     END

    CLOSE cPurgeList
    DEALLOCATE cPurgeList

RETURN
GO
/******************************************************************************
**
**  Procedure: PurgePurgeLog
**  Description: This routine will purge (delete) all rows from the PurgeLog
**   table that were entered prior to the passed-in retention date.  (This
**   can be used as a template for other purge routines.)
**        
**
**  Return values:  none
** 
**  Input parameters:   Once done, we should have no data from before this
**                       date stored within the table.
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  08/30/2005  PKelley   More robust incarnation
**  
*******************************************************************************/
CREATE PROCEDURE PurgePurgeLog

    @RetainThru  datetime

AS

    SET NOCOUNT on

    DECLARE
      @HowMany   int
     ,@Earliest  datetime
     ,@Latest    datetime

    --  Figure out how much there is to delete
    SELECT
       @HowMany  = count(*)
      ,@Earliest = min(Executed)
      ,@Latest   = max(Executed)
     from PurgeLog
     where Executed < @RetainThru

    IF isnull(@HowMany, 0) > 0
     BEGIN
        --  There's stuff to delete.  If there's too much to delete all at once,
        --  you'll need to break the delete into several pieces.  ("Too much"
        --  is, of course, extremely subjective.)
        DELETE PurgeLog
         where Executed < @RetainThru

        --  Log the fact that it's been deleted
        INSERT PurgeLog (Subject, ItemsDeleted, First, Last)
         values ('Purge Log', @HowMany, @Earliest, @Latest)
     END

    --  Log the most recent run in the parent table
    UPDATE PurgeManagement
     set LastCall = getdate()
     where Subject = 'Purge Log'

RETURN
GO


/*

Add a step to the purge job to call PurgeMaster in a database.  The step will not be
added if this database already appears in the job.

The new step will be inserted as the first step in the job, and set such that if the
step fails the job will stop there as "failed".  (It is MUCH easier to do this than
to modify existing steps!)  The MSDB purge will always be the last step this way.

*/

DECLARE
  @TargetDatabase  varchar(100)
 ,@JobID           binary(16)
 ,@StepName        varchar(100)
 ,@ReturnCode      int


SET @TargetDatabase = db_name()
SET @StepName = 'Purge ' + @TargetDatabase


SELECT @JobID = job_id
 from msdb..sysJobs
 where name = '_Purge Non-essential Data'


EXECUTE @ReturnCode = msdb..sp_add_jobstep
  @job_id = @JobID
 ,@step_id = 1
 ,@step_name = @StepName
 ,@command = N'EXECUTE PurgeManager'
 ,@on_success_action = 3
 ,@on_fail_action = 2
 ,@database_name = @TargetDatabase

IF @ReturnCode <> 0
    --  Could not add job step
    GOTO _Failed_

GOTO _Done_


_Failed_:
RAISERROR ('Error adding Purge job step', 11, 1)

_Done_:
GO
