/*

This script installs a database defragmentation utility.  The following objects will
be created:
 - Table master.dbo.IndexDefragLog, only if it does not already exist
 - Stored procedure master.dbo.sp_phkIndexDefragUtility (alters if already exists)
 - Stored procedure msdb.dbo.BuildDefragJob (recreates if already exists)

Once installed, it will need to be configured.  The general idea is:
 - Identify when databases (particularly the larger ones) are completely backed up
 - Set up a job to run a few hours before the complete backups
 - Have these jobs call the defrag utility for the databases that will be backed up

My presumption is that large databases will be backed up weekly, and defrag runs will
occur monthly during the second week of the month.  Stored procedure msdb.dbo.BuildDefragJob
should be used to implement this (as of Sep 2007). It gets called like so:

    EXECUTE msdb.dbo.BuildDefragJob
      'JobName'
     ,'TargetDatabase'
     ,'JobAuditLogFolder'
     ,'JobAuditLogFile'
     ,AuditLogFileRetentionDays

An example:
    EXECUTE msdb.dbo.BuildDefragJob
      'Defraginator'
     ,'Datafeed'
     ,'C:\AuditLogs\DBMaintenancePlans'
     ,'Defrag.log'
     ,30



--  Uninstall routine:
USE master
DROP TABLE dbo.IndexDefragLog
DROP PROCEDURE dbo.sp_phkIndexDefragUtility
USE msdb
DROP PROCEDURE dbo.BuildDefragJob
PRINT 'Defrag utility removed from system.
You will have to manually identify and delete all jobs that call the
defrag utility--this script may help find them:

    SELECT sj.name Job, sjs.step_name JobStep
     from msdb.dbo.sysJobSteps sjs
      inner join msdb.dbo.sysJobs sj
       on sj.job_id = sjs.job_id
     where sjs.step_name like ''Defrag Database %''
     order by sj.name, sjs.step_name'


*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  09/05/2007  PKelley   Ready to roll out!
**  09/06/2007  PKelley   Make AsOf a full datetime (some reindexes are fast!)
**  09/13/2007  PKelley   APPEND to job output file!
**  11/20/2007  PKelley   Fixed some schema-related issues and other bugs.
**
**************************************************************************

*/

--  Create table master.dbo.IndexDefragLog
USE master

IF isnull(objectproperty(object_id('IndexDefragLog'), 'IsUserTable'), 0) = 0
 BEGIN
    --  Create the table if it's not already there
    CREATE TABLE dbo.IndexDefragLog
     (
       AsOf               datetime      not null
        constraint DF_IndexDefragLog__AsOf
         default CURRENT_TIMESTAMP
      ,DBName             sysname       not null
      ,TableName          sysname       not null
      ,IndexName          sysname       not null
      ,IndexType          varchar(20)   not null
      ,Partition          int           not null
      ,Pages              bigint        not null
      ,FragPercent        float         not null
      ,Fill_Factor        tinyint       not null
      ,IsPrimaryKey       char(1)       not null
      ,IsUnique           char(1)       not null
      ,RecommendedAction  varchar(50)   not null
     )

    CREATE clustered INDEX IX_IndexDefragLog__AsOf
     on dbo.IndexDefragLog (AsOf)
    CREATE nonclustered INDEX IX_IndexDefragLog__DBName
     on dbo.IndexDefragLog (DBName)
 END


--  Create and configure procedure sp_phkIndexDefragUtility.  Note comments
--  in the procedure header!
IF objectproperty(object_id('sp_phkIndexDefragUtility'), 'isProcedure') is null
 BEGIN
    --  Procedure (or function) does not exist, create a dummy placeholder
    DECLARE @Placeholder varchar(100)
    SET @Placeholder = 'CREATE PROCEDURE dbo.sp_phkIndexDefragUtility AS RETURN 0'
    EXECUTE (@PlaceHolder)

    --  Make it a system procedure (!!)
    EXECUTE sp_ms_markSystemObject 'sp_phkIndexDefragUtility'
 END

GO
/******************************************************************************
**
**  Procedure: sp_phkIndexDefragUtility
**  Description: Tool for managing table/index fragmentation.  Parameters
**   allow you to report on current status, or to reindex, reorganize, or
**   take no action as determined by the algorithms herein.
**
**  !! IMPORTANT !!  This procedure is created in the master database, and then
**  is marked as a system object via the *undocumented* procedure sp_ms_markSystemObject.
**  This allows the procedure to be called (as an "sp_" procedure) from other
**  databases AND be able to reference the system objects (such as sys.objects)
**  in the target database.  (If not done, references to system objects resolve
**  to those in the master database.)  This appears safe enough to do, but it's
**  undocumented, and who knows what if any other ramifications this action entails.
**   Be wary!
**
**
**  Return values:  none
**
**  Input parameters:   See below!
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:        Description:
**  -------     -------------  -------------------------------------------
**  08/02/2007  Eric Peterson  Adapted and modified by PKelley from article in SQL
**                              Server Magazine, July 2007, with code downloaded
**                              from InstantDoc 96059.  Routine was originally
**                              called  "cspDefragIndexes".
**  09/13/2007  PKelley        Worked in TRY/CATCH for when online reindexing
**                              runs afoul of blobby datatypes.
**  09/28/2007  PKelley        Factored in schemas.  Suprise--Eric already had
**                              done it, I just had to fix my own additions.
**  11/20/2007  PKelley        Fixed some schema-related issues, made thresholds
**                              a bit more configurable.
**  01/09/2009  WGH	       Bracketized @objectname as certain reserved words
**                              (e.g. User) as object names cause problems in 
**                              64-bit T-SQL
**
*******************************************************************************/
ALTER PROCEDURE dbo.sp_phkIndexDefragUtility 

    @Action   varchar(20)  = 'Report'
      --  'Defrag' to run the defrag routines, anything else produces a report

   ,@TableIn  varchar(200) = NULL
      --  Specify the table to process, or null for all user tables

   ,@Debug    bit          = 0
      --  Pass 1 to only list commands that would be run in Defrag mode
    
AS

    SET NOCOUNT ON

    DECLARE
      @objectid          int
     ,@holdOid           int
     ,@indexid           int
     ,@DBid              int
     ,@partitioncount    bigint
     ,@schemaname        sysname
     ,@objectname        sysname
     ,@indexname         sysname
     ,@partitionnum      bigint
     ,@frag              float
     ,@command           varchar(8000)
     ,@PageLock          int
     ,@cnt               int
     ,@WithOnline        varchar(20)
     ,@Error             int
     ,@Pages             bigint
     ,@ReorgThreshold    real
     ,@RebuildThreshold  real


    --  Set "Constants"
    SET @ReorgThreshold   =  5.0
      --  If fragmentation percent is less than this value, no action will be taken

    SET @RebuildThreshold = 30.0
      --  If fragmentation percent is between @ReorgThreshold and this value inclusive,
      --  if possible (see comments elsewhere) the index will be reorganized.  If not
      --  possible or greater than this value, the index will be rebuilt.


    --  Configure settings
    SET @dbid = db_id()

    IF @TableIn is null
        --  All tables
        SET @objectid = NULL
    ELSE
     BEGIN
        --  User-specified table
        SELECT @objectid = object_id
         from sys.objects
         where name = @TableIn

        IF @objectid is null
         BEGIN
        --  No such table
            PRINT 'Table "' + @TableIn + '" not found'
            RETURN -1
         END
     END

    --  Determine if online processing can be done
    SET @WithOnline = ''
    IF left(cast(serverproperty('edition') as varchar(100)), 9) in ('Developer', 'Enterpris') -- intentionally abbreviated
       SET @WithOnline = ', online = on'


    CREATE TABLE #IndexFragmentationTable
     (
       objectid      int     null
      ,indexid       int     null
      ,partitionnum  int     null
      ,pages         bigint  null
      ,frag          float   null
     )

    --  Load key data
    INSERT #IndexFragmentationTable
     select
        object_id
       ,index_id
       ,partition_number
       ,page_count
       ,avg_fragmentation_in_percent
      from sys.dm_db_index_physical_stats (@dbid, @objectid, NULL , NULL, 'LIMITED')


    IF isnull(@Action, 'Report') <> 'Defrag'
        --  Just show a fragmentation "report"
        SELECT   db_name()         as DBName
                ,case s.name when 'dbo' then '' else s.name + '.' end
                  + t.name         as TableName
                ,isnull(i.name, ' <heap>')  as IndexName 
                ,i.type_Desc       as IndexType
                ,w.partitionnum    as [Partition]
                ,w.pages           as Pages
                ,w.frag            as FragPct
                ,i.fill_factor     as [FillFactor]
                ,case i.is_primary_key
                   when 1    then 'Y'
                   else ' '
                 end as PKIndex
                ,case i.is_unique
                   when 1    then 'Y'
                   else ' '
                 end as [Unique]
                ,case
                   when w.pages < 8 then 'Under 8 pages, irrelevant'
                   when i.index_id = 0 and w.frag < @ReorgThreshold then 'Heap'
                   when i.index_id = 0 then 'Heap, may require advanced defrag tactics'
                   when w.frag < @ReorgThreshold then 'No action'
                   when w.frag <= @RebuildThreshold and i.Allow_Page_locks = 1 then 'Reorganize'
                   else 'Rebuild'
                 end  RecommendedAction
         from #IndexFragmentationTable w
         inner join sys.objects t on w.objectId = t.object_id
         inner join sys.schemas s on s.schema_id = t.schema_id
         inner join sys.indexes i on w.objectId = i.object_id and w.indexId = i.index_id
        where t.object_id = isnull(@objectid, t.object_id)
        order by t.name, i.Index_Id

    ELSE
     BEGIN
        --  Do the defragmentation work

        IF @Debug = 1
            PRINT 'DEBUG mode, commands are only listed and NOT executed'

        PRINT ''

        --  Set up a cursor loop, within which we build and execute dynamic SQL
        DECLARE IndexFragmentationCursor CURSOR FOR 
         select objectid, indexid, partitionnum, pages, frag
          from #IndexFragmentationTable
          order by object_name(objectid), indexid


        OPEN IndexFragmentationCursor

        -- Loop through the IndexFragmentationCursor.
        FETCH NEXT
         FROM IndexFragmentationCursor
         INTO @objectid, @indexid, @partitionnum, @Pages, @frag

        SET @holdOid = @objectid
        PRINT '--  Processing indexes for ' + object_name(@objectid) + '  --------------'

        WHILE @@FETCH_STATUS = 0
         BEGIN
            --  New object, finish off last object
            IF @objectid != @HoldOid
             BEGIN
                --  About to work on new table, update stats of the last table
                SET @command = 'UPDATE STATISTICS ' + @schemaname + '.' + '[' + @objectname + ']'
                PRINT @command

                IF @Debug = 0
                    EXECUTE(@command)
             END


            --  Set variables for this index/partition
            SELECT @objectname = o.name, @schemaname = s.name
             from sys.objects AS o
             join sys.schemas as s ON s.schema_id = o.schema_id
             where o.object_id = @objectid

            SELECT @indexname = name, @PageLock  = Allow_Page_locks
             FROM sys.indexes
             WHERE object_id = @objectid AND index_id = @indexid

            SELECT @partitioncount = count(*)
             FROM sys.partitions
             WHERE object_id = @objectid AND index_id = @indexid

            --  Set up for new object
            IF @objectid != @HoldOid
             BEGIN
                SET @holdOid = @objectId
                PRINT ''
                PRINT '--  Processing indexes for ' +'[' + @objectname + ']' + '  --------------'
             END


            SET @Error = 0

            IF @indexid = 0
                --  Heap, no index work to be done
                Print '--  Heap, defragging would require building (and dropping) a clustered index'

            ELSE IF @Pages < 8
                --  Small (i.e. mixed extent) table, no index work to be done
                Print '--  Under 8 pages = mixed extents, so can''t really defrag ' + @indexname

            ELSE IF @frag < @ReorgThreshold
                --  Don't process if fragmentation is negligible
                Print '--  Fragmentation under ' + cast(@ReorgThreshold as varchar(10)) + '% for ' + @indexname

            ELSE IF  @frag between @ReorgThreshold and @RebuildThreshold and @PageLock = 1
             BEGIN
                --  Reorganize if fragmentation is low AND pagelocks enabled
                SET @command = 'ALTER INDEX ' + @indexname + ' ON ' + @schemaname + '.' + '[' + @objectname + ']' + ' REORGANIZE'
                IF @partitioncount > 1
                    SET @command = @command + ' PARTITION=' + CONVERT (CHAR, @partitionnum)

                IF @Debug = 0
                    EXECUTE (@command)

                SET @Error = @@error
                IF @Error <> 0 GOTO _Problem_
                PRINT @command
             END

            ELSE IF @frag >= @RebuildThreshold
             or ( @PageLock = 0 and @frag > @ReorgThreshold )
             BEGIN
                --  Rebuild if fragmentation is high OR fragmentation is low and pagelocks NOT enabled
                SET @command = 'ALTER INDEX ' + @indexname + ' ON ' + @schemaname + '.' + '[' + @objectname + ']' + ' REBUILD'

                IF @partitioncount = 1
                    --  Not partitioned, can perform online (if using valid edition)
                    SET @command = @command + ' with (sort_in_tempdb = on' + @WithOnline + ')'
                ELSE
                    --  Partitioned table, cannot perform online
                    SET @command = @command + ' PARTITION=' + cast(@partitionnum as varchar(10))
                     + ' with (sort_in_tempdb = on)'


                --  Issues with online reindexing of blob-esque datatypes
                BEGIN TRY
                    IF @Debug = 0
                        EXECUTE (@command)
                END TRY
                BEGIN CATCH
                    IF error_number() = 2725
                     BEGIN
                        --  Attempt to perform an online reindex operation conflicts with a
                        --  blob-type column.  Take out the online clause and try again
                        PRINT '--  Blob-type column encountered, reverting to offline reindex'
                        SET @command = replace(@command, ', online = on', '')
                        EXECUTE (@command)
                        SET @Error = @@error
                        --  If this blows up, standard error handling should catch it
                     END
                    ELSE
                        --  Something else happened, pass it on
                        SET @Error = error_number()
                END CATCH

                IF @Error <> 0 GOTO _Problem_
                PRINT @command
 END

            GOTO _Loop_

            _Problem_:
            Print 'Error #' + cast(@Error as varchar(10)) + ' generated while running: ' + @command

            _Loop_:
            FETCH NEXT
             FROM IndexFragmentationCursor
             INTO @objectid, @indexid, @partitionnum, @Pages, @frag
         END

        --  update stats on the last table worked on
        SET @command = 'UPDATE STATISTICS ' + @schemaname + '.' + '[' + @objectname + ']'
        PRINT @command
        IF @Debug = 0
            EXECUTE(@command)

        -- Close and deallocate the cursor.
        CLOSE IndexFragmentationCursor
        DEALLOCATE IndexFragmentationCursor
      END

RETURN

GO

--  Create procedure msdb.dbo.BuildDefragJob
USE msdb

IF objectproperty(object_id('BuildDefragJob'), 'isProcedure') = 1
    DROP PROCEDURE dbo.BuildDefragJob

GO
/******************************************************************************
**
**  Procedure: BuildDefragJob
**  Description: Add a step to a job to call the sp_phkIndexDefragUtility routine
**   for a target database.  This is intended to be run weekly, a few hours
**   before a complete backup of the target database.  The procedure will be
**   run in "report" mode, with results stored in master.dbo.IndexDefragLog;
**   then, if and only if it's the second week of the month, the defrag
**   routine will be run.  (Yes, this may need to be revised, depending on
**   rate of fragmentation.)
**
**  If the specified job does not exist, it will be created.
**
**
**  Return values:  none
**
**  Input parameters:   See below
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  09/05/2007  PKelley   Procedure created, based on other similar routines
**  
*****************************************************************************/
CREATE PROCEDURE dbo.BuildDefragJob

     @JobName        sysname
       --  The job to add this defrag call to (will create if necessary)

    ,@DBName         sysname
       --  The database to defrag

    ,@LogFileFolder  nvarchar(200)
       --  Folder in which to create the log file.  KEEP IT CONSISTANT when
       --  setting up jobs!

    ,@LogFileName    nvarchar(200)
       --  Log file name.  KEEP IT CONSISTANT when setting up jobs!

    ,@RetainDays     int
       --  How long to keep log files, only used when job is first created

AS

    SET NOCOUNT on

    --  Set things up
    DECLARE
      @ReturnCode       int
     ,@JobID            binary(16)
     ,@JobFailStep  int
     ,@Command          nvarchar(2000)
     ,@IsNewJob         bit
     ,@StepName         sysname
     ,@FullLogFile    nvarchar(400)

    SET @ReturnCode = 0
    SET @IsNewJob = 0
    SET @StepName = N'Defrag Database ' + @DBName
    SET @FullLogFile = @LogFileFolder + N'\' + @LogFileName

    BEGIN TRANSACTION  ----------------------------------------------------------------------

    SELECT
       @JobID = sj.job_id
      ,@JobFailStep = max(sjs.step_id) + 1  --  Last step is always "on failure" step
     from sysJobs sj
      inner join sysJobSteps sjs
       on sjs.job_id = sj.job_id
     where sj.name = @JobName
     group by sj.job_id

    IF @JobID is null
     BEGIN
        --  Job does not exist, create it
        PRINT 'Job "' + @JobName + '" will be created'
        SET @IsNewJob = 1
        SET @JobFailStep = 3

        --  If not found, create category "Database Maintenance"
        IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
         BEGIN
            EXECUTE @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
            IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
         END

        --  Create the job
        EXECUTE @ReturnCode = msdb.dbo.sp_add_job
          @job_name              = @JobName
         ,@enabled               = 0
         ,@notify_level_eventlog = 2
         ,@notify_level_email    = 0
         ,@notify_level_netsend  = 0
         ,@notify_level_page     = 0
         ,@delete_level          = 0
         ,@description           = N'Database defragmentation routine'
         ,@category_name         = N'Database Maintenance'
         ,@owner_login_name      = N'sa'
         ,@job_id                = @JobID OUTPUT
        IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

        --  Create the "on success" step
        SET @Command = N'--  Timestamp audit log files ,delete sufficiently aged ones
EXECUTE sp_FileTimestampAndCycle ''' + @LogFileFolder + N''', ''' + @LogFileName + N''' , ' + cast(@RetainDays as varchar(3))

        EXECUTE @ReturnCode = msdb.dbo.sp_add_jobstep
          @job_id               = @jobId
         ,@step_name            = N'Timestamp and cycle audit log (on success)'
         ,@step_id              = 1
         ,@cmdexec_success_code = 0
         ,@on_success_action    = 1
         ,@on_success_step_id   = 0
         ,@on_fail_action       = 2
         ,@on_fail_step_id      = 0
         ,@retry_attempts       = 0
         ,@retry_interval       = 1
         ,@os_run_priority      = 0
         ,@subsystem            = N'TSQL'
         ,@command              = @Command
         ,@database_name        = N'master'
         ,@flags                = 0
        IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

        --  Create the "on failure" step
        SET @Command = N'--  Timestamp audit log files ,delete sufficiently aged ones
EXECUTE sp_FileTimestampAndCycle ''' + @LogFileFolder + ''', ''' + @LogFileName + ''' , ' + cast(@RetainDays as varchar(3))
        EXECUTE @ReturnCode = msdb.dbo.sp_add_jobstep
          @job_id               = @jobId
         ,@step_name            = N'Timestamp and cycle audit log (on failure)'
         ,@step_id              = 2
         ,@cmdexec_success_code = 0
         ,@on_success_action    = 2
         ,@on_success_step_id   = 0
         ,@on_fail_action       = 2
         ,@on_fail_step_id      = 0
         ,@retry_attempts       = 0
         ,@retry_interval       = 1
         ,@os_run_priority      = 0
         ,@subsystem            = N'TSQL'
         ,@command              = @Command
         ,@database_name        = N'master'
         ,@flags                = 0
        IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

        --  Set first step
        EXECUTE @ReturnCode = msdb.dbo.sp_update_job @job_id = @JobID, @start_step_id = 1
        IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

        --  Set job server
        EXECUTE @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @JobID, @server_name = N'(local)'
        IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

        PRINT 'Job created--disabled, and with no schedule'
     END

    ELSE
        --  Job already exists, make sure a step for this database is not already present
        IF exists (select 1
                    from dbo.sysJobSteps
                    where job_id = @JobID
                     and step_name = @StepName)
         BEGIN
            --  A step already exists in the job for this database
            PRINT 'Step "' + @StepName + '" found in job "' + @JobName + '" -- job has not been changed'
            GOTO QuitWithRollback
         END

    --  Insert the defrag call as the new first step
    SET @Command = N'--  Call the defrag procedure (which must be present in this database)
SET NOCOUNT on

--  Log current fragmentation state
INSERT master.dbo.IndexDefragLog
  (
    DBName        ,TableName  ,IndexName          ,IndexType
   ,Partition     ,Pages      ,FragPercent        ,Fill_Factor
   ,IsPrimaryKey  ,IsUnique   ,RecommendedAction
  )
 execute dbo.sp_phkIndexDefragUtility ''Report''

IF datepart(dd, getdate()) between 8 and 14
 BEGIN
    --  Called during second week of the month.  Perform the actual defrag routines
    --  and log results
    EXECUTE dbo.sp_phkIndexDefragUtility ''Defrag''

    INSERT master.dbo.IndexDefragLog
      (
        DBName        ,TableName  ,IndexName          ,IndexType
       ,Partition     ,Pages      ,FragPercent        ,Fill_Factor
       ,IsPrimaryKey  ,IsUnique   ,RecommendedAction
      )
     execute dbo.sp_phkIndexDefragUtility ''Report''
 END
'

    PRINT '(Disregard the "non-existant step" message)'
    EXECUTE @ReturnCode = msdb.dbo.sp_add_jobstep
      @job_id               = @JobID
     ,@step_name            = @StepName
     ,@step_id              = 1
     ,@cmdexec_success_code = 0
     ,@on_success_action    = 3
     ,@on_success_step_id   = 0
     ,@on_fail_action       = 4
     ,@on_fail_step_id      = @JobFailStep
     ,@retry_attempts       = 0
     ,@retry_interval       = 0
     ,@os_run_priority      = 0
     ,@subsystem            = N'TSQL'
     ,@command              = @Command
     ,@database_name        = @DBName
     ,@output_file_name     = @FullLogFile
     ,@flags                = 2  --  Append to output file
    IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

    COMMIT TRANSACTION  ---------------------------------------------------------------------

    PRINT  'Make sure the audit log folder exists!'
    GOTO EndSave

    QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION  ----------------------------------------------

    EndSave:

RETURN 0
GO
