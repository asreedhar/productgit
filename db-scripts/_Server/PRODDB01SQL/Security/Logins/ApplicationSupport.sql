----------------------------------------------------------------------------------------
--	CREATE LOGIN
----------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.server_principals WHERE name = N'FIRSTLOOK\ApplicationSupport')

	CREATE LOGIN [FIRSTLOOK\ApplicationSupport] FROM WINDOWS WITH DEFAULT_DATABASE=[IMT]


----------------------------------------------------------------------------------------
--	CREATE USER, ROLE, AND ASSIGN db_datareader, DataManagement (if available)
----------------------------------------------------------------------------------------


EXEC sp_map_exec 
'
USE [?]

CREATE USER [FIRSTLOOK\ApplicationSupport] FOR LOGIN [FIRSTLOOK\ApplicationSupport]


IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = "DATABASE_ROLE"
			AND name = "ApplicationSupportManager"
		)

	CREATE ROLE [ApplicationSupportManager] AUTHORIZATION [dbo]

EXEC sp_addrolemember N"db_datareader", N"FIRSTLOOK\ApplicationSupport"

IF EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = "DATABASE_ROLE"
			AND name = "DataManagement"
			)
	EXEC sp_addrolemember N"DataManagement", N"FIRSTLOOK\ApplicationSupport"
',
'SELECT	name
FROM	MASTER.sys.databases
WHERE	owner_sid <> 0x01
	and name <> "IMT_Archive"
',0


USE master

GRANT VIEW ANY DEFINITION TO [FIRSTLOOK\ApplicationSupport]

GO	

USE Market

GRANT EXEC ON Pricing.LoadOwnerDistanceBucket TO ApplicationSupportManager
GRANT EXEC ON Pricing.LoadSearchResult_F_ByOwnerID TO ApplicationSupportManager
GRANT EXEC ON Pricing.LoadSearchSales_F_ByOwnerID TO ApplicationSupportManager

GO