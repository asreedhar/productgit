
EXEC sp_map_exec

'USE [?]
IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = "WINDOWS_GROUP"
			AND name = "FIRSTLOOK\DatabaseEngineers"
		)
	CREATE USER [FIRSTLOOK\DatabaseEngineers] FOR LOGIN [FIRSTLOOK\DatabaseEngineers]

EXEC sp_addrolemember N"db_datareader", N"FIRSTLOOK\DatabaseEngineers"
',
'
SELECT	name
FROM	sys.databases
WHERE	name not in ("master","tempdb","model","msdb") and is_read_only = 0
'
GO

USE Reports
GO
GRANT EXEC TO [FIRSTLOOK\DatabaseEngineers]

GO


USE Market

EXEC sp_addrolemember N"ReportingUser", N"FIRSTLOOK\DatabaseEngineers"

GO