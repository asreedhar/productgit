--------------------------------------------------------------------------------
--	
--	CREATE A LOGIN FOR THE SALES FORCE EXTRACTOR SERVICE/APPLICATION
--	
--	NO NEED TO DO ROLE-BASED PERMS AS ONLY ONE LOGIN WILL NEED 
--	THESE PERMISSIONS AND SETTING UP A ROLE WOULD BE OVERKILL.
--
--------------------------------------------------------------------------------
USE master

CREATE LOGIN SalesForceExtractorService WITH PASSWORD=N'$3(@!#Dc9s!zx', DEFAULT_DATABASE=Reports, CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO

--------------------------------------------------------------------------------
USE IMT

CREATE USER SalesForceExtractorService FOR LOGIN SalesForceExtractorService
ALTER USER SalesForceExtractorService WITH DEFAULT_SCHEMA=dbo
GRANT SELECT ON dbo.DealerUpgrade TO SalesForceExtractorService
GRANT SELECT ON dbo.DealerPreference TO SalesForceExtractorService
GRANT SELECT ON dbo.BusinessUnitRelationship TO SalesForceExtractorService
GRANT SELECT ON dbo.BusinessUnit TO SalesForceExtractorService

GRANT SELECT ON dbo.InternetAdvertiser_ThirdPartyEntity TO SalesForceExtractorService

GRANT SELECT ON dbo.AIP_EventType TO SalesForceExtractorService
GRANT SELECT ON dbo.DMSExportBusinessUnitPreference TO SalesForceExtractorService

GRANT SELECT ON dbo.DealerPreference_Pricing TO SalesForceExtractorService
GRANT SELECT ON dbo.InternetAdvertiserDealership TO SalesForceExtractorService

GRANT SELECT ON dbo.AIP_Event TO SalesForceExtractorService
GRANT SELECT ON dbo.Inventory_ListPriceHistory TO SalesForceExtractorService
GRANT SELECT ON dbo.Member TO SalesForceExtractorService
GRANT SELECT ON dbo.AppraisalFormOptions TO SalesForceExtractorService

GRANT SELECT ON dbo.AppraisalValues TO SalesForceExtractorService
GRANT SELECT ON dbo.Appraisals TO SalesForceExtractorService
GRANT SELECT ON dbo.tbl_VehicleSale TO SalesForceExtractorService
GRANT SELECT ON dbo.Inventory TO SalesForceExtractorService
GO

--------------------------------------------------------------------------------
USE Market


CREATE USER SalesForceExtractorService FOR LOGIN SalesForceExtractorService
ALTER USER SalesForceExtractorService WITH DEFAULT_SCHEMA=Pricing

GRANT SELECT ON Merchandising.Advertisement_Properties  TO SalesForceExtractorService
GRANT SELECT ON Pricing.VehiclePricingDecisionInput TO SalesForceExtractorService

GRANT SELECT ON Pricing.Owner TO SalesForceExtractorService
GRANT SELECT ON Pricing.VehicleMarketHistory TO SalesForceExtractorService
GRANT EXEC ON Pricing.Load#InventoryStrategy TO SalesForceExtractorService

GRANT SELECT ON Merchandising.VehicleAdvertisement TO SalesForceExtractorService
GRANT SELECT ON Pricing.Search TO SalesForceExtractorService

GO

--------------------------------------------------------------------------------
USE Reports

CREATE USER SalesForceExtractorService FOR LOGIN SalesForceExtractorService
ALTER USER SalesForceExtractorService WITH DEFAULT_SCHEMA=dbo
GO

--------------------------------------------------------------------------------
USE EdgeScorecard

CREATE USER SalesForceExtractorService FOR LOGIN SalesForceExtractorService
ALTER USER SalesForceExtractorService WITH DEFAULT_SCHEMA=dbo
GRANT SELECT ON Measures TO SalesForceExtractorService
GO

--------------------------------------------------------------------------------
USE InteractionLog

CREATE USER SalesForceExtractorService FOR LOGIN SalesForceExtractorService
ALTER USER SalesForceExtractorService WITH DEFAULT_SCHEMA=dbo
GRANT SELECT ON track.Action TO SalesForceExtractorService
GRANT SELECT ON track.Resource TO SalesForceExtractorService

GO

--------------------------------------------------------------------------------
USE FLDW

CREATE USER SalesForceExtractorService FOR LOGIN SalesForceExtractorService
ALTER USER SalesForceExtractorService WITH DEFAULT_SCHEMA=dbo
GRANT SELECT ON track.Action TO SalesForceExtractorService
GRANT SELECT ON track.Resource TO SalesForceExtractorService
GRANT SELECT ON InventoryActive TO SalesForceExtractorService

GO
--------------------------------------------------------------------------------
USE HAL

CREATE USER SalesForceExtractorService FOR LOGIN SalesForceExtractorService
ALTER USER SalesForceExtractorService WITH DEFAULT_SCHEMA=dbo
GRANT SELECT ON dbo.AppraisalReviewBookletPreference TO SalesForceExtractorService
GO

--------------------------------------------------------------------------------
USE Reports

GRANT EXEC ON Reports.dbo.GetUsageMetricsForSalesForce TO SalesForceExtractorService
GRANT EXEC ON Reports.dbo.StoreUsageMetric#Detail TO SalesForceExtractorService

GO


