USE [Utility]

IF NOT EXISTS (	SELECT	1 
		FROM	sys.database_principals
		WHERE	type_desc = 'SQL_USER'
			AND NAME = 'FirstlookReports'
			)
	CREATE USER [FirstlookReports] FOR LOGIN [FirstlookReports]
GO

