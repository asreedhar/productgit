----------------------------------------------------------------------------------------
--	CREATE LOGIN
----------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.server_principals WHERE name = N'FIRSTLOOK\DMSi#DataloadAdmin')

	CREATE LOGIN [FIRSTLOOK\DMSi#DataloadAdmin] FROM WINDOWS WITH DEFAULT_DATABASE=[DBASTAT]


----------------------------------------------------------------------------------------
--	CREATE USER, ROLE, AND ASSIGN db_datareader, DataManagement (if available)
----------------------------------------------------------------------------------------

USE DBASTAT

CREATE USER [FIRSTLOOK\DMSi#DataloadAdmin] FOR LOGIN [FIRSTLOOK\DMSi#DataloadAdmin]


IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'DATABASE_ROLE'
			AND name = 'DataloadAdmin'
		)

	CREATE ROLE [DataloadAdmin] AUTHORIZATION [dbo]
	
GO


GRANT SELECT, INSERT, DELETE, UPDATE ON dbo.DealerDatasource TO DataloadAdmin
GRANT SELECT, INSERT, DELETE, UPDATE ON dbo.DealershipIdConversion TO DataloadAdmin
GRANT INSERT, UPDATE, DELETE, SELECT ON dbo.DealerVehicleStatusCode TO DataloadAdmin
GRANT INSERT, UPDATE, DELETE, SELECT ON dbo.DatasourceDealerProperties TO DataloadAdmin
GRANT SELECT, INSERT, UPDATE, DELETE ON Configuration.BusinessUnitDatafeed TO DataloadAdmin
GO


GRANT EXEC ON dbo.GetDealerIDFromStockLogic TO DataloadAdmin
GRANT EXEC ON dbo.GetDealerIDFromLotLocationLogic TO DataloadAdmin
GRANT EXEC ON dbo.GetDealerIDFromStatusCodeLogic TO DataloadAdmin
GRANT EXEC ON dbo.GetTradePurchaseFromStockLogic TO DataloadAdmin

GO

EXEC sp_addrolemember N'DataloadAdmin', N'FIRSTLOOK\DMSi#DataloadAdmin'
GO

USE IMT



IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'DATABASE_ROLE'
			AND name = 'DataloadAdmin'
		)

	CREATE ROLE [DataloadAdmin] AUTHORIZATION [dbo]
	
GO

GRANT UPDATE ON Extract.DealerConfiguration TO [DataloadAdmin]

EXEC sp_addrolemember N'DataloadAdmin', N'FIRSTLOOK\DMSi#DataloadAdmin'
GO

USE Archive

IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'DATABASE_ROLE'
			AND name = 'DataloadAdmin'
		)

	CREATE ROLE [DataloadAdmin] AUTHORIZATION [dbo]

IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'FIRSTLOOK\DMSi#DataloadAdmin'
		)
	CREATE USER [FIRSTLOOK\DMSi#DataloadAdmin] FOR LOGIN [FIRSTLOOK\DMSi#DataloadAdmin]

	
EXEC sp_addrolemember N'DataloadAdmin', N'FIRSTLOOK\DMSi#DataloadAdmin'

GRANT SELECT ON Dataload_Raw_History_Archive TO DataloadAdmin
GO
