----------------------------------------------------------------------------------------
--	CREATE LOGIN
----------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.server_principals WHERE name = N'FIRSTLOOK\ProductionSQLAgentJobMonitor')

	CREATE LOGIN [FIRSTLOOK\ProductionSQLAgentJobMonitor] FROM WINDOWS WITH DEFAULT_DATABASE=[msdb]


----------------------------------------------------------------------------------------
--	CREATE USER, ROLE, AND ASSIGN db_datareader, DataManagement (if available)
----------------------------------------------------------------------------------------


CREATE USER [FIRSTLOOK\ProductionSQLAgentJobMonitor] FOR LOGIN [FIRSTLOOK\ProductionSQLAgentJobMonitor]

EXEC sp_addrolemember N'SQLAgentReaderRole', N'FIRSTLOOK\ProductionSQLAgentJobMonitor'
GO
	