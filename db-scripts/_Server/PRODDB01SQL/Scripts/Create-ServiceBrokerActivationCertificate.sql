
-- ********************************************************************
-- ESTABLISH DATABASE TRUST RELATIONSHIPS
-- @see http://www.sommarskog.se/grantperm.html#certcrossdb
-- @see http://msdn2.microsoft.com/en-us/library/ms188304.aspx
-- ********************************************************************
--
-- Without paths, certs are backed-up to the default DATA folder
--
-- ********************************************************************
USE IMT
GO

CREATE CERTIFICATE ServiceBrokerActivationCertificate
   ENCRYPTION BY PASSWORD = '25RHwk3F'
   WITH SUBJECT = 'Service Broker Trust',
   START_DATE = '20080124', EXPIRY_DATE = '20200101'
GO

BACKUP CERTIFICATE ServiceBrokerActivationCertificate TO FILE = 'ServiceBrokerActivationCertificate.cer'
WITH PRIVATE KEY (
	FILE = 'ServiceBrokerActivationCertificate.pvk',
	ENCRYPTION BY PASSWORD = 'lHh56gPu', -- was 'Tomorrow never knows'
	DECRYPTION BY PASSWORD = '25RHwk3F'  -- was 'All you need is love'
)
GO

CREATE USER ServiceBrokerActivationUser FROM CERTIFICATE ServiceBrokerActivationCertificate
GO

GRANT AUTHENTICATE TO ServiceBrokerActivationUser
GO

EXEC sp_addrolemember @rolename='db_datareader', @membername = 'ServiceBrokerActivationUser'
GO

USE FLDW
GO

CREATE CERTIFICATE ServiceBrokerActivationCertificate FROM FILE = 'ServiceBrokerActivationCertificate.cer'
WITH PRIVATE KEY (
	FILE = 'ServiceBrokerActivationCertificate.pvk',
	DECRYPTION BY PASSWORD = 'lHh56gPu', -- was 'Tomorrow never knows'
	ENCRYPTION BY PASSWORD = 'n362ED4J') -- was 'A day in life'
GO

CREATE USER ServiceBrokerActivationUser FROM CERTIFICATE ServiceBrokerActivationCertificate
GO

GRANT AUTHENTICATE TO ServiceBrokerActivationUser
GO

EXEC sp_addrolemember @rolename='db_datareader', @membername = 'ServiceBrokerActivationUser'
GO

USE VehicleCatalog
GO

CREATE CERTIFICATE ServiceBrokerActivationCertificate FROM FILE = 'ServiceBrokerActivationCertificate.cer'
WITH PRIVATE KEY (
	FILE = 'ServiceBrokerActivationCertificate.pvk',
	DECRYPTION BY PASSWORD = 'lHh56gPu', -- was 'Tomorrow never knows'
	ENCRYPTION BY PASSWORD = 'n362ED4J') -- was 'A day in life'
GO

CREATE USER ServiceBrokerActivationUser FROM CERTIFICATE ServiceBrokerActivationCertificate
GO

GRANT AUTHENTICATE TO ServiceBrokerActivationUser
GO

EXEC sp_addrolemember @rolename='db_datareader', @membername = 'ServiceBrokerActivationUser'
GO

USE Market
GO

CREATE CERTIFICATE ServiceBrokerActivationCertificate FROM FILE = 'ServiceBrokerActivationCertificate.cer'
WITH PRIVATE KEY (
	FILE = 'ServiceBrokerActivationCertificate.pvk',
	DECRYPTION BY PASSWORD = 'lHh56gPu', -- was 'Tomorrow never knows'
	ENCRYPTION BY PASSWORD = 'n362ED4J') -- was 'A day in life'
GO

-- kill all other connections to market so we can apply the schema change

DECLARE @SQL varchar(max)

SET @SQL = ''

SELECT @SQL = @SQL + 'Kill ' + Convert(varchar, SPID) + ';'
FROM MASTER..SysProcesses
WHERE DBId = DB_ID('Market') AND SPID <> @@SPID

EXEC(@SQL)
GO

IF (SELECT is_broker_enabled FROM sys.databases WHERE database_id = DB_ID()) = 0
ALTER DATABASE Market SET ENABLE_BROKER
GO
