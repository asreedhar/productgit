USE [msdb]

BEGIN TRANSACTION
DECLARE @ReturnCode INT,
	@JobID UNIQUEIDENTIFIER
	
SELECT @ReturnCode = 0

SELECT	@JobID = job_id
FROM	msdb.dbo.sysjobs S
WHERE	name = 'Dataload: VehicleCatalog'

--   Delete Step [Timestamp and cycle audit logs (on failure)]    
EXEC @ReturnCode = msdb.dbo.sp_delete_jobstep @job_id=@JobID, @step_id=12

--   Delete Step [Timestamp and cycle audit logs (on success)]    
EXEC @ReturnCode = msdb.dbo.sp_delete_jobstep @job_id=@JobID, @step_id=11

EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Load Listing Vehicle_History_Categorization', 
		@step_id=11, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=14, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\\PRODDB01SQL\Packages\Dataloads\Market\Load_Listing_Vehicle_Interface_Categorization.dtsx" /CONFIGFILE "\\PRODDB01SQL\Packages\Dataloads\Market\Load_Listing_Vehicle_History_Categorization.dtsConfig" /MAXCONCURRENT " -1 " /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@output_file_name=N'\\PRODDB01SQL\AuditLogs\DataLoadJobs\VehicleCatalog.log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Update VehicleSales Model Configurations]    Script Date: 03/23/2010 17:35:38 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Update VehicleSales Model Configurations', 
		@step_id=12, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=14, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC Listing.Vehicle_History_Categorization#UpdateVehicleSalesModelConfigurations', 
		@database_name=N'Market', 
		@output_file_name=N'\\PRODDB01SQL\AuditLogs\DataLoadJobs\VehicleCatalog.log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@JobID, @step_name=N'Timestamp and cycle audit logs (on success)', 
		@step_id=13, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_fail_action=2, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--  Timestamp audit log files, delete sufficiently aged ones
EXECUTE sp_FileTimestampAndCycle ''\\PRODDB01SQL\AuditLogs\DataLoadJobs'', ''Chrome.log'', 35
', 
		@database_name=N'master', 
		@flags=0

IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback


EXEC msdb.dbo.sp_add_jobstep @job_id=@JobID, @step_name=N'Timestamp and cycle audit logs (on failure)', 
		@step_id=14, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_fail_action=2, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--  Timestamp audit log files, delete sufficiently aged ones
EXECUTE sp_FileTimestampAndCycle ''\\PRODDB01SQL\AuditLogs\DataLoadJobs'', ''Chrome.log'', 35
', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback


EXEC @ReturnCode = msdb.dbo.sp_update_jobstep @job_id=@JobID, @step_id=1 , 
		@on_fail_action=4, 
		@on_fail_step_id=14
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_update_jobstep @job_id=@JobID, @step_id=2 , 
		@on_fail_action=4, 
		@on_fail_step_id=14
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_update_jobstep @job_id=@JobID, @step_id=3 , 
		@on_fail_action=4, 
		@on_fail_step_id=14
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_update_jobstep @job_id=@JobID, @step_id=4 , 
		@on_fail_action=4, 
		@on_fail_step_id=14
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_update_jobstep @job_id=@JobID, @step_id=5 , 
		@on_fail_action=4, 
		@on_fail_step_id=14
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_update_jobstep @job_id=@JobID, @step_id=6 , 
		@on_fail_action=4, 
		@on_fail_step_id=14
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_update_jobstep @job_id=@JobID, @step_id=7 , 
		@on_fail_action=4, 
		@on_fail_step_id=14
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_update_jobstep @job_id=@JobID, @step_id=8 , 
		@on_fail_action=4, 
		@on_fail_step_id=14
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_update_jobstep @job_id=@JobID, @step_id=9 , 
		@on_fail_action=4, 
		@on_fail_step_id=14
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_update_jobstep @job_id=@JobID, @step_id=10 , 
		@on_fail_action=4, 
		@on_fail_step_id=14
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO
