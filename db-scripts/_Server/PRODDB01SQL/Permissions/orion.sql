USE [master]
GO
CREATE LOGIN [FIRSTLOOK\orion] FROM WINDOWS WITH DEFAULT_DATABASE=[master]
GO
USE [IMT]
GO
CREATE USER [FIRSTLOOK\orion] FOR LOGIN [FIRSTLOOK\orion]
GO
USE [IMT]
GO
EXEC sp_addrolemember N'db_datareader', N'FIRSTLOOK\orion'
GO
USE [Market]
GO
CREATE USER [FIRSTLOOK\orion] FOR LOGIN [FIRSTLOOK\orion]
GO
USE [Market]
GO
EXEC sp_addrolemember N'db_datareader', N'FIRSTLOOK\orion'
GO
USE [msdb]
GO
CREATE USER [FIRSTLOOK\orion] FOR LOGIN [FIRSTLOOK\orion]
GO
USE [msdb]
GO
EXEC sp_addrolemember N'db_datareader', N'FIRSTLOOK\orion'
GO
USE [Reports]
GO
CREATE USER [FIRSTLOOK\orion] FOR LOGIN [FIRSTLOOK\orion]
GO
USE [Reports]
GO
EXEC sp_addrolemember N'db_datareader', N'FIRSTLOOK\orion'
GO
USE [ReportHost]
GO
CREATE USER [orion] FOR LOGIN [orion]
GO
USE [ReportHost]
GO
EXEC sp_addrolemember N'db_datareader', N'orion'
GO
GRANT SELECT ON rc_BusinessUnitParentReturn TO orion