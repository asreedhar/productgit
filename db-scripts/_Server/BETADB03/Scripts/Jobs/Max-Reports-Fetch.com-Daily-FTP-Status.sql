USE [msdb]
GO
/****** Object:  Job [Max Reports : Fetch.com Daily FTP Status]    Script Date: 05/06/2011 16:22:33 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 05/06/2011 16:22:33 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Max Reports : Fetch.com Daily FTP Status', 
		@enabled=0, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [run report script]    Script Date: 05/06/2011 16:22:34 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'run report script', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'declare	@recipients varchar(max), @copy_recipients varchar(max), @blind_copy_recipients varchar(max)

select
	@recipients = ''dhillis@incisent.com;zbrown@incisent.com;dsundaram@incisent.com;mclay@incisent.com;breeve@incisent.com''
	, @copy_recipients  = ''''
	, @blind_copy_recipients = ''''






set nocount on
set transaction isolation level read uncommitted



declare
	@subject nvarchar(255)	
	, @importance varchar(6)
	, @body nvarchar(max)

	

-- Request Status
if exists(
	SELECT *
	FROM Datafeeds.Extract.FetchDotCom#Request r
	JOIN Datafeeds.Extract.FetchDotCom#RequestDealer rd ON r.RequestId = rd.RequestId
	WHERE 
		r.DateCreated > DATEADD(dd, -5, GETDATE())
		and DateLoaded IS NULL 
		and DATEDIFF(hh, DateCreated, GETDATE()) >= 27
)
begin
	select
		@subject = ''Fetch.com Daily FTP Status: LATE REQUESTS FOUND''
		, @importance = ''High''
end
else
begin
	select
		@subject = ''Fetch.com Daily FTP Status: OK''
		, @importance = ''Normal''
end
	
    
set @body =
		N''<H1>Fetch FTP Request Status</H1>'' +
		N''<table border="1">'' +
		N''<tr>'' +
		N''<th>RequestId</th><th>RequestTypeId</th><th>Handle</th><th>Description</th><th>DateCreated</th><th>DateLoaded</th><th>DistinctDealerCount</th><th>DealerPeriodsCount</th><th>LoadingStatus</th>'' +
		''</tr>'' +
		cast
		(
			(
				SELECT 
					td = r.RequestId, '''',
					td = r.RequestTypeId, '''',
					td = r.Handle, '''',
					td = rt.Description, '''',
					td = r.DateCreated, '''',
					td = isnull(convert(varchar(23), r.DateLoaded, 121), ''''), '''',
					td = COUNT(DISTINCT rd.BusinessUnitId), '''',
					td = COUNT(ALL rd.BusinessUnitId), '''',
					td = CASE 
						WHEN DateLoaded IS NULL THEN 
						  CASE 
							  WHEN DATEDIFF(hh, DateCreated, GETDATE()) >= 27 THEN ''Late''
							  ELSE ''Pending''
						  END
						ELSE
						  CASE WHEN DATEDIFF(hh, DateCreated, DateLoaded) < 27 THEN ''Loaded On Time''
						  ELSE ''Loaded Late''
						  END
					end, ''''
				FROM Datafeeds.Extract.FetchDotCom#Request r
				JOIN Datafeeds.Extract.FetchDotCom#RequestDealer rd ON r.RequestId = rd.RequestId
				JOIN Datafeeds.Extract.FetchDotCom#RequestType rt ON rt.RequestTypeId = r.RequestTypeId
				WHERE r.DateCreated > DATEADD(dd, -5, GETDATE())
				GROUP BY r.RequestId, r.Handle, r.RequestTypeId, rt.Description, r.DateCreated, r.DateLoaded
				ORDER BY r.RequestId
				for xml path(''tr''), type 
			) as nvarchar(max) 
		) +
		N''</table>''
			

exec msdb.dbo.sp_send_dbmail
	@profile_name = ''MaxAd Reports''
	, @recipients = @recipients
	, @copy_recipients = @copy_recipients
	, @blind_copy_recipients = @blind_copy_recipients
	, @subject = @subject
    , @body = @body
    , @body_format = ''HTML''
    , @importance = @importance



go

', 
		@database_name=N'Datafeeds', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'daily 8 am', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20110506, 
		@active_end_date=99991231, 
		@active_start_time=80000, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
