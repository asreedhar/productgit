IF NOT EXISTS (	SELECT	*
		FROM	sys.server_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'Firstlook\Analytics'
		)

	CREATE LOGIN [FIRSTLOOK\Analytics] FROM WINDOWS WITH DEFAULT_DATABASE=[Merchandising]

GO


EXEC sp_map_exec

'USE [?]

EXEC sp_change_users_login @Action = "Auto_Fix", @UserNamePattern = ''Firstlook\Analytics''

IF NOT EXISTS (	SELECT	*
		FROM	sys.database_principals
		WHERE	type_desc = "WINDOWS_GROUP"
			AND name = "Firstlook\Analytics"
		)
		
	CREATE USER [FIRSTLOOK\Analytics] FOR LOGIN [FIRSTLOOK\Analytics] 
	
EXEC sp_addrolemember N"db_datareader", N"FIRSTLOOK\Analytics"
',
'
SELECT	name
FROM	sys.databases
WHERE	name not in ("master","tempdb","model","msdb")
'

GO
USE msdb
EXEC sp_addrolemember N'SQLAgentReaderRole', N'FIRSTLOOK\Analytics'
GO


USE master

GRANT VIEW ANY DEFINITION TO [FIRSTLOOK\Analytics]

GO	

