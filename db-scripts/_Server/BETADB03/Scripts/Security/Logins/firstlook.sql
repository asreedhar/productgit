--
-- Script Login, If Necessary
--

IF NOT EXISTS
(
SELECT	1
FROM	sys.server_principals
WHERE	type_desc = 'SQL_LOGIN'
AND	NAME='firstlook'
)

CREATE LOGIN [firstlook] WITH PASSWORD='5W6bMe_$-Jo2', DEFAULT_DATABASE=[Lot], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
