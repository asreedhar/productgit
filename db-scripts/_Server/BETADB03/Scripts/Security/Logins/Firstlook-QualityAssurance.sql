IF NOT EXISTS (	SELECT	1
		FROM	sys.server_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'Firstlook\QualityAssurance'
		)

	CREATE LOGIN [FIRSTLOOK\QualityAssurance] FROM WINDOWS WITH DEFAULT_DATABASE=[Merchandising]

GO


EXEC sp_map_exec

'USE [?]

EXEC sp_change_users_login @Action = "Auto_Fix", @UserNamePattern = ''Firstlook\QualityAssurance''


IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = "WINDOWS_GROUP"
			AND name = "Firstlook\QualityAssurance"
		)
	CREATE USER [FIRSTLOOK\QualityAssurance] FOR LOGIN [FIRSTLOOK\QualityAssurance] 

EXEC sp_addrolemember N"db_datareader", N"FIRSTLOOK\QualityAssurance"

GRANT EXEC TO [FIRSTLOOK\QualityAssurance] 
',
'
SELECT	name
FROM	sys.databases
WHERE	name not in ("master","tempdb","model","msdb") and is_read_only = 0
'

GO
USE msdb
EXEC sp_addrolemember N'SQLAgentReaderRole', N'FIRSTLOOK\QualityAssurance'
GO


USE master

GRANT VIEW ANY DEFINITION TO [FIRSTLOOK\QualityAssurance]

GO	
