IF NOT EXISTS (	SELECT	1
		FROM	sys.server_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'Firstlook\DatabaseEngineers'
		)

	CREATE LOGIN [FIRSTLOOK\DatabaseEngineers] FROM WINDOWS WITH DEFAULT_DATABASE=[master]

GO
USE master
grant exec to [Firstlook\DatabaseEngineers]
GO

EXEC sp_map_exec

'USE [?]

IF NOT EXISTS (	SELECT	*
		FROM	sys.database_principals
		WHERE	type_desc = "WINDOWS_GROUP"
			AND name = "Firstlook\DatabaseEngineers"
		)
		
	CREATE USER [FIRSTLOOK\DatabaseEngineers] FOR LOGIN [FIRSTLOOK\DatabaseEngineers]
	
EXEC sp_addrolemember N"db_datareader", N"FIRSTLOOK\DatabaseEngineers"
EXEC sp_change_users_login @Action = "Auto_Fix", @UserNamePattern = ''Firstlook\DatabaseEngineers''
',
'
SELECT	name
FROM	sys.databases
WHERE	name not in ("master","tempdb","model","msdb") and is_read_only = 0
'

GO
USE msdb

EXEC sp_addrolemember N'SQLAgentOperatorRole', N'FIRSTLOOK\DatabaseEngineers'
GO

USE [msdb]
EXEC sp_addrolemember N'SQLAgentReaderRole', N'FIRSTLOOK\DatabaseEngineers'
GO

--allow engineers to use SQL Profiler
USE [master]
go
GRANT ALTER TRACE TO [Firstlook\DatabaseEngineers]
GO

USE Utility

GRANT EXEC TO [Firstlook\DatabaseEngineers]

GO

USE Lot

EXEC sp_addrolemember N'db_datareader', N'FirstLook\DatabaseEngineers'
EXEC sp_addrolemember N'db_datawriter', N'FirstLook\DatabaseEngineers'
EXEC sp_addrolemember N'db_ddladmin', N'FirstLook\DatabaseEngineers'

GO
