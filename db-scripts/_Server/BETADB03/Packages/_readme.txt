-- CONFIGURATION FILES GO HERE --

The directory structure under Packages should be exactly the same as the
folder structure under \\BETADB03\Packages.  But since we don't need a 
copy of every package in this folder, as they will be common across servers,
all we need are the configuration files.