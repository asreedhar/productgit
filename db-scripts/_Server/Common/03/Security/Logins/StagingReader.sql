--
-- Create StagingReader Login on 03 servers
--
-- (Note: This script should work in all 3 environments)
--

DECLARE @PWD VARCHAR(50)
DECLARE @SQL VARCHAR(2000)

SET @PWD='readme' -- Default

IF @@SERVERNAME='BETADB03'
	SET @PWD=@PWD+'B3ta'

IF @@SERVERNAME='PRODDB03SQL'
	SET @PWD=@PWD+'Pr0d'

SET @SQL='CREATE LOGIN [StagingReader] WITH PASSWORD=''' + @PWD + ''', DEFAULT_DATABASE=[Staging], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF'

IF  NOT EXISTS (SELECT * FROM sys.server_principals WHERE name = N'StagingReader' AND type_desc='SQL_LOGIN')	
	EXEC (@SQL)

GO