USE [msdb]

DECLARE @JobName	VARCHAR(100), 
	@Description	VARCHAR(200),
	@ConfigFileName	VARCHAR(50),
	@LogFileName	VARCHAR(50),
	@Category		VARCHAR(50),
	@Step1Name		VARCHAR(100),
	@Owner			VARCHAR(50),
	@OutputFileName	VARCHAR(100),
	@LogCommand		VARCHAR(250),
	@Command		VARCHAR(250),
	@OperatorName	VARCHAR(100),
	@LogFilePath	VARCHAR(100),
	@StartTime		INT,
	@ReturnCode INT

SET @JobName = 'Pusher: Mu Sigma - Daily Listings'
SET @Description ='This job utilizes the Pusher.dtsx package to create, archive and place on the FTP server, files of the'
SET @Description =@Description + ' Market Listings and the related Model Configuration IDs and their attributes for Mu Sigma.'
SET @ConfigFileName ='Pusher-MuSigma-Listings.dtsConfig'	
SET @LogFileName = 'MuSigma_Listings.log'
SET @Category ='Outbound Datafeed' 
SET @Step1Name ='Pusher: Mu Sigma - Daily Listings'
SET @Owner ='sa'
SET @StartTime =70000 -- 7 am

SET @LogFilePath = '\\' + @@SERVERNAME + '\AuditLogs\DataLoadJobs'
SET @OutputFileName = @LogFilePath + '\' + @LogFileName
SET @LogCommand = 'EXECUTE sp_FileTimestampAndCycle ''' + @LogFilePath +''', ''' + @LogFileName + '.log'', 7'
SET @OperatorName = NULL	-- NEEDS TO CHANGE PER ENV.
SET @Command = '/FILE "\\'+ @@SERVERNAME+ '\Packages\Pusher\Pusher.dtsx" /CONFIGFILE "\\' 
				+ @@SERVERNAME +'\Packages\Pusher\'+ @ConfigFileName +'" /MAXCONCURRENT " -1 " /CHECKPOINTING OFF /REPORTING E'



BEGIN TRANSACTION

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=@Category AND category_class=1)
BEGIN
	EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=@Category
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
END

IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = @JobName)
	EXEC msdb.dbo.sp_delete_job
	        @job_name = @JobName, -- sysname
	        @delete_history = 0, -- bit
	        @delete_unused_schedule = 1-- bit

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@JobName,
		@enabled=0, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=@Description, 
		@category_name=@Category, 
		@owner_login_name=@Owner, @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=@Step1Name, 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=@Command, 
		@database_name=N'master', 
		@output_file_name= @OutputFileName, 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1

IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Post Market Listing Run', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20100609, 
		@active_end_date=99991231, 
		@active_start_time=70000, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO