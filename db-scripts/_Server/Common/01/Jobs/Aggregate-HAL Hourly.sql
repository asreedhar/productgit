

USE [msdb]

DECLARE @JobName	VARCHAR(100), 
	@LogFilePath	VARCHAR(100),
	@LogFileName	VARCHAR(50),
	@OutputFileName	VARCHAR(100),
	@LogCommand	VARCHAR(250)
	
SET @JobName = 'Aggregate: HAL Daily'
SET @LogFilePath = '\\' + @@SERVERNAME + '\AuditLogs\SQLAgentJobs'
SET @LogFileName = 'HAL_Aggregation_Daily.log'
SET @OutputFileName = @LogFilePath + '\' + @LogFileName
SET @LogCommand = 'EXECUTE sp_FileTimestampAndCycle ''' + @LogFilePath +''', ''' + @LogFileName + '.log'', 7'


IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = @JobName)
	EXEC msdb.dbo.sp_delete_job
	        @job_name = @JobName, -- sysname
	        @delete_history = 1, -- bit
	        @delete_unused_schedule = 1-- bit


/****** Object:  Job [Aggregate: HAL Daily]    Script Date: 01/18/2010 17:03:26 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Aggregate]    Script Date: 01/18/2010 17:03:26 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Aggregate' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Aggregate'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@JobName, 
		@enabled=1, 
		@notify_level_eventlog=2, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'Aggregate', 
		@owner_login_name=N'sa', 
		@job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Daily Aggregation]    Script Date: 01/18/2010 17:03:26 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Daily Aggregation', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=3, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC BuildAggregates @Mode = 2
', 
		@database_name=N'HAL', 
		@output_file_name=@OutputFileName, 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Timestamp and cycle audit logs (on success)]    Script Date: 01/18/2010 17:03:27 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Timestamp and cycle audit logs (on success)', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=@LogCommand,
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Timestamp and cycle audit logs (on failure)]    Script Date: 01/18/2010 17:03:27 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Timestamp and cycle audit logs (on failure)', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, 
		@subsystem=N'TSQL', 
		@command=@LogCommand,
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Daily at 08:00:00', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20070125, 
		@active_end_date=99991231, 
		@active_start_time=80000, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO

USE [msdb]

DECLARE @JobName	VARCHAR(100), 
	@LogFilePath	VARCHAR(100),
	@LogFileName	VARCHAR(50),
	@OutputFileName	VARCHAR(100),
	@LogCommand	VARCHAR(250)
	
SET @JobName = 'Aggregate: HAL Hourly'
SET @LogFilePath = '\\' + @@SERVERNAME + '\AuditLogs\SQLAgentJobs'
SET @LogFileName = 'HAL_Aggregation_Daily.log'
SET @OutputFileName = @LogFilePath + '\' + @LogFileName
SET @LogCommand = 'EXECUTE sp_FileTimestampAndCycle ''' + @LogFilePath +''', ''' + @LogFileName + '.log'', 7'


IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = @JobName)
	EXEC msdb.dbo.sp_delete_job
	        @job_name = @JobName, -- sysname
	        @delete_history = 1, -- bit
	        @delete_unused_schedule = 1-- bit


/****** Object:  Job [Aggregate: HAL Hourly]    Script Date: 01/18/2010 17:03:47 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Aggregate]    Script Date: 01/18/2010 17:03:47 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Aggregate' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Aggregate'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Aggregate: HAL Hourly', 
		@enabled=1, 
		@notify_level_eventlog=2, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'Aggregate', 
		@owner_login_name=N'sa', 
		@job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Hourly Aggregation]    Script Date: 01/18/2010 17:03:48 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Hourly Aggregation', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=3, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC BuildAggregates @Mode = 1', 
		@database_name=N'HAL', 
		@output_file_name=OutputFileName, 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Timestamp and cycle audit logs (on success)]    Script Date: 01/18/2010 17:03:48 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Timestamp and cycle audit logs (on success)', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, 
		@subsystem=N'TSQL', 
		@command=@LogCommand,
		@database_name=N'HAL', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Timestamp and cycle audit logs (on failure)]    Script Date: 01/18/2010 17:03:48 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Timestamp and cycle audit logs (on failure)', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, 
		@subsystem=N'TSQL', 
		@command=@LogCommand,
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Daily once', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=8, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20080320, 
		@active_end_date=99991231, 
		@active_start_time=102000, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
GO