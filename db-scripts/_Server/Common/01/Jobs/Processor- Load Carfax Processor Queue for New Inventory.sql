USE [msdb]

DECLARE @JobName	VARCHAR(100), 
	@OperatorName	VARCHAR(100),
	@OwnerName		VARCHAR(100),
	@Category		VARCHAR(100)
	
SET @JobName = 'Processor: Load Carfax Processor Queue for New Inventory'
SET @OperatorName = NULL	-- NEEDS TO CHANGE PER ENV.
SET @OwnerName ='sa'
SET @Category	= 'Processor'
 
BEGIN TRANSACTION

DECLARE @ReturnCode INT
SELECT @ReturnCode = 0

IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = @JobName)
	EXEC msdb.dbo.sp_delete_job
	        @job_name = @JobName, -- sysname
	        @delete_history = 0, -- bit
	        @delete_unused_schedule = 1-- bit
	        
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=@Category AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=@Category
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END
 
DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@JobName,
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Create Carfax Report for New Inventory, All Dealers', 
		@category_name=@Category, 
		@owner_login_name=@OwnerName, 
		@notify_email_operator_name=@OperatorName, @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Populate the queue', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXECUTE Carfax.ReportProcessorCommand#CreateNewAutoPurchaseInventory_All', 
		@database_name=N'IMT', 
		@flags=0
		
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO