/*** MAK	04/22/2010	Script for Market Listing. ****/
/*** MAK	04/27/2010	Correct procedure name. ****/

USE [msdb]
GO


DECLARE @JobName	VARCHAR(100), 
	@LogFilePath	VARCHAR(100),
	@LogFileName	VARCHAR(50),
	@OutputFileName	VARCHAR(100),
	@LogCommand	VARCHAR(250),
	@OperatorName	VARCHAR(100),
	@OwnerName VARCHAR(50),
	@DTSXFilePath VARCHAR(100),
	@ConfigFilePath VARCHAR(100),
	@MarketListingProcessCommand VARCHAR(500),
	@LoadListingPricingFactTablesCommand VARCHAR(500)
	
SET @JobName = 'DataLoad: Market Listing and Pricing Schemas'
SET @LogFilePath = '\\' + @@SERVERNAME + '\AuditLogs\DataLoadJobs'
SET @LogFileName = 'Market.log'
SET @OutputFileName = @LogFilePath + '\' + @LogFileName
SET @LogCommand = 'EXECUTE sp_FileTimestampAndCycle ''' + @LogFilePath +''', ''' + @LogFileName + '.log'', 7'
SET @OwnerName ='sa'
SET @OperatorName = NULL	-- NEEDS TO CHANGE PER ENV.  his is not used in this job, but acts as a place holder.
SET @DTSXFilePath='\\' + @@SERVERNAME + '\Packages\Dataloads\Market\'
SET @ConfigFilePath='\\' + @@SERVERNAME + '\Packages\Dataloads\Market\'


/* @Command for Step_ID = 1. */
SET @MarketListingProcessCommand = '/FILE "'+ @DTSXFilePath + 'MarketListing-LoadProcess.dtsx"' +
									' /CONFIGFILE "'+ @ConfigFilePath + 'MarketListing-LoadProcess.dtsConfig" '
									+ '/MAXCONCURRENT " -1 " /CHECKPOINTING OFF /REPORTING E'  
	
/* @Command for Step_ID = 3. */
SET @LoadListingPricingFactTablesCommand = '/FILE "'+ @DTSXFilePath + 'Load_ListingPricingFactTables.dtsx"' +
									' /CONFIGFILE "'+ @ConfigFilePath + 'Load_ListingPricingFactTables.dtsConfig" '
									+ '/MAXCONCURRENT " -1 " /CHECKPOINTING OFF /REPORTING E'  
									

BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0

IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = @JobName)
	EXEC msdb.dbo.sp_delete_job
	        @job_name = @JobName, -- sysname
	        @delete_history = 0, -- bit
	        @delete_unused_schedule = 1-- bit
 


/****** Object:  JobCategory [DataLoad]    Script Date: 04/22/2010 13:55:58 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'DataLoad' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'DataLoad'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@JobName, 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'DataLoad', 
		@owner_login_name=@OwnerName, @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Market Listing Load Process]    Script Date: 04/23/2010 13:44:03 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Market Listing Load Process', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=7, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=@MarketListingProcessCommand,
		@database_name=N'master', 
		@output_file_name=@OutputFileName, 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Correct Search ModelConfiguration IDs]    Script Date: 04/23/2010 13:44:03 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Correct Search ModelConfiguration IDs', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=7, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC Pricing.Search#CorrectModelConfiguration', 
		@database_name=N'Market', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Update Market Statistics and Recompile Pricing.LoadSearchResult_F_ByOwnerID]    Script Date: 04/23/2010 13:44:03 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Update Market Statistics and Recompile Pricing.LoadSearchResult_F_ByOwnerID', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=7, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC sp_updatestats
GO
EXEC sp_recompile ''Pricing.LoadSearchResult_F_ByOwnerID''
GO', 
		@database_name=N'Market', 
		@output_file_name=@OutputFileName,  
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Load Listing Fact and Pricing Aggregate Tables]    Script Date: 04/23/2010 13:44:03 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Load Listing Fact and Pricing Aggregate Tables', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=7, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=@LoadListingPricingFactTablesCommand,
		@database_name=N'master', 
		@output_file_name=@OutputFileName, 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Delete Expired MSG reports]    Script Date: 04/23/2010 13:44:03 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Delete Expired MSG reports', 
		@step_id=5, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=7, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC Reporting.Report#DeleteExpiredReports', 
		@database_name=N'Market', 
		@output_file_name=@OutputFileName,  
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Timestamp and cycle audit logs (on success)]    Script Date: 04/23/2010 13:44:03 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Timestamp and cycle audit logs (on success)', 
		@step_id=6, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command= @LogCommand, 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Timestamp and cycle audit logs (on failure)]    Script Date: 04/23/2010 13:44:04 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Timestamp and cycle audit logs (on failure)', 
		@step_id=7, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=@LogCommand, 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Daily at 02:45:00', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20071219, 
		@active_end_date=99991231, 
		@active_start_time=24500, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO