USE [msdb]
GO
BEGIN TRANSACTION

DECLARE @ReturnCode INT
SELECT @ReturnCode = 0

DECLARE @OperatorsGroup VARCHAR(50)
DECLARE @JobName VARCHAR(60)
DECLARE @Category VARCHAR(50)

SET @JobName ='Processor: Load Carfax Processor Queue for Expired Reports'
SET @Category ='Processor'

IF (@@SERVERNAME LIKE 'PRODDB%') SET @OperatorsGroup ='ProductionOperationsGroup'

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Processor' AND category_class=1)
BEGIN
	EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Processor'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

IF NOT EXISTS (SELECT SJ.name  FROM msdb.dbo.sysjobs SJ
JOIN msdb.dbo.syscategories SC ON SJ.category_id = SC.category_id WHERE SJ.name=@JobName AND SC.Name =@Category)
BEGIN
	DECLARE @jobId BINARY(16)
	EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@JobName ,
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Load Carfax Processor Queue for Expired Reports', 
		@category_name=@Category, 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=@OperatorsGroup, @job_id = @jobId OUTPUT

	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	/****** Object:  Step [Populate the queue]    Script Date: 07/16/2010 17:13:31 ******/
	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Populate the queue', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXECUTE Carfax.ReportProcessorCommand#RepurchaseExpiredInventoryReports', 
		@database_name=N'IMT', 
		@flags=0
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1

	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Nightly, 12:20am', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20090720, 
		@active_end_date=99991231, 
		@active_start_time=2000, 
		@active_end_time=235959
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'

	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	COMMIT TRANSACTION

GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
END