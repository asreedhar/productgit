USE [msdb]

DECLARE @JobName	VARCHAR(100), 
	@LogFilePath	VARCHAR(100),
	@LogFileName	VARCHAR(50),
	@OutputFileName	VARCHAR(100),
	@LogCommand	VARCHAR(250),
	@OperatorName	VARCHAR(100),
	@OwnerName		VARCHAR(100),
	@DatafeedID		INT,
	@DatafeedCode	VARCHAR(50),
	@Category		VARCHAR(50),
	@AlertWMIQuery	VARCHAR(500),
	@ResolveCommand VARCHAR(500),
	@JobDescription VARCHAR(100),
	@Step2Name		VARCHAR(50),
	@Step2Command	VARCHAR(50),
	@Step6Name		VARCHAR(50),
	@Step6Command	VARCHAR(500),
	@AlertName VARCHAR(50)

SET @DatafeedID =101
SET @DatafeedCode ='ColumbusFair'
SET @LogFileName = 'ColumbusFair.log'
SET @Category ='Dataload'
SET @OperatorName = NULL	-- NEEDS TO CHANGE PER ENV.
SET @OwnerName ='sa'

SET @JobName = 'DataLoad: '+ @DatafeedCode + ' auction data'
SET @JobDescription =@DatafeedCode + ' auction data load routine'
SET @LogFilePath = '\\' + @@SERVERNAME + '\AuditLogs\DataLoadJobs'
SET @OutputFileName = @LogFilePath + '\' + @LogFileName
SET @LogCommand = 'EXECUTE sp_FileTimestampAndCycle ''' + @LogFilePath +''', ''' + @LogFileName + ''', 7'

SET @Step2Name = 'Stage ' + @DatafeedCode + '  data' 
SET @Step2Command ='EXECUTE  DBO.ETL_ProcessDatasource ' + CAST(@DatafeedID AS VARCHAR(5))

SET @Step6Name = 'Resolve failed ' + @DatafeedCode + ' load'  

SET @Step6Command ='--  First, rollback "de-staged" data if necessary'+   CHAR(13)+ CHAR(10)
SET @Step6Command = @Step6Command + 'EXECUTE RollbackStagedData_'+ @DatafeedCode +'   '+CAST(@DatafeedID AS VARCHAR(5))+   CHAR(13)+ CHAR(10)+   CHAR(13)+ CHAR(10)
SET @Step6Command = @Step6Command + '----  Unlock the process for the next attempt'+   CHAR(13)+ CHAR(10)
SET @Step6Command = @Step6Command +'EXECUTE SetLockState ''Unlocked''' 
 
SET @AlertWMIQuery ='SELECT * FROM __instancecreationevent WITHIN 30 WHERE TargetInstance ISA "Cim_DirectoryContainsFile" AND TargetInstance.GroupComponent="Win32_Directory.Name=\"I:\\\\INT_B\\\\Datafeeds\\\\'+@DatafeedCode+'\""'  
SET @AlertName = @DatafeedCode + ' Datafeed Watcher'


BEGIN TRANSACTION

IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = @JobName)
	EXEC msdb.dbo.sp_delete_job
	        @job_name = @JobName, -- sysname
	        @delete_history = 0, -- bit
	        @delete_unused_schedule = 1-- bit
	        
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=@Category AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=@Category
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@JobName, 
		@enabled=1, 
		@notify_level_eventlog=2, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description= @JobDescription, 
		@category_name=@Category, 
		@owner_login_name= @OwnerName, 
		@notify_email_operator_name=@OperatorName, 
		@job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Lock the Load Process', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=7, 
		@retry_attempts=4, 
		@retry_interval=2, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--  Only one Online Auction dataload process may run at a given time
EXECUTE SetLockState ''Locked''', 
		@database_name=N'Auction', 
		@output_file_name=@OutputFileName, 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=@Step2Name, 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=6, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=@Step2Command,
		@database_name=N'DBASTAT', 
		@output_file_name=@OutputFileName, 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Load Future Auction Data', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=6, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXECUTE LoadData', 
		@database_name=N'Auction', 
		@output_file_name=@OutputFileName, 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Unlock the load process', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=7, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'----  Only one Online Auction dataload process may run at a given time
EXECUTE SetLockState ''Unlocked''', 
		@database_name=N'Auction', 
		@output_file_name=@OutputFileName, 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Timestamp and cycle audit logs (on success)', 
		@step_id=5, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=@LogCommand, 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=@Step6Name,
		@step_id=6, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=@Step6Command, 
		@database_name=N'Auction', 
		@output_file_name=@OutputFileName, 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Timestamp and cycle audit logs (on failure)]    Script Date: 04/21/2010 16:32:54 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Timestamp and cycle audit logs (on failure)', 
		@step_id=7, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=@LogCommand, 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Every 20 minutes between 00:13:00 and 23:59:59', 
		@enabled=0, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=4, 
		@freq_subday_interval=20, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20060817, 
		@active_end_date=99991231, 
		@active_start_time=1300, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

 

IF EXISTS(SELECT 1 FROM msdb.dbo.sysalerts S WHERE name = @AlertName)
BEGIN
	 	EXEC dbo.sp_delete_alert @name =@AlertName 
 
END	

EXEC msdb.dbo.sp_add_alert @name=@AlertName, 
		@message_id=0, 
		@severity=0, 
		@enabled=1, 
		@delay_between_responses=0, 
		@include_event_description_in=0, 
		@category_name=N'[Uncategorized]', 
		@wmi_namespace=N'\\.\root\cimv2', 
		@wmi_query=@AlertWMIQuery,		
		@job_id=@jobId
		
GO		
 	