-----------------------------------------------------------------------------------------------------------------------
--  TO ADD MESSAGES, copy and paste a union all statement from below.  Please keep messages in numerical order!
-----------------------------------------------------------------------------------------------------------------------


set nocount on

declare @messages table
(
	message_id int not null,
	language_id smallint not null,
	severity tinyint not null,
	is_event_logged bit not null,
	text nvarchar(255) not null,
	message_dirty bit null, -- used by the upsert part of this script, no need to add to large union all statement below for new messages
	spLang sysname null -- same as message_dirty ^
)



insert @messages
(
	message_id,
	language_id,
	severity,
	is_event_logged,
	text
)
          select message_id = 50100, language_id = 1033, severity = 16, is_event_logged = 0, text = '%s cannot be null. 
   Please reexecute with a more appropriate value.'
union all select message_id = 50101, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Mode expects a value of either ''A'' (All Inventory) or ''R'' (Pricing Risk). 
   Please reexecute with a more appropriate value.'
union all select message_id = 50102, language_id = 1033, severity = 16, is_event_logged = 0, text = 'SaleStrategy expects a value of either ''A'' (All Inventory) or ''R'' (Retail Only). 
   Please reexecute with a more appropriate value.'
union all select message_id = 50103, language_id = 1033, severity = 16, is_event_logged = 0, text = 'ColumnIndex must be between 1 and 6. 
   Please reexecute with a more appropriate value.'
union all select message_id = 50104, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Filter Mode must hold a different value to Mode. 
   Please reexecute with a more appropriate value.'
union all select message_id = 50105, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Inventory %d is inactive. 
   Please reexecute with a more appropriate value.'
union all select message_id = 50106, language_id = 1033, severity = 16, is_event_logged = 0, text = 'No such %s record with ID %d. 
   Please reexecute with a more appropriate value.'
union all select message_id = 50107, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Invalid VIN ''%s''. 
   Please reexecute with a more appropriate value.'
union all select message_id = 50108, language_id = 1033, severity = 16, is_event_logged = 0, text = '%s must be greater or equal to zero. 
   Please reexecute with a more appropriate value.'
union all select message_id = 50109, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Invalid %s ''%d''; expected value between %d and %d
Please reexecute with a more appropriate value.'
union all select message_id = 50110, language_id = 1033, severity = 16, is_event_logged = 0, text = 'No such %s record with ID %s. 
   Please reexecute with a more appropriate value.'
union all select message_id = 50111, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Data Error!  %s must be between %d and %d characters'
union all select message_id = 50112, language_id = 1033, severity = 16, is_event_logged = 0, text = 'That account is already associated with a dealer.'
union all select message_id = 50113, language_id = 1033, severity = 16, is_event_logged = 0, text = 'The record exists in the %s table.'
union all select message_id = 50114, language_id = 1033, severity = 16, is_event_logged = 0, text = 'The VIN %s is not 17 characters long.'
union all select message_id = 50115, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Row has wrong version in %s table.'
union all select message_id = 50116, language_id = 1033, severity = 16, is_event_logged = 0, text = 'The Response for Request does not have a success code (500 or 501).'
union all select message_id = 50117, language_id = 1033, severity = 16, is_event_logged = 0, text = 'OwnerCount cannot be < 0.'
union all select message_id = 50118, language_id = 1033, severity = 16, is_event_logged = 0, text = 'ExpirationDate cannot be in the past.'
union all select message_id = 50200, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Data Error! 
   Expected 1 row but got %d rows!'
union all select message_id = 50201, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Data Error! 
   Pricing Graph Mode ''A'' expects two or more rows!'
union all select message_id = 50202, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Data Error! 
   Pricing Graph Mode ''R'' expects six rows!'
   
/*
union all select message_id = 50203, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Data Error! 
   Failed to %s a row in table %s'
union all select message_id = 50203, language_id = 1033, severity = 16, is_event_logged = 0, text = 'DealerID %d already exists in this table.'
*/

union all select message_id = 50204, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Data Error! 
   Expected %s %d row(s) but got %d rows!'
union all select message_id = 50300, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Processing Error: VehicleCatalogID is NULL'
union all select message_id = 50301, language_id = 1033, severity = 16, is_event_logged = 0, text = 'A SearchID or OwnerID must be passed into this stored procedure'
union all select message_id = 50302, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Unable to obtain "PING Aggregation" applock.'
union all select message_id = 50303, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Unable to release "PING Aggregation" applock.'
union all select message_id = 50304, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Deadlock occured during processing.'
union all select message_id = 50305, language_id = 1033, severity = 16, is_event_logged = 0, text = '%s is an invalid squish VIN.'
union all select message_id = 50306, language_id = 1033, severity = 16, is_event_logged = 0, text = '%s does not have a valid TMV region associated with it.'
union all select message_id = 50310, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Report has not yet been generated.'
union all select message_id = 50311, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Report generated with error.'
union all select message_id = 50312, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Concurrent change detected!'
union all select message_id = 50401, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Invalid provider identifier or name.'
union all select message_id = 50402, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Invalid provider status identifier or name.'
union all select message_id = 60101, language_id = 1033, severity = 11, is_event_logged = 0, text = 'Unable to find DatasourceID for DatafeedCode "%s".'
union all select message_id = 60102, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Unable to determine file type for file name "%s", DatasourceID #%d.'
union all select message_id = 60103, language_id = 1033, severity = 11, is_event_logged = 0, text = 'Fatal Staging Error: File "%s" already processed.'
union all select message_id = 60104, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Unable to determine DatafeedCode for file name "%s", DatasourceID #%d.'


-----------------------------------------------------------------------------------------------------------------------
--  DO NOT MODIFY SCRIPT BELOW THIS LINE!
-----------------------------------------------------------------------------------------------------------------------

-- look for dupes
if exists(select * from @messages group by message_id having count(*) > 1)
begin
	raiserror('key number re-use! messages must be unique!', 16, 1)
	select OffendingMessages = message_id from @messages group by message_id having count(*) > 1
	return
end

-- find messages that need upsert
update m set
	message_dirty =
		case
			when exists(
					select * from sys.messages
					where
						message_id = m.message_id
						and language_id = m.language_id
						and severity = m.severity
						and is_event_logged = m.is_event_logged
						and text = m.text
				) then 0
			else 1
		end
from @messages m



-- get system language name
update m set
	spLang = l.name
from
	@messages m
	join master.dbo.syslanguages l
		on m.language_id = l.msglangid
		

		
-- bail on language lookup or other data issues		
if exists(select * from @messages where message_dirty is null or spLang is null)
begin
	raiserror('Error with messages, see result set!', 16, 1)
	select * from @messages where message_dirty is null or spLang is null
	return
end


declare
	@message_id int,
	@language_id smallint,
	@severity tinyint,
	@text nvarchar(255),
	@spLang sysname,
	@spWithLog varchar(5)


-- deletes
while (1=1)
begin
	select top 1
		@message_id = message_id
	from sys.messages s
	where
		message_id >= 50000
		and not exists (select * from @messages where message_id = s.message_id)
	
	if (@@rowcount <> 1) break

	if exists(select * from sys.messages where message_id = @message_id)
	begin
		exec sp_dropmessage @msgnum = @message_id, @lang = 'all'
	end
end


-- upserts
while (1=1)
begin
	select top 1
		@message_id = message_id,
		@language_id = language_id,
		@severity = severity,
		@text = text,
		@spLang = spLang,
		@spWithLog = case is_event_logged when 1 then 'TRUE' else 'FALSE' end
	from @messages m
	where message_dirty = 1
	
	if (@@rowcount <> 1) break
	delete @messages where message_id = @message_id

	
	
	if exists(select * from sys.messages where message_id = @message_id)
	begin
		exec sp_dropmessage @msgnum = @message_id, @lang = 'all'
	end
	
	
	exec sp_addmessage
		@msgnum = @message_id,
		@severity = @severity,
		@msgtext = @text,
		@lang = @spLang,
		@with_log = @spWithLog,
		@replace = null
end

go