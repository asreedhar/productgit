IF NOT EXISTS (	SELECT	1
		FROM	sys.server_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'Firstlook\Engineering'
		)

	CREATE LOGIN [FIRSTLOOK\Engineering] FROM WINDOWS WITH DEFAULT_DATABASE=[IMT]

GO


EXEC sp_map_exec

'USE [?]
IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = "WINDOWS_GROUP"
			AND name = "FIRSTLOOK\Engineering"
		)
	CREATE USER [FIRSTLOOK\Engineering] FOR LOGIN [FIRSTLOOK\Engineering] 
	
EXEC sp_addrolemember N"db_datareader", N"FIRSTLOOK\Engineering"
',
'
SELECT	name
FROM	sys.databases
WHERE	name not in ("master","tempdb","model","msdb") and is_read_only = 0
'

GO
USE msdb
EXEC sp_addrolemember N'SQLAgentReaderRole', N'FIRSTLOOK\Engineering'
GO


USE master

GRANT VIEW ANY DEFINITION TO [FIRSTLOOK\Engineering]

GO	

USE Market
EXEC sp_addrolemember 'PricingUser', N'FIRSTLOOK\Engineering'
GO