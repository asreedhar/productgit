USE master
GO

alter login firstlook disable; alter login merchandisingWebsite disable;
GO

-----------------------------------
/* kill the SPIDS one by one, instead of trying all at the same time - which is more error prone */
DECLARE @t TABLE (k INT IDENTITY(1,1) NOT NULL, spid INT)

INSERT INTO @t (spid) SELECT spid FROM MASTER..SysProcesses WHERE DBId = DB_ID('Market') AND SPID <> @@SPID

DECLARE @i INT, @s VARCHAR(max)

SET @i = 1
WHILE @i <= (SELECT MAX(k) FROM @t)
BEGIN
  select @s = 'Kill ' + Convert(varchar, SPID) FROM @t WHERE k = @i
	BEGIN TRY
      EXECUTE (@s)
	END TRY
	BEGIN CATCH
	END CATCH;   
  SET @i = @i + 1	
END 
-----------------------------------

GO

IF (SELECT is_broker_enabled FROM sys.databases WHERE database_id = DB_ID()) = 0
ALTER DATABASE Market SET NEW_BROKER
GO

USE Market;

DECLARE @thumb VARBINARY(32)

SELECT  @thumb = thumbprint
FROM    sys.certificates
WHERE   name = 'ServiceBrokerActivationCertificate'

SELECT  crypt_property AS signature
FROM    sys.crypt_properties
WHERE   thumbprint = @thumb
AND     OBJECT_NAME(major_id) = 'Report#Generate_ServiceBroker'

IF (@@ROWCOUNT = 0)
        ADD SIGNATURE TO Reporting.Report#Generate_ServiceBroker
                BY CERTIFICATE ServiceBrokerActivationCertificate
                WITH PASSWORD = 'n362ED4J'
GO

ALTER QUEUE Reporting.ReportGeneration#TargetQueue WITH
        STATUS = ON,
        ACTIVATION (
                STATUS                  = ON,
                PROCEDURE_NAME          = Reporting.Report#Generate_ServiceBroker,
                MAX_QUEUE_READERS       = 1,
                EXECUTE AS OWNER
        )
GO


DELETE RG
FROM    Market.Reporting.Report R
       INNER JOIN Market.Reporting.ReportGenerator RG ON R.ReportID = RG.ReportID
WHERE  ReportStatusID IN (1,4)
go

DELETE R
FROM    Market.Reporting.Report R
WHERE  ReportStatusID IN (1,4)
go


alter login firstlook enable; alter login merchandisingWebsite enable;
go
