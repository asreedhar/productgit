-----------------------------------------------------------------------------------------------------------------------
--  TO ADD MESSAGES, copy and paste a union all statement from below.  Please keep messages in numerical order!
-----------------------------------------------------------------------------------------------------------------------


set nocount on

declare @messages table
(
	message_id int not null,
	language_id smallint not null,
	severity tinyint not null,
	is_event_logged bit not null,
	text nvarchar(255) not null,
	message_dirty bit null, -- used by the upsert part of this script, no need to add to large union all statement below for new messages
	spLang sysname null -- same as message_dirty ^
)



insert @messages
(
	message_id,
	language_id,
	severity,
	is_event_logged,
	text
)
          select message_id = 50100, language_id = 1033, severity = 16, is_event_logged = 0, text = 'The parameter %s cannot be null.'
union all select message_id = 50400, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Optimistic locking failed.  The record in %s has changed since you last retrieved the data.'
union all select message_id = 50500, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Id does not exist in table %s.'
union all select message_id = 50600, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Id already in use in table %s.'
union all select message_id = 50700, language_id = 1033, severity = 16, is_event_logged = 0, text = 'Cannot delete from table %s.  Record in use.'
union all select message_id = 50801, language_id = 1033, severity = 16, is_event_logged = 0, text = 'LogonA and LogonFI are not on the same Host.'
union all select message_id = 50802, language_id = 1033, severity = 16, is_event_logged = 0, text = 'All Connections for the same dealer must be on the same Host.'



-----------------------------------------------------------------------------------------------------------------------
--  DO NOT MODIFY SCRIPT BELOW THIS LINE!
-----------------------------------------------------------------------------------------------------------------------

-- look for dupes
if exists(select * from @messages group by message_id having count(*) > 1)
begin
	raiserror('key number re-use! messages must be unique!', 16, 1)
	select OffendingMessages = message_id from @messages group by message_id having count(*) > 1
	return
end

-- find messages that need upsert
update m set
	message_dirty =
		case
			when exists(
					select * from sys.messages
					where
						message_id = m.message_id
						and language_id = m.language_id
						and severity = m.severity
						and is_event_logged = m.is_event_logged
						and text = m.text
				) then 0
			else 1
		end
from @messages m



-- get system language name
update m set
	spLang = l.name
from
	@messages m
	join master.dbo.syslanguages l
		on m.language_id = l.msglangid
		

		
-- bail on language lookup or other data issues		
if exists(select * from @messages where message_dirty is null or spLang is null)
begin
	raiserror('Error with messages, see result set!', 16, 1)
	select * from @messages where message_dirty is null or spLang is null
	return
end


declare
	@message_id int,
	@language_id smallint,
	@severity tinyint,
	@text nvarchar(255),
	@spLang sysname,
	@spWithLog varchar(5)


-- deletes
while (1=1)
begin
	select top 1
		@message_id = message_id
	from sys.messages s
	where
		message_id >= 50000
		and not exists (select * from @messages where message_id = s.message_id)
	
	if (@@rowcount <> 1) break

	if exists(select * from sys.messages where message_id = @message_id)
	begin
		exec sp_dropmessage @msgnum = @message_id, @lang = 'all'
	end
end


-- upserts
while (1=1)
begin
	select top 1
		@message_id = message_id,
		@language_id = language_id,
		@severity = severity,
		@text = text,
		@spLang = spLang,
		@spWithLog = case is_event_logged when 1 then 'TRUE' else 'FALSE' end
	from @messages m
	where message_dirty = 1
	
	if (@@rowcount <> 1) break
	delete @messages where message_id = @message_id

	
	
	if exists(select * from sys.messages where message_id = @message_id)
	begin
		exec sp_dropmessage @msgnum = @message_id, @lang = 'all'
	end
	
	
	exec sp_addmessage
		@msgnum = @message_id,
		@severity = @severity,
		@msgtext = @text,
		@lang = @spLang,
		@with_log = @spWithLog,
		@replace = null
end

go