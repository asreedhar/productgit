------------------------------------------------------------------------
--	CREATE THE APPLICATION LOGIN
------------------------------------------------------------------------



IF NOT EXISTS (	SELECT	1
		FROM	sys.server_principals
		WHERE	type_desc = 'SQL_LOGIN'
			AND name = 'firstlook'
		) 
		

	CREATE LOGIN firstlook WITH PASSWORD='N@d@123', DEFAULT_DATABASE = tempdb, CHECK_EXPIRATION = OFF, CHECK_POLICY = OFF


GO
