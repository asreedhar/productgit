IF NOT EXISTS (	SELECT	1
		FROM	sys.server_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'Firstlook\Analytics'
		)

	CREATE LOGIN [FIRSTLOOK\Analytics] FROM WINDOWS WITH DEFAULT_DATABASE=[Merchandising]

GO


EXEC sp_map_exec

'USE [?]

EXEC sp_change_users_login @Action = "Auto_Fix", @UserNamePattern = ''Firstlook\Analytics''


IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = "WINDOWS_GROUP"
			AND name = "FIRSTLOOK\Analytics"
		)	

	CREATE USER [FIRSTLOOK\Analytics] FOR LOGIN [FIRSTLOOK\Analytics]

 
EXEC sp_addrolemember N"db_datawriter", N"FIRSTLOOK\QA"
EXEC sp_addrolemember N"db_datareader", N"FIRSTLOOK\Analytics"
',
'
SELECT	name
FROM	sys.databases
WHERE	name not in ("master","tempdb","model","msdb") and is_read_only = 0
'

GO
USE msdb
EXEC sp_addrolemember N'SQLAgentReaderRole', N'FIRSTLOOK\Analytics'
GO


USE master

GRANT VIEW ANY DEFINITION TO [FIRSTLOOK\Analytics]

GO	

