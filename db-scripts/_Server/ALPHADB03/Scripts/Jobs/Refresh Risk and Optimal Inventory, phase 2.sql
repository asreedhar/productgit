USE [msdb]


IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = 'Refresh Risk and Optimal Inventory, phase 2')
	EXEC msdb.dbo.sp_delete_job
	        @job_name = 'Refresh Risk and Optimal Inventory, phase 2', -- sysname
	        @delete_history = 1, -- bit
	        @delete_unused_schedule = 1-- bit
GO
/****** Object:  Job [Refresh Risk and Optimal Inventory, phase 2]    Script Date: 02/16/2010 09:35:03 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Java Processor]    Script Date: 02/16/2010 09:35:04 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Java Processor' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Java Processor'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Refresh Risk and Optimal Inventory, phase 2', 
		@enabled=1, 
		@notify_level_eventlog=2, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Run the CIA processor to create/populate the Optimal Inventory Engine data.', 
		@category_name=N'Java Processor', 
		@owner_login_name=N'sa', 
		@job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Check for phase 1 completion]    Script Date: 02/16/2010 09:35:04 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Check for phase 1 completion', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=5, 
		@retry_attempts=23, 
		@retry_interval=10, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--  Check if phase 1 completed
SET NOCOUNT on

DECLARE
  @Phase1     varchar(50)
 ,@Phase2     varchar(50)
 ,@Phase1_At  datetime
 ,@Phase2_At  datetime
 ,@Interim    varchar(50)

SET @Phase1  = ''Refresh R&O Inventory, phase 1 complete''
SET @Phase2  = ''Refresh R&O Inventory, phase 2 complete''
SET @Interim = ''Refresh R&O Inventory, phase 2 begun''


SELECT @Phase1_At = LastUpdated
 from BETADB01.Utility.dbo.EventFlag
 where Event = @Phase1

SELECT @Phase2_At = LastUpdated
 from EventFlag
 where Event = @Phase2

--  If phase 1 flag exists and (is after phase 2 flag or phase 2 flag does not exist)
IF @Phase1_At is not null and (@Phase1_At > @Phase2_At or @Phase2_At is null)
 BEGIN
    --  Unprocessed data found
    IF exists (select 1 from EventFlag where Event = @Interim)
        --  Update entry
        UPDATE EventFlag
         set LastUpdated = @Phase1_At
         where Event = @Interim
    ELSE
        --  Create entry
        INSERT EventFlag (Event, LastUpdated)
         values (@Interim, @Phase1_At)
 END

ELSE
    --  No new data
    RAISERROR(''Fresh R&O data not found'', 11, 1)
', 
		@database_name=N'Utility', 
		@output_file_name=N'\\BETADB03\AuditLogs\SQLAgentJobs\RefreshRisk&OptimalInventory_2.log', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Optimal Inventory Engine]    Script Date: 02/16/2010 09:35:04 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Optimal Inventory Engine', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=5, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXECUTE dbo.LaunchCIAEngine

', 
		@database_name=N'Utility', 
		@output_file_name=N'\\BETADB03\AuditLogs\SQLAgentJobs\RefreshRisk&OptimalInventory_2.log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Mark phase 2 as completed]    Script Date: 02/16/2010 09:35:04 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Mark phase 2 as completed', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=5, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--  Flag phase 2 as completed
SET NOCOUNT on

DECLARE
  @Phase2     varchar(50)
 ,@Interim    varchar(50)

SET @Phase2  = ''Refresh R&O Inventory, phase 2 complete''
SET @Interim = ''Refresh R&O Inventory, phase 2 begun''


IF exists (select 1 from EventFlag where Event = @Phase2)
    --  Update entry
    UPDATE EventFlag
     set LastUpdated = (select LastUpdated from EventFlag where Event = @Interim)
     where Event = @Phase2

ELSE
    --  Create entry
    INSERT EventFlag (Event, LastUpdated)
     select @Phase2, LastUpdated
      from EventFlag
      where Event = @Interim
', 
		@database_name=N'Utility', 
		@output_file_name=N'\\BETADB03\AuditLogs\SQLAgentJobs\RefreshRisk&OptimalInventory_2.log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Timestamp and cycle audit logs (on success)]    Script Date: 02/16/2010 09:35:04 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Timestamp and cycle audit logs (on success)', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXECUTE sp_FileTimestampAndCycle ''\\BETADB03\AuditLogs\SQLAgentJobs'', ''RefreshRisk&OptimalInventory_2.log'', 14', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Timestamp and cycle audit logs (on failure)]    Script Date: 02/16/2010 09:35:05 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Timestamp and cycle audit logs (on failure)', 
		@step_id=5, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXECUTE sp_FileTimestampAndCycle ''\\BETADB03\AuditLogs\SQLAgentJobs'', ''RefreshRisk&OptimalInventory_2.log'', 14', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Ad-hoc', 
		@enabled=0, 
		@freq_type=1, 
		@freq_interval=0, 
		@freq_subday_type=0, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20080806, 
		@active_end_date=99991231, 
		@active_start_time=40000, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Daily at 02:28:00', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20070123, 
		@active_end_date=99991231, 
		@active_start_time=22800, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
