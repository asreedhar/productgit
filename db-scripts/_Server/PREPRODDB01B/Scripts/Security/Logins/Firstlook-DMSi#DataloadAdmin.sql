IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'Firstlook\DMSi#DataloadAdmin'
		)

	CREATE LOGIN [FIRSTLOOK\DMSi#DataloadAdmin] FROM WINDOWS WITH DEFAULT_DATABASE=[DBASTAT]

GO

USE master
GRANT VIEW ANY DEFINITION TO [Firstlook\DMSi#DataloadAdmin]
GO

EXEC sp_map_exec

'USE [?]
CREATE USER [FIRSTLOOK\DMSi#DataloadAdmin] FOR LOGIN [FIRSTLOOK\DMSi#DataloadAdmin]
EXEC sp_addrolemember N"db_datareader", N"FIRSTLOOK\DMSi#DataloadAdmin"
EXEC sp_addrolemember N"db_datawriter", N"FIRSTLOOK\DMSi#DataloadAdmin"
',
'
SELECT	name
FROM	sys.databases
WHERE	owner_sid <> 0x01
'

GO

USE [msdb]
EXEC sp_addrolemember N'SQLAgentReaderRole', N'FIRSTLOOK\DMSi#DataloadAdmin'
GO


USE IMT

EXEC sp_addrolemember N'db_owner', N'FIRSTLOOK\DMSi#DataloadAdmin'
GO

USE DBASTAT

EXEC sp_addrolemember N'db_owner', N'FIRSTLOOK\DMSi#DataloadAdmin'
GO
