IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'Firstlook\DMSi'
		)

	CREATE LOGIN [FIRSTLOOK\DMSi] FROM WINDOWS WITH DEFAULT_DATABASE=[IMT]

GO

USE master
GRANT VIEW ANY DEFINITION TO [Firstlook\DMSi]
GO

EXEC sp_map_exec

'USE [?]
CREATE USER [FIRSTLOOK\DMSi] FOR LOGIN [FIRSTLOOK\DMSi]
EXEC sp_addrolemember N"db_datareader", N"FIRSTLOOK\DMSi"
EXEC sp_addrolemember N"db_datawriter", N"FIRSTLOOK\DMSi"
',
'
SELECT	name
FROM	sys.databases
WHERE	owner_sid <> 0x01
'

GO

USE [msdb]
EXEC sp_addrolemember N'SQLAgentReaderRole', N'FIRSTLOOK\DMSi'
GO


