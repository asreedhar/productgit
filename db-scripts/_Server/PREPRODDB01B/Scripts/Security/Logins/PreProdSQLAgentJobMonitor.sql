----------------------------------------------------------------------------------------
--	CREATE LOGIN
----------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.server_principals WHERE name = N'FIRSTLOOK\ProductionSQLAgentJobMonitor')

	CREATE LOGIN [FIRSTLOOK\PreProdSQLAgentJobMonitor] FROM WINDOWS WITH DEFAULT_DATABASE=[msdb]


----------------------------------------------------------------------------------------
--	CREATE USER, ROLE, AND ASSIGN db_datareader, DataManagement (if available)
----------------------------------------------------------------------------------------
GO

USE msdb

CREATE USER [FIRSTLOOK\PreProdSQLAgentJobMonitor] FOR LOGIN [FIRSTLOOK\PreProdSQLAgentJobMonitor]


EXEC sp_addrolemember N'SQLAgentReaderRole', N'FIRSTLOOK\PreProdSQLAgentJobMonitor'
GO
	