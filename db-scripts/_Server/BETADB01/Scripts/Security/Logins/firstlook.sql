IF NOT EXISTS (	SELECT	1
		FROM	sys.server_principals
		WHERE	type_desc = 'sql_login'
			AND name = 'firstlook'
		)

	CREATE LOGIN firstlook WITH PASSWORD = 'look@tm31st', DEFAULT_DATABASE = IMT, CHECK_EXPIRATION = OFF, CHECK_POLICY = OFF

GO