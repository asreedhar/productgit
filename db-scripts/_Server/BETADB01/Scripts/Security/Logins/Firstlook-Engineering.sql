USE master

GRANT VIEW SERVER STATE TO [FIRSTLOOK\Engineering]
GRANT VIEW ANY DEFINITION TO [Firstlook\Engineering]
 
GO

USE VehicleCatalog

GRANT SELECT ON Edmunds.GetEdmundsTMVValues TO [Firstlook\Engineering]
GRANT EXEC ON Edmunds.GetEdmundsTMV TO [Firstlook\Engineering]
GO

use msdb

EXEC sp_addrolemember N'SQLAgentOperatorRole', N'Firstlook\Engineering'
go