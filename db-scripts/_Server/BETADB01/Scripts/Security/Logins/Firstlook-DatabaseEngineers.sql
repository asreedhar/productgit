IF NOT EXISTS (	SELECT	1
		FROM	sys.server_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'Firstlook\DatabaseEngineers'
		)

	CREATE LOGIN [FIRSTLOOK\DatabaseEngineers] FROM WINDOWS WITH DEFAULT_DATABASE=[master]

GO
USE master
grant exec to [Firstlook\DatabaseEngineers]
GO

EXEC sp_map_exec

'USE [?]
IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = "WINDOWS_GROUP"
			AND name = "FIRSTLOOK\DatabaseEngineers"
		)
	CREATE USER [FIRSTLOOK\DatabaseEngineers] FOR LOGIN [FIRSTLOOK\DatabaseEngineers]

EXEC sp_addrolemember N"db_datareader", N"FIRSTLOOK\DatabaseEngineers"
EXEC sp_addrolemember N"db_datawriter", N"FIRSTLOOK\DatabaseEngineers"
GRANT EXEC TO [Firstlook\DatabaseEngineers]
',
'
SELECT	name
FROM	sys.databases
WHERE	name not in ("master","tempdb","model","msdb") and is_read_only = 0
'
GO

USE [master]
GO

GRANT ALTER TRACE TO [Firstlook\DatabaseEngineers]
GO

GRANT VIEW ANY DEFINITION TO [FIRSTLOOK\DatabaseEngineers]
GO