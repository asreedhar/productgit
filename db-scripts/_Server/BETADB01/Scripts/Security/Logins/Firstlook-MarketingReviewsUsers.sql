IF NOT EXISTS (SELECT * FROM sys.server_principals WHERE name = N'FIRSTLOOK\MarketingReviewsUsers')
BEGIN
	CREATE LOGIN [FIRSTLOOK\MarketingReviewsUsers] FROM WINDOWS WITH DEFAULT_DATABASE=[Merchandising], DEFAULT_LANGUAGE=[us_english]
END
GO
