IF NOT EXISTS (	SELECT	1
		FROM	sys.server_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'Firstlook\Analytics'
		)

	CREATE LOGIN [FIRSTLOOK\Analytics] FROM WINDOWS WITH DEFAULT_DATABASE=[Market]

GO


EXEC sp_map_exec

'USE [?]

EXEC sp_change_users_login @Action = "Auto_Fix", @UserNamePattern = ''Firstlook\Analytics''


IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = "WINDOWS_GROUP"
			AND name = "Firstlook\Analytics"
		)
	CREATE USER [FIRSTLOOK\Analytics] FOR LOGIN [FIRSTLOOK\Analytics] 

EXEC sp_addrolemember N"db_datareader", N"FIRSTLOOK\Analytics"
EXEC sp_addrolemember N"db_datawriter", N"FIRSTLOOK\Analytics"
GRANT EXEC TO [FIRSTLOOK\Analytics] 
',
'
SELECT	name
FROM	sys.databases
WHERE	name not in ("master","tempdb","model","msdb", "HAL") and is_read_only = 0
'

GO
USE msdb
EXEC sp_addrolemember N'SQLAgentReaderRole', N'FIRSTLOOK\Analytics'
GO


USE master

GRANT VIEW ANY DEFINITION TO [FIRSTLOOK\Analytics]

GO	

USE Market

EXEC sp_addrolemember N'ReportingUser', N'FIRSTLOOK\Analytics'
GO

-- need to investigate user Analytics in HAL -- mapped to login FIRSTLOOK\Analytics!