----------------------------------------------------------------------------------------
--	CREATE LOGIN
----------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT * FROM sys.server_principals WHERE name = N'FIRSTLOOK\BetaTester')

	CREATE LOGIN [FIRSTLOOK\BetaTester] FROM WINDOWS WITH DEFAULT_DATABASE=[IMT]


----------------------------------------------------------------------------------------
--	CREATE USER, ROLE, AND ASSIGN db_datareader
----------------------------------------------------------------------------------------


EXEC sp_map_exec 
'
USE [?]

IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = "WINDOWS_GROUP"
			AND name = "Firstlook\BetaTester"
		)
	CREATE USER [FIRSTLOOK\BetaTester] FOR LOGIN [FIRSTLOOK\BetaTester]

EXEC sp_addrolemember N"db_datareader", N"FIRSTLOOK\BetaTester"

',
'SELECT	name
FROM	MASTER.sys.databases
WHERE	name not in ("master","tempdb","model","msdb") and is_read_only = 0
',0


USE master

GRANT VIEW ANY DEFINITION TO [FIRSTLOOK\BetaTester]

GO	


