CREATE LOGIN [FIRSTLOOK\svcCarfaxBeta] FROM WINDOWS WITH DEFAULT_DATABASE=[IMT]
GO

USE IMT

EXEC sp_addrolemember N'CarfaxProcessorService', N'Firstlook\svcCarfaxBeta'
GO