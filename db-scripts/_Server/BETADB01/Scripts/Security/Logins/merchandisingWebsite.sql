------------------------------------------------------------------------
--	CREATE THE APPLICATION LOGIN
------------------------------------------------------------------------

if not exists (
	select * from sys.server_principals
	where
		type_desc = 'sql_login'
		and name = 'merchandisingWebsite'
)
	create login merchandisingWebsite with password = 'JBd!95Zk(8x+', default_database = tempdb, check_expiration = off, check_policy = off
go



------------------------------------------------------------------------
--	FIX THE LOGIN MAPPINGS SINCE THE DBs ARE PROD CUTS
------------------------------------------------------------------------

EXEC sp_map_exec

'USE [?]

EXEC sp_change_users_login @Action = "Auto_Fix", @UserNamePattern = "merchandisingWebsite"
',
'
SELECT	name
FROM	sys.databases
WHERE	name not in ("master","tempdb","model","msdb") and is_read_only = 0
'

GO
