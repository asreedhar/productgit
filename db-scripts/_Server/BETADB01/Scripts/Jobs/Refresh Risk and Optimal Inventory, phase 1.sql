USE [msdb]


IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = 'Refresh Risk and Optimal Inventory, phase 1')
	EXEC msdb.dbo.sp_delete_job
	        @job_name = 'Refresh Risk and Optimal Inventory, phase 1', -- sysname
	        @delete_history = 1, -- bit
	        @delete_unused_schedule = 1-- bit
GO

/****** Object:  Job [Refresh Risk and Optimal Inventory, phase 1]    Script Date: 02/16/2010 09:38:47 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Aggregate]    Script Date: 02/16/2010 09:38:47 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Aggregate' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Aggregate'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Refresh Risk and Optimal Inventory, phase 1', 
		@enabled=1, 
		@notify_level_eventlog=2, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'Recalculate assorted business values for dealers configured with RunDayOfWeek = the current day of the week', 
		@category_name=N'Aggregate', 
		@owner_login_name=N'sa', 
		@job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Discount Rate and Time Horizon]    Script Date: 02/16/2010 09:38:49 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Discount Rate and Time Horizon', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=6, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXECUTE dbo.PopulateDiscountRateAndTimeHorizon  @Mode = 0,  @RunDayOfWeekFilter = 1', 
		@database_name=N'IMT', 
		@output_file_name=N'\\BETADB01\AuditLogs\SQLAgentJobs\RefreshRisk&OptimalInventory_1.log', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Valuation]    Script Date: 02/16/2010 09:38:49 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Valuation', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=6, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXECUTE dbo.UpdateDealerValuation  @mode=0,  @RunDayOfWeekFilter = 1', 
		@database_name=N'IMT', 
		@output_file_name=N'\\BETADB01\AuditLogs\SQLAgentJobs\RefreshRisk&OptimalInventory_1.log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Inventory Lights]    Script Date: 02/16/2010 09:38:50 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Inventory Lights', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=6, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXECUTE dbo.InventoryLightProcessor  @mode = 1, @RunDayOfWeekFilter = 1', 
		@database_name=N'IMT', 
		@output_file_name=N'\\BETADB01\AuditLogs\SQLAgentJobs\RefreshRisk&OptimalInventory_1.log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Flag completion of phase 1]    Script Date: 02/16/2010 09:38:50 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Flag completion of phase 1', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=6, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--  Set the event flag for this process
DECLARE @Phase1 varchar(50)
SET @Phase1 = ''Refresh R&O Inventory, phase 1 complete''

IF exists (select 1 from EventFlag where Event = @Phase1)
    --  Update entry
    UPDATE EventFlag
     set LastUpdated = getdate()
     where Event = @Phase1
ELSE
    --  Create entry
    INSERT EventFlag (Event, LastUpdated)
     values (@Phase1, getdate())
', 
		@database_name=N'Utility', 
		@output_file_name=N'\\BETADB01\AuditLogs\SQLAgentJobs\RefreshRisk&OptimalInventory_1.log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Timestamp and cycle audit logs (on success)]    Script Date: 02/16/2010 09:38:50 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Timestamp and cycle audit logs (on success)', 
		@step_id=5, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--  Deletes all files marked as older than [14] days
EXECUTE sp_FileTimestampAndCycle ''\\BETADB01\AuditLogs\SQLAgentJobs'', ''RefreshRisk&OptimalInventory_1.log'', 14', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Timestamp and cycle audit logs (on failure)]    Script Date: 02/16/2010 09:38:50 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Timestamp and cycle audit logs (on failure)', 
		@step_id=6, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=1, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--  Deletes all files marked as older than [14] days
EXECUTE sp_FileTimestampAndCycle ''\\BETADB01\AuditLogs\SQLAgentJobs'', ''RefreshRisk&OptimalInventory_1.log'', 14', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Daily at 01:00:00', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20040311, 
		@active_end_date=99991231, 
		@active_start_time=10000, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
