USE [msdb]
GO


IF NOT EXISTS (SELECT 1 FROM msdb.dbo.sysoperators WHERE name = 'TestSuite')

	EXEC msdb.dbo.sp_add_operator @name=N'TestSuite', 
			@enabled=1, 
			@pager_days=0, 
			@email_address=N'ccortellini@incisent.com; mkipiniak@incisent.com;swenmouth@incisent.com;nbarad@incisent.com'
GO

IF NOT EXISTS (SELECT 1 FROM msdb.dbo.sysoperators WHERE name = 'TestSuite' AND email_address = 'ccortellini@incisent.com; swenmouth@incisent.com; mkipiniak@incisent.com')
	EXEC msdb.dbo.sp_update_operator @name=N'TestSuite', 
			@enabled=1, 
			@pager_days=0, 
			@email_address=N'ccortellini@incisent.com; swenmouth@incisent.com; mkipiniak@incisent.com', 
			@pager_address=N'', 
			@netsend_address=N''
GO

USE [msdb]

IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = 'TestSuite: Run TestCase#GenerateOnErrorTests')
	EXEC msdb.dbo.sp_delete_job
	        @job_name = 'TestSuite: Run TestCase#GenerateOnErrorTests', -- sysname
	        @delete_history = 1, -- bit
	        @delete_unused_schedule = 1-- bit

GO

/****** Object:  Job [TestSuite:  Run TestCase#GenerateOnErrorTests]    Script Date: 03/18/2010 16:30:55 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [TestSuite]]    Script Date: 03/18/2010 16:30:55 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'TestSuite' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'TestSuite'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'TestSuite: Run TestCase#GenerateOnErrorTests', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'This job runs a procedure that runs all the the TestCases in the Core.TestCaseGenerator table in the TestSuite database.', 
		@category_name=N'TestSuite', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name = N'TestSuite',
		@job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Run the procedure: TestSuite.Core.TestCase#GenerateOnErrorTests]    Script Date: 03/18/2010 16:30:56 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Run the procedure: TestSuite.Core.TestCase#GenerateOnErrorTests', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC [Core].[TestCase#GenerateOnErrorTests]', 
		@database_name=N'TestSuite', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Run Core.TestCase#GenerateOnErrorTests', 
		@enabled=1, 
		@freq_type=1, 
		@freq_interval=0, 
		@freq_subday_type=0, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20100318, 
		@active_end_date=99991231, 
		@active_start_time=173000, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Run Core.TestCase#GenerateOnErrorTests', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20100318, 
		@active_end_date=99991231, 
		@active_start_time=80030, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:


GO

DECLARE @job_id VARCHAR(50)
SELECT	@job_ID =job_id
FROM	 msdb.dbo.sysjobs 
WHERE	name = 'TestSuite: Run TestCase#GenerateOnErrorTests'

EXEC msdb.dbo.sp_update_job @job_id=@job_id, 
		@notify_level_email=2, 
		@notify_level_netsend=2, 
		@notify_level_page=2, 
		@notify_email_operator_name=N'TestSuite'


GO


USE [msdb]

IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = 'TestSuite: Run Core.TestRunner')
	EXEC msdb.dbo.sp_delete_job
	        @job_name = 'TestSuite: Run Core.TestRunner', -- sysname
	        @delete_history = 1, -- bit
	        @delete_unused_schedule = 1-- bit

GO
/****** Object:  Job [TestSuite:  Run Core.TestRunner]    Script Date: 03/18/2010 16:27:44 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [TestSuite]]    Script Date: 03/18/2010 16:27:44 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'TestSuite' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'TestSuite'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'TestSuite: Run Core.TestRunner', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'This job runs the procedure TestSuite.Core.TestRunner.  Upon its completion the job sends an email with the results to those in the Operator ''TestSuite''.', 
		@category_name=N'TestSuite', 
		@owner_login_name=N'sa', 
		@job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Run stored procedure TestSuite.Core.TestRunner]    Script Date: 03/23/2010 19:08:52 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Run stored procedure TestSuite.Core.TestRunner', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=3, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC [Core].[TestRunner]', 
		@database_name=N'TestSuite', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Send Test Suite Results email]    Script Date: 03/23/2010 19:08:52 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Send Test Suite Results email', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC Core.TestSuite#EmailResults 
@OperatorName =''TestSuite''', 
		@database_name=N'TestSuite', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Run Core.TestRunner test', 
		@enabled=1, 
		@freq_type=1, 
		@freq_interval=0, 
		@freq_subday_type=0, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20100318, 
		@active_end_date=99991231, 
		@active_start_time=170000, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Run Daily Core.TestRunner', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20100318, 
		@active_end_date=99991231, 
		@active_start_time=80000, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO