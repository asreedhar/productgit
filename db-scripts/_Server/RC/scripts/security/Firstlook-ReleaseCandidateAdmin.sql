



IF NOT EXISTS (	SELECT	1
		FROM	sys.server_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'Firstlook\ReleaseCandidateAdmin'
		)

	CREATE LOGIN [FIRSTLOOK\ReleaseCandidateAdmin]  FROM WINDOWS WITH DEFAULT_DATABASE=[master]

GO



EXEC sp_map_exec 
'
USE [?]

IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = "WINDOWS_GROUP"
			AND name = "Firstlook\ReleaseCandidateAdmin"
		)
	CREATE USER [FIRSTLOOK\ReleaseCandidateAdmin] FOR LOGIN [FIRSTLOOK\ReleaseCandidateAdmin]

EXEC sp_addrolemember N"db_owner", N"FIRSTLOOK\ReleaseCandidateAdmin"

',
'SELECT	name
FROM	MASTER.sys.databases
WHERE	name not in ("master","tempdb","model","msdb") and is_read_only = 0
'
GO

USE master

GRANT VIEW ANY DEFINITION TO [FIRSTLOOK\ReleaseCandidateAdmin]

GO	

USE [msdb]

IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'Firstlook\ReleaseCandidateAdmin'
		)
	CREATE USER [FIRSTLOOK\ReleaseCandidateAdmin] FOR LOGIN [FIRSTLOOK\ReleaseCandidateAdmin]


EXEC sp_addrolemember N'SQLAgentUserRole', N'FIRSTLOOK\ReleaseCandidateAdmin'
GO


