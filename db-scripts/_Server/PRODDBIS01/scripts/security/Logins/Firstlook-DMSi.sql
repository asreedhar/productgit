IF NOT EXISTS (	SELECT	1
		FROM	sys.server_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'Firstlook\DMSi'
		)

	CREATE LOGIN [FIRSTLOOK\DMSi] FROM WINDOWS WITH DEFAULT_DATABASE=[SIS]

GO
