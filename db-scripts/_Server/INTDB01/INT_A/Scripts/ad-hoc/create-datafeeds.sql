INSERT
INTO	DatafeedTargetFolder (DatafeedCode, TargetFolder, IsActive, Priority)

SELECT	DatafeedCode, 
	TargetFolder	= REPLACE(REPLACE(REPLACE(TargetFolder, 'PRODDB01SQL','INTDB01'), 'PRODDB03SQL','ALPHADB03'), 'Datafeeds','Datafeeds'),
	IsActive, 3
FROM	dbo.DatafeedTargetFolder DTF
WHERE	DatafeedCode IN ('CarsDotComListing', 'DMi', 'AutoTrader_Vehicles', 'GetAuto', 'ATC')
	AND TargetFolder LIKE '%PROD%'