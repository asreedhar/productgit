USE [msdb]

DECLARE @JobName	VARCHAR(100), 
	@LogFilePath	VARCHAR(100),
	@LogFileName	VARCHAR(50),
	@OutputFileName	VARCHAR(100),
	@LogCommand	VARCHAR(250)
	@EmailOperator	VARCHAR(128)

SET @JobName = 'DataLoad: VehicleCatalog'
SET @LogFilePath = '\\' + @@SERVERNAME + '\AuditLogs\DataLoadJobs'
SET @LogFileName = 'VehicleCatalog.log'
SET @OutputFileName = @LogFilePath + '\' + @LogFileName
SET @LogCommand = 'EXECUTE sp_FileTimestampAndCycle ''' + @LogFilePath +''', ''' + @LogFileName + '.log'', 7'
SET @EmailOperator = NULL	


BEGIN TRANSACTION

IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = @JobName)
	EXEC msdb.dbo.sp_delete_job
	        @job_name = @JobName, -- sysname
	        @delete_history = 1, -- bit
	        @delete_unused_schedule = 1-- bit

DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [DataLoad]    Script Date: 03/23/2010 17:27:01 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'DataLoad' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'DataLoad'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DataLoad: VehicleCatalog', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'DataLoad', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=@EmailOperator, 
		@job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Check if files are ready to load]    Script Date: 03/23/2010 17:27:01 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Check if files are ready to load', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=12, 
		@retry_attempts=23, 
		@retry_interval=5, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--  Is file "_NewChrome.txt" found?
SET NOCOUNT on
DECLARE @FileExists  int

EXECUTE master.dbo.xp_fileexist  ''\\INTDB01\INT_A\Datafeeds\Chrome\_NewChrome.txt'', @FileExists OUTPUT

IF @FileExists = 0
    RAISERROR (''"Flag file" indicating Chrome data files are available is not found'', 11, 1)
', 
		@database_name=N'master', 
		@output_file_name=N'\\INTDB01\INT_A\AuditLogs\DataLoadJobs\VehicleCatalog.log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Load Chrome VINMatch Staging]    Script Date: 03/23/2010 17:27:01 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Load Chrome VINMatch Staging', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=12, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\\INTDB01\INT_A\Packages\Dataloads\ChromeStaging\ImportChrome.VINMatch.All.dtsx" /CONFIGFILE "\\INTDB01\INT_A\Packages\Dataloads\ChromeStaging\ImportChrome.VINMatch.All.dtsConfig" /MAXCONCURRENT " -1 " /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@output_file_name=N'\\INTDB01\INT_A\AuditLogs\DataLoadJobs\VehicleCatalog.log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Load Chrome NVD Staging]    Script Date: 03/23/2010 17:27:01 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Load Chrome NVD Staging', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=12, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\\INTDB01\INT_A\Packages\Dataloads\ChromeStaging\ImportChrome.NVD.AllYears.dtsx" /CONFIGFILE "\\INTDB01\INT_A\Packages\Dataloads\ChromeStaging\ImportChrome.NVD.AllYears.dtsConfig" /MAXCONCURRENT " -1 " /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@output_file_name=N'\\INTDB01\INT_A\AuditLogs\DataLoadJobs\VehicleCatalog.log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Delete "new data" file]    Script Date: 03/23/2010 17:27:02 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Delete "new data" file', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=12, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--  Chrome data has been staged, delete "new data" flag file
SET NOCOUNT on
EXECUTE master.dbo.xp_cmdshell ''DEL  \\INTDB01\INT_A\Datafeeds\Chrome\_NewChrome.txt''
', 
		@database_name=N'master', 
		@output_file_name=N'\\INTDB01\INT_A\AuditLogs\DataLoadJobs\VehicleCatalog.log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Load Firstlook Schema in VehicleCatalog]    Script Date: 03/23/2010 17:27:02 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Load Firstlook Schema in VehicleCatalog', 
		@step_id=5, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=12, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\\INTDB01\INT_A\Packages\Dataloads\VehicleCatalog\LoadFirstlookSchemaFromChrome.dtsx" /CONFIGFILE "\\INTDB01\INT_A\Packages\Dataloads\VehicleCatalog\LoadFirstlookSchemaFromChrome.dtsConfig" /MAXCONCURRENT " -1 " /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@output_file_name=N'\\INTDB01\INT_A\AuditLogs\DataLoadJobs\VehicleCatalog.log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Load Categorization Schema from Firstlook Schema]    Script Date: 03/23/2010 17:27:02 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Load Categorization Schema from Firstlook Schema', 
		@step_id=6, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=12, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\\INTDB01\INT_A\Packages\Dataloads\VehicleCatalog\LoadCategorizationSchemaFromFirstLook.dtsx" /CONFIGFILE "\\INTDB01\INT_A\Packages\Dataloads\VehicleCatalog\LoadCategorizationSchemaFromFirstLook.dtsConfig" /MAXCONCURRENT " -1 " /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@output_file_name=N'\\INTDB01\INT_A\AuditLogs\DataLoadJobs\VehicleCatalog.log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Exec Pricing.Search#CorrectModelConfiguration]    Script Date: 03/23/2010 17:27:02 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Exec Pricing.Search#CorrectModelConfiguration', 
		@step_id=7, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=12, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC Pricing.Search#CorrectModelConfiguration  ', 
		@database_name=N'Market', 
		@output_file_name=N'\\INTDB01\INT_A\AuditLogs\DataLoadJobs\VehicleCatalog.log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Load IMT MakeModelGrouping and GroupingDescription tables]    Script Date: 03/23/2010 17:27:02 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Load IMT MakeModelGrouping and GroupingDescription tables', 
		@step_id=8, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=12, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXECUTE dbo.PopulateMakeModelGroupingDescriptionTables', 
		@database_name=N'IMT', 
		@output_file_name=N'\\INTDB01\INT_A\AuditLogs\DataLoadJobs\VehicleCatalog.log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Load HAL VehicleLine from IMT..MakeModelGrouping]    Script Date: 03/23/2010 17:27:02 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Load HAL VehicleLine from IMT..MakeModelGrouping', 
		@step_id=9, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=12, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC dbo.PopulateVehicleLine', 
		@database_name=N'HAL', 
		@output_file_name=N'\\INTDB01\INT_A\AuditLogs\DataLoadJobs\VehicleCatalog.log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Populate Vehicle Grouping Description Light table]    Script Date: 03/23/2010 17:27:02 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Populate Vehicle Grouping Description Light table', 
		@step_id=10, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=12, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC dbo.PopulateGroupingDescriptionLight', 
		@database_name=N'IMT', 
		@output_file_name=N'\\INTDB01\INT_A\AuditLogs\DataLoadJobs\VehicleCatalog.log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Timestamp and cycle audit logs (on success)]    Script Date: 03/23/2010 17:27:03 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Timestamp and cycle audit logs (on success)', 
		@step_id=11, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--  Timestamp audit log files, delete sufficiently aged ones
EXECUTE sp_FileTimestampAndCycle ''\\INTDB01\INT_A\AuditLogs\DataLoadJobs'', ''VehicleCatalog.log'', 35
', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Timestamp and cycle audit logs (on failure)]    Script Date: 03/23/2010 17:27:03 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Timestamp and cycle audit logs (on failure)', 
		@step_id=12, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--  Timestamp audit log files, delete sufficiently aged ones
EXECUTE sp_FileTimestampAndCycle ''\\INTDB01\INT_A\AuditLogs\DataLoadJobs'', ''VehicleCatalog.log'', 35
', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Ad-hoc', 
		@enabled=0, 
		@freq_type=1, 
		@freq_interval=0, 
		@freq_subday_type=0, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20100208, 
		@active_end_date=99991231, 
		@active_start_time=224500, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'On Friday, 10:45pm', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=64, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20071004, 
		@active_end_date=99991231, 
		@active_start_time=224500, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO