USE [msdb]
GO



IF EXISTS (	SELECT	1
		FROM	msdb.dbo.sysjobs S
		WHERE	s.NAME = 'DataLoad: Market Listing and Pricing Schemas'
		)
		
		EXEC msdb.dbo.sp_delete_job  @job_name = 'DataLoad: Market Listing and Pricing Schemas', @delete_history = 0, @delete_unused_schedule = 1
GO

DECLARE @notify_email_operator_name VARCHAR(128)

SET @notify_email_operator_name = NULL

/****** Object:  Job [DataLoad: Market Listing and Pricing Schemas]    Script Date: 01/27/2009 14:59:36 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [DataLoad]    Script Date: 01/27/2009 14:59:36 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'DataLoad' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'DataLoad'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'DataLoad: Market Listing and Pricing Schemas', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=2, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'DataLoad', 
		@owner_login_name=N'sa', 
		@notify_email_operator_name=@notify_email_operator_name, 
		@job_id = @jobId OUTPUT
		
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Market Listing Daily Process]    Script Date: 01/27/2009 14:59:37 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Market Listing Daily Process', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=6, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\\INTDB01\Packages\Dataloads\Market\MarketListing-DailyProcess.dtsx" /CONFIGFILE "\\INTDB01\Packages\Dataloads\Market\MarketListing-DailyProcess.dtsConfig" /MAXCONCURRENT " -1 " /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@output_file_name=N'\\INTDB01\AuditLogs\DataLoadJobs\MarketListingAndPricingSchemas.log', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Delete extraneous GetAuto files (temporary)]    Script Date: 01/27/2009 14:59:37 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Delete extraneous GetAuto files (temporary)', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=5, 
		@on_fail_action=3, 
		@on_fail_step_id=6, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'CmdExec', 
		@command=N'DEL \\INTDB01\Datafeeds\GetAuto\LINKS.txt', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Update Market Statistics and Recompile Pricing.LoadSearchResult_F_ByOwnerID]    Script Date: 01/27/2009 14:59:37 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Update Market Statistics and Recompile Pricing.LoadSearchResult_F_ByOwnerID', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=5, 
		@on_fail_action=4, 
		@on_fail_step_id=6, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC sp_updatestats
GO
EXEC sp_recompile ''Pricing.LoadSearchResult_F_ByOwnerID''
GO', 
		@database_name=N'Market', 
		@output_file_name=N'\\INTDB01\AuditLogs\DataLoadJobs\MarketListingAndPricingSchemas.log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Load Listing Fact and Pricing Aggregate Tables]    Script Date: 01/27/2009 14:59:37 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Load Listing Fact and Pricing Aggregate Tables', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=6, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\\INTDB01\Packages\DataLoads\Market\Load_ListingPricingFactTables.dtsx" /CONFIGFILE "\\INTDB01\Packages\DataLoads\Market\Load_ListingPricingFactTables.dtsConfig" /MAXCONCURRENT " -1 " /CHECKPOINTING OFF /REPORTING E', 
		@database_name=N'master', 
		@output_file_name=N'\\INTDB01\AuditLogs\DataLoadJobs\MarketListingAndPricingSchemas.log', 
		@flags=2
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Timestamp and cycle audit logs (on success)]    Script Date: 01/27/2009 14:59:37 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Timestamp and cycle audit logs (on success)', 
		@step_id=5, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'--  Timestamp audit log files ,delete sufficiently aged ones
EXECUTE sp_FileTimestampAndCycle ''\\INTDB01\AuditLogs\DataLoadJobs'', ''MarketListingAndPricingSchemas.log'', 7
', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Timestamp and cycle audit logs (on failure)]    Script Date: 01/27/2009 14:59:37 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Timestamp and cycle audit logs (on failure)', 
		@step_id=6, 
		@cmdexec_success_code=0, 
		@on_success_action=2, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'
--  Timestamp audit log files ,delete sufficiently aged ones
EXECUTE sp_FileTimestampAndCycle ''\\INTDB01\AuditLogs\DataLoadJobs'', ''MarketListingAndPricingSchemas.log'', 7', 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'Daily at 12:45am', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20071219, 
		@active_end_date=99991231, 
		@active_start_time=4500, 
		@active_end_time=235959
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:



 