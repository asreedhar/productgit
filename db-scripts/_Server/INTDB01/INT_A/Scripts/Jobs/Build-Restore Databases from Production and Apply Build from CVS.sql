
USE [msdb]

IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = 'Build: Restore Databases from Production and Apply Build from CVS')
	EXEC msdb.dbo.sp_delete_job
	        @job_name = 'Build: Restore Databases from Production and Apply Build from CVS', -- sysname
	        @delete_history = 1, -- bit
	        @delete_unused_schedule = 1-- bit
GO

/****** Object:  Job [Build: Restore Databases from Production and Apply Build from CVS]    Script Date: 02/24/2010 15:42:53 ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [Database Maintenance]    Script Date: 02/24/2010 15:42:53 ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'Database Maintenance' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'Database Maintenance'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'Build: Restore Databases from Production and Apply Build from CVS', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'Database Maintenance', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Run db-builder package to restore databases and apply scripts from CVS]    Script Date: 02/24/2010 15:42:54 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Run db-builder package to restore databases and apply scripts from CVS', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=N'/FILE "\\INTDB01\INT_A\Packages\DB-Builder\DB-Builder.dtsx" /CONFIGFILE "\\INTDB01\INT_A\Packages\db-builder\Build-INTDB01-INT_A.dtsConfig" /MAXCONCURRENT " -1 " /CHECKPOINTING OFF /SET "\Package.Variables[User::RunServerBuild].Properties[Value]";True /SET "\Package.Variables[User::RestoreDatabases].Properties[Value]";True /SET "\Package.Variables[User::GetProjectsFromCVS].Properties[Value]";True /SET "\Package.Variables[User::ApplyBuildScripts].Properties[Value]";True /REPORTING E', 
		@database_name=N'master', 
		@output_file_name=N'\\INTDB01\INT_A\AuditLogs\BuildJobs\Build-INTDB01-INT_A.Log', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Post-restore cleanup of IMT]    Script Date: 02/24/2010 15:42:54 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Post-restore cleanup of IMT', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=2, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'EXEC Deploy.PostRestoreCleanUp', 
		@database_name=N'Utility', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Load FLDW Sales and Inventory]    Script Date: 02/24/2010 15:42:54 ******/
--EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Load FLDW Sales and Inventory', 
--		@step_id=3, 
--		@cmdexec_success_code=0, 
--		@on_success_action=3, 
--		@on_success_step_id=0, 
--		@on_fail_action=2, 
--		@on_fail_step_id=0, 
--		@retry_attempts=0, 
--		@retry_interval=0, 
--		@os_run_priority=0, @subsystem=N'TSQL', 
--		@command=N'
--EXEC dbo.BuildWarehouseTableGroup
--	@WarehouseTableGroupID = 5', 
--		@database_name=N'FLDW', 
--		@flags=0
--IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Re run PING aggs]    Script Date: 02/24/2010 15:42:54 ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Re run PING aggs', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'
EXEC Utility.dbo.RunMarketPingAggregations', 
		@database_name=N'Utility', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
