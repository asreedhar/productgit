IF NOT EXISTS (	SELECT	1
		FROM	sys.server_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'Firstlook\DatabaseEngineers'
		)

	CREATE LOGIN [FIRSTLOOK\DatabaseEngineers] FROM WINDOWS WITH DEFAULT_DATABASE=[master]

GO


EXEC sp_map_exec

'USE [?]

IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = "WINDOWS_GROUP"
			AND name = "FIRSTLOOK\DatabaseEngineers"
		)
	CREATE USER [FIRSTLOOK\DatabaseEngineers] FOR LOGIN [FIRSTLOOK\DatabaseEngineers]
	 
EXEC sp_addrolemember N"db_owner", N"FIRSTLOOK\DatabaseEngineers"
',
'
SELECT	name
FROM	sys.databases
WHERE	name not in ("master","tempdb","model","msdb") and is_read_only = 0
'

GO

USE [msdb]

EXEC sp_addrolemember N'SQLAgentOperatorRole', N'FIRSTLOOK\DatabaseEngineers'
EXEC sp_addrolemember N'SQLAgentUserRole', N'FIRSTLOOK\DatabaseEngineers'
EXEC sp_addrolemember N'SQLAgentReaderRole', N'FIRSTLOOK\DatabaseEngineers'
GO

USE [master]
go
GRANT ALTER TRACE TO [Firstlook\DatabaseEngineers]
GO

