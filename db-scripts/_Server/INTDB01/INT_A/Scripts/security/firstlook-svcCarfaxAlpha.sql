
IF NOT EXISTS (	SELECT	1
		FROM	sys.server_principals
		WHERE	type_desc = 'WINDOWS_LOGIN'
			AND name = 'Firstlook\svcCarfaxAlpha'
		)

	CREATE LOGIN [FIRSTLOOK\svcCarfaxAlpha] FROM WINDOWS WITH DEFAULT_DATABASE=[IMT]
GO

USE IMT
GO
IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'WINDOWS_USER'
			AND name = 'Firstlook\svcCarfaxAlpha'
		)

	CREATE USER [Firstlook\svcCarfaxAlpha] FOR LOGIN [Firstlook\svcCarfaxAlpha] WITH DEFAULT_SCHEMA=[dbo]
GO

EXEC sp_addrolemember N'CarfaxProcessorService', N'Firstlook\svcCarfaxAlpha'
GO
USE VehicleCatalog
GO
IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'WINDOWS_USER'
			AND name = 'Firstlook\svcCarfaxAlpha'
		)

	CREATE USER [Firstlook\svcCarfaxAlpha] FOR LOGIN [Firstlook\svcCarfaxAlpha] WITH DEFAULT_SCHEMA=[dbo]
GO

EXEC sp_addrolemember N'CarfaxProcessorService', N'Firstlook\svcCarfaxAlpha'
GO
