IF NOT EXISTS (	SELECT	1
		FROM	sys.server_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'Firstlook\AlphaDatabaseAdmin'
		) BEGIN
		
	CREATE LOGIN [FIRSTLOOK\AlphaDatabaseAdmin] FROM WINDOWS WITH DEFAULT_DATABASE=[master]
	EXEC master..sp_addsrvrolemember @loginame = N'FIRSTLOOK\AlphaDatabaseAdmin', @rolename = N'sysadmin'
END	
GO
