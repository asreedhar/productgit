------------------------------------------------------------------------
--	CREATE THE APPLICATION LOGIN
------------------------------------------------------------------------



IF NOT EXISTS (	SELECT	1
		FROM	sys.server_principals
		WHERE	type_desc = 'SQL_LOGIN'
			AND name = 'firstlook'
		) 
		

	CREATE LOGIN firstlook WITH PASSWORD='k*@nb(@$1x', DEFAULT_DATABASE=IMT


GO


------------------------------------------------------------------------
--	FIX THE LOGIN MAPPINGS SINCE THE DBs ARE PROD CUTS
------------------------------------------------------------------------

EXEC sp_map_exec

'USE [?]

EXEC sp_change_users_login @Action = "Auto_Fix", @UserNamePattern = "firstlook"
',
'
SELECT	name
FROM	sys.databases
WHERE	name not in ("master","tempdb","model","msdb") and is_read_only = 0
'

GO
