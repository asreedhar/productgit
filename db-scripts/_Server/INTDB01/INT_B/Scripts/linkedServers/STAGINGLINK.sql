--
-- Create Linked Server on 01 and point to 03 in the same environment
--	Note: There are differences for the INT_? environments (use ALPHADB03)
--
DECLARE @SRV VARCHAR(50)
DECLARE @DataSource VARCHAR(50)
DECLARE @SQL VARCHAR(2000)

SET @SRV='STAGINGLINK'
SET @DataSource='ALPHADB03' -- Default Value

IF @@SERVERNAME LIKE 'INTDB01%'
	SET @DataSource='ALPHADB03'

IF @@SERVERNAME = 'BETADB01'
	SET @DataSource='BETADB03'

IF @@SERVERNAME = 'PRODDB01SQL'
	SET @DataSource='PRODDB03SQL'


IF  NOT EXISTS (SELECT srv.name FROM sys.servers srv WHERE srv.server_id != 0 AND srv.name = @SRV)
BEGIN
	EXEC master.dbo.sp_addlinkedserver @server = @SRV, @srvproduct=@DataSource, @provider=N'SQLNCLI', @datasrc=@DataSource, @catalog=N'Staging'
	
	EXEC master.dbo.sp_serveroption @server=@SRV, @optname=N'collation compatible', @optvalue=N'true'
	EXEC master.dbo.sp_serveroption @server=@SRV, @optname=N'data access', @optvalue=N'true'
	EXEC master.dbo.sp_serveroption @server=@SRV, @optname=N'dist', @optvalue=N'false'
	EXEC master.dbo.sp_serveroption @server=@SRV, @optname=N'pub', @optvalue=N'false'
	EXEC master.dbo.sp_serveroption @server=@SRV, @optname=N'rpc', @optvalue=N'true'
	EXEC master.dbo.sp_serveroption @server=@SRV, @optname=N'rpc out', @optvalue=N'true'
	EXEC master.dbo.sp_serveroption @server=@SRV, @optname=N'sub', @optvalue=N'false'
	EXEC master.dbo.sp_serveroption @server=@SRV, @optname=N'connect timeout', @optvalue=N'0'
	EXEC master.dbo.sp_serveroption @server=@SRV, @optname=N'collation name', @optvalue=null
	EXEC master.dbo.sp_serveroption @server=@SRV, @optname=N'lazy schema validation', @optvalue=N'false'
	EXEC master.dbo.sp_serveroption @server=@SRV, @optname=N'query timeout', @optvalue=N'0'
	EXEC master.dbo.sp_serveroption @server=@SRV, @optname=N'use remote collation', @optvalue=N'true'
END

--
-- Create Server Impersonation
--
DECLARE @PWD VARCHAR(50)
SET @PWD='readme'

IF @@SERVERNAME='BETADB03'
	SET @PWD=@PWD+'B3ta'

IF @@SERVERNAME='PRODDB03SQL'
	SET @PWD=@PWD+'Pr0d'

EXEC master.dbo.sp_addlinkedsrvlogin @rmtsrvname=@SRV,@useself=N'False',@locallogin=NULL,@rmtuser=N'StagingReader',@rmtpassword=@PWD
