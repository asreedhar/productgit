USE [msdb]
GO

DECLARE @ReturnCode INT
DECLARE @CategoryName VARCHAR(50)
DECLARE @JobName VARCHAR(50)
DECLARE @JobDescription VARCHAR(100)
DECLARE @OwnerLoginName VARCHAR(20)

DECLARE @Step1Name VARCHAR(50)
DECLARE @Step2Name VARCHAR(50)
DECLARE @Step3Name VARCHAR(50)
 
DECLARE @DBBuilderPath VARCHAR(50)
DECLARE @DBBuilderPackagePath VARCHAR(100)
DECLARE @DBBuilderConfigPath VARCHAR(100)
DECLARE @Step1CommandText VARCHAR(500)
DECLARE @Step2CommandText VARCHAR(500)
DECLARE @Step3CommandText VARCHAR(500)
 
SET @CategoryName ='Build'
SET @JobName= 'Build 2.0: Restore & Build Mode'
SET @JobDescription = 'Run the DB-Builder 2.0 package to restore and patch databases.'
SET @OwnerLoginName ='sa'

SET @Step1Name ='Disable all non-builder jobs.'
SET @Step2Name ='Run DB-Builder 2.0 package.'
SET @Step3Name ='Reenable previously disabled jobs.'

SET @DBBuilderPath ='C:\db-builder\DbBuilder2.0\Int_B'
SET @DBBuilderPackagePath= @DBBuilderPath+'\bin\DB-Builder.dtsx'
SET @DBBuilderConfigPath=@DBBuilderPath+'\IntB-Restore-And-Build.dtsConfig'

SET @Step1CommandText='IF EXISTS (SELECT 1 	FROM Utility.INFORMATION_SCHEMA.Tables	WHERE Table_Name =''DisabledJobs'' AND Table_Schema =''Deploy'') DROP TABLE Utility.Deploy.DisabledJobs
	
SELECT 	name 
INTO 	Utility.Deploy.DisabledJobs 
FROM 	msdb.dbo.sysjobs S 
WHERE enabled = 1 and name not like ''Build 2.0%''


EXEC sp_map_exec ''EXEC sp_update_job @job_name = "?", @enabled = 0'',
                    ''select name FROM     Utility.Deploy.DisabledJobs'',
                    0'
                    
SET @Step2CommandText='/FILE "' +@DBBuilderPackagePath +  + '"'+ ' /CONFIGFILE ' +'"'+ @DBBuilderConfigPath + '"' + ' /MAXCONCURRENT " -1 " /CHECKPOINTING OFF /REPORTING E'  
SET @Step3CommandText='EXEC sp_map_exec ''EXEC sp_update_job @job_name = "?", @enabled = 1'',
                    ''select name FROM     Utility.Deploy.DisabledJobs'',
                    0
                    
                    DROP TABLE Utility.Deploy.DisabledJobs'

SELECT @ReturnCode = 0
BEGIN TRANSACTION

IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = @JobName)
	EXEC msdb.dbo.sp_delete_job
	        @job_name = @JobName, -- sysname
	        @delete_history = 0, -- bit
	        @delete_unused_schedule = 1-- bit

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=@CategoryName AND category_class=1)
	BEGIN
	EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=@CategoryName
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
	END


DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@JobName, 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=@JobDescription, 
		@category_name=@CategoryName, 
		@owner_login_name=@OwnerLoginName, @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

		EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=@Step1Name, 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=3, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=@Step1CommandText, 
		@database_name=N'master', 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=@Step2Name, 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=4, 
		@on_fail_step_id=3, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'SSIS', 
		@command=@Step2CommandText,
		@database_name=N'master', 
		@flags=0
		
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=@Step3Name, 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=@Step3CommandText, 
		@database_name=N'master', 
		@flags=0
 
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:
