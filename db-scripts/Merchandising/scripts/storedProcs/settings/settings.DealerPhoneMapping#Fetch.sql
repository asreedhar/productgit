IF OBJECT_ID('settings.DealerPhoneMapping#Fetch') IS NOT NULL
	DROP PROCEDURE [settings].[DealerPhoneMapping#Fetch]
GO
/* --------------------------------------------------------------------
 *
 * Summary
 * -------
 * 
 *   Gets the phone mapping for the specified business unit
 * 
 * Parameters
 * ----------
 * 
 *   @BusinessUnitId INT
 * 
 * Exceptions
 * ----------
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [settings].[DealerPhoneMapping#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT	DPM.*
	FROM	[settings].[DealerPhoneMapping] DPM
	WHERE	DPM.[BusinessUnitId] = @BusinessUnitId
	
END
GO

GRANT EXECUTE ON [settings].[DealerPhoneMapping#Fetch] TO MerchandisingUser
GO

