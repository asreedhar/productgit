if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[equipmentDisplayDescription#Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[equipmentDisplayDescription#Update]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_equipmentDisplayDescription_Update.sql,v 1.1 2009/01/05 21:23:51 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Update the equipment Description for a dealer
 * 
 * 
 * Parameters
 * ----------
 * 
 * @RankingID int   - integer id of the ranking item
 * @DisplayDescription varchar - the description to be used in published materials
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[equipmentDisplayDescription#Update]
	-- Add the parameters for the stored procedure here
	@RankingID int,
	@DisplayDescription varchar(80)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE settings.EquipmentDisplayRankings
    SET displayDescription = @DisplayDescription
    WHERE rankingID = @RankingID                

END
GO

GRANT EXECUTE ON [Settings].[equipmentDisplayDescription#Update] TO MerchandisingUser 
GO