USE [Merchandising]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Boucher
-- Create date: 11/14/2011
-- Description: settings.AutoApprove#Upsert
--				Updates or inserts auto approve settings by business unit ID
-- =============================================
IF object_id('settings.AutoApprove#Upsert') IS NOT NULL
	DROP PROCEDURE [settings].[AutoApprove#Upsert]
GO
CREATE PROCEDURE [settings].[AutoApprove#Upsert]

	-- Parameters
	@BusinessUnitId int,
	@FirstName VARCHAR(32),
	@LastName VARCHAR(32),
	@Email VARCHAR(128),
	@Status int,
	@UserName VARCHAR(32),
	@Bounced bit

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS (select 1
		FROM settings.AutoApprove
		WHERE 
			businessUnitId = @BusinessUnitId)
	BEGIN
		UPDATE settings.AutoApprove
		SET
			[FirstName] = @FirstName,
			[LastName] = @LastName,			
			[Email] = @Email,
			[Status] = @Status,
			[UpdatedOn] = CURRENT_TIMESTAMP,
			[UpdatedBy] = @UserName,
			[EmailBounced] = @Bounced
		WHERE
			businessUnitId = @BusinessUnitId
	END
	ELSE
	BEGIN
		INSERT INTO settings.AutoApprove
		(
			[businessUnitId],
			[FirstName],
			[LastName],
			[Email],
			[Status],
			[CreatedOn],
			[CreatedBy],
			[EmailBounced]
		)
		VALUES
		(	
			@BusinessUnitId,
			@FirstName,
			@LastName,
			@Email,
			@Status,
			CURRENT_TIMESTAMP,
			@UserName,
			@Bounced
		)
	END

END
GO
GRANT EXECUTE ON [settings].[AutoApprove#Upsert] TO MerchandisingUser;
GO