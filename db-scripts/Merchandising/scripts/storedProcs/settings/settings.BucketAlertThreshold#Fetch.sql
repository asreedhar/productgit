IF object_id('settings.BucketAlertThreshold#Fetch') is not null
	DROP PROCEDURE settings.BucketAlertThreshold#Fetch
GO


CREATE PROCEDURE settings.BucketAlertThreshold#Fetch
	@BusinessUnitId INT
AS
begin

	set nocount on

	declare @invCount int;
	select @invCount = count(inventoryId) from workflow.Inventory where BusinessUnitID = @BusinessUnitId;


	with Defaults as 
	(
		select BusinessUnitId = @BusinessUnitId, Bucket, active, Limit, LimitTypeId, emailActive -- FB: 27993 
		from Merchandising.settings.BucketAlertThresholdDefault
	)
	select 
		Id = coalesce(bat.id, -1), 
		BusinessUnitId = @BusinessUnitId, 
		Bucket = coalesce(bat.Bucket,def.Bucket),
		Active = coalesce(bat.Active, def.active), 
		Limit = coalesce(
			bat.Limit,
			case
				when def.LimitTypeId = 1 then def.Limit
				when def.LimitTypeId = 2 then cast(@invCount * cast(def.Limit as real)/100 as int)
			end
		),
		EmailActive = coalesce(bat.EmailActive, def.EmailActive)
	from Defaults def
	full join Merchandising.settings.BucketAlertsThreshold bat
		on def.BusinessUnitId = bat.BusinessUnitId 
		and bat.Bucket = def.Bucket
	where bat.BusinessUnitId = @BusinessUnitId or def.BusinessUnitId = @BusinessUnitId;


END
GO

grant execute on settings.BucketAlertThreshold#Fetch to MerchandisingUser
GO
