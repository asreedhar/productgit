if object_id('settings.GoogleAnalyticsProfile#Delete') is not null
	drop procedure settings.GoogleAnalyticsProfile#Delete
go

create procedure settings.GoogleAnalyticsProfile#Delete
	@businessUnitId int,
	@type int
as

set nocount on
set transaction isolation level read uncommitted

delete from Merchandising.settings.GoogleAnalyticsProfile
where BusinessUnitId = @businessUnitId and Type = @type

go

grant execute on settings.GoogleAnalyticsProfile#Delete to MerchandisingUser
go
