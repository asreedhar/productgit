use Merchandising
go
if object_id('settings.Merchandising_ShowGroupLevelDashboard#Update') is not null
	drop procedure settings.Merchandising_ShowGroupLevelDashboard#Update
go


create procedure settings.Merchandising_ShowGroupLevelDashboard#Update
	@businessUnitID int,
	@value bit
as

set nocount on
set transaction isolation level read uncommitted


update s
set s.ShowGroupLevelDashboard = @value
from (select parentId
	from imt.dbo.businessunitrelationship
	where businessunitid = @businessUnitID) p
join imt.dbo.businessunitrelationship br on p.parentID = br.parentID
join merchandising.settings.merchandising s on s.businessUnitid = br.businessunitid

go

grant execute on settings.Merchandising_ShowGroupLevelDashboard#Update to MerchandisingUser
go

