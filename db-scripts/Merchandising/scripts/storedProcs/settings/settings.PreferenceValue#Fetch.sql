IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[settings].[PreferenceValue#Fetch]')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE [settings].[PreferenceValue#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [settings].[PreferenceValue#Fetch]
    @BusinessUnitId INT ,
    @PreferenceId INT,
    @PreferenceValue SQL_VARIANT OUTPUT
AS 
    BEGIN

        SET NOCOUNT ON		

	/* 
    	This stored proc should be considered a stub for a more robust preference management system (in the works)
    	- dh, 2011-06-08
	*/


	SELECT @PreferenceValue = 
	CASE WHEN @PreferenceId = 1 -- Reporting Preference: Minimum Age in Days for Vehicle Status Report
	    THEN 7 -- all dealers get 7
	ELSE NULL	    
	END
	FROM imt.dbo.BusinessUnit WHERE BusinessUnitID = @BusinessUnitId
	
	RETURN 
    END
GO

GRANT EXECUTE ON [settings].[PreferenceValue#Fetch] TO MerchandisingUser 
GO

