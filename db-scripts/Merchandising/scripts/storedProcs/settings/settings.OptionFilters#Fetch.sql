if exists (select * 
				from dbo.sysobjects 
				where id = object_id(N'[settings].[OptionFilters#Fetch]') 
				and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [settings].[OptionFilters#Fetch]
GO

CREATE PROCEDURE [settings].[OptionFilters#Fetch]
	@ManufactureId int
	
AS
BEGIN

	-- return minimum MSRP
	SELECT MinimumMsrp
		FROM settings.OptionFilter
		WHERE ManufactureId = @ManufactureId

	-- return include phrases
	SELECT IncludePhrase
		FROM settings.OptionFilterIncludePhrase
		WHERE ManufactureId = @ManufactureId
		ORDER BY IncludePhrase
	
	-- reutrn exclude phrases
	SELECT ExcludePhrase
		FROM settings.OptionFilterExcludePhrase
		WHERE ManufactureId = @ManufactureId
		ORDER BY ExcludePhrase


END
GO

GRANT EXECUTE ON [settings].[OptionFilters#Fetch] TO MerchandisingUser
GO
