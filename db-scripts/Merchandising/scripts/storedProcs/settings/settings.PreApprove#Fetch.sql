USE [Merchandising]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Boucher
-- Create date: 11/14/2011
-- Description: settings.PreApprove#Fetch
--				Gets 'AutoApprove' settings by business unit ID
-- =============================================
IF object_id('settings.PreApprove#Fetch') IS NOT NULL
	DROP PROCEDURE [settings].[PreApprove#Fetch]
GO
