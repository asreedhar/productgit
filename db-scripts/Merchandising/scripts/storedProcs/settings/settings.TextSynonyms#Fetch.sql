IF OBJECT_ID('settings.TextSynonyms#Fetch') IS NOT NULL
	DROP PROCEDURE [settings].[TextSynonyms#Fetch]
GO

CREATE PROCEDURE [settings].[TextSynonyms#Fetch]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT TextValue, TextSynonym
	FROM	settings.TextSynonyms

GO

GRANT EXECUTE ON [settings].[TextSynonyms#Fetch] TO [MerchandisingUser]
GO
