if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[CategoryReplacement#Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[CategoryReplacement#Delete]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/08/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_categoryReplacement_delete.sql,v 1.1 2009/07/27 23:55:41 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * delete a category replacement
 * 
 * 
 * Parameters
 * ----------
 * 
 * @Name varchar(50)
 * @ReplacementType int --1 system, 2 dealer
 * @BusinessUnitId int
 * @OverridesReplacementId int = NULL ...only provide value here whenever overriding a system replacement
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[CategoryReplacement#Delete]
	-- Add the parameters for the stored procedure here
	@ReplacementId int,
	@BusinessUnitId int,
	@MemberLogin varchar(30)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--only delete those that were dealer replacements
	IF EXISTS (SELECT 1 
		FROM settings.CategoryReplacement cr
		JOIN settings.CategoryReplacement_Dealer crd
			ON crd.replacementId = cr.replacementId
			WHERE cr.replacementId = @ReplacementId 
				AND cr.replacementType = 2
				AND crd.businessUnitId = @BusinessUnitId)
	BEGIN
		--create an active category replacement...
		DELETE FROM settings.CategoryReplacement_Item WHERE replacementId = @ReplacementId
		
		DELETE FROM settings.CategoryReplacement_Dealer WHERE replacementId = @ReplacementId
		
		--should this actually remove all categoryPlacements that were overrides for this one as well?  probably not as they were deliberately customized to replace something that is now being deleted
		DELETE FROM settings.CategoryReplacement_DealerOverride WHERE overrideForReplacementId = @ReplacementId OR overrideWithReplacementId = @ReplacementId	
		
		DELETE FROM settings.CategoryReplacement WHERE replacementId = @ReplacementId
	END
	ELSE IF EXISTS (SELECT 1 FROM settings.CategoryReplacement cr WHERE cr.replacementId = @ReplacementId) --otherwise, we can try to override it
	BEGIN
		declare @substituteId int
		exec settings.CategoryReplacement#Insert @Name='', @BusinessUnitId=@BusinessUnitId, @ReplacementType=2, @OverridesReplacementId=@ReplacementId, @MemberLogin=@MemberLogin, @ReplacementId=@substituteId output
	END
END

GO

GRANT EXECUTE ON [Settings].[CategoryReplacement#Delete] TO MerchandisingUser 
GO