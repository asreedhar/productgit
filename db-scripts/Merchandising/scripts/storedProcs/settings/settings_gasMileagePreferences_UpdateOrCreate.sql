if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[gasMileagePreferences#UpdateOrCreate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[gasMileagePreferences#UpdateOrCreate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/08/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_gasMileagePreferences_UpdateOrCreate.sql,v 1.1 2008/12/09 18:24:16 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * creates a new vehicle metric entry or updates the existing entry
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId int   - integer id of the businesUnit for which to get the alert list
	@segmentId int
	@goodCityGasMileage int,
	@goodHighwayGasMileage int,
	
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[gasMileagePreferences#UpdateOrCreate]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@segmentId int,
	@goodCityGasMileage int,
	@goodHighwayGasMileage int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 from settings.GasMileagePreferences
			where businessUnitId = @BusinessUnitId AND segmentId = @segmentId)
		BEGIN
			UPDATE settings.gasMileagePreferences
				SET 
					goodHighwayGasMileage = @goodHighwayGasMileage, 
					goodCityGasMileage = @goodCityGasMileage
				WHERE businessUnitId = @BusinessUnitId
				AND segmentId = @segmentId
		END
	ELSE
		BEGIN
			INSERT INTO settings.GasMileagePreferences
				(
				businessUnitId, 
				segmentId, 
				goodHighwayGasMileage, 
				goodCityGasMileage
				)
			VALUES
			(
				@BusinessUnitId, 
				@segmentId, 
				@goodHighwayGasMileage, 
				@goodCityGasMileage
			)
		END

END

GO

GRANT EXECUTE ON [Settings].[gasMileagePreferences#UpdateOrCreate] TO MerchandisingUser 
GO