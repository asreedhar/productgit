use Merchandising
go

if object_id('settings.Environment#Fetch') is not null
	drop procedure settings.Environment#Fetch
go


create procedure settings.Environment#Fetch
	@SettingName varchar(100)
as

set nocount on
set transaction isolation level read uncommitted

select SettingValue
from Merchandising.settings.Environment
where SettingName = @SettingName

go

grant execute on settings.Environment#Fetch to MerchandisingUser
go
