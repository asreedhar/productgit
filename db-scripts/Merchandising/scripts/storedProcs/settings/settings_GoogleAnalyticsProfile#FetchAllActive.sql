
/****** Object:  StoredProcedure [settings].[GoogleAnalyticsProfile#FetchAllActive]    Script Date: 05/29/2013 09:29:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[settings].[GoogleAnalyticsProfile#FetchAllActive]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [settings].[GoogleAnalyticsProfile#FetchAllActive]
GO

/****** Object:  StoredProcedure [settings].[GoogleAnalyticsProfile#FetchAllActive]    Script Date: 05/29/2013 09:29:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-05-29
-- Description:	Get all the active GA profiles
-- =============================================
CREATE PROCEDURE [settings].[GoogleAnalyticsProfile#FetchAllActive] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT gap.BusinessUnitId, gap.Type, gap.ProfileId, gap.ProfileName, gap.DestinationId 
	FROM Merchandising.settings.GoogleAnalyticsProfile gap 
	JOIN Merchandising.settings.GoogleAnalyticsToken gat ON gat.BusinessUnitId = gap.BusinessUnitId
	JOIN IMT.dbo.BusinessUnit bu ON gap.BusinessUnitId = bu.BusinessUnitID
	WHERE gat.Active = 1 and bu.Active = 1
 
END

GO

grant execute on [settings].[GoogleAnalyticsProfile#FetchAllActive] to MerchandisingUser
GO

