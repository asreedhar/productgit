if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[CategoryReplacements#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[CategoryReplacements#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/08/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_categoryReplacements_fetch.sql,v 1.1 2009/07/27 23:55:41 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch the category replacements
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId int   - integer id of the businesUnit for which to get list

 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[CategoryReplacements#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
declare @replacements table (
	replacementId int,
	name varchar(50),
	replacementType int
)
insert into @replacements (replacementId, name, replacementType)
select 
	DISTINCT
	cr.replacementId,
	cr.name,
	cr.replacementType
from settings.categoryReplacement cr
JOIN settings.categoryReplacement_Item cri
ON cri.replacementId = cr.replacementId
LEFT JOIN settings.categoryReplacement_Dealer crd
	ON crd.replacementId = cr.replacementId
	
LEFT JOIN settings.categoryReplacement_DealerOverride cro
	ON cro.overrideForReplacementId = cr.replacementId
	and cro.businessUnitId = @BusinessUnitId
--LEFT JOIN settings.categoryReplacement cr2  --join back the override
--	ON cro.overrideWithReplacementId = cr2.replacementId
WHERE (cr.replacementType = 1 OR crd.businessUnitId = @BusinessUnitId)
AND cr.active = 1  --either there is no replacement and the primary is active...OR the replacement is active
AND cro.overrideForReplacementId IS NULL -- we don't take overriden ones...

select * from @replacements	
	
SELECT cri.itemId, cri.replacementId, cri.categoryId, ct.userFriendlyName
FROM settings.categoryReplacement_Item cri
JOIN vehicleCatalog.chrome.categories ct
ON ct.categoryId = cri.categoryId
AND ct.countryCode = 1
where replacementId IN (select replacementId FROM @replacements)
	
END

GO

GRANT EXECUTE ON [Settings].[CategoryReplacements#Fetch] TO MerchandisingUser 
GO