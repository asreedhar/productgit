if object_id('settings.GroupLevelDashboardActive#Fetch') is not null
	drop procedure settings.GroupLevelDashboardActive#Fetch
go

create procedure settings.GroupLevelDashboardActive#Fetch
	
as

set nocount on
set transaction isolation level read uncommitted

select distinct parentid
from merchandising.settings.merchandising s
join imt.dbo.businessunitrelationship br on br.businessunitid = s.businessunitid
where ShowGroupLevelDashboard = 1

go

grant execute on settings.GroupLevelDashboardActive#Fetch to MerchandisingUser
go
