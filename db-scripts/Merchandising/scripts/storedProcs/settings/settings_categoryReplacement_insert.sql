if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[CategoryReplacement#Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[CategoryReplacement#Insert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/08/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_categoryReplacement_insert.sql,v 1.1 2009/07/27 23:55:41 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * insert a new category replacement
 * 
 * 
 * Parameters
 * ----------
 * 
 * @Name varchar(50)
 * @ReplacementType int --1 system, 2 dealer
 * @BusinessUnitId int
 * @OverridesReplacementId int = NULL ...only provide value here whenever overriding a system replacement
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[CategoryReplacement#Insert]
	-- Add the parameters for the stored procedure here
	
	@Name varchar(50),
	@BusinessUnitId int = NULL,
	@ReplacementType int = 2,
	@OverridesReplacementId int = NULL,
	@MemberLogin varchar(30) = '',
	@ReplacementId int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--create an active category replacement...
	--if it already exists AND we are NOT overriding
	if EXISTS (select 1 FROM settings.CategoryReplacement 
		WHERE replacementId = @ReplacementId
		AND replacementType = 2) --where dealer
	BEGIN
		UPDATE cr
			SET cr.Name = @Name,
				cr.lastUpdatedBy = @MemberLogin,
				cr.lastUpdatedOn = getdate()
		FROM settings.CategoryReplacement cr
		JOIN settings.CategoryReplacement_Dealer crd
			ON crd.replacementId = cr.replacementId
		WHERE cr.replacementId = @ReplacementId
		--as long as a row was updated, the replacement id should be correct...
	END
	ELSE
	BEGIN
		--insert a new one if it didn't already exist *OR* we are overriding a system one
		INSERT INTO 
			settings.CategoryReplacement
		(
			[replacementType],
			[name],
			[active],
			[createdBy],
			[createdOn],
			[lastUpdatedBy],
			[lastUpdatedOn]	
		) 
		VALUES (
			@ReplacementType, 
			@Name,
			1,
			@MemberLogin,
			getdate(),
			@MemberLogin,
			getdate()	
		)
		IF @@ROWCOUNT > 0
			SELECT @ReplacementId = SCOPE_IDENTITY()
		
		IF @BusinessUnitId IS NOT NULL
		BEGIN
			exec settings.CategoryReplacement#Assign @BusinessUnitID=@BusinessUnitId, @ReplacementId=@@IDENTITY, @MemberLogin=@MemberLogin
		END
	
		--if we are overriding with this item...and it was created/updated successfully
		IF (@OverridesReplacementId IS NOT NULL AND @@ROWCOUNT > 0) 
		BEGIN		
			
			INSERT INTO settings.CategoryReplacement_DealerOverride
			(
				[overrideForReplacementId],
				[overrideWithReplacementId],
				[businessUnitId],
				[createdBy],
				[createdOn],
				[lastUpdatedBy],
				[lastUpdatedOn]
			)
			SELECT
				@OverridesReplacementId,
				@ReplacementId,
				@BusinessUnitId,
				@MemberLogin,
				getdate(),
				@MemberLogin,
				getdate()					
		
		END
	END
	
END

GO

GRANT EXECUTE ON [Settings].[CategoryReplacement#Insert] TO MerchandisingUser 
GO