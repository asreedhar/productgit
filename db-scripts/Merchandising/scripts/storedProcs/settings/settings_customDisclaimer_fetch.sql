if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[CustomDisclaimer#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[CustomDisclaimer#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sombke, Bryant
-- Create date: <04/27/2010>
-- =============================================
/* --------------------------------------------------------------------
 *
 * Summary
 * -------
 * 
 *   Fetches the custom disclaimer for the specified business unit
 * 
 * Parameters
 * ----------
 * 
 *   @BusinessUnitId int
 * 
 * Exceptions
 * ----------
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[CustomDisclaimer#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		SELECT businessUnitId, isAppend, customText
		FROM settings.CustomDisclaimer
		WHERE businessUnitId = @BusinessUnitId
		
END

GO

GRANT EXECUTE ON [Settings].[CustomDisclaimer#Fetch] TO MerchandisingUser
GO 