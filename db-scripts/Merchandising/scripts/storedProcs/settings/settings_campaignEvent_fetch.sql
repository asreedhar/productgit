if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[campaignEvent#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [settings].[campaignEvent#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <2/5/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_campaignEvent_fetch.sql,v 1.1 2009/11/09 17:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the model marketing for a vehicle
 * 
 * 
 * Parameters
 * ----------
 *  @ChromeStyleId - vehicle chrome style id
 
 *  -marketingType: 1 - Intro Text; 2 - Vehicle Tagline; 3 - Model Awards
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [settings].[campaignEvent#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@CampaignEventId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
		
	select ce.*, 
		ISNULL(thm.name,'') as themeName, 
		ISNULL(adt.name,'') as templateName
	FROM settings.CampaignEvents ce
	LEFT JOIN templates.themes thm
		ON thm.id = ce.themeId
		AND (thm.businessUnitId = 100150 OR thm.businessUnitId = @BusinessUnitId)
	LEFT JOIN templates.adtemplates adt
		ON adt.templateID = ce.templateId
		AND (adt.businessUnitId = 100150 OR adt.businessUnitId = @BusinessUnitId)
	WHERE ce.businessUnitId = @BusinessUnitId	
	and campaignEventId = @CampaignEventId
	
END
GO

GRANT EXECUTE ON [settings].[campaignEvent#Fetch] TO MerchandisingUser 
GO