if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[createDealerTemplates]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[createDealerTemplates]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/08/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_createDealerTemplates.sql,v 1.3 2009/08/19 21:27:28 jkelley Exp $$
 * 
 * Summary
 * -------
 * 
 * creates a dealer's templates in the system
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId int   - integer id of the businesUnit for which to get the alert list
	
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[createDealerTemplates]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
IF NOT EXISTS(select 1 from templates.adTemplates where businessUnitId = @BusinessUnitID)
BEGIN
--get all templateIDs           
declare @templateIDs table (
	rowId int IDENTITY(1,1),
	templateId int
)           
insert into @templateIDs (templateId)
SELECT
	[templateId]
FROM [templates].[AdTemplates]
WHERE businessUnitId = 100150
and active = 1



declare @ct int, @i int, @currId int, @insId int

--set defaults
select @i=1, @ct = count(*) from @templateIDs

while @i <= @ct
BEGIN
	select @currId = templateId FROM @templateIDs
	WHERE rowId = @i
	
	INSERT INTO [templates].[AdTemplates]
	([name],[businessUnitId],vehicleProfileID,parent)
	SELECT [name], @BusinessUnitId, vehicleProfileID, parent
	FROM [templates].[AdTemplates]
	WHERE templateId = @currId
	
	select @insId = SCOPE_IDENTITY()

	INSERT INTO [templates].[AdTemplateItems]
	(templateID, blurbId, [sequence],priority, required, parentID)
	SELECT @insId, blurbId, [sequence],priority,required,parentID
	FROM [templates].[AdTemplateItems]
	WHERE templateId = @currId
	
	select @i = @i + 1
END --end while
END --end if exists

END --end proc code block

GO

GRANT EXECUTE ON [Settings].[createDealerTemplates] TO MerchandisingUser 
GO