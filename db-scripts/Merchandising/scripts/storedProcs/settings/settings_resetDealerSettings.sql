if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[resetDealerSettings]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[resetDealerSettings]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/08/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_resetDealerSettings.sql,v 1.1 2009/02/10 23:55:08 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * resets the dealer to the default firstlook settings in the system
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId int   - integer id of the businesUnit for which to get the alert list
	
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[resetDealerSettings]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
DELETE FROM [Merchandising].[settings].[Merchandising] WHERE businessUnitId = @BusinessUnitId
DELETE FROM [Merchandising].[settings].[MerchandisingDescriptionPreferences] WHERE businessUnitId = @BusinessUnitId
DELETE FROM [Merchandising].[settings].[RequiredPhotos] WHERE businessUnitId = @BusinessUnitId
DELETE FROM [Merchandising].[settings].[GasMileagePreferences] WHERE businessUnitId = @BusinessUnitId
DELETE FROM [Merchandising].[settings].[EquipmentDisplayRankings] WHERE businessUnitId = @BusinessUnitId
DELETE FROM [Merchandising].[builder].[VehicleConditionAdjectives] WHERE businessUnitId = @BusinessUnitId
DELETE FROM [Merchandising].[builder].[AdditionalInfoCategories] WHERE businessUnitId = @BusinessUnitId

----CLEAR DESTINATIONS?----

exec settings.createDealer @BusinessUnitId
    

END

GO

GRANT EXECUTE ON [Settings].[resetDealerSettings] TO MerchandisingUser 
GO