if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[CategoryReplacementItem#Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[CategoryReplacementItem#Insert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/08/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_categoryReplacementItem_insert.sql,v 1.1 2009/07/27 23:55:41 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch the category replacements
 * 
 * 
 * Parameters
 * ----------
 * 
 * @ReplacementId int
 * @CategoryId int --chrome categoryId to include in replacement
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[CategoryReplacementItem#Insert]
	-- Add the parameters for the stored procedure here
	@ReplacementId int,
	@CategoryId int,
	@MemberLogin varchar(30)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

IF NOT EXISTS (  --don't duplicate
	SELECT 1 
	FROM 
		settings.categoryReplacement_Item
	WHERE 
		replacementId = @ReplacementId 
		AND categoryId = @CategoryId
)
BEGIN	
	INSERT INTO 
		settings.categoryReplacement_Item
	(
		replacementId,
		categoryId,
		[createdBy],
		[createdOn],
		[lastUpdatedBy],
		[lastUpdatedOn]
	) 
	VALUES (
		@ReplacementId, 
		@CategoryId,
		@MemberLogin,
		getdate(),
		@MemberLogin,
		getdate()
	)
END


	
END

GO

GRANT EXECUTE ON [Settings].[CategoryReplacementItem#Insert] TO MerchandisingUser 
GO