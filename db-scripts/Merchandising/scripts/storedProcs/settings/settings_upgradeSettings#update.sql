if object_id('settings.upgradeSettings#Update') is not null
	drop procedure settings.upgradeSettings#Update
go

CREATE PROCEDURE settings.upgradeSettings#Update
	@businessUnitID int,
	@maxForSmartphone bit = NULL,
	@autoWindowStickerTemplateId int = NULL,
	@MAXForWebsite20 BIT = NULL,
	@MaxDigitalShowroom BIT = NULL,
	@UsePhoneMapping BIT = NULL,
	@UseDealerRater BIT = NULL,
	@EnableShowroomBookValuations BIT = NULL,
	@VehiclesInShowroom TINYINT = NULL,
	@EnableShowroomReports BIT = NULL
as

	SET NOCOUNT ON
	set transaction isolation level read uncommitted

	BEGIN TRY

		if NOT EXISTS(SELECT * FROM Merchandising.settings.Merchandising WHERE businessUnitID = @businessUnitID)
			raiserror('settings.upgradeSettings#Update proc: Dealer not set up!', 16, 1)

		UPDATE Merchandising.settings.Merchandising 
			SET	maxForSmartphone = ISNULL(@maxForSmartphone, maxForSmartphone),
				autoWindowStickerTemplateId = @autoWindowStickerTemplateId,
				MAXForWebsite20 = ISNULL(@MAXForWebsite20, MAXForWebsite20),
				MaxDigitalShowroom = ISNULL(@MaxDigitalShowroom, MaxDigitalShowroom),
				UsePhoneMapping = ISNULL(@UsePhoneMapping, UsePhoneMapping),
				UseDealerRater = ISNULL(@UseDealerRater, UseDealerRater),
				EnableShowroomBookValuations = ISNULL(@EnableShowroomBookValuations, EnableShowroomBookValuations),
				vehiclesInShowroom = ISNULL(@VehiclesInShowroom, vehiclesInShowroom),
				EnableShowroomReports = ISNULL(@EnableShowroomReports, EnableShowroomReports)
		WHERE businessUnitID = @businessUnitID
		
	END TRY
	BEGIN CATCH

			DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE();

			-- Use RAISERROR inside the CATCH block to return error
			-- information about the original error that caused
			-- execution to jump to the CATCH block.
			RAISERROR (@ErrorMessage, -- Message text.
					   @ErrorSeverity, -- Severity.
					   @ErrorState -- State.
					   );
		
		RETURN 1
	END CATCH
	
	
	SET NOCOUNT OFF

RETURN 0
go

GRANT EXECUTE ON settings.upgradeSettings#Update to MerchandisingUser
go
