if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[ListingOptions#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[ListingOptions#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/08/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_listingOptions_fetch.sql,v 1.3 2009/11/09 17:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch all the listing options from market
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[ListingOptions#Fetch]
	-- Add the parameters for the stored procedure here		
	@OnlyUnmapped bit,
	@ProviderID int = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		IF @OnlyUnmapped = 1
		BEGIN
		SELECT optionId, description,optionCount
		from merchandising.optionsMapping
		WHERE 
			chromeCategoryId IS NULL
			AND (@ProviderID IS NULL OR providerId = @ProviderID)
			ORDER BY optionCount DESC
		END
		ELSE
		BEGIN
			SELECT optionId, description,optionCount
			FROM merchandising.optionsMapping
			WHERE (@ProviderID IS NULL OR providerId = @ProviderID)
			ORDER BY optionCount DESC
		END
		

END

GO

GRANT EXECUTE ON [Settings].[ListingOptions#Fetch] TO MerchandisingUser 
GO