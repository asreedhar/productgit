if object_id('settings.GoogleAnalyticsRefreshToken#Upsert') is not null
	drop procedure settings.GoogleAnalyticsRefreshToken#Upsert
go

create procedure settings.GoogleAnalyticsRefreshToken#Upsert
	@businessUnitId int,
	@clientId varchar(150),
	@clientSecret varchar(100),
	@refreshToken varchar(100)
as

set nocount on
set transaction isolation level read uncommitted

if exists (select 1 from Merchandising.settings.GoogleAnalyticsToken where BusinessUnitId = @businessUnitId)
	begin
	
	update Merchandising.settings.GoogleAnalyticsToken
	set RefreshToken = @refreshToken, ClientId = @clientId, ClientSecret = @clientSecret, Active = 1
	where BusinessUnitId = @businessUnitId
	
	end
else
	begin
	
	insert into Merchandising.settings.GoogleAnalyticsToken (BusinessUnitId, RefreshToken, ClientId, ClientSecret, Active)
	values(@businessUnitID, @refreshToken, @clientId, @clientSecret, 1)
	
	end

go

grant execute on settings.GoogleAnalyticsRefreshToken#Upsert to MerchandisingUser
go
