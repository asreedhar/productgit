use Merchandising

if object_id('settings.CreateDealer') is not null
	drop procedure settings.CreateDealer
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/08/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_createDealer.sql,v 1.9 2009/11/09 17:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * creates a new dealer in the system
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId int   - integer id of the businesUnit for which to get the alert list
	
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE settings.CreateDealer
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int
	
	
AS
SET NOCOUNT ON;
	
begin try

	begin transaction
	
	
	if NOT EXISTS (SELECT * FROM Settings.Merchandising WHERE businessUnitId = @BusinessUnitId)
	BEGIN
		--insert basic settings
		INSERT INTO [settings].[Merchandising]
				   ([businessUnitID]
				   ,[desiredPhotoCount]
				   ,[photoRequiredForRelease]
				   ,[desiredDescriptionLength]
				   ,[descriptionRequiredForRelease]
				   ,[autogenerateDescriptions]
				   ,[minDesiredEquipmentCount]
				   ,[approvalRequiredForRelease]
				   ,[minWeeklySearchCount]
				   ,[minWeeklyClickThruRatio]
				   ,[windowStickerTemplateID]
				   ,[useCarfax]
				   ,[videoDealerId]
				   ,[carfaxUsername]
				   ,[carfaxPassword]
				   ,[maxForSmartphone]
				   ,[showOnlineClassifiedOverview])
			 SELECT
			 @BusinessUnitId, 
				[desiredPhotoCount]
				   ,[photoRequiredForRelease]
				   ,[desiredDescriptionLength]
				   ,[descriptionRequiredForRelease]
				   ,[autogenerateDescriptions]
				   ,[minDesiredEquipmentCount]
				   ,[approvalRequiredForRelease]
				   ,[minWeeklySearchCount]
				   ,[minWeeklyClickThruRatio]
				   ,[windowStickerTemplateID]
				   ,1
				   ,NULL
				   ,''
				   ,''
				   ,[maxForSmartphone]
				   ,[showOnlineClassifiedOverview]
				   FROM settings.Merchandising
				WHERE businessUnitId = 100150
	END

	IF NOT EXISTS (SELECT * FROM Settings.MerchandisingDescriptionPreferences WHERE businessUnitId = @BusinessUnitId)
	BEGIN
		--description prefs
		INSERT INTO [settings].[MerchandisingDescriptionPreferences]
				   ([businessUnitId]
				   ,[maxOptionsTier0]
				   ,[maxOptionsTier1]
				   ,[maxOptionsTier2]
				   ,[maxOptionsTier3]
				   ,[goodBookDifferential]
				   ,[goodMsrpDifferential]
				   ,[minNumberForCountBelowBookDisplay]
				   ,[minNumberForCountBelowPriceDisplay]
				   ,[goodWarrantyMileageRemaining]
				   ,[maxWarrantiesToDisplay]
				   ,[goodCrashTestRating]
				   ,[maxCrashTestRatingsToDisplay]
				   ,[countTier1ForLoaded]
				   ,[textSeparator]
				   ,[useLongDescriptionAsDefault]
				   ,[desiredPreviewLength]
				   )
		SELECT   @BusinessUnitId
				   ,[maxOptionsTier0]
				   ,[maxOptionsTier1]
				   ,[maxOptionsTier2]
				   ,[maxOptionsTier3]
				   ,[goodBookDifferential]
				   ,[goodMsrpDifferential]
				   ,[minNumberForCountBelowBookDisplay]
				   ,[minNumberForCountBelowPriceDisplay]
				   ,[goodWarrantyMileageRemaining]
				   ,[maxWarrantiesToDisplay]
				   ,[goodCrashTestRating]
				   ,[maxCrashTestRatingsToDisplay]
				   ,[countTier1ForLoaded]
				   ,[textSeparator]
				   ,[useLongDescriptionAsDefault]
				   ,[desiredPreviewLength]
		FROM [settings].[MerchandisingDescriptionPreferences]
		WHERE businessUnitId = 100150
	END

	IF NOT EXISTS (SELECT * FROM Settings.GasMileagePreferences WHERE businessUnitId = @BusinessUnitId)
	BEGIN
		-------GAS MIlEAGE---------
		INSERT INTO [settings].[GasMileagePreferences]
				   ([businessUnitId]
				   ,[segmentId]
				   ,[goodHighwayGasMileage]
				   ,[goodCityGasMileage])
		SELECT @BusinessUnitId
					,[segmentId]
				   ,[goodHighwayGasMileage]
				   ,[goodCityGasMileage]
		FROM [settings].[GasMileagePreferences]
		WHERE businessUnitId = 100150
	END

	IF NOT EXISTS (SELECT * FROM Settings.EquipmentDisplayRankings WHERE businessUnitId = @BusinessUnitId)
	BEGIN
		----------EQUIP DISPLAY RANKINGS-----------
		INSERT INTO [settings].[EquipmentDisplayRankings]
				   ([businessUnitID]
				   ,[categoryID]
				   ,[displayPriority]
				   ,[tierNumber]
				   ,[typeID]
				   ,[categoryDesc]
				   ,[displayDescription])
		SELECT @BusinessUnitId
				,[categoryID]
				   ,[displayPriority]
				   ,[tierNumber]
				   ,[typeID]
				   ,[categoryDesc]
				   ,[displayDescription]
		FROM [settings].[EquipmentDisplayRankings]
		WHERE businessUnitId = 100150
	END

	IF NOT EXISTS (SELECT * FROM builder.VehicleConditionAdjectives WHERE businessUnitId = @BusinessUnitId)
	BEGIN
		----------CONDITION ADJECTIVES---------------
		INSERT INTO [builder].[VehicleConditionAdjectives]
				   ([ConditionDescription]
				   ,[ConditionLevel]
				   ,[businessUnitID])
		SELECT	[ConditionDescription]
				   ,[ConditionLevel]
					,@BusinessUnitId
		FROM [builder].[VehicleConditionAdjectives]
		WHERE businessUnitId = 100150
	END

	IF NOT EXISTS (SELECT * FROM Settings.AlertingPreferences WHERE businessUnitId = @BusinessUnitId)
	BEGIN
		-------------ALERTING PREFERENCES--------------
		INSERT INTO [settings].[AlertingPreferences]
				(businessUnitId
				, daysToEquipmentComplete
				, daysToFirstPhoto
				, daysToPhotosComplete
				, daysToPricingComplete
				, daysToApproval
				, maxDaysToApprovedAfterPending
				, daysToIncludeForActivityCalculations
				, maxDaysUnderMerchandised)
		SELECT
				@BusinessUnitId
				, daysToEquipmentComplete
				, daysToFirstPhoto
				, daysToPhotosComplete
				, daysToPricingComplete
				, daysToApproval
				, maxDaysToApprovedAfterPending
				, daysToIncludeForActivityCalculations
				, maxDaysUnderMerchandised
		FROM [settings].[AlertingPreferences]
		WHERE businessUnitId = 100150
	END

	IF NOT EXISTS (SELECT * FROM builder.AdditionalInfoCategories WHERE businessUnitId = @BusinessUnitId)
	BEGIN
		-----------Additional INFO------------------
		INSERT INTO [builder].[AdditionalInfoCategories]
				   ([infoBuilderDisplay]
				   ,[businessUnitID]
				   ,[infoAdDisplay]
				   ,[enabled])
		SELECT
			[infoBuilderDisplay]
				   ,@BusinessUnitId
				   ,[infoAdDisplay]
				   ,[enabled]
		FROM [builder].[AdditionalInfoCategories]
		WHERE businessUnitId = 100150
	end



	--------------template copies
	exec settings.createDealerTemplates @BusinessUnitId
	
	-- Insert any missing OptionConfiguration records
	exec builder.InsertMissingOptionsConfigurationRecords @BusinessUnitId = @BusinessUnitId, @MemberLogin = '#CreateDealer'
	
	commit
	
	return 0
end try
begin catch
	rollback
	declare @error varchar(1000)
	
	set @error =
		'ErrorNumber ' + isnull(convert(varchar(11), error_number()), '<null>') +
		', ErrorSeverity ' + isnull(convert(varchar(11), error_severity()), '<null>') +
		', ErrorState ' + isnull(convert(varchar(11), error_state()), '<null>') +
		', ErrorProcedure ' + isnull(error_procedure(), '<null>') +
		', ErrorLine ' + isnull(convert(varchar(11), error_line()), '<null>') +
		', ErrorMessage ' + isnull(error_message(), '<null>')
	
	raiserror(@error, 16, 1)
	
	return 1
end catch



         
-----------DEALERDESTINATIONDEFAULTS----------

-----------DEALER CERTIFICATION CONFIGURATION--------
-----------LOT LOCATIONS?----------------

GO

GRANT EXECUTE ON settings.CreateDealer TO MerchandisingUser 
GO