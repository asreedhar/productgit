if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[CustomStandardEquipment#Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[CustomStandardEquipment#Delete]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sombke, Bryant
-- Create date: <04/29/2010>
-- =============================================
/* --------------------------------------------------------------------
 *
 * Summary
 * -------
 * 
 *   Deletes the custom standard equipment description for the specified business unit and Option Code
 * 
 * Parameters
 * ----------
 * 
 *   @BusinessUnitId int,
 *   @UsedNew int,
 *	 @OptionCode NVARCHAR(10)
 * 
 * Exceptions
 * ----------
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[CustomStandardEquipment#Delete]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
    @UsedNew int,
 	@OptionCode NVARCHAR(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN
		DELETE FROM settings.CustomStandardEquipment
		WHERE businessUnitId=@businessUnitId
		AND UsedNew=@UsedNew
		AND OptionCode=@OptionCode
	END
	
END
GO

GRANT EXECUTE ON [Settings].[CustomStandardEquipment#Delete] TO MerchandisingUser
GO    