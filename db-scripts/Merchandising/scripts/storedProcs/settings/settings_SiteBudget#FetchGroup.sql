use Merchandising
go

if object_id('settings.SiteBudget#FetchGroup') is not null
	drop procedure settings.SiteBudget#FetchGroup
GO

create procedure settings.SiteBudget#FetchGroup
	@GroupID int, 
	@StartDate datetime, 
	@EndDate datetime 
as

set nocount on
set transaction isolation level read uncommitted

declare @dates table ( Date datetime )

while (@StartDate <= @EndDate)
begin
insert into @dates
values (@StartDate )
select @StartDate=DATEADD(MM,1,@StartDate)
end


select bu.businessunitid,
	bu.businessunit,
	t.destinationid,
	sum(settings.Budget(bu.businessunitid, t.destinationid, date)) as Budget
	from imt.dbo.BusinessunitRelationship bur
	join imt.dbo.Businessunit bu on bur.businessunitid = bu.businessunitid
	join (select businessUnitid, destinationId 
		from settings.SiteBudget sb
		group by businessUnitid, destinationId ) t on t.businessunitid = bu.businessunitid
	cross join @dates d
	where bur.ParentID = @GroupID
group by bu.businessunitid,
		bu.businessunit,
		t.destinationid
having sum(settings.Budget(bu.businessunitid, t.destinationid, date)) is not null

GO

grant execute on settings.SiteBudget#FetchGroup to MerchandisingUser 
GO
