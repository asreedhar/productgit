if object_id('settings.GoogleAnalyticsRefreshToken#Fetch') is not null
	drop procedure settings.GoogleAnalyticsRefreshToken#Fetch
go

create procedure settings.GoogleAnalyticsRefreshToken#Fetch
	@businessUnitID int
as

set nocount on
set transaction isolation level read uncommitted

select ClientId, ClientSecret, RefreshToken
from Merchandising.settings.GoogleAnalyticsToken
where BusinessUnitID = @businessUnitID

go

grant execute on settings.GoogleAnalyticsRefreshToken#Fetch to MerchandisingUser
go
