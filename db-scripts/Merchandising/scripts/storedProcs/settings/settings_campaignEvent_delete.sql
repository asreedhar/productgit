if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[campaignEvent#Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [settings].[campaignEvent#Delete]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <2/5/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_campaignEvent_delete.sql,v 1.1 2009/11/09 17:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * delete the campaign event
 * 
 * 
 * Parameters
 * ----------
 *  @BusinessUnitId - 
 *  @CampaignEventId
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [settings].[campaignEvent#Delete]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@CampaignEventId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	DELETE FROM settings.CampaignEvents
	WHERE 
		businessUnitId = @BusinessUnitId
		AND campaignEventId = @CampaignEventId

	
	
END
GO

GRANT EXECUTE ON [settings].[campaignEvent#Delete] TO MerchandisingUser 
GO