USE [Merchandising]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Boucher
-- Create date: 11/14/2011
-- Description: settings.PreApprove#Upsert
--				Updates or inserts 'pre-approve with review' settings by business unit ID
-- =============================================
IF object_id('settings.PreApprove#Upsert') IS NOT NULL
	DROP PROCEDURE [settings].[PreApprove#Upsert]
GO
