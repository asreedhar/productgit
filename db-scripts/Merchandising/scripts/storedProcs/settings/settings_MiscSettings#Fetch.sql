use Merchandising
go

if object_id('settings.MiscSettings#Fetch') is not null
	drop procedure settings.MiscSettings#Fetch
go


create procedure settings.MiscSettings#Fetch
	@businessUnitID int
as

set nocount on
set transaction isolation level read uncommitted

select 
	sendOptimalFormat,
	franchiseId,
	priceNewCars,
	showDashboard,
	ShowGroupLevelDashboard,
	analyticsSuite,
	batchAutoload,
	MaxVersion,
	WebLoaderEnabled,
	ModelLevelFrameworksEnabled,
	AutoOffline_WholesalePlanTrigger,
	ShowCtrGraph,
	DealershipSegment,
	BulkPriceNewCars,
	showOnlineClassifiedOverview,
	ShowTimeToMarket,
	GIDProviderNew,
	GIDProviderUsed,
	DMSName,
	DMSEmail,
	ReportDataSourceNew,
	ReportDataSourceUsed
from Merchandising.settings.Merchandising
where businessUnitID = @businessUnitID

go

grant execute on settings.MiscSettings#Fetch to MerchandisingUser
go
