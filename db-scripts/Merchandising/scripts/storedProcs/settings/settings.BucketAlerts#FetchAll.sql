IF OBJECT_ID('settings.BucketAlerts#FetchAll') IS NOT NULL
	DROP PROCEDURE [settings].[BucketAlerts#FetchAll]
GO

CREATE PROCEDURE [settings].[BucketAlerts#FetchAll]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT	[BusinessUnitId],
			[Active],
			[Email],
			[NewUsed],
			[EmailBounced],
			[WeeklyEmail],
			[WeeklyEmailBounced],
			[BionicHealthReportEmail],
			[BionicHealthReportEmailBounced],
			[GroupBionicHealthReportEmail],
			[GroupBionicHealthReportEmailBounced]
	FROM	[settings].[BucketAlerts]

GO

GRANT EXECUTE ON [settings].[BucketAlerts#FetchAll] TO [MerchandisingUser]
GO
