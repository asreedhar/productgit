USE [Merchandising]
GO

/****** Object:  StoredProcedure [settings].[EdtDestinations#FetchGoogleVdpWebsites]    Script Date: 05/01/2013 16:53:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[settings].[EdtDestinations#FetchGoogleVdpWebsites]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [settings].[EdtDestinations#FetchGoogleVdpWebsites]
GO

USE [Merchandising]
GO

/****** Object:  StoredProcedure [settings].[EdtDestinations#FetchGoogleVdpWebsites]    Script Date: 05/01/2013 16:53:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [settings].[EdtDestinations#FetchGoogleVdpWebsites]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		destinationID, description
	FROM settings.EdtDestinations
	WHERE
		hasGoogleVdps = 1

END

GO

grant execute on [settings].[EdtDestinations#FetchGoogleVdpWebsites] to MerchandisingUser
GO

