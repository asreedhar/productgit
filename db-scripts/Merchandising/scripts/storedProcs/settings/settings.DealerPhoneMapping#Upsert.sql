IF OBJECT_ID('settings.DealerPhoneMapping#Upsert') IS NOT NULL
	DROP PROCEDURE [settings].[DealerPhoneMapping#Upsert]
GO
/* --------------------------------------------------------------------
 *
 * Summary
 * -------
 * 
 *   Inserts/updates the phone mapping for the specified business unit
 * 
 * Parameters
 * ----------
 * 
 *   @BusinessUnitId INT,
 *	 @MappedPhone NVARCHAR(14),
 *	 @ProvisionedPhone NVARCHAR(14)
 * 
 * Exceptions
 * ----------
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [settings].[DealerPhoneMapping#Upsert]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId INT,
 	@MappedPhone NVARCHAR(14),
 	@ProvisionedPhone NVARCHAR(14),
 	@Purchased BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM [settings].[DealerPhoneMapping] WHERE [BusinessUnitID] = @BusinessUnitID)
	BEGIN
		UPDATE [settings].[DealerPhoneMapping]
		SET	[MappedPhone] = @MappedPhone,
			[ProvisionedPhone] = @ProvisionedPhone,
			[Purchased] = @Purchased
		WHERE [BusinessUnitId] = @businessUnitId
	END
	ELSE
	BEGIN
		INSERT INTO [settings].[DealerPhoneMapping] ([BusinessUnitId], [MappedPhone], [ProvisionedPhone], [Purchased])
		VALUES (@BusinessUnitId, @MappedPhone, @ProvisionedPhone, @Purchased)
	END
	
END
GO

GRANT EXECUTE ON [settings].[DealerPhoneMapping#Upsert] TO MerchandisingUser
GO

