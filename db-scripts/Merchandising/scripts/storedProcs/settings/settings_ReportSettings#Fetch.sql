if object_id('settings.ReportSettings#Fetch') is not null
	drop procedure settings.ReportSettings#Fetch
go


create procedure settings.ReportSettings#Fetch
	@businessUnitID int
as


set nocount on
set transaction isolation level read uncommitted

select 
COALESCE(lowActivityThreshold, 0.01) as lowActivityThreshold, 
COALESCE(highActivityThreshold, 0.04) as highActivityThreshold, 
COALESCE(minSearchCountThreshold, 100) as minSearchCountThreshold,
COALESCE(VehicleOnlineStatusReportMinAgeDays, 7) as VehicleOnlineStatusReportMinAgeDays,
COALESCE(AdCompleteFlags, 7) as AdCompleteFlags,
MerchandisingGraphDefault,
TimeToMarketGoalDays

from Merchandising.settings.Merchandising
where businessUnitID = @businessUnitID


go

grant execute on settings.ReportSettings#Fetch to MerchandisingUser
go
