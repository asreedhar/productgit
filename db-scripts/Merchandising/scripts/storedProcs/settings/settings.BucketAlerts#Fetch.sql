IF OBJECT_ID('settings.BucketAlerts#Fetch') IS NOT NULL
	DROP PROCEDURE [settings].[BucketAlerts#Fetch]
GO

CREATE PROCEDURE [settings].[BucketAlerts#Fetch]
	@BusinessUnitId INT
AS
BEGIN

	SELECT	[BusinessUnitId],
			[Active],
			[Email],
			[NewUsed],
			[EmailBounced],
			[WeeklyEmail],
			[WeeklyEmailBounced],
			[BionicHealthReportEmail],
			[BionicHealthReportEmailBounced],
			[GroupBionicHealthReportEmail],
			[GroupBionicHealthReportEmailBounced]
	FROM	[settings].[BucketAlerts]
	WHERE	[BusinessUnitId] = @BusinessUnitId

END
GO

GRANT EXECUTE ON [settings].[BucketAlerts#Fetch] TO [MerchandisingUser]
GO
