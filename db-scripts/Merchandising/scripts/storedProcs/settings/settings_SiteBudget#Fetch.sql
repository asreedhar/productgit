use Merchandising
go

if object_id('settings.SiteBudget#Fetch') is not null
	drop procedure settings.SiteBudget#Fetch
go

-- returns the most recent value
create procedure settings.SiteBudget#Fetch
	@BusinessUnitId int,
	@DestinationId int,
	@MonthApplicable datetime
as

set nocount on
set transaction isolation level read uncommitted

select top 1 *
from settings.SiteBudget
where BusinessUnitId = @BusinessUnitId
  and DestinationId = @DestinationId
  and MonthApplicable <= @MonthApplicable
order by MonthApplicable desc

go

grant execute on settings.SiteBudget#Fetch to MerchandisingUser
go

