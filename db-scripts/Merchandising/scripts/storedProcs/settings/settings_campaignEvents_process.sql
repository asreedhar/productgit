if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[campaignEvents#Process]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [settings].[campaignEvents#Process]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <2/5/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_campaignEvents_process.sql,v 1.1 2009/11/09 17:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * processes the campaign event
 * 
 * 
 * Parameters
 * ----------
 *  @BusinessUnitId 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [settings].[campaignEvents#Process]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@InventoryType int = 2
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @processName varchar(30)
	set @processName = 'campaignProcessor'
	CREATE TABLE #Inventory (
		rowId int identity(1,1),
		BusinessUnitId int NOT NULL,
		Vin varchar(17) NOT NULL,
		StockNumber varchar(15) NOT NULL,
		InventoryReceivedDate datetime NOT NULL, 
		vehicleYear int NOT NULL, 
		UnitCost decimal(9,2) NOT NULL, 
		AcquisitionPrice decimal (8,2) NOT NULL, 
		Certified tinyint NOT NULL, 
		Make varchar(20) NOT NULL,	
		model varchar(50) NOT NULL,
		VehicleTrim varchar(50) NULL,
		inventoryID int NOT NULL,
		MileageReceived int NULL, 
		TradeOrPurchase tinyint NOT NULL, 
		ListPrice decimal(8,2) NULL,
		InventoryType tinyint NOT NULL,
		InventoryStatusCD int NOT NULL,
		BaseColor varchar(50) NOT NULL,
		VehicleLocation varchar(20) NULL,
		LotPrice DECIMAL(8,2) NULL,
		MSRP DECIMAL(9,2) NULL,
		VehicleCatalogId int NULL,
		isLocked bit NOT NULL default(0),
		equipmentLoaded int NOT NULL default(0),
		photosLoaded int NOT NULL default(0),
		pendingCandidate int NOT NULL default(0),
		chromeStyleId int NULL
	)
	
	INSERT INTO 	#Inventory (BusinessUnitId, Vin, StockNumber, InventoryReceivedDate, vehicleYear, UnitCost, 
					AcquisitionPrice, Certified, Make, model, vehicleTrim, inventoryID, MileageReceived, TradeOrPurchase,
					ListPrice, InventoryType, InventoryStatusCD, BaseColor, VehicleLocation, VehicleCatalogId) 	
	EXEC Interface.InventoryList#Fetch @BusinessUnitId=@BusinessUnitID, @UsedOrNew=@InventoryType
	
	UPDATE de
	SET de.templateId = ce.templateId,
		de.themeId = ce.themeId,
		de.isAutoPilot = 1,
		de.lastUpdatedBy = @processName,
		de.lastUpdatedOn = getdate()
	FROM
		settings.campaignEvents ce
		JOIN #Inventory inv
			on inv.businessUnitId = ce.businessUnitId
			and datediff(d, inv.inventoryReceivedDate, getdate()) = ce.vehicleAgeInDaysForAction
		JOIN builder.descriptions de
			ON de.businessUnitId = inv.businessUnitId
			and de.inventoryId = inv.inventoryId		
	WHERE ce.regenerateDescription = 1
		and ce.businessUnitId = @BusinessUnitId
	

	--set posting status for those already posted
	UPDATE vs
	SET vs.statusLevel = 2,
		vs.lastUpdatedBy = @processName,
		vs.lastUpdatedOn = getdate()
	FROM
		settings.campaignEvents ce
		JOIN #Inventory inv
			on inv.businessUnitId = ce.businessUnitId
			and datediff(d, inv.inventoryReceivedDate, getdate()) = ce.vehicleAgeInDaysForAction
		JOIN builder.vehicleStatus vs
			ON vs.businessUnitId = inv.businessUnitId
			and vs.inventoryId = inv.inventoryId
		
	WHERE ce.regenerateDescription = 1
		and ce.businessUnitId = @BusinessUnitId
		and vs.statusTypeId = 5
		
	
	DROP TABLE #Inventory
	
END
GO

GRANT EXECUTE ON [settings].[campaignEvents#Process] TO MerchandisingUser 
GO