if object_id('settings.GoogleAnalyticsProfile#Fetch') is not null
	drop procedure settings.GoogleAnalyticsProfile#Fetch
go

create procedure settings.GoogleAnalyticsProfile#Fetch
	@businessUnitId int,
	@type int
as

set nocount on
set transaction isolation level read uncommitted

select ProfileId, ProfileName, DestinationId
from Merchandising.settings.GoogleAnalyticsProfile
where BusinessUnitID = @businessUnitID and Type = @type

go

grant execute on settings.GoogleAnalyticsProfile#Fetch to MerchandisingUser
go
