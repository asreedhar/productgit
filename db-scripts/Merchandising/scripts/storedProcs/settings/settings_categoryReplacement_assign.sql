if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[CategoryReplacement#Assign]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[CategoryReplacement#Assign]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/08/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_categoryReplacement_assign.sql,v 1.1 2009/07/27 23:55:41 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * assign a category replacement to a dealer
 * 
 * 
 * Parameters
 * ----------
 * 
 * @ReplacementId
 * @BusinessUnitId int
 * @MemberLogin varchar(30)
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[CategoryReplacement#Assign]
	-- Add the parameters for the stored procedure here
	
	@BusinessUnitId int,
	@ReplacementId int,
	@MemberLogin varchar(30) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--create an active category replacement...
	IF NOT EXISTS (select 1 from settings.CategoryReplacement_Dealer 
		WHERE businessUnitId = @BusinessUnitId
			AND replacementId = @ReplacementId)
	BEGIN
	INSERT INTO 
		settings.CategoryReplacement_Dealer
	(
		
		[businessUnitId],
		[replacementId],
		[createdBy],
		[createdOn],
		[lastUpdatedBy],
		[lastUpdatedOn]	
	) 
	VALUES (
		@BusinessUnitId,
		@ReplacementId,
		@MemberLogin,
		getdate(),
		@MemberLogin,
		getdate()	
	)
	END
		
	
END

GO

GRANT EXECUTE ON [Settings].[CategoryReplacement#Assign] TO MerchandisingUser 
GO