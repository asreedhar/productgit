use Merchandising
go

if object_id('settings.MiscSettings#Update') is not null
	drop procedure settings.MiscSettings#Update
go


create procedure settings.MiscSettings#Update
	@businessUnitID int,
	@sendOptimalFormat bit,
	@franchiseId int,
	@priceNewCars bit,
	@showDashboard bit,
	@showGroupLevelDashboard bit,
	@analyticsSuite bit,
	@batchAutoload bit,
	@MaxVersion tinyint = 2,
	@WebLoaderEnabled bit = 0,
	@ModelLevelFrameworksEnabled bit = 0,
	@AutoOffline_WholesalePlanTrigger bit = null,
	@ShowCtrGraph bit,
	@DealershipSegment int = null,
	@BulkPriceNewCars bit,
	@showOnlineClassifiedOverview bit = null, -- FB: 28329 
	@ShowTimeToMarket bit = null, -- FB: 28466
	@GIDProviderNew tinyint = 0,
	@GIDProviderUsed tinyint = 0,
	@DMSName varchar(255),
	@DMSEmail varchar(255),
	@ReportDataSourceNew tinyint = 0,
	@ReportDataSourceUsed tinyint = 0
as

set nocount on
set transaction isolation level read uncommitted

if not exists(select * from Merchandising.settings.Merchandising where businessUnitID = @businessUnitID)
begin
	raiserror('settings.MiscSettings#Update proc: Dealer not set up!', 16, 1)
	return 1
end

-- ReportDataSource = 4 corresponds to Cars API
declare @IsReportDataSourceCarsApiNewOrUsed BIT
SET @IsReportDataSourceCarsApiNewOrUsed = CASE WHEN (@ReportDataSourceNew = 4 OR @ReportDataSourceUsed = 4) THEN 1 ELSE 0 END

update Merchandising.settings.Merchandising set 
	sendOptimalFormat = @sendOptimalFormat, 
	franchiseId = @franchiseId, 
	priceNewCars = @priceNewCars,
	BulkPriceNewCars = @BulkPriceNewCars,
	showDashboard = @showDashboard,
	showGroupLevelDashboard = @showGroupLevelDashboard,
	analyticsSuite = @analyticsSuite,
	batchAutoload = @batchAutoload,
	MaxVersion = @MaxVersion,
	WebloaderEnabled = @WebloaderEnabled,
	ModelLevelFrameworksEnabled = @ModelLevelFrameworksEnabled,
	AutoOffline_WholesalePlanTrigger = isnull(@AutoOffline_WholesalePlanTrigger, AutoOffline_WholesalePlanTrigger),
	ShowCtrGraph = @ShowCtrGraph,
	DealershipSegment = @DealershipSegment,
	showOnlineClassifiedOverview = IsNull(@showOnlineClassifiedOverview, showOnlineClassifiedOverview),
	ShowTimeToMarket = IsNull(@ShowTimeToMarket, ShowTimeToMarket),
	GIDProviderNew = @GIDProviderNew, 
	GIDProviderUsed = @GIDProviderUsed,
	DMSName = @DMSName,
	DMSEmail = @DMSEmail,
	ReportDataSourceNew = @ReportDataSourceNew,
	ReportDataSourceUsed = @ReportDataSourceUsed,
	-- FB 32041: Set AdCompleteFlags to 5 if the ReportDataSource for new or used is being set to CarsApi
	-- This will disable the description checkbox and enable the photos and price checkboxes
	AdCompleteFlags = CASE WHEN (@IsReportDataSourceCarsApiNewOrUsed = 1) THEN 5 ELSE AdCompleteFlags END
where businessUnitID = @businessUnitID

IF (@IsReportDataSourceCarsApiNewOrUsed = 1)
BEGIN
	DECLARE @currDate as datetime
	SET @currDate = CAST(GETDATE() AS DATE)
	EXEC dashboard.TimeToMarket#SetCarsApiStartDate @businessUnitID, @currDate
END

return 0

go

grant execute on settings.MiscSettings#Update to MerchandisingUser
go