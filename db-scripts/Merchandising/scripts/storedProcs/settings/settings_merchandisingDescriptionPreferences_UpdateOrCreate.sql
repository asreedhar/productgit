if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[merchandisingDescriptionPreferences#UpdateOrCreate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[merchandisingDescriptionPreferences#UpdateOrCreate]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/12/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_merchandisingDescriptionPreferences_UpdateOrCreate.sql,v 1.11 2010/02/09 21:07:40 bsombke Exp $
 * 
 * Summary
 * -------
 * 
 * creates a new vehicle metric entry or updates the existing entry
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId int   - integer id of the businesUnit for which to get the alert list
	@goodBookDifferential decimal,
	@goodCrashTestRating int,
	@goodMsrpDifferential decimal,
	@goodWarrantyMileageRemaining int,
	@maxCrashTestRatingsToDisplay int,
	@maxOptionsTier0 int,
	@maxOptionsTier1 int,
	@maxOptionsTier2 int,
	@maxOptionsTier3 int,
	@maxWarrantiesToDisplay int,
	@minNumberForCountBelowBookDisplay int,
	@minNumberForCountBelowPriceDisplay int
	@countTier1ForLoaded int,
	@textSeparator varchar(10) -- text to use when separating text in the description
	@preferredBookoutCategoryId int,
	@defaultTemplateId int,
	@useLongDescriptionAsDefault bit - whether to use the long form as default
	@desiredPreviewLength int
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [settings].[merchandisingDescriptionPreferences#UpdateOrCreate]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@goodBookDifferential decimal,
	@goodCrashTestRating int,
	@goodMsrpDifferential decimal,
	@goodWarrantyMileageRemaining int,
	@maxCrashTestRatingsToDisplay int,
	@maxOptionsTier0 int,
	@maxOptionsTier1 int,
	@maxOptionsTier2 int,
	@maxOptionsTier3 int,
	@maxWarrantiesToDisplay int,
	@minNumberForCountBelowBookDisplay int,
	@minNumberForCountBelowPriceDisplay int,
	@countTier1ForLoaded int,
	@textSeparator varchar(10),
	@preferredBookoutCategoryId int,
	@defaultTemplateId int,
	@useLongDescriptionAsDefault bit,
	@desiredPreviewLength int,
	@minGoodPriceReduction int,
	@useCallToActionInPreview bit,
	@minValidJDPowerRating float,
	@minFavourablePriceComparisons int,
	@Threshold_ProofPoint int,
	@KBB_ProofPoint int,
	@NADA_ProofPoint int,
	@MktAvg_ProofPoint int,
	@MSRP_ProofPoint int,
	@Edmunds_ProofPoint int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 from settings.merchandisingDescriptionPreferences
			where businessUnitId = @BusinessUnitId)
		BEGIN
			UPDATE settings.merchandisingDescriptionPreferences
				SET 
				maxOptionsTier0 = @maxOptionsTier0,
				maxOptionsTier1 = @maxOptionsTier1, 
				maxOptionsTier2 = @maxOptionsTier2,
				maxOptionsTier3 = @maxOptionsTier3,
				goodBookDifferential = @goodBookDifferential, 
				goodMsrpDifferential = @goodMsrpDifferential, 
				minNumberForCountBelowBookDisplay = @minNumberForCountBelowBookDisplay,
				minNumberForCountBelowPriceDisplay = @minNumberForCountBelowPriceDisplay, 
				goodWarrantyMileageRemaining = @goodWarrantyMileageRemaining, 
				maxWarrantiesToDisplay = @maxWarrantiesToDisplay, 
				goodCrashTestRating = @goodCrashTestRating, 
				maxCrashTestRatingsToDisplay = @maxCrashTestRatingsToDisplay,
				countTier1ForLoaded = @countTier1ForLoaded,
				textSeparator = @textSeparator,
				preferredBookoutCategoryId = @preferredBookoutCategoryId,
				defaultTemplateId = @defaultTemplateId,
				useLongDescriptionAsDefault = @useLongDescriptionAsDefault,
				desiredPreviewLength = @desiredPreviewLength,
				minGoodPriceReduction = @minGoodPriceReduction, 
				useCallToActionInPreview = @useCallToActionInPreview,
				minValidJDPowerRating = @minValidJDPowerRating,
				FavourableComparisonThreshold=@minFavourablePriceComparisons,
				Threshold_ProofPoint=@Threshold_ProofPoint,
				KBB_ProofPoint= @KBB_ProofPoint,
				NADA_ProofPoint=@NADA_ProofPoint,
				MktAvg_ProofPoint=@MktAvg_ProofPoint,
				MSRP_ProofPoint=@MSRP_ProofPoint,
				Edmunds_ProofPoint=@Edmunds_ProofPoint
				
				
				WHERE businessUnitId = @BusinessUnitId
		END
	ELSE
		BEGIN
			INSERT INTO settings.merchandisingDescriptionPreferences
				(
				businessUnitId, maxOptionsTier0, maxOptionsTier1, maxOptionsTier2, maxOptionsTier3,
				goodBookDifferential, goodMsrpDifferential, minNumberForCountBelowBookDisplay,
				minNumberForCountBelowPriceDisplay,
				goodWarrantyMileageRemaining, maxWarrantiesToDisplay, goodCrashTestRating, maxCrashTestRatingsToDisplay, 
				countTier1ForLoaded, textSeparator,preferredBookoutCategoryId,
				defaultTemplateId, useLongDescriptionAsDefault, desiredPreviewLength,
				minGoodPriceReduction, useCallToActionInPreview, minValidJDPowerRating,FavourableComparisonThreshold, 
				Threshold_ProofPoint,KBB_ProofPoint, NADA_ProofPoint, MktAvg_ProofPoint, MSRP_ProofPoint, Edmunds_ProofPoint
				)
			VALUES
			(
				@BusinessUnitId, 
				@maxOptionsTier0, 
				@maxOptionsTier1, 
				@maxOptionsTier2, 
				@maxOptionsTier3,
				@goodBookDifferential, 
				@goodMsrpDifferential, 
				@minNumberForCountBelowBookDisplay,
				@minNumberForCountBelowPriceDisplay, 
				@goodWarrantyMileageRemaining, 
				@maxWarrantiesToDisplay, 
				@goodCrashTestRating, 
				@maxCrashTestRatingsToDisplay,
				@countTier1ForLoaded,
				@textSeparator,
				@preferredBookoutCategoryId,
				@defaultTemplateId,
				@useLongDescriptionAsDefault,
				@desiredPreviewLength,
				@minGoodPriceReduction, 
				@useCallToActionInPreview,
				@minValidJDPowerRating,
				@minFavourablePriceComparisons,
				@Threshold_ProofPoint,
				@KBB_ProofPoint,
				@NADA_ProofPoint,
				@MktAvg_ProofPoint,
				@MSRP_ProofPoint,
				@Edmunds_ProofPoint
			)
		END

END



GO

GRANT EXECUTE ON [Settings].[merchandisingDescriptionPreferences#UpdateOrCreate] TO MerchandisingUser 
GO