if object_id('settings.ReportSettings#Update') is not null
	drop procedure settings.ReportSettings#Update
go


create procedure settings.ReportSettings#Update
	@businessUnitID int,
	@lowActivityThreshold real,
	@highActivityThreshold real,
	@minSearchCountThreshold int,
    @vehicleOnlineStatusReportMinAgeDays int,
    @adCompleteFlags int,
    @merchandisingGraphDefault tinyint,
    @TimeToMarketGoalDays int
as

set nocount on
set transaction isolation level read uncommitted

if not exists(select * from Merchandising.settings.Merchandising where businessUnitID = @businessUnitID)
begin
	raiserror('settings.ReportSettings#Update proc: Dealer not set up!', 16, 1)
	return 1
end

update Merchandising.settings.Merchandising
set 
    lowActivityThreshold = @lowActivityThreshold ,
    highActivityThreshold = @highActivityThreshold ,
    minSearchCountThreshold = @minSearchCountThreshold,
    VehicleOnlineStatusReportMinAgeDays = @vehicleOnlineStatusReportMinAgeDays,
    AdCompleteFlags = @adCompleteFlags,
    MerchandisingGraphDefault = @merchandisingGraphDefault,
    TimeToMarketGoalDays = @TimeToMarketGoalDays

    where businessUnitID = @businessUnitID

return 0

go

grant execute on settings.ReportSettings#Update to MerchandisingUser
go


