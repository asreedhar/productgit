USE [Merchandising]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Boucher
-- Create date: 11/14/2011
-- Description: settings.AutoApprove#Fetch
--				Gets 'AutoApprove' settings by business unit ID
-- =============================================
IF object_id('settings.AutoApprove#Fetch') IS NOT NULL
	DROP PROCEDURE [settings].[AutoApprove#Fetch]
GO
CREATE PROCEDURE [settings].[AutoApprove#Fetch]

	-- Parameters
	@BusinessUnitId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [FirstName],
		   [LastName],
		   [Email],
		   [Status], 
		   [CreatedOn],
		   [CreatedBy],
		   [UpdatedOn],
		   [UpdatedBy],
		   [EmailBounced]
    FROM settings.AutoApprove
	WHERE businessUnitID = @BusinessUnitId

END
GO
GRANT EXECUTE ON [settings].[AutoApprove#Fetch] TO MerchandisingUser;
GO