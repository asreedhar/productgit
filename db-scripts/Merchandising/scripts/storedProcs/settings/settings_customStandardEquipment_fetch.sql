if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[CustomStandardEquipment#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[CustomStandardEquipment#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sombke, Bryant
-- Create date: <04/27/2010>
-- =============================================
/* --------------------------------------------------------------------
 *
 * Summary
 * -------
 * 
 *   Fetches the custom standard equipment description for the specified business unit
 * 
 * Parameters
 * ----------
 * 
 *   @BusinessUnitId int
 *	 @UsedNew int
 *	 @OptionCode nvarchar(10)
 * 
 * Exceptions
 * ----------
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[CustomStandardEquipment#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		SELECT businessUnitId, UsedNew, OptionCode, CustomText
		FROM settings.CustomStandardEquipment
		WHERE businessUnitId=@BusinessUnitId
		
END

GO

GRANT EXECUTE ON [Settings].[CustomStandardEquipment#Fetch] TO MerchandisingUser
GO  