if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[GetCustomStandardEquipmentDesc]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[GetCustomStandardEquipmentDesc]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sombke, Bryant
-- Create date: <04/27/2010>
-- =============================================
/* --------------------------------------------------------------------
 *
 * Summary
 * -------
 * 
 *   Fetches the custom standard equipment description for the specified business unit
 * 
 * Parameters
 * ----------
 * 
 *   @BusinessUnitId int
 *	 @UsedNew int
 *	 @OptionCode nvarchar(10)
 * 
 * Exceptions
 * ----------
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[GetCustomStandardEquipmentDesc]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
 	@UsedNew int,
 	@OptionCode nvarchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		SELECT CustomText
		FROM settings.CustomStandardEquipment
		WHERE businessUnitId=@BusinessUnitId
		AND UsedNew=@UsedNew
		AND Optioncode=@OptionCode
		
END

GO

GRANT EXECUTE ON [Settings].[GetCustomStandardEquipmentDesc] TO MerchandisingUser
GO   