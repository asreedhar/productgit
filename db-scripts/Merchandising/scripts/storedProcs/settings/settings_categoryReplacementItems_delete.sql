if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[CategoryReplacementItems#Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[CategoryReplacementItems#Delete]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/08/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_categoryReplacementItems_delete.sql,v 1.1 2009/07/27 23:55:41 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch the category replacements
 * 
 * 
 * Parameters
 * ----------
 * 
 * @ReplacementItemId int
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[CategoryReplacementItems#Delete]
	-- Add the parameters for the stored procedure here
	@ReplacementId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DELETE FROM settings.categoryReplacement_Item
	where replacementId = @ReplacementId
	
END

GO

GRANT EXECUTE ON [Settings].[CategoryReplacementItems#Delete] TO MerchandisingUser 
GO