IF object_id('settings.AutoWindowSticker#Fetch') is not null
	DROP PROCEDURE settings.AutoWindowSticker#Fetch
GO


CREATE PROCEDURE settings.AutoWindowSticker#Fetch
	@BusinessUnitId INT
AS
BEGIN

	SELECT AutoWindowStickerId, BusinessUnitId, Email, EmailBounced, CreatedOn, CreatedBy, UpdatedOn, UpdatedBy, MinimumDays
		FROM settings.AutoWindowSticker
		WHERE BusinessUnitId = @BusinessUnitId

END
GO

grant execute on settings.AutoWindowSticker#Fetch to MerchandisingUser
GO

