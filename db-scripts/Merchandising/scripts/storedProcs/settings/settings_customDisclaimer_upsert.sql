if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[CustomDisclaimer#Upsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[CustomDisclaimer#Upsert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sombke, Bryant
-- Create date: <04/27/2010>
-- =============================================
/* --------------------------------------------------------------------
 *
 * Summary
 * -------
 * 
 *   Inserts/updates the custom disclaimer for the specified business unit
 * 
 * Parameters
 * ----------
 * 
 *   @BusinessUnitId int,
 *   @isAppend bit,
 *	 @customText NVARCHAR(255)
 * 
 * Exceptions
 * ----------
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[CustomDisclaimer#Upsert]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@isAppend bit,
	@customText NVARCHAR(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM settings.CustomDisclaimer WHERE businessUnitID = @businessUnitID)
	BEGIN
		UPDATE settings.CustomDisclaimer
		SET isAppend=@isAppend, customText=@customText
		WHERE businessUnitId=@businessUnitId
	END
	ELSE
	BEGIN
		INSERT INTO settings.CustomDisclaimer (businessUnitId, isAppend, customText)
		VALUES (@businessUnitId, @isAppend, @customText)
	END
	
END
GO

GRANT EXECUTE ON [Settings].[CustomDisclaimer#Upsert] TO MerchandisingUser
GO  