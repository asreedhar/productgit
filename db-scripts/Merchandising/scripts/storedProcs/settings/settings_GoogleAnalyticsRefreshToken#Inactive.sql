if object_id('settings.GoogleAnalyticsRefreshToken#Inactive') is not null
	drop procedure settings.GoogleAnalyticsRefreshToken#Inactive
go

create procedure settings.GoogleAnalyticsRefreshToken#Inactive
	@businessUnitId int
as

set nocount on
set transaction isolation level read uncommitted

if exists (select 1 from Merchandising.settings.GoogleAnalyticsToken where BusinessUnitId = @businessUnitId and Active = 1)
	update Merchandising.settings.GoogleAnalyticsToken set Active = 0 WHERE BusinessUnitId = @businessUnitId and Active = 1

go

grant execute on settings.GoogleAnalyticsRefreshToken#Inactive to MerchandisingUser
go