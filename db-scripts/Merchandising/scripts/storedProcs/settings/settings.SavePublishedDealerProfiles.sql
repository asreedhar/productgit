IF object_id('settings.SavePublishedDealerProfiles') is not null
	DROP PROCEDURE settings.SavePublishedDealerProfiles
GO

CREATE PROCEDURE settings.SavePublishedDealerProfiles
	@DealerProfilesTable settings.DealerProfiles readonly
AS
BEGIN

declare 
	@ErrorMessage nvarchar(4000),
	@ErrorNumber int,
	@ErrorSeverity int,
	@ErrorState int,
	@ErrorLine int,
	@ErrorProcedure nvarchar(200);
        


begin tran

begin try

	delete from Merchandising.settings.DealerProfilePublishedSnapshot

	insert into Merchandising.settings.DealerProfilePublishedSnapshot
	( 
		BusinessUnitID ,
		BusinessUnitName ,
		BusinessUnitCode ,
		OwnerHandle ,
		GroupId ,
		[Address] ,
		City ,
		[Description] ,
		Email ,
		Phone ,
		Url ,
		LogoUrl,
		[State],
		ZipCode ,
		HasMaxForWebsite1 ,
		HasMaxForWebsite2 ,
		HasMobileShowroom ,
		HasShowroom,
		HasShowroomBookValuations,
		HasNewVehiclesEnabled,
		HasUsedVehiclesEnabled,
		HasProfitMax
		
	)
	select 
		BusinessUnitID ,
		BusinessUnitName ,
		BusinessUnitCode ,
		OwnerHandle ,
		GroupId ,
		[Address] ,
		City ,
		[Description] ,
		Email ,
		Phone ,
		Url ,
		LogoUrl,
		[State],
		ZipCode ,
		HasMaxForWebsite1 ,
		HasMaxForWebsite2 ,
		HasMobileShowroom ,
		HasShowroom,
		HasShowroomBookValuations,
		HasNewVehiclesEnabled,
		HasUsedVehiclesEnabled,
		HasProfitMax
	from @DealerProfilesTable
	
end try
begin catch
	if @@TRANCOUNT > 0 rollback;
	
	select  
		@ErrorNumber = error_number(),
		@ErrorSeverity = error_severity(),
		@ErrorState = error_state(),
		@ErrorLine = error_line(),
		@ErrorProcedure = isnull(error_procedure(), '-');

	select @ErrorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 'Message: '+ error_message();

	raiserror 
		(
		@ErrorMessage, 
		@ErrorSeverity, 
		1,               
		@ErrorNumber,
		@ErrorSeverity,
		@ErrorState,
		@ErrorProcedure,
		@ErrorLine
		);	

end catch

if @@TRANCOUNT > 0 commit;

END
GO

grant execute on settings.SavePublishedDealerProfiles to MerchandisingUser
GO
