IF object_id('settings.GetDealerProfilePublishingUpdates') is not null
	DROP PROCEDURE settings.GetDealerProfilePublishingUpdates
GO


CREATE PROCEDURE settings.GetDealerProfilePublishingUpdates
AS
BEGIN

	-- resultset 1: adds and updates
	select BusinessUnitID,BusinessUnitName,BusinessUnitCode,OwnerHandle,GroupId,Address,City,Description,Email,Phone,Url,State,ZipCode,HasMaxForWebsite1,HasMaxForWebsite2,HasMobileShowroom,HasShowroom,LogoUrl,HasShowroomBookValuations,HasNewVehiclesEnabled,HasUsedVehiclesEnabled, HasProfitMAX
	from Merchandising.settings.DealerProfile 
	except 
	select BusinessUnitID,BusinessUnitName,BusinessUnitCode,OwnerHandle,GroupId,Address,City,Description,Email,Phone,Url,State,ZipCode,HasMaxForWebsite1,HasMaxForWebsite2,HasMobileShowroom,HasShowroom,LogoUrl,HasShowroomBookValuations,HasNewVehiclesEnabled,HasUsedVehiclesEnabled, HasProfitMax
	from Merchandising.settings.DealerProfilePublishedSnapshot
	
	-- resultset 2: deletes
	select OwnerHandle DeleteOwnerHandle from Merchandising.settings.DealerProfilePublishedSnapshot
	except 
	select OwnerHandle from Merchandising.settings.DealerProfile

	-- resultset 3: entire snapshot	
	select * from Merchandising.settings.DealerProfile 
	

END
GO

grant execute on settings.GetDealerProfilePublishingUpdates to MerchandisingUser
GO
