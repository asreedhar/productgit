if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[merchandisingPreferences#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[merchandisingPreferences#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_merchandisingPreferences_Fetch.sql,v 1.2 2009/01/05 21:23:51 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * creates a new vehicle metric entry or updates the existing entry
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId int   - integer id of the businesUnit for which to get the alert list

 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[merchandisingPreferences#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--select the dealer's preferences
	SELECT * FROM settings.merchandising
		WHERE businessUnitId = @BusinessUnitId
		

END
GO

GRANT EXECUTE ON [Settings].[merchandisingPreferences#Fetch] TO MerchandisingUser 
GO