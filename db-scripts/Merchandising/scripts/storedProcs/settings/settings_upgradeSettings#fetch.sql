if object_id('settings.upgradeSettings#Fetch') is not null
	drop procedure settings.upgradeSettings#Fetch
go


CREATE PROCEDURE settings.upgradeSettings#Fetch
	@businessUnitID int
AS

	SET NOCOUNT ON
	set transaction isolation level read uncommitted

	SELECT	maxForSmartphone, autoWindowStickerTemplateId, MAXForWebsite20, MaxDigitalShowroom, UsePhoneMapping, UseDealerRater, EnableShowroomBookValuations, vehiclesInShowroom, EnableShowroomReports 
		FROM Merchandising.settings.Merchandising
		WHERE businessUnitID = @businessUnitID
		
	SET NOCOUNT OFF
RETURN
go

grant execute on settings.upgradeSettings#Fetch to MerchandisingUser
go
