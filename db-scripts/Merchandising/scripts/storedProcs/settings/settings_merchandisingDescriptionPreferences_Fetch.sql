if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[merchandisingDescriptionPreferences#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[merchandisingDescriptionPreferences#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_merchandisingDescriptionPreferences_Fetch.sql,v 1.6 2009/06/24 22:30:24 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * creates a new vehicle metric entry or updates the existing entry
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId int   - integer id of the businesUnit for which to get the alert list

 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[merchandisingDescriptionPreferences#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @prefBuid int
	
	IF EXISTS (SELECT 1 from settings.merchandisingDescriptionPreferences
			where businessUnitId = @BusinessUnitId)
		SELECT @prefBuid = @BusinessUnitId 
	ELSE
		SELECT @prefBuid = 100150


		SELECT mdp.*, ISNULL(adt.name, '') as defaultTemplateName,DP.BookOutPreferenceId,DP.GuideBook2BookOutPreferenceId,ISNULL(KCT.ShowRetail,0) KBBConsumerToolEnabled,CASE WHEN ISNULL(DU.Active,0)=1 AND ISNULL(DU.EffectiveDateActive,0)=1 THEN 1 ELSE 0 END  EdmudsActive FROM settings.merchandisingDescriptionPreferences mdp
		INNER JOIN IMT.dbo.DealerPreference DP on DP.BusinessUnitID=mdp.businessUnitId
		LEFT JOIN IMT.dbo.DealerUpgrade DU on DU.BusinessUnitID=DP.BusinessUnitID AND DU.DealerUpgradeCD=20 
		LEFT JOIN IMT.DBO.DealerPreference_KBBConsumerTool KCT on KCT.BusinessUnitID=DP.BusinessUnitID
		LEFT JOIN templates.adTemplates adt
			ON adt.templateId = mdp.defaultTemplateId
		WHERE mdp.businessUnitId = @prefBuid
		
	--select businessunit gas mileage prefs in second results set
	SELECT gmp.segmentId, s.segment, gmp.goodHighwayGasMileage, gmp.goodCityGasMileage FROM settings.gasMileagePreferences gmp
		INNER JOIN vehicleCatalog.FirstLook.Segment s
			ON s.segmentId = gmp.segmentId
		WHERE businessUnitId = @prefBuid


END
GO

GRANT EXECUTE ON [Settings].[merchandisingDescriptionPreferences#Fetch] TO MerchandisingUser 
GO