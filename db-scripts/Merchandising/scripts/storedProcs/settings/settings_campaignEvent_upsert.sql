if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[campaignEvent#Upsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [settings].[campaignEvent#Upsert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <2/5/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_campaignEvent_upsert.sql,v 1.1 2009/11/09 17:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * update or insert the campaign event
 * 
 * 
 * Parameters
 * ----------
 *  @CampaignEventId is NULL when performing an insert
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [settings].[campaignEvent#Upsert]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@VehicleAgeInDaysForAction int,
	@RegenerateDescription bit = false,
	@TemplateId int = null,
	@ThemeId int = null,
	@RepriceTypeId int,
	@RepricePercentage int = NULL,
	@CampaignEventId int = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF EXISTS (select 1
		FROM settings.CampaignEvents
		WHERE 
			businessUnitId = @BusinessUnitId
			AND campaignEventId = @CampaignEventId)
	BEGIN
		UPDATE settings.CampaignEvents
		SET
			[regenerateDescription] = @RegenerateDescription,
			[vehicleAgeInDaysForAction] = @VehicleAgeInDaysForAction,			
			[templateId] = @TemplateId,
			[themeId] = @ThemeId,
			[repriceTypeId] = @RepriceTypeId,
			[repricePercentage] = @RepricePercentage	
		WHERE
			businessUnitId = @BusinessUnitId AND
			campaignEventId = @CampaignEventId
			
	END
	ELSE
	BEGIN
		INSERT INTO settings.CampaignEvents
		(
			[businessUnitId],
			[vehicleAgeInDaysForAction],
			[regenerateDescription],
			[templateId],
			[themeId],
			[repriceTypeId],
			[repricePercentage]
		)
		VALUES
		(	
			@BusinessUnitId,
			@VehicleAgeInDaysForAction,
			@RegenerateDescription,
			@TemplateId,
			@ThemeId,
			@RepriceTypeId,
			@RepricePercentage	
		)
	END
	
	
	
END
GO

GRANT EXECUTE ON [settings].[campaignEvent#Upsert] TO MerchandisingUser 
GO