if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[equipmentDisplayRankings#Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[equipmentDisplayRankings#Update]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_equipmentDisplayRankings_Update.sql,v 1.2 2009/03/13 21:22:35 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Update the equipment rankings for a dealer
 * 
 * 
 * Parameters
 * ----------
 * 
 * @RankingID int   - integer id of the ranking item
 * @DisplayPriority int - lower number is higher priority
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[equipmentDisplayRankings#Update]
	-- Add the parameters for the stored procedure here
	@RankingID int,
	@DisplayPriority int,
	@Tier int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE settings.EquipmentDisplayRankings
    SET displayPriority = @DisplayPriority,
    tierNumber = @Tier
    WHERE rankingID = @RankingID
                    

END
GO

GRANT EXECUTE ON [Settings].[equipmentDisplayRankings#Update] TO MerchandisingUser 
GO