-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-04-18
-- Description:	GetAll MAX 3.0 Business Unit Ids
-- =============================================

USE Merchandising
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[settings].[MaxBusinessUnits#FetchByVersion]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [settings].MaxBusinessUnits#FetchByVersion
GO

CREATE PROCEDURE settings.MaxBusinessUnits#FetchByVersion 
	@MaxVersion int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT m.businessUnitID
	FROM Merchandising.settings.Merchandising m
	JOIN IMT.dbo.BusinessUnit b ON b.BusinessUnitID = m.businessUnitID
	WHERE 
		m.MaxVersion = @MaxVersion
		AND b.Active = 1
END
GO

grant execute on settings.MaxBusinessUnits#FetchByVersion to MerchandisingUser
GO

