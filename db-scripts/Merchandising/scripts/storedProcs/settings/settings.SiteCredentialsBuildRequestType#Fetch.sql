IF OBJECT_ID('settings.SiteCredentialsBuildRequestType#Fetch') IS NOT NULL
	DROP PROCEDURE [settings].[SiteCredentialsBuildRequestType#Fetch]
GO

CREATE PROCEDURE [settings].[SiteCredentialsBuildRequestType#Fetch]
	@BusinessUnitId int
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT DISTINCT Dealer_SiteCredentials.siteId, Dealer_SiteCredentials.buildRequestType
	FROM workflow.Inventory Inventory
	INNER JOIN VehicleCatalog.Chrome.Divisions Divisions ON Inventory.Make = Divisions.DivisionName 
		AND Divisions.CountryCode = 1
	INNER JOIN merchandising.VehicleAutoLoadSettings VehicleAutoLoadSettings ON Divisions.ManufacturerID = VehicleAutoLoadSettings.ManufacturerID
	INNER JOIN settings.Dealer_SiteCredentials Dealer_SiteCredentials ON VehicleAutoLoadSettings.siteId = Dealer_SiteCredentials.siteId 
		AND Inventory.BusinessUnitID = Dealer_SiteCredentials.businessUnitId
	WHERE Inventory.BusinessUnitID = @BusinessUnitId
	AND buildRequestType  = 2 -- slurpee
	AND Dealer_SiteCredentials.siteId IN (1,2) -- GM, BMW

GO

GRANT EXECUTE ON [settings].[SiteCredentialsBuildRequestType#Fetch] TO [MerchandisingUser]
GO
