IF OBJECT_ID('settings.BucketAlerts#Upsert') IS NOT NULL
	DROP PROCEDURE [settings].[BucketAlerts#Upsert]
GO

CREATE PROCEDURE [settings].[BucketAlerts#Upsert]
	@BusinessUnitId INT,
	@Active BIT,
	@Email VARCHAR(1000),
	@NewUsed INT,
	@User VARCHAR(80),
	@Bounced BIT,
	@WeeklyEmail VARCHAR(1000),
	@WeeklyBounced BIT,
	@BionicHealthReportEmail VARCHAR(1000),
	@BionicHealthReportEmailBounced BIT,
	@GroupBionicHealthReportEmail VARCHAR(1000),
	@GroupBionicHealthReportEmailBounced BIT
AS
BEGIN
	
	IF EXISTS (SELECT [BusinessUnitId] FROM [settings].[BucketAlerts] WHERE [BusinessUnitId] = @BusinessUnitId)
		UPDATE	[settings].[BucketAlerts]
		SET	[BusinessUnitId] = @BusinessUnitId,
			[Active] = @Active,
			[Email] = @Email,
			[NewUsed] = @NewUsed,
			[UpdatedOn] = GetDate(),
			[UpdatedBy] = @User,
			[EmailBounced] = @Bounced,
			[WeeklyEmail] = @WeeklyEmail,
			[WeeklyEmailBounced] = @WeeklyBounced,
			[BionicHealthReportEmail] = @BionicHealthReportEmail,
			[BionicHealthReportEmailBounced] = @BionicHealthReportEmailBounced,
			[GroupBionicHealthReportEmail] = @GroupBionicHealthReportEmail,
			[GroupBionicHealthReportEmailBounced] = @GroupBionicHealthReportEmailBounced
		WHERE	BusinessUnitId = @BusinessUnitId
	ELSE
		INSERT INTO settings.BucketAlerts
		(
			[BusinessUnitId],
			[Active],
			[Email],
			[NewUsed],
			[CreatedOn],
			[CreatedBy],
			[UpdatedOn],
			[UpdatedBy],
			[EmailBounced],
			[WeeklyEmail],
			[WeeklyEmailBounced],
			[BionicHealthReportEmail],
			[BionicHealthReportEmailBounced],
			[GroupBionicHealthReportEmail],
			[GroupBionicHealthReportEmailBounced]
		)
		VALUES
		(
			@BusinessUnitId,
			@Active,
			@Email,
			@NewUsed,
			GetDate(),
			@User,
			GetDate(),
			@User,
			@Bounced,
			@WeeklyEmail,
			@WeeklyBounced,
			@BionicHealthReportEmail,
			@BionicHealthReportEmailBounced,
			@GroupBionicHealthReportEmail,
			@GroupBionicHealthReportEmailBounced
		)
END
GO

GRANT EXECUTE ON [settings].[BucketAlerts#Upsert] TO [MerchandisingUser]
GO
