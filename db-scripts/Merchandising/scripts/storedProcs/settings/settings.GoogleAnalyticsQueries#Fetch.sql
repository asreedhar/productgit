USE [Merchandising]
GO

/****** Object:  StoredProcedure [settings].[GoogleAnalyticsQueries#Fetch]    Script Date: 05/01/2013 16:53:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[settings].[GoogleAnalyticsQueries#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [settings].[GoogleAnalyticsQueries#Fetch]
GO

USE [Merchandising]
GO

/****** Object:  StoredProcedure [settings].[GoogleAnalyticsQueries#Fetch]    Script Date: 05/01/2013 16:53:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [settings].[GoogleAnalyticsQueries#Fetch]
	-- Add the parameters for the stored procedure here
	@ProfileId int = 0, 
	@QueryType int = 0,
	@BusinessUnitId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		gq.QueryName, gq.Dimensions, gq.Filter, gq.Limit, gq.Metrics, gq.Segment, gq.Sort
	FROM Merchandising.settings.GoogleAnalyticsProfile gap
	JOIN Merchandising.settings.EdtDestinationQueries edq ON gap.DestinationId = edq.EdtDestinationId OR edq.EdtDestinationId = 0
	JOIN Merchandising.settings.GoogleQuery gq ON gq.QueryId = edq.GoogleQueryId
	JOIN Merchandising.settings.GoogleQueryType gqt ON gqt.QueryTypeId = gq.QueryType

	WHERE 
		gap.ProfileId = @ProfileId
		AND gqt.QueryTypeId = @QueryType
		AND gap.BusinessUnitId = @BusinessUnitId
END

GO
grant execute on [settings].[GoogleAnalyticsQueries#Fetch] to MerchandisingUser
GO



