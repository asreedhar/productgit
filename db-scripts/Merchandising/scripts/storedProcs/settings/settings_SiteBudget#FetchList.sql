use Merchandising
go

if object_id('settings.SiteBudget#FetchList') is not null
	drop procedure settings.SiteBudget#FetchList
go


create procedure settings.SiteBudget#FetchList
	@BusinessUnitId int,
	@MonthApplicable datetime
as

set nocount on
set transaction isolation level read uncommitted


Select b.* 
From (
	Select DestinationId, MAX(MonthApplicable) as MonthApplicable
	From settings.SiteBudget
	Where BusinessUnitId = @BusinessUnitId
	  and MonthApplicable <= @MonthApplicable
	Group By DestinationId
) as x 
Inner Join settings.SiteBudget b 
	On x.DestinationId = b.DestinationId 
	And x.MonthApplicable = b.MonthApplicable 
	And b.BusinessUnitId = @BusinessUnitId

go

grant execute on settings.SiteBudget#FetchList to MerchandisingUser
go

