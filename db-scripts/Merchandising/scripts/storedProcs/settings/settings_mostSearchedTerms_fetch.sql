if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[MostSearchedTerms#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[MostSearchedTerms#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/08/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_mostSearchedTerms_fetch.sql,v 1.1 2009/11/09 17:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch the most searched terms
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[MostSearchedTerms#Fetch]
	-- Add the parameters for the stored procedure here
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT * FROM settings.mostSearchedEquipment
	WHERE active = 1
	ORDER BY mostSearchedRanking asc
END

GO

GRANT EXECUTE ON [Settings].[MostSearchedTerms#Fetch] TO MerchandisingUser 
GO