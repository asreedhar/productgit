if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[previewPreferences#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [settings].[previewPreferences#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <2/13/10>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_previewPreferences_fetch.sql,v 1.1 2010/02/23 19:31:51 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Get the preview prefs for a dealer (used or new)
 * 
 * 
 * Parameters
 * ----------
 *  @BusinessUnitId int,
	@NewOrUsed int  1-New Vehicles  2-Used Vehicles
 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [settings].[previewPreferences#Fetch]
	@BusinessUnitId int,
	@NewOrUsed int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	
	IF NOT EXISTS (SELECT 1 FROM settings.PreviewPreferences 
		WHERE businessUnitId = @BusinessUnitId
		AND newOrUsed = @NewOrUsed
		)
	BEGIN
		SELECT @BusinessUnitId = 100150
	END
	
	
	
	SELECT * 
	FROM settings.previewPreferences
	WHERE businessUnitId = @BusinessUnitId	
	and newOrUsed = @NewOrUsed
	
END
GO

GRANT EXECUTE ON [settings].[previewPreferences#Fetch] TO MerchandisingUser 
GO