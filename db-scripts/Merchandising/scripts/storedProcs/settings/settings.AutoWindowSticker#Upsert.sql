IF object_id('settings.AutoWindowSticker#Upsert') is not null
	DROP PROCEDURE settings.AutoWindowSticker#Upsert
GO

CREATE PROCEDURE settings.AutoWindowSticker#Upsert
	@BusinessUnitId INT,
	@Email VARCHAR(1000),
	@EmailBounced BIT,
	@User VARCHAR(80),
	@MinimumDays smallint = 0
AS
BEGIN
	
	/*
	*	only one allowed right now per business unit!!!!!
	*
	*/

	IF EXISTS ( SELECT 1
				FROM settings.AutoWindowSticker
				WHERE BusinessUnitId = @BusinessUnitId)
		BEGIN
			UPDATE settings.AutoWindowSticker
			SET
				BusinessUnitId = @BusinessUnitId,
				Email = @Email,
				EmailBounced = @EmailBounced,
				MinimumDays = @MinimumDays,
				UpdatedOn = GetDate(),
				UpdatedBy = @User
			WHERE BusinessUnitId = @BusinessUnitId
		END
	ELSE
		BEGIN
			INSERT INTO settings.AutoWindowSticker
			(
				BusinessUnitId,
				Email,
				EmailBounced,
				CreatedOn,
				CreatedBy,
				UpdatedOn,
				UpdatedBy,
				MinimumDays
			)
			VALUES
			(
				@BusinessUnitId,
				@Email,
				@EmailBounced,
				GetDate(),
				@User,
				GetDate(),
				@User,
				@MinimumDays
			)
		END

END
GO

grant execute on settings.AutoWindowSticker#Upsert to MerchandisingUser
GO
