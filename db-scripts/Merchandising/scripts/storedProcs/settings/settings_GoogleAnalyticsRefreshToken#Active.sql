if object_id('settings.GoogleAnalyticsRefreshToken#Active') is not null
	drop procedure settings.GoogleAnalyticsRefreshToken#Active
go

create procedure settings.GoogleAnalyticsRefreshToken#Active
	@businessUnitId int
as

set nocount on
set transaction isolation level read uncommitted

if exists (select 1 from Merchandising.settings.GoogleAnalyticsToken where BusinessUnitId = @businessUnitId and Active = 1)
	select 1 as 'Exist' else select 0 as 'Exist'

go

grant execute on settings.GoogleAnalyticsRefreshToken#Active to MerchandisingUser
go