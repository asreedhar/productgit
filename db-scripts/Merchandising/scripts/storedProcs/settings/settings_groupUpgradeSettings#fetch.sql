if object_id('settings.groupUpgradeSettings#Fetch') is not null
	drop procedure settings.groupUpgradeSettings#Fetch
go


CREATE PROCEDURE settings.groupUpgradeSettings#Fetch
	@groupId int
AS

	SET NOCOUNT ON
	set transaction isolation level read uncommitted

	SELECT	Merchandising.businessUnitID, maxForSmartphone, autoWindowStickerTemplateId, MAXForWebsite20, MaxDigitalShowroom, UsePhoneMapping, UseDealerRater, EnableShowroomBookValuations, vehiclesInShowroom, EnableShowroomReports 
	FROM Merchandising.settings.Merchandising
	JOIN IMT.dbo.BusinessUnitRelationship br ON br.BusinessUnitID = Merchandising.businessUnitID
	WHERE br.ParentID = @groupId
		
	SET NOCOUNT OFF
RETURN
go

grant execute on settings.groupUpgradeSettings#Fetch to MerchandisingUser
go
