if object_id('settings.GoogleAnalyticsProfile#Upsert') is not null
	drop procedure settings.GoogleAnalyticsProfile#Upsert
go

create procedure settings.GoogleAnalyticsProfile#Upsert
	@businessUnitId int,
	@type int,
	@profileId int,
	@profileName varchar(100),
	@destinationId int = 0
as

set nocount on
set transaction isolation level read uncommitted

if exists (select 1 from Merchandising.settings.GoogleAnalyticsProfile where BusinessUnitId = @businessUnitId and Type = @type)
	begin
	
	update Merchandising.settings.GoogleAnalyticsProfile
	set ProfileId = @profileId, ProfileName = @profileName, DestinationId = @destinationId
	where BusinessUnitId = @businessUnitId and Type = @type

	end
else
	begin
	
	insert into Merchandising.settings.GoogleAnalyticsProfile (BusinessunitId, Type, ProfileId, ProfileName, DestinationId)
	values(@businessUnitId, @type, @profileId, @profileName, @destinationId)

	end

go

grant execute on settings.GoogleAnalyticsProfile#Upsert to MerchandisingUser
go