if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[equipmentDisplayRankings#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[equipmentDisplayRankings#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_equipmentDisplayRankings_Fetch.sql,v 1.1 2009/01/05 21:23:51 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch the equipment rankings for a dealer
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId int   - integer id of the businesUnit for which to get the alert list
 * @TierNumber int

 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[equipmentDisplayRankings#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@TierNumber int = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    SELECT 
		rankingID, 
		categoryDesc,
		displayPriority,
		displayDescription,
		isSticky
    FROM settings.EquipmentDisplayRankings 
    WHERE businessUnitID = @BusinessUnitID 
		AND (@TierNumber IS NULL OR tierNumber = @TierNumber)
    ORDER BY displayPriority ASC
	

END
GO

GRANT EXECUTE ON [Settings].[equipmentDisplayRankings#Fetch] TO MerchandisingUser 
GO