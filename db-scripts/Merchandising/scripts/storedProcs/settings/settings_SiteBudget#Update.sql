use Merchandising
go

if object_id('settings.SiteBudget#Update') is not null
	drop procedure settings.SiteBudget#Update
go


create procedure settings.SiteBudget#Update
	@BusinessUnitId int,
	@DestinationId int,
	@MonthApplicable datetime,
	@Budget int,
	@DescriptionText varchar(32)
as

set nocount on
set transaction isolation level read uncommitted

set @MonthApplicable = dateadd(month, datediff(month, 0, @MonthApplicable), 0) -- first of the month


if exists (select * from settings.SiteBudget
	where BusinessUnitId = @BusinessUnitId
	  and DestinationId = @DestinationId
	  and MonthApplicable = @MonthApplicable)
begin

	-- PK already exists in db
	update settings.SiteBudget
	set Budget = @Budget,
	    DescriptionText = @DescriptionText,
	    UpdatedOn = GetDate()
	where BusinessUnitId = @BusinessUnitId
	  and DestinationId = @DestinationId
	  and MonthApplicable = @MonthApplicable

end
else
begin
	
	-- PK does not exist
	insert into settings.SiteBudget
	   (BusinessUnitId
	   ,DestinationId
	   ,MonthApplicable
	   ,DescriptionText
	   ,Budget
	   ,UpdatedOn)
	values
	   (@BusinessUnitId
	   ,@DestinationId
	   ,@MonthApplicable
	   ,@DescriptionText
	   ,@Budget
	   ,GetDate());

end

return 0

go

grant execute on settings.SiteBudget#Update to MerchandisingUser
go


