if exists (select * from dbo.sysobjects where id = object_id(N'[Settings].[CustomStandardEquipment#Upsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Settings].[CustomStandardEquipment#Upsert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sombke, Bryant
-- Create date: <04/27/2010>
-- =============================================
/* --------------------------------------------------------------------
 *
 * Summary
 * -------
 * 
 *   Inserts/updates the custom standard equipment description for the specified business unit
 * 
 * Parameters
 * ----------
 * 
 *   @BusinessUnitId int,
 *   @UsedNew int,
 *	 @OptionCode NVARCHAR(10),
 *	 @customText NVARCHAR(255)
 * 
 * Exceptions
 * ----------
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Settings].[CustomStandardEquipment#Upsert]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
    @UsedNew int,
 	@OptionCode NVARCHAR(10),
 	@customText NVARCHAR(255)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM settings.CustomStandardEquipment
				WHERE businessUnitID=@businessUnitID
				AND UsedNew=@UsedNew
				AND OptionCode=@OptionCode)
	BEGIN
		UPDATE settings.CustomStandardEquipment
		SET CustomText=@customText
		WHERE businessUnitId=@businessUnitId
		AND UsedNew=@UsedNew
		AND OptionCode=@OptionCode
	END
	ELSE
	BEGIN
		INSERT INTO settings.CustomStandardEquipment (businessUnitId, UsedNew, OptionCode, CustomText)
		VALUES (@BusinessUnitId, @UsedNew, @OptionCode, @customText)
	END
	
END
GO

GRANT EXECUTE ON [Settings].[CustomStandardEquipment#Upsert] TO MerchandisingUser
GO   