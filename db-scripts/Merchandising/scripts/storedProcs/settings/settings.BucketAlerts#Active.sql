IF OBJECT_ID('settings.BucketAlerts#Active') IS NOT NULL
	DROP PROCEDURE [settings].[BucketAlerts#Active]
GO

CREATE PROCEDURE [settings].[BucketAlerts#Active]
AS
BEGIN
	SELECT	BA.[BusinessUnitId],
			[EmailBounced],
			[WeeklyEmailBounced],
			[BionicHealthReportEmailBounced],
			[GroupBionicHealthReportEmailBounced],
			[Email],
			[WeeklyEmail],
			[BionicHealthReportEmail],
			[GroupBionicHealthReportEmail]
	FROM	[settings].[BucketAlerts] BA
	JOIN	(
				SELECT	[BusinessUnitId]
				FROM	[settings].[BucketAlertsThreshold]
				WHERE	[EmailActive] = 1
				GROUP BY	[BusinessUnitId]
			) BAT	ON	BA.[BusinessUnitId] = BAT.[BusinessUnitId]
	WHERE	(
				([Email] IS NOT NULL AND LEN(RTRIM([Email])) != 0) OR
				([WeeklyEmail] IS NOT NULL AND LEN(RTRIM([WeeklyEmail])) != 0) OR
				([BionicHealthReportEmail] IS NOT NULL AND LEN(RTRIM([BionicHealthReportEmail])) != 0) OR
				([GroupBionicHealthReportEmail] IS NOT NULL AND LEN(RTRIM([GroupBionicHealthReportEmail])) != 0)
			)
END
GO

GRANT EXECUTE ON [settings].[BucketAlerts#Active] TO [MerchandisingUser]
GO

