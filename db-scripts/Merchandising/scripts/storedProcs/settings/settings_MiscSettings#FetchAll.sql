IF OBJECT_ID('settings.MiscSettings#FetchAll') IS NOT NULL
	DROP PROCEDURE [settings].[MiscSettings#FetchAll]
GO

CREATE PROCEDURE [settings].[MiscSettings#FetchAll]
AS
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT	[BusinessUnitId],
			[sendOptimalFormat],
			[franchiseId],
			[priceNewCars],
			[showDashboard],
			[ShowGroupLevelDashboard],
			[analyticsSuite],
			[batchAutoload],
			[MaxVersion],
			[WebLoaderEnabled],
			[ModelLevelFrameworksEnabled],
			[AutoOffline_WholesalePlanTrigger],
			[ShowCtrGraph],
			[DealershipSegment],
			[BulkPriceNewCars],
			[showOnlineClassifiedOverview],
			[ShowTimeToMarket],
			[GIDProviderNew],
			[GIDProviderUsed],
			[DMSName],
			[DMSEmail],
			[ReportDataSourceNew],
			[ReportDataSourceUsed]
	FROM	[settings].[Merchandising]

GO

GRANT EXECUTE ON [settings].[MiscSettings#FetchAll] TO [MerchandisingUser]
GO
