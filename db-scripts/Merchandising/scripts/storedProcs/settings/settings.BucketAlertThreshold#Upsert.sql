IF object_id('settings.BucketAlertThreshold#Upsert') is not null
	DROP PROCEDURE settings.BucketAlertThreshold#Upsert
GO

CREATE PROCEDURE settings.BucketAlertThreshold#Upsert
	@Id INT = NULL,	
	@BusinessUnitId INT,
	@Bucket INT,
	@Active BIT,
	@Limit INT,
	@EmailActive BIT -- FB 27993
AS
BEGIN

	IF NOT EXISTS (SELECT 1
						FROM settings.BucketAlertsThreshold
						WHERE Id = @Id)
		BEGIN
			-- find by BusinessID and Bucket?
			SELECT @Id = id
				FROM settings.BucketAlertsThreshold BucketAlertsThreshold
				WHERE BucketAlertsThreshold.BusinessUnitId = @BusinessUnitId
				AND BucketAlertsThreshold.Bucket = @Bucket
				
			IF @@ROWCOUNT = 0
				SET @Id = NULL
		END
		
	IF @Id IS NOT NULL
		BEGIN
			UPDATE settings.BucketAlertsThreshold
			SET
				BusinessUnitId = @BusinessUnitId,
				Bucket = @Bucket,
				Active = @Active,
				Limit = @Limit,
				EmailActive = @EmailActive
			WHERE Id = @Id
		END
	ELSE
		BEGIN
			INSERT INTO settings.BucketAlertsThreshold
			(
				BusinessUnitId,
				Bucket,
				Active,
				Limit,
				EmailActive
			)
			VALUES
			(
				@BusinessUnitId,
				@Bucket,
				@Active,
				@Limit,
				@EmailActive
			)
		END

END
GO

grant execute on settings.BucketAlertThreshold#Upsert to MerchandisingUser
GO
