if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[previewPreferences#Upsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [settings].[previewPreferences#Upsert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <2/5/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: settings_previewPreferences_upsert.sql,v 1.1 2010/02/23 19:31:51 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * update or insert the dealer's preview preferences
 * 
 * 
 * Parameters
 * ----------
 * @PreferenceId is NULL when performing an insert
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [settings].[previewPreferences#Upsert]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@NewOrUsed int,
	@PreferenceId int = NULL,
	@useMissionMarker bit,
    @usePriceBelowBook bit,
    @useOneOwner bit,
    @usePriceReduced bit,
    @useCertified bit,
    @useVehicleCondition bit,
    @useVehicleConditionStatements bit,
    @useJdPowerRatings bit,
    @useModelAwards bit,
    @useExpertReviews bit,
    @useCrashTestRatings bit,
    @useWarranties bit,
    @useFuelEconomy bit,
    @useLowMilesPerYear bit,
    @useFinancingSpecial bit,
    @useTrim bit,
    @useColors bit,
    @useKeyInfoCheckboxes bit,
    @useKeyVehicleEquipment bit,
    @useDrivetrain bit
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF EXISTS (select 1
		FROM settings.previewPreferences
		WHERE 
			businessUnitId = @BusinessUnitId
			AND PreferenceId = @PreferenceId)
	BEGIN
		UPDATE settings.previewPreferences
		SET
			newOrUsed = @newOrUsed,
			useMissionMarker = @useMissionMarker,
			usePriceBelowBook = @usePriceBelowBook,
			useOneOwner = @useOneOwner,
			usePriceReduced = @usePriceReduced,
			useCertified = @useCertified,
			useVehicleCondition = @useVehicleCondition,
			useVehicleConditionStatements = @useVehicleConditionStatements,
			useJdPowerRatings = @useJdPowerRatings,
			useModelAwards = @useModelAwards,
			useExpertReviews = @useExpertReviews,
			useCrashTestRatings = @useCrashTestRatings,
			useWarranties = @useWarranties,
			useFuelEconomy = @useFuelEconomy,
			useLowMilesPerYear = @useLowMilesPerYear,
			useFinancingSpecial = @useFinancingSpecial,
			useTrim = @useTrim,
			useColors = @useColors,
			useKeyInfoCheckboxes = @useKeyInfoCheckboxes,
			useKeyVehicleEquipment = @useKeyVehicleEquipment,
			useDrivetrain = @useDrivetrain
		WHERE
			businessUnitId = @BusinessUnitId AND
			PreferenceId = @PreferenceId
			
	END
	ELSE
	BEGIN
		INSERT INTO settings.previewPreferences
		(			
			BusinessUnitId,
			newOrUsed,
			useMissionMarker,
			usePriceBelowBook,
			useOneOwner,
			usePriceReduced,
			useCertified,
			useVehicleCondition,
			useVehicleConditionStatements,
			useJdPowerRatings,
			useModelAwards,
			useExpertReviews,
			useCrashTestRatings,
			useWarranties,
			useFuelEconomy,
			useLowMilesPerYear,
			useFinancingSpecial,
			useTrim,
			useColors,
			useKeyInfoCheckboxes,
			useKeyVehicleEquipment,
			useDrivetrain
		)
		VALUES
		(				
			@BusinessUnitId,
			@newOrUsed,
			@useMissionMarker,
			@usePriceBelowBook,
			@useOneOwner,
			@usePriceReduced,
			@useCertified,
			@useVehicleCondition,
			@useVehicleConditionStatements,
			@useJdPowerRatings,
			@useModelAwards,
			@useExpertReviews,
			@useCrashTestRatings,
			@useWarranties,
			@useFuelEconomy,
			@useLowMilesPerYear,
			@useFinancingSpecial,
			@useTrim,
			@useColors,
			@useKeyInfoCheckboxes,
			@useKeyVehicleEquipment,
			@useDrivetrain
		)
	END
	
	
	
END
GO

GRANT EXECUTE ON [settings].[previewPreferences#Upsert] TO MerchandisingUser 
GO