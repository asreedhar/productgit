USE [Merchandising]
GO

/****** Object:  StoredProcedure [dashboard].[WebsiteMetrics#FetchRange]    Script Date: 05/01/2013 16:53:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dashboard].[WebsiteMetrics#FetchRange]') AND type in (N'P', N'PC'))
	DROP PROCEDURE [dashboard].[WebsiteMetrics#FetchRange]
GO

USE [Merchandising]
GO

/****** Object:  StoredProcedure [settings].[GoogleAnalyticsQueries#FetchRange]    Script Date: 05/01/2013 16:53:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dashboard].[WebsiteMetrics#FetchRange]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int = 0, 
	@StartDate DateTime = NULL,
	@EndDate DateTime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT 
		wm.MonthApplicable,
		wm.NewVDPs,
		wm.UsedVDPs,
		wm.WebsiteProviderID,
		WebsiteProviderName = COALESCE( edt.description, wpf.Name )
	FROM dashboard.WebsiteMetrics wm
	JOIN settings.Merchandising merch               ON merch.businessUnitID = @BusinessUnitId
	LEFT JOIN settings.EdtDestinations edt          ON wm.WebsiteProviderID = edt.destinationID
	LEFT JOIN settings.WebsiteProvidersFreeform wpf ON wm.WebsiteProviderID = wpf.WebsiteProviderID
	WHERE wm.BusinessUnitID = @BusinessUnitId
		AND ( @StartDate IS NULL OR wm.MonthApplicable >= @StartDate )
		AND ( @EndDate IS NULL   OR wm.MonthApplicable <= @EndDate )
		AND wm.WebsiteProviderID = merch.SelectedWebsiteProvider
		
END

GO

grant execute on [dashboard].[WebsiteMetrics#FetchRange] to MerchandisingUser
GO

