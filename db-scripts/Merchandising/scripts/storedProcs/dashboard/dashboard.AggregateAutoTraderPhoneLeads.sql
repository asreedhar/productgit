if object_id('dashboard.AggregateAutoTraderPhoneLeads') is not null
	drop procedure dashboard.AggregateAutoTraderPhoneLeads
go

/*
	this process will re-write phone leads every time, because the CallTracking file is processed
	before the performance summary file... no way to correlate them, so we'll just re-aggregate
	everyday until we get this whole metrics system up in hadoop/Amazon cloud
	
*/
create procedure dashboard.AggregateAutoTraderPhoneLeads
	@LoadID int
as
set nocount on
set transaction isolation level read uncommitted


-- first, upsert the stats from this LoadId into postings.PhoneLeadsAutoTraderAggregation

; with LoadStats as
(
	select
		businessUnitId,
		Month,
		[1] as NewPhoneLeads,
		[2] as UsedPhoneLeads
	from
		(
			select
				businessUnitId,
				Month = dateadd(month, datediff(month, 0, LeadDate), 0),
				InventoryType
			from 
				postings.PhoneLeads ph
			where
				ph.Load_ID = @LoadID
		) as sourceTable
		pivot
		(
			count(InventoryType)
			for InventoryType in ([1],[2])
		) as p	
)
update pla set
	NewPhoneLeads = ls.NewPhoneLeads + pla.NewPhoneLeads,
	UsedPhoneLeads = ls.UsedPhoneLeads + pla.UsedPhoneLeads
from
	Merchandising.postings.PhoneLeadsAutoTraderAggregation pla
	join LoadStats ls
		on pla.businessUnitId = ls.businessUnitId
		and pla.Month = ls.Month
	


; with LoadStats as
(
	select
		businessUnitId,
		Month,
		[1] as NewPhoneLeads,
		[2] as UsedPhoneLeads
	from
		(
			select
				businessUnitId,
				Month = dateadd(month, datediff(month, 0, LeadDate), 0),
				InventoryType
			from 
				postings.PhoneLeads ph
			where
				ph.Load_ID = @LoadID
		) as sourceTable
		pivot
		(
			count(InventoryType)
			for InventoryType in ([1],[2])
		) as p	
)
insert Merchandising.postings.PhoneLeadsAutoTraderAggregation
(
	businessUnitId,
	Month,
	NewPhoneLeads,
	UsedPhoneLeads
)
select
	businessUnitId,
	Month,
	NewPhoneLeads,
	UsedPhoneLeads
from LoadStats ls
where
	not exists
	(
		select *
		from Merchandising.postings.PhoneLeadsAutoTraderAggregation
		where
			businessUnitId = ls.businessUnitId
			and Month = ls.Month
	)
		


-- now, update dashboard.PerformanceSummary with everything in postings.PhoneLeadsAutoTraderAggregation
update metrics set
	NewPhoneLeads = pla.NewPhoneLeads,
	UsedPhoneLeads = pla.UsedPhoneLeads
from
	Merchandising.dashboard.PerformanceSummary metrics
	join Merchandising.postings.PhoneLeadsAutoTraderAggregation pla
		on metrics.BusinessUnitId = pla.businessUnitID
		and metrics.DestinationId = 2
		and metrics.Month = pla.Month
	
go
