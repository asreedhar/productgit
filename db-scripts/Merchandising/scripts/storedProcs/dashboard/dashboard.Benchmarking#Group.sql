use Merchandising
go

if object_id('dashboard.Benchmarking#Group') is not null
	drop procedure dashboard.Benchmarking#Group
go


create procedure dashboard.Benchmarking#Group
	@GroupID int, 	
	@VehicleType tinyint
as

set nocount on
set transaction isolation level read uncommitted

;WITH cte as
	(
		SELECT ROW_NUMBER() OVER(PARTITION BY bu.businessunitid, SiteId ORDER BY Year DESC, Month DESC) as RowNumber, bu.businessunitid, SiteId, LeadCost, ImpressionCost
		from imt.dbo.BusinessunitRelationship bur
		join imt.dbo.Businessunit bu on bur.businessunitid = bu.businessunitid
		join Merchandising.settings.Merchandising s on bu.businessunitid = s.businessunitid
		join Merchandising.dbo.BenchmarkingData bmd on bmd.DealershipSegmentType = s.DealershipSegment
		where VehicleType = @VehicleType and bur.ParentID = @GroupID
	)
	select businessunitid, SiteId, LeadCost, ImpressionCost
	from cte
	where RowNumber = 1

go

grant execute on dashboard.Benchmarking#Group to MerchandisingUser
go




