if object_id('dashboard.UpdateInventoryIdAutoTraderHendrickOnline') is not null
	drop procedure dashboard.UpdateInventoryIdAutoTraderHendrickOnline
go

/*
	ATHendrick Online feed does not include BuId or a suitable translation,
	use Vin/Stock to look up the BuId/InvId pair and write back to the table
	using the interface proc dashboard.GetBusinessUnitIdAndInventoryId 
*/
create procedure dashboard.UpdateInventoryIdAutoTraderHendrickOnline
	@LoadId int
as
set nocount on
set transaction isolation level read uncommitted

declare
	@id int,
	@Vin varchar(17),
	@StockNumber varchar(15),
	@FeedDate datetime,
	@return int,
	@errorMessage varchar(max)
 
 
 declare @inputs table
 (
	id int identity(0,1) not null,
	Vin varchar(17) not null,
	StockNumber varchar(15) not null,
	FeedDate datetime not null
 )
 
insert @inputs
(
	Vin,
	StockNumber,
	FeedDate
)
select
	Vin,
	StockNumber,
	FeedDate
from Merchandising.merchandising.AutoTraderHendrickOnline
where LoadId = @LoadId


while (1=1)
begin
	select top 1 
		@id = id,
		@Vin = Vin,
		@StockNumber = StockNumber,
		@FeedDate = FeedDate
	from @inputs
	if (@@rowcount <> 1) break
	delete @inputs where id = @id
	
	-- a return value of 1 means we couldn't look up the inventoryId, 0 is success, anything else is a failure
	exec @return = Merchandising.dashboard.GetBusinessUnitIdAndInventoryId 
		@DatasourceInterfaceID = 2009,
		@BusinessUnitId = null,
		@RequestId = null,
		@Month = null,
		@Vin = @Vin,
		@StockNumber = @StockNumber,
		@InventoryEffectiveDate = @FeedDate,
		@InventoryId = null,
		@IsForOnline = 1
	
	if (@return not in (0, 1))
	begin
		set @errorMessage = 
			'failed to exec GetBusinessUnitIdAndInventoryId from UpdateInventoryIdAutoTraderHendrickOnline!'
			+ char(10) + '@return = ' + isnull(convert(varchar(max), @return), '<null>')
			+ char(10) + '@LoadId = ' + isnull(convert(varchar(max), @LoadId), '<null>')
			+ char(10) + '@Vin = ' + isnull(convert(varchar(max), @Vin), '<null>')
			+ char(10) + '@StockNumber = ' + isnull(convert(varchar(max), @StockNumber), '<null>')
			+ char(10) + '@FeedDate = ' + isnull(convert(varchar(max), @FeedDate ), '<null>')

		raiserror(@errorMessage, 16, 1)
		return 1
	end
end	




return 0

go	