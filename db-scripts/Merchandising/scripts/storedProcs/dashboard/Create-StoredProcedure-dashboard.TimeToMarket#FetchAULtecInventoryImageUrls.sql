-- ==============================================
-- Author:		Travis Huber
-- Create date: 2013-04-17
-- Description:	Get vins for photos that are not 
--				in the TimeToMarketTracking table 
--				but are in the External feed
-- ==============================================
USE Merchandising
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dashboard].[TimeToMarket#FetchAULtecInventoryImageUrls]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE dashboard.TimeToMarket#FetchAULtecInventoryImageUrls
GO

CREATE PROCEDURE dashboard.TimeToMarket#FetchAULtecInventoryImageUrls
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int, 
	@StartDate DateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @LatestFileId int;
	SET @LatestFileId = (SELECT TOP 1 Datafeeds.Extract.AULtec#InventoryImageURLs.ExtractFlatFileID FROM Datafeeds.Extract.AULtec#InventoryImageURLs WHERE BusinessUnitID = @BusinessUnitId ORDER BY ExtractFlatFileID DESC);

	DECLARE @BuVinsNeedPhotos TABLE (
		BusinessUnitId int,
		InventoryId int,
		StockNumber nvarchar(MAX),
		VIN nvarchar(MAX)
	)

	INSERT INTO @BuVinsNeedPhotos
	EXEC Merchandising.dashboard.TimeToMarket#VehiclesWithNoPhotosFetch @BusinessUnitId, @StartDate;

	SELECT VIN, ImageURLs 
	FROM Datafeeds.Extract.AULtec#InventoryImageURLs 
	WHERE ExtractFlatFileID = @LatestFileId 
		AND BusinessUnitID = @BusinessUnitID
		AND ImageURLs IS NOT NULL 
		AND VIN IN (
			SELECT VIN FROM @BuVinsNeedPhotos
		)
END
GO

grant execute on dashboard.TimeToMarket#FetchAULtecInventoryImageUrls to MerchandisingUser
GO

USE Datafeeds
GO
grant select on Extract.AULtec#InventoryImageURLs to MerchandisingWebsite
GO

