if object_id('dashboard.DealerListingSiteInventoryCountsTrend#Group#NewAndUsed') is not null
	drop procedure dashboard.DealerListingSiteInventoryCountsTrend#Group#NewAndUsed
GO

create procedure dashboard.DealerListingSiteInventoryCountsTrend#Group#NewAndUsed
	@GroupID int, 
	@StartDate datetime, 
	@EndDate datetime 
as

set transaction isolation level read uncommitted
set nocount on


select
	bu.businessunit,
	bu.businessunitid,
	d.description,
	d.DestinationId,
	count(distinct a.inventoryId) as count,
	a.month
from 
	imt.dbo.BusinessunitRelationship bur
	join imt.dbo.Businessunit bu
		on bur.businessunitid = bu.businessunitid
	join Merchandising.dashboard.VehicleActivityInventoryCounts a
		on bu.BusinessUnitID = a.BusinessUnitID
	join Merchandising.settings.EdtDestinations d
		on d.destinationID = a.DestinationId
where
	bur.ParentID = @GroupID 
	and bu.Active = 1 
	and a.Month between @StartDate and @EndDate
group by
	bu.businessunit,
	bu.businessunitid,
	d.description,
	d.DestinationId,
	a.month
option (maxdop 1)	


GO

