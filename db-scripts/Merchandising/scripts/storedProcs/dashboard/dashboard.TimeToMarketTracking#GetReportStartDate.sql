-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-06-20
-- Description:	Get the report start date per BusinessUnit
-- =============================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dashboard].[TimeToMarket#GetReportStartDate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dashboard].[TimeToMarket#GetReportStartDate]
GO


CREATE PROCEDURE dashboard.TimeToMarket#GetReportStartDate 
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT [FirstPhotoReportStartDate], [ReportStartDate], [CarsApiStartDate] FROM Merchandising.dashboard.TimeToMarketTracking#BuReportStart
    WHERE BusinessUnitId = @BusinessUnitId
END
GO

GRANT EXECUTE on dashboard.TimeToMarket#GetReportStartDate to MerchandisingUser
GO
