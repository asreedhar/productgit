USE [Merchandising]

if object_id('dashboard.GetSitePerformanceData#Group') is not null
	drop procedure dashboard.GetSitePerformanceData#Group
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
USE [Merchandising]
GO
/****** Object:  StoredProcedure [dashboard].[GetSitePerformanceData#Group]    Script Date: 11/29/2011 11:41:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dashboard].[GetSitePerformanceData#Group]
	@groupID int, 
	@VehicleType int,
	@StartDate datetime,
	@EndDate datetime
	
as

set nocount on
set transaction isolation level read uncommitted
	
if (@VehicleType = 0)
begin
	-- used and new
	select
	SearchPageViewCountInPeriod = sum(m.NewSearchPageViews + m.UsedSearchPageViews),
	DetailPageViewCountInPeriod = sum(m.NewDetailPageViews + m.UsedDetailPageViews),
	EmailLeadsCountInPeriod = sum(m.NewEmailLeads + m.UsedEmailLeads),
	MapsViewedCountInPeriod = sum(m.NewMapsViewed + m.UsedMapsViewed),
	AdPrintedCountInPeriod = sum(m.NewAdPrints + m.UsedAdPrints),
	ChatRequestsCountInPeriod = sum(isnull(m.NewChatRequests,0) + isnull(m.UsedChatRequests,0)),
	PhoneLeadsCountInPeriod = sum(isnull(m.NewPhoneLeads,0) + isnull(m.UsedPhoneLeads,0)),
	bu.businessunit,
	bu.businessunitid,
	DestinationName = d.description,
	m.DestinationId,
	m.Month
	from imt.dbo.BusinessunitRelationship bur
		join imt.dbo.Businessunit bu on bur.businessunitid = bu.businessunitid
		join Merchandising.dashboard.PerformanceSummary m on m.businessunitid = bu.businessunitid
		join Merchandising.settings.EdtDestinations d on d.destinationID = m.DestinationId
	where
		bur.ParentID = @groupID and bu.Active = 1
		and m.month between @StartDate and @EndDate
	group by
		bu.businessunit,
		bu.businessunitid,
		d.description,
		m.DestinationId,
		m.month

end
else if (@VehicleType = 1)
begin
	-- new
	select
	SearchPageViewCountInPeriod = sum(m.NewSearchPageViews),
	DetailPageViewCountInPeriod = sum(m.NewDetailPageViews),
	EmailLeadsCountInPeriod = sum(m.NewEmailLeads),
	MapsViewedCountInPeriod = sum(m.NewMapsViewed),
	AdPrintedCountInPeriod = sum(m.NewAdPrints),
	ChatRequestsCountInPeriod = sum(isnull(m.NewChatRequests,0)),
	PhoneLeadsCountInPeriod = sum(isnull(m.NewPhoneLeads,0)),
	bu.businessunit,
	bu.businessunitid,
	DestinationName = d.description,
	m.DestinationId,
	m.Month
	from imt.dbo.BusinessunitRelationship bur
		join imt.dbo.Businessunit bu on bur.businessunitid = bu.businessunitid
		join Merchandising.dashboard.PerformanceSummary m on m.businessunitid = bu.businessunitid
		join Merchandising.settings.EdtDestinations d on d.destinationID = m.DestinationId
	where
		bur.ParentID = @groupID and bu.Active = 1
		and m.month between @StartDate and @EndDate
	group by
		bu.businessunit,
		bu.businessunitid,
		d.description,
		m.DestinationId,
		m.Month
		
end
else if (@VehicleType = 2)
begin
	-- used
	select
	SearchPageViewCountInPeriod = sum(m.UsedSearchPageViews),
	DetailPageViewCountInPeriod = sum(m.UsedDetailPageViews),
	EmailLeadsCountInPeriod = sum(m.UsedEmailLeads),
	MapsViewedCountInPeriod = sum(m.UsedMapsViewed),
	AdPrintedCountInPeriod = sum(m.UsedAdPrints),
	ChatRequestsCountInPeriod = sum(isnull(m.UsedChatRequests,0)),
	PhoneLeadsCountInPeriod = sum(isnull(m.UsedPhoneLeads,0)),
	bu.businessunit,
	bu.businessunitid,
	DestinationName = d.description,
	m.DestinationId,
	m.Month
	from imt.dbo.BusinessunitRelationship bur
		join imt.dbo.Businessunit bu on bur.businessunitid = bu.businessunitid
		join Merchandising.dashboard.PerformanceSummary m on m.businessunitid = bu.businessunitid
		join Merchandising.settings.EdtDestinations d on d.destinationID = m.DestinationId
	where
		bur.ParentID = @groupID and bu.Active = 1
		and m.month between @StartDate and @EndDate
	group by
		bu.businessunit,
		bu.businessunitid,
		d.description,
		m.DestinationId,
		m.Month
end


GO

GRANT EXECUTE ON dashboard.GetSitePerformanceData#Group TO MerchandisingUser 
GO