if object_id('dashboard.CalculateInventoryDifferentials') is not null
	drop procedure dashboard.CalculateInventoryDifferentials
go

/*
	This proc takes an incoming file from fetch by @LoadId,
	and for every Vin/Month combination, if the record is later
	than the last record we've saved in SiteMetricsByStoreDateInventory,
	we upsert this data point.
	
	This data eventually supports the low activity report, which
	is based on data that is rolled up into SiteMetricsByStoreInventory
	which represents a lifetime
*/
create procedure dashboard.CalculateInventoryDifferentials
	@LoadId int,
	@DatasourceInterfaceID int,
	@destinationID int,
	@debugVin varchar(17) = null,
	@debugStockNumber varchar(15) = null,
	@useVinMigration bit = 0
as
set nocount on
set transaction isolation level read uncommitted

if exists(select * from Merchandising.dashboard.InventoryAdPerformance where LoadId = @LoadId)
begin
	raiserror('Can not process LoadId %d, it has already been processed!', 16, 1)
	return 1
end


-- NOTE Vin should be varchar(17), StockNumber varchar(15) but some of the data is bigger, just let it fail BuId/InvId lookup

declare
	@id int
	, @BusinessUnitId int
	, @Vin varchar(50)
	, @StockNumber varchar(50)
	, @InventoryId int
	, @Month datetime
	, @LogicalDate datetime
	
	, @SearchPageViews int
	, @DetailPageViews int
	, @EmailLeads int
	, @MapsViewed int
	, @AdPrints int

	, @PrevMonth datetime
	, @PrevLogicalDate datetime
	, @PrevSearchPageViews int
	, @PrevDetailPageViews int
	, @PrevEmailLeads int
	, @PrevMapsViewed int
	, @PrevAdPrints int


	, @DupeBoundarySearchPageViews int
	, @DupeBoundaryDetailPageViews int
	, @DupeBoundaryEmailLeads int
	, @DupeBoundaryMapsViewed int
	, @DupeBoundaryAdPrints int
	
			
	, @error varchar(max)
	, @debug bit
	, @requestId int
	, @queueLoadId int
		
	, @prevInputExists bit
	, @prevMetricsAreFlatlined bit

	, @AvgSearchPageViewsRatio float(24)
	, @AvgDetailPageViewsRatio float(24)
	, @AvgMapsViewedRatio float(24)
	, @AvgAdPrintsRatio float(24)
	
	, @SmallDrop bit
	
	, @DateCollected datetime
	
declare @NoInvIdSkipRows int ;  set @NoInvIdSkipRows = 0





set @debug = 1


set @debug = 
	case
		when @debugVin is not null and @debugStockNumber is null then null
		when @debugVin is null and @debugStockNumber is not null then null
		when @debugVin is not null and @debugStockNumber is not null then 1
		when @DatasourceInterfaceId = 2010 and (@debugVin is not null or @debugStockNumber is not null) then null
		else 0
	end

if (@debug is null)
begin
	raiserror('@debugVin and @debugStockNumber both need to be null to operate in normal mode, or non-null to operate in single vehicle debug mode! (can not debug @DatasourceInterfaceId 2010', 16, 1)
	return 1
end

	
if (@debug = 1 and @useVinMigration = 1)
begin
	raiserror('@debugVin and @debugStockNumber can be set OR @useVinMigration, but not both (trim your dashboard.VinMigration table if necessary)', 16, 1)
	return 1
end



declare @Inputs table
(
	id int not null identity (0,1) primary key clustered
	, BusinessUnitId int null
	, InventoryId int null
	, Vin varchar(50) null
	, StockNumber varchar(50) null
	, Month datetime not null
	, LogicalDate datetime not null
	, SearchPageViews int not null
	, DetailPageViews int not null
	, EmailLeads int not null
	, MapsViewed int not null
	, AdPrints int not null
	
	, requestId int null
	, LoadId int not null
	, DateCollected datetime not null
)


declare @BusinessUnitMonth table
(
	BusinessUnitId int not null
	, Month datetime not null
)

declare @ratios table
(
	BusinessUnitId int not null,
	DestinationId int not null,
	ReportMonth datetime not null,
	UsedSearchPageViewsRatio float(24) not null,
	UsedDetailPageViewsRatio float(24) not null,
	UsedMapsViewedRatio float(24) not null,
	UsedAdPrintsRatio float(24) not null
)


-- declare an output InventoryAdPerformance table

declare @Outputs table
(
	BusinessUnitId int not null,
	InventoryId int not null,
	DestinationId int not null,
	SearchPageViews int not null,
	DetailPageViews int not null,
	EmailLeads int not null,
	MapsViewed int not null,
	AdPrints int not null,
	Boundary bit not null,
	Date datetime not null,
	LoadId int not null,
	SmallDrop bit not null,
	
	primary key clustered 
	(
		BusinessUnitId,
		InventoryId,
		DestinationId,
		Date
	)
)

declare @Files table
(
	LoadId int not null
)


-- if we have previously processed any LoadId that are greater than the one we are currently doing, we must reprocess all that data:
insert @Files (LoadId)
select distinct LoadId
from Merchandising.dashboard.InventoryAdPerformance metrics
where
	LoadId > @LoadId
	and exists
	(
		select *
		from DBASTAT.dbo.Dataload_History
		where
			Load_ID = metrics.LoadId
			and DatasourceInterfaceID = @DatasourceInterfaceID
	)


if not exists(select * from @Files where LoadId = @LoadId)
begin
	insert @Files (LoadId) values (@LoadId)
end



if (@debug = 0 and @useVinMigration = 0)
begin
	insert @Inputs
	(
		BusinessUnitId
		, InventoryId
		, Vin
		, StockNumber
		, Month
		, LogicalDate
		, SearchPageViews
		, DetailPageViews
		, EmailLeads
		, MapsViewed
		, AdPrints
		, requestId
		, LoadId
		, DateCollected
	)
	select
		businessUnitID
		, InventoryId
		, Vin
		, StockNumber
		, Month
		, LogicalDate
		, SearchPageViews
		, DetailPageViews
		, EmailLeads
		, MapsViewed
		, AdPrints
		, requestId
		, Load_ID
		, DateCollected
	from dashboard.RawMetrics
	where
		DatasourceInterfaceId = @DatasourceInterfaceID
		and Load_ID in (select LoadId from @Files)
	order by Load_ID, LogicalDate, DbTimestamp
end
else if (@debug = 1)
begin
	insert @Inputs
	(
		BusinessUnitId
		, InventoryId
		, Vin
		, StockNumber
		, Month
		, LogicalDate
		, SearchPageViews
		, DetailPageViews
		, EmailLeads
		, MapsViewed
		, AdPrints
		, requestId
		, LoadId
		, DateCollected		
	)
	select
		businessUnitID
		, InventoryId
		, Vin
		, StockNumber
		, Month
		, LogicalDate
		, SearchPageViews
		, DetailPageViews
		, EmailLeads
		, MapsViewed
		, AdPrints
		, requestId
		, Load_ID
		, DateCollected
	from dashboard.RawMetrics
	where
		DatasourceInterfaceId = @DatasourceInterfaceID
		and Load_ID in (select LoadId from @Files)
		and Vin = @debugVin
		and StockNumber = @debugStockNumber
	order by Load_ID, LogicalDate, DbTimestamp
end
else if (@useVinMigration = 1 and @DatasourceInterfaceID = 2006)
begin
	insert @Inputs
	(
		BusinessUnitId
		, InventoryId
		, Vin
		, StockNumber
		, Month
		, LogicalDate
		, SearchPageViews
		, DetailPageViews
		, EmailLeads
		, MapsViewed
		, AdPrints
		, requestId
		, LoadId
		, DateCollected
	)
	select
		businessUnitID
		, InventoryId
		, Vin
		, StockNumber
		, Month
		, LogicalDate
		, SearchPageViews
		, DetailPageViews
		, EmailLeads
		, MapsViewed
		, AdPrints
		, requestId
		, Load_ID
		, DateCollected
	from dashboard.RawMetrics m
	where
		DatasourceInterfaceId = @DatasourceInterfaceID
		and Load_ID in (select LoadId from @Files)
		and exists
		(
			select *
			from dashboard.VinMigration
			where
				DatasourceInterfaceId = m.DatasourceInterfaceId
				and Vin = m.VIN
				and StockNumber = m.StockNumber
		)
	order by Load_ID, LogicalDate, DbTimestamp
end
else if (@useVinMigration = 1 and @DatasourceInterfaceID <> 2006)
begin
	insert @Inputs
	(
		BusinessUnitId
		, InventoryId
		, Vin
		, StockNumber
		, Month
		, LogicalDate
		, SearchPageViews
		, DetailPageViews
		, EmailLeads
		, MapsViewed
		, AdPrints
		, requestId
		, LoadId
		, DateCollected
	)
	select
		businessUnitID
		, InventoryId
		, Vin
		, StockNumber
		, Month
		, LogicalDate
		, SearchPageViews
		, DetailPageViews
		, EmailLeads
		, MapsViewed
		, AdPrints
		, requestId
		, Load_ID
		, DateCollected
	from dashboard.RawMetrics m
	where
		DatasourceInterfaceId = @DatasourceInterfaceID
		and Load_ID in (select LoadId from @Files)
		and exists
		(
			select *
			from dashboard.VinMigration
			where
				DatasourceInterfaceId = m.DatasourceInterfaceId
				and BusinessUnitId = m.businessUnitID
				and Vin = m.VIN
				and StockNumber = m.StockNumber
		)
	order by Load_ID, LogicalDate, DbTimestamp
end


if (@DatasourceInterfaceID = 2004)
begin
	/*
	 Load the distinct businessunits and months we have in our input.
	 This is used in a couple of different places when we are processing
	 this particular input (datasourceinterfaceid = 2004)
	*/
	insert @BusinessUnitMonth
	(
		BusinessUnitId
		, Month
	)
	select distinct
		businessUnitId
		, month
	from @inputs
		
end



if (@DatasourceInterfaceID = 2006)
begin
	-- AutoTraderHendrick feed doesn't populate report month, and the SSIS package that
	-- loads it doesn't know about it, so the column is set to default getdate(),
	-- here we update it to match the feed date:
	
	update merchandising.merchandising.AutoTraderHendrickVehicleSummary set
		Month = dateadd(day, datediff(day, 0, FeedDate), 0)
	where LoadId = @LoadId
end




-- apply the feed blacklist, it's easier to just take the extra perf hit here than to
-- wrap this up into all the various debug interfaces about for loading the input queue:
delete i
from
	@Inputs i
	join Merchandising.dashboard.BusinessUnitFeedBlacklist b
		on i.businessUnitId = b.BusinessUnitId
where
	b.DatasourceInterfaceId = @DatasourceInterfaceId
	
	
	
	
-- ok, now that everything is loaded up, start looping over the input data to create our output:
	
begin try

	while (1=1)
	begin
		select top 1
			@id = id
			, @BusinessUnitId = BusinessUnitId
			, @InventoryId = InventoryId
			, @Vin = Vin
			, @StockNumber = StockNumber
			, @Month = Month
			, @LogicalDate = LogicalDate
			, @SearchPageViews = SearchPageViews
			, @DetailPageViews = DetailPageViews
			, @EmailLeads = EmailLeads
			, @MapsViewed = MapsViewed
			, @AdPrints = AdPrints
		
			, @requestId = requestId
			, @queueLoadId = LoadId
		
			, @DateCollected = DateCollected	
			-- variable resets
			
			, @PrevMonth = null
			, @PrevLogicalDate = null
			, @PrevSearchPageViews = null
			, @PrevDetailPageViews = null
			, @PrevEmailLeads = null
			, @PrevMapsViewed = null
			, @PrevAdPrints = null
	
			, @prevInputExists = 0
			, @prevMetricsAreFlatlined = 0
			, @SmallDrop = 0
		from @Inputs
		order by id
			
		if (@@rowcount <> 1) break
		delete @Inputs where id = @id
		
		
		if (@debug = 1)
		begin
			select 'just popped off input queue', Month = @Month, 
			LogicalDate = @LogicalDate,
			SearchPageViews = @SearchPageViews
		end
		
		if (@BusinessUnitId is null or @InventoryId is null)
		begin
			-- lookup InventoryId (and write it to the raw input data)
			exec Merchandising.dashboard.GetBusinessUnitIdAndInventoryId
				@DatasourceInterfaceID = @DatasourceInterfaceID,
				@BusinessUnitId = @BusinessUnitId output,
				@RequestId = @RequestId,
				@Month = @Month,
				@Vin = @Vin,
				@StockNumber = @StockNumber,
				@InventoryEffectiveDate = @LogicalDate,
				@InventoryId = @InventoryId output
		end
		
		if (@BusinessUnitId is null or @InventoryId is null)
		begin
			-- we can't do any kind of stats for a car we can't look up
			set @NoInvIdSkipRows = @NoInvIdSkipRows + 1
			if (@debug = 1) print 'could not look up BuId/InvId'
			continue
		end



		if
		(
			-- cars.com scraped after they changed to lifetime SPV
			@DatasourceInterfaceId = 2004
			and @LogicalDate >= convert(datetime, '20120906', 112)
		)
		begin
			-- cars.com lifetime SPV algorithm
			if (@debug = 1)
			begin
				select 'debug, cars lifetime SPV algorith'
			end
			
			if exists
			(
				-- we've already processed this scrape date, or, there are dates logged in the future of this one
				select
					*
				from
					(
						select
							BusinessUnitId,
							InventoryId,
							DestinationId,
							Date
						from Merchandising.dashboard.InventoryAdPerformance iap
						where not exists(select * from @Files where LoadId = iap.LoadId)
						
						union all
						
						select
							BusinessUnitId,
							InventoryId,
							DestinationId,
							Date
						from @Outputs
					) a
				where
					BusinessUnitId = @BusinessUnitId
					and InventoryId = @InventoryId
					and DestinationId = @DestinationId
					and Date >= @LogicalDate
			)
			begin
				if (@debug = 1)
				begin
					select 'debug, cars lifetime SPV algorithm, ignoring dupe inputs'
				end
				
				continue
			end
			
			-- get previous data points from input
			select top 1
				@PrevMonth = Month
				, @PrevLogicalDate = LogicalDate
				, @PrevSearchPageViews = SearchPageViews
				, @PrevDetailPageViews = DetailPageViews
				, @PrevEmailLeads = EmailLeads
				, @PrevMapsViewed = MapsViewed
				, @PrevAdPrints = AdPrints
			from Merchandising.dashboard.RawMetrics
			where
				DatasourceInterfaceId = @DatasourceInterfaceID
				and businessUnitID = @businessUnitID
				and inventoryId = @inventoryId
				and LogicalDate < @LogicalDate
				and datediff(month, Month, DateCollected) = 0
			order by InventoryId, LogicalDate desc, DbTimestamp desc
			
			if (@@rowcount = 1)
			begin
				-- if previous input exists, subtract current stats from it
				-- if < 0, just zero it out


				if (@debug = 1)
				begin
					select 'debug, cars lifetime SPV algorithm, subtracting current stats from previous input'
					, [@SearchPageViews] = @SearchPageViews
					, [@DetailPageViews] = @DetailPageViews
					, [@EmailLeads] = @EmailLeads
					, [@MapsViewed] = @MapsViewed
					, [@AdPrints] = @AdPrints
					, [@SmallDrop] = @SmallDrop

					, [@PrevSearchPageViews] = @PrevSearchPageViews
					, [@PrevDetailPageViews] = @PrevDetailPageViews
					, [@PrevEmailLeads] = @PrevEmailLeads
					, [@PrevMapsViewed] = @PrevMapsViewed
					, [@PrevAdPrints] = @PrevAdPrints
				end


				
				select
					@SearchPageViews = case when @SearchPageViews - @PrevSearchPageViews > 0 then @SearchPageViews - @PrevSearchPageViews else 0 end
					, @DetailPageViews = case when @DetailPageViews - @PrevDetailPageViews > 0 then @DetailPageViews - @PrevDetailPageViews else 0 end
					, @EmailLeads = case when @EmailLeads - @PrevEmailLeads > 0 then @EmailLeads - @PrevEmailLeads else 0 end
					, @MapsViewed = case when @MapsViewed - @PrevMapsViewed > 0 then @MapsViewed - @PrevMapsViewed else 0 end
					, @AdPrints = case when @AdPrints - @PrevAdPrints > 0 then @AdPrints - @PrevAdPrints else 0 end
					
					
			end
			else
			begin
			
				select
					@PrevSearchPageViews = 0
					, @PrevDetailPageViews = 0
					, @PrevEmailLeads = 0
					, @PrevMapsViewed = 0
					, @PrevAdPrints = 0
					
				if (@debug = 1)
				begin
					select 'debug, cars lifetime SPV algorithm, no previous input to subtract from'
				end					
					
			end
			
			-- now, since we want to add the differentials we just calculated to the previous output, we need the previous output:
			select top 1
				@PrevSearchPageViews = SearchPageViews
				, @PrevDetailPageViews = DetailPageViews
				, @PrevEmailLeads = EmailLeads
				, @PrevMapsViewed = MapsViewed
				, @PrevAdPrints = AdPrints
			from
			(
				select
					BusinessUnitId,
					InventoryId,
					DestinationId,
					Date,
					Boundary,
					SearchPageViews,
					DetailPageViews,
					EmailLeads,
					MapsViewed,
					AdPrints
				from Merchandising.dashboard.InventoryAdPerformance iap
				where not exists(select * from @Files where LoadId = iap.LoadId)
				
				union all
				
				select
					BusinessUnitId,
					InventoryId,
					DestinationId,
					Date,
					Boundary,
					SearchPageViews,
					DetailPageViews,
					EmailLeads,
					MapsViewed,
					AdPrints
				from @Outputs
				
			) a   
			where
				BusinessUnitId = @BusinessUnitId
				and InventoryId = @InventoryId
				and DestinationId = @DestinationId
				and Date < @LogicalDate
			order by
				Date desc
		
			if (@@rowcount = 0)
			begin
				-- zero out the stats in case there was no previous output
				select
					@PrevSearchPageViews = 0
					, @PrevDetailPageViews = 0
					, @PrevEmailLeads = 0
					, @PrevMapsViewed = 0
					, @PrevAdPrints = 0
			end
			
			goto outputStorage
		end -- cars lifetime SPV algorithm





		-- look to see if this date already exists in the output?  if so shift the graph
		select
			@PrevSearchPageViews = SearchPageViews
			, @PrevDetailPageViews = DetailPageViews
			, @PrevEmailLeads = EmailLeads
			, @PrevMapsViewed = MapsViewed
			, @PrevAdPrints = AdPrints
		from
			(
				select
					BusinessUnitId,
					InventoryId,
					DestinationId,
					Date,
					SearchPageViews,
					DetailPageViews,
					EmailLeads,
					MapsViewed,
					AdPrints
				from Merchandising.dashboard.InventoryAdPerformance iap
				where not exists(select * from @Files where LoadId = iap.LoadId)
				
				union all
				
				select
					BusinessUnitId,
					InventoryId,
					DestinationId,
					Date,
					SearchPageViews,
					DetailPageViews,
					EmailLeads,
					MapsViewed,
					AdPrints
				from @Outputs
			) a
		where
			BusinessUnitId = @BusinessUnitId
			and InventoryId = @InventoryId
			and DestinationId = @DestinationId
			and Date = @LogicalDate
			
		if (@@rowcount = 1)
		begin
			-- we definitely have a value in our output for this scrape date already
			
			
			if (@DatasourceInterfaceId = 2010)
			begin
				-- cars.com direct gives us 7 days worth of data per vehicle in the file every day, once we've processed a day, ignore it
				continue
			end
			
			
			-- look in the base table and look in the temp invadperformance table (union)
			select top 1
				@DupeBoundarySearchPageViews = SearchPageViews
				, @DupeBoundaryDetailPageViews = DetailPageViews
				, @DupeBoundaryEmailLeads = EmailLeads
				, @DupeBoundaryMapsViewed = MapsViewed
				, @DupeBoundaryAdPrints = AdPrints
			from
				(
					select
						BusinessUnitId,
						InventoryId,
						DestinationId,
						Date,
						Boundary,
						SearchPageViews,
						DetailPageViews,
						EmailLeads,
						MapsViewed,
						AdPrints
					from Merchandising.dashboard.InventoryAdPerformance iap
					where not exists(select * from @Files where LoadId = iap.LoadId)
					
					union all
					
					select
						BusinessUnitId,
						InventoryId,
						DestinationId,
						Date,
						Boundary,
						SearchPageViews,
						DetailPageViews,
						EmailLeads,
						MapsViewed,
						AdPrints
					from @Outputs
					
				) a   
			where
				BusinessUnitId = @BusinessUnitId
				and InventoryId = @InventoryId
				and DestinationId = @DestinationId
				and Boundary = 1
				and Date < @LogicalDate
			order by Date desc

			if (@@rowcount = 0)
			begin
				select
					@DupeBoundarySearchPageViews = 0
					, @DupeBoundaryDetailPageViews = 0
					, @DupeBoundaryEmailLeads = 0
					, @DupeBoundaryMapsViewed = 0
					, @DupeBoundaryAdPrints = 0
			end			
			-- derive the previous input for this replacement input
			select
				@PrevSearchPageViews = @PrevSearchPageViews - @DupeBoundarySearchPageViews
				, @PrevDetailPageViews = @PrevDetailPageViews - @DupeBoundaryDetailPageViews
				, @PrevEmailLeads = @PrevEmailLeads - @DupeBoundaryEmailLeads
				, @PrevMapsViewed = @PrevMapsViewed - @DupeBoundaryMapsViewed
				, @PrevAdPrints = @PrevAdPrints - @DupeBoundaryAdPrints


			-- calculate the difference between the previous input and the replacement input so we can 
			-- use this to adjust the rest of the data points
			select
				@PrevSearchPageViews = @SearchPageViews - @PrevSearchPageViews
				, @PrevDetailPageViews = @DetailPageViews - @PrevDetailPageViews
				, @PrevEmailLeads = @EmailLeads - @PrevEmailLeads
				, @PrevMapsViewed = @MapsViewed - @PrevMapsViewed
				, @PrevAdPrints = @AdPrints - @PrevAdPrints
				
				
			-- Add the adjustment amount to the data points that were recorded since
			update Merchandising.dashboard.InventoryAdPerformance set
				SearchPageViews = SearchPageViews + @PrevSearchPageViews
				, DetailPageViews = DetailPageViews + @PrevDetailPageViews
				, EmailLeads = EmailLeads + @PrevEmailLeads
				, MapsViewed = MapsViewed + @PrevMapsViewed
				, AdPrints = AdPrints + @PrevAdPrints
			where
				BusinessUnitId = @BusinessUnitId
				and InventoryId = @InventoryId
				and DestinationId = @DestinationId
				and Date >= @LogicalDate

			update @Outputs set
				SearchPageViews = SearchPageViews + @PrevSearchPageViews
				, DetailPageViews = DetailPageViews + @PrevDetailPageViews
				, EmailLeads = EmailLeads + @PrevEmailLeads
				, MapsViewed = MapsViewed + @PrevMapsViewed
				, AdPrints = AdPrints + @PrevAdPrints
			where
				BusinessUnitId = @BusinessUnitId
				and InventoryId = @InventoryId
				and DestinationId = @DestinationId
				and Date >= @LogicalDate
				

			if (@debug = 1) print 'shifted the graph due to duplicate data point'

			continue

		end -- duplicate input logic
			
			
			
			
		if (@DatasourceInterfaceId = 2010)
		begin
			-- cars.com direct gives us simple metrics:  the ACTUAL SPV, DPV etc per day, not a cumulative running total.
			-- simply add today's metrics to most recent previous day's metrics.
			
			select top 1
				@PrevSearchPageViews = SearchPageViews
				, @PrevDetailPageViews = DetailPageViews
				, @PrevEmailLeads = EmailLeads
				, @PrevMapsViewed = MapsViewed
				, @PrevAdPrints = AdPrints
			from
			(
				select
					BusinessUnitId,
					InventoryId,
					DestinationId,
					Date,
					Boundary,
					SearchPageViews,
					DetailPageViews,
					EmailLeads,
					MapsViewed,
					AdPrints
				from Merchandising.dashboard.InventoryAdPerformance iap
				where not exists(select * from @Files where LoadId = iap.LoadId)
				
				union all
				
				select
					BusinessUnitId,
					InventoryId,
					DestinationId,
					Date,
					Boundary,
					SearchPageViews,
					DetailPageViews,
					EmailLeads,
					MapsViewed,
					AdPrints
				from @Outputs
				
			) a   
			where
				BusinessUnitId = @BusinessUnitId
				and InventoryId = @InventoryId
				and DestinationId = @DestinationId
				and Date < @LogicalDate
			order by
				Date desc
		
			if (@@rowcount = 0)
			begin
				-- zero out the stats in case there was no previous output
				select
					@PrevSearchPageViews = 0
					, @PrevDetailPageViews = 0
					, @PrevEmailLeads = 0
					, @PrevMapsViewed = 0
					, @PrevAdPrints = 0
			end
		
			goto outputStorage
		end -- cars.com direct special processing
					
					

		-- get previous data points from input
		
		select top 1
			@PrevMonth = Month
			, @PrevLogicalDate = LogicalDate
			, @PrevSearchPageViews = SearchPageViews
			, @PrevDetailPageViews = DetailPageViews
			, @PrevEmailLeads = EmailLeads
			, @PrevMapsViewed = MapsViewed
			, @PrevAdPrints = AdPrints
		from Merchandising.dashboard.RawMetrics
		where
			DatasourceInterfaceId = @DatasourceInterfaceID
			and businessUnitID = @businessUnitID
			and inventoryId = @inventoryId
			and LogicalDate < @LogicalDate
		order by InventoryId, LogicalDate desc, DbTimestamp desc
		
		if (@@rowcount <> 0) set @prevInputExists = 1
		
		
		if (@debug = 1)
		begin
		
		
			select 
				'pulling prev values before looking for flatline pattern / main algorithm stuff',
				[@LogicalDate] = @LogicalDate,
				[@prevInputExists] = @prevInputExists,
				[@PrevLogicalDate] = @PrevLogicalDate,
				[@PrevMonth] = @PrevMonth,
				[@PrevSearchPageViews] = @PrevSearchPageViews
		end
			
			


		if (@debug = 1)
		begin
			select 'debug small drop', SearchPageViews
			from Merchandising.dashboard.InventoryAdPerformance
			where
				businessUnitID = @businessUnitID
				and InventoryId = @InventoryId
				and DestinationId = @destinationId
				and Date =
				(
					select max(Date)
					from Merchandising.dashboard.InventoryAdPerformance
					where
						businessUnitID = @businessUnitID
						and InventoryId = @InventoryId
						and DestinationId = @destinationId
						and Date < @LogicalDate
				)
				and SmallDrop = 1
				
				
			select 'debug small drop calculation answer'	,				@SearchPageViews / 
					(isnull
					(
						nullif
						(
							(
								-- check to see if previous input was written as a small drop output
								select SearchPageViews
								from Merchandising.dashboard.InventoryAdPerformance
								where
									businessUnitID = @businessUnitID
									and InventoryId = @InventoryId
									and DestinationId = @destinationId
									and Date =
									(
										select max(Date)
										from Merchandising.dashboard.InventoryAdPerformance
										where
											businessUnitID = @businessUnitID
											and InventoryId = @InventoryId
											and DestinationId = @destinationId
											and Date < @LogicalDate
									)
									and SmallDrop = 1
							),
							0
						),
						-1
					)
					* 1.0) 
					
		end							
									
		
		/*

			In one specific case where a vehicle is listed late in a month we can lose SPV due to
			the fact that the SPV for the first listing month is so low there is never a "drop" or
			reset in the second listing month.  The second variable in this case is that in the past,
			Fetch has often scraped the prior month on the first of any given month due to the fact
			that the remote sites don't always publish their data on the first of a month.  While this
			is a Fetch bug that appears to be fixed, it still exists in a lot of older data.
			
			The data for this case (for a given vehicle, which avg SPV of 33/day) looks like this:
			
			
			<Report>Month	Scraped		SearchPageViews		Description
			-------------	-------		---------------		---------------------------------------------------------------------------------
			January			Jan 31		33					these are the inital SPV for the car when it was listed late Jan
			February		Feb 1		33					these are still Jan's SPV even though they are being reported as Feb
			February		Feb 2		66					these are the actual SPV for both Feb 1 & 2, sites and/or Fetch finally caught up
			
						
			Without this special code here, which sets the variable @prevMetricsAreFlatlined, the output of the above input will be exactly
			like the input:
			
			
			Date		SearchPageViews
			-------		---------------
			Jan 31		33			
			Feb 1		33			
			Feb 2		66			
			
			
			However, the output should really look like this since the 66 SPV on Feb 2 are unique to the 33 SPV on Jan 31:
			
			
			Date		SearchPageViews
			-------		---------------
			Jan 31		33			
			Feb 1		33			
			Feb 2		99			

			
			Note that the code below to detect this issue is in a specific order for performance (least
			expensive clauses to most expensive)
			
		*/
		
		if
		(
			(
				-- it takes two points to make a flat line 
				select count(*)
				from Merchandising.dashboard.RawMetrics
				where
					DatasourceInterfaceId = @DatasourceInterfaceID
					and businessUnitID = @businessUnitID
					and inventoryId = @inventoryId
					and LogicalDate < @LogicalDate
			) > 1
			and exists
			(
				-- this flat line has to have at least one data point in the past month
				select *
				from Merchandising.dashboard.RawMetrics
				where
					DatasourceInterfaceId = @DatasourceInterfaceID
					and businessUnitID = @businessUnitID
					and inventoryId = @inventoryId
					and LogicalDate < @LogicalDate
					and Month < @Month
			)
			and exists
			(
				-- this flat line has to also have at least one data point in the current month
				select *
				from Merchandising.dashboard.RawMetrics
				where
					DatasourceInterfaceId = @DatasourceInterfaceID
					and businessUnitID = @businessUnitID
					and inventoryId = @inventoryId
					and LogicalDate < @LogicalDate
					and Month = @Month
			)
			and not exists
			(
				-- there are no previous datapoints that are not flat for this vehicle
				select *
				from Merchandising.dashboard.RawMetrics
				where
					DatasourceInterfaceId = @DatasourceInterfaceID
					and businessUnitID = @businessUnitID
					and inventoryId = @inventoryId
					and LogicalDate < @LogicalDate
					and SearchPageViews <> @PrevSearchPageViews
			)
		)
		begin
			set @prevMetricsAreFlatlined = 1
		end

		if (@prevInputExists = 0)
		begin
			-- no previous input means that there is also no previous boundaries
			select
				@PrevMonth = @Month
				, @PrevLogicalDate = null
				, @PrevSearchPageViews = 0
				, @PrevDetailPageViews = 0
				, @PrevEmailLeads = 0
				, @PrevMapsViewed = 0
				, @PrevAdPrints = 0
		end
		else if
		(
			@prevInputExists = 1
			and @PrevSearchPageViews > 0 -- protect against rare but real divide by zero error
			and
			(
				(@SearchPageViews / (@PrevSearchPageViews * 1.0) between .85 and .99999)
				or
				(
					@SearchPageViews / 
						(
							isnull
							(
								nullif
								(
									(
										-- check to see if previous input was written as a small drop output
										select SearchPageViews
										from Merchandising.dashboard.InventoryAdPerformance
										where
											businessUnitID = @businessUnitID
											and InventoryId = @InventoryId
											and DestinationId = @destinationId
											and Date =
											(
												select max(Date)
												from Merchandising.dashboard.InventoryAdPerformance
												where
													businessUnitID = @businessUnitID
													and InventoryId = @InventoryId
													and DestinationId = @destinationId
													and Date < @LogicalDate
											)
											and SmallDrop = 1
									),
									0
								),
								-1
							)
							* 1.0
						) between .85 and .99999
					)
			)
			and not
			-- this is a historical scrape when a new store is turned on, we always want to count it as a boundary but this small drop code has to run before boundary code
			(
				@LogicalDate = dateadd(day, -1, dateadd(month, 1, @Month))
				and not exists
				(
					select *
					from Merchandising.dashboard.RawMetrics
					where
						DatasourceInterfaceId = @DatasourceInterfaceID
						and businessUnitID = @businessUnitID
						and inventoryId = @inventoryId
						and Month = @Month
						and LogicalDate <> @LogicalDate
				)
			)
			
		)
		begin
			-- "small drop" code:  basically ignore any input in the small drop category,
			-- just store this date's statistics as equivalent to the value before the small drop
				
			-- always reset these variables, even if there wasn't a previous small drop
			select
				@SearchPageViews = 0
				, @DetailPageViews = 0
				, @EmailLeads = 0
				, @MapsViewed = 0
				, @AdPrints = 0
				, @SmallDrop = 1
				
			select				
				@PrevSearchPageViews = SearchPageViews
				, @PrevDetailPageViews = DetailPageViews
				, @PrevEmailLeads = EmailLeads
				, @PrevMapsViewed = MapsViewed
				, @PrevAdPrints = AdPrints
			from Merchandising.dashboard.InventoryAdPerformance
			where
				businessUnitID = @businessUnitID
				and InventoryId = @InventoryId
				and DestinationId = @destinationId
				and Date =
				(
					select max(Date)
					from Merchandising.dashboard.InventoryAdPerformance
					where
						businessUnitID = @businessUnitID
						and InventoryId = @InventoryId
						and DestinationId = @destinationId
						and Date < @LogicalDate
				)
				
			if (@debug = 1)				
			begin
				select 'debug, in small drop code'
					, [@SearchPageViews] = @SearchPageViews
					, [@DetailPageViews] = @DetailPageViews
					, [@EmailLeads] = @EmailLeads
					, [@MapsViewed] = @MapsViewed
					, [@AdPrints] = @AdPrints
					, [@SmallDrop] = @SmallDrop

					, [@PrevSearchPageViews] = @PrevSearchPageViews
					, [@PrevDetailPageViews] = @PrevDetailPageViews
					, [@PrevEmailLeads] = @PrevEmailLeads
					, [@PrevMapsViewed] = @PrevMapsViewed
					, [@PrevAdPrints] = @PrevAdPrints
			end
		end
		else if
		(
			-- there is a drop in metrics
			(@SearchPageViews < @PrevSearchPageViews)
			or
			-- this car was flatlined in the past and we don't want to lose the flatline stats
			(@prevMetricsAreFlatlined = 1 and (@SearchPageViews > @PrevSearchPageViews))
			or
			-- this is a historical scrape when a new store is turned on, we always want to count it as a boundary
			(
				@LogicalDate = dateadd(day, -1, dateadd(month, 1, @Month))
				and not exists
				(
					select *
					from Merchandising.dashboard.RawMetrics
					where
						DatasourceInterfaceId = @DatasourceInterfaceID
						and businessUnitID = @businessUnitID
						and inventoryId = @inventoryId
						and Month = @Month
						and LogicalDate <> @LogicalDate
				)
			)	
			
		)
		begin
			-- the last input row is a boundary
			update Merchandising.dashboard.InventoryAdPerformance set
				Boundary = 1
				, @PrevSearchPageViews = SearchPageViews
				, @PrevDetailPageViews = DetailPageViews
				, @PrevEmailLeads = EmailLeads
				, @PrevMapsViewed = MapsViewed
				, @PrevAdPrints = AdPrints
			where
				businessUnitID = @businessUnitID
				and InventoryId = @InventoryId
				and DestinationId = @destinationId
				and Date =
					(
						select top 1 Date
						from Merchandising.dashboard.InventoryAdPerformance
						where
							businessUnitID = @businessUnitID
							and InventoryId = @InventoryId
							and DestinationId = @destinationId
						order by Date desc
					)

			if (@@rowcount > 1)
			begin
				raiserror('expecting to only mark one row as boundary in InventoryAdPerformance!', 16, 1)
				break
			end
			
			update @Outputs set
				Boundary = 1
				, @PrevSearchPageViews = SearchPageViews
				, @PrevDetailPageViews = DetailPageViews
				, @PrevEmailLeads = EmailLeads
				, @PrevMapsViewed = MapsViewed
				, @PrevAdPrints = AdPrints
			where
				businessUnitID = @businessUnitID
				and InventoryId = @InventoryId
				and DestinationId = @destinationId
				and Date =
					(
						select top 1 Date
						from @Outputs
						where
							businessUnitID = @businessUnitID
							and InventoryId = @InventoryId
							and DestinationId = @destinationId
						order by Date desc
					)
								
			if (@@rowcount > 1)
			begin
				raiserror('expecting to only mark one row as boundary in @Outputs!', 16, 1)
				break
			end
		end
		else
		begin
			-- there was previous input, and, we didn't have to mark a boundary, so retrieve the
			-- previous boundary numbers (zero out if they don't exist... we have prev input but
			-- no prev boundaries)
			
			select top 1
				@PrevSearchPageViews = SearchPageViews
				, @PrevDetailPageViews = DetailPageViews
				, @PrevEmailLeads = EmailLeads
				, @PrevMapsViewed = MapsViewed
				, @PrevAdPrints = AdPrints
			from
				(
					select
						BusinessUnitId,
						InventoryId,
						DestinationId,
						Date,
						Boundary,
						SearchPageViews,
						DetailPageViews,
						EmailLeads,
						MapsViewed,
						AdPrints
					from Merchandising.dashboard.InventoryAdPerformance iap
					where not exists(select * from @Files where LoadId = iap.LoadId)
					
					union all
					
					select
						BusinessUnitId,
						InventoryId,
						DestinationId,
						Date,
						Boundary,
						SearchPageViews,
						DetailPageViews,
						EmailLeads,
						MapsViewed,
						AdPrints
					from @Outputs
					
				) a   	
			where
				businessUnitID = @businessUnitID
				and InventoryId = @InventoryId
				and DestinationId = @destinationId
				and Boundary = 1
			order by Date desc
			
			if (@@rowcount <> 1)
			begin
				-- no previous input means that there is also no previous boundaries
				select
					@PrevSearchPageViews = 0
					, @PrevDetailPageViews = 0
					, @PrevEmailLeads = 0
					, @PrevMapsViewed = 0
					, @PrevAdPrints = 0
			end
			
		end
		
		
		
		-- by now, we have non-null data in our @Prev variables, and, the numbers represent the additive boundary.  Go store the result

outputStorage:			
		insert @Outputs
		(
			BusinessUnitId,
			InventoryId,
			DestinationId,
			Date,
			SearchPageViews,
			DetailPageViews,
			EmailLeads,
			MapsViewed,
			AdPrints,
			Boundary,
			LoadId,
			SmallDrop
		)
		values
		(
			@BusinessUnitId
			, @InventoryId
			, @destinationID
			, @LogicalDate
			, @SearchPageViews + @PrevSearchPageViews
			, @DetailPageViews + @PrevDetailPageViews
			, @EmailLeads + @PrevEmailLeads
			, @MapsViewed + @PrevMapsViewed
			, @AdPrints + @PrevAdPrints
			, 0
			, @queueLoadId
			, @SmallDrop
		)

	end -- @Inputs loop


	-- start a transaction and swap the data

	begin try

		begin tran
		
		delete base
		from Merchandising.dashboard.InventoryAdPerformance base
		where LoadId in (select LoadId from @Files)
		
		insert Merchandising.dashboard.InventoryAdPerformance
		(
			BusinessUnitId,
			InventoryId,
			DestinationId,
			SearchPageViews,
			DetailPageViews,
			EmailLeads,
			MapsViewed,
			AdPrints,
			Boundary,
			Date,
			LoadId,
			SmallDrop
		)
		select
			BusinessUnitId,
			InventoryId,
			DestinationId,
			SearchPageViews,
			DetailPageViews,
			EmailLeads,
			MapsViewed,
			AdPrints,
			Boundary,
			Date,
			LoadId,
			SmallDrop
		from @Outputs
		
		
		commit
	end try
	begin catch
		rollback
		
		
		set @error =
			'Failed to load data from @Outputs into InventoryAdPerformance! ' +
			' ErrorNumber ' + isnull(convert(varchar(11), error_number()), '<null>') +
			', ErrorSeverity ' + isnull(convert(varchar(11), error_severity()), '<null>') +
			', ErrorState ' + isnull(convert(varchar(11), error_state()), '<null>') +
			', ErrorProcedure ' + isnull(error_procedure(), '<null>') +
			', ErrorLine ' + isnull(convert(varchar(11), error_line()), '<null>') +
			', ErrorMessage ' + isnull(error_message(), '<null>')
		
		raiserror(@error, 16, 1)
		return 1
	end catch -- swap between @Outputs and InventoryAdPerformance table
	
	

	

	/*
		if we're processing cars vehicle summary data, we need to calculate
		new vs. used ratios based on this inventory data that will be applied
		to the store-level performance summary data
		
		we only do that for BusinessUnitId/DestinationId/Month that exists in dashboard.PerformanceSummary
		
		after calculating the new ratios, we will update the SPV, DPV etc but adding new + used and breaking
		them back out using the ratios.  this way new + used will always match the total that came in
		on the PerfSummary feed for cars.com
	*/
	if @DatasourceInterfaceID = 2004
	begin
	
		begin try
		


			;with MinMaxReportDatePerMonth
			as
			(	-- Get the max report date (scraped date) for every bu, inv, destination, month
				select
					BusinessUnitId
					,InventoryId
					,DestinationId
					,ReportMonth = dateadd(month, datediff(month, 0, Date), 0)
					,MinReportDate = min(Date)
					,MaxReportDate = max(Date)
					,RecCount = count(*)
				from Merchandising.dashboard.InventoryAdPerformance
				group by 
					BusinessUnitId
					,InventoryId
					,DestinationId
					,dateadd(month, datediff(month, 0, Date), 0) -- ReportMonth
			)
			,InvAdPerfByMonth as
			( -- Get the last set of stats for the max date for each bu, inv, destination, month
				select 
					mm.BusinessUnitId
					,mm.InventoryId
					,mm.DestinationId
					,mm.ReportMonth
					,SearchPageViews = case RecCount when 1 then  maxiap.SearchPageViews else maxiap.SearchPageViews - miniap.SearchPageViews end
					,DetailPageViews = case RecCount when 1 then  maxiap.DetailPageViews else maxiap.DetailPageViews - miniap.DetailPageViews end
					,EmailLeads = case RecCount when 1 then  maxiap.EmailLeads else maxiap.EmailLeads - miniap.EmailLeads end
					,MapsViewed = case RecCount when 1 then  maxiap.MapsViewed else maxiap.MapsViewed - miniap.MapsViewed end
					,AdPrints = case RecCount when 1 then  maxiap.AdPrints else maxiap.AdPrints - miniap.AdPrints end
				from MinMaxReportDatePerMonth mm
				join Merchandising.dashboard.InventoryAdPerformance miniap
					on mm.BusinessUnitId = miniap.BusinessUnitId
					and mm.InventoryId = miniap.InventoryId
					and mm.DestinationId = miniap.DestinationId
					and mm.MinReportDate = miniap.Date
				join Merchandising.dashboard.InventoryAdPerformance maxiap
					on mm.BusinessUnitId = maxIAP.BusinessUnitId
					and mm.InventoryId = maxIAP.InventoryId
					and mm.DestinationId = maxIAP.DestinationId
					and mm.MaxReportDate = maxIAP.Date
			)
			,PerformanceSummaryRollup
			as 
			(	-- Roll up the ad performance by bu, report month, destination, and inventory type (used/new)
				select 
					iap.BusinessUnitId
					,ReportMonth
					,DestinationId
					,InventoryType
					,SearchPageViews = sum(iap.SearchPageViews) 
					,DetailPageViews = sum(iap.DetailPageViews)
					,EmailLeads = sum(iap.EmailLeads)
					,MapsViewed = sum(iap.MapsViewed)
					,AdPrints = sum(iap.AdPrints)
				from InvAdPerfByMonth iap
				join IMT.dbo.Inventory inv
					on inv.InventoryId = iap.InventoryId
				group by iap.BusinessUnitId, ReportMonth, DestinationId, InventoryType
			) -- end of CTEs

			insert @ratios
			(
				BusinessUnitId,
				DestinationId,
				ReportMonth,
				UsedSearchPageViewsRatio,
				UsedDetailPageViewsRatio,
				UsedMapsViewedRatio,
				UsedAdPrintsRatio
			)
			select 
				BusinessUnitId
				, DestinationId
				, ReportMonth
				
				, UsedSearchPageViewsRatio = sum(case InventoryType when 2 then SearchPageViews else 0 end) / isnull(nullif((1.0 * sum(SearchPageViews)), 0), 1)
				, UsedDetailPageViewsRatio = sum(case InventoryType when 2 then DetailPageViews else 0 end) / isnull(nullif((1.0 * sum(DetailPageViews)), 0), 1)
				, UsedMapsViewedRatio = sum(case InventoryType when 2 then MapsViewed else 0 end) / isnull(nullif((1.0 * sum(MapsViewed)), 0), 1)
				, UsedAdPrintsRatio = sum(case InventoryType when 2 then AdPrints else 0 end) / isnull(nullif((1.0 * sum(AdPrints)), 0), 1)
			from PerformanceSummaryRollup ps
			where
				DestinationId = 1 -- cars.com
				and exists 
				(	-- only get rows in our input
					select *
					from @BusinessUnitMonth
					where 
						BusinessUnitId = ps.BusinessUnitId 
						and Month = ps.ReportMonth
				)
				and exists
				(
					select *
					from Merchandising.dashboard.PerformanceSummary
					where
						BusinessUnitId = ps.BusinessUnitId
						and Month = ps.ReportMonth
						and DestinationId = ps.DestinationId
				)
			group by
				BusinessUnitId,
				ReportMonth,
				DestinationId



			-- get averages from all stores to protect against garbage ratios
			select
				@AvgSearchPageViewsRatio = avg(UsedSearchPageViewsRatio),
				@AvgDetailPageViewsRatio = avg(UsedDetailPageViewsRatio),
				@AvgMapsViewedRatio = avg(UsedMapsViewedRatio),
				@AvgAdPrintsRatio = avg(UsedAdPrintsRatio)
			from Merchandising.dashboard.PerformanceSummary
			where DestinationId = 1


			update @ratios set
				UsedSearchPageViewsRatio = @AvgSearchPageViewsRatio,
				UsedDetailPageViewsRatio = @AvgDetailPageViewsRatio,
				UsedMapsViewedRatio = @AvgMapsViewedRatio,
				UsedAdPrintsRatio = @AvgAdPrintsRatio
			where
				UsedSearchPageViewsRatio in (0,1) or UsedSearchPageViewsRatio not between 0 and 1
				or UsedDetailPageViewsRatio in (0,1) or UsedDetailPageViewsRatio not between 0 and 1
				or UsedMapsViewedRatio in (0,1) or UsedMapsViewedRatio not between 0 and 1
				or UsedAdPrintsRatio in (0,1) or UsedAdPrintsRatio not between 0 and 1


			update ps set
				UsedSearchPageViewsRatio = r.UsedSearchPageViewsRatio,
				UsedDetailPageViewsRatio = r.UsedDetailPageViewsRatio,
				UsedMapsViewedRatio = r.UsedMapsViewedRatio,
				UsedAdPrintsRatio = r.UsedAdPrintsRatio,
				
				NewSearchPageViews = (ps.NewSearchPageViews + ps.UsedSearchPageViews) - convert(int, (ps.NewSearchPageViews + ps.UsedSearchPageViews) * r.UsedSearchPageViewsRatio),
				NewDetailPageViews =  (ps.NewDetailPageViews + ps.UsedDetailPageViews) - convert(int, (ps.NewDetailPageViews + ps.UsedDetailPageViews) * r.UsedDetailPageViewsRatio),
				NewMapsViewed = (ps.NewMapsViewed + ps.UsedMapsViewed) - convert(int, (ps.NewMapsViewed + ps.UsedMapsViewed) * r.UsedMapsViewedRatio),
				NewAdPrints = (ps.NewAdPrints + ps.UsedAdPrints) - convert(int, (ps.NewAdPrints + ps.UsedAdPrints) * r.UsedAdPrintsRatio),
				
				UsedSearchPageViews = convert(int, (ps.NewSearchPageViews + ps.UsedSearchPageViews) * r.UsedSearchPageViewsRatio),
				UsedDetailPageViews = convert(int, (ps.NewDetailPageViews + ps.UsedDetailPageViews) * r.UsedDetailPageViewsRatio),
				UsedMapsViewed = convert(int, (ps.NewMapsViewed + ps.UsedMapsViewed) * r.UsedMapsViewedRatio),
				UsedAdPrints = convert(int, (ps.NewAdPrints + ps.UsedAdPrints) * r.UsedAdPrintsRatio)
		
			from
				Merchandising.dashboard.PerformanceSummary ps
				join @ratios r
					on ps.BusinessUnitId = r.BusinessUnitId
					and ps.DestinationId = r.DestinationId
					and ps.Month = r.ReportMonth

		end try
		begin catch
			
			set @error =
				'Failed to load data to PerformanceSummary! ' +
				' ErrorNumber ' + isnull(convert(varchar(11), error_number()), '<null>') +
				', ErrorSeverity ' + isnull(convert(varchar(11), error_severity()), '<null>') +
				', ErrorState ' + isnull(convert(varchar(11), error_state()), '<null>') +
				', ErrorProcedure ' + isnull(error_procedure(), '<null>') +
				', ErrorLine ' + isnull(convert(varchar(11), error_line()), '<null>') +
				', ErrorMessage ' + isnull(error_message(), '<null>')
			
			raiserror(@error, 16, 1)
			return 1
		end catch

	end  -- @DatasourceInterfaceID = 2004

			
end try
begin catch
	set @error =
		'Failed to process VehSummary data!' + char(10) 
		+ ' @BusinessUnitId ' + isnull(convert(varchar(11), @BusinessUnitId), '<null>') + char(10)
		+ ' @InventoryId ' + isnull(convert(varchar(11), @InventoryId), '<null>') + char(10)
		+ ' @LoadId ' + isnull(convert(varchar(11), @LoadId), '<null>') + char(10)
		+ ' @Vin ' + isnull(@Vin, '<null>') + char(10)
		+ ' @StockNumber ' + isnull(@StockNumber, '<null>') + char(10)
		+			
			
		+ ' ErrorNumber ' + isnull(convert(varchar(11), error_number()), '<null>')  + char(10)
		+ ', ErrorSeverity ' + isnull(convert(varchar(11), error_severity()), '<null>')  + char(10)
		+ ', ErrorState ' + isnull(convert(varchar(11), error_state()), '<null>')  + char(10)
		+ ', ErrorProcedure ' + isnull(error_procedure(), '<null>')  + char(10)
		+ ', ErrorLine ' + isnull(convert(varchar(11), error_line()), '<null>')  + char(10)
		+ ', ErrorMessage ' + isnull(error_message(), '<null>')
	
	raiserror(@error, 16, 1)
	
	return 1
end catch


exec Merchandising.dashboard.AggregateVehicleActivityInventoryCounts

--print 'Number of rows skipped due to not finding BuId/InvId lookup: ' + convert(varchar(10), @NoInvIdSkipRows)

return 0

go