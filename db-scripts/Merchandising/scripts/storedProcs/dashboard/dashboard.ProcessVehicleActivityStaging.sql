if object_id('dashboard.ProcessVehicleActivityStaging') is not null
	drop procedure dashboard.ProcessVehicleActivityStaging
go

create procedure dashboard.ProcessVehicleActivityStaging
	@LoadId int,
	@DatasourceInterfaceId int,
	@DestinationId int
as
set nocount on
set transaction isolation level read uncommitted





	
begin try
	begin tran
	
	
	if (@DatasourceInterfaceId = 2010)
	begin
		-- Cars.com direct does not send an online or activity file, copy to those staging areas
		set identity_insert dashboard.VehicleOnlineStaging on
		
		insert dashboard.VehicleOnlineStaging
		(
			BusinessUnitId,
			InventoryId,
			LoadId,
			RowId,
			DestinationId,
			DateCollected,
			HasPhoto,
			PhotoCount,
			DateLoaded,
			DatasourceInterfaceId,
			Vin,
			StockNumber,
			DaysLive,
			ErrorCode,
			RequestId
		)
		select
			BusinessUnitId,
			InventoryId,
			LoadId,
			RowId,
			DestinationId,
			DateCollected,
			null,
			PhotoCount,
			DateLoaded,
			DatasourceInterfaceId,
			Vin,
			'Cars - Dummy', -- online staging does not allow nulls here, but that is only for inventoryId lookup, which has already been done
			DaysLive,
			ErrorCode,
			RequestId
		from dashboard.VehicleActivityStaging
		where
			LoadId = @LoadId
			and ErrorCode = 0
			
		set identity_insert dashboard.VehicleOnlineStaging off
	end
			
	commit
end try
begin catch
	rollback
	
	set identity_insert dashboard.VehicleOnlineStaging off
	set identity_insert dashboard.PerformanceSummaryStaging off
		
	declare @error varchar(1000)
	
	set @error =
		'error processing VehiclActivityStaging!'
		+ char(10) + '@LoadId = ' + isnull(convert(varchar(max), @LoadId), '<null>')
		+ 'ErrorNumber ' + isnull(convert(varchar(11), error_number()), '<null>') +
		', ErrorSeverity ' + isnull(convert(varchar(11), error_severity()), '<null>') +
		', ErrorState ' + isnull(convert(varchar(11), error_state()), '<null>') +
		', ErrorProcedure ' + isnull(error_procedure(), '<null>') +
		', ErrorLine ' + isnull(convert(varchar(11), error_line()), '<null>') +
		', ErrorMessage ' + isnull(error_message(), '<null>')
	
	raiserror(@error, 16, 1)
	
	return 1
end catch





	
return 0


go