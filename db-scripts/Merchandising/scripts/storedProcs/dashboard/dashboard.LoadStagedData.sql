if object_id('dashboard.LoadStagedData') is not null
	drop procedure dashboard.LoadStagedData
go


create procedure dashboard.LoadStagedData
	@LoadId int,
	@DatasourceInterfaceId int,
	@DestinationId int
as
set nocount on
set transaction isolation level read uncommitted




declare
	@id int,
	@return int,
 	@IsOnline bit,
	@IsActivity bit,
	@IsPerformanceSummary bit


exec dashboard.DecodeDatasourceInterfaceId 
    @DatasourceInterfaceId = @DatasourceInterfaceId,
    @IsOnline = @IsOnline output,
    @IsActivity = @IsActivity output,
    @IsPerformanceSummary = @IsPerformanceSummary output
    
    


if (@IsOnline = 1 or @IsActivity = 1)
begin
	exec @return = dashboard.SetInventoryId 
		@LoadId = @LoadId,
		@IsOnline = @IsOnline,
		@IsActivity = @IsActivity,
		@IsPerformanceSummary = @IsPerformanceSummary
	if (@return <> 0)
	begin
		raiserror('error in dashboard.SetInventoryId, return value: %d', 16, 1, @return)
		return 1
	end
end



-- Activity processed first, since DSID 2010 -- cars.com direct, only has activity and then we derive online, performance from it
if (@IsActivity = 1)
begin
	exec @return = dashboard.ProcessVehicleActivityStaging
		@LoadId = @LoadId,
		@DatasourceInterfaceId = @DatasourceInterfaceId,
		@DestinationId = @DestinationId
		
	if (@return <> 0)
	begin
		raiserror('error in dashboard.ProcessVehicleActivityStaging, return value: %d', 16, 1, @return)
		return 1
	end
end



if (@IsOnline = 1 or @DatasourceInterfaceId = 2010)
begin
	exec @return = dashboard.ProcessVehicleOnlineStaging 
		 @LoadId = @LoadId
		 
	if (@return <> 0)
	begin
		raiserror('error in dashboard.ProcessVehicleActivityStaging, return value: %d', 16, 1, @return)
		return 1
	end
	
	exec @return = dashboard.SaveLastOnlineSnapshot
		@LoadID = @LoadId,
		@DatasourceInterfaceID = @DatasourceInterfaceId
	
	if (@return <> 0)
	begin
		raiserror('error in dashboard.SaveLastOnlineSnapshot, return value: %d', 16, 1, @return)
		return 1
	end	
end 

 
 
return 0
 
go
 