if object_id('dashboard.SaveLastOnlineSnapshot') is not null
	drop procedure dashboard.SaveLastOnlineSnapshot
go

create procedure dashboard.SaveLastOnlineSnapshot
	@LoadID int,
	@DatasourceInterfaceID int
as
set nocount on
set transaction isolation level read uncommitted


if (@DatasourceInterfaceID = 2000) -- Fetch.Com Cars VehicleOnline
begin
	; with ThisSnapshot as
	(
		select
			BusinessUnitID,
			RequestId = max(RequestId),
			DateLoaded = max(DateLoaded)
		from Merchandising.merchandising.CarsDotComVehOnline
		where
			Load_ID = @LoadID
			and len(Vin) = 17
		group by BusinessUnitID
	)
	merge Merchandising.dashboard.LastOnlineSnapshot as target
	using (select BusinessUnitId, RequestId, DateLoaded from ThisSnapshot) as source (BusinessUnitId, RequestId, DateLoaded)
	on (target.BusinessUnitId = source.BusinessUnitId)
	when matched then
		update set FetchCarsRequest = source.RequestId, FetchCarsDateLoaded = source.DateLoaded
	when not matched then
		insert (BusinessUnitId, FetchCarsRequest, FetchCarsDateLoaded) values (source.BusinessUnitId, source.RequestId, source.DateLoaded)
	;
end
else if (@DatasourceInterfaceID = 14) -- Fetch.Com VehicleOnline (AutoTrader)
begin
	; with ThisSnapshot as
	(
		select
			BusinessUnitID,
			RequestId = max(RequestId),
			DateLoaded = max(DateLoaded)
		from Merchandising.merchandising.AutoTraderVehOnline
		where
			Load_ID = @LoadID
			and len(Vin) = 17
		group by BusinessUnitID
	)
	merge Merchandising.dashboard.LastOnlineSnapshot as target
	using (select BusinessUnitId, RequestId, DateLoaded from ThisSnapshot) as source (BusinessUnitId, RequestId, DateLoaded)
	on (target.BusinessUnitId = source.BusinessUnitId)
	when matched then
		update set FetchAtRequest = source.RequestId, FetchAtDateLoaded = source.DateLoaded
	when not matched then
		insert (BusinessUnitId, FetchAtRequest, FetchAtDateLoaded) values (source.BusinessUnitId, source.RequestId, source.DateLoaded)
	;
end
else if (@DatasourceInterfaceID = 2009) -- AutoTrader Hendrick Online status
begin
	; with ThisSnapshot as
	(
		select
			BusinessUnitID,
			LoadId = max(LoadId),
			DateLoaded = max(FeedDate)
		from Merchandising.merchandising.AutoTraderHendrickOnline
		where
			BusinessUnitId is not null
			and LoadId = @LoadID
			and len(Vin) = 17
		group by BusinessUnitID
	)
	merge Merchandising.dashboard.LastOnlineSnapshot as target
	using (select BusinessUnitId, LoadId, DateLoaded from ThisSnapshot) as source (BusinessUnitId, LoadId, DateLoaded)
	on (target.BusinessUnitId = source.BusinessUnitId)
	when matched then
		update set AtDirectLoad = source.LoadId, AtDirectDateLoaded = source.DateLoaded
	when not matched then
		insert (BusinessUnitId, AtDirectLoad, AtDirectDateLoaded) values (source.BusinessUnitId, source.LoadId, source.DateLoaded)
	;
end
else if (@DatasourceInterfaceID = 2010) -- SiteMetrics CarsDotCom VehiclesActivity
begin
	; with ThisSnapshot as
	(
		select
			BusinessUnitID,
			LoadId = max(LoadId),
			DateLoaded = max(DateCollected)
		from Merchandising.dashboard.VehicleOnline
		where LoadId = @LoadID
		group by BusinessUnitID
	)
	merge Merchandising.dashboard.LastOnlineSnapshot as target
	using (select BusinessUnitId, LoadId, DateLoaded from ThisSnapshot) as source (BusinessUnitId, LoadId, DateLoaded)
	on (target.BusinessUnitId = source.BusinessUnitId)
	when matched then
		update set CarsDirectLoad = source.LoadId, CarsDirectDateLoaded = source.DateLoaded
	when not matched then
		insert (BusinessUnitId, CarsDirectLoad, CarsDirectDateLoaded) values (source.BusinessUnitId, source.LoadId, source.DateLoaded)
	;
end
else
begin
	raiserror('Merchandising.dashboard.SaveLastOnlineSnapshot switches on DatasourceInterfaceId.  No code found for DatasourceInterfaceId %d!', 16, 1, @DatasourceInterfaceID)
end



go




