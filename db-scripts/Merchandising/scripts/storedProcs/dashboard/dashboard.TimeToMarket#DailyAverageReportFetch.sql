-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-04-24
-- Description:	Fetch the daily averages for a date range
-- =============================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dashboard].[TimeToMarket#DailyAverageReportFetch]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dashboard].[TimeToMarket#DailyAverageReportFetch]
GO


CREATE PROCEDURE dashboard.TimeToMarket#DailyAverageReportFetch 
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int, 
	@StartDate DATETIME = NULL,
	@EndDate DATETIME = NULL,
	@VehicleType int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--DECLARE @BusinessUnitId int;
	--DECLARE @VehicleType int;
	--DECLARE @StartDate DateTime;
	--DECLARE @EndDate DateTime;

	-- All the inventory
	DECLARE @VehicleDays TABLE (
		[InventoryId] int,
		[ElapsedDays] int
	);

	DECLARE @DailyPhotoData TABLE (
		[PhotosFirstDate] DATETIME,
		[FirstDateCount] int,
		[ElapsedDays] int
	)

	DECLARE @DailyAverages TABLE (
		[Date] DATETIME NULL,
		GIDAverageDays int NULL,
		PhotosAverageDays int NULL,
		DescriptionAverageDays int NULL,
		PriceAverageDays int NULL,
		AdApprovalAverageDays int NULL,
		CompleteAdAverageDays int NULL
	)

	--SELECT @BusinessUnitId = 101071, @VehicleType = 0, @StartDate = '05-01-2013', @EndDate = '05-31-2013';

	INSERT INTO @VehicleDays
	SELECT 
		i.InventoryID, DATEDIFF(DAY, i.InventoryReceivedDate, ttm.PhotosFirstDate ) AS DaysPassed
	FROM IMT.dbo.Inventory i
	JOIN Merchandising.dashboard.TimeToMarketTracking ttm ON ttm.InventoryId = i.InventoryID AND ttm.BusinessUnitId = i.BusinessUnitID
	WHERE ttm.PhotosFirstDate BETWEEN @StartDate AND @EndDate
		AND ttm.BusinessUnitId = @BusinessUnitId AND (@VehicleType = 0 OR i.InventoryType = @VehicleType)


	INSERT INTO @DailyPhotoData
	SELECT ttm.PhotosFirstDate, COUNT(PhotosFirstDate) AS FirstDateCount, SUM(ElapsedDays) AS ElapsedDays
		FROM Merchandising.dashboard.TimeToMarketTracking ttm
		JOIN IMT.dbo.Inventory i ON i.BusinessUnitID = ttm.BusinessUnitId AND i.InventoryID = ttm.InventoryId
		JOIN @VehicleDays vd ON vd.InventoryId = ttm.InventoryID
		WHERE ttm.BusinessUnitId = @BusinessUnitId
		AND (@VehicleType = 0 OR i.InventoryType = @VehicleType)
		AND ttm.[PhotosFirstDate] BETWEEN @StartDate AND @EndDate
		GROUP BY PhotosFirstDate
		

	INSERT INTO @DailyAverages(Date, PhotosAverageDays)
	SELECT [PhotosFirstDate], ElapsedDays / FirstDateCount
	FROM @DailyPhotoData

	SELECT * FROM @DailyAverages;

END
GO

GRANT EXECUTE on dashboard.TimeToMarket#DailyAverageReportFetch  to MerchandisingUser
GO
