if object_id('dashboard.AuditVehSummaryDifferentials') is not null
	drop procedure dashboard.AuditVehSummaryDifferentials
go

create procedure dashboard.AuditVehSummaryDifferentials
	@LoadId int
as
set nocount on
set transaction isolation level read uncommitted

/*
	for each InventoryId / Month in the input file,
	get the last corresponding row from the raw fetch data,
	sum up all the daily differentials >= first of that month,
	log any mismatches into a table
*/

declare @TotalUnits int


declare @vehicles table
(
	BusinessUnitId int not null,
	InventoryId int not null,
	Month datetime not null,

	InputSearchPageViews int not null,
	InputDetailPageViews int not null,
	InputEmailLeads int not null,
	InputMapsViewed int not null,
	InputAdPrints int not null,


	OutputSearchPageViews int not null,
	OutputDetailPageViews int not null,
	OutputEmailLeads int not null,
	OutputMapsViewed int not null,
	OutputAdPrints int not null
)
		
		
		
		
		
		
; with Inputs as
(
	select distinct
		businessUnitID,
		InventoryId,
		Month
	from Merchandising.dashboard.RawMetrics
	where Load_ID = @LoadId
)
, Metrics as
(
	select
		m.BusinessUnitId,
		m.InventoryId,
		Month = dateadd(month, datediff(month, 0, m.Date), 0),
		SearchPageViews = sum(m.SearchPageViews),
		DetailPageViews = sum(m.DetailPageViews),
		EmailLeads = sum(m.EmailLeads),
		MapsViewed = sum(m.MapsViewed),
		AdPrints = sum(m.AdPrints)
	from
		Merchandising.dashboard.SiteMetricsByStoreDateInventory m
	where
		exists
		(
			select *
			from Inputs
			where
				businessUnitID = m.BusinessUnitId
				and InventoryId = m.InventoryId
				and Month = dateadd(month, datediff(month, 0, m.Date), 0)
		)
	group by
		m.BusinessUnitId,
		m.InventoryId,
		dateadd(month, datediff(month, 0, m.Date), 0)
)
, RawFetch as
(
	select
		metrics.BusinessUnitId,
		metrics.InventoryId,
		metrics.month,
		SearchPageViews,
		DetailPageViews,
		EmailLeads,
		MapsViewed,
		AdPrints
	from
		(
			select
				m.destinationID,
				m.businessUnitID,
				m.InventoryId,
				m.month,
				DateLoaded = max(m.DateLoaded)
			from
				Merchandising.dashboard.RawMetrics m
				join Inputs i
					on m.businessUnitID = i.businessUnitID
					and m.InventoryId = i.InventoryId
					and m.Month = i.Month
			group by
				m.destinationID,
				m.businessUnitID,
				m.InventoryId,
				m.Month
		) lastRow
		join Merchandising.dashboard.RawMetrics metrics
			on lastRow.destinationID = metrics.destinationID
			and lastRow.businessUnitID = metrics.businessUnitID
			and lastRow.InventoryId = metrics.InventoryId
			and lastRow.month = metrics.Month
			and lastRow.DateLoaded = metrics.DateLoaded
)
insert @vehicles
(
	BusinessUnitId,
	InventoryId,
	Month,

	InputSearchPageViews,
	InputDetailPageViews,
	InputEmailLeads,
	InputMapsViewed,
	InputAdPrints,

	OutputSearchPageViews,
	OutputDetailPageViews,
	OutputEmailLeads,
	OutputMapsViewed,
	OutputAdPrints
)
select
	i.businessUnitID,
	i.InventoryId,
	i.Month,
	
	r.SearchPageViews,
	r.DetailPageViews,
	r.EmailLeads,
	r.MapsViewed,
	r.AdPrints,
	
	m.SearchPageViews,
	m.DetailPageViews,
	m.EmailLeads,
	m.MapsViewed,
	m.AdPrints
from
	Inputs i
	left join Metrics m
		on i.businessUnitID = m.BusinessUnitId
		and i.InventoryId = m.InventoryId
		and i.Month = m.Month
	left join RawFetch r
		on i.businessUnitID = r.BusinessUnitId
		and i.InventoryId = r.InventoryId
		and i.Month = r.Month


set @TotalUnits = @@rowcount

select top 10 
	BusinessUnitId,
	InventoryId,
	Month
from @vehicles
group by
	BusinessUnitId,
	InventoryId,
	Month
having count(*) > 1



BusinessUnitId	InventoryId	Month
101328	18974172	2012-02-01 00:00:00.000

return 


insert dashboard.MismatchedVehSummaryDifferentials
(
	LoadId,
	TotalUnits,
	AuditedOn,
	BusinessUnitId,
	InventoryId,
	Month,
	InputSearchPageViews,
	InputDetailPageViews,
	InputEmailLeads,
	InputMapsViewed,
	InputAdPrints,
	OutputSearchPageViews,
	OutputDetailPageViews,
	OutputEmailLeads,
	OutputMapsViewed,
	OutputAdPrints
)
select
	@LoadId,
	@TotalUnits,
	getdate(),
	BusinessUnitId,
	InventoryId,
	Month,
	InputSearchPageViews,
	InputDetailPageViews,
	InputEmailLeads,
	InputMapsViewed,
	InputAdPrints,
	OutputSearchPageViews,
	OutputDetailPageViews,
	OutputEmailLeads,
	OutputMapsViewed,
	OutputAdPrints
from @vehicles
where
	InputSearchPageViews <> OutputSearchPageViews
	or InputDetailPageViews <> OutputDetailPageViews
	or InputEmailLeads <> OutputEmailLeads
	or InputMapsViewed <> OutputMapsViewed
	or InputAdPrints <> OutputAdPrints	



go