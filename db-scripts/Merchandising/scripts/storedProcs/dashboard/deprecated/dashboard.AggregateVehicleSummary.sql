if object_id('dashboard.AggregateVehicleSummary') is not null
	drop procedure dashboard.AggregateVehicleSummary
go

/*
	This proc takes an incoming file from fetch by @LoadID,
	loads up all the distinct VINs, and for each VIN in the file
	calculates aggregations from the minimum month in the data
	forward through the maximum month then replaces the data in 
	dashboard.SiteMetricsByStoreDataInventory in a single transaction.
	
*/
create procedure dashboard.AggregateVehicleSummary
	@LoadID int
	, @destinationID int
	, @debugMessages bit = 0
as
set nocount on
set transaction isolation level read uncommitted


declare @Vins table
(
	BusinessUnitId int not null
	, VIN varchar(17) not null
	, InventoryId int not null
	, MinMonth datetime
	, MaxMonth datetime
	primary key clustered
	(
		BusinessUnitId,
		VIN,
		InventoryId
	)
)

declare @VinMonthMetrics table
(
	id int identity (0,1) not null primary key clustered,
	Date datetime not null,
	--
	SearchPageViews int not null,
	DetailPageViews int not null,
	EmailLeads int not null,
	MapsViewed int not null,
	AdPrints int not null
)


declare @metrics table
(
	id int identity(0,1) not null
	, BusinessUnitId int not null
	, InventoryId int not null
	, destinationID int not null
	, Date datetime not null
	, SearchPageViews int not null
	, DetailPageViews int not null
	, EmailLeads int not null
	, MapsViewed int not null
	, AdPrints int not null
	--
	primary key clustered 
	(
		BusinessUnitId
		, InventoryId
		, destinationID
		, Date
	)
)


declare
	@Vin varchar(17)
	, @MinMonth datetime
	, @MaxMonth datetime
	, @currentMonth datetime
	--	
	, @id int
	, @BusinessUnitId int
	, @InventoryId int
	, @Date datetime
	--
	, @SearchPageViews int
	, @DetailPageViews int
	, @EmailLeads int
	, @MapsViewed int
	, @AdPrints int
	--
	, @PrevSearchPageViews int
	, @PrevDetailPageViews int
	, @PrevEmailLeads int
	, @PrevMapsViewed int
	, @PrevAdPrints int
	--
	, @error varchar(max)
	, @dupeDatesDiscarded int
	, @lastDayOfMonth datetime
	, @lastSearchPageViews int
	, @kindOfRow tinyint
	, @dupeValueForSPV int
	, @dupeDateForSPV datetime

insert @Vins
(
	BusinessUnitId
	, Vin
	, InventoryId
	, MinMonth
	, MaxMonth
)
select
	businessUnitID
	, Vin
	, InventoryId
	, min(Month)
	, max(Month)
from Merchandising.dashboard.RawMetrics
where
	Load_ID = @LoadID
	and destinationID = @destinationID
group by
	businessUnitID
	, VIN
	, InventoryId







set @dupeDatesDiscarded = 0


/* loop through vins */
begin try
	while (1=1)
	begin
		select top 1
			@BusinessUnitId = BusinessUnitId
			, @Vin = Vin
			, @InventoryId = InventoryId
			, @MinMonth = MinMonth
			, @MaxMonth = MaxMonth
		from @Vins
		
		if (@@rowcount <> 1) break
		delete @Vins where BusinessUnitId = @BusinessUnitId and Vin = @Vin and InventoryId = @InventoryId
		


			
			
		/* loop through months for this vin */		
		
		set @currentMonth = @MinMonth
		while (@currentMonth <= @MaxMonth)
		begin
		
			delete @VinMonthMetrics
			
			
			
			/* load the data for this month and Vin */
			
			insert @VinMonthMetrics
			(
				Date
				, SearchPageViews
				, DetailPageViews
				, EmailLeads
				, MapsViewed
				, AdPrints
			)
			select
				m.DateLoaded
				, m.SearchPageViews
				, m.DetailPageViews
				, m.EmailLeads
				, m.MapsViewed
				, m.AdPrints

			from Merchandising.dashboard.RawMetrics m
			where
				businessUnitID = @BusinessUnitId
				and VIN = @Vin
				and InventoryId = @InventoryId
				and Month = @currentMonth
				and destinationID = @destinationID


					
			

			/* 
				Cars.com doesn't always roll/reset their numbers at the beginning of the month.
				
				See sample data:
				
				DateLoaded		Month			SearchPageViews
				----------		-----			---------------
				12/31/2011		12/01/2011		428
				01/01/2012		01/01/2012		428
				01/02/2012		01/01/2012		428
				01/03/2012		01/01/2012		428
				01/04/2012		01/01/2012		697
				
				So we have to:
				
				1) find the last data point for the previous month
				2) find the last row in our stats (in the above example, DateLoaded = 1/4) that participates in the contiguous string of dupes
				3) update our stats to zero out these dupes, up to and including the row found in step 2
			*/			


			-- 1)
			select top 1 @dupeValueForSPV = SearchPageViews
			from Merchandising.dashboard.RawMetrics m
			where
				businessUnitID = @BusinessUnitId
				and VIN = @Vin
				and InventoryId = @InventoryId
				and Month = dateadd(month, -1, @currentMonth)
				and destinationID = @destinationID
			order by DateLoaded desc

				
			-- 2)	
			select 
				@dupeDateForSPV = min(statsLow.Date)
			from
				(
					select
						rowId = row_number() over (order by Date),
						Date,
						SearchPageViews
					from @VinMonthMetrics
				) statsLow
				join
				(
					select
						rowId = row_number() over (order by Date),
						Date,
						SearchPageViews
					from @VinMonthMetrics
				) statsHigh
					on statsLow.rowId + 1 = statsHigh.rowId
			where
				statsLow.SearchPageViews - statsHigh.SearchPageViews <> 0
				and statsLow.SearchPageViews = @dupeValueForSPV


			-- 3)
			update @VinMonthMetrics set
				SearchPageViews = 0
				, DetailPageViews = 0
				, EmailLeads = 0
				, MapsViewed = 0
				, AdPrints = 0
			 where date <= @dupeDateForSPV






			-- now that the data has been scrubbed for Cars.com dupes, scroll through the metrics and calculate daily values.
			
			set @id = -1
			while (1=1)
			begin
				select top 1
					@id = vmm.id
					, @Date = dateadd(day, 0, datediff(day, 0, vmm.Date))
					, @SearchPageViews = vmm.SearchPageViews
					, @DetailPageViews = vmm.DetailPageViews
					, @EmailLeads = vmm.EmailLeads
					, @MapsViewed = vmm.MapsViewed
					, @AdPrints = vmm.AdPrints
				from @VinMonthMetrics vmm
				where id > @id
				order by id
					
				if (@@rowcount <> 1) break
							
			
				if (@debugMessages = 1)
				begin
					print
						'@currentMonth = ' + convert(varchar, @currentMonth, 101)
						+ ' @Date = ' + convert(varchar, @Date, 101)
						+ ' @SearchPageVies = ' + convert(varchar, @SearchPageViews)
						+ ' @DetailPageViews = ' + convert(varchar, @DetailPageViews)
				end
				
				
				if (datediff(month, @currentMonth, @Date) > 0)
				begin
					-- if the date of the data is later than last day of month, use this day as last day of month
					-- if there was already data for last day of month and this data has more SearchPageViews, use it in place of
					-- previous data
					
					set @lastDayOfMonth = dateadd(day, -1, dateadd(month, 1, @currentMonth))


					select
						@lastSearchPageViews = SearchPageViews
					from @metrics
					where
						BusinessUnitId = @BusinessUnitId
						and InventoryId = @InventoryId
						and destinationID = @destinationID
						and Date = @lastDayOfMonth
						and SearchPageViews < @SearchPageViews
					
					if (@@rowcount = 0)
					begin
						-- we haven't inserted any data for last day of month yet
						set @Date = @lastDayOfMonth
					end
					else if (@lastSearchPageViews < @SearchPageViews)
					begin
						set @Date = @lastDayOfMonth
						
						-- we found a record above, and, the record in @metrics has less searches, nuke it.
						delete @metrics
						where
							BusinessUnitId = @BusinessUnitId
							and InventoryId = @InventoryId
							and destinationID = @destinationID
							and Date = @lastDayOfMonth
					end
				end			
				
				
				if exists(
					select * from @metrics
					where
						BusinessUnitId = @BusinessUnitId
						and InventoryId = @InventoryId
						and destinationID = @destinationID
						and Date = @Date
				)
				begin
					-- we shouldn't have duplicate dates but some of the old data has it
					set @dupeDatesDiscarded = @dupeDatesDiscarded + 1
					
					if (@debugMessages = 1) print 'Duplicate data point discarded.'
					continue
				end
				

				
				select top 1		
					@PrevSearchPageViews = SearchPageViews
					, @PrevDetailPageViews = DetailPageViews
					, @PrevEmailLeads = EmailLeads
					, @PrevMapsViewed = MapsViewed
					, @PrevAdPrints = AdPrints
				from @VinMonthMetrics
				where Date < @Date
				order by Date desc
				
				if (@@rowcount = 0)
				begin
					select top 1		
						@PrevSearchPageViews = 0
						, @PrevDetailPageViews = 0
						, @PrevEmailLeads = 0
						, @PrevMapsViewed = 0
						, @PrevAdPrints = 0
				end

					
				set @kindOfRow = 
					case
						when @SearchPageViews > @PrevSearchPageViews then 1 -- we need to calculate a differential
						when @SearchPageViews = @PrevSearchPageViews then 2 -- no change in the input data, store zeros for all data points
						when @SearchPageViews < @PrevSearchPageViews then 3 -- don't have a clue why the numbers would drop, but let's assume if there is a drop we need to count the whole number and not a diff
					end
					
				insert @metrics
				(
					BusinessUnitId,
					destinationID,
					Date,
					InventoryId,
					--
					SearchPageViews,
					DetailPageViews,
					EmailLeads,
					MapsViewed,
					AdPrints
				)
				values
				(
					@BusinessUnitId
					, @destinationID
					, @Date
					, @InventoryId
					--
					, case
						when @kindOfRow = 1 then @SearchPageViews - @PrevSearchPageViews
						when @kindOfRow = 2 then 0
						when @kindOfRow = 3 then @SearchPageViews
					end

					, case
						when @kindOfRow = 1 then @DetailPageViews - @PrevDetailPageViews
						when @kindOfRow = 2 then 0
						when @kindOfRow = 3 then @DetailPageViews
					end
					
					, case
						when @kindOfRow = 1 then @EmailLeads - @PrevEmailLeads
						when @kindOfRow = 2 then 0
						when @kindOfRow = 3 then @EmailLeads
					end
					
					, case
						when @kindOfRow = 1 then @MapsViewed - @PrevMapsViewed
						when @kindOfRow = 2 then 0
						when @kindOfRow = 3 then @MapsViewed
					end
					
					, case
						when @kindOfRow = 1 then @AdPrints - @PrevAdPrints
						when @kindOfRow = 2 then 0
						when @kindOfRow = 3 then @AdPrints
					end
				)
						

				


			end -- (scrolling through each id/Date in @VinMonthMetrics)

			
			set @currentMonth = dateadd(month, 1, @currentMonth)
		end -- end month loop for Vin
		
	end -- end Vin loop
end try
begin catch
	set @error =
		'Failed to aggregate data in temporary storage!' + char(10) 
		+ ' @BusinessUnitId ' + isnull(convert(varchar(11), @BusinessUnitId), '<null>')
		+ ' @Vin ' + isnull(@Vin, '<null>')
		+ ' @InventoryId ' + isnull(convert(varchar(11), @InventoryId), '<null>')
		+ ' @LoadID ' + isnull(convert(varchar(11), @LoadID), '<null>')
		+ ' @destinationID ' + isnull(convert(varchar(11), @destinationID), '<null>')
		+			
			
		' ErrorNumber ' + isnull(convert(varchar(11), error_number()), '<null>') +
		', ErrorSeverity ' + isnull(convert(varchar(11), error_severity()), '<null>') +
		', ErrorState ' + isnull(convert(varchar(11), error_state()), '<null>') +
		', ErrorProcedure ' + isnull(error_procedure(), '<null>') +
		', ErrorLine ' + isnull(convert(varchar(11), error_line()), '<null>') +
		', ErrorMessage ' + isnull(error_message(), '<null>')
	
	raiserror(@error, 16, 1)
	
	return 1
end catch




/* now replace live aggregate tables with this new data */

begin try
	begin tran


	delete live
	from
		Merchandising.dashboard.SiteMetricsByStoreDateInventory live
		join @metrics m
			on live.BusinessUnitId = m.BusinessUnitId
			and live.InventoryId = m.InventoryId
			and live.destinationID = m.destinationId
			and live.Date = m.Date
						
	insert Merchandising.dashboard.SiteMetricsByStoreDateInventory
	(
		BusinessUnitId,
		InventoryId,
		destinationID,
		Date,
		SearchPageViews,
		DetailPageViews,
		EmailLeads,
		MapsViewed,
		AdPrints
	)
	select
		BusinessUnitId,
		InventoryId,
		destinationID,
		Date,
		SearchPageViews,
		DetailPageViews,
		EmailLeads,
		MapsViewed,
		AdPrints
	from @metrics






	
	
		
	delete live
	from
		Merchandising.dashboard.SiteMetricsByStoreInventory live
		join @metrics m
			on live.BusinessUnitId = m.BusinessUnitId
			and live.InventoryId = m.InventoryId
			and live.destinationID = m.destinationID
			
	insert Merchandising.dashboard.SiteMetricsByStoreInventory
	(
		BusinessUnitId,
		InventoryId,
		destinationID,
		SearchPageViews,
		DetailPageViews,
		EmailLeads,
		MapsViewed,
		AdPrints
	)
	select
		live.BusinessUnitId,
		live.InventoryId,
		live.destinationID,
		sum(live.SearchPageViews),
		sum(live.DetailPageViews),
		sum(live.EmailLeads),
		sum(live.MapsViewed),
		sum(live.AdPrints)
	from
		Merchandising.dashboard.SiteMetricsByStoreDateInventory live
	where
		exists(
			select *
			from @metrics m
			where
				live.BusinessUnitId = m.BusinessUnitId
				and live.InventoryId = m.InventoryId
				and live.destinationID = m.destinationId
		)
	
	group by
		live.BusinessUnitId,
		live.InventoryId,
		live.destinationID
	
	







	
	delete live
	from
		Merchandising.dashboard.SiteMetricsByStoreDate live
		join @metrics m
			on live.BusinessUnitId = m.BusinessUnitId
			and live.destinationID = m.destinationID
			and live.Date = m.Date

			
	insert Merchandising.dashboard.SiteMetricsByStoreDate
	(
		BusinessUnitId,
		InventoryType,
		destinationID,
		Date,
		SearchPageViews,
		DetailPageViews,
		EmailLeads,
		MapsViewed,
		AdPrints
	)
	select
		live.BusinessUnitId,
		i.InventoryType,
		live.destinationID,
		live.Date,
		sum(live.SearchPageViews),
		sum(live.DetailPageViews),
		sum(live.EmailLeads),
		sum(live.MapsViewed),
		sum(live.AdPrints)
	from
		Merchandising.dashboard.SiteMetricsByStoreDateInventory live
		join FLDW.dbo.Inventory i
			on live.BusinessUnitId = i.BusinessUnitId
			and live.InventoryId = i.InventoryID
	where
		exists(
			select *
			from
				@metrics m
			where
				live.BusinessUnitId = m.BusinessUnitId
				and live.destinationID = m.destinationID
				and live.Date = m.Date
		)
	group by
		live.BusinessUnitId,
		i.InventoryType,
		live.destinationID,
		live.Date
	
	
	
	
	
	
	
	
	delete live
	from
		Merchandising.dashboard.SiteMetricsByStore live
		join @metrics m
			on live.BusinessUnitId = m.BusinessUnitId
			and live.destinationID = m.destinationID

			
	insert Merchandising.dashboard.SiteMetricsByStore
	(
		BusinessUnitId,
		InventoryType,
		destinationID,
		SearchPageViews,
		DetailPageViews,
		EmailLeads,
		MapsViewed,
		AdPrints
	)
	select
		live.BusinessUnitId,
		i.InventoryType,
		live.destinationID,
		sum(live.SearchPageViews),
		sum(live.DetailPageViews),
		sum(live.EmailLeads),
		sum(live.MapsViewed),
		sum(live.AdPrints)
	from
		Merchandising.dashboard.SiteMetricsByStoreDateInventory live
		join FLDW.dbo.Inventory i
			on live.BusinessUnitId = i.BusinessUnitId
			and live.InventoryId = i.InventoryID
	where
		exists(
			select *
			from 
				@metrics m
			where
				live.BusinessUnitId = m.BusinessUnitId
				and live.destinationID = m.destinationID
		)
	group by
		live.BusinessUnitId,
		i.InventoryType,
		live.destinationID
		

	commit
	


			
end try
begin catch
	rollback
	
	set @error =
		'Failed to transfer temporary data to live storage! ' +
		' ErrorNumber ' + isnull(convert(varchar(11), error_number()), '<null>') +
		', ErrorSeverity ' + isnull(convert(varchar(11), error_severity()), '<null>') +
		', ErrorState ' + isnull(convert(varchar(11), error_state()), '<null>') +
		', ErrorProcedure ' + isnull(error_procedure(), '<null>') +
		', ErrorLine ' + isnull(convert(varchar(11), error_line()), '<null>') +
		', ErrorMessage ' + isnull(error_message(), '<null>')
	
	raiserror(@error, 16, 1)
end catch



print 'duplicate date data points discarded: ' + convert(varchar(11), @dupeDatesDiscarded)
go
