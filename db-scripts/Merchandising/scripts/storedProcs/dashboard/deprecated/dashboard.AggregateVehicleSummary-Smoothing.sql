if object_id('dashboard.AggregateVehicleSummary') is not null
	drop procedure dashboard.AggregateVehicleSummary
go

/*
	This proc takes an incoming file from fetch by @LoadID,
	loads up all the distinct VINs, and for each VIN in the file
	calculates aggregations from the minimum month in the data
	forward through the maximum month then replaces the data in 
	dashboard.SiteMetricsByStoreDataInventory in a single transaction.
	
*/
create procedure dashboard.AggregateVehicleSummary
	@LoadID int
	, @destinationID int
as
set nocount on
set transaction isolation level read uncommitted


declare @Vins table
(
	VIN varchar(17) not null primary key clustered
	, InventoryId int not null
	, MinMonth datetime
	, MaxMonth datetime
)

declare @VinMonthMetrics table
(
	id int identity (0,1) not null
	, BusinessUnitId int not null
	, destinationID int not null
	, Vin varchar(17) not null
	--
	, SearchPageViews int not null
	, DetailPageViews int not null
	, EmailLeads int not null
	, MapsViewed int not null
	, AdPrints int not null
	--
	, NumberOfDaysToFill tinyint not null
	, DiffSearches int not null
	, Date datetime null
)

declare @MonthDays table
(
	Month datetime not null primary key clustered
	, NumberOfDays tinyint not null
)


declare @metrics table
(
	id int identity(0,1) not null
	, BusinessUnitId int not null
	, InventoryId int not null
	, destinationID int not null
	, Date datetime not null
	, LifetimeSearchPageViews int not null
	, LifetimeDetailPageViews int not null
	, LifetimeEmailLeads int not null
	, LifetimeMapsViewed int not null
	, LifetimeAdPrints int not null
	, SearchPageViews int not null
	, DetailPageViews int not null
	, EmailLeads int not null
	, MapsViewed int not null
	, AdPrints int not null
	--
	primary key clustered 
	(
		BusinessUnitId
		, InventoryId
		, destinationID
		, Date
	)
)


declare
	@Vin varchar(17)
	, @MinMonth datetime
	, @MaxMonth datetime
	, @currentMonth datetime
	, @monthlySearchPageViews int
	, @monthlyDataPoints tinyint
	, @avgDailySearchPageViews int
	, @necessaryDataPoints tinyint
	, @errorMessage varchar(max)
	, @rows int
	, @currentDay tinyint
	
	
	
	
	
declare
	@id int
	, @metricsRows int
	, @BusinessUnitId int
	, @InventoryId int
	--
	, @Date datetime
	--
	, @LifetimeSearchPageViews int
	, @LifetimeDetailPageViews int
	, @LifetimeEmailLeads int
	, @LifetimeMapsViewed int
	, @LifetimeAdPrints int
	--
	, @loopDate datetime
	, @GapSearchPageViews int
	, @GapDetailPageViews int
	, @GapEmailLeads int
	, @GapMapsViewed int
	, @GapAdPrints int
	, @FinalGapSearchPageViews int
	, @FinalGapDetailPageViews int
	, @FinalGapEmailLeads int
	, @FinalGapMapsViewed int
	, @FinalGapAdPrints int	
	, @i int
	--
	, @DailySearchPageViews int
	, @DailyDetailPageViews int
	, @DailyEmailLeads int
	, @DailyMapsViewed int
	, @DailyAdPrints int
	--
	, @PrevLifetimeSearchPageViews int
	, @PrevLifetimeDetailPageViews int
	, @PrevLifetimeEmailLeads int
	, @PrevLifetimeMapsViewed int
	, @PrevLifetimeAdPrints int
	--
	, @NumberOfDaysToFill tinyint
	
		
select 'debug: Vin and Month selection is hardcoded!'	
insert @Vins
(
	Vin
	, InventoryId
	, MinMonth
	, MaxMonth
)
select
	Vin
	, InventoryId
	, min(Month)
	, max(Month)
from Merchandising.dashboard.RawMetrics
where
	Load_ID = @LoadID
	and destinationID = @destinationID
	and VIN in ('1D8HN54P78B133929', '1FMEU63E67UB19206')
	-- and Month in ('2011-08-01 00:00:00.000', '2011-09-01 00:00:00.000')
group by
	VIN
	, InventoryId





/* loop through vins */

while (1=1)
begin
	select top 1
		@Vin = Vin
		, @InventoryId = InventoryId
		, @MinMonth = MinMonth
		, @MaxMonth = MaxMonth
	from @Vins
	
	if (@@rowcount <> 1) break
	delete @Vins where Vin = @Vin
	
--	select 'debug months'
--		, MinMonth = @MinMonth
--		, MaxMonth = @MaxMonth
		
		
		
		
		
		
	/* loop through months for this vin */		
	
	set @currentMonth = @MinMonth
	while (@currentMonth <= @MaxMonth)
	begin
	
		delete @VinMonthMetrics
		
		select
			@LifetimeSearchPageViews = 0
			, @LifetimeDetailPageViews = 0
			, @LifetimeEmailLeads = 0
			, @LifetimeMapsViewed = 0
			, @LifetimeAdPrints = 0
				
				
		/* find out how many data points the month should have */
		
		if not exists(select * from @MonthDays where Month = @currentMonth)
		begin
			-- insert a row for this particular month for how many data points we expect
			-- which is either the total number of days in the month, or, if it is the current
			-- month, today's date minus one since we expect at least a one day lag
			if (datepart(year, @currentMonth) = datepart(year, getdate()) and datepart(month, @currentMonth) = datepart(month, getdate()))
			begin
				insert @MonthDays (Month, NumberOfDays) values (@currentMonth, datepart(day, getdate()) - 1)
			end
			else
			begin
				insert @MonthDays (Month, NumberOfDays) values (@currentMonth, datediff(day, @currentMonth, dateadd(month, 1, @currentMonth)))
			end
			
		end
		
		select @necessaryDataPoints = NumberOfDays
		from @MonthDays
		where Month = @currentMonth
		
		
		
		
		
		
		
		/* load the data for this month and Vin */
		
		insert @VinMonthMetrics
		(
			BusinessUnitId
			, destinationID
			, Vin
			, SearchPageViews
			, DetailPageViews
			, EmailLeads
			, MapsViewed
			, AdPrints
			, NumberOfDaysToFill
			, DiffSearches
		)
		select
			m.businessUnitID
			, @destinationID
			, m.Vin
			, m.SearchPageViews
			, m.DetailPageViews
			, m.EmailLeads
			, m.MapsViewed
			, m.AdPrints
			, 1
			, -1
		from Merchandising.dashboard.RawMetrics m
		where
			VIN = @Vin
			and Month = @currentMonth
		order by m.SearchPageViews
		
		select
			@monthlySearchPageViews = max(SearchPageViews) - min(SearchPageViews)
			, @monthlyDataPoints = count(*)
			, @avgDailySearchPageViews = (max(SearchPageViews) - min(SearchPageViews)) / count(*)
		from @VinMonthMetrics
		
--		select
--			'debug data points'
--			, monthlyDataPoints = @monthlyDataPoints
--			, necessaryDataPoints = @necessaryDataPoints
			
			
		
		if (@avgDailySearchPageViews = 0)
		begin
			-- if there is only one row or all the data points are the same the max - min above will make this 0
			-- and we'll get divide by zero later
			set @avgDailySearchPageViews = 1
		end
		
		
		
			
		/* use heuristic to make sure we can enter data for every date in the month */
		
		if (@monthlyDataPoints <> @necessaryDataPoints)
		begin
			-- we need to figure out which data points will be used to gap fill more than one date
			-- the data in @VinMonthMetrics is already sorted by SearchPageViews
			-- calculate the difference between each data point and figure out if that data point
			-- can "be responsible" for more than 1 day based on how many avgDailySearchPageViews can
			-- fit into it
			
			update v set
				NumberOfDaysToFill = 
					case
						when potential.DaysCanFit < 1 then 1
						when potential.DaysCanFit > 255 then 255
						else potential.DaysCanFit
					end
				, DiffSearches = potential.DiffSearches
			from
				@VinMonthMetrics v
				join
				(
					select
						greater.id
						, DiffSearches = greater.SearchPageViews - lesser.SearchPageViews
						, DaysCanFit = (greater.SearchPageViews - lesser.SearchPageViews) / @avgDailySearchPageViews
					from
						@VinMonthMetrics lesser
						join @VinMonthMetrics greater
							on lesser.id + 1 = greater.id

					union all

					-- get the first row, which won't join in the id offset above				
					select
						id
						, DiffSearches = SearchPageViews
						, DaysCanFit = SearchPageViews / @avgDailySearchPageViews
					from @VinMonthMetrics
					where id = (select min(id) from @VinMonthMetrics)
				) potential
					on v.id = potential.id
					
			if exists(select * from @VinMonthMetrics where DiffSearches = -1)
			begin
				set @errorMessage = 
					'logic flaw in dashboard.AggregateVehicleSummary, all DiffSearches should be properly calculated.  Vin: ' 
					+ @Vin + ' CurrentMonth: '
					+ convert(varchar(10), @currentMonth, 101)
				raiserror(@errorMessage, 16, 1)
			end


			-- select 'debug VinMetrics before data heuristics', * from @VinMonthMetrics order by NumberOfDaysToFill desc, DiffSearches desc
		
			
			-- it is now possible that we have accounted for too many days in the period,
			-- so shave days off of the smallest data points until we have the right number
			-- of days for the month.
			while (select sum(NumberOfDaysToFill) from @VinMonthMetrics) > @necessaryDataPoints
			begin

				update @VinMonthMetrics
					set NumberOfDaysToFill = case when (select max(NumberOfDaysToFill) from @VinMonthMetrics) > 1 then 1 else 0 end
				where
					id =
						(
							select top 1 id
							from @VinMonthMetrics
							where NumberOfDaysToFill > 1
							order by
								DiffSearches
								, SearchPageViews
						)
			end
			
			
			
			-- now, we may not have enough data points, either because the code that shaved off days
			-- above took off too many, or, because we just didn't get enough data points
			-- by doing the original update to NumberOfDaysToFill / @avgDailySearchPageViews
			-- add 1 day to as many data points as we need to assign data points that will be used to gap fill
			while (select sum(NumberOfDaysToFill) from @VinMonthMetrics) < @necessaryDataPoints
			begin
				select @rows = @necessaryDataPoints - sum(NumberOfDaysToFill)
				from @VinMonthMetrics
				
				update v
					set NumberOfDaysToFill = NumberOfDaysToFill + 1
				from
					@VinMonthMetrics v
					join 
					(
						select top (@rows)
							id
						from @VinMonthMetrics
						order by
							DiffSearches desc
							, SearchPageViews asc
					) a
						on v.id = a.id							
			end
			
			if (select sum(NumberOfDaysToFill) from @VinMonthMetrics) < @necessaryDataPoints
			begin
				raiserror('logic bug in calculating data points, should be exactly the right number of days by now!', 16, 1)
			end
		end -- end date heuristics
		

		
		--select 'debug VinMetrics after data heuristics', * from @VinMonthMetrics order by NumberOfDaysToFill desc, DiffSearches desc
		
		--select 'debug VinMetrics in data order b4 lifetime', * from @VinMonthMetrics order by SearchPageViews
		
		
				
				
				
				
				
				
				
		/* time to finally calculate the daily data points for the month/Vin */
		
		
		
		-- update all data points to reflect the running lifetime balance based on the max previous row in
		-- either dashboard.SiteMetricsByStoreDateInventory or @metrics
		if (@currentMonth = @MinMonth)
		begin
			select top 1
				@LifetimeSearchPageViews = LifetimeSearchPageViews
				, @LifetimeDetailPageViews = LifetimeDetailPageViews
				, @LifetimeEmailLeads = LifetimeEmailLeads
				, @LifetimeMapsViewed = LifetimeMapsViewed
				, @LifetimeAdPrints = LifetimeAdPrints
			from Merchandising.dashboard.SiteMetricsByStoreDateInventory
			where
				BusinessUnitId = @BusinessUnitId
				and InventoryId = @InventoryId
				and destinationID = @destinationId
				and Date < @currentMonth
			order by
				Date desc
		end
		else
		begin
			select top 1
				@LifetimeSearchPageViews = LifetimeSearchPageViews
				, @LifetimeDetailPageViews = LifetimeDetailPageViews
				, @LifetimeEmailLeads = LifetimeEmailLeads
				, @LifetimeMapsViewed = LifetimeMapsViewed
				, @LifetimeAdPrints = LifetimeAdPrints
			from @metrics
			where
				BusinessUnitId = @BusinessUnitId
				and InventoryId = @InventoryId
				and destinationID = @destinationId
				and Date < @currentMonth
			order by
				Date desc
		end
		
		if (@LifetimeSearchPageViews is null)
		begin
			select
				@LifetimeSearchPageViews = 0
				, @LifetimeDetailPageViews = 0
				, @LifetimeEmailLeads = 0
				, @LifetimeMapsViewed = 0
				, @LifetimeAdPrints = 0
		end
		
		update @VinMonthMetrics set
			SearchPageViews = SearchPageViews + @LifetimeSearchPageViews
			, DetailPageViews = DetailPageViews + @LifetimeDetailPageViews
			, EmailLeads = EmailLeads + @LifetimeEmailLeads
			, MapsViewed = MapsViewed + @LifetimeMapsViewed
			, AdPrints = AdPrints + @LifetimeAdPrints
		
			
		--select 'debug VinMetrics in data order post lifetime', * from @VinMonthMetrics order by SearchPageViews			
				
				
		set @currentDay = 0
		while (@currentDay < @necessaryDataPoints)
		begin
			select					
				@PrevLifetimeSearchPageViews = @LifetimeSearchPageViews
				, @PrevLifetimeDetailPageViews = @LifetimeDetailPageViews
				, @PrevLifetimeEmailLeads = @LifetimeEmailLeads
				, @PrevLifetimeMapsViewed = @LifetimeMapsViewed
				, @PrevLifetimeAdPrints = @LifetimeAdPrints

			select top 1
				@id = vmm.id
				, @BusinessUnitId = vmm.BusinessUnitId
				, @destinationID = vmm.destinationID
				, @Vin = vmm.Vin
				, @Date = dateadd(day, @currentDay, @currentMonth)
				, @NumberOfDaysToFill = vmm.NumberOfDaysToFill
				, @LifetimeSearchPageViews = vmm.SearchPageViews
				, @LifetimeDetailPageViews = vmm.DetailPageViews
				, @LifetimeEmailLeads = vmm.EmailLeads
				, @LifetimeMapsViewed = vmm.MapsViewed
				, @LifetimeAdPrints = vmm.AdPrints
			from
				@VinMonthMetrics vmm
			where Date is null
			order by
				SearchPageViews
				
			update @VinMonthMetrics set Date = @Date where id = @id
							
			
		
			
			
--				select 'debug variables 1'
--				, id = @id
--				, InventoryId = @InventoryId
--				, CurrentDateLoaded = @Date
--				, CurrentTotalSearchPageViews = @CurrentTotalSearchPageViews
--				
--				, LifetimeSearchPageViews = @LifetimeSearchPageViews
--				, PreviousTotalSearchPageViews = @PreviousTotalSearchPageViews
			
		
		


			select
				@loopDate = @Date
				, @i = 1
				--	
				, @DailySearchPageViews = case when @LifetimeSearchPageViews > @PrevLifetimeSearchPageViews then @LifetimeSearchPageViews - @PrevLifetimeSearchPageViews else 0 end
				, @DailyDetailPageViews = case when @LifetimeDetailPageViews > @PrevLifetimeDetailPageViews then @LifetimeDetailPageViews - @PrevLifetimeDetailPageViews else 0 end
				, @DailyEmailLeads = case when @LifetimeEmailLeads > @PrevLifetimeEmailLeads then @LifetimeEmailLeads - @PrevLifetimeEmailLeads else 0 end
				, @DailyMapsViewed = case when @LifetimeMapsViewed > @PrevLifetimeMapsViewed then @LifetimeMapsViewed - @PrevLifetimeMapsViewed else 0 end
				, @DailyAdPrints = case when @LifetimeAdPrints > @PrevLifetimeAdPrints then @LifetimeAdPrints - @PrevLifetimeAdPrints else 0 end
				
			

				
			-- calculate the numbers we will use since there may be days we are backfilling
			select
				  @GapSearchPageViews = @DailySearchPageViews / @NumberOfDaysToFill
				, @GapDetailPageViews = @DailyDetailPageViews / @NumberOfDaysToFill
				, @GapEmailLeads = @DailyEmailLeads / @NumberOfDaysToFill
				, @GapMapsViewed = @DailyMapsViewed / @NumberOfDaysToFill
				, @GapAdPrints = @DailyAdPrints / @NumberOfDaysToFill

				
			-- now, we want to make sure we insert the proper numbers
			-- for example, if there are 3 SearchPageViews and 5 days, integer math means we will
			-- aggregate 0 SearchPageViews for 5 days and completely lose the incoming 3 value.
			-- 
			-- we need to make sure that we account for the stats coming in, so we need to calculate
			-- a final "catch up" number to insert on the last day of the loop (@loopDate = @CurrentLoadDate)
			-- and because of integer math it will vary for each data point.
					

			select
				  @FinalGapSearchPageViews = @DailySearchPageViews - (@DailySearchPageViews / @NumberOfDaysToFill * @NumberOfDaysToFill)
				, @FinalGapDetailPageViews = @DailyDetailPageViews - (@DailyDetailPageViews / @NumberOfDaysToFill * @NumberOfDaysToFill)
				, @FinalGapEmailLeads = @DailyEmailLeads - (@DailyEmailLeads / @NumberOfDaysToFill * @NumberOfDaysToFill)
				, @FinalGapMapsViewed = @DailyMapsViewed - (@DailyMapsViewed / @NumberOfDaysToFill * @NumberOfDaysToFill)
				, @FinalGapAdPrints = @DailyAdPrints - (@DailyAdPrints / @NumberOfDaysToFill * @NumberOfDaysToFill)
				
				
--				select 'debug values divided by NumDays'
--					, id = @id
--					, loopDate = @loopDate
--					, NumberOfDaysToFill = @NumberOfDaysToFill
--					, DailySearchPageViews = @DailySearchPageViews
--					, DailyDetailPageViews = @DailyDetailPageViews
--					, DailyEmailLeads = @DailyEmailLeads
--					, DailyMapsViewed = @DailyMapsViewed
--					, DailyAdPrints = @DailyAdPrints
--					, GapSearchPageViews = @GapSearchPageViews
--					, GapDetailPageViews = @GapDetailPageViews
--					, GapEmailLeads = @GapEmailLeads
--					, GapMapsViewed = @GapMapsViewed
--					, GapAdPrints = @GapAdPrints
--					, FinalGapSearchPageViews = @FinalGapSearchPageViews
--					, FinalGapDetailPageViews = @FinalGapDetailPageViews
--					, FinalGapEmailLeads = @FinalGapEmailLeads
--					, FinalGapMapsViewed = @FinalGapMapsViewed
--					, FinalGapAdPrints = @FinalGapAdPrints
					
				while (@i <= @NumberOfDaysToFill)
				begin
					insert @metrics
					(
						BusinessUnitId,
						destinationID,
						Date,
						InventoryId,
						
						LifetimeSearchPageViews,
						LifetimeDetailPageViews,
						LifetimeEmailLeads,
						LifetimeMapsViewed,
						LifetimeAdPrints,
						
						SearchPageViews,
						DetailPageViews,
						EmailLeads,
						MapsViewed,
						AdPrints
					)
					values
					(
						@BusinessUnitId, -- BusinessUnitId - int
						@destinationID, -- destinationID - int
						@loopDate, -- Date - datetime
						@InventoryId, -- InventoryId - int

						case when @i = @NumberOfDaysToFill then @FinalGapSearchPageViews + @i * @GapSearchPageViews else @i * @GapSearchPageViews end + @PrevLifetimeSearchPageViews, -- LifetimeSearchPageViews - int
						case when @i = @NumberOfDaysToFill then @FinalGapDetailPageViews + @i * @GapDetailPageViews else @i * @GapDetailPageViews end + @PrevLifetimeDetailPageViews, -- LifetimeDetailPageViews - int
						case when @i = @NumberOfDaysToFill then @FinalGapEmailLeads + @i * @GapEmailLeads else @i * @GapEmailLeads end + @PrevLifetimeEmailLeads, -- LifetimeEmailLeads - int
						case when @i = @NumberOfDaysToFill then @FinalGapMapsViewed + @i * @GapMapsViewed else @i * @GapMapsViewed end + @PrevLifetimeMapsViewed, -- LifetimeMapsViewed - int
						case when @i = @NumberOfDaysToFill then @FinalGapAdPrints + @i * @GapAdPrints else @i * @GapAdPrints end + @PrevLifetimeAdPrints, -- LifetimeAdPrints - int
						--
						case when @i = @NumberOfDaysToFill then @FinalGapSearchPageViews + @GapSearchPageViews else @GapSearchPageViews end , -- SearchPageViews - int
						case when @i = @NumberOfDaysToFill then @FinalGapDetailPageViews + @GapDetailPageViews else @GapDetailPageViews end, -- DetailPageViews - int
						case when @i = @NumberOfDaysToFill then @FinalGapEmailLeads + @GapEmailLeads else @GapEmailLeads end, -- EmailLeads - int
						case when @i = @NumberOfDaysToFill then @FinalGapMapsViewed + @GapMapsViewed else @GapMapsViewed end, -- MapsViewed - int
						case when @i = @NumberOfDaysToFill then @FinalGapAdPrints + @GapAdPrints else @GapAdPrints end  -- AdPrints - int
					)
					
					select
						@i = @i + 1
						, @loopDate = dateadd(day, 1, @loopDate)
						, @currentDay = @currentDay + 1
						
				end

			
		--
		--	select 'debug variables'
		--		, id = @id
		--		, BusinessUnitId = @BusinessUnitId
		--		, destinationID = @destinationID
		--		, InventoryId = @InventoryId
		--		, Vin = @Vin
		--		, CurrentDateLoaded = @Date
		--		, CurrentTotalSearchPageViews = @CurrentTotalSearchPageViews
		--		, CurrentTotalDetailPageViews = @CurrentTotalDetailPageViews
		--		, CurrentTotalEmailLeads = @CurrentTotalEmailLeads
		--		, CurrentTotalMapsViewed = @CurrentTotalMapsViewed
		--		, CurrentTotalAdPrints = @CurrentTotalAdPrints
		--		--
		--		, PreviousTotalSearchPageViews = @PreviousTotalSearchPageViews
		--		, PreviousTotalDetailPageViews = @PreviousTotalDetailPageViews
		--		, PreviousTotalEmailLeads = @PreviousTotalEmailLeads
		--		, PreviousTotalMapsViewed = @PreviousTotalMapsViewed
		--		, PreviousTotalAdPrints = @PreviousTotalAdPrints
		--	
		--	select 'debug @fileMetrics'
		--		, *
		--	from @fileMetrics
		--	where InventoryId = @InventoryId
		--	order by id


		end -- @currentDay < @necessaryDataPoints (scrolling through each point in @VinMonthMetrics)

		
		set @currentMonth = dateadd(month, 1, @currentMonth)
	end -- end month loop for Vin
	
end -- end Vin loop

select 'debug: need to wrap entire vin loop in try...catch so we do not later commit bad data due to logic errors'







/* now replace live aggregate tables with this new data */

begin try
	begin tran

	delete live
	from
		Merchandising.dashboard.SiteMetricsByStoreDateInventory live
		join @metrics m
			on live.BusinessUnitId = m.BusinessUnitId
			and live.InventoryId = m.InventoryId
			and live.destinationID = m.destinationId
			and live.Date = m.Date
						
	insert Merchandising.dashboard.SiteMetricsByStoreDateInventory
	(
		BusinessUnitId,
		InventoryId,
		destinationID,
		Date,
		LifetimeSearchPageViews,
		LifetimeDetailPageViews,
		LifetimeEmailLeads,
		LifetimeMapsViewed,
		LifetimeAdPrints,
		SearchPageViews,
		DetailPageViews,
		EmailLeads,
		MapsViewed,
		AdPrints
	)
	select
		BusinessUnitId,
		InventoryId,
		destinationID,
		Date,
		LifetimeSearchPageViews,
		LifetimeDetailPageViews,
		LifetimeEmailLeads,
		LifetimeMapsViewed,
		LifetimeAdPrints,
		SearchPageViews,
		DetailPageViews,
		EmailLeads,
		MapsViewed,
		AdPrints
	from @metrics
	
	
		
	delete live
	from
		Merchandising.dashboard.SiteMetricsByStoreInventory live
		join @metrics m
			on live.BusinessUnitId = m.BusinessUnitId
			and live.InventoryId = m.InventoryId
			and live.destinationID = m.destinationID
			
	insert Merchandising.dashboard.SiteMetricsByStoreInventory
	(
		BusinessUnitId,
		InventoryId,
		destinationID,
		LifetimeSearchPageViews,
		LifetimeDetailPageViews,
		LifetimeEmailLeads,
		LifetimeMapsViewed,
		LifetimeAdPrints
	)
	select
		live.BusinessUnitId,
		live.InventoryId,
		live.destinationID,
		sum(live.SearchPageViews),
		sum(live.DetailPageViews),
		sum(live.EmailLeads),
		sum(live.MapsViewed),
		sum(live.AdPrints)
	from
		Merchandising.dashboard.SiteMetricsByStoreDateInventory live
	where
		exists(
			select *
			from @metrics m
			where
				live.BusinessUnitId = m.BusinessUnitId
				and live.InventoryId = m.InventoryId
				and live.destinationID = m.destinationId
				-- leave out the date from this join since we have to replace this inventory for all dates in target
		)
	
	group by
		live.BusinessUnitId,
		live.InventoryId,
		live.destinationID
	
	
	
	delete live
	from
		Merchandising.dashboard.SiteMetricsByStoreDate live
		join @metrics m
			on live.BusinessUnitId = m.BusinessUnitId
			and live.destinationID = m.destinationID
			and live.Date = m.Date
			
			
	insert Merchandising.dashboard.SiteMetricsByStoreDate
	(
		BusinessUnitId,
		InventoryType,
		destinationID,
		Date,
		LifetimeSearchPageViews,
		LifetimeDetailPageViews,
		LifetimeEmailLeads,
		LifetimeMapsViewed,
		LifetimeAdPrints,
		SearchPageViews,
		DetailPageViews,
		EmailLeads,
		MapsViewed,
		AdPrints
	)
	select
		live.BusinessUnitId,
		i.InventoryType,
		live.destinationID,
		live.Date,
		sum(live.LifetimeSearchPageViews),
		sum(live.LifetimeDetailPageViews),
		sum(live.LifetimeEmailLeads),
		sum(live.LifetimeMapsViewed),
		sum(live.LifetimeAdPrints),
		sum(live.SearchPageViews),
		sum(live.DetailPageViews),
		sum(live.EmailLeads),
		sum(live.MapsViewed),
		sum(live.AdPrints)
	from
		Merchandising.dashboard.SiteMetricsByStoreDateInventory live
		join FLDW.dbo.Inventory i
			on live.BusinessUnitId = i.BusinessUnitId
			and live.InventoryId = i.InventoryID
	where
		exists(
			select *
			from
				@metrics m
			where
				live.BusinessUnitId = m.BusinessUnitId
				and live.destinationID = m.destinationID
				and live.Date = m.Date
				-- leave out the inventoryId from this join since we have to replace these dates for all inventory in target
		)
	group by
		live.BusinessUnitId,
		i.InventoryType,
		live.destinationID,
		live.Date
	
	
	
	delete live
	from
		Merchandising.dashboard.SiteMetricsByStore live
		join @metrics m
			on live.BusinessUnitId = m.BusinessUnitId
			and live.destinationID = m.destinationID
		join FLDW.dbo.Inventory i
			on m.BusinessUnitId = i.BusinessUnitId
			and m.InventoryId = i.InventoryID
			and live.InventoryType = i.InventoryType
			
	insert Merchandising.dashboard.SiteMetricsByStore
	(
		BusinessUnitId,
		InventoryType,
		destinationID,
		LifetimeSearchPageViews,
		LifetimeDetailPageViews,
		LifetimeEmailLeads,
		LifetimeMapsViewed,
		LifetimeAdPrints
	)
	select
		live.BusinessUnitId,
		i.InventoryType,
		live.destinationID,
		sum(live.SearchPageViews),
		sum(live.DetailPageViews),
		sum(live.EmailLeads),
		sum(live.MapsViewed),
		sum(live.AdPrints)
	from
		Merchandising.dashboard.SiteMetricsByStoreDateInventory live
		join FLDW.dbo.Inventory i
			on live.BusinessUnitId = i.BusinessUnitId
			and live.InventoryId = i.InventoryID
	where
		exists(
			select *
			from 
				@metrics m
			where
				live.BusinessUnitId = m.BusinessUnitId
				and live.destinationID = m.destinationID
		)
	group by
		live.BusinessUnitId,
		i.InventoryType,
		live.destinationID
		

	commit
	


			
end try
begin catch
	rollback
	declare @error varchar(1000)
	
	set @error =
		'ErrorNumber ' + isnull(convert(varchar(11), error_number()), '<null>') +
		', ErrorSeverity ' + isnull(convert(varchar(11), error_severity()), '<null>') +
		', ErrorState ' + isnull(convert(varchar(11), error_state()), '<null>') +
		', ErrorProcedure ' + isnull(error_procedure(), '<null>') +
		', ErrorLine ' + isnull(convert(varchar(11), error_line()), '<null>') +
		', ErrorMessage ' + isnull(error_message(), '<null>')
	
	raiserror(@error, 16, 1)
end catch



go
