USE [Merchandising]


if object_id('dashboard.DashboardData#Fetch') is not null
	drop procedure dashboard.DashboardData#Fetch
GO

Create procedure [dashboard].[DashboardData#Fetch]
	@businessUnitID int
as

set nocount on
set transaction isolation level read uncommitted
-- first get counts

-- Get Counts 

--select * from #Inventory

select 0 as InventoryType,  (select count(*) from FLDW.dbo.InventoryActive where BusinessUnitID = @businessUnitID) as InventoryCount 
Union All
select 1 as InventoryType,  (select count(*) from FLDW.dbo.InventoryActive where InventoryType = 1 and BusinessUnitID = @businessUnitID) as InventoryCount 
Union All
select 2 as InventoryType,  (select count(*) from FLDW.dbo.InventoryActive where InventoryType = 2 and BusinessUnitID = @businessUnitID) as InventoryCount 




select '1' as UsedOrNewType, 148 as SearchCount, 78 as ViewCount, 25 as ActionCount, 7 as Period, 'AutoTrader' as Destination

UNION ALL


select '1' as UsedOrNewType, 154 as SearchCount, 76 as ViewCount, 35 as ActionCount, 14 as Period, 'AutoTrader' as Destination

UNION ALL

select '1' as UsedOrNewType, 132 as SearchCount, 56 as ViewCount, 45 as ActionCount, 30 as Period, 'AutoTrader' as Destination

UNION ALL

select '1' as UsedOrNewType, 178 as SearchCount, 98 as ViewCount, 23 as ActionCount, 180 as Period, 'AutoTrader' as Destination

UNION ALL

select '2' as UsedOrNewType, 112 as SearchCount, 45 as ViewCount, 15 as ActionCount, 7 as Period, 'AutoTrader' as Destination

UNION ALL


select '2' as UsedOrNewType, 154 as SearchCount, 67 as ViewCount, 17 as ActionCount, 14 as Period, 'AutoTrader' as Destination

UNION ALL

select '2' as UsedOrNewType, 198 as SearchCount, 9 as ViewCount, 20 as ActionCount, 30 as Period, 'AutoTrader' as Destination

UNION ALL

select '2' as UsedOrNewType, 187 as SearchCount, 54 as ViewCount, 7 as ActionCount, 180 as Period, 'AutoTrader' as Destination

UNION ALL


select '1' as UsedOrNewType, 148 as SearchCount, 78 as ViewCount, 25 as ActionCount, 7 as Period, 'Cars' as Destination

UNION ALL


select '1' as UsedOrNewType, 99 as SearchCount, 29 as ViewCount, 8 as ActionCount, 14 as Period, 'Cars' as Destination

UNION ALL

select '1' as UsedOrNewType, 55 as SearchCount, 46 as ViewCount, 12 as ActionCount, 30 as Period, 'Cars' as Destination

UNION ALL

select '1' as UsedOrNewType, 132 as SearchCount, 43 as ViewCount, 34 as ActionCount, 180 as Period, 'Cars' as Destination

UNION ALL

select '2' as UsedOrNewType, 154 as SearchCount, 98 as ViewCount, 54 as ActionCount, 7 as Period, 'Cars' as Destination

UNION ALL


select '2' as UsedOrNewType, 167 as SearchCount, 50 as ViewCount, 10 as ActionCount, 14 as Period, 'Cars' as Destination

UNION ALL

select '2' as UsedOrNewType, 156 as SearchCount, 56 as ViewCount, 13 as ActionCount, 30 as Period, 'Cars' as Destination

UNION ALL

select '2' as UsedOrNewType, 108 as SearchCount, 44 as ViewCount, 12 as ActionCount, 180 as Period, 'Cars' as Destination





GO

GRANT EXECUTE ON dashboard.DashboardData#Fetch TO MerchandisingUser 
GO
