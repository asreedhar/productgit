if object_id('dashboard.AggregateVehSummary') is not null
	drop procedure dashboard.AggregateVehSummary
go

/*
	This proc takes an incoming file from fetch by @LoadId,
	and for every Vin/Month combination, if the record is later
	than the last record we've saved in SiteMetricsByStoreDateInventory,
	we upsert this data point.
	
	This data eventually supports the low activity report, which
	is based on data that is rolled up into SiteMetricsByStoreInventory
	which represents a lifetime
*/
create procedure dashboard.AggregateVehSummary
	@LoadId int,
	@DatasourceInterfaceID int,
	@destinationID int
as
set nocount on
set transaction isolation level read uncommitted


-- NOTE Vin should be varchar(17), StockNumber varchar(15) but some of the data is bigger, just let it fail BuId/InvId lookup

declare
	@id int
	, @BusinessUnitId int
	, @Vin varchar(50)
	, @StockNumber varchar(50)
	, @InventoryId int
	, @Month datetime
	, @DateScraped datetime
	, @LastDateScraped datetime

	, @SearchPageViews int
	, @DetailPageViews int
	, @EmailLeads int
	, @MapsViewed int
	, @AdPrints int
	
	, @error varchar(max)



declare @NoInvIdSkipRows int ;  set @NoInvIdSkipRows = 0



/*
	save these data points into dashboard.InventoryAdPerformanceByMonth :
	
	bu
	inv
	month
	
	<stats columns>

	DateScraped
	loadid
		
	once we have that saved, roll it up into dashboard.InventoryAdPerformance
	
	do not populate the other tables...
	
*/



declare @Inputs table
(
	id int not null identity (0,1) primary key clustered
	, BusinessUnitId int null
	, Vin varchar(50) not null
	, StockNumber varchar(50) not null
	, Month datetime not null
	, DateScraped datetime not null
	, SearchPageViews int not null
	, DetailPageViews int not null
	, EmailLeads int not null
	, MapsViewed int not null
	, AdPrints int not null
)


declare @BusinessUnitMonth table
(
	BusinessUnitId int not null
	, Month datetime not null
)

if (@DatasourceInterfaceID = 13)
begin
	insert @Inputs
	(
		BusinessUnitId
		, Vin
		, StockNumber
		, Month
		, DateScraped
		, SearchPageViews
		, DetailPageViews
		, EmailLeads
		, MapsViewed
		, AdPrints
	)
	select
		metrics.businessUnitID
		, metrics.VIN
		, metrics.StockNumber
		, metrics.Month
		, DateScraped = dateadd(day, 0, datediff(day, 0, isnull(metrics.FetchDateInserted, metrics.DateLoaded)))
		, SearchPageViews = metrics.AppearedInSearch
		, DetailPageViews = metrics.DetailsPageViews
		, EmailLeads = metrics.EmailsSent
		, MapsViewed = metrics.MapViews
		, AdPrints = metrics.DetailsPagePrinted
	from
		Merchandising.merchandising.AutoTraderVehSummary metrics
	where
		metrics.Load_ID = @LoadId		
end


if (@DatasourceInterfaceID = 2004)
begin
	insert @Inputs
	(
		BusinessUnitId
		, Vin
		, StockNumber
		, Month
		, DateScraped
		, SearchPageViews
		, DetailPageViews
		, EmailLeads
		, MapsViewed
		, AdPrints
	)
	select
		metrics.businessUnitID
		, metrics.VIN
		, metrics.StockNumber
		, metrics.Month
		, DateScraped = dateadd(day, 0, datediff(day, 0, isnull(metrics.FetchDateInserted, metrics.DateLoaded)))
		, SearchPageViews = metrics.InSearchResult
		, DetailPageViews = metrics.DetailViewed
		, EmailLeads = metrics.EmailSent
		, MapsViewed = metrics.MapViewed
		, AdPrints = metrics.AdsPrinted
	from
		Merchandising.merchandising.CarsDotComVehSummary metrics
	where
		metrics.Load_ID = @LoadId		

	/*
	 load the distinct businessunits and months we have in our input
	 this is used in a couple of different places when we are processing
	 this particular input (datasourceinterfaceid = 2004)
	*/
	insert @BusinessUnitMonth
	(
		BusinessUnitId
		, Month
	)
	select distinct
		businessUnitId
		, month
	from @inputs
		
end
		


if (@DatasourceInterfaceID = 2006)
begin
	insert @Inputs
	(
		Month
		, Vin
		, StockNumber
		, DateScraped
		, SearchPageViews
		, DetailPageViews
		, EmailLeads
		, MapsViewed
		, AdPrints
	)
	select
		Month = dateadd(month, datediff(month, 0, metrics.FeedDate), 0)
		, metrics.VIN
		, metrics.StockNumber
		, DateScraped = metrics.FeedDate
		, metrics.SearchPageViews
		, metrics.DetailPageViews
		, metrics.EmailLeads
		, metrics.MapsViewed
		, metrics.AdPrints
	from
		Merchandising.merchandising.AutoTraderHendrickVehicleSummary metrics
	where
		metrics.LoadId = @LoadId		
end






begin try

	while (1=1)
	begin
		select top 1
			@id = id
			, @BusinessUnitId = BusinessUnitId
			, @InventoryId = null
			, @Vin = Vin
			, @StockNumber = StockNumber
			, @Month = Month
			, @DateScraped = DateScraped
			, @SearchPageViews = SearchPageViews
			, @DetailPageViews = DetailPageViews
			, @EmailLeads = EmailLeads
			, @MapsViewed = MapsViewed
			, @AdPrints = AdPrints
		from @Inputs
		order by id
			
		if (@@rowcount <> 1) break
		delete @Inputs where id = @id
		
		
		-- get businessUnitID/InventoryId from VIN
		if (@BusinessUnitId is not null)
		begin
			select top 1
				@InventoryId = i.InventoryID
			from
				IMT.dbo.tbl_Vehicle v
				join IMT.dbo.Inventory i
					on v.VehicleID = i.VehicleID
			where
				v.Vin = @Vin
				and i.StockNumber = @StockNumber
				and datediff(day, @DateScraped, i.InventoryReceivedDate) < 1
				and i.BusinessUnitID = @BusinessUnitId
			order by
				i.InventoryReceivedDate desc
		end
		else
		begin
			-- no BuId
			select top 1
				@BusinessUnitId = i.BusinessUnitID
				, @InventoryId = i.InventoryID
			from
				IMT.dbo.tbl_Vehicle v
				join IMT.dbo.Inventory i
					on v.VehicleID = i.VehicleID
			where
				v.Vin = @Vin
				and i.StockNumber = @StockNumber
				and datediff(day, @DateScraped, i.InventoryReceivedDate) < 1
			order by
				i.InventoryReceivedDate desc
		end
		
		
		if (@BusinessUnitId is null or @InventoryId is null)
		begin
			-- figure out what to do with these rows when we code the full monty...
			set @NoInvIdSkipRows = @NoInvIdSkipRows + 1
			continue
		end
		
		
		select @LastDateScraped = max(DateScraped)
		from Merchandising.dashboard.InventoryAdPerformanceByMonth
		where
			BusinessUnitId = @BusinessUnitId
			and InventoryId = @InventoryId
			and Month = @Month
			and DestinationId = @DestinationId
						
		if (@DateScraped > isnull(@LastDateScraped, '01/01/1900'))
		begin
			-- save off this record
			begin try
				begin tran
				
					if (@LastDateScraped is null)
					begin
						insert Merchandising.dashboard.InventoryAdPerformanceByMonth
						(
							BusinessUnitId,
							InventoryId,
							Month,
							SearchPageViews,
							DetailPageViews,
							EmailLeads,
							MapsViewed,
							AdPrints,
							DateScraped,
							LastLoadId,
							destinationId
						)
						values
						(
							@BusinessUnitId,
							@InventoryId,
							@Month,
							@SearchPageViews,
							@DetailPageViews,
							@EmailLeads,
							@MapsViewed,
							@AdPrints,
							@DateScraped,
							@LoadId,
							@destinationID
						)
					end
					else
					begin
						update Merchandising.dashboard.InventoryAdPerformanceByMonth set
							SearchPageViews = @SearchPageViews
							, DetailPageViews = @DetailPageViews
							, EmailLeads = @EmailLeads
							, MapsViewed = @MapsViewed
							, AdPrints = @AdPrints
							, DateScraped = @DateScraped
							, LastLoadId = @LoadId
						where
							BusinessUnitId = @BusinessUnitId
							and InventoryId = @InventoryId
							and Month = @Month
							and DestinationId = @DestinationId
					end

					delete Merchandising.dashboard.InventoryAdPerformance
					where
						BusinessUnitId = @BusinessUnitId
						and InventoryId = @InventoryId
						and DestinationId = @destinationID
						
					insert Merchandising.dashboard.InventoryAdPerformance
					(
						BusinessUnitId,
						InventoryId,
						DestinationId,
						SearchPageViews,
						DetailPageViews,
						EmailLeads,
						MapsViewed,
						AdPrints
					)
					select
						@BusinessUnitId
						, @InventoryId
						, @destinationID
						, sum(SearchPageViews)
						, sum(DetailPageViews)
						, sum(EmailLeads)
						, sum(MapsViewed)
						, sum(AdPrints)
					from Merchandising.dashboard.InventoryAdPerformanceByMonth
					where
						BusinessUnitId = @BusinessUnitId
						and InventoryId = @InventoryId
						and DestinationId = @DestinationId
					group by
						BusinessUnitId,
						InventoryId,
						DestinationId
	
				commit
			end try
			begin catch
				rollback
				
				set @error =
					'Failed to commit data to InventoryAdPerformanceByMonth / InventoryAdPerformance! ' +
					' ErrorNumber ' + isnull(convert(varchar(11), error_number()), '<null>') +
					', ErrorSeverity ' + isnull(convert(varchar(11), error_severity()), '<null>') +
					', ErrorState ' + isnull(convert(varchar(11), error_state()), '<null>') +
					', ErrorProcedure ' + isnull(error_procedure(), '<null>') +
					', ErrorLine ' + isnull(convert(varchar(11), error_line()), '<null>') +
					', ErrorMessage ' + isnull(error_message(), '<null>')
				
				raiserror(@error, 16, 1)
			end catch
		end
		
		

		


	end -- @Inputs loop


	/*
	if we're processing cars vehicle summary data, we need to load the 
	recent performance summary table using the vehicle summary data	so 
	we can break the stats out by inventory type (used/new)
	*/
	if @DatasourceInterfaceID = 2004
	begin
	
		begin try

			begin tran
			
			-- delete the rows we're about to replace
			delete ps 
			from Merchandising.dashboard.PerformanceSummary ps
			where
				ps.DestinationId = @destinationId
				and exists 
				(
					  -- only get rows in our input
					  select *
					  from @BusinessUnitMonth 
					  where
							BusinessUnitId = ps.BusinessUnitId 
							and Month = ps.Month
				)

		
			-- replace the rows we just deleted
			; with perfsummary
			as 
			(
			select 
			inv.BusinessUnitId, act.Month, act.DestinationId, inv.InventoryType,
			sum(act.SearchPageViews) SearchPageViews,
			sum(act.DetailPageViews) DetailPageViews,
			sum(act.EmailLeads) EmailLeads,
			sum(act.MapsViewed) MapsViewed,
			sum(act.AdPrints) AdPrints
			from Merchandising.dashboard.InventoryAdPerformanceByMonth act 
			-- get the inventory type
			join IMT.dbo.Inventory inv
				on inv.BusinessUnitId = act.BusinessUnitId
				and inv.InventoryId = act.InventoryId
			where 
				act.DestinationId = @destinationID
				and exists 
				(	-- only get rows in our input
					select *
					from @BusinessUnitMonth
					where 
						BusinessUnitId = act.BusinessUnitId 
						and Month = act.Month
				)
			group by inv.BusinessUnitId, act.Month, act.DestinationId, inv.InventoryType
			)  -- End of CTE
			insert Merchandising.dashboard.PerformanceSummary
				(
					BusinessUnitId
					, DestinationId
					, Month
					, NewSearchPageViews
					, NewDetailPageViews
					, NewEmailLeads
					, NewMapsViewed
					, NewAdPrints
					, UsedSearchPageViews
					, UsedDetailPageViews
					, UsedEmailLeads
					, UsedMapsViewed
					, UsedAdPrints
					, LastLoadId
				)				
			select 
				New.BusinessUnitId
				, @destinationID
				, New.Month
				, NewSearchPageViews
				, NewDetailPageViews
				, NewEmailLeads
				, NewMapsViewed
				, NewAdPrints
				, UsedSearchPageViews
				, UsedDetailPageViews
				, UsedEmailLeads
				, UsedMapsViewed
				, UsedAdPrints
				, @LoadId
			from
			(
				select 
					BusinessUnitId, Month,
					isnull(SearchPageViews, 0) NewSearchPageViews,
					isnull(DetailPageViews, 0) NewDetailPageViews,
					isnull(EmailLeads, 0) NewEmailLeads,
					isnull(MapsViewed, 0) NewMapsViewed,
					isnull(AdPrints, 0) NewAdPrints
				from perfsummary
				where inventorytype = 1
				
			) New
			
			join 
			
			(
				select 
					BusinessUnitId, Month,
					isnull(SearchPageViews, 0) UsedSearchPageViews,
					isnull(DetailPageViews, 0) UsedDetailPageViews,
					isnull(EmailLeads, 0) UsedEmailLeads,
					isnull(MapsViewed, 0) UsedMapsViewed,
					isnull(AdPrints, 0) UsedAdPrints
				from perfsummary
				where inventorytype = 2
				
			) Used
			on New.BusinessUnitId = Used.BusinessUnitId and New.Month = Used.Month
			
			
		commit
		end try
		begin catch
			rollback
			
			set @error =
				'Failed to load data to PerformanceSummary! ' +
				' ErrorNumber ' + isnull(convert(varchar(11), error_number()), '<null>') +
				', ErrorSeverity ' + isnull(convert(varchar(11), error_severity()), '<null>') +
				', ErrorState ' + isnull(convert(varchar(11), error_state()), '<null>') +
				', ErrorProcedure ' + isnull(error_procedure(), '<null>') +
				', ErrorLine ' + isnull(convert(varchar(11), error_line()), '<null>') +
				', ErrorMessage ' + isnull(error_message(), '<null>')
			
			raiserror(@error, 16, 1)
		end catch

	end  -- @DatasourceInterfaceID = 2004

			
end try
begin catch
	set @error =
		'Failed to process VehSummary data!' + char(10) 
		+ ' @BusinessUnitId ' + isnull(convert(varchar(11), @BusinessUnitId), '<null>')
		+ ' @InventoryId ' + isnull(convert(varchar(11), @InventoryId), '<null>')
		+ ' @LoadId ' + isnull(convert(varchar(11), @LoadId), '<null>')
		+			
			
		' ErrorNumber ' + isnull(convert(varchar(11), error_number()), '<null>') +
		', ErrorSeverity ' + isnull(convert(varchar(11), error_severity()), '<null>') +
		', ErrorState ' + isnull(convert(varchar(11), error_state()), '<null>') +
		', ErrorProcedure ' + isnull(error_procedure(), '<null>') +
		', ErrorLine ' + isnull(convert(varchar(11), error_line()), '<null>') +
		', ErrorMessage ' + isnull(error_message(), '<null>')
	
	raiserror(@error, 16, 1)
	
	return 1
end catch


print 'Number of rows skipped due to not finding BuId/InvId lookup: ' + convert(varchar(10), @NoInvIdSkipRows)

return 0

go