if object_id('dashboard.AggregateAutoTraderPerfSummary') is not null
	drop procedure dashboard.AggregateAutoTraderPerfSummary
go

/*
	This proc takes an incoming file from fetch by @LoadID
	and upserts to dashboard.PerformanceSummary
	
*/
create procedure dashboard.AggregateAutoTraderPerfSummary
	@LoadId int
as
set nocount on
set transaction isolation level read uncommitted

	-- for whatever reason, the data imported from Fetch has 2 stats flip-flopped:
	-- the columns that say [New/Used]VehiclesSearched are actually detail page views,
	-- and the columns that say [New/Used]DetailPagesViewed are actually search page views
	
	; with LatestStats as
	(
		select
			p.businessUnitID,
			DestinationId = 2,
			p.Month,
			
			NewSearchPageViews = max(p.NewDetailPagesViewed),
			NewDetailPageViews = max(p.NewVehiclesSearched),
			NewEmailLeads = max(p.NewEmailsToDealership),
			NewMapsViewed = max(p.NewViewedMaps),
			NewAdPrints = max(p.NewPrintableAds),
			
			UsedSearchPageViews = max(p.UsedDetailPagesViewed),
			UsedDetailPageViews = max(p.UsedVehiclesSearched),
			UsedEmailLeads = max(p.UsedEmailsToDealership),
			UsedMapsViewed = max(p.UsedViewedMaps),
			UsedAdPrints = max(p.UsedPrintableAds)
		from Merchandising.merchandising.AutoTraderPerfSummary p
		where p.Load_ID = @LoadId
		group by
			p.businessUnitID,
			p.Month
	)
	update metrics set
		NewSearchPageViews = s.NewSearchPageViews,
		NewDetailPageViews = s.NewDetailPageViews,
		NewEmailLeads = s.NewEmailLeads,
		NewMapsViewed = s.NewMapsViewed,
		NewAdPrints = s.NewAdPrints,
		
		UsedSearchPageViews = s.UsedSearchPageViews,
		UsedDetailPageViews = s.UsedDetailPageViews,
		UsedEmailLeads = s.UsedEmailLeads,
		UsedMapsViewed = s.UsedMapsViewed,
		UsedAdPrints = s.UsedAdPrints,
				
		LastLoadId = @LoadId
	from
		Merchandising.dashboard.PerformanceSummary metrics
		join LatestStats s
			on metrics.BusinessUnitId = s.businessUnitID
			and metrics.DestinationId = s.DestinationId
			and metrics.Month = s.Month
			
		
		
		
	; with LatestStats as
	(
		select
			p.businessUnitID,
			DestinationId = 2,
			p.Month,
			
			NewSearchPageViews = max(p.NewDetailPagesViewed),
			NewDetailPageViews = max(p.NewVehiclesSearched),
			NewEmailLeads = max(p.NewEmailsToDealership),
			NewMapsViewed = max(p.NewViewedMaps),
			NewAdPrints = max(p.NewPrintableAds),
			
			UsedSearchPageViews = max(p.UsedDetailPagesViewed),
			UsedDetailPageViews = max(p.UsedVehiclesSearched),
			UsedEmailLeads = max(p.UsedEmailsToDealership),
			UsedMapsViewed = max(p.UsedViewedMaps),
			UsedAdPrints = max(p.UsedPrintableAds)
		from Merchandising.merchandising.AutoTraderPerfSummary p
		where p.Load_ID = @LoadId
		group by
			p.businessUnitID,
			p.Month
	)		
	insert Merchandising.dashboard.PerformanceSummary
	(
		BusinessUnitId,
		DestinationId,
		Month,
		
		NewSearchPageViews,
		NewDetailPageViews,
		NewEmailLeads,
		NewMapsViewed,
		NewAdPrints,
		
		UsedSearchPageViews,
		UsedDetailPageViews,
		UsedEmailLeads,
		UsedMapsViewed,
		UsedAdPrints,
		
		LastLoadId
	)
	select
		businessUnitID,
		DestinationId,
		Month,
		
		NewSearchPageViews,
		NewDetailPageViews,
		NewEmailLeads,
		NewMapsViewed,
		NewAdPrints,
		
		UsedSearchPageViews,
		UsedDetailPageViews,
		UsedEmailLeads,
		UsedMapsViewed,
		UsedAdPrints,
		
		@LoadId
	from LatestStats s
	where
		not exists
		(
			select *
			from Merchandising.dashboard.PerformanceSummary
			where
				BusinessUnitId = s.businessUnitID
				and DestinationId = s.DestinationId
				and Month = s.Month
		)
	
go
