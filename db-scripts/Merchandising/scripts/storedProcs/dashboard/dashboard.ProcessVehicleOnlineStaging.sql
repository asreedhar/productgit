if object_id('dashboard.ProcessVehicleOnlineStaging') is not null
	drop procedure dashboard.ProcessVehicleOnlineStaging
go

/*
	lookup InventoryId using incoming BusinessUnitId, Vin, StockNumber, and DateCollected, and
	then write it to the staged input data
*/

create procedure dashboard.ProcessVehicleOnlineStaging
	@LoadId int
as
set nocount on
set transaction isolation level read uncommitted


declare
	@id int,
	@businessUnitId int,
	@maxDateCollectedStaging datetime,
	@maxStoreDateCollectedStaging datetime,
	@maxDateCollectedLive datetime
	
	
declare @stores table
(	
	id int identity(0,1) not null,
	businessUnitId int
)

insert @stores (businessUnitId)
select distinct businessUnitId
from dashboard.VehicleOnlineStaging
where LoadId = @LoadId

-- this is used to make sure that if we have internal dupes in the file, we only load the rows from the latest snapshot date
select @maxDateCollectedStaging = max(DateCollected)
from dashboard.VehicleOnlineStaging
where
	LoadId = @LoadId


if (@maxDateCollectedStaging is null)
begin
	-- nothing to process in file
	return 0
end
		
set @maxDateCollectedStaging = dateadd(day, datediff(day, 0, @maxDateCollectedStaging), 0)
		
while (1=1)
begin
	select top 1
		@id = id,
		@businessUnitId = businessUnitId
	from @stores
	
	if (@@ROWCOUNT) <> 1 break;
	delete @stores where id = @id
	
	select @maxStoreDateCollectedStaging = max(DateCollected)
	from dashboard.VehicleOnlineStaging
	where
		LoadId = @LoadId
		and BusinessUnitId = @businessUnitId
	
	select @maxDateCollectedLive = max(DateCollected)
	from dashboard.VehicleOnline
	where
		LoadId = @LoadId
		and BusinessUnitId = @businessUnitId	
	
	-- prevent us from loading an older snapshot for this store
	if (@maxStoreDateCollectedStaging > isnull(@maxDateCollectedLive, 0))
	begin
		begin try
			begin tran
			
			update stage set
				ErrorCode = 2 -- would-be primary key violation in normalized table
			from
				dashboard.VehicleOnlineStaging stage
			where
				LoadId = @LoadId
				and BusinessUnitId = @businessUnitId
				and ErrorCode = 0
				and exists
				(
					select *
					from dashboard.VehicleOnline
					where
						BusinessUnitId = stage.BusinessUnitId
						and InventoryId = stage.InventoryId
						and LoadId = stage.LoadId
				)
		

		
			update stage set
				ErrorCode = 5 -- Online records that are older than the file date snapshot
			from Merchandising.dashboard.VehicleOnlineStaging stage
			where
				LoadId = @LoadId
				and BusinessUnitId = @businessUnitId
				and ErrorCode = 0
				and DateCollected < @maxDateCollectedStaging
				
				
			update stage set
				ErrorCode = 4 -- would-be primary key violation internally in this LoadId
			from Merchandising.dashboard.VehicleOnlineStaging stage
			where
				LoadId = @LoadId
				and BusinessUnitId = @businessUnitId
				and ErrorCode = 0
				and not exists
				(
					select *
					from 
						(
							select
								BusinessUnitId,
								InventoryId,
								LoadId,
								DateCollected = max(DateCollected)
							from Merchandising.dashboard.VehicleOnlineStaging
							where
								LoadId = @LoadId
								and BusinessUnitId = @businessUnitId
								and ErrorCode = 0
							group by
								businessUnitId,
								InventoryId,
								LoadId
						) a
					where
						a.BusinessUnitId = stage.BusinessUnitId
						and a.InventoryId = stage.InventoryId
						and a.LoadId = stage.LoadId
						and a.DateCollected = stage.DateCollected
				)


			insert dashboard.VehicleOnline
			(
				BusinessUnitId,
				InventoryId,
				LoadId,
				DestinationId,
				DateCollected,
				HasPhoto,
				PhotoCount,
				DaysLive,
				DateLoaded,
				DatasourceInterfaceId,
				RequestId
			)
			select
				BusinessUnitId,
				InventoryId,
				LoadId,
				DestinationId,
				DateCollected,
				HasPhoto,
				PhotoCount,
				DaysLive,
				DateLoaded,
				DatasourceInterfaceId,
				RequestId
			from dashboard.VehicleOnlineStaging
			where
				LoadId = @LoadId
				and BusinessUnitId = @businessUnitId	
				and ErrorCode = 0
				
			delete dashboard.VehicleOnlineStaging
			where
				LoadId = @LoadId
				and BusinessUnitId = @businessUnitId	
				and ErrorCode = 0
				
			delete dashboard.VehicleOnline
			where
				BusinessUnitId = @businessUnitId
				and LoadId < 
					(
						select min(LoadId)
						from
							(
								select top 30 LoadId
								from dashboard.VehicleOnline
								where BusinessUnitId = @businessUnitId
								order by LoadId desc
							) a
					)
				
			commit
		end try
		begin catch
			rollback
			declare @error varchar(1000)
	
			set @error =
				'error processing VehicleOnlineStaging!'
				+ char(10) + '@LoadId = ' + isnull(convert(varchar(max), @LoadId), '<null>')
				+ 'ErrorNumber ' + isnull(convert(varchar(11), error_number()), '<null>') +
				', ErrorSeverity ' + isnull(convert(varchar(11), error_severity()), '<null>') +
				', ErrorState ' + isnull(convert(varchar(11), error_state()), '<null>') +
				', ErrorProcedure ' + isnull(error_procedure(), '<null>') +
				', ErrorLine ' + isnull(convert(varchar(11), error_line()), '<null>') +
				', ErrorMessage ' + isnull(error_message(), '<null>')
			
			raiserror(@error, 16, 1)
			
			return 1
		end catch

	end -- check to make sure the online load we are processing is the latest for the buid
	else
	begin
		-- we are trying to load an old file
		
		update dashboard.VehicleOnlineStaging set
			ErrorCode = 3
		where
			LoadId = @LoadId
			and BusinessUnitId = @businessUnitId
			and ErrorCode = 0
	end
end


-- no transaction needed here for the cleanup of staging:

delete dashboard.VehicleOnlineStaging
where DateLoaded < dateadd(day, -90, getdate())


return 0


go