if object_id('dashboard.AggregateAutoTraderHendrickPerfSummary') is not null
	drop procedure dashboard.AggregateAutoTraderHendrickPerfSummary
go

/*
	This proc takes an incoming file from fetch by @LoadID
	and upserts to dashboard.PerformanceSummary
	
*/
create procedure dashboard.AggregateAutoTraderHendrickPerfSummary
	@LoadId int
as
set nocount on
set transaction isolation level read uncommitted

	
	
	; with LatestStats as
	(
		select
			p.businessUnitID,
			DestinationId = 2,
			month = p.ReportMonth,
			
			NewSearchPageViews = max(p.NewSearchPageViews),
			NewDetailPageViews = max(p.NewDetailPageViews),
			NewEmailLeads = max(p.NewEmailLeads),
			NewMapsViewed = max(p.NewMapsViewed),
			NewAdPrints = max(p.NewAdPrints),
			NewPhoneLeads = max(p.NewPhoneLeads),
			
			UsedSearchPageViews = max(p.UsedSearchPageViews),
			UsedDetailPageViews = max(p.UsedDetailPageViews),
			UsedEmailLeads = max(p.UsedEmailLeads),
			UsedMapsViewed = max(p.UsedMapsViewed),
			UsedAdPrints = max(p.UsedAdPrints),
			UsedPhoneLeads = max(p.UsedPhoneLeads)
		from Merchandising.merchandising.AutoTraderHendrickPerformanceSummary p
		where p.LoadId = @LoadId
		group by
			p.businessUnitID,
			p.ReportMonth
	)
	update metrics set
		NewSearchPageViews = s.NewSearchPageViews,
		NewDetailPageViews = s.NewDetailPageViews,
		NewEmailLeads = s.NewEmailLeads,
		NewMapsViewed = s.NewMapsViewed,
		NewAdPrints = s.NewAdPrints,
		NewPhoneLeads = s.NewPhoneLeads,
		
		UsedSearchPageViews = s.UsedSearchPageViews,
		UsedDetailPageViews = s.UsedDetailPageViews,
		UsedEmailLeads = s.UsedEmailLeads,
		UsedMapsViewed = s.UsedMapsViewed,
		UsedAdPrints = s.UsedAdPrints,
		UsedPhoneLeads = s.UsedPhoneLeads,
				
		LastLoadId = @LoadId
	from
		Merchandising.dashboard.PerformanceSummary metrics
		join LatestStats s
			on metrics.BusinessUnitId = s.businessUnitID
			and metrics.DestinationId = s.DestinationId
			and metrics.Month = s.Month
			
		
		
		
	; with LatestStats as
	(
		select
			p.businessUnitID,
			DestinationId = 2,
			month = p.ReportMonth,
			
			NewSearchPageViews = max(p.NewSearchPageViews),
			NewDetailPageViews = max(p.NewDetailPageViews),
			NewEmailLeads = max(p.NewEmailLeads),
			NewMapsViewed = max(p.NewMapsViewed),
			NewAdPrints = max(p.NewAdPrints),
			NewPhoneLeads = max(p.NewPhoneLeads),
			
			UsedSearchPageViews = max(p.UsedSearchPageViews),
			UsedDetailPageViews = max(p.UsedDetailPageViews),
			UsedEmailLeads = max(p.UsedEmailLeads),
			UsedMapsViewed = max(p.UsedMapsViewed),
			UsedAdPrints = max(p.UsedAdPrints),
			UsedPhoneLeads = max(p.UsedPhoneLeads)
		from Merchandising.merchandising.AutoTraderHendrickPerformanceSummary p
		where p.LoadId = @LoadId
		group by
			p.businessUnitID,
			p.ReportMonth
	)		
	insert Merchandising.dashboard.PerformanceSummary
	(
		BusinessUnitId,
		DestinationId,
		Month,
		
		NewSearchPageViews,
		NewDetailPageViews,
		NewEmailLeads,
		NewMapsViewed,
		NewAdPrints,
		NewPhoneLeads,
		
		UsedSearchPageViews,
		UsedDetailPageViews,
		UsedEmailLeads,
		UsedMapsViewed,
		UsedAdPrints,
		UsedPhoneLeads,
		
		LastLoadId
	)
	select
		businessUnitID,
		DestinationId,
		Month,
		
		NewSearchPageViews,
		NewDetailPageViews,
		NewEmailLeads,
		NewMapsViewed,
		NewAdPrints,
		NewPhoneLeads,
		
		UsedSearchPageViews,
		UsedDetailPageViews,
		UsedEmailLeads,
		UsedMapsViewed,
		UsedAdPrints,
		UsedPhoneLeads,
		
		@LoadId
	from LatestStats s
	where
		not exists
		(
			select *
			from Merchandising.dashboard.PerformanceSummary
			where
				BusinessUnitId = s.businessUnitID
				and DestinationId = s.DestinationId
				and Month = s.Month
		)
	
go
