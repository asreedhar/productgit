if object_id('dashboard.GetBusinessUnitIdAndInventoryId') is not null
	drop procedure dashboard.GetBusinessUnitIdAndInventoryId
go

/*
	lookup BusinessUnitId / InventoryId using incoming Vin and Scraped date, and
	then write it to the raw input data and return in output variables
*/
create procedure dashboard.GetBusinessUnitIdAndInventoryId
	@DatasourceInterfaceID int,
	@BusinessUnitId int output,
	@RequestId int,
	@Month datetime,
	@Vin varchar(50),
	@StockNumber varchar(50),
	@InventoryEffectiveDate datetime,
	@InventoryId int output,
	@IsForOnline bit = 0
as
set nocount on
set transaction isolation level read uncommitted


-- NOTE Vin should be varchar(17), just let it fail BuId/InvId lookup
	
declare
	@vinLookupErrorMessage varchar(max)
	,@updateErrorMessage varchar(max)
	,@errorMessageDetails varchar(max)


set @errorMessageDetails = char(10) + '@DatasourceInterfaceID = ' + isnull(convert(varchar(max), @DatasourceInterfaceID), '<null>')
			+ char(10) + '@BusinessUnitId = ' + isnull(convert(varchar(max), @BusinessUnitId), '<null>')
			+ char(10) + '@RequestId = ' + isnull(convert(varchar(max), @RequestId), '<null>')
			+ char(10) + '@Month = ' + isnull(convert(varchar(max), @Month), '<null>')
			+ char(10) + '@Vin = ' + isnull(convert(varchar(max), @Vin), '<null>')
			+ char(10) + '@StockNumber = ' + isnull(convert(varchar(max), @StockNumber), '<null>')
			+ char(10) + '@InventoryEffectiveDate = ' + isnull(convert(varchar(max), @InventoryEffectiveDate), '<null>')
			+ char(10) + '@InventoryId = ' + isnull(convert(varchar(max), @InventoryId), '<null>')
	

if (@IsForOnline = 1)
begin

	/*
		04-30-2013 Daniel Hillis
		Breaking out online vin mapping to its separate logic
		
		The feed for online activity does not currently contain a dealer identifier, so
		as of now, BusinessUnitId is always null here.  If we get here with a BusinessUnitId
		set, there is a problem.
	*/

	if (@BusinessUnitId is not null)
	begin
		set @vinLookupErrorMessage = 'BusinessUnitId was not expected when @IsForOnline was set to 1.  We expect to look up both BusinessUnitId and InventoryId for online.' + @errorMessageDetails
		raiserror(@vinLookupErrorMessage, 16, 1)
	end

	-- no BuId
	select top 1
		@BusinessUnitId = i.BusinessUnitID
		, @InventoryId = i.InventoryID
	from
		IMT.dbo.tbl_Vehicle v
		join IMT.dbo.Inventory i
			on v.VehicleID = i.VehicleID
	where
		v.Vin = @Vin
		and datediff(day, @InventoryEffectiveDate, i.InventoryReceivedDate) < 1
		
		-- The following "ands" are for "online" only
		and i.InventoryActive = 1 
		and exists 
		(	-- only use inventory at active business units
			select * 
			from IMT.dbo.BusinessUnit bu 
			where bu.active = 1
			and bu.BusinessUnitId = i.BusinessUnitId
		)
	order by
		i.InventoryReceivedDate desc


end
-- All non-online purposes below:
else		
-- get businessUnitID/InventoryId from VIN
if (@BusinessUnitId is not null)
begin
	select top 1
		@InventoryId = i.InventoryID
	from
		IMT.dbo.tbl_Vehicle v
		join IMT.dbo.Inventory i
			on v.VehicleID = i.VehicleID
	where
		v.Vin = @Vin
		and datediff(day, @InventoryEffectiveDate, i.InventoryReceivedDate) < 1
		and i.BusinessUnitID = @BusinessUnitId
	order by
		i.InventoryReceivedDate desc
end
else
begin
	-- no BuId
	select top 1
		@BusinessUnitId = i.BusinessUnitID
		, @InventoryId = i.InventoryID
	from
		IMT.dbo.tbl_Vehicle v
		join IMT.dbo.Inventory i
			on v.VehicleID = i.VehicleID
	where
		v.Vin = @Vin
		and datediff(day, @InventoryEffectiveDate, i.InventoryReceivedDate) < 1
	order by
		i.InventoryReceivedDate desc
end


if (@BusinessUnitId is null or @InventoryId is null)
begin
	return 1
end

set @updateErrorMessage = 'expecting to update exactly one record with the new InventoryId from Vin lookup!' + @errorMessageDetails
	
if (@DatasourceInterfaceID = 13)
begin
	update Merchandising.merchandising.AutoTraderVehSummary set
		InventoryId = @InventoryId
	where
		businessUnitID = @BusinessUnitId
		and RequestId = @RequestId
		and Month = @Month
		and VIN = @Vin
		and StockNumber = @StockNumber
	
	if (@@rowcount <> 1)
	begin
		raiserror(@updateErrorMessage, 16, 1)
		return 2
	end
end
else if (@DatasourceInterfaceID = 2004)
begin
	update Merchandising.merchandising.CarsDotComVehSummary set
		InventoryId = @InventoryId
	where
		businessUnitID = @BusinessUnitId
		and RequestId = @RequestId
		and Month = @Month
		and VIN = @Vin
		and StockNumber = @StockNumber
	if (@@rowcount <> 1)
	begin
		raiserror(@updateErrorMessage, 16, 1)
		return 2
	end
end
else if (@DatasourceInterfaceID = 2006)
begin
	update Merchandising.merchandising.AutoTraderHendrickVehicleSummary set
		InventoryId = @InventoryId,
		BusinessUnitId = @BusinessUnitId
	where
		VIN = @Vin
		and StockNumber = @StockNumber
		and FeedDate = @InventoryEffectiveDate
	if (@@rowcount <> 1)
	begin
		raiserror(@updateErrorMessage, 16, 1)
		return 2
	end
end
else if (@DatasourceInterfaceID = 2009)
begin
	update Merchandising.merchandising.AutoTraderHendrickOnline set
		InventoryId = @InventoryId,
		BusinessUnitId = @BusinessUnitId
	where
		VIN = @Vin
		and StockNumber = @StockNumber
		and FeedDate = @InventoryEffectiveDate
	if (@@rowcount <> 1)
	begin
		raiserror(@updateErrorMessage, 16, 1)
		return 2
	end
end
else
begin
	raiserror('dashboard.GetBusinessUnitIdAndInventoryId switches on @DatasourceInterfaceId, no code found for %d', 16, 1, @DatasourceInterfaceId)
	return 3
end

return 0

go	