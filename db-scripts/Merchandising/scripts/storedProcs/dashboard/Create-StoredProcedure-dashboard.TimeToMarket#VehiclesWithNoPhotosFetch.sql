-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-04-16
-- Description:	Get a list of vehicles that do 
--				not have photos in the 
--				TimeToMarketTracking table
-- =============================================
USE Merchandising
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dashboard].[TimeToMarket#VehiclesWithNoPhotosFetch]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE dashboard.TimeToMarket#VehiclesWithNoPhotosFetch
GO

CREATE PROCEDURE dashboard.TimeToMarket#VehiclesWithNoPhotosFetch 
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int = 0, 
	@StartDate DateTime = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

/*********************************
	Get the list of active inventory per business unit that are not listed in [Merchandising].[dashboard].[TimeToMarketTracking] OR have NULL [PhotosFirstDate]
	That is the list of vehicles we'll be checking the External file for and also checking against the PhotosService
	******************************/
	IF @BusinessUnitId > 0 AND @StartDate IS NOT NULL
	BEGIN
	SELECT
		inv.BusinessUnitID, inv.InventoryID, inv.StockNumber, veh.VIN
	FROM
		IMT.dbo.Inventory inv
		LEFT JOIN Merchandising.dashboard.TimeToMarketTracking ttmt ON inv.BusinessUnitID = ttmt.BusinessUnitId AND inv.InventoryID = ttmt.InventoryId
		INNER JOIN IMT.dbo.Vehicle veh ON inv.VehicleID = veh.VehicleID
	WHERE
		inv.BusinessUnitID = @BusinessUnitId
		AND inv.InventoryActive = 1
		AND inv.InventoryReceivedDate > @StartDate
		AND ttmt.PhotosFirstDate IS NULL	
	END
END
GO

grant execute on dashboard.TimeToMarket#VehiclesWithNoPhotosFetch to MerchandisingUser
GO

