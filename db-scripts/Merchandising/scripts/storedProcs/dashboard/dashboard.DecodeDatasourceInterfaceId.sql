if object_id('dashboard.DecodeDatasourceInterfaceId') is not null
	drop procedure dashboard.DecodeDatasourceInterfaceId
go

/*
	translate DatasourceInterfaceId to boolean IsOnline, IsActivity, IsPerformanceSummary, etc...
*/

create procedure dashboard.DecodeDatasourceInterfaceId
	@DatasourceInterfaceId int,
	@IsOnline bit output,
	@IsActivity bit output,
	@IsPerformanceSummary bit output
as
set nocount on
set transaction isolation level read uncommitted

	
select
	@IsOnline = case when @DatasourceInterfaceId in (14, 2000) then 1 else 0 end,
	@IsActivity = case when @DatasourceInterfaceId in (2010) then 1 else 0 end,
	@IsPerformanceSummary = 0
	
return 0
	
go