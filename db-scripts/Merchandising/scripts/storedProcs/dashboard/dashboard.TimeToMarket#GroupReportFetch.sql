IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dashboard.TimeToMarket#GroupReportFetch') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE dashboard.TimeToMarket#GroupReportFetch
GO
/****** Object:  StoredProcedure [dashboard].[TimeToMarket#GroupReportFetch]    Script Date: 06/03/2013 10:24:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dashboard].[TimeToMarket#GroupReportFetch] 
	--                              EXAMPLE PARAMETERS (for testing or development)
	@GroupId int,                -- int = 101566; -- All Star Automotive Group
	@StartDate DateTime = NULL,  -- DateTime = '10/01/2013 00:00:00';
	@EndDate DateTime = NULL,    -- DateTime = '10/31/2013 23:59:59';
	@VehicleType int = 0         -- int = 0; -- Both
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    DECLARE @table TABLE
        (
          BusinessUnitId INT NOT NULL PRIMARY KEY
        )
    INSERT  INTO @table
            SELECT  DISTINCT br.BusinessUnitId
            FROM    Merchandising.settings.Merchandising settings
                    JOIN IMT.dbo.BusinessUnitRelationship br ON br.BusinessUnitID = settings.BusinessUnitId
            WHERE  br.ParentID = @GroupId
                    AND settings.showTimeToMarket = 1

	SELECT 
		i.InventoryActive, bu.BusinessUnitID, bu.BusinessUnit, ttmt.InventoryID,
        i.InventoryReceivedDate AS DateAddedToInventory,
		PriceFirstDate, PhotosFirstDate, DescriptionFirstDate, AdApprovalFirstDate, CompleteAdFirstDate, GIDFirstDate
	FROM Merchandising.dashboard.TimeToMarketTracking ttmt
	JOIN IMT.dbo.Inventory i ON ttmt.BusinessUnitId = i.BusinessUnitID AND ttmt.InventoryId = i.InventoryID
	JOIN imt.dbo.BusinessUnit bu ON bu.BusinessUnitID = i.BusinessUnitID
	JOIN @table TTM_BUs ON TTM_BUs.BusinessUnitId = ttmt.BusinessUnitId
	WHERE (@VehicleType = 0 OR i.InventoryType = @VehicleType)
	AND (
		@StartDate IS NULL OR (
			ttmt.CompleteAdFirstDate > @StartDate OR
			ttmt.PhotosFirstDate > @StartDate
		)
	) AND (
		@EndDate IS NULL OR (
			ttmt.CompleteAdFirstDate < @EndDate OR
			ttmt.PhotosFirstDate < @EndDate
		)
	)
	ORDER BY ttmt.BusinessUnitID, ttmt.InventoryID
		
		
END

GO


GRANT EXECUTE on dashboard.TimeToMarket#GroupReportFetch  to MerchandisingUser
GO

