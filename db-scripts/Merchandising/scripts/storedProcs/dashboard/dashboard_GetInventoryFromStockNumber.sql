USE [Merchandising]

if object_id('dashboard.GetInventoryFromStockNumber') is not null
	drop procedure dashboard.GetInventoryFromStockNumber
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dashboard].[GetInventoryFromStockNumber]
        @businessUnitID INT,
        @StockNumber VARCHAR(15)
AS


SELECT	I.InventoryID, I.InventoryType
FROM	FLDW.dbo.InventoryActive I

WHERE	I.BusinessUnitID = @businessUnitID and I.StockNumber = @StockNumber
GO

GRANT EXECUTE ON dashboard.GetInventoryFromStockNumber TO MerchandisingUser 
GO