if object_id('dashboard.GetMetricsGaps') is not null
	drop procedure dashboard.GetMetricsGaps
go

/*

Get BusinessUnit/Months for which we have some metrics data, but not the month-end data for the given DestinationId
This only looks at the current month and 12 past months.

*/
create procedure dashboard.GetMetricsGaps
	@RequestTypeID tinyint
as

set nocount on
set transaction isolation level read uncommitted

declare @month datetime, @currentMonth datetime, @cutoff datetime, @DestinationId int

-- Convert the RequestTypeId to a DestinationId
select @DestinationId = 
case 
	when @RequestTypeID = 1 then 2 -- Autotrader
	when @RequestTypeID = 3 then 1 -- Cars.com
end


declare @Months table ([Month] datetime)

set @currentMonth = dateadd(month, datediff(month, 0, getdate()), 0)
set @cutoff = dateadd(month, -12, @currentMonth)

set @month = @cutoff

while @Month <= @currentMonth
begin
	insert @Months
	values (@Month)
	
	set @Month = dateadd(month,1,@Month)
end

declare @BusinessUnitDestMonth table(BusinessUnitId int, DestinationId int, [Month] datetime, LastDayOfMonth datetime)

-- All BusinessUnits in our world
;with BusinessUnit as 
(
	select distinct businessUnitId
	from Merchandising.settings.DealerListingSites
	where destinationID = @DestinationId
)
-- All months for every BusinessUnitId in our world
insert @BusinessUnitDestMonth(BusinessUnitId, DestinationId, [Month], LastDayOfMonth)
select BusinessUnitId, @DestinationId, m.Month, dateadd(day, -1, dateadd(month, 1, dateadd(month, datediff(month, 0, m.Month), 0)))
from BusinessUnit
cross join @Months m

if (@DestinationId = 1) -- Cars.com
begin

	with PerformanceSummaryLogicalDate as
	(
		select BusinessUnitId, DestinationId,
				case when datediff(month, DateCollected, [Month]) != 0
				then dateadd(day, -1, dateadd(month, 1, [Month])) -- last day of month (retroscrape)
				else dateadd(day, datediff(day, 0, DateCollected), 0)  -- flattened date collected
			end as LogicalDate
		from Merchandising.dashboard.PerformanceSummary ps
	)
	select distinct businessUnitID, DestinationId, FlatMonth = Month
	from @BusinessUnitDestMonth budm
	where not exists (
		-- no end of month Performance Summary data
		select * 
		from PerformanceSummaryLogicalDate a
		where a.BusinessUnitId = budm.BusinessUnitId
		and a.DestinationId = budm.DestinationId
		and a.LogicalDate = budm.LastDayOfMonth
	)
	order by BusinessUnitId, Month -- required for SSIS :(
end
else -- everything else (i.e., Autotrader)
begin
	select distinct businessUnitID, DestinationId, FlatMonth = Month
	from @BusinessUnitDestMonth budm
	where not exists (
		-- no end of month IAP data
		select * 
		from Merchandising.dashboard.InventoryAdPerformance a
		where a.BusinessUnitId = budm.BusinessUnitId
		and a.DestinationId = budm.DestinationId
		and dateadd(month, datediff(month, 0, [Date]), 0) = budm.Month
		and a.Date = budm.LastDayOfMonth
	)
	order by BusinessUnitId, Month -- required for SSIS :(
end



go	