if object_id('dashboard.DealerListingSiteInventoryCounts') is not null
	drop procedure dashboard.DealerListingSiteInventoryCounts
GO

create procedure dashboard.DealerListingSiteInventoryCounts
	@BusinessUnitID int, -- 100147 -- Rick Hendrick City Chevrolet
	@VehicleType int, -- {0, 1, 2}
	@StartDate datetime, -- '2012/10/01'
	@EndDate datetime -- '2012/10/31'
as

set transaction isolation level read uncommitted
set nocount on

select 
	d.description,
	count(distinct p.inventoryId) as count,
	p.month
from
	Merchandising.dashboard.VehicleActivityInventoryCounts p
	cross join Merchandising.settings.EdtDestinations d
where
	p.BusinessUnitId = @BusinessUnitID
	and d.destinationID = p.DestinationId
	and (@VehicleType = 0 or p.InventoryType = @VehicleType)
	and p.month >= @StartDate
	and p.month <= @EndDate
group by
	d.description,
	p.month
order by p.month


GO

grant execute on dashboard.DealerListingSiteInventoryCounts to MerchandisingUser 
grant execute on dashboard.DealerListingSiteInventoryCounts to merchandisingWebsite 
GO

