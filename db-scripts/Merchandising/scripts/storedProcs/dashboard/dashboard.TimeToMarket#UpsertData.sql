-- ==========================================================================================
-- Author:		Travis Huber
-- Create date: 2013-06-19
-- Description:	Update all TTM data
-- 
-- Updated: 2014-02-26
--		Save the same data to the new MAX--- fields
-- Updated: 2014-03-20
--		Save all data to the correct fields -- To be released 2014-04-01
-- ==========================================================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dashboard].[TimeToMarket#UpsertData]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dashboard].[TimeToMarket#UpsertData]
GO

CREATE PROCEDURE dashboard.TimeToMarket#UpsertData 
	@VinList dbo.TimeToMarketTableType READONLY
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Perform the update.
	UPDATE ttm
	  SET
		-- Each of these values should be nullable from the client so we should end up with NULL for unset values 
		ttm.GIDFirstDate	= vl.GIDFirstDate,
		ttm.AdApprovalFirstDate			= vl.AdApprovalFirstDate,
		ttm.CompleteAdFirstDate			= vl.CompleteAdFirstDate,
		
		-- for now use all the same values
		ttm.MinimumPhotosFirstDate		= vl.MinimumPhotosFirstDate,
		ttm.PhotosFirstDate 			= vl.PhotosFirstDate,
		ttm.DescriptionFirstDate 		= vl.DescriptionFirstDate,
		ttm.PriceFirstDate 			= vl.PriceFirstDate,
		-- soon switch 
		ttm.MAXDescriptionFirstDate		= vl.MAXDescriptionFirstDate,
		ttm.MAXMinimumPhotosFirstDate	= vl.MAXMinimumPhotosFirstDate,
		ttm.MAXPhotosFirstDate			= vl.MAXPhotosFirstDate,
		ttm.MAXPriceFirstDate			= vl.MAXPriceFirstDate
		
	  FROM Merchandising.dashboard.TimeToMarketTracking AS ttm
		INNER JOIN @VinList AS vl ON vl.BusinessUnitId = ttm.BusinessUnitId AND vl.InventoryId = ttm.InventoryId
	  WHERE ttm.Inserted IS NOT NULL;

	-- Create a table just for the inserts
	DECLARE @InsertVinList dbo.TimeToMarketTableType;
	INSERT INTO @InsertVinList
	SELECT vl.* FROM @VinList vl
	LEFT OUTER JOIN Merchandising.dashboard.TimeToMarketTracking ttm 
		ON ttm.InventoryId = vl.InventoryId 
		AND ttm.BusinessUnitId = vl.BusinessUnitId
	WHERE ttm.Inserted IS NULL


	--Perform the insert.
	INSERT INTO Merchandising.dashboard.TimeToMarketTracking (
		BusinessUnitId, InventoryId, GIDFirstDate, 
		PhotosFirstDate, DescriptionFirstDate, PriceFirstDate, AdApprovalFirstDate, CompleteAdFirstDate, 
		MinimumPhotosFirstDate, MAXMinimumPhotosFirstDate, MAXPhotosFirstDate, MAXPriceFirstDate, MAXDescriptionFirstDate)
	SELECT BusinessUnitId, InventoryId, GIDFirstDate, 
		PhotosFirstDate, DescriptionFirstDate, PriceFirstDate, AdApprovalFirstDate, CompleteAdFirstDate, 
		MinimumPhotosFirstDate, MAXMinimumPhotosFirstDate, MAXPhotosFirstDate, MAXPriceFirstDate, MAXDescriptionFirstDate
	FROM @InsertVinList vl
	
END
GO

grant execute on dashboard.TimeToMarket#UpsertData to MerchandisingUser
go

