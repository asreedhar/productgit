-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-06-20
-- Description:	Set the report start date per BusinessUnit
-- =============================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dashboard].[TimeToMarket#SetReportStartDate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dashboard].[TimeToMarket#SetReportStartDate]
GO


CREATE PROCEDURE dashboard.TimeToMarket#SetReportStartDate 
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int, 
	@ReportStartDate DATETIME,
	@HasCarsApi BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    IF NOT EXISTS( SELECT * FROM Merchandising.dashboard.TimeToMarketTracking#BuReportStart WHERE BusinessUnitId = @BusinessUnitId)
    BEGIN
        INSERT Merchandising.dashboard.TimeToMarketTracking#BuReportStart
        VALUES(@BusinessUnitId, @ReportStartDate, @ReportStartDate, @ReportStartDate)
    END
    ELSE
    BEGIN
		UPDATE Merchandising.dashboard.TimeToMarketTracking#BuReportStart
		SET FirstPhotoReportStartDate = @ReportStartDate
		WHERE BusinessUnitId = @BusinessUnitId AND FirstPhotoReportStartDate IS NULL;
		
		UPDATE Merchandising.dashboard.TimeToMarketTracking#BuReportStart
		SET ReportStartDate = @ReportStartDate
		WHERE BusinessUnitId = @BusinessUnitId AND ReportStartDate IS NULL;
		
		IF (@HasCarsApi = 1)
		BEGIN
			UPDATE Merchandising.dashboard.TimeToMarketTracking#BuReportStart
			SET CarsApiStartDate = @ReportStartDate
			WHERE BusinessUnitId = @BusinessUnitId AND CarsApiStartDate IS NULL;
		END
    END
    
    
END
GO

GRANT EXECUTE on dashboard.TimeToMarket#SetReportStartDate  to MerchandisingUser
GO
