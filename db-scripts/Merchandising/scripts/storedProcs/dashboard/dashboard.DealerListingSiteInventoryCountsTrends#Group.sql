if object_id('dashboard.DealerListingSiteInventoryCountsTrend#Group') is not null
	drop procedure dashboard.DealerListingSiteInventoryCountsTrend#Group
GO

create procedure dashboard.DealerListingSiteInventoryCountsTrend#Group
	@GroupID int, 
	@VehicleType int, 
	@StartDate datetime, 
	@EndDate datetime 
as

set transaction isolation level read uncommitted
set nocount on

if (@VehicleType = 0)
begin
	exec dashboard.DealerListingSiteInventoryCountsTrend#Group#NewAndUsed
		@GroupID = @GroupId,
		@StartDate = @StartDate, 
		@EndDate = @EndDate
end
else
begin
	exec dashboard.DealerListingSiteInventoryCountsTrend#Group#InventoryType
		@GroupID = @GroupId,
		@StartDate = @StartDate, 
		@EndDate = @EndDate,
		@VehicleType = @VehicleType
end

GO

grant execute on dashboard.DealerListingSiteInventoryCountsTrend#Group to MerchandisingUser 
grant execute on dashboard.DealerListingSiteInventoryCountsTrend#Group to merchandisingWebsite 
GO

