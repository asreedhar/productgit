if object_id('dashboard.AggregateCarsDotComPerfSummary') is not null
	drop procedure dashboard.AggregateCarsDotComPerfSummary
go

/*
	This proc takes an incoming file from fetch by @LoadID
	and upserts to dashboard.PerformanceSummary
	
*/
create procedure dashboard.AggregateCarsDotComPerfSummary
	@LoadId int,
	@DatasourceInterfaceId int
as
set nocount on
set transaction isolation level read uncommitted


declare @ratios table
(
	BusinessUnitId int not null,
	DestinationId int not null,
	ReportMonth datetime not null,
	UsedSearchPageViewsRatio float(24) not null,
	UsedDetailPageViewsRatio float(24) not null,
	UsedMapsViewedRatio float(24) not null,
	UsedAdPrintsRatio float(24) not null
)

declare @BusinessUnitMonth table
(
	BusinessUnitId int,
	Month datetime
)		

declare
	@AvgSearchPageViewsRatio float(24),
	@AvgDetailPageViewsRatio float(24),
	@AvgMapsViewedRatio float(24),
	@AvgAdPrintsRatio float(24),
	@businessUnitId int,
	@Month datetime,
	@error varchar(max)
	
	
-- load up our existing ratios from dashboard.PerformanceSummary
insert @BusinessUnitMonth
(
	BusinessUnitId,
	Month
)
select distinct
	businessUnitID,
	Month
from Merchandising.merchandising.CarsDotComPerfSummary
where Load_ID = @LoadId

insert @ratios
(
	BusinessUnitId,
	DestinationId,
	ReportMonth,
	UsedSearchPageViewsRatio,
	UsedDetailPageViewsRatio,
	UsedMapsViewedRatio,
	UsedAdPrintsRatio
)
select
	ps.BusinessUnitId,
	1,
	ps.Month,
	ps.UsedSearchPageViewsRatio,
	ps.UsedDetailPageViewsRatio,
	ps.UsedMapsViewedRatio,
	ps.UsedAdPrintsRatio
from
	@BusinessUnitMonth b
	join Merchandising.dashboard.PerformanceSummary ps
		on b.BusinessUnitId = ps.BusinessUnitId
		and b.Month = ps.Month
		and ps.DestinationId = 1
where
	-- these columns will be null on existing rows that haven't gone through this new ratio process
	ps.UsedSearchPageViewsRatio is not null
	and ps.UsedDetailPageViewsRatio is not null
	and ps.UsedMapsViewedRatio is not null
	and ps.UsedAdPrintsRatio is not null		
		
		
-- for each businessUnit/Month combo, attempt to use a previous month's ratio if it exists, if not, use an average of all stores

select
	@AvgSearchPageViewsRatio = avg(UsedSearchPageViewsRatio),
	@AvgDetailPageViewsRatio = avg(UsedDetailPageViewsRatio),
	@AvgMapsViewedRatio = avg(UsedMapsViewedRatio),
	@AvgAdPrintsRatio = avg(UsedAdPrintsRatio)
from Merchandising.dashboard.PerformanceSummary
where DestinationId = 1

while (1=1)
begin
	select top 1
		@businessUnitId = b.BusinessUnitId,
		@Month = b.Month
	from @BusinessUnitMonth b
	where
		not exists
		(
			select *
			from @ratios
			where
				BusinessUnitId = b.BusinessUnitId
				and ReportMonth = b.Month
		)
	
	if (@@rowcount) = 0 break
	

	-- use previous month for ratio
	insert @ratios
	(
		BusinessUnitId,
		DestinationId,
		ReportMonth,
		UsedSearchPageViewsRatio,
		UsedDetailPageViewsRatio,
		UsedMapsViewedRatio,
		UsedAdPrintsRatio
	)
	select top 1
		BusinessUnitId,
		DestinationId,
		@Month,
		UsedSearchPageViewsRatio,
		UsedDetailPageViewsRatio,
		UsedMapsViewedRatio,
		UsedAdPrintsRatio
	from Merchandising.dashboard.PerformanceSummary
	where
		businessUnitId = @businessUnitId
		and Month < @Month
		and DestinationId = 1
		and UsedSearchPageViewsRatio is not null
		and UsedDetailPageViewsRatio is not null
		and UsedMapsViewedRatio is not null
		and UsedAdPrintsRatio is not null		
		
	order by Month desc
	
	if (@@rowcount = 0)
	begin
		insert @ratios
		(
			BusinessUnitId,
			DestinationId,
			ReportMonth,
			UsedSearchPageViewsRatio,
			UsedDetailPageViewsRatio,
			UsedMapsViewedRatio,
			UsedAdPrintsRatio
		)
		values
		(
			@businessUnitId,
			1,
			@Month,
			@AvgSearchPageViewsRatio,
			@AvgDetailPageViewsRatio,
			@AvgMapsViewedRatio,
			@AvgAdPrintsRatio
		)
	end
end


-- this step shouldn't be necessary as calculated ratios from dashboard.CalculateInventoryDifferentials should already be clean, but just in case:

update @ratios set
	UsedSearchPageViewsRatio = @AvgSearchPageViewsRatio,
	UsedDetailPageViewsRatio = @AvgDetailPageViewsRatio,
	UsedMapsViewedRatio = @AvgMapsViewedRatio,
	UsedAdPrintsRatio = @AvgAdPrintsRatio
where
	UsedSearchPageViewsRatio in (0,1) or UsedSearchPageViewsRatio not between 0 and 1
	or UsedDetailPageViewsRatio in (0,1) or UsedDetailPageViewsRatio not between 0 and 1
	or UsedMapsViewedRatio in (0,1) or UsedMapsViewedRatio not between 0 and 1
	or UsedAdPrintsRatio in (0,1) or UsedAdPrintsRatio not between 0 and 1




-- go ahead and upsert the perf numbers now
begin try
	begin tran

	; with LastFile as
	(
		select
			p.businessUnitID,
			p.Month,
			
			InSearchResult = max(p.InSearchResult),
			RequestDetail = max(p.RequestDetail),
			EmailDealerNew = max(EmailDealerNew),
			ViewMap = max(p.ViewMap),
			PrintAD = max(p.PrintAD),
			
			EmailDealerUsed = max(p.EmailDealerUsed),
      
			PhoneLeadsNew = max(p.PhoneProspectNew),
			PhoneLeadsUsed = max(p.PhoneProspectsUsed),
					
			LastLoadId = @LoadId,
			RequestId = max(p.RequestId),
			DateCollected = max(coalesce(p.FetchDateInserted, p.DateLoaded, '1900-01-01')),
			DateLoaded = max(p.DateLoaded)
		from
			Merchandising.merchandising.CarsDotComPerfSummary p
		where
			p.Load_ID = @LoadId
		group by
			p.businessUnitID,
			p.Month
	)
	, LatestStats as
	(

		select
			p.businessUnitID,
			DestinationId = 1,
			p.Month,
			
			NewSearchPageViews = p.InSearchResult - convert(int, p.InSearchResult * r.UsedSearchPageViewsRatio),
			NewDetailPageViews = p.RequestDetail - convert(int, p.RequestDetail * r.UsedDetailPageViewsRatio),
			NewEmailLeads = p.EmailDealerNew,
			NewMapsViewed = p.ViewMap - convert(int, p.ViewMap * r.UsedMapsViewedRatio),
			NewAdPrints = p.PrintAD - convert(int, p.PrintAD * r.UsedAdPrintsRatio),
			NewPhoneLeads = p.PhoneLeadsNew,
			
			UsedSearchPageViews = convert(int, p.InSearchResult * r.UsedSearchPageViewsRatio),
			UsedDetailPageViews = convert(int, p.RequestDetail * r.UsedDetailPageViewsRatio),
			UsedEmailLeads = p.EmailDealerUsed,
			UsedMapsViewed = convert(int, p.ViewMap * r.UsedMapsViewedRatio),
			UsedAdPrints = convert(int, p.PrintAD * r.UsedAdPrintsRatio),
			UsedPhoneLeads = p.PhoneLeadsUsed,
					
			LastLoadId = @LoadId,
			RequestId = p.RequestId,
			DateCollected,
			p.DateLoaded,
			
			r.UsedSearchPageViewsRatio,
			r.UsedDetailPageViewsRatio,
			r.UsedMapsViewedRatio,
			r.UsedAdPrintsRatio
		from
			LastFile p
			join @ratios r
				on p.businessUnitID = r.BusinessUnitId
				and r.DestinationId = 1
				and p.month = r.ReportMonth
	)
	update m set
		NewSearchPageViews = l.NewSearchPageViews,
		NewDetailPageViews = l.NewDetailPageViews,
		NewEmailLeads = l.NewEmailLeads,
		NewMapsViewed = l.NewMapsViewed,
		NewAdPrints = l.NewAdPrints,
		NewPhoneLeads = l.NewPhoneLeads,

		
		UsedSearchPageViews = l.UsedSearchPageViews,
		UsedDetailPageViews = l.UsedDetailPageViews,
		UsedEmailLeads = l.UsedEmailLeads,
		UsedMapsViewed = l.UsedMapsViewed,
		UsedAdPrints = l.UsedAdPrints,
		UsedPhoneLeads = l.UsedPhoneLeads,

    
		LastLoadId = l.LastLoadId,
		RequestId = l.RequestId,
		DateCollected = l.DateCollected,
		DateLoaded = l.DateLoaded,
		
		UsedSearchPageViewsRatio = l.UsedSearchPageViewsRatio,
		UsedDetailPageViewsRatio = l.UsedDetailPageViewsRatio,
		UsedMapsViewedRatio = l.UsedMapsViewedRatio,
		UsedAdPrintsRatio = l.UsedAdPrintsRatio,
		
		DatasourceInterfaceId = @DatasourceInterfaceId
	from
		Merchandising.dashboard.PerformanceSummary m
		join LatestStats l
			on m.BusinessUnitId = l.businessUnitID
			and m.DestinationId = l.DestinationId
			and m.Month = l.month





						
			
	; with LastFile as
	(
		select
			p.businessUnitID,
			p.Month,
			
			InSearchResult = max(p.InSearchResult),
			RequestDetail = max(p.RequestDetail),
			EmailDealerNew = max(EmailDealerNew),
			ViewMap = max(p.ViewMap),
			PrintAD = max(p.PrintAD),
			
			EmailDealerUsed = max(p.EmailDealerUsed),
      
			PhoneLeadsNew = max(p.PhoneProspectNew),
			PhoneLeadsUsed = max(p.PhoneProspectsUsed),
					
			LastLoadId = @LoadId,
			RequestId = max(p.RequestId),
			DateCollected = max(coalesce(p.FetchDateInserted, p.DateLoaded, '1900-01-01')),
			DateLoaded = max(p.DateLoaded)
		from
			Merchandising.merchandising.CarsDotComPerfSummary p
		where
			p.Load_ID = @LoadId
		group by
			p.businessUnitID,
			p.Month  
	)
	, LatestStats as
	(
		select
			p.businessUnitID,
			DestinationId = 1,
			p.Month,
			
			NewSearchPageViews = p.InSearchResult - convert(int, p.InSearchResult * r.UsedSearchPageViewsRatio),
			NewDetailPageViews = p.RequestDetail - convert(int, p.RequestDetail * r.UsedDetailPageViewsRatio),
			NewEmailLeads = p.EmailDealerNew,
			NewMapsViewed = p.ViewMap - convert(int, p.ViewMap * r.UsedMapsViewedRatio),
			NewAdPrints = p.PrintAD - convert(int, p.PrintAD * r.UsedAdPrintsRatio),
			NewPhoneLeads = p.PhoneLeadsNew,
			
			UsedSearchPageViews = convert(int, p.InSearchResult * r.UsedSearchPageViewsRatio),
			UsedDetailPageViews = convert(int, p.RequestDetail * r.UsedDetailPageViewsRatio),
			UsedEmailLeads = p.EmailDealerUsed,
			UsedMapsViewed = convert(int, p.ViewMap * r.UsedMapsViewedRatio),
			UsedAdPrints = convert(int, p.PrintAD * r.UsedAdPrintsRatio),
			UsedPhoneLeads = p.PhoneLeadsUsed,
					
			LastLoadId = @LoadId,
			RequestId = p.RequestId,
			DateCollected,
			p.DateLoaded,
			
			r.UsedSearchPageViewsRatio,
			r.UsedDetailPageViewsRatio,
			r.UsedMapsViewedRatio,
			r.UsedAdPrintsRatio
		from
			LastFile p
			join @ratios r
				on p.businessUnitID = r.BusinessUnitId
				and r.DestinationId = 1
				and p.month = r.ReportMonth
	)
	insert Merchandising.dashboard.PerformanceSummary
	(
		BusinessUnitId,
		DestinationId,
		Month,

		NewSearchPageViews,
		NewDetailPageViews,
		NewEmailLeads,
		NewMapsViewed,
		NewAdPrints,
		NewPhoneLeads,

		UsedSearchPageViews,
		UsedDetailPageViews,
		UsedEmailLeads,
		UsedMapsViewed,
		UsedAdPrints,
		UsedPhoneLeads,

		LastLoadId,
		RequestId,
		DateCollected,
		DateLoaded,
		
		
		UsedSearchPageViewsRatio,
		UsedDetailPageViewsRatio,
		UsedMapsViewedRatio,
		UsedAdPrintsRatio,
		
		DatasourceInterfaceId
	)
	select 
		BusinessUnitId,
		DestinationId,
		Month,

		NewSearchPageViews,
		NewDetailPageViews,
		NewEmailLeads,
		NewMapsViewed,
		NewAdPrints,
		NewPhoneLeads,

		UsedSearchPageViews,
		UsedDetailPageViews,
		UsedEmailLeads,
		UsedMapsViewed,
		UsedAdPrints,
		UsedPhoneLeads,

		LastLoadId,
		RequestId,
		DateCollected,
		DateLoaded,
		
		
		UsedSearchPageViewsRatio,
		UsedDetailPageViewsRatio,
		UsedMapsViewedRatio,
		UsedAdPrintsRatio,
		
		@DatasourceInterfaceId
	from LatestStats l
	where 
		not exists
		(
			select *
			from Merchandising.dashboard.PerformanceSummary 
			where
				BusinessUnitId = l.businessUnitID
				and DestinationId = l.DestinationId
				and Month = l.month
		)

	commit
end try
begin catch
	rollback
	
	set @error =
		'Failed to load data to PerformanceSummary! ' +
		' ErrorNumber ' + isnull(convert(varchar(11), error_number()), '<null>') +
		', ErrorSeverity ' + isnull(convert(varchar(11), error_severity()), '<null>') +
		', ErrorState ' + isnull(convert(varchar(11), error_state()), '<null>') +
		', ErrorProcedure ' + isnull(error_procedure(), '<null>') +
		', ErrorLine ' + isnull(convert(varchar(11), error_line()), '<null>') +
		', ErrorMessage ' + isnull(error_message(), '<null>')
	
	raiserror(@error, 16, 1)
	return 1
end catch

	
return 0
	

go



		
	
