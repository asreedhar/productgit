if object_id('dashboard.DealerListingSiteInventoryCounts#Group') is not null
	drop procedure dashboard.DealerListingSiteInventoryCounts#Group
GO

create procedure dashboard.DealerListingSiteInventoryCounts#Group
	@GroupID int, 
	@VehicleType int, 
	@StartDate datetime, 
	@EndDate datetime 
as

set transaction isolation level read uncommitted
set nocount on

if (@VehicleType = 0)
begin
	exec dashboard.DealerListingSiteInventoryCounts#Group#NewAndUsed
		@GroupID = @GroupId,
		@StartDate = @StartDate, 
		@EndDate = @EndDate
end
else
begin
	exec dashboard.DealerListingSiteInventoryCounts#Group#InventoryType
		@GroupID = @GroupId,
		@StartDate = @StartDate, 
		@EndDate = @EndDate,
		@VehicleType = @VehicleType
end

GO

grant execute on dashboard.DealerListingSiteInventoryCounts#Group to MerchandisingUser 
grant execute on dashboard.DealerListingSiteInventoryCounts#Group to merchandisingWebsite 
GO

