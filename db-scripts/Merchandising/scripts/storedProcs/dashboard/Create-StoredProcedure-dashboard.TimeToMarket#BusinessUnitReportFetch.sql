
-- ==============================================
-- Author:		Travis Huber
-- Create date: 2013-04-18
-- Description:	Get report data per businessunit
--				Optional Start and End dates are
--				Inclusive and should be date only
--				eg: 2013-04-01
--				OR beginning of day:
--					2013-04-01 00:00:00
-- 
-- BUGZID: 26724 - added active flag to hide inventory URL in UI, tureen 10/23/2013
-- ==============================================
USE Merchandising
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'dashboard.TimeToMarket#BusinessUnitReportFetch') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE dashboard.TimeToMarket#BusinessUnitReportFetch
GO

CREATE PROCEDURE dashboard.TimeToMarket#BusinessUnitReportFetch 
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@StartDate DateTime = NULL,
	@EndDate DateTime = NULL,
	@VehicleType int = 0
AS
BEGIN
	
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	
	
	SET NOCOUNT ON;

	SELECT i.InventoryActive,
		ttmt.BusinessUnitID, bu.BusinessUnit, ttmt.InventoryID, v.VehicleID, v.VIN, i.StockNumber, CAST( v.VehicleYear AS VARCHAR) AS [Year], v.Make, v.Model, v.VehicleTrim AS [Trim], i.InventoryReceivedDate AS DateAddedToInventory,
		PriceFirstDate, PhotosFirstDate, DescriptionFirstDate, AdApprovalFirstDate, CompleteAdFirstDate, GIDFirstDate
	FROM Merchandising.dashboard.TimeToMarketTracking ttmt
	JOIN IMT.dbo.Inventory i ON ttmt.BusinessUnitId = i.BusinessUnitID AND ttmt.InventoryId = i.InventoryID
    JOIN IMT.dbo.Vehicle v ON i.VehicleID = v.VehicleID
    JOIN IMT.dbo.BusinessUnit bu ON bu.BusinessUnitId = ttmt.BusinessUnitId
	WHERE ttmt.BusinessUnitId = @BusinessUnitId
		AND (@VehicleType = 0 OR i.InventoryType = @VehicleType)
		AND ( 
			@StartDate IS NULL OR 
			(
				ttmt.CompleteAdFirstDate > @StartDate OR
				ttmt.PhotosFirstDate > @StartDate
			)
		) AND (
			@EndDate IS NULL OR
			(
				ttmt.CompleteAdFirstDate < @EndDate OR
				ttmt.PhotosFirstDate < @EndDate
			)
		)
		
END
GO


GRANT EXECUTE on dashboard.TimeToMarket#BusinessUnitReportFetch  to MerchandisingUser
GO
