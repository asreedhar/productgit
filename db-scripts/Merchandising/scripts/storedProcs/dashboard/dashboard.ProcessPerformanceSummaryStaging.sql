if object_id('dashboard.ProcessPerformanceSummaryStaging') is not null
	drop procedure dashboard.ProcessPerformanceSummaryStaging
go

create procedure dashboard.ProcessPerformanceSummaryStaging
	@LoadId int,
	@DatasourceInterfaceId int
as
set nocount on
set transaction isolation level read uncommitted


if (@DatasourceInterfaceId = 2010)
begin
	-- Cars.com direct feed
	; with StoresAndMonths as
	(
		select distinct
			BusinessUnitId,
			DestinationId,
			Month
		from dashboard.PerformanceSummaryStagingCarsDotComDirect
		where LoadId = @LoadId
	)
	, LatestStats as
	(
		select
			BusinessUnitId,
			DestinationId,
			Month,
			
			NewSearchPageViews = sum(NewSearchPageViews),
			NewDetailPageViews = sum(NewDetailPageViews),
			NewEmailLeads = sum(NewEmailLeads),
			NewMapsViewed = sum(NewMapsViewed),
			NewAdPrints = sum(NewAdPrints),
			UsedSearchPageViews = sum(UsedSearchPageViews),
			UsedDetailPageViews = sum(UsedDetailPageViews),
			UsedEmailLeads = sum(UsedEmailLeads),
			UsedMapsViewed = sum(UsedMapsViewed),
			UsedAdPrints = sum(UsedAdPrints),
			NewPhoneLeads = sum(NewPhoneLeads),
			NewWebsiteClicks = sum(NewWebsiteClicks),
			NewChatRequests = sum(NewChatRequests),
			NewVideosViewed = sum(NewVideosViewed),
			UsedPhoneLeads = sum(UsedPhoneLeads),
			UsedWebsiteClicks = sum(UsedWebsiteClicks),
			UsedChatRequests = sum(UsedChatRequests),
			UsedVideosViewed = sum(UsedVideosViewed),
			
			RequestId = convert(int, null),
			DateCollected = max(DateCollected),
			DateLoaded = max(DateLoaded),
			DatasourceInterfaceId = max(DatasourceInterfaceId)
			
		from dashboard.PerformanceSummaryStagingCarsDotComDirect m
		where
			exists
			(
				select *
				from StoresAndMonths
				where
					BusinessUnitId = m.BusinessUnitId
					and DestinationId = m.DestinationId
					and m.Month = m.Month
			)
		group by
			BusinessUnitId,
			DestinationId,
			Month
	)
	update metrics set
		NewSearchPageViews = s.NewSearchPageViews,
		NewDetailPageViews = s.NewDetailPageViews,
		NewEmailLeads = s.NewEmailLeads,
		NewMapsViewed = s.NewMapsViewed,
		NewAdPrints = s.NewAdPrints,
		UsedSearchPageViews = s.UsedSearchPageViews,
		UsedDetailPageViews = s.UsedDetailPageViews,
		UsedEmailLeads = s.UsedEmailLeads,
		UsedMapsViewed = s.UsedMapsViewed,
		UsedAdPrints = s.UsedAdPrints,
		LastLoadId = @LoadId,
		NewPhoneLeads = s.NewPhoneLeads,
		NewWebsiteClicks = s.NewWebsiteClicks,
		NewChatRequests = s.NewChatRequests,
		NewVideosViewed = s.NewVideosViewed,
		UsedPhoneLeads = s.UsedPhoneLeads,
		UsedWebsiteClicks = s.UsedWebsiteClicks,
		UsedChatRequests = s.UsedChatRequests,
		UsedVideosViewed = s.UsedVideosViewed,
		RequestId = s.RequestId,
		DateCollected = s.DateCollected,
		DateLoaded = s.DateLoaded,
		DatasourceInterfaceId = s.DatasourceInterfaceId
	from
		Merchandising.dashboard.PerformanceSummary metrics
		join LatestStats s
			on metrics.BusinessUnitId = s.businessUnitID
			and metrics.DestinationId = s.DestinationId
			and metrics.Month = s.Month
			
			
			
			
			
			
	; with StoresAndMonths as
	(
		select distinct
			BusinessUnitId,
			DestinationId,
			Month
		from dashboard.PerformanceSummaryStagingCarsDotComDirect
		where LoadId = @LoadId
	)
	, LatestStats as
	(
		select
			BusinessUnitId,
			DestinationId,
			Month,
			
			NewSearchPageViews = sum(NewSearchPageViews),
			NewDetailPageViews = sum(NewDetailPageViews),
			NewEmailLeads = sum(NewEmailLeads),
			NewMapsViewed = sum(NewMapsViewed),
			NewAdPrints = sum(NewAdPrints),
			UsedSearchPageViews = sum(UsedSearchPageViews),
			UsedDetailPageViews = sum(UsedDetailPageViews),
			UsedEmailLeads = sum(UsedEmailLeads),
			UsedMapsViewed = sum(UsedMapsViewed),
			UsedAdPrints = sum(UsedAdPrints),
			NewPhoneLeads = sum(NewPhoneLeads),
			NewWebsiteClicks = sum(NewWebsiteClicks),
			NewChatRequests = sum(NewChatRequests),
			NewVideosViewed = sum(NewVideosViewed),
			UsedPhoneLeads = sum(UsedPhoneLeads),
			UsedWebsiteClicks = sum(UsedWebsiteClicks),
			UsedChatRequests = sum(UsedChatRequests),
			UsedVideosViewed = sum(UsedVideosViewed),
			
			RequestId = convert(int, null),
			DateCollected = max(DateCollected),
			DateLoaded = max(DateLoaded),
			DatasourceInterfaceId = max(DatasourceInterfaceId)
			
		from dashboard.PerformanceSummaryStagingCarsDotComDirect m
		where
			exists
			(
				select *
				from StoresAndMonths
				where
					BusinessUnitId = m.BusinessUnitId
					and DestinationId = m.DestinationId
					and m.Month = m.Month
			)
		group by
			BusinessUnitId,
			DestinationId,
			Month
	)
	insert dashboard.PerformanceSummary
	(
		BusinessUnitId,
		DestinationId,
		Month,
		NewSearchPageViews,
		NewDetailPageViews,
		NewEmailLeads,
		NewMapsViewed,
		NewAdPrints,
		UsedSearchPageViews,
		UsedDetailPageViews,
		UsedEmailLeads,
		UsedMapsViewed,
		UsedAdPrints,
		LastLoadId,
		NewPhoneLeads,
		NewWebsiteClicks,
		NewChatRequests,
		NewVideosViewed,
		UsedPhoneLeads,
		UsedWebsiteClicks,
		UsedChatRequests,
		UsedVideosViewed,
		RequestId,
		DateCollected,
		DateLoaded,
		DatasourceInterfaceId
	)
	select
		BusinessUnitId,
		DestinationId,
		Month,
		NewSearchPageViews,
		NewDetailPageViews,
		NewEmailLeads,
		NewMapsViewed,
		NewAdPrints,
		UsedSearchPageViews,
		UsedDetailPageViews,
		UsedEmailLeads,
		UsedMapsViewed,
		UsedAdPrints,
		@LoadId,
		NewPhoneLeads,
		NewWebsiteClicks,
		NewChatRequests,
		NewVideosViewed,
		UsedPhoneLeads,
		UsedWebsiteClicks,
		UsedChatRequests,
		UsedVideosViewed,
		RequestId,
		DateCollected,
		DateLoaded,
		DatasourceInterfaceId
	from LatestStats s
	where
		not exists
		(
			select *
			from dashboard.PerformanceSummary
			where
				BusinessUnitId = s.businessUnitID
				and DestinationId = s.DestinationId
				and Month = s.Month
		)










end
else
begin
	-- all other non-Cars.com direct feeds
	
	; with LatestStats as
	(
		select
			BusinessUnitId,
			DestinationId,
			Month,
			
			NewSearchPageViews = max(NewSearchPageViews),
			NewDetailPageViews = max(NewDetailPageViews),
			NewEmailLeads = max(NewEmailLeads),
			NewMapsViewed = max(NewMapsViewed),
			NewAdPrints = max(NewAdPrints),
			UsedSearchPageViews = max(UsedSearchPageViews),
			UsedDetailPageViews = max(UsedDetailPageViews),
			UsedEmailLeads = max(UsedEmailLeads),
			UsedMapsViewed = max(UsedMapsViewed),
			UsedAdPrints = max(UsedAdPrints),
			NewPhoneLeads = max(NewPhoneLeads),
			NewWebsiteClicks = max(NewWebsiteClicks),
			NewChatRequests = max(NewChatRequests),
			NewVideosViewed = max(NewVideosViewed),
			UsedPhoneLeads = max(UsedPhoneLeads),
			UsedWebsiteClicks = max(UsedWebsiteClicks),
			UsedChatRequests = max(UsedChatRequests),
			UsedVideosViewed = max(UsedVideosViewed),
			
			RequestId = max(RequestId),
			DateCollected = max(DateCollected),
			DateLoaded = max(DateLoaded),
			DatasourceInterfaceId = max(DatasourceInterfaceId)
		from dashboard.PerformanceSummaryStaging
		where LoadId = @LoadId
		group by
			BusinessUnitId,
			DestinationId,
			Month
	)
	update metrics set
		NewSearchPageViews = s.NewSearchPageViews,
		NewDetailPageViews = s.NewDetailPageViews,
		NewEmailLeads = s.NewEmailLeads,
		NewMapsViewed = s.NewMapsViewed,
		NewAdPrints = s.NewAdPrints,
		UsedSearchPageViews = s.UsedSearchPageViews,
		UsedDetailPageViews = s.UsedDetailPageViews,
		UsedEmailLeads = s.UsedEmailLeads,
		UsedMapsViewed = s.UsedMapsViewed,
		UsedAdPrints = s.UsedAdPrints,
		LastLoadId = @LoadId,
		NewPhoneLeads = s.NewPhoneLeads,
		NewWebsiteClicks = s.NewWebsiteClicks,
		NewChatRequests = s.NewChatRequests,
		NewVideosViewed = s.NewVideosViewed,
		UsedPhoneLeads = s.UsedPhoneLeads,
		UsedWebsiteClicks = s.UsedWebsiteClicks,
		UsedChatRequests = s.UsedChatRequests,
		UsedVideosViewed = s.UsedVideosViewed,
		RequestId = s.RequestId,
		DateCollected = s.DateCollected,
		DateLoaded = s.DateLoaded,
		DatasourceInterfaceId = s.DatasourceInterfaceId
	from
		Merchandising.dashboard.PerformanceSummary metrics
		join LatestStats s
			on metrics.BusinessUnitId = s.businessUnitID
			and metrics.DestinationId = s.DestinationId
			and metrics.Month = s.Month
			
			
			
			
			
			
	; with LatestStats as
	(
		select
			BusinessUnitId,
			DestinationId,
			Month,
			
			NewSearchPageViews = max(NewSearchPageViews),
			NewDetailPageViews = max(NewDetailPageViews),
			NewEmailLeads = max(NewEmailLeads),
			NewMapsViewed = max(NewMapsViewed),
			NewAdPrints = max(NewAdPrints),
			UsedSearchPageViews = max(UsedSearchPageViews),
			UsedDetailPageViews = max(UsedDetailPageViews),
			UsedEmailLeads = max(UsedEmailLeads),
			UsedMapsViewed = max(UsedMapsViewed),
			UsedAdPrints = max(UsedAdPrints),
			NewPhoneLeads = max(NewPhoneLeads),
			NewWebsiteClicks = max(NewWebsiteClicks),
			NewChatRequests = max(NewChatRequests),
			NewVideosViewed = max(NewVideosViewed),
			UsedPhoneLeads = max(UsedPhoneLeads),
			UsedWebsiteClicks = max(UsedWebsiteClicks),
			UsedChatRequests = max(UsedChatRequests),
			UsedVideosViewed = max(UsedVideosViewed),
			
			RequestId = max(RequestId),
			DateCollected = max(DateCollected),
			DateLoaded = max(DateLoaded),
			DatasourceInterfaceId = max(DatasourceInterfaceId)
		from dashboard.PerformanceSummaryStaging
		where LoadId = @LoadId
		group by
			BusinessUnitId,
			DestinationId,
			Month
	)
	insert dashboard.PerformanceSummary
	(
		BusinessUnitId,
		DestinationId,
		Month,
		NewSearchPageViews,
		NewDetailPageViews,
		NewEmailLeads,
		NewMapsViewed,
		NewAdPrints,
		UsedSearchPageViews,
		UsedDetailPageViews,
		UsedEmailLeads,
		UsedMapsViewed,
		UsedAdPrints,
		LastLoadId,
		NewPhoneLeads,
		NewWebsiteClicks,
		NewChatRequests,
		NewVideosViewed,
		UsedPhoneLeads,
		UsedWebsiteClicks,
		UsedChatRequests,
		UsedVideosViewed,
		RequestId,
		DateCollected,
		DateLoaded,
		DatasourceInterfaceId
	)
	select
		BusinessUnitId,
		DestinationId,
		Month,
		NewSearchPageViews,
		NewDetailPageViews,
		NewEmailLeads,
		NewMapsViewed,
		NewAdPrints,
		UsedSearchPageViews,
		UsedDetailPageViews,
		UsedEmailLeads,
		UsedMapsViewed,
		UsedAdPrints,
		@LoadId,
		NewPhoneLeads,
		NewWebsiteClicks,
		NewChatRequests,
		NewVideosViewed,
		UsedPhoneLeads,
		UsedWebsiteClicks,
		UsedChatRequests,
		UsedVideosViewed,
		RequestId,
		DateCollected,
		DateLoaded,
		DatasourceInterfaceId
	from LatestStats s
	where
		not exists
		(
			select *
			from dashboard.PerformanceSummary
			where
				BusinessUnitId = s.businessUnitID
				and DestinationId = s.DestinationId
				and Month = s.Month
		)
end
			
		
		
go
