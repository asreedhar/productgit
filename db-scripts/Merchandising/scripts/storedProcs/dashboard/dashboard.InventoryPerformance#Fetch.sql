if object_id('dashboard.InventoryPerformance#Fetch') is not null
	drop procedure dashboard.InventoryPerformance#Fetch
GO

create procedure dashboard.InventoryPerformance#Fetch
	@InventoryId int
as
set nocount on
set transaction isolation level read uncommitted

SELECT IAP.DestinationId, EDT.Description, IAP.Date, IAP.SearchPageViews, IAP.DetailPageViews 
FROM IMT.dbo.Inventory I
JOIN Merchandising.dashboard.InventoryAdPerformance IAP ON IAP.InventoryID = I.InventoryID AND IAP.BusinessUnitID = I.BusinessUnitID
JOIN Merchandising.settings.EdtDestinations EDT on IAP.DestinationID = EDT.DestinationID
WHERE I.InventoryID = @InventoryId
ORDER BY IAP.DestinationId, IAP.Date

go

grant execute on dashboard.InventoryPerformance#Fetch to MerchandisingUser 
go