-- =============================================
-- Author:		Arjun Seshadri
-- Create date: 2015-02-12
-- Description:	Set the cars api start date per BusinessUnit
-- =============================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dashboard].[TimeToMarket#SetCarsApiStartDate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dashboard].[TimeToMarket#SetCarsApiStartDate]
GO


CREATE PROCEDURE dashboard.TimeToMarket#SetCarsApiStartDate
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int, 
	@CarsApiStartDate DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS( SELECT * FROM Merchandising.dashboard.TimeToMarketTracking#BuReportStart WHERE BusinessUnitId = @BusinessUnitId)
    BEGIN
        INSERT Merchandising.dashboard.TimeToMarketTracking#BuReportStart
        VALUES(@BusinessUnitId, NULL, NULL, @CarsApiStartDate)
    END
    ELSE
    BEGIN
		UPDATE Merchandising.dashboard.TimeToMarketTracking#BuReportStart
		SET CarsApiStartDate = @CarsApiStartDate
		WHERE BusinessUnitId = @BusinessUnitId AND CarsApiStartDate IS NULL;
	END
END
GO

GRANT EXECUTE on dashboard.TimeToMarket#SetCarsApiStartDate  to MerchandisingUser
GO
