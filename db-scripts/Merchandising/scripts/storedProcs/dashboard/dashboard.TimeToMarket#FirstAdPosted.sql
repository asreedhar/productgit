-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-06-20
-- Description:	Get the first AdPosted date for 
--				a set of inventoryIds
-- =============================================
USE Merchandising
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dashboard].[TimeToMarket#FirstAdPostedFetch]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE dashboard.TimeToMarket#FirstAdPostedFetch
GO

CREATE PROCEDURE dashboard.TimeToMarket#FirstAdPostedFetch 
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int = 0, 
	@inventoryIds dbo.StringValTableType readonly
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	/******************************************************************
	   return a map of inventoryids and the first posting date
	   from the postings.VehicleAdScheduling
	 ******************************************************************/
	IF @BusinessUnitId > 0 AND (SELECT COUNT(*) FROM @inventoryIds) > 0
	BEGIN
	
		SELECT inventoryId as InventoryId, MIN(actualReleaseTime) as ReleaseTime
		FROM Merchandising.postings.VehicleAdScheduling vas
		JOIN @inventoryIds i ON vas.businessUnitID = @BusinessUnitId AND CAST(i.StringValue AS INT) = vas.inventoryId AND vas.actualReleaseTime IS NOT NULL
		GROUP BY vas.inventoryId
	END
END
GO

grant execute on dashboard.TimeToMarket#FirstAdPostedFetch to MerchandisingUser
GO

