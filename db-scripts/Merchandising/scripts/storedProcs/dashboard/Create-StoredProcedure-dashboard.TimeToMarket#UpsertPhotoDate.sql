-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-04-17
-- Description:	Update first photo date for a 
--				list of vins
-- =============================================
USE Merchandising
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dashboard].[TimeToMarket#UpsertPhotoDate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dashboard].[TimeToMarket#UpsertPhotoDate]
GO

CREATE PROCEDURE dashboard.TimeToMarket#UpsertPhotoDate 
	@BusinessUnitId int, 
	@VinList dbo.StringValTableType READONLY,
	@BatchDate DATETIME = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF @BatchDate IS NULL SET @BatchDate = GETDATE();

	DECLARE @UpdateValues TABLE
	(
		BusinessUnitId int,
		InventoryId int
	)
	
	DECLARE @InsertValues TABLE
	(
		BusinessUnitId int,
		InventoryId int
	)
	
	-- Get the set to be updated.
	INSERT INTO @UpdateValues ( BusinessUnitId, InventoryId)
	SELECT @BusinessUnitId AS BusinessUnitId, inv.InventoryID
	FROM Merchandising.dashboard.TimeToMarketTracking ttmt
	JOIN IMT.dbo.Inventory inv ON ttmt.InventoryId = inv.InventoryID 
		AND ttmt.BusinessUnitId = inv.BusinessUnitID
	JOIN IMT.dbo.Vehicle veh ON veh.VehicleID = inv.VehicleID
	WHERE ttmt.BusinessUnitId = @BusinessUnitId 
		AND ttmt.PhotosFirstDate = NULL
		AND veh.VIN IN (SELECT StringValue FROM @VinList)

	-- Get the set to be inserted
	INSERT INTO @InsertValues ( BusinessUnitId, InventoryId)
	SELECT @BusinessUnitId AS BusinessUnitId, inv.InventoryID --, ttmt.*
	FROM IMT.dbo.Inventory inv 
	LEFT JOIN Merchandising.dashboard.TimeToMarketTracking ttmt ON ttmt.InventoryId = inv.InventoryID 
		AND ttmt.BusinessUnitId = inv.BusinessUnitID
	JOIN IMT.dbo.Vehicle veh ON veh.VehicleID = inv.VehicleID
	WHERE inv.BusinessUnitId = @BusinessUnitId AND inv.InventoryActive = 1 AND veh.VIN IN (SELECT StringValue FROM @VinList)
	AND ttmt.Inserted IS NULL


	-- Perform the update.
	UPDATE Merchandising.dashboard.TimeToMarketTracking
	SET PhotosFirstDate = @BatchDate WHERE BusinessUnitId = @BusinessUnitId 
		AND InventoryId IN (SELECT InventoryId FROM @UpdateValues)

	--Perform the insert.
	INSERT INTO Merchandising.dashboard.TimeToMarketTracking (BusinessUnitId, InventoryId, PhotosFirstDate)
	SELECT BusinessUnitId, InventoryId, @BatchDate
	FROM @InsertValues
END
GO

grant execute on dashboard.TimeToMarket#UpsertPhotoDate to MerchandisingUser
go
