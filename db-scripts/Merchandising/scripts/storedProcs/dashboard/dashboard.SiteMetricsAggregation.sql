if object_id('dashboard.SiteMetricsAggregation') is not null
	drop procedure dashboard.SiteMetricsAggregation
go

create procedure dashboard.SiteMetricsAggregation
	@LoadID int,
	@DatasourceInterfaceID int,
	@destinationID int
	
as
set nocount on
set transaction isolation level read uncommitted



	
if @DatasourceInterfaceID in (10, 11, 2001, 2002, 2005)
begin
	-- nothing to do for these incoming files
	return 0
end
else if @DatasourceInterfaceID = 9 -- AutoTrader phone leads
begin
	exec Merchandising.dashboard.AggregateAutoTraderPhoneLeads 
		@LoadId = @LoadID
end
else if @DatasourceInterfaceID = 2003 -- Performance Summary file for Cars.com
	exec Merchandising.dashboard.AggregateCarsDotComPerfSummary 
	    @LoadId = @LoadID, -- int
	    @DatasourceInterfaceId = @DatasourceInterfaceID -- int
	
else if @DatasourceInterfaceID = 12 -- Performance Summary file for AT
begin
	exec Merchandising.dashboard.AggregateAutoTraderPerfSummary
		@LoadID = @LoadID
end
else if @DatasourceInterfaceID in (13, 2004, 2006) -- Vehicle Summary file for AT (13), Cars (2004), AT/Hendrick (2006)
begin
	exec Merchandising.dashboard.CalculateInventoryDifferentials
		@LoadID = @LoadID,
		@DatasourceInterfaceID = @DatasourceInterfaceID,
		@destinationID  = @destinationID
end
else if @DatasourceInterfaceID in (2007, 2008) -- Performance Summary file for Hendrick stores directly from AutoTrader (one is monthly, one is daily, same format)
begin
	exec Merchandising.dashboard.AggregateAutoTraderHendrickPerfSummary
		@LoadID = @LoadID
end
else if @DatasourceInterfaceID = 2009 -- AT/Hendrick Online
begin
	exec Merchandising.dashboard.UpdateInventoryIdAutoTraderHendrickOnline 
	    @LoadId = @LoadID
	    
	exec dashboard.SaveLastOnlineSnapshot
		@LoadID = @LoadId,
		@DatasourceInterfaceID = @DatasourceInterfaceId	    
end
else if @DatasourceInterfaceID in (2000, 14) -- fetch cars and fetch at online
begin
	exec dashboard.SaveLastOnlineSnapshot
		@LoadID = @LoadId,
		@DatasourceInterfaceID = @DatasourceInterfaceId
end
else
begin
	raiserror('Merchandising.dashboard.SiteMetricsAggregation switches on DatasourceInterfaceId.  No code found for DatasourceInterfaceId %d!', 16, 1, @DatasourceInterfaceID)
end



go




