if object_id('dashboard.AggregateVehicleActivityInventoryCounts') is not null
	drop procedure dashboard.AggregateVehicleActivityInventoryCounts
go

create procedure dashboard.AggregateVehicleActivityInventoryCounts
as

set transaction isolation level read uncommitted
set nocount on


; with invByMonth as
(
	select
		p.BusinessUnitId,
		i.InventoryType,
		Month = dateadd(month, datediff(month, 0, p.Date), 0),
		p.DestinationId,
		p.inventoryId
	from
		Merchandising.dashboard.InventoryAdPerformance p
		join IMT.dbo.Inventory i
			on p.InventoryId = i.InventoryID
	group by
		p.BusinessUnitId,
		i.InventoryType,
		dateadd(month, datediff(month, 0, p.Date), 0),
		p.DestinationId,
		p.InventoryID
)
insert Merchandising.dashboard.VehicleActivityInventoryCounts
(
	 businessUnitId,
	 inventoryType,
	 month,
	 destinationId,
	 inventoryId
)
select
	 businessUnitId,
	 inventoryType,
	 month,
	 destinationId,
	 inventoryId
from invByMonth i
where not exists
	(
		select *
		from Merchandising.dashboard.VehicleActivityInventoryCounts
		where
			businessUnitId = i.BusinessUnitId
			and inventoryType = i.InventoryType
			and month = i.Month
			and destinationId = i.DestinationId
			and inventoryId = i.InventoryId
	)
go

go
