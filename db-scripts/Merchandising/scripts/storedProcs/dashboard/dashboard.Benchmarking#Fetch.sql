use Merchandising
go

if object_id('dashboard.Benchmarking#Fetch') is not null
	drop procedure dashboard.Benchmarking#Fetch
go


create procedure dashboard.Benchmarking#Fetch
	@SiteId tinyint,
	@VehicleType tinyint,
	@DealerSegmentType tinyint,
	@EndDate datetime
as

set nocount on
set transaction isolation level read uncommitted

SELECT TOP 1 ImpressionCost, LeadCost
FROM Merchandising.dbo.BenchmarkingData bd
WHERE SiteId = @SiteId AND VehicleType = @VehicleType AND DealershipSegmentType = @DealerSegmentType
	AND CAST( CAST(bd.Year AS varchar(4)) + '-'+ CAST(bd.Month AS VARCHAR(2)) + '-' + '1' AS DATE) <= @EndDate
ORDER BY Year DESC, Month DESC

go

grant execute on dashboard.Benchmarking#Fetch to MerchandisingUser
go




