if object_id('dashboard.PriceEvents#Fetch') is not null
	drop procedure dashboard.PriceEvents#Fetch
GO

create procedure dashboard.PriceEvents#Fetch
	@InventoryId int
as
set nocount on
set transaction isolation level read uncommitted

SELECT Source, RepriceDate, NewPrice
FROM Merchandising.Reports.InventoryPriceHistory
WHERE InventoryID = @InventoryId
ORDER BY RepriceDate

go

grant execute on dashboard.PriceEvents#Fetch to MerchandisingUser 
go