if object_id('dashboard.SetInventoryId') is not null
	drop procedure dashboard.SetInventoryId
go

/*
	lookup InventoryId using incoming BusinessUnitId, Vin, and DateCollected, and
	then write it to the staged input data
*/

create procedure dashboard.SetInventoryId
	@LoadId int,
	@IsOnline bit,
	@IsActivity bit,
	@IsPerformanceSummary bit
as
set nocount on
set transaction isolation level read uncommitted


	
declare
	@updateErrorMessage varchar(max),
	@RowId int,
	@Vin varchar(17),
	@DateCollected datetime,
	@BusinessUnitId int,
	@InventoryId int,
	@inputRows int




set @updateErrorMessage =
	'expecting to update exactly one record with the new InventoryId from Vin lookup!'
	+ char(10) + '@LoadId = ' + isnull(convert(varchar(max), @LoadId), '<null>')
	+ char(10) + '@IsOnline = ' + isnull(convert(varchar(max), @IsOnline), '<null>')
	+ char(10) + '@IsActivity = ' + isnull(convert(varchar(max), @IsActivity), '<null>')
	+ char(10) + '@IsPerformanceSummary = ' + isnull(convert(varchar(max), @IsPerformanceSummary), '<null>')



 declare @online table
 (
	LoadId int not null,
	RowId int not null,
	BusinessUnitId int not null,
	Vin varchar(17) not null,
	DateCollected datetime not null,
	
	primary key clustered
	(
		LoadId,
		RowId
	)
 )


 declare @activity table
 (
	LoadId int not null,
	RowId int not null,
	BusinessUnitId int not null,
	Vin varchar(17) not null,
	DateCollected datetime not null,
	
	primary key clustered
	(
		LoadId,
		RowId
	)
 )


-- load input queues
	if (@isOnline = 1)
	begin
		insert @online
		(
			LoadId,
			RowId,
			BusinessUnitId,
			Vin,
			DateCollected
		)
		select
			LoadId,
			RowId,
			BusinessUnitId,
			Vin,
			DateCollected
		from Merchandising.dashboard.VehiclesOnlineStaging
		where
			LoadId = @LoadId
			and BusinessUnitId is not null
			
		set @inputRows = @@rowcount
	end
	
	if (@IsActivity = 1)
	begin
		insert @activity
		(
			LoadId,
			RowId,
			BusinessUnitId,
			Vin,
			DateCollected
		)
		select
			LoadId,
			RowId,
			BusinessUnitId,
			Vin,
			DateCollected
		from Merchandising.dashboard.VehicleActivityStaging
		where
			LoadId = @LoadId
			
		set @inputRows = @@rowcount
	end

-- process queues (@inputRows is not decremented, it is only there to protect infinite loop on misconfiguration or empty file)
while (1=1 and isnull(@inputRows, 0) > 1)
begin
	set @InventoryId = null
	
	-- get PK and lookup data from input queues
		if (@isOnline = 1)
		begin
			select top 1
				@LoadId = LoadId,
				@RowId = RowId,
				@BusinessUnitId = BusinessUnitId,
				@Vin = Vin,
				@DateCollected = DateCollected
			from @online
			
			if (@@rowcount = 0) break
			delete @online 
			where
				LoadId = @LoadId
				and RowId = @RowId
		end
		
		if (@IsActivity = 1)
		begin
			select top 1
				@LoadId = LoadId,
				@RowId = RowId,
				@BusinessUnitId = BusinessUnitId,
				@Vin = Vin,
				@DateCollected = DateCollected
			from @activity
			
			if (@@rowcount = 0) break
			delete @activity
			where
				LoadId = @LoadId
				and RowId = @RowId
		end


	-- lookup InvId
	/*
		2/26/2013 Dave Speer:
		
		Changed lookup to only match VIN to active inventory.  This didn't make sense when we were going
		to use this proc for all feeds, but since it is only being used for Cars Direct online report,
		no sense in matching to inactive inventory, they will never show on the report.  Had a case
		where there were two possible InventoryId based on InventoryReceivedDate vs. DateCollected
		and the most recent one was inactive but the next most recent one was still active.  How that
		happened, I don't know, but we'll filter out the inactive ones.
		
		4/29/2013 Daniel Hillis:
		We realized we were using inventory at inactive dealerships to map vins.  This became apparent when 
		two dealerships merged, and we continued to map vins to inventory at the inactive dealership.  This 
		was causing problems at the active dealership, so we now check the BU.Active bit.
		
	*/

		select top 1
			@InventoryId = i.InventoryID
		from
			IMT.dbo.tbl_Vehicle v
			join IMT.dbo.Inventory i
				on v.VehicleID = i.VehicleID
		where
			v.Vin = @Vin
			and datediff(day, @DateCollected, i.InventoryReceivedDate) < 1
			and i.BusinessUnitID = @BusinessUnitId
			and i.InventoryActive = 1
			and exists 
			(	-- only use inventory at active business units
				select * 
				from IMT.dbo.BusinessUnit bu 
				where bu.active = 1
				and bu.BusinessUnitId = i.BusinessUnitId
			)			
		order by
			i.InventoryReceivedDate desc

		

	-- write InvId to staging tables
		if (@isOnline = 1)
		begin
			update Merchandising.dashboard.VehiclesOnlineStaging set
				InventoryId = @InventoryId,
				ErrorCode = case when @InventoryId is null then 1 else ErrorCode end
			where
				LoadId = @LoadId
				and RowId = @RowId
			
			if (@@rowcount <> 1)
			begin
				raiserror(@updateErrorMessage, 16, 1)
				return 1
			end
		end		
		
		if (@IsActivity = 1)
		begin
			update Merchandising.dashboard.VehicleActivityStaging set
				InventoryId = @InventoryId,
				ErrorCode = case when @InventoryId is null then 1 else ErrorCode end
			where
				LoadId = @LoadId
				and RowId = @RowId
			
			if (@@rowcount <> 1)
			begin
				raiserror(@updateErrorMessage, 16, 1)
				return 1
			end
		end		
end


return 0

go	