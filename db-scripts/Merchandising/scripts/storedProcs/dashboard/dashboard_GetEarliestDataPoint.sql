if object_id('dashboard.GetEarliestDataPoint') is not null
	drop procedure dashboard.GetEarliestDataPoint
GO

create procedure dashboard.GetEarliestDataPoint
	@BusinessUnitId int, 
	@SiteId int
as
set nocount on
set transaction isolation level read uncommitted

select top 1 [Month]
from dashboard.PerformanceSummary
where
	BusinessUnitId = @BusinessUnitId
	and destinationID = @SiteId
order by [Month] asc


go

grant execute on dashboard.GetEarliestDataPoint to MerchandisingUser 
go