USE [Merchandising]
GO

/****** Object:  StoredProcedure demo.BusinessUnit#Fetch    Script Date: 07/17/2013 14:18:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-07-17
-- Description:	Get Demo BusinessUnit from buid
-- =============================================
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'demo.BusinessUnit#Fetch') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE demo.BusinessUnit#Fetch
GO

CREATE PROCEDURE demo.BusinessUnit#Fetch 
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT B.[BusinessUnitID]
		  ,[BusinessUnitTypeID]
		  ,[BusinessUnit]
		  ,[BusinessUnitCode]
		  ,[Active]
		FROM [Merchandising].[demo].[BusinessUnit] B
		WHERE B.BusinessUnitID = @BusinessUnitId


END

GO

grant execute on demo.BusinessUnit#Fetch to MerchandisingUser
go
