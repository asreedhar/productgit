SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-07-18
-- Description:	Deactivate Demo Business Units
-- =============================================
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'demo.BusinessUnit#Deactivate') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE demo.BusinessUnit#Deactivate
GO

CREATE PROCEDURE demo.BusinessUnit#Deactivate 
	@BusinessUnitId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE demo.BusinessUnit
	SET Active = 0
	WHERE BusinessUnitID = @BusinessUnitId

END
GO

grant execute on demo.BusinessUnit#Deactivate to MerchandisingUser
go

