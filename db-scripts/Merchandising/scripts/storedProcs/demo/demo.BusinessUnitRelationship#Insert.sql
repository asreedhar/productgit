USE [Merchandising]
GO

/****** Object:  StoredProcedure demo.BusinessUnitRelationship#Insert    Script Date: 07/17/2013 14:18:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-07-17
-- Description:	Insert new BU relationship
-- =============================================
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'demo.BusinessUnitRelationship#Insert') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE demo.BusinessUnitRelationship#Insert
GO

CREATE PROCEDURE demo.BusinessUnitRelationship#Insert 
	-- Add the parameters for the stored procedure here
	@GroupId int,
	@BusinessUnitId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF NOT EXISTS(SELECT * FROM demo.BusinessUnitRelationship WHERE ParentId = @GroupId AND BusinessUnitId = @BusinessUnitId)
	BEGIN
		INSERT INTO demo.BusinessUnitRelationship(ParentId, BusinessUnitId)
		VALUES (@GroupId, @BusinessUnitId)
	END

END

GO

grant execute on demo.BusinessUnitRelationship#Insert to MerchandisingUser
go
