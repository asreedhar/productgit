USE [Merchandising]
GO

/****** Object:  StoredProcedure [demo].[BusinessUnit#FetchFromUsername]    Script Date: 08/01/2013 14:28:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[demo].[BusinessUnit#FetchFromUsername]') AND type in (N'P', N'PC'))
DROP PROCEDURE [demo].[BusinessUnit#FetchFromUsername]
GO

USE [Merchandising]
GO

/****** Object:  StoredProcedure [demo].[BusinessUnit#FetchFromUsername]    Script Date: 08/01/2013 14:28:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [demo].[BusinessUnit#FetchFromUsername] 
	-- Add the parameters for the stored procedure here
	@UserName nvarchar(max) = '',
	@ApplicationName nvarchar(MAX) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @ApplicationId uniqueidentifier;
	DECLARE @UserId uniqueidentifier;
	
	SELECT @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE ApplicationName = @ApplicationName;
	SELECT @UserId = UserId FROM dbo.aspnet_Users WHERE LoweredUserName = @UserName AND ApplicationId = @ApplicationId;

	;WITH Ids AS (
		SELECT 
			demo.GetProfilePropertyValue('DemoBusinessUnitId', PropertyNames, PropertyValuesString) AS DemoBusinessUnitId
		FROM aspnet_Profile p
		JOIN aspnet_Users u on u.UserId = p.UserId
		WHERE u.UserName = @UserName
		AND ApplicationId = @ApplicationId
	)
	SELECT B.[BusinessUnitID]
		  ,[BusinessUnitTypeID]
		  ,[BusinessUnit]
		  ,[BusinessUnitCode]
		  ,[Active]
	FROM [Merchandising].[demo].[BusinessUnit] B
	JOIN Ids i ON B.BusinessUnitID = i.DemoBusinessUnitId

END


GO


