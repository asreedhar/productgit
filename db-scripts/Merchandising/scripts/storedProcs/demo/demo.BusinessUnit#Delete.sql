SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-07-18
-- Description:	Delete Business Units -- But you
--				should deactivate instead
-- =============================================
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'demo.BusinessUnit#Delete') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE demo.BusinessUnit#Delete
GO

CREATE PROCEDURE demo.BusinessUnit#Delete 
	@BusinessUnitId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DELETE FROM demo.BusinessUnit
	WHERE BusinessUnitID = @BusinessUnitId

END
GO

grant execute on demo.BusinessUnit#Delete to MerchandisingUser
go

