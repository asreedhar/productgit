USE [Merchandising]
GO

/****** Object:  StoredProcedure demo.BusinessUnitRelationship#Delete    Script Date: 07/17/2013 14:18:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-07-17
-- Description:	Delete BU relationship
-- =============================================
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'demo.BusinessUnitRelationship#Delete') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE demo.BusinessUnitRelationship#Delete
GO

CREATE PROCEDURE demo.BusinessUnitRelationship#Delete 
	-- Add the parameters for the stored procedure here
	@GroupId int,
	@BusinessUnitId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DELETE FROM demo.BusinessUnitRelationship
	WHERE ParentID = @GroupId
	AND BusinessUnitID = @BusinessUnitId

END

GO

grant execute on demo.BusinessUnitRelationship#Delete to MerchandisingUser
go
