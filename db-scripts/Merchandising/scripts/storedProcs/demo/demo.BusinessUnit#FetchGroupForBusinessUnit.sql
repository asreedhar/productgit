USE [Merchandising]
GO

/****** Object:  StoredProcedure demo.BusinessUnit#FetchGroupForBusinessUnit    Script Date: 07/17/2013 14:18:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-07-17
-- Description:	Get Demo Business Unit Group 
--				from a BusinessUnitId
-- =============================================
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'demo.BusinessUnit#FetchGroupForBusinessUnit') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE demo.BusinessUnit#FetchGroupForBusinessUnit
GO

CREATE PROCEDURE demo.BusinessUnit#FetchGroupForBusinessUnit 
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT B.[BusinessUnitID]
		  ,[BusinessUnitTypeID]
		  ,[BusinessUnit]
		  ,[BusinessUnitCode]
		  ,[Active]
	FROM [Merchandising].[demo].[BusinessUnit] B
	JOIN [Merchandising].[demo].[BusinessUnitRelationship] BR ON BR.ParentID = B.BusinessUnitID
	WHERE BR.BusinessUnitID = @BusinessUnitId

END

GO

grant execute on demo.BusinessUnit#FetchGroupForBusinessUnit to MerchandisingUser
go
