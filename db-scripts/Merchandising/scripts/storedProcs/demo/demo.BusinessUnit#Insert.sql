SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-07-18
-- Description:	Insert Demo Business Units
-- =============================================
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'demo.BusinessUnit#Insert') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE demo.BusinessUnit#Insert
GO

CREATE PROCEDURE demo.BusinessUnit#Insert 
	-- Add the parameters for the stored procedure here
	@BusinessUnitName nvarchar(40) = '', 
	@BusinessUnitTypeId int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @BusinessUnitCode varchar(20);

    -- Insert statements for procedure here
    IF(@BusinessUnitName <> '')
    BEGIN
		SET @BusinessUnitCode = demo.GetBusinessUnitCode(@BusinessUnitName);
		
		INSERT INTO demo.BusinessUnit( BusinessUnit, BusinessUnitCode, BusinessUnitTypeID)
			VALUES ( @BusinessUnitName, @BusinessUnitCode, @BusinessUnitTypeId)

	END
	
	SELECT [BusinessUnitID], [BusinessUnitTypeID], [BusinessUnit], [BusinessUnitCode], [Active]
	FROM demo.BusinessUnit
	WHERE BusinessUnitCode = @BusinessUnitCode
	
END
GO

grant execute on demo.BusinessUnit#Insert to MerchandisingUser
go

