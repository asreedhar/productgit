USE [Merchandising]
GO

/****** Object:  StoredProcedure demo.BusinessUnit#FetchGroupMembers    Script Date: 07/17/2013 14:18:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-07-17
-- Description:	Get Demo BusinessUnit from buid
-- =============================================
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'demo.BusinessUnit#FetchGroupMembers') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE demo.BusinessUnit#FetchGroupMembers
GO

CREATE PROCEDURE demo.BusinessUnit#FetchGroupMembers 
	-- Add the parameters for the stored procedure here
	@GroupId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT B.[BusinessUnitID]
		  ,[BusinessUnitTypeID]
		  ,[BusinessUnit]
		  ,[BusinessUnitCode]
		  ,[Active]
	FROM [Merchandising].[demo].[BusinessUnit] B
	JOIN [Merchandising].[demo].[BusinessUnitRelationship] BR ON BR.BusinessUnitId = B.BusinessUnitID
	WHERE BR.ParentId = @GroupId

END

GO

grant execute on demo.BusinessUnit#FetchGroupMembers to MerchandisingUser
go
