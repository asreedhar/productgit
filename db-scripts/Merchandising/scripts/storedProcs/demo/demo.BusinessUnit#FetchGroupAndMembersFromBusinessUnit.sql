USE [Merchandising]
GO

/****** Object:  StoredProcedure demo.BusinessUnit#FetchGroupAndMembers    Script Date: 07/17/2013 14:18:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-07-17
-- Description:	Get a Demo Group and all the 
--				Business Units from a BUID
-- =============================================
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'demo.BusinessUnit#FetchGroupAndMembersFromBusinessUnit') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE demo.BusinessUnit#FetchGroupAndMembersFromBusinessUnit
GO

CREATE PROCEDURE demo.BusinessUnit#FetchGroupAndMembersFromBusinessUnit 
	@BusinessUnitId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @GroupId int;
	SELECT @GroupId = ParentId FROM [Merchandising].[demo].[BusinessUnitRelationship]
	WHERE BusinessUnitID = @BusinessUnitId;
	
	SELECT B.[BusinessUnitID], [BusinessUnitTypeID], [BusinessUnit], [BusinessUnitCode], [Active]
	FROM [Merchandising].[demo].[BusinessUnit] B
	WHERE B.BusinessUnitID = @GroupId
	
	UNION ALL
	
	SELECT B.[BusinessUnitID], [BusinessUnitTypeID], [BusinessUnit], [BusinessUnitCode], [Active]
	FROM [Merchandising].[demo].[BusinessUnit] B
	JOIN [Merchandising].[demo].[BusinessUnitRelationship] BR ON BR.BusinessUnitId = B.BusinessUnitID
	WHERE BR.ParentId = @GroupId
	
	ORDER BY BusinessUnitTypeID
	
END

GO

grant execute on demo.BusinessUnit#FetchGroupAndMembersFromBusinessUnit to MerchandisingUser
go
