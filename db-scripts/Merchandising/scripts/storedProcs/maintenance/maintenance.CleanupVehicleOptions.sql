IF OBJECT_ID('[maintenance].[CleanupVehicleOptions]', 'P') IS NOT NULL
    DROP PROCEDURE [maintenance].[CleanupVehicleOptions]
GO
CREATE  PROCEDURE [maintenance].[CleanupVehicleOptions]
    @QuitRunningAt DATETIME2
AS
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
    DECLARE @rowAffected AS INTEGER = 1
      , @MaxRows INTEGER= 1000;
	--DECLARE  @QuitRunningAt DATETIME2; SET @QuitRunningAt=DATEADD(m,10,GETDATE())
	
    IF OBJECT_ID('tempdb..#Deletable') IS NOT NULL
        DROP TABLE #Deletable
    IF OBJECT_ID('tempdb..#Worktable') IS NOT NULL
        DROP TABLE #Worktable

    CREATE TABLE #Deletable
        (
          BusinessUnitId INT NOT NULL
        , Processed INTEGER DEFAULT ( 0 )
        , InventoryId INT NOT NULL
        , PRIMARY KEY ( Processed, BusinessUnitId, InventoryId ) WITH ( IGNORE_DUP_KEY = ON )
        )
    CREATE TABLE #Worktable
        (
          BusinessUnitId INT NOT NULL
        , InventoryId INT NOT NULL
        , PRIMARY KEY ( BusinessUnitId, InventoryId )
        )
	
	/*
		Stage Items for builder.Descriptions  
		and builder.descriptionContents (because of FK to builder.Descriptions)
	*/
    INSERT  #Deletable
            ( BusinessUnitId
            , InventoryId
            , Processed
            )
            SELECT  [a].[businessUnitID]
                  , [a].[inventoryId]
                  , Proccessed = 0
            FROM    [builder].[Descriptions] a
                    LEFT OUTER JOIN [FLDW].[dbo].[InventoryActive] i ON [i].[BusinessUnitID] = [a].[businessUnitID]
                                                                        AND [i].[InventoryID] = [a].[inventoryId]
            WHERE   [i].[InventoryID] IS NULL
  
	--Process builder.descriptionContents, done first due to FK
    WHILE EXISTS ( SELECT   1
                   FROM     #Deletable
                   WHERE    Processed = 0 )
        AND GETDATE() < @QuitRunningAt
        BEGIN
            INSERT  #Worktable
                    ( BusinessUnitId
                    , InventoryId
                    )
                    SELECT TOP ( @MaxRows )
                            BusinessUnitId
                          , InventoryId
                    FROM    #Deletable
                    WHERE   Processed = 0

            DELETE  dc
            FROM    [builder].[descriptionContents] dc
                    INNER JOIN #Worktable wt ON [dc].[businessUnitId] = [wt].[BusinessUnitId]
                                                AND [dc].[inventoryId] = [wt].[InventoryId]
		      
            UPDATE  d
            SET     [d].[Processed] = 1
            FROM    #Deletable d
                    INNER JOIN #Worktable w ON [d].[InventoryId] = [w].[InventoryId]
                                               AND [d].[BusinessUnitId] = [w].[BusinessUnitId]

            TRUNCATE TABLE #Worktable
        END 

	--Process builder.Descriptions
    WHILE EXISTS ( SELECT   1
                   FROM     #Deletable
                   WHERE    Processed != 2 )
        AND GETDATE() < @QuitRunningAt
        BEGIN
            INSERT  #Worktable
                    ( BusinessUnitId
                    , InventoryId
                    )
                    SELECT TOP ( @MaxRows )
                            BusinessUnitId
                          , InventoryId
                    FROM    #Deletable
                    WHERE   Processed = 1

            DELETE  dc
            FROM    [builder].[Descriptions] dc
                    INNER JOIN #Worktable wt ON [dc].[businessUnitID] = [wt].[BusinessUnitId]
                                                AND [dc].[inventoryId] = [wt].[InventoryId]
		     
            UPDATE  d
            SET     [d].[Processed] = 2
            FROM    #Deletable d
                    INNER JOIN #Worktable w ON [d].[InventoryId] = [w].[InventoryId]
                                               AND [d].[BusinessUnitId] = [w].[BusinessUnitId]
		
            TRUNCATE TABLE #Worktable
        END 


	/*
		Stage Itmes for audit.Descriptions 
		Distinct required because root table does not have
		PK on BUID,InvId

		IF there was a FK between audit and builder, we could use same tables,
		but there is no guarantee with foreign key!
	*/
    TRUNCATE TABLE #Deletable
    INSERT  #Deletable
            ( BusinessUnitId
            , InventoryId
            , Processed
            )
            SELECT  [a].[businessUnitID]
                  , [a].[inventoryId]
                  , Proccessed = 0
            FROM    [audit].[Descriptions] a
                    LEFT OUTER JOIN [FLDW].[dbo].[InventoryActive] i ON [i].[BusinessUnitID] = [a].[businessUnitID]
                                                                        AND [i].[InventoryID] = [a].[inventoryId]
            WHERE   [i].[InventoryID] IS NULL
   
	--Process audit.Descriptions
    WHILE EXISTS ( SELECT   1
                   FROM     #Deletable
                   WHERE    Processed = 0 )
        AND GETDATE() < @QuitRunningAt
        BEGIN
            INSERT  #Worktable
                    ( BusinessUnitId
                    , InventoryId
                    )
                    SELECT TOP ( @MaxRows )
                            BusinessUnitId
                          , InventoryId
                    FROM    #Deletable
                    WHERE   Processed = 0

            DELETE  d
            FROM    [audit].[Descriptions] d
                    INNER JOIN #Worktable wt ON [d].[businessUnitID] = [wt].[BusinessUnitId]
                                                AND [d].[inventoryId] = [wt].[InventoryId]
		      
            UPDATE  d
            SET     [d].[Processed] = 1
            FROM    #Deletable d
                    INNER JOIN #Worktable w ON [d].[InventoryId] = [w].[InventoryId]
                                               AND [d].[BusinessUnitId] = [w].[BusinessUnitId]

            TRUNCATE TABLE #Worktable
        END 
	
	/*
		Stage Itmes for [postings].[Publications]
		Distinct required because root table does not have
		PK on BUID,InvId

		IF there was a FK between postings.Publications 
		and builder.descriptions, we could use same tables,
		but there is no guarantee with foreign key!
	*/
    TRUNCATE TABLE #Deletable
    INSERT  #Deletable
            ( BusinessUnitId
            , InventoryId
            , Processed
            )
            SELECT  [a].[businessUnitID]
                  , [a].[inventoryId]
                  , Proccessed = 0
            FROM    [postings].[Publications] a
                    LEFT OUTER JOIN [FLDW].[dbo].[InventoryActive] i ON [i].[BusinessUnitID] = [a].[businessUnitID]
                                                                        AND [i].[InventoryID] = [a].[inventoryId]
            WHERE   [i].[InventoryID] IS NULL
   
	--Process postings.Publications
    WHILE EXISTS ( SELECT   1
                   FROM     #Deletable
                   WHERE    Processed = 0 )
        AND GETDATE() < @QuitRunningAt
        BEGIN
            INSERT  #Worktable
                    ( BusinessUnitId
                    , InventoryId
                    )
                    SELECT TOP ( @MaxRows )
                            BusinessUnitId
                          , InventoryId
                    FROM    #Deletable
                    WHERE   Processed = 0

            DELETE  d
            FROM    [postings].[Publications] d
                    INNER JOIN #Worktable wt ON [d].[businessUnitID] = [wt].[BusinessUnitId]
                                                AND [d].[inventoryId] = [wt].[InventoryId]
		      
            UPDATE  d
            SET     [d].[Processed] = 1
            FROM    #Deletable d
                    INNER JOIN #Worktable w ON [d].[InventoryId] = [w].[InventoryId]
                                               AND [d].[BusinessUnitId] = [w].[BusinessUnitId]

            TRUNCATE TABLE #Worktable
        END 

    TRUNCATE TABLE #Deletable
    INSERT  #Deletable
            ( BusinessUnitId
            , InventoryId
            , Processed
            )
            SELECT  [a].[businessUnitID]
                  , [a].[inventoryId]
                  , Proccessed = 0
            FROM    [postings].[VehicleAdScheduling] a
                    LEFT OUTER JOIN [FLDW].[dbo].[InventoryActive] i ON [i].[BusinessUnitID] = [a].[businessUnitID]
                                                                        AND [i].[InventoryID] = [a].[inventoryId]
            WHERE   [i].[InventoryID] IS NULL
   
	--Process [postings].[VehicleAdScheduling]
    WHILE EXISTS ( SELECT   1
                   FROM     #Deletable
                   WHERE    Processed = 0 )
        AND GETDATE() < @QuitRunningAt
        BEGIN
            INSERT  #Worktable
                    ( BusinessUnitId
                    , InventoryId
                    )
                    SELECT TOP ( @MaxRows )
                            BusinessUnitId
                          , InventoryId
                    FROM    #Deletable
                    WHERE   Processed = 0

            DELETE  d
            FROM    [postings].[VehicleAdScheduling] d
                    INNER JOIN #Worktable wt ON [d].[businessUnitID] = [wt].[BusinessUnitId]
                                                AND [d].[inventoryId] = [wt].[InventoryId]
		      
            UPDATE  d
            SET     [d].[Processed] = 1
            FROM    #Deletable d
                    INNER JOIN #Worktable w ON [d].[InventoryId] = [w].[InventoryId]
                                               AND [d].[BusinessUnitId] = [w].[BusinessUnitId]

            TRUNCATE TABLE #Worktable
        END 
	
	/*
		Stage Item for builder.Advertisement
	*/
    TRUNCATE TABLE #Deletable
    INSERT  #Deletable
            ( BusinessUnitId
            , InventoryId
            , Processed
            )
            SELECT  [a].[businessUnitId]
                  , [a].[inventoryId]
                  , Proccessed = 0
            FROM    [builder].[Advertisement] a
                    LEFT OUTER JOIN [FLDW].[dbo].[InventoryActive] i ON [i].[BusinessUnitID] = [a].[businessUnitId]
                                                                        AND [i].[InventoryID] = [a].[inventoryId]
            WHERE   [i].[InventoryID] IS NULL
   
	--Process builder.Advertisement
    WHILE EXISTS ( SELECT   1
                   FROM     #Deletable
                   WHERE    Processed = 0 )
        AND GETDATE() < @QuitRunningAt
        BEGIN
            INSERT  #Worktable
                    ( BusinessUnitId
                    , InventoryId
                    )
                    SELECT TOP ( @MaxRows )
                            BusinessUnitId
                          , InventoryId
                    FROM    #Deletable
                    WHERE   Processed = 0

            DELETE  a
            FROM    [builder].[Advertisement] a
                    INNER JOIN #Worktable wt ON [a].[businessUnitId] = [wt].[BusinessUnitId]
                                                AND [a].[inventoryId] = [wt].[InventoryId]
		      
            UPDATE  d
            SET     [d].[Processed] = 1
            FROM    #Deletable d
                    INNER JOIN #Worktable w ON [d].[InventoryId] = [w].[InventoryId]
                                               AND [d].[BusinessUnitId] = [w].[BusinessUnitId]

            TRUNCATE TABLE #Worktable
        END 
		
	/*
		Stage Item for merchandising.AutoTraderVehSummary
	*/
    TRUNCATE TABLE #Deletable
    INSERT  #Deletable
            ( BusinessUnitId
            , InventoryId
            , Processed
            )
            SELECT  [a].[BusinessUnitID]
                  , [a].[InventoryId]
                  , Proccessed = 0
            FROM    [merchandising].[AutoTraderVehSummary] a
                    LEFT OUTER JOIN [FLDW].[dbo].[InventoryActive] i ON [i].[BusinessUnitID] = [a].[BusinessUnitID]
                                                                        AND [i].[InventoryID] = [a].[InventoryId]
            WHERE   [i].[InventoryID] IS NULL
   
	--Process merchandising.AutoTraderVehSummary
    WHILE EXISTS ( SELECT   1
                   FROM     #Deletable
                   WHERE    Processed = 0 )
        AND GETDATE() < @QuitRunningAt
        BEGIN
            INSERT  #Worktable
                    ( BusinessUnitId
                    , InventoryId
                    )
                    SELECT TOP ( @MaxRows )
                            BusinessUnitId
                          , InventoryId
                    FROM    #Deletable
                    WHERE   Processed = 0

            DELETE  a
            FROM    [merchandising].[AutoTraderVehSummary] a
                    INNER JOIN #Worktable wt ON [a].[BusinessUnitID] = [wt].[BusinessUnitId]
                                                AND [a].[InventoryId] = [wt].[InventoryId]
		      
            UPDATE  d
            SET     [d].[Processed] = 1
            FROM    #Deletable d
                    INNER JOIN #Worktable w ON [d].[InventoryId] = [w].[InventoryId]
                                               AND [d].[BusinessUnitId] = [w].[BusinessUnitId]

            TRUNCATE TABLE #Worktable
        END 

	/*
		Stage Item for audit.VehicleOptions
		
		Distinct required because root table does not have
		PK on BUID,InvId
	*/
    TRUNCATE TABLE #Deletable
    INSERT  #Deletable
            ( BusinessUnitId
            , InventoryId
            , Processed
            )
            SELECT  [a].[businessUnitID]
                  , [a].[inventoryID]
                  , Proccessed = 0
            FROM    [audit].[VehicleOptions] a
                    LEFT OUTER JOIN [FLDW].[dbo].[InventoryActive] i ON [i].[BusinessUnitID] = [a].[businessUnitID]
                                                                        AND [i].[InventoryID] = [a].[inventoryID]
            WHERE   [i].[InventoryID] IS NULL
   
	--Process audit.VehicleOptions
    WHILE EXISTS ( SELECT   1
                   FROM     #Deletable
                   WHERE    Processed = 0 )
        AND GETDATE() < @QuitRunningAt
        BEGIN
            INSERT  #Worktable
                    ( BusinessUnitId
                    , InventoryId
                    )
                    SELECT TOP ( @MaxRows )
                            BusinessUnitId
                          , InventoryId
                    FROM    #Deletable
                    WHERE   Processed = 0

            DELETE  vo
            FROM    [audit].[VehicleOptions] vo
                    INNER JOIN #Worktable wt ON [vo].[inventoryID] = [wt].[InventoryId]
		      
            UPDATE  d
            SET     [d].[Processed] = 1
            FROM    #Deletable d
                    INNER JOIN #Worktable w ON [d].[InventoryId] = [w].[InventoryId]
                                               AND [d].[BusinessUnitId] = [w].[BusinessUnitId]

            TRUNCATE TABLE #Worktable
        END 

    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED


GO


