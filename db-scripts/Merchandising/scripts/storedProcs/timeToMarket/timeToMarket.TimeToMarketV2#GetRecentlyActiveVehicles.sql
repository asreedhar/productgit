-- ======================================================
-- Author:		Travis Huber
-- Create date: 2014-07-14
-- Description:	Get a list of recently active vehicles
--				for BusinessUnit @BusinessUnitId
--				that were inserted after @StartDate
-- ======================================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[timeToMarket].[TimeToMarketV2#GetRecentlyActiveVehicles]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE timeToMarket.TimeToMarketV2#GetRecentlyActiveVehicles
GO

CREATE PROCEDURE [timeToMarket].[TimeToMarketV2#GetRecentlyActiveVehicles] 
	@BusinessUnitId int, 
	@EarliestInventoryReceivedDate DateTime,
	@RecentlyActiveDays tinyint = 45,
	@IncludeDoNotPost bit = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		inv.BusinessUnitID, inv.InventoryID, inv.StockNumber, veh.VIN, CAST(inv.InventoryType AS int) AS InventoryType, CAST(inv.InventoryActive AS bit) AS InventoryActive, inv.DeleteDt,
		inv.InventoryReceivedDate, ttmt.Inserted, ttmt.Updated, ttmt.GIDFirstDate, 
		
		ttmt.PhotosFirstDate, 
		ttmt.DescriptionFirstDate, 
		ttmt.PriceFirstDate, 
		ttmt.MinimumPhotosFirstDate,
		
		ttmt.MAXMinimumPhotosFirstDate, 
		ttmt.MAXPhotosFirstDate, 
		ttmt.MAXPriceFirstDate,
		ttmt.MAXDescriptionFirstDate,
		
		ttmt.AdApprovalFirstDate, ttmt.CompleteAdFirstDate
	FROM
		IMT.dbo.Inventory inv
		LEFT JOIN Merchandising.dashboard.TimeToMarketTracking ttmt ON inv.BusinessUnitID = ttmt.BusinessUnitId AND inv.InventoryID = ttmt.InventoryId
		INNER JOIN IMT.dbo.Vehicle veh ON inv.VehicleID = veh.VehicleID
		LEFT JOIN Merchandising.builder.OptionsConfiguration oc ON oc.businessUnitID = inv.BusinessUnitID AND oc.inventoryId = inv.InventoryID

	WHERE
		inv.BusinessUnitID = @BusinessUnitId
		AND inv.InventoryReceivedDate > @EarliestInventoryReceivedDate 
		AND (
			@IncludeDoNotPost = 1 OR
			(oc.doNotPostFlag = 0 OR oc.doNotPostFlag IS NULL)
		)
		AND (	
			inv.InventoryActive = 1 OR 
			inv.DeleteDt > DATEADD(dd, (0 - @RecentlyActiveDays), CAST(GETDATE() AS DATE))
		)
		-- NULL complete dates or complete ads after the adstatus date (4/1/2014) are fair game.
		AND (ISNULL(ttmt.CompleteAdFirstDate, CAST('2014-04-01' AS DATE)) > '2014-03-31')
END

grant execute on timeToMarket.TimeToMarketV2#GetRecentlyActiveVehicles to MerchandisingUser

GO


