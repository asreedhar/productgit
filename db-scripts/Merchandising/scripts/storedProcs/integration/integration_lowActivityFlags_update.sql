 if exists (select * from dbo.sysobjects where id = object_id(N'[integration].[LowActivityFlags#Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [integration].[LowActivityFlags#Update]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <4/24/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: integration_lowActivityFlags_update.sql,v 1.3 2009/06/22 17:22:10 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * update the low activity flags for vehicles
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [integration].[LowActivityFlags#Update]

	
AS
BEGIN TRY

	SET NOCOUNT ON;
	
 	--THIS SCRIPT UPDATES THE LOW ACTIVITY FLAG FOR ALL VEHICLES WHERE THERE IS CURRENT DATA
 	
 	--cases: 1) not enough searches
 	--2) enough searches to have a clickthru count, and clickthru too low
 	--3)Negative trend developing in search volume or clickthru ratio
	UPDATE 	oc 
	SET 	oc.lowActivityFlag = 
			CASE WHEN
				(rw.dayRecordsInAggregate > 5 AND  --need a min number of days to collect...
					(
						(
							rw.sumSearchViewed/rw.dayRecordsInAggregate < sm.minWeeklySearchCount/7 
						) 
						OR 
						(
							rw.sumSearchViewed > 200 AND --needs a min number of searches for clickthru to matter
							rw.clickThruRatio < sm.minWeeklyClickthruRatio
						)
						OR
						(
							cast(rw.sumSearchViewed as real)/rw.dayRecordsInAggregate <= 0.6*cast(pp.sumSearchViewed as real)/pp.dayRecordsInAggregate 
							OR rw.clickThruRatio <= 0.6*pp.clickThruRatio
						)
					)
				)
			THEN 1 
			ELSE 0 
			END
	FROM 	builder.optionsConfiguration oc
	INNER JOIN
		postings.RollingWeekVehicleMetrics rw
		ON rw.businessUnitId = oc.businessUnitId
		AND rw.inventoryId = oc.inventoryId 
	LEFT JOIN postings.PastPerformance pp
		ON pp.businessUnitId = rw.businessUnitId
		AND pp.inventoryId = rw.inventoryId
	INNER JOIN 
		settings.merchandising sm
		ON sm.businessUnitId = rw.businessUnitId
	
			
END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler

END CATCH
GO

GRANT EXECUTE ON [integration].[LowActivityFlags#Update] TO MerchandisingUser 
GO

