USE [Merchandising]
go

if object_id('integration.UpdateFetchAutoTraderRoundRobinDateQueued') is not null
	drop procedure integration.UpdateFetchAutoTraderRoundRobinDateQueued
go


create procedure integration.UpdateFetchAutoTraderRoundRobinDateQueued
	@RequestTypeId int,
	@BusinessUnitId int,
	@ReportMonth datetime,
	@DateQueued datetime
as

set transaction isolation level read uncommitted
set nocount on

-- update the DateQueued ONLY if there has been an actual request sent
-- after the last queued date...

update integration.FetchAutoTraderRoundRobin set
	DateQueued = @DateQueued
where
	RequestTypeId = @RequestTypeId
	and BusinessUnitId = @BusinessUnitId
	and ReportMonth = @ReportMonth
	and isnull(DateLastRequested, '1900-01-01') > DateQueued

set transaction isolation level read committed

go