if exists (select * from dbo.sysobjects where id = object_id(N'[integration].[CampaignEvents#Run]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [integration].[CampaignEvents#Run]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <4/24/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: integration_campaignEvents_run.sql,v 1.1 2009/11/09 17:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * loads the lot provider information from available data sources
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [integration].[CampaignEvents#Run]

	
AS
BEGIN TRY
	
	SET NOCOUNT ON;
	
	DECLARE @Step		VARCHAR(250),
		@ProcName	VARCHAR(128),
		@ParentLogID	INT
	
	SET @ProcName = OBJECT_NAME(@@PROCID)
	SET @Step = 'Started'

	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @ParentLogID OUTPUT
 
	----------------------------------------------------------------------------------------
	SET @Step = 'DECLARE VARIABLES'
	----------------------------------------------------------------------------------------
	
	declare @dealerCtr int, @dealerCt int,
			@CurrentBuid int, @processName varchar(10)
	set @processName = 'campaignProcessor'
	
	declare @dealerTbl table (
		RowId int IDENTITY(1,1),
		BusinessUnitId int NOT NULL		
	)
	
	----------------------------------------------------------------------------------------
	SET @Step = 'FETCH PARTICIPATING DEALERS'
	----------------------------------------------------------------------------------------
	
	--GET DEALERS WITH A LOT PROVIDER IN THE Merchandising.SETTINGS.Merchandising table
	INSERT INTO @dealerTbl (businessUnitId)
	SELECT DISTINCT businessUnitId FROM settings.campaignEvents
	
	--GET DEALER LOOP VARS
	select @dealerCtr = 1, @dealerCt = count(*) FROM @dealerTbl
	
	----------------------------------------------------------------------------------------
	SET @Step = 'LOOP OVER EACH DEALER AND PROCESS EVENTS'
	----------------------------------------------------------------------------------------
	
	WHILE @dealerCtr <= @dealerCt
	BEGIN
		SELECT @CurrentBuid = businessUnitId
		FROM @dealerTbl
		WHERE rowId = @dealerCtr
		
		EXEC settings.CampaignEvents#Process @BusinessUnitId = @CurrentBuid
	
		SELECT @dealerCtr = @dealerCtr + 1
	END			
	
	SET @Step = 'Completed'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @ParentLogID
	
END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler @Message = @Step

END CATCH
GO

GRANT EXECUTE ON [integration].[CampaignEvents#Run] TO MerchandisingUser 
GO

