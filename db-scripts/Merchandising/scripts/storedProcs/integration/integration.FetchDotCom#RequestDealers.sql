if exists (select * from dbo.sysobjects where id = object_id(N'[integration].[FetchDotCom#RequestDealers]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [integration].[FetchDotCom#RequestDealers]
GO



CREATE PROCEDURE [integration].[FetchDotCom#RequestDealers] (
	@RequestTypeID tinyint
--- DESCRIPTION -----------------
-- 
-- Builds list of fetch dealers per request type
-- 
--- HISTORY -----------------
-- 
-- 11/12/2013 DGH Created
-- 
)
AS  

SET NOCOUNT ON 

declare @bucodes table(bucode varchar(20))

IF @RequestTypeID in 
(
	1  -- Auto Trader Performance Metrics
	,2 -- Auto Trader Vehicle Online
)
begin
	INSERT @bucodes( bucode )
	SELECT  
			bu.BusinessUnitCode
	FROM    Merchandising.Settings.DealerListingSites dls  
			INNER JOIN Merchandising.Settings.Merchandising m ON m.businessUnitID = dls.businessUnitID  
			INNER JOIN IMT.dbo.BusinessUnit bu ON bu.BusinessUnitID = dls.businessUnitID
	WHERE   destinationId = 2  
			AND userName <> ''  
			AND password <> ''
			AND (dls.dateLoginFailure IS NULL OR dls.dateLoginFailureCleared >= dls.dateLoginFailure)

	-- Add demo business units
	UNION ALL
	SELECT  bu.BusinessUnitCode
	FROM    Merchandising.Settings.DealerListingSites dls  
			INNER JOIN Merchandising.Settings.Merchandising m ON m.businessUnitID = dls.businessUnitID  
			INNER JOIN Merchandising.demo.BusinessUnit bu ON bu.BusinessUnitID = dls.businessUnitID
	WHERE   destinationId = 2  
			AND userName <> ''  
			AND password <> ''
			AND (dls.dateLoginFailure IS NULL OR dls.dateLoginFailureCleared >= dls.dateLoginFailure)
			
end
else if @RequestTypeID in 
(
	3  -- Cars.com metrics
	,4 -- Cars.com Vehicle online
)
begin
	INSERT @bucodes( bucode )
	SELECT  bu.BusinessUnitCode
	FROM    Merchandising.Settings.DealerListingSites dls  
			INNER JOIN Merchandising.Settings.Merchandising m ON m.businessUnitID = dls.businessUnitID  
			INNER JOIN IMT.dbo.BusinessUnit bu ON bu.BusinessUnitID = dls.businessUnitID
	WHERE   destinationId = 1
			AND userName <> ''  
			AND password <> ''
			AND (dls.dateLoginFailure IS NULL OR dls.dateLoginFailureCleared >= dls.dateLoginFailure)
	
	-- Add demo business units
	UNION ALL
	SELECT  bu.BusinessUnitCode
	FROM    Merchandising.Settings.DealerListingSites dls  
			INNER JOIN Merchandising.Settings.Merchandising m ON m.businessUnitID = dls.businessUnitID  
			INNER JOIN Merchandising.demo.BusinessUnit bu ON bu.BusinessUnitID = dls.businessUnitID
	WHERE   destinationId = 1
			AND userName <> ''  
			AND password <> ''
			AND (dls.dateLoginFailure IS NULL OR dls.dateLoginFailureCleared >= dls.dateLoginFailure)

end

select Dealer = bucode 
from @bucodes

SET NOCOUNT OFF

GO
