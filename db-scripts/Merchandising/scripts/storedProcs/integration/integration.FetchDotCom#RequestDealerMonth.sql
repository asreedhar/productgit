if exists (select * from dbo.sysobjects where id = object_id(N'[integration].[FetchDotCom#RequestDealerMonth]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [integration].[FetchDotCom#RequestDealerMonth]
GO



CREATE PROCEDURE [integration].[FetchDotCom#RequestDealerMonth] (
	@RequestTypeID tinyint
--- DESCRIPTION -----------------
-- 
-- Builds list of months to download for each dealer
-- 
--- HISTORY -----------------
-- 
-- 03/01/2011 DMW Created
-- 
)
AS  

SET NOCOUNT ON 

DECLARE @Months TABLE ([Month] datetime)
declare
	@lastYear datetime,
	@Month datetime


select	
	@lastYear = dateadd(year, -1, getdate()),
	@month = dateadd(day, datediff(day, 0, @lastYear) - datepart(day, @lastYear), 1)
	
	
WHILE @Month < GETDATE()
BEGIN
	INSERT @Months
	VALUES (@Month)
	
	SET @Month = DATEADD(MONTH,1,@Month)
END


IF @RequestTypeID = 1 -- Auto Trader Performance Metrics
BEGIN
	SELECT  dls.businessUnitID,  
			bu.BusinessUnitCode,  
			userName = ltrim(rtrim(dls.userName)),  
			dls.password,
			[Month],
			dls.ListingSiteDealerId
	FROM    Merchandising.Settings.DealerListingSites dls  
			INNER JOIN Merchandising.Settings.Merchandising m ON m.businessUnitID = dls.businessUnitID  
			INNER JOIN IMT.dbo.BusinessUnit bu ON bu.BusinessUnitID = dls.businessUnitID
			CROSS JOIN @Months
	WHERE   destinationId = 2  
			AND userName <> ''  
			AND password <> ''
			AND (dls.dateLoginFailure IS NULL OR dls.dateLoginFailureCleared >= dls.dateLoginFailure)

	-- Add demo business units
	UNION ALL
	SELECT  dls.businessUnitID,  
			bu.BusinessUnitCode,  
			userName = ltrim(rtrim(dls.userName)),  
			dls.password,
			[Month],
			dls.ListingSiteDealerId
	FROM    Merchandising.Settings.DealerListingSites dls  
			INNER JOIN Merchandising.Settings.Merchandising m ON m.businessUnitID = dls.businessUnitID  
			INNER JOIN Merchandising.demo.BusinessUnit bu ON bu.BusinessUnitID = dls.businessUnitID
			CROSS JOIN @Months
	WHERE   destinationId = 2  
			AND userName <> ''  
			AND password <> ''
			AND (dls.dateLoginFailure IS NULL OR dls.dateLoginFailureCleared >= dls.dateLoginFailure)
	order by BusinessUnitID, Month
			
end
ELSE if @RequestTypeID = 2 -- Auto Trader Vehicle Online
BEGIN
	SELECT  dls.businessUnitID,  
			bu.BusinessUnitCode,  
			userName = ltrim(rtrim(dls.userName)),  
			dls.password,
			NULL [Month],
			dls.ListingSiteDealerId
	FROM    Merchandising.Settings.DealerListingSites dls  
			INNER JOIN Merchandising.Settings.Merchandising m ON m.businessUnitID = dls.businessUnitID  
			INNER JOIN IMT.dbo.BusinessUnit bu ON bu.BusinessUnitID = dls.businessUnitID
	WHERE   destinationId = 2  
			AND userName <> ''  
			AND password <> ''
			AND (dls.dateLoginFailure IS NULL OR dls.dateLoginFailureCleared > dls.dateLoginFailure)
	-- Add demo business units
	UNION ALL
	SELECT  dls.businessUnitID,  
			bu.BusinessUnitCode,  
			userName = ltrim(rtrim(dls.userName)),  
			dls.password,
			NULL [Month],
			dls.ListingSiteDealerId
	FROM    Merchandising.Settings.DealerListingSites dls  
			INNER JOIN Merchandising.Settings.Merchandising m ON m.businessUnitID = dls.businessUnitID  
			INNER JOIN Merchandising.demo.BusinessUnit bu ON bu.BusinessUnitID = dls.businessUnitID
	WHERE   destinationId = 2  
			AND userName <> ''  
			AND password <> ''
			AND (dls.dateLoginFailure IS NULL OR dls.dateLoginFailureCleared > dls.dateLoginFailure)
	order by BusinessUnitID, Month

end
else if @RequestTypeID = 3 -- Cars.com metrics
begin

	SELECT  dls.businessUnitID,  
			bu.BusinessUnitCode,  
			userName = ltrim(rtrim(dls.userName)),  
			dls.password,
			[Month],
			dls.ListingSiteDealerId
	FROM    Merchandising.Settings.DealerListingSites dls  
			INNER JOIN Merchandising.Settings.Merchandising m ON m.businessUnitID = dls.businessUnitID  
			INNER JOIN IMT.dbo.BusinessUnit bu ON bu.BusinessUnitID = dls.businessUnitID
			CROSS JOIN @Months
	WHERE   destinationId = 1
			AND userName <> ''  
			AND password <> ''
			AND (dls.dateLoginFailure IS NULL OR dls.dateLoginFailureCleared >= dls.dateLoginFailure)
	
	-- Add demo business units
	UNION ALL
	SELECT  dls.businessUnitID,  
			bu.BusinessUnitCode,  
			userName = ltrim(rtrim(dls.userName)),  
			dls.password,
			[Month],
			dls.ListingSiteDealerId
	FROM    Merchandising.Settings.DealerListingSites dls  
			INNER JOIN Merchandising.Settings.Merchandising m ON m.businessUnitID = dls.businessUnitID  
			INNER JOIN Merchandising.demo.BusinessUnit bu ON bu.BusinessUnitID = dls.businessUnitID
			CROSS JOIN @Months
	WHERE   destinationId = 1
			AND userName <> ''  
			AND password <> ''
			AND (dls.dateLoginFailure IS NULL OR dls.dateLoginFailureCleared >= dls.dateLoginFailure)
	order by BusinessUnitID, Month

end
else if @RequestTypeID = 4 -- Cars.com Vehicle online
begin
	SELECT  dls.businessUnitID,  
			bu.BusinessUnitCode,  
			userName = ltrim(rtrim(dls.userName)),  
			dls.password,
			NULL [Month],
			dls.ListingSiteDealerId
	FROM    Merchandising.Settings.DealerListingSites dls  
			INNER JOIN Merchandising.Settings.Merchandising m ON m.businessUnitID = dls.businessUnitID  
			INNER JOIN IMT.dbo.BusinessUnit bu ON bu.BusinessUnitID = dls.businessUnitID
	WHERE   destinationId = 1  
			AND userName <> ''  
			AND password <> ''
			AND (dls.dateLoginFailure IS NULL OR dls.dateLoginFailureCleared > dls.dateLoginFailure)
	-- Add demo business units
	UNION ALL
	SELECT  dls.businessUnitID,  
			bu.BusinessUnitCode,  
			userName = ltrim(rtrim(dls.userName)),  
			dls.password,
			NULL [Month],
			dls.ListingSiteDealerId
	FROM    Merchandising.Settings.DealerListingSites dls  
			INNER JOIN Merchandising.Settings.Merchandising m ON m.businessUnitID = dls.businessUnitID  
			INNER JOIN Merchandising.demo.BusinessUnit bu ON bu.BusinessUnitID = dls.businessUnitID
	WHERE   destinationId = 1  
			AND userName <> ''  
			AND password <> ''
			AND (dls.dateLoginFailure IS NULL OR dls.dateLoginFailureCleared > dls.dateLoginFailure)
	order by BusinessUnitID, Month

end

SET NOCOUNT OFF

GO
