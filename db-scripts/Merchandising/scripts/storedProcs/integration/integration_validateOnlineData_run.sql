 if exists (select * from dbo.sysobjects where id = object_id(N'[integration].[ValidateOnlineData#Run]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [integration].[ValidateOnlineData#Run]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <4/24/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: integration_validateOnlineData_run.sql,v 1.11 2010/01/05 17:44:33 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * loads the lot provider information from available data sources
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [integration].[ValidateOnlineData#Run]

	
AS
BEGIN TRY
	
	SET NOCOUNT ON;
	
	DECLARE @Step VARCHAR(250)
 
	/*
	PSEUDO CODE FOR Online data reconciliation
	1) get all data for active dealer publications...where publication date was earlier than yesterday (for a Wed run this equates to posting on Mon or earlier - in theory, this gives 2 nights for the ad to reach us)
		vin, inventoryid, description, mileage, price
	2) get all known market listing data for the vehicle
	3) compare price, mileage, description for vin...
		3a) if any don't match, set mismatch flags...
		3b) if any empty or zero, set empty flags
		
	--
	*/
	
	
	DELETE FROM postings.OnlineStatus
	--THIS PROCESS Collects the lot provider data and inserts options onto Wanamaker vehicles
	--Where the vehicles are unlocked and mapped options exist

	----------------------------------------------------------------------------------------
	SET @Step = 'DECLARE VARIABLES'
	----------------------------------------------------------------------------------------
	
	declare @dealerCtr int, @dealerCt int, 
		@CurrentBuid int, @processName varchar(10)
	set @processName = 'onlineupdate'
	
	declare @dealerTbl table (
		RowId int IDENTITY(1,1),
		BusinessUnitId int NOT NULL
	)
	
		
	CREATE TABLE #Inventory (
		rowId int identity(1,1),
		BusinessUnitId int NOT NULL,
		Vin varchar(17) NOT NULL,
		StockNumber varchar(15) NOT NULL,
		InventoryReceivedDate datetime NOT NULL, 
		vehicleYear int NOT NULL, 
		UnitCost decimal(9,2) NOT NULL, 
		AcquisitionPrice decimal (8,2) NOT NULL, 
		Certified tinyint NOT NULL, 
		Make varchar(20) NOT NULL, 
		model varchar(50) NOT NULL,
		VehicleTrim varchar(50) NULL,
		inventoryID int NOT NULL,
		MileageReceived int NULL, 
		TradeOrPurchase tinyint NOT NULL, 
		ListPrice decimal(8,2) NULL,
		InventoryType tinyint NOT NULL,
		InventoryStatusCD int NOT NULL,
		BaseColor varchar(100) NOT NULL,
		VehicleLocation varchar(20) NULL,
		LotPrice DECIMAL(8,2) null,
		MSRP DECIMAL(9,2) NULL,
		VehicleCatalogId int NULL,
		MerchandisingDescription varchar(max) NOT NULL Default('')
	)

			
		
	--GET DEALERS WITH A LOT PROVIDER IN THE Merchandising.SETTINGS.Merchandising table
	INSERT INTO @dealerTbl (businessUnitId)
	SELECT distinct businessUnitId
	FROM settings.Merchandising
	
	
	--GET DEALER LOOP VARS
	select @dealerCtr = 1, @dealerCt = count(*) FROM @dealerTbl
	
	WHILE @dealerCtr <= @dealerCt
	BEGIN
		--GET CURRENT DEALER/LOT PROVIDER
		select @CurrentBuid = businessUnitId
		FROM @dealerTbl
		WHERE RowId = @dealerCtr
	
		INSERT INTO #Inventory (BusinessUnitId, Vin, StockNumber, InventoryReceivedDate, vehicleYear, UnitCost, 
			AcquisitionPrice, Certified, Make, model, vehicleTrim, inventoryID, MileageReceived, TradeOrPurchase,
			ListPrice, InventoryType, InventoryStatusCD, BaseColor, VehicleLocation, LotPrice, MSRP, VehicleCatalogId) 
		EXEC Interface.InventoryList#Fetch @BusinessUnitId=@CurrentBuid, @UsedOrNew=0
	
				
		insert into postings.onlineStatus
		(businessUnitId, inventoryId, destinationId, mileageMatch, listPriceMatch, descriptionMatch, descriptionEmpty, onlineMileage, onlinePrice, onlineHighlightsPreview)
		
		select 
			inv.businessUnitId,
			inv.inventoryId,
			il.providerId, 
			case when inv.mileageReceived = il.mileage then 1 else 0 end, --mileage match
			case when inv.listPrice = il.ListPrice then 1 else 0 end, --list match
			case when 
				substring(ISNULL(rtrim(descr.merchandisingDescription + ' ' + footer),''),1,20) = substring(ISNULL(il.SellerDescription,''),1,20)
				THEN 1
				ELSE 0
				END, --highlights match			
			case when ISNULL(il.SellerDescription, '') = '' then 1 else 0 end, --descrip empty
			il.mileage,
			il.ListPrice,
			substring(il.SellerDescription, 1, 47) + '...'
		FROM 
			#Inventory inv
			JOIN Interface.Inventory#Listing il
				ON il.inventoryId = inv.inventoryId
			LEFT JOIN builder.descriptions descr
				on descr.inventoryId = inv.inventoryId
				and descr.businessUnitId = inv.businessUnitId
		where not exists(
			select * from postings.OnlineStatus
			where
				businessUnitId = inv.BusinessUnitId
				and inventoryId = inv.inventoryID
				and destinationId = il.providerId
		)

	
		--TRUNCATE - remove data start identity seed back at 1
		TRUNCATE TABLE #Inventory
	
		SELECT @dealerCtr = @dealerCtr + 1
	END
	
	DROP TABLE #Inventory
END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler @Message = @Step

END CATCH
GO

GRANT EXECUTE ON [integration].[ValidateOnlineData#Run] TO MerchandisingUser 
GO

