if exists (select * from dbo.sysobjects where id = object_id(N'[integration].[LotProviderIntegration#Run]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [integration].[LotProviderIntegration#Run]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <4/24/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: integration_lotProviderIntegration_run.sql,v 1.22 2010/08/19 21:24:39 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * loads the lot provider information from available data sources
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [integration].[LotProviderIntegration#Run]
	@debugBusinessUnitId int = null,
	@debugInventoryId int = null -- these two params must be set in a pair
AS
BEGIN TRY
	
	SET NOCOUNT ON;
	
	
	if
	(
		@debugBusinessUnitId is null and @debugInventoryId is not null
		or
		@debugBusinessUnitId is not null and @debugInventoryId is null
	)
	begin
		declare @msg varchar(1000)
		set @msg = 
			'@debugBusinessUnitId and @debugInventoryId both need to be null ' +
			'for normal operation or they both need to have a value for debug ops.  ' + 
			'@debugBusinessUnitId = ' + isnull(convert(varchar(11), @debugBusinessUnitId), '<null>') +
			', @debugInventoryId = ' + isnull(convert(varchar(11), @debugInventoryId), '<null>')
		raiserror(@msg, 16, 1)
		return 1
	end
	
	DECLARE @Step		VARCHAR(250),
		@ProcName	VARCHAR(128),
		@ParentLogID	INT
	
	SET @ProcName = OBJECT_NAME(@@PROCID)
	SET @Step = 'Started'

	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @ParentLogID OUTPUT
 
	/*
	PSEUDO CODE FOR OPTION PROCESSING
	FOREACH businessUnitId in wanamaker businessUnits?
	
		INSERT (vin, options list) into lotOptions on wanamaker database (PRODDB03SQL\Merchandising)
	
	END FOREACH
	
	FOREACH vehicle in active inventory, 
	
		IF vehicle does not exist in optionsConfiguration
			create vehicle
		END IF
		
		IF (vehicle not locked)
			FETCH options (split on delim) FROM lotOptions 
			JOIN to mapping table to get chrome options
			
			INSERT vehicle options into table 
			WHERE categoryId NOT IN builder.VehicleOptions for invId/buid/categoryId
			
			IF options added (@@row_count > 0), 
				set the vehicle equipment statusIds (1,2) to value 2
			END IF
			IF photo status (statusId 3) and both equipment statuses are complete (statuslevel 2), 
				set to vehicle posting status to pending (statusId 5, statusLevel 2)
			END IF
		END IF
		
	END FOREACH	
	*/
	
	
	--THIS PROCESS Collects the lot provider data and inserts options onto Wanamaker vehicles
	--Where the vehicles are unlocked and mapped options exist
	
	----------------------------------------------------------------------------------------
	SET @Step = 'DECLARE VARIABLES'
	----------------------------------------------------------------------------------------
	
	declare @dealerCtr int, @dealerCt int, @lotProviderId int,
		@CurrentBuid int, @processName varchar(10), @PhotoRequired bit
	set @processName = 'autoload'
	
	declare @dealerTbl table (
		RowId int IDENTITY(1,1),
		BusinessUnitId int NOT NULL,
		LotProviderId int NOT NULL,
		PhotoRequiredForRelease bit NOT NULL
	)
	
	--inner loop variables
	declare @i int, @ct int, @InventoryId int, @BusinessUnitId int, @PhotoStatus int, 
		@EquipStatus int, @RowUpdated int, @StockNumber varchar(15), @PendingCandidate int, 
		@IsLocked bit, @vin varchar(17), @currChromeStyleId int, @lotDataChromeStyleId int, 
		@lotDataExtColor varchar(50), @lotDataTransmission int
	

	
	
	----------------------------------------------------------------------------------------
	SET @Step = 'CREATE TEMPORARY TABLES'
	----------------------------------------------------------------------------------------
	
	CREATE TABLE #Inventory (
		rowId int identity(1,1),
		BusinessUnitId int NOT NULL,
		inventoryID int NOT NULL,
		StockNumber varchar(15) NOT NULL,
		Vin varchar(17) NOT NULL
	)
	

	
	----------------------------------------------------------------------------------------
	SET @Step = 'FETCH PARTICIPATING DEALERS'
	----------------------------------------------------------------------------------------
	
	--GET DEALERS WITH A LOT PROVIDER IN THE Merchandising.SETTINGS.Merchandising table
	if (@debugBusinessUnitId is null)
	begin
		INSERT INTO @dealerTbl (businessUnitId, lotProviderId, photoRequiredForRelease)
		SELECT businessUnitId, lotProviderId, photoRequiredForRelease
		FROM settings.Merchandising
		WHERE lotProviderId is not null
	end
	else
	begin
		INSERT INTO @dealerTbl (businessUnitId, lotProviderId, photoRequiredForRelease)
		SELECT businessUnitId, lotProviderId, photoRequiredForRelease
		FROM settings.Merchandising
		where
			BusinessUnitId = @debugBusinessUnitId
			and lotProviderId is not null
	end
	
	--GET DEALER LOOP VARS
	select @dealerCtr = 1, @dealerCt = count(*) FROM @dealerTbl
	
	-- loop over dealers
	WHILE @dealerCtr <= @dealerCt
	BEGIN
	
		--GET CURRENT DEALER/LOT PROVIDER
		select 
			@lotProviderId = lotProviderId, 
			@CurrentBuid = businessUnitId,
			@PhotoRequired = photoRequiredForRelease
		FROM @dealerTbl
		WHERE RowId = @dealerCtr
		
		----------------------------------------------------------------------------------------
		SET @Step = 'FETCH THE INVENTORY FOR DEALER'
		----------------------------------------------------------------------------------------
		

		insert #Inventory
		(
			BusinessUnitId
			, inventoryID
			, StockNumber
			, Vin
		)
		select
			ia.BusinessUnitId,
			ia.InventoryId,
			ia.StockNumber,
			v.Vin
		from 
			FLDW.dbo.InventoryActive ia
			join FLDW.dbo.Vehicle v 
				on ia.vehicleid = v.vehicleid
				and ia.businessUnitId = v.businessUnitId
		where
			ia.BusinessUnitId = @CurrentBuid
			and ia.InventoryActive = 1
			and not exists(
				select *
				from builder.VehicleStatus
				where
					businessUnitId = ia.BusinessUnitId
					and inventoryId = ia.inventoryId
					and statusTypeId = 5
					and statusLevel > 1
			)	
	
		
		
		SET @Step = @Step + ', ' + CAST(@@ROWCOUNT AS VARCHAR) + ' rows affected.'
		EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @ParentLogID
			
		if (@debugBusinessUnitId is not null)
		begin
			delete #Inventory where inventoryID <> @debugInventoryId
		end
		

		
		
		--INIT for loop
		SELECT @i=1, @ct = count(*) FROM #Inventory
	
		if (@debugBusinessUnitId is not null)
		begin
			print 'beginning to process inventoryId: ' + convert(varchar(11), @debugInventoryId) + '. There should be only one record in #Inventory.  Actual record count in #Inventory: ' + convert(varchar(11), @ct)
			select @i = rowId, @ct = rowId from #Inventory
			select 'debug', i = @i, ct = @ct
		end
		
		--LOOP for each inventoryItem (for this dealer)
		WHILE @i <= @ct
		BEGIN
	
			--clear lot data values
			SELECT @lotDataChromeStyleId = 0, @lotDataExtColor = '', @lotDataTransmission = NULL
			
			select 
				@BusinessUnitId = i.businessUnitId, 
				@InventoryId = i.inventoryid, 
				@StockNumber = i.StockNumber, 
				@vin = i.vin, 
				@EquipStatus = 0,
				@RowUpdated = 0, 
				
				@IsLocked = isnull(oc.lotDataImportLock, 0),
				@currChromeStyleId = oc.chromeStyleId,
			
				@PhotoStatus = case when isnull(vs.photoStatus, 0) < 2 then 0 else 1 end		
				
			from
				#Inventory i
				left join Merchandising.builder.OptionsConfiguration oc
						on i.BusinessUnitId = oc.BusinessUnitId
						and i.InventoryId = oc.InventoryId
				left join
				(	
					select
						statuses.BusinessUnitId,
						statuses.InventoryId,
						photoStatus = isnull(statuses.[3], 0)
					from 
						(
							select
								BusinessUnitId,
								InventoryId,
								StatusTypeId,
								StatusLevel
							from Merchandising.builder.VehicleStatus
						) bvs
						pivot
						(
							max(statusLevel) for StatusTypeId in ([3])
						) statuses
				) vs
					on i.BusinessUnitId = vs.BusinessUnitId
					and i.InventoryId = vs.InventoryId
				
			where rowId = @i
			
			
			
	
			--REMOVE ANY LOCKED ROWS
	
			IF @IsLocked = 0
			BEGIN
				--NOTE:  SHOULD WE DELETE VEHICLES WHERE THEY ARE ALREADY ONLINE?
		
				----------------------------------------------------------------------------------------
				SET @Step = 'Prep vehicle configuration'
				----------------------------------------------------------------------------------------
				
				--IF THERE ISN'T A VEHICLE YET, CREATE THE PLACEHOLDER
				IF NOT EXISTS (SELECT 1 FROM builder.OptionsConfiguration WHERE businessUnitId = @BusinessUnitId and inventoryId = @InventoryId)
				BEGIN 
					EXEC builder.VehicleConfiguration#Create @BusinessUnitId = @BusinessUnitId, @InventoryId = @InventoryId, @MemberLogin = @processName, @StockNumber = @StockNumber
				END
				
				----------------------------------------------------------------------------------------			
				SET @Step = 'Set chrome style'
				----------------------------------------------------------------------------------------
				
				
				SELECT @lotDataChromeStyleId = chromeStyleId, @lotDataExtColor = ISNULL(color,'')
				FROM Listing.Vehicle veh
				JOIN Listing.Color co
					ON co.colorId = veh.colorId
				WHERE veh.vin = @vin
				
				--update the chrome style for this inventory item when 
				--1) it wasn't set already (negative or null)
				--2) the new style is positive valued
				--3) they are actually different
				UPDATE builder.OptionsConfiguration
				SET chromeStyleId = COALESCE(@currChromeStyleId, @lotDataChromeStyleId), 
					lastUpdatedBy = @processName, 
					lastUpdatedOn = getdate()
				WHERE inventoryId = @InventoryId
				AND (chromeStyleId < 0 OR chromeStyleId IS NULL)
				AND @lotDataChromeStyleId > 0
				AND @lotDataChromeStyleID <> @currChromeStyleId
			
				--if we have a color...update as long as there wasn't one already
				UPDATE builder.OptionsConfiguration
				SET extColor1 = @lotDataExtColor, 
					lastUpdatedBy = @processName, 
					lastUpdatedOn = getdate()
				WHERE inventoryId = @InventoryId
				AND extColor1 = ''
				AND @lotDataExtColor <> ''				
				
				----------------------------------------------------------------------------------------
				SET @Step = 'Insert options into vehicle'
				----------------------------------------------------------------------------------------
				
				--1130 is Transmission-Auto
				--1131 is Transmission-Manual
				--use these to map the normalizedTransmission from the lot data as an override
				SELECT @lotDataTransmission = case 
					when StandardizedTransmission = 'AUTOMATIC' then 1130
					when StandardizedTransmission = 'MANUAL' then 1131
					else NULL end
				FROM Listing.Vehicle veh
				JOIN Listing.Transmission tr
				ON tr.TransmissionID = veh.TransmissionID
				WHERE 
					veh.vin = @vin
					
				--if we got a transmission map from the lot data provider...let's 
				IF NOT @lotDataTransmission IS NULL
				BEGIN
					--first we need to remove any previously entered optional transmission (created by this process)
					DELETE FROM builder.vehicleOptions
					where businessUnitId = @BusinessUnitId
					and inventoryId = @InventoryId
					and optionId IN (1130,1131)
					and isStandardEquipment = 0
					
					--add the most recent lotDataTrans to the options set...this will override any standard transmission provided by the selected chrome style id
					INSERT INTO builder.vehicleOptions (businessUnitId, inventoryId, optionId, optionCode,detailText,optionText,isStandardEquipment)
					SELECT 
						@BusinessUnitId, 
						@InventoryId, 
						@lotDataTransmission, 
						'',
						'',
						'',
						0
				END
				
				INSERT INTO builder.vehicleOptions (businessUnitId, inventoryId, optionId, optionCode,detailText,optionText,isStandardEquipment)
				SELECT 
					@BusinessUnitId, 
					@InventoryId, 
					om.chromeCategoryId, 
					'',
					'',
					'',
					0
				FROM Lot.Listing.Vehicle veh
				INNER JOIN Lot.Listing.VehicleOption vo_l
					ON vo_l.vehicleId = veh.vehicleId
				INNER JOIN Merchandising.OptionsMapping om
					ON om.optionid = vo_l.OptionID
				LEFT JOIN builder.VehicleOptions vo
					ON vo.inventoryId = @InventoryId AND
					vo.optionId = om.chromeCategoryId
				WHERE 
					veh.vin = @vin
					AND vo.optionId IS NULL			--we only want rows that aren't already on vehicle
					AND NOT om.chromeCategoryId IS NULL
					AND (@lotProviderId = 0 OR veh.providerId = @lotProviderId)
		
				--LOG NULL CHROMECATEGORIES HERE?
				
		
				--if rowcount > 0, then equipment was added
				IF @@ROWCOUNT > 0
				BEGIN
					SELECT @EquipStatus = 1, @RowUpdated = 1
					EXEC builder.VehicleStatus#UpdateOrCreate @BusinessUnitId = @BusinessUnitId, @InventoryId = @InventoryId,
							@StatusTypeId = 1, @StatusLevel = 2, @MemberLogin = @processName			
				
					EXEC builder.VehicleStatus#UpdateOrCreate @BusinessUnitId = @BusinessUnitId, @InventoryId = @InventoryId,
							@StatusTypeId = 2, @StatusLevel = 2, @MemberLogin = @processName		
					
				END
				
				
				
				----------------------------------------------------------------------------------------
				SET @Step = 'UPDATE OVERALL STATUS...'
				----------------------------------------------------------------------------------------
				
				------CHECK TO SEE IF PHOTOS ARE ALREADY COMPLETE...and the Equip is complete
				IF @RowUpdated = 1 AND @EquipStatus = 1 AND (@PhotoStatus = 1 OR @PhotoRequired = 0) AND @PendingCandidate = 1 --ALL READY for approval ONLY Update the vehicle posting status (id 5) if it was a pending candidate
				BEGIN
					--IF SO, we need to create a posting status and set to 2 (NOTE WE ARE IGNORING THE PRICING STATUS FOR THIS PHASE - CHECK FOR PRICE = 0?
					EXEC builder.VehicleStatus#UpdateOrCreate @BusinessUnitId = @BusinessUnitId, @InventoryId = @InventoryId,
							@StatusTypeId = 5, @StatusLevel = 2, @MemberLogin = @processName		
							
				
				END
			END --end if clause to determine if it was not locked...
			

			--increment
			SELECT @i = @i + 1
		end -- inventory loop
	
		--TRUNCATE - remove data start identity seed back at 1
		TRUNCATE TABLE #Inventory
	
		SELECT @dealerCtr = @dealerCtr + 1
	end -- dealer loop
	

	DROP TABLE #Inventory
	
	

	
	SET @Step = 'Completed'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @ParentLogID
	
END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler @Message = @Step

END CATCH
GO

GRANT EXECUTE ON [integration].[LotProviderIntegration#Run] TO MerchandisingUser 
GO

