if object_id('integration.FetchAutoTraderRoundRobinGetStalestStores') is not null
	drop procedure integration.FetchAutoTraderRoundRobinGetStalestStores
go


create procedure integration.FetchAutoTraderRoundRobinGetStalestStores
	@RequestTypeId int
as

set transaction isolation level read uncommitted
set nocount on


select top 300
	rr.BusinessUnitId,
	rr.ReportMonth,
	cred.userName, 
	cred.password,
	cred.businessUnitCode,
	cred.ListingSiteDealerId
from
	integration.FetchAutoTraderRoundRobin rr
	join
	(
		SELECT  dls.businessUnitID,  
				bu.BusinessUnitCode,  
				userName = ltrim(rtrim(dls.userName)),  
				dls.password,
				dls.ListingSiteDealerId
		FROM    Merchandising.Settings.DealerListingSites dls  
				INNER JOIN Merchandising.Settings.Merchandising m ON m.businessUnitID = dls.businessUnitID  
				INNER JOIN IMT.dbo.BusinessUnit bu ON bu.BusinessUnitID = dls.businessUnitID
		WHERE   destinationId = 2  
				AND userName <> ''  
				AND password <> ''
				AND (dls.dateLoginFailure IS NULL OR dls.dateLoginFailureCleared >= dls.dateLoginFailure)
	) cred
		on rr.businessUnitId = cred.businessUnitID
where
	rr.RequestTypeId = 1
	and	rr.DateQueued > isnull(rr.DateLastRequested, '1900-01-01')
	and rr.ReportMonth = dateadd(month, datediff(month, 0, getdate()), 0)
order by
	rr.DateQueued,
	cred.businessUnitID



go

