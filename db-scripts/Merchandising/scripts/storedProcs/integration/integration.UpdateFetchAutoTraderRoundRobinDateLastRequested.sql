USE [Merchandising]
go


if object_id('integration.UpdateFetchAutoTraderRoundRobinDateLastRequested') is not null
	drop procedure integration.UpdateFetchAutoTraderRoundRobinDateLastRequested
go


create procedure integration.UpdateFetchAutoTraderRoundRobinDateLastRequested
	@RequestTypeId int,
	@BusinessUnitId int,
	@ReportMonth datetime
as

set transaction isolation level read uncommitted
set nocount on

update integration.FetchAutoTraderRoundRobin set
	DateLastRequested = getdate()
where
	RequestTypeId = @RequestTypeId
	and BusinessUnitId = @BusinessUnitId
	and ReportMonth = @ReportMonth

set transaction isolation level read committed
set nocount off


go