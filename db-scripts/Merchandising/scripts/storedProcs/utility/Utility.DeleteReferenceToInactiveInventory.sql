IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Utility].[DeleteReferencesToInactiveInventory]') AND type in (N'P', N'PC'))
DROP PROCEDURE Utility.DeleteReferencesToInactiveInventory
GO

CREATE PROCEDURE  Utility.DeleteReferencesToInactiveInventory AS
/*********************************************************************************************************************
**														    **
**  Description:    This procedure deletes reference to Inactive Inventory in the Merchandising tables.		    **
**		    The temporary table #InventoryActive stores the InventoryID, BusinessUnitID of data		    **
**		    present in the view FLDW.dbo.InventoryActive.  The delete statements delete rows where	    **
**		    the InventoryID, BusinessUnitID do not exist in the object table.				    **
**														    **		    
**		    The tables that from which the data is deleted are:						    **
**														    **
**		1. audit.Descriptions				13. builder.VehicleConditions			    **
**		2. audit.OptionsConfiguration			14. builder.VehicleDetailedOptions		    **
**		3. audit.VehicleStatus				15. builder.VehicleOptions			    **
**		4. builder.ConditionChecklist			
**		5. builder.Description_SampleTextItems		17. builder.VehicleStatus			    **
**		6. builder.EquipmentMapping			18. builder.OptionsConfiguration    		    **
**		7. builder.Inventory_KeyInformation		19. postings.OnlineStatus			    **
**		8. builder.LoaderNotes				20. postings.Publications			    **
**		9. builder.NewEquipmentMapping			21. postings.SoldVehicleMetrics			    **
**					22. postings.VehicleAdScheduling		    **
**		11. builder.descriptionContents			23. postings.VehicleMetrics			    **
**		12. builder.Descriptions									    **
**														    **
**		11 has a key to 12, 8,13-17 to	18.								    **
**														    **
**********************************************************************************************************************
**														    **
**		    MAK	    09/21/2010		Initial Design and Development					    **
**														    **
**********************************************************************************************************************/

CREATE TABLE #InventoryActive
(InventoryID INT ,BusinessUnitID INT
PRIMARY KEY (InventoryID, BusinessUnitID))           

INSERT
INTO      #InventoryActive (InventoryID,BusinessUnitID)
SELECT	    InventoryID ,BusinessUnitID
FROM	    FLDW.dbo.InventoryActive IA WITH (NOLOCK)
--FROM	FLDW.dbo.InventoryActive  


BEGIN TRY

    -- 1.

    DELETE X
    FROM   audit.Descriptions X
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID
		       )        
                     
    -- 2.          
                                 
    DELETE X
    FROM  audit.OptionsConfiguration X
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID
		       )             
    -- 3.                   

    DELETE X
    FROM   audit.VehicleStatus X
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID
		       )     
    -- 4.

    DELETE X
    FROM   builder.ConditionChecklist X
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID
		       )        
    -- 5.                   
                                 
    DELETE X
    FROM   builder.Description_SampleTextItems X
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID
		       )             
    -- 6.         

    DELETE X
    FROM   builder.EquipmentMapping X
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID
		       )      
    -- 7.                   
                                   
    DELETE X
    FROM   builder.Inventory_KeyInformation X
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID
		       )      
                   
    --8.    
                              
    DELETE X
    FROM   builder.LoaderNotes X
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID
		       )       
              
    -- 9.                   
                                 
    DELETE X
    FROM   builder.NewEquipmentMapping X
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID
		       )
                       
     
    -- 11. 
     
    DELETE X
    FROM   builder.Descriptions X
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID
		       ) 
               
    -- 12.                   
                       
    DELETE X
    FROM   builder.descriptionContents X
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID

		       )    
    -- 13. 
     
    DELETE X
    FROM   builder.VehicleConditions X
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID
		       ) 
               
    -- 14.                   
                       
    DELETE X
    FROM   builder.VehicleDetailedOptions X
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID

		       )                       
    -- 15.
                       
    DELETE X
    FROM   builder.VehicleOptions X
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID
		       )    
                      
  
    -- 17.

    DELETE X
    FROM   builder.VehicleStatus X
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID
		       )    
                       
    -- 18.
                                                                 
    DELETE X
    FROM   builder.OptionsConfiguration X
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID
		      )     
    -- 19.
         
    DELETE X
    FROM   postings.OnlineStatus X
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID
		       )                          

    --20.         
                       
    DELETE X
    FROM   postings.Publications X
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID
		       )                          
                
    -- 21.                                     
                                         
    DELETE X
    FROM  Postings.SoldVehicleMetrics
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID
		       )         
                       
    -- 22.                   
                       
    DELETE X
    FROM  Postings.VehicleAdScheduling X
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID
		       ) 
                       
    -- 23.                   
                       
    DELETE X
    FROM   Postings.VehicleMetrics
    WHERE NOT EXISTS (      SELECT  1 
			    FROM	#InventoryActive IA 
			    WHERE	X.inventoryId = IA.InventoryID AND X.businessUnitID = IA.BusinessUnitID
		       )                                                                                                     
 

END TRY 

BEGIN CATCH

    EXEC sp_ErrorHandler

END CATCH
 
GO