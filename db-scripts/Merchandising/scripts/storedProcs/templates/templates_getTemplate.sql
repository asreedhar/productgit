if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[getTemplate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[getTemplate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_getTemplate.sql,v 1.12 2010/01/13 15:30:03 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Fetch the contents of a single template
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitID     - integer id of the businesUnit
 * @TemplateID  - integer id of the template to fetch
 * 
 * Returns XML of template contents
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[getTemplate]
	
	@BusinessUnitID int,
	@TemplateID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @parentBuid int
	
	SELECT @parentBuid = bur.parentId 
	FROM Interface.BusinessUnit bu
	JOIN Interface.BusinessUnitRelationship bur
		ON bur.businessUnitId = bu.businessUnitId
	WHERE bu.businessunitid = @BusinessUnitID
	

	Select 
		AdTemplate.templateId,
		AdTemplate.name,
		AdTemplate.active,
		AdTemplate.vehicleProfileId,
		AdTemplate.businessUnitId,
		
		TemplateItem.blurbID, 
		TemplateItem.sequence, 
		TemplateItem.priority, 
		TemplateItem.required, 
		
		ISNULL(TemplateItem.parentID, -1) as parentID,

		BlurbTemplate.blurbID, 
		BlurbTemplate.blurbName, 
		BlurbTemplate.blurbTemplate, 
		BlurbTemplate.blurbCondition,
		BlurbTemplate.blurbCategory,
		
		
		SampleText.blurbTextID, 
		SampleText.blurbPreText, 
		SampleText.blurbPostText,
		SampleText.active,
		SampleText.isLongForm,
		SampleText.isHeader,
		SampleText.themeId,
		
		
		VehicleProfile.vehicleProfileId,
		VehicleProfile.title,
		VehicleProfile.makeIdList,
		VehicleProfile.modelList,
		VehicleProfile.trimList,
		VehicleProfile.segmentIdList,
		VehicleProfile.marketClassIdList,
		VehicleProfile.minAge,
		VehicleProfile.maxAge,
		VehicleProfile.startYear,
		VehicleProfile.endYear,
		VehicleProfile.minPrice,
		VehicleProfile.maxPrice,
		VehicleProfile.certifiedStatusFilter,
		VehicleProfile.vehicleType
		
		FROM (SELECT adt.templateId, adt.name, adt.active, vp.vehicleProfileId, adt.BusinessUnitId
				FROM templates.AdTemplates adt 
				LEFT JOIN templates.VehicleProfiles vp 
				ON vp.vehicleProfileId = adt.vehicleProfileID) as AdTemplate
			LEFT JOIN templates.AdTemplateItems TemplateItem
				ON AdTemplate.templateID = TemplateItem.templateId
			LEFT JOIN templates.blurbTemplates BlurbTemplate 
				ON BlurbTemplate.blurbID = TemplateItem.blurbID
			LEFT JOIN templates.blurbSampleText SampleText 
				ON SampleText.blurbId = BlurbTemplate.blurbId
				AND 
				(
					SampleText.businessUnitId = @BusinessUnitId 
					OR SampleText.businessUnitId = 100150
					OR (NOT @parentBuid IS NULL AND SampleText.BusinessUnitId = @parentBuid)
				)
			LEFT JOIN templates.BlurbSampleText_DealerOverride bstdo
				ON bstdo.businessUnitId = @BusinessUnitId
				AND bstdo.blurbTextId = SampleText.blurbTextId
			LEFT JOIN templates.vehicleProfiles VehicleProfile
				ON VehicleProfile.vehicleProfileId = SampleText.vehicleProfileId
		WHERE AdTemplate.templateID = @TemplateID 
			AND (
				AdTemplate.BusinessUnitId = @BusinessUnitId OR AdTemplate.BusinessUnitId = 100150
				OR (NOT @parentBuid IS NULL AND AdTemplate.BusinessUnitId = @parentBuid)
			)
			AND (SampleText.active = 1
			OR SampleText.active IS NULL)
			AND COALESCE(bstdo.enabled,1) <> 0  --if null, it's included, or if enabled, it's included
		ORDER BY TemplateItem.sequence, BlurbTemplate.blurbID
		FOR XML AUTO, Root('template')
		

END
GO

GRANT EXECUTE ON [templates].[getTemplate] TO MerchandisingUser 
GO