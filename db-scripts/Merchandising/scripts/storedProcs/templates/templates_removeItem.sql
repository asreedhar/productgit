if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[removeItem]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[removeItem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_removeItem.sql,v 1.3 2008/11/20 20:01:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Removes an item from a template
 * 
 * 
 * Parameters
 * ----------
 * @TemplateID - The id of the template to modify
 * @BlurbID - the id of the blurb/snippet to remove from the template
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[removeItem]
	
	@TemplateID int,
	@BlurbID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DELETE FROM 
		templates.AdTemplateItems 
	WHERE templateID = @TemplateID 
		AND blurbID = @BlurbID
	

END
GO

GRANT EXECUTE ON [templates].[removeItem] TO MerchandisingUser 
GO