if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[getAvailableTemplateItemListByType]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[getAvailableTemplateItemListByType]
GO


CREATE PROCEDURE [templates].[getAvailableTemplateItemListByType]
	@BusinessUnitID int,
	@blurbTypeID int
AS

BEGIN

	SET NOCOUNT ON

	SELECT blurbId, blurbName
	FROM templates.BlurbTemplates BlurbTemplates
	WHERE BlurbTemplates.blurbTypeID = @blurbTypeID
	AND  (BlurbTemplates.BusinessUnitID = @BusinessUnitID OR BlurbTemplates.BusinessUnitID = 100150)
	ORDER BY blurbName
    
    SET NOCOUNT OFF

END
GO

GRANT EXECUTE ON [templates].[getAvailableTemplateItemListByType] TO MerchandisingUser 
GO