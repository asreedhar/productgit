if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[getTemplateConversions]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[getTemplateConversions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_getTemplateConversions.sql,v 1.3 2008/11/20 20:01:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the data access strings for templating text generation
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the conversion list
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[getTemplateConversions]
	
	@BusinessUnitID int
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT templateDisplayText, dataAccessString
        FROM Templates.MerchandisingPoints mp 
        WHERE (BusinessUnitID = @BusinessUnitID OR BusinessUnitID = 100150)
	

END
GO

GRANT EXECUTE ON [templates].[getTemplateConversions] TO MerchandisingUser 
GO