if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[getSampleText]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[getSampleText]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_getSampleText.sql,v 1.9 2009/10/06 14:40:52 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Gets a single sample text entry
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit 
 * @BlurbtextID  - integer id of the sample text
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[getSampleText]
	
	@BusinessUnitID int,
	@BlurbTextID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT blurbTextId, 
		blurbPreText, 
		blurbPostText, 
		bst.active, 
		bst.isLongForm,
		vp.vehicleProfileId, 
		vp.title,
		ISNULL(makeIdList,'') as makeIdList, 
		ISNULL(modelList,'') as modelList, 
		ISNULL(trimList,'') as trimList,
		ISNULL(segmentIdList,'') as segmentIdList,
		ISNULL(marketClassIdList,'') as marketClassIdList,
		startYear,
		endYear,
		minAge,
		maxAge,
		minPrice,
		maxPrice,
		vp.certifiedStatusFilter,
		thm.id as themeId,
		thm.name as themeName,
		isHeader
		FROM templates.blurbSampleText bst 
		LEFT JOIN templates.vehicleProfiles vp
		ON vp.vehicleProfileId = bst.vehicleProfileId
		LEFT JOIN templates.themes thm
		on thm.id = bst.themeId
		WHERE BlurbTextID = @BlurbTextID
			AND bst.BusinessUnitID = @BusinessUnitID

END
GO

GRANT EXECUTE ON [templates].[getSampleText] TO MerchandisingUser 
GO