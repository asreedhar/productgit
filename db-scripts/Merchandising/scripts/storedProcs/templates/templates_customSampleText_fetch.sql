if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[CustomSampleText#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[CustomSampleText#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_customSampleText_fetch.sql,v 1.1 2009/11/09 17:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Fetch the customizeable sample text for a dealer
 * 
 * 
 * Parameters
 * ----------
 * 
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[CustomSampleText#Fetch]
	
	@BusinessUnitID int	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select * from templates.blurbtemplates
            where (businessUnitId = 100150 OR businessUnitId = @BusinessUnitId)
            and NOT dealerCustomizeableOrder IS NULL
    ORDER BY dealerCustomizeableOrder ASC

END
GO

GRANT EXECUTE ON [templates].[CustomSampleText#Fetch] TO MerchandisingUser 
GO