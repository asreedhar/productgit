if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[PushModelLevelFrameworks]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[PushModelLevelFrameworks]
GO

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

CREATE PROCEDURE [templates].[PushModelLevelFrameworks]
	@StoreId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @stores table (BusinessUnitID int not null)
	declare @templates table (templateID int not null)

	insert @stores (BusinessUnitID)
	select @StoreId

	insert @templates (templateID)
	select templateID from Merchandising.templates.AdTemplates where templateID in
	(
		select templateId from
		(
			select Brand = 'BMW', Framework = 'BMW 3 Series', templateId = 7212
			union all select Brand = 'BMW', Framework = 'BMW 3 Series New', templateId = 7548
			union all select Brand = 'BMW', Framework = 'BMW 5 Series', templateId = 7229
			union all select Brand = 'BMW', Framework = 'BMW 5 Series New', templateId = 7566
			union all select Brand = 'BMW', Framework = 'BMW 7 Series', templateId = 7265
			union all select Brand = 'BMW', Framework = 'BMW 7 Series New', templateId = 7717
			union all select Brand = 'BMW', Framework = 'BMW X3', templateId = 7323
			union all select Brand = 'BMW', Framework = 'BMW X3 New', templateId = 7684
			union all select Brand = 'BMW', Framework = 'BMW X5', templateId = 7324
			union all select Brand = 'BMW', Framework = 'BMW X5 New', templateId = 7565
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Chrysler 200', templateId = 8630
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Chrysler 200 New', templateId = 8629
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Chrysler 300', templateId = 8643
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Chrysler 300 New', templateId = 8644
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Chrysler 300C', templateId = 8651
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Chrysler 300C New', templateId = 8652
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Chrysler Town & Country', templateId = 8627
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Chrysler Town & Country New', templateId = 8564
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Charger', templateId = 8854
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Charger New', templateId = 8855
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Durango', templateId = 8824
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Durango 2009-2006', templateId = 8826
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Durango New', templateId = 8825
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Grand Caravan', templateId = 8792
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Grand Caravan New', templateId = 8793
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Journey', templateId = 8845
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Journey New', templateId = 8852
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Nitro', templateId = 8858
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Nitro New', templateId = 8859
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Compass', templateId = 8748
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Compass New', templateId = 8749
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Grand Cherokee', templateId = 8697
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Grand Cherokee New', templateId = 8698
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Liberty', templateId = 8746
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Liberty New', templateId = 8747
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Patriot', templateId = 8699
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Patriot New', templateId = 8700
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Wrangler', templateId = 8681
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Wrangler New', templateId = 8682
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Wrangler Rubicon', templateId = 8683
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Wrangler Rubicon New', templateId = 8684
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Wrangler Unlimited', templateId = 8677
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Wrangler Unlimited New', templateId = 8678
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Wrangler Unlimited Rubico', templateId = 8679
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Wrangler Unlimited RubNew', templateId = 8680
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Ram Dakota', templateId = 9314
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Ram Dakota New', templateId = 9315
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Ram Truck', templateId = 9312
			union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Ram Truck New', templateId = 9313
			union all select Brand = 'GM', Framework = 'Buick Enclave', templateId = 8458
			union all select Brand = 'GM', Framework = 'Buick Enclave New', templateId = 8459
			union all select Brand = 'GM', Framework = 'Buick LaCrosse', templateId = 8506
			union all select Brand = 'GM', Framework = 'Buick LaCrosse New', templateId = 8507
			union all select Brand = 'GM', Framework = 'Cadillac CTS', templateId = 8464
			union all select Brand = 'GM', Framework = 'Cadillac CTS New', templateId = 8465
			union all select Brand = 'GM', Framework = 'Cadillac Escalade', templateId = 8462
			union all select Brand = 'GM', Framework = 'Cadillac Escalade New', templateId = 8463
			union all select Brand = 'GM', Framework = 'Chevy Cruze', templateId = 8401
			union all select Brand = 'GM', Framework = 'Chevy Cruze New', templateId = 8402
			union all select Brand = 'GM', Framework = 'Chevy Malibu', templateId = 8382
			union all select Brand = 'GM', Framework = 'Chevy Malibu New', templateId = 8466
			union all select Brand = 'GM', Framework = 'Chevy Silverado', templateId = 8701
			union all select Brand = 'GM', Framework = 'Chevy Silverado New', templateId = 8702
			union all select Brand = 'GM', Framework = 'Chevy Suburban', templateId = 8496
			union all select Brand = 'GM', Framework = 'Chevy Suburban New', templateId = 8497
			union all select Brand = 'GM', Framework = 'Chevy Tahoe', templateId = 8403
			union all select Brand = 'GM', Framework = 'Chevy Tahoe New', templateId = 8404
			union all select Brand = 'GM', Framework = 'Chevy Traverse', templateId = 8456
			union all select Brand = 'GM', Framework = 'Chevy Traverse New', templateId = 8457
			union all select Brand = 'GM', Framework = 'GMC Acadia', templateId = 8460
			union all select Brand = 'GM', Framework = 'GMC Acadia New', templateId = 8461
			union all select Brand = 'GM', Framework = 'GMC Yukon', templateId = 8405
			union all select Brand = 'GM', Framework = 'GMC Yukon New', templateId = 8406
			union all select Brand = 'GM', Framework = 'GMC Yukon New Car', templateId = 8407
			union all select Brand = 'GM', Framework = 'GMC Yukon XL', templateId = 8498
			union all select Brand = 'GM', Framework = 'GMC Yukon XL New', templateId = 8499
			union all select Brand = 'Honda', Framework = 'Honda Accord', templateId = 7393
			union all select Brand = 'Honda', Framework = 'Honda Accord New', templateId = 7774
			union all select Brand = 'Honda', Framework = 'Honda Civic', templateId = 7392
			union all select Brand = 'Honda', Framework = 'Honda Civic New', templateId = 7693
			union all select Brand = 'Honda', Framework = 'Honda CR-V', templateId = 7466
			union all select Brand = 'Honda', Framework = 'Honda CR-V New', templateId = 7815
			union all select Brand = 'Honda', Framework = 'Honda Odyssey', templateId = 7416
			union all select Brand = 'Honda', Framework = 'Honda Odyssey New', templateId = 7806
			union all select Brand = 'Honda', Framework = 'Honda Pilot', templateId = 7432
			union all select Brand = 'Honda', Framework = 'Honda Pilot New', templateId = 7814

			union all select Brand = 'Volkswagen', Framework = 'Volkswagen Jetta 2012 New', templateId = 24113
			union all select Brand = 'Volkswagen', Framework = 'Volkswagen Jetta 2011-12', templateId = 24114
			union all select Brand = 'Volkswagen', Framework = 'Volkswagen Jetta 2005-10', templateId = 24115
			union all select Brand = 'Volkswagen', Framework = 'Volkswagen Passat 2012 New', templateId =	24116
			union all select Brand = 'Volkswagen', Framework = 'Volkswagen Passat 2011-12', templateId = 24117
			union all select Brand = 'Volkswagen', Framework = 'Volkswagen Passat 2006-10', templateId = 24118
			union all select Brand = 'Volkswagen', Framework = 'Volkswagen Golf 2012 New', templateId = 24722
			union all select Brand = 'Volkswagen', Framework = 'Volkswagen Golf 2012', templateId =	24723
			union all select Brand = 'Volkswagen', Framework = 'Volkswagen Golf 2011-', templateId = 24724
			union all select Brand = 'Volkswagen', Framework = 'Volkswagen CC 2012 New', templateId = 24743
			union all select Brand = 'Volkswagen', Framework = 'Volkswagen CC', templateId = 24744
			union all select Brand = 'Volkswagen', Framework = 'Volkswagen Touareg 2012 New',templateId =	24751
			union all select Brand = 'Volkswagen', Framework = 'Volkswagen Touareg 2011-12',templateId = 24752
			union all select Brand = 'Volkswagen', Framework = 'Volkswagen Touareg 2004-10',templateId = 24753
			union all select Brand = 'Volkswagen', Framework = 'Volkswagen Tiguan for 2012 New',templateId = 25177
			union all select Brand = 'Volkswagen', Framework = 'Volkswagen Tiguan 2012',templateId = 25178
			union all select Brand = 'Volkswagen', Framework = 'Volkswagen Tiguan 2004-10',templateId = 25179

			union all select Brand = 'Mazda', Framework = 'Mazda6 2012 New', templateId = 23893
			union all select Brand = 'Mazda', Framework = 'Mazda6 2009-2012', templateId = 23901
			union all select Brand = 'Mazda', Framework = 'Mazda6 2006-08', templateId = 23903
			union all select Brand = 'Mazda', Framework = 'Mazda5 2012 New', templateId = 23908
			union all select Brand = 'Mazda', Framework = 'Mazda5', templateId = 23909
			union all select Brand = 'Mazda', Framework = 'Mazda CX-9 2012 New', templateId = 23910
			union all select Brand = 'Mazda', Framework = 'Mazda CX-9', templateId = 23911
			union all select Brand = 'Mazda', Framework = 'Mazda CX-7 2012 New', templateId = 23912
			union all select Brand = 'Mazda', Framework = 'Mazda CX-7', templateId = 23913
			union all select Brand = 'Mazda', Framework = 'Mazda2 2012 New', templateId = 23935
			union all select Brand = 'Mazda', Framework = 'Mazda2', templateId = 23936
			union all select Brand = 'Mazda', Framework = 'Mazda CX-5 2012-13', templateId = 23944
			union all select Brand = 'Mazda', Framework = 'Mazda CX-5', templateId = 23945

			union all select Brand = 'Ford', Framework = 'Ford Focus 2007-2012', templateId = 9320
			union all select Brand = 'Ford', Framework = 'Ford Focus New for 2011-2012', templateId =9321
			union all select Brand = 'Ford', Framework = 'Ford Fusion 2007-2012', templateId =   9322
			union all select Brand = 'Ford', Framework = 'Ford Fusion New for 2011-2012 ', templateId = 9323
			union all select Brand = 'Ford', Framework = 'Ford Mustang 2006-2012', templateId = 9324
			union all select Brand = 'Ford', Framework = 'Ford Mustang New for 2011-2012', templateId = 9325
			union all select Brand = 'Ford', Framework = 'Ford Escape 2006-2012', templateId = 9328
			union all select Brand = 'Ford', Framework = 'Ford Escape New for 2011-2012', templateId = 9329
			union all select Brand = 'Ford', Framework = 'Ford Edge 2007-2012', templateId = 9337
			union all select Brand = 'Ford', Framework = 'Ford Edge New for 2011-2012', templateId = 9338
			union all select Brand = 'Ford', Framework = 'Ford Explorer 2006-2010', templateId = 9339
			union all select Brand = 'Ford', Framework = 'Ford Explorer 2011-2012', templateId = 9376
			union all select Brand = 'Ford', Framework = 'Ford Explorer New for 2011-2012', templateId = 9340
			union all select Brand = 'Ford', Framework = 'Ford F-150 2006-2012', templateId = 9613
			union all select Brand = 'Ford', Framework = 'Ford F-150 New for 2011-2012', templateId = 9614
		
			union all select Brand = 'Hyundai', Framework = 'Hyundai Elantra New for 2012', templateID = 25180
			union all select Brand = 'Hyundai', Framework = 'Hyundai Elantra 2011-12', templateID = 25181
			union all select Brand = 'Hyundai', Framework = 'Hyundai Elantra 2007-10', templateID = 25182
			union all select Brand = 'Hyundai', Framework = 'Hyundai Accent New for 2012', templateID = 25183
			union all select Brand = 'Hyundai', Framework = 'Hyundai Accent 2012', templateID = 25184
			union all select Brand = 'Hyundai', Framework = 'Hyundai Accent 2006-11', templateID = 25185
			union all select Brand = 'Hyundai', Framework = 'Hyundai Tucson New for 2012', templateID = 25200
			union all select Brand = 'Hyundai', Framework = 'Hyundai Tucson 2010-12', templateID = 25201
			union all select Brand = 'Hyundai', Framework = 'Hyundai Tucson 2005-09', templateID = 25202
			union all select Brand = 'Hyundai', Framework = 'Hyundai Santa Fe New for 2012', templateID = 25203
			union all select Brand = 'Hyundai', Framework = 'Hyundai Santa Fe 2007-12', templateID = 25204
			union all select Brand = 'Hyundai', Framework = 'Hyundai Genesis New for 2012', templateID = 25205
			union all select Brand = 'Hyundai', Framework = 'Hyundai Genesis 2009-12', templateID = 25206
			union all select Brand = 'Hyundai', Framework = 'Hyundai Sonata New for 2012', templateID = 25165
			union all select Brand = 'Hyundai', Framework = 'Hyundai Sonata 2011-12', templateID = 25166
			union all select Brand = 'Hyundai', Framework = 'Hyundai Sonata 2009-10', templateID = 25167
			union all select Brand = 'Hyundai', Framework = 'Hyundai Sonata 2006-08', templateID = 25168
			
			union all select Brand = 'BMW', Framework = 'BMW 3 Series New 2013', templateId = 74805
			union all select Brand = 'BMW', Framework = 'BMW X3 Used 2013', templateId = 74818
			union all select Brand = 'Chevrolet', Framework = 'Chevy Tahoe New for 2013', templateId = 67593
			union all select Brand = 'Chrysler', Framework = 'Chrysler 200 New 2013', templateId = 78336
			union all select Brand = 'Chrysler', Framework = 'Chrysler 300 New 2013', templateId = 78332
			union all select Brand = 'Chrysler', Framework = 'Chrysler Town & Country New 20', templateId = 78334
			union all select Brand = 'Dodge', Framework = 'Dodge Charger New 2013', templateId = 78326
			union all select Brand = 'Ford', Framework = 'Ford Edge New 2013', templateId = 78387
			union all select Brand = 'Ford', Framework = 'Ford Escape New 2013', templateId = 78383
			union all select Brand = 'Ford', Framework = 'Ford Explorer New 2013', templateId = 78385
			union all select Brand = 'Ford', Framework = 'Ford F-150 New 2013', templateId = 78381
			union all select Brand = 'Ford', Framework = 'Ford Focus Used 2013', templateId = 78379
			union all select Brand = 'Honda', Framework = 'Honda CR-V New 2013', templateId = 78320
			union all select Brand = 'Honda', Framework = 'Honda Pilot New 2013', templateId = 78324
			union all select Brand = 'Jeep', Framework = 'Jeep Grand Cherokee New 2013', templateId = 78389
			union all select Brand = 'Jeep', Framework = 'Jeep Patriot New 2013', templateId = 78393
			union all select Brand = 'Jeep', Framework = 'Jeep Wrangler New 2013', templateId = 78391
			union all select Brand = 'Mazda', Framework = 'Mazda CX-9 New 2013', templateId = 78343
			union all select Brand = 'Mazda', Framework = 'Mazda5 Used 2013', templateId = 78341
			union all select Brand = 'Mazda', Framework = 'Mazda6 New 2013', templateId = 78338
			union all select Brand = 'Volkswagen', Framework = 'Volkswagen CC New 2013', templateId = 78357
			union all select Brand = 'Volkswagen', Framework = 'Volkswagen Golf New 2013', templateId = 78355
			union all select Brand = 'Volkswagen', Framework = 'Volkswagen Tiguan New 2013', templateId = 78373
			union all select Brand = 'Volkswagen', Framework = 'Volkswagen Touareg New 2013', templateId = 78359
			
		) a
	)


	--- do not edit script below this line, only edit the stores and templates list above

	set xact_abort on
	set transaction isolation level serializable
	begin transaction

	declare
		@rows int,
		@businessUnitId int,
		@sourceTemplateId int,
		@vehicleProfileId int


	declare @storeProfiles table
	(
		BusinessUnitID int not null,
		sourceTemplateId int not null,
		vehicleProfileId int null,
		processed bit not null
		
	)

	insert @storeProfiles
	(
		BusinessUnitID,
		sourceTemplateId,
		processed
	)
	select
		s.BusinessUnitID,
		t.templateID,
		0
	from
		@stores s
		cross join @templates t


	declare @vpToDelete table
	(
		vehicleProfileID int
	)

	-- delete all VehicleProfile, AdTemplate, and AdTemplateItems for the new stores 
	-- that are associated with the "master" templates we are about to clone.  these
	-- records might not exist, but if they do this script will not correctly map the
	-- deltas that may occur if a store only has a partial framework frmo the masters.

	insert @vpToDelete
	(
		vehicleProfileID
	)
	select
		vp.vehicleProfileID
	from
		Merchandising.templates.VehicleProfiles vp
		join Merchandising.templates.AdTemplates at
			on vp.vehicleProfileID = at.vehicleProfileId
		join @storeProfiles sp
			on at.SourceTemplateId = sp.sourceTemplateId
			and at.businessUnitID = sp.BusinessUnitID
			and vp.businessUnitId = sp.BusinessUnitID
		
	delete ati
	from
		Merchandising.templates.AdTemplateItems ati
		join Merchandising.templates.AdTemplates at
			on ati.templateID = at.templateID
		join @storeProfiles sp
			on at.businessUnitID = sp.BusinessUnitID
			and at.SourceTemplateId = sp.sourceTemplateId

	delete at
	from
		Merchandising.templates.AdTemplates at
		join @storeProfiles sp
			on at.SourceTemplateId = sp.sourceTemplateId
			and at.businessUnitID = sp.BusinessUnitID

	delete vp
	from
		Merchandising.templates.VehicleProfiles vp
		join @vpToDelete d
			on vp.vehicleProfileID = d.vehicleProfileID

	while (1=1)
	begin
		select top 1
			@businessUnitId = BusinessUnitID,
			@sourceTemplateId = sourceTemplateId
		from @storeProfiles
		where processed = 0
		
		if (@@rowcount <> 1) break;
		
		insert Merchandising.templates.VehicleProfiles
		( 
			businessUnitId,
			title,
			makeIDList,
			modelList,
			marketClassIdList,
			startYear,
			endYear,
			minAge,
			maxAge,
			minPrice,
			maxPrice,
			certifiedStatusFilter,
			trimList,
			segmentIdList,
			vehicleType
		)
		select
			@businessUnitID,
			vp.title,
			vp.makeIDList,
			vp.modelList,
			vp.marketClassIdList,
			vp.startYear,
			vp.endYear,
			vp.minAge,
			vp.maxAge,
			vp.minPrice,
			vp.maxPrice,
			vp.certifiedStatusFilter,
			vp.trimList,
			vp.segmentIdList,
			vp.vehicleType	
		from
			Merchandising.templates.AdTemplates at
			join Merchandising.templates.VehicleProfiles vp
				on at.vehicleProfileId = vp.vehicleProfileID
		where at.templateID = @sourceTemplateId
		
		select @rows = @@rowcount, @vehicleProfileId = scope_identity()
		
		if (@rows > 1) raiserror('incorrect rows inserted into vehicle profiles!', 16, 1)
		
		update @storeProfiles set
			vehicleProfileId = @vehicleProfileId,
			processed = 1
		where
			BusinessUnitID = @businessUnitId
			and sourceTemplateId = @sourceTemplateId
		
		set @rows = @@rowcount
		if (@rows <> 1) raiserror('incorrect rows updated in local table!', 16, 1)
		
	end	-- VehicleProfile insert loop


	print ''
	print ''

	select @rows = count(*) from @storeProfiles
	print convert(varchar(10), @rows) + ' new VehicleProfiles added.'




	-- insert the new templates
	insert Merchandising.templates.AdTemplates
	(
		name,
		businessUnitID,
		vehicleProfileId,
		parent,
		active,
		SourceTemplateId
	)
	select
		at.name,
		sp.BusinessUnitID,
		sp.vehicleProfileId,
		at.parent,
		at.active,
		SourceTemplateId = at.templateID
	from
		Merchandising.templates.AdTemplates at
		join @storeProfiles sp
			on at.templateID = sp.sourceTemplateId
		

	-- ok go clone the AdTemplateItems tied to @templates list to the new @stores list
	insert Merchandising.templates.AdTemplateItems
	(
		templateID,
		blurbID,
		sequence,
		priority,
		required,
		parentID
	)
	select
		newTemplates.templateID,
		ati.blurbID,
		ati.sequence,
		ati.priority,
		ati.required,
		ati.parentID
	from
		Merchandising.templates.AdTemplateItems ati
		join Merchandising.templates.AdTemplates at
			on ati.templateID = at.templateID
		join @templates t
			on at.templateID = t.templateID
		join Merchandising.templates.AdTemplates newTemplates
			on newTemplates.SourceTemplateId = at.templateID
		join @stores s
			on newTemplates.businessUnitID = s.BusinessUnitID

	commit
	set xact_abort off
	set transaction isolation level read uncommitted

END
GO

GRANT EXECUTE ON [templates].[PushModelLevelFrameworks] TO MerchandisingUser 
GO




