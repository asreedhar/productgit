if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[TemplateItem#Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[TemplateItem#Update]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_templateItem_update.sql,v 1.2 2009/08/19 21:27:28 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Update the sequence and priority of template items
 * 
 * 
 * Parameters
 * ----------
 * @TemplateID - id of template that is being edited
 * @BlurbID - id of blurb/snippet to update
 
 * @Sequence - the sequence number to use
 * @Priority - the snippet priority in the description
 * @BusinessUnitId - buid of the user requesting the update to prevent unallowed changes to other buid's template items
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[TemplateItem#Update]
	@TemplateID int,
	@BlurbID int,
	@Sequence int,
	@Priority int,
	@BusinessUnitId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE ATI
	
		SET [sequence] = @Sequence, priority = @Priority
		FROM templates.AdTemplateItems ATI
		JOIN templates.AdTemplates AdT
			ON AdT.templateId = ATI.templateId
		WHERE blurbID = @BlurbID 
			AND ATI.templateID = @TemplateID
			AND AdT.businessUnitId = @BusinessUnitId
			
	

END
GO

GRANT EXECUTE ON [templates].[TemplateItem#Update] TO MerchandisingUser 
GO