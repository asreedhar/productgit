if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[deleteTemplateItem]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[deleteTemplateItem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_deleteTemplateItem.sql,v 1.2 2009/09/10 16:06:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Deletes a single snippet IF the snippet is owned by the businessUnit
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit requesting the delete
 * @BlurbId - the blurb/snippet to retrieve
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[deleteTemplateItem]
	
	@BusinessUnitID int,
	@BlurbID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--first delete from all templates where it is used (by this dealer only)
	--if some other dealer is using an ad template item by this dealer, there is a different problem
	DELETE ati FROM 
	templates.AdTemplateItems as ati
	join templates.adTemplates adt on adt.templateId = ati.templateId
	WHERE
	adt.businessUnitId = @BusinessUnitId
	and ati.blurbId = @BlurbID

	DELETE FROM templates.blurbTemplates 
	WHERE blurbId = @BlurbID
		AND businessUnitId = @BusinessUnitID

END
GO

GRANT EXECUTE ON [templates].[deleteTemplateItem] TO MerchandisingUser 
GO