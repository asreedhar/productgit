if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[deleteTemplate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[deleteTemplate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_deleteTemplate.sql,v 1.3 2008/11/20 20:01:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Deletes a template
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit that owns the template
 * @TemplateId - integer id of the template to delete
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[deleteTemplate]
	
	@BusinessUnitID int,
	@TemplateID int
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DELETE FROM 
			templates.adTemplates
	WHERE templateID = @TemplateID
		AND businessUnitID = @BusinessUnitID
	

END
GO

GRANT EXECUTE ON [templates].[deleteTemplate] TO MerchandisingUser 
GO