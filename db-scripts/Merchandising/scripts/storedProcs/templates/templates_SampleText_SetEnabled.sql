if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[SampleText#SetEnabled]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[SampleText#SetEnabled]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $$
 * 
 * Summary
 * -------
 * 
 * updates or creates blurb sample text
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the destination businesUnit
 * @BlurbTextID int - id of blurb text to copy
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[SampleText#SetEnabled]
	
	@BusinessUnitID int,
	@BlurbTextID int,
	@Enabled bit
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	IF EXISTS(SELECT 1 FROM templates.BlurbSampleText 
				WHERE businessUnitId = 100150 and blurbTextId = @BlurbTextId)
    BEGIN
		
		IF NOT EXISTS(SELECT 1
		FROM templates.BlurbSampleText_DealerOverride 
		WHERE businessUnitId = @BusinessUnitID 
		AND blurbTextId = @BlurbTextId)
		BEGIN
			INSERT INTO templates.BlurbSampleText_DealerOverride 
			(businessUnitId, blurbTextId, enabled)
			VALUES (@BusinessUnitId, @BlurbTextId, @Enabled)
        END
        ELSE
        BEGIN
			UPDATE templates.BlurbSampleText_DealerOverride
			SET enabled = @Enabled
			WHERE businessUnitId = @BusinessUnitId
				AND blurbTextId = @BlurbTextId
        END
        
    END
				
      
	

END
GO

GRANT EXECUTE ON [templates].[SampleText#SetEnabled] TO MerchandisingUser 
GO