if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[getMatchingTemplates]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[getMatchingTemplates]
GO

/****** Object:  StoredProcedure [templates].[getMatchingTemplates]    Script Date: 10/26/2008 13:10:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Jonathan Kelley>
-- Create date: <Aug 27 208>
-- Description:	<gets appropriate templateIDs for the vehicle configuration>
-- =============================================

CREATE PROCEDURE [templates].[getMatchingTemplates]
	-- Add the parameters for the stored procedure here
	@businessUnitID int,
	@year int,
	@listPrice decimal,
	@age int,
	@makeName varchar(20),
	@modelName varchar(20),
	@chromeStyleID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- get make, model, and mkt class
	declare @makeID int
	set @makeID = (SELECT top(1) divisionID from vehiclecatalog.chrome.divisions where divisionName = @makeName AND countrycode = 1)

	--REMOVED DUE TO THE WAY CHROME DATA IS STORED AND WE ARE PRESENTING ONLY DISTINCT MODEL NAMES, NOT ALL MODELIDs
	--declare @modelID int
	--set @modelID = (SELECT top(1) modelID from vehiclecatalog.chrome.models where modelName = @modelName AND countrycode = 1)


	declare @marketClassID int
	set @marketClassID = (SELECT top(1) MktClassID from vehiclecatalog.chrome.Styles where StyleID = @chromeStyleID AND countrycode = 1)

	
--section gets templates that fit the vehicle basics
		select adt.templateID from templates.AdTemplates as adt 
			INNER JOIN templates.vehicleProfile AS vp
			ON vp.vehicleProfileId = adt.vehicleProfileId
		where (adt.BusinessUnitID = @businessUnitID OR tr.BusinessUnitID = 100150)
		AND (
				(
					@makeID IN (SELECT * FROM dbo.iter$simple_intlist_to_tbl(vp.makeIDList))
				) 
				OR vp.makeIDList = ''
			)
		AND (
				(
					@modelName IN (SELECT * FROM dbo.iter$simple_stringlist_to_tbl(vp.modelList))
				) 
				OR vp.modelList = ''
			)
		AND (
				(
					@marketClassID IN (SELECT * FROM dbo.iter$simple_intlist_to_tbl(vp.marketClassIdList))
				) 
				OR vp.marketClassIdList = ''
			)
		AND (@year BETWEEN vp.startYear AND vp.endYear)
		AND (@listPrice BETWEEN vp.minPrice AND vp.maxPrice)
		AND (@age BETWEEN vp.minAge AND vp.maxAge)
		

END
GO

GRANT EXECUTE ON [templates].[getMatchingTemplates] TO MerchandisingUser 
GO