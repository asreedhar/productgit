if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[DefaultTemplateMapping#Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[DefaultTemplateMapping#Insert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_defaultTemplateMapping_insert.sql,v 1.2 2010/02/09 22:31:40 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Creates a new template map for a vehicle set.  At least one of the MarketClassId, DivisionId, ModelId, ChromeStyleId
   Must be set to be a valid call
   The procedure returns the newly created mappingId
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId int
	@MarketClassId int = NULL,
	@DivisionId int = NULL,
	@ModelId int = NULL,
	@ModelName varchar(50) = NULL, --modelname allows you to insert for a model across years
	@ChromeStyleId int = NULL,
	@TemplateId int	
 
 * Returns the newly created mappingSetId for linking by themes
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[DefaultTemplateMapping#Insert]
	@BusinessUnitId int,
	@MarketClassId int = NULL,
	@DivisionId int = NULL,
	@ModelId int = NULL,
	@ModelName varchar(50) = NULL,
	@ModelYear int = NULL,
	@Trim varchar(50) = NULL,
	@ChromeStyleId int = NULL,
	@TemplateId int,
	@ThemeSetId int = NULL,
	@VehicleType INT	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF (@ChromeStyleId IS NULL 
		AND @Trim IS NULL
		AND @ModelId IS NULL
		AND @ModelName IS NULL 
		AND @DivisionId IS NULL 
		AND @MarketClassID IS NULL)
	BEGIN
		RAISERROR('Please supply at least one of the vehicle set descriptor fields',16,1)
		RETURN @@ERROR
	END
	
	IF NOT EXISTS (SELECT 1 
					FROM vehicleCatalog.chrome.styles sty
					JOIN vehicleCatalog.chrome.models mdl
						ON mdl.modelID = sty.modelId
					WHERE 
						(
							(@ChromeStyleId IS NULL OR sty.styleId = @ChromeStyleId) AND
							(@Trim IS NULL OR sty.Trim = @Trim) AND
							(@ModelId IS NULL OR mdl.modelId = @ModelId) AND 
							(@ModelName IS NULL OR mdl.ModelName = @ModelName) AND
							(@ModelYear IS NULL OR mdl.ModelYear = @ModelYear) AND
							(@DivisionId IS NULL OR mdl.divisionID = @DivisionId) AND 
							(@MarketClassID IS NULL OR sty.mktClassId = @MarketClassId)
							AND sty.countryCode = 1
							AND mdl.countryCode = 1
						)
					)
	BEGIN
		RAISERROR('Please supply a valid set of vehicle set selectors',16,1)
		RETURN @@ERROR
	END
	
	--when chromestyle is not specifiec, store by modelIds
	IF @ChromeStyleId IS NULL AND @Trim IS NULL
	BEGIN
	
		--first delete any where specificity matches for modelId and businessUnitId
		DELETE FROM templates.DefaultTemplate_Model
		WHERE 
			businessUnitId = @BusinessUnitID
			AND (modelId IN (
								SELECT DISTINCT sty.modelId 
								FROM vehicleCatalog.chrome.styles sty
								JOIN vehicleCatalog.chrome.models mdl
									ON mdl.modelID = sty.modelId
								WHERE 
								(
									(@ModelId IS NULL OR mdl.modelId = @ModelId) AND 
									(@ModelName IS NULL OR mdl.modelName = @ModelName) AND 
									(@ModelYear IS NULL OR mdl.ModelYear = @ModelYear) AND
									(@DivisionId IS NULL OR mdl.divisionID = @DivisionId) AND 
									(@MarketClassID IS NULL OR sty.mktClassId = @MarketClassId)
									AND sty.countryCode = 1
									AND mdl.countryCode = 1
								)
							)
					)
			AND (
					((@modelId IS NOT NULL OR @modelName IS NOT NULL) AND specificityLevel = 3)
					OR (@divisionId IS NOT NULL AND specificityLevel = 2)
					OR (@MarketClassID IS NOT NULL AND specificityLevel = 1)
				)
			AND ( vehicleType = @VehicleType)
					
			
							
		INSERT INTO templates.DefaultTemplate_Model
		(	
			BusinessUnitId,
			ModelId,			
			templateId,
			themeSetId,
			specificityLevel,
			vehicleType
		)
		SELECT 
			DISTINCT
			@BusinessUnitId, 
			sty.modelId, 			
			@TemplateId, 
			@ThemeSetId,
			CASE 
				WHEN (@modelId IS NOT NULL OR @modelName IS NOT NULL) THEN 3
				WHEN @divisionId IS NOT NULL THEN 2
				WHEN @MarketClassID IS NOT NULL THEN 1
				ELSE 0
			END,
			@VehicleType
		FROM 
			vehicleCatalog.chrome.styles sty
			JOIN vehicleCatalog.chrome.models mdl
				ON mdl.modelID = sty.modelId
			WHERE 
			(
				(@ModelId IS NULL OR mdl.modelId = @ModelId) AND 
				(@ModelName IS NULL OR mdl.modelName = @ModelName) AND 
				(@ModelYear IS NULL OR mdl.ModelYear = @ModelYear) AND
				(@DivisionId IS NULL OR mdl.divisionID = @DivisionId) AND 
				(@MarketClassID IS NULL OR sty.mktClassId = @MarketClassId)
				AND sty.countryCode = 1
				AND mdl.countryCode = 1
			)
						
	END
	ELSE IF @ChromeStyleId IS NULL  --occurs when trim is not null, but chromeStyleId is
	BEGIN
		DELETE FROM templates.DefaultTemplate_Vehicle
		WHERE 
			businessUnitId = @BusinessUnitID
			AND (chromeStyleId IN (
								SELECT DISTINCT sty.styleId
								FROM vehicleCatalog.chrome.styles sty
								JOIN vehicleCatalog.chrome.models mdl
									ON mdl.modelID = sty.modelId
								WHERE 
								(
									sty.trim = @Trim AND
									(@ModelId IS NULL OR mdl.modelId = @ModelId) AND 
									(@ModelName IS NULL OR mdl.modelName = @ModelName) AND 
									(@ModelYear IS NULL OR mdl.ModelYear = @ModelYear) AND
									(@DivisionId IS NULL OR mdl.divisionID = @DivisionId) AND 
									(@MarketClassID IS NULL OR sty.mktClassId = @MarketClassId)
									AND sty.countryCode = 1
									AND mdl.countryCode = 1
								)
							)
					)
			AND (vehicleType = @VehicleType)
		
		INSERT INTO templates.DefaultTemplate_Vehicle
		(	
			BusinessUnitId,
			ChromeStyleId,			
			templateId,
			themeSetId,
			vehicleType			
		)
		SELECT
			@BusinessUnitId, 			
			styleId, 
			@TemplateId, 
			@ThemeSetId,
			@VehicleType		

			FROM vehicleCatalog.chrome.styles sty
			JOIN vehicleCatalog.chrome.models mdl
				ON mdl.modelID = sty.modelId
			WHERE 
			(
				sty.trim = @Trim AND
				(@ModelId IS NULL OR mdl.modelId = @ModelId) AND 
				(@ModelName IS NULL OR mdl.modelName = @ModelName) AND 
				(@ModelYear IS NULL OR mdl.ModelYear = @ModelYear) AND
				(@DivisionId IS NULL OR mdl.divisionID = @DivisionId) AND 
				(@MarketClassID IS NULL OR sty.mktClassId = @MarketClassId)
				AND sty.countryCode = 1
				AND mdl.countryCode = 1
			)
		
	END
	ELSE
	BEGIN
		
		if EXISTS (SELECT 1 FROM templates.DefaultTemplate_Vehicle
					WHERE businessUnitId = @BusinessUnitId 
					AND chromeStyleId = @ChromeStyleId AND vehicleType = @VehicleType)
		BEGIN
			UPDATE templates.DefaultTemplate_Vehicle
			SET 	
				templateId = @TemplateId,				
				themeSetId = @ThemeSetId
			WHERE 
				businessUnitId = @BusinessUnitId 
				AND chromeStyleId = @ChromeStyleId
				AND vehicleType = @VehicleType
		END
		ELSE
		BEGIN
			INSERT INTO templates.DefaultTemplate_Vehicle
			(	
				BusinessUnitId,
				ChromeStyleId,			
				templateId,
				themeSetId,
				vehicleType			
			)
			VALUES(
				@BusinessUnitId, 			
				@ChromeStyleId, 
				@TemplateId, 
				@ThemeSetId,
				@VehicleType			
			)
		END
				
	END

END

GO

GRANT EXECUTE ON [templates].[DefaultTemplateMapping#Insert] TO MerchandisingUser 
GO