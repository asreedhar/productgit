if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[getFullTemplatesAndHierarchy]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[getFullTemplatesAndHierarchy]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_getFullTemplatesAndHierarchy.sql,v 1.3 2008/11/20 20:01:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch the full template hierarchy and contents
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[getFullTemplatesAndHierarchy]
	
	@BusinessUnitID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT Template.name as templateName, Template.templateID, TemplateItem.blurbID, TemplateItem.blurbName
    FROM templates.AdTemplates Template
    INNER JOIN (SELECT ti.templateID, 
					BlurbName, 
					ti.BlurbID, 
					ti.Sequence, 
					ti.Required, 
					ti.Priority 
                FROM templates.AdTemplateItems ti 
					INNER JOIN templates.blurbTemplates b
					ON b.BlurbID = ti.BlurbID) AS TemplateItem
		ON Template.templateID = TemplateItem.templateID
    Where Template.BusinessUnitID = @BusinessUnitID
    ORDER BY Template.TemplateID, TemplateItem.Sequence
	FOR XML AUTO, Root('Templates')
	

END
GO

GRANT EXECUTE ON [templates].[getFullTemplatesAndHierarchy] TO MerchandisingUser 
GO