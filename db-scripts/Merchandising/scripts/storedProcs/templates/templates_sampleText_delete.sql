if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[SampleText#Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[SampleText#Delete]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_sampleText_delete.sql,v 1.2 2010/01/13 00:13:06 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Deletes a single item of sample text entry
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit 
 * @BlurbTextID  - integer id of the sample text
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[SampleText#Delete]
	
	@BusinessUnitID int,
	@BlurbTextID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE
    FROM templates.blurbSampleText 
	WHERE BlurbTextID = @BlurbTextID
		AND BusinessUnitID = @BusinessUnitID
		
	IF EXISTS(SELECT 1 FROM templates.BlurbSampleText 
        WHERE businessUnitId = @BusinessUnitId 
        AND blurbTextId = @BlurbTextId)
    BEGIN
        DELETE FROM templates.blurbSampleText WHERE blurbTextId = @BlurbTextId
    END
    ELSE IF EXISTS(SELECT 1 FROM templates.BlurbSampleText 
					WHERE businessUnitId = 100150 
					AND blurbTextId = @BlurbTextId)
    BEGIN
        INSERT INTO templates.BlurbSampleText_DealerOverride 
        (businessUnitId, blurbTextId, enabled)
        VALUES (@BusinessUnitId, @BlurbTextId, 0)            
    END

END
GO

GRANT EXECUTE ON [templates].[SampleText#Delete] TO MerchandisingUser 
GO