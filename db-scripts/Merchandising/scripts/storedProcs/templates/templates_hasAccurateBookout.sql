if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[hasAccurateBookout]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[hasAccurateBookout]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_hasAccurateBookout.sql,v 1.5 2009/01/05 21:23:51 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the latest bookout from kbb
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId - vehicle inventory id
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[hasAccurateBookout]
	
	@BusinessUnitID int,
	@InventoryId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	SET NOCOUNT ON;
	

	
	
	IF EXISTS(SELECT BookValue as [Value]
		FROM [FLDW].dbo.InventoryBookout_F
		WHERE (BusinessUnitID = @BusinessUnitID) 
			AND (InventoryID = @InventoryId)
			AND (BookoutValueTypeID = 2) -- FINAL VALUE
			AND (ThirdPartyCategoryID IN (1,9, 10)) --NADA Retail, KBB RETAIL, and Galves mkt ready
			AND (IsValid = 1)
			AND (IsAccurate = 1)
    )
    	select 1
    ELSE
		select 0
			

END
GO

GRANT EXECUTE ON [templates].[hasAccurateBookout] TO MerchandisingUser 
GO