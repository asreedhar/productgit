if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[reorderTemplateItems]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[reorderTemplateItems]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_reorderTemplateItems.sql,v 1.3 2008/11/20 20:01:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Reorder the sequence of template items
 * 
 * 
 * Parameters
 * ----------
 * @TemplateID - id of template that is being edited
 * @BlurbId1 - id of first blurb/snippet to update
 * @BlurbId2 - id of second blurb/snippet to update
 * @Seq1 - the current (preswap) sequence of the first item
 * @Seq2 - the current (preswap) sequence of the second item
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[reorderTemplateItems]
	@TemplateID int,
	@BlurbId1 int,
	@BlurbId2 int,
	@Seq1 int,
	@Seq2 int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE templates.AdTemplateItems 
		SET sequence = @Seq2 
		WHERE blurbID = @BlurbID1 
			AND templateID = @TemplateID
	UPDATE templates.AdTemplateItems 
		SET sequence = @Seq1 
		WHERE blurbID = @BlurbID2 
			AND templateID = @TemplateID
	

END
GO

GRANT EXECUTE ON [templates].[reorderTemplateItems] TO MerchandisingUser 
GO