if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[getAvailableTemplateItemList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[getAvailableTemplateItemList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_getAvailableTemplateItemList.sql,v 1.4 2009/07/07 14:41:21 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the list of available template items
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[getAvailableTemplateItemList]
	
	@BusinessUnitID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT Category.blurbTypeName, Category.blurbTypeId, BlurbTemplate.blurbName, BlurbTemplate.blurbId
    FROM templates.blurbTypes AS Category 
    INNER JOIN templates.blurbTemplates AS BlurbTemplate
        ON BlurbTemplate.blurbTypeId = Category.blurbTypeId
    WHERE BlurbTemplate.BusinessUnitID = @BusinessUnitID OR BlurbTemplate.BusinessUnitID = 100150
    ORDER BY Category.blurbTypeId, BlurbTemplate.blurbName
    FOR XML Auto, Root('ToolBox')

END
GO

GRANT EXECUTE ON [templates].[getAvailableTemplateItemList] TO MerchandisingUser 
GO