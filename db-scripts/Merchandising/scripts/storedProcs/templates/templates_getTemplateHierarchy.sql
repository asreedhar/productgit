if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[getTemplateHierarchy]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[getTemplateHierarchy]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_getTemplateHierarchy.sql,v 1.5 2009/04/08 23:52:39 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Get the hierarchy of templates for the dealer
 * returns xml of templates
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[getTemplateHierarchy]
	
	@BusinessUnitID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select Template.templateID, 
		Template.name as templateName, 
		level2.templateID, 
		level2.name as templateName, 
		level3.templateID, 
		level3.name as templateName 
        FROM templates.adtemplates Template
        
		--perform two levels of self join
		left join templates.adtemplates level2 
			on Template.templateid = level2.parent
        left join templates.adtemplates level3 
			on level2.templateid = level3.parent
        where Template.parent is null 
			and Template.businessUnitID = @BusinessUnitID 
			
        order by Template.templateid
    FOR XML AUTO, ROOT('Templates')
	

END
GO

GRANT EXECUTE ON [templates].[getTemplateHierarchy] TO MerchandisingUser 
GO