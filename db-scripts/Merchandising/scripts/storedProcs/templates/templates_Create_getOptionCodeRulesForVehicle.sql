if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[getOptionCodeRulesForVehicle]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[getOptionCodeRulesForVehicle]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [templates].[getOptionCodeRulesForVehicle]
	
	@BusinessUnitID int,
	@InventoryID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT mappings.*, rules.*, inv.inventorytype 
		FROM templates.optioncoderulemappings mappings
		INNER JOIN FLDW.dbo.InventoryActive inv ON inv.inventoryid = @inventoryid 
												AND inv.businessunitid = @businessunitid
		INNER JOIN templates.optioncoderules rules ON rules.ruleid = mappings.ruleid 
													AND (mappings.inventoryType = inv.inventoryType OR mappings.InventoryType = 0)
		ORDER BY ASCII(optionCode), LEN(textToReplace) DESC -- specific order to process, by specific option code and biggest phrase first


END
GO

GRANT EXECUTE ON [templates].[getOptionCodeRulesForVehicle] TO MerchandisingUser 
GO
