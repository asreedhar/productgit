if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[insertItem]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[insertItem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_insertItem.sql,v 1.4 2009/01/05 21:23:51 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Inserts a new item into a template
 * 
 * 
 * Parameters
 * ----------
 * 
 * @TemplateID - The id of the template to modify
 * @BlurbID - the id of the blurb/snippet to add to the template
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[insertItem]
	
	@TemplateID int,
	@BlurbID int
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF NOT EXISTS (SELECT 1 FROM templates.AdTemplateItems WHERE templateID = @TemplateID and blurbID = @BlurbID)
    BEGIN
        DECLARE @newSeq int
        set @newSeq = 
			ISNULL((SELECT max(sequence)+1 
			FROM templates.AdTemplateItems 
			WHERE templateID = @TemplateID 
			GROUP BY TemplateID), 1)

        declare @newPri int
        set @newPri = ISNULL((SELECT max(priority)+1 FROM templates.AdTemplateItems WHERE templateID = @TemplateID GROUP BY TemplateID), 1)
        
        INSERT INTO templates.AdTemplateItems (templateID, blurbID, sequence, priority, required)
            VALUES (@TemplateID, @BlurbID, @newSeq, @newPri,0)
    END
	

END
GO

GRANT EXECUTE ON [templates].[insertItem] TO MerchandisingUser 
GO