if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[updateOrCreateTemplateItem]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[updateOrCreateTemplateItem]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_updateOrCreateTemplateItem.sql,v 1.3 2008/11/20 20:01:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * updates or creates a new template item
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit 
 * @BlurbID int, - blurb/snippet id
 * @BlurbName varchar(50), - new blurb name
 * @BlurbTypeID int, - blurb type id
 * @BlurbTemplate varchar(500) = '', - actual template contents before using template conversions
 * @BlurbCondition varchar(500) = '', - actual template conditions before using conversions
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[updateOrCreateTemplateItem]
	
	@BusinessUnitID int,
	@BlurbID int,
	@BlurbName varchar(50),
	@BlurbTypeID int,
	@BlurbTemplate varchar(500) = '',
	@BlurbCondition varchar(500) = ''
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF EXISTS (SELECT blurbId FROM templates.blurbTemplates WHERE blurbId = @BlurbID)
    BEGIN
        UPDATE templates.blurbTemplates 
        SET blurbName = @BlurbName, 
			blurbTemplate = @BlurbTemplate, 
			blurbCondition = @BlurbCondition, 
			blurbTypeId = @BlurbTypeID
        WHERE blurbId = @BlurbID
    END
    ELSE
    BEGIN
        INSERT INTO templates.blurbTemplates 
			(blurbName, 
				blurbTemplate, 
				blurbCondition, 
				blurbTypeId, 
				businessUnitID)
        VALUES (@BlurbName, 
				@BlurbTemplate, 
				@BlurbCondition, 
				@BlurbTypeID, 
				@BusinessUnitID)
    END

END
GO

GRANT EXECUTE ON [templates].[updateOrCreateTemplateItem] TO MerchandisingUser 
GO