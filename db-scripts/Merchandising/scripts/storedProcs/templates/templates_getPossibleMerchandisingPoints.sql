if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[getPossibleMerchandisingPoints]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[getPossibleMerchandisingPoints]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_getPossibleMerchandisingPoints.sql,v 1.3 2008/11/20 20:01:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Gets all merchandising points for a certain rule type
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @RuleType - type of points to retrieve
		--3 basic vehicle information
		
		--4 misc
		--5 consumer info
		--6 carfax info
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[getPossibleMerchandisingPoints]
	
	@BusinessUnitID int,
	@RuleType int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * 
		from Templates.MerchandisingPoints 
		WHERE (BusinessUnitID = 100150 
				OR BusinessUnitID = @BusinessUnitID) 
		AND ruleType = @RuleType	

END
GO

GRANT EXECUTE ON [templates].[getPossibleMerchandisingPoints] TO MerchandisingUser 
GO