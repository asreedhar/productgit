if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[SampleTextLibrary#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[SampleTextLibrary#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_SampleTextLibrary_fetch.sql,v 1.1 2010/01/13 00:13:06 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Gets all sample texts that apply to a blurb/snippet - accounts for FirstLook library or dealer library
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @BlurbID - the id of the blurb/snippet to use to get the sample text entries 
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[SampleTextLibrary#Fetch]
	
	@BusinessUnitID int,
	@LibraryBusinessUnitId int,
	@BlurbID int,
	@ThemeID int = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	Select 
		ISNULL(vp.title, '') as Title, 
        ISNULL(bst.vehicleProfileId,5) as vehicleProfileId, 
        thm.id as themeId,
        thm.name as themeName,
        blurbPreText, 
        blurbPostText,
        bst.blurbTextId,
        coalesce(bstdo.enabled, active) as active
        
    FROM templates.blurbSampleText bst
    LEFT JOIN templates.vehicleProfiles vp
		ON vp.vehicleProfileId = bst.vehicleProfileId
    LEFT JOIN templates.themes thm
		ON thm.ID = bst.themeId
    LEFT JOIN templates.BlurbSampleText_DealerOverride bstdo                    
		ON bstdo.businessUnitId = @BusinessUnitId
        AND bstdo.blurbTextId = bst.blurbTextId                   
    WHERE bst.businessUnitId = @LibraryBusinessUnitId
        AND bst.blurbId = @BlurbId
        AND ((@ThemeId IS NULL) OR (thm.id = @ThemeId))   
                                   

END
GO

GRANT EXECUTE ON [templates].[SampleTextLibrary#Fetch] TO MerchandisingUser 
GO