if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[Template#Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[Template#Update]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_template_update.sql,v 1.2 2009/08/19 21:27:28 jkelley Exp $$
 * 
 * Summary
 * -------
 * 
 * Update a template's details
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitID     - integer id of the businessUnit
 * @TemplateID  - integer id of the template to fetch
 * @VehicleProfileId - id of vehicle profile to use for template (nullable)
 * 
 * 
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[Template#Update]
	
	@BusinessUnitID int,
	@TemplateID int,
	@Name varchar(50),
	@Active bit,
	@VehicleProfileId int = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	UPDATE templates.AdTemplates 
	SET [name] = @Name, 
		active = @Active,
		vehicleProfileId = @VehicleProfileId
	WHERE templateId = @TemplateId
	AND businessUnitId = @BusinessUnitId
		

END
GO

GRANT EXECUTE ON [templates].[Template#Update] TO MerchandisingUser 
GO