if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[getCategoryExclusionMap]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[getCategoryExclusionMap]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_getCategoryExclusionMap.sql,v 1.3 2008/11/20 20:01:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * get the mapping for which chrome categories to exclude in case of overlap
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[getCategoryExclusionMap]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT * from templates.EquipmentExclusionMap
	

END
GO

GRANT EXECUTE ON [templates].[getCategoryExclusionMap] TO MerchandisingUser 
GO