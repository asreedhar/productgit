if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[getLatestKbbConsumerValue]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[getLatestKbbConsumerValue]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Vivek, Rane>
-- Create date: <22/10/15>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_getLatestKbbConsumerValue.sql,v 1.2 2009/03/13 21:22:35 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the latest bookout from kbb
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId - vehicle inventory id

 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[getLatestKbbConsumerValue]
	
	@BusinessUnitID int,
	@InventoryId int	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	   SELECT bo.BookValue  		
        FROM [FLDW].dbo.InventoryKbbConsumerValue_F bo
        WHERE (bo.BusinessUnitID = @BusinessUnitID) 
            AND (bo.inventoryId = @InventoryId)             
	
END
GO

GRANT EXECUTE ON [templates].[getLatestKbbConsumerValue] TO MerchandisingWebsite 
GO