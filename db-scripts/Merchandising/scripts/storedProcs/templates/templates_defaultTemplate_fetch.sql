if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[DefaultTemplate#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[DefaultTemplate#Fetch]
GO

/****** Object:  StoredProcedure [templates].[DefaultTemplate#Fetch]    Script Date: 10/26/2008 13:10:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Jonathan Kelley>
-- Create date: <Aug 27 208>
-- Description:	<gets appropriate templateIDs for the vehicle configuration>
-- =============================================

CREATE PROCEDURE [templates].[DefaultTemplate#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@ChromeStyleID INT,
	@VehicleType Int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @templateId int, @themeSetId INT, @anyVehicleType INT
	SET @anyVehicleType = 0;
	
	--get mappingId and themeSetId from:
	--1) dealer customized by styleId
	--2) dealer customized by modelId
	--3) FirstLook defaults for styleId
	--4) FirstLook defaults for modelId
	--if none of these are present, there will be no returned rows...so no default template/theme
	
	if exists (
	
		select 1 
		from templates.DefaultTemplate_Vehicle vdt
		JOIN templates.AdTemplates adt ON adt.templateID = vdt.templateId
		where 
			vdt.businessUnitId = @BusinessUnitId and 
			vdt.chromeStyleId = @ChromeStyleId AND 
			(vdt.vehicleType = @VehicleType OR vdt.vehicleType = @anyVehicleType)
			AND adt.active = 1
	
	)
	BEGIN
		
		select top(1) @templateId = vdt.templateId, @themeSetId = vdt.themeSetId
		from templates.DefaultTemplate_Vehicle vdt
		JOIN templates.AdTemplates adt ON adt.templateID = vdt.templateId
		where 
			vdt.businessUnitId = @BusinessUnitId and 
			vdt.chromeStyleId = @ChromeStyleId AND 
			adt.active = 1 and
			(vdt.vehicleType = @VehicleType OR vdt.vehicleType = @anyVehicleType)
		
	END
	ELSE IF exists (
		
		select 1 
		from templates.DefaultTemplate_Model vdt
		JOIN templates.AdTemplates adt ON adt.templateID = vdt.templateId
		where 
			vdt.businessUnitId = @BusinessUnitId AND 
			(vdt.vehicleType = @VehicleType OR vdt.vehicleType = @anyVehicleType) and 
			adt.active = 1 and
			modelId = (select modelId from vehicleCatalog.chrome.styles where styleId = @ChromeStyleId)
		
		)
	BEGIN
		select top(1) @templateId = vdt.templateId, @themeSetId = vdt.themeSetId
		from templates.DefaultTemplate_Model vdt
		JOIN templates.AdTemplates adt ON adt.templateID = vdt.templateId
		where 
			vdt.businessUnitId = @BusinessUnitId and 
			(vdt.vehicleType = @VehicleType OR vdt.vehicleType = @anyVehicleType) and 
			adt.active = 1 and
			modelId = (SELECT modelId from vehicleCatalog.chrome.styles where styleId = @ChromeStyleId)
		ORDER BY specificityLevel DESC  --higher is more specific	
	END
	ELSE IF exists ( 
					SELECT 1 
					FROM templates.DefaultTemplate_Vehicle vdt
					JOIN templates.AdTemplates adt 
						ON vdt.templateId = adt.sourceTemplateId 
						AND adt.businessUnitId = @BusinessUnitId
					WHERE (	(vdt.businessUnitId = 100150 OR vdt.BusinessUnitId = 101621) AND 
							chromeStyleId = @ChromeStyleId AND 
							adt.active = 1 AND
							(vehicleType = @VehicleType OR vehicleType = @anyVehicleType)									
					)--is there a firstLook default that maps to this dealer's templates? 
					)
	BEGIN
		SELECT TOP(1) 
			@templateId = adt.templateId, 
			@themeSetId = themeSetId
		FROM templates.DefaultTemplate_Vehicle vdt
		JOIN templates.AdTemplates adt 
			ON vdt.templateId = adt.sourceTemplateId 
			AND adt.businessUnitId = @BusinessUnitId
		WHERE (	(vdt.businessUnitId = 100150 OR vdt.BusinessUnitId = 101621) AND 
				chromeStyleId = @ChromeStyleId AND 
				adt.active = 1 AND
				(vehicleType = @VehicleType OR vehicleType = @anyVehicleType))
	END
	ELSE
	BEGIN --note this looks for dealer templates that map to the firstlook defaults
		select top(1) @templateId = adt.templateId, @themeSetId = themeSetId
		from templates.DefaultTemplate_Model vdt
		JOIN templates.AdTemplates adt 
			ON vdt.templateId = adt.sourceTemplateId 
			AND adt.businessUnitId = @BusinessUnitId					
		where ( 
			(vdt.businessUnitId = 100150 OR vdt.BusinessUnitId = 101621) and 
			(vehicleType = @VehicleType OR vehicleType = @anyVehicleType) and 
			adt.active = 1 and
			modelId = (SELECT modelId from vehicleCatalog.chrome.styles where styleId = @ChromeStyleId))
		ORDER BY specificityLevel DESC
	END
	
	--get the default templateId
	select @templateId as templateId, 1 as pricingAggressiveness
	 
	--get the matching themes (possibly many for future support)
	select * from templates.themeSet
	where themeSetId = @themeSetId

END

GO

GRANT EXECUTE ON [templates].[DefaultTemplate#Fetch] TO MerchandisingUser 
GO