if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[VehicleProfile#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[VehicleProfile#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_vehicleProfile_fetch.sql,v 1.1 2009/01/19 19:51:23 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the rules for applying a template
 * 
 * 
 * Parameters
 * ----------
 * 
 * @TemplateID - integer id of the template for which to fetch the rules
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[VehicleProfile#Fetch]
	
	@VehicleProfileID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT vp.* FROM Templates.VehicleProfiles vp
                WHERE vehicleProfileID = @VehicleProfileID

END
GO

GRANT EXECUTE ON [templates].[VehicleProfile#Fetch] TO MerchandisingUser 
GO