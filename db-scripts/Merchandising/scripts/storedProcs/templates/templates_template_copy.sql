if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[Template#Copy]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[Template#Copy]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_template_copy.sql,v 1.1 2009/08/19 21:27:28 jkelley Exp $$
 * 
 * Summary
 * -------
 * 
 * Create a new template
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitID     - integer id of the businessUnit
 * @Name  - name of the new template
 * 
 * 
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[Template#Copy]
	
	@BusinessUnitId int,
	@TemplateIdToCopy int,
	@NameOfNewTemplate varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	DECLARE @NewTemplateId INT;
	DECLARE @NewProfileId INT;
	
	insert Merchandising.templates.VehicleProfiles
	( 
		businessUnitId,
		title,
		makeIDList,
		modelList,
		marketClassIdList,
		startYear,
		endYear,
		minAge,
		maxAge,
		minPrice,
		maxPrice,
		certifiedStatusFilter,
		trimList,
		segmentIdList,
		vehicleType
	)
	select
		@BusinessUnitID,
		vp.title,
		vp.makeIDList,
		vp.modelList,
		vp.marketClassIdList,
		vp.startYear,
		vp.endYear,
		vp.minAge,
		vp.maxAge,
		vp.minPrice,
		vp.maxPrice,
		vp.certifiedStatusFilter,
		vp.trimList,
		vp.segmentIdList,
		vp.vehicleType	
	from
		Merchandising.templates.AdTemplates at
		join Merchandising.templates.VehicleProfiles vp
			on at.vehicleProfileId = vp.vehicleProfileID
	where at.templateID = @TemplateIdToCopy
	
	SELECT @NewProfileId = SCOPE_IDENTITY()
	
	INSERT INTO [templates].[AdTemplates] ([name], [businessUnitId], vehicleProfileID, parent, SourceTemplateId)
	SELECT @NameOfNewTemplate, @BusinessUnitId, @NewProfileId, parent, @TemplateIdToCopy
	FROM [templates].[AdTemplates]
	WHERE templateId = @TemplateIdToCopy
	
	select @NewTemplateId = SCOPE_IDENTITY()

	INSERT INTO [templates].[AdTemplateItems] (templateID, blurbId, [sequence],priority, required, parentID)
	SELECT @NewTemplateId, blurbId, [sequence],priority,required,parentID
	FROM [templates].[AdTemplateItems]
	WHERE templateId = @TemplateIdToCopy
	
	select @NewTemplateId
	
END
GO

GRANT EXECUTE ON [templates].[Template#Copy] TO MerchandisingUser 
GO