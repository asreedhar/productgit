if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[insertSnippetText]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[insertSnippetText]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_insertSnippetText.sql,v 1.6 2009/10/06 14:40:52 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Inserts a new sample text for a blurb/snippet
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit 
 * @BlurbID int,   - integer blurb/snippet id
 * @VehicleProfileId int - NULL - profile that applies to the text
 * @BlurbPreText varchar(500), -- text to precede the template body
 * @BlurbPostText varchar(500) -- text to follow the template body
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[insertSnippetText]
	
	@BusinessUnitID int,
	@BlurbID int,
	@VehicleProfileId int = NULL,
	@BlurbPreText varchar(500),
	@BlurbPostText varchar(500),
	@IsLongForm bit = 0,
	@ThemeId int = NULL,
	@IsHeader bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	INSERT INTO templates.blurbSampleText 
        (blurbID, vehicleProfileId, blurbPreText, blurbPostText, businessUnitID, isLongForm, themeId, isHeader) 
        VALUES (@BlurbID, @VehicleProfileId, @BlurbPreText, @BlurbPostText, @BusinessUnitID, @IsLongForm, @ThemeId, @IsHeader)
	

END
GO

GRANT EXECUTE ON [templates].[insertSnippetText] TO MerchandisingUser 
GO