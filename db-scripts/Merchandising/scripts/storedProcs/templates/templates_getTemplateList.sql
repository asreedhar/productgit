if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[getTemplateList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[getTemplateList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_getTemplateList.sql,v 1.8 2009/09/14 14:42:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Get the list of template names and ids for a business unit
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[getTemplateList]
	
	@BusinessUnitID int,
	@IncludeSystem bit = 0,
	@IncludeActive bit = 1,
	@IncludeInactive bit = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @parentBuid int
	
	SELECT @parentBuid = bur.parentId 
	FROM Interface.BusinessUnit bu
	JOIN Interface.BusinessUnitRelationship bur
		ON bur.businessUnitId = bu.businessUnitId
	WHERE bu.businessunitid = @BusinessUnitID
	
	SELECT Template.name as templateName, Template.templateID
        FROM templates.AdTemplates Template
        WHERE 
        (
			Template.BusinessUnitID = @BusinessUnitID 
			OR (NOT @parentBuid IS NULL AND Template.BusinessUnitId = @parentBuid)
			OR (@IncludeSystem = 1 AND Template.BusinessUnitId = 100150)
		)
        AND (@IncludeInactive = 1 OR active <> 0)
        AND (@IncludeActive = 1 OR active <> 1)
        ORDER BY Template.TemplateID
	

END
GO

GRANT EXECUTE ON [templates].[getTemplateList] TO MerchandisingUser 
GO