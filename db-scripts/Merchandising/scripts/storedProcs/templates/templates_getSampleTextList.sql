if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[getSampleTextList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[getSampleTextList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_getSampleTextList.sql,v 1.9 2009/10/06 14:40:52 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Gets all sample texts that apply to a blurb/snippet
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @BlurbID - the id of the blurb/snippet to use to get the sample text entries 
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[getSampleTextList]
	
	@BusinessUnitID int,
	@BlurbID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	SELECT blurbTextId, 
		blurbPreText, 
		blurbPostText, 
		bst.active, 
		bst.isLongForm,
		vp.vehicleProfileId,
		vp.title,
		vp.makeIdList,
		vp.modelList,
		vp.trimList,
		vp.segmentIdList,
		vp.marketClassIdList,
		vp.minAge,
		vp.maxAge,
		vp.startYear,
		vp.endYear,
		vp.minPrice,
		vp.maxPrice,
		vp.certifiedStatusFilter,
		vp.vehicleType,
		ISNULL(makeIdList,'') as makeIdList, 
		ISNULL(modelList,'') as modelList, 
		ISNULL(trimList,'') as trimList,
		ISNULL(segmentIdList,'') as segmentIdList,
		ISNULL(marketClassIdList,'') as marketClassIdList,
		startYear,
		endYear,
		minAge,
		maxAge,
		minPrice,
		maxPrice,
		thm.name as themeName,
		thm.id as themeId,
		isHeader
		FROM templates.blurbSampleText bst 
		LEFT JOIN templates.vehicleProfiles vp
		ON vp.vehicleProfileId = bst.vehicleProfileId
		LEFT JOIN templates.themes thm
		on thm.id = bst.themeId
		WHERE blurbID = @BlurbID 
			AND bst.businessUnitID = @BusinessUnitID

END
GO

GRANT EXECUTE ON [templates].[getSampleTextList] TO MerchandisingUser 
GO