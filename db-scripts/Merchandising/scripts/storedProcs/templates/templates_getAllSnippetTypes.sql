if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[getAllSnippetTypes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[getAllSnippetTypes]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_getAllSnippetTypes.sql,v 1.3 2008/11/20 20:01:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Gets all snippet types available
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[getAllSnippetTypes]
		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
SELECT blurbTypeId, blurbTypeName 
from Templates.BlurbTypes


END
GO

GRANT EXECUTE ON [templates].[getAllSnippetTypes] TO MerchandisingUser 
GO