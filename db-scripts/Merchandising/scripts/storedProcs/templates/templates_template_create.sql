if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[Template#Create]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[Template#Create]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_template_create.sql,v 1.1 2009/01/19 19:51:23 jkelley Exp $$
 * 
 * Summary
 * -------
 * 
 * Create a new template
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitID     - integer id of the businessUnit
 * @Name  - name of the new template
 * 
 * 
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[Template#Create]
	
	@BusinessUnitID int,
	@Name varchar(50)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	INSERT INTO templates.AdTemplates (businessUnitId, [name])
	VALUES (@BusinessUnitId, @Name)	

END
GO

GRANT EXECUTE ON [templates].[Template#Create] TO MerchandisingUser 
GO