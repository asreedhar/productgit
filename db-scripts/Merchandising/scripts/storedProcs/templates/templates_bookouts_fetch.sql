if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[Bookouts#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[Bookouts#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <1/5/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_bookouts_fetch.sql,v 1.1 2009/01/19 19:51:23 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch all bookouts for the vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId - vehicle inventory id
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[Bookouts#Fetch]
	
	@BusinessUnitID int,
	@InventoryId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

	SELECT bo.BookValue,
		bo.BookoutValueTypeID,
		bo.ThirdPartyCategoryID,
		bo.IsValid,
		bo.IsAccurate,
		bo.UnitCost,
		ia.ListPrice
        FROM fldw.dbo.InventoryActive ia 
        JOIN [FLDW].dbo.InventoryBookout_F bo
			ON bo.BusinessUnitID = ia.BusinessUnitID
			AND bo.InventoryID = ia.InventoryID
        WHERE (bo.BusinessUnitID = @BusinessUnitID) 
		AND (bo.inventoryId = @InventoryId)
        AND bo.ThirdPartyCategoryID < 19 -- max has enums for the first 18 categories (bookouts)
END
GO

GRANT EXECUTE ON [templates].[Bookouts#Fetch] TO MerchandisingUser 
GO