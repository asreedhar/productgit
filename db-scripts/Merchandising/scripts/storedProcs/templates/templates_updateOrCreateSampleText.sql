if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[updateOrCreateSampleText]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[updateOrCreateSampleText]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_updateOrCreateSampleText.sql,v 1.8 2010/01/13 00:13:06 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * updates or creates blurb sample text
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit
 * @BlurbID int,  - id of blurb to which to attache the sample text
 * @BlurbTextID int = -1, - id of blurb text to update (-1 for new)
 * @BlurbPreText varchar(500) = '', - text to preceed teh template body
 * @BlurbPostText varchar(500) = '' - text to follow the template body
 * @IsLongForm bit - if the sample text applies to the long form version only 
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[updateOrCreateSampleText]
	
	@BusinessUnitID int,
	@BlurbID int,
	@BlurbTextID int = -1,
	@VehicleProfileId int = NULL,
	@BlurbPreText varchar(500) = '',
	@BlurbPostText varchar(500) = '',
	@Active bit = 1,
	@IsLongForm bit = 0,
	@ThemeId int = NULL,
	@IsHeader bit = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF EXISTS(SELECT 1 FROM templates.blurbSampleText where blurbID = @BlurbID AND BlurbTextID = @BlurbTextID AND BusinessUnitID = @BusinessUnitID)
        BEGIN
        UPDATE templates.blurbSampleText 
			SET blurbPreText = @BlurbPreText, 
				blurbPostText = @BlurbPostText,
				vehicleProfileId = @VehicleProfileId,
				active = @Active,
				isLongForm = @IsLongForm,
				themeId = @ThemeId,
				isHeader = @IsHeader
            WHERE BlurbTextID = @BlurbTextID 
				AND businessUnitID = @BusinessUnitID
        END
      ELSE
      BEGIN
        INSERT INTO templates.blurbSampleText 
			(blurbID, 
				blurbPreText,	
				blurbPostText, 
				vehicleProfileId,
				businessUnitID,
				active,
				isLongForm,
				themeId,
				isHeader) 
		VALUES 
			(@BlurbID, 
				@BlurbPreText, 
				@BlurbPostText, 
				@VehicleProfileId,
				@BusinessUnitID,
				@Active,
				@IsLongForm,
				@ThemeId,
				@IsHeader)
      END 
      
    -- TH: 2014-04-23
	-- Fogbugz case: 29058 - push MaxAd Tagline to Merchandising Tagline for MMS and Website 2.0
	--
    -- First we need the OwnerId
	Declare @Results Table (
		[Id]                    INT             NOT NULL,
		[Handle]                VARCHAR(36)     NOT NULL,
		[Name]                  VARCHAR(200)    NOT NULL,
		[OwnerEntityTypeId]     INT             NOT NULL,
		[OwnerEntityId]         INT             NOT NULL
	);
	
	DECLARE	@OwnerId int, 
			@OwnerHandle varchar(36);
	
	INSERT INTO @Results
	EXEC [Market].[Pricing].[Owner#FetchByDealerId] @BusinessUnitId
	
	SELECT @OwnerId = Id, @OwnerHandle = Handle FROM @Results
	
	-- If we have an ownerid and this ThemeId is "All Cars" 
	IF(@OwnerId > 0 AND (@ThemeId IS NULL OR @ThemeId = 5))
	BEGIN
		IF NOT EXISTS ( SELECT * FROM IMT.Marketing.DealerGeneralPreference WHERE OwnerID = @OwnerId)
		BEGIN
			DECLARE @DealerResults TABLE
			(
				Name VARCHAR(64),
				Address1 VARCHAR(100),
				Address2 VARCHAR(100),
				City VARCHAR(50),
				State VARCHAR(3),
				ZipCode VARCHAR(10),
				PhoneNumber	VARCHAR(50)
			)
			INSERT INTO @DealerResults
			exec IMT.Marketing.DealerGeneralPreference#Create @OwnerHandle -- "Create" doesn't actually create the record. It just returns data that *should* create the record
			
			IF EXISTS(SELECT * FROM @DealerResults)
			BEGIN
				DECLARE @AddressLine1 VARCHAR(500),
						@AddressLine2 VARCHAR(500),
						@City VARCHAR(500),
						@DisplayContactInfoOnWebPDF BIT,
						@StateCode VARCHAR(2),
						@ZipCode VARCHAR(5),
						@Url VARCHAR(500),
						@SalesEmailAddress VARCHAR(500),
						@SalesPhoneNumber VARCHAR(14),
						@ExtendedTagLine VARCHAR(250),
						@DaysValidFor INT,
						@Disclaimer VARCHAR(500),    
						@InsertUser VARCHAR(80);
						
				SELECT TOP 1 @AddressLine1=Address1, @AddressLine2=Address2, @City=City, @StateCode=State, @ZipCode=ZipCode, @SalesPhoneNumber=PhoneNumber
				FROM @DealerResults;
				
				exec IMT.Marketing.DealerGeneralPreference#Insert @OwnerHandle,	-- "Insert" actuall creates the record
					@AddressLine1,
					@AddressLine2,
					@City,
					0,
					@StateCode,
					@ZipCode,
					'',
					'',
					@SalesPhoneNumber,
					'',
					0,
					'',
					'max_user'
			END
		END
        
		-- Update the dealer's ExtendedTagLine with this new text
		UPDATE IMT.Marketing.DealerGeneralPreference
		SET ExtendedTagLine = @blurbPreText
		WHERE OwnerID = @OwnerId
	END
END

GO

GRANT EXECUTE ON [templates].[updateOrCreateSampleText] TO MerchandisingUser 
GO