
/****** Object:  StoredProcedure [templates].[getLatestEdmundsValue]    Script Date: 12/29/2014 06:54:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[templates].[getLatestEdmundsValue]') AND type in (N'P', N'PC'))
DROP PROCEDURE [templates].[getLatestEdmundsValue]
GO

/****** Object:  StoredProcedure [templates].[getLatestEdmundsValue]    Script Date: 12/29/2014 06:54:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Vivek Rane
-- Create date: 2014/12/23
-- Description:	This stored procedure returns Edmunds value based on Inventory ID and BusinessUnitId
-- =============================================
CREATE PROCEDURE [templates].[getLatestEdmundsValue]
	@BusinessUnitID int,
	@InventoryId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE
	@OwnerTypeID INT 
	
	SELECT @OwnerTypeID=OwnerTypeID from market.Pricing.Owner WHERE OwnerEntityID=@BusinessUnitID

	SELECT market.Pricing.NewGetEdmundsTMVValueByVehicle(@OwnerTypeID,@BusinessUnitID,1,@InventoryId) edmundsTMV 


END

GO

GRANT execute on templates.getLatestEdmundsValue to merchandisingwebsite

GO

