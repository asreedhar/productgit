if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[getAllConditions]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[getAllConditions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_getAllConditions.sql,v 1.3 2008/11/20 20:01:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Gets all conditions that can be applied to a template for a dealer
 * Currently based on the conditions used to date should reduce to pull all available from a separate table
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[getAllConditions]
	
	@BusinessUnitID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT DISTINCT substring(BlurbCondition, 2, 
		case 
			when (0 < len(BlurbCondition)-2) then 
				len(BlurbCondition)-2 
			else 0 
		end) as BlurbCondition 
	FROM Templates.BlurbTemplates 
	WHERE (BusinessUnitID = 100150 
			OR BusinessUnitID = @BusinessUnitID)
	

END
GO

GRANT EXECUTE ON [templates].[getAllConditions] TO MerchandisingUser 
GO