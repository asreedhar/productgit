if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[VehicleProfile#UpdateOrCreate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[VehicleProfile#UpdateOrCreate]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_vehicleProfile_updateOrCreate.sql,v 1.3 2009/06/22 17:22:10 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the vehicle matching profile
 * 
 * 
 * Parameters
 * ----------
 * ALL params can be null except templateID and should be null if they 
 * should be ignored by the rule
 * @VehicleProfileID - id of template for which to create the rules
 *	@makeIDList - comma separated list of chrome makeIDs
 *	@modelList - comma separated list of chrome modelNames
 *	@marketClassList - comma separated list of chrome marketClassIDs
 *	@startYear int - first year to accept for rule, 
 *	@endYear int - last year to accept for rule, 
 *  @minAge int - min age in days for rule
 *  @maxAge int - max age in days for rule
 *	@minPrice decimal - min price to accept for rule, 
 *	@maxPrice decimal - max price to accept for rule
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Templates].[VehicleProfile#UpdateOrCreate]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@VehicleProfileId int = NULL,
	@Title varchar(30),
	@makeIDList varchar(max), 
	@modelList varchar(max), 
	@trimList varchar(max),
	@segmentIdList varchar(max),
	@marketClassIdList varchar(max), 	
	@startYear int, 
	@endYear int, 
	@minAge int,
	@maxAge int,
	@minPrice decimal, 
	@maxPrice decimal,
	@certifiedStatusFilter BIT,
	@vehicleType int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @retId int
	select @retId = -1
	
	IF EXISTS (SELECT 1
				FROM Templates.VehicleProfiles
				WHERE vehicleProfileID = @VehicleProfileId 
				AND businessUnitId = @BusinessUnitId)
		BEGIN
			UPDATE Templates.VehicleProfiles
				SET title = @Title, makeIDList = @makeIDList, modelList = @modelList, trimList = @trimList,
					segmentIdList = @segmentIdList, marketClassIdList = @marketClassIdList, startYear = @startYear, endYear = @endYear, 
					minAge = @minAge, maxAge = @maxAge, minPrice = @minPrice, maxPrice = @maxPrice,
					certifiedStatusFilter = @certifiedStatusFilter, vehicleType = @vehicleType
					
				WHERE vehicleProfileID = @VehicleProfileId AND businessUnitId = @BusinessUnitId
		END
	ELSE
		BEGIN
			INSERT INTO Templates.VehicleProfiles
			(businessUnitId, title, makeIDList, modelList, trimList, segmentIdList, marketClassIdList, startYear, endYear, 
				minAge, maxAge, minPrice, maxPrice, certifiedStatusFilter, vehicleType)
			VALUES (@BusinessUnitId, @Title, @makeIDList, @modelList, @trimList, @segmentIdList, @marketClassIdList, @startYear, @endYear, 
				@minAge, @maxAge, @minPrice, @maxPrice, @certifiedStatusFilter, @vehicleType)
			select @retId = CAST(SCOPE_IDENTITY() as INT)
		END
	select @retId
END
GO

GRANT EXECUTE ON [templates].[VehicleProfile#UpdateOrCreate] TO MerchandisingUser 
GO