if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[getTemplateItem]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[getTemplateItem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_getTemplateItem.sql,v 1.8 2009/06/22 17:22:10 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Gets a single snippet and sample text
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @BlurbId - the blurb/snippet to retrieve
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[getTemplateItem]
	
	@BusinessUnitID int,
	@BlurbID int,
	@IncludeDefaults bit = 1
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT bt.*, 
        ISNULL(st.blurbTextID, 0) as blurbTextID, 
        ISNULL(st.blurbPreText,'') as blurbPreText,  
        ISNULL(st.blurbPostText,'') as blurbPostText, 
        ISNULL(st.isLongForm,0) as isLongForm,
        ISNULL(vp.makeIdList,'') as makeIdList,
        ISNULL(vp.modelList,'') as modelList,
        ISNULL(vp.trimList,'') as trimList,
        ISNULL(vp.segmentIdList,'') as segmentIdList,
        ISNULL(vp.marketClassIdList,'') as marketClassIdList,
        vp.minPrice, vp.maxPrice, vp.minAge, vp.maxAge, vp.startYear, vp.endYear, vp.certifiedStatusFilter
        from templates.blurbTemplates bt
        LEFT JOIN templates.blurbSampleText st on st.blurbId = bt.blurbId
        LEFT JOIN templates.vehicleProfiles vp on st.vehicleProfileId = vp.vehicleProfileId
        
        WHERE bt.blurbId = @BlurbID 
        AND (bt.businessUnitId = @BusinessUnitID OR (@IncludeDefaults = 1 AND bt.businessUnitId = 100150))  --grab FirstLook blurbs if defaults included
        AND (st.active = 1 OR (@IncludeDefaults = 1 AND st.active IS NULL)) --grab any without any sample text if defaults are considered

END
GO

GRANT EXECUTE ON [templates].[getTemplateItem] TO MerchandisingUser 
GO