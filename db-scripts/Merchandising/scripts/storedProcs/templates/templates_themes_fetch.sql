if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[themes#fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[themes#fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/3/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_themes_fetch.sql,v 1.1 2009/10/06 14:40:52 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch active themes for this dealer
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[themes#fetch]
	@BusinessUnitID int = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * from templates.themes
	where (@BusinessUnitId IS NULL OR businessUnitId = @BusinessUnitId)

END
GO

GRANT EXECUTE ON [templates].[themes#fetch] TO MerchandisingUser 
GO