if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[getLatestBookoutValue]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[getLatestBookoutValue]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_getLatestBookoutValue.sql,v 1.2 2009/03/13 21:22:35 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the latest bookout from kbb
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId - vehicle inventory id
 * @ThirdPartyCategoryId
 * @ValueType
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[getLatestBookoutValue]
	
	@BusinessUnitID int,
	@InventoryId int,
	@ThirdPartyCategoryId int,
	@ValueType int = 2
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	SELECT bo.bookoutValueTypeId
		, bo.bookValue
		,bo.isAccurate
		,bo.isValid
		,bo.ThirdPartyCategoryID
		
        FROM [FLDW].dbo.InventoryBookout_F bo
        WHERE (bo.BusinessUnitID = @BusinessUnitID) 
            AND (bo.inventoryId = @InventoryId) 
			AND (bo.bookoutValueTypeID = @ValueType) --FINAL VALUE
            AND (bo.thirdPartyCategoryID = @ThirdPartyCategoryId)
            
	
END
GO

GRANT EXECUTE ON [templates].[getLatestBookoutValue] TO MerchandisingUser 
GO