if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[getVehicleCountBelowBook]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [templates].[getVehicleCountBelowBook]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: templates_getVehicleCountBelowBook.sql,v 1.4 2008/11/25 14:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the latest bookout from kbb
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [templates].[getVehicleCountBelowBook]
	
	@BusinessUnitID int,
	@minAmountBelowBookToInclude decimal = 1.0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	SELECT count(*) 
		FROM (
		SELECT ia.inventoryId, bookValue as value, listPrice	
        FROM FLDW.dbo.InventoryActive ia  
        JOIN [FLDW].dbo.InventoryBookout_F b 
			ON b.BusinessUnitID = ia.BusinessUnitID
			AND b.InventoryID = ia.InventoryID
        WHERE ia.BusinessUnitID = @BusinessUnitID
--            AND (ThirdPartyID = 2) --KBB (add other books?) (3,4)?
            AND (bookoutValueTypeID = 2) --Final Value
            AND (thirdPartyCategoryID = 1) --Retail (add for other books?) (9,10)?
            AND (IsValid = 1)
            AND (IsAccurate = 1)
            AND BookValue > 100
			AND	ListPrice > 100
			AND BookValue - listPrice  > @minAmountBelowBookToInclude
		) as tbl
		
END
GO

GRANT EXECUTE ON [templates].[getVehicleCountBelowBook] TO MerchandisingUser 
GO