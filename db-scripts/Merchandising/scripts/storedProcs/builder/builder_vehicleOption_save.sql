if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleOption#Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[VehicleOption#Save]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_vehicleOption_save.sql,v 1.3 2009/04/06 22:56:21 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * save a vehicle option
 * 
 * 
 * Parameters
 * ----------
 * 
	@BusinessUnitId int,
	@InventoryId int,
	@OptionID int,
	@OptionCode varchar(20),
	@OptionText varchar(300),
	@DetailText varchar(300)
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[VehicleOption#Save]
	
	@BusinessUnitId int,
	@InventoryId int,
	@OptionID int,
	@OptionCode varchar(20),
	@OptionText varchar(300),
	@DetailText varchar(300),
	@IsStandardEquipment bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
		IF not exists (select 1 
						from builder.vehicleoptions 
							where businessunitid=@BusinessUnitID 
								and InventoryId = @InventoryId 
								and optionid=@OptionID)
        BEGIN
            INSERT INTO builder.VehicleOptions
            (BusinessUnitID, InventoryId, OptionID, OptionCode, OptionText, DetailText, IsStandardEquipment) 
            VALUES (@BusinessUnitID, @InventoryId, @OptionID,
					@OptionCode,@OptionText,@DetailText, @IsStandardEquipment)
        END
        ELSE
        BEGIN
            UPDATE builder.VehicleOptions
            SET optionCode = @OptionCode, 
				detailText = @DetailText,
                optionText = @OptionText,
                IsStandardEquipment = @IsStandardEquipment
            WHERE BusinessUnitID = @BusinessUnitID
                AND InventoryId = @InventoryId
                AND OptionID = @OptionID
        END
			
END
GO

GRANT EXECUTE ON [builder].[VehicleOption#Save] TO MerchandisingUser 
GO