USE [Merchandising]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[AutoloadStatus#Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[AutoloadStatus#Save]
GO
/****** Object:  StoredProcedure [builder].[AutoloadStatus#Save]    Script Date: 12/28/2011 11:42:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [builder].[AutoloadStatus#Save]
	
	@BusinessUnitID int,
	@InventoryID int,
	@StatusTypeId int, 
	@startTime datetime = null,
	@endTime datetime = null,
	@MemberLogin varchar(30)
	
AS
BEGIN


	if @startTime IS NULL
		SET @startTime = GETDATE()

	IF EXISTS( SELECT 1 FROM builder.AutoloadStatus WHERE BusinessUnitID = @BusinessUnitID AND InventoryID = @InventoryID )
		BEGIN
		
			UPDATE builder.AutoloadStatus
			SET StartTime = @startTime,
				EndTime = @endTime,
				LastUpdatedOn = GETDATE(),
				LastUpdatedBy = @MemberLogin,
				statusTypeId = @StatusTypeId
			WHERE BusinessUnitID = @BusinessUnitID AND InventoryID = @InventoryID
						
		END
	ELSE
		BEGIN

			INSERT INTO [builder].[AutoloadStatus] (businessUnitId, inventoryId, statusTypeId, createdOn, createdBy, 
			lastUpdatedOn, lastUpdatedBy,startTime, endTime)
			VALUES (@BusinessUnitId, @InventoryId, @StatusTypeId, GETDATE(), @MemberLogin, GetDate(), @MemberLogin,
			@startTime, @endTime)
			
		END
			
END
GO
GRANT EXECUTE ON [builder].[AutoloadStatus#Save] TO MerchandisingUser 
GO