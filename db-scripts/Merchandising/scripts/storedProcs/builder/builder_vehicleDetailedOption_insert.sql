if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[vehicleDetailedOption#Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[vehicleDetailedOption#Insert]
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_vehicleDetailedOption_insert.sql,v 1.1 2010/01/28 23:12:44 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Inserts a vehicle's detailed option override a businessUnit
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId - integer inventory id
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[vehicleDetailedOption#Insert]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@OptionCode varchar(20),
	@IsStandard bit,
	@CustomTitle varchar(2000),
	@CustomBody varchar(2000),
	@ChromeStyleId int = NULL,
	@ModelId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @addedId int
	
		INSERT INTO builder.DetailedEquipmentDescriptions 
		(
			[optionCode],
			[equipmentTitle],
			[equipmentDescription],
			[isStandard]
		)
		VALUES 
		(
			@OptionCode,
			@CustomTitle,
			@CustomBody,
			@IsStandard
		)
		
		--get id for newly created item
		select @addedId = SCOPE_IDENTITY()

		INSERT INTO builder.Vehicle_DetailedEquipmentDescriptions 
		(
			[businessUnitId],
			[modelId],
			[chromeStyleId],
			[equipmentDescriptionId]
		)
		VALUES
		(
			@BusinessUnitId,
			@ModelId,
			@ChromeStyleId,
			@addedId
		)
		
		
END
GO

GRANT EXECUTE ON [builder].[vehicleDetailedOption#Insert] TO MerchandisingUser 
GO