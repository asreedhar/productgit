if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[AutoLoadAudit#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[AutoLoadAudit#Fetch]
GO

CREATE PROCEDURE [builder].[AutoLoadAudit#Fetch]

AS
BEGIN

	SELECT BU.BusinessUnitID, BU.BusinessUnit, ALST.StatusType, count(ALST.StatusType) as StatusCount
	FROM Merchandising.Builder.AutoloadStatus ALS
	JOIN IMT.dbo.BusinessUnit BU ON BU.BusinessUnitID = ALS.BusinessUnitID
	JOIN FLDW.dbo.InventoryActive I ON I.InventoryID = ALS.InventoryID AND ALS.BusinessUnitID = I.BusinessUnitID
	JOIN Merchandising.Builder.AutoloadStatusTypes ALST ON ALS.StatusTypeID = ALST.StatusTypeID
	LEFT JOIN Merchandising.Settings.Merchandising S ON BU.BusinessUnitID = S.BusinessUnitID
	GROUP BY BU.BusinessUnitID, BU.BusinessUnit, ALST.StatusType, S.BatchAutoLoad
	ORDER BY BU.BusinessUnit

END
GO

GRANT EXECUTE ON [builder].[AutoLoadAudit#Fetch] TO MerchandisingUser
GO