if object_id('builder.Inventory#Fetch') is not null
	drop procedure builder.Inventory#Fetch
GO

/****** Object:  StoredProcedure [Inventory#Fetch]    Script Date: 10/29/2008 17:22:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_Inventory_Fetch.sql,v 1.20 2010/08/19 21:24:39 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * gets all inventory items that meet the input var filter criteria
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 *	@StockNumber varchar(17) = '', - defaults to returning a list of stock items...
 *  @Vin varchar(17) = '' - defaults to returning all vins -- suggest only using one of stock/vin
 *	@merchandisingStatus int = 240 - defaults to all merchandising statuses valid values are - 16 - not posted, 32 - pending approval, 64 - online needs attention, 128 - online
 *	@inventoryType int = 0, - defaults to all inventory.  valid vals are 0 - all, 1 - new, 2 - used
 *	@activityFilter int = 7 - defaults to all activities.  valid vals are 1 - needs pricing, 2 - needs description attention, 4 - needs photos
 *  @searchParameter varchar(20) - this can be a text segment representing a stocknumber, make, or model
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE builder.Inventory#Fetch
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@StockNumber varchar(17) = '',
	@Vin varchar(17) = '',
	@merchandisingStatus int = 240,
	@inventoryType int = 2,
	@activityFilter int = 7,
	@overdueFlag bit = 0,
	@sortExpression varchar(100) = 'inventoryReceivedDate ASC, inv.inventoryId',
	@startRowIndex int = 0,
	@maximumRows int = 10000,
	@InventoryObjective int = NULL,
	@debug bit = 0
	
AS
set nocount on
set transaction isolation level read uncommitted

declare @errorMessage varchar(1000)


/*
	12/29/2011 Dave Speer
	
	The app appears to call this proc with the only parameter variations being BusinessUnitID and inventoryType
	
	The proc call is usually:
	exec builder.Inventory#Fetch @BusinessUnitID=102360,@merchandisingStatus=240,@inventoryType=0,@activityFilter=7,@overdueFlag=0,@startRowIndex=0,@maximumRows=10000
	
	Raise an error if the incoming parameters aren't as expected until we can factor the parameters out of the middle tier.
	
*/
if
(
	@StockNumber <> ''
	or @Vin <> ''
	or @merchandisingStatus <> 240
	or @activityFilter <> 7
	or @overdueFlag <> 0
	or @sortExpression <> 'inventoryReceivedDate ASC, inv.inventoryId'
	or @startRowIndex <> 0
	or @maximumRows <> 10000
	or @InventoryObjective is not null
	or @debug <> 0
)
begin
	set @errorMessage = 
		'Incoming parameters not as expected!'
		+ '  @BusinessUnitID = ' + isnull(convert(varchar(100), @BusinessUnitID), '<null>')
		+ ', @StockNumber = ' + isnull(@StockNumber, '<null>')
		+ ', @Vin = ' + isnull(@Vin, '<null>')
		+ ', @merchandisingStatus = ' + isnull(convert(varchar(100), @merchandisingStatus), '<null>')
		+ ', @inventoryType = ' + isnull(convert(varchar(100), @inventoryType), '<null>')
		+ ', @activityFilter = ' + isnull(convert(varchar(100), @activityFilter), '<null>')
		+ ', @overdueFlag = ' + isnull(convert(varchar(100), @overdueFlag), '<null>')
		+ ', @sortExpression = ' + isnull(@sortExpression, '<null>')
		+ ', @startRowIndex = ' + isnull(convert(varchar(100), @startRowIndex), '<null>')
		+ ', @maximumRows = ' + isnull(convert(varchar(100), @maximumRows), '<null>')
		+ ', @InventoryObjective = ' + isnull(convert(varchar(100), @InventoryObjective), '<null>')
		+ ', @debug = ' + isnull(convert(varchar(100), @debug), '<null>')

	raiserror(@errorMessage, 16, 1)
	return 1
end
	



if (@inventoryType = 0)
begin
	select
		Vin, 
		wi.InventoryID,
		StockNumber,
		statusBucket,
		InventoryReceivedDate, 
		vehicleYear, 
		UnitCost, 
		Certified,
		CertifiedID,
		CertifiedProgramId,
		make, 
		model, 
		trim,
		afterMarketTrim,
		marketClass,
		segmentId,
		MileageReceived, 
		TradeOrPurchase, 
		ListPrice, 
		BaseColor,
		ExtColor1,
		ExteriorColorCode,
		ExtColor2,
		ExteriorColorCode2,
		IntColor,
		InteriorColorCode,
		VehicleLocation,
		lastUpdateListPrice,
		datePosted,
		wi.ChromeStyleId,
		VehicleCatalogId,
		lowActivityFlag,
		HasCurrentBookValue,
		exteriorStatus, 
		interiorStatus,
		pricingStatus,
		photoStatus, 
		adReviewStatus,
		postingStatus,
		InventoryType,
		desiredPhotoCount,
		LotPrice,
		MSRP,
		DueForRepricing,
		SpecialPrice,
		HasKeyInformation, -- TODO: Remove this field from the result set (See Case 20442)
		HasCarfax,
		HasCarfaxAsAdmin,
		autoloadStatusTypeId,
		autoloadEndTime,
		NoPackages,
		AutoWindowStickerPrinted
	from workflow.Inventory wi
	where wi.BusinessUnitID = @BusinessUnitID
end
else
begin
	select
		Vin, 
		wi.InventoryID,
		StockNumber,
		statusBucket,
		InventoryReceivedDate, 
		vehicleYear, 
		UnitCost, 
		Certified,
		CertifiedID,
		CertifiedProgramId,
		make, 
		model, 
		trim,
		afterMarketTrim,
		marketClass,
		segmentId,
		MileageReceived, 
		TradeOrPurchase, 
		ListPrice, 
		BaseColor,
		ExtColor1,
		ExteriorColorCode,
		ExtColor2,
		ExteriorColorCode2,
		IntColor,
		InteriorColorCode,
		VehicleLocation,
		lastUpdateListPrice,
		datePosted,
		wi.ChromeStyleId,
		VehicleCatalogId,
		lowActivityFlag,
		HasCurrentBookValue,
		exteriorStatus, 
		interiorStatus,
		pricingStatus,
		photoStatus, 
		adReviewStatus,
		postingStatus,
		InventoryType,
		desiredPhotoCount,
		LotPrice,
		MSRP,
		DueForRepricing,
		SpecialPrice,
		HasKeyInformation, -- TODO: Remove this field from the result set (See Case 20442)
		HasCarfax,
		HasCarfaxAsAdmin,
		autoloadStatusTypeId,
		autoloadEndTime,
		NoPackages,
		AutoWindowStickerPrinted
	from workflow.Inventory wi
	where
		wi.BusinessUnitID = @BusinessUnitID
		and inventoryType = @inventoryType
end



go	
	

GRANT EXECUTE ON [builder].[Inventory#Fetch] TO MerchandisingUser 
GO