if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[descriptionItem#Merge]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[descriptionItem#Merge]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [builder].[descriptionItem#Merge]
	
	@BusinessUnitId int,
	@InventoryId int,
	@descriptionContent builder.descriptionContent READONLY	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- create a table to match the target table (didn't want to be able to pass in mismatched bu and inv)
	DECLARE @source TABLE
	(
		businessUnitId int,
		inventoryId int,
		descriptionItemTypeId  int
	)
	
	INSERT INTO @source
		(businessUnitId, inventoryId, descriptionItemTypeId )
		SELECT @BusinessUnitId, @InventoryId, descriptionItemTypeId 
			FROM @descriptionContent;
	
	
	
	WITH currentDescriptionContent 
          AS (SELECT businessUnitId, inventoryID, descriptionItemTypeId
              FROM   builder.descriptionContents
              WHERE businessUnitID = @businessUnitId
              AND   inventoryId = @inventoryId
            )
	MERGE currentDescriptionContent AS target
	USING (
			SELECT businessUnitId, inventoryID, descriptionItemTypeId
				FROM @source
			) AS source
		ON (
				target.businessUnitId = source.businessUnitId AND 
				target.inventoryId = source.inventoryId AND 
				target.descriptionItemTypeId = source.descriptionItemTypeId 
			)
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (businessUnitID, inventoryID, descriptionItemTypeId)
		VALUES(source.businessUnitID, source.inventoryID, source.descriptionItemTypeId)
	WHEN NOT MATCHED BY SOURCE THEN
		DELETE
		;
		
END
GO

GRANT EXECUTE ON [builder].[descriptionItem#Merge] TO MerchandisingUser 
GO