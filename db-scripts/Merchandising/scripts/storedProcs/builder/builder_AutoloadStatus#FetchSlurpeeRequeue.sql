if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[AutoloadStatus#FetchSlurpeeRequeue]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[AutoloadStatus#FetchSlurpeeRequeue]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [builder].[AutoloadStatus#FetchSlurpeeRequeue]
	@BusinessUnitId int
AS
BEGIN

SELECT DISTINCT Inventory.InventoryID
    FROM workflow.Inventory Inventory
    INNER JOIN VehicleCatalog.Chrome.Divisions Divisions ON Inventory.Make = Divisions.DivisionName
									                        AND Divisions.CountryCode = 1
    INNER JOIN merchandising.VehicleAutoLoadSettings VehicleAutoLoadSettings ON Divisions.ManufacturerID = VehicleAutoLoadSettings.ManufacturerID
    INNER JOIN settings.Dealer_SiteCredentials Dealer_SiteCredentials ON VehicleAutoLoadSettings.siteId = Dealer_SiteCredentials.siteId
													                        AND Inventory.BusinessUnitID = Dealer_SiteCredentials.businessUnitId
    WHERE Dealer_SiteCredentials.isLockedOut = 0
    AND Dealer_SiteCredentials.buildRequestType = 2
    AND Inventory.BusinessUnitID = @BusinessUnitId
    AND NOT EXISTS (SELECT *
                        FROM builder.AutoloadStatus AutoloadStatus
                        WHERE inventory.BusinessUnitID = AutoloadStatus.businessUnitID
                        AND  inventory.InventoryID = AutoloadStatus.InventoryID
                        AND autoloadStatusTypeId = 100)
    AND NOT EXISTS (SELECT *
                        FROM builder.AutoloadStatus AutoloadStatus
                        INNER JOIN builder.AutoloadStatusTypes AutoloadStatusTypes ON AutoloadStatus.statusTypeID = AutoloadStatusTypes.statusTypeID
                        WHERE inventory.BusinessUnitID = AutoloadStatus.businessUnitID
                        AND inventory.InventoryID = AutoloadStatus.InventoryID
                        AND AutoloadStatusTypes.requeueMinutes IS NOT NULL
                        AND AutoloadStatus.startTime > DATEADD(MINUTE, - AutoloadStatusTypes.requeueMinutes, GETDATE())
                        )



END 
GO

GRANT EXECUTE ON [builder].[AutoloadStatus#FetchSlurpeeRequeue] TO MerchandisingUser 
GO
