if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[InventoryItem#Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[InventoryItem#Update]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_inventoryItem_update.sql,v 1.1 2008/12/09 18:24:16 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * update the inventory item through the interfaces
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - integer id of businessunit
 * @InventoryId - int id of inventory 
 * @Mileage - mileage of vehicle
 * @Certified - certified flag - 0 or 1
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[InventoryItem#Update]
	
	@BusinessUnitId int,
	@InventoryId int,
	@Mileage int,
	@Certified tinyint
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	EXEC Interface.Inventory#Update @BusinessUnitId, @InventoryId, @Mileage, @Certified
	
END
GO

GRANT EXECUTE ON [builder].[InventoryItem#Update] TO MerchandisingUser 
GO