if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[CertificationFeatureDisplay#Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[CertificationFeatureDisplay#Delete]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* --------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * Remove the association of a certification program to a dealer.
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - integer id of businessunit
 * @CertificationTypeId - type of certification to remove
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[CertificationFeatureDisplay#Delete]
	
	@BusinessUnitId int,
	@CertificationTypeId int
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM settings.dealerCertificationConfiguration 
	WHERE businessUnitId = @BusinessUnitId
	AND certificationTypeId = @CertificationTypeId
			
END
GO

GRANT EXECUTE ON [builder].[CertificationFeatureDisplay#Delete] TO MerchandisingUser 
GO