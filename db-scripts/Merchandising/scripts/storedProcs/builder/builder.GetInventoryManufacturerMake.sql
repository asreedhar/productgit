if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[GetInventoryManufacturerMake]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[GetInventoryManufacturerMake]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [builder].[GetInventoryManufacturerMake]
	@InventoryId int,
	@MakeId int output ,
	@MakeName varchar(50) output,
	@ManufacturerId int output,
	@ManufacturerName varchar(50) output
AS
BEGIN

	SET NOCOUNT ON

	select 
		@MakeId = MakeID, 
		@MakeName = MakeName, 
		@ManufacturerId = ManufacturerID, 
		@ManufacturerName = ManufacturerName
	from Merchandising.builder.InventoryManufacturerMake
	where InventoryID = @InventoryID
	
END
GO
GRANT EXECUTE ON [builder].[GetInventoryManufacturerMake] TO MerchandisingUser 
GO

GRANT EXECUTE ON [builder].[GetInventoryManufacturerMake] TO firstlook 
GO