if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[FinancingSpecials#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[FinancingSpecials#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_financingSpecials_fetch.sql,v 1.7 2009/11/09 17:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch the business unit configured certification item for 
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - integer id of businessunit
 * @CertificationTypeId - integer id of the certification type to fetch
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[FinancingSpecials#Fetch]
	
	@OwnerEntityTypeId int,
	@OwnerEntityId int,
	@SpecialId int = NULL,
	@CertificationTypeId int = NULL
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT fs.specialId,
	fs.ownerEntityTypeId, 
	fs.ownerEntityId, 
	fs.title as specialTitle,
	fs.description,
	fs.expiresOn,
	fs.onlyCertifieds, 
	fs.vehicleProfileId,
	vp.title, vp.makeIdList, vp.modelList, vp.trimList, vp.segmentIdList, vp.marketClassIdList, 
	vp.startYear, vp.endYear, vp.minAge, vp.maxAge, vp.minPrice, vp.maxPrice, vp.certifiedStatusFilter
	FROM builder.FinancingSpecials fs
	LEFT JOIN templates.vehicleProfiles vp
		ON vp.vehicleProfileId = fs.vehicleProfileId
	LEFT JOIN settings.cpoCertifieds cc
		ON cc.cpoGroupId = fs.ownerEntityId
		AND fs.ownerEntityTypeId = 3
	
	WHERE 
	(expiresOn IS NULL OR expiresOn > getdate())
	AND (
	--get all for this owner
		(	fs.ownerEntityId = @OwnerEntityId
			AND fs.ownerEntityTypeId = @OwnerEntityTypeId
			AND 
			(	@SpecialId IS NULL   --special ID doesn't matter if NULL was passed in
				OR specialId = @SpecialId  --MATCH special Id if requested
			)
			AND 
			(	fs.OnlyCertifieds = 0  --MATCH certified - either any cert level, OR cert required and a certificationId supplied
				OR 
				(
					fs.OnlyCertifieds = 1 
					AND NOT @CertificationTypeId IS NULL)
				)
		)
		--also get any from the CPO office for the supplied certification id
		OR 
		(	cc.CertificationTypeId = @CertificationTypeId 
			AND fs.ownerEntityTypeId = 3 -- only CPO groups
		)
	)
	
	
END
GO

GRANT EXECUTE ON [builder].[FinancingSpecials#Fetch] TO MerchandisingUser 
GO