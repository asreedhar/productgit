if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[OldCheckEquipmentMapping]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[OldCheckEquipmentMapping]
GO

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go











-- =============================================
-- Author:		<Fitzpatrick, Benin>
-- Create date: <2/2/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_OldCheckEquipmentMapping.sql,v 1.1 2010/03/09 23:03:51 bfitzpatrick Exp $
 * 
 * Summary finally
 * -------
 * 
 * Update or insert any given set of Equipment to a vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @inventoryID
 * @businessUnitID - defaults to 
 * @EngineTypeCategoryID - 
 * @TransmissionTypeCategoryID - 
 * @DriveTrainCategoryID - 
 * @FuelCategoryID -
 * @createdBy	
 * @createdOn
 * @updatedBy
 * @updatedOn
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[OldCheckEquipmentMapping]
	@ChromeStyleID int,
	@inventoryID int,
	@businessUnitID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF EXISTS (SELECT 1 FROM builder.EquipmentMapping WHERE businessUnitID = @businessUnitID AND inventoryID = @inventoryID)
	BEGIN
		SELECT * FROM builder.EquipmentMapping
		WHERE inventoryID = @inventoryID
		AND businessUnitID = @businessUnitID
	END
	ELSE
	BEGIN
		SELECT Distinct VehicleCatalog.Chrome.StyleGenericEquipment.ChromeStyleID, VehicleCatalog.Chrome.StyleGenericEquipment.CategoryID, CategoryUTF, Description, StyleAvailability
                FROM VehicleCatalog.Chrome.StyleGenericEquipment
                JOIN VehicleCatalog.Chrome.Category
				ON VehicleCatalog.Chrome.Category.CategoryID = VehicleCatalog.Chrome.StyleGenericEquipment.CategoryID
				WHERE VehicleCatalog.Chrome.StyleGenericEquipment.ChromeStyleID = @ChromeStyleID
                AND VehicleCatalog.Chrome.StyleGenericEquipment.CountryCode = 1
				AND VehicleCatalog.Chrome.Category.CategoryUTF ='Engine'
	
		SELECT Distinct VehicleCatalog.Chrome.StyleGenericEquipment.ChromeStyleID,  VehicleCatalog.Chrome.StyleGenericEquipment.CategoryID, CategoryUTF, Description, StyleAvailability
                FROM  VehicleCatalog.Chrome.StyleGenericEquipment
                JOIN VehicleCatalog.Chrome.Category
				ON VehicleCatalog.Chrome.Category.CategoryID = VehicleCatalog.Chrome.StyleGenericEquipment.CategoryID
				WHERE VehicleCatalog.Chrome.StyleGenericEquipment.ChromeStyleID = @ChromeStyleID
                AND VehicleCatalog.Chrome.StyleGenericEquipment.CountryCode = 1
				AND VehicleCatalog.Chrome.Category.CategoryUTF ='Transmission'
	
		SELECT Distinct VehicleCatalog.Chrome.StyleGenericEquipment.ChromeStyleID,  VehicleCatalog.Chrome.StyleGenericEquipment.CategoryID, CategoryUTF, Description, StyleAvailability
                FROM  VehicleCatalog.Chrome.StyleGenericEquipment
                JOIN VehicleCatalog.Chrome.Category
				ON VehicleCatalog.Chrome.Category.CategoryID = VehicleCatalog.Chrome.StyleGenericEquipment.CategoryID
				WHERE VehicleCatalog.Chrome.StyleGenericEquipment.ChromeStyleID = @ChromeStyleID
                AND VehicleCatalog.Chrome.StyleGenericEquipment.CountryCode = 1
				AND VehicleCatalog.Chrome.Category.CategoryUTF ='Drivetrain'

		SELECT Distinct VehicleCatalog.Chrome.StyleGenericEquipment.ChromeStyleID,  VehicleCatalog.Chrome.StyleGenericEquipment.CategoryID, CategoryUTF, Description, StyleAvailability
                FROM VehicleCatalog.Chrome.StyleGenericEquipment
                JOIN VehicleCatalog.Chrome.Category
				ON VehicleCatalog.Chrome.Category.CategoryID = VehicleCatalog.Chrome.StyleGenericEquipment.CategoryID
				WHERE VehicleCatalog.Chrome.StyleGenericEquipment.ChromeStyleID = @ChromeStyleID
                AND VehicleCatalog.Chrome.StyleGenericEquipment.CountryCode = 1
				AND VehicleCatalog.Chrome.Category.CategoryUTF ='Fuel system'
	END
END
GO

GRANT EXECUTE ON [builder].[OldCheckEquipmentMapping] TO MerchandisingUser 
GO










 