if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[CheckEquipmentMapping]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[CheckEquipmentMapping]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Fitzpatrick, Benin>
-- Create date: <2/2/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_CheckEquipmentMapping.sql,v 1.1 2010/03/09 23:03:51 bfitzpatrick Exp $
 * 
 * Summary
 * -------
 * 
 * Update or insert any given set of Equipment to a vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @inventoryID
 * @businessUnitID - defaults to 
 * @EngineTypeCategoryID - 
 * @TransmissionTypeCategoryID - 
 * @DriveTrainCategoryID - 
 * @FuelCategoryID -
 * @createdBy	
 * @createdOn
 * @updatedBy
 * @updatedOn
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[CheckEquipmentMapping]
	@ChromeStyleID int,
	@inventoryID int,
	@businessUnitID int
AS
BEGIN

	DECLARE @Results TABLE (
		InventoryID INT NOT NULL,
		BusinessUnitID INT NOT NULL,
		EngineTypeCategoryID INT NULL,
		TransmissionTypeCategoryID INT NULL,
		DriveTrainCategoryID INT NULL,
		FuelCategoryID INT NULL
	)
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF EXISTS (SELECT 1 FROM builder.EquipmentMapping WHERE businessUnitID = @businessUnitID AND inventoryID = @inventoryID)
	BEGIN

		INSERT INTO @Results(
			InventoryID,
			BusinessUnitID,
			EngineTypeCategoryID,
			TransmissionTypeCategoryID,
			DriveTrainCategoryID,
			FuelCategoryID
		)
		SELECT 
			InventoryID,
			BusinessUnitID,
			EngineTypeCategoryID,
			TransmissionTypeCategoryID,
			DriveTrainCategoryID,
			FuelCategoryID
		FROM builder.EquipmentMapping
		WHERE inventoryID = @inventoryID
		AND businessUnitID = @businessUnitID
	END
	ELSE
	BEGIN
		INSERT INTO @Results(
			InventoryID,
			BusinessUnitID,
			EngineTypeCategoryID,
			TransmissionTypeCategoryID,
			DriveTrainCategoryID,
			FuelCategoryID
		)
		SELECT Distinct 
			@InventoryID,
			@BusinessUnitID,
			SGE.CategoryID, 
			NULL,
			NULL,
			NULL
		FROM VehicleCatalog.Chrome.StyleGenericEquipment SGE
		JOIN VehicleCatalog.Chrome.Categories C
			ON C.CategoryID = SGE.CategoryID
		WHERE SGE.ChromeStyleID = @ChromeStyleID
		AND SGE.CountryCode = 1
		AND C.CategoryTypeFilter ='Engine'
	
		INSERT INTO @Results(
			InventoryID,
			BusinessUnitID,
			EngineTypeCategoryID,
			TransmissionTypeCategoryID,
			DriveTrainCategoryID,
			FuelCategoryID
		)
		SELECT Distinct 
			@InventoryID,
			@BusinessUnitID,
			NULL, 
			SGE.CategoryID,
			NULL,
			NULL
		FROM VehicleCatalog.Chrome.StyleGenericEquipment SGE
		JOIN VehicleCatalog.Chrome.Categories C
			ON C.CategoryID = SGE.CategoryID
		WHERE SGE.ChromeStyleID = @ChromeStyleID
		AND SGE.CountryCode = 1
		AND C.CategoryTypeFilter ='Transmission'
	
		INSERT INTO @Results(
			InventoryID,
			BusinessUnitID,
			EngineTypeCategoryID,
			TransmissionTypeCategoryID,
			DriveTrainCategoryID,
			FuelCategoryID
		)
		SELECT Distinct 
			@InventoryID,
			@BusinessUnitID,
			NULL, 
			NULL,
			SGE.CategoryID,
			NULL
		FROM VehicleCatalog.Chrome.StyleGenericEquipment SGE
		JOIN VehicleCatalog.Chrome.Categories C
			ON C.CategoryID = SGE.CategoryID
		WHERE SGE.ChromeStyleID = @ChromeStyleID
		AND SGE.CountryCode = 1
		AND C.CategoryTypeFilter ='Drivetrain'

		INSERT INTO @Results(
			InventoryID,
			BusinessUnitID,
			EngineTypeCategoryID,
			TransmissionTypeCategoryID,
			DriveTrainCategoryID,
			FuelCategoryID
		)
		SELECT Distinct 
			@InventoryID,
			@BusinessUnitID,
			NULL, 
			NULL,
			NULL,
			SGE.CategoryID
		FROM VehicleCatalog.Chrome.StyleGenericEquipment SGE
		JOIN VehicleCatalog.Chrome.Categories C
			ON C.CategoryID = SGE.CategoryID
		WHERE SGE.ChromeStyleID = @ChromeStyleID
		AND SGE.CountryCode = 1
		AND C.CategoryTypeFilter ='Fuel system'
	END
			
	SELECT
		InventoryID,
		BusinessUnitID,
		EngineTypeCategoryID,
		TransmissionTypeCategoryID,
		DriveTrainCategoryID,
		FuelCategoryID
	FROM @Results
END
GO

GRANT EXECUTE ON [builder].[CheckEquipmentMapping] TO MerchandisingUser 
GO

  