
/****** Object:  StoredProcedure [builder].[Load#Inventory]    Script Date: 06/26/2015 12:01:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[builder].[Load#Inventory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [builder].[Load#Inventory]
GO


/****** Object:  StoredProcedure [builder].[Load#Inventory]    Script Date: 06/26/2015 12:01:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [builder].[Load#Inventory]
	@DealerId       INT,          
	@SaleStrategy      CHAR,          
	@InventoryFilter   VARCHAR(50) = NULL  -- StockNumber or Year or Make or Model; can't be sure w/o parsing...
	AS
          
SET NOCOUNT ON          
SET ANSI_WARNINGS ON    
          
DECLARE @err INT          
          
------------------------------------------------------------------------------------------------          
-- Validate Parameter Values          
------------------------------------------------------------------------------------------------          
         
 DECLARE @OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT
 select @OwnerEntityID = @DealerId 
 select @OwnerEntityTypeID = OwnerTypeID from market.Pricing.owner where OwnerEntityID = @DealerId
 select @OwnerID = OwnerID from market.Pricing.owner where OwnerEntityID = @DealerId

---------Temporary Basis------ 
DECLARE @HardLimit INT, @PercentageLimit INT  
          
SELECT @HardLimit = 3,@PercentageLimit = 10   
-------------------------------
   
          
EXEC Market.[Pricing].[ValidateParameter_SaleStrategy] @SaleStrategy          
IF @err <> 0 GOTO Failed          

          
------------------------------------------------------------------------------------------------          
-- API Output Schema defined in outer scope          
------------------------------------------------------------------------------------------------          
          
CREATE TABLE #Inventory (
      AgeIndex				TINYINT,
      AgeDescription        Varchar(20),      
      --IsOverridden        BIT,
      InventoryId			INT,
      StatusBucket			INT,
      VIN					VARCHAR(17) NOT NULL,
      Age					INT NULL,
      VehicleDescription	VARCHAR(200) NOT NULL,
      StokNumber			VARCHAR(50) NULL,
      ChromeStyleID			INT,
      Trim                  VARCHAR(100) NULL,
      VehicleColor			VARCHAR(100) NULL,
      BaseColor				VARCHAR(50) NULL,
      ExtColor1				VARCHAR(50) NULL,      
      ExteriorColorCode		VARCHAR(50) NULL,
      ExtColor2				VARCHAR(50) NULL,
      ExteriorColorCode2	VARCHAR(50) NULL,
      InteriorColor			VARCHAR(50) NULL,
      InteriorColorCode		VARCHAR(50) NULL,
      VehicleMileage		INT NULL,
      UnitCost              INT NULL,
      ListPrice				INT NULL,      
      MarketDaysSupply		INT,          
      DueForPlanning		BIT,      
      VehicleYear			INT,				--	 remove later on
      Make					VARCHAR(50),		--   remove later on 
      Model					VARCHAR(50)  		--   remove later on 
)        
          
------------------------------------------------------------------------------------------------          
--  OPTIMIZATION -- HAD TROUBLE WHEN INLINED          
------------------------------------------------------------------------------------------------          
          
CREATE TABLE #MarketPricingBucket (MarketPricingBucketID tinyint,AgeDescription varchar(10), LowAge tinyint, HighAge tinyint null, MinMarketPct tinyint null, MaxMarketPct tinyint null, MinGross int null)          
	          
	INSERT          
	INTO #MarketPricingBucket          
	SELECT MarketPricingBucketID = IBR.RangeID,          
	AgeDescription = IBR.Description,
	LowAge   = IBR.Low,          
	HighAge   = IBR.High,          
	MinMarketPct  = IBR.Value1,          
	MaxMarketPct  = IBR.Value2,          
	MinGross  = IBR.Value3           
	      
	FROM Market.Pricing.GetOwnerInventoryAgeBucket(@OwnerID) IBR          
          
SELECT @err = @@ERROR          
IF @err <> 0 GOTO Failed          
          

          
IF @InventoryFilter IS NULL SET @InventoryFilter = '' ELSE SET @InventoryFilter = LTRIM(RTRIM(@InventoryFilter))          
          
DECLARE @InventoryFilter_A VARCHAR(50),          
        @InventoryFilter_B VARCHAR(50),          
        @InventoryFilter_C VARCHAR(50),          
        @InventoryFilter_D VARCHAR(50)          
          
IF @InventoryFilter IS NOT NULL BEGIN          
          
	SELECT @InventoryFilter_A = CASE WHEN Rank = 1 THEN value ELSE @InventoryFilter_A END,          
	  @InventoryFilter_B = CASE WHEN Rank = 2 THEN value ELSE @InventoryFilter_B END,          
	  @InventoryFilter_C = CASE WHEN Rank = 3 THEN value ELSE @InventoryFilter_C END,          
	  @InventoryFilter_D = CASE WHEN Rank = 4 THEN value ELSE @InventoryFilter_D END          
	FROM IMT.dbo.split(@InventoryFilter, ' ')          
          
END          
          
--PRINT @InventoryFilter_A          
--PRINT @InventoryFilter_B          
--PRINT @InventoryFilter_C          
--PRINT @InventoryFilter_D          

          
CREATE TABLE #InventoryStrategy          
  (InventoryID INT,          
  BusinessUnitID INT,          
  VehicleID INT,          
  ListPrice decimal(12,2),--changed to prevent integer math problems          
  InventoryActive INT,          
  InventoryType INT,          
  StockNumber VARCHAR(30),          
  PlanReminderDate SMALLDATETIME,           
  InventoryReceivedDate SMALLDATETIME,            
  AIP_EventCategoryID INT,           
  HasActivePlan BIT,          
  EverBeenPlanned BIT           
  PRIMARY KEY (InventoryID))          
           
EXEC market.Pricing.Load#InventoryStrategy @OwnerEntityID,@SaleStrategy          

--Temporary basis

DECLARE @MarketDaysSupplyBasePeriod INT, @PowerRegionID INT          

SELECT @MarketDaysSupplyBasePeriod = COALESCE(DPP.MarketDaysSupplyBasePeriod, DPS.MarketDaysSupplyBasePeriod, 90),          
  @PowerRegionID = CASE WHEN U.Active = 1 THEN PowerRegionID ELSE NULL END          
FROM [IMT]..BusinessUnit B          
LEFT JOIN [IMT]..DealerPreference_Pricing DPP ON B.BusinessUnitID = DPP.BusinessUnitID          
LEFT JOIN [IMT]..DealerPreference_Pricing DPS ON DPS.BusinessUnitID = 100150          
LEFT JOIN [IMT]..DealerPreference_JDPowerSettings DPJ ON B.BusinessUnitID = DPJ.BusinessUnitID          
LEFT JOIN [IMT]..DealerUpgrade U          
    ON B.BusinessUnitID = U.BusinessUnitID          
    AND U.DealerUpgradeCD = 18          
    AND U.Active = 1          
    AND U.EffectiveDateActive = 1          
WHERE B.BusinessUnitID = @OwnerEntityID   
------------------

 ;WITH GenExtColor AS  
(  
SELECT DISTINCT
I.InventoryID AS inventoryId,ISNULL(Colors.GenericExtColor,'-') AS color
FROM   #InventoryStrategy I
INNER JOIN Merchandising.builder.OptionsConfiguration oc ON I.businessUnitID = oc.businessUnitID AND I.InventoryID = oc.InventoryID
INNER JOIN VehicleCatalog.Chrome.Colors	ON oc.chromeStyleID = Colors.StyleID AND Colors.countryCode = 1	AND oc.ExteriorColorCode = Colors.Ext1ManCode
)          

------------------

INSERT           
INTO  #Inventory          
           
SELECT 
		AgeIndex			= MPB.MarketPricingBucketID,           
		AgeDescription      = MPB.AgeDescription ,
		InventoryID			= I.InventoryID,          
		StatusBucket		= COALESCE(WI.StatusBucket,0),
		VIN                 = V.VIN,
		Age					= (CASE WHEN DATEDIFF(DD, I.InventoryReceivedDate, GETDATE()) > 0 THEN DATEDIFF(DD, I.InventoryReceivedDate, GETDATE()) ELSE 1 END),
		VehicleDescription	= CAST(WI.VehicleYear AS VARCHAR) + ' ' + WI.Make + ' ' + WI.Model + ' ' + ISNULL(CASE WHEN VCCS.trim = '' THEN 'base' ELSE VCCS.trim END + CASE WHEN VCCS.styleNameWOTrim = '' THEN '' ELSE ' - ' + VCCS.styleNameWOTrim END,''),
		StokNumber			= I.StockNumber,
		ChromeStyleID		= WI.chromeStyleID,
		Trim				= CASE WHEN VCCS.trim = '' THEN 'base' ELSE VCCS.trim END + ' - '+ VCCS.styleNameWOTrim, 
		VehicleColor		= ISNULL(GEC.color,'-'),
		BaseColor			= WI.BaseColor,
		ExtColor1			= WI.ExtColor1,	
		ExteriorColorCode	= WI.ExteriorColorCode,
		ExtColor2			= WI.ExtColor2,
		ExteriorColorCode2	= WI.ExteriorColorCode2,
		InteriorColor		= WI.InteriorColorCode,
		InteriorColorCode	= WI.InteriorColorCode,
		VehicleMileage		= IA.MileageReceived,		
		UnitCost			= IA.UnitCost,	
		--IsOverridden      = CASE WHEN DI.CategorizationOverrideExpiryDate > GETDATE() THEN 1 ELSE 0 END,          	  
		ListPrice				= ROUND(I.ListPrice,0),          
		MarketDaysSupply = CASE WHEN COALESCE(SSF.SalesInBasePeriod,0) < @HardLimit THEN NULL          
			WHEN ((SSF.SalesInBasePeriod / CAST(SRF.Units AS REAL))*100.0) < @PercentageLimit THEN NULL          
			ELSE ROUND(SRF.Units / (SSF.SalesInBasePeriod / CAST(@MarketDaysSupplyBasePeriod AS REAL)),0)            
			END,          
		DueForPlanning		= CASE WHEN (COALESCE(I.PlanReminderDate,GETDATE()) <= GETDATE()) THEN 1 ELSE 0 END ,          
		VehicleYear			= WI.VehicleYear,
		Make					= WI.Make,
		Model					= WI.Model	  
             
           
FROM #InventoryStrategy I -- MAK 09/23/08  This already covers 'Used Vehicles' and  the Business Unit ID from the Load#InventoryStrategy proc.           

   JOIN FLDW.dbo.InventoryActive IA ON I.InventoryID=IA.InventoryID AND IA.BusinessUnitID=I.BusinessUnitID      
   JOIN [FLDW].dbo.Vehicle V ON I.BusinessUnitID = V.BusinessUnitID AND I.VehicleID = V.VehicleID          
   JOIN [IMT]..MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID            
   JOIN #MarketPricingBucket MPB ON (CASE WHEN DATEDIFF(DD,I.InventoryReceivedDate, GETDATE()) > 0 THEN DATEDIFF(DD, I.InventoryReceivedDate, GETDATE()) ELSE 1 END) BETWEEN MPB.LowAge AND COALESCE(MPB.HighAge,9999)                   
   
  LEFT JOIN Merchandising.workflow.Inventory WI ON I.InventoryID =WI.InventoryID 
  LEFT JOIN VehicleCatalog.Chrome.Styles VCCS ON VCCS.StyleID=WI.ChromeStyleId
  
  LEFT JOIN Market.Pricing.Search S ON S.OwnerID = @OwnerID AND I.InventoryID = S.VehicleEntityID AND S.VehicleEntityTypeID = 1          
  LEFT JOIN Market.Pricing.SearchResult_F SRF ON SRF.OwnerID = @OwnerID AND S.SearchID = SRF.SearchID AND S.DefaultSearchTypeID = SRF.SearchTypeID          
  LEFT JOIN Market.Pricing.SearchSales_F SSF ON SSF.OwnerID = @OwnerID AND S.SearchID = SSF.SearchID AND S.DefaultSearchTypeID = SSF.SearchTypeID          
 
  LEFT JOIN GenExtColor GEC ON GEC.inventoryid=I.InventoryID 
  --LEFT JOIN merchandising.builder.OptionsConfiguration OPC ON I.InventoryID = OPC.inventoryid and I.BusinessUnitID= OPC.BusinessUnitID
  --LEFT JOIN VehicleCatalog.Chrome.Colors
		--		ON OPC.chromeStyleID = Colors.StyleID 
		--		AND Colors.countryCode = 1
		--		AND OPC.ExteriorColorCode = Colors.Ext1ManCode
  --LEFT JOIN VehicleCatalog.Chrome.Styles VCS ON VCS.StyleID = OPC.chromeStyleID
            
         
            
WHERE  (@InventoryFilter IS NULL OR           
		(          
		I.StockNumber = @InventoryFilter          
		OR V.Vin = @InventoryFilter          
		OR 
			(          
			 (          
			  @InventoryFilter_A IS NULL          
			  OR WI.Model LIKE '%' + @InventoryFilter_A + '%'          
			  OR WI.Make LIKE '%' + @InventoryFilter_A + '%'          
			  OR VCCS.StyleNameWOTrim LIKE '%' + @InventoryFilter_A + '%'          
			  OR CAST(WI.VehicleYear AS VARCHAR) LIKE '%' + @InventoryFilter_A + '%'          
			 )          
			 AND           
			 (          
			  @InventoryFilter_B IS NULL           
			  OR WI.Model LIKE '%' + @InventoryFilter_B + '%'          
			  OR WI.Make LIKE '%' + @InventoryFilter_B + '%'          
			  OR VCCS.StyleNameWOTrim LIKE '%' + @InventoryFilter_B + '%'          
			  OR CAST(WI.VehicleYear AS VARCHAR) LIKE '%' + @InventoryFilter_B + '%'          
			 )          
			 AND           
			 (          
			  @InventoryFilter_C IS NULL           
			  OR WI.Model LIKE '%' + @InventoryFilter_C + '%'          
			  OR WI.Make LIKE '%' + @InventoryFilter_C + '%'          
			  OR VCCS.StyleNameWOTrim LIKE '%' + @InventoryFilter_C + '%'          
			  OR CAST(WI.VehicleYear AS VARCHAR) LIKE '%' + @InventoryFilter_C + '%'          
			 )          
			 AND           
			 (          
			  @InventoryFilter_D IS NULL           
			  OR WI.Model LIKE '%' + @InventoryFilter_D + '%'          
			  OR WI.Make LIKE '%' + @InventoryFilter_D + '%'          
			  OR VCCS.StyleNameWOTrim LIKE '%' + @InventoryFilter_D + '%'          
			  OR CAST(WI.VehicleYear AS VARCHAR) LIKE '%' + @InventoryFilter_D + '%'           
			 )          
			)          
		)          
	)           
        
        
SELECT * FROM #Inventory   
DROP TABLE #Inventory   
DROP TABLE #InventoryStrategy           
          
                  
SELECT @err = @@ERROR          
IF @err <> 0 GOTO Failed          
          

------------------------------------------------------------------------------------------------          
-- Failure          
------------------------------------------------------------------------------------------------          
          
Failed:          
          
RETURN @err   
   


GO





GRANT EXECUTE ON builder.Load#Inventory TO MerchandisingUser 
GO


