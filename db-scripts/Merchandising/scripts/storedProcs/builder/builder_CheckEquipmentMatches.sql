if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[CheckEquipmentMatches]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[CheckEquipmentMatches]
GO
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go











-- =============================================
-- Author:		<Fitzpatrick, Benin>
-- Create date: <2/2/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_CheckEquipmentMatches.sql,v 1.1 2010/03/09 23:03:51 bfitzpatrick Exp $
 * 
 * Summary finally
 * -------
 * 
 * Update or insert any given set of Equipment to a vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @inventoryID
 * @businessUnitID - defaults to 
 * @EngineTypeCategoryID - 
 * @TransmissionTypeCategoryID - 
 * @DriveTrainCategoryID - 
 * @FuelCategoryID -
 * @createdBy	
 * @createdOn
 * @updatedBy
 * @updatedOn
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[CheckEquipmentMatches]
	@ChromeStyleID int,
	@engineCat varchar(50),
	@transCat varchar(50),
	@drivetrainCat varchar(50),
	@fuelCat varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN
		select Distinct VehicleCatalog.Chrome.StyleGenericEquipment.ChromeStyleID, VehicleCatalog.Chrome.StyleGenericEquipment.CategoryID, CategoryUTF, Description, StyleAvailability
                From VehicleCatalog.Chrome.StyleGenericEquipment
                JOIN VehicleCatalog.Chrome.Category
				ON VehicleCatalog.Chrome.Category.CategoryID = VehicleCatalog.Chrome.StyleGenericEquipment.CategoryID
				Where VehicleCatalog.Chrome.StyleGenericEquipment.ChromeStyleID = @ChromeStyleID
                AND VehicleCatalog.Chrome.StyleGenericEquipment.CountryCode = 1
				AND VehicleCatalog.Chrome.Category.CategoryUTF =@engineCat

	
		select Distinct VehicleCatalog.Chrome.StyleGenericEquipment.ChromeStyleID, VehicleCatalog.Chrome.StyleGenericEquipment.CategoryID, CategoryUTF, Description, StyleAvailability
                From VehicleCatalog.Chrome.StyleGenericEquipment
                JOIN VehicleCatalog.Chrome.Category
				ON VehicleCatalog.Chrome.Category.CategoryID = VehicleCatalog.Chrome.StyleGenericEquipment.CategoryID
				Where VehicleCatalog.Chrome.StyleGenericEquipment.ChromeStyleID = @ChromeStyleID
                AND VehicleCatalog.Chrome.StyleGenericEquipment.CountryCode = 1
				AND VehicleCatalog.Chrome.Category.CategoryUTF =@transCat
	
		select Distinct VehicleCatalog.Chrome.StyleGenericEquipment.ChromeStyleID, VehicleCatalog.Chrome.StyleGenericEquipment.CategoryID, CategoryUTF, Description, StyleAvailability
                From VehicleCatalog.Chrome.StyleGenericEquipment
                JOIN VehicleCatalog.Chrome.Category
				ON VehicleCatalog.Chrome.Category.CategoryID = VehicleCatalog.Chrome.StyleGenericEquipment.CategoryID
				Where VehicleCatalog.Chrome.StyleGenericEquipment.ChromeStyleID = @ChromeStyleID
                AND VehicleCatalog.Chrome.StyleGenericEquipment.CountryCode = 1
				AND VehicleCatalog.Chrome.Category.CategoryUTF =@drivetrainCat

		select Distinct VehicleCatalog.Chrome.StyleGenericEquipment.ChromeStyleID, VehicleCatalog.Chrome.StyleGenericEquipment.CategoryID, CategoryUTF, Description, StyleAvailability
                From VehicleCatalog.Chrome.StyleGenericEquipment
                JOIN VehicleCatalog.Chrome.Category
				ON VehicleCatalog.Chrome.Category.CategoryID = VehicleCatalog.Chrome.StyleGenericEquipment.CategoryID
				Where VehicleCatalog.Chrome.StyleGenericEquipment.ChromeStyleID = @ChromeStyleID
                AND VehicleCatalog.Chrome.StyleGenericEquipment.CountryCode = 1
				AND VehicleCatalog.Chrome.Category.CategoryUTF =@fuelCat

	END
			
	--select SCOPE_IDENTITY()
END
GO

GRANT EXECUTE ON [builder].[CheckEquipmentMatches] TO MerchandisingUser 
GO








