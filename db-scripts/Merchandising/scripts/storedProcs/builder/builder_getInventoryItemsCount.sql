if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[getInventoryItemsCount]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[getInventoryItemsCount]
GO


/****** Object:  StoredProcedure [builder].[getInventoryItemsCount]    Script Date: 10/29/2008 11:32:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_getInventoryItemsCount.sql,v 1.16 2009/11/09 17:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets count of inventory items that meet the input var filter criteria
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 *	@merchandisingStatus int = 240 - defaults to all merchandising statuses valid values are - 16 - not posted, 32 - pending approval, 64 - online needs attention, 128 - online
 *	@inventoryType int = 0, - defaults to all inventory.  valid vals are 0 - all, 1 - new, 2 - used
 *	@activityFilter int = 7 - defaults to all activities.  valid vals are 1 - needs pricing, 2 - needs description attention, 4 - needs photos
 * @searchParameter - includes its own wildcards to search make, model, stocknumber, vin
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[getInventoryItemsCount]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@merchandisingStatus int = 240,
	@inventoryType int = 2,
	@activityFilter int = 7,
	@searchParameter varchar(20) = ''

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	CREATE TABLE #Inventory (
		BusinessUnitId int NOT NULL,
		Vin varchar(17) NOT NULL,
		StockNumber varchar(15) NOT NULL,
		InventoryReceivedDate datetime NOT NULL, 
		vehicleYear int NOT NULL, 
		UnitCost decimal(9,2) NOT NULL, 
		AcquisitionPrice decimal (8,2) NOT NULL, 
		Certified tinyint NOT NULL, 
		Make varchar(20) NOT NULL, 
		model varchar(50) NOT NULL,
		VehicleTrim varchar(50) NULL,
		inventoryID int NOT NULL,
		MileageReceived int NULL, 
		TradeOrPurchase tinyint NOT NULL, 
		ListPrice decimal(8,2) NULL,
		InventoryType tinyint NOT NULL,
		InventoryStatusCD int NOT NULL,		
		BaseColor varchar(50) NOT NULL,
		VehicleLocation varchar(20) NULL,
		LotPrice DECIMAL(8,2) NULL,
		MSRP DECIMAL(9,2) NULL,
		VehicleCatalogId int NULL
		,PRIMARY KEY (businessUnitId, inventoryId)
	)

INSERT INTO #Inventory EXEC Interface.InventoryList#Fetch @BusinessUnitId=@BusinessUnitID, @UsedOrNew=@inventoryType

SELECT count(*)
    FROM #Inventory inv
    LEFT JOIN builder.OptionsConfiguration oc ON oc.inventoryId = inv.inventoryId AND oc.businessUnitID = inv.businessUnitID
    LEFT JOIN (
		SELECT InventoryId, BusinessUnitId, 
			ISNULL([1], 0) as exteriorStatus,
			ISNULL([2], 0) as interiorStatus,
			ISNULL([3], 0) as photoStatus,
			ISNULL([4], 0) as pricingStatus,
			ISNULL([5], 0) as postingStatus,
			ISNULL([6], 0) as adReviewStatus
			FROM 
			(SELECT InventoryId, BusinessUnitId, StatusTypeId, StatusLevel
				FROM builder.VehicleStatus) vs
			PIVOT
			(
			max(statusLevel)
			FOR StatusTypeId
			IN ([1],[2],[3],[4],[5],[6])
			) as pvt
	) st ON st.inventoryId = inv.inventoryId AND st.businessUnitId = inv.businessUnitId
	WHERE (@inventoryType = 0 OR inv.inventoryType = @inventoryType)
	AND (@merchandisingStatus = 240  --ALL BUCKETS
			OR (@merchandisingStatus = 8  --DO NOT POST LIST
					AND oc.doNotPostFlag = 1
				)
			OR (@merchandisingStatus = 16  --NOT ONLINE NEEDS ATTENTION
					AND (
							oc.doNotPostFlag = 0 AND
							(oc.onlineFlag IS NULL OR oc.onlineFlag = 0  --not online flag (flag = zero)
							)
							AND (
								st.postingStatus IS NULL OR st.postingStatus <= 1)  --not posted and not pending
					)
				)
			OR (@merchandisingStatus = 32  --PENDING APPROVAL
				AND (st.postingStatus = 2) 
				)
			OR (@merchandisingStatus = 64  --ONLINE AND NEEDS ATTENTION
					AND (oc.onlineFlag = 1) 
					AND (st.postingStatus IS NULL OR st.postingStatus <= 1 OR --these two indicate not posted by us or manual needs attention
						st.exteriorStatus IS NULL OR st.exteriorStatus = 0 OR 
						 st.interiorStatus IS NULL OR st.interiorStatus = 0 OR
						st.photoStatus IS NULL OR st.photoStatus = 0 OR
						st.pricingStatus IS NULL OR st.pricingStatus = 0) -- TODO: Does adReviewStatus go here too?
					AND NOT (st.postingStatus = 3)
				)	
			OR (@merchandisingStatus = 128 --ONLINE
					AND (st.postingStatus = 3 OR oc.onlineFlag = 1))  --posting status = 3 (successful release)
		)
	
	AND (@activityFilter = 7 
		OR	(@activityFilter = 1 AND (st.pricingStatus IS NULL OR st.pricingStatus < 1)) 
		OR	(@activityFilter = 2 AND (st.interiorStatus IS NULL OR st.interiorStatus < 1 OR
									  st.exteriorStatus IS NULL OR st.exteriorStatus < 1)) 
		OR	(@activityFilter = 4 AND (st.photoStatus IS NULL OR st.photoStatus < 1)) -- TODO: Does adReviewStatus go here too?
		)
	AND (@searchParameter = '' OR
			(inv.StockNumber LIKE @searchParameter 
				OR inv.Make LIKE @searchParameter 
				OR inv.Model LIKE @searchParameter 
				OR (len(@searchParameter) = 19 --account for percents on search text 
						AND inv.Vin LIKE @searchParameter))
		)
   	
	DROP TABLE #Inventory
END
GO

GRANT EXECUTE ON [builder].[getInventoryItemsCount] TO MerchandisingUser 

GO