if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[getInventoryItem]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[getInventoryItem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- This proc now uses the Workflow.Inventory view.
CREATE PROCEDURE [builder].[getInventoryItem]
	@BusinessUnitID int,
	@InventoryId int
AS
BEGIN

	SET NOCOUNT ON

    select
		Vin, 
		inventoryID,
		StockNumber,
		statusBucket,
		InventoryReceivedDate, 
		vehicleYear, 
		UnitCost, 
		Certified,
		CertifiedID,
		CertifiedProgramId,
		make, 
		model, 
		trim,
		afterMarketTrim,
		marketClass,
		segmentId,
		MileageReceived, 
		TradeOrPurchase, 
		ListPrice, 
		BaseColor,
		ExtColor1,
		ExteriorColorCode,
		ExtColor2,
		ExteriorColorCode2,
		IntColor,
		InteriorColorCode,
		VehicleLocation,
		lastUpdateListPrice,
		datePosted,
		ChromeStyleId,
		VehicleCatalogId,
		lowActivityFlag,
		HasCurrentBookValue,
		exteriorStatus, 
		interiorStatus,
		pricingStatus,
		photoStatus, 
		adReviewStatus,
		postingStatus,
		InventoryType,
		desiredPhotoCount,
		LotPrice,
		MSRP,
		DueForRepricing,
		SpecialPrice,
		HasKeyInformation, -- TODO: Remove this field from the result set (See Case 20442)
		HasCarfax,
		HasCarfaxAsAdmin,
		autoloadStatusTypeId,
		autoloadEndTime,
		NoPackages,
		AutoWindowStickerPrinted		
	from workflow.Inventory
	where BusinessUnitID = @BusinessUnitID
	    and InventoryID = @InventoryID
END
GO
GRANT EXECUTE ON [builder].[getInventoryItem] TO MerchandisingUser 
GO