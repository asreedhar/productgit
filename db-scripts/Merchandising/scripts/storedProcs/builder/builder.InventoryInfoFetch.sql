
/****** Object:  StoredProcedure [builder].[InventoryInfoFetch]    Script Date: 05/28/2015 11:41:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[builder].[InventoryInfoFetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [builder].[InventoryInfoFetch]
GO


/****** Object:  StoredProcedure [builder].[InventoryInfoFetch]    Script Date: 05/28/2015 11:41:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Mohit Singhi>
-- Create date: <09/06/2015>
-- Description:	<SP to retrieve Inventory details for the MAX Pricing tool>
-- =============================================
CREATE PROCEDURE [builder].[InventoryInfoFetch]
	-- Add the parameters for the stored procedure here
	@InventoryId INT,
	@DealerId INT
	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	
	IF (@DealerId != (SELECT BusinessUnitId  FROM Merchandising.workflow.Inventory I
	WHERE  I.InventoryId = @InventoryId))
	RAISERROR ( 'Invalid Inventory Id', 16, 10)
	
	ELSE
	
	DECLARE @RESULTS TABLE (InventoryID INT ,BusinessUnitID INT ,Vin VARCHAR (17),StockNumber VARCHAR(15) ,
StatusBucket INT ,InventoryReceivedDate SMALLDATETIME  ,VehicleYear INT  ,UnitCost DECIMAL ,AcquisitionPrice DECIMAL,
Certified INT ,CertifiedId VARCHAR(20),	Make VARCHAR(50),	Model VARCHAR (255),ChromeStyleId INT ,
Trim VARCHAR(50),	TradeOrPurchase TINYINT  ,MileageReceived INT ,ListPrice DECIMAL,BaseColor VARCHAR(50),
ExtColor1 VARCHAR(100),ExteriorColorCode VARCHAR(10),ExtColor2 VARCHAR(100),ExteriorColorCode2 VARCHAR(10),
IntColor VARCHAR(100),InteriorColorCode VARCHAR(10),VehicleLocation VARCHAR(20),InventoryStatusCD TINYINT,
DescriptionSample VARCHAR(MAX),LastUpdateListPrice DECIMAL ,DatePosted DATETIME,exteriorStatus INT ,
interiorStatus INT ,photoStatus INT  ,adReviewStatus INT ,pricingStatus INT ,postingStatus INT  ,onlineFlag SMALLINT ,
InventoryType TINYINT ,DesiredPhotoCount INT,LotPrice DECIMAL,MSRP DECIMAL,SpecialPrice DECIMAL,
DueForRepricing BIT,lowActivityFlag BIT,HasCurrentBookValue BIT,HasKeyInformation BIT,HasCarfax BIT,
HasCarfaxAsAdmin BIT,Name VARCHAR(50),CertifiedProgramId INT ,ModelId INT ,MakeID  INT  ,CarsDotComMakeID INT ,
CarsDotComModelId INT,AutoTraderMake VARCHAR(100),AutoTraderModel VARCHAR(100))


INSERT INTO @RESULTS
        ( InventoryID ,
          BusinessUnitID ,
          Vin ,
          StockNumber ,
          StatusBucket ,
          InventoryReceivedDate ,
          VehicleYear ,
          UnitCost ,
          AcquisitionPrice ,
          Certified ,
          CertifiedId ,
          Make ,
          Model ,
          ChromeStyleId ,
          Trim ,
          TradeOrPurchase ,
          MileageReceived ,
          ListPrice ,
          BaseColor ,
          ExtColor1 ,
          ExteriorColorCode ,
          ExtColor2 ,
          ExteriorColorCode2 ,
          IntColor ,
          InteriorColorCode ,
          VehicleLocation ,
          InventoryStatusCD ,
          DescriptionSample ,
          LastUpdateListPrice ,
          DatePosted ,
          exteriorStatus ,
          interiorStatus ,
          photoStatus ,
          adReviewStatus ,
          pricingStatus ,
          postingStatus ,
          onlineFlag ,
          InventoryType ,
          DesiredPhotoCount ,
          LotPrice ,
          MSRP ,
          SpecialPrice ,
          DueForRepricing ,
          lowActivityFlag ,
          HasCurrentBookValue ,
          HasKeyInformation ,
          HasCarfax ,
          HasCarfaxAsAdmin ,
          Name ,
          CertifiedProgramId ,
          ModelId ,
          MakeID 
        )
	
SELECT 
	I.InventoryID,	I.BusinessUnitID,	I.Vin,	I.StockNumber,	I.StatusBucket,	I.InventoryReceivedDate,	I.VehicleYear AS [Year],	I.UnitCost,
	I.AcquisitionPrice,	i.Certified,	I.CertifiedId,	I.Make,	I.Model,I.ChromeStyleId,	I.Trim,	I.TradeOrPurchase,	I.MileageReceived,
	I.ListPrice,	I.BaseColor,	I.ExtColor1,	I.ExteriorColorCode,	I.ExtColor2,	I.ExteriorColorCode2,	I.IntColor,
	I.InteriorColorCode,	I.VehicleLocation,	I.InventoryStatusCD,	I.DescriptionSample,	I.LastUpdateListPrice,
	I.DatePosted,	I.exteriorStatus,	I.interiorStatus,	I.photoStatus,I.adReviewStatus,I.pricingStatus,I.postingStatus,
	I.onlineFlag,I.InventoryType,i.DesiredPhotoCount,I.LotPrice,I.MSRP,I.SpecialPrice,I.DueForRepricing,i.lowActivityFlag,
	I.HasCurrentBookValue,I.HasKeyInformation,I.HasCarfax,I.HasCarfaxAsAdmin,CP.Name,I.CertifiedProgramId	
	,MMG.ModelId
	,CAST (VCM.MakeID AS INT) AS MakeID 
  
    FROM Merchandising.workflow.Inventory I             
    JOIN IMT.dbo.Inventory WI ON I.InventoryID= WI.InventoryID        
    JOIN FLDW.dbo.Vehicle V ON V.VehicleID=WI.VehicleID AND V.BusinessUnitID=I.BusinessUnitID
    LEFT JOIN FLDW.dbo.MakeModelGrouping MMG ON MMG.MakeModelGroupingID=V.MakeModelGroupingID AND V.BusinessUnitID=I.BusinessUnitID
    LEFT JOIN [VehicleCatalog].[Categorization].[Make] VCM ON I.Make = VCM.Name
    LEFT JOIN IMT.dbo.Inventory_CertificationNumber CN ON I.InventoryID = CN.InventoryID 
    LEFT JOIN IMT.Certified.CertifiedProgram CP ON I.CertifiedProgramId = CP.CertifiedProgramId   
	WHERE I.InventoryID = @InventoryId
	
	
	
	UPDATE @RESULTS SET CarsDotComMakeID=pc2.value,CarsDotComModelId=pc.value
	FROM  IMT.dbo.Inventory I 
    JOIN FLDW.dbo.Vehicle V ON V.VehicleID=I.VehicleID AND V.BusinessUnitID=I.BusinessUnitID
    LEFT JOIN FLDW.dbo.MakeModelGrouping MMG ON MMG.MakeModelGroupingID=V.MakeModelGroupingID AND V.BusinessUnitID=I.BusinessUnitID
    JOIN [IMT]..PING_GroupingCodes PGC ON  MMG.MakeModelGroupingID = PGC.MakeModelGroupingID
	JOIN [IMT]..PING_CODE PC ON PGC.CodeID = PC.ID
	JOIN [IMT]..PING_CODE PC2 ON PC.ParentID = PC2.ID
	WHERE PC.providerId=2 AND I.InventoryID=@InventoryId
	
	UPDATE @RESULTS SET AutoTraderMake=pc2.value,AutoTraderModel=pc.value
	FROM  IMT.dbo.Inventory I 
    JOIN FLDW.dbo.Vehicle V ON V.VehicleID=I.VehicleID AND V.BusinessUnitID=I.BusinessUnitID
    LEFT JOIN FLDW.dbo.MakeModelGrouping MMG ON MMG.MakeModelGroupingID=V.MakeModelGroupingID AND V.BusinessUnitID=I.BusinessUnitID
    JOIN [IMT]..PING_GroupingCodes PGC ON  MMG.MakeModelGroupingID = PGC.MakeModelGroupingID
	JOIN [IMT]..PING_CODE PC ON PGC.CodeID = PC.ID
	JOIN [IMT]..PING_CODE PC2 ON PC.ParentID = PC2.ID
	WHERE pc2.providerId=4 AND I.InventoryID=@InventoryId
	
	SELECT * FROM @RESULTS
	
	
END



GO

GRANT EXECUTE ON builder.InventoryInfoFetch TO MerchandisingUser 
GO