use Merchandising

if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[updateVehicleStyleID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[updateVehicleStyleID]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_updateVehicleStyleID.sql,v 1.4 2008/11/25 14:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * updates the saved styleid for a vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId = vehicle inventory id
 * @ChromeStyleID = chrome style id to use for vehicle
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[updateVehicleStyleID]
	
	@BusinessUnitID int,
	@InventoryId int,
	@ChromeStyleID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

UPDATE builder.OptionsConfiguration 
    SET chromeStyleID = @ChromeStyleID 
    WHERE BusinessUnitID = @BusinessUnitID AND inventoryId = @InventoryId

exec workflow.UpdateOptionsConfigurationNoPackagesField @BusinessUnitID = @BusinessUnitId, @InventoryId = @InventoryId

END
GO

GRANT EXECUTE ON [builder].[updateVehicleStyleID] TO MerchandisingUser 
GO