IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[builder].[Autoload#ReloadInventory]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [builder].[Autoload#ReloadInventory]
GO

CREATE PROC builder.Autoload#ReloadInventory @businessUnitID int, 
											@manufactureId int = null,
											@resetStatus bit = 0,
											@resetStyle bit = 0,
											@resetOptions bit = 0,
											@resetColors bit = 0,
											@MemberLogin varchar(30) = 'Autoload#ReloadInventory'
AS
	SET NOCOUNT ON

	BEGIN TRY
		-- check options ... if nothing just return status
		if @manufactureId = null OR (@resetStatus = 0 AND @resetStyle = 0 AND @resetOptions = 0 AND @resetColors = 0)
			begin
				EXEC builder.Autoload#InventoryStatus @businessUnitID = @businessUnitID,
														@manufactureId = @manufactureId
				return
			end
			
		-- update autoload status to -9 (delayed)
		if @resetStatus = 1
			begin
				UPDATE AutoloadStatus
					SET statusTypeId = -9,
			            lastUpdatedOn = getdate(),
			            lastUpdatedBy = @MemberLogin
					FROM builder.autoLoadStatus 
					INNER JOIN workflow.inventory ON Inventory.BusinessUnitID = autoloadStatus.businessUnitID
															AND Inventory.InventoryID = autoloadStatus.InventoryID
					INNER JOIN VehicleCatalog.Chrome.Divisions ON Divisions.CountryCode = 1
																AND Divisions.ManufacturerID = @manufactureId
																AND inventory.Make = Divisions.DivisionName
					WHERE inventory.businessUnitID = @BusinessUnitID
			end
		
		-- clear chrome style ID from optionsConfiguration
		if @resetStyle = 1
			begin
				UPDATE OptionsConfiguration
					SET chromeStyleID = -1,
			            lastUpdatedOn = getdate(),
			            lastUpdatedBy = @MemberLogin
					FROM builder.OptionsConfiguration
					INNER JOIN workflow.inventory ON Inventory.BusinessUnitID = OptionsConfiguration.businessUnitID
															AND Inventory.InventoryID = OptionsConfiguration.InventoryID
					INNER JOIN VehicleCatalog.Chrome.Divisions ON Divisions.CountryCode = 1
																AND Divisions.ManufacturerID = @manufactureId
																AND inventory.Make = Divisions.DivisionName
					WHERE inventory.businessUnitID = @BusinessUnitID
					AND inventory.autoloadStatusTypeId IS NOT NULL  
			end
			
		-- delete vehicle delete options
		if @resetOptions = 1
			begin

				DELETE VehicleDetailedOptions
					FROM builder.VehicleDetailedOptions
					INNER JOIN workflow.inventory ON Inventory.BusinessUnitID = VehicleDetailedOptions.businessUnitID
															AND Inventory.InventoryID = VehicleDetailedOptions.InventoryID
					INNER JOIN VehicleCatalog.Chrome.Divisions ON Divisions.CountryCode = 1
																AND Divisions.ManufacturerID = @manufactureId
																AND inventory.Make = Divisions.DivisionName
					WHERE inventory.businessUnitID = @BusinessUnitID
					AND inventory.autoloadStatusTypeId IS NOT NULL  
			
			end
			
		-- reset colors
		if @resetColors = 1
			begin
				UPDATE OptionsConfiguration
					SET intColor = '',
						extColor1 = '',
						extColor2 = '',
						InteriorColorCode = null,
						ExteriorColorCode = null,
						ExteriorColorCode2 = null,
			            lastUpdatedOn = getdate(),
			            lastUpdatedBy = @MemberLogin
					FROM builder.OptionsConfiguration
					INNER JOIN workflow.inventory ON Inventory.BusinessUnitID = OptionsConfiguration.businessUnitID
															AND Inventory.InventoryID = OptionsConfiguration.InventoryID
					INNER JOIN VehicleCatalog.Chrome.Divisions ON Divisions.CountryCode = 1
																AND Divisions.ManufacturerID = @manufactureId
																AND inventory.Make = Divisions.DivisionName
					WHERE inventory.businessUnitID = @BusinessUnitID
					AND inventory.autoloadStatusTypeId IS NOT NULL  
			end
	
	
    END TRY
    BEGIN CATCH
    
			DECLARE @ErrorMessage NVARCHAR(4000),
			  @ErrorNumber INT,
			  @ErrorSeverity INT,
			  @ErrorState INT,
			  @ErrorLine INT,
			  @ErrorProcedure NVARCHAR(200) ;

				-- Assign variables to error-handling functions that 
				-- capture information for RAISERROR.
			SELECT @ErrorNumber = ERROR_NUMBER(), @ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE(), @ErrorLine = ERROR_LINE(),
					@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-') ;

				-- Building the message string that will contain original
				-- error information.
			SET @ErrorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' +
					'Message: ' + ERROR_MESSAGE() ;

				-- Raise an error: msg_str parameter of RAISERROR will contain
				-- the original error information.
			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, -- parameter: original error number.
			  @ErrorSeverity, -- parameter: original error severity.
			  @ErrorState, -- parameter: original error state.
			  @ErrorProcedure, -- parameter: original error procedure name.
			  @ErrorLine-- parameter: original error line number.
					)

    END CATCH
		
	SET NOCOUNT OFF
	
RETURN
GO

GRANT EXECUTE ON [builder].[Autoload#ReloadInventory] TO MerchandisingUser
go
