if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[KeyInformation#Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[KeyInformation#Save]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_keyInformation_save.sql,v 1.1 2009/11/09 17:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Save the key info for a vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - integer id of businessunit
 * @InventoryId - integer id of the inventory item
 * @InfoId
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[KeyInformation#Save]
	
	@BusinessUnitId int,
	@InventoryId int,
	@InfoId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO
		builder.Inventory_KeyInformation			
	(businessUnitId, inventoryId, infoId)
	VALUES
	(@BusinessUnitId, @InventoryId, @InfoId)	

END
GO

GRANT EXECUTE ON [builder].[KeyInformation#Save] TO MerchandisingUser 
GO