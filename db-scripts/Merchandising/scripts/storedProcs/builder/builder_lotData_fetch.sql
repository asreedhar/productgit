if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[LotData#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[LotData#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * fetch the lot data for a vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - needed for rankings data
 * @InventoryId - inventoryId of vehicle
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[LotData#Fetch]
	@BusinessUnitId int,
	@InventoryId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--DO NOT INLINE OR CHANGE THIS TABLE - CONTRACTUAL INTERFACE TO CODE
	CREATE TABLE #Inventory (
		BusinessUnitId int NOT NULL,
		Vin varchar(17) NOT NULL,
		StockNumber varchar(15) NOT NULL,
		InventoryReceivedDate datetime NOT NULL, 
		vehicleYear int NOT NULL, 
		UnitCost decimal(9,2) NOT NULL, 
		AcquisitionPrice decimal (8,2) NOT NULL, 
		Certified tinyint NOT NULL, 
		Make varchar(20) NOT NULL, 
		model varchar(50) NOT NULL,
		VehicleTrim varchar(50) NULL,
		inventoryID int NOT NULL,
		MileageReceived int NULL, 
		TradeOrPurchase tinyint NOT NULL, 
		ListPrice decimal(8,2) NULL,
		InventoryType tinyint NOT NULL,
		InventoryStatusCD int NOT NULL,
		BaseColor varchar(50) NOT NULL,
		VehicleLocation varchar(20) NULL
		,ChromeStyleId int NULL
		,LotPrice decimal(8,2)
		,MSRP DECIMAL(9,2) NULL,
		VehicleCatalogId int null
	)
	
	--fetch the inventory item, to get the vin
	INSERT INTO #Inventory exec Interface.InventoryItem#Fetch @BusinessUnitId, @InventoryId
			
	declare @Vin varchar(17)
	select @Vin = vin
	FROM #Inventory
	WHERE inventoryId = @InventoryId
			
	declare @lotProviderId int

	--select the lotProviderId
	select @lotProviderId = LotProviderId 
	FROM settings.merchandising 
	WHERE businessUnitId = @BusinessUnitId
	
	select 
		cl.color, 
		ve.ModelYear, 
		mk.Make, 
		mdl.Model, 
		tr.Trim,
		ve.ListPrice, 
		ve.Mileage, 
		ve.Certified
	FROM Lot.Listing.Vehicle ve
	JOIN Lot.Listing.Make mk
		ON mk.makeId = ve.makeId
	JOIN Lot.Listing.Model mdl
		ON mdl.modelId = ve.modelId
	JOIN Lot.Listing.Trim tr
		ON tr.trimId = ve.trimId
	JOIN Lot.Listing.Color cl
		ON cl.colorId = ve.colorId
	WHERE 
		ve.VIN = @Vin
		AND (
			@lotProviderId IS NULL 
			OR ve.providerId = @lotProviderId
			)	

			
END
GO

GRANT EXECUTE ON [builder].[LotData#Fetch] TO MerchandisingUser 
GO