if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleOption#BulkSave]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[VehicleOption#BulkSave]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [builder].[VehicleOption#BulkSave]
	@businessUnitId int,
	@inventoryId int,
    @vehicleOptions [builder].[VehicleOption] READONLY
AS
	BEGIN
	
		SET NOCOUNT ON;
		
		WITH currentVehicleOptions
              AS ( SELECT  VehicleOptions.[vehicleOptionID]
                          , VehicleOptions.[businessUnitID]
                          , VehicleOptions.[optionID]
                          , VehicleOptions.[optionCode]
                          , VehicleOptions.[detailText]
                          , VehicleOptions.[optionText]
                          , VehicleOptions.[inventoryId]
                          , VehicleOptions.[isStandardEquipment]
                  FROM    builder.VehicleOptions vehicleOptions
                  WHERE vehicleOptions.businessUnitID = @businessUnitId
                  AND   vehicleOptions.inventoryId = @inventoryId
                )
        MERGE currentVehicleOptions AS target
        USING
            ( SELECT    @businessUnitID,
						@inventoryId,
						[OptionID]
                      , [OptionCode]
                      , [OptionText]
                      , [DetailText]
                      , [IsStandardEquipment]
              FROM      @vehicleOptions
            ) AS source ( BusinessUnitId, InventoryId, OptionID, OptionCode, OptionText, DetailText, IsStandardEquipment )
        ON ( [target].[BusinessUnitId] = [source].[BusinessUnitId]
            AND [target].[InventoryId] = [source].[InventoryId]
            AND [target].[OptionID] = [source].[OptionID]
          )
        WHEN NOT MATCHED BY SOURCE THEN
            DELETE
            -- remove invalid entries if the table parameter contains a complete set
        WHEN MATCHED AND (
                        target.[optionCode]!= [source].[OptionCode]
						OR target.[detailText] != [source].[DetailText]
						OR target.[optionText] != [source].[OptionText]
						OR target.[IsStandardEquipment] != [source].[IsStandardEquipment])
					THEN
            UPDATE SET
                    [optionCode] = [source].[OptionCode]
                  , [detailText] = [source].[DetailText]
                  , [optionText] = [source].[OptionText]
                  , [IsStandardEquipment] = [source].[IsStandardEquipment]
        WHEN NOT MATCHED BY TARGET THEN
            INSERT ( [BusinessUnitID]
                  , [InventoryId]
                  , [OptionID]
                  , [OptionCode]
                  , [OptionText]
                  , [DetailText]
                  , [IsStandardEquipment]
                  )
            VALUES ( [source].[BusinessUnitID]
                  , [source].[InventoryId]
                  , [source].[OptionID]
                  , [source].[OptionCode]
                  , [source].[OptionText]
                  , [source].[DetailText]
                  , [source].[IsStandardEquipment]
                  )	;

		SET NOCOUNT OFF


	END

GO

GRANT EXECUTE ON [builder].[VehicleOption#BulkSave] TO MerchandisingUser 
GO
