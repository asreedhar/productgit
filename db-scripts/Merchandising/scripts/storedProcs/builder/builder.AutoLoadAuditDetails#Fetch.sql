if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[AutoLoadAuditDetails#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[AutoLoadAuditDetails#Fetch]
GO

CREATE PROCEDURE [builder].[AutoLoadAuditDetails#Fetch]
	@BusinessUnitID int
	
AS
BEGIN

	SELECT V.Vin, ALS.StartTime, ALS.EndTime, ALST.StatusType, MMG.Make, I.InventoryType, i.InventoryID, Manufacturers.ManufacturerName,
				Divisions.ManufacturerId, Divisions.DivisionId
		FROM Merchandising.Builder.AutoloadStatus ALS
		JOIN FLDW.dbo.InventoryActive I ON I.InventoryID = ALS.InventoryID AND ALS.BusinessUnitID = I.BusinessUnitID
		JOIN IMT.dbo.tbl_Vehicle V ON I.VehicleID = V.VehicleID
		JOIN IMT.dbo.MakeModelGrouping MMG ON V.MakeModelGroupingID = MMG.MakeModelGroupingID
		JOIN Merchandising.Builder.AutoloadStatusTypes ALST ON ALS.StatusTypeID = ALST.StatusTypeID
		LEFT OUTER JOIN VehicleCatalog.Chrome.Divisions ON mmg.Make = Divisions.DivisionName
															AND divisions.CountryCode = 1
		LEFT OUTER JOIN VehicleCatalog.Chrome.Manufacturers ON Divisions.ManufacturerID = Manufacturers.ManufacturerID
															AND Divisions.CountryCode = Manufacturers.CountryCode
		WHERE ALS.BusinessUnitID = @BusinessUnitID
		ORDER BY EndTime DESC

END
GO

GRANT EXECUTE ON [builder].[AutoLoadAuditDetails#Fetch] TO MerchandisingUser
GO

