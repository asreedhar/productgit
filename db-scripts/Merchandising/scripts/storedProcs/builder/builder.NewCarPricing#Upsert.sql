IF OBJECT_ID('builder.NewCarPricing#Upsert') IS NOT NULL
	DROP PROCEDURE builder.NewCarPricing#Upsert
GO
CREATE PROCEDURE [builder].[NewCarPricing#Upsert]
	@InventoryID int,
	@SpecialPrice decimal(9,2),
	@UserName varchar(80) = SUSER_SNAME,
	@ProcessName varchar(80) = SUSER_SNAME
AS
    SET NOCOUNT ON;
    WITH    inventoryNewCarPricing
              AS ( SELECT   *
                   FROM     [builder].[NewCarPricing] a
                   WHERE    [a].[inventoryId] = @inventoryId
                 )
        MERGE inventoryNewCarPricing AS target
        USING
            ( SELECT @InventoryID, @SpecialPrice
            ) AS source ( InventoryId, SpecialPrice )
        ON ( [target].[InventoryId] = [source].[InventoryId] )
        WHEN MATCHED AND ( [target].[SpecialPrice] != [source].[SpecialPrice] ) THEN
            UPDATE SET
                    [SpecialPrice] = @SpecialPrice
                  , [UpdatedOn] = GETDATE()
				  , [UpdatedBy] = @UserName
				  , [ProcessName] = @ProcessName
        WHEN NOT MATCHED BY TARGET THEN
            INSERT ( [InventoryID]
                   , [SpecialPrice]
                   , [CreatedOn]
                   , [UpdatedOn]
                   , [CreatedBy]
                   , [UpdatedBy]
				   , [ProcessName]
                   )
            VALUES ( [source].[InventoryID]
                   , [source].[SpecialPrice]
                   , GETDATE()
                   , GETDATE()
				   , @UserName
				   , @UserName
				   , @ProcessName
                   )
        OUTPUT
            $ACTION
          , ISNULL(INSERTED.[InventoryID], DELETED.[InventoryID])
          , ISNULL(INSERTED.[SpecialPrice], DELETED.[SpecialPrice])
          , ISNULL(INSERTED.[CreatedOn], DELETED.[CreatedOn])
          , ISNULL(INSERTED.[UpdatedOn], DELETED.[UpdatedOn])
          , ISNULL(INSERTED.[CreatedBy], DELETED.[CreatedBy])
          , ISNULL(INSERTED.[UpdatedBy], DELETED.[UpdatedBy])
		  , ISNULL(INSERTED.[ProcessName], DELETED.[ProcessName])
            INTO [MerchandisingArchive].[builder].[NewCarPricingAudit] ;
    SET NOCOUNT OFF
GO
GRANT EXECUTE ON builder.NewCarPricing#Upsert to MerchandisingUser
GO