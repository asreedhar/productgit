/****** Object:  StoredProcedure [builder].[FetchTierOneEquipments]    Script Date: 05/05/2015 08:33:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[builder].[FetchTierOneEquipments]') AND type in (N'P', N'PC'))
DROP PROCEDURE [builder].[FetchTierOneEquipments]
GO


/****** Object:  StoredProcedure [builder].[FetchTierOneEquipments]    Script Date: 05/05/2015 08:33:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:	Pradnya Sawant
-- Create date: 29/04/2015
-- Description:Get the Tier-1 Equipments for an inventory
-- =============================================
CREATE PROCEDURE [builder].[FetchTierOneEquipments]
 
@BusinessUnitId int,
@InventoryId int
AS
BEGIN

	SET NOCOUNT ON;
	
DECLARE 
@rankBuid int,
@localBusinessUnitId int,
@localInventoryId int

SELECT @localBusinessUnitId=@BusinessUnitId,@localInventoryId=@InventoryId

IF EXISTS (SELECT 1 FROM settings.EquipmentDisplayRankings WHERE businessUnitId = @localBusinessUnitId)
	SELECT @rankBuid = @localBusinessUnitId
ELSE
	SELECT @rankBuid = 100150
	
		SELECT 
		DISTINCT coalesce(MSCD.Description,edr.displayDescription) as userFriendlyName -- this is used for displaying the equipment item as it appears in Max
		, EDR.tierNumber as tierNumber
		, ct.UserFriendlyName AS CategoryName -- This is used for matching equipment items to market listings
			FROM builder.vehicleOptions VO
			--join categories to get full category objeCT for
			--use by application logic
			INNER JOIN vehicleCatalog.CHrome.Categories AS CT
				ON CT.categoryID = VO.optionID
			INNER JOIN vehicleCatalog.CHrome.CategoryHeaders AS CH 
				ON CH.CategoryHeaderID = CT.CategoryHeaderID 
			INNER JOIN 
				settings.EquipmentDisplayRankings EDR
				ON EDR.businessUnitId = @RankBuid
				AND EDR.categoryId = VO.optionId	
			INNER JOIN 
				builder.OptionsConfiguration OC
				ON OC.businessunitid = VO.businessunitid
				AND OC.inventoryid = VO.inventoryid
			LEFT JOIN CHrome.ManufaCTurerSpecificCategoryDescriptions MSCD
				ON MSCD.CHromeStyleID = OC.CHromeStyleID
				and MSCD.CategoryID = VO.optionID	
			WHERE VO.businessunitid = @localBusinessUnitId 
				AND VO.inventoryId = @localInventoryId
				AND CH.countrycode = 1 and CT.countrycode = 1 
				AND CT.categoryTypeFilter != 'Transmission - Type'
				--AND EDR.tierNumber = 1
				ORDER BY EDR.tierNumber ASC
			    
			
			
	SET NOCOUNT OFF;
END


GO

GRANT EXECUTE ON [builder].[FetchTierOneEquipments] TO MerchandisingUser 
GO
