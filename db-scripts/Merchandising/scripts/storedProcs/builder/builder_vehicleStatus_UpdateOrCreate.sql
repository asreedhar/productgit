if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleStatus#UpdateOrCreate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[VehicleStatus#UpdateOrCreate]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_vehicleStatus_UpdateOrCreate.sql,v 1.2 2009/04/18 02:18:45 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * update the status flags for a build step for a given vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId- vehicle inventory id
 * @MemberLogin - firstlook login of member who is updating the status
 * @StatusTypeId - status type of the update
 * @StatusLevel - level to indicate the status
 * @Version - timestamp for matching record concurrency
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[VehicleStatus#UpdateOrCreate]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@InventoryId int,
	@MemberLogin varchar(30),
	@StatusTypeId int,
	@StatusLevel int,
	@Version timestamp = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF EXISTS (SELECT 1 FROM builder.VehicleStatus 
				WHERE businessUnitId = @BusinessUnitId 
				AND inventoryId = @InventoryId 
				AND statusTypeId = @StatusTypeId
				AND (@Version IS NULL OR version = @Version))
	BEGIN
		--it exists...only update if we are actually changing value to 2 (isn't 2 already)
		EXEC builder.VehicleStatus#Update @BusinessUnitId = @BusinessUnitId, @InventoryId = @InventoryId,
			@StatusTypeId = @StatusTypeId, @StatusLevel = @StatusLevel, @MemberLogin = @MemberLogin, @Version = @Version
	END
	ELSE
	BEGIN
	--it does not exists, create it
		EXEC builder.VehicleStatus#Create @BusinessUnitId = @BusinessUnitId, @InventoryId = @InventoryId,
			@StatusTypeId = @StatusTypeId, @StatusLevel = @StatusLevel, @MemberLogin = @MemberLogin
	END
		
END
GO

GRANT EXECUTE ON [builder].[VehicleStatus#UpdateOrCreate] TO MerchandisingUser 
GO

