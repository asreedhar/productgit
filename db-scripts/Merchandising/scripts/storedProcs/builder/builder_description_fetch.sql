if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[Description#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[Description#Fetch]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_description_fetch.sql,v 1.6 2010/02/07 22:12:23 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch description for a vehicle or return a new one
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId= vehicle inventory id
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[Description#Fetch]
	
	@BusinessUnitID int,
	@InventoryId int
		
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	
	IF EXISTS (SELECT 1 FROM builder.Descriptions 
				WHERE BusinessUnitID = @BusinessUnitID AND inventoryId = @InventoryId)
	BEGIN
			SELECT 
			acceleratorAdvertisementId, 
			merchandisingDescription,
			footer,
			lastUpdatedOn,
			lastUpdatedBy,
			lastUpdateListPrice,
			isAutoPilot,
			isLongForm,
			expiresOn,
			templateId,
			themeId,
			version
			FROM builder.Descriptions 
			WHERE BusinessUnitID = @BusinessUnitID 
				AND inventoryId = @InventoryId
				
			--------------------------------------
			--FETCH VEHICLE DESCRIPTION CONTENTS
			--------------------------------------	
			SELECT descriptionItemTypeId
			FROM builder.descriptionContents
			WHERE inventoryId = @InventoryId
			AND businessUnitId = @BusinessUnitId
			
			--------------------------------------
			--FETCH VEHICLE SAMPLE TEXT IDS
			--------------------------------------	
			SELECT blurbTextId
			FROM builder.Description_SampleTextItems
			WHERE inventoryId = @InventoryId
			AND businessUnitId = @BusinessUnitId
			ORDER BY textSequence ASC
			

	END
	ELSE
	BEGIN
		SELECT 
			NULL as acceleratorAdvertisementId, 
			'' as merchandisingDescription,
			'' as footer,
			'' as lastUpdatedBy,
			null as lastUpdatedOn,
			null as lastUpdateListPrice,
			cast(0 as bit) as isAutoPilot,
			cast(0 as bit) as isLongForm,
			NULL as version,
			NULL as expiresOn,
			NULL as templateId,
			NULL as themeId
			FROM builder.Descriptions 
			where 0=1
	END	

END
GO

GRANT EXECUTE ON [builder].[Description#Fetch] TO MerchandisingUser 
GO