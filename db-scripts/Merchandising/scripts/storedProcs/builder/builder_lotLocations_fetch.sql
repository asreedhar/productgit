if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[LotLocations#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[LotLocations#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_lotLocations_fetch.sql,v 1.1 2008/12/09 18:24:16 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch the lot locations for a given business unit
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - integer id of businessunit
 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[LotLocations#Fetch]
	
	@BusinessUnitId int

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT lotLocationId, lotLocation
	FROM settings.lotLocations
	WHERE businessUnitId = @BusinessUnitID
	
			
END
GO

GRANT EXECUTE ON [builder].[LotLocations#Fetch] TO MerchandisingUser 
GO