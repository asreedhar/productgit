if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[FinancingSpecial#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[FinancingSpecial#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_financingSpecial_fetch.sql,v 1.4 2009/10/06 14:40:52 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch the specified special
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - integer id of businessunit
 * @SpecialId - integer id of the special
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[FinancingSpecial#Fetch]
	
	@SpecialId int = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT fs.specialId,
	fs.ownerEntityTypeId, 
	fs.ownerEntityId, 
	fs.title as specialTitle,
	fs.description,
	fs.expiresOn,
	fs.onlyCertifieds, 
	fs.vehicleProfileId,
	vp.title, vp.makeIdList, vp.modelList, vp.trimList, vp.segmentIdList, vp.marketClassIdList, 
	vp.startYear, vp.endYear, vp.minAge, vp.maxAge, vp.minPrice, vp.maxPrice, vp.certifiedStatusFilter
	FROM builder.FinancingSpecials fs
	LEFT JOIN templates.vehicleProfiles vp
		ON vp.vehicleProfileId = fs.vehicleProfileId
	
	WHERE 
		
		specialId = @SpecialId
		AND (expiresOn IS NULL OR expiresOn > getdate())	
	
	
END
GO

GRANT EXECUTE ON [builder].[FinancingSpecial#Fetch] TO MerchandisingUser 
GO