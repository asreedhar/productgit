if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[FinancingSpecial#Create]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[FinancingSpecial#Create]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_financingSpecial_create.sql,v 1.2 2009/02/10 23:55:08 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * create a financing special for a dealer - may contain a certification type
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - integer id of businessunit
 * @CertificationTypeId - integer id of the certification type to fetch
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[FinancingSpecial#Create]
	
	@OwnerEntityTypeId int,
	@OwnerEntityId int,
	@SpecialTitle varchar(50),
	@Description varchar(300),
	@ExpiresOn datetime,
	@OnlyCertifieds bit = 0,
	@VehicleProfileId int = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


INSERT INTO builder.financingSpecials
([ownerEntityTypeId],
 [ownerEntityId],
	[title],
	[description],
	[expiresOn],
	[onlyCertifieds],
	[vehicleProfileId])
VALUES  (@OwnerEntityTypeId,
		@OwnerEntityId,
		@SpecialTitle,
		@Description,
		@ExpiresOn,
		@OnlyCertifieds,
		@VehicleProfileId)
END
GO

GRANT EXECUTE ON [builder].[FinancingSpecial#Create] TO MerchandisingUser 
GO