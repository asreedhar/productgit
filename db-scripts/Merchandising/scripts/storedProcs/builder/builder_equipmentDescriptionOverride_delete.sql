if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[EquipmentDescriptionOverride#Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[EquipmentDescriptionOverride#Delete]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_equipmentDescriptionOverride_delete.sql,v 1.1 2010/01/28 23:12:44 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * deletes an override
 * 
 * 
 * Parameters
 * ----------
 * @BusinessUnitId int - businessUnit requesting their overrides
 * @ChromeStyleId     - style id of vehicle for overrides
 * @OptionCode - code to remove customization
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[EquipmentDescriptionOverride#Delete]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@ChromeStyleID int,
	@OptionCode varchar(20)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--get all ids that need deletion
	declare @idsToDelete table (id int)
	insert into @idsToDelete (id) 
	SELECT ded.equipmentDescriptionId
	FROM 
		builder.DetailedEquipmentDescriptions ded
	JOIN builder.Vehicle_DetailedEquipmentDescriptions vded
		ON vded.equipmentDescriptionid = ded.equipmentDescriptionId
	WHERE optionCode = @OptionCode
	and ChromeStyleId = @ChromeStyleId
	and businessUnitId = @BusinessUnitId
	
	--if there weren't any matching styleIds, we need to delete it for the entire model
	if (@@ROWCOUNT <= 0)
	BEGIN
		insert into @idsToDelete (id)
		select ded.equipmentDescriptionId
		FROM 
		builder.DetailedEquipmentDescriptions ded
		JOIN builder.Vehicle_DetailedEquipmentDescriptions vded
			ON vded.equipmentDescriptionid = ded.equipmentDescriptionId		
		WHERE optionCode = @OptionCode
		and ModelId = (Select modelId from vehiclecatalog.chrome.styles where styleId = @ChromeStyleId)
		and businessUnitId = @BusinessUnitId	
	END
	
	
	delete from 
	builder.Vehicle_DetailedEquipmentDescriptions
	where equipmentDescriptionid IN (select id from @idsToDelete)
		
	--delete if it's in the deleted list, and also no longer referenced by another vehicle (could be referenced by other styleIds)
	delete from 
	builder.DetailedEquipmentDescriptions
	where equipmentDescriptionid IN (select id from @idsToDelete)
	and equipmentDescriptionId NOT IN (select equipmentDescriptionId from builder.Vehicle_DetailedEquipmentDescriptions)
	
	
END
GO

GRANT EXECUTE ON [builder].[EquipmentDescriptionOverride#Delete] TO MerchandisingUser 
GO