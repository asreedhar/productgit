if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[LotDataImportLock#Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[LotDataImportLock#Update]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_lotDataImportLock_update.sql,v 1.1 2009/05/01 23:03:21 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * update the lot locations for a given business unit
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - integer id of businessunit
 * @InventoryId - inventory item id
 * @LotDataImportLock - bit (true) when lot data is locked out (i.e. should not be added to the vehicle)
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[LotDataImportLock#Update]
	
	@BusinessUnitId int,
	@InventoryId int,
	@LotDataImportLock bit

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE builder.optionsConfiguration
	SET LotDataImportLock = @LotDataImportLock
	WHERE businessUnitId = @BusinessUnitID
	AND inventoryId = @InventoryId
	
			
END
GO

GRANT EXECUTE ON [builder].[LotDataImportLock#Update] TO MerchandisingUser 
GO