if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[EquipmentMappingUpsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[EquipmentMappingUpsert]
GO

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


-- =============================================
-- Author:		<Fitzpatrick, Benin>
-- Create date: <2/2/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_EquipmentMappingUpsert.sql,v 1.1 2010/03/10 15:05:02 bfitzpatrick Exp $
 * 
 * Summary finally
 * -------
 * 
 * Update or insert any given set of Equipment to a vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @inventoryID
 * @businessUnitID - defaults to 
 * @EngineTypeCategoryID - 
 * @TransmissionTypeCategoryID - 
 * @DriveTrainCategoryID - 
 * @FuelCategoryID -
 * @createdBy	
 * @createdOn
 * @updatedBy
 * @updatedOn
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[EquipmentMappingUpsert]
	@inventoryId int,
	@businessUnitID int,
	@EngineTypeCategoryID int,  
	@TransmissionTypeCategoryID int, 
	@DriveTrainCategoryID int,
	@FuelCategoryID int,
	@EngineAvailability varchar(50),
	@TransmissionAvailability varchar(50),
	@DriveTrainAvailability varchar(50),
	@FuelAvailability varchar(50),
	@editedBy varchar(50)
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF EXISTS (SELECT 1 FROM builder.EquipmentMapping WHERE businessUnitID = @businessUnitID AND inventoryID = @inventoryID)
	BEGIN
		declare @updatedOn datetime
		select @updatedOn = getdate()
		UPDATE builder.EquipmentMapping
		SET EngineTypeCategoryID = @EngineTypeCategoryID, TransmissionTypeCategoryID = @TransmissionTypeCategoryID, 
		DriveTrainCategoryID = @DriveTrainCategoryID,  FuelCategoryID = @FuelCategoryID, EngineAvailability = @EngineAvailability,
		TransmissionAvailability = @TransmissionAvailability, DriveTrainAvailability = @DriveTrainAvailability, 
		FuelAvailability = @FuelAvailability,
		updatedBy = @editedBy, updatedOn = @updatedOn
		WHERE
			businessUnitID = @businessUnitID 
			AND inventoryID = @inventoryID
	END
	ELSE
	BEGIN
		declare @createdOn datetime
		select @createdOn = getdate()
	INSERT INTO builder.EquipmentMapping
	(inventoryID, businessUnitID, EngineTypeCategoryID, TransmissionTypeCategoryID, DriveTrainCategoryID,
	 FuelCategoryID, EngineAvailability, TransmissionAvailability, DriveTrainAvailability, FuelAvailability, createdBy, createdOn)
	VALUES
	(@inventoryID,  @businessUnitID, @EngineTypeCategoryID, @TransmissionTypeCategoryID, @DriveTrainCategoryID,
     @FuelCategoryID, @EngineAvailability, @TransmissionAvailability, @DriveTrainAvailability, @FuelAvailability, @editedBy, @createdOn)
	END
			
	--select SCOPE_IDENTITY()
END
GO

GRANT EXECUTE ON [builder].[EquipmentMappingUpsert] TO MerchandisingUser 
GO







 