if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[Advertisement#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[Advertisement#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [builder].[Advertisement#Fetch]
	
	@BusinessUnitId int,
	@InventoryId int
	
AS
BEGIN
	SELECT * 
	FROM builder.Advertisement ba
	WHERE ba.businessUnitId = @BusinessUnitId AND ba.inventoryId = @InventoryId
			
END
GO

GRANT EXECUTE ON [builder].[Advertisement#Fetch] TO MerchandisingUser 
GO