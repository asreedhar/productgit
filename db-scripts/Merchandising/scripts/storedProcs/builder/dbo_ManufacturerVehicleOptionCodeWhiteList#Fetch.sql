IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[ManufacturerVehicleOptionCodeWhiteList#Fetch]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ManufacturerVehicleOptionCodeWhiteList#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ManufacturerVehicleOptionCodeWhiteList#Fetch]
	@Manufacturer varchar(100),
	@Make varchar(100) = null
	
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @ManufacturerID int, @MakeID int
	SET @ManufacturerID = (SELECT TOP 1 ManufacturerId FROM VehicleCatalog.Chrome.Manufacturers WHERE ManufacturerName like @Manufacturer)
	IF @Make IS NOT NULL 
		SET @MakeID = (SELECT TOP 1 DivisionId FROM VehicleCatalog.Chrome.Divisions WHERE DivisionName like @Make) 
	
	SELECT OptionCode 
	  FROM [dbo].[ManufacturerVehicleOptionCodeWhiteList] 
	 WHERE VehicleManufacturerID = @ManufacturerID 
		AND (@Make IS NULL OR VehicleMakeID = @MakeID)

END
GO

GRANT EXECUTE ON [dbo].[ManufacturerVehicleOptionCodeWhiteList#Fetch] TO MerchandisingUser 
GO
