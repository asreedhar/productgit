USE [Merchandising]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[AutoloadStatus#FetchAllWithNoStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[AutoloadStatus#FetchAllWithNoStatus]
GO
/****** Object:  StoredProcedure [builder].[AutoloadStatus#FetchAllWithErrorsForBU]    Script Date: 12/28/2011 11:42:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [builder].[AutoloadStatus#FetchAllWithNoStatus]
	
	@BusinessUnitID int
	
AS
BEGIN

SELECT Inventory.InventoryID
	from workflow.Inventory
    INNER JOIN VehicleCatalog.Chrome.Divisions Divisions ON Inventory.Make = Divisions.DivisionName
									                        AND Divisions.CountryCode = 1
    INNER JOIN merchandising.VehicleAutoLoadSettings VehicleAutoLoadSettings ON Divisions.ManufacturerID = VehicleAutoLoadSettings.ManufacturerID
    INNER JOIN settings.Dealer_SiteCredentials Dealer_SiteCredentials ON VehicleAutoLoadSettings.siteId = Dealer_SiteCredentials.siteId
													                        AND Inventory.BusinessUnitID = Dealer_SiteCredentials.businessUnitId
	WHERE NOT EXISTS (SELECT *
						FROM builder.AutoloadStatus
						WHERE inventory.BusinessUnitID = AutoloadStatus.businessUnitID
						AND inventory.InventoryID = AutoloadStatus.InventoryID)
	AND Dealer_SiteCredentials.isLockedOut = 0
	AND Inventory.businessUnitId = @BusinessUnitID 						
			
END
GO
GRANT EXECUTE ON [builder].[AutoloadStatus#FetchAllWithNoStatus]TO MerchandisingUser 
GO


