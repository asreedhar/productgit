
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[builder].[GetVehicleOfflineStatus]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE builder.GetVehicleOfflineStatus

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE builder.GetVehicleOfflineStatus
    @BusinessUnitId INT ,
    @Vin VARCHAR (17) ,
    @IsOffline BIT OUT
AS 
    BEGIN

        SET NOCOUNT ON ;
            
		SELECT  @IsOffline = ISNULL(oc.doNotPostFlag, 0)
		FROM    FLDW.dbo.[InventoryActive] ia
				JOIN Merchandising.builder.OptionsConfiguration oc ON oc.businessUnitID = ia.BusinessUnitID
																   AND oc.inventoryId = ia.InventoryID
		WHERE   ia.BusinessUnitID = @BusinessUnitId
				AND ia.VIN = @Vin
				AND oc.doNotPostFlag = 1
    END

GO

GRANT EXECUTE ON builder.GetVehicleOfflineStatus TO MerchandisingUser 
GO