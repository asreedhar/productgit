if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[DescriptionSampleTextItem#Merge]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[DescriptionSampleTextItem#Merge]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [builder].[DescriptionSampleTextItem#Merge]
	
	@BusinessUnitId int,
	@InventoryId int,
	@sampleTextItem [builder].[sampleTextItem] READONLY	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- create a table to match the target table (didn't want to be able to pass in mismatched bu and inv)
	DECLARE @source TABLE
	(
		businessUnitId int,
		inventoryId int,
		blurbTextId int,
		textSequence int
	)
	
	INSERT INTO @source
		(businessUnitId, inventoryId, blurbTextId, textSequence)
		SELECT @BusinessUnitId, @InventoryId, blurbTextId, textSequence
			FROM @sampleTextItem;
	
	
	
	WITH currentSampleTextItem
          AS (SELECT businessUnitId, inventoryID, blurbTextID, textSequence
              FROM   builder.Description_SampleTextItems
              WHERE businessUnitID = @businessUnitId
              AND   inventoryId = @inventoryId
            )
	MERGE currentSampleTextItem AS target
	USING (
			SELECT businessUnitId, inventoryID, blurbTextID, textSequence
				FROM @source
			) AS source
		ON (
				target.businessUnitId = source.businessUnitId AND 
				target.inventoryId = source.inventoryId AND 
				target.textSequence = source.textSequence 
			)
	WHEN MATCHED AND target.blurbTextID != source.blurbTextID THEN 
		UPDATE SET target.blurbTextId = source.blurbTextId
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (businessUnitID, inventoryID, blurbTextId, textSequence)
		VALUES(source.businessUnitID, source.inventoryID, source.blurbTextId, source.textSequence)
	WHEN NOT MATCHED BY SOURCE THEN
		DELETE
		;
		
END
GO

GRANT EXECUTE ON [builder].[DescriptionSampleTextItem#Merge] TO MerchandisingUser 
GO