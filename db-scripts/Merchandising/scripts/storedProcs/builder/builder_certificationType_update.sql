if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[CertificationType#Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[CertificationType#Update]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_certificationType_update.sql,v 1.1 2008/12/09 18:24:16 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch the business unit configured certification item for 
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - integer id of businessunit
 * @InventoryId - integer id of inventory item
 * @CertificationTypeId - integer id of the certification type to fetch
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[CertificationType#Update]
	
	@BusinessUnitId int,
	@InventoryId int,
	@CertificationTypeId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--determine what template they are using for photo items (own or default from FL)
	UPDATE builder.optionsConfiguration
		SET certificationTypeId = @CertificationTypeId
		WHERE businessUnitId = @BusinessUnitID
		AND inventoryId = @InventoryId
		
END
GO

GRANT EXECUTE ON [builder].[CertificationType#Update] TO MerchandisingUser 
GO