if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[AutoloadStatus#FetchOpenRequeue]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[AutoloadStatus#FetchOpenRequeue]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [builder].[AutoloadStatus#FetchOpenRequeue]
	@BusinessUnitId int = null
AS
BEGIN

	SET NOCOUNT ON
	
		-- pretty short and sweet ..
		SELECT DISTINCT Inventory.BusinessUnitID, Inventory.InventoryID, autoloadEndTime
			FROM workflow.Inventory
			INNER JOIN VehicleCatalog.Chrome.Divisions Divisions ON Inventory.Make = Divisions.DivisionName
																	AND Divisions.CountryCode = 1
			INNER JOIN merchandising.VehicleAutoLoadSettings VehicleAutoLoadSettings ON Divisions.ManufacturerID = VehicleAutoLoadSettings.ManufacturerID
			INNER JOIN settings.CredentialedSites ON VehicleAutoLoadSettings.siteId = CredentialedSites.siteId
			WHERE CredentialedSites.canRequestData = 1
			AND CredentialedSites.DealerCredentialsRequired = 0
			AND (@BusinessUnitId IS NULL OR Inventory.BusinessUnitID = @BusinessUnitId)
			AND NOT EXISTS (SELECT *
								FROM builder.AutoloadStatus AutoloadStatus
								WHERE inventory.BusinessUnitID = AutoloadStatus.businessUnitID
								AND  inventory.InventoryID = AutoloadStatus.InventoryID
								AND autoloadStatusTypeId = 100)
			AND NOT EXISTS (SELECT *
								FROM builder.AutoloadStatus AutoloadStatus
								INNER JOIN builder.AutoloadStatusTypes AutoloadStatusTypes ON AutoloadStatus.statusTypeID = AutoloadStatusTypes.statusTypeID
								WHERE inventory.BusinessUnitID = AutoloadStatus.businessUnitID
								AND inventory.InventoryID = AutoloadStatus.InventoryID
								AND AutoloadStatusTypes.requeueMinutes IS NOT NULL
								AND AutoloadStatus.startTime > DATEADD(MINUTE, - AutoloadStatusTypes.requeueMinutes, GETDATE())
								)
			ORDER BY autoloadEndTime
	
	
	
	SET NOCOUNT OFF



END 
GO

GRANT EXECUTE ON [builder].[AutoloadStatus#FetchOpenRequeue] TO MerchandisingUser 
GO
