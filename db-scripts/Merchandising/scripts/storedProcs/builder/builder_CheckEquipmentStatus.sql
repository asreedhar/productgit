if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[CheckEquipmentStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[CheckEquipmentStatus]
GO

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go












-- =============================================
-- Author:		<Fitzpatrick, Benin>
-- Create date: <2/2/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_CheckEquipmentStatus.sql,v 1.1 2010/03/09 23:03:51 bfitzpatrick Exp $
 * 
 * Summary finally
 * -------
 * 
 * Update or insert any given set of Equipment to a vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @inventoryID
 * @businessUnitID - defaults to 
 * @EngineTypeCategoryID - 
 * @TransmissionTypeCategoryID - 
 * @DriveTrainCategoryID - 
 * @FuelCategoryID -
 * @createdBy	
 * @createdOn
 * @updatedBy
 * @updatedOn
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[CheckEquipmentStatus]
	@ChromeStyleID int,
	@engineID int,
	@transID int,
	@drivetrainID int,
	@fuelID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN
		SELECT StyleAvailability
		FROM VehicleCatalog.Chrome.StyleGenericEquipment
		WHERE ChromeStyleID = @ChromeStyleID
		AND CategoryID = @engineID 
		AND VehicleCatalog.Chrome.StyleGenericEquipment.CountryCode = 1

	
		SELECT StyleAvailability 
		FROM VehicleCatalog.Chrome.StyleGenericEquipment
		WHERE ChromeStyleID = @ChromeStyleID
		AND CategoryID = @transID
		AND VehicleCatalog.Chrome.StyleGenericEquipment.CountryCode = 1

	
		SELECT StyleAvailability 
		FROM VehicleCatalog.Chrome.StyleGenericEquipment
		WHERE ChromeStyleID = @ChromeStyleID
		AND CategoryID = @drivetrainID
		AND VehicleCatalog.Chrome.StyleGenericEquipment.CountryCode = 1

		SELECT StyleAvailability 
		FROM VehicleCatalog.Chrome.StyleGenericEquipment
		WHERE ChromeStyleID = @ChromeStyleID
		AND CategoryID = @fuelID
		AND VehicleCatalog.Chrome.StyleGenericEquipment.CountryCode = 1

	END
			
	--select SCOPE_IDENTITY()
END
GO

GRANT EXECUTE ON [builder].[CheckEquipmentStatus] TO MerchandisingUser 
GO










