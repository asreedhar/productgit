IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[builder].[BuildRequestLog#Insert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [builder].[BuildRequestLog#Insert]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE builder.BuildRequestLog#Insert
	@businessUnitId	int ,
	@vin varchar(17) ,
	@statusTypeId int ,
	@siteId	int ,
	@credentialBusinessUnit int = null,
	@requestedBy varchar(30) = null,
	@BuildRequestLogId int = null OUTPUT
AS
BEGIN

	SET NOCOUNT ON;
	
	DECLARE @requestedOn smalldatetime
	
	SET @requestedOn = GETDATE()
	
	INSERT INTO builder.BuildRequestLog
		(
			businessUnitId,
			vin,
			statusTypeId,
			siteId,
			credentialBusinessUnit,
			requestedOn,
			requestedBy
		)
		VALUES
		(
			@businessUnitId,
			@vin,
			@statusTypeId,
			@siteId,
			@credentialBusinessUnit,
			@requestedOn,
			@requestedBy
		)
		
	SET @BuildRequestLogId = SCOPE_IDENTITY()

	/* one day	
	-- set lastAccess on date on dealer_networkCredentials if date is not from cache
	if(@fromCachedData = 0 AND @credentialBusinessUnit IS NOT NULL)
		UPDATE settings.Dealer_SiteCredentials
			SET lastAttempt = @requestedOn
			WHERE businessUnitId = @credentialBusinessUnit
			AND siteID = @siteId
	*/
		
	END		
GO

GRANT EXECUTE ON [builder].[BuildRequestLog#Insert] TO MerchandisingUser 
GO
