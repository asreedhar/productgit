if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[EquipmentDescriptionOverrides#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[EquipmentDescriptionOverrides#Fetch]
GO

/****** Object:  StoredProcedure [builder].[EquipmentDescriptionOverrides#Fetch]    Script Date: 11/07/2008 08:50:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_equipmentDescriptionOverrides_fetch.sql,v 1.1 2010/01/28 23:12:44 jkelley Exp $
 * FB: 27316 - filter by @businessID, tureen 11/7/2013
 * 
 * 
 * Summary
 * -------
 * 
 * gets the equipment overrides for a particular vehicle and dealer
 * 
 * 
 * Parameters
 * ----------
 * @BusinessUnitId int - businessUnit requesting their overrides
 * @ChromeStyleId     - style id of vehicle for overrides
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */


CREATE PROCEDURE [builder].[EquipmentDescriptionOverrides#Fetch]
	-- Add the parameters for the stored procedure here
    @BusinessUnitId INT,
    @ChromeStyleID INT
AS 

-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
	
SET NOCOUNT ON ;

SELECT  opt.Sequence,
        opt.OptionCode,
        opt.CategoryList,
        ded.equipmentTitle AS description,
        ded.equipmentDescription AS ExtDescription,
        CAST(1 AS BIT) AS isCustomized,
        ohs.Header AS header,
        opt.TypeFilter,
        pr.Invoice,
        pr.MSRP,
        pr.PriceState,
        pr.Condition,
        oks.OptionKindID,
        oks.OptionKind
FROM    vehiclecatalog.chrome.options opt
        INNER JOIN VehicleCatalog.Chrome.OptHeaders AS ohs ON ohs.HeaderID = opt.HeaderID
        INNER JOIN VehicleCatalog.Chrome.OptKinds AS oks ON oks.OptionKindID = opt.OptionKindID
        INNER JOIN VehicleCatalog.Chrome.Prices AS pr ON pr.OptionCode = opt.OptionCode
                                                         AND pr.StyleID = opt.StyleID
        INNER JOIN ( SELECT DISTINCT
							--chromeStyleId,
                            equipmentTitle,
                            equipmentDescription,
                            optionCode
                     FROM   builder.Vehicle_DetailedEquipmentDescriptions vded
                            INNER JOIN builder.DetailedEquipmentDescriptions ded ON vded.equipmentDescriptionId = ded.equipmentDescriptionId
                     WHERE  vded.businessUnitId = @BusinessUnitId -- limit to business unit - FB:27316 - tureen 11/7/2013
						AND (
							vded.chromeStyleId = @ChromeStyleId
                            OR ( vded.chromeStyleId IS NULL
                                 AND vded.modelId = ( SELECT    ModelID
                                                      FROM      vehicleCatalog.chrome.styles
                                                      WHERE     CountryCode = 1
														AND styleId = @ChromeStyleId
                                                    )
                               )
                            )
                   ) AS ded ON ded.optionCode = opt.optionCode
WHERE   ( opt.CountryCode = 1 )
        AND ( ohs.CountryCode = 1 )
        AND ( oks.CountryCode = 1 )
        AND ( pr.CountryCode = 1 )
        AND ( opt.styleId = @ChromeStyleId )
        AND Availability IN ( 'R', ' ', 'F' )
ORDER BY ohs.header,
        pr.styleID DESC,
        pr.msrp DESC
        
        
RETURN

GO

GRANT EXECUTE ON [builder].[EquipmentDescriptionOverrides#Fetch] TO MerchandisingUser 
GO

