if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleConditions#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[VehicleConditions#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_vehicleConditions_fetch.sql,v 1.1 2008/12/11 19:02:32 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch the business unit configured certification item for 
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - integer id of businessunit
 * @InventoryId - integer id of inventory item
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[VehicleConditions#Fetch]
	
	@BusinessUnitId int,
	@InventoryId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select vc.*, vct.title FROM builder.vehicleConditions vc
	INNER JOIN builder.vehicleConditionTypes vct
		ON vct.vehicleConditionTypeId = vc.vehicleConditionTypeId
	where businessUnitId = @BusinessUnitID
	AND inventoryId = @InventoryId
END
GO

GRANT EXECUTE ON [builder].[VehicleConditions#Fetch] TO MerchandisingUser 
GO