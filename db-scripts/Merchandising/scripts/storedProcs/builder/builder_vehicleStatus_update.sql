if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleStatus#Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[VehicleStatus#Update]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_vehicleStatus_update.sql,v 1.3 2009/04/16 15:02:09 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * update the status flags for a build step for a given vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId- vehicle inventory id
 * @MemberLogin - firstlook login of member who is updating the status
 * @StatusTypeId - status type of the update
 * @StatusLevel - level to indicate the status
 * @Version - timestamp for matching record concurrency
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[VehicleStatus#Update]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@InventoryId int,
	@MemberLogin varchar(30),
	@StatusTypeId int,
	@StatusLevel int,
	@Version timestamp = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF NOT EXISTS (SELECT 1 
		FROM builder.VehicleStatus
		WHERE 
		businessUnitId = @BusinessUnitId
		AND inventoryId = @InventoryId
		AND statusTypeId = @StatusTypeId
		AND (@Version IS NULL OR version = @Version))
		BEGIN
			declare @usingMember int
			select @usingMember = lastUpdatedBy
				FROM builder.VehicleStatus
				WHERE 
				businessUnitId = @BusinessUnitId
				AND inventoryId = @InventoryId
				AND statusTypeId = @StatusTypeId
			IF @usingMember <> NULL
				BEGIN
					RAISERROR('The status record is in use by %s',16,1, @usingMember)
				END
			ELSE
			BEGIN
				RAISERROR('The status record does not exist and may have been deleted',16,1)
			END
			RETURN @@ERROR
		END
	
	UPDATE builder.VehicleStatus
	SET statusLevel = @StatusLevel,
		lastUpdatedBy = @MemberLogin,
		lastUpdatedOn = getdate()
	WHERE 
		businessUnitId = @BusinessUnitId
		AND inventoryId = @InventoryId
		AND statusTypeId = @StatusTypeId

END
GO

GRANT EXECUTE ON [builder].[VehicleStatus#Update] TO MerchandisingUser 
GO