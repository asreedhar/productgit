if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[CertificationTypesManaged#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[CertificationTypesManaged#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_certificationTypesManaged_fetch.sql,v 1.1 2009/01/19 19:51:23 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetches all certification types for the cpo management group
 * if no businessUnit is specified, all certification types in the database are returned
 * 
 * 
 * Parameters
 * ----------
 * @CpoGroupId
 * 
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[CertificationTypesManaged#Fetch]
	@CpoGroupId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	
		SELECT DISTINCT mc.certificationTypeId, mc.[name]
		FROM builder.manufacturerCertifications mc
		INNER JOIN settings.cpoCertifieds ccs
			ON ccs.certificationTypeId = mc.certificationTypeId
		WHERE ccs.cpoGroupId = @CpoGroupId -- it is this cpo group
	
END
GO

GRANT EXECUTE ON [builder].[CertificationTypesManaged#Fetch] TO MerchandisingUser 
GO