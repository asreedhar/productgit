if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[LoaderNote#Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[LoaderNote#Update]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_loaderNote_update.sql,v 1.1 2009/01/05 21:23:51 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * update a loader note
 * 
 * 
 * Parameters
 * ----------
 * 
 * @NoteId     - integer id of the businesUnit for which to get the alert list
 * @MemberLogin - firstlook login of member who is updating the status
 * @NoteText - text of note
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[LoaderNote#Update]
	-- Add the parameters for the stored procedure here
	@NoteId int,
	@MemberLogin varchar(30),
	@NoteText varchar(500)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	UPDATE builder.LoaderNotes
	SET noteText = @NoteText,
		writtenBy = @MemberLogin,
		writtenOn = getdate()
	WHERE 
		noteId = @NoteID
		

END
GO

GRANT EXECUTE ON [builder].[LoaderNote#Update] TO MerchandisingUser 
GO