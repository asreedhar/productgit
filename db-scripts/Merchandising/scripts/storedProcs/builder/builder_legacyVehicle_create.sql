if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[LegacyVehicle#Create]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[LegacyVehicle#Create]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_legacyVehicle_create.sql,v 1.1 2009/03/26 21:14:06 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the current configuration of a vehicle by business unit and stock number
 * this config has detailed options text for option drill down functionality
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId - integer inventory id
 * @StockNumber
 * @Description
 * @PhotoUrls
 * @OptionsList
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[LegacyVehicle#Create]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@InventoryId int,
	@StockNumber varchar(15),
	@Description varchar(5000),
	@PhotoUrls varchar(8000),
	@OptionsList varchar(5000),
	@NeedsAction bit
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	IF NOT EXISTS (SELECT 1 FROM builder.OptionsConfiguration 
				WHERE BusinessUnitID = @BusinessUnitID 
				AND inventoryId = @InventoryId
			)
	BEGIN
		
		IF @NeedsAction = 1
			SELECT @NeedsAction = 0
		ELSE
			SELECT @NeedsAction = 1
			
		
			INSERT INTO builder.OptionsConfiguration 
				(BusinessUnitID, 
				inventoryId,
				stockNumber,
				isLegacy,
				onlineFlag,
				createdOn, 
				createdBy, 
				lastUpdatedOn, 
				lastUpdatedBy) 
            VALUES (@BusinessUnitID, 
					@InventoryId, 
					@StockNumber, 
					1,  --IS LEGACY...
					@NeedsAction,					
					getdate(), '', getdate(), '')						
	END
	
	IF NOT EXISTS (SELECT 1 FROM builder.descriptions
					WHERE BusinessUnitID = @BusinessUnitID 
					AND inventoryId = @InventoryId)
	BEGIN
			--------------------------
			--ADD THE DESCRIPTION	
			---------------------------	
			INSERT INTO builder.Descriptions
			(BusinessUnitId,
				inventoryid,
				acceleratorAdvertisementId,
				merchandisingDescription,
				footer,
				expiresOn,
				createdOn,
				createdBy,
				lastUpdatedOn,
				lastUpdatedBy)
			VALUES (@BusinessUnitId,
				@Inventoryid,
				NULL,
				@Description,
				'',
				dateadd(day,1000,getdate()),
				getdate(),
				'',
				getdate(),
				'')
			
	END
	
	exec interface.Advertisement#Release @BusinessUnitId, @InventoryId, @Description, @PhotoUrls, @OptionsList
	
	

END
GO

GRANT EXECUTE ON [builder].[LegacyVehicle#Create] TO MerchandisingUser 
GO