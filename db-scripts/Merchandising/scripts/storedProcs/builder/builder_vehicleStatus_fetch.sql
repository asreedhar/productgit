if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleStatus#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[VehicleStatus#Fetch]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_vehicleStatus_fetch.sql,v 1.2 2008/12/11 04:58:35 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * create a status record flags for a build step for a given vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId- vehicle inventory id
 * @StatusTypeId = vehicle status type id (defaults to null - fetches all)
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[VehicleStatus#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@InventoryId int,
	@StatusTypeId int = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	SELECT 
		ISNULL(inventoryId, @InventoryId) as inventoryId,
		st.statusType, 
		st.statusTypeId,
		statusLevel, --RETURN NULL TO INDICATE LACK OF EXISTENCE 
		
		-- For Hondas, default package status (ext and int equipment) to "complete"
		defaultStatusLevel = 
			case 
				when st.statusTypeId in (1,2) -- Exterior Equipment (steps used for Packages)
				and exists ( -- is a honda (as of writing, this query matches that used in workflow.inventory to determine the make of a vehicle)
					select * 
					from FLDW.dbo.InventoryActive i
					join FLDW.dbo.Vehicle v on i.BusinessUnitID = v.BusinessUnitID and i.VehicleID = v.VehicleID
					join IMT.dbo.MakeModelGrouping mmg on v.MakeModelGroupingID = mmg.MakeModelGroupingID
					join Merchandising.builder.OptionsConfiguration oc on oc.businessUnitID = i.BusinessUnitID and oc.inventoryId = i.InventoryID
					left join VehicleCatalog.chrome.Styles sty on sty.StyleID = oc.Chromestyleid and sty.CountryCode = 1
					left join VehicleCatalog.chrome.Models mdl on mdl.ModelID = sty.ModelID and mdl.CountryCode = 1
					left join VehicleCatalog.chrome.Divisions div on div.DivisionID = mdl.DivisionID and div.CountryCode = 1
					where i.BusinessUnitID = @BusinessUnitID 
					and i.InventoryID = @InventoryId 
					and coalesce(div.DivisionName, mmg.Make) = 'HONDA'
				)
				then 2 -- complete
				else null
			end,		
		
		createdBy, 
		createdOn, 
		lastUpdatedBy, 
		lastUpdatedOn, 
		version
		FROM builder.VehicleStatusTypes st
		LEFT JOIN builder.VehicleStatus vs
			ON vs.statusTypeId = st.statusTypeId
			AND inventoryId = @InventoryId
			AND businessUnitId = @BusinessUnitId
		WHERE 
			(@StatusTypeId IS NULL OR vs.statusTypeId = @StatusTypeId)
		
END
GO

GRANT EXECUTE ON [builder].[VehicleStatus#Fetch] TO MerchandisingUser 
GO