if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[GetLotInfo#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[GetLotInfo#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [builder].[GetLotInfo#Fetch]
	@BusinessUnitId int,
	@InventoryId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF EXISTS (SELECT 1 FROM Merchandising.settings.Merchandising 
				WHERE businessUnitId = @BusinessUnitId)
	BEGIN
				
		SELECT lot_vehicle.ChromeStyleId, inventory_active.vin, lot_vehicle.optioncodes, lot_color.Color
		FROM FLDW.dbo.InventoryActive inventory_active
		JOIN Lot.Listing.Vehicle lot_vehicle ON lot_vehicle.vin = inventory_active.vin
		JOIN Lot.Listing.Color lot_color on lot_color.ColorID = lot_vehicle.ColorID
		JOIN Merchandising.settings.merchandising settings ON settings.BusinessUnitId = @BusinessUnitId
		WHERE inventory_active.BusinessUnitID = @BusinessUnitId 
			  and inventory_active.InventoryID = @InventoryId
			  and settings.lotProviderID = lot_vehicle.ProviderID

	END
	ELSE
	BEGIN
				
		SELECT lot_vehicle.ChromeStyleId, inventory_active.vin, lot_vehicle.optioncodes, lot_color.Color
		FROM FLDW.dbo.InventoryActive inventory_active
		JOIN Lot.Listing.Vehicle lot_vehicle ON lot_vehicle.vin = inventory_active.vin
		JOIN Lot.Listing.Color lot_color on lot_color.ColorID = lot_vehicle.ColorID
		where inventory_active.BusinessUnitID = @BusinessUnitId 
			  and inventory_active.InventoryID = @InventoryId

	END
			
END
GO

GRANT EXECUTE ON [builder].[GetLotInfo#Fetch] TO MerchandisingUser 
GO