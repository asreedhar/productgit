if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[Inventory#Count]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[Inventory#Count]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Sombke, Bryant>
-- Create date: <02/19/10>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_Inventory_Count.sql,v 1.2 2010/03/10 00:26:57 bsombke Exp $
 * 
 * Summary
 * -------
 * 
 * gets all inventory items that meet the input var filter criteria
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 *	@StockNumber varchar(17) = '', - defaults to returning a list of stock items...
 *  @Vin varchar(17) = '' - defaults to returning all vins -- suggest only using one of stock/vin
 *	@merchandisingStatus int = 240 - defaults to all merchandising statuses valid values are - 16 - not posted, 32 - pending approval, 64 - online needs attention, 128 - online
 *	@inventoryType int = 0, - defaults to all inventory.  valid vals are 0 - all, 1 - new, 2 - used
 *	@activityFilter int = 7 - defaults to all activities.  valid vals are 1 - needs pricing, 2 - needs description attention, 4 - needs photos
 *  @searchParameter varchar(20) - this can be a text segment representing a stocknumber, make, or model
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[Inventory#Count]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@StockNumber varchar(17) = '',
	@Vin varchar(17) = '',
	@merchandisingStatus int = 240,
	@inventoryType int = 2,
	@activityFilter int = 7,
	@overdueFlag bit = 0,
	@sortExpression varchar(30) = 'inventoryReceivedDate ASC',
	@startRowIndex int = 0,
	@maximumRows int = 1000,
	@InventoryObjective int = NULL,
	@debug bit = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @statusCodeBuid int
	IF EXISTS (SELECT 1 FROM settings.InventoryObjectiveMapping WHERE businessUnitId = @BusinessUnitId)
		set @statusCodeBuid = @BusinessUnitId
	ELSE
		set @statusCodeBuid = 100150

	declare @prefsBuid int
	IF EXISTS (SELECT 1 FROM settings.alertingPreferences WHERE businessUnitId = @BusinessUnitId)
		select @prefsBuid = @BusinessUnitID
	ELSE
		SELECT @prefsBuid = 100150				
				
	if @debug = 1 print '1)load inventory: ' + convert(varchar,getdate(),121)

	CREATE TABLE #Inventory (
		BusinessUnitId int NOT NULL,
		Vin varchar(17) NOT NULL,
		StockNumber varchar(15) NOT NULL,
		InventoryReceivedDate datetime NOT NULL, 
		vehicleYear int NOT NULL, 
		UnitCost decimal(9,2) NOT NULL, 
		AcquisitionPrice decimal (8,2) NOT NULL, 
		Certified tinyint NOT NULL, 
		Make varchar(20) NOT NULL, 
		model varchar(50) NOT NULL,
		VehicleTrim varchar(50) NULL,
		inventoryID int NOT NULL,
		MileageReceived int NULL, 
		TradeOrPurchase tinyint NOT NULL, 
		ListPrice decimal(8,2) NULL,
		InventoryType tinyint NOT NULL,
		InventoryStatusCD int NOT NULL,
		BaseColor varchar(50) NOT NULL,
		VehicleLocation varchar(20) NULL,
		LotPrice DECIMAL(8,2) NULL,
		MSRP DECIMAL(9,2) NULL,
		VehicleCatalogId int NULL
		,PRIMARY KEY (businessUnitId, inventoryId)
	)

	INSERT INTO #Inventory EXEC Interface.InventoryList#Fetch @BusinessUnitId=@BusinessUnitID, @UsedOrNew=@inventoryType

	if @debug = 1 print 'load thumbnails: ' + convert(varchar,getdate(),121)
	
	CREATE TABLE #Thumbnails (
		imageThumbnailUrl varchar(200),
		inventoryId int,
		businessUnitId int,
		photoCount int
	)
	INSERT INTO #Thumbnails EXEC builder.getThumbnails @BusinessUnitID

	if @debug = 1 print 'get veh status: ' + convert(varchar,getdate(),121)
	
	CREATE TABLE #VehicleDataAndStatus (
		businessUnitID int NOT NULL
		,inventoryID int NOT NULL
		,isLegacy bit NOT NULL
		,chromeStyleID int NOT NULL
		,lotLocationID int NULL
		,afterMarketTrim varchar(50) NOT NULL
		,[specialId] int NULL
		,[doNotPostFlag] bit NOT NULL
		,[onlineFlag] smallint NOT NULL
		,[lotDataImportLock] bit NOT NULL
		,DescriptionSample varchar(180) NOT NULL
		,lastUpdateListPrice decimal(8,2) NULL
		,datePosted datetime NULL
		,exteriorStatus INT NOT NULL
		,interiorStatus INT NOT NULL
		,photoStatus INT NOT NULL 
		,adReviewStatus INT NOT NULL
		,pricingStatus INT NOT NULL 
		,postingStatus INT NOT NULL
		,daysPending INT NULL
		,stepsComplete INT NOT NULL
		,lowActivityFlag BIT NOT NULL
		,statusBucket INT NOT NULL

		,descriptionAlert BIT NOT NULL
		,photosAlert BIT NOT NULL
		,pricingAlert BIT NOT NULL
		,remerchandisingFlag BIT NOT NULL
		,NoPackages BIT NOT NULL
	)

	EXEC builder.VehicleData#Load @BusinessUnitId

if @debug = 1 print 'prep sort exp: ' + convert(varchar,getdate(),121)

	
select @sortExpression = case
WHEN @sortExpression like 'stockNumber%' OR 
	 @sortExpression like 'inventoryId%'
THEN 'inv.' + @sortExpression

ELSE @sortExpression
END

	if @debug = 1 print 'prep dyn select: ' + convert(varchar,getdate(),121)

declare @sql nvarchar(4000)
set @sql = '
SELECT count(*) FROM (
SELECT ROW_NUMBER() OVER(ORDER BY ' + @sortExpression + ') as RowNum,
Vin, 
inv.inventoryID,
inv.StockNumber,
statusBucket,
remerchandisingFlag,
pricingAlert,
descriptionAlert,
photosAlert,
InventoryReceivedDate, 
vehicleYear, 
UnitCost, 
AcquisitionPrice, 
Certified, 
ISNULL(div.DivisionName, inv.Make) as make, 
ISNULL(sty.CFModelName,inv.model) as model, 
COALESCE(sty.trim, inv.VehicleTrim, '''') as trim, 
ISNULL(mkt.marketClass, '''') as marketClass,
ISNULL(mkt.marketClassId, 0) as marketClassId,
ISNULL(mkt.segmentId, 1) as segmentId,
ISNULL(vp.imageThumbnailUrl, '''') as thumbnailUrl, 
ISNULL(vp.photoCount, 0) as photoCount,
ISNULL(lotLocationId,0) as lotLocationId,
MileageReceived, 
TradeOrPurchase, 
ListPrice, 
BaseColor,
inv.VehicleLocation,
InventoryStatusCD,
oc.DescriptionSample,
oc.datePosted,
NULL as chromeStyleId,
ISNULL(lowActivityFlag,0) as lowActivityFlag,
ISNULL(oc.exteriorStatus, 0) as exteriorStatus, 
ISNULL(oc.interiorStatus, 0) as interiorStatus,
ISNULL(oc.photoStatus, 0) as photoStatus, 
ISNULL(oc.adReviewStatus, 0) as adReviewStatus,
ISNULL(oc.pricingStatus, 0) as pricingStatus, 
ISNULL(oc.postingStatus, 0) as postingStatus,
ISNULL(oc.onlineFlag, 0) as onlineFlag,
ISNULL(oc.stepsComplete,0) as stepsComplete,
inv.InventoryType
 
    FROM #Inventory inv
    LEFT JOIN #VehicleDataAndStatus oc ON oc.inventoryId = inv.inventoryId AND oc.businessUnitID = inv.businessUnitID
    LEFT JOIN #Thumbnails vp ON inv.businessUnitID = vp.businessUnitID AND inv.inventoryId = vp.inventoryId
    LEFT JOIN vehicleCatalog.chrome.styles sty ON sty.styleID = oc.chromestyleid and sty.countrycode = 1                    
	LEFT JOIN vehicleCatalog.chrome.models mdl ON mdl.modelID = sty.modelID and mdl.countrycode = 1
	LEFT JOIN vehicleCatalog.FirstLook.MarketClass mkt ON mkt.marketClassID = sty.mktClassID
	LEFT JOIN vehicleCatalog.chrome.Divisions div ON div.divisionID = mdl.divisionID and div.countrycode = 1
	LEFT JOIN settings.alertingPreferences ap ON ap.businessUnitId = ' + convert(nvarchar(7), @prefsBuid) + '
	
    WHERE '
    + case
		WHEN @InventoryObjective IS NULL THEN ''
		ELSE
			 
			'inventoryStatusCD IN (SELECT inventoryStatusCD 
									FROM settings.InventoryObjectiveMapping
									WHERE businessUnitId = ' + convert(nvarchar, @statusCodeBuid) + ' AND
									objectiveTypeId = ' + convert(nvarchar, @InventoryObjective) + ') AND '
		END
    + case 
		WHEN @StockNumber = '' THEN ''
		ELSE ' inv.StockNumber = ' + @StockNumber + ' AND '
		END
	+ case 
		WHEN @Vin = ''
		THEN ''
		ELSE ' inv.Vin = ' + @Vin + ' AND '
		END
	+ case 
		WHEN @inventoryType = 0
		THEN ''
		ELSE ' inv.inventoryType = ' + CONVERT(nvarchar(2), @inventoryType) + ' AND '
		END
	
	+ case WHEN @merchandisingStatus = 240 THEN ''  --all vehicles
			WHEN @merchandisingStatus = 16 THEN		--web load incomplete
			' (statusBucket IS NULL OR statusBucket = ' + CONVERT(nvarchar(5), @merchandisingStatus) + ') AND ' 
			WHEN @merchandisingStatus = 64 THEN
			' (statusBucket = 128 AND remerchandisingFlag = 1) AND '
			WHEN @merchandisingStatus = 128 THEN
			' (statusBucket = 128 AND remerchandisingFlag = 0) AND '
			WHEN @merchandisingStatus = 192 THEN
			' statusBucket = 128 AND '
		   ELSE ' statusBucket = ' + CONVERT(nvarchar(5), @merchandisingStatus) + ' AND ' 
		END
	+ CASE 
		WHEN @overdueFlag = 0 THEN ''  --NOT OVERDUE
		WHEN @merchandisingStatus = 32 --OVERDUE AND pending
			THEN ' daysPending > ap.maxDaysToApprovedAfterPending AND '
		WHEN @merchandisingStatus = 16 AND @activityFilter = 1   --OVERDUE AND PRICING INCOMPLETE
			THEN ' datediff(d,inventoryReceivedDate,getdate()) > ap.daysToPricingComplete AND '
		WHEN @merchandisingStatus = 16 AND @activityFilter = 2   --OVERDUE AND EQUIP INCOMPLETE
			THEN ' datediff(d,inventoryReceivedDate,getdate()) > ap.daysToEquipmentComplete AND '
		WHEN @merchandisingStatus = 16 AND @activityFilter = 4   --OVERDUE AND PHOTOS INCOMPLETE
			THEN ' datediff(d,inventoryReceivedDate,getdate()) > ap.daysToPhotosComplete AND '
		WHEN @merchandisingStatus = 64
			THEN 
				CASE @activityFilter
					--pricing alert
					WHEN 1 THEN ' pricingAlert = 1 AND '
					--inadequate description
					WHEN 2 THEN ' descriptionAlert = 1 AND '
					--insufficient photos
					WHEN 4 THEN ' photosAlert = 1 AND '
					--low activity
					ELSE ' lowActivityFlag = 1 AND '
				END
		WHEN @merchandisingStatus = 240
			THEN
				CASE @activityFilter
					WHEN 1 THEN ' (listPrice = 0 OR listPrice IS NULL) AND '
					ELSE ''
				END
			
		ELSE ' datediff(d,inventoryReceivedDate,getdate()) > ap.daysToApproval AND '
		END			
	
	+ CASE WHEN @activityFilter = 7 OR (@merchandisingStatus = 64 AND @overdueFlag > 0)
				THEN ' 1 = 1'
			WHEN @activityFilter = 1 
				THEN ' (oc.pricingStatus IS NULL OR oc.pricingStatus < 2) '
			WHEN @activityFilter = 2 
				THEN ' (oc.interiorStatus IS NULL OR oc.interiorStatus < 2 OR
						oc.exteriorStatus IS NULL OR oc.exteriorStatus < 2) '
			WHEN @activityFilter = 4 
				THEN ' (oc.photoStatus IS NULL OR oc.photoStatus < 2) ' -- TODO: Do we need to check adReviewStatus here too?
		END
	
+ ' ) as vehInfo
WHERE RowNum BETWEEN ' + CONVERT(nvarchar(10), @startRowIndex + 1) + 
			' AND ' + CONVERT(nvarchar(10), (@startRowIndex + @maximumRows))
	if @debug = 1 print 'exec dyn select: ' + convert(varchar,getdate(),121)
EXEC sp_executesql @sql
	
	if @debug = 1 print 'fin: ' + convert(varchar,getdate(),121)
	
	DROP TABLE #Inventory
	DROP TABLE #VehicleDataAndStatus
	DROP TABLE #Thumbnails
END
GO

GRANT EXECUTE ON [builder].[Inventory#Count] TO MerchandisingUser 
GO