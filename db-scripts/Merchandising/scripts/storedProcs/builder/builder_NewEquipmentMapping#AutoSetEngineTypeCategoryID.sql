IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'Builder.NewEquipmentMapping#AutoSetEngineTypeCategoryID')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE Builder.NewEquipmentMapping#AutoSetEngineTypeCategoryID
GO

CREATE PROCEDURE Builder.NewEquipmentMapping#AutoSetEngineTypeCategoryID
					@MemberLogin  varchar(50) = '#nemMaintenance'
AS 
    SET NOCOUNT ON

    BEGIN TRY
    
		-- create table to hold results
		DECLARE @result TABLE
		(
			BusinessUnitID int not null,
			InventoryID int not null, 
			EngineTypeCategoryID  int not null
		)
		
		-- get vehicles that do not have their engine catagory set
		INSERT INTO @result
			(BusinessUnitID, InventoryID, EngineTypeCategoryID)
		SELECT InventoryActive.BusinessUnitID, InventoryActive.InventoryID, EngineTypeCategoryID
			FROM FLDW.dbo.InventoryActive InventoryActive
			INNER JOIN FLDW.dbo.Vehicle vehicle	ON InventoryActive.vehicleid = vehicle.vehicleid
												AND InventoryActive.businessUnitId = vehicle.businessUnitId
			INNER JOIN (SELECT DISTINCT VINPattern_Prefix, EngineTypeCategoryID
								FROM VehicleCatalog.Chrome.VINPattern VINPattern
								WHERE CountryCode = 1
								AND EngineTypeCategoryID != 0) uniqueVinPattern ON uniqueVinPattern.VINPattern_Prefix = SUBSTRING(vehicle.Vin,0,9) + SUBSTRING(vehicle.Vin,10,1)
			WHERE InventoryActive.InventoryActive = 1
			AND EXISTS (SELECT *
                        FROM IMT.dbo.DealerUpgrade
                        WHERE DealerUpgrade.BusinessUnitID = InventoryActive.BusinessUnitID
                        AND DealerUpgrade.DealerUpgradeCD = 24) -- Max Ad
			AND NOT EXISTS (SELECT *
								FROM Merchandising.builder.NewEquipmentMapping
								WHERE InventoryActive.BusinessUnitID = NewEquipmentMapping.businessUnitID
								AND InventoryActive.InventoryID = NewEquipmentMapping.inventoryID
								AND NewEquipmentMapping.EngineTypeCategoryID > 0)
			-- a couple of Porsche's have weird vin patterns with multiply engines, Carrarra GT and 911
			AND NOT EXISTS(SELECT VINPattern_Prefix, COUNT(DISTINCT EngineTypeCategoryID)
								FROM VehicleCatalog.Chrome.VINPattern VINPattern
								WHERE CountryCode = 1
								AND VINPattern_Prefix = SUBSTRING(vehicle.Vin,0,9) + SUBSTRING(vehicle.Vin,10,1)
								GROUP BY VINPattern_Prefix
								HAVING COUNT(DISTINCT VINPattern.EngineTypeCategoryID) > 1)
		
		

		-- insert new rows
		INSERT INTO merchandising.builder.NewEquipmentMapping
			(
				inventoryID, 
				businessUnitID,
				EngineTypeCategoryID,
				TransmissionTypeCategoryID,
				DriveTrainCategoryID,
				FuelCategoryID,
				createdBy,
				createdOn
			)
			SELECT result.InventoryID,
					result.BusinessUnitID,
					result.EngineTypeCategoryID,
					-1,
					-1,
					-1,
					@MemberLogin,
					GETDATE()
				FROM @result result
				WHERE NOT EXISTS (SELECT *
									FROM merchandising.builder.NewEquipmentMapping
									WHERE NewEquipmentMapping.businessUnitID = result.businessUnitID
									AND NewEquipmentMapping.inventoryID = result.inventoryID)
		

		-- update existing rows
		UPDATE NewEquipmentMapping
			SET EngineTypeCategoryID = result.EngineTypeCategoryID,
				updatedBy = @MemberLogin,
				updatedOn = GETDATE()
			FROM merchandising.builder.NewEquipmentMapping
			INNER JOIN @result result ON NewEquipmentMapping.businessUnitID = result.BusinessUnitID
										AND NewEquipmentMapping.inventoryID = result.inventoryID
			WHERE NewEquipmentMapping.EngineTypeCategoryID <= 0 -- only if it has not been set
		
		

    END TRY
    BEGIN CATCH

			DECLARE @ErrorMessage NVARCHAR(4000),
			  @ErrorNumber INT,
			  @ErrorSeverity INT,
			  @ErrorState INT,
			  @ErrorLine INT,
			  @ErrorProcedure NVARCHAR(200) ;

				-- Assign variables to error-handling functions that 
				-- capture information for RAISERROR.
			SELECT @ErrorNumber = ERROR_NUMBER(), @ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE(), @ErrorLine = ERROR_LINE(),
					@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-') ;

				-- Building the message string that will contain original
				-- error information.
			SET @ErrorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' +
					'Message: ' + ERROR_MESSAGE() ;

				-- Raise an error: msg_str parameter of RAISERROR will contain
				-- the original error information.
			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, -- parameter: original error number.
			  @ErrorSeverity, -- parameter: original error severity.
			  @ErrorState, -- parameter: original error state.
			  @ErrorProcedure, -- parameter: original error procedure name.
			  @ErrorLine-- parameter: original error line number.
					)

    END CATCH
    
    SET NOCOUNT ON
GO

GRANT EXECUTE ON Builder.NewEquipmentMapping#AutoSetEngineTypeCategoryID TO MerchandisingUser 
GO