if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[getAdditionalInfoItems]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[getAdditionalInfoItems]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_getAdditionalInfoItems.sql,v 1.4 2009/06/22 17:22:10 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * retrieves the 'additional' information items for a business unit - defaults to FirstLook's
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[getAdditionalInfoItems]
	
	@BusinessUnitID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF EXISTS (SELECT infoId FROM builder.AdditionalInfoCategories WHERE businessUnitID = @BusinessUnitID)
		BEGIN
			SELECT * FROM builder.AdditionalInfoCategories 
				WHERE businessUnitID = @BusinessUnitID
				ORDER BY Priority
		END
	ELSE
		BEGIN
			SELECT * FROM builder.AdditionalInfoCategories
				WHERE businessUnitID = 100150
				ORDER BY Priority
		END
END
GO

GRANT EXECUTE ON [builder].[getAdditionalInfoItems] TO MerchandisingUser 
GO