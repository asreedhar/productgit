if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[CertificationFeature#Upsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[CertificationFeature#Upsert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * Create a certification feature
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId
 * @CertificationFeatureId - defaults to -1 meaning a new record is needed, if a match is found for the id, it is updated
 * @CertificationTypeId - type of certification to update
 * @CertificationFeatureTypeId - Feature type id
 * @Description - feature description
 * @Active bit - flag if feture is active
 * @BeginYear int (nullable)
 * @EndYear int (nullable)
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[CertificationFeature#Upsert]
	@BusinessUnitId int,
	@CertificationFeatureId int = -1,
	@CertificationTypeId int,
	@CertificationFeatureTypeId int,
	@Description varchar(300),
	@Active bit = 1,
	@BeginYear INT = NULL,
	@EndYear INT = NULL 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF EXISTS (SELECT 1 FROM builder.CertificationFeatures WHERE certificationFeatureId = @CertificationFeatureId)
	BEGIN
		UPDATE builder.CertificationFeatures
		SET [description] = @Description, active = @Active, businessUnitId = @BusinessUnitId, MinYear = @BeginYear, MaxYear = @EndYear
		WHERE
			certificationFeatureId = @CertificationFeatureId
	END
	ELSE
	BEGIN
	INSERT INTO builder.CertificationFeatures
	(businessUnitId, certificationTypeId, certificationFeatureTypeId, [description], MinYear, MaxYear)
	VALUES
	(@BusinessUnitId, @CertificationTypeId, @CertificationFeatureTypeId, @Description, @BeginYear, @EndYear)
	END
			
	select SCOPE_IDENTITY()
END
GO

GRANT EXECUTE ON [builder].[CertificationFeature#Upsert] TO MerchandisingUser 
GO