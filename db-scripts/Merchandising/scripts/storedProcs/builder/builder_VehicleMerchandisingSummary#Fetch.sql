if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleMerchandisingSummary#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [builder].[VehicleMerchandisingSummary#Fetch]
GO

CREATE PROCEDURE builder.VehicleMerchandisingSummary#Fetch
	@OwnerHandle varchar(36),
	@VehicleHandle varchar(36)
AS

	SET NOCOUNT ON
	
	BEGIN TRY
	
		-- convert the owner id and vehicle id to business unit and inventory id
		DECLARE	@OwnerEntityTypeID INT, @OwnerEntityID INT, @OwnerID INT, @VehicleEntityTypeID INT, @VehicleEntityID INT

		EXEC [Market].[Pricing].[ValidateParameter_OwnerHandle] @OwnerHandle, @OwnerEntityTypeID out, @OwnerEntityID out, @OwnerID out
		EXEC [Market].[Pricing].[ValidateParameter_VehicleHandle] @VehicleHandle, @VehicleEntityTypeID out, @VehicleEntityID out

		-- table for results
		DECLARE @result TABLE
		(
			BusinessUnitID int not null,
			InventoryID int not null,
			Engine varchar(100) null,
			Transmission varchar(100) null,
			DriveTrain varchar(100) null,
			BaseColor varchar(100) null,
			GenericExtColor varchar(100) null,
			IntColor varchar(100) null,
			MileageReceived	int null,
			InventoryType tinyint null
		)
		
		INSERT INTO @result
			(BusinessUnitID, InventoryID)
			VALUES(@OwnerEntityID, @VehicleEntityID)
			
		-- set color and milage
		UPDATE @result
			SET BaseColor = Inventory.BaseColor,
				IntColor = Inventory.IntColor,
				MileageReceived = Inventory.MileageReceived,
				InventoryType = Inventory.InventoryType
			FROM @result result
			INNER JOIN workflow.Inventory Inventory ON result.BusinessUnitID = Inventory.BusinessUnitID
													AND  result.InventoryID = Inventory.InventoryID
													
		if @@ROWCOUNT != 1
			RAISERROR('Cannot find inventory item for businessUnitID: %d inventory id: %d', 16, 1, @OwnerEntityID, @VehicleEntityID)
			
		-- the following might now exists for every vehicle

		-- set engine
		UPDATE @result
			SET Engine = Categories.UserFriendlyName
			FROM @result result
			INNER JOIN builder.NewEquipmentMapping NewEquipmentMapping ON result.BusinessUnitID = NewEquipmentMapping.businessUnitID
																		AND result.InventoryID = NewEquipmentMapping.InventoryID
			INNER JOIN  VehicleCatalog.Chrome.Categories Categories ON 	NewEquipmentMapping.EngineTypeCategoryID = Categories.CategoryID
																		AND Categories.CountryCode = 1
			
		-- set tranny
		UPDATE @result
			SET Transmission = Categories.UserFriendlyName
			FROM @result result
			INNER JOIN builder.NewEquipmentMapping NewEquipmentMapping ON result.BusinessUnitID = NewEquipmentMapping.businessUnitID
																		AND result.InventoryID = NewEquipmentMapping.InventoryID
			INNER JOIN  VehicleCatalog.Chrome.Categories Categories ON 	NewEquipmentMapping.TransmissionTypeCategoryID = Categories.CategoryID

		-- set drive train
		UPDATE @result
			SET DriveTrain = Categories.UserFriendlyName
			FROM @result result
			INNER JOIN builder.NewEquipmentMapping NewEquipmentMapping ON result.BusinessUnitID = NewEquipmentMapping.businessUnitID
																		AND result.InventoryID = NewEquipmentMapping.InventoryID
			INNER JOIN  VehicleCatalog.Chrome.Categories Categories ON 	NewEquipmentMapping.DriveTrainCategoryID = Categories.CategoryID
			
		-- set generic ext color
		UPDATE @result
			SET GenericExtColor = Colors.GenericExtColor
			FROM @result result
			INNER JOIN Merchandising.builder.OptionsConfiguration oc
				ON result.businessUnitID = oc.businessUnitID
				AND result.InventoryID = oc.InventoryID
			INNER JOIN VehicleCatalog.Chrome.Colors
				ON oc.chromeStyleID = Colors.StyleID 
				AND Colors.countryCode = 1
				AND oc.ExteriorColorCode = Colors.Ext1ManCode

		SELECT *
			FROM @result
		
	END TRY

	BEGIN CATCH
		EXEC dbo.sp_ErrorHandler
	END CATCH
		
	
	
	SET NOCOUNT OFF
	
RETURN
GO

GRANT EXECUTE ON [builder].[VehicleMerchandisingSummary#Fetch] TO MerchandisingUser 
GO
