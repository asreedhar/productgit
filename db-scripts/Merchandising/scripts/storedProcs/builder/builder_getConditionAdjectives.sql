if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[getConditionAdjectives]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[getConditionAdjectives]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_getConditionAdjectives.sql,v 1.4 2009/01/27 22:19:44 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * get the conditions used by a business unit
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitID     - BusinessUnitID to use
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[getConditionAdjectives]
	
	@BusinessUnitID varchar(17)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	

if exists (select 1 from builder.vehicleConditionAdjectives where businessUnitID = @BusinessUnitID)
	select conditionDescription from builder.vehicleConditionAdjectives where businessUnitID = @BusinessUnitID
else
	select conditionDescription from builder.vehicleConditionAdjectives where businessUnitID = 100150
	

END
GO

GRANT EXECUTE ON [builder].[getConditionAdjectives] TO MerchandisingUser 
GO