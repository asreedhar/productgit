use Merchandising


if object_id('builder.InsertMissingOptionsConfigurationRecords') is not null
       drop procedure builder.InsertMissingOptionsConfigurationRecords
go

create procedure [builder].[InsertMissingOptionsConfigurationRecords]
    @MemberLogin varchar(30) = '#InsertOcProc',
    @BusinessUnitID int = null,
    @InventoryID int = null
as

set nocount on
set transaction isolation level read uncommitted

declare @missing table (BusinessUnitID int not null,
                          InventoryID int not null,
                          primary key clustered (BusinessUnitID, InventoryID))

if @BusinessUnitID is null
begin
    insert into @missing (BusinessUnitID, InventoryID)
        select inv.BusinessUnitID, inv.InventoryID
        from FLDW.dbo.InventoryActive inv
        where inv.InventoryActive = 1
            and not exists (select 1 
                            from Merchandising.builder.OptionsConfiguration oc
                            where oc.BusinessUnitID = inv.BusinessUnitID
                                and oc.InventoryID = inv.InventoryID)
            and exists (select 1 
                        from IMT.dbo.DealerUpgrade du
                        where du.BusinessUnitID = inv.BusinessUnitID
                            and du.DealerUpgradeCD = 24) -- Max Ad
end
else if @InventoryID is null
begin
    insert into @missing (BusinessUnitID, InventoryID)
        select inv.BusinessUnitID, inv.InventoryID
        from FLDW.dbo.InventoryActive inv
        where inv.InventoryActive = 1
            and not exists (select 1
                            from Merchandising.builder.OptionsConfiguration oc
                            where oc.BusinessUnitID = inv.BusinessUnitID
                                and oc.InventoryID = inv.InventoryID)
            and inv.BusinessUnitID = @BusinessUnitID
end
else
begin
    insert into @missing (BusinessUnitID, InventoryID)
        select inv.BusinessUnitID, inv.InventoryID
        from FLDW.dbo.InventoryActive inv
        where inv.InventoryActive = 1
            and not exists (select 1 
                            from Merchandising.builder.OptionsConfiguration oc
                            where oc.BusinessUnitID = inv.BusinessUnitID
                                and oc.InventoryID = inv.InventoryID)
            and inv.BusinessUnitID = @BusinessUnitID
            and inv.InventoryID = @InventoryID;
end

-- NOTE: the insert is duplicated 3 times to obtain an optimized query plan for each scenario:
--  All active inventory for dealers that have Max Ad, 
--  Inventory for a single business unit, 
--  Or a single inventory item.
if @BusinessUnitID is null
begin
    insert into Merchandising.builder.OptionsConfiguration
        (BusinessUnitID, InventoryID, StockNumber, ChromeStyleID, NoPackages,
         CreatedOn, CreatedBy, LastUpdatedOn, LastUpdatedBy)
    select inv.BusinessUnitID, 
        inv.InventoryID, 
        inv.StockNumber,
        inv.ChromeStyleID,
        cast(case when inv.MakeHasWhitelistedPackages = 1 and inv.VehicleHasWhiteListedPackages = 0 
             then 1 else 0 end as bit) as NoPackages,
        GetDate() as CreatedOn, @MemberLogin as CreatedBy, 
        GetDate() as LastUpdatedOn, @MemberLogin as LastUpdatedBy
    from Merchandising.workflow.InventoryHasWhiteListedPackages inv
        inner join @missing missing
            on inv.BusinessUnitID = missing.BusinessUnitID
                and inv.InventoryID = missing.InventoryID
       /*
              Since there has been time elapsed bewtween builing @missing 
              and executing the insert AND there is another process inserting
              records into OC, must check for existence again to prevent PK dups.
              As long as #records is relatively small (<2-3K) the impact should be neglible
       */
       where not exists (select 1 
                                         from Merchandising.builder.OptionsConfiguration oc 
                                         where oc.BusinessUnitID = inv.BusinessUnitID
                                                and oc.InventoryID = inv.InventoryID)
end
else if @InventoryId is null
begin
    insert into Merchandising.builder.OptionsConfiguration
        (BusinessUnitID, InventoryID, StockNumber, ChromeStyleID, NoPackages,
         CreatedOn, CreatedBy, LastUpdatedOn, LastUpdatedBy)
    select inv.BusinessUnitID, 
        inv.InventoryID, 
        inv.StockNumber,
        inv.ChromeStyleID,
        cast(case when inv.MakeHasWhitelistedPackages = 1 and inv.VehicleHasWhiteListedPackages = 0 
             then 1 else 0 end as bit) as NoPackages,
        GetDate() as CreatedOn, @MemberLogin as CreatedBy, 
        GetDate() as LastUpdatedOn, @MemberLogin as LastUpdatedBy
    from Merchandising.workflow.InventoryHasWhiteListedPackages inv
        inner join @missing missing
            on inv.BusinessUnitID = missing.BusinessUnitID
                and inv.InventoryID = missing.InventoryID
    where inv.BusinessUnitID = @BusinessUnitID
        and missing.BusinessUnitID = @BusinessUnitID
              /*
                     Since there has been time elapsed bewtween builing @missing 
                     and executing the insert AND there is another process inserting
                     records into OC, must check for existence again to prevent PK dups.
                     As long as #records is relatively small (<2-3K) the impact should be neglible
              */
              and not exists (select 1 
                            from Merchandising.builder.OptionsConfiguration oc 
                            where oc.BusinessUnitID = inv.BusinessUnitID
                                and oc.InventoryID = inv.InventoryID)
end
else
begin
    insert into Merchandising.builder.OptionsConfiguration
        (BusinessUnitID, InventoryID, StockNumber, ChromeStyleID, NoPackages,
         CreatedOn, CreatedBy, LastUpdatedOn, LastUpdatedBy)
    select inv.BusinessUnitID, 
        inv.InventoryID, 
        inv.StockNumber,
        inv.ChromeStyleID,
        cast(case when inv.MakeHasWhitelistedPackages = 1 and inv.VehicleHasWhiteListedPackages = 0 
             then 1 else 0 end as bit) as NoPackages,
        GetDate() as CreatedOn, @MemberLogin as CreatedBy, 
        GetDate() as LastUpdatedOn, @MemberLogin as LastUpdatedBy
    from Merchandising.workflow.InventoryHasWhiteListedPackages inv
        inner join @missing missing
            on inv.BusinessUnitID = missing.BusinessUnitID
                and inv.InventoryID = missing.InventoryID
    where inv.BusinessUnitID = @BusinessUnitID
        and inv.InventoryId = @InventoryID
        and missing.BusinessUnitID = @BusinessUnitID
        and missing.InventoryID = @InventoryID
              /*
                     Since there has been time elapsed bewtween builing @missing 
                     and executing the insert AND there is another process inserting
                     records into OC, must check for existence again to prevent PK dups.
                     
                     As long as #records is relatively small (<2-3K) the impact should be neglible.
              */
              and not exists (select 1 
                    from Merchandising.builder.OptionsConfiguration oc 
                    where oc.BusinessUnitID = inv.BusinessUnitID
                        and oc.InventoryID = inv.InventoryID)
end
GO

GRANT EXECUTE ON builder.InsertMissingOptionsConfigurationRecords TO MerchandisingUser 
GO