if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[ConditionChecklist#Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[ConditionChecklist#Save]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_conditionChecklist_save.sql,v 1.1 2009/06/22 17:22:10 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Save the checklist of condition attributes
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - integer id of businessunit
 * @InventoryId - integer id inventory item
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[ConditionChecklist#Save]
	
	@BusinessUnitId int,
	@InventoryId int,
	@isDealerMaintained bit,
	@isFullyDetailed bit,
	@hasNoPanelScratches bit,
	@hasNoVisibleDents bit,
	@hasNoVisibleRust bit,
	@hasNoKnownAccidents bit,
	@hasNoKnownBodyWork bit,
	@hasPassedDealerInspection bit,
	@haveAllKeys bit,
	@noKnownMechanicalProblems bit,
	@hadAllRequiredScheduledMaintenancePerformed bit,
	@haveServiceRecords bit,
	@haveOriginalManuals bit
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
		
	IF not exists (select 1 
						from builder.ConditionChecklist
							where businessunitid = @BusinessUnitID 
								and InventoryId = @InventoryId 
								)
        BEGIN
            INSERT INTO builder.ConditionChecklist
			(businessUnitId, 
			inventoryId, 
			isDealerMaintained,
			isFullyDetailed,
			hasNoPanelScratches,
			hasNoVisibleDents,
			hasNoVisibleRust,
			hasNoKnownAccidents,
			hasNoKnownBodyWork,
			hasPassedDealerInspection,
			haveAllKeys,
			noKnownMechanicalProblems,
			hadAllRequiredScheduledMaintenancePerformed,
			haveServiceRecords,
			haveOriginalManuals
			) 
			VALUES
			(@BusinessUnitId, @InventoryId, 
			@isDealerMaintained,
			@isFullyDetailed,
			@hasNoPanelScratches,
			@hasNoVisibleDents,
			@hasNoVisibleRust,
			@hasNoKnownAccidents,
			@hasNoKnownBodyWork,
			@hasPassedDealerInspection,
			@haveAllKeys,
			@noKnownMechanicalProblems,
			@hadAllRequiredScheduledMaintenancePerformed,
			@haveServiceRecords,
			@haveOriginalManuals
			) 
        END
        ELSE
        BEGIN
            
            UPDATE builder.ConditionChecklist
            SET
			isDealerMaintained = @isDealerMaintained,
			isFullyDetailed = @isFullyDetailed,
			hasNoPanelScratches = @hasNoPanelScratches,
			hasNoVisibleDents = @hasNoVisibleDents,
			hasNoVisibleRust = @hasNoVisibleRust,
			hasNoKnownAccidents = @hasNoKnownAccidents,
			hasNoKnownBodyWork = @hasNoKnownBodyWork,
			hasPassedDealerInspection = @hasPassedDealerInspection,
			haveAllKeys = @haveAllKeys,
			noKnownMechanicalProblems = @noKnownMechanicalProblems,
			hadAllRequiredScheduledMaintenancePerformed = @hadAllRequiredScheduledMaintenancePerformed,
			haveServiceRecords = @haveServiceRecords,
			haveOriginalManuals = @haveOriginalManuals
			WHERE businessUnitId = @BusinessUnitId AND
			inventoryId = @InventoryId
			
			
         
        END
			
END
GO

GRANT EXECUTE ON [builder].[ConditionChecklist#Save] TO MerchandisingUser 
GO