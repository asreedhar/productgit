if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[UpdateDriveMapUpsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[UpdateDriveMapUpsert]
GO

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


-- =============================================
-- Author:		<Fitzpatrick, Benin>
-- Create date: <2/2/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_UpdateDriveMapUpsert.sql,v 1.2 2010/05/20 17:46:06 bfung Exp $
 * 
 * Summary finally
 * -------
 * 
 * Update or insert any given set of Equipment to a vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @inventoryID
 * @businessUnitID - defaults to 
 * @EngineTypeCategoryID - 
 * @TransmissionTypeCategoryID - 
 * @DriveTrainCategoryID - 
 * @FuelCategoryID -
 * @createdBy	
 * @createdOn
 * @updatedBy
 * @updatedOn
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[UpdateDriveMapUpsert]
	@inventoryID int,
	@businessUnitID int,
	@DriveTrainCategoryID int  
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF EXISTS (SELECT 1 FROM builder.NewEquipmentMapping WHERE businessUnitID = @businessUnitID AND inventoryID = @inventoryID)
	BEGIN
		declare @updatedOn datetime
		select @updatedOn = getdate()
		UPDATE builder.NewEquipmentMapping
		SET DriveTrainCategoryID = @DriveTrainCategoryID, updatedOn = @updatedOn
		WHERE
			businessUnitID = @businessUnitID 
			AND inventoryID = @inventoryID
	END
	ELSE
	BEGIN
		declare @createdOn datetime
		select @createdOn = getdate()
	INSERT INTO builder.NewEquipmentMapping
	(inventoryID, businessUnitID, EngineTypeCategoryID, TransmissionTypeCategoryID, DriveTrainCategoryID,
	 FuelCategoryID, createdBy, createdOn)
	VALUES
	(@inventoryID,  @businessUnitID, -1, -1, @DriveTrainCategoryID, -1, NULL, @createdOn)
	END
			
	--select SCOPE_IDENTITY()
END
GO

GRANT EXECUTE ON [builder].[UpdateDriveMapUpsert] TO MerchandisingUser 
GO



  

     