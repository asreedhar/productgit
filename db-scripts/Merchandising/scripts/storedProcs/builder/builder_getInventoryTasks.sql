if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[getInventoryTasks]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[getInventoryTasks]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_getInventoryTasks.sql,v 1.9 2010/03/09 16:56:46 bsombke Exp $
 * 
 * Summary
 * -------
 * 
 * gets the inventory item tasks for a business unit
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[getInventoryTasks]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@inventoryType int = 2,
	@InventoryObjective int = NULL	
	
		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @prefsBuid int
	IF EXISTS (SELECT 1 FROM settings.alertingPreferences WHERE businessUnitId = @BusinessUnitId)
		select @prefsBuid = @BusinessUnitID
	ELSE
		SELECT @prefsBuid = 100150
	
	declare @statusCodeBuid int
	IF EXISTS (SELECT 1 FROM settings.InventoryObjectiveMapping WHERE businessUnitId = @BusinessUnitId)
		set @statusCodeBuid = @BusinessUnitId
	ELSE
		set @statusCodeBuid = 100150
	
	CREATE TABLE #Inventory (
		BusinessUnitId int NOT NULL,
		Vin varchar(17) NOT NULL,
		StockNumber varchar(15) NOT NULL,
		InventoryReceivedDate datetime NOT NULL, 
		vehicleYear int NOT NULL, 
		UnitCost decimal(9,2) NOT NULL, 
		AcquisitionPrice decimal (8,2) NOT NULL, 
		Certified tinyint NOT NULL, 
		Make varchar(20) NOT NULL, 
		model varchar(50) NOT NULL,
		VehicleTrim varchar(50) NULL,		
		inventoryID int NOT NULL,
		MileageReceived int NULL, 
		TradeOrPurchase tinyint NOT NULL, 
		ListPrice decimal(8,2) NULL,
		InventoryType tinyint NOT NULL,
		InventoryStatusCD int NOT NULL,
		BaseColor varchar(50) NOT NULL,
		VehicleLocation varchar(20) NULL,
		LotPrice DECIMAL(8,2) NULL,
		MSRP DECIMAL(9,2) NULL,
		VehicleCatalogId int NULL
		,PRIMARY KEY (businessUnitId, inventoryId)
		
	)

	INSERT INTO #Inventory EXEC Interface.InventoryList#Fetch @BusinessUnitId=@BusinessUnitID, @UsedOrNew=@inventoryType

	CREATE TABLE #VehicleDataAndStatus (
		businessUnitID int NOT NULL
		,inventoryID int NOT NULL
		,isLegacy bit NOT NULL
		,chromeStyleID int NOT NULL
		,lotLocationID int NULL
		,afterMarketTrim varchar(50) NOT NULL
		,[specialId] int NULL
		,[doNotPostFlag] bit NOT NULL
		,[onlineFlag] smallint NOT NULL
		,[lotDataImportLock] bit NOT NULL
		,DescriptionSample varchar(180) NOT NULL
		,lastUpdateListPrice decimal(8,2) NULL
		,datePosted datetime NULL
		,exteriorStatus INT NOT NULL
		,interiorStatus INT NOT NULL
		,photoStatus INT NOT NULL 
		,adReviewStatus INT NOT NULL
		,pricingStatus INT NOT NULL 
		,postingStatus INT NOT NULL
		,daysPending INT NULL
		,stepsComplete INT NOT NULL
		,lowActivityFlag BIT NOT NULL
		,statusBucket INT NOT NULL

		,descriptionAlert BIT NOT NULL
		,photosAlert BIT NOT NULL
		,pricingAlert BIT NOT NULL
		,remerchandisingFlag BIT NOT NULL
		,NoPackages BIT NOT NULL
	)

	EXEC builder.VehicleData#Load @BusinessUnitId
	
	
	declare @ctRatio float
	select @ctRatio = minWeeklyClickThruRatio FROM settings.Merchandising WHERE businessUnitId = 100150


	SELECT
		count(*) as totalCt,
		sum(case when statusBucket = 128 AND remerchandisingFlag = 0 THEN 1 ELSE 0 END) as onlineCt,
		sum(case when statusBucket = 128 AND remerchandisingFlag = 1 THEN 1 ELSE 0 END) as onlineNeedActionCt,
		sum(case when statusBucket = 8 then 1 else 0 end) as doNotPostCt,
		sum(case when statusBucket IS NULL OR statusBucket = 16 then 1 else 0 end) as notOnlineCt,
		sum(case when statusBucket = 32 then 1 else 0 end) as pendingCt,
		
		sum(case when statusBucket IS NULL OR (statusBucket = 16 and (exteriorStatus IS NULL OR interiorStatus IS NULL OR exteriorStatus < 2 OR interiorStatus < 2)) then 1 else 0 end) as needEquipmentCt,
		sum(case when statusBucket IS NULL OR (statusBucket = 16 and (photoStatus IS NULL OR photoStatus < 2)) then 1 else 0 end) as needPhotosCt,
		sum(case when statusBucket IS NULL OR (statusBucket = 16 and (pricingStatus IS NULL OR pricingStatus < 2)) then 1 else 0 end) as needPricingCt,
		sum(case when statusBucket IS NULL OR (statusBucket = 16 and (adReviewStatus IS NULL OR adReviewStatus < 2)) then 1 else 0 end) as needAdReviewCt,

		sum(case when inv.listPrice > 0 then 0 else 1 end) as noPriceCt,
		sum(case when statusBucket = 128 and lowActivityFlag = 1 then 1 else 0 end) as lowActivityCt,
		sum(case when statusBucket = 128 and descriptionAlert = 1 THEN 1 ELSE 0 END) as underMerchDescriptionCt, --pub descrip will be null until posted, or the last pub until new posting
		sum(case when statusBucket = 128 and photosAlert = 1 THEN 1 ELSE 0 END) as underMerchPhotosCt,       --will be null until posted
		sum(case when statusBucket = 128 and pricingAlert = 1 THEN 1 ELSE 0 END) as underMerchPricingCt,  --will be null until posted

		sum(case when (statusBucket IS NULL OR statusBucket = 16) AND (exteriorStatus IS NULL OR interiorStatus IS NULL OR exteriorStatus < 2 OR interiorStatus < 2) AND datediff(d,inventoryReceivedDate,getdate()) > ap.daysToEquipmentComplete THEN 1 ELSE 0 END) as overdueEquipmentCt,
		sum(case when (statusBucket IS NULL OR statusBucket = 16) AND (photoStatus IS NULL OR photoStatus < 2) AND datediff(d,inventoryReceivedDate,getdate()) > ap.daysToPhotosComplete THEN 1 ELSE 0 END) as overduePhotosCt,
		sum(case when (statusBucket IS NULL OR statusBucket = 16) AND (pricingStatus IS NULL OR pricingStatus < 2) AND datediff(d,inventoryReceivedDate,getdate()) > ap.daysToPricingComplete THEN 1 ELSE 0 END) as overduePricingCt,
		sum(case when postingStatus IN (0,2) AND datediff(d,inventoryReceivedDate,getdate()) > daysToApproval THEN 1 ELSE 0 END) as overdueToPostOnlineCt,
		sum(case when statusBucket = 32 AND daysPending > ap.maxDaysToApprovedAfterPending THEN 1 ELSE 0 END) as overdueStillPendingCt
			
    FROM #Inventory inv
    LEFT JOIN #VehicleDataAndStatus vds ON vds.inventoryId = inv.inventoryId AND vds.businessUnitID = inv.businessUnitID
    LEFT JOIN postings.RollingWeekVehicleMetrics vm ON vm.inventoryId = inv.inventoryId AND vm.businessUnitId = inv.businessUnitId
    INNER JOIN settings.AlertingPreferences ap ON ap.businessUnitId = @prefsBuid
    WHERE
    @InventoryObjective IS NULL OR
    inv.inventoryStatusCD IN (SELECT inventoryStatusCD 
									FROM settings.InventoryObjectiveMapping
									WHERE businessUnitId = @statusCodeBuid AND
									objectiveTypeId = @InventoryObjective)
	
	DROP TABLE #Inventory
	DROP TABLE #VehicleDataAndStatus
	END
GO

GRANT EXECUTE ON [builder].[getInventoryTasks] TO MerchandisingUser 
GO