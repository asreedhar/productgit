if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[DescriptionItems#Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[DescriptionItems#Delete]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <1/5/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_descriptionItems_delete.sql,v 1.1 2009/01/05 21:23:51 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * deletes all description item types for a given inventory id
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - integer id of businessunit
 * @InventoryId - integer id of the inventory item
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[DescriptionItems#Delete]
	
	@BusinessUnitId int,
	@InventoryId int
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


DELETE FROM builder.descriptionContents
WHERE businessUnitId = @BusinessUnitId
	AND inventoryId = @InventoryId
		
END
GO

GRANT EXECUTE ON [builder].[DescriptionItems#Delete] TO MerchandisingUser 
GO