if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[CertificationTypes#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[CertificationTypes#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_certificationTypes_fetch.sql,v 1.3 2009/01/27 22:19:44 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetches all certification types for the businessUnit
 * if no businessUnit is specified, all certification types in the database are returned
 * 
 * 
 * Parameters
 * ----------
 * @BusinessUnitId
 * 
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[CertificationTypes#Fetch]
	@BusinessUnitId int = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	IF @BusinessUnitId IS NOT NULL 
	BEGIN
		SELECT DISTINCT mc.certificationTypeId, mc.[name]
		FROM builder.manufacturerCertifications mc
		INNER JOIN settings.dealerCertificationConfiguration dcs
			ON dcs.certificationTypeId = mc.certificationTypeId
		WHERE dcs.businessUnitId = @BusinessUnitId -- it is this business unit
		ORDER BY mc.[name] asc
	END
	ELSE
	BEGIN
		
		SELECT certificationTypeId, [name] 
			FROM builder.manufacturerCertifications
			ORDER BY [name] asc
			
	END	
END
GO

GRANT EXECUTE ON [builder].[CertificationTypes#Fetch] TO MerchandisingUser 
GO