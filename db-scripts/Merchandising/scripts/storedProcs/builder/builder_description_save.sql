if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[Description#Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[Description#Save]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_description_save.sql,v 1.4 2009/10/06 14:40:52 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * updates the merchandising description for a vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId= vehicle inventory id
 * @MerchandisingDescription = new description for vehicles
 * @ExpiresOn - date that the description expires
 * @MemberLogin - member modifying the description
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[Description#Save]
	
	@BusinessUnitID int,
	@InventoryId int,
	@AcceleratorAdvertisementId int,
	@MerchandisingDescription varchar(max),
	@Footer varchar(2000),
	@CurrentListPrice decimal(8,2),
	@ExpiresOn datetime = NULL,
	@IsAutoPilot bit = 0,
	@IsLongForm bit = 0,
	@TemplateId int = NULL,
	@ThemeId int = NULL,
	@Version timestamp output,
	@MemberLogin varchar(30) = ''
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	
	IF EXISTS (SELECT 1 FROM builder.Descriptions 
				WHERE BusinessUnitID = @BusinessUnitID AND inventoryId = @InventoryId)
	BEGIN
	
		--VERSION VALIDATION
		IF NOT EXISTS (SELECT 1 
			FROM builder.Descriptions
			WHERE 
			businessUnitId = @BusinessUnitId
			AND inventoryId = @InventoryId
			AND version = @Version)
		BEGIN
			declare @usingMember varchar(30)
			select @usingMember = lastUpdatedBy
				FROM builder.Descriptions
				WHERE 
				businessUnitId = @BusinessUnitId
				AND inventoryId = @InventoryId				
			IF @usingMember <> NULL
				BEGIN
					RAISERROR('The vehicle record is in use by %s',16,1, @usingMember)
				END
			ELSE
			BEGIN
				RAISERROR('The vehicle record does not exist and may have been deleted',16,1)
			END
			RETURN @@ERROR
		END
		
		--UPDATE
		UPDATE builder.Descriptions 
			SET 
				acceleratorAdvertisementId = @AcceleratorAdvertisementId,
				merchandisingDescription = @MerchandisingDescription,
				footer = @Footer,
				expiresOn = @ExpiresOn,
				templateId = @TemplateId,
				themeId = @ThemeId,
				lastUpdatedBy = @MemberLogin, 
				lastUpdatedOn = getdate(),
				lastUpdateListPrice = @CurrentListPrice,
				isAutoPilot = @IsAutoPilot,
				isLongForm = @IsLongForm
			WHERE BusinessUnitID = @BusinessUnitID AND inventoryId = @InventoryId
			
	END
	ELSE
	BEGIN
	
		--NO DESCRIPTION FOR VEHICLE YET
		INSERT INTO builder.Descriptions 
		(
		businessUnitId,
		inventoryId,
		acceleratorAdvertisementId, 
		merchandisingDescription, 
		footer,
		expiresOn,
		templateId,
		themeId,
		createdBy,
		createdOn,
		lastUpdatedBy, 
		lastUpdatedOn,
		lastUpdateListPrice,
		isAutoPilot,
		isLongForm)
		VALUES( 
			@BusinessUnitId,
			@InventoryId,
			@AcceleratorAdvertisementId,
			@MerchandisingDescription,
			@Footer,
			@ExpiresOn, 
			@TemplateId,
			@ThemeId,
			@MemberLogin, 
			getdate(),
			@MemberLogin, 
			getdate(),
			@CurrentListPrice,
			@IsAutoPilot,
			@IsLongForm
		)
		
		
	END

	SELECT @Version = version FROM builder.Descriptions
		WHERE BusinessUnitID = @BusinessUnitID AND inventoryId = @InventoryId

END
GO

GRANT EXECUTE ON [builder].[Description#Save] TO MerchandisingUser 
GO