if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleData#Load]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[VehicleData#Load]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * gets all inventory items that meet the input var filter criteria
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * 
 * #VehicleDataAndStatus defined in outer scope
 
 CREATE TABLE #VehicleDataAndStatus (
		businessUnitID int NOT NULL
		,inventoryID int NOT NULL
		,isLegacy bit NOT NULL
		,chromeStyleID int NOT NULL
		,lotLocationID int NULL
		,afterMarketTrim varchar(50) NOT NULL
		,[specialId] int NULL
		,[doNotPostFlag] bit NOT NULL
		,[onlineFlag] smallint NOT NULL
		,[lotDataImportLock] bit NOT NULL		
		,DescriptionSample varchar(180) NOT NULL
		,exteriorStatus INT NOT NULL
		,interiorStatus INT NOT NULL
		,photoStatus INT NOT NULL 
		,adReviewStatus INT NOT NULL  
		,pricingStatus INT NOT NULL 
		,postingStatus INT NOT NULL
		,daysPending INT NULL
		,stepsComplete INT NOT NULL
		,lowActivityFlag BIT NOT NULL
		,statusBucket INT NOT NULL

		,descriptionAlert BIT NOT NULL
		,photosAlert BIT NOT NULL
		,pricingAlert BIT NOT NULL
		,remerchandisingFlag BIT NOT NULL
		,NoPackages BIT NOT NULL
	)
 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[VehicleData#Load]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@InventoryID int = NULL,
	@debug bit = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    declare @prefDescLen int, @prefPhotoCt int, 
        @settingsBuId int
    
    set @settingsBuId = 
        case when exists (select 1 
                          from Merchandising.settings.Merchandising
                          where BusinessUnitId = @BusinessUnitId)
             then @BusinessUnitId 
             else 100150 end
    
    select @prefDescLen = desiredDescriptionLength,
            @prefPhotoCt = desiredPhotoCount
        from Merchandising.settings.Merchandising
        where BusinessUnitId = @settingsBuId
        
    if @debug = 1 print '1)load inventory: ' + convert(varchar,getdate(),121)

	CREATE TABLE #Statuses (
		businessUnitId int,
		inventoryId int,
		exteriorStatus int,
		interiorStatus int,
		photoStatus int,
		adReviewStatus int,
		pricingStatus int,
		postingStatus int,
		daysPending int DEFAULT(0),
		PRIMARY KEY (businessUnitId,inventoryId)
		
	)
	
	insert into #Statuses (businessUnitId,inventoryId, exteriorStatus, interiorStatus, photoStatus, pricingStatus,postingStatus, adReviewStatus)
	SELECT businessUnitId,InventoryId,
	ISNULL([1], 0) as exteriorStatus,
	ISNULL([2], 0) as interiorStatus,
	ISNULL([3], 0) as photoStatus,
	ISNULL([4], 0) as pricingStatus,
	ISNULL([5], 0) as postingStatus,
	ISNULL([6], 0) as adReviewStatus
	FROM 
	
	(SELECT BusinessUnitId, InventoryId, StatusTypeId, StatusLevel
		FROM builder.VehicleStatus
		WHERE businessUnitId = @BusinessUnitId
			AND (@InventoryId IS NULL 
			OR inventoryId = @InventoryId)
		) vs
		PIVOT
		(
		max(statusLevel)
		FOR StatusTypeId
		IN ([1],[2],[3],[4],[5],[6])
		) as pvt
	
	UPDATE st
	set daysPending = datediff(d, vs.lastUpdatedOn, getdate())
	FROM 
	#Statuses st
	JOIN builder.vehicleStatus vs 
	ON vs.businessUnitId = st.businessUnitId
		AND vs.inventoryid = st.inventoryId 
		AND vs.statusTypeId = 5 --posting status
		AND vs.statusLevel = 2 --2 is pending
		
		

	INSERT INTO #VehicleDataAndStatus
	SELECT 
	
	  oc.[businessUnitID]
      ,oc.[inventoryId]
      ,[isLegacy]
      ,oc.[chromeStyleID]
      ,[lotLocationId]
      ,[afterMarketTrim]
      ,[specialId]
      ,[doNotPostFlag]
      ,[onlineFlag]
      ,[lotDataImportLock]
      ,substring(ISNULL(de.MerchandisingDescription,''),1,110) as DescriptionSample
      ,de.lastUpdateListPrice,
		pub.datePosted,
		ISNULL(st.exteriorStatus,0) as exteriorStatus, 
		ISNULL(st.interiorStatus,0) as interiorStatus, 
		ISNULL(st.photoStatus,0) as photoStatus, 
		ISNULL(st.adReviewStatus,0) as adReviewStatus,
		ISNULL(st.pricingStatus,0) as pricingStatus, 
		ISNULL(st.postingStatus,0) as postingStatus,
		
		daysPending,
		CASE WHEN st.exteriorStatus > 0 THEN 1 ELSE 0 END +
		CASE WHEN st.interiorStatus > 0 THEN 1 ELSE 0 END +
		CASE WHEN st.photoStatus > 0 THEN 1 ELSE 0 END + 
		CASE WHEN st.pricingStatus > 0 THEN 1 ELSE 0 END +
		CASE WHEN st.adReviewStatus > 0 THEN 1 ELSE 0 END as stepsComplete, -- Is this correct? Shouldn't the comparison be > 1?
        0 as lowActivityFlag, -- deprecated. Has been moved to builder.Inventory#Fetch        
		CASE 
		WHEN doNotPostFlag = 1 THEN 8                        --doNotPost bucket
		WHEN st.postingStatus = 2 THEN 32                    --pending approval bucket
		WHEN onlineFlag = 1 OR st.postingStatus >= 3 THEN 128   --online ok
		ELSE 16                                              --web loading incomplete
		END as statusBucket,

		CASE WHEN len(isnull(COALESCE(pub.MerchandisingDescription,de.MerchandisingDescription),'')) < @prefDescLen THEN 1 ELSE 0 END as descriptionAlert,
--		CASE WHEN (ISNULL(pub.photoCount,0) < @prefPhotoCt) THEN 1 ELSE 0 END as photosAlert,
		0 as photosAlert,
		CASE WHEN (pub.pricePosted < 0.01) THEN 1 ELSE 0 END as pricingAlert,
		CASE WHEN len(ISNULL(COALESCE(pub.MerchandisingDescription,de.MerchandisingDescription),'')) < @prefDescLen 
--					OR (isnull(pub.photoCount,0) < @prefPhotoCt)
					OR (pub.pricePosted < 0.01) 
					OR  (lowActivityFlag = 1)
				THEN 1 ELSE 0 END as remerchandisingFlag,
	    oc.NoPackages 

    FROM builder.OptionsConfiguration oc 		
    LEFT JOIN #Statuses st ON st.businessUnitId = oc.businessUnitId AND st.inventoryId = oc.inventoryId
	LEFT JOIN builder.Descriptions de ON de.inventoryId = oc.inventoryId AND de.businessUnitId = oc.businessUnitId
	LEFT JOIN 
		(
			SELECT inventoryId, 
				businessUnitId, 
				pricePosted, 
				datePosted, 
--				photoCount,				
				MerchandisingDescription
			FROM postings.Publications
			WHERE businessUnitId = @BusinessUnitId
			AND dateRemoved IS NULL  --Get live publications ONLY (NULL indicates that it has not been removed)
		) as pub
		ON pub.inventoryId = oc.inventoryId 
		AND pub.businessUnitId = oc.businessUnitId
    
	WHERE 
		oc.businessUnitId = @BusinessUnitId
		AND (@InventoryID IS NULL OR oc.inventoryId = @InventoryId)	
	
	DROP TABLE #Statuses
	
END
GO

GRANT EXECUTE ON [builder].[VehicleData#Load] TO MerchandisingUser 
GO