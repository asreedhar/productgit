if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[Advertisement#Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[Advertisement#Save]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [builder].[Advertisement#Save]
	
	@BusinessUnitId int,
	@InventoryId INT,
	@AdvertisementXml VARCHAR(MAX),
	@MemberLogin VARCHAR(30) = ''	
	
AS
BEGIN

	IF EXISTS( SELECT 1 FROM builder.Advertisement WHERE BusinessUnitID = @BusinessUnitID AND InventoryID = @InventoryID )
		BEGIN
		
			UPDATE builder.Advertisement
			SET AdvertisementXml = @AdvertisementXml,
				LastUpdatedOn = GETDATE(),
				LastUpdatedBy = @MemberLogin
			WHERE BusinessUnitID = @BusinessUnitID AND InventoryID = @InventoryID
						
		END
	ELSE
		BEGIN

			INSERT INTO [builder].[Advertisement] (businessUnitId, inventoryId, advertisementXml, createdOn, createdBy )
			VALUES (@BusinessUnitId, @InventoryId, @AdvertisementXml, GETDATE(), @MemberLogin)
			
		END
			
END
GO

GRANT EXECUTE ON [builder].[Advertisement#Save] TO MerchandisingUser 
GO