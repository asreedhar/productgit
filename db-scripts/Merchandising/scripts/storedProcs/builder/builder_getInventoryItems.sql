if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[getInventoryItems]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[getInventoryItems]
GO

/****** Object:  StoredProcedure [builder].[getInventoryItems]    Script Date: 10/29/2008 17:22:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_getInventoryItems.sql,v 1.14 2010/08/19 21:24:39 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * gets all inventory items that meet the input var filter criteria
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 *	@StockNumber varchar(17) = '', - defaults to returning a list of stock items...
 *  @Vin varchar(17) = '' - defaults to returning all vins -- suggest only using one of stock/vin
 *	@merchandisingStatus int = 240 - defaults to all merchandising statuses valid values are - 16 - not posted, 32 - pending approval, 64 - online needs attention, 128 - online
 *	@inventoryType int = 0, - defaults to all inventory.  valid vals are 0 - all, 1 - new, 2 - used
 *	@activityFilter int = 7 - defaults to all activities.  valid vals are 1 - needs pricing, 2 - needs description attention, 4 - needs photos
 *  @searchParameter varchar(20) - this can be a text segment representing a stocknumber, make, or model
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[getInventoryItems]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@StockNumber varchar(17) = '',
	@Vin varchar(17) = '',
	@merchandisingStatus int = 240,
	@inventoryType int = 0,
	@activityFilter int = 7,
	@searchParameter varchar(20) = ''

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	CREATE TABLE #Inventory (
		BusinessUnitId int NOT NULL,
		Vin varchar(17) NOT NULL,
		StockNumber varchar(15) NOT NULL,
		InventoryReceivedDate datetime NOT NULL, 
		vehicleYear int NOT NULL, 
		UnitCost decimal(9,2) NOT NULL, 
		AcquisitionPrice decimal (8,2) NOT NULL, 
		Certified tinyint NOT NULL, 
		Make varchar(20) NOT NULL, 
		model varchar(50) NOT NULL,
		inventoryID int NOT NULL,
		MileageReceived int NULL, 
		TradeOrPurchase tinyint NOT NULL, 
		ListPrice decimal(8,2) NULL,
		InventoryType tinyint NOT NULL,
		InventoryStatusCD int NOT NULL,
		BaseColor varchar(50) NOT NULL,
		VehicleLocation varchar(20) NULL,
		LotPrice DECIMAL(8,2) NULL,
		MSRP DECIMAL(9,2) NULL,
		VehicleCatalogId int NULL
		,PRIMARY KEY (businessUnitId, inventoryId)
	)

	INSERT INTO #Inventory EXEC Interface.InventoryList#Fetch @BusinessUnitID
	
	CREATE TABLE #Thumbnails (
		imageThumbnailUrl varchar(100),
		inventoryId int,
		businessUnitId int,
		photoCount int
	)
	INSERT INTO #Thumbnails EXEC builder.getThumbnails @BusinessUnitID



SELECT 

	Vin, 
	inv.StockNumber, 
	InventoryReceivedDate, 
	vehicleYear, 
	UnitCost, 
	AcquisitionPrice, 
	Certified, 
	ISNULL(div.DivisionName, inv.Make) as make, 
	ISNULL(mdl.ModelName,inv.model) as model, 
	ISNULL(sty.trim, '') as trim, 
	ISNULL(mkt.marketClass, '') as marketClass,
	ISNULL(mkt.marketClassId, 0) as marketClassId,
	ISNULL(mkt.segmentId, 1) as segmentId,
	inv.inventoryID,
	ISNULL(vp.imageThumbnailUrl, '') as thumbnailUrl, 
	ISNULL(vp.photoCount, 0) as photoCount,
	ISNULL(ll.lotLocationId, '') as lotLocationId,
	ISNULL(oc.CertificationTypeId, -1) as certificationTypeId,
	ISNULL(mc.name, '') as certificationType,
	MileageReceived, 
	TradeOrPurchase, 
	ListPrice, 
	BaseColor,
	InventoryStatusCD,
	ISNULL(st.exteriorStatus, 0) as exteriorStatus, 
	ISNULL(st.interiorStatus, 0) as interiorStatus,
	ISNULL(st.photoStatus, 0) as photoStatus, 
	ISNULL(st.adReviewStatus, 0) as adReviewStatus,
	ISNULL(st.pricingStatus, 0) as pricingStatus, 
	ISNULL(st.postingStatus, 0) as postingStatus,
	ISNULL(oc.onlineFlag, 0) as onlineFlag,
	CASE WHEN st.exteriorStatus > 0 THEN 1 ELSE 0 END +
	CASE WHEN st.interiorStatus > 0 THEN 1 ELSE 0 END +
	CASE WHEN st.photoStatus > 0 THEN 1 ELSE 0 END + 
	CASE WHEN st.adReviewStatus > 0 THEN 1 ELSE 0 END +
	CASE WHEN st.pricingStatus > 0 THEN 1 ELSE 0 END as stepsComplete -- TODO: Should this be >= 2 for 'Complete'?
 
FROM #Inventory inv
LEFT JOIN builder.OptionsConfiguration oc ON oc.inventoryId = inv.inventoryId AND oc.businessUnitID = inv.businessUnitID
LEFT JOIN settings.LotLocations ll ON ll.lotLocationId = oc.lotLocationId AND ll.businessUnitID = oc.businessUnitID
LEFT JOIN builder.manufacturerCertifications mc ON mc.certificationTypeId = oc.CertificationTypeId
LEFT JOIN #Thumbnails vp ON inv.businessUnitID = vp.businessUnitID AND inv.inventoryId = vp.inventoryId
LEFT JOIN vehicleCatalog.chrome.styles sty ON sty.styleID = oc.chromestyleid and sty.countrycode = 1                    
LEFT JOIN vehicleCatalog.chrome.models mdl ON mdl.modelID = sty.modelID and mdl.countrycode = 1
LEFT JOIN vehicleCatalog.FirstLook.MarketClass mkt ON mkt.marketClassID = sty.mktClassID
LEFT JOIN vehicleCatalog.chrome.Divisions div ON div.divisionID = mdl.divisionID and div.countrycode = 1
LEFT JOIN (
	SELECT InventoryId, BusinessUnitId, 
		ISNULL([1], 0) as exteriorStatus,
		ISNULL([2], 0) as interiorStatus,
		ISNULL([3], 0) as photoStatus,
		ISNULL([4], 0) as pricingStatus,
		ISNULL([5], 0) as postingStatus,
		ISNULL([6], 0) as adReviewStatus
		FROM 
		(SELECT InventoryId, BusinessUnitId, StatusTypeId, StatusLevel
			FROM builder.VehicleStatus) vs
		PIVOT
		(
		max(statusLevel)
		FOR StatusTypeId
		IN ([1],[2],[3],[4],[5],[6])
		) as pvt
) st ON st.inventoryId = inv.inventoryId AND st.businessUnitId = inv.businessUnitId
	
WHERE 
(@StockNumber = '' OR inv.StockNumber = @StockNumber)
AND (@Vin = '' OR inv.Vin = @Vin)
AND (@inventoryType = 0 OR inv.inventoryType = @inventoryType)
AND (@merchandisingStatus = 240  --ALL BUCKETS
		OR (@merchandisingStatus = 16  --NOT ONLINE NEEDS ATTENTION
				AND (
						(oc.onlineFlag IS NULL OR oc.onlineFlag = 0  --not online flag (flag = zero)
						)
						AND (
							st.postingStatus IS NULL OR st.postingStatus <= 1)  --not posted and not pending
				)
			)
		OR (@merchandisingStatus = 32  --PENDING APPROVAL
			AND (st.postingStatus = 2) 
			)
		OR (@merchandisingStatus = 64  --ONLINE AND NEEDS ATTENTION
				AND (oc.onlineFlag = 1) 
				AND (st.postingStatus IS NULL OR st.postingStatus <= 1 OR --these two indicate not posted by us or manual needs attention
					st.exteriorStatus IS NULL OR st.exteriorStatus = 0 OR 
					 st.interiorStatus IS NULL OR st.interiorStatus = 0 OR
					st.photoStatus IS NULL OR st.photoStatus = 0 OR
					st.pricingStatus IS NULL OR st.pricingStatus = 0) -- TODO: Should adReviewStatus be used here too?
				AND NOT (st.postingStatus = 3)
			)	
		OR (@merchandisingStatus = 128 --ONLINE
				AND (st.postingStatus = 3 OR oc.onlineFlag = 1))  --posting status = 3 (successful release - but may not have appeared ONLINE YET)
	)

AND (@activityFilter = 7 
	OR	(@activityFilter = 1 AND (st.pricingStatus IS NULL OR st.pricingStatus < 1)) 
	OR	(@activityFilter = 2 AND (st.interiorStatus IS NULL OR st.interiorStatus < 1 OR
								  st.exteriorStatus IS NULL OR st.exteriorStatus < 1)) 
	OR	(@activityFilter = 4 AND (st.photoStatus IS NULL OR st.photoStatus < 1)) -- TODO: Should adReviewStatus be used here too?
	)
AND (@searchParameter = '' OR
		(inv.StockNumber LIKE @searchParameter 
			OR inv.Make LIKE @searchParameter 
			OR inv.Model LIKE @searchParameter 
			OR (len(@searchParameter) = 19 --account for percents on search text 
					AND inv.Vin LIKE @searchParameter))
	)

	
	DROP TABLE #Inventory
	DROP TABLE #Thumbnails
END
GO

GRANT EXECUTE ON [builder].[getInventoryItems] TO MerchandisingUser 
GO