if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleStatus#Create]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[VehicleStatus#Create]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_vehicleStatus_create.sql,v 1.3 2009/04/16 15:02:09 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * create a status record flags for a build step for a given vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId- vehicle inventory id
 * @MemberLogin - firstlook login for member who is updating the status
 * @StatusTypeId - status type of the update
 * @StatusLevel - level to indicate the status
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[VehicleStatus#Create]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@InventoryId int,
	@MemberLogin varchar(30),
	@StatusTypeId int,
	@StatusLevel int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	IF EXISTS (SELECT 1
		FROM builder.VehicleStatus
		WHERE 
		businessUnitId = @BusinessUnitId
		AND inventoryId = @InventoryId
		AND statusTypeId = @StatusTypeId)
		BEGIN
			RAISERROR('The status record already exists',16,1)
			RETURN @@ERROR
		END
	
	INSERT INTO builder.VehicleStatus
	(businessUnitId, inventoryId, statusTypeId,
	statusLevel, createdBy, createdOn, 
	lastUpdatedBy, lastUpdatedOn)
	VALUES (
	@BusinessUnitId, @InventoryId, @StatusTypeId,
	@StatusLevel, @MemberLogin, getdate(), 
	@MemberLogin,getdate()
	)
END
GO

GRANT EXECUTE ON [builder].[VehicleStatus#Create] TO MerchandisingUser 
GO