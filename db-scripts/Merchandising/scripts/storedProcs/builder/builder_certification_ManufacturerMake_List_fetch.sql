if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[Certification_ManufacturerMake_List#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[Certification_ManufacturerMake_List#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Moeller, Kurt>
-- Create date: <02/23/11>
-- =============================================

CREATE PROCEDURE [builder].[Certification_ManufacturerMake_List#Fetch]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT	cg.name AS 'Manufacturer', cg.cpoGroupId, mc.name AS 'Make', mc.certificationTypeId
	FROM		[Merchandising].builder.manufacturerCertifications mc
				LEFT JOIN [Merchandising].settings.cpoCertifieds cc ON cc.certificationTypeId = mc.certificationTypeId
				LEFT JOIN [Merchandising].settings.cpoGroups cg ON cg.cpoGroupId = cc.cpoGroupId
	ORDER BY	cg.name



END
GO

GRANT EXECUTE ON [builder].[Certification_ManufacturerMake_List#Fetch] TO MerchandisingUser 
GO