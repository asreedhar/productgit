if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[selectedEquipment#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[selectedEquipment#Fetch]
GO


/****** Object:  StoredProcedure [builder].[selectedEquipment#Fetch]    Script Date: 11/02/2008 18:10:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_selectedEquipment_fetch.sql,v 1.1 2009/05/01 23:03:21 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the current configuration of a vehicle by business unit and stock number
 * this config has detailed options text for option drill down functionality
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId - integer inventory id
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[selectedEquipment#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@InventoryId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
--------------------------------------
--FETCH SELECTED VEHICLE GENERIC EQUIPMENT
--------------------------------------
		select ct.categoryID, 
				ct.UserFriendlyName, --replaced with the dealer custom text
				ch.CategoryHeaderID, 
				ch.CategoryHeader, 
				'C' as [state],
				4 as tierNumber, 
				99999 as displayPriority,
				isStandardEquipment

			FROM builder.vehicleOptions vo
			--join categories to get full category object for
			--use by application logic
			INNER JOIN vehicleCatalog.Chrome.Categories AS ct
				ON ct.categoryID = vo.optionID
			INNER JOIN vehicleCatalog.Chrome.CategoryHeaders AS ch 
				ON ch.CategoryHeaderID = ct.CategoryHeaderID 
			WHERE vo.businessunitid = @BusinessUnitID 
				AND inventoryId = @InventoryId
				AND ch.countrycode = 1 and ct.countrycode = 1
			
	
END
GO

GRANT EXECUTE ON [builder].[selectedEquipment#Fetch] TO MerchandisingUser 
GO