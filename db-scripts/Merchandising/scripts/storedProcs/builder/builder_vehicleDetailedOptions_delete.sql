if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleDetailedOptions#Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[VehicleDetailedOptions#Delete]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_vehicleDetailedOptions_delete.sql,v 1.1 2009/02/02 21:12:13 jkelley Exp $$
 * 
 * Summary
 * -------
 * 
 * delete existing detailed options for vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - integer id of businessunit
 * @InventoryId - integer id of inventory item
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[VehicleDetailedOptions#Delete]
	
	@BusinessUnitId int,
	@InventoryId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	DELETE FROM builder.vehicleDetailedOptions 
		WHERE businessunitid = @BusinessUnitID 
		AND InventoryId = @InventoryId
			
END
GO

GRANT EXECUTE ON [builder].[VehicleDetailedOptions#Delete] TO MerchandisingUser 
GO