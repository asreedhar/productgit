if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[getKeyDisplayCategories]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[getKeyDisplayCategories]
GO


/****** Object:  StoredProcedure [builder].[getKeyDisplayCategories]    Script Date: 11/06/2008 11:51:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_getKeyDisplayCategories.sql,v 1.3 2008/11/20 20:01:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * get the display section filters
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[getKeyDisplayCategories]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select categoryID, categoryHeader from chrome.categories c 
    inner join chrome.categoryHeaders ch on ch.categoryheaderid = c.categoryheaderid
    WHERE c.categoryheaderid IN (SELECT DISTINCT categoryHeaderId FROM merchandising.keyDisplayCategories) 
        AND c.countrycode = 1 
        AND ch.countrycode = 1 
    ORDER by ch.categoryheader

END
GO

GRANT EXECUTE ON [builder].[getKeyDisplayCategories] TO MerchandisingUser 
GO