if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[getOrCreateDetailedVehicleConfiguration]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[getOrCreateDetailedVehicleConfiguration]
GO


/****** Object:  StoredProcedure [builder].[getOrCreateDetailedVehicleConfiguration]    Script Date: 11/02/2008 18:10:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_getOrCreateDetailedVehicleConfiguration.sql,v 1.20 2010/03/17 14:47:53 bfitzpatrick Exp $
 * 
 * Summary
 * -------
 * 
 * gets the current configuration of a vehicle by business unit and stock number
 * this config has detailed options text for option drill down functionality
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId - integer inventory id
 * @MemberLogin
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[getOrCreateDetailedVehicleConfiguration]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@InventoryId int,
	@MemberLogin varchar(30)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	IF NOT EXISTS (SELECT 1 FROM builder.OptionsConfiguration 
				WHERE BusinessUnitID = @BusinessUnitID 
				AND inventoryId = @InventoryId
			)
	BEGIN
		EXEC builder.VehicleConfiguration#Create @BusinessUnitId, @InventoryId, @MemberLogin			
	END

declare @rankings table (
	categoryID int,
	tierNumber int,
	displayPriority int,
	displayDescription varchar(80)
)		

declare @rankBuid int
IF EXISTS (SELECT 1 FROM settings.EquipmentDisplayRankings WHERE businessUnitId = @BusinessUnitId)
	select @rankBuid = @BusinessUnitID
ELSE
	select @rankBuid = 100150



		SELECT 
			oc.chromeStyleID,
			veh.VehicleCatalogId,
			veh.VIN,
			ISNULL(de.merchandisingDescription, '') as merchandisingDescription, 
			ISNULL(de.footer,'') as footer,
			de.acceleratorAdvertisementId,  --allow null
			ISNULL(extColor1, '') as extColor1, 
			ISNULL(extColor2, '') as extColor2, 
			ISNULL(intColor, '') as intColor, 
			ISNULL(vehicleCondition, '') as vehicleCondition, 
			tireTread, --allow null, code handles it
			ISNULL(afterMarketCategoryString, '') as afterMarketCategoryString, 
			ISNULL(afterMarketTrim, '') as afterMarketTrim, 
			ISNULL(additionalInfoText, '') as additionalInfoText,			
			ISNULL(specialId, -1) as specialId,
			ISNULL(de.expiresOn, DATEADD(day,365,getdate())) as expiresOn,
			oc.inventoryID,
			ISNULL(reconditioningText, '') as reconditioningText,
			ISNULL(reconditioningValue, 0) as reconditioningValue,
			lotDataImportLock,
			oc.version,
			
			oc.exteriorColorCode,
			oc.exteriorColorCode2,
			oc.interiorColorCode,
			
			--get condition checklist also...could get separately, but might as well just dump it here
			ISNULL(isDealerMaintained, 0) as isDealerMaintained,
			ISNULL(isFullyDetailed, 0) as isFullyDetailed,
			ISNULL(hasNoPanelScratches, 0) as hasNoPanelScratches,
			ISNULL(hasNoVisibleDents, 0) as hasNoVisibleDents,
			ISNULL(hasNoVisibleRust, 0) as hasNoVisibleRust,
			ISNULL(hasNoKnownAccidents, 0) as hasNoKnownAccidents,
			ISNULL(hasNoKnownBodyWork, 0) as hasNoKnownBodyWork,
			ISNULL(hasPassedDealerInspection, 0) as hasPassedDealerInspection,
			ISNULL(haveAllKeys, 0) as haveAllKeys,
			ISNULL(noKnownMechanicalProblems, 0) as noKnownMechanicalProblems,
			ISNULL(hadAllRequiredScheduledMaintenancePerformed, 0) as hadAllRequiredScheduledMaintenancePerformed,
			ISNULL(haveServiceRecords, 0) as haveServiceRecords,
			ISNULL(haveOriginalManuals, 0) as haveOriginalManuals
		FROM builder.OptionsConfiguration oc 
		    left join FLDW.dbo.Inventory inv
		        on oc.BusinessUnitID = inv.BusinessUnitID and oc.InventoryID = inv.InventoryID
		    left join FLDW.dbo.Vehicle veh
		        on inv.BusinessUnitID = veh.BusinessUnitID and inv.VehicleID = veh.VehicleID
		    LEFT JOIN builder.Descriptions de
		        ON de.BusinessUnitID = oc.BusinessUnitID 
			        AND de.InventoryId = oc.InventoryId 
		    LEFT JOIN builder.ConditionChecklist cc
		        ON cc.businessUnitId = oc.BusinessUnitID
			        AND cc.inventoryId = oc.inventoryId
		WHERE oc.BusinessUnitID = @BusinessUnitID 
			AND oc.InventoryId = @InventoryId

--------------------------------------
--FETCH SELECTED VEHICLE GENERIC EQUIPMENT
--------------------------------------

		select ct.categoryID, 
				ISNULL(optionCode, '') as optionCode, 
				ISNULL(detailText, '') as detailText,
				optionText,
				ct.CategoryTypeFilter, 
				coalesce(MSCD.Description,edr.displayDescription) as userFriendlyName, --replaced with the dealer custom text
				ch.CategoryHeaderID, 
				ch.CategoryHeader, 
				'C' as [state],
				edr.tierNumber, 
				edr.displayPriority,
				isStandardEquipment

			FROM builder.vehicleOptions vo
			--join categories to get full category object for
			--use by application logic
			INNER JOIN vehicleCatalog.Chrome.Categories AS ct
				ON ct.categoryID = vo.optionID
			INNER JOIN vehicleCatalog.Chrome.CategoryHeaders AS ch 
				ON ch.CategoryHeaderID = ct.CategoryHeaderID 
			INNER JOIN 
				settings.EquipmentDisplayRankings edr
				ON edr.businessUnitId = @rankBuid
				AND edr.categoryId = vo.optionId	
			INNER JOIN 
				builder.OptionsConfiguration oc
				ON oc.businessunitid = vo.businessunitid
				AND oc.inventoryid = vo.inventoryid
			LEFT JOIN chrome.ManufacturerSpecificCategoryDescriptions MSCD
				ON MSCD.ChromeStyleID = oc.ChromeStyleID
				and MSCD.CategoryID = vo.optionID	
			WHERE vo.businessunitid = @BusinessUnitID 
				AND vo.inventoryId = @InventoryId
				AND ch.countrycode = 1 and ct.countrycode = 1 
				AND ct.categoryTypeFilter != 'Transmission - Type'
				order by isStandardEquipment asc, 
			optionID
	
--------------------------------------
--FETCH SELECTED VEHICLE DETAILED EQUIPMENT
--------------------------------------	
		select 
			ISNULL(optionCode, '') as optionCode, 
			ISNULL(detailText, '') as detailText,
			optionText,
			categoryList,
			isStandardEquipment

			FROM builder.vehicleDetailedOptions vo
			
			WHERE businessunitid = @BusinessUnitID 
				AND inventoryId = @InventoryId
			
	
--------------------------------------
--FETCH VEHICLE CONDITION REPORT 
--------------------------------------	
		select vc.vehicleConditionId, 
			vc.vehicleConditionTypeID, 
			vc.conditionLevel,
			vc.description,
			vct.title
			
		FROM builder.vehicleConditions vc
			INNER JOIN builder.vehicleConditionTypes vct
				ON vc.vehicleConditionTypeId = vct.vehicleConditionTypeId
		WHERE vc.inventoryId = @InventoryId
			AND vc.businessUnitId = @BusinessUnitId
	
--------------------------------------
--FETCH VEHICLE NOTES
--------------------------------------		
		select noteId, noteText, writtenOn, writtenBy
		FROM builder.loaderNotes
		WHERE inventoryId = @InventoryId
		AND businessUnitId = @BusinessUnitId
		ORDER BY writtenOn ASC
		
		------------------------------------
		--- FETCH "KEY INFO"
		------------------------------------
		SELECT 
			iki.infoId, 
			aic.infoAdDisplay as description
		FROM builder.Inventory_KeyInformation iki
		JOIN builder.additionalInfoCategories aic
			ON aic.infoId = iki.infoId
		WHERE iki.businessUnitId = @BusinessUnitId
			AND iki.inventoryId = @InventoryId
			AND aic.enabled = 1
		ORDER BY aic.priority asc
		
	

END
GO

GRANT EXECUTE ON [builder].[getOrCreateDetailedVehicleConfiguration] TO MerchandisingUser 
GO