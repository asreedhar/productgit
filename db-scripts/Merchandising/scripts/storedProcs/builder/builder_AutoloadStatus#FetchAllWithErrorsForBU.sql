USE [Merchandising]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[AutoloadStatus#FetchAllWithErrorsForBU]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[AutoloadStatus#FetchAllWithErrorsForBU]
GO
/****** Object:  StoredProcedure [builder].[AutoloadStatus#FetchAllWithErrorsForBU]    Script Date: 12/28/2011 11:42:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [builder].[AutoloadStatus#FetchAllWithErrorsForBU]
	
	@BusinessUnitID int
	
AS
BEGIN

	select status.inventoryId, status.statusTypeId 
	from [builder].[AutoloadStatus] status
	inner join [FLDW].[dbo].[InventoryActive] inv 	
		on inv.businessunitid = status.businessunitid and status.inventoryid = inv.inventoryID
	where status.businessUnitId = @BusinessUnitID and statusTypeId  < 0
			
END
GO
GRANT EXECUTE ON [builder].[AutoloadStatus#FetchAllWithErrorsForBU] TO MerchandisingUser 
GO