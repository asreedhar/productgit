if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[CertificationFeatureDisplay#Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[CertificationFeatureDisplay#Update]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_certificationFeatureDisplay_update.sql,v 1.1 2009/01/19 19:51:23 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Update the desired certification features for a dealer for a particular certification
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - integer id of businessunit
 * @CertificationTypeId - type of certification to update
 * @CertificationFeatureId - feature id to update for that certification
 * @FeatureDisplayFlag - flag to indicate whether or not to display the feature
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[CertificationFeatureDisplay#Update]
	
	@BusinessUnitId int,
	@CertificationTypeId int,
	@CertificationFeatureId int,
	@FeatureDisplayFlag bit
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--determine what template they are using for photo items (own or default from FL)
	IF EXISTS (SELECT 1 FROM settings.dealerCertificationConfiguration WHERE
		businessUnitId = @BusinessUnitId 
		AND CertificationTypeId = @CertificationTypeId	
		AND CertificationFeatureId = @CertificationFeatureId )
	BEGIN
		UPDATE settings.dealerCertificationConfiguration 
		SET FeatureDisplayFlag = @FeatureDisplayFlag
		WHERE businessUnitId = @BusinessUnitId 
		AND CertificationTypeId = @CertificationTypeId	
		AND CertificationFeatureId = @CertificationFeatureId
	END
	ELSE
	BEGIN
		INSERT INTO settings.dealerCertificationConfiguration 
			(businessUnitId, CertificationTypeId, CertificationFeatureId, FeatureDisplayFlag)
			VALUES
			(@BusinessUnitId, @CertificationTypeId, @CertificationFeatureId, @FeatureDisplayFlag)
			
	END
			
END
GO

GRANT EXECUTE ON [builder].[CertificationFeatureDisplay#Update] TO MerchandisingUser 
GO