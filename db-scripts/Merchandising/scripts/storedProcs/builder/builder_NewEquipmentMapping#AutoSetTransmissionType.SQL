IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'Builder.NewEquipmentMapping#AutoSetTransmissionType')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE Builder.NewEquipmentMapping#AutoSetTransmissionType
GO

CREATE PROCEDURE Builder.NewEquipmentMapping#AutoSetTransmissionType
					@MemberLogin  varchar(50) = '#nemMaintenance'
AS 
    SET NOCOUNT ON

    BEGIN TRY
    
		-- create a temp table to optimize query and final not exists
		DECLARE @nonUnique TABLE
		(
			VINPattern_Prefix varchar(9)
		)
		
		INSERT INTO @nonUnique
			SELECT VINPattern_Prefix
				FROM VehicleCatalog.Chrome.VINPattern VINPattern
				INNER JOIN VehicleCatalog.Chrome.VINEquipment ON VINPattern.VINPatternID = VINEquipment.VINPatternID
																	AND VINPattern.CountryCode = VINEquipment.CountryCode
				INNER JOIN VehicleCatalog.Chrome.Categories ON VINEquipment.CountryCode = Categories.CountryCode
															AND  VINEquipment.CategoryID = Categories.CategoryID
				WHERE VINPattern.CountryCode = 1
				AND Categories.CategoryHeaderID = 15 -- transmittion
				AND Categories.CategoryTypeFilter = 'Transmission'
				AND VINEquipment.VINAvailability = 'Installed'
				GROUP BY VINPattern_Prefix
				HAVING COUNT(DISTINCT VINEquipment.CategoryID) > 1

		-- create table to hold results
		DECLARE @result TABLE
		(
			BusinessUnitID int not null,
			InventoryID int not null, 
			TransmissionTypeCategoryID  int not null
		)
		
		-- get vehicles that do not have their transmission catagory set
		-- this is the second shot at just getting 'automatic' or 'manual' by type
		INSERT INTO @result
			(BusinessUnitID, InventoryID, TransmissionTypeCategoryID)
				SELECT InventoryActive.BusinessUnitID, InventoryActive.InventoryID, CategoryID
					FROM FLDW.dbo.InventoryActive InventoryActive
					INNER JOIN FLDW.dbo.Vehicle vehicle	ON InventoryActive.vehicleid = vehicle.vehicleid
														AND InventoryActive.businessUnitId = vehicle.businessUnitId
					INNER JOIN (SELECT DISTINCT VINPattern_Prefix, VINEquipment.CategoryID
										FROM VehicleCatalog.Chrome.VINPattern VINPattern
										INNER JOIN VehicleCatalog.Chrome.VINEquipment ON VINPattern.VINPatternID = VINEquipment.VINPatternID
																							AND VINPattern.CountryCode = VINEquipment.CountryCode
										INNER JOIN VehicleCatalog.Chrome.Categories ON VINEquipment.CountryCode = Categories.CountryCode
																					AND  VINEquipment.CategoryID = Categories.CategoryID
										WHERE VINPattern.CountryCode = 1
										AND Categories.CategoryHeaderID = 15 -- transmittion
										AND Categories.CategoryTypeFilter = 'Transmission'
										AND VINEquipment.VINAvailability = 'Installed'
										) uniqueVinPattern ON uniqueVinPattern.VINPattern_Prefix = SUBSTRING(vehicle.Vin,0,9) + SUBSTRING(vehicle.Vin,10,1)
					WHERE InventoryActive.InventoryActive = 1
					AND EXISTS (SELECT *
								FROM IMT.dbo.DealerUpgrade
								WHERE DealerUpgrade.BusinessUnitID = InventoryActive.BusinessUnitID
								AND DealerUpgrade.DealerUpgradeCD = 24) -- Max Ad
					AND NOT EXISTS (SELECT *
										FROM Merchandising.builder.NewEquipmentMapping
										WHERE InventoryActive.BusinessUnitID = NewEquipmentMapping.businessUnitID
										AND InventoryActive.InventoryID = NewEquipmentMapping.inventoryID
										AND NewEquipmentMapping.TransmissionTypeCategoryID > 0)
					AND NOT EXISTS(SELECT *
									FROM @nonUnique nonUnique
									WHERE nonUnique.VINPattern_Prefix = uniqueVinPattern.VINPattern_Prefix)
		

		-- insert new rows
		INSERT INTO merchandising.builder.NewEquipmentMapping
			(
				inventoryID, 
				businessUnitID,
				EngineTypeCategoryID,
				TransmissionTypeCategoryID,
				DriveTrainCategoryID,
				FuelCategoryID,
				createdBy,
				createdOn
			)
			SELECT result.InventoryID,
					result.BusinessUnitID,
					-1,
					result.TransmissionTypeCategoryID,
					-1,
					-1,
					@MemberLogin,
					GETDATE()
				FROM @result result
				WHERE NOT EXISTS (SELECT *
									FROM merchandising.builder.NewEquipmentMapping
									WHERE NewEquipmentMapping.businessUnitID = result.businessUnitID
									AND NewEquipmentMapping.inventoryID = result.inventoryID)
		

		-- update existing rows
		UPDATE NewEquipmentMapping
			SET TransmissionTypeCategoryID = result.TransmissionTypeCategoryID,
				updatedBy = @MemberLogin,
				updatedOn = GETDATE()
			FROM merchandising.builder.NewEquipmentMapping
			INNER JOIN @result result ON NewEquipmentMapping.businessUnitID = result.BusinessUnitID
										AND NewEquipmentMapping.inventoryID = result.inventoryID
			WHERE NewEquipmentMapping.TransmissionTypeCategoryID <= 0 -- only if it has not been set
		
		

    END TRY
    BEGIN CATCH

			DECLARE @ErrorMessage NVARCHAR(4000),
			  @ErrorNumber INT,
			  @ErrorSeverity INT,
			  @ErrorState INT,
			  @ErrorLine INT,
			  @ErrorProcedure NVARCHAR(200) ;

				-- Assign variables to error-handling functions that 
				-- capture information for RAISERROR.
			SELECT @ErrorNumber = ERROR_NUMBER(), @ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE(), @ErrorLine = ERROR_LINE(),
					@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-') ;

				-- Building the message string that will contain original
				-- error information.
			SET @ErrorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' +
					'Message: ' + ERROR_MESSAGE() ;

				-- Raise an error: msg_str parameter of RAISERROR will contain
				-- the original error information.
			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, -- parameter: original error number.
			  @ErrorSeverity, -- parameter: original error severity.
			  @ErrorState, -- parameter: original error state.
			  @ErrorProcedure, -- parameter: original error procedure name.
			  @ErrorLine-- parameter: original error line number.
					)

    END CATCH
    
    SET NOCOUNT ON
GO

GRANT EXECUTE ON Builder.NewEquipmentMapping#AutoSetTransmissionType TO MerchandisingUser 
GO