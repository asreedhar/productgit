SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'builder.VehicleReferenceData#Upsert') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE builder.VehicleReferenceData#Upsert
GO

CREATE PROCEDURE builder.VehicleReferenceData#Upsert 
	@Vin char(17), 
	@DataTypeId int, 
	@DataSource nvarchar(500),
	@Value nvarchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @VehicleId int;
	SELECT @VehicleId = VehicleId FROM IMT.dbo.tbl_Vehicle WHERE Vin = @Vin;
	
	IF EXISTS (SELECT * FROM builder.VehicleReferenceData WHERE VehicleId = @VehicleId AND DataTypeId = @DataTypeId)
	BEGIN
		UPDATE builder.VehicleReferenceData SET DataSource = @DataSource, Value = @Value
		WHERE VehicleId = @VehicleId AND DataTypeId = @DataTypeId
	END
	ELSE
	BEGIN
		INSERT INTO builder.VehicleReferenceData (VehicleId, DataTypeId, DataSource, Value)
		VALUES( @VehicleId, @DataTypeId, @DataSource, @Value )
	END
END

go
grant execute on builder.VehicleReferenceData#Upsert to MerchandisingUser
go

