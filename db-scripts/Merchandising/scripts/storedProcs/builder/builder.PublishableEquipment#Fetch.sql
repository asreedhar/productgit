-- =============================================
-- Author:		Travis Huber
-- Create date: 2014-02-07
-- Description: Get a list of active equipment 
--				by vin
-- =============================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'builder.PublishableEquipment#Fetch') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE builder.PublishableEquipment#Fetch
GO

CREATE PROCEDURE builder.PublishableEquipment#Fetch
	@BusinessUnitId INT = null,
	@InventoryId INT = null,
	@Vin char(17)
AS
SET NOCOUNT ON



	DECLARE @rankBuid int 

	/*
	This is used to get data for a inventory item, specified by VIN in a message.  The message has BusinessUnitId, InventoryId properties
	along with VIN.  This stored procedure used to just search for an active inventory item solely based on VIN.  This could return multiple 
	rows in some cases.  In these cases, no order or preference was given to pick one particular buid/invid combination.  We are changing this 
	so that we will use more specific information if we have it available.  We are keeping the VIN-only lookup logic in here just in case 
	some messages come through without the businessUnitId and inventoryId properties being set.  If this is the case, we will revert to
	the VIN only lookup, but if we get any other information to allow us to more specifically identify an inventoryItem, we will use it.
	
	dhillis, 2015-06-17
	*/
	
	IF ( @BusinessUnitId IS NOT NULL AND @InventoryId IS NOT NULL )
	BEGIN
		-- InventoryId and BusinessUnitId are provided (ideal situation)
		-- If we get a @BusinessUnitId and @InventoryId, scope the VIN search by BUID
		SELECT @BusinessUnitID = BusinessUnitId, @InventoryId = InventoryId
			FROM FLDW.dbo.InventoryActive 
			WHERE BusinessUnitID = @BusinessUnitId 
			AND InventoryID = @InventoryId
			AND Vin = @Vin; -- Make sure the VIN is the same as it was when the message was created
	END
	ELSE IF (@InventoryId IS NOT NULL)
	BEGIN
		-- VIN and InventoryId are provided, use both
		SELECT @BusinessUnitID = BusinessUnitId, @InventoryId = InventoryId
			FROM FLDW.dbo.InventoryActive 
			WHERE InventoryID = @InventoryId
			AND VIN = @Vin -- Make sure the VIN is the same as it was when the message was created
	END
	ELSE IF (@BusinessUnitId IS NOT NULL)
	BEGIN
		-- VIN and BusinessUnitId are provided
		-- If we get a @BusinessUnitId and @Vin, scope the VIN search by BUID
		SELECT @BusinessUnitID = BusinessUnitId, @InventoryId = InventoryId
			FROM FLDW.dbo.InventoryActive 
			WHERE BusinessUnitID = @BusinessUnitId 
			AND Vin = @vin
	END
	ELSE
	BEGIN
		-- Only VIN is provided (not ideal), but leaving this here for backward compatibility.
		-- This could potentially return multiple rows, not sure which one we'll get, but this is 
		-- what we're currently doing, so at least it won't be a step backwards.
		SELECT @BusinessUnitID = ia.BusinessUnitId, @InventoryId = ia.InventoryId
			FROM FLDW.dbo.InventoryActive ia
			WHERE ia.Vin = @vin
			AND EXISTS 
			(	-- if we have multiple active inventory items, let's make sure we return one
				-- with vehicle options.  During testing, I found that calls to this proc would
				-- return an empty resultset when it happened to pick an inventoryitem without
				-- any rows in this table.
				SELECT * 
				FROM builder.VehicleOptions vo
				WHERE vo.businessUnitID = ia.BusinessUnitID
				AND vo.inventoryId = ia.InventoryID
			)
	END
	        
		

	IF EXISTS (SELECT 1 FROM settings.EquipmentDisplayRankings WHERE businessUnitId = @BusinessUnitId)
			select @rankBuid = @BusinessUnitID
	ELSE
			select @rankBuid = 100150

	SELECT vo.inventoryId, 
			ct.categoryID,
			ct.UserFriendlyName AS Equipment,
			ch.CategoryHeaderID,
			ch.CategoryHeader AS Category,
			edr.tierNumber as Tier,
			edr.displayPriority AS displayPriority,
			isStandardEquipment AS Standard,
			oc.lastUpdatedOn

      FROM builder.vehicleOptions vo
      --join categories to get full category object for
      --use by application logic
      INNER JOIN vehicleCatalog.Chrome.Categories AS ct
            ON ct.categoryID = vo.optionID
      INNER JOIN vehicleCatalog.Chrome.CategoryHeaders AS ch
            ON ch.CategoryHeaderID = ct.CategoryHeaderID
      INNER JOIN
           settings.EquipmentDisplayRankings edr
            ON edr.businessUnitId = @rankBuid
            AND edr.categoryId = vo.optionId
      INNER JOIN
           builder.OptionsConfiguration oc
            ON oc.businessUnitID = vo.businessUnitID
            AND oc.inventoryId = vo.inventoryId
           
      WHERE vo.businessunitid = @BusinessUnitID
            AND vo.inventoryId = @InventoryId
            AND ch.countrycode = 1 and ct.countrycode = 1
            ORDER BY CategoryHeader, tierNumber ASC


GO

GRANT EXECUTE ON builder.PublishableEquipment#Fetch TO MerchandisingUser
GO
