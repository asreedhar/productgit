use Merchandising
go

if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleConfiguration#Releasable]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[VehicleConfiguration#Releasable]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_vehicleConfiguration_releasable.sql,v 1.2 2009/11/09 17:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Make a vehicle releasable
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId int   - integer id of the businesUnit for which to get the alert list
 * @InventoryId - vehicle inventory id
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[VehicleConfiguration#Releasable]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@InventoryId int,
	@MemberLogin varchar(30),
	@UserInitiated bit = 1
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--if no record, create it - this will default into the releaseable mode
	IF NOT EXISTS (SELECT 1 FROM builder.OptionsConfiguration WHERE businessUnitId = @BusinessUnitId and inventoryId = @InventoryId)
	BEGIN
		EXEC builder.VehicleConfiguration#Create @BusinessUnitId, @InventoryId, @MemberLogin
	END
	ELSE 
	BEGIN
		--update do not post...
		UPDATE builder.OptionsConfiguration
		SET doNotPostFlag = 0,
		    DoNotPostLocked = case when @UserInitiated = 1 then 1 else DoNotPostLocked end,
			lastUpdatedBy = @MemberLogin,
			lastUpdatedOn = getdate()
		WHERE 
			businessUnitId = @BusinessUnitId
			AND inventoryId = @InventoryId
			
		--update posting settings? 
		--set postingStautus = 0
		--set onlineFlag = 0?
		
		--set doNotPostFlag on MerchandisingInteface as well
		EXEC Interface.VehicleExportStatus#Update @BusinessUnitId, @InventoryId, 0
	END	
	
	----SHOULD WE republish existing postings (the latest) at this point if applicable?
    
END

GO

GRANT EXECUTE ON [builder].[VehicleConfiguration#Releasable] TO MerchandisingUser 
GO