if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[getLowActivityAlerts]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[getLowActivityAlerts]
GO

/****** Object:  StoredProcedure [builder].[getLowActivityAlerts]    Script Date: 10/26/2008 13:09:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/21/08>
-- Description:	<Gets the current low activity alerts for a vehicle
--				this is intended to be used in the build process to show what needs to be done>
-- =============================================

CREATE PROCEDURE [builder].[getLowActivityAlerts]
	@BusinessUnitID int,
	@InventoryId int,
	@NegativeTrendingThreshold real
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

SELECT
		CASE 
			WHEN 
				wvm.dayRecordsInAggregate > 5 AND 7*wvm.sumSearchViewed/wvm.dayRecordsInAggregate < minWeeklySearchCount 
				THEN '- low searches -'
			ELSE '' 
		END AS SearchAlert,
		CASE 
			WHEN wvm.dayRecordsInAggregate > 5 AND wvm.clickThruRatio < minWeeklyClickThruRatio 
				THEN '- low clickthru rate -'
			ELSE '' 
		END AS ClickthruAlert,
		CASE 
			WHEN wvm.maxAvgPrice = 0 THEN ''
			WHEN wvm.maxVehiclePriceInWeek > 1.1*wvm.maxAvgPrice 
				THEN '- potentially overpriced -'
			WHEN wvm.maxVehiclePriceInWeek < 0.9*wvm.maxAvgPrice 
				THEN '- potentially underpriced -' 
			ELSE '' 
		END AS PricingAlert,
		wvm.sumSearchViewed/NULLIF(wvm.dayRecordsInAggregate,0) as avgSearchViewed,
		wvm.sumSearchViewed,
		wvm.clickThruRatio,
		pp.sumSearchViewed as previousSumSearchViewed, pp.clickThruRatio as previousClickThruRatio,
		wvm.maxAvgPrice,
		wvm.totalActions,
		wvm.dayRecordsInAggregate,
		pp.dayRecordsInAggregate as previousDayRecordsInAggregate,
		
		CASE
			WHEN wvm.dayRecordsInAggregate > 5 
				AND 
				(
					cast(wvm.sumSearchViewed as real)/wvm.dayRecordsInAggregate <= @NegativeTrendingThreshold*cast(pp.sumSearchViewed as real)/pp.dayRecordsInAggregate 
					OR wvm.clickThruRatio <= @NegativeTrendingThreshold*pp.clickThruRatio
				)
			THEN cast(1 as bit)
			ELSE cast(0 as bit)
			END as hasNegativeTrend
		FROM 
			postings.RollingWeekVehicleMetrics wvm
			INNER JOIN settings.merchandising m 
				ON m.businessUnitID = wvm.businessUnitID
			LEFT JOIN postings.PastPerformance pp
				On pp.businessUnitId = wvm.businessUnitId
				AND pp.inventoryId = wvm.inventoryId

		WHERE wvm.businessUnitID = @BusinessUnitID 
			AND wvm.inventoryId = @InventoryId



END
GO

GRANT EXECUTE ON [builder].[getLowActivityAlerts] TO MerchandisingUser 
GO
