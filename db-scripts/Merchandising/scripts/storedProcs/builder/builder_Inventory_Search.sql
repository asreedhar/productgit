if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[Inventory#Search]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[Inventory#Search]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * gets all inventory items that meet the input var filter criteria
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 *	@StockNumber varchar(17) = '', - defaults to returning a list of stock items...
 *  @Vin varchar(17) = '' - defaults to returning all vins -- suggest only using one of stock/vin
 *	@merchandisingStatus int = 240 - defaults to all merchandising statuses valid values are - 16 - not posted, 32 - pending approval, 64 - online needs attention, 128 - online
  *	@activityFilter int = 7 - defaults to all activities.  valid vals are 1 - needs pricing, 2 - needs description attention, 4 - needs photos
 *  @searchParameter varchar(20) - this can be a text segment representing a stocknumber, make, or model
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[Inventory#Search]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@searchParameter varchar(20) = '',
	@sortExpression varchar(30) = 'stockNumber ASC',
	@startRowIndex int = 0,
	@maximumRows int = 1000

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	CREATE TABLE #Inventory (
		BusinessUnitId int NOT NULL,
		Vin varchar(17) NOT NULL,
		StockNumber varchar(15) NOT NULL,
		InventoryReceivedDate datetime NOT NULL, 
		vehicleYear int NOT NULL, 
		UnitCost decimal(9,2) NOT NULL, 
		AcquisitionPrice decimal (8,2) NOT NULL, 
		Certified tinyint NOT NULL, 
		Make varchar(20) NOT NULL, 
		model varchar(50) NOT NULL,
		VehicleTrim varchar(50) NULL,		
		inventoryID int NOT NULL,
		MileageReceived int NULL, 
		TradeOrPurchase tinyint NOT NULL, 
		ListPrice decimal(8,2) NULL,
		InventoryType tinyint NOT NULL,
		InventoryStatusCD int NOT NULL,
		BaseColor varchar(50) NOT NULL,
		VehicleLocation varchar(20) NULL,
		LotPrice DECIMAL(8,2) NULL,
		MSRP DECIMAL(9,2) NULL,
		VehicleCatalogId int NULL
		,PRIMARY KEY (businessUnitId, inventoryId)
	)

	INSERT INTO #Inventory EXEC Interface.InventoryList#Fetch @BusinessUnitId=@BusinessUnitID, @UsedOrNew=0
	
	CREATE TABLE #Thumbnails (
		imageThumbnailUrl varchar(200),
		inventoryId int,
		businessUnitId int,
		photoCount int
	)
	INSERT INTO #Thumbnails EXEC builder.getThumbnails @BusinessUnitID

	CREATE TABLE #VehicleDataAndStatus (
		businessUnitID int NOT NULL
		,inventoryID int NOT NULL
		,isLegacy bit NOT NULL
		,chromeStyleID int NOT NULL
		,lotLocationID int NULL
		,afterMarketTrim varchar(50) NOT NULL
		,[specialId] int NULL
		,[doNotPostFlag] bit NOT NULL
		,[onlineFlag] smallint NOT NULL
		,[lotDataImportLock] bit NOT NULL
		,DescriptionSample varchar(180) NOT NULL
		,lastUpdateListPrice decimal(8,2) NULL
		,datePosted datetime NULL
		,exteriorStatus INT NOT NULL
		,interiorStatus INT NOT NULL
		,photoStatus INT NOT NULL 
		,adReviewStatus INT NOT NULL
		,pricingStatus INT NOT NULL 
		,postingStatus INT NOT NULL
		,daysPending INT NULL
		,stepsComplete INT NOT NULL
		,lowActivityFlag BIT NOT NULL
		,statusBucket INT NOT NULL

		,descriptionAlert BIT NOT NULL
		,photosAlert BIT NOT NULL
		,pricingAlert BIT NOT NULL
		,remerchandisingFlag BIT NOT NULL
		,NoPackages BIT NOT NULL
	)

	EXEC builder.VehicleData#Load @BusinessUnitId
	
select @sortExpression = case
WHEN @sortExpression like 'stockNumber%' OR 
	 @sortExpression like 'inventoryId%'
THEN 'inv.' + @sortExpression
ELSE @sortExpression
END
	   
declare @sql nvarchar(4000), @parmdef nvarchar(100)
set @parmdef = '@searchParm nvarchar(50)'
set @sql = '
select * FROM (
SELECT ROW_NUMBER() OVER(ORDER BY ' + @sortExpression + ') as RowNum,
Vin, 
inv.StockNumber, 
InventoryReceivedDate, 
vehicleYear, 
UnitCost, 
AcquisitionPrice, 
Certified, 
ISNULL(div.DivisionName, inv.Make) as make, 
ISNULL(mdl.ModelName,inv.model) as model, 
COALESCE(sty.trim, inv.VehicleTrim, '''') as trim, 
ISNULL(mkt.marketClass, '''') as marketClass,
ISNULL(mkt.marketClassId, 0) as marketClassId,
ISNULL(mkt.segmentId, 1) as segmentId,
inv.inventoryID,
ISNULL(vp.imageThumbnailUrl, '''') as thumbnailUrl, 
ISNULL(vp.photoCount, 0) as photoCount,
ISNULL(lotLocationId, 0) as lotLocationId,
MileageReceived, 
TradeOrPurchase, 
ListPrice, 
BaseColor,
inv.VehicleLocation,
InventoryStatusCD,
NULL as chromeStyleId,
ISNULL(oc.DescriptionSample,'''') as DescriptionSample,
oc.datePosted,
ISNULL(lowActivityFlag,0) as lowActivityFlag,
ISNULL(oc.exteriorStatus, 0) as exteriorStatus, 
ISNULL(oc.interiorStatus, 0) as interiorStatus,
ISNULL(oc.photoStatus, 0) as photoStatus, 
ISNULL(oc.adReviewStatus, 0) as adReviewStatus,
ISNULL(oc.pricingStatus, 0) as pricingStatus, 
ISNULL(oc.postingStatus, 0) as postingStatus,
ISNULL(oc.onlineFlag, 0) as onlineFlag,
ISNULL(oc.stepsComplete,0) as stepsComplete,
inv.InventoryType
 
    FROM #Inventory inv
    LEFT JOIN #VehicleDataAndStatus oc ON oc.inventoryId = inv.inventoryId AND oc.businessUnitID = inv.businessUnitID
    LEFT JOIN #Thumbnails vp ON inv.businessUnitID = vp.businessUnitID AND inv.inventoryId = vp.inventoryId
    LEFT JOIN vehicleCatalog.chrome.styles sty ON sty.styleID = oc.chromestyleid and sty.countrycode = 1                    
	LEFT JOIN vehicleCatalog.chrome.models mdl ON mdl.modelID = sty.modelID and mdl.countrycode = 1
	LEFT JOIN vehicleCatalog.FirstLook.MarketClass mkt ON mkt.marketClassID = sty.mktClassID
	LEFT JOIN vehicleCatalog.chrome.Divisions div ON div.divisionID = mdl.divisionID and div.countrycode = 1
	
    WHERE '
    + CASE WHEN @searchParameter = ''
		THEN ' 1 = 1 ' --end of dynamic sql to make and's work out
		--account for percents on search text via 19 for vin(17)
		ELSE ' (inv.StockNumber LIKE @searchParm ' +
				' OR inv.Make LIKE @searchParm ' + 
				' OR inv.Model LIKE @searchParm) '
	  END
	 + CASE WHEN len(@searchParameter) = 19
		THEN ' OR inv.Vin LIKE @searchParm '
		ELSE ''
	   END
	
+ ' ) as vehInfo
WHERE RowNum BETWEEN ' + CONVERT(nvarchar(10), @startRowIndex + 1) + 
			' AND ' + CONVERT(nvarchar(10), (@startRowIndex + @maximumRows))
			
EXEC sp_executesql @sql, @parmdef, @searchParm = @searchParameter
	
	DROP TABLE #Inventory
	DROP TABLE #Thumbnails
	DROP TABLE #VehicleDataAndStatus
END
GO

GRANT EXECUTE ON [builder].[Inventory#Search] TO MerchandisingUser 
GO