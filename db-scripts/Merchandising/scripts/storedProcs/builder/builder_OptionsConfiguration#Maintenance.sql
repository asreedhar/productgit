IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'builder.OptionsConfiguration#Maintenance')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE builder.OptionsConfiguration#Maintenance
GO

create procedure builder.OptionsConfiguration#Maintenance
as

set nocount on
set transaction isolation level read uncommitted

declare @Member varchar(30)
set @Member = '#OcMaintenance'

exec builder.InsertMissingOptionsConfigurationRecords @MemberLogin = @Member
exec workflow.UpdateOptionsConfigurationChromeStyleIdField @MemberLogin = @Member
exec workflow.UpdateOptionsConfigurationNoPackagesField @MemberLogin = @Member

-- set EngineTypeCategoryID on builder.NewEquipmentMapping FB: 31847
set @Member = '#nemMaintenance'

exec merchandising.Builder.NewEquipmentMapping#AutoSetEngineTypeCategoryID @MemberLogin = @Member

-- set TransmittionType, FuleType and Drive Train on builder.NewEquipmentMapping FB: 32061

exec merchandising.Builder.NewEquipmentMapping#AutoSetTransmissionTypeCategoryID  @MemberLogin = @Member
exec merchandising.Builder.NewEquipmentMapping#AutoSetFuelTypeCategoryID @MemberLogin = @Member
exec merchandising.Builder.NewEquipmentMapping#AutoSetDriveTrainCategoryID @MemberLogin = @Member

-- second shot at transmission, just looking for 'automatic' or 'manual' if something more specific not found above
exec merchandising.Builder.NewEquipmentMapping#AutoSetTransmissionType @MemberLogin = @Member
GO

GRANT EXECUTE ON builder.OptionsConfiguration#Maintenance TO MerchandisingUser 
GO

