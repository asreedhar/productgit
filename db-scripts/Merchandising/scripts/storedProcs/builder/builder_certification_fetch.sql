if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[Certification#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[Certification#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* --------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * fetch the business unit configured certification item
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[Certification#Fetch]
	
	@BusinessUnitId int,
	@InventoryId int,
	@MakeName varchar(50)
	-- I could just as easily acquire Make from the DB, but I'd rather just take it as a parameter.
	
	
AS
BEGIN
	set nocount on;

	declare @CertificationPreviewText varchar(300)

	-- get the ownerid for this dealer	
	declare @ownerId int
	select @ownerId = OwnerId 
	from Market.Pricing.Owner 
	where OwnerTypeID = 1 and OwnerEntityID = @BusinessUnitId
	
	declare @ProgramIdToUse int, @MfrSpecified bit, @ProgramPersisted bit
	exec imt.Certified.GetValidatedProgramId @BusinessUnitId, @InventoryId, @MakeName, @ValidatedProgramId = @ProgramIdToUse output, @IsManfacturerSpecified = @MfrSpecified output, @ProgramIsPersisted = @ProgramPersisted output
	
	
	
	-- result set 1 of 2
	-- TODO: add new column for OCP
	select @ProgramIdToUse as ProgramId, coalesce(@CertificationPreviewText, 'DEFAULT_CERTIFICATION_PREVIEW_TEXT') as CertificationPreviewText, @MfrSpecified as ReportedByManufacturer, @ProgramPersisted as ProgramPersisted, cp.MarketingName as MarketingName
	from imt.Certified.CertifiedProgram cp
	left join imt.Certified.OwnerCertifiedProgram ocp on ocp.CertifiedProgramId = cp.CertifiedProgramId and ocp.OwnerId = @ownerId
	where cp.CertifiedProgramID = @ProgramIdToUse
	
	-- result set 2 of 2
	select 
		cpb.Name as BenefitName
		,cpb.Text as BenefitText
		,coalesce(ocpb.IsProgramHighlight, cpb.IsProgramHighlight) as BenefitDisplay
	from imt.Certified.CertifiedProgram cp
	join imt.Certified.CertifiedProgramBenefit cpb 
		on cp.CertifiedProgramID = cpb.CertifiedProgramID
	left join imt.Certified.OwnerCertifiedProgram ocp 
		on cp.CertifiedProgramID = ocp.CertifiedProgramID 
		and ocp.OwnerId = @ownerId
	left join imt.Certified.OwnerCertifiedProgramBenefit ocpb
		on ocp.OwnerCertifiedProgramID = ocpb.OwnerCertifiedProgramID
		and cpb.CertifiedProgramBenefitID = ocpb.CertifiedProgramBenefitID
	where cp.CertifiedProgramID = @ProgramIdToUse
	order by coalesce(ocpb.Rank, cpb.Rank)		
			
END
GO

GRANT EXECUTE ON [builder].[Certification#Fetch] TO MerchandisingUser 
GO