if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[HighInternetPrice#Select]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[HighInternetPrice#Select]
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/12/09>
-- Description:	Return the high price for a vehicle from its pricing history
-- =============================================
CREATE PROCEDURE [builder].[HighInternetPrice#Select] 
	@InventoryId int   
AS
SET NOCOUNT ON
BEGIN
   
	declare @HighInternetPrice decimal
	EXEC Interface.HighInternetPrice#Fetch @InventoryId, @HighInternetPrice output
	select @HighInternetPrice as HighInternetPrice
  
END

GO

GRANT EXECUTE ON [builder].[HighInternetPrice#Select] TO MerchandisingUser
GO