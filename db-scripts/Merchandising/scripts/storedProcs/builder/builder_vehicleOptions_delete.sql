if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleOptions#Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[VehicleOptions#Delete]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_vehicleOptions_delete.sql,v 1.1 2008/12/11 19:02:32 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * delete existing condition values for vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - integer id of businessunit
 * @InventoryId - integer id of inventory item
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[VehicleOptions#Delete]
	
	@BusinessUnitId int,
	@InventoryId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	DELETE FROM builder.vehicleOptions 
		WHERE businessunitid = @BusinessUnitID 
		AND InventoryId = @InventoryId
			
END
GO

GRANT EXECUTE ON [builder].[VehicleOptions#Delete] TO MerchandisingUser 
GO