if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleOptionsMapped#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[VehicleOptionsMapped#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * fetch the mapped options for a vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - needed for rankings data
 * @InventoryId - inventoryId of vehicle
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[VehicleOptionsMapped#Fetch]
	@BusinessUnitId int,
	@InventoryId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	declare @rankings table (
		categoryID int,
		tierNumber int,
		displayPriority int,
		displayDescription varchar(200)
	)		
	declare @lotProviderId int
	
	IF EXISTS (SELECT 1 FROM settings.EquipmentDisplayRankings 
				WHERE businessUnitId = @BusinessUnitId)
	BEGIN
		INSERT INTO @rankings (categoryID, tierNumber, displayPriority,displayDescription)
			SELECT categoryId, tierNumber, displayPriority,displayDescription
			FROM settings.EquipmentDisplayRankings 
			WHERE businessUnitId = @BusinessUnitId
	END
	ELSE
	BEGIN
		INSERT INTO @rankings (categoryID, tierNumber, displayPriority,displayDescription)
			SELECT categoryId, tierNumber, displayPriority,displayDescription
			FROM settings.EquipmentDisplayRankings 
			WHERE businessUnitId = 100150
	END
	
	INSERT INTO @rankings (categoryId, tierNumber, displayPriority, displayDescription)
	select categoryId, 4, 100000, userFriendlyName
	FROM vehiclecatalog.chrome.categories
	where categoryId not in (SELECT categoryId
				FROM @rankings)
	and countryCode = 1
	
	--select the lotProviderId
	select @lotProviderId = LotProviderId 
	FROM settings.merchandising 
	WHERE businessUnitId = @BusinessUnitId

	declare @Vin varchar(17)
	select @Vin = VIN
	FROM FLDW.dbo.InventoryActive
	WHERE BusinessUnitID = @BusinessUnitId
	AND InventoryID = @InventoryId

	SELECT DISTINCT om.chromeCategoryId as categoryId,
		'' as optionCode, 
		'' as detailText,
		'' as optionText,
		ct.CategoryTypeFilter, 
		r.displayDescription as userFriendlyName, --replaced with the dealer custom text
		ch.CategoryHeaderID, 
		ch.CategoryHeader, 
		'C' as [state],
		r.tierNumber, 
		r.displayPriority,
		CAST(0 as bit) as isStandardEquipment,
		om.description
	FROM
	( SELECT 
		ol.optionID
		,ol.Description
	FROM 
	
	Listing.Vehicle ve 
	INNER JOIN Listing.VehicleOption op 
		ON ve.vehicleid = op.vehicleid
	INNER JOIN Listing.OptionsList ol 
		ON ol.optionid = op.optionid
	WHERE 
		ve.VIN = @Vin
		AND (ve.providerId = @lotProviderId)
	) as mp
	INNER JOIN Merchandising.OptionsMapping om
	ON om.optionid = mp.OptionID
	LEFT JOIN vehicleCatalog.Chrome.Categories AS ct
		ON ct.categoryID = om.chromeCategoryId
		and ct.countrycode = 1
	JOIN vehicleCatalog.Chrome.CategoryHeaders AS ch 
		ON ch.CategoryHeaderID = ct.CategoryHeaderID 
		AND ch.countrycode = 1
	LEFT JOIN @rankings r 
		ON r.categoryId = ct.categoryId
	
			
END
GO

GRANT EXECUTE ON [builder].[VehicleOptionsMapped#Fetch] TO MerchandisingUser 
GO