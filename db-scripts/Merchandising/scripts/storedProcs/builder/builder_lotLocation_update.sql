if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[LotLocation#Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[LotLocation#Update]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_lotLocation_update.sql,v 1.1 2008/12/09 18:24:16 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * update the lot locations for a given business unit
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - integer id of businessunit
 * @InventoryId - inventory item id
 * @LotLocationId - id of the new lot location
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[LotLocation#Update]
	
	@BusinessUnitId int,
	@InventoryId int,
	@LotLocationId int

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE builder.optionsConfiguration
	SET LotLocationId = @LotLocationId
	WHERE businessUnitId = @BusinessUnitID
	AND inventoryId = @InventoryId
	
			
END
GO

GRANT EXECUTE ON [builder].[LotLocation#Update] TO MerchandisingUser 
GO