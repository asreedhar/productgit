if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[dealers#fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[dealers#fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_dealers_fetch.sql,v 1.1 2009/01/05 21:23:51 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch dealers
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[dealers#fetch]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	CREATE TABLE #Dealers (
		BusinessUnitID int NOT NULL,
		BusinessUnit varchar(40) NOT NULL,
		BusinessUnitTypeID int NOT NULL
	)
	
	insert into #Dealers exec interface.dealers#fetch
	
	SELECT * FROM #Dealers 
	ORDER BY BusinessUnit ASC
	
	DROP TABLE #Dealers
	
END
GO

GRANT EXECUTE ON [builder].[dealers#fetch] TO MerchandisingUser 
GO