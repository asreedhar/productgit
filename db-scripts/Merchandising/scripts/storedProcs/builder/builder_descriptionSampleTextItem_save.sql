if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[DescriptionSampleTextItem#Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[DescriptionSampleTextItem#Save]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <1/5/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_descriptionSampleTextItem_save.sql,v 1.1 2009/11/09 17:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * create a record for the contents of a description
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - integer id of businessunit
 * @InventoryId - integer id of the inventory item
 * @DescriptionItemTypeId - integer id of description item
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[DescriptionSampleTextItem#Save]
	
	@BusinessUnitId int,
	@InventoryId int,
	@BlurbTextId int,
	@TextSequence int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


INSERT INTO builder.Description_SampleTextItems
([businessUnitId],
	[inventoryId],
	[blurbTextId],
	[textSequence]
	)
VALUES  (@BusinessUnitId,
		@InventoryId,
		@BlurbTextId,
		@TextSequence)
		
END
GO

GRANT EXECUTE ON [builder].[DescriptionSampleTextItem#Save] TO MerchandisingUser 
GO