if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[setSelectedTemplate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[setSelectedTemplate]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_setSelectedTemplate.sql,v 1.5 2009/10/06 14:40:52 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * sets the seleted template for the vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit 
 * @InventoryId - inventory id of vehicle
 * @TemplateID - id of template to use for vehicle
 *

 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[setSelectedTemplate]
	
	@BusinessUnitID int,
	@InventoryId int,
	@TemplateID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE builder.Descriptions
		SET templateID = @TemplateID
		WHERE businessUnitID = @BusinessUnitID 
			AND inventoryId = @InventoryId

END
GO

GRANT EXECUTE ON [builder].[setSelectedTemplate] TO MerchandisingUser 
GO