if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[CertificationFeature#Deactivate]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[CertificationFeature#Deactivate]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * Summary
 * -------
 * 
 * deactivate a certification feature
 * 
 * 
 * Parameters
 * ----------
 * 
 * @CertificationFeatureId - certificationfeatureid to update
 * 
 * Exceptions
 * ----------
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[CertificationFeature#Deactivate]
	@CertificationFeatureId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF EXISTS (SELECT 1 FROM builder.CertificationFeatures WHERE certificationFeatureId = @CertificationFeatureId)
	BEGIN
		UPDATE builder.CertificationFeatures
		SET active = 0
		WHERE certificationFeatureId = @CertificationFeatureId
	END
END
GO

GRANT EXECUTE ON [builder].[CertificationFeature#Deactivate] TO MerchandisingUser 
GO