
/****** Object:  StoredProcedure [builder].[getPriceComparisonsAndDealerThresholds]    Script Date: 07/01/2015 05:16:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[builder].[getPriceComparisonsAndDealerThresholds]') AND type in (N'P', N'PC'))
DROP PROCEDURE [builder].[getPriceComparisonsAndDealerThresholds]
GO



/****** Object:  StoredProcedure [builder].[getPriceComparisonsAndDealerThresholds]    Script Date: 07/01/2015 05:16:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_getInventoryItems.sql,v 1.14 2010/08/19 21:24:39 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * gets all inventory items that meet the input var filter criteria
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list

 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE   [builder].[getPriceComparisonsAndDealerThresholds]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@InventoryType int = 2

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


Declare  @DollarVariancePref TABLE 
(PriceProviderID INT,
PriceProviderDesc VARCHAR(50),
MinimumDollarVariance INT)
 
 
 
 
 
INSERT INTO @DollarVariancePref(PriceProviderID,PriceProviderDesc,MinimumDollarVariance)
SELECT
         pp.PriceProviderID,
         pp.Description as ProviderDescription,
        opp.MinimumDollarVariance
       FROM IMT.Marketing.MarketAnalysisOwnerPriceProvider opp
     INNER JOIN IMT.Marketing.MarketAnalysisPriceProvider pp
         ON pp.PriceProviderID = opp.PriceProviderID 
         join Market.Pricing.Owner MPO on opp.OwnerId = MPO.OwnerID
         where MPO.OwnerEntityID = @BusinessUnitID and pp.PriceProviderId in (3,4,6,7,8)
         
 --SELECT * FROM #DollarVariancePref   
 
 DECLARE @OriginalMSRP INT, @MarketInternetPrice INT,
 @EdmundsTMV INT,@NADARetailValue INT,@KBBValue INT,
 @ThresholdProofPoint INT,@AlertThreshold INT, @ComparisonColor INT, @OwnerHandle VARCHAR(36)
 
 SELECT @OriginalMSRP = (SELECT MinimumDollarVariance FROM @DollarVariancePref where PriceProviderId = 3 )
 
 SELECT @MarketInternetPrice = (SELECT MinimumDollarVariance FROM @DollarVariancePref where PriceProviderId = 4 )
 
 SELECT @EdmundsTMV = (SELECT MinimumDollarVariance FROM @DollarVariancePref where PriceProviderId = 6 )
 
 SELECT @NADARetailValue = (SELECT MinimumDollarVariance FROM @DollarVariancePref where PriceProviderId = 7 )
 
 SELECT @KBBValue = (SELECT MinimumDollarVariance FROM @DollarVariancePref where PriceProviderId = 8 )
 
 SELECT @OwnerHandle=(SELECT Handle FROM Market.Pricing.Owner WHERE OwnerEntityID=@BusinessUnitID)
 
 SELECT @ThresholdProofPoint = (SELECT Threshold_ProofPoint from Merchandising.settings.merchandisingDescriptionPreferences where businessUnitId=@BusinessUnitID);
 
 CREATE TABLE #Temp(InventoryID INT, ComparisonColor INT)
 INSERT INTO #Temp EXEC [Market].[Pricing].GetPriceComparisonColor @OwnerHandle;

with Defaults as 
 (
  select BusinessUnitId = @BusinessUnitId, Bucket, active, Limit, LimitTypeId, emailActive -- FB: 27993 
  from Merchandising.settings.BucketAlertThresholdDefault where Bucket=30
 )
 
 select @AlertThreshold = coalesce(
   bat.Limit,def.Limit)
 from Defaults def
 full join Merchandising.settings.BucketAlertsThreshold bat
  on def.BusinessUnitId = bat.BusinessUnitId 
  and bat.Bucket = def.Bucket
 where bat.BusinessUnitId = @BusinessUnitId or def.BusinessUnitId = @BusinessUnitId;
 
 
if @inventoryType = 0
begin
select 
PC.InventoryID as InventoryID,
Convert(INT,coalesce(PC.ListPrice,0)) as ListPrice,
Convert(INT,coalesce(PC.MSRP,0)) as MSRP,
Convert(INT,coalesce(PC.NADA,0)) as NADA,
Convert(INT,coalesce(PC.KBB,0)) as KBB,
Convert(INT,coalesce(PC.PingMarketAvg,0)) as PingMarketAvg,
Convert(INT,coalesce(Pc.EdmundsTMV,0)) as EdmundsTMV,
Convert(INT,coalesce(Pc.KbbConsumerValue,0)) as KbbConsumerValue,
ISNULL(@OriginalMSRP,0) as MSRPDollarT,
ISNULL(@MarketInternetPrice,0) as MIPDollarT,
ISNULL(@KBBValue,0) as KbbDollarT,
ISNULL(@NADARetailValue,0) as NadaDollarT,
ISNULL(@EdmundsTMV,0) as EdmundsDollarT,
ISNULL(T.ComparisonColor,0)as ComparisonColor,
ISNULL(@ThresholdProofPoint,0) as ThresholdProofPoint,
ISNULL(@AlertThreshold,0) as AlertThreshold
from [Market].Pricing.priceComparisons PC
JOIN #Temp T ON T.InventoryID=PC.InventoryID
Where BusinessUnitId=@BusinessUnitID

END

Else

Begin

select 
PC.InventoryID as InventoryID,
Convert(INT,coalesce(PC.ListPrice,0)) as ListPrice,
Convert(INT,coalesce(PC.MSRP,0)) as MSRP,
Convert(INT,coalesce(PC.NADA,0)) as NADA,
Convert(INT,coalesce(PC.KBB,0)) as KBB,
Convert(INT,coalesce(PC.PingMarketAvg,0)) as PingMarketAvg,
Convert(INT,coalesce(Pc.EdmundsTMV,0)) as EdmundsTMV,
Convert(INT,coalesce(Pc.KbbConsumerValue,0)) as KbbConsumerValue,
ISNULL(@OriginalMSRP,0) as MSRPDollarT,
ISNULL(@MarketInternetPrice,0) as MIPDollarT,
ISNULL(@KBBValue,0) as KbbDollarT,
ISNULL(@NADARetailValue,0) as NadaDollarT,
ISNULL(@EdmundsTMV,0) as EdmundsDollarT,
ISNULL(T.ComparisonColor,0)as ComparisonColor,
ISNULL(@ThresholdProofPoint,0) as ThresholdProofPoint,
ISNULL(@AlertThreshold,0) as AlertThreshold 
from [Market].Pricing.priceComparisons PC
JOIN #Temp T ON T.InventoryID=PC.InventoryID
Where BusinessUnitId=@BusinessUnitID
and InventoryType=@InventoryType

End

DROP TABLE #Temp
END

GO

GRANT EXEC ON builder.getPriceComparisonsAndDealerThresholds to MerchandisingWebsite
GO


