if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleConfiguration#Create]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[VehicleConfiguration#Create]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId - integer inventory id
 * @MemberLogin
 * @StockNumber - stockNumber to use if available
 */

CREATE PROCEDURE [builder].[VehicleConfiguration#Create]
	@BusinessUnitId int,
	@InventoryId int,
	@MemberLogin varchar(30),
	@StockNumber varchar(15) = ''
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	set transaction isolation level read uncommitted
	
	exec Merchandising.builder.InsertMissingOptionsConfigurationRecords @MemberLogin = @MemberLogin,
		@BusinessUnitID = @BusinessUnitID, @InventoryID = @InventoryID
		
	if @StockNumber <> ''
	begin
		update Merchandising.builder.OptionsConfiguration 
			set StockNumber = @StockNumber
			where BusinessUnitID = @BusinessUnitID
				and InventoryID = @InventoryID
	end

END
GO

GRANT EXECUTE ON [builder].[VehicleConfiguration#Create] TO MerchandisingUser 
GO