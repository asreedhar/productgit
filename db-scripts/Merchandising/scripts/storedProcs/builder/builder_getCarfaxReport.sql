if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[getCarfaxReport]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[getCarfaxReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_getCarfaxReport.sql,v 1.3 2008/11/20 20:01:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * get the saved carfax report
 * 
 * 
 * Parameters
 * ----------
 * 
 * @VIN     - VIN of the vehicle
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[getCarfaxReport]
	
	@VIN varchar(17)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT * from merchandising.carfaxReport WHERE vin = @VIN
	

END
GO

GRANT EXECUTE ON [builder].[getCarfaxReport] TO MerchandisingUser 
GO