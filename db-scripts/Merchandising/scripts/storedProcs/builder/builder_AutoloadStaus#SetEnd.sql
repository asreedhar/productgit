if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[AutoloadStatus#SetEnd]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[AutoloadStatus#SetEnd]
GO
/****** Object:  StoredProcedure [builder].[AutoloadStatus#SetEnd]    Script Date: 12/28/2011 11:42:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [builder].[AutoloadStatus#SetEnd]
	@BusinessUnitID int,
	@InventoryID int,
	@StatusTypeId int, 
	@MemberLogin varchar(30)
	
AS
BEGIN


	-- check if limit has been reached for this autoload status from build request log
	DECLARE @substituteResultStatusId int
	
	SELECT @substituteResultStatusId = BuildRequestLimit.resultStatusId
		FROM settings.BuildRequestLimit
		INNER JOIN (
						SELECT TOP 1 siteID, statusTypeID, requestCount = COUNT(*), lastRequest = MAX(requestedOn)
							FROM builder.BuildRequestLog
							INNER JOIN workflow.Inventory ON BuildRequestLog.businessUnitId = Inventory.BusinessUnitID
															AND BuildRequestLog.vin = Inventory.vin
							WHERE Inventory.BusinessUnitID = @BusinessUnitID
							AND Inventory.InventoryID = @InventoryID
							GROUP BY siteID, statusTypeID
							ORDER BY MAX(requestedOn) DESC -- want the last build request totals only
					)
			 totalOfLastRequest ON BuildRequestLimit.siteId = totalOfLastRequest.siteId
															AND BuildRequestLimit.statusTypeId = totalOfLastRequest.statusTypeId
		WHERE totalOfLastRequest.statusTypeId = @StatusTypeId -- only if the type we are saving to autoload status is same as last request (ie. if successful we dont give a s**$)
		AND totalOfLastRequest.requestCount >= BuildRequestLimit.limit

	if @substituteResultStatusId IS NOT NULL 
		SET @StatusTypeId = @substituteResultStatusId -- we found a substitue status, replace what is going to be saved
	
			
	IF EXISTS( SELECT 1 FROM builder.AutoloadStatus WHERE BusinessUnitID = @BusinessUnitID AND InventoryID = @InventoryID )
		begin
			
			UPDATE builder.AutoloadStatus
				SET EndTime = GETDATE(),
					LastUpdatedOn = GETDATE(),
					LastUpdatedBy = @MemberLogin,
					statusTypeId = @StatusTypeId
				WHERE BusinessUnitID = @BusinessUnitID 
				AND InventoryID = @InventoryID
		end
	else
		begin
			INSERT INTO [builder].[AutoloadStatus] 
				(
					businessUnitId, 
					inventoryId, 
					statusTypeId, 
					createdOn, 
					createdBy, 
					lastUpdatedOn, 
					lastUpdatedBy,
					startTime, 
					endTime
				)
				VALUES 
				(
					@BusinessUnitId, 
					@InventoryId, 
					@StatusTypeId, 
					GETDATE(), 
					@MemberLogin, 
					GETDATE(), 
					@MemberLogin,	
					GETDATE(), 
					GETDATE()
				)
		
		end
			
			
						
			
END
GO
GRANT EXECUTE ON [builder].[AutoloadStatus#SetEnd] TO MerchandisingUser 
GO