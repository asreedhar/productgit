USE [Merchandising]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[AutoloadStatus#FetchByBUAndStatusId]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[AutoloadStatus#FetchByBUAndStatusId]
GO
/****** Object:  StoredProcedure [builder].[AutoloadStatus#FetchAllWithErrorsForBU]    Script Date: 12/28/2011 11:42:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [builder].[AutoloadStatus#FetchByBUAndStatusId]
	
	@BusinessUnitID int,
	@StatusTypeId int
	
AS
BEGIN

	select inventoryId from [builder].[AutoloadStatus] where businessUnitId = @businessUnitId and statusTypeId  = @StatusTypeId
			
END
GO
GRANT EXECUTE ON [builder].[AutoloadStatus#FetchByBUAndStatusId]TO MerchandisingUser 
GO