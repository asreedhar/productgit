if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[detailedOptions#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[detailedOptions#Fetch]
GO


/****** Object:  StoredProcedure [builder].[detailedOptions#Fetch]    Script Date: 11/02/2008 18:10:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_detailedOptions_fetch.sql,v 1.1 2009/04/27 16:15:54 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Fetch the vehicle's detailed options selections
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId - integer inventory id
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[detailedOptions#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@InventoryId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
--------------------------------------
--FETCH SELECTED VEHICLE DETAILED EQUIPMENT
--------------------------------------	
		select 
			ISNULL(optionCode, '') as optionCode, 
			ISNULL(detailText, '') as detailText,
			optionText,
			categoryList,
			isStandardEquipment

			FROM builder.vehicleDetailedOptions vo
			
			WHERE businessunitid = @BusinessUnitID 
				AND inventoryId = @InventoryId
	
END
GO

GRANT EXECUTE ON [builder].[detailedOptions#Fetch] TO MerchandisingUser 
GO