if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[LoaderNote#Create]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[LoaderNote#Create]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_loaderNote_create.sql,v 1.1 2009/01/05 21:23:51 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * create a new loader note attached to a vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId- vehicle inventory id
 * @MemberLogin - firstlook login for member who is updating the status
 * @NoteText - text of the note to save
 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[LoaderNote#Create]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@InventoryId int,
	@MemberLogin varchar(30),
	@NoteText varchar(500)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT INTO builder.LoaderNotes
	(businessUnitId, inventoryId, noteText,
	writtenBy, writtenOn)
	VALUES (
	@BusinessUnitId, @InventoryId, @NoteText,
	@MemberLogin, getdate()
	)
END
GO

GRANT EXECUTE ON [builder].[LoaderNote#Create] TO MerchandisingUser 
GO