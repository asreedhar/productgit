if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleConfiguration#Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[VehicleConfiguration#Save] 
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_vehicleConfiguration_save.sql,v 1.9 2009/10/06 14:40:52 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * save a vehicle configuration
 * 
 * 
 * Parameters
 * ----------
 * 
	@BusinessUnitId int,
	@InventoryId int,
	@chromeStyleId int,
	@afterMarketCodeList varchar(500),
	@extColor1 varchar(100),
	@extColor2 varchar(100),
	@intColor varchar(100),
	@AfterMarketTrim varchar(100),
	@Condition varchar(50),
	@MerchandisingDescription varchar(max),
	@ExpiresOn datetime = NULL - the date on which the configuration (and description in particular) expires
	@IncludeVideo bit,
	@AdditionalInfoText varchar(300),	
	@SpecialId int,
	@TireTread int,
	@ReconditioningText varchar(250)
	@ReconditioningValue decimal
	
	@MemberLogin varchar(50)
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[VehicleConfiguration#Save] 
	
	@BusinessUnitId int,
	@InventoryId int,
	@chromeStyleId int,
	@afterMarketCodeList varchar(500),
	@extColor1 varchar(100),
	@extColor2 varchar(100),
	@intColor varchar(100),
	@AfterMarketTrim varchar(100),
	@Condition varchar(50),
	@AdditionalInfoText varchar(300),
	@SpecialId int,
	@TireTread int,
	@ReconditioningText varchar(250),
	@ReconditioningValue decimal,
	@Version timestamp,
	@MemberLogin varchar(30),
	@ExteriorColorCode varchar(10),
	@ExteriorColorCode2 varchar(10),
	@InteriorColorCode varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS (SELECT 1 
		FROM builder.OptionsConfiguration
		WHERE businessUnitId = @BusinessUnitId
		    AND inventoryId = @InventoryId)
	BEGIN
		RAISERROR('This vehicle record does not exist or may have been deleted',16,1)
		RETURN @@ERROR
	END
		
		UPDATE builder.OptionsConfiguration
            SET chromeStyleID = @chromeStyleID,
            afterMarketCategoryString = @afterMarketCodeList,
            extColor1 = @extColor1, extColor2 = @extColor2, intColor = @intColor, 
            afterMarketTrim = @AfterMarketTrim, 
            vehicleCondition = @Condition, 
            tireTread = @TireTread,
            additionalInfoText = @AdditionalInfoText, 
            specialID = @SpecialId,
            inventoryId = @InventoryID,
            reconditioningText = @ReconditioningText,
			reconditioningValue = @ReconditioningValue,
			exteriorColorCode = @ExteriorColorCode,
			exteriorColorCode2 = @ExteriorColorCode2,
			interiorColorCode = @InteriorColorCode,			
            lastUpdatedOn = getdate(),
            lastUpdatedBy = @MemberLogin
            
        WHERE BusinessUnitID = @BusinessUnitID AND InventoryId = @InventoryId
			
END
GO

GRANT EXECUTE ON [builder].[VehicleConfiguration#Save] TO MerchandisingUser 
GO