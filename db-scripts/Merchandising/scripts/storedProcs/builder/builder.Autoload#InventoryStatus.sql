IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[builder].[Autoload#InventoryStatus]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [builder].[Autoload#InventoryStatus]
GO

CREATE PROC builder.Autoload#InventoryStatus @businessUnitID int, @manufactureId int = null
AS
	SET NOCOUNT ON
	
	-- if manufacture is null ... just return the manufacturer / credential information
	if(@manufactureId IS NULL)
		begin
			SELECT BusinessUnit.businessUnit, Manufacturers.ManufacturerID, manufacturers.ManufacturerName, credentialedSites.siteName, 
					Dealer_SiteCredentials.username, encryptedPassword, isLockedOut, DealerCode
				FROM settings.Dealer_SiteCredentials
				INNER JOIN merchandising.vehicleAutoloadSettings ON Dealer_SiteCredentials.siteId = VehicleAutoLoadSettings.siteId
				INNER JOIN VehicleCatalog.Chrome.Manufacturers	ON VehicleAutoLoadSettings.ManufacturerID = manufacturers.ManufacturerID
																		AND manufacturers.CountryCode = 1
				INNER JOIN settings.CredentialedSites ON Dealer_SiteCredentials.siteId = CredentialedSites.siteId
				INNER JOIN IMT.dbo.BusinessUnit ON Dealer_SiteCredentials.businessUnitId = businessUnit.BusinessUnitID
				WHERE Dealer_SiteCredentials.businessUnitId = @businessUnitID
				
				return
	
		end
	
	
	
	-- show status
	SELECT inventory.BusinessUnitID, businessUnit.BusinessUnit, inventory.InventoryID, vin, vehicleYear, Make, model, autoloadStatus.statusTypeId, AutoloadStatus.startTime, AutoloadStatus.endTime, AutoloadStatus.lastUpdatedBy, AutoloadStatus.lastUpdatedOn, AutoloadStatus.createdBy
		FROM builder.autoLoadStatus 
		INNER JOIN workflow.inventory ON Inventory.BusinessUnitID = autoloadStatus.businessUnitID
												AND Inventory.InventoryID = autoloadStatus.InventoryID
		INNER JOIN imt.dbo.BusinessUnit ON inventory.BusinessUnitID = businessUnit.BusinessUnitID
		INNER JOIN VehicleCatalog.Chrome.Divisions ON Divisions.CountryCode = 1
													AND Divisions.ManufacturerID = @manufactureId
													AND inventory.Make = Divisions.DivisionName
		WHERE inventory.businessUnitID = @BusinessUnitID
		ORDER BY startTime DESC
	
	
	-- show style and colors
	SELECT vin, inventory.BusinessUnitID, inventory.InventoryID, 
			OptionsConfiguration.chromeStyleID, 
			OptionsConfiguration.ExteriorColorCode, OptionsConfiguration.extColor1, 
			OptionsConfiguration.ExteriorColorCode2, OptionsConfiguration.extColor2, 
			OptionsConfiguration.InteriorColorCode, OptionsConfiguration.intColor,
			OptionsConfiguration.lastUpdatedBy, OptionsConfiguration.lastUpdatedOn
		FROM builder.OptionsConfiguration
		INNER JOIN workflow.inventory ON Inventory.BusinessUnitID = OptionsConfiguration.businessUnitID
												AND Inventory.InventoryID = OptionsConfiguration.InventoryID
		INNER JOIN VehicleCatalog.Chrome.Divisions ON Divisions.CountryCode = 1
													AND Divisions.ManufacturerID = @manufactureId
													AND inventory.Make = Divisions.DivisionName
		WHERE inventory.businessUnitID = @BusinessUnitID  
		AND inventory.autoloadStatusTypeId IS NOT NULL
		ORDER BY vin
	
	-- show options
	SELECT vin, VehicleDetailedOptions.*
		FROM builder.VehicleDetailedOptions
		INNER JOIN workflow.inventory ON Inventory.BusinessUnitID = VehicleDetailedOptions.businessUnitID
												AND Inventory.InventoryID = VehicleDetailedOptions.InventoryID
		INNER JOIN imt.dbo.BusinessUnit ON inventory.BusinessUnitID = businessUnit.BusinessUnitID
		INNER JOIN VehicleCatalog.Chrome.Divisions ON Divisions.CountryCode = 1
													AND Divisions.ManufacturerID = @manufactureId
													AND inventory.Make = Divisions.DivisionName
		WHERE inventory.businessUnitID = @BusinessUnitID  
		AND inventory.autoloadStatusTypeId IS NOT NULL
		ORDER BY vin, VehicleDetailedOptions.optionCode
	
	-- show creds
	SELECT Manufacturers.ManufacturerID, manufacturers.ManufacturerName, credentialedSites.siteName, Dealer_SiteCredentials.*
		FROM settings.Dealer_SiteCredentials
		INNER JOIN merchandising.vehicleAutoloadSettings ON Dealer_SiteCredentials.siteId = VehicleAutoLoadSettings.siteId
		INNER JOIN VehicleCatalog.Chrome.Manufacturers	ON VehicleAutoLoadSettings.ManufacturerID = manufacturers.ManufacturerID
																AND manufacturers.CountryCode = 1
		INNER JOIN settings.CredentialedSites ON Dealer_SiteCredentials.siteId = CredentialedSites.siteId
		WHERE Dealer_SiteCredentials.businessUnitId = @businessUnitID
		AND VehicleAutoLoadSettings.ManufacturerID = @manufactureId
	
	
	SET NOCOUNT OFF
	
RETURN
GO

GRANT EXECUTE ON [builder].[Autoload#InventoryStatus] TO MerchandisingUser
go
