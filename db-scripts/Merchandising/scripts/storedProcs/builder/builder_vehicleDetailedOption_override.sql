if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[vehicleDetailedOption#Override]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[vehicleDetailedOption#Override]
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_vehicleDetailedOption_override.sql,v 1.1 2010/01/28 23:12:44 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Override the vehicle's detailed options selections for a businessUnit
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId - integer inventory id
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[vehicleDetailedOption#Override]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@OptionCode varchar(20),
	@IsStandard bit,
	@CustomTitle varchar(2000),
	@CustomBody varchar(2000),
	@ChromeStyleId int = NULL,
	@ModelId int = NULL,
	@ApplyToEntireModel bit = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
		if @ModelId IS NULL AND @ChromeStyleId IS NULL
		BEGIN
			RAISERROR('Either a modelId or a ChromeStyleId is required',16,1)
			return @@ERROR
		END
			
--------------------------------------
--FETCH SELECTED VEHICLE DETAILED EQUIPMENT
--------------------------------------	
--make sure we get a valid model id (note both model and style cannot be null)
if (@ModelId IS NULL)
BEGIN
	select @ModelId = modelId from vehiclecatalog.chrome.styles where styleId = @ChromeStyleId
END

--null the styleId if applying to entire model, and remove current items for that model
if (@ApplyToEntireModel = 1)
BEGIN
	select @ChromeStyleId = NULL
	declare @idsToDelete table (id int)
	insert into @idsToDelete (id) 
	SELECT ded.equipmentDescriptionId
	FROM 
	builder.DetailedEquipmentDescriptions ded
	JOIN builder.Vehicle_DetailedEquipmentDescriptions vded
		ON vded.equipmentDescriptionid = ded.equipmentDescriptionId
	WHERE optionCode = @OptionCode
	and ModelId = @ModelId
	and businessUnitId = @BusinessUnitId
	
	delete from 
	builder.Vehicle_DetailedEquipmentDescriptions
	where equipmentDescriptionid IN (select id from @idsToDelete)
	
	--deleting the entire model, so remove it if it was in the list
	delete from 
	builder.DetailedEquipmentDescriptions
	where equipmentDescriptionid IN (select id from @idsToDelete)
	
	exec builder.vehicleDetailedOption#Insert 
			@BusinessUnitID=@BusinessUnitID,
			@OptionCode=@OptionCode,
			@IsStandard=@IsStandard,
			@CustomTitle=@CustomTitle,
			@CustomBody=@CustomBody,
			@ChromeStyleId=@ChromeStyleId,
			@ModelId=@ModelId
		
END
ELSE
BEGIN
	if EXISTS (SELECT 1 FROM builder.DetailedEquipmentDescriptions ded
			JOIN builder.Vehicle_DetailedEquipmentDescriptions vded
				ON vded.equipmentDescriptionid = ded.equipmentDescriptionId
			WHERE optionCode = @OptionCode
			and ModelId = @ModelId
			AND (chromeStyleId = @ChromeStyleId)
			and businessUnitId = @BusinessUnitId
			)
	BEGIN
		UPDATE ded
		set equipmentTitle = @CustomTitle, 
			equipmentDescription = @CustomBody
		FROM builder.DetailedEquipmentDescriptions ded
		JOIN builder.Vehicle_DetailedEquipmentDescriptions vded
			ON vded.equipmentDescriptionid = ded.equipmentDescriptionId
		WHERE optionCode = @OptionCode
		and ModelId = @ModelId
		AND (chromeStyleId IS NULL OR chromeStyleId = @ChromeStyleId)
		and businessUnitId = @BusinessUnitId
	END
	ELSE 
	BEGIN
		exec builder.vehicleDetailedOption#Insert 
			@BusinessUnitID=@BusinessUnitID,
			@OptionCode=@OptionCode,
			@IsStandard=@IsStandard,
			@CustomTitle=@CustomTitle,
			@CustomBody=@CustomBody,
			@ChromeStyleId=@ChromeStyleId,
			@ModelId=@ModelId
		
	END
END
			
		
		
END
GO

GRANT EXECUTE ON [builder].[vehicleDetailedOption#Override] TO MerchandisingUser 
GO