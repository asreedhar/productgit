if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[DescriptionItem#Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[DescriptionItem#Save]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <1/5/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_descriptionItem_save.sql,v 1.1 2009/01/05 21:23:51 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * create a record for the contents of a description
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - integer id of businessunit
 * @InventoryId - integer id of the inventory item
 * @DescriptionItemTypeId - integer id of description item
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[DescriptionItem#Save]
	
	@BusinessUnitId int,
	@InventoryId int,
	@DescriptionItemTypeId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


INSERT INTO builder.descriptionContents
([businessUnitId],
	[inventoryId],
	[descriptionItemTypeId]
	)
VALUES  (@BusinessUnitId,
		@InventoryId,
		@DescriptionItemTypeId)
		
END
GO

GRANT EXECUTE ON [builder].[DescriptionItem#Save] TO MerchandisingUser 
GO