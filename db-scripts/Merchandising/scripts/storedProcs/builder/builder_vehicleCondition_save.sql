if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleCondition#Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[VehicleCondition#Save]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_vehicleCondition_save.sql,v 1.2 2008/12/11 19:23:38 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch the business unit configured certification item for 
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - integer id of businessunit
 * @InventoryId - integer id inventory item
 * @ConditionTypeId - vehicle condition type id
 * @ConditionLevel - integer value for condition levels
 * @Description - text description to use for condition
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[VehicleCondition#Save]
	
	@BusinessUnitId int,
	@InventoryId int,
	@ConditionTypeId int,
	@ConditionLevel int,
	@Description varchar(30)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
		
	IF not exists (select 1 
						from builder.vehicleConditions
							where businessunitid = @BusinessUnitID 
								and InventoryId = @InventoryId 
								and vehicleConditionTypeId = @ConditionTypeId)
        BEGIN
            INSERT INTO builder.VehicleConditions
			(businessUnitId, inventoryId, vehicleConditionTypeId, conditionLevel, description) 
			VALUES
			(@BusinessUnitId, @InventoryId, @ConditionTypeId, @ConditionLevel, @Description) 
        END
        ELSE
        BEGIN
            
            UPDATE builder.VehicleConditions
            SET
			
			conditionLevel = @ConditionLevel, 
			description = @Description
			WHERE businessUnitId = @BusinessUnitId AND
			inventoryId = @InventoryId AND 
			vehicleConditionTypeId = @ConditionTypeId
			
         
        END
			
END
GO

GRANT EXECUTE ON [builder].[VehicleCondition#Save] TO MerchandisingUser 
GO