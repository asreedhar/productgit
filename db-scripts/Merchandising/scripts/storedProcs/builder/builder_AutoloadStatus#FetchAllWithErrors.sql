
if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[AutoloadStatus#FetchAllWithErrors]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[AutoloadStatus#FetchAllWithErrors]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [builder].[AutoloadStatus#FetchAllWithErrors]
		
AS
BEGIN

	SELECT I.BusinessUnitID, I.InventoryID
	FROM Merchandising.Builder.AutoloadStatus ALS
	JOIN FLDW.dbo.InventoryActive I ON I.InventoryID = ALS.InventoryID
	WHERE ALS.StatusTypeID < 0
			
END
GO
GRANT EXECUTE ON [builder].[AutoloadStatus#FetchAllWithErrors] TO MerchandisingUser 
GO