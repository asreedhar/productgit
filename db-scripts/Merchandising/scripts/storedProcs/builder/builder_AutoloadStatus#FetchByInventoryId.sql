USE [Merchandising]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[AutoloadStatus#FetchByInventoryId]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[AutoloadStatus#FetchByInventoryId]
GO
/****** Object:  StoredProcedure [builder].[AutoloadStatus#FetchByInventoryId]    Script Date: 12/28/2011 11:42:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [builder].[AutoloadStatus#FetchByInventoryId]
	
	@BusinessUnitID int,
	@InventoryID int
	
AS
BEGIN

	select * from [builder].[AutoloadStatus] where businessUnitId = @businessUnitId and inventoryId = @inventoryId
			
END
GO
GRANT EXECUTE ON [builder].[AutoloadStatus#FetchByInventoryId] TO MerchandisingUser 
GO