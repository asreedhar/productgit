IF OBJECT_ID ( '[builder].[LotLoaded#Save]', 'P' ) IS NOT NULL 
DROP PROCEDURE [builder].[LotLoaded#Save]
GO

CREATE Procedure [builder].[LotLoaded#Save]
	@InventoryID int
AS

BEGIN

 IF NOT EXISTS (SELECT 1 FROM builder.LotLoaded WHERE InventoryID = @InventoryID)
    BEGIN
	    INSERT INTO builder.LotLoaded (InventoryID, CreatedOn, UpdatedOn)
		VALUES(@InventoryID, GETDATE(), GETDATE())
    END
 ELSE
	BEGIN
		UPDATE builder.LotLoaded
		SET UpdatedOn = GETDATE()
		WHERE InventoryID = @InventoryID  		
	END	
	
	
END
GO

GRANT EXECUTE ON [builder].[LotLoaded#Save] TO MerchandisingUser 
GO