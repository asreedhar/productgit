if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleConditionTypes#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[VehicleConditionTypes#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_vehicleConditionTypes_fetch.sql,v 1.1 2008/12/11 19:02:32 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch valid condition types
 * 
 * 
 * Parameters
 * ----------
 * @Category - category to select 0 - both, 1-exterior, 2-interior
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[VehicleConditionTypes#Fetch]
	@Category int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select vct.title, vct.vehicleConditionTypeId FROM builder.vehicleConditionTypes vct
		where @Category = 0 OR category = @Category
END
GO

GRANT EXECUTE ON [builder].[VehicleConditionTypes#Fetch] TO MerchandisingUser 
GO