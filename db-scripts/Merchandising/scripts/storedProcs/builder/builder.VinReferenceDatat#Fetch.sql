-- =============================================
-- Author:		Travis Huber
-- Create date: 2014-03-11
-- Description: Get a list of reference data 
--				by vin
-- =============================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'builder.VinReferenceData#Fetch') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE builder.VinReferenceData#Fetch
GO

CREATE PROCEDURE builder.VinReferenceData#Fetch	
	@Vin char(17)
AS
	SET NOCOUNT ON

	DECLARE @ChromeStyleId int;

	SELECT @ChromeStyleId = ChromeStyleId
		FROM Merchandising.workflow.Inventory wi 
		WHERE wi.Vin = @vin;

	SELECT 
		bs.StyleName AS BodyStyle, ci.Text AS CrashRating, tt.Title AS [MpgRating], ts.Text AS MpgValue
	FROM	VehicleCatalog.Chrome.Styles bs 
		JOIN VehicleCatalog.Chrome.ConsInfo ci ON bs.CountryCode = ci.CountryCode AND ci.StyleID = bs.StyleID and ci.TypeID = 4 -- Crash Test Results
		JOIN VehicleCatalog.Chrome.TechSpecs ts ON bs.CountryCode = ts.CountryCode AND ts.StyleID = bs.StyleID
		JOIN VehicleCatalog.Chrome.TechTitles tt ON ts.CountryCode = tt.CountryCode AND tt.TitleID = ts.TitleID AND tt.TechTitleHeaderID = 14 -- Mileage
	WHERE	bs.StyleID = @ChromeStyleId	
		AND bs.CountryCode = 1
		
	SELECT CAST(rating.SourceID AS INT) SourceID, CAST(rating.CircleRating AS REAL) CircleRating, src.[Description]
	FROM VehicleCatalog.Firstlook.VehicleCatalog_JDPowerRating rating
		INNER JOIN Merchandising.workflow.Inventory inventory ON inventory.VehicleCatalogId = rating.VehicleCatalogID
		INNER JOIN VehicleCatalog.Firstlook.JDPowerRatingSource src ON src.SourceID = rating.SourceID
	WHERE inventory.Vin = @Vin


GO

GRANT EXECUTE ON builder.VinReferenceData#Fetch TO MerchandisingUser
