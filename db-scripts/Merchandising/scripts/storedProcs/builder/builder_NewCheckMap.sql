if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[NewCheckMapping]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[NewCheckMapping]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Fitzpatrick, Benin>
-- Create date: <2/2/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_NewCheckMap.sql,v 1.1 2010/03/25 20:24:24 bfitzpatrick Exp $
 * 
 * Summary
 * -------
 * 
 * Update or insert any given set of Equipment to a vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @inventoryID
 * @businessUnitID - defaults to 
 * @EngineTypeCategoryID - 
 * @TransmissionTypeCategoryID - 
 * @DriveTrainCategoryID - 
 * @FuelCategoryID -
 * @createdBy	
 * @createdOn
 * @updatedBy
 * @updatedOn
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[NewCheckMapping]
	@inventoryID int,
	@businessUnitID int
AS
BEGIN

	DECLARE @Results TABLE (
		InventoryID INT NOT NULL,
		BusinessUnitID INT NOT NULL,
		EngineTypeCategoryID INT NULL,
		TransmissionTypeCategoryID INT NULL,
		DriveTrainCategoryID INT NULL,
		FuelCategoryID INT NULL
	)
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
		SELECT 
			InventoryID,
			BusinessUnitID,
			EngineTypeCategoryID,
			TransmissionTypeCategoryID,
			DriveTrainCategoryID,
			FuelCategoryID
		FROM builder.NewEquipmentMapping
		WHERE inventoryID = @inventoryID
		AND businessUnitID = @businessUnitID
END
GO

GRANT EXECUTE ON [builder].[NewCheckMapping] TO MerchandisingUser 
GO

   