IF OBJECT_ID('builder.NewCarPricing#Delete') IS NOT NULL
    DROP PROCEDURE builder.NewCarPricing#Delete
go


CREATE PROCEDURE builder.NewCarPricing#Delete
    @InventoryID INT
  , @SpecialPrice DECIMAL(9, 2)
  , @UserName VARCHAR(80) = SUSER_SNAME
  , @ProcessName VARCHAR(80) = SUSER_SNAME
AS
    BEGIN
	
        DELETE  FROM builder.NewCarPricing
        OUTPUT  'DELETE'
              , DELETED.[InventoryID]
              , DELETED.[SpecialPrice]
              , DELETED.[CreatedOn]
              , GETDATE()
              , DELETED.[CreatedBy]
              , @UserName
              , @ProcessName
                INTO [MerchandisingArchive].[builder].[NewCarPricingAudit]
        WHERE   InventoryID = @InventoryID
                AND SpecialPrice = @SpecialPrice
	
    END

GO
GRANT EXECUTE ON builder.NewCarPricing#Delete TO MerchandisingUser
go