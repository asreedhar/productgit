IF OBJECT_ID ( '[builder].[LotLoaded#Exists]', 'P' ) IS NOT NULL 
DROP PROCEDURE [builder].[LotLoaded#Exists]
GO

CREATE Procedure [builder].[LotLoaded#Exists]
	@InventoryID int
AS

BEGIN

	DECLARE @Exists BIT
    SET @Exists = 0     
	IF EXISTS ( SELECT 1 FROM builder.LotLoaded WHERE InventoryID = @InventoryID)
	SET @Exists = 1

    RETURN @Exists

END
GO

GRANT EXECUTE ON [builder].[LotLoaded#Exists] TO MerchandisingUser 
GO