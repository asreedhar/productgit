if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[DescriptionSampleTextItems#Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[DescriptionSampleTextItems#Delete]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <1/5/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_descriptionSampleTextItems_delete.sql,v 1.1 2009/11/09 17:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * deletes all description sample text items for a given inventory id
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId - integer id of businessunit
 * @InventoryId - integer id of the inventory item
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[DescriptionSampleTextItems#Delete]
	
	@BusinessUnitId int,
	@InventoryId int
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


DELETE FROM builder.Description_SampleTextItems
WHERE businessUnitId = @BusinessUnitId
	AND inventoryId = @InventoryId
		
END
GO

GRANT EXECUTE ON [builder].[DescriptionSampleTextItems#Delete] TO MerchandisingUser 
GO