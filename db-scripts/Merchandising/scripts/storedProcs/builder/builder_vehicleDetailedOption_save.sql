if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleDetailedOption#Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [builder].[VehicleDetailedOption#Save]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: builder_vehicleDetailedOption_save.sql,v 1.1 2009/02/02 21:12:13 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * save a detailed vehicle option
 * 
 * 
 * Parameters
 * ----------
 * 
	@BusinessUnitId int,
	@InventoryId int,
	@OptionCode varchar(20),
	@OptionText varchar(300),
	@DetailText varchar(800),  -- change from 300 to 800, tureen 9/23/2013
	@CategoryList varchar(300) --list of categories added/removed by option
	@IsStandardEquipment bit
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [builder].[VehicleDetailedOption#Save]
	
	@BusinessUnitId int,
	@InventoryId int,
	@OptionCode varchar(20),
	@OptionText varchar(300),
	@DetailText varchar(800),
	@CategoryList varchar(300),
	@IsStandardEquipment bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    INSERT INTO builder.VehicleDetailedOptions
        (BusinessUnitID, InventoryId, OptionCode, OptionText, DetailText, CategoryList, IsStandardEquipment) 
    VALUES (@BusinessUnitID, @InventoryId,
			@OptionCode,@OptionText,@DetailText, @CategoryList, @IsStandardEquipment)
			
END
GO

GRANT EXECUTE ON [builder].[VehicleDetailedOption#Save] TO MerchandisingUser 
GO