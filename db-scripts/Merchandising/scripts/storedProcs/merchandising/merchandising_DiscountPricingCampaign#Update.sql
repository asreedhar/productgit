-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-08-15
-- Description:	Update discount pricing data
-- =============================================
USE Merchandising
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[merchandising].[DiscountPricingCampaign#Update]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [merchandising].[DiscountPricingCampaign#Update]
GO

CREATE PROCEDURE merchandising.DiscountPricingCampaign#Update
	 @CampaignId int
	,@BusinessUnitId int
	,@DiscountType int
	,@DealerDiscount decimal(9,2)
	,@ManufacturerRebate decimal(9,2)
	,@ExpirationDate datetime
	,@CreatedBy varchar(250)
    ,@CampaignFilter varchar(max) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DECLARE @Results table(
		[CampaignID] [int] NOT NULL,
		[BusinessUnitId] [int] NOT NULL,
		[DiscountType] [int] NOT NULL,
		[DealerDiscount] [decimal](9, 2) NOT NULL,
		[ManufacturerRebate] [decimal](9, 2) NOT NULL,
		[CreationDate] [datetime] NOT NULL,
		[ExpirationDate] [datetime] NOT NULL,
		[CreatedBy] [varchar](250) NOT NULL,
        [CampaignFilter] [varchar](max) NULL
	);
	
	 UPDATE [Merchandising].[merchandising].[DiscountPricingCampaign]
	 
	 SET BusinessUnitId = @BusinessUnitId, DiscountType = @DiscountType, DealerDiscount = @DealerDiscount, ExpirationDate = @ExpirationDate, CreatedBy = @CreatedBy, CampaignFilter = @CampaignFilter 
		OUTPUT INSERTED.CampaignID, INSERTED.BusinessUnitId, INSERTED.DiscountType, INSERTED.DealerDiscount, INSERTED.ManufacturerRebate, INSERTED.CreationDate,
			INSERTED.ExpirationDate, INSERTED.CreatedBy, INSERTED.CampaignFilter
			INTO @Results
	WHERE CampaignID = @CampaignId;

	SELECT CampaignID, BusinessUnitId, DiscountType, DealerDiscount, ManufacturerRebate, CreationDate, ExpirationDate, CreatedBy, CampaignFilter
	FROM @Results

END
GO

grant execute on [merchandising].[DiscountPricingCampaign#Update] to MerchandisingUser
go

