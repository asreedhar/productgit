if exists (select * from dbo.sysobjects where id = object_id(N'[Merchandising].[CategoryMappings#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Merchandising].[CategoryMappings#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/08/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: merchandising_categoryMappings_fetch.sql,v 1.1 2009/03/26 21:14:06 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch all the category mappings from merchandising
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Merchandising].[CategoryMappings#Fetch]
	-- Add the parameters for the stored procedure here		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
		SELECT 
			om.[OptionID]
			,om.[Description] as optionText
			,om.chromeCategoryId as categoryId
			,ISNULL(ct.userFriendlyName,'') as category
		FROM [Merchandising].[OptionsMapping] om
		LEFT JOIN vehicleCatalog.chrome.categories ct
		ON ct.categoryId = om.chromeCategoryid
		AND ct.countryCode = 1
		
		ORDER BY optionId ASC

END

GO

GRANT EXECUTE ON [Merchandising].[CategoryMappings#Fetch] TO MerchandisingUser 
GO