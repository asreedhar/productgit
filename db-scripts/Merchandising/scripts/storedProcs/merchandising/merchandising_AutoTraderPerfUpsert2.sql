if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[AutoTraderPerfUpsert2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[AutoTraderPerfUpsert2]
GO

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


-- =============================================
-- Author:		<Fitzpatrick, Benin>
-- Create date: <2/2/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: merchandising_AutoTraderPerfUpsert2.sql,v 1.2 2010/03/26 14:01:21 bfitzpatrick Exp $
 * 
 * Summary finally
 * -------
 * 
 * Update or insert any given set of Equipment to a vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @inventoryID
 * @businessUnitID - defaults to 
 * @EngineTypeCategoryID - 
 * @TransmissionTypeCategoryID - 
 * @DriveTrainCategoryID - 
 * @FuelCategoryID -
 * @createdBy	
 * @createdOn
 * @updatedBy
 * @updatedOn
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [merchandising].[AutoTraderPerfUpsert2]
	@businessUnitId int,
	@month datetime,
	@UsedAvgDailyVehicles	int ,
	@UsedPctWithPrice	decimal(3, 0) ,
	@UsedPctWithComments		decimal(3, 0) ,
	@UsedPctWithPhotos	decimal(3, 0) ,
	@UsedPctMultiplePhotos	decimal(3, 0) ,
	@UsedPctSinglePhoto		decimal(3, 0) ,
	@UsedPrimarySource	varchar(50) ,
	@NewAvgDailyVehicles		int ,
	@NewPctWithPrice		decimal(3, 0) ,
	@NewPctWithComments	decimal(3, 0) ,
	@NewPctWithPhotos	decimal(3, 0) ,
	@NewPctMultiplePhotos	decimal(3, 0) ,
	@NewPctSinglePhoto	decimal(3, 0) ,
	@NewPrimarySource	varchar(50) ,
	@PartnerSelectedMarkets	varchar(50) ,
	@PartnerBannerAdImpressions1	int ,
	@PartnerBannerAdImpressions2	int ,
	@PartnerBannerAdClickThrus1	int ,
	@PartnerBannerAdClickThrus2	int 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF EXISTS (SELECT 1 FROM merchandising.AutoTraderPerfSummary WHERE businessUnitID = @businessUnitID  AND month = @month)
	BEGIN
		UPDATE merchandising.AutoTraderPerfSummary
		SET 
		businessUnitID  = @businessUnitID ,
		month = @month ,
		UsedAvgDailyVehicles = @UsedAvgDailyVehicles ,
		UsedPctWithPrice = @UsedPctWithPrice ,
		UsedPctWithComments = @UsedPctWithComments ,
		UsedPctWithPhotos =	@UsedPctWithPhotos ,
		UsedPctMultiplePhotos	= @UsedPctMultiplePhotos ,
		UsedPctSinglePhoto	= @UsedPctSinglePhoto ,
		UsedPrimarySource = @UsedPrimarySource ,
		NewAvgDailyVehicles = @NewAvgDailyVehicles ,
		NewPctWithPrice = @NewPctWithPrice ,
		NewPctWithComments	= @NewPctWithComments ,
		NewPctWithPhotos = @NewPctWithPhotos,
		NewPctMultiplePhotos = @NewPctMultiplePhotos ,
		NewPctSinglePhoto = @NewPctSinglePhoto ,
		NewPrimarySource = @NewPrimarySource ,
		PartnerSelectedMarkets = @PartnerSelectedMarkets ,
		PartnerBannerAdImpressions1 = @PartnerBannerAdImpressions1 ,
		PartnerBannerAdImpressions2 = @PartnerBannerAdImpressions2 ,
		PartnerBannerAdClickThrus1	= @PartnerBannerAdClickThrus1 ,
		PartnerBannerAdClickThrus2	= @PartnerBannerAdClickThrus2
		WHERE
			businessUnitID = @businessUnitID
			AND month = @month 
	END
	ELSE
	BEGIN
	INSERT INTO merchandising.AutoTraderPerfSummary
	(
		businessUnitID ,
		month , 
		UsedAvgDailyVehicles,
		UsedPctWithPrice ,
		UsedPctWithComments ,
		UsedPctWithPhotos ,
		UsedPctMultiplePhotos	,
		UsedPctSinglePhoto,	
		UsedPrimarySource ,
		NewAvgDailyVehicles ,
		NewPctWithPrice ,
		NewPctWithComments	,
		NewPctWithPhotos ,
		NewPctMultiplePhotos ,
		NewPctSinglePhoto ,
		NewPrimarySource ,
		PartnerSelectedMarkets ,
		PartnerBannerAdImpressions1 ,
		PartnerBannerAdImpressions2 ,
		PartnerBannerAdClickThrus1 ,
		PartnerBannerAdClickThrus2	
	 )
	VALUES
	(
		@businessUnitID ,
		@month , 
		@UsedAvgDailyVehicles ,
		@UsedPctWithPrice ,
		@UsedPctWithComments ,
		@UsedPctWithPhotos	,
		@UsedPctMultiplePhotos ,
		@UsedPctSinglePhoto	 ,
		@UsedPrimarySource ,
		@NewAvgDailyVehicles ,
		@NewPctWithPrice ,
		@NewPctWithComments	 ,
		@NewPctWithPhotos	 ,
		@NewPctMultiplePhotos	,
		@NewPctSinglePhoto	 ,
		@NewPrimarySource	 ,
		@PartnerSelectedMarkets	 ,
		@PartnerBannerAdImpressions1,
		@PartnerBannerAdImpressions2 ,
		@PartnerBannerAdClickThrus1 ,
		@PartnerBannerAdClickThrus2	 
	)
	END		
END
GO

GRANT EXECUTE ON [merchandising].[AutoTraderPerfUpsert2] TO MerchandisingUser 
GO








  