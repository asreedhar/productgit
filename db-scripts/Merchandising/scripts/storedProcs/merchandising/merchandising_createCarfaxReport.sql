if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[createCarfaxReport]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[createCarfaxReport]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: merchandising_createCarfaxReport.sql,v 1.3 2009/01/19 19:51:23 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the alerts for all vehicles that are posted on the internet 
 * for a businessUnit
 * 
 * 
 * Parameters
 * ----------
 *  OwnershipText - text about carfax ownership, 
 *	BBGText - text about carfax buyback guarantee, 
 *	TotalLossText - text about total loss indicators, 
 *	FrameDamageText - text about frame damage indicators, 
 *	AirbagDeploymentText - text about airbag deployment indicators, 
 *  OdometerRollbackText - text about odomoter rollback indicators, 
 *	AccidentIndicatorsText - tetx about accident indicators, 
 *	ManufacturerRecallText - text about manufacturer recalls, 
 *	vin - the vin for the vehicle in the report
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [merchandising].[createCarfaxReport]
	-- Add the parameters for the stored procedure here
	@OwnershipText varchar(50), 
	@BBGText varchar(50), 
	@TotalLossText varchar(50), 
	@FrameDamageText varchar(50), 
	@AirbagDeploymentText varchar(50), 
    @OdometerRollbackText varchar(50), 
	@AccidentIndicatorsText varchar(50), 
	@ManufacturerRecallText varchar(50), 
	@Problem varchar(50),
	@ExpiresOn datetime,
	@VIN varchar(17)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
INSERT INTO merchandising.carfaxReport 
    (OwnershipText, 
	BBGText, 
	TotalLossText, 
	FrameDamageText, 
	AirbagDeploymentText, 
    OdometerRollbackText, 
	AccidentIndicatorsText, 
	ManufacturerRecallText, 
	Problem,
	ExpiresOn,
	vin) 
    Values 
	(@OwnershipText, 
	@BBGText, 
	@TotalLossText, 
	@FrameDamageText, 
	@AirbagDeploymentText, 
    @OdometerRollbackText, 
	@AccidentIndicatorsText, 
	@ManufacturerRecallText, 
	@Problem,
	@ExpiresOn,
	@VIN)
	

END
GO

GRANT EXECUTE ON [merchandising].[createCarfaxReport] TO MerchandisingUser 
GO