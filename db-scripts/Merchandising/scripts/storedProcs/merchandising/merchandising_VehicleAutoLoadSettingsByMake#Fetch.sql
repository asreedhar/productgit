IF OBJECT_ID('merchandising.VehicleAutoLoadSettingsByMake#Fetch') IS NOT NULL
	DROP PROCEDURE merchandising.VehicleAutoLoadSettingsByMake#Fetch
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE merchandising.VehicleAutoLoadSettingsByMake#Fetch
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT	vals.ManufacturerID, m.ManufacturerName, vals.IsAutoLoadEnabled, mk.Make
	  FROM	merchandising.VehicleAutoLoadSettings vals
			JOIN VehicleCatalog.Chrome.Manufacturers m ON m.ManufacturerID = vals.ManufacturerID AND m.CountryCode = 1
			LEFT JOIN VehicleCatalog.Chrome.Divisions d ON d.ManufacturerID = vals.ManufacturerID AND d.CountryCode = 1
			LEFT JOIN VehicleCatalog.FirstLook.Make mk ON mk.Make = d.DivisionName
		
END
GO

GRANT EXECUTE ON merchandising.VehicleAutoLoadSettingsByMake#Fetch TO MerchandisingUser 
GO
