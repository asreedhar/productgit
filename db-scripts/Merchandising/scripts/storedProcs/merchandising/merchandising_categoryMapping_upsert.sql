if exists (select * from dbo.sysobjects where id = object_id(N'[Merchandising].[CategoryMapping#Upsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Merchandising].[CategoryMapping#Upsert]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/08/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: merchandising_categoryMapping_upsert.sql,v 1.1 2009/03/26 21:14:06 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * insert record for category mapping to listing options
 * 
 * 
 * Parameters
 * ----------
 * @OptionId int, - integer id for listing option
 * @OptionText varchar(50), - text of the option in listings
 * @CategoryId int - chrome category for the mapping
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Merchandising].[CategoryMapping#Upsert]
	-- Add the parameters for the stored procedure here		
	@OptionId int,
	@OptionText varchar(50),
	@CategoryId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	--update the record IF and ONLY IF the chromecategoryId is null...otherwise, add a row
	IF EXISTS (SELECT 1 FROM [Merchandising].[OptionsMapping] WHERE optionId = @OptionId AND [description] = @OptionText AND chromeCategoryId IS NULL)
	BEGIN
		UPDATE [Merchandising].[OptionsMapping]
		SET ChromeCategoryId = @Categoryid
		WHERE optionId = @OptionId AND [description] = @OptionText
	END
	ELSE
	BEGIN
		INSERT INTO [Merchandising].[OptionsMapping]
		(OptionID,Description,ChromeCategoryId)
		VALUES
		(@OptionId, @OptionText,@CategoryId)
	END
	

END

GO

GRANT EXECUTE ON [Merchandising].[CategoryMapping#Upsert] TO MerchandisingUser 
GO