if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[WebLoaderPhotos#BatchComplete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[WebLoaderPhotos#BatchComplete]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE PROCEDURE [merchandising].[WebLoaderPhotos#BatchComplete]
       @InventoryID int
AS
BEGIN
       
       DECLARE @batch TABLE (batch UNIQUEIDENTIFIER PRIMARY KEY)
       INSERT @batch ( batch )
       SELECT DISTINCT Batch FROM Merchandising.Merchandising.WebLoaderPhotos
       WHERE InventoryID=@InventoryId
 
 
       SELECT B.Batch,
                     B.Taken,
                     B.BatchComplete,
                     IsComplete = CASE WHEN B.BatchCount = C.FinishedCount THEN CAST(1 as bit) ELSE CAST(0 as bit) END
       FROM ( SELECT InventoryID, p.Batch, Max(Taken) As Taken, MAX(UpLoaded) as BatchComplete, Count(*) as BatchCount
              FROM Merchandising.Merchandising.WebLoaderPhotos p
              INNER JOIN @batch b ON b.Batch= p.Batch
              WHERE Position != -1 --Submitted batch
              GROUP BY InventoryID, p.Batch) B
       LEFT JOIN (   SELECT p.Batch, Count(*) as FinishedCount
                     FROM Merchandising.Merchandising.WebLoaderPhotos p
                     INNER JOIN @batch b ON b.Batch= p.Batch
                     WHERE Status != 0 -- all photos in the batch have completed or had an error
                     GROUP BY p.Batch) C ON B.Batch = C.Batch 
       WHERE B.InventoryID = @InventoryID
       ORDER BY B.Taken, IsComplete DESC
       
END 
 
GO


GRANT EXECUTE ON [merchandising].[WebLoaderPhotos#BatchComplete] TO MerchandisingUser 
GO