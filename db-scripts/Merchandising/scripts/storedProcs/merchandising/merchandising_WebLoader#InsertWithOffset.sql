if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[WebLoaderPhotos#InsertWithOffset]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[WebLoaderPhotos#InsertWithOffset]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [merchandising].[WebLoaderPhotos#InsertWithOffset]
	@InventoryID int,
	@Taken DateTime,
	@Batch uniqueidentifier,
	@Handle uniqueidentifier,
	@Position int,
	@UserName varchar(50),
	@Offset int,
	@Version varchar(20)
AS
BEGIN
	INSERT INTO merchandising.WebLoaderPhotos (InventoryID, Handle, Batch, Position, Taken, UserName, PositionOffset, WebloaderVersion, Received, Status)
	VALUES(@InventoryID, @Handle, @Batch, @Position, @Taken, @UserName, @Offset, @Version, GETDATE(), 0)
END 
GO

GRANT EXECUTE ON [merchandising].[WebLoaderPhotos#InsertWithOffset] TO MerchandisingUser 
GO