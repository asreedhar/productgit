-- ================================================
-- Author:		Arjun Seshadri
-- Create date: 2014-08-29
-- Description:	Get Position Offset for photo batch
-- ================================================

USE Merchandising
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[WebLoaderPhotos#FetchPositionOffset]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[WebLoaderPhotos#FetchPositionOffset]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [merchandising].[WebLoaderPhotos#FetchPositionOffset]
	@Batch uniqueidentifier
AS
BEGIN
	SELECT PositionOffset 
	FROM merchandising.WebLoaderPhotos 
	WHERE Batch=@Batch 
	AND PositionOffset IS NOT NULL
END 
GO

GRANT EXECUTE ON [merchandising].[WebLoaderPhotos#FetchPositionOffset] TO MerchandisingUser 
GO

