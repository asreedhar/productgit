IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[merchandising].[DealerSiteCredentials#setLockoutStatus]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [merchandising].[DealerSiteCredentials#setLockoutStatus]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [merchandising].[DealerSiteCredentials#setLockoutStatus]
	@BusinessUnitId int,
	@siteId int,
	@isLockedOut bit,
	@credentialVersion timestamp
AS
BEGIN
	SET NOCOUNT ON;
	
	-- check if there is an actual update to perform
	IF NOT EXISTS ( SELECT  1
						FROM    settings.Dealer_SiteCredentials
						WHERE   businessUnitId = @BusinessUnitId
								AND siteId = @SiteId
								AND isLockedOut != @isLockedOut)
		return -- nothing to do, just bail

	
	UPDATE settings.Dealer_SiteCredentials 
		SET isLockedOut = @isLockedOut
	 WHERE businessUnitId = @BusinessUnitId 
		AND siteId = @siteId
		AND credentialVersion = @credentialVersion
							
	if @@ROWCOUNT != 1
		begin
			RAISERROR('This credential update failed becuase it has been updated by another user', 16, 1)
			return
		end
		
		
	
END		
GO

GRANT EXECUTE ON [merchandising].[DealerSiteCredentials#setLockoutStatus] TO MerchandisingUser 
GO
