if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[WebLoaderPhotos#ExistsForInventory]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[WebLoaderPhotos#ExistsForInventory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [merchandising].[WebLoaderPhotos#ExistsForInventory]
	@InventoryID int
AS
BEGIN

	IF EXISTS (SELECT * FROM merchandising.WebLoaderPhotos WHERE InventoryID = @InventoryID)
		select 1 as 'Exist' else select 0 as 'Exist'

END 
GO

GRANT EXECUTE ON [merchandising].[WebLoaderPhotos#ExistsForInventory] TO MerchandisingUser 
GO