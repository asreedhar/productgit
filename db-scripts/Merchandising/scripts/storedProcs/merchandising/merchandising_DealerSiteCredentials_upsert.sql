IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[merchandising].[DealerSiteCredentials#upsert]')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE [merchandising].[DealerSiteCredentials#upsert]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [merchandising].[DealerSiteCredentials#upsert]
    @BusinessUnitId INT ,
    @SiteId INT ,
    @Username VARCHAR(50),
    @EncryptedPassword VARCHAR(50),
    @LockOut BIT = NULL,
	@DealerCode VARCHAR(50),
	@credentialVersion timestamp = null,
	@rowsUpdated int = null OUTPUT
AS 
    BEGIN
        SET NOCOUNT ON ;
        
        SET @rowsUpdated = 0
        
        IF EXISTS ( SELECT  1
                    FROM    settings.Dealer_SiteCredentials
                    WHERE   businessUnitId = @BusinessUnitId
                            AND siteId = @SiteId ) 
            BEGIN
				-- we know it is an update, must have a rowVersion for concurrency check
				if @credentialVersion IS NULL
					begin
						RAISERROR('This credential cannot be updated because a row version was not sent for update', 16, 1)
						return
					end

				-- check if there is an actual update to perform
				IF NOT EXISTS ( SELECT  1
									FROM    settings.Dealer_SiteCredentials
									WHERE   businessUnitId = @BusinessUnitId
											AND siteId = @SiteId
											AND ( username != @Username 
												OR encryptedPassword != @EncryptedPassword
												OR IsNull(DealerCode,'') != IsNull(@DealerCode, '')
												OR isLockedOut != COALESCE(@LockOut, isLockedOut)
												)
								)
						return -- nothing to do, just bail
            
				-- version is good, changes to apply ... perform update
                UPDATE  settings.Dealer_SiteCredentials
					SET     username = @Username ,
							encryptedPassword = @EncryptedPassword ,
							isLockedOut = COALESCE(@LockOut, isLockedOut),
							DealerCode = @DealerCode
					WHERE   businessUnitId = @BusinessUnitId
							AND siteId = @SiteId
							AND credentialVersion = @credentialVersion
							
				SET @rowsUpdated = @@ROWCOUNT
				
				if @rowsUpdated != 1
					begin
						RAISERROR('This credential update failed because it has been updated by another user', 16, 1)
						return
					end
							
					

            END
        ELSE 
            BEGIN
                INSERT  INTO settings.Dealer_SiteCredentials
                        ( businessUnitId ,
                          siteId ,
                          username ,
                          encryptedPassword ,
                          isLockedOut,
						  DealerCode
                        )
                VALUES  ( @BusinessUnitId ,
                          @SiteId ,
                          @Username ,
                          @EncryptedPassword ,
                          @LockOut,
						  @DealerCode
                        )             

	            SET @rowsUpdated = @@ROWCOUNT

            END
            
									 
    END		
GO


GRANT EXECUTE ON [merchandising].[DealerSiteCredentials#upsert] TO MerchandisingUser 
GO									 
