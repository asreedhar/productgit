-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-08-15
-- Description:	fetch inventory / 
--				discount campaign by expiration
-- =============================================

USE Merchandising
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'merchandising.DiscountPricingCampaign#FetchDueToExpire') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE merchandising.DiscountPricingCampaign#FetchDueToExpire
GO

CREATE PROCEDURE merchandising.DiscountPricingCampaign#FetchDueToExpire
	@ExpireDate DATETIME
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		dpc.CampaignID, dpc.BusinessUnitId, dpc.DiscountType, dpc.DealerDiscount, dpc.ManufacturerRebate, dpc.CreationDate, dpc.ExpirationDate, dpc.CreatedBy, dpc.CampaignFilter, dpc.HasBeenExpired
	FROM Merchandising.merchandising.DiscountPricingCampaign dpc
	WHERE 
		ExpirationDate < @ExpireDate
		AND (HasBeenExpired IS NULL 
			 OR HasBeenExpired = 0)
	ORDER BY dpc.CampaignID
	
END
GO

grant execute on merchandising.DiscountPricingCampaign#FetchDueToExpire to MerchandisingUser
go

