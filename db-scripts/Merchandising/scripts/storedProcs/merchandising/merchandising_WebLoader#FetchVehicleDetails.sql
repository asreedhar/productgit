if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[WebLoaderPhotos#FetchVehicleDetails]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[WebLoaderPhotos#FetchVehicleDetails]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE PROCEDURE [merchandising].[WebLoaderPhotos#FetchVehicleDetails]
       @InventoryID INT,
       @Login VARCHAR(100)
AS
BEGIN
       
        SELECT    I.StockNumber, V.Vin, M.FirstName, M.LastName    
        FROM IMT.dbo.Inventory I 
        JOIN IMT.dbo.Vehicle V on I.vehicleid = V.vehicleid    
        JOIN IMT.dbo.Member M on M.Login = @Login  
        WHERE I.InventoryID = @InventoryID
END 
 
GO


GRANT EXECUTE ON [merchandising].[WebLoaderPhotos#FetchVehicleDetails] TO MerchandisingUser 
GO