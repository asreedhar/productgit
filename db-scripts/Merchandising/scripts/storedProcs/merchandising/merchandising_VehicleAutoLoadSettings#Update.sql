IF OBJECT_ID('merchandising.VehicleAutoLoadSettings#Update') IS NOT NULL
	DROP PROCEDURE merchandising.VehicleAutoLoadSettings#Update
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE merchandising.VehicleAutoLoadSettings#Update
	@manufacturerIdList varchar(4000)
AS
BEGIN
	SET NOCOUNT ON;
	
	--Clear all settings
	UPDATE	merchandising.VehicleAutoLoadSettings
	SET		IsAutoLoadEnabled = 0

	--Set according to id list
	UPDATE	merchandising.VehicleAutoLoadSettings
	SET		IsAutoLoadEnabled = 1
	WHERE	ManufacturerID IN (SELECT convert(int,Data) FROM dbo.Split(@manufacturerIdList,','))
		
END
GO

GRANT EXECUTE ON merchandising.VehicleAutoLoadSettings#Update TO MerchandisingUser 
GO
