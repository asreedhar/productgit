if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[AutoTraderVehUpsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[AutoTraderVehUpsert]
GO

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


-- =============================================
-- Author:		<Fitzpatrick, Benin>
-- Create date: <2/2/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: merchandising_AutoTraderVehUpsert.sql,v 1.1 2010/03/25 18:59:33 bfitzpatrick Exp $
 * 
 * Summary finally
 * -------
 * 
 * Update or insert any given set of Equipment to a vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @inventoryID
 * @businessUnitID - defaults to 
 * @EngineTypeCategoryID - 
 * @TransmissionTypeCategoryID - 
 * @DriveTrainCategoryID - 
 * @FuelCategoryID -
 * @createdBy	
 * @createdOn
 * @updatedBy
 * @updatedOn
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [merchandising].[AutoTraderVehUpsert]
	
	@businessUnitID int,
	@month datetime,
	@VIN varchar(50) ,
	@Type	varchar(50) ,
	@YearMakeModel	varchar(50) ,
	@StockNumber	varchar(50) ,
	@Price	money ,
	@AppearedInSearch	int ,
	@DetailsPageViews	int ,
	@MapViews	int ,
	@EmailsSent	int ,
	@DetailsPagePrinted	int,
	@HasSpotLightAds bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF EXISTS (SELECT 1 FROM merchandising.AutoTraderVehSummary WHERE businessUnitID = @businessUnitID AND month = @month AND VIN = @VIN)
	BEGIN
		UPDATE merchandising.AutoTraderVehSummary
		SET 
		businessUnitID = @businessUnitID,
		month = @month,
		VIN = @VIN ,
		Type= @Type	,
		YearMakeModel = @YearMakeModel ,
		StockNumber = @StockNumber,
		Price = @Price,
		AppearedInSearch = @AppearedInSearch	,
		DetailsPageViews = @DetailsPageViews ,
		MapViews = @MapViews,
		EmailsSent = @EmailsSent,
		DetailsPagePrinted = @DetailsPagePrinted,
		HasSpotLightAds = @HasSpotLightAds 
		WHERE
			businessUnitID = @businessUnitID 
			AND month = @month
			AND VIN = @VIN
	END
	ELSE
	BEGIN
	INSERT INTO merchandising.AutoTraderVehSummary
	(
		businessUnitID ,
		month ,
		VIN  ,
		Type	,
		YearMakeModel	 ,
		StockNumber	 ,
		Price	 ,
		AppearedInSearch	 ,
		DetailsPageViews	 ,
		MapViews	 ,
		EmailsSent	 ,
		DetailsPagePrinted	,
		HasSpotLightAds 
	 )
	VALUES
	(
		@businessUnitID ,
		@month ,
		@VIN  ,
		@Type	 ,
		@YearMakeModel	 ,
		@StockNumber	 ,
		@Price	 ,
		@AppearedInSearch	 ,
		@DetailsPageViews	 ,
		@MapViews	 ,
		@EmailsSent	 ,
		@DetailsPagePrinted	,
		@HasSpotLightAds 
	)
	END		
END
GO

GRANT EXECUTE ON [merchandising].[AutoTraderVehUpsert] TO MerchandisingUser 
GO







  