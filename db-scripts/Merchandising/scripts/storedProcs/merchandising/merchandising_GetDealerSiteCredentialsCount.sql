IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[merchandising].[GetDealerSiteCredentialsCount]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [merchandising].[GetDealerSiteCredentialsCount]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

--Gets the count of the unlocked site credentials for autoload

CREATE PROCEDURE [merchandising].[GetDealerSiteCredentialsCount]
	@BusinessUnitId int
	
AS
BEGIN
	SET NOCOUNT ON;
	
	select count(*) from 
	  settings.Dealer_SiteCredentials
	 WHERE businessUnitId = @BusinessUnitId 
	AND isLockedOut = 0
		
	END		
GO

GRANT EXECUTE ON [merchandising].[GetDealerSiteCredentialsCount] TO MerchandisingUser 
GO
