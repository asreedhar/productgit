-- ===============================================
-- Author:		Arjun Seshadri
-- Create date: 2014-08-29
-- Description:	Update photo batch position offset
-- ===============================================

USE Merchandising
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[WebLoaderPhotos#UpdatePositionOffset]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[WebLoaderPhotos#UpdatePositionOffset]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [merchandising].[WebLoaderPhotos#UpdatePositionOffset]
	@Batch uniqueidentifier,
	@Offset int
AS
BEGIN
	UPDATE [Merchandising].[WebLoaderPhotos] SET PositionOffset = @Offset 
	WHERE Batch = @Batch
END 
GO

GRANT EXECUTE ON [merchandising].[WebLoaderPhotos#UpdatePositionOffset] TO MerchandisingUser 
GO
