if object_id('merchandising.DealerFeedInfo#Update') is not null
	drop procedure merchandising.DealerFeedInfo#Update
go

-- Update the DealerFeedInfo with update to date information from other sources (Denormalize for performance)
create procedure merchandising.DealerFeedInfo#Update
	@destinationId int,
	@DatasourceInterfaceId int
as
begin
set nocount on
set transaction isolation level read uncommitted


declare @dealerFeedInfoStats table
(
	BusinessUnitID int not null, 
	DestinationID int not null,
	DatasourceInterfaceID int not null,
	LastSuccessfulRequestID int null, 
	LastSuccessfulDateLoaded datetime null
)



-- Get data aggregations for VehiclesOnline from various sources
if (@DatasourceInterfaceID in (9, 10, 11, 12, 13, 2001, 2002, 2003, 2004, 2005))
begin
	-- nothing to do for these data sources
	return 0
end
else if (@DatasourceInterfaceId = 14)
begin
	-- AutoTrader
	insert @dealerFeedInfoStats
	(
		BusinessUnitID,
		DestinationID,
		DatasourceInterfaceID,
		LastSuccessfulRequestID,
		LastSuccessfulDateLoaded
	)
	select
		BusinessUnitID, 
		@destinationId,
		@DatasourceInterfaceId,
		max(RequestID) as LastSuccessfulRequestID,
		max(DateLoaded) as LastSuccessfulDateLoaded
	from merchandising.AutoTraderVehOnline
	group by BusinessUnitID
end
else if (@DatasourceInterfaceId = 2000)
begin
	-- Cars.com
	insert @dealerFeedInfoStats
	(
		BusinessUnitID,
		DestinationID,
		DatasourceInterfaceID,
		LastSuccessfulRequestID,
		LastSuccessfulDateLoaded
	)
	select
		BusinessUnitID, 
		@destinationId,
		@DatasourceInterfaceId,
		max(RequestID) as LastSuccessfulRequestID,
		max(DateLoaded) as LastSuccessfulDateLoaded
	from merchandising.CarsDotComVehOnline
	group by BusinessUnitID
end
else
begin
	raiserror('merchandising.DealerFeedInfo#Update is coded to deal with specific @DatasourceInterfaceId, and the one provided: (%d) has no code!', 16, 1, @DatasourceInterfaceId)
	return 1
end



-- Update existing rows
update merchandising.DealerFeedInfo
    set LastSuccessfulDateLoaded = dfiStats.LastSuccessfulDateLoaded,
        LastSuccessfulRequestID = dfiStats.LastSuccessfulRequestID
    from merchandising.DealerFeedInfo as dfi
        inner join @dealerFeedInfoStats as dfiStats
        on dfi.BusinessUnitID = dfiStats.BusinessUnitID
            and dfi.DestinationID = dfiStats.DestinationID
            and dfi.DatasourceInterfaceID = dfiStats.DatasourceInterfaceID
        
-- Insert new rows
insert into merchandising.DealerFeedInfo
    select dfiStats.BusinessUnitID, 
           dfiStats.DestinationID,
           dfiStats.DatasourceInterfaceID,
           dfiStats.LastSuccessfulDateLoaded,
           dfiStats.LastSuccessfulRequestID
        from @dealerFeedInfoStats as dfiStats
            left outer join merchandising.DealerFeedInfo as dfi
            on dfiStats.BusinessUnitID = dfi.BusinessUnitID
                and dfiStats.DestinationID = dfi.DestinationID
                and dfiStats.DatasourceInterfaceID = dfi.DatasourceInterfaceID
        where
            dfi.BusinessUnitID is null
end
GO

GRANT EXECUTE ON merchandising.DealerFeedInfo#Update TO MerchandisingUser 
GO

