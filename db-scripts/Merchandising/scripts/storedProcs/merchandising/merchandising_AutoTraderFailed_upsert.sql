if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[AutoTraderFailed#Upsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[AutoTraderFailed#Upsert]
GO

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

-- =============================================
-- Author:		<Sombke, Bryant>
-- Create date: <4/5/10>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * Summary finally
 * -------
 * Update or insert any record indicating a business unit's AutoTrader login credentials are no longer valid.
 * 
 * Parameters
 * ----------
 * @businessUnitID
 * @dateFailed
 *
 * Exceptions
 * ----------
 * 
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [merchandising].[AutoTraderFailed#Upsert]
	
	@businessUnitID int,
	@dateFailed datetime
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE settings.DealerListingSites 
		SET dateLoginFailure = @dateFailed 
		WHERE businessUnitID = @businessUnitID AND destinationID = 2
END
GO

GRANT EXECUTE ON [merchandising].[AutoTraderFailed#Upsert] TO MerchandisingUser 
GO