-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-08-28
-- Description:	Expire discount campaign
-- =============================================

USE Merchandising
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[merchandising].[DiscountPricingCampaign#Expire]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [merchandising].[DiscountPricingCampaign#Expire]
GO

CREATE PROCEDURE merchandising.DiscountPricingCampaign#Expire
	@BusinessUnitId int,
	@CampaignId int
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE Merchandising.merchandising.DiscountPricingCampaign
	SET HasBeenExpired = 1
	WHERE 
		BusinessUnitId = @BusinessUnitId
		AND CampaignId = @CampaignId
END
GO

grant execute on [merchandising].[DiscountPricingCampaign#Expire] to MerchandisingUser
go

