IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[merchandising].[DealerSiteCredentials#Fetch]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [merchandising].[DealerSiteCredentials#Fetch]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [merchandising].[DealerSiteCredentials#Fetch]
	@BusinessUnitId int,
	@siteId int,
	@allowLockedOutCredentials bit	
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT * 
	  FROM settings.Dealer_SiteCredentials
	 WHERE businessUnitId = @BusinessUnitId 
		AND siteId = @siteId
		AND (@allowLockedOutCredentials = 1 OR isLockedOut = 0)
		
	END		
GO

GRANT EXECUTE ON [merchandising].[DealerSiteCredentials#Fetch] TO MerchandisingUser 
GO
