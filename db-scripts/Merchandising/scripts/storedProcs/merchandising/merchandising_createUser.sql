if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[createUser]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[createUser]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: merchandising_createUser.sql,v 1.1 2008/12/11 04:58:35 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the alerts for all vehicles that are posted on the internet 
 * for a businessUnit
 * 
 * 
 * Parameters
 * ----------
 *  @MemberLogin - varchar member login (should match an existing firstlook login)
 
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [merchandising].[createUser]
	-- Add the parameters for the stored procedure here
	@MemberLogin varchar(80)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	CREATE TABLE #Members (
		MemberId int NOT NULL,
		FirstName varchar(20) NOT NULL,
		LastName varchar(20) NOT NULL,
		[Login] varchar(80) NOT NULL,
		EmailAddress varchar(129) NULL,
		MemberType int NULL,
		LoginStatus int NULL
	)
	INSERT INTO #Members EXEC Interface.MemberLogin#Fetch @MemberLogin
	

	INSERT INTO merchandising.settings.membermapping (merchandisingMemberLogin, firstlookMemberId)
	SELECT [Login], MemberId FROM #Members

	DROP TABLE #Members
END
GO

GRANT EXECUTE ON [merchandising].[createUser] TO MerchandisingUser 
GO