if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[AutoTraderExFetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[AutoTraderExFetch]
GO

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


-- =============================================
-- Author:		<Fitzpatrick, Benin>
-- Create date: <2/2/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: merchandising_AutoTraderExFetch.sql,v 1.1 2010/03/25 20:52:33 bsombke Exp $
 * 
 * Summary finally
 * -------
 * 
 * Update or insert any given set of Equipment to a vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @inventoryID
 * @businessUnitID - defaults to 
 * @EngineTypeCategoryID - 
 * @TransmissionTypeCategoryID - 
 * @DriveTrainCategoryID - 
 * @FuelCategoryID -
 * @createdBy	
 * @createdOn
 * @updatedBy
 * @updatedOn
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [merchandising].[AutoTraderExFetch]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT * FROM merchandising.AutoTraderExSummary
	END		
GO

GRANT EXECUTE ON [merchandising].[AutoTraderExFetch] TO MerchandisingUser 
GO







  