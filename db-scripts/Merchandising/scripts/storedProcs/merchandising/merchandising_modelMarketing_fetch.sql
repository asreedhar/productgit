if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[modelMarketing#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[modelMarketing#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <2/5/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: merchandising_modelMarketing_fetch.sql,v 1.1 2009/02/10 23:55:08 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the model marketing for a vehicle
 * 
 * 
 * Parameters
 * ----------
 *  @ChromeStyleId - vehicle chrome style id
 
 *  -marketingType: 1 - Intro Text; 2 - Vehicle Tagline; 3 - Model Awards
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [merchandising].[modelMarketing#Fetch]
	-- Add the parameters for the stored procedure here
	@ChromeStyleId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
		
	select awardText as [text], 3 as marketingType, importanceRanking
	FROM vehicleCatalog.chrome.styles sty
	INNER JOIN vehicleCatalog.chrome.models mdl
	ON mdl.modelId = sty.modelId
	INNER JOIN vehicleCatalog.chrome.divisions div
	ON div.divisionId = mdl.divisionId
	INNER JOIN merchandising.modelAwards ma
		ON ma.modelYear = mdl.modelYear
		AND ma.make like div.DivisionName
		AND (ma.model like mdl.ModelName
			OR ma.model like sty.cfModelName)
		AND (ma.trim = ''
			OR ma.trim like sty.trim)
	WHERE sty.styleId = @ChromeStyleId
	and sty.countryCode = 1
	and mdl.countryCode = 1
	and div.countryCode = 1
	ORDER BY importanceRanking asc

	
END
GO

GRANT EXECUTE ON [merchandising].[modelMarketing#Fetch] TO MerchandisingUser 
GO