-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-08-15
-- Description:	Insert inventory / 
--				discount campaign
-- =============================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[merchandising].[DiscountPricingCampaignInventory#Insert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [merchandising].[DiscountPricingCampaignInventory#Insert]
GO

CREATE PROCEDURE merchandising.DiscountPricingCampaignInventory#Insert
	  @CampaignId int
	 ,@InventoryId int
	 ,@ActivatingUser varchar(250)
	 ,@SpecialPrice decimal(9,2)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		BEGIN TRANSACTION
		
			DECLARE @OriginalPrice decimal(9,2);
			SET @OriginalPrice = (SELECT ncp.SpecialPrice FROM Merchandising.builder.NewCarPricing ncp WHERE InventoryID = @InventoryId);
			
			DECLARE @Results table(
				CampaignID int NOT NULL,
				InventoryId int NOT NULL,
				OriginalNewCarPrice decimal(9, 2) NULL,
				Active bit,
				ActivatedBy varchar(250),
				ActivatedDate datetime NULL,
				DeactivatedBy varchar(250),
				DeactivatedDate datetime NULL
			);
			
			DECLARE @TimeNow DATETIME = (SELECT GETDATE());
			
			IF EXISTS(SELECT * FROM Merchandising.merchandising.DiscountPricingCampaignInventory WHERE CampaignID = @CampaignId AND InventoryId = @InventoryId)
				BEGIN
					UPDATE Merchandising.merchandising.DiscountPricingCampaignInventory 
					SET Active = 1, ActivatedBy = @ActivatingUser, ActivatedDate = @TimeNow, DeactivatedBy = NULL, DeactivatedDate = NULL
					OUTPUT @CampaignId, @InventoryId, @OriginalPrice, INSERTED.Active, INSERTED.ActivatedBy, INSERTED.ActivatedDate, INSERTED.DeactivatedBy, INSERTED.DeactivatedDate
						INTO @Results
					WHERE CampaignID = @CampaignId 
					AND InventoryId = @InventoryId
				END
			ELSE
				BEGIN
				INSERT INTO [Merchandising].[merchandising].[DiscountPricingCampaignInventory]
					OUTPUT INSERTED.CampaignId, INSERTED.InventoryId, INSERTED.OriginalNewCarPrice, INSERTED.Active, INSERTED.ActivatedBy, INSERTED.ActivatedDate, INSERTED.DeactivatedBy, INSERTED.DeactivatedDate
					INTO @Results
				VALUES
					   (@CampaignId
					   ,@InventoryId
					   ,@OriginalPrice
					   ,1,
					   @ActivatingUser,
					   null,
					   @TimeNow,
					   null)
				END
				
			-- Insert the new special price into the new car pricing table
			DECLARE @UpsertUserName varchar(80)
			SET @UpsertUserName = SUBSTRING(@ActivatingUser, 1, 80)
			EXECUTE builder.NewCarPricing#Upsert @InventoryId, @SpecialPrice, @UpsertUserName -- Upsert requires 80 chars or less
			
			-- Return the results to the app
			SELECT CampaignId, InventoryId, OriginalNewCarPrice, Active, ActivatedBy, DeactivatedBy, ActivatedDate, DeactivatedDate FROM @Results;
		
		-- do it
		COMMIT TRANSACTION

    END TRY
    BEGIN CATCH -- Shamelessly stolen from merchandising.DiscountPricingCampaignInventory#Deactivate
    
    		ROLLBACK TRANSACTION

			DECLARE @ErrorMessage NVARCHAR(4000),
			  @ErrorNumber INT,
			  @ErrorSeverity INT,
			  @ErrorState INT,
			  @ErrorLine INT,
			  @ErrorProcedure NVARCHAR(200) ;

				-- Assign variables to error-handling functions that 
				-- capture information for RAISERROR.
			SELECT @ErrorNumber = ERROR_NUMBER(), @ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE(), @ErrorLine = ERROR_LINE(),
					@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-') ;

				-- Building the message string that will contain original
				-- error information.
			SET @ErrorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' +
					'Message: ' + ERROR_MESSAGE() ;

				-- Raise an error: msg_str parameter of RAISERROR will contain
				-- the original error information.
			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, -- parameter: original error number.
			  @ErrorSeverity, -- parameter: original error severity.
			  @ErrorState, -- parameter: original error state.
			  @ErrorProcedure, -- parameter: original error procedure name.
			  @ErrorLine-- parameter: original error line number.
			)

    END CATCH
	
	
	SET NOCOUNT OFF
END
GO

grant execute on merchandising.DiscountPricingCampaignInventory#Insert to MerchandisingUser
go

