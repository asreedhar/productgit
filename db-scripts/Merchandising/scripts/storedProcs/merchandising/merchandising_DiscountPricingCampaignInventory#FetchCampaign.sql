-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-08-20
-- Description:	fetch discount campaigns by inv.
-- =============================================

USE Merchandising
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[merchandising].[DiscountPricingCampaignInventory#FetchCampaign]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [merchandising].[DiscountPricingCampaignInventory#FetchCampaign]
GO

CREATE PROCEDURE merchandising.DiscountPricingCampaignInventory#FetchCampaign
	@InventoryId int,
	@OnlyActive bit = 0
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT dpc.CampaignID, dpc.BusinessUnitId, dpc.DiscountType, dpc.DealerDiscount, dpc.ManufacturerRebate, dpc.CreationDate, dpc.ExpirationDate, dpc.CreatedBy, dpc.CampaignFilter, dpc.HasBeenExpired,
		dpci.InventoryId, dpci.OriginalNewCarPrice, dpci.Active, dpci.ActivatedBy, dpci.DeactivatedBy, dpci.ActivatedDate, dpci.DeactivatedDate, dpci.OriginalNewCarPrice
	FROM Merchandising.merchandising.DiscountPricingCampaign dpc
	JOIN Merchandising.merchandising.DiscountPricingCampaignInventory dpci ON dpci.CampaignID = dpc.CampaignID
	WHERE 
		InventoryId = @InventoryId
		AND (@OnlyActive = 0 OR Active = @OnlyActive)
	ORDER BY dpc.CampaignID, dpci.InventoryId
END
GO

grant execute on [merchandising].[DiscountPricingCampaignInventory#FetchCampaign] to MerchandisingUser
go

