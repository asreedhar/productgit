if object_id('merchandising.DealerCredentialsFailed') is not null
	drop procedure merchandising.DealerCredentialsFailed
go



create procedure merchandising.DealerCredentialsFailed
	@businessUnitID int,
	@destinationID int, -- Merchandising.settings.EdtDestinations
	@dateFailed datetime
as
set nocount on
	
update settings.DealerListingSites set
	dateLoginFailure = @dateFailed
where
	businessUnitID = @businessUnitID
	and destinationID = @destinationID
go

