SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'merchandising.DiscountPricingCampaignInventory#InAnInvalidState') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE merchandising.DiscountPricingCampaignInventory#InAnInvalidState
GO

CREATE PROCEDURE  merchandising.DiscountPricingCampaignInventory#InAnInvalidState
	@BusinessUnitId int
AS
BEGIN
	SET NOCOUNT ON;
	;WITH DiscountCampaigns AS 
	(
		SELECT 
		BusinessUnitId, CampaignID, DiscountType, DealerDiscount, ManufacturerRebate
		FROM merchandising.DiscountPricingCampaign
		WHERE DiscountPricingCampaign.BusinessUnitId = @BusinessUnitId
		AND DiscountPricingCampaign.HasBeenExpired = 0
	)
	, DiscountInventory AS
	(
		SELECT 
		dpci.InventoryId, InventoryActive.MSRP, InventoryActive.UnitCost
		, CASE DiscountCampaigns.DiscountType 
			WHEN 0 THEN MSRP + DealerDiscount + ManufacturerRebate
			WHEN 1 THEN UnitCost + DealerDiscount + ManufacturerRebate
		END AS CalculatedDiscount
			
		FROM merchandising.DiscountPricingCampaignInventory dpci
		JOIN DiscountCampaigns ON DiscountCampaigns.CampaignID = dpci.CampaignID 
		JOIN FLDW..InventoryActive ON InventoryActive.BusinessUnitID = DiscountCampaigns.BusinessUnitId 
			AND InventoryActive.InventoryID = dpci.InventoryId
		WHERE dpci.Active = 1
	)

	SELECT DiscountInventory.InventoryId --, SpecialPrice, CalculatedDiscount
	FROM DiscountInventory 
	LEFT JOIN builder.NewCarPricing ON NewCarPricing.InventoryID = DiscountInventory.InventoryId
	WHERE ISNULL(SpecialPrice,0) <> DiscountInventory.CalculatedDiscount

	SET NOCOUNT OFF
	
	END
GO

grant execute on merchandising.DiscountPricingCampaignInventory#InAnInvalidState to MerchandisingUser
go

