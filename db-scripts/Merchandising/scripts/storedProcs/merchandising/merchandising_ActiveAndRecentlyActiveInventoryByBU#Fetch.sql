-- ======================================================
-- Author:		Arjun Seshadri
-- Create date: 2014-08-6
-- Description:	Get a list of active and recently active 
--				vehicles for BusinessUnit @BusinessUnitId
-- ======================================================
USE Merchandising
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[merchandising].[ActiveAndRecentlyActiveInventoryByBU#Fetch]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE merchandising.ActiveAndRecentlyActiveInventoryByBU#Fetch
GO

CREATE PROCEDURE [merchandising].[ActiveAndRecentlyActiveInventoryByBU#Fetch] 
	@BusinessUnitId int, 
	@RecentlyActiveDays tinyint = 45,
	@NewOnly bit = 1
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		inv.InventoryID, veh.VIN
	FROM
		IMT.dbo.Inventory inv
		INNER JOIN IMT.dbo.Vehicle veh ON inv.VehicleID = veh.VehicleID
		LEFT JOIN Merchandising.builder.OptionsConfiguration oc ON oc.businessUnitID = inv.BusinessUnitID AND oc.inventoryId = inv.InventoryID

	WHERE
		inv.BusinessUnitID = @BusinessUnitId
		AND (	
			inv.InventoryActive = 1 OR 
			inv.DeleteDt > DATEADD(dd, (0 - 45), CAST(GETDATE() AS DATE))
		)
		AND (@NewOnly = 0 OR InventoryType = @NewOnly)
END

GO

GRANT EXECUTE on merchandising.ActiveAndRecentlyActiveInventoryByBU#Fetch to MerchandisingUser

GO