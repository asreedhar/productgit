if exists (select * from dbo.sysobjects where id = object_id(N'[Merchandising].[EquipmentItems#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Merchandising].[EquipmentItems#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [Merchandising].[EquipmentItems#Fetch]
	@BusinessUnitID int,
	@InventoryID int
	
AS
BEGIN

	SELECT DISTINCT EquipmentList, ScheduledReleaseTime
	FROM Merchandising.Postings.VehicleAdScheduling
	WHERE BusinessUnitID = @BusinessUnitID AND InventoryID = @InventoryID
		
END

GO

GRANT EXECUTE ON [Merchandising].[EquipmentItems#Fetch] TO MerchandisingUser 
GO