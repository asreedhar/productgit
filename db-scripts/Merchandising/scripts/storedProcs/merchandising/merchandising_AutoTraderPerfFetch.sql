if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[AutoTraderPerfFetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[AutoTraderPerfFetch]
GO

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


-- =============================================
-- Author:		<Fitzpatrick, Benin>
-- Create date: <2/2/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: merchandising_AutoTraderPerfFetch.sql,v 1.1 2010/03/25 18:59:33 bfitzpatrick Exp $
 * 
 * Summary finally
 * -------
 * 
 * Update or insert any given set of Equipment to a vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @inventoryID
 * @businessUnitID - defaults to 
 * @EngineTypeCategoryID - 
 * @TransmissionTypeCategoryID - 
 * @DriveTrainCategoryID - 
 * @FuelCategoryID -
 * @createdBy	
 * @createdOn
 * @updatedBy
 * @updatedOn
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [merchandising].[AutoTraderPerfFetch]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT * FROM merchandising.AutoTraderPerfSummary
	END		
GO

GRANT EXECUTE ON [merchandising].[AutoTraderPerfFetch] TO MerchandisingUser 
GO







  