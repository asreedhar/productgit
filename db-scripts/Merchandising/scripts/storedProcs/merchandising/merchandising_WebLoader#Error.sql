if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[WebLoaderPhotos#Error]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[WebLoaderPhotos#Error]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [merchandising].[WebLoaderPhotos#Error]
	@InventoryID int,
	@Handle uniqueidentifier
AS
BEGIN
	
	UPDATE merchandising.WebLoaderPhotos
	SET Status = -1 
	WHERE InventoryID = @InventoryID AND Handle = @Handle
	
END 
GO

GRANT EXECUTE ON [merchandising].[WebLoaderPhotos#Error] TO MerchandisingUser 
GO