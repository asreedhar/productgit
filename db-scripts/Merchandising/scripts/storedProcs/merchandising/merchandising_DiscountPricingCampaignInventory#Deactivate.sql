-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-08-15
--				: 2014-08-05 - change to update/delete special price
-- Description:	Deactivate inventory / 
--				discount campaign
-- =============================================

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[merchandising].[DiscountPricingCampaignInventory#Deactivate]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [merchandising].[DiscountPricingCampaignInventory#Deactivate]
GO

CREATE PROCEDURE merchandising.DiscountPricingCampaignInventory#Deactivate
	@CampaignId int,
	@DeactivatingUser varchar(250),
	@InventoryId int = NULL,
	@ExclusionId tinyint = NULL
AS
BEGIN
	SET NOCOUNT ON

	BEGIN TRY
	
		-- wrap this in a transaction
		BEGIN TRANSACTION
		
		-- find all that are going to be effected
		SELECT CampaignID, InventoryId, OriginalNewCarPrice
			INTO #dpci
			FROM Merchandising.merchandising.DiscountPricingCampaignInventory
			WHERE CampaignId = @CampaignId
			AND (@InventoryId IS NULL OR InventoryId = @InventoryId)
			-- considered putting on and addition filter for 'active' tureen 8/5/2014

		-- de-active campaign inventory row
		UPDATE merchandising.DiscountPricingCampaignInventory
			SET Active = 0, 
				DeactivatedBy = @DeactivatingUser,
				DeactivatedDate = GETDATE()
			FROM merchandising.DiscountPricingCampaignInventory DiscountPricingCampaignInventory
			INNER JOIN #dpci ON DiscountPricingCampaignInventory.InventoryId = #dpci.inventoryID
								AND DiscountPricingCampaignInventory.CampaignID = #dpci.CampaignID
				
		
		-- update new car price if present before campaign
		UPDATE NewCarPricing
			SET SpecialPrice = #dpci.OriginalNewCarPrice
			FROM builder.NewCarPricing NewCarPricing
			INNER JOIN #dpci ON NewCarPricing.InventoryID = #dpci.InventoryId
			WHERE #dpci.OriginalNewCarPrice IS NOT NULL
		
		-- delete new car pricing if no previous price
		DELETE NewCarPricing
			FROM builder.NewCarPricing NewCarPricing
			INNER JOIN #dpci ON NewCarPricing.InventoryID = #dpci.InventoryId
			WHERE #dpci.OriginalNewCarPrice IS NULL
			
		-- insert exclusion type if a exclusion if given FB: 30572 - Manual changed price
		if @ExclusionId IS NOT NULL
			INSERT INTO merchandising.DiscountPricingCampaignExclusion
				(CampaignID, InventoryId, ExclusionId, CreationDate, CreatedBy
				)
				SELECT CampaignID, InventoryId, @ExclusionId, GETDATE(), @DeactivatingUser
					FROM #dpci
					WHERE NOT EXISTS ( -- don't try to insert a row that is already there.
						SELECT *
						FROM merchandising.DiscountPricingCampaignExclusion dpce
						WHERE dpce.InventoryId = #dpci.InventoryId
						AND dpce.CampaignID = #dpci.CampaignID
					)
			
		-- return the row to the app for processing
		SELECT #dpci.CampaignID, #dpci.InventoryId, DiscountPricingCampaignInventory.Active, OriginalNewCarPrice = NewCarPricing.SpecialPrice, DiscountPricingCampaignInventory.ActivatedBy, DiscountPricingCampaignInventory.DeactivatedBy
			FROM #dpci
			INNER JOIN merchandising.DiscountPricingCampaignInventory DiscountPricingCampaignInventory ON #dpci.CampaignID = DiscountPricingCampaignInventory.CampaignID
																										AND  #dpci.InventoryId = DiscountPricingCampaignInventory.InventoryId
			LEFT OUTER JOIN builder.NewCarPricing NewCarPricing ON #dpci.InventoryId = NewCarPricing.InventoryID
		
		-- the dishes are down, commit!!
		COMMIT TRANSACTION


		-- previous code
		--UPDATE Merchandising.merchandising.DiscountPricingCampaignInventory
		--SET Active = 0
		--,DeactivatedBy = @DeactivatingUser
		--,DeactivatedDate = GETDATE()
		--WHERE 
		--	CampaignId = @CampaignId
		--	AND (@InventoryId IS NULL OR InventoryId = @InventoryId)
			
		
    END TRY
    BEGIN CATCH
    
    		ROLLBACK TRANSACTION

			DECLARE @ErrorMessage NVARCHAR(4000),
			  @ErrorNumber INT,
			  @ErrorSeverity INT,
			  @ErrorState INT,
			  @ErrorLine INT,
			  @ErrorProcedure NVARCHAR(200) ;

				-- Assign variables to error-handling functions that 
				-- capture information for RAISERROR.
			SELECT @ErrorNumber = ERROR_NUMBER(), @ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE(), @ErrorLine = ERROR_LINE(),
					@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-') ;

				-- Building the message string that will contain original
				-- error information.
			SET @ErrorMessage = N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' +
					'Message: ' + ERROR_MESSAGE() ;

				-- Raise an error: msg_str parameter of RAISERROR will contain
				-- the original error information.
			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, -- parameter: original error number.
			  @ErrorSeverity, -- parameter: original error severity.
			  @ErrorState, -- parameter: original error state.
			  @ErrorProcedure, -- parameter: original error procedure name.
			  @ErrorLine-- parameter: original error line number.
					)

    END CATCH
	
	
	
	
	
	
	
	SET NOCOUNT OFF



END
GO

grant execute on [merchandising].[DiscountPricingCampaignInventory#Deactivate] to MerchandisingUser
go

