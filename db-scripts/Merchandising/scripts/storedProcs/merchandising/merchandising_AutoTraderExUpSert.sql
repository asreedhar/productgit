if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[AutoTraderExUpsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[AutoTraderExUpsert]
GO

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


-- =============================================
-- Author:		<Fitzpatrick, Benin>
-- Create date: <2/2/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: merchandising_AutoTraderExUpSert.sql,v 1.3 2010/03/26 22:03:32 bsombke Exp $
 * 
 * Summary finally
 * -------
 * 
 * Update or insert any given set of Equipment to a vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @inventoryID
 * @businessUnitID - defaults to 
 * @EngineTypeCategoryID - 
 * @TransmissionTypeCategoryID - 
 * @DriveTrainCategoryID - 
 * @FuelCategoryID -
 * @createdBy	
 * @createdOn
 * @updatedBy
 * @updatedOn
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [merchandising].[AutoTraderExUpsert]
	
	@businessUnitID int ,
	@month datetime,
	@AvgDayNew int ,
	@AvgDayUsed int,
	@AvgDayTotal int ,
	@TimesSeenInSearch int ,
	@BannerAdImpressions int ,
	@FindDealerSearchImpressions int ,
	@TotalExposure int ,
	@ViewedMaps int ,
	@DetailPagesViewed int ,
	@PrintableAdsRequested int ,
	@ViewedWebsiteOrInventory int ,
	@FindDealerClickThrus int ,
	@BannerAdClickThrus int ,
	@TotalActivity int ,
	@CallsToDealership int ,
	@EmailsToDealership int ,
	@SecureCreditApplications int ,
	@TotalTrackableProspects int ,
	@TotalShoppersInArea int 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF EXISTS (SELECT 1 FROM merchandising.AutoTraderExSummary WHERE businessUnitID = @businessUnitID AND month = @month)
	BEGIN
		UPDATE merchandising.AutoTraderExSummary
		SET 
		businessUnitID = @businessUnitId,
		month = @month,
		AvgDayNew = @AvgDayNew ,
		AvgDayUsed = @AvgDayUsed,
		AvgDayTotal = AvgDayTotal,
		TimesSeenInSearch = @TimesSeenInSearch,
		BannerAdImpressions = @BannerAdImpressions,
		FindDealerSearchImpressions = @FindDealerSearchImpressions,
		TotalExposure = @TotalExposure,
		ViewedMaps = @ViewedMaps,
		DetailPagesViewed  = @DetailPagesViewed,
		PrintableAdsRequested  = @PrintableAdsRequested,
		ViewedWebsiteOrInventory  = @ViewedWebsiteOrInventory,
		FindDealerClickThrus  = @FindDealerClickThrus,
		BannerAdClickThrus  = @BannerAdClickThrus,
		TotalActivity = @TotalActivity,
		CallsToDealership = @CallsToDealership,
		EmailsToDealership  = @EmailsToDealership,
		SecureCreditApplications = @SecureCreditApplications ,
		TotalTrackableProspects = @TotalTrackableProspects,
		TotalShoppersInArea = @TotalShoppersInArea
		WHERE
			businessUnitID = @businessUnitID
			and month = @month 
	END
	ELSE
	BEGIN
	INSERT INTO merchandising.AutoTraderExSummary
	(
		businessUnitID ,
		month,
		AvgDayNew  ,
		AvgDayUsed ,
		AvgDayTotal  ,
		TimesSeenInSearch  ,
		BannerAdImpressions  ,
		FindDealerSearchImpressions  ,
		TotalExposure  ,
		DetailPagesViewed  ,
		ViewedMaps ,
		PrintableAdsRequested  ,
		ViewedWebsiteOrInventory  ,
		FindDealerClickThrus  ,
		BannerAdClickThrus  ,
		TotalActivity  ,
		CallsToDealership  ,
		EmailsToDealership  ,
		SecureCreditApplications  ,
		TotalTrackableProspects  ,
		TotalShoppersInArea  
	 )
	VALUES
	(
		@businessUnitID  ,
		@month, 
		@AvgDayNew  ,
		@AvgDayUsed ,
		@AvgDayTotal  ,
		@TimesSeenInSearch  ,
		@BannerAdImpressions  ,
		@FindDealerSearchImpressions  ,
		@TotalExposure  ,
		@DetailPagesViewed  ,
		@ViewedMaps ,
		@PrintableAdsRequested  ,
		@ViewedWebsiteOrInventory  ,
		@FindDealerClickThrus  ,
		@BannerAdClickThrus  ,
		@TotalActivity  ,
		@CallsToDealership  ,
		@EmailsToDealership  ,
		@SecureCreditApplications  ,
		@TotalTrackableProspects  ,
		@TotalShoppersInArea  
	)
	END		
END
GO

GRANT EXECUTE ON [merchandising].[AutoTraderExUpsert] TO MerchandisingUser 
GO







  