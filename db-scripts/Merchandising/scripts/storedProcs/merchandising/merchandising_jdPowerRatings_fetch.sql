if exists (select * from dbo.sysobjects where id = object_id(N'[Merchandising].[JdPowerRatings#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Merchandising].[JdPowerRatings#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <4/6/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: merchandising_jdPowerRatings_fetch.sql,v 1.3 2010/03/11 19:58:48 bsombke Exp $
 * 
 * Summary
 * -------
 * 
 * fetch current ad id in accelerator
 * 
 * 
 * Parameters
 * ----------
 * @ModelYear    -  Vehicle Model Year
 * @Make         -  Vehicle Make Name
 * @Model        -  Vehicle Model Name
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Merchandising].[JdPowerRatings#Fetch]
	-- Add the parameters for the stored procedure here
	@ModelYear int,
	@Make varchar(30),
	@Model varchar(30),
	@SegmentID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT * FROM merchandising.jdPowerRatings
	WHERE [Year] = @ModelYear
	AND [Make] = @Make
	AND [Model] = @Model
	AND ([SegmentID] = @SegmentID OR [SegmentID] = 0)
		

END

GO

GRANT EXECUTE ON [Merchandising].[JdPowerRatings#Fetch] TO MerchandisingUser 
GO