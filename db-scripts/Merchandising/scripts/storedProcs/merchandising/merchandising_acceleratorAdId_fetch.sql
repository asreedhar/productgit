if exists (select * from dbo.sysobjects where id = object_id(N'[Merchandising].[AcceleratorAdId#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Merchandising].[AcceleratorAdId#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <3/31/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: merchandising_acceleratorAdId_fetch.sql,v 1.1 2009/04/06 22:56:22 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch current ad id in accelerator
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Merchandising].[AcceleratorAdId#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@InventoryId int,
	@AdvertisementId int output
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	EXEC Interface.currentAdvertisementId#Fetch @BusinessUnitId, 1, @InventoryId, 1, @AdvertisementId output
	
		

END

GO

GRANT EXECUTE ON [Merchandising].[AcceleratorAdId#Fetch] TO MerchandisingUser 
GO