if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[WebLoaderPhotos#Insert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[WebLoaderPhotos#Insert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [merchandising].[WebLoaderPhotos#Insert]
	@InventoryID int,
	@Taken DateTime,
	@Batch uniqueidentifier,
	@Handle uniqueidentifier,
	@Position int,
	@UserName varchar(50),
	@BatchCount INT OUTPUT
AS
BEGIN
	
	DECLARE @out TABLE (photoid INT)
	
	INSERT INTO merchandising.WebLoaderPhotos (InventoryID, Handle, Batch, Position, Taken, UserName, Received, Status)
	OUTPUT inserted.photoid INTO @out
	VALUES(@InventoryID, @Handle, @Batch, @Position, @Taken, @UserName, GETDATE(), 0)

	SELECT @BatchCount = Count(*)
	FROM merchandising.WebLoaderPhotos 
	WHERE Batch = @Batch AND photoid <= (SELECT MAX(photoid) FROM @out)
	
END 
GO

GRANT EXECUTE ON [merchandising].[WebLoaderPhotos#Insert] TO MerchandisingUser 
GO