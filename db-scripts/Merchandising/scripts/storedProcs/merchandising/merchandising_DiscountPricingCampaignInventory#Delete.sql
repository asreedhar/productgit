-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-08-15
-- Description:	delete inventory / 
--				discount campaign
-- =============================================

USE Merchandising
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[merchandising].[DiscountPricingCampaignInventory#Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [merchandising].[DiscountPricingCampaignInventory#Delete]
GO

CREATE PROCEDURE merchandising.DiscountPricingCampaignInventory#Delete
	@CampaignId int,
	@InventoryId int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM Merchandising.merchandising.DiscountPricingCampaignInventory
	WHERE 
		CampaignId = @CampaignId
		AND (@InventoryId IS NULL OR InventoryId = @InventoryId)
END
GO

grant execute on [merchandising].[DiscountPricingCampaignInventory#Delete] to MerchandisingUser
go

