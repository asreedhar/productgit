if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[WebLoaderPhotos#Order]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[WebLoaderPhotos#Order]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [merchandising].[WebLoaderPhotos#Order]
	@Batch uniqueidentifier
AS

BEGIN
	
UPDATE P
SET P.Position = WLP.Position
FROM merchandising.WebLoaderPhotos P
JOIN (SELECT P.Handle, ROW_NUMBER() OVER(ORDER BY P.Taken) AS Position
	FROM merchandising.WebLoaderPhotos P
	WHERE P.Batch = @Batch) WLP ON P.Handle = WLP.Handle
	

SELECT WP.InventoryID, I.StockNumber, V.Vin, Handle, Taken, Position
FROM merchandising.WebLoaderPhotos WP
JOIN IMT.dbo.Inventory I on I.InventoryID = WP.InventoryID
JOIN IMT.dbo.Vehicle V on I.vehicleid = V.vehicleid
WHERE Batch = @Batch
ORDER BY Position

END 
GO

GRANT EXECUTE ON [merchandising].[WebLoaderPhotos#Order] TO MerchandisingUser 
GO