-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-08-15
-- Description:	fetch inventory / 
--				discount campaign
-- =============================================

USE Merchandising
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[merchandising].[DiscountPricingCampaignInventory#Fetch]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [merchandising].[DiscountPricingCampaignInventory#Fetch]
GO

CREATE PROCEDURE merchandising.DiscountPricingCampaignInventory#Fetch
	@CampaignId int,
	@InventoryId int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT dpci.CampaignID, dpci.InventoryId, dpci.OriginalNewCarPrice, dpci.Active
	, dpci.ActivatedBy, dpci.DeactivatedBy, dpci.ActivatedDate, dpci.DeactivatedDate 
	FROM Merchandising.merchandising.DiscountPricingCampaignInventory dpci
	WHERE 
		CampaignId = @CampaignId
		AND (@InventoryId IS NULL OR InventoryId = @InventoryId)
END
GO

grant execute on [merchandising].[DiscountPricingCampaignInventory#Fetch] to MerchandisingUser
go

