IF OBJECT_ID('merchandising.VehicleAutoLoadSettings#Fetch') IS NOT NULL
	DROP PROCEDURE merchandising.VehicleAutoLoadSettings#Fetch
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE merchandising.VehicleAutoLoadSettings#Fetch
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT	vals.ManufacturerID, m.ManufacturerName, vals.IsAutoLoadEnabled
	  FROM	merchandising.VehicleAutoLoadSettings vals
			JOIN VehicleCatalog.Chrome.Manufacturers m ON m.ManufacturerID = vals.ManufacturerID AND m.CountryCode = 1
		
END
GO

GRANT EXECUTE ON merchandising.VehicleAutoLoadSettings#Fetch TO MerchandisingUser 
GO
