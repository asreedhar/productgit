-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-08-15
-- Description:	fetch inventory / 
--				discount campaign by campaign id
-- =============================================

USE Merchandising
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[merchandising].[DiscountPricingCampaign#Fetch]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [merchandising].[DiscountPricingCampaign#Fetch]
GO

CREATE PROCEDURE merchandising.DiscountPricingCampaign#Fetch
	@BusinessUnitId int,
	@CampaignId int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT dpc.CampaignID, dpc.BusinessUnitId, dpc.DiscountType, dpc.DealerDiscount, dpc.ManufacturerRebate, dpc.CreationDate, dpc.ExpirationDate, dpc.CreatedBy, dpc.CampaignFilter, dpc.HasBeenExpired 
	FROM Merchandising.merchandising.DiscountPricingCampaign dpc
	WHERE 
		BusinessUnitId = @BusinessUnitId
		AND (@CampaignId IS NULL OR CampaignId = @CampaignId)
		
	ORDER BY CampaignID
END
GO

grant execute on [merchandising].[DiscountPricingCampaign#Fetch] to MerchandisingUser
go

