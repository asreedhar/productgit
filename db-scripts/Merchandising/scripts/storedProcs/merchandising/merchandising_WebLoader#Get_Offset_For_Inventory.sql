if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[WebLoaderPhotos#Get_Offset_For_Inventory]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[WebLoaderPhotos#Get_Offset_For_Inventory]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [merchandising].[WebLoaderPhotos#Get_Offset_For_Inventory]
	@InventoryID int
AS
BEGIN

	SELECT ISNULL(MAX(PositionOffset), 0) AS Offset FROM merchandising.WebLoaderPhotos WHERE InventoryID = @InventoryID

END 
GO

GRANT EXECUTE ON [merchandising].[WebLoaderPhotos#Get_Offset_For_Inventory] TO MerchandisingUser 
GO