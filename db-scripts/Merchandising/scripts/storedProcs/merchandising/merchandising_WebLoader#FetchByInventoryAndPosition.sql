if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[WebLoaderPhotos#FetchByInventoryAndPosition]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[WebLoaderPhotos#FetchByInventoryAndPosition]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE PROCEDURE [merchandising].[WebLoaderPhotos#FetchByInventoryAndPosition]
       @InventoryID INT,
       @Position INT,
       @Login VARCHAR(100)
AS
BEGIN
       
        SELECT    I.StockNumber, V.Vin, Handle, Taken, Position, WP.Batch, M.FirstName, M.LastName  
        FROM merchandising.WebLoaderPhotos WP  
        JOIN IMT.dbo.Inventory I on I.InventoryID = WP.InventoryID  
        JOIN IMT.dbo.Vehicle V on I.vehicleid = V.vehicleid  
        JOIN IMT.dbo.Member M on M.Login = @Login
        WHERE WP.InventoryID = @InventoryID AND Position=@Position
END 
 
GO


GRANT EXECUTE ON [merchandising].[WebLoaderPhotos#FetchByInventoryAndPosition] TO MerchandisingUser 
GO