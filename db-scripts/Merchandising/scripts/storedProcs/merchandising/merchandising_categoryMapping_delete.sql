if exists (select * from dbo.sysobjects where id = object_id(N'[Merchandising].[CategoryMapping#Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Merchandising].[CategoryMapping#Delete]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/08/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: merchandising_categoryMapping_delete.sql,v 1.1 2009/03/26 21:14:06 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * delete all mappings for a given listing option
 * 
 * 
 * Parameters
 * ----------
 * @OptionId - the listing option id
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Merchandising].[CategoryMapping#Delete]
	-- Add the parameters for the stored procedure here		
	@OptionId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
		DELETE FROM [Merchandising].[OptionsMapping]
		WHERE
			optionId = @OptionId


END

GO

GRANT EXECUTE ON [Merchandising].[CategoryMapping#Delete] TO MerchandisingUser 
GO