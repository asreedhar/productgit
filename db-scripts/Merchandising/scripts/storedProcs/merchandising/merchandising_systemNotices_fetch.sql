if exists (select * from dbo.sysobjects where id = object_id(N'[Merchandising].[SystemNotices#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Merchandising].[SystemNotices#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/08/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: merchandising_systemNotices_fetch.sql,v 1.1 2009/11/09 17:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch all the active system notices
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Merchandising].[SystemNotices#Fetch]
	-- Add the parameters for the stored procedure here		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
		SELECT messageText
		FROM merchandising.SystemMessages
		WHERE active = 1 and getdate() < expiresOn 		

END

GO

GRANT EXECUTE ON [Merchandising].[SystemNotices#Fetch] TO MerchandisingUser 
GO