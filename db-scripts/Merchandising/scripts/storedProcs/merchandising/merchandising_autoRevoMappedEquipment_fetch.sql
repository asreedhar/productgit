if exists (select * from dbo.sysobjects where id = object_id(N'[Merchandising].[AutoRevoMappedEquipment#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Merchandising].[AutoRevoMappedEquipment#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/08/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: merchandising_autoRevoMappedEquipment_fetch.sql,v 1.1 2009/04/08 23:52:39 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch the mappings for an inventory item's generic equipment
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Merchandising].[AutoRevoMappedEquipment#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@InventoryId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
		SELECT DISTINCT [AutoRevoOptionID]		
		FROM builder.VehicleOptions vo
		INNER JOIN [Merchandising].[AutoRevoMapping] arm
		ON arm.chromeCategoryId = vo.optionId
		WHERE vo.businessUnitId = @BusinessUnitId
		AND vo.inventoryId = @InventoryId
		ORDER BY autoRevoOptionId ASC

END

GO

GRANT EXECUTE ON [Merchandising].[AutoRevoMappedEquipment#Fetch] TO MerchandisingUser 
GO