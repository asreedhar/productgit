-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-08-15
-- Description:	Insert discount pricing data
-- =============================================
USE Merchandising
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[merchandising].[DiscountPricingCampaign#Insert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [merchandising].[DiscountPricingCampaign#Insert]
GO

CREATE PROCEDURE merchandising.DiscountPricingCampaign#Insert
	 @BusinessUnitId int
	,@DiscountType int
	,@DealerDiscount decimal(9,2)
	,@ManufacturerRebate decimal(9,2)
	,@ExpirationDate datetime
    ,@CreatedBy varchar(250)
	,@CampaignFilter varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DECLARE @Results table(
		[CampaignID] [int] NOT NULL,
		[BusinessUnitId] [int] NOT NULL,
		[DiscountType] [int] NOT NULL,
		[DealerDiscount] [decimal](9, 2) NOT NULL,
		[CreationDate] [datetime] NOT NULL,
		[ExpirationDate] [datetime] NOT NULL,
		[CreatedBy] [varchar](250) NOT NULL,
        	[CampaignFilter] [varchar](max) NOT NULL,
		[ManufacturerRebate] [decimal](9, 2) NOT NULL,
		[HasBeenExpired] [bit] NULL
	);
	
	INSERT INTO [Merchandising].[merchandising].[DiscountPricingCampaign]
		OUTPUT INSERTED.CampaignID, INSERTED.BusinessUnitId, INSERTED.DiscountType, INSERTED.DealerDiscount, INSERTED.CreationDate,
			INSERTED.ExpirationDate, INSERTED.CreatedBy, INSERTED.CampaignFilter, INSERTED.ManufacturerRebate, INSERTED.HasBeenExpired
			INTO @Results
     VALUES
           (@BusinessUnitId
           ,@DiscountType
           ,@DealerDiscount
           ,GETDATE()
           ,@ExpirationDate
           ,@CreatedBy
           ,@CampaignFilter
           ,@ManufacturerRebate
           ,0)

	SELECT CampaignID, BusinessUnitId, DiscountType, DealerDiscount, ManufacturerRebate, CreationDate, ExpirationDate, CreatedBy, CampaignFilter, HasBeenExpired
	FROM @Results

END
GO

grant execute on [merchandising].[DiscountPricingCampaign#Insert] to MerchandisingUser
go

