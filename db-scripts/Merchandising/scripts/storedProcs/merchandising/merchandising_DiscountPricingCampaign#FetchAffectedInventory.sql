-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-08-20
-- Description:	fetch affected inventory by 
--				businessunit and optional 
--				campaign id
-- Modified:	2014-12-29: FB - 31953 only 
--				include active inventory
-- =============================================

USE Merchandising
GO

SET ANSI_NULLS ON
GO
	SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[merchandising].[DiscountPricingCampaign#FetchAffectedInventory]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [merchandising].[DiscountPricingCampaign#FetchAffectedInventory]
GO

CREATE PROCEDURE merchandising.DiscountPricingCampaign#FetchAffectedInventory
	@BusinessUnitId int,
	@CampaignId int = NULL
AS
BEGIN
	SET NOCOUNT ON;

	SELECT dpc.CampaignID, dpc.BusinessUnitId, dpc.DiscountType, dpc.DealerDiscount, dpc.ManufacturerRebate, dpc.CreationDate, dpc.ExpirationDate, dpc.CreatedBy, dpc.CampaignFilter, dpc.HasBeenExpired,
		dpci.InventoryId, dpci.OriginalNewCarPrice, dpci.Active, dpci.ActivatedBy, dpci.DeactivatedBy
	FROM Merchandising.merchandising.DiscountPricingCampaign dpc
	JOIN Merchandising.merchandising.DiscountPricingCampaignInventory dpci ON dpci.CampaignID = dpc.CampaignID
	WHERE 
		BusinessUnitId = @BusinessUnitId
		AND (@CampaignId IS NULL OR dpc.CampaignID = @CampaignId)
		AND dpci.Active = 1
	ORDER BY dpc.CampaignID, dpci.InventoryId

END
GO

grant execute on [merchandising].[DiscountPricingCampaign#FetchAffectedInventory] to MerchandisingUser
go

