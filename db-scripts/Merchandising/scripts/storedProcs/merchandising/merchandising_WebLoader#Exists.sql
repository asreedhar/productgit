if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[WebLoaderPhotos#Exists]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[WebLoaderPhotos#Exists]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [merchandising].[WebLoaderPhotos#Exists]
	@InventoryID int,
	@Handle uniqueidentifier 
AS
BEGIN

	IF EXISTS (SELECT * FROM merchandising.WebLoaderPhotos WHERE InventoryID = @InventoryID AND Handle = @Handle)
		select 1 as 'Exist' else select 0 as 'Exist'

END 
GO

GRANT EXECUTE ON [merchandising].[WebLoaderPhotos#Exists] TO MerchandisingUser 
GO