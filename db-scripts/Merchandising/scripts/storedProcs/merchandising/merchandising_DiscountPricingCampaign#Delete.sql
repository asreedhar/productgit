-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-08-15
-- Description:	delete discount campaign
-- =============================================

USE Merchandising
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[merchandising].[DiscountPricingCampaign#Delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [merchandising].[DiscountPricingCampaign#Delete]
GO

CREATE PROCEDURE merchandising.DiscountPricingCampaign#Delete
	@BusinessUnitId int,
	@CampaignId int
AS
BEGIN
	SET NOCOUNT ON;

	DELETE
	FROM Merchandising.merchandising.DiscountPricingCampaign
	WHERE 
		BusinessUnitId = @BusinessUnitId
		AND CampaignId = @CampaignId
END
GO

grant execute on [merchandising].[DiscountPricingCampaign#Delete] to MerchandisingUser
go

