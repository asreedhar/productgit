IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[merchandising].[DealerSiteCredentials#delete]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [merchandising].[DealerSiteCredentials#delete]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [merchandising].[DealerSiteCredentials#delete]
	@BusinessUnitId int,
	@SiteId int,
	@credentialVersion timestamp = null
	
AS
BEGIN
	SET NOCOUNT ON;
	
	-- check if there is an actual update to perform
	IF NOT EXISTS ( SELECT  1
						FROM    settings.Dealer_SiteCredentials
						WHERE   businessUnitId = @BusinessUnitId
								AND siteId = @SiteId)
		return -- nothing to do, just bail
		
	-- we know that this delete is real, must have a rowVersion for concurrency check
	if @credentialVersion IS NULL
		begin
			RAISERROR('This credential cannot be deleted because a row version was not sent for a concurrency check', 16, 1)
			return
		end
		
		
	DELETE FROM settings.Dealer_SiteCredentials 
		WHERE businessUnitId = @BusinessUnitId 
		AND siteId = @SiteId
		AND credentialVersion = @credentialVersion

	if @@ROWCOUNT != 1
		begin
			RAISERROR('This credential update failed becuase it has been updated by another user', 16, 1)
			return
		end
	
END		
GO

GRANT EXECUTE ON [merchandising].[DealerSiteCredentials#delete] TO MerchandisingUser 
GO
