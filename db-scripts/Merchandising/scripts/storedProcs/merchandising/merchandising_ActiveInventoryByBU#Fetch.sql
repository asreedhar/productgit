-- ======================================================
-- Author:		Arjun Seshadri
-- Create date: 2014-08-6
-- Description:	Get a list of active and recently active 
--				vehicles for BusinessUnit @BusinessUnitId
-- ======================================================
USE Merchandising
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[merchandising].[ActiveInventoryByBU#Fetch]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE merchandising.ActiveInventoryByBU#Fetch
GO

CREATE PROCEDURE [merchandising].[ActiveInventoryByBU#Fetch] 
	@BusinessUnitId int, 
	@RecentlyActiveDays tinyint = 45,
	@NewOnly bit = 1
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
		inv.InventoryID, inv.VIN
	FROM
		FLDW..InventoryActive inv

	WHERE
		inv.BusinessUnitID = @BusinessUnitId
		AND (	
			inv.InventoryActive = 1 
		)
		AND (@NewOnly = 0 OR InventoryType = @NewOnly)
END

GO

GRANT EXECUTE on merchandising.ActiveInventoryByBU#Fetch to MerchandisingUser

GO