if exists (select * from dbo.sysobjects where id = object_id(N'[Merchandising].[RefreshAvailableMappings]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Merchandising].[RefreshAvailableMappings]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/08/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: merchandising_refreshAvailableMappings.sql,v 1.4 2009/12/14 15:54:30 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * delete all mappings for a given listing option
 * 
 * 
 * Parameters
 * ----------
 * @OptionId - the listing option id
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Merchandising].[RefreshAvailableMappings]
	-- Add the parameters for the stored procedure here		
	@UseLotDataFlag	bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

CREATE TABLE #tmpOps (
	OptionID int NOT NULL,
	Description varchar(200) NOT NULL,	
	providerId int NOT NULL,
	optionCount int NOT NULL
	
)

	INSERT INTO #tmpOps EXEC Listing.AllListingOptions#Fetch	

	--clear out old counts...
	UPDATE merchandising.optionsMapping
	SET optionCount = 0
	
	INSERT INTO merchandising.optionsMapping (optionCount,description,optionid, providerId)
	SELECT tm.optionCount, tm.Description, tm.OptionID, tm.providerId
	FROM #tmpOps tm
	LEFT JOIN merchandising.optionsMapping om
	ON om.optionid = tm.optionid
	WHERE om.description IS NULL

	UPDATE om
	SET om.optionCount = tm.optionCount, om.providerId = tm.providerId
	FROM
	merchandising.optionsMapping om
	LEFT JOIN #tmpOps tm
	ON tm.optionId = om.optionId
	WHERE NOT tm.optionCount IS NULL

	
DROP TABLE #tmpOps
END

GO

GRANT EXECUTE ON [Merchandising].[RefreshAvailableMappings] TO MerchandisingUser 
GO