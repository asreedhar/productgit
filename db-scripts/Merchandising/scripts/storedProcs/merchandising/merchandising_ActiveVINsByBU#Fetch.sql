-- ======================================================================
-- Author:		Arjun Seshadri
-- Create date: 2014-08-6
-- Description:	Get a list of active VINs for BusinessUnit @BusinessUnitId
-- =======================================================================
USE Merchandising
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[merchandising].[ActiveVINsByBU#Fetch]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE merchandising.ActiveVINsByBU#Fetch
GO

CREATE PROCEDURE [merchandising].[ActiveVINsByBU#Fetch] 
	@BusinessUnitId int 
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT 
		VIN
	FROM
		FLDW.dbo.InventoryActive
		LEFT JOIN Merchandising.builder.OptionsConfiguration 
				  ON OptionsConfiguration.businessUnitID = InventoryActive.BusinessUnitID
				  AND OptionsConfiguration.inventoryId = InventoryActive.InventoryID
	WHERE
		InventoryActive.BusinessUnitID = @BusinessUnitId
		AND ISNULL(OptionsConfiguration.doNotPostFlag, 0) = 0

END

GO

GRANT EXECUTE on merchandising.ActiveVINsByBU#Fetch to MerchandisingUser

GO
