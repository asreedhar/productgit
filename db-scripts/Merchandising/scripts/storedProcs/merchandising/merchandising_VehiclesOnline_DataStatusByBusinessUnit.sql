if  exists (select * from sys.objects where object_id = object_id(N'[merchandising].[VehiclesOnline#DataStatusByBusinessUnit]') and type in (N'P', N'PC'))
drop procedure [merchandising].[VehiclesOnline#DataStatusByBusinessUnit]
GO

create procedure merchandising.VehiclesOnline#DataStatusByBusinessUnit(@businessUnitId int)
-- Retrieve status about each feed by business unit by feed.
as
begin

set nocount on

-- Setup mapping between DestinationId and DatasourceInterfaceId from DataFeeds.dbo.DatafeedFileType
declare @map table (DestinationId int, DatasourceInterfaceId int, primary key (destinationId))
insert into @map values (2, 14) -- Map AutoTrader to FetchDotCom-Response/VehicleOnline

select @BusinessUnitId, map.DestinationId, 
	dfi.LastSuccessfulDateLoaded, 
	cast(case when len(dls.userName) > 0 and len(dls.[password]) > 0 then 1 else 0 end as bit) as hasCredentials,
	cast(case when dls.dateLoginFailure is null or dls.dateLoginFailureCleared >= dateLoginFailure 
		then 0 else 1 end as bit) as hasInvalidCredentials

from @map as map

	left join settings.DealerListingSites as dls
	on map.DestinationId = dls.DestinationId 
		and dls.BusinessUnitId = @BusinessUnitId
	left join merchandising.DealerFeedInfo as dfi
	on map.DestinationId = dfi.DestinationId 
		and map.DatasourceInterfaceId = dfi.DatasourceInterfaceId
		and dfi.BusinessUnitId = @BusinessUnitId
	join imt.dbo.businessUnitRelationship bur 
		on bur.businessUnitId = dls.businessUnitId		

where 

	(bur.parentId = 100068 and 1=2)	-- Hack: Don't return results for Hendrick's group (BUGZID 24363)

	or 

	(bur.parentId != 100068 and dls.BusinessUnitId = @BusinessUnitId)
	
		
end
GO

GRANT EXECUTE ON merchandising.VehiclesOnline#DataStatusByBusinessUnit TO MerchandisingUser 
GO