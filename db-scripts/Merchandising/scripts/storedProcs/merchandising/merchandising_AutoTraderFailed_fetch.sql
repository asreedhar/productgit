if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[AutoTraderFailed#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [merchandising].[AutoTraderFailed#Fetch]
GO

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

-- =============================================
-- Author:		<Sombke, Bryant>
-- Create date: <4/5/10>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * Summary finally
 * -------
 * Fetch the list of business units whose AutoTrader logins are currently failing.
 * 
 * Parameters
 * ----------
 *
 * Exceptions
 * ----------
 * 
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [merchandising].[AutoTraderFailed#Fetch]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT businessUnitID, dateLoginFailure as dateFailed, dateLoginFailureCleared as dateCleared 
	FROM settings.DealerListingSites
	WHERE destinationID = 2 -- 2 = AutoTrader.com
END
GO

GRANT EXECUTE ON [merchandising].[AutoTraderFailed#Fetch] TO MerchandisingUser 
GO