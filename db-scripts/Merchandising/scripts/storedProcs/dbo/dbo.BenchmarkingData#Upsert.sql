-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-04-24
-- Description:	Set Benchmarking numbers by month
-- =============================================
USE Merchandising
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[BenchmarkingData#Upsert]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[BenchmarkingData#Upsert]
GO

CREATE PROCEDURE dbo.BenchmarkingData#Upsert 
	@ID int = NULL,
	@Year int, 
	@Month int,
	@ImpressionCost decimal(18,2),
	@LeadCost decimal(18,2),
	@SiteId tinyint,
	@DealershipSegmentType tinyint,
	@VehicleType tinyint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @ID IS NULL
	BEGIN
		SELECT @ID = ID FROM [Merchandising].[dbo].BenchmarkingData 
		WHERE Year = @Year 
			AND Month = @Month 
			AND SiteId = @SiteId 
			AND VehicleType = @VehicleType 
			AND DealershipSegmentType = @DealershipSegmentType;
	END
	
	-- No record found... insert
	IF @ID IS NULL
	BEGIN
		INSERT INTO [Merchandising].[dbo].[BenchmarkingData] (Year, Month, ImpressionCost, LeadCost, SiteId, DealershipSegmentType, VehicleType)
		VALUES (@Year, @Month, @ImpressionCost, @LeadCost, @SiteId, @DealershipSegmentType, @VehicleType)
	END
	ELSE
	BEGIN
		UPDATE [Merchandising].[dbo].[BenchmarkingData]
		SET Year = @Year, Month = @Month, ImpressionCost = @ImpressionCost, LeadCost = @LeadCost, SiteId = @SiteId, DealershipSegmentType = @DealershipSegmentType, VehicleType = @VehicleType
		WHERE ID = @ID
	END
END
GO

GRANT EXECUTE ON [dbo].[BenchmarkingData#Upsert] TO MerchandisingUser
GO
