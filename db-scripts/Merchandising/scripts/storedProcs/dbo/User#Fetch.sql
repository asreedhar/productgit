use Merchandising
go

if object_id('dbo.User#Fetch', 'P') is not null
    drop procedure dbo.User#Fetch
go

create procedure dbo.User#Fetch
	@login varchar(100)
as

set nocount on
set transaction isolation level read uncommitted

-- Get IMT user info
select MemberID, Login, FirstName, LastName, MemberType 
from IMT.dbo.Member
where Login = @login

-- Get IMT dealership list
select BU.BusinessUnitId
from 
    IMT.dbo.Member M
    inner join IMT.dbo.MemberAccess MA 
        on M.MemberID = MA.MemberID
    inner join IMT.dbo.BusinessUnit BU
        on MA.BusinessUnitID = BU.BusinessUnitID
    inner join IMT.dbo.DealerPreference DP
        on BU.BusinessUnitID = DP.BusinessUnitID
where BU.BusinessUnitTypeID = 4
    and BU.Active = 1
    and DP.GoLiveDate is not null
    and M.Login = @login

-- Lookup Merchandising ApplicationId
declare @applicationId uniqueidentifier
set @applicationId = null

select @applicationId = ApplicationId
from Merchandising.dbo.aspnet_Applications
where LoweredApplicationName = N'merchandising'

-- Get Merchandising UserId
declare @userId uniqueidentifier
set @userId = null

select @userId = UserId
from Merchandising.dbo.aspnet_Users
where LoweredUserName = lower(@login)
        and ApplicationId = @applicationId

select @userId as UserId

-- Get list of Merchandising RoleNames
select R.Rolename
from Merchandising.dbo.aspnet_UsersInRoles UR
    inner join Merchandising.dbo.aspnet_Roles R
        on UR.RoleId = R.RoleId
where R.ApplicationId = @applicationId
    and UR.UserId = @userId
    
go

grant execute on dbo.User#Fetch to MerchandisingUser
go