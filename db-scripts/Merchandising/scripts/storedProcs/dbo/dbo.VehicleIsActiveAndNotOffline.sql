
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VehicleIsActiveAndNotOffline]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[VehicleIsActiveAndNotOffline]
GO

CREATE PROC dbo.[VehicleIsActiveAndNotOffline] @InventoryId INT
AS 
    BEGIN
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

		--DECLARE @BusinessUnitId int = 100148,
		--		@InventoryId int = 35460618
		SELECT Inventory.InventoryActive, ISNULL(OptionsConfiguration.doNotPostFlag, 0) AS IsOffline
		FROM IMT..Inventory
		LEFT JOIN Merchandising.builder.OptionsConfiguration 
				ON OptionsConfiguration.inventoryId = Inventory.InventoryID AND 
				   OptionsConfiguration.businessUnitID = Inventory.BusinessUnitID
		WHERE Inventory.InventoryID = @InventoryId

	END

    
GO

GRANT EXEC ON dbo.[VehicleIsActiveAndNotOffline] TO MerchandisingUser
GO
