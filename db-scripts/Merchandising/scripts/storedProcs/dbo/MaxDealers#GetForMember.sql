
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MaxDealers#GetForMember]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[MaxDealers#GetForMember]
GO


CREATE PROC dbo.MaxDealers#GetForMember @MemberId INT
AS 
    BEGIN

        DECLARE @MemberIsAdmin BIT
        SELECT  @MemberIsAdmin = CASE WHEN MemberType = 1 THEN 1
                                      ELSE 0
                                 END
        FROM    imt.dbo.Member
        WHERE   MemberID = @MemberId

        DECLARE @ActiveMaxDealers TABLE
            (
              BusinessUnitId INT ,
              BusinessUnit VARCHAR(40)
            )
        INSERT  INTO @ActiveMaxDealers
                ( BusinessUnitId ,
                  BusinessUnit
                )
                SELECT  bu.BusinessUnitId ,
                        bu.BusinessUnit
                FROM    imt.dbo.BusinessUnit bu
                        JOIN imt.dbo.DealerPreference dp ON dp.BusinessUnitID = bu.BusinessUnitID
                        JOIN imt.dbo.DealerUpgrade du ON du.BusinessUnitID = bu.BusinessUnitID
                WHERE   bu.Active = 1
                        AND bu.BusinessUnitTypeID = 4
                        AND du.DealerUpgradeCD = 24 -- MAX AD (Wanamaker)
                        AND du.EffectiveDateActive = 1
                        AND dp.GoLiveDate IS NOT NULL

        IF @MemberIsAdmin = 1 
            BEGIN

                SELECT  BusinessUnitID ,
                        BusinessUnit
                FROM    @ActiveMaxDealers
                ORDER BY BusinessUnit

            END
        ELSE 
            BEGIN
                SELECT  MD.BusinessUnitID ,
                        MD.BusinessUnit
                FROM    @ActiveMaxDealers MD
                        JOIN IMT.dbo.MemberAccess MA1 ON MA1.BusinessUnitID = MD.BusinessUnitID
                WHERE   MA1.MemberID = @MemberId
                ORDER BY BusinessUnit
            END

    END
    
GO

GRANT EXEC ON dbo.MaxDealers#GetForMember TO MerchandisingUser
GO

                    