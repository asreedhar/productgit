﻿IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[ELMAH_GetErrorXml]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[ELMAH_GetErrorXml]
GO

/* This Procedure was modified to point to a table schema: audit.ELMAH_Error, different than the default: dbo.ELMAH_Error. If new release versions of this Procedure are supplied and used, they must be modified with the correct database schema. */

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE [dbo].[ELMAH_GetErrorXml]
(
    @Application NVARCHAR(60),
    @ErrorId UNIQUEIDENTIFIER
)
AS

    SET NOCOUNT ON

    SELECT 
        [AllXml]
    FROM 
        [audit].[ELMAH_Error]
    WHERE
        [ErrorId] = @ErrorId
    AND
        [Application] = @Application

GO

GRANT EXECUTE ON [dbo].[ELMAH_GetErrorXml] TO merchandisingWebsite

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

