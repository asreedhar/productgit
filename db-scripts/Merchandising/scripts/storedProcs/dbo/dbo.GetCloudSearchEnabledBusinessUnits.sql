
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCloudSearchEnabledBusinessUnits]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].GetCloudSearchEnabledBusinessUnits
GO

CREATE PROC dbo.[GetCloudSearchEnabledBusinessUnits] 
	
AS 
    BEGIN
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

		SELECT businessUnitID AS BusinessUnitId, Handle AS OwnerHandle 
		FROM Merchandising.settings.Merchandising 
		JOIN Market.Pricing.Owner ON Owner.OwnerEntityID = Merchandising.businessUnitID 
		WHERE (MaxDigitalShowroom = 1 OR MAXForWebsite20 = 1 OR maxForSmartphone = 1)

	END  
GO

GRANT EXEC ON dbo.GetCloudSearchEnabledBusinessUnits TO MerchandisingUser
GO
