use Merchandising
go

if object_id('dbo.Dealers#Fetch', 'P') is not null
    drop procedure dbo.Dealers#Fetch
go

create procedure dbo.Dealers#Fetch
as

set nocount on
set transaction isolation level read uncommitted

select  DU.BusinessUnitId, cast(DU.DealerUpgradeCD as tinyint) as DealerUpgradeCD
from IMT.dbo.DealerUpgrade DU
    inner join IMT.dbo.BusinessUnit BU
        on DU.BusinessUnitID = BU.BusinessUnitID
where DU.Active = 1
    and DU.EffectiveDateActive = 1
    and BU.Active = 1;

select BU.BusinessUnitID, O.Handle, BU.BusinessUnitCode, BU.BusinessUnit, M.WebLoaderEnabled
from IMT.dbo.BusinessUnit BU
    left join Market.Pricing.Owner O
        on O.OwnerTypeID = 1 -- Dealer 
            and O.OwnerEntityID = BU.BusinessUnitID
    left join Merchandising.settings.Merchandising M
        on BU.BusinessUnitID = M.BusinessUnitID
where BU.Active = 1
    and BU.BusinessUnitTypeID = 4;
    
go

grant execute on dbo.Dealers#Fetch to MerchandisingUser
go
