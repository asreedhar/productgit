
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RecentlyActiveAndNotOffline]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].RecentlyActiveAndNotOffline
GO

CREATE PROC dbo.[RecentlyActiveAndNotOffline] 
	@OwnerHandle varchar(40),
	@BusinessUnitId int,
	@DeleteDate datetime
AS 
    BEGIN
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

		SELECT @OwnerHandle AS OwnerHandle, VIN AS Vin 
		FROM IMT.dbo.Inventory 
		JOIN IMT.dbo.Vehicle ON Vehicle.VehicleId = Inventory.VehicleId 
		LEFT JOIN Merchandising.builder.OptionsConfiguration 
			ON OptionsConfiguration.businessUnitID = Inventory.BusinessUnitID
			AND OptionsConfiguration.inventoryId = Inventory.InventoryID
		WHERE Inventory.BusinessUnitId = @BusinessUnitId AND (InventoryActive = 1 OR 
		Inventory.DeleteDt > @DeleteDate) AND ISNULL(doNotPostFlag, 0) = 0

	END  
GO

GRANT EXEC ON dbo.RecentlyActiveAndNotOffline TO MerchandisingUser
GO
