-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-04-24
-- Description:	Get Benchmarking numbers by month
-- =============================================
USE Merchandising
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[BenchmarkingData#Fetch]') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [dbo].[BenchmarkingData#Fetch]
GO

CREATE PROCEDURE dbo.BenchmarkingData#Fetch 
	@SiteId tinyint,
	@Year int, 
	@Month int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		ID, [Year], [Month], ImpressionCost, LeadCost, SiteId, DealershipSegmentType, VehicleType
	FROM [Merchandising].[dbo].[BenchmarkingData] 
	WHERE [SiteId] = @SiteId 
		AND [Year] = @Year 
		AND [Month] = @Month
END
GO

GRANT EXECUTE ON [dbo].[BenchmarkingData#Fetch] TO MerchandisingUser
GO

