use Merchandising

if object_id('workflow.UpdateOptionsConfigurationChromeStyleIdField') is not null
	drop procedure workflow.UpdateOptionsConfigurationChromeStyleIdField
go

-- This is a maintenance procedure.
-- Execute this stored procedure to set any missing ChromeStyleID values that are resolvable.
-- It would be appropriate to run this procedure after chrome data has been reloaded in 
-- case VIN matching has improved.

create procedure workflow.UpdateOptionsConfigurationChromeStyleIdField
    @MemberLogin varchar(30) = '#UpdateChromeStyleId',
    @BusinessUnitID int = null,
    @InventoryID int = null
as

set nocount on
set transaction isolation level read uncommitted

if @BusinessUnitID is null and @InventoryID is null
begin

	update oc 
		set ChromeStyleID = ch.ChromeStyleID,
			LastUpdatedOn = getdate(),
			LastUpdatedBy = @MemberLogin
		from Merchandising.builder.OptionsConfiguration oc
			inner join Merchandising.chrome.InventoryChromeStyle ch
			  on oc.BusinessUnitID = ch.BusinessUnitID
				and oc.InventoryID = ch.InventoryID
		where oc.ChromeStyleID = -1 
			and oc.ChromeStyleID <> ch.ChromeStyleID
end 
else if @BusinessUnitID is not null and @InventoryID is not null
begin

	update oc 
		set ChromeStyleID = ch.ChromeStyleID,
			LastUpdatedOn = getdate(),
			LastUpdatedBy = @MemberLogin
		from Merchandising.builder.OptionsConfiguration oc
			inner join Merchandising.chrome.InventoryChromeStyle ch
			  on oc.BusinessUnitID = ch.BusinessUnitID
				and oc.InventoryID = ch.InventoryID
		where oc.ChromeStyleID = -1 
			and oc.ChromeStyleID <> ch.ChromeStyleID
			and oc.BusinessUnitId = @BusinessUnitID
			and oc.InventoryID = @InventoryID

end
GO
        
GRANT EXECUTE ON workflow.UpdateOptionsConfigurationChromeStyleIdField TO MerchandisingUser 
GO