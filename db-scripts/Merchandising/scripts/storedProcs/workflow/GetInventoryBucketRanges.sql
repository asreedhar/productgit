use Merchandising
go

if object_id('workflow.GetInventoryBucketRanges') is not null
	drop procedure workflow.GetInventoryBucketRanges
go

create procedure workflow.GetInventoryBucketRanges
    @BusinessUnitId int
as

set nocount on
set transaction isolation level read uncommitted

select RangeID, Low, High 
from IMT.dbo.GetInventoryBucketRanges(@BusinessUnitId, 7)
order by Low

go

grant execute on workflow.GetInventoryBucketRanges to MerchandisingUser
go
