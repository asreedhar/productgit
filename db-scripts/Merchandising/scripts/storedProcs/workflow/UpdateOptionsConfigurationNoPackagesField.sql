if object_id('workflow.UpdateOptionsConfigurationNoPackagesField') is not null
	drop procedure workflow.UpdateOptionsConfigurationNoPackagesField
go

create procedure workflow.UpdateOptionsConfigurationNoPackagesField
    @MemberLogin varchar(30) = '#UpdateNoPkg',
    @BusinessUnitID int = null,
    @InventoryID int = null
as

set nocount on
set transaction isolation level read uncommitted

if @BusinessUnitID is null
begin
    
    with NoPackages as
    (
        select BusinessUnitID, InventoryID, 
            cast(case when MakeHasWhitelistedPackages = 1 and VehicleHasWhiteListedPackages = 0 
                 then 1 else 0 end as bit) as NoPackages
        from Merchandising.workflow.InventoryHasWhiteListedPackages
    )
    update oc 
        set NoPackages = inv.NoPackages,
            LastUpdatedOn = getdate(),
            LastUpdatedBy = @MemberLogin
        from Merchandising.builder.OptionsConfiguration oc
            inner join NoPackages inv
              on oc.BusinessUnitID = inv.BusinessUnitID
                and oc.InventoryID = inv.InventoryID
        where oc.NoPackages <> inv.NoPackages
end
else
begin
    if @InventoryID is null
    begin
        raiserror('@InventoryID must be defined if @BusinessUnitID is defined.', 16, 1);
        return 1;
    end;
    
    with NoPackages as
    (
        select BusinessUnitID, InventoryID, 
            cast(case when MakeHasWhitelistedPackages = 1 and VehicleHasWhiteListedPackages = 0 
                 then 1 else 0 end as bit) as NoPackages
        from Merchandising.workflow.InventoryHasWhiteListedPackages
    )
    update oc 
        set NoPackages = inv.NoPackages,
            LastUpdatedOn = getdate(),
            LastUpdatedBy = @MemberLogin
        from Merchandising.builder.OptionsConfiguration oc
            inner join NoPackages inv
              on oc.BusinessUnitID = inv.BusinessUnitID
                and oc.InventoryID = inv.InventoryID
        where oc.NoPackages <> inv.NoPackages 
            and oc.BusinessUnitID = @BusinessUnitID
            and oc.InventoryID = @InventoryID
end
GO

GRANT EXECUTE ON workflow.UpdateOptionsConfigurationNoPackagesField TO MerchandisingUser 
GO