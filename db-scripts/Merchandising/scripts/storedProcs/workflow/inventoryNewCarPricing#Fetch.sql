USE [Merchandising]
GO

/****** Object:  StoredProcedure [workflow].[InventoryNewCarPricing#Fetch]    Script Date: 11/08/2013 16:19:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[workflow].[InventoryNewCarPricing#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [workflow].[InventoryNewCarPricing#Fetch]
GO

USE [Merchandising]
GO

/****** Object:  StoredProcedure [workflow].[InventoryNewCarPricing#Fetch]    Script Date: 11/08/2013 16:19:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [workflow].[InventoryNewCarPricing#Fetch] @businessUnitID int
AS

	BEGIN
	

		SET NOCOUNT ON

		CREATE TABLE #inv
		(
			businessUnitID int NOT NULL,
			inventoryID int NOT NULL,
			chromeStyleID int NOT NULL,
			myEngine varchar(2000) NULL,
			optionCode varchar(20) NULL,
			[standard] bit NULL
		)

		-- create a driver
		INSERT INTO #inv
			(businessUnitID, inventoryID, chromeStyleID)
		SELECT businessUnitID, inventoryID, chromeStyleID
			FROM workflow.inventory
			WHERE businessUnitID = @BusinessUnitId
			AND chromeStyleid > 0


		-- engine from packages
		UPDATE #inv
			SET myEngine  = options.PON, standard = 0, optionCode = options.OptionCode
			FROM #inv
			JOIN VehicleCatalog.Chrome.StyleCats styleCats ON styleCats.StyleID = #inv.ChromeStyleId 
															AND styleCats.CountryCode = 1
			INNER JOIN VehicleCatalog.Chrome.Options options ON styleCats.CountryCode = options.CountryCode
																AND styleCats.styleID = options.styleID 
																AND styleCats.sequence = options.sequence
			INNER JOIN Merchandising.builder.VehicleDetailedOptions vehicleDetailedOptions ON vehicleDetailedOptions.businessUnitID = #inv.businessUnitID
																								AND  vehicleDetailedOptions.inventoryID = #inv.inventoryID
																								AND vehicleDetailedOptions.optionCode = options.OptionCode
			WHERE styleCats.CategoryID IN (
				1045,
				1046,
				1047,
				1048,
				1049,
				1050,
				1051,
				1052,
				1135,
				1165)


		-- engine from equipment
		--UPDATE #inv
		--	SET #inv.myEngine  = opt.PON
		--	FROM #inv
		--	INNER JOIN builder.VehicleOptions vo ON vo.businessUnitID = #inv.businessUnitID 
		--											AND vo.inventoryId = #inv.inventoryID
		--	INNER JOIN VehicleCatalog.Chrome.StyleCats sc ON sc.StyleID = #inv.ChromeStyleId 
		--											AND sc.CountryCode = 1
		--	INNER JOIN VehicleCatalog.Chrome.Options opt ON opt.CountryCode = sc.CountryCode
		--													AND opt.styleID = sc.styleID
		--													AND opt.sequence = sc.sequence 
		--	WHERE myEngine IS NULL
		--		AND sc.CategoryID IN (
		--			1045,
		--			1046,
		--			1047,
		--			1048,
		--			1049,
		--			1050,
		--			1051,
		--			1052,
		--			1135,
		--			1165
		--		)


		-- Select FROM standards
		UPDATE #inv
			SET #inv.myEngine  = std.standard, standard = 1
				FROM #inv wi
				JOIN vehicleCatalog.chrome.stylecats sc on sc.StyleID = wi.ChromeStyleId
				INNER JOIN vehicleCatalog.chrome.standards std on std.styleId = sc.styleId
					  AND sc.featureType = 'S'
					  AND std.sequence = sc.sequence
					  AND std.countrycode = 1
				WHERE sc.CountryCode = 1
					  AND categoryId IN (
							 1045,
							 1046,
							 1047,
							 1048,
							 1049,
							 1050,
							 1051,
							 1052,
							 1135,
							 1165)
					  AND [state] <> 'D'
					  AND myEngine IS NULL


			
		SELECT inventory.InventoryID,
			Styles.CFStyleName AS BodyStyleName, 
			myEngine AS EngineDescription,
			#inv.optionCode AS OptionCode,
			#inv.standard AS IsStandard
			FROM workflow.inventory inventory 
			LEFT JOIN VehicleCatalog.Chrome.Styles Styles ON inventory.chromeStyleID = Styles.StyleID
			LEFT JOIN #inv ON inventory.inventoryID = #inv.inventoryID 
													AND #inv.businessUnitID = inventory.businessUnitID
			WHERE inventory.businessUnitID = @BusinessUnitId
			
			
		DROP TABLE #inv
			
			
		SET NOCOUNT OFF
	
END


GO
grant execute on workflow.InventoryNewCarPricing#Fetch to MerchandisingUser
GO


