use Merchandising
go

if object_id('workflow.GetEligibleAutoOfflineBusinessUnits') is not null
    drop procedure workflow.GetEligibleAutoOfflineBusinessUnits
go

create procedure workflow.GetEligibleAutoOfflineBusinessUnits
as
begin
    set nocount on
    set transaction isolation level read committed
    
    select M.BusinessUnitID
    from Merchandising.settings.Merchandising M
    where M.AutoOffline_WholesalePlanTrigger = 1
end
go

grant execute on workflow.GetEligibleAutoOfflineBusinessUnits to MerchandisingUser
go
