use Merchandising
go

if object_id('workflow.GetAutoOfflineStatus') is not null
    drop procedure workflow.GetAutoOfflineStatus
go

create procedure workflow.GetAutoOfflineStatus
    @businessUnitId int
as
begin
    set nocount on
    set transaction isolation level read committed
    
    declare @ret int
    
    -- Determine which AutoOffline triggers are set for this business unit
    declare @WholesalePlanTrigger bit
    select @WholesalePlanTrigger = AutoOffline_WholesalePlanTrigger
        from Merchandising.settings.Merchandising
        where businessUnitID = @businessUnitId
    
    -- Did any triggers apply?
    if @WholesalePlanTrigger = 0 
        return 0
    
    create table #AutoOffline (InventoryID int not null)
    
    if @WholesalePlanTrigger = 1
    begin
        if object_id('tempdb.dbo.#InventoryStrategy') is not null
            drop table #InventoryStrategy
            
        create table #InventoryStrategy
                (
                 InventoryID int,
                 BusinessUnitID int,
                 VehicleID int,
                 ListPrice int,
                 InventoryActive tinyint,
                 InventoryType tinyint,
                 StockNumber varchar(15),
                 PlanReminderDate smalldatetime,
                 InventoryReceivedDate smalldatetime,
                 AIP_EventCategoryID int,
                 HasActivePlan bit,
                 EverBeenPlanned bit 
                 primary key (InventoryID, BusinessUnitID)
                )
    
        -- Get the vehicles currently in the Wholesale Plan
        exec @ret = Market.Pricing.Load#InventoryStrategy 
            @OwnerEntityID = @BusinessUnitID,
            @SaleStrategy = 'W'
        if @ret <> 0
        begin
            raiserror (N'Failed call to Load#InventoryStrategy with error %d', 16, 1, @ret);
            return @ret
        end
        
        -- Add wholesale plan vehicles into the #AutoOffline inventory list
        insert into #AutoOffline (InventoryID)
        select InventoryID
            from #InventoryStrategy
            where BusinessUnitID = @businessUnitId
                and AIP_EventCategoryID = 2
                and InventoryID not in (select InventoryID from #AutoOffline)
                
        drop table #InventoryStrategy
    end;
       
    -- Return the list of vehicles that need their offline status changed
    with OfflineStatus as
    (
        select INV.BusinessUnitID, 
            INV.InventoryID,
            coalesce(OC.DoNotPostFlag, 0) as DoNotPost,
            coalesce(OC.DoNotPostLocked, 0) as DoNotPostLocked,
            cast(case when AO.InventoryID is not null then 1 else 0 end as bit) as ShouldBeOffline
        from FLDW.dbo.InventoryActive INV
            left outer join Merchandising.builder.OptionsConfiguration OC
                on INV.BusinessUnitID = OC.BusinessUnitID and INV.InventoryID = OC.inventoryId
            left outer join #AutoOffline AO
                on INV.InventoryID = AO.InventoryID
        where INV.InventoryActive = 1
    )
    select BusinessUnitID, InventoryID, ShouldBeOffline
    from OfflineStatus OS
    where OS.BusinessUnitID = @businessUnitId
        and OS.DoNotPostLocked = 0
        and OS.DoNotPost <> OS.ShouldBeOffline
    
    return 0
end
go

grant execute on workflow.GetAutoOfflineStatus to MerchandisingUser
go
