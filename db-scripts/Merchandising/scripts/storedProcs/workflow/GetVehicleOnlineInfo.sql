use Merchandising
go

if object_id('workflow.GetVehiclesOnlineInfo') is not null
	drop procedure workflow.GetVehiclesOnlineInfo
go


create procedure workflow.GetVehiclesOnlineInfo
	@businessUnitId int,
	@usedOrNew tinyint,
	@minAgeInDays int
as

set nocount on
set transaction isolation level read uncommitted

select *
from Merchandising.Reports.VehiclesOnlineInfo
where BusinessUnitId = @businessUnitId and ( @usedOrNew is null or @usedOrNew = 0 or InventoryType = @usedOrNew ) and AgeInDays >= @minAgeInDays
go

grant execute on workflow.GetVehiclesOnlineInfo to MerchandisingUser
go
