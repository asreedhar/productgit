if exists (select * from dbo.sysobjects where id = object_id(N'[Postings].[status#Update]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Postings].[status#Update]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: postings_status_Update.sql,v 1.6 2008/11/25 14:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * creates a new vehicle metric entry or updates the existing entry
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId int   - integer id of the businesUnit for which to get the alert list
 * @InventoryId - vehicle inventory id
 * @ReleaseStatus int - status of vehicle in workflow (0-error; 1-not online; 2-pending; 3-online)
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Postings].[status#Update]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@InventoryId int,
	@ReleaseStatus int
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE builder.OptionsConfiguration 
		SET postingStatus = @ReleaseStatus
		WHERE 
		inventoryId = @InventoryId
		AND businessUnitID = @BusinessUnitID
	
	IF @ReleaseStatus = 2 OR @ReleaseStatus = 1 --set to pending approval
	BEGIN        
    DELETE FROM postings.VehicleAdScheduling 
        WHERE businessUnitID = @BusinessUnitID 
        AND inventoryId = @InventoryId
        AND advertisementStatus IN (0,1,2) --future release or pending release soon
    END
END

GO

GRANT EXECUTE ON [postings].[status#Update] TO MerchandisingUser 
GO