if exists (select * from dbo.sysobjects where id = object_id(N'[postings].[WebMetricReportFilters#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [postings].[WebMetricReportFilters#Fetch]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- Description:	<gets the alerts for all vehicles that are posted on the internet for a businessUnit>
-- =============================================
CREATE PROCEDURE [postings].[WebMetricReportFilters#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@StartDate datetime,
	@EndDate datetime,
	@ModelFilter varchar =  NULL,
	@MakeFilter varchar = NULL,
	@YearFilter int = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	CREATE TABLE #Inventory (
		BusinessUnitId int NOT NULL,
		Vin varchar(17) NOT NULL,
		StockNumber varchar(15) NOT NULL,
		InventoryReceivedDate datetime NOT NULL, 
		vehicleYear int NOT NULL, 
		UnitCost decimal(9,2) NOT NULL, 
		AcquisitionPrice decimal (8,2) NOT NULL, 
		Certified tinyint NOT NULL, 
		Make varchar(20) NOT NULL, 
		model varchar(50) NOT NULL,
		VehicleTrim varchar(50) NULL,
		segmentId int NOT NULL,
		segment varchar(50) NOT NULL,
		inventoryID int NOT NULL,
		MileageReceived int NULL, 
		TradeOrPurchase tinyint NOT NULL, 
		ListPrice decimal(8,2) NULL,
		InventoryType tinyint NOT NULL,
		InventoryStatusCD int NOT NULL,
		BaseColor varchar(50) NOT NULL,
		VehicleLocation varchar(20) NULL
	)

	declare @earliestReceivedDate datetime
	select  @earliestReceivedDate = DATEADD(day,-365,@StartDate)
	INSERT INTO #Inventory EXEC Interface.HistoricalInventoryList#Fetch @BusinessUnitID=@BusinessUnitID, @earliestReceivedDate=@earliestReceivedDate

	CREATE TABLE #VehicleAggregate (
			daysIncluded int, 
			vin varchar(17), 
			listPriceAvg decimal,
			unitCostAvg decimal, 
			make varchar(50), 
			model varchar(50), 
			segmentId int,
			segment varchar(50),
			vehicleYear int,
			mileageReceived int,
			inventoryReceivedDate datetime,
			currentPrice int,
			mktAvg int,
			searchViewed int,
			detailsViewed int,
			adsPrinted int,
			emails int,
			mapsViewed int,
			websiteClickthrus int
	)

	INSERT INTO #VehicleAggregate
	(
			daysIncluded, 
			vin,
			listPriceAvg,
			unitCostAvg, 
			make,
			model,
			segmentId,
			segment,
			vehicleYear,
			mileageReceived,
			inventoryReceivedDate,
			currentPrice,
			mktAvg,
			searchViewed,
			detailsViewed,
			adsPrinted,
			emails,
			mapsViewed,
			websiteClickthrus
	)
	SELECT count(*) as daysIncluded, 
		inv.vin, 
		avg(listPrice) as listPriceAvg, 
		avg(unitcost) as unitCostAvg, 
		make, model, segmentId, segment, vehicleYear,
		max(mileageReceived) as mileageReceived, 
		min(inventoryReceivedDate) as inventoryReceivedDate
	  
		,avg([currentPrice]) as currentPrice
		,avg([mktAvgPrice]) as mktAvg
		,sum([searchViewed]) as searchViewed
		,sum([detailsViewed]) as detailsViewed
		,sum([adsPrinted]) as adsPrinted  
		,sum([emails]) as emails
	  ,sum([mapsViewed]) as mapsViewed
	  ,sum([websiteClickthrus]) as websiteClickthrus
	  FROM #Inventory inv
	INNER JOIN postings.vehicleMetrics vm
	ON vm.vin = inv.vin
	where startDate >= @StartDate
	AND startDate <= @EndDate
	GROUP BY inv.vin, make, model, segment, segmentId, vehicleYear

	select distinct make from #VehicleAggregate
	select distinct model from #VehicleAggregate
	select distinct vehicleYear from #VehicleAggregate
	select distinct segment from #VehicleAggregate

DROP TABLE #Inventory
DROP TABLE #VehicleAggregate


END
GO

GRANT EXECUTE ON [postings].[WebMetricReportFilters#Fetch] TO MerchandisingUser 
GO