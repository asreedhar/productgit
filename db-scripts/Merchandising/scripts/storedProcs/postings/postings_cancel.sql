if exists (select * from dbo.sysobjects where id = object_id(N'[Postings].[Cancel]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Postings].[Cancel]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: postings_cancel.sql,v 1.1 2008/12/11 04:58:35 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * cancel scheduled postings for a vehicle
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId int   - integer id of the businesUnit for which to get the alert list
 * @InventoryId - vehicle inventory id
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Postings].[Cancel]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@InventoryId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    DELETE FROM postings.VehicleAdScheduling 
        WHERE businessUnitID = @BusinessUnitID 
        AND inventoryId = @InventoryId
        AND advertisementStatus IN (0,1,2) --future release or pending release soon
    
END

GO

GRANT EXECUTE ON [postings].[Cancel] TO MerchandisingUser 
GO