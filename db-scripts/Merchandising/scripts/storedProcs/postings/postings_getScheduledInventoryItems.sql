if exists (select * from dbo.sysobjects where id = object_id(N'[postings].[getScheduledInventoryItems]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [postings].[getScheduledInventoryItems]
GO

/****** Object:  StoredProcedure [builder].[getScheduledInventoryItems]    Script Date: 10/31/2008 16:39:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: postings_getScheduledInventoryItems.sql,v 1.16 2010/03/10 00:26:58 bsombke Exp $
 * 
 * Summary
 * -------
 * 
 * gets all inventory items that are currently scheduled for posting
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [postings].[getScheduledInventoryItems]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@InventoryType int = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


CREATE TABLE #Inventory (
		BusinessUnitId int NOT NULL,
		Vin varchar(17) NOT NULL,
		StockNumber varchar(15) NOT NULL,
		InventoryReceivedDate datetime NOT NULL, 
		vehicleYear int NOT NULL, 
		UnitCost decimal(9,2) NOT NULL, 
		AcquisitionPrice decimal (8,2) NOT NULL, 
		Certified tinyint NOT NULL, 
		Make varchar(20) NOT NULL, 
		model varchar(50) NOT NULL,
		VehicleTrim varchar(50) NULL,
		inventoryID int NOT NULL,
		MileageReceived int NULL, 
		TradeOrPurchase tinyint NOT NULL, 
		ListPrice decimal(8,2) NULL,
		InventoryType tinyint NOT NULL,
		InventoryStatusCD int NOT NULL,
		BaseColor varchar(50) NOT NULL,
		VehicleLocation varchar(20) NULL,
		LotPrice DECIMAL(8,2) NULL,
		MSRP DECIMAL(9,2) NULL,
		VehicleCatalogId int NULL
		,PRIMARY KEY (businessUnitId, inventoryId)
	)

INSERT INTO #Inventory EXEC Interface.InventoryList#Fetch @BusinessUnitId=@BusinessUnitID, @UsedOrNew=@InventoryType

CREATE TABLE #Thumbnails (
		imageThumbnailUrl varchar(200),
		inventoryId int,
		businessUnitId int,
		photoCount int
	)
	INSERT INTO #Thumbnails EXEC builder.getThumbnails @BusinessUnitID

	CREATE TABLE #VehicleDataAndStatus (
		businessUnitID int NOT NULL
		,inventoryID int NOT NULL
		,isLegacy bit NOT NULL
		,chromeStyleID int NOT NULL
		,lotLocationID int NULL
		,afterMarketTrim varchar(50) NOT NULL
		,[specialId] int NULL
		,[doNotPostFlag] bit NOT NULL
		,[onlineFlag] smallint NOT NULL
		,[lotDataImportLock] bit NOT NULL
		,DescriptionSample varchar(180) NOT NULL
		,lastUpdateListPrice decimal(8,2) NULL
		,datePosted datetime NULL
		,exteriorStatus INT NOT NULL
		,interiorStatus INT NOT NULL
		,photoStatus INT NOT NULL 
		,adReviewStatus INT NOT NULL
		,pricingStatus INT NOT NULL 
		,postingStatus INT NOT NULL
		,daysPending INT NULL
		,stepsComplete INT NOT NULL
		,lowActivityFlag BIT NOT NULL
		,statusBucket INT NOT NULL

		,descriptionAlert BIT NOT NULL
		,photosAlert BIT NOT NULL
		,pricingAlert BIT NOT NULL
		,remerchandisingFlag BIT NOT NULL
		,NoPackages BIT NOT NULL
	)

	EXEC builder.VehicleData#Load @BusinessUnitId
	


SELECT Vin, 
inv.StockNumber, 
datediff(d,InventoryReceivedDate,getdate()) as age, 
vehicleYear, 
UnitCost, 
AcquisitionPrice, 
cast(Certified as bit) as certified, 
ISNULL(div.divisionName, inv.Make) as make, 
ISNULL(mdl.ModelName,inv.model) as model, 
ISNULL(mk.marketClass, '') as marketClass,
ISNULL(mk.marketClassID, 0) as marketClassID,
ISNULL(mk.segmentID, 1) as segmentId,
COALESCE(sty.trim, inv.VehicleTrim, '') as trim, 
inv.inventoryID,
ISNULL(vp.imageThumbnailUrl, '') as thumbnailUrl, 
ISNULL(vp.photoCount, 0) as photoCount,
ISNULL(lotLocationId, 0) as lotLocationId,

MileageReceived, 
TradeOrPurchase, 
vs.ListPrice, 
BaseColor,
vs.destinationId,
ed.description as destinationName,
vs.schedulingId as scheduledAdvertisementid,
vs.advertisementStatus,
vs.expiresOn,
vs.merchandisingDescription,
vs.equipmentList,
vs.approvedByMemberLogin,
vs.approvedTimeStamp,
vs.scheduledReleaseTime,
cast(case when pricingStatus >= 2 then 0 else 1 end as bit) as needsPricing,
inv.InventoryType




    FROM #Inventory inv
	
    LEFT JOIN #VehicleDataAndStatus oc ON oc.inventoryId = inv.inventoryId AND oc.BusinessUnitID = inv.BusinessUnitID
    LEFT JOIN #Thumbnails vp ON inv.businessUnitID = vp.businessUnitID AND inv.inventoryId = vp.inventoryId
    LEFT JOIN vehicleCatalog.chrome.styles sty ON sty.styleID = oc.chromestyleid and sty.countrycode = 1 
	LEFT JOIN vehicleCatalog.chrome.models mdl ON mdl.modelID = sty.modelID and mdl.countrycode = 1
	LEFT JOIN vehicleCatalog.chrome.divisions div ON div.divisionID = mdl.divisionID and div.countryCode = 1
	LEFT JOIN vehicleCatalog.FirstLook.MarketClass mk ON mk.marketClassID = sty.mktClassID
	INNER JOIN postings.vehicleadscheduling  vs
		ON vs.inventoryId = inv.inventoryId and vs.businessUnitId = inv.businessUnitId
    INNER JOIN settings.edtDestinations ed
		ON ed.destinationId = vs.destinationId
	WHERE inv.businessunitid = @BusinessUnitID 			
        AND advertisementStatus <= 3
        order by inventoryId, destinationId
	
DROP TABLE #Inventory
DROP TABLE #VehicleDataAndStatus
END
GO

GRANT EXECUTE ON [postings].[getScheduledInventoryItems] TO MerchandisingUser 
GO