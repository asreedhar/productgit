if exists (select * from dbo.sysobjects where id = object_id(N'[Postings].[OnlineDescription#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Postings].[OnlineDescription#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: postings_onlineDescription_fetch.sql,v 1.1 2009/01/19 19:51:23 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch the current sellers description from our recovered listings table
 * 
 * Parameters
 * ----------
 * 
 * @VIN
 * @ProviderId - check market.providers (autotrader=1, getauto=2 google base=3)
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Postings].[OnlineDescription#Fetch]
	-- Add the parameters for the stored procedure here
	@VIN varchar(17),
	@ProviderId int = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    exec Interface.OnlineDescription#Fetch @VIN, @ProviderId
END

GO

GRANT EXECUTE ON [postings].[OnlineDescription#Fetch] TO MerchandisingUser 
GO