if exists (select * from dbo.sysobjects where id = object_id(N'[postings].[TopVehicleMetrics#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [postings].[TopVehicleMetrics#Fetch]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- Description:	<gets the alerts for all vehicles that are posted on the internet for a businessUnit>
-- =============================================
CREATE PROCEDURE [postings].[TopVehicleMetrics#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@MetricSourceID int,
	@StartDate datetime,
	@EndDate datetime,
	@inventoryType int = 2
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SET ANSI_WARNINGS OFF

	CREATE TABLE #Inventory (
		BusinessUnitId int NOT NULL,
		Vin varchar(17) NOT NULL,
		StockNumber varchar(15) NOT NULL,
		InventoryReceivedDate datetime NOT NULL, 
		vehicleYear int NOT NULL, 
		UnitCost decimal(9,2) NOT NULL, 
		AcquisitionPrice decimal (8,2) NOT NULL, 
		Certified tinyint NOT NULL, 
		Make varchar(20) NOT NULL, 
		model varchar(50) NOT NULL,
		VehicleTrim varchar(50) NULL,
		segmentId int NOT NULL,
		segment varchar(50) NOT NULL,
		inventoryID int NOT NULL,
		MileageReceived int NULL, 
		TradeOrPurchase tinyint NOT NULL, 
		ListPrice decimal(8,2) NULL,
		InventoryType tinyint NOT NULL,
		InventoryStatusCD int NOT NULL,
		BaseColor varchar(50) NOT NULL,
		VehicleLocation varchar(20) NULL
	)

	declare @earliestReceivedDate datetime
	select  @earliestReceivedDate = DATEADD(day,-365,@StartDate)
	INSERT INTO #Inventory EXEC Interface.HistoricalInventoryList#Fetch @BusinessUnitID=@BusinessUnitID, @earliestReceivedDate=@earliestReceivedDate, @UsedOrNew=@inventoryType

	CREATE TABLE #VehicleAggregate (
			dateRange varchar(3),
			daysIncluded int, 
			vin varchar(17), 
			stockNumber varchar(15),
			listPriceAvg decimal,
			unitCostAvg decimal, 
			make varchar(50), 
			model varchar(50),
			segmentId int, 
			segment varchar(50),
			vehicleYear int,
			mileageReceived int,
			inventoryReceivedDate datetime,
			currentPrice int,
			mktAvg int,
			searchViewed int,
			detailsViewed int,
			adsPrinted int,
			emails int,
			mapsViewed int,
			websiteClickthrus int,
			totalActions int
	)
	
	

	INSERT INTO #VehicleAggregate
	(
			dateRange,
			daysIncluded, 
			vin,
			stockNumber,
			listPriceAvg,
			unitCostAvg, 
			make,
			model,
			segmentId,
			segment,
			vehicleYear,
			mileageReceived,
			inventoryReceivedDate,
			currentPrice,
			mktAvg,
			searchViewed,
			detailsViewed,
			adsPrinted,
			emails,
			mapsViewed,
			websiteClickthrus,
			totalActions
	)
	SELECT 
		'cur' as dateRange,
		count(*) as daysIncluded, 
		inv.vin, inv.stockNumber,
		ISNULL(avg(listPrice), 0) as listPriceAvg, 
		avg(unitcost) as unitCostAvg, 
		make, model, 
		segmentId, segment, 
		vehicleYear,
		max(mileageReceived) as mileageReceived, 
		min(inventoryReceivedDate) as inventoryReceivedDate
	  
		,avg([currentPrice]) as currentPrice
		,avg([mktAvgPrice]) as mktAvg
		,sum([searchViewed]) as searchViewed
		,sum([detailsViewed]) as detailsViewed
		,sum([adsPrinted]) as adsPrinted  
		,sum([emails]) as emails
	  ,sum([mapsViewed]) as mapsViewed
	  ,sum([websiteClickthrus]) as websiteClickthrus
	  ,sum([adsPrinted])+sum([emails])+sum([mapsViewed])+sum([websiteClickthrus]) as totalActions
	  FROM #Inventory inv
	INNER JOIN postings.vehicleMetrics vm
	ON vm.vin = inv.vin
	where startDate >= @StartDate
	AND startDate <= @EndDate
	AND (@MetricSourceId = 0 OR sourceId = @MetricSourceId)
	GROUP BY inv.vin, make, model, segmentId, segment, vehicleYear, inv.stockNumber

	select va.*,
		emails+mapsViewed+adsPrinted+websiteClickthrus as activityCount
		,CAST(searchViewed as float)/NULLIF(daysIncluded,0) as searchPerVehicleDay
		,CAST(detailsViewed as float)/NULLIF(daysIncluded,0) as detailsPerVehicleDay
		,7*CAST(totalActions as float)/NULLIF(daysIncluded,0) as actionsPerVehicleWeek
		,100*CAST(detailsViewed as float)/NULLIF(searchViewed,0) as clickThruPercent
		,100*CAST(totalActions as float)/NULLIF(detailsViewed,0) as activityPercent
	 from #VehicleAggregate as va
	
	ORDER BY detailsPerVehicleDay DESC


DROP TABLE #Inventory
DROP TABLE #VehicleAggregate
SET ANSI_WARNINGS ON

END
GO

GRANT EXECUTE ON [postings].[TopVehicleMetrics#Fetch] TO MerchandisingUser 
GO