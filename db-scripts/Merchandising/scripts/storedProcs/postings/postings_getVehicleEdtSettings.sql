if exists (select * from dbo.sysobjects where id = object_id(N'[postings].[getVehicleEdtSettings]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [postings].[getVehicleEdtSettings]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: postings_getVehicleEdtSettings.sql,v 1.5 2009/03/13 21:22:35 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Gets all EDT destinations currently configured for the vehicle and business unit
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId		- vehicle inventory id
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [postings].[getVehicleEdtSettings]
	
	@BusinessUnitID int,
	@InventoryId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT activeFlag, 
			d.Description, 
			d.DestinationID, 
			datePosted, 
			pricePosted 
	FROM settings.DealerListingSites s 
	INNER JOIN settings.EdtDestinations d 
		ON d.destinationID = s.destinationID 
	LEFT JOIN (SELECT * 
				FROM postings.publications 
				WHERE inventoryId = @InventoryId
					AND BusinessUnitID = @BusinessUnitID) p 
		ON p.destinationID = d.destinationID 
	WHERE s.BusinessUnitID = @BusinessUnitID
	

END
GO

GRANT EXECUTE ON [postings].[getVehicleEdtSettings] TO MerchandisingUser 
GO