
/****** Object:  StoredProcedure [postings].[SscheduleListPrice#Update]    Script Date: 05/05/2015 11:34:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[postings].[SscheduleListPrice#Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [postings].[SscheduleListPrice#Update]
GO


/****** Object:  StoredProcedure [postings].[SscheduleListPrice#Update]    Script Date: 05/05/2015 11:34:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  <Abhishek Patil>  
-- Create date: <04/28/15>  
-- =============================================  
/* --------------------------------------------------------------------  
 *    
 *   
 * Summary  
 * -------  
 *   
 * creates a new vehicle list price entry or updates the existing entry  
 *   
 *   
 * Parameters  
 * ----------  
 *   
 * @BusinessUnitId int   - integer id of the businesUnit for which to get the alert list  
 * @InventoryId - vehicle inventory id  
 *   
 * Exceptions  
 * ----------  
 *   
 *  
 * -------------------------------------------------------------------- */  
  
CREATE PROCEDURE [postings].[SscheduleListPrice#Update] 
 -- Add the parameters for the stored procedure here  
 @BusinessUnitID INT,  
 @InventoryId INT,
 @ApproverLogin VARCHAR(30), 
 @ListPrice DECIMAL = NULL, 
 @SpecialPrice DECIMAL(9,2),
 @RecordsAffected INT OUTPUT 
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
 IF EXISTS (SELECT 1  
                FROM postings.VehicleAdScheduling   
                WHERE   
                    businessUnitID = @BusinessUnitID   
                    AND inventoryId = @InventoryId)  
    BEGIN  
  --BEGIN TRY  
  -- BEGIN TRANSACTION PriceSynch --scared that this might cause rollbacks when it's intended to fix a FB 13227  
   
        UPDATE postings.VehicleAdScheduling   
        SET listPrice = @ListPrice,  
            approvedByMemberLogin = @ApproverLogin,  
            approvedTimeStamp = GETDATE(),
			SpecialPrice = @SpecialPrice
        WHERE   
            businessUnitID = @BusinessUnitID   
            AND inventoryId = @InventoryId  
     
  -- Synching up to mitigate "Outdated price" bug, FB 13227  
  UPDATE builder.Descriptions  
  SET lastUpdateListPrice = CASE WHEN @SpecialPrice IS NULL THEN @ListPrice ELSE @SpecialPrice END  
  WHERE businessUnitID = @BusinessUnitID   
        AND inventoryId = @InventoryId  
    
    
   SET @RecordsAffected = @@ROWCOUNT
  -- COMMIT TRANSACTION PriceSynch  
  -- END TRY  
  -- BEGIN CATCH  
  -- IF XACT_STATE <> 0  
  --  ROLLBACK TRANSACTION PriceSynch  
  --  
  -- EXEC sp_ErrorHandler  
  -- END CATCH  
    END  
   
  -- COMMIT TRANSACTION PriceSynch  
  -- END TRY  
  -- BEGIN CATCH  
  -- IF XACT_STATE <> 0  
  --  ROLLBACK TRANSACTION PriceSynch  
  --  
  -- EXEC sp_ErrorHandler  
  -- END CATCH  
      
    DELETE FROM postings.VehicleAdScheduling   
        WHERE businessUnitID = @BusinessUnitID   
        AND inventoryId = @InventoryId  
        AND scheduledReleaseTime IS NULL   
        AND destinationEnabled = 0  
END  
  

GO


GO

GRANT EXECUTE ON [postings].[SscheduleListPrice#Update] TO MerchandisingUser 
GO