if exists (select * from dbo.sysobjects where id = object_id(N'[Postings].[schedule#Create]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Postings].[schedule#Create]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: postings_schedule_create.sql,v 1.6 2009/11/09 17:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * creates a new vehicle metric entry or updates the existing entry
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId int   - integer id of the businesUnit for which to get the alert list
 * @InventoryId - vehicle inventory id
 * @DestinationId int - status of vehicle in workflow (0-error; 1-not online; 2-pending; 3-online)
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Postings].[schedule#Create]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@InventoryId int,
	@ReleaseTime datetime,
	@ApproverLogin varchar(30),
	@MerchandisingDescription varchar(max),
	@Footer varchar(2000),
	@EquipmentList varchar(max),
	@ListPrice decimal = NULL,
	@ExpiresOn datetime = NULL,
	@ExteriorColor varchar(50) = NULL,
	@InteriorColor varchar(50) = NULL,
	@InteriorType varchar(50) = NULL,
	@MappedOptionIds varchar(1000) = NULL,
	@OptionCodes varchar(300) = NULL,
	@HighlightCalloutText varchar(200) = NULL,
	@SeoKeywords varchar(200) = NULL,
	@SpecialPrice decimal(9,2),
	@AutoReGenerate bit
	,@MSRP int = NULL
	,@ManufacturerRebate int = NULL
	,@DealerDiscount int = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF EXISTS (SELECT 1
                FROM postings.VehicleAdScheduling 
                WHERE 
                    businessUnitID = @BusinessUnitID 
                    AND inventoryId = @InventoryId)
    BEGIN
		--BEGIN TRY
		--	BEGIN TRANSACTION PriceSynch --scared that this might cause rollbacks when it's intended to fix a FB 13227
	
        UPDATE postings.VehicleAdScheduling 
        SET scheduledReleaseTime = @ReleaseTime, destinationEnabled = 1 ,
            advertisementStatus = 1, --set to future
            merchandisingDescription = @MerchandisingDescription,
            footer = @Footer,
            equipmentList = @EquipmentList,
            listPrice = @ListPrice,
            expiresOn = @ExpiresOn,
            approvedByMemberLogin = @ApproverLogin,
            approvedTimeStamp = getdate(),
            exteriorColor = @ExteriorColor,
            interiorColor = @InteriorColor,
            interiorType = @InteriorType,
            mappedOptionIds = @MappedOptionIds,
            optionCodes = @OptionCodes,
            highlightCalloutText = @HighlightCalloutText,
            seoKeywords = @SeoKeywords,
			SpecialPrice = @SpecialPrice,
			autoRegenerate = @AutoReGenerate
			,MSRP = @MSRP
			,DealerDiscount = @DealerDiscount
			,ManufacturerRebate = @ManufacturerRebate

        WHERE 
            businessUnitID = @BusinessUnitID 
            AND inventoryId = @InventoryId
			
		-- Synching up to mitigate "Outdated price" bug, FB 13227
		UPDATE builder.Descriptions
		SET lastUpdateListPrice = CASE WHEN @SpecialPrice is null THEN @ListPrice ELSE @SpecialPrice END
		WHERE businessUnitID = @BusinessUnitID 
        AND inventoryId = @InventoryId
		
		--	COMMIT TRANSACTION PriceSynch
		-- END TRY
		-- BEGIN CATCH
		--	IF XACT_STATE <> 0
		--		ROLLBACK TRANSACTION PriceSynch
		--
		--	EXEC sp_ErrorHandler
		-- END CATCH
    END
    ELSE
    BEGIN
		--BEGIN TRY
		--	BEGIN TRANSACTION PriceSynch --scared that this might cause rollbacks when it's intended to fix a FB 13227
		
        INSERT INTO postings.VehicleAdScheduling 
            (businessUnitID, inventoryId,  
            scheduledReleaseTime, destinationEnabled, advertisementStatus, approvedByMemberLogin,
            merchandisingDescription, footer, equipmentList, listPrice, expiresOn, exteriorColor, interiorColor,
            interiorType, mappedOptionIds, optionCodes, highlightCalloutText, seoKeywords, SpecialPrice, autoRegenerate, MSRP, DealerDiscount, ManufacturerRebate)
            
            VALUES (@BusinessUnitID, @InventoryId,  
					@ReleaseTime, 1, 1, @ApproverLogin,
					@MerchandisingDescription, @Footer, @EquipmentList, @ListPrice, @ExpiresOn,
					@ExteriorColor, @InteriorColor, @InteriorType, @MappedOptionIds, @OptionCodes,
					@HighlightCalloutText, @SeoKeywords, @SpecialPrice, @AutoReGenerate, @MSRP, @DealerDiscount, @ManufacturerRebate)  --set status to future
		
		-- Synching up to mitigate "Outdated price" bug, FB 13227
		UPDATE builder.Descriptions
		SET lastUpdateListPrice = CASE WHEN @SpecialPrice is null THEN @ListPrice ELSE @SpecialPrice END
		WHERE businessUnitID = @BusinessUnitID 
        AND inventoryId = @InventoryId
		
		--	COMMIT TRANSACTION PriceSynch
		-- END TRY
		-- BEGIN CATCH
		--	IF XACT_STATE <> 0
		--		ROLLBACK TRANSACTION PriceSynch
		--
		--	EXEC sp_ErrorHandler
		-- END CATCH
    END
    
    DELETE FROM postings.VehicleAdScheduling 
        WHERE businessUnitID = @BusinessUnitID 
        AND inventoryId = @InventoryId
        AND scheduledReleaseTime IS NULL 
        AND destinationEnabled = 0
END

GO

GRANT EXECUTE ON [postings].[schedule#Create] TO MerchandisingUser 
GO
