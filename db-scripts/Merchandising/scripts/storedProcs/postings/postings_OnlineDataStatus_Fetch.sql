      if exists (select * from dbo.sysobjects where id = object_id(N'[postings].[OnlineDataStatus#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [postings].[OnlineDataStatus#Fetch]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:        <Kelley, Jonathan>
-- Create date: <5/4/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: postings_OnlineDataStatus_Fetch.sql,v 1.8 2010/03/10 00:26:58 bsombke Exp $
 * 
 * Summary
 * -------
 * 
 * Gets a report of the quality of online data
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @IssueType                 -the issue with the data for which to search
 *                                  1: No Description
                                    2: Description Mismatch
                                    3: Price Mismatch
                                    4: Mileage Mismatch
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [postings].[OnlineDataStatus#Fetch]
      -- Add the parameters for the stored procedure here
      @BusinessUnitID int,
      @IssueType int,
      @sortExpression varchar(30) = 'inventoryId ASC',
      @startRowIndex int = 0,
      @maximumRows int = 1000,
      @debug bit = 0,
      @inventoryType int = 2
      
AS
BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;

                        
      if @debug = 1 print '1)load inventory: ' + convert(varchar,getdate(),121)

      CREATE TABLE #Inventory (
            BusinessUnitId int NOT NULL,
            Vin varchar(17) NOT NULL,
            StockNumber varchar(15) NOT NULL,
            InventoryReceivedDate datetime NOT NULL, 
            vehicleYear int NOT NULL, 
            UnitCost decimal(9,2) NOT NULL, 
            AcquisitionPrice decimal (8,2) NOT NULL, 
            Certified tinyint NOT NULL, 
            Make varchar(20) NOT NULL, 
            model varchar(50) NOT NULL,
            VehicleTrim varchar(50) NULL,
            inventoryID int NOT NULL,
            MileageReceived int NULL, 
            TradeOrPurchase tinyint NOT NULL, 
            ListPrice decimal(8,2) NULL,
            InventoryType tinyint NOT NULL,
            InventoryStatusCD int NOT NULL,
            BaseColor varchar(50) NOT NULL,
            VehicleLocation varchar(20) NULL,
            LotPrice DECIMAL(8,2) NULL,
			MSRP DECIMAL(9,2) NULL,
			VehicleCatalogId int NULL
            ,PRIMARY KEY (businessUnitId, inventoryId)
      )

      INSERT INTO #Inventory EXEC Interface.InventoryList#Fetch @BusinessUnitId=@BusinessUnitID, @UsedOrNew=@inventoryType
      
      if @debug = 1 print 'load thumbnails: ' + convert(varchar,getdate(),121)
      
      CREATE TABLE #Thumbnails (
            imageThumbnailUrl varchar(200),
            inventoryId int,
            businessUnitId int,
            photoCount int
      )
      INSERT INTO #Thumbnails EXEC builder.getThumbnails @BusinessUnitID

      if @debug = 1 print 'get veh status: ' + convert(varchar,getdate(),121)
      
      CREATE TABLE #VehicleDataAndStatus (
            businessUnitID int NOT NULL
            ,inventoryID int NOT NULL
            ,isLegacy bit NOT NULL
            ,chromeStyleID int NOT NULL
            ,lotLocationID int NULL
            ,afterMarketTrim varchar(50) NOT NULL
            ,[specialId] int NULL
            ,[doNotPostFlag] bit NOT NULL
            ,[onlineFlag] smallint NOT NULL
            ,[lotDataImportLock] bit NOT NULL
            ,DescriptionSample varchar(180) NOT NULL
            ,lastUpdateListPrice decimal(8,2) NULL
            ,datePosted datetime NULL
            ,exteriorStatus INT NOT NULL
            ,interiorStatus INT NOT NULL
            ,photoStatus INT NOT NULL
            ,adReviewStatus INT NOT NULL
            ,pricingStatus INT NOT NULL 
            ,postingStatus INT NOT NULL
            ,daysPending INT NULL
            ,stepsComplete INT NOT NULL
            ,lowActivityFlag BIT NOT NULL
            ,statusBucket INT NOT NULL

            ,descriptionAlert BIT NOT NULL
            ,photosAlert BIT NOT NULL
            ,pricingAlert BIT NOT NULL
            ,remerchandisingFlag BIT NOT NULL
            ,NoPackages BIT NOT NULL
      )

      EXEC builder.VehicleData#Load @BusinessUnitId

if @debug = 1 print 'prep sort exp: ' + convert(varchar,getdate(),121)

      
select @sortExpression = case
WHEN @sortExpression like 'stockNumber%' OR 
       @sortExpression like 'inventoryId%'
THEN 'inv.' + @sortExpression

ELSE @sortExpression
END

      if @debug = 1 print 'prep dyn select: ' + convert(varchar,getdate(),121)

declare @sql nvarchar(4000)
set @sql = '
select * FROM (
SELECT ROW_NUMBER() OVER(ORDER BY ' + 
      CASE 
            WHEN @sortExpression = '' THEN 'inventoryReceivedDate ASC '
            ELSE @sortExpression
            END 
            + ') as RowNum,
Vin, 
inv.inventoryID,
inv.StockNumber, 
InventoryReceivedDate, 
vehicleYear, 
UnitCost, 
AcquisitionPrice, 
cast(Certified as bit) as certified, 
ISNULL(div.DivisionName, inv.Make) as make, 
ISNULL(sty.CFModelName,inv.model) as model, 
COALESCE(sty.trim, inv.VehicleTrim, '''') as trim, 
ISNULL(mkt.marketClass, '''') as marketClass,
ISNULL(mkt.marketClassId, 0) as marketClassId,
ISNULL(mkt.segmentId, 1) as segmentId,
ISNULL(vp.imageThumbnailUrl, '''') as thumbnailUrl, 
ISNULL(vp.photoCount, 0) as photoCount,
ISNULL(lotLocationId,0) as lotLocationId,
MileageReceived, 
TradeOrPurchase, 
ListPrice, 
BaseColor,
inv.VehicleLocation,
InventoryStatusCD,
oc.DescriptionSample,
NULL as chromeStyleId,
ISNULL(lowActivityFlag,0) as lowActivityFlag,
ISNULL(oc.exteriorStatus, 0) as exteriorStatus, 
ISNULL(oc.interiorStatus, 0) as interiorStatus,
ISNULL(oc.photoStatus, 0) as photoStatus, 
ISNULL(oc.adReviewStatus, 0) as adReviewStatus,
ISNULL(oc.pricingStatus, 0) as pricingStatus, 
ISNULL(oc.postingStatus, 0) as postingStatus,
ISNULL(oc.onlineFlag, 0) as onlineFlag,
ISNULL(oc.stepsComplete,0) as stepsComplete,
ISNULL(os.descriptionEmpty,1) as descriptionEmpty,
ISNULL(os.descriptionMatch,0) as descriptionMatch,
ISNULL(os.listPriceMatch,0) as listPriceMatch,
ISNULL(os.mileageMatch,0) as mileageMatch,
ISNULL(os.onlinePrice,0) as onlinePrice,
ISNULL(os.onlineMileage,0) as onlineMileage,
ISNULL(os.onlineHighlightsPreview,''No highlights online'') as onlineHighlightsPreview


 
    FROM #Inventory inv
    LEFT JOIN #VehicleDataAndStatus oc ON oc.inventoryId = inv.inventoryId AND oc.businessUnitID = inv.businessUnitID
    LEFT JOIN #Thumbnails vp ON inv.businessUnitID = vp.businessUnitID AND inv.inventoryId = vp.inventoryId
    LEFT JOIN vehicleCatalog.chrome.styles sty ON sty.styleID = oc.chromestyleid and sty.countrycode = 1                    
      LEFT JOIN vehicleCatalog.chrome.models mdl ON mdl.modelID = sty.modelID and mdl.countrycode = 1
      LEFT JOIN vehicleCatalog.FirstLook.MarketClass mkt ON mkt.marketClassID = sty.mktClassID
      LEFT JOIN vehicleCatalog.chrome.Divisions div ON div.divisionID = mdl.divisionID and div.countrycode = 1
      LEFT JOIN postings.OnlineStatus os ON os.businessUnitId = inv.businessUnitID AND os.inventoryId = inv.inventoryId
    WHERE '
      + CASE @IssueType 
            WHEN 1 THEN ' os.descriptionEmpty = 1 '
            WHEN 2 THEN ' os.descriptionMatch = 0 '
            WHEN 3 THEN ' os.listPriceMatch = 0 '
            WHEN 4 THEN ' os.mileageMatch = 0 '
            WHEN 5 THEN ' os.destinationId IS NULL '
            ELSE ' 1 = 1 '
      END

+ ' ) as vehInfo
WHERE RowNum BETWEEN ' + CONVERT(nvarchar(10), @startRowIndex + 1) + 
                  ' AND ' + CONVERT(nvarchar(10), (@startRowIndex + @maximumRows))
      if @debug = 1 print 'exec dyn select: ' + convert(varchar,getdate(),121)
EXEC sp_executesql @sql
      
      if @debug = 1 print 'fin: ' + convert(varchar,getdate(),121)
      
      DROP TABLE #Inventory
      DROP TABLE #VehicleDataAndStatus
      DROP TABLE #Thumbnails
END
GO

GRANT EXECUTE ON [postings].[OnlineDataStatus#Fetch] TO MerchandisingUser 
GO
