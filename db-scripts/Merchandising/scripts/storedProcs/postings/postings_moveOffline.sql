use Merchandising
go

if exists (select * from dbo.sysobjects where id = object_id(N'[Postings].[MoveOffline]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Postings].[MoveOffline]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: postings_moveOffline.sql,v 1.2 2009/11/09 17:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * MoveOffline removes a vehicle from the scheduled postings, pulls a publication offline, 
 * clears the data from the adReleases table, and sets the doNotSendFlag to 1
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId int   - integer id of the businesUnit for which to get the alert list
 * @InventoryId - vehicle inventory id
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Postings].[MoveOffline]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@InventoryId int,
	@MemberLogin varchar(30),
	@UserInitiated bit = 1
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF NOT EXISTS (SELECT 1 FROM builder.OptionsConfiguration WHERE businessUnitId = @BusinessUnitId and inventoryId = @InventoryId)
	BEGIN
		EXEC builder.VehicleConfiguration#Create @BusinessUnitId, @InventoryId, @MemberLogin
	END
	
	--config guaranteed at this point...proceed to update row with do not post value
	UPDATE builder.OptionsConfiguration
	SET doNotPostFlag = 1,
	    DoNotPostLocked = case when @UserInitiated = 1 then 1 else DoNotPostLocked end,
		lastUpdatedBy = @MemberLogin,
		lastUpdatedOn = getdate()
	WHERE 
		businessUnitId = @BusinessUnitId
		AND inventoryId = @InventoryId
		
	
    DELETE FROM postings.VehicleAdScheduling 
        WHERE businessUnitID = @BusinessUnitId 
        AND inventoryId = @InventoryId
        AND advertisementStatus IN (0,1,2) --future release or pending release soon
        
    UPDATE postings.Publications
    SET dateRemoved = getdate()
    WHERE businessUnitId = @BusinessUnitId
        AND inventoryId = @InventoryId
        AND dateRemoved IS null
    
    EXEC Interface.Advertisement#Delete @BusinessUnitId, @InventoryId
    
    EXEC Interface.VehicleExportStatus#Update @BusinessUnitId, @InventoryId, 1
    
    exec builder.VehicleStatus#UpdateOrCreate @BusinessUnitID,
        @InventoryID,
        @MemberLogin,
        @StatusTypeId = 5, -- Posting Status
        @StatusLevel = 2 -- Needs Approval
END

GO

GRANT EXECUTE ON [postings].[MoveOffline] TO MerchandisingUser 
GO