if exists (select * from dbo.sysobjects where id = object_id(N'[postings].[PerformanceSeries#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [postings].[PerformanceSeries#Fetch]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- Description:	<gets the performance series for a vehicle, a source, and a date range
-- =============================================
CREATE PROCEDURE [postings].[PerformanceSeries#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@StockNumber varchar(15),
	@MetricSourceId int = 1,
	@StartDate datetime,
	@EndDate datetime
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		startDate, 
		startDate as endDate, 
		searchViewed, 
		detailsViewed, 
		emails, 
		adsPrinted, 
		mapsViewed, 
		websiteClickthrus,
	    currentPrice, 
	    mktAvgPrice
    FROM postings.VehicleMetrics
    WHERE stockNumber = @StockNumber
        AND businessUnitId = @BusinessUnitId
        AND startDate BETWEEN @StartDate AND @EndDate
        AND sourceId = @MetricSourceId

END
GO

GRANT EXECUTE ON [postings].[PerformanceSeries#Fetch] TO MerchandisingUser 
GO