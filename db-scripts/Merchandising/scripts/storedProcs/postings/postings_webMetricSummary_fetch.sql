if exists (select * from dbo.sysobjects where id = object_id(N'[postings].[WebMetricSummary#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [postings].[WebMetricSummary#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- Description:	<gets the summary web metrics>
-- =============================================
CREATE PROCEDURE [postings].[WebMetricSummary#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@StartDate datetime,
	@EndDate datetime
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT
		[description] as metricSource,
		searchViewed,
		detailsViewed,
		activityCount,
		daysIncluded,
		CAST(searchViewed AS float)/daysIncluded as searchPerVehicleDay,
		CAST(detailsViewed AS float)/daysIncluded as detailsPerVehicleDay,
		7*CAST(activityCount AS float)/daysIncluded as actionsPerVehicleWeek,
		CAST(detailsViewed AS float)/searchViewed as clickthruPercent,
		CAST(activityCount AS float)/detailsViewed as activityPercent
	FROM (
		SELECT 
			edt.description, 
			sum(searchViewed) as searchViewed,
			sum(detailsViewed) as detailsViewed,
			sum([adsPrinted])+sum([emails])+sum([mapsViewed])+sum([websiteClickthrus]) as activityCount,
			count(*) as daysIncluded
			
		FROM postings.VehicleMetrics vm
		INNER JOIN settings.edtDestinations edt
		ON edt.destinationID = vm.sourceId
		WHERE vm.businessUnitId = @BusinessUnitId
		AND vm.startDate BETWEEN @StartDate AND @EndDate
		GROUP BY vm.sourceID, edt.description
	) as metrics
		
		
		

	

END
GO

GRANT EXECUTE ON [postings].[WebMetricSummary#Fetch] TO MerchandisingUser 
GO