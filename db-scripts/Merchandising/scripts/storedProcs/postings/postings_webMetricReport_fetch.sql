if exists (select * from dbo.sysobjects where id = object_id(N'[postings].[WebMetricReport#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [postings].[WebMetricReport#Fetch]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- Description:	<gets the alerts for all vehicles that are posted on the internet for a businessUnit>
-- =============================================
CREATE PROCEDURE [postings].[WebMetricReport#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@MetricSourceID int,
	@StartDate datetime,
	@EndDate datetime,
	@GroupParameter int = 0,
	@StartYearFilter int = -1,
	@EndYearFilter int = 30000, --assume this code won't be used for 28,000 years
	@MakeFilter varchar(30) = '',
	@ModelFilter varchar(30) = '',
	@SegmentFilter varchar(30) = '',
	@inventoryType int = 2
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SET ANSI_WARNINGS OFF

	CREATE TABLE #Inventory (
		BusinessUnitId int NOT NULL,
		Vin varchar(17) NOT NULL,
		StockNumber varchar(15) NOT NULL,
		InventoryReceivedDate datetime NOT NULL, 
		vehicleYear int NOT NULL, 
		UnitCost decimal(9,2) NOT NULL, 
		AcquisitionPrice decimal (8,2) NOT NULL, 
		Certified tinyint NOT NULL, 
		Make varchar(20) NOT NULL, 
		model varchar(50) NOT NULL,
		VehicleTrim varchar(50) NULL,
		segmentId int NOT NULL,
		segment varchar(50) NOT NULL,
		inventoryID int NOT NULL,
		MileageReceived int NULL, 
		TradeOrPurchase tinyint NOT NULL, 
		ListPrice decimal(8,2) NULL,
		InventoryType tinyint NOT NULL,
		InventoryStatusCD int NOT NULL,
		BaseColor varchar(50) NOT NULL,
		VehicleLocation varchar(20) NULL
	)

	declare @earliestReceivedDate datetime
	select  @earliestReceivedDate = DATEADD(day,-365,@StartDate)
	INSERT INTO #Inventory EXEC Interface.HistoricalInventoryList#Fetch @BusinessUnitID=@BusinessUnitID, @earliestReceivedDate=@earliestReceivedDate, @UsedOrNew=@inventoryType

	CREATE TABLE #VehicleAggregate (
			dateRange varchar(3),
			daysIncluded int, 
			vin varchar(17), 
			stockNumber varchar(15),
			listPriceAvg decimal,
			unitCostAvg decimal, 
			make varchar(50), 
			model varchar(50),
			segmentId int, 
			segment varchar(50),
			vehicleYear int,
			mileageReceived int,
			inventoryReceivedDate datetime,
			currentPrice int,
			mktAvg int,
			searchViewed int,
			detailsViewed int,
			adsPrinted int,
			emails int,
			mapsViewed int,
			websiteClickthrus int,
			totalActions int
	)
	
	

	INSERT INTO #VehicleAggregate
	(
			dateRange,
			daysIncluded, 
			vin,
			stockNumber,
			listPriceAvg,
			unitCostAvg, 
			make,
			model,
			segmentId,
			segment,
			vehicleYear,
			mileageReceived,
			inventoryReceivedDate,
			currentPrice,
			mktAvg,
			searchViewed,
			detailsViewed,
			adsPrinted,
			emails,
			mapsViewed,
			websiteClickthrus,
			totalActions
	)
	SELECT 
		'cur' as dateRange,
		count(*) as daysIncluded, 
		inv.vin, inv.stockNumber,
		ISNULL(avg(listPrice), 0) as listPriceAvg, 
		avg(unitcost) as unitCostAvg, 
		make, model, 
		segmentId, segment, 
		vehicleYear,
		max(mileageReceived) as mileageReceived, 
		min(inventoryReceivedDate) as inventoryReceivedDate
	  
		,avg([currentPrice]) as currentPrice
		,avg([mktAvgPrice]) as mktAvg
		,sum([searchViewed]) as searchViewed
		,sum([detailsViewed]) as detailsViewed
		,sum([adsPrinted]) as adsPrinted  
		,sum([emails]) as emails
	  ,sum([mapsViewed]) as mapsViewed
	  ,sum([websiteClickthrus]) as websiteClickthrus
	  ,sum([adsPrinted])+sum([emails])+sum([mapsViewed])+sum([websiteClickthrus]) as totalActions
	  FROM #Inventory inv
	INNER JOIN postings.vehicleMetrics vm
	ON vm.vin = inv.vin
	where startDate >= @StartDate
	AND startDate <= @EndDate
	AND (@MetricSourceId = 0 OR sourceId = @MetricSourceId)
	GROUP BY inv.vin, make, model, segmentId, segment, vehicleYear, inv.stockNumber
	
if @GroupParameter = -1
BEGIN
	select 
		sum(totalActions) as activityCount
		,sum(searchViewed) as searchViewed
		,sum(detailsViewed) as detailsViewed
		,CAST(sum(searchViewed) as float)/NULLIF(sum(daysIncluded),0) as searchPerVehicleDay
		,CAST(sum(detailsViewed) as float)/NULLIF(sum(daysIncluded),0) as detailsPerVehicleDay
		,7*CAST(sum(totalActions) as float)/NULLIF(sum(daysIncluded),0) as actionsPerVehicleWeek
		,100*CAST(sum(detailsViewed) as float)/NULLIF(sum(searchViewed),0) as clickThruPercent
		,100*CAST(sum(totalActions) as float)/NULLIF(sum(detailsViewed),0) as activityPercent
	 from #VehicleAggregate as va
	WHERE ((@ModelFilter = '') OR (model like @ModelFilter)) AND
				(vehicleYear BETWEEN @StartYearFilter AND @EndYearFilter) AND
				(@SegmentFilter = '' OR segment LIKE @SegmentFilter) AND
				((@MakeFilter = '') OR (make like @MakeFilter))
END
ELSE IF @GroupParameter = 0
BEGIN
	select va.*,
		emails+mapsViewed+adsPrinted+websiteClickthrus as activityCount
		,CAST(searchViewed as float)/NULLIF(daysIncluded,0) as searchPerVehicleDay
		,CAST(detailsViewed as float)/NULLIF(daysIncluded,0) as detailsPerVehicleDay
		,7*CAST(totalActions as float)/NULLIF(daysIncluded,0) as actionsPerVehicleWeek
		,100*CAST(detailsViewed as float)/NULLIF(searchViewed,0) as clickThruPercent
		,100*CAST(totalActions as float)/NULLIF(detailsViewed,0) as activityPercent
	 from #VehicleAggregate as va
	WHERE ((@ModelFilter = '') OR (model like @ModelFilter)) AND
				(vehicleYear BETWEEN @StartYearFilter AND @EndYearFilter) AND
				(@SegmentFilter = '' OR segment LIKE @SegmentFilter) AND
				((@MakeFilter = '') OR (make like @MakeFilter))
	ORDER BY detailsPerVehicleDay DESC
END 
ELSE
BEGIN
--if not looking for individual rows, insert aged values into table
	INSERT INTO #VehicleAggregate
	(
			dateRange,
			daysIncluded, 
			vin,
			stockNumber,
			listPriceAvg,
			unitCostAvg, 
			make,
			model,
			segmentId,
			segment,
			vehicleYear,
			mileageReceived,
			inventoryReceivedDate,
			currentPrice,
			mktAvg,
			searchViewed,
			detailsViewed,
			adsPrinted,
			emails,
			mapsViewed,
			websiteClickthrus,
			totalActions
	)
	SELECT 
		'1mo' as dateRange,
		count(*) as daysIncluded, 
		inv.vin, inv.stockNumber,
		ISNULL(avg(listPrice),0) as listPriceAvg, 
		avg(unitcost) as unitCostAvg, 
		make, model, 
		segmentId, segment, 
		vehicleYear,
		max(mileageReceived) as mileageReceived, 
		min(inventoryReceivedDate) as inventoryReceivedDate
	  
		,avg([currentPrice]) as currentPrice
		,avg([mktAvgPrice]) as mktAvg
		,sum([searchViewed]) as searchViewed
		,sum([detailsViewed]) as detailsViewed
		,sum([adsPrinted]) as adsPrinted  
		,sum([emails]) as emails
	  ,sum([mapsViewed]) as mapsViewed
	  ,sum([websiteClickthrus]) as websiteClickthrus
	  ,sum([adsPrinted])+sum([emails])+sum([mapsViewed])+sum([websiteClickthrus]) as totalActions
	  FROM #Inventory inv
	INNER JOIN postings.vehicleMetrics vm
	ON vm.vin = inv.vin
	where startDate >= DATEADD(d,-60,@EndDate)
	AND startDate <= DATEADD(d,-30,@EndDate)
	AND (@MetricSourceId = 0 OR sourceId = @MetricSourceId)
	GROUP BY inv.vin, make, model, segmentId, segment, vehicleYear, inv.stockNumber
	
	INSERT INTO #VehicleAggregate
	(
			dateRange,
			daysIncluded, 
			vin,
			stockNumber,
			listPriceAvg,
			unitCostAvg, 
			make,
			model,
			segmentId,
			segment,
			vehicleYear,
			mileageReceived,
			inventoryReceivedDate,
			currentPrice,
			mktAvg,
			searchViewed,
			detailsViewed,
			adsPrinted,
			emails,
			mapsViewed,
			websiteClickthrus,
			totalActions
	)
	SELECT 
		'3mo' as dateRange,
		count(*) as daysIncluded, 
		inv.vin, inv.stockNumber,
		ISNULL(avg(listPrice),0) as listPriceAvg, 
		avg(unitcost) as unitCostAvg, 
		make, model, 
		segmentId, segment, 
		vehicleYear,
		max(mileageReceived) as mileageReceived, 
		min(inventoryReceivedDate) as inventoryReceivedDate
	  
		,avg([currentPrice]) as currentPrice
		,avg([mktAvgPrice]) as mktAvg
		,sum([searchViewed]) as searchViewed
		,sum([detailsViewed]) as detailsViewed
		,sum([adsPrinted]) as adsPrinted  
		,sum([emails]) as emails
	  ,sum([mapsViewed]) as mapsViewed
	  ,sum([websiteClickthrus]) as websiteClickthrus
	  ,sum([adsPrinted])+sum([emails])+sum([mapsViewed])+sum([websiteClickthrus]) as totalActions
	  FROM #Inventory inv
	INNER JOIN postings.vehicleMetrics vm
	ON vm.vin = inv.vin
	where startDate >= DATEADD(d,-120,@EndDate)
	AND startDate <= DATEADD(d,-90,@EndDate)
	AND (@MetricSourceId = 0 OR sourceId = @MetricSourceId)
	GROUP BY inv.vin, make, model, segmentId, segment, vehicleYear, inv.stockNumber
	
	
END


if @GroupParameter = 1
BEGIN
	
	select make ,count(*) as vehiclesIncluded
		,sum(daysIncluded) as daysIncluded
		,sum(searchViewed) as searchViewed
		,sum(detailsViewed) as detailsViewed
		,sum(emails) as emails
		,sum(mapsViewed) as mapsViewed
		,sum(adsPrinted) as adsPrinted
		,sum(websiteClickthrus) as websiteClickthrus
		,sum(totalActions) as activityCount
		,CAST(sum(searchViewed) as float)/NULLIF(sum(daysIncluded),0) as searchPerVehicleDay
		,CAST(sum(detailsViewed) as float)/NULLIF(sum(daysIncluded),0) as detailsPerVehicleDay
		,7*CAST(sum(totalActions) as float)/NULLIF(sum(daysIncluded),0) as actionsPerVehicleWeek
		,100*CAST(sum(detailsViewed) as float)/NULLIF(sum(searchViewed),0) as clickThruPercent
		,100*CAST(sum(totalActions) as float)/NULLIF(sum(detailsViewed),0) as activityPercent
		,sum(searchViewed1mo) as searchViewed1mo
		,sum(detailsViewed1mo) as detailsViewed1mo
		,sum(totalActions1mo) as activityCount1mo
		,CAST(sum(searchViewed1mo) as float)/NULLIF(sum(daysIncluded1mo),0) as searchPerVehicleDay1mo
		,CAST(sum(detailsViewed1mo) as float)/NULLIF(sum(daysIncluded1mo),0) as detailsPerVehicleDay1mo
		,7*CAST(sum(totalActions1mo) as float)/NULLIF(sum(daysIncluded1mo),0) as actionsPerVehicleWeek1mo
		,100*CAST(sum(detailsViewed1mo) as float)/NULLIF(sum(searchViewed1mo),0) as clickThruPercent1mo
		,100*CAST(sum(totalActions1mo) as float)/NULLIF(sum(detailsViewed1mo),0) as activityPercent1mo
		,sum(searchViewed3mo) as searchViewed3mo
		,sum(detailsViewed3mo) as detailsViewed3mo
		,sum(totalActions3mo) as activityCount3mo
		,CAST(sum(searchViewed3mo) as float)/NULLIF(sum(daysIncluded3mo),0) as searchPerVehicleDay3mo
		,CAST(sum(detailsViewed3mo) as float)/NULLIF(sum(daysIncluded3mo),0) as detailsPerVehicleDay3mo
		,7*CAST(sum(totalActions3mo) as float)/NULLIF(sum(daysIncluded3mo),0) as actionsPerVehicleWeek3mo
		,100*CAST(sum(detailsViewed3mo) as float)/NULLIF(sum(searchViewed3mo),0) as clickThruPercent3mo
		,100*CAST(sum(totalActions3mo) as float)/NULLIF(sum(detailsViewed3mo),0) as activityPercent3mo
		 
	FROM (
	SELECT --ISNULL(va.make, ISNULL(va2.make, va3.make)) as make,
	va.make, --we'll get rid of any nulls (from outer join) in where
	va.daysIncluded,
	va.searchViewed,
	va.detailsViewed,
	va.emails,
	va.mapsViewed,
	va.adsPrinted,
	va.websiteClickthrus,
	va.totalActions,
	
	va2.daysIncluded as daysIncluded1mo,
	va2.searchViewed as searchViewed1mo,
	va2.detailsViewed as detailsViewed1mo,
	va2.emails as emails1mo,
	va2.mapsViewed as mapsViewed1mo,
	va2.adsPrinted as adsPrinted1mo,
	va2.websiteClickthrus as websiteClickthrus1mo,
	va2.totalActions as totalActions1mo,
	
	va3.daysIncluded as daysIncluded3mo,
	va3.searchViewed as searchViewed3mo,
	va3.detailsViewed as detailsViewed3mo,
	va3.emails as emails3mo,
	va3.mapsViewed as mapsViewed3mo,
	va3.adsPrinted as adsPrinted3mo,
	va3.websiteClickthrus as websiteClickthrus3mo,
	va3.totalActions as totalActions3mo
	
	FROM
	(
		SELECT vin,
			daysIncluded,
			searchViewed,
			detailsViewed,
			emails,
			mapsViewed,
			adsPrinted,
			websiteClickthrus,
			totalActions,
			make
		FROM #VehicleAggregate
		WHERE (@ModelFilter = '' OR model like @ModelFilter) AND
				(vehicleYear BETWEEN @StartYearFilter AND @EndYearFilter) AND
				(@SegmentFilter = '' OR segment LIKE @SegmentFilter) AND
				(@MakeFilter = '' OR make like @MakeFilter)
				AND dateRange = 'cur'		
	) as va
	FULL OUTER JOIN (
		SELECT vin,
			daysIncluded,
			searchViewed,
			detailsViewed,
			emails,
			mapsViewed,
			adsPrinted,
			websiteClickthrus,
			totalActions,
			make
		FROM #VehicleAggregate
		WHERE (@ModelFilter = '' OR model like @ModelFilter) AND
				(vehicleYear BETWEEN @StartYearFilter AND @EndYearFilter) AND
				(@SegmentFilter = '' OR segment LIKE @SegmentFilter) AND
				(@MakeFilter = '' OR make like @MakeFilter)
				AND dateRange = '1mo'		
	) as va2
	ON va.vin = va2.vin
	FULL OUTER JOIN (
		SELECT vin,
			daysIncluded,
			searchViewed,
			detailsViewed,
			emails,
			mapsViewed,
			adsPrinted,
			websiteClickthrus,
			totalActions,
			make
		FROM #VehicleAggregate
		WHERE (@ModelFilter = '' OR model like @ModelFilter) AND
				(vehicleYear BETWEEN @StartYearFilter AND @EndYearFilter) AND
				(@SegmentFilter = '' OR segment LIKE @SegmentFilter) AND
				(@MakeFilter = '' OR make like @MakeFilter)
				AND dateRange = '3mo'		
	) as va3
	ON va2.vin = va3.vin
	) as myrows
	WHERE NOT make IS NULL
	GROUP BY make
	
END

if @GroupParameter = 2
BEGIN
	
	select top(10) model ,count(*) as vehiclesIncluded
		,sum(daysIncluded) as daysIncluded
		,sum(searchViewed) as searchViewed
		,sum(detailsViewed) as detailsViewed
		,sum(emails) as emails
		,sum(mapsViewed) as mapsViewed
		,sum(adsPrinted) as adsPrinted
		,sum(websiteClickthrus) as websiteClickthrus
		,sum(totalActions) as activityCount
		,CAST(sum(searchViewed) as float)/NULLIF(sum(daysIncluded),0) as searchPerVehicleDay
		,CAST(sum(detailsViewed) as float)/NULLIF(sum(daysIncluded),0) as detailsPerVehicleDay
		,7*CAST(sum(totalActions) as float)/NULLIF(sum(daysIncluded),0) as actionsPerVehicleWeek
		,100*CAST(sum(detailsViewed) as float)/NULLIF(sum(searchViewed),0) as clickThruPercent
		,100*CAST(sum(totalActions) as float)/NULLIF(sum(detailsViewed),0) as activityPercent
		,sum(searchViewed1mo) as searchViewed1mo
		,sum(detailsViewed1mo) as detailsViewed1mo
		,sum(totalActions1mo) as activityCount1mo
		,CAST(sum(searchViewed1mo) as float)/NULLIF(sum(daysIncluded1mo),0) as searchPerVehicleDay1mo
		,CAST(sum(detailsViewed1mo) as float)/NULLIF(sum(daysIncluded1mo),0) as detailsPerVehicleDay1mo
		,7*CAST(sum(totalActions1mo) as float)/NULLIF(sum(daysIncluded1mo),0) as actionsPerVehicleWeek1mo
		,100*CAST(sum(detailsViewed1mo) as float)/NULLIF(sum(searchViewed1mo),0) as clickThruPercent1mo
		,100*CAST(sum(totalActions1mo) as float)/NULLIF(sum(detailsViewed1mo),0) as activityPercent1mo
		,sum(searchViewed3mo) as searchViewed3mo
		,sum(detailsViewed3mo) as detailsViewed3mo
		,sum(totalActions3mo) as activityCount3mo
		,CAST(sum(searchViewed3mo) as float)/NULLIF(sum(daysIncluded3mo),0) as searchPerVehicleDay3mo
		,CAST(sum(detailsViewed3mo) as float)/NULLIF(sum(daysIncluded3mo),0) as detailsPerVehicleDay3mo
		,7*CAST(sum(totalActions3mo) as float)/NULLIF(sum(daysIncluded3mo),0) as actionsPerVehicleWeek3mo
		,100*CAST(sum(detailsViewed3mo) as float)/NULLIF(sum(searchViewed3mo),0) as clickThruPercent3mo
		,100*CAST(sum(totalActions3mo) as float)/NULLIF(sum(detailsViewed3mo),0) as activityPercent3mo
	FROM (
	SELECT --ISNULL(va.make, ISNULL(va2.make, va3.make)) as make,
	va.model, --we'll get rid of any nulls (from outer join) in where
	va.daysIncluded,
	va.searchViewed,
	va.detailsViewed,
	va.emails,
	va.mapsViewed,
	va.adsPrinted,
	va.websiteClickthrus,
	va.totalActions,
	
	va2.daysIncluded as daysIncluded1mo,
	va2.searchViewed as searchViewed1mo,
	va2.detailsViewed as detailsViewed1mo,
	va2.emails as emails1mo,
	va2.mapsViewed as mapsViewed1mo,
	va2.adsPrinted as adsPrinted1mo,
	va2.websiteClickthrus as websiteClickthrus1mo,
	va2.totalActions as totalActions1mo,
	
	va3.daysIncluded as daysIncluded3mo,
	va3.searchViewed as searchViewed3mo,
	va3.detailsViewed as detailsViewed3mo,
	va3.emails as emails3mo,
	va3.mapsViewed as mapsViewed3mo,
	va3.adsPrinted as adsPrinted3mo,
	va3.websiteClickthrus as websiteClickthrus3mo,
	va3.totalActions as totalActions3mo
	
	FROM
	(
		SELECT vin,
			daysIncluded,
			searchViewed,
			detailsViewed,
			emails,
			mapsViewed,
			adsPrinted,
			websiteClickthrus,
			totalActions,
			model
		FROM #VehicleAggregate
		WHERE (@ModelFilter = '' OR model like @ModelFilter) AND
				(vehicleYear BETWEEN @StartYearFilter AND @EndYearFilter) AND
				(@SegmentFilter = '' OR segment LIKE @SegmentFilter) AND
				(@MakeFilter = '' OR make like @MakeFilter)
				AND dateRange = 'cur'		
	) as va
	FULL OUTER JOIN (
		SELECT vin,
			daysIncluded,
			searchViewed,
			detailsViewed,
			emails,
			mapsViewed,
			adsPrinted,
			websiteClickthrus,
			totalActions,
			model
		FROM #VehicleAggregate
		WHERE (@ModelFilter = '' OR model like @ModelFilter) AND
				(vehicleYear BETWEEN @StartYearFilter AND @EndYearFilter) AND
				(@SegmentFilter = '' OR segment LIKE @SegmentFilter) AND
				(@MakeFilter = '' OR make like @MakeFilter)
				AND dateRange = '1mo'		
	) as va2
	ON va.vin = va2.vin
	FULL OUTER JOIN (
		SELECT vin,
			daysIncluded,
			searchViewed,
			detailsViewed,
			emails,
			mapsViewed,
			adsPrinted,
			websiteClickthrus,
			totalActions,
			model
		FROM #VehicleAggregate
		WHERE (@ModelFilter = '' OR model like @ModelFilter) AND
				(vehicleYear BETWEEN @StartYearFilter AND @EndYearFilter) AND
				(@SegmentFilter = '' OR segment LIKE @SegmentFilter) AND
				(@MakeFilter = '' OR make like @MakeFilter)
				AND dateRange = '3mo'		
	) as va3
	ON va2.vin = va3.vin
	) as myrows
	WHERE NOT model IS NULL
	GROUP BY model
	ORDER BY vehiclesIncluded DESC
END

if @GroupParameter = 3  --group by year
BEGIN
	select vehicleYear,count(*) as vehiclesIncluded
		,sum(daysIncluded) as daysIncluded
		,sum(searchViewed) as searchViewed
		,sum(detailsViewed) as detailsViewed
		,sum(emails) as emails
		,sum(mapsViewed) as mapsViewed
		,sum(adsPrinted) as adsPrinted
		,sum(websiteClickthrus) as websiteClickthrus
		,sum(totalActions) as activityCount
		,CAST(sum(searchViewed) as float)/NULLIF(sum(daysIncluded),0) as searchPerVehicleDay
		,CAST(sum(detailsViewed) as float)/NULLIF(sum(daysIncluded),0) as detailsPerVehicleDay
		,7*CAST(sum(totalActions) as float)/NULLIF(sum(daysIncluded),0) as actionsPerVehicleWeek
		,100*CAST(sum(detailsViewed) as float)/NULLIF(sum(searchViewed),0) as clickThruPercent
		,100*CAST(sum(totalActions) as float)/NULLIF(sum(detailsViewed),0) as activityPercent
		,sum(searchViewed1mo) as searchViewed1mo
		,sum(detailsViewed1mo) as detailsViewed1mo
		,sum(totalActions1mo) as activityCount1mo
		,CAST(sum(searchViewed1mo) as float)/NULLIF(sum(daysIncluded1mo),0) as searchPerVehicleDay1mo
		,CAST(sum(detailsViewed1mo) as float)/NULLIF(sum(daysIncluded1mo),0) as detailsPerVehicleDay1mo
		,7*CAST(sum(totalActions1mo) as float)/NULLIF(sum(daysIncluded1mo),0) as actionsPerVehicleWeek1mo
		,100*CAST(sum(detailsViewed1mo) as float)/NULLIF(sum(searchViewed1mo),0) as clickThruPercent1mo
		,100*CAST(sum(totalActions1mo) as float)/NULLIF(sum(detailsViewed1mo),0) as activityPercent1mo
		,sum(searchViewed3mo) as searchViewed3mo
		,sum(detailsViewed3mo) as detailsViewed3mo
		,sum(totalActions3mo) as activityCount3mo
		,CAST(sum(searchViewed3mo) as float)/NULLIF(sum(daysIncluded3mo),0) as searchPerVehicleDay3mo
		,CAST(sum(detailsViewed3mo) as float)/NULLIF(sum(daysIncluded3mo),0) as detailsPerVehicleDay3mo
		,7*CAST(sum(totalActions3mo) as float)/NULLIF(sum(daysIncluded3mo),0) as actionsPerVehicleWeek3mo
		,100*CAST(sum(detailsViewed3mo) as float)/NULLIF(sum(searchViewed3mo),0) as clickThruPercent3mo
		,100*CAST(sum(totalActions3mo) as float)/NULLIF(sum(detailsViewed3mo),0) as activityPercent3mo
	
	FROM (
	SELECT 
	va.vehicleYear, --we'll get rid of any nulls (from outer join) in where
	va.daysIncluded,
	va.searchViewed,
	va.detailsViewed,
	va.emails,
	va.mapsViewed,
	va.adsPrinted,
	va.websiteClickthrus,
	va.totalActions,
	
	va2.daysIncluded as daysIncluded1mo,
	va2.searchViewed as searchViewed1mo,
	va2.detailsViewed as detailsViewed1mo,
	va2.emails as emails1mo,
	va2.mapsViewed as mapsViewed1mo,
	va2.adsPrinted as adsPrinted1mo,
	va2.websiteClickthrus as websiteClickthrus1mo,
	va2.totalActions as totalActions1mo,
	
	va3.daysIncluded as daysIncluded3mo,
	va3.searchViewed as searchViewed3mo,
	va3.detailsViewed as detailsViewed3mo,
	va3.emails as emails3mo,
	va3.mapsViewed as mapsViewed3mo,
	va3.adsPrinted as adsPrinted3mo,
	va3.websiteClickthrus as websiteClickthrus3mo,
	va3.totalActions as totalActions3mo
	
	FROM
	(
		SELECT vin,
			daysIncluded,
			searchViewed,
			detailsViewed,
			emails,
			mapsViewed,
			adsPrinted,
			websiteClickthrus,
			totalActions,
			vehicleYear
		FROM #VehicleAggregate
		WHERE (@ModelFilter = '' OR model like @ModelFilter) AND
				(vehicleYear BETWEEN @StartYearFilter AND @EndYearFilter) AND
				(@SegmentFilter = '' OR segment LIKE @SegmentFilter) AND
				(@MakeFilter = '' OR make like @MakeFilter)
				AND dateRange = 'cur'		
	) as va
	FULL OUTER JOIN (
		SELECT vin,
			daysIncluded,
			searchViewed,
			detailsViewed,
			emails,
			mapsViewed,
			adsPrinted,
			websiteClickthrus,
			totalActions,
			vehicleYear
		FROM #VehicleAggregate
		WHERE (@ModelFilter = '' OR model like @ModelFilter) AND
				(vehicleYear BETWEEN @StartYearFilter AND @EndYearFilter) AND
				(@SegmentFilter = '' OR segment LIKE @SegmentFilter) AND
				(@MakeFilter = '' OR make like @MakeFilter)
				AND dateRange = '1mo'		
	) as va2
	ON va.vin = va2.vin
	FULL OUTER JOIN (
		SELECT vin,
			daysIncluded,
			searchViewed,
			detailsViewed,
			emails,
			mapsViewed,
			adsPrinted,
			websiteClickthrus,
			totalActions,
			vehicleYear
		FROM #VehicleAggregate
		WHERE (@ModelFilter = '' OR model like @ModelFilter) AND
				(vehicleYear BETWEEN @StartYearFilter AND @EndYearFilter) AND
				(@SegmentFilter = '' OR segment LIKE @SegmentFilter) AND
				(@MakeFilter = '' OR make like @MakeFilter)
				AND dateRange = '3mo'		
	) as va3
	ON va2.vin = va3.vin
	) as myrows
	WHERE NOT vehicleYear IS NULL
	GROUP BY vehicleYear
END	
	
if @GroupParameter = 4  --group by segment
BEGIN
	
	select segment,
		count(*) as vehiclesIncluded
		,sum(daysIncluded) as daysIncluded
		,sum(searchViewed) as searchViewed
		,sum(detailsViewed) as detailsViewed
		,sum(emails) as emails
		,sum(mapsViewed) as mapsViewed
		,sum(adsPrinted) as adsPrinted
		,sum(websiteClickthrus) as websiteClickthrus
		,sum(totalActions) as activityCount
		,CAST(sum(searchViewed) as float)/NULLIF(sum(daysIncluded),0) as searchPerVehicleDay
		,CAST(sum(detailsViewed) as float)/NULLIF(sum(daysIncluded),0) as detailsPerVehicleDay
		,7*CAST(sum(totalActions) as float)/NULLIF(sum(daysIncluded),0) as actionsPerVehicleWeek
		,100*CAST(sum(detailsViewed) as float)/NULLIF(sum(searchViewed),0) as clickThruPercent
		,100*CAST(sum(totalActions) as float)/NULLIF(sum(detailsViewed),0) as activityPercent
		,sum(searchViewed1mo) as searchViewed1mo
		,sum(detailsViewed1mo) as detailsViewed1mo
		,sum(totalActions1mo) as activityCount1mo
		,CAST(sum(searchViewed1mo) as float)/NULLIF(sum(daysIncluded1mo),0) as searchPerVehicleDay1mo
		,CAST(sum(detailsViewed1mo) as float)/NULLIF(sum(daysIncluded1mo),0) as detailsPerVehicleDay1mo
		,7*CAST(sum(totalActions1mo) as float)/NULLIF(sum(daysIncluded1mo),0) as actionsPerVehicleWeek1mo
		,100*CAST(sum(detailsViewed1mo) as float)/NULLIF(sum(searchViewed1mo),0) as clickThruPercent1mo
		,100*CAST(sum(totalActions1mo) as float)/NULLIF(sum(detailsViewed1mo),0) as activityPercent1mo
		,sum(searchViewed3mo) as searchViewed3mo
		,sum(detailsViewed3mo) as detailsViewed3mo
		,sum(totalActions3mo) as activityCount3mo
		,CAST(sum(searchViewed3mo) as float)/NULLIF(sum(daysIncluded3mo),0) as searchPerVehicleDay3mo
		,CAST(sum(detailsViewed3mo) as float)/NULLIF(sum(daysIncluded3mo),0) as detailsPerVehicleDay3mo
		,7*CAST(sum(totalActions3mo) as float)/NULLIF(sum(daysIncluded3mo),0) as actionsPerVehicleWeek3mo
		,100*CAST(sum(detailsViewed3mo) as float)/NULLIF(sum(searchViewed3mo),0) as clickThruPercent3mo
		,100*CAST(sum(totalActions3mo) as float)/NULLIF(sum(detailsViewed3mo),0) as activityPercent3mo
	FROM (
	SELECT --ISNULL(va.make, ISNULL(va2.make, va3.make)) as make,
	va.segment, --we'll get rid of any nulls (from outer join) in where
	va.daysIncluded,
	va.searchViewed,
	va.detailsViewed,
	va.emails,
	va.mapsViewed,
	va.adsPrinted,
	va.websiteClickthrus,
	va.totalActions,
	
	va2.daysIncluded as daysIncluded1mo,
	va2.searchViewed as searchViewed1mo,
	va2.detailsViewed as detailsViewed1mo,
	va2.emails as emails1mo,
	va2.mapsViewed as mapsViewed1mo,
	va2.adsPrinted as adsPrinted1mo,
	va2.websiteClickthrus as websiteClickthrus1mo,
	va2.totalActions as totalActions1mo,
	
	va3.daysIncluded as daysIncluded3mo,
	va3.searchViewed as searchViewed3mo,
	va3.detailsViewed as detailsViewed3mo,
	va3.emails as emails3mo,
	va3.mapsViewed as mapsViewed3mo,
	va3.adsPrinted as adsPrinted3mo,
	va3.websiteClickthrus as websiteClickthrus3mo,
	va3.totalActions as totalActions3mo
	
	FROM
	(
		SELECT vin,
			daysIncluded,
			searchViewed,
			detailsViewed,
			emails,
			mapsViewed,
			adsPrinted,
			websiteClickthrus,
			totalActions,
			segment
		FROM #VehicleAggregate
		WHERE (@ModelFilter = '' OR model like @ModelFilter) AND
				(vehicleYear BETWEEN @StartYearFilter AND @EndYearFilter) AND
				(@SegmentFilter = '' OR segment LIKE @SegmentFilter) AND
				(@MakeFilter = '' OR make like @MakeFilter)
				AND dateRange = 'cur'		
	) as va
	FULL OUTER JOIN (
		SELECT vin,
			daysIncluded,
			searchViewed,
			detailsViewed,
			emails,
			mapsViewed,
			adsPrinted,
			websiteClickthrus,
			totalActions,
			segment
		FROM #VehicleAggregate
		WHERE (@ModelFilter = '' OR model like @ModelFilter) AND
				(vehicleYear BETWEEN @StartYearFilter AND @EndYearFilter) AND
				(@SegmentFilter = '' OR segment LIKE @SegmentFilter) AND
				(@MakeFilter = '' OR make like @MakeFilter)
				AND dateRange = '1mo'		
	) as va2
	ON va.vin = va2.vin
	FULL OUTER JOIN (
		SELECT vin,
			daysIncluded,
			searchViewed,
			detailsViewed,
			emails,
			mapsViewed,
			adsPrinted,
			websiteClickthrus,
			totalActions,
			segment
		FROM #VehicleAggregate
		WHERE (@ModelFilter = '' OR model like @ModelFilter) AND
				(vehicleYear BETWEEN @StartYearFilter AND @EndYearFilter) AND
				(@SegmentFilter = '' OR segment LIKE @SegmentFilter) AND
				(@MakeFilter = '' OR make like @MakeFilter)
				AND dateRange = '3mo'		
	) as va3
	ON va2.vin = va3.vin
	) as myrows
	WHERE NOT segment IS NULL
	GROUP BY segment
	
END



DROP TABLE #Inventory
DROP TABLE #VehicleAggregate
SET ANSI_WARNINGS ON

END
GO

GRANT EXECUTE ON [postings].[WebMetricReport#Fetch] TO MerchandisingUser 
GO