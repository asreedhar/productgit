use Merchandising
go

if object_id('Reports.GetVehicleActivityInfo') is not null
	drop procedure Reports.GetVehicleActivityInfo
go


create procedure Reports.GetVehicleActivityInfo
	@businessUnitId int,
	@usedOrNew tinyint,
	@minAgeInDays int
as

set nocount on
set transaction isolation level read uncommitted

select *
from Merchandising.Reports.VehicleActivityInfo
where BusinessUnitId = @businessUnitId and ( @usedOrNew is null or @usedOrNew = 0 or InventoryType = @usedOrNew ) and AgeInDays >= @minAgeInDays
go

grant execute on Reports.GetVehicleActivityInfo to MerchandisingUser
go
