if object_id('Reports.EmailMaxAdUsage') is not null
	drop procedure Reports.EmailMaxAdUsage
go


/*
	exec Reports.EmailMaxAdUsage
		@recipients = 'dspeer@incisent.com'
		, @copy_recipients  = ''
		, @blind_copy_recipients = ''
	
*/
create procedure Reports.EmailMaxAdUsage
	@recipients varchar(max),
	@copy_recipients varchar(max),
	@blind_copy_recipients varchar(max)
as
set nocount on
set transaction isolation level read uncommitted


declare @body nvarchar(max)





-- setup header HTML
set @body =
		N'<H1>Max Ad Usage By Store (' + convert(char(10), getdate(), 101) + ')</H1>' +
N'
<br>
Filters:
<ul>
	<li>active inventory only</li>
	<li>ListPrice > $0</li>
	<li>vehicle is not moved offline</li>
</ul>
<br>
' +
		N'<table border="1">' +
		N'<tr><th></th><th colspan="4"></th><th colspan="8">Ads Approved by Week</th><th></th></tr>' +
		N'<tr><th></th><th colspan="4">Inventory Counts</th><th colspan="4">Used Cars</th><th colspan="4">New Cars</th><th></th></tr>' +
		N'<tr>' +
		N'<th>BusinessUnit</th><th>Used Cars</th><th>Used Cars w/Ads</th><th>New Cars</th><th>New Cars w/Ads</th>'
		
		
select
	@body = @body 
	+ N'<th>' + convert(nchar(10), WeekLabel, 101) + '</th>'
from Merchandising.Reports.MaxAdUsageWeeks()
order by WeekNumber

select
	@body = @body 
	+ N'<th>' + convert(nchar(10), WeekLabel, 101) + '</th>'
from Merchandising.Reports.MaxAdUsageWeeks()
order by WeekNumber		
				
set @body = @body +
		N'<th>BU Code</th>' +
		N'</tr>'
		
				
		
set @body = @body + 
	cast
	(
		(
			select
				td = BusinessUnit, '',
				td = isnull([UsedCars], ''), '',
				td = isnull([UsedCarswAds], ''), '',
				td = isnull([NewCars], ''), '',
				td = isnull([NewCarswAds], ''), '',
				td = isnull(UsedWeek1Ads, ''), '',
				td = isnull(UsedWeek2Ads, ''), '',
				td = isnull(UsedWeek3Ads, ''), '',
				td = isnull(UsedWeek4Ads, ''), '',
				td = isnull(NewWeek1Ads, ''), '',
				td = isnull(NewWeek2Ads, ''), '',
				td = isnull(NewWeek3Ads, ''), '',
				td = isnull(NewWeek4Ads, ''), '',
				td = BusinessUnitCode, ''
			from Merchandising.Reports.MaxAdUsage
			order by
				BusinessUnit
			for xml path('tr'), type
		)
		as nvarchar(max)
	)	


set @body = @body + N'</table>'
			



exec msdb.dbo.sp_send_dbmail
	@profile_name = 'SQL Agent Email Profile'
	, @recipients = @recipients
	, @copy_recipients = @copy_recipients
	, @blind_copy_recipients = @blind_copy_recipients
	, @subject = 'Report: Max Ad Usage By Store'
    , @body = @body
    , @body_format = 'HTML'
    
go
    