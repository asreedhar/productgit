if exists (select * from dbo.sysobjects where id = object_id(N'[alerts].[Subscription#Create]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [alerts].[Subscription#Create]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: alerts_subscription_create.sql,v 1.1 2009/01/05 21:23:51 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * create subscription for user
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
	@ReportId int,
	@BusinessUnitId int,
	@MemberId int,
	@MemberLogin varchar(80),
	@EmailAddress - destination email for alert
	@DayOfWeek int, -- day of week for subscription
	@TimeOfDay int -- time of day for subscription (24 hour clock)
 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [alerts].[Subscription#Create]
	-- Add the parameters for the stored procedure here
	@ReportId int,
	@BusinessUnitId int,
	@MemberId int,
	@MemberLogin varchar(80),
	@EmailAddress varchar(100),
	@DayOfWeek int,
	@TimeOfDay int = 5
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO alerts.subscriptions (reportId, businessUnitId, memberId, memberLogin, emailAddress,[dayOfWeek], timeOfDay)
	VALUES (@ReportId, @BusinessUnitId, @MemberId, @MemberLogin, @EmailAddress,@DayOfWeek, @TimeOfDay)
	
END
GO

GRANT EXECUTE ON [alerts].[Subscription#Create] TO MerchandisingUser 
GO