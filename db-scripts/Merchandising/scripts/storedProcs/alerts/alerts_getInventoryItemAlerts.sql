if exists (select * from dbo.sysobjects where id = object_id(N'[alerts].[getInventoryItemAlerts]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [alerts].[getInventoryItemAlerts]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: alerts_getInventoryItemAlerts.sql,v 1.6 2010/01/06 20:06:14 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets all inventory items that meet the input var filter criteria
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 *	@StockNumber varchar(17) = '', - defaults to returning a list of stock items...
 *  @Vin varchar(17) = '' - defaults to returning all vins -- suggest only using one of stock/vin
 *	@merchandisingStatus int = 240 - defaults to all merchandising statuses valid values are - 16 - not posted, 32 - pending approval, 64 - online needs attention, 128 - online
 *	@inventoryType int = 0, - defaults to all inventory.  valid vals are 0 - all, 1 - new, 2 - used
 *	@activityFilter int = 7 - defaults to all activities.  valid vals are 1 - needs pricing, 2 - needs description attention, 4 - needs photos
 *  @searchParameter varchar(20) - this can be a text segment representing a stocknumber, make, or model
 * @minAgeInDays int = the minimum age of a vehicle to return (used for alerting purposes)
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [alerts].[getInventoryItemAlerts]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@merchandisingStatus int = 240,
	@activityFilter int = 7,
	@minAgeInDays int,
	@UsedOrNew int = 2
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	CREATE TABLE #Inventory (
		BusinessUnitId int NOT NULL,
		Vin varchar(17) NOT NULL,
		StockNumber varchar(15) NOT NULL,
		InventoryReceivedDate datetime NOT NULL, 
		vehicleYear int NOT NULL, 
		UnitCost decimal(9,2) NOT NULL, 
		AcquisitionPrice decimal (8,2) NOT NULL, 
		Certified tinyint NOT NULL, 
		Make varchar(20) NOT NULL, 
		model varchar(50) NOT NULL,
		VehicleTrim varchar(50) NULL,
		inventoryID int NOT NULL,
		MileageReceived int NULL, 
		TradeOrPurchase tinyint NOT NULL, 
		ListPrice decimal(8,2) NULL,
		InventoryType tinyint NOT NULL,
		InventoryStatusCD int NOT NULL,
		BaseColor varchar(50) NOT NULL,
		VehicleLocation varchar(20) NULL,
		LotPrice DECIMAL(8,2) NULL,
		MSRP DECIMAL(9,2) NULL,
		VehicleCatalogId int NULL
		,PRIMARY KEY (businessUnitId, inventoryId)
	)

	INSERT INTO #Inventory EXEC Interface.InventoryList#Fetch @BusinessUnitId=@BusinessUnitID, @UsedOrNew=@UsedOrNew

	CREATE TABLE #Thumbnails (
		imageThumbnailUrl varchar(200),
		inventoryId int,
		businessUnitId int,
		photoCount int
	)
	INSERT INTO #Thumbnails EXEC builder.getThumbnails @BusinessUnitID



SELECT 

Vin, 
inv.StockNumber, 
InventoryReceivedDate, 
vehicleYear, 
UnitCost, 
AcquisitionPrice, 
Certified, 
ISNULL(div.DivisionName, inv.Make) as make, 
ISNULL(mdl.ModelName,inv.model) as model, 
COALESCE(sty.trim, inv.vehicleTrim, '') as trim, 
inv.inventoryID,
ISNULL(vp.imageThumbnailUrl, '') as thumbnailUrl, 
ISNULL(vp.photoCount, 0) as photoCount,
ISNULL(ll.lotLocationId, '') as lotLocationId,
ISNULL(oc.CertificationTypeId, -1) as certificationTypeId,
ISNULL(mc.name, '') as certificationType,
MileageReceived, 
ListPrice, 
BaseColor,
inv.VehicleLocation,
InventoryStatusCD,
ISNULL(st.exteriorStatus, 0) as exteriorStatus, 
ISNULL(st.interiorStatus, 0) as interiorStatus,
ISNULL(st.photoStatus, 0) as photoStatus, 
ISNULL(st.adReviewStatus, 0) as adReviewStatus,
ISNULL(st.pricingStatus, 0) as pricingStatus, 
ISNULL(st.postingStatus, 0) as postingStatus,
ISNULL(oc.onlineFlag, 0) as onlineFlag


    FROM #Inventory inv
    LEFT JOIN builder.OptionsConfiguration oc ON oc.inventoryId = inv.inventoryId AND oc.businessUnitID = inv.businessUnitID
    LEFT JOIN settings.LotLocations ll ON ll.lotLocationId = oc.lotLocationId AND ll.businessUnitID = oc.businessUnitID
    LEFT JOIN builder.manufacturerCertifications mc ON mc.certificationTypeId = oc.CertificationTypeId
    LEFT JOIN #Thumbnails vp ON inv.businessUnitID = vp.businessUnitID AND inv.inventoryId = vp.inventoryId
    LEFT JOIN vehicleCatalog.chrome.styles sty ON sty.styleID = oc.chromestyleid and sty.countrycode = 1                    
	LEFT JOIN vehicleCatalog.chrome.models mdl ON mdl.modelID = sty.modelID and mdl.countrycode = 1
	LEFT JOIN vehicleCatalog.chrome.Divisions div ON div.divisionID = mdl.divisionID and div.countrycode = 1
	LEFT JOIN (
		SELECT InventoryId, BusinessUnitId, 
			ISNULL([1], 0) as exteriorStatus,
			ISNULL([2], 0) as interiorStatus,
			ISNULL([3], 0) as photoStatus,
			ISNULL([4], 0) as pricingStatus,
			ISNULL([5], 0) as postingStatus,
			ISNULL([6], 0) as adReviewStatus
			FROM 
			(SELECT InventoryId, BusinessUnitId, StatusTypeId, StatusLevel
				FROM builder.VehicleStatus) vs
			PIVOT
			(
			max(statusLevel)
			FOR StatusTypeId
			IN ([1],[2],[3],[4],[5],[6])
			) as pvt
	) st ON st.inventoryId = oc.inventoryId AND st.businessUnitId = oc.businessUnitId
    WHERE 
	(@UsedOrNew = 0 OR inv.inventoryType = @UsedOrNew)
	AND (DateDiff(d, inventoryReceivedDate, getdate()) >= @minAgeInDays
	AND (@merchandisingStatus = 240  --ALL BUCKETS
			OR (@merchandisingStatus = 16  --NOT ONLINE NEEDS ATTENTION
					AND (
							(oc.onlineFlag IS NULL OR oc.onlineFlag = 0  --not online flag (flag = zero)
							)
							AND (
								st.postingStatus IS NULL OR st.postingStatus <= 1)  --not posted and not pending
					)
				)
			OR (@merchandisingStatus = 32  --PENDING APPROVAL
				AND (st.postingStatus = 2) 
				)
			OR (@merchandisingStatus = 64  --ONLINE AND NEEDS ATTENTION
					AND ((st.postingStatus IS NULL OR st.postingStatus <= 1) AND oc.onlineFlag = 1) 
					AND (st.exteriorStatus IS NULL OR st.exteriorStatus < 2 OR 
						st.interiorStatus IS NULL OR st.interiorStatus < 2 OR
						st.photoStatus IS NULL OR st.photoStatus < 2 OR
						st.pricingStatus IS NULL OR st.pricingStatus < 2)) -- TODO: Does AdReviewStatus need to go here?
			OR (@merchandisingStatus = 128 --ONLINE
					AND st.postingStatus = 3)  --posting status = 3 (successful release)
		)
	
	AND (@activityFilter = 7 
		OR	(@activityFilter = 1 AND (st.pricingStatus IS NULL OR st.pricingStatus < 2)) 
		OR	(@activityFilter = 2 AND (st.interiorStatus IS NULL OR st.interiorStatus < 2 OR
									  st.exteriorStatus IS NULL OR st.exteriorStatus < 2)) 
		OR	(@activityFilter = 4 AND (st.photoStatus IS NULL OR st.photoStatus < 2)) -- TODO: Does AdReviewStatus need to go here?
		)
	)
	ORDER by inventoryReceivedDate ASC
	
	DROP TABLE #Inventory
	DROP TABLE #Thumbnails
END
GO

GRANT EXECUTE ON [alerts].[getInventoryItemAlerts] TO MerchandisingUser 
GO