if exists (select * from dbo.sysobjects where id = object_id(N'[alerts].[LoadSummarySubscriptions#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [alerts].[LoadSummarySubscriptions#Fetch]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <4/9/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: alerts_LoadSummarySubscriptions_fetch.sql,v 1.2 2009/04/22 14:39:32 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch summary report
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [alerts].[LoadSummarySubscriptions#Fetch]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select s.businessUnitId, s.memberId, s.memberLogin, s.emailAddress
	
	FROM alerts.subscriptions s 
	INNER JOIN settings.alertingPreferences ap
		ON s.businessUnitId = ap.businessUnitId
	WHERE [dayOfWeek]  = datepart(dw, getdate())
			AND timeOfDay = datepart(hh, getdate())
			and reportId = 6 --summary report
END
GO

GRANT EXECUTE ON [alerts].[LoadSummarySubscriptions#Fetch] TO MerchandisingUser 
GO