if exists (select * from dbo.sysobjects where id = object_id(N'[alerts].[stillPending#fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [alerts].[stillPending#fetch]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: alerts_stillPending_fetch.sql,v 1.5 2009/11/09 17:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets all inventory items that are still not online after a specified number of days
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @minAgeInDays int = the minimum age of a vehicle to return (used for alerting purposes)
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [alerts].[stillPending#fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@minAgeInDays int,
	@UsedOrNew int = 2
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	CREATE TABLE #Inventory (
		BusinessUnitId int NOT NULL,
		Vin varchar(17) NOT NULL,
		StockNumber varchar(15) NOT NULL,
		InventoryReceivedDate datetime NOT NULL, 
		vehicleYear int NOT NULL, 
		UnitCost decimal(9,2) NOT NULL, 
		AcquisitionPrice decimal (8,2) NOT NULL, 
		Certified tinyint NOT NULL, 
		Make varchar(20) NOT NULL, 
		model varchar(50) NOT NULL,
		VehicleTrim varchar(50) NULL,
		inventoryID int NOT NULL,
		MileageReceived int NULL, 
		TradeOrPurchase tinyint NOT NULL, 
		ListPrice decimal(8,2) NULL,
		InventoryType tinyint NOT NULL,
		InventoryStatusCD int NOT NULL,
		BaseColor varchar(50) NOT NULL,
		VehicleLocation varchar(20) NULL,
		LotPrice DECIMAL(8,2) NULL,
		MSRP DECIMAL(9,2) NULL,
		VehicleCatalogId int NULL
		,PRIMARY KEY (businessUnitId, inventoryId)
	)

	INSERT INTO #Inventory EXEC Interface.InventoryList#Fetch @BusinessUnitId=@BusinessUnitID, @UsedOrNew=@UsedOrNew


SELECT 

Vin, 
inv.StockNumber, 
InventoryReceivedDate, 
vehicleYear, 
UnitCost, 
AcquisitionPrice, 
Certified, 
ISNULL(div.DivisionName, inv.Make) as make, 
ISNULL(mdl.ModelName,inv.model) as model, 
COALESCE(sty.trim, inv.VehicleTrim, '') as trim, 
inv.inventoryID,
ISNULL(ll.lotLocationId, '') as lotLocationId,
ISNULL(oc.CertificationTypeId, -1) as certificationTypeId,
ISNULL(mc.name, '') as certificationType,
MileageReceived, 
ListPrice,
BaseColor,
inv.VehicleLocation,
InventoryStatusCD,
daysPending
    FROM #Inventory inv
    LEFT JOIN builder.OptionsConfiguration oc ON oc.inventoryId = inv.inventoryId AND oc.businessUnitID = inv.businessUnitID
    LEFT JOIN settings.LotLocations ll ON ll.lotLocationId = oc.lotLocationId AND ll.businessUnitID = oc.businessUnitID
    LEFT JOIN builder.manufacturerCertifications mc ON mc.certificationTypeId = oc.CertificationTypeId
    LEFT JOIN vehicleCatalog.chrome.styles sty ON sty.styleID = oc.chromestyleid and sty.countrycode = 1                    
	LEFT JOIN vehicleCatalog.chrome.models mdl ON mdl.modelID = sty.modelID and mdl.countrycode = 1
	LEFT JOIN vehicleCatalog.chrome.Divisions div ON div.divisionID = mdl.divisionID and div.countrycode = 1
	LEFT JOIN (SELECT datediff(d, st.lastUpdatedOn, getdate()) as daysPending, 
				statusTypeId, 
				statusLevel, inventoryId, businessUnitId
			FROM builder.VehicleStatus st 
			) as st
			ON st.inventoryid = oc.inventoryId 
			AND st.businessUnitId = oc.businessUnitId
	WHERE st.statusTypeId = 5 --posting status
		AND st.statusLevel = 2 --2 is pending
		AND st.daysPending > @minAgeInDays
		AND (@UsedOrNew = 0 OR inv.inventoryType = @UsedOrNew)
			
	ORDER BY inventoryReceivedDate ASC
	
DROP TABLE #Inventory

END
GO

GRANT EXECUTE ON [alerts].[stillPending#fetch] TO MerchandisingUser 
GO