if exists (select * from dbo.sysobjects where id = object_id(N'[alerts].[LoadNotOnlineSubscriptions#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [alerts].[LoadNotOnlineSubscriptions#Fetch]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: alerts_LoadNotOnlineSubscriptions_fetch.sql,v 1.1 2009/01/05 21:23:51 jkelley Exp $$
 * 
 * Summary
 * -------
 * 
 * fetch still not online report subscription rows 
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [alerts].[LoadNotOnlineSubscriptions#Fetch]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	select businessUnitId, memberId, memberLogin, emailAddress, [dayOfWeek], timeOfDay,
		daysToApproval, maxDaysToApprovedAfterPending,
		[4] as includeStillPending,[5] as includeStillNotOnline
	
	FROM (
		SELECT reportId, s.memberId, memberLogin, emailAddress, [dayOfWeek], timeOfDay, s.businessUnitId, 
			daysToApproval, maxDaysToApprovedAfterPending
		FROM alerts.subscriptions s 
		INNER JOIN settings.alertingPreferences ap
			ON s.businessUnitId = ap.businessUnitId
		WHERE [dayOfWeek]  = datepart(dw, getdate())
			AND timeOfDay = datepart(hh, getdate())) p --11) as p --
	PIVOT (
		count(reportId)
	FOR reportId
	IN ([4],[5])
	) as pvt		
END
GO

GRANT EXECUTE ON [alerts].[LoadNotOnlineSubscriptions#Fetch] TO MerchandisingUser 
GO