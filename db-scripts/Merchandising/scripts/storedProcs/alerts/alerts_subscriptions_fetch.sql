if exists (select * from dbo.sysobjects where id = object_id(N'[alerts].[Subscriptions#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [alerts].[Subscriptions#Fetch]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: alerts_subscriptions_fetch.sql,v 1.1 2009/01/05 21:23:51 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Fetchs subscriptions for the user for a given report
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 *	@MemberId int - member id for whom to Fetch subscriptions
 * @ReportId int
 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [alerts].[Subscriptions#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@MemberId int,
	@ReportId int = NULL

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select businessUnitId, memberLogin, reportId,emailAddress,timeOfDay,
		[1],[2],[3],[4],[5],[6],[7]
	FROM (
		SELECT reportId, s.memberId, memberLogin, emailAddress,[dayOfWeek], timeOfDay, s.businessUnitId
		FROM alerts.subscriptions s 
		WHERE businessUnitId = @BusinessUnitId
		AND memberId = @MemberId
		AND (reportId = @ReportId OR reportId IS NULL)) as p
	PIVOT (
		count(memberId)
	FOR [dayOfWeek]
	IN ([1],[2],[3],[4],[5],[6],[7])
	) as pvt		
	

END
GO

GRANT EXECUTE ON [alerts].[Subscriptions#Fetch] TO MerchandisingUser 
GO