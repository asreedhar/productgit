if exists (select * from dbo.sysobjects where id = object_id(N'[alerts].[getPhotoAlerts]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [alerts].[getPhotoAlerts]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: alerts_getPhotoAlerts.sql,v 1.6 2010/01/06 20:06:14 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets all inventory items that meet the input var filter criteria
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 *	@merchandisingStatus int = 240 - defaults to all merchandising statuses valid values are - 16 - not posted, 32 - pending approval, 64 - online needs attention, 128 - online
 *  @searchParameter varchar(20) - this can be a text segment representing a stocknumber, make, or model
 * @minAgeInDays int = the minimum age of a vehicle to return (used for alerting purposes)
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [alerts].[getPhotoAlerts]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@maxDaysToPhotoComplete int,
	@maxDaysToFirstPhoto int,
	@UsedOrNew int = 2
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	CREATE TABLE #Inventory (
		BusinessUnitId int NOT NULL,
		Vin varchar(17) NOT NULL,
		StockNumber varchar(15) NOT NULL,
		InventoryReceivedDate datetime NOT NULL, 
		vehicleYear int NOT NULL, 
		UnitCost decimal(9,2) NOT NULL, 
		AcquisitionPrice decimal (8,2) NOT NULL, 
		Certified tinyint NOT NULL, 
		Make varchar(20) NOT NULL, 
		model varchar(50) NOT NULL,
		VehicleTrim varchar(50) NULL,
		inventoryID int NOT NULL,
		MileageReceived int NULL, 
		TradeOrPurchase tinyint NOT NULL, 
		ListPrice decimal(8,2) NULL,
		InventoryType tinyint NOT NULL,
		InventoryStatusCD int NOT NULL,
		BaseColor varchar(50) NOT NULL,
		VehicleLocation varchar(20) NULL,
		LotPrice DECIMAL(8,2) NULL,
		MSRP DECIMAL(9,2) NULL,
		VehicleCatalogId int NULL
		,PRIMARY KEY (businessUnitId, inventoryId)
	)

	INSERT INTO #Inventory EXEC Interface.InventoryList#Fetch @BusinessUnitId=@BusinessUnitID, @UsedOrNew=@UsedOrNew

	CREATE TABLE #Thumbnails (
		imageThumbnailUrl varchar(200),
		inventoryId int,
		businessUnitId int,
		photoCount int
	)
	INSERT INTO #Thumbnails EXEC builder.getThumbnails @BusinessUnitID



SELECT 

Vin, 
inv.StockNumber, 
InventoryReceivedDate, 
vehicleYear, 
UnitCost, 
AcquisitionPrice, 
Certified, 
ISNULL(div.DivisionName, inv.Make) as make, 
ISNULL(mdl.ModelName,inv.model) as model, 
COALESCE(sty.trim, inv.VehicleTrim, '') as trim, 
inv.inventoryID,
ISNULL(vp.imageThumbnailUrl, '') as thumbnailUrl, 
ISNULL(vp.photoCount, 0) as photoCount,
ISNULL(ll.lotLocationId, '') as lotLocationId,
ISNULL(oc.CertificationTypeId, -1) as certificationTypeId,
ISNULL(mc.name, '') as certificationType,
MileageReceived, 
ListPrice, 
BaseColor,
inv.VehicleLocation,
InventoryStatusCD,
ISNULL(st.photoStatus, 0) as photoStatus, 
ISNULL(st.postingStatus, 0) as postingStatus,
ISNULL(oc.onlineFlag, 0) as onlineFlag


    FROM #Inventory inv
    LEFT JOIN builder.OptionsConfiguration oc ON oc.inventoryId = inv.inventoryId AND oc.businessUnitID = inv.businessUnitID
    LEFT JOIN settings.LotLocations ll ON ll.lotLocationId = oc.lotLocationId AND ll.businessUnitID = oc.businessUnitID
    LEFT JOIN builder.manufacturerCertifications mc ON mc.certificationTypeId = oc.CertificationTypeId
    LEFT JOIN #Thumbnails vp ON inv.businessUnitID = vp.businessUnitID AND inv.inventoryId = vp.inventoryId
    LEFT JOIN vehicleCatalog.chrome.styles sty ON sty.styleID = oc.chromestyleid and sty.countrycode = 1                    
	LEFT JOIN vehicleCatalog.chrome.models mdl ON mdl.modelID = sty.modelID and mdl.countrycode = 1
	LEFT JOIN vehicleCatalog.chrome.Divisions div ON div.divisionID = mdl.divisionID and div.countrycode = 1
	LEFT JOIN (
		SELECT InventoryId, BusinessUnitId, 
			ISNULL([3], 0) as photoStatus,
			ISNULL([5], 0) as postingStatus
			FROM 
			(SELECT InventoryId, BusinessUnitId, StatusTypeId, StatusLevel
				FROM builder.VehicleStatus) vs
			PIVOT
			(
			max(statusLevel)
			FOR StatusTypeId
			IN ([3],[5])
			) as pvt
	) st ON st.inventoryId = oc.inventoryId AND st.businessUnitId = oc.businessUnitId
    WHERE 
	(@UsedOrNew = 0 OR inv.inventoryType = @UsedOrNew)
	AND 
	(
		(
			DateDiff(d, inventoryReceivedDate, getdate()) >= @maxDaysToPhotoComplete 
			AND (st.photoStatus IS NULL OR st.photoStatus < 2)
		)
		OR 
		(
			DateDiff(d,inventoryReceivedDate, getdate()) >= @maxDaysToFirstPhoto
			AND (vp.photoCount IS NULL OR vp.photoCount = 0)
		)
	)
	
	
	ORDER by inventoryReceivedDate ASC
	
	DROP TABLE #Inventory
	DROP TABLE #Thumbnails
END
GO

GRANT EXECUTE ON [alerts].[getPhotoAlerts] TO MerchandisingUser 
GO