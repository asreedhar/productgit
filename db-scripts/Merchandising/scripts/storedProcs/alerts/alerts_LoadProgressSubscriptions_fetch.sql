if exists (select * from dbo.sysobjects where id = object_id(N'[alerts].[LoadProgressSubscriptions#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [alerts].[LoadProgressSubscriptions#Fetch]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: alerts_LoadProgressSubscriptions_fetch.sql,v 1.1 2009/01/05 21:23:51 jkelley Exp $$
 * 
 * Summary
 * -------
 * 
 * fetch equipment report subscription rows 
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [alerts].[LoadProgressSubscriptions#Fetch]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	select businessUnitId, memberId, memberLogin, emailAddress,[dayOfWeek], timeOfDay,
		daysToEquipmentComplete, daysToFirstPhoto, daysToPhotosComplete, daysToPricingComplete,
		[1] as includeEquipment,[2] as includePhotos,[3] as includePrice
	
	FROM (
		SELECT reportId, s.memberId, memberLogin, emailAddress,[dayOfWeek], timeOfDay, s.businessUnitId, 
			daysToEquipmentComplete, daysToFirstPhoto, daysToPhotosComplete, daysToPricingComplete
		FROM alerts.subscriptions s 
		INNER JOIN settings.alertingPreferences ap
			ON s.businessUnitId = ap.businessUnitId
		WHERE [dayOfWeek]  = datepart(dw, getdate())
			AND timeOfDay = datepart(hh, getdate())) p --11) as p 
	PIVOT (
		count(reportId)
	FOR reportId
	IN ([1],[2],[3])
	) as pvt		
END
GO

GRANT EXECUTE ON [alerts].[LoadProgressSubscriptions#Fetch] TO MerchandisingUser 
GO