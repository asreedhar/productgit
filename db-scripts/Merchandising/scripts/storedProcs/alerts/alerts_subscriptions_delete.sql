if exists (select * from dbo.sysobjects where id = object_id(N'[alerts].[Subscriptions#Delete]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [alerts].[Subscriptions#Delete]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: alerts_subscriptions_delete.sql,v 1.1 2009/01/05 21:23:51 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * deletes all subscriptions for the user
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 *	@MemberId int - member id for whom to delete subscriptions
 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [alerts].[Subscriptions#Delete]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@MemberId int

	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DELETE FROM alerts.subscriptions 
	WHERE businessUnitId = @BusinessUnitId
	AND memberId = @MemberId
	
END
GO

GRANT EXECUTE ON [alerts].[Subscriptions#Delete] TO MerchandisingUser 
GO