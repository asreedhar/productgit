if object_id('Release.DealerWithAdvertisements#Fetch') is not null
	drop procedure Release.DealerWithAdvertisements#Fetch
go


/*
 *
 * should be ignored by the rule
 * @AdvertisementStatus - status of advertisement...
*/

create procedure Release.DealerWithAdvertisements#Fetch
	@AdvertisementStatus int
	
as
set nocount on;
set transaction isolation level read uncommitted

declare @releaseDestinations table (
	dealerId int
)

if (@AdvertisementStatus = 2)
begin	
	insert @releaseDestinations
	(
		dealerId
	)
	select distinct
		VAS.BusinessUnitID
	from
		Merchandising.postings.VehicleAdScheduling VAS
	where 
		 scheduledReleaseTime < getdate()
	     and advertisementStatus in (1,2)
end
else
begin
	insert @releaseDestinations
	(
		dealerId
	)
	select distinct
		VAS.BusinessUnitID
	from
		Merchandising.postings.VehicleAdScheduling VAS
	where 
		 scheduledReleaseTime < getdate()
	     and advertisementStatus = @AdvertisementStatus
end	

--select our valid dealers...
select distinct
	bu.businessUnitId as dealerId,
	bu.businessUnit as dealerName
from 
	(
		select 
			b.businessUnitID,
			b.businessUnit
		from
			IMT.dbo.BusinessUnit b
			join IMT.dbo.DealerPreference p
				on p.businessUnitId = b.businessUnitId
		where
			b.businessUnitTypeID = 4
			and b.active = 1
			and p.goLiveDate is not null
				
	) bu
	join @releaseDestinations rd
		on bu.businessUnitId = rd.dealerId

GO

GRANT EXECUTE ON [Release].[DealerWithAdvertisements#Fetch] TO MerchandisingUser 
GO

