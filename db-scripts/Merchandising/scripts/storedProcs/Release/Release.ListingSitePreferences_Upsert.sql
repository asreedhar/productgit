if exists (select * from dbo.sysobjects where id = object_id(N'[Release].[ListingSitePreferences#Upsert]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Release].[ListingSitePreferences#Upsert]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/10/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Release.ListingSitePreferences_Upsert.sql,v 1.1 2010/08/04 19:13:43 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * retrieve listing site preferences for dealer
 * 
 * 
 * Parameters
 * ----------
 * 
 *
 * should be ignored by the rule
 * @BusinessUnitId - id of business unit
 * @DestinationId - id of destination --> NULL if you want all prefs
 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Release].[ListingSitePreferences#Upsert]
	-- Add the parameters for the stored procedure here
	
	@BusinessUnitId int,
	@DestinationId int,
	@ListNew bit = 1,
	@ListUsed bit = 1,
	@ListCertified bit = 1,
	@ActiveFlag bit = 1, 
	@PushFrequency int = 24, 
	@SendPhotos bit = 1, 
	@SendOptions bit = 1, 
	@SendDescription bit = 1, 
	@SendPricing bit = 0,
	@SendVideos bit = 1,
	@SendNewCars bit = 1,
	@SendUsedCars bit = 1,
	@Username varchar(50) = NULL,
	@Password varchar(50) = NULL,
	@ReleaseEnabled bit = NULL,
	@CollectionEnabled bit = NULL,
	@NewDestinationId int = NULL --only required for updates that are changing the destinationid
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF EXISTS(SELECT 1 FROM settings.dealerListingSites WHERE businessUnitId = @BusinessUnitID AND destinationId = @DestinationId)
	BEGIN
		
		if (@NewDestinationID IS NULL)
		BEGIN
			select @NewDestinationID = destinationId
			from settings.dealerListingSites 
			WHERE businessUnitId = @BusinessUnitID AND destinationId = @DestinationId
		END
		
		DECLARE @currentTime AS datetime
		SET @currentTime = GETDATE()
		
		UPDATE dls
		SET 			
			destinationID = @NewDestinationId,
			ListNew = @ListNew,
			ListUsed = @ListUsed,
			ListCertified = @ListCertified,
			activeFlag = @ActiveFlag, 
			pushFrequency = @PushFrequency, 
			sendPhotos = @SendPhotos, 
			sendOptions = @SendOptions, 
			sendDescription = @SendDescription, 
			sendPricing = @SendPricing, 
			sendVideos = @SendVideos, 
			sendNewCars = @SendNewCars, 
			sendUsedCars = @SendUsedCars,
			releaseEnabled = case when @ReleaseEnabled IS NULL then dls.releaseEnabled
								  when canReleaseTo = 0 then 0
								  else @ReleaseEnabled end, 
			collectionEnabled = case 
									when @CollectionEnabled IS NULL then dls.collectionEnabled
									when canCollectMetricsFrom = 0 then 0
									else @CollectionEnabled end,
			username = case when @Username IS NULL then dls.username else @Username end,
			password = case when @Password IS NULL then dls.password else @Password end,
			dateLoginFailureCleared = CASE 
				WHEN (dls.DateLoginFailure IS NOT NULL)
				THEN (CASE WHEN dls.dateLoginFailure > @currentTime THEN dls.dateLoginFailure ELSE @currentTime END) -- Ensure cleared date is >= failed date
				ELSE dls.dateLoginfailureCleared END
		FROM settings.dealerListingSites dls
			JOIN settings.edtDestinations ed
			ON ed.destinationId = @NewDestinationId
		WHERE 
			(businessUnitID = @BusinessUnitID) 
			AND (dls.destinationID = @DestinationID)
	END
	ELSE
	BEGIN
		INSERT INTO settings.DealerListingSites 
		(
			businessUnitID, 
			destinationID,
			ListNew,
			ListUsed,
			ListCertified,
			activeFlag, 
			pushFrequency, 
			sendPhotos, 
			sendOptions, 
			sendDescription, 
			sendPricing,
			sendVideos,
			sendNewCars,
			sendUsedCars, 
			username,
			[password],
			listingSiteDealerId,
			releaseEnabled,
			collectionEnabled
		) 
		SELECT 
		
			@BusinessUnitID, 
			@DestinationID,
			@ListNew,
			@ListUsed,
			@ListCertified,
			@ActiveFlag, 
			@PushFrequency, 
			@SendPhotos, 
			@SendOptions, 
			@SendDescription, 
			@SendPricing,
			@SendVideos,
			@SendNewCars,
			@SendUsedCars, 	
			ISNULL(@Username,''),
			ISNULL(@Password,''),
			null,
			case when @ReleaseEnabled IS NULL then canReleaseTo else @ReleaseEnabled end as releaseEnabled, 
			case when @CollectionEnabled IS NULL then canCollectMetricsFrom else @CollectionEnabled end as collectionEnabled
		FROM settings.EdtDestinations 
		WHERE destinationId = @DestinationId
	END	

				
END 

GO

GRANT EXECUTE ON [Release].[ListingSitePreferences#Upsert] TO MerchandisingUser 
GO 

