if exists (select * from dbo.sysobjects where id = object_id(N'[Release].[MissedAutoLoad#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Release].[MissedAutoLoad#Fetch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [Release].[MissedAutoLoad#Fetch]
	
AS
BEGIN


	SELECT DISTINCT Inventory.BusinessUnitID
		FROM workflow.Inventory
		INNER JOIN VehicleCatalog.Chrome.Divisions Divisions ON Inventory.Make = Divisions.DivisionName
																AND Divisions.CountryCode = 1
		INNER JOIN merchandising.VehicleAutoLoadSettings VehicleAutoLoadSettings ON Divisions.ManufacturerID = VehicleAutoLoadSettings.ManufacturerID
		INNER JOIN settings.Dealer_SiteCredentials Dealer_SiteCredentials ON VehicleAutoLoadSettings.siteId = Dealer_SiteCredentials.siteId
																				AND Inventory.BusinessUnitID = Dealer_SiteCredentials.businessUnitId
		WHERE NOT EXISTS (SELECT *
							FROM builder.AutoloadStatus
							WHERE inventory.BusinessUnitID = AutoloadStatus.businessUnitID
							AND inventory.InventoryID = AutoloadStatus.InventoryID)
		AND Dealer_SiteCredentials.isLockedOut = 0
	
END 
GO

GRANT EXECUTE ON [Release].[MissedAutoLoad#Fetch] TO MerchandisingUser 
GO
