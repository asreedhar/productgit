if exists (select * from dbo.sysobjects where id = object_id(N'[Release].[MissedAutoApprove#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Release].[MissedAutoApprove#Fetch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [Release].[MissedAutoApprove#Fetch]
	
AS
BEGIN

	select I.BusinessUnitID, AP.Status
	from merchandising.settings.AutoApprove AP
	join fldw.dbo.inventoryActive I on I.BusinessUnitID = AP.BusinessUnitID
	join imt.dbo.BusinessUnit BU ON I.BusinessUnitID = BU.BusinessUnitID
	join merchandising.builder.OptionsConfiguration OC on I.BusinessUnitID = OC.BusinessUnitID AND I.InventoryID = OC.InventoryID
	where AP.Status != 0 and BU.Active = 1 and OC.chromeStyleID != -1 and OC.doNotPostFlag = 0
	and not exists(select * from merchandising.postings.vehicleadscheduling VAS where I.BusinessUnitID = VAS.BusinessUnitID AND I.InventoryID = VAS.InventoryID )
	GROUP BY I.BusinessUnitID, AP.Status
	
END 
GO

GRANT EXECUTE ON [Release].[MissedAutoApprove#Fetch] TO MerchandisingUser 
GO