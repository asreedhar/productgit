USE [Merchandising]
GO

/****** Object:  StoredProcedure [Release].[SetBucketAlertsEmailBounced]    Script Date: 09/05/2014 22:44:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Release].[SetBucketAlertsEmailBounced]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Release].[SetBucketAlertsEmailBounced]
GO

USE [Merchandising]
GO

/****** Object:  StoredProcedure [Release].[SetBucketAlertsEmailBounced]    Script Date: 09/05/2014 22:44:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [Release].[SetBucketAlertsEmailBounced]
@BusinessUnitId int,
@EmailBounced bit = null,
@WeeklyEmailBounced bit = null,
@BionicHealthReportEmailBounced bit = null,
@GroupBionicHealthReportEmailBounced bit = null
AS
BEGIN

       UPDATE Merchandising.settings.BucketAlerts

       SET    EmailBounced = ISNULL(@EmailBounced, EmailBounced),
              WeeklyEmailBounced = ISNULL(@WeeklyEmailBounced, WeeklyEmailBounced),
              BionicHealthReportEmailBounced = ISNULL(@BionicHealthReportEmailBounced, BionicHealthReportEmailBounced),
              GroupBionicHealthReportEmailBounced = ISNULL(@GroupBionicHealthReportEmailBounced, GroupBionicHealthReportEmailBounced)

       WHERE  BusinessUnitID = @BusinessUnitID


END

GO


GRANT EXECUTE ON [Release].[SetBucketAlertsEmailBounced] TO MerchandisingUser 
GO




