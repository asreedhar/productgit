if exists (select * from dbo.sysobjects where id = object_id(N'[Release].[ThirdPartyLogin#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Release].[ThirdPartyLogin#Fetch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/10/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Release.ThirdPartyLogin_Fetch.sql,v 1.1 2010/08/04 19:13:43 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * retrieve
 * 
 * 
 * Parameters
 * ----------
 * 
 *
 * should be ignored by the rule
 * @DestinationId - id of destination
 * @BusinessUnitId - id of business unit
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Release].[ThirdPartyLogin#Fetch]
	-- Add the parameters for the stored procedure here
	@DestinationId int,
	@BusinessUnitId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @credentials table (
		userName varchar(50),
		password varchar(50)
	)
	
	INSERT INTO @credentials
	SELECT userName, password
		FROM settings.DealerListingSites
		WHERE businessUnitId = @BusinessUnitId
			AND destinationId = @DestinationId
	
	SELECT * FROM @credentials WHERE userName <> '' and password <> '';
	
END 

GO

GRANT EXECUTE ON [Release].[ThirdPartyLogin#Fetch] TO MerchandisingUser 
GO