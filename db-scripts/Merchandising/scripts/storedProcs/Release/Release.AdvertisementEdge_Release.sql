if exists (select * from dbo.sysobjects where id = object_id(N'[Release].[AdvertisementEdge#Release]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Release].[AdvertisementEdge#Release]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/10/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Release.AdvertisementEdge_Release.sql,v 1.1 2010/08/04 19:13:43 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * retrieve
 * 
 * 
 * Parameters
 * ----------
 * 
 *
 * should be ignored by the rule
 * @ThirdPartyId - third party id to which to release the advertisement information
 * @Text - merchandising description text for advert
 * @ListPrice - new internet list price for advert
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Release].[AdvertisementEdge#Release]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@InventoryId int,
	@ThirdPartyId int,
	@Text varchar(max),
	@OptionsText varchar(max),
	@ListPrice decimal,
	@ApprovedByMemberLogin varchar(30),
	@ExpiresOn datetime = NULL,
	@ExteriorColor varchar(50) = NULL,
	@InteriorColor varchar(50) = NULL,
	@InteriorType varchar(50) = NULL,
	@MappedOptionIds varchar(1000) = NULL,
	@OptionCodes varchar(300) = NULL,
	@HtmlMerchandisingDescription varchar(max),--= NULL,
	@HighlightText varchar(200) = NULL,
	@Keywords varchar(200) = NULL,
	@EquipmentList varchar(500) = null,
	@SpecialPrice int,
	@ExteriorColorCode varchar(50),
	@InteriorColorCode varchar(50)
	,@MSRP int = NULL
	,@ManufacturerRebate int = NULL
	,@DealerDiscount int = NULL
	,@GasMileageCity int = NULL
	,@GasMileageHighway int = NULL
		
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--UPDATE
	
	
	declare 
		@equipCt int, 
		@templateId int, 
		@FullStyleCode varchar(20),
		@Trim varchar(35),
		@WheelBase varchar(12),
		@ChromeStyleId int,
		@VehicleCondition varchar(50),
		@TireTread int
	
		
	SELECT @equipCt = count(*)
	FROM builder.vehicleOptions 
	WHERE businessUnitId = @BusinessUnitId 
		AND inventoryId = @InventoryId
	
	SELECT @templateId = de.templateId
		, @FullStyleCode = ISNULL(sty.FullStyleCode,'')
		, @Trim = COALESCE(NULLIF(oc.AfterMarketTrim,''), ISNULL( LEFT(sty.StyleName,50),'')) 
		, @WheelBase = CAST(swb.wheelbase as varchar(12))
		, @ChromeStyleId = NULLIF(oc.chromestyleId, -1) --silly old use of -1 instead of NULL
		, @VehicleCOndition = NULLIF(oc.vehicleCondition, '')
		, @TireTread = oc.tireTread	
	FROM builder.optionsConfiguration oc
	LEFT JOIN builder.Descriptions de
	on de.businessUnitId = oc.businessUnitId
	and de.inventoryId = oc.inventoryId
	LEFT JOIN VehicleCatalog.Chrome.Styles sty
		ON sty.styleID = oc.chromeStyleId
		AND sty.countryCode = 1
	LEFT JOIN VehicleCatalog.Chrome.StyleWheelBase swb
		ON sty.styleId = swb.chromestyleId
		and swb.countrycode = 1
	WHERE oc.businessUnitId = @BusinessUnitId 
		AND oc.inventoryId = @InventoryId
		
		
	
	EXEC Interface.Advertisement#Release 
		@BusinessUnitId=@BusinessUnitId
		,@InventoryId=@InventoryId
		,@MerchandisingDescription=@Text
		,@OptionsList=@OptionsText
		,@ExteriorColor=@ExteriorColor
		,@InteriorColor=@InteriorColor
		,@InteriorType=@InteriorType
		,@MappedOptionIds=@MappedOptionIds
		,@ChromeStyleId=@ChromeStyleId
		,@OptionCodes=@OptionCodes
		,@HtmlMerchandisingDescription=@HtmlMerchandisingDescription
		,@HighlightText=@HighlightText
		,@Keywords=@Keywords
		,@ModelCode=@FullStyleCode
		,@Trim=@Trim
		,@ExteriorColorCode=@ExteriorColorCode
		,@InteriorColorCode=@InteriorColorCode
		--,@BedLength=NULL
		,@WheelBase=@WheelBase
		,@GasMileageCity = @GasMileageCity 
		,@GasMileageHighway = @GasMileageHighway
		,@VehicleCondition = @VehicleCondition
		,@TireTread = @TireTread
		,@StandardFeaturesList = @EquipmentList
		,@SpecialPrice = @SpecialPrice 
        ,@MSRP = @MSRP
        ,@ManufacturerRebate = @ManufacturerRebate
        ,@DealerDiscount = @DealerDiscount
    
		
	
	--update the postings table to set removal date
	UPDATE postings.Publications SET dateRemoved = getdate()
	WHERE businessUnitId = @BusinessUnitId
	AND inventoryId = @InventoryId
	
	
	
	INSERT INTO postings.Publications 
	(businessUnitId
	,inventoryId
	,datePosted
	,destinationId
	,merchandisingDescription
	,equipmentCount
	,pricePosted
	,templateId
	,approvedByMemberLogin
	,dateRemoved
	,expiresOn
	,exteriorColor
	,interiorColor
	,interiorType
	,MappedOptionIds
	,chromeStyleId --styleid should cover modelcode etc that are based around the chromestyle for auditing
	,optionCodes
	)
	VALUES
	(@BusinessUnitId
	,@InventoryId
	,getdate()
	,NULL		--destinationId disabled for now...
	,@Text
	,@equipCt
	,@ListPrice
	,ISNULL(@templateId,-1)
	,@ApprovedByMemberLogin
	,NULL		--no date removed yet
	,@ExpiresOn
	,@ExteriorColor
	,@InteriorColor
	,@InteriorType
	,@MappedOptionIds
	,@ChromeStyleId
	,@OptionCodes
	
	)
	
	--now insert the condition information through interface procs
	
	--declare all the various items
	declare @exteriorCondition varchar(1),
		@paintCondition varchar(1),
		@trimCondition varchar(1),
		@glassCondition varchar(1),
		@interiorCondition varchar(1),
		@carpetCondition varchar(1),
		@seatsCondition varchar(1),
		@headlinerCondition varchar(1),
		@dashboardCondition varchar(1),
		@isDealerMaintained bit,
		@isFullyDetailed bit,
		@hasNoPanelScratches bit,
		@hasNoVisibleDents bit,
		@hasNoVisibleRust bit,
		@hasNoKnownAccidents bit,
		@hasNoKnownBodyWork bit,
		@hasPassedDealerInspection bit,
		@haveAllKeys bit,
		@noKnownMechanicalProblems bit,
		@hadAllRequiredScheduledMaintenancePerformed bit,
		@haveServiceRecords bit,
		@haveOriginalManuals bit
		
	SELECT 	 
		@exteriorCondition = ISNULL(substring(exteriorCondition,1,1),''),
		@paintCondition = ISNULL(substring(paintCondition,1,1),''),
		@trimCondition = ISNULL(substring(trimCondition,1,1),''),
		@glassCondition = ISNULL(substring(glassCondition,1,1),''),
		@interiorCondition = ISNULL(substring(interiorCondtiion,1,1),''),
		@carpetCondition = ISNULL(substring(carpetCondition,1,1),''),
		@seatsCondition = ISNULL(substring(seatsCondition,1,1),''),
		@headlinerCondition = ISNULL(substring(headlinerCondition,1,1),''),
		@dashboardCondition = ISNULL(substring(dashboardCondition,1,1),''),
		@isDealerMaintained = ISNULL(isDealerMaintained,0),
		@isFullyDetailed = ISNULL(isFullyDetailed,0),
		@hasNoPanelScratches = ISNULL(hasNoPanelScratches,0),
		@hasNoVisibleDents = ISNULL(hasNoVisibleDents,0),
		@hasNoVisibleRust = ISNULL(hasNoVisibleRust,0),
		@hasNoKnownAccidents = ISNULL(hasNoKnownAccidents,0),
		@hasNoKnownBodyWork = ISNULL(hasNoKnownBodyWork,0),
		@hasPassedDealerInspection = ISNULL(hasPassedDealerInspection,0),
		@haveAllKeys = ISNULL(haveAllKeys,0),
		@noKnownMechanicalProblems = ISNULL(noKnownMechanicalProblems,0),
		@hadAllRequiredScheduledMaintenancePerformed = ISNULL(hadAllRequiredScheduledMaintenancePerformed,0),
		@haveServiceRecords = ISNULL(haveServiceRecords,0),
		@haveOriginalManuals = ISNULL(haveOriginalManuals,0)
	FROM builder.optionsConfiguration oc
	LEFT JOIN builder.VehicleConditionReport vcr
		ON oc.businessUnitID = vcr.businessUnitID
		AND oc.inventoryId = vcr.inventoryID
	LEFT JOIN builder.ConditionChecklist cc
		ON oc.businessUnitId = cc.BusinessUnitId
		AND oc.inventoryId = cc.InventoryId
	WHERE oc.businessUnitId = @BusinessUnitId
		AND oc.inventoryId = @InventoryId		
		
	EXEC Interface.ConditionInformation#Release 
		@BusinessUnitId=@BusinessUnitId,
		@InventoryId=@InventoryId,
		@exteriorCondition = @exteriorCondition,
		@paintCondition = @paintCondition,
		@trimCondition = @trimCondition,
		@glassCondition = @glassCondition,
		@interiorCondition = @interiorCondition,
		@carpetCondition = @carpetCondition,
		@seatsCondition = @seatsCondition,
		@headlinerCondition = @headlinerCondition,
		@dashboardCondition = @dashboardCondition,
		@isDealerMaintained = @isDealerMaintained,
		@isFullyDetailed = @isFullyDetailed,
		@hasNoPanelScratches = @hasNoPanelScratches,
		@hasNoVisibleDents = @hasNoVisibleDents,
		@hasNoVisibleRust = @hasNoVisibleRust,
		@hasNoKnownAccidents = @hasNoKnownAccidents,
		@hasNoKnownBodyWork = @hasNoKnownBodyWork,
		@hasPassedDealerInspection = @hasPassedDealerInspection,
		@haveAllKeys = @haveAllKeys,
		@noKnownMechanicalProblems = @noKnownMechanicalProblems,
		@hadAllRequiredScheduledMaintenancePerformed = @hadAllRequiredScheduledMaintenancePerformed,
		@haveServiceRecords = @haveServiceRecords,
		@haveOriginalManuals = @haveOriginalManuals
		
		
	SELECT 1
END

GO

GRANT EXECUTE ON [Release].[AdvertisementEdge#Release] TO MerchandisingUser 
GO 
