if exists (select * from dbo.sysobjects where id = object_id(N'[Release].[DealerIdsForDistributionProviderId#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Release].[DealerIdsForDistributionProviderId#Fetch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
 * Returns dealers that use the specified provider
*/
CREATE procedure [Release].[DealerIdsForDistributionProviderId#Fetch]
	@ProviderId int
	
as

set nocount on;
set transaction isolation level read uncommitted

SELECT distinct BusinessUnitID
FROM IMT.Distribution.AccountPreferences AP
JOIN IMT.dbo.BusinessUnit BU ON BU.BusinessUnitID = AP.DealerID
WHERE ProviderID = @ProviderId

GO

GRANT EXECUTE ON [Release].[DealerIdsForDistributionProviderId#Fetch] TO MerchandisingUser 
GO