if exists (select * from dbo.sysobjects where id = object_id(N'[Release].[GetLatestBookouts]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Release].[GetLatestBookouts]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [Release].[GetLatestBookouts]
	@BookoutId int
AS
BEGIN

	SELECT DISTINCT BookoutID, BusinessUnitID, InventoryID
	FROM FLDW.dbo.InventoryBookout_F
	WHERE BookoutID > @BookoutId
	ORDER BY BookoutID DESC
	
END 
GO

GRANT EXECUTE ON [Release].[GetLatestBookouts] TO MerchandisingUser 
GO