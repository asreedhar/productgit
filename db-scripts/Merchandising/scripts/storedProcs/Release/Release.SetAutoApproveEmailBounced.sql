if exists (select * from dbo.sysobjects where id = object_id(N'[Release].[SetAutoApproveEmailBounced]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Release].[SetAutoApproveEmailBounced]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [Release].[SetAutoApproveEmailBounced]
	@BusinessUnitId int,
	@Bounced bit
AS
BEGIN

	UPDATE Merchandising.settings.AutoApprove
	SET EmailBounced = @Bounced
	WHERE BusinessUnitID = @BusinessUnitID

END 
GO

GRANT EXECUTE ON [Release].[SetAutoApproveEmailBounced] TO MerchandisingUser 
GO