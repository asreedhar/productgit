if exists (select * from dbo.sysobjects where id = object_id(N'[Release].[Advertisement#UpdateStatus]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Release].[Advertisement#UpdateStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/10/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Release.Advertisement_UpdateStatus.sql,v 1.1 2010/08/04 19:13:43 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * retrieve
 * 
 * 
 * Parameters
 * ----------
 * 
 *
 * should be ignored by the rule
 * @AdvertisementId - id of advertisement
 * @AdvertisementStatus - status of advertisement...
 *
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Release].[Advertisement#UpdateStatus]
	-- Add the parameters for the stored procedure here
	@AdvertisementId int,
	@AdvertisementStatus int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF @AdvertisementStatus = 1 --retry
	BEGIN
		UPDATE postings.vehicleAdScheduling
		SET advertisementStatus = @AdvertisementStatus, scheduledReleaseTime = getdate()
		WHERE schedulingId = @AdvertisementId
	END
	ELSE IF @AdvertisementStatus = 4 --successful release
	BEGIN
		UPDATE postings.vehicleAdScheduling
		SET advertisementStatus = @AdvertisementStatus, actualReleaseTime = getdate()
		WHERE schedulingId = @AdvertisementId
			AND advertisementStatus <> 4 --only update w/ the release time if it wasn't already released...
	END
	ELSE
	BEGIN
	--here we only update the status, as it wasn't successfully released...(not status of 4)
		UPDATE postings.vehicleAdScheduling
		SET advertisementStatus = @AdvertisementStatus
		WHERE schedulingId = @AdvertisementId		
	END
	
END 

GO

GRANT EXECUTE ON [Release].[Advertisement#UpdateStatus] TO MerchandisingUser 
GO