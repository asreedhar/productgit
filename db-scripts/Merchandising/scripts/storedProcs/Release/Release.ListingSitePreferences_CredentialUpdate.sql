if exists (select * from dbo.sysobjects where id = object_id(N'[Release].ListingSitePreferences#CredentialUpdate') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Release.ListingSitePreferences#CredentialUpdate
go

set ansi_nulls on
go
set quoted_identifier on
go

-- =============================================
-- Author:		Daniel Hillis
-- Create date: 2012-08-28
-- This proc will just update the credential columns of a DealerListingSite row.
-- It will update Username, Password, and will record a cleared loging failure as well.
-- =============================================

create procedure Release.ListingSitePreferences#CredentialUpdate
	@BusinessUnitId int,
	@DestinationId int,
	@Username varchar(50),
	@Password varchar(50)
as
begin

	set nocount on;

	if not exists
	(
		select * 
		from settings.dealerListingSites dls
		where businessUnitId = @BusinessUnitID 
		and destinationId = @DestinationId
		and exists (select * from Merchandising.settings.EdtDestinations where dls.destinationID = destinationID)
	)
		raiserror(N'The dealerlistingsite row does not exist and therefore cannot be updated.',16,1)
		
	update Merchandising.settings.DealerListingSites
	set userName = @Username
	,password = @Password
	,dateLoginFailureCleared = 
	case 
		when (DateLoginFailure is not null) 
		
			then (  -- Ensure cleared date is >= failed date
				case 
					when dateLoginFailure > getdate() then dateLoginFailure 
					else getdate() 
				end
			)
		else dateLoginfailureCleared 
		end	
	
	where businessUnitID = @BusinessUnitId
	and destinationID = @DestinationId
end 

go

grant execute on Release.ListingSitePreferences#CredentialUpdate TO MerchandisingUser 
go 



