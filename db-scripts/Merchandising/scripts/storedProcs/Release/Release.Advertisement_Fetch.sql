if exists (select * from dbo.sysobjects where id = object_id(N'[Release].[Advertisement#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Release].[Advertisement#Fetch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [Release].[Advertisement#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
    @InventoryId int
AS
BEGIN

	SELECT 
		schedulingID as advertisementId, 
		oc.inventoryId,
		ISNULL(vas.merchandisingDescription, '') as merchandisingDescription, 
		ISNULL(vas.footer, '') as footer,
		ISNULL(vas.equipmentList, '') as equipmentList,
		ISNULL(vas.listPrice, 0.0) as listPrice, 
		oc.stockNumber, 
		vas.approvedByMemberLogin,
		vas.expiresOn,
		vas.exteriorColor,
		vas.interiorColor,
		vas.interiorType,
		vas.mappedOptionIds,
		vas.optionCodes,
		vas.highlightCalloutText,
		vas.seoKeywords,
		vas.SpecialPrice,
		inv.inventoryType,
		vas.MSRP,
		vas.ManufacturerRebate,
		vas.DealerDiscount
		FROM postings.vehicleAdScheduling vas
			JOIN builder.optionsConfiguration oc ON oc.businessUnitId = vas.businessUnitId AND oc.inventoryId = vas.inventoryId
			JOIN imt.dbo.inventory inv on inv.inventoryid = vas.inventoryid
		WHERE vas.businessUnitId = @BusinessUnitId AND vas.InventoryID = @InventoryId	
	
END 
GO

GRANT EXECUTE ON [Release].[Advertisement#Fetch] TO MerchandisingUser 
GO
