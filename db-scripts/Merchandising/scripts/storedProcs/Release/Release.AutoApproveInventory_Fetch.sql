if exists (select * from dbo.sysobjects where id = object_id(N'[Release].[AutoApproveInventory#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Release].[AutoApproveInventory#Fetch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [Release].[AutoApproveInventory#Fetch]
	@BusinessUnitId int
AS
BEGIN

	SELECT DISTINCT(VS.InventoryID)
	FROM Merchandising.Postings.VehicleAdScheduling VS
	JOIN FLDW.dbo.InventoryActive I ON I.InventoryID = VS.InventoryID
	WHERE VS.BusinessUnitID = @BusinessUnitId AND VS.AdvertisementStatus = 4 AND VS.ActualReleaseTime >= DATEADD(day, -1, GETDATE()) AND VS.AutoRegenerate = 1

END 
GO

GRANT EXECUTE ON [Release].[AutoApproveInventory#Fetch] TO MerchandisingUser 
GO