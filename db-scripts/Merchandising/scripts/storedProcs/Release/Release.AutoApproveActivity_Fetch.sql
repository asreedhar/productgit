if exists (select * from dbo.sysobjects where id = object_id(N'[Release].[AutoApproveActivity#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Release].[AutoApproveActivity#Fetch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [Release].[AutoApproveActivity#Fetch]
	
AS
BEGIN

	SELECT VS.BusinessUnitID, PA.Email, PA.EmailBounced
	FROM Merchandising.Postings.VehicleAdScheduling VS
	JOIN Merchandising.Settings.AutoApprove PA ON PA.BusinessUnitID = VS.BusinessUnitID AND PA.Status != 0
	WHERE AdvertisementStatus = 4 AND ActualReleaseTime >= DATEADD(day, -1, GETDATE()) AND AutoRegenerate = 1
	GROUP BY VS.BusinessUnitID, PA.Email, PA.EmailBounced

END 
GO

GRANT EXECUTE ON [Release].[AutoApproveActivity#Fetch] TO MerchandisingUser 
GO