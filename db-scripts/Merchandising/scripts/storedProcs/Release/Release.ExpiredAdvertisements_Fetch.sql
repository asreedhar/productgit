if exists (select * from dbo.sysobjects where id = object_id(N'[Release].[ExpiredAdvertisements#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Release].[ExpiredAdvertisements#Fetch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/10/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Release.ExpiredAdvertisements_Fetch.sql,v 1.1 2010/08/04 19:13:43 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * retrieve expired or expiring ads
 * 
 * 
 * Parameters
 * ----------
 * 
 *
 * should be ignored by the rule
 * @BusinessUnitId - id of business unit
 * @DaysToExpiry - number of days prior to expiration to flag vehicle as expiring soon: default 7 days
 * 
 * History
 * ----------
 * wgh		12/16/2011	Optimizations
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Release].[ExpiredAdvertisements#Fetch]
       -- Add the parameters for the stored procedure here
       @BusinessUnitId int,
       @DaysToExpiry int = 7,
       @InventoryType int = 2
       
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

SELECT	po.merchandisingDescription,
        po.inventoryId,
        po.pricePosted
FROM  	postings.Publications po
        INNER JOIN [FLDW].dbo.InventoryActive inv ON po.businessUnitID = inv.businessunitid
                                                        AND po.inventoryid = inv.inventoryid
WHERE  inv.BusinessUnitId = @BusinessUnitId
        AND ( @InventoryType = 0
              OR inv.inventoryType = @InventoryType
            )
        AND ( NOT expiresOn IS NULL
            )
        AND expiresOn - @DaysToExpiry < GETDATE()
        AND dateRemoved IS NULL  --still active

	
END 
GO

GRANT EXECUTE ON [Release].[ExpiredAdvertisements#Fetch] TO MerchandisingUser 
GO