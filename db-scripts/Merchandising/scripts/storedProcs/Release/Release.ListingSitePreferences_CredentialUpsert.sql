if exists (select * from dbo.sysobjects where id = object_id(N'[Release].ListingSitePreferences#CredentialUpsert') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Release.ListingSitePreferences#CredentialUpsert
go

set ansi_nulls on
go
set quoted_identifier on
go


create procedure Release.ListingSitePreferences#CredentialUpsert
	@BusinessUnitId int,
	@DestinationId int,
	@Username varchar(50),
	@Password varchar(50),
	@ListingSiteDealerId varchar(30)
as
begin

	set nocount on;

	if not exists
	(
		select * 
		from settings.dealerListingSites dls
		where businessUnitId = @BusinessUnitID 
		and destinationId = @DestinationId
	)
	begin
		
		insert into Merchandising.settings.DealerListingSites (
			BusinessUnitId,
			DestinationId,
			Username,
			Password,
			ListingSiteDealerId,
			ActiveFlag,
			PushFrequency,
			SendPhotos,
			SendOptions,
			SendDescription,
			SendPricing,
			SendVideos,
			SendNewCars,
			SendUsedCars,
			ReleaseEnabled,
			CollectionEnabled
		) values (
			@BusinessUnitId,
			@DestinationId,
			@Username,
			@Password,
			@ListingSiteDealerId,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0
		)


	end
	else
	begin

		update Merchandising.settings.DealerListingSites set
			Username = @Username,
			Password = @Password,
			ListingSiteDealerId = @ListingSiteDealerId,
			dateLoginFailureCleared = 
				case 
					when (DateLoginFailure is not null) 
						then (  -- Ensure cleared date is >= failed date
							case 
								when dateLoginFailure > getdate() then dateLoginFailure 
								else getdate() 
							end
						)
					else dateLoginfailureCleared 
					end	
		where businessUnitID = @BusinessUnitId
		and destinationID = @DestinationId

	end

end 

go

grant execute on Release.ListingSitePreferences#CredentialUpsert TO MerchandisingUser 
go 



