if exists (select * from dbo.sysobjects where id = object_id(N'[Release].[MaxBookoutId]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Release].[MaxBookoutId]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [Release].[MaxBookoutId]
	
AS
BEGIN

	SELECT MAX (BookoutID) as max
	FROM FLDW.dbo.InventoryBookout_F
	
END 
GO

GRANT EXECUTE ON [Release].[MaxBookoutId] TO MerchandisingUser 
GO