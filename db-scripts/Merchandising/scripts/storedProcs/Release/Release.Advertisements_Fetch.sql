USE [Merchandising]
GO
/****** Object:  StoredProcedure [Release].[Advertisements#Fetch]    Script Date: 09/24/2013 15:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/10/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Release.Advertisements_Fetch.sql,v 1.1 2010/08/04 19:13:43 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * retrieve
 * 
 * 
 * Parameters
 * ----------
 * 
 *
 * should be ignored by the rule
 * @BusinessUnitId - id of business unit
 * @DestinationId - id of destination to poll for advertisements
 * @AdvertisementStatus - status of advertisement...
 *
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

ALTER PROCEDURE [Release].[Advertisements#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@AdvertisementStatus int,
	@InventoryType int = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	CREATE TABLE #Inventory (
		BusinessUnitId int NOT NULL,
		Vin varchar(17) NOT NULL,
		StockNumber varchar(15) NOT NULL,
		InventoryReceivedDate datetime NOT NULL, 
		vehicleYear int NOT NULL, 
		UnitCost decimal(9,2) NOT NULL, 
		AcquisitionPrice decimal (8,2) NOT NULL, 
		Certified tinyint NOT NULL, 
		Make varchar(20) NOT NULL, 
		model varchar(50) NOT NULL,
		VehicleTrim varchar(50) NULL,
		inventoryID int NOT NULL,
		MileageReceived int NULL, 
		TradeOrPurchase tinyint NOT NULL, 
		ListPrice decimal(8,2) NULL,
		InventoryType tinyint NOT NULL,
		InventoryStatusCD int NOT NULL,
		BaseColor varchar(100) NOT NULL,
		VehicleLocation varchar(20) NULL,
		LotPrice DECIMAL(8,2) NULL,
		MSRP DECIMAL(9,2) NULL,
		VehicleCatalogId int NULL, PRIMARY KEY (businessUnitId, inventoryId)
	)

	INSERT INTO #Inventory EXEC Interface.InventoryList#Fetch @BusinessUnitId=@BusinessUnitID, @UsedOrNew=@InventoryType

	SELECT
		schedulingID as advertisementId, 
		oc.inventoryId,
		ISNULL(vas.merchandisingDescription, '') as merchandisingDescription, 
		ISNULL(vas.footer, '') as footer,
		ISNULL(vas.equipmentList, '') as equipmentList,
		ISNULL(vas.listPrice, 0.0) as listPrice, 
		oc.stockNumber, 
		ISNULL(inv.vin, '') as vin,
		vas.approvedByMemberLogin,
		vas.expiresOn,
		vas.exteriorColor,
		vas.interiorColor,
		vas.interiorType,
		vas.mappedOptionIds,
		vas.optionCodes,
		vas.highlightCalloutText,
		vas.seoKeywords,
		vas.SpecialPrice,
        inv.inventoryType
        ,vas.MSRP
        ,vas.DealerDiscount
        ,vas.ManufacturerRebate
        
        FROM postings.vehicleAdScheduling vas
			INNER JOIN builder.optionsConfiguration oc
				ON oc.businessUnitId = vas.businessUnitId
					AND oc.inventoryId = vas.inventoryId
			INNER JOIN workflow.Inventory inv 
				ON inv.businessUnitId = oc.businessUnitId
					AND inv.inventoryId = oc.inventoryId
		WHERE vas.businessUnitId = @BusinessUnitId
			AND (advertisementStatus = @AdvertisementStatus
				--or clause catches errors in status flag...
					OR (@AdvertisementStatus = 2
							AND vas.scheduledReleaseTime < getDate()
							AND vas.advertisementStatus NOT IN (0,3,4) --not in error...--not in progress either...--not online
							
						)
				)	
		
		ORDER BY vas.scheduledReleaseTime DESC
	
	
	DROP TABLE #Inventory
	
END 

