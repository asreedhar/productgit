if exists (select * from dbo.sysobjects where id = object_id(N'[Release].[ListingSitePreferences#ClearLoginFailure]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Release].[ListingSitePreferences#ClearLoginFailure]
GO

create procedure [Release].[ListingSitePreferences#ClearLoginFailure] (
	@businessUnitID int,
	@destinationID int
-- DESCRIPTION: 
-- Sets the settings.DealerListingSites.dateLoginFailureCleared field for a specific business unit and destination.
)
as
begin
	set nocount on
	
	declare @currentTime as datetime
	set @currentTime = getdate()
	
	update dls
		set dateLoginFailureCleared = 
			-- ensure that dateLoginFailureCleared ends up >= dateLoginFailure
			case when dls.dateLoginFailure is null or dls.dateLoginFailure < @currentTime 
			then @currentTime 
			else dls.dateLoginFailure 
			end
		from settings.dealerListingSites dls
		where dls.businessUnitID = @businessUnitID and dls.destinationID = @destinationID

end

GO

grant execute on [Release].[ListingSitePreferences#ClearLoginFailure] to MerchandisingUser

GO
