if exists (select * from dbo.sysobjects where id = object_id(N'[Release].[DealerApprovedInventory#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Release].[DealerApprovedInventory#Fetch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [Release].[DealerApprovedInventory#Fetch]
	-- Add the parameters for the stored procedure here
	@AdvertisementStatus int,
	@BusinessUnitID int
AS
BEGIN

	SELECT VAS.InventoryID, VAS.ApprovedByMemberLogin
    FROM (
		SELECT VAS.BusinessUnitID, VAS.InventoryID, VAS.ApprovedByMemberLogin
			FROM Merchandising.postings.VehicleAdScheduling VAS
			WHERE (advertisementStatus = @AdvertisementStatus
				--or clause catches errors in status flag...
				OR (@AdvertisementStatus = 2
						AND vas.scheduledReleaseTime < getDate()
						AND vas.advertisementStatus IN (1,2)							
						--either future or pending...
						--not in progress either
						--not in error...
					)
				)
			GROUP BY VAS.BusinessUnitID, VAS.InventoryID, VAS.ApprovedByMemberLogin) VAS 
	JOIN FLDW.dbo.InventoryActive I ON I.InventoryID = VAS.InventoryID AND I.Businessunitid = @BusinessUnitID
    AND I.InventoryType = 2
	Group BY VAS.InventoryID, VAS.ApprovedByMemberLogin

END 

GO

GRANT EXECUTE ON [Release].[DealerApprovedInventory#Fetch] TO MerchandisingUser 
GO
