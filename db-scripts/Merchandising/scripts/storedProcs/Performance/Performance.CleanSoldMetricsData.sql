if exists (select * from dbo.sysobjects where id = object_id(N'[Performance].[CleanSoldMetricsData]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Performance].[CleanSoldMetricsData]
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Performance.CleanSoldMetricsData.sql,v 1.1 2010/08/04 18:49:56 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * resolve the inventory id for the sold vehicle metrics we collected
 * compute any daily stats for non-daily raw data
 * 
 * 
 * Parameters
 * ----------
 * @BusinessUnitId int,
 
 *  
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Performance].[CleanSoldMetricsData]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@DataCollectionDate datetime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


CREATE TABLE #Inventory (
		BusinessUnitId int NOT NULL,
		Vin varchar(17) NOT NULL,
		StockNumber varchar(15) NOT NULL,
		InventoryReceivedDate datetime NOT NULL, 
		vehicleYear int NOT NULL, 
		UnitCost decimal(9,2) NOT NULL, 
		AcquisitionPrice decimal (8,2) NOT NULL, 
		Certified tinyint NOT NULL, 
		Make varchar(20) NOT NULL, 
		model varchar(50) NOT NULL,
		VehicleTrim varchar(50) NULL,
		segmentId int NOT NULL,
		segment varchar(50) NOT NULL,
		inventoryID int NOT NULL,
		MileageReceived int NULL, 
		TradeOrPurchase tinyint NOT NULL, 
		ListPrice decimal(8,2) NULL,
		InventoryType tinyint NOT NULL,
		InventoryStatusCD int NOT NULL,
		BaseColor varchar(50) NOT NULL,
		VehicleLocation varchar(20) NULL
)


--default to one year ago
	declare @EarliestReceivedDate datetime
	if @DataCollectionDate IS NULL
		select @EarliestReceivedDate = DATEADD(day,-365,getdate())
	ELSE
		select @EarliestReceivedDate = DATEADD(day,-365,@DataCollectionDate)
	
	INSERT INTO #Inventory EXEC Interface.HistoricalInventoryList#Fetch @BusinessUnitID=@BusinessUnitID, @EarliestReceivedDate=@EarliestReceivedDate


UPDATE svm
SET svm.inventoryId = inv.inventoryId
FROM postings.SoldVehicleMetrics svm
INNER JOIN #Inventory inv
	ON inv.businessUnitId = svm.businessUnitId
	AND inv.stockNumber = svm.stockNumber
	



DROP TABLE #Inventory

END

GO

GRANT EXECUTE ON [Performance].[CleanSoldMetricsData] TO MerchandisingUser 
GO