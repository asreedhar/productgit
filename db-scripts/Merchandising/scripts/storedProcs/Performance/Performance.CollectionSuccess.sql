if exists (select * from dbo.sysobjects where id = object_id(N'[Performance].[CollectionSuccess#Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Performance].[CollectionSuccess#Save]
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Performance.CollectionSuccess.sql,v 1.1 2010/08/04 18:49:56 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * flag the success of a data collection for a dealer and listing site
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId int   - integer id of the businesUnit for which to get the alert list
 * @SourceId int - 0 unknown, 1 cars.com, 2 autotrader, 3 dealer website
 * @ActiveOrSold - 1 for active inventory, 2 for sold inventory
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Performance].[CollectionSuccess#Save]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@SourceID int,
	@ActiveOrSold int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @ActiveOrSold = 1
	BEGIN
		UPDATE settings.dealerListingSites
		SET lastActiveInventoryCollection = getdate()
		WHERE destinationId = @SourceID 
		AND businessUnitId = @BusinessUnitID
	END
	ELSE
	BEGIN
		UPDATE settings.dealerListingSites
		SET lastSoldInventoryCollection = getdate()
		WHERE destinationId = @SourceID 
		AND businessUnitId = @BusinessUnitID
	END
END

GO

GRANT EXECUTE ON [Performance].[CollectionSuccess#Save] TO MerchandisingUser 
GO