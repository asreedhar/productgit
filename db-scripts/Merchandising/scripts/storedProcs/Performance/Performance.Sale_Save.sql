if exists (select * from dbo.sysobjects where id = object_id(N'[Performance].[Sale#Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Performance].[Sale#Save]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/10/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Performance.Sale_Save.sql,v 1.1 2010/08/04 18:49:56 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * retrieve
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Performance].[Sale#Save]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@SourceId int,
	@StockNumber varchar(17),
	@RemovedDate datetime, 
    @LastLeadDate datetime, 
    @LastLeadType varchar(30), 
    @AdsPrinted int, 
    @DaysLive int, 
    @SearchViewed int, 
    @DetailsViewed int, 
    @Emails int, 
    @MapsViewed int, 
    @WebsiteClickthrus int, 
    @ModelYear int, 
    @Make varchar(30), 
    @Model varchar(30), 
    @UsedOrNew int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF NOT EXISTS (SELECT stockNumber 
	FROM postings.soldVehicleMetrics
	WHERE stockNumber = @StockNumber
		AND removedDate = @RemovedDate 
		AND businessUnitId = @BusinessUnitId)
	BEGIN
    INSERT INTO postings.soldVehicleMetrics (
                    sourceId, stockNumber, removedDate,
                    lastLeadDate, lastLeadType, adsPrinted, daysLive, 
                    searchViewed, detailsViewed, emails, mapsViewed, 
                    websiteClickthrus, modelYear, make, model, usedOrNew, businessUnitID)
                VALUES (
                     @SourceId, @StockNumber,@RemovedDate, 
                    @LastLeadDate, @LastLeadType, @AdsPrinted, @DaysLive, 
                    @SearchViewed, @DetailsViewed, @Emails, @MapsViewed, 
                    @WebsiteClickthrus, @ModelYear, @Make, @Model, @UsedOrNew, @BusinessUnitID)
    SELECT scope_identity() as [identity_value]
	END
	ELSE
	SELECT 0
END 

GO

GRANT EXECUTE ON [Performance].[Sale#Save] TO MerchandisingUser 
GO