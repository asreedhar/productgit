if exists (select * from dbo.sysobjects where id = object_id(N'[Performance].[Dealers#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Performance].[Dealers#Fetch]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/10/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Performance.Dealers_Fetch.sql,v 1.1 2010/08/04 18:49:56 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * retrieve
 * 
 * 
 * Parameters
 * ----------
 * @ActiveOrSales - get delears with sources for active data or sales data
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Performance].[Dealers#Fetch]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	CREATE TABLE #Dealers (
		BusinessUnitID int NOT NULL,
		BusinessUnit varchar(40) NOT NULL,
		BusinessUnitTypeID int NOT NULL
	)
	
	INSERT INTO #Dealers EXEC Interface.Dealers#Fetch
	
	--contains all destinations for the dealers located in dealers
	declare @performanceSources table (
		dealerId int,
		sourceId int,
		sourceName varchar(50),
		lastActiveInventoryCollection datetime NULL,
		lastSoldInventoryCollection datetime NULL
	)
	
	--select all where the dealer has an information source configured
	
			
	INSERT INTO @performanceSources (dealerId, sourceId, sourceName, lastActiveInventoryCollection, lastSoldInventoryCollection)
		SELECT DISTINCT ddd.businessUnitId, ddd.destinationId, ed.description, ddd.lastActiveInventoryCollection, ddd.lastSoldInventoryCollection
			FROM settings.DealerListingSites ddd
			INNER JOIN settings.EdtDestinations ed 
				on ed.destinationId = ddd.destinationId
			WHERE ddd.collectionEnabled = 1
			
			 	
	SELECT distinct bu.businessUnit as dealerName, 
		bu.businessUnitID as dealerId 
		FROM #Dealers bu
		INNER JOIN settings.DealerListingSites ddd
			ON bu.businessUnitId = ddd.businessUnitId
		WHERE ddd.collectionEnabled = 1
	
	--fetch all	
	SELECT * FROM @performanceSources
	--WHERE lastActiveInventoryCollection IS NULL
	--OR datediff(hh,lastActiveInventoryCollection,getDate()) > 22  --at least 22 hours ago
	
	SELECT * FROM @performanceSources
	WHERE lastSoldInventoryCollection IS NULL
	OR (datediff(day,lastSoldInventoryCollection,getDate()) > 5  --at least 5 days ago
		AND datepart(day,getDate()) >= 4  --must be at least 4th of month
		)
				
			
	
	DROP TABLE #Dealers
END 

GO

GRANT EXECUTE ON [Performance].[Dealers#Fetch] TO MerchandisingUser 
GO