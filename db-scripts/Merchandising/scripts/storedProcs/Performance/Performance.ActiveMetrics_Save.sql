if exists (select * from dbo.sysobjects where id = object_id(N'[Performance].[ActiveMetrics#Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Performance].[ActiveMetrics#Save]
GO


/****** Object:  StoredProcedure [Performance].[ActiveMetrics#Save]    Script Date: 10/27/2008 17:16:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Performance.ActiveMetrics_Save.sql,v 1.1 2010/08/04 18:49:56 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * creates a new vehicle metric entry - if already exists, return -1
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId int   - integer id of the businesUnit for which to get the alert list
 * @SourceId int - 0 unknown, 1 cars.com, 2 autotrader, 3 dealer website
 *	@Vin varchar(17), -vehicle VIN
 *	@StockNumber varchar(17), - vehicle stocknumber
 *	@StartDate datetime, -first date for collection period
 *	@EndDate datetime, - last date for collection period
 *	@AdsPrinted int, - number of ads printed during collection period
 *	@CurrentPrice int,- vehicle price at time of collection
 *  @MktAvgPrice int, - average market price at time of collection
 *	@DaysLive int, -number days vehicle has been live at time of collection
 *	@SearchViewed int, - number of search views during collection period
 *  @DetailsViewed int,- number of detail views during collection period 
 *	@Emails int, - number of emails generated during collection period
 *	@MapsViewed int, - number of map views during collection period
 *   @WebsiteClickthrus int - number of websited clickthrus during collection period
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Performance].[ActiveMetrics#Save]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@SourceID int,
	@Vin varchar(17), 
	@StockNumber varchar(17),
	@StartDate datetime, 
	@EndDate datetime,
	@AdsPrinted int, 
	@CurrentPrice int,
    @MktAvgPrice int, 
	@DaysLive int, 
	@SearchViewed int,
    @DetailsViewed int, 
	@Emails int, 
	@MapsViewed int, 
    @WebsiteClickthrus int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--if we have this vehicle in our inventory, set its flag to online!
	IF EXISTS ( SELECT 1 from builder.OptionsConfiguration
				WHERE businessUnitID = @BusinessUnitID
					AND stockNumber = @StockNumber)
	BEGIN
		
		UPDATE builder.OptionsConfiguration
			SET onlineFlag = 1
			WHERE businessUnitID = @BusinessUnitID
					AND stockNumber = @StockNumber
	END
	
	
	IF EXISTS ( SELECT 1 from postings.rawVehicleMetrics 
			where vin = @Vin
				AND stockNumber = @StockNumber
				AND startDate = @StartDate
				AND endDate = @EndDate
				AND sourceId = @SourceId
				)
		BEGIN
			UPDATE postings.rawVehicleMetrics
			SET adsPrinted = @AdsPrinted,
				searchViewed = @SearchViewed,
				detailsViewed = @DetailsViewed, 
				emails = @Emails, 
				mapsViewed = @MapsViewed, 
				websiteClickthrus = @WebsiteClickthrus
			WHERE
				vin = @Vin
				AND stockNumber = @StockNumber
				AND startDate = @StartDate
				AND endDate = @EndDate
				AND sourceId = @SourceId
			
			--select out that id
			SELECT metricId 
			FROM postings.rawVehicleMetrics
			WHERE
				vin = @Vin
				AND stockNumber = @StockNumber
				AND startDate = @StartDate
				AND endDate = @EndDate
				AND sourceId = @SourceId
		END
	ELSE
		BEGIN
			INSERT INTO postings.rawVehicleMetrics (sourceId, vin, stockNumber, startDate,
				endDate, adsPrinted, currentPrice,
				mktAvgPrice, daysLive, searchViewed,
				detailsViewed, emails, mapsViewed, 
				websiteClickthrus, businessUnitID)
			VALUES (
				@SourceId, @Vin, @StockNumber,@StartDate, @EndDate, @AdsPrinted, @CurrentPrice,
				@MktAvgPrice, @DaysLive, @SearchViewed,
				@DetailsViewed, @Emails, @MapsViewed, 
				@WebsiteClickthrus, @BusinessUnitID)
				SELECT scope_identity() as [identity_value]
		END	
END

GO

GRANT EXECUTE ON [Performance].[ActiveMetrics#Save] TO MerchandisingUser 
GO