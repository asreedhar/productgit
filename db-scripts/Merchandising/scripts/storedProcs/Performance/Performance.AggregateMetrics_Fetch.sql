if exists (select * from dbo.sysobjects where id = object_id(N'[Performance].[AggregateMetrics#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Performance].[AggregateMetrics#Fetch]
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Performance.AggregateMetrics_Fetch.sql,v 1.1 2010/08/04 18:49:56 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * fetch data for a specified time period
 * if an inventoryId is supplied, get for that vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId int   - integer id of the businesUnit for which to get the alert list
 * @StartDate datetime - first day to include
 * @EndDate datetime - last day to include
 * @inventoryId in NULL - vehicle to fetch
 *  
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Performance].[AggregateMetrics#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@StartDate datetime,
	@EndDate datetime,
	@InventoryId int = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT businessunitid, 
		stockNumber, 
		inventoryId,
		vin,
		count(*) as daysIncluded,
		sum(adsPrinted) as adsPrinted, 
		avg(currentPrice) as currentPrice,
		avg(mktAvgPrice) as mktAvgPrice, 
		max(daysLive) as daysLive,
		sum(searchViewed) as searchViewed, 
		sum(detailsViewed) as detailsViewed,
		sum(emails) as emails, 
		sum(mapsViewed) as mapsViewed, 
		sum(websiteClickthrus) as websiteClickthrus 
	FROM postings.vehicleMetrics 
		WHERE businessUnitID = @BusinessUnitID
		AND startDate BETWEEN @StartDate AND @EndDate
		AND (@InventoryId IS NULL OR inventoryId = @InventoryId)
		GROUP BY businessUnitId, vin, stockNumber, inventoryId

END

GO

GRANT EXECUTE ON [Performance].[AggregateMetrics#Fetch] TO MerchandisingUser 
GO