if exists (select * from dbo.sysobjects where id = object_id(N'[Performance].[PhoneLead#Save]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Performance].[PhoneLead#Save]
GO


/****** Object:  StoredProcedure [Performance].[PhoneLead#Save]    Script Date: 10/27/2008 17:16:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <6/22/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Performance.PhoneLead_Save.sql,v 1.1 2010/08/04 18:49:56 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * creates a new vehicle phone lead
 * 
 * 
 * Parameters
 * ----------
 * 
	@BusinessUnitID int,
	@SourceID int,
	@LeadDate datetime, 
	@CallerPhone varchar(20),
	@CallerZip int, 
	@CallStatus varchar(50),
    @CallDurationMinutes int, 
	@CallDurationSeconds int * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Performance].[PhoneLead#Save]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@SourceID int,
	@LeadDate datetime, 
	@CallerPhone varchar(20),
	@CallerZip int, 
	@CallStatus varchar(50),
    @CallDurationMinutes int, 
	@CallDurationSeconds int
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--if we have this vehicle in our inventory, set its flag to online!
	IF EXISTS ( SELECT 1 from postings.phoneLeads
			where businessUnitId = @BusinessUnitId
				AND sourceId = @SourceId
				AND leadDate = @LeadDate
				AND CallerPhone = @CallerPhone
				AND CallerZip = @CallerZip
				AND CallStatus = @CallStatus
				)
		BEGIN
			UPDATE postings.phoneLeads
			SET CallDurationMinutes = @CallDurationMinutes,
				CallDurationSeconds = @CallDurationSeconds
			WHERE
				businessUnitId = @BusinessUnitId
				AND sourceId = @SourceId
				AND leadDate = @LeadDate
				AND CallerPhone = @CallerPhone
				AND CallerZip = @CallerZip
				AND CallStatus = @CallStatus
			
		
		END
	ELSE
		BEGIN
			INSERT INTO postings.phoneLeads (businessUnitId, sourceId, 
				leadDate, CallerPhone, CallerZip, CallStatus, 
				CallDurationMinutes, CallDurationSeconds)
			VALUES (
				@BusinessUnitId, @SourceId, 
				@LeadDate, @CallerPhone, @CallerZip, @CallStatus, 
				@CallDurationMinutes, @CallDurationSeconds)				
		END	
END

GO

GRANT EXECUTE ON [Performance].[PhoneLead#Save] TO MerchandisingUser 
GO