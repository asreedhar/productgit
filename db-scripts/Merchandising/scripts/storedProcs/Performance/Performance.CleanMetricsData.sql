if exists (select * from dbo.sysobjects where id = object_id(N'[Performance].[CleanMetricsData]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Performance].[CleanMetricsData]
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Performance.CleanMetricsData.sql,v 1.1 2010/08/04 18:49:56 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * resolve the inventory id for the metrics we collected
 * compute any daily stats for non-daily raw data
 * 
 * 
 * Parameters
 * ----------
 * @BusinessUnitId int,
 * @DataCollectionDate datetime - date for which the data shoudl be cleansed and loaded from raw table
 *  
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Performance].[CleanMetricsData]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@DataCollectionDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


CREATE TABLE #Inventory (
		BusinessUnitId int NOT NULL,
		Vin varchar(17) NOT NULL,
		StockNumber varchar(15) NOT NULL,
		InventoryReceivedDate datetime NOT NULL, 
		vehicleYear int NOT NULL, 
		UnitCost decimal(9,2) NOT NULL, 
		AcquisitionPrice decimal (8,2) NOT NULL, 
		Certified tinyint NOT NULL, 
		Make varchar(20) NOT NULL, 
		model varchar(50) NOT NULL,
		VehicleTrim varchar(50) NULL,
		segmentId int NOT NULL,
		segment varchar(50) NOT NULL,
		inventoryID int NOT NULL,
		MileageReceived int NULL, 
		TradeOrPurchase tinyint NOT NULL, 
		ListPrice decimal(8,2) NULL,
		InventoryType tinyint NOT NULL,
		InventoryStatusCD int NOT NULL,
		BaseColor varchar(50) NOT NULL,
		VehicleLocation varchar(20) NULL
)


--default to one year ago
	declare @EarliestReceivedDate datetime
	select @EarliestReceivedDate = DATEADD(day,-365,getdate())
	
	INSERT INTO #Inventory EXEC Interface.HistoricalInventoryList#Fetch @BusinessUnitID=@BusinessUnitID, @EarliestReceivedDate=@EarliestReceivedDate


create table #Metrics (
	sourceId int,
	businessUnitId int,
	stockNumber varchar(15),
	vin varchar(17),
	currentPrice int,
	mktAvgPrice int,
	daysLive int,
	startDate datetime, 
	detailsViewed int,
	searchViewed int,
	adsPrinted int,
	mapsViewed int,
	emails int,
	websiteClickthrus int,
	inventoryId int NULL
)


--get metrics where the data is for one day (the target collection day)
insert into #Metrics (
	sourceId,
	businessUnitId,
	stockNumber,
	vin,
	currentPrice,
	mktAvgPrice,
	daysLive,
	startDate, 
	detailsViewed,
	searchViewed,
	adsPrinted,
	mapsViewed,
	emails,
	websiteClickthrus,
	inventoryId
)
select
	sourceId,
	businessUnitId,
	stockNumber,
	vin,
	currentPrice,
	mktAvgPrice,
	daysLive,
	startDate, 
	detailsViewed,
	searchViewed,
	adsPrinted,
	mapsViewed,
	emails,
	websiteClickthrus,
	NULL as inventoryId
FROM postings.RawVehicleMetrics rvm
WHERE rvm.startDate = rvm.endDate
and rvm.startDate = @DataCollectionDate
and rvm.businessUnitId = @BusinessUnitId

--get metrics for days where we need to compute based on month to date data
insert into #Metrics (
	sourceId,
	businessUnitId,
	stockNumber,
	vin,
	currentPrice,
	mktAvgPrice,
	daysLive,
	startDate, 
	detailsViewed,
	searchViewed,
	adsPrinted,
	mapsViewed,
	emails,
	websiteClickthrus,
	inventoryId
)
select 
a.sourceId,
a.businessUnitId,
a.stockNumber,
a.vin,
a.currentPrice,
a.mktAvgPrice,
a.daysLive,
a.endDate as startDate, 
a.detailsViewed-b.detailsViewed as detailsViewed,
a.searchViewed-b.searchViewed as searchViewed,
a.adsPrinted-b.adsPrinted as adsPrinted,
a.mapsViewed-b.mapsViewed as mapsViewed,
a.emails-b.emails as emails,
a.websiteClickthrus-b.websiteClickthrus as websiteClickthrus,
NULL as inventoryId

FROM postings.rawVehicleMetrics a
INNER JOIN postings.rawVehicleMetrics b
	ON a.businessUnitId = b.businessUnitId				--join on bus unit, vin, then dates
	AND a.vin = b.vin									--vin match
	AND b.startDate = a.startDate						--start date is same
	AND b.endDate = dateadd(day,-1,a.endDate)			--end date needs to be one off
	AND a.sourceId = b.sourceId							--must be from same data source (autotrader, cars, etc)
WHERE a.startDate <> a.endDate						-- must be a raw data sample over more than one day to bother making it daily!
	AND a.businessUnitId = @BusinessUnitId
	AND a.endDate = @DataCollectionDate





UPDATE vm
SET vm.inventoryId = inv.inventoryId
FROM #Metrics vm
INNER JOIN #Inventory inv
	ON inv.businessUnitId = vm.businessUnitId
	AND inv.vin = vm.vin
	
INSERT INTO postings.VehicleMetrics
	(sourceId,
	businessUnitId,
	vin,
	stockNumber,
	startDate, 
	adsPrinted,
	currentPrice,
	mktAvgPrice,
	daysLive,
	searchViewed,
	detailsViewed,
	emails,
	mapsViewed,
	websiteClickthrus,
	inventoryId)
SELECT 
	m.sourceId,
	m.businessUnitId,
	m.vin,
	m.stockNumber,
	m.startDate, 
	m.adsPrinted,
	m.currentPrice,
	m.mktAvgPrice,
	m.daysLive,
	m.searchViewed,
	m.detailsViewed,
	m.emails,
	m.mapsViewed,
	m.websiteClickthrus,
	m.inventoryId
FROM #Metrics m
LEFT JOIN postings.vehicleMetrics vm
ON vm.vin = m.vin
AND vm.startDate = m.startDate
AND vm.sourceId = m.sourceId
AND vm.businessUnitId = m.businessUnitId
where vm.vin IS NULL --only insert where we don't already have a row

DROP TABLE #Metrics
DROP TABLE #Inventory

END

GO

GRANT EXECUTE ON [Performance].[CleanMetricsData] TO MerchandisingUser 
GO