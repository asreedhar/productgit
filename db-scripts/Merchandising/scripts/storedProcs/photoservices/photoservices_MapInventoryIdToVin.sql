if object_id('photoservices.MapInventoryIdToVin') is not null
	drop procedure photoservices.MapInventoryIdToVin
GO

create procedure photoservices.MapInventoryIdToVin
    @inventoryID int
as
begin
    set nocount on
    select vehicle.Vin
        from IMT.dbo.Inventory as inventory
            inner join IMT.dbo.tbl_Vehicle as vehicle
            on inventory.VehicleID = vehicle.VehicleID
        where inventory.InventoryID = @inventoryID
end
GO

grant execute on photoservices.MapInventoryIdToVin to MerchandisingUser 
GO