if object_id('photoservices.MapBusinessUnitIdToBusinessUnitCode') is not null
	drop procedure photoservices.MapBusinessUnitIdToBusinessUnitCode
GO

create procedure photoservices.MapBusinessUnitIdToBusinessUnitCode
    @businessUnitId int
as
begin
    set nocount on
    select BusinessUnitCode 
        from IMT.dbo.BusinessUnit 
        where BusinessUnitID = @businessUnitID
end
GO

grant execute on photoservices.MapBusinessUnitIdToBusinessUnitCode to MerchandisingUser 
GO