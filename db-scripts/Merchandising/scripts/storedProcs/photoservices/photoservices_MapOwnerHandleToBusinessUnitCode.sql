if object_id('photoservices.MapOwnerHandleToBusinessUnitCode') is not null
	drop procedure photoservices.MapOwnerHandleToBusinessUnitCode
GO

create procedure photoservices.MapOwnerHandleToBusinessUnitCode
    @ownerHandle uniqueidentifier
as
begin
    set nocount on
    select BU.BusinessUnitCode 
        from Market.Pricing.[Owner] as OW 
            inner join IMT.dbo.BusinessUnit as BU 
            on OW.OwnerEntityID = BU.BusinessUnitID
        where OW.Handle = @ownerHandle
end
GO

grant execute on photoservices.MapOwnerHandleToBusinessUnitCode to MerchandisingUser 
GO