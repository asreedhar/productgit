if exists (select * from dbo.sysobjects where id = object_id(N'[Listing].[AllListingOptions#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Listing].[AllListingOptions#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <12/08/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Listing.AllListingOptions_Fetch.sql,v 1.2 2009/12/14 15:54:30 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch all the listing options from the lot data
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Listing].[AllListingOptions#Fetch]
	-- Add the parameters for the stored procedure here		
	@UsedOrNew int = 2	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
		SELECT 
			ol.[OptionID]
			,ol.[Description]
			,max(veh.ProviderId) as ProviderId
			,count(*) as optionCount
		FROM Lot.[Listing].[OptionsList] ol
		INNER JOIN Lot.listing.vehicleOption vo
			ON vo.optionId = ol.optionId
		JOIN Lot.listing.vehicle veh
			ON veh.vehicleId = vo.vehicleId
		GROUP BY ol.optionId, ol.description
		ORDER BY COUNT(*) DESC		

END

GO

GRANT EXECUTE ON [Listing].[AllListingOptions#Fetch] TO MerchandisingUser 
GO