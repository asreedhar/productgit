USE [Merchandising]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Boucher
-- Create date: 6/10/2011
-- Description: Snippets.Snippet#Delete
-- Remove snippet from database
-- =============================================
IF object_id('Snippets.Snippet#Delete') IS NOT NULL
	DROP PROCEDURE [Snippets].[Snippet#Delete]
GO
CREATE PROCEDURE [Snippets].[Snippet#Delete] 

	-- Parameters
	@SnippetId int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @errorNumber int
	DECLARE @errorSeverity int
	DECLARE @errorState int
	DECLARE @errorMessage nvarchar(126)	
	DECLARE @errorProc nvarchar(126)
	DECLARE @errorLine int

	BEGIN TRY

		BEGIN TRAN

		DELETE FROM Snippets.Marketing_Tag
		WHERE MarketingId = @SnippetId;

		DELETE FROM Snippets.Marketing_Vehicle
		WHERE MarketingId = @SnippetId;

		--DELETE FROM Snippets.Marketing_Parent
		--WHERE MarketingId = @SnippetId;

		DELETE FROM Snippets.Marketing
		WHERE Id = @SnippetId;

		COMMIT

	END TRY

	BEGIN CATCH
    
		SELECT 
			@errorNumber = ERROR_NUMBER(),
			@errorSeverity= ERROR_SEVERITY(),
			@errorState = ERROR_STATE(),
			@errorProc = ERROR_PROCEDURE(),
			@errorLine = ERROR_LINE(),
			@errorMessage = ERROR_MESSAGE()

		IF @@TRANCOUNT > 0
			ROLLBACK TRAN;

		RAISERROR (N'Insert failed, error number: %d, stored proc: %s, line number: %d, error message: %s', -- Message text.
		   @errorSeverity, -- Severity,
		   @errorState, -- State,
		   @errorNumber,
		   @errorProc,
		   @errorLine,
		   @errorMessage)

	END CATCH

END
GO
GRANT EXECUTE ON [Snippets].[Snippet#Delete] TO MerchandisingUser;
GO