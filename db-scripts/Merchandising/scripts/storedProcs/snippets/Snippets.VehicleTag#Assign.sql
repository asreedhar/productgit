IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[VehicleTag#Assign]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Snippets].[VehicleTag#Assign]
GO

CREATE PROCEDURE [Snippets].[VehicleTag#Assign] (
	@marketingId int,
	@tagId int
)
AS

	INSERT INTO Snippets.Marketing_Tag
	(marketingId,tagId) VALUES (@marketingId, @tagId)	

GO

GRANT EXECUTE ON [Snippets].[VehicleTag#Assign] TO MerchandisingUser
GO
