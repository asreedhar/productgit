SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Boucher
-- Create date: 5/21/2011
-- Description: Snippets.Tag#Delete
-- Deletes a snippet tag by ID
-- =============================================
IF object_id('Snippets.Tag#Delete') IS NOT NULL
	DROP PROCEDURE [Snippets].[Tag#Delete]
GO
CREATE PROCEDURE [Snippets].[Tag#Delete] 
	-- Add the parameters for the stored procedure here
	@TagId int
AS
BEGIN
	-- SET NOCOUNT OFF to retrieve rows affected
	SET NOCOUNT OFF;

	declare @errorNumber int
	declare @errorSeverity int
	declare @errorState int
	declare @errorMessage nvarchar(126)	
	declare @errorProc nvarchar(126)
	declare @errorLine int

	BEGIN TRY

		declare @mappedRowCount int

		-- Copy mappings to 'delete' table
		INSERT INTO Snippets.Marketing_Tag_Delete (TagId, MarketingId, Description)
			SELECT a.tagId, a.marketingId, b.description
			FROM Snippets.Marketing_Tag a LEFT JOIN Snippets.Tag b ON a.tagId = b.id
			WHERE tagId = @TagId;

		-- Remove mappings
		DELETE FROM Snippets.Marketing_Tag WHERE tagId = @TagId;

		set @mappedRowCount = @@ROWCOUNT
		
		-- Remove tag
		DELETE FROM Snippets.Tag WHERE id = @TagId;

		SELECT @mappedRowCount;

	END TRY

	BEGIN CATCH

	SELECT 
        @errorNumber = ERROR_NUMBER(),
        @errorSeverity= ERROR_SEVERITY(),
        @errorState = ERROR_STATE(),
        @errorProc = ERROR_PROCEDURE(),
        @errorLine = ERROR_LINE(),
        @errorMessage = ERROR_MESSAGE()

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		RAISERROR (N'Delete failed, error number: %d, stored proc: %s, line number: %d, error message: %s', -- Message text.
           @errorSeverity, -- Severity,
           @errorState, -- State,
           @errorNumber,
		   @errorProc,
		   @errorLine,
           @errorMessage)

	END CATCH

END
GO
GRANT EXECUTE ON [Snippets].[Tag#Delete] TO MerchandisingUser
GO