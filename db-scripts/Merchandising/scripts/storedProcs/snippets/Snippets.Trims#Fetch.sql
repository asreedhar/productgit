USE [Merchandising]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Kevin Boucher
-- Create date:		5/25/2011
-- Procedure:		Snippets.Trims#Fetch
--
-- Gets list of vehicle trims based on model Ids
-- =============================================
IF object_id('Snippets.Trims#Fetch') IS NOT NULL
	DROP PROCEDURE [Snippets].[Trims#Fetch]
GO
CREATE PROCEDURE [Snippets].[Trims#Fetch]
	
	-- Parameters
	@ModelIds varchar(max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT styleID, styleName FROM VehicleCatalog.Chrome.Styles styles JOIN VehicleCatalog.Chrome.Models models
	ON models.modelid = styles.modelid
	WHERE models.modelid in (SELECT Data FROM dbo.split(@ModelIds, ','))
	ORDER BY styleName ASC

--	SELECT DISTINCT makes.DivisionId, makes.ManufacturerId, makes.DivisionName
--	FROM VehicleCatalog.Chrome.Divisions makes JOIN VehicleCatalog.Chrome.Models models ON models.DivisionId = makes.DivisionId
--	WHERE models.ModelID in (SELECT Data FROM dbo.split(@ModelIds, ','))
--	ORDER BY DivisionName ASC


END
GO
GRANT EXECUTE ON [Snippets].[Trims#Fetch] TO MerchandisingUser;
GO