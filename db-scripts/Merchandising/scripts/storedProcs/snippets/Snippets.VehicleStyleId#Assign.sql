IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[VehicleStyleId#Assign]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Snippets].[VehicleStyleId#Assign]
GO

CREATE PROCEDURE [Snippets].[VehicleStyleId#Assign] (
	@marketingId int,
	@styleId int
)
AS

	INSERT INTO Snippets.Marketing_Vehicle
	(marketingId,chromeStyleId) VALUES (@marketingId, @styleId)	

GO

GRANT EXECUTE ON [Snippets].[VehicleStyleId#Assign] TO MerchandisingUser
GO
