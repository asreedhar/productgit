USE [Merchandising]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Boucher
-- Create date: 5/25/2011
-- Description: Snippets.Makes#Fetch
-- Get list of vehicle makes for snippet assignemnt
-- =============================================
IF object_id('Snippets.Makes#Fetch') IS NOT NULL
	DROP PROCEDURE [Snippets].[Makes#Fetch]
GO
CREATE PROCEDURE [Snippets].[Makes#Fetch] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT DivisionId, ManufacturerId, DivisionName FROM VehicleCatalog.Chrome.Divisions
	WHERE countrycode = 1
	ORDER BY divisionName ASC

END
GO
GRANT EXECUTE ON [Snippets].[Makes#Fetch] TO MerchandisingUser;
GO