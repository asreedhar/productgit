IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[Marketing#Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Snippets].[Marketing#Update]
GO

CREATE PROCEDURE [Snippets].[Marketing#Update] (
	@marketingId int,
	@marketingText varchar(500),
	@marketingType int,
	@sourceId int,
	@isVerbatim bit	
	
)
AS

	UPDATE Snippets.Marketing
	SET
		marketingText = @marketingText,
		marketingType = @marketingType,
		sourceId = @sourceId,
		isVerbatim = @isVerbatim
	WHERE id = @marketingId	

GO

GRANT EXECUTE ON [Snippets].[Marketing#Update] TO MerchandisingUser
GO

