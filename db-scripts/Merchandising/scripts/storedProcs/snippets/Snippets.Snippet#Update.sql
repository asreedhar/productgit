SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Boucher
-- Create date: 6/7/2011
-- Description: Snippets.Snippet#Update
-- Update snippet
-- =============================================
IF object_id('Snippets.Snippet#Update') IS NOT NULL
	DROP PROCEDURE [Snippets].[Snippet#Update]
GO
CREATE PROCEDURE [Snippets].[Snippet#Update] 

	-- Parameters
	@Id int,
	@MarketingText varchar(500),
	@MarketingType int,
	@SourceId int,
	@IsVerbatim bit,
	@Tags varchar(max),
	@Styles varchar(max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @errorNumber int
	DECLARE @errorSeverity int
	DECLARE @errorState int
	DECLARE @errorMessage nvarchar(126)	
	DECLARE @errorProc nvarchar(126)
	DECLARE @errorLine int

	BEGIN TRY

		BEGIN TRAN

		-- Insert statements for procedure here
		UPDATE Snippets.Marketing
		SET
			marketingText = @MarketingText,
			sourceId = @SourceId,
			isVerbatim = @IsVerbatim
		WHERE id = @Id;

		-- Update tag mappings
		DELETE FROM Snippets.Marketing_Tag WHERE marketingId = @Id;
		IF @Tags <> 'null' -- 'null' is passed if no tags are selected
			INSERT INTO Snippets.Marketing_Tag (marketingId, tagId)
				SELECT @Id, Data FROM dbo.split(@Tags, ',')
				
		-- Update chrome style mappings
		DELETE FROM Snippets.Marketing_Vehicle WHERE marketingId = @Id;
		INSERT INTO Snippets.Marketing_Vehicle (marketingId, chromeStyleId)
			SELECT @Id, Data FROM dbo.split(@Styles, ',')

		SELECT @Id

		COMMIT

	END TRY

	BEGIN CATCH
    
		SELECT 
			@errorNumber = ERROR_NUMBER(),
			@errorSeverity= ERROR_SEVERITY(),
			@errorState = ERROR_STATE(),
			@errorProc = ERROR_PROCEDURE(),
			@errorLine = ERROR_LINE(),
			@errorMessage = ERROR_MESSAGE()

		IF @@TRANCOUNT > 0
			ROLLBACK TRAN;

		RAISERROR (N'Update failed, error number: %d, stored proc: %s, line number: %d, error message: %s', -- Message text.
		   @errorSeverity, -- Severity,
		   @errorState, -- State,
		   @errorNumber,
		   @errorProc,
		   @errorLine,
		   @errorMessage)

	END CATCH

END
GO
GRANT EXECUTE ON [Snippets].[Snippet#Update] TO MerchandisingUser;
GO