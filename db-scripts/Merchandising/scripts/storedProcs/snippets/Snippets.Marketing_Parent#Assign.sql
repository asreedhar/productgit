IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[Marketing_Parent#Assign]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Snippets].[Marketing_Parent#Assign]
GO

CREATE PROCEDURE [Snippets].[Marketing_Parent#Assign]
	
	@MarketingId int,
	@ParentId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	if exists (select marketingId from snippets.Marketing_Parent WHERE marketingId = @MarketingId)
	BEGIN
		UPDATE Snippets.Marketing_Parent SET parentId = @ParentId where marketingId = @MarketingId
	END
	else
	BEGIN
		INSERT INTO Snippets.Marketing_Parent (marketingId, parentId) VALUES (@MarketingId, @ParentId)
	END

END
GO

GRANT EXECUTE ON [Snippets].[Marketing_Parent#Assign] TO MerchandisingUser 
GO