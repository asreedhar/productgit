IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[Marketing#Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Snippets].[Marketing#Delete]
GO

CREATE Procedure [Snippets].[Marketing#Delete]
(
	@MarketingId int
)


AS

	EXEC Snippets.MarketingConnections#Delete @MarketingId

	DELETE FROM Snippets.Marketing
	WHERE Id = @MarketingId
GO

GRANT EXECUTE ON [Snippets].[Marketing#Delete] TO MerchandisingUser 
GO
