IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[Marketing_UserRating#Assign]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Snippets].[Marketing_UserRating#Assign]
GO

CREATE PROCEDURE [Snippets].[Marketing_UserRating#Assign] (
	@marketingId int,
	@ratingScore int,
	@businessUnitId bit	
)
AS

	INSERT INTO Snippets.UserRating
	(marketingId,ratingScore,businessUnitID) 
	VALUES (@marketingId, @ratingScore,@businessUnitId)	

GO

GRANT EXECUTE ON [Snippets].[Marketing_UserRating#Assign] TO MerchandisingUser
GO
