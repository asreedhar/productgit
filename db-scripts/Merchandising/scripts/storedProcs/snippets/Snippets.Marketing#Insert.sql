IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[Marketing#Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Snippets].[Marketing#Insert]
GO

CREATE PROCEDURE Snippets.Marketing#Insert (
	@marketingText varchar(500),
	@marketingType int,
	@sourceId int,
	@isVerbatim bit	
	
)
AS

	INSERT INTO Snippets.Marketing
	(marketingText,marketingType,sourceId,isVerbatim) 
	VALUES (@marketingText, @marketingType,@sourceId,@isVerbatim)	
	
	SELECT cast(scope_identity() as int)

GO

GRANT EXECUTE ON [Snippets].[Marketing#Insert] TO MerchandisingUser
GO
