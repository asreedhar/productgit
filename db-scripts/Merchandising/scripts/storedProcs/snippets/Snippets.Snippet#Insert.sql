USE [Merchandising]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Boucher
-- Create date: 6/1/2011
-- Description: Snippets.Snippet#Insert
-- Add/update snippet to/in database
-- =============================================
IF object_id('Snippets.Snippet#Insert') IS NOT NULL
	DROP PROCEDURE [Snippets].[Snippet#Insert]
GO
CREATE PROCEDURE [Snippets].[Snippet#Insert] 

	-- Parameters
	@MarketingText varchar(500),
	@MarketingType int,
	@SourceId int,
	@IsVerbatim bit,
	@Tags varchar(max),
	@Styles varchar(max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @NewSnippetId int

	DECLARE @errorNumber int
	DECLARE @errorSeverity int
	DECLARE @errorState int
	DECLARE @errorMessage nvarchar(126)	
	DECLARE @errorProc nvarchar(126)
	DECLARE @errorLine int

	BEGIN TRY

		BEGIN TRAN

		-- Insert statements for procedure here
		INSERT INTO Snippets.Marketing (marketingText, marketingType, sourceId, isVerbatim)
		VALUES (@MarketingText, @MarketingType, @SourceId, @IsVerbatim)

		SET @NewSnippetId = SCOPE_IDENTITY();

		IF @Tags != 'null'
		BEGIN
			INSERT INTO Snippets.Marketing_Tag (marketingId, tagId)
				SELECT @NewSnippetId, Data FROM dbo.split(@Tags, ',')
		END

		INSERT INTO Snippets.Marketing_Vehicle (marketingId, chromeStyleId)
			SELECT @NewSnippetId, Data FROM dbo.split(@Styles, ',')

		SELECT @NewSnippetId

		COMMIT

	END TRY

	BEGIN CATCH
    
		SELECT 
			@errorNumber = ERROR_NUMBER(),
			@errorSeverity= ERROR_SEVERITY(),
			@errorState = ERROR_STATE(),
			@errorProc = ERROR_PROCEDURE(),
			@errorLine = ERROR_LINE(),
			@errorMessage = ERROR_MESSAGE()

		IF @@TRANCOUNT > 0
			ROLLBACK TRAN;

		RAISERROR (N'Insert failed, error number: %d, stored proc: %s, line number: %d, error message: %s', -- Message text.
		   @errorSeverity, -- Severity,
		   @errorState, -- State,
		   @errorNumber,
		   @errorProc,
		   @errorLine,
		   @errorMessage)

	END CATCH

END
GO
GRANT EXECUTE ON [Snippets].[Snippet#Insert] TO MerchandisingUser;
GO