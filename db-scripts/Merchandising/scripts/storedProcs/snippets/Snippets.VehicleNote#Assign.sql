IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[VehicleNote#Assign]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Snippets].[VehicleNote#Assign]
GO

CREATE PROCEDURE [Snippets].[VehicleNote#Assign] (
	@marketingId int,
	@notesText varchar(500)
)
AS

	INSERT INTO Snippets.MarketingNote
	(marketingId,notesText) VALUES (@marketingId, @notesText)	

GO

GRANT EXECUTE ON [Snippets].[VehicleNote#Assign] TO MerchandisingUser
GO
