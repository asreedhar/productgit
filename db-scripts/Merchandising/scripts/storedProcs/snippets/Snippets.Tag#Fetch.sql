SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Boucher
-- Create date: 5/20/2011
-- Description: Snippets.Tag#Fetch
-- Returns all snippet tags or a single snippet
-- tag if supplied with optional @TagId parameter
-- =============================================
IF object_id('Snippets.Tag#Fetch') IS NOT NULL
	DROP PROCEDURE [Snippets].[Tag#Fetch]
GO
CREATE PROCEDURE [Snippets].[Tag#Fetch] 
	-- Add the parameters for the stored procedure here
	@TagId int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT tags.id, tags.description
	FROM Snippets.Tag tags
    WHERE (id = @TagId or @TagId is null)
	ORDER BY tags.description ASC
END
GO
GRANT EXECUTE ON [Snippets].[Tag#Fetch] TO MerchandisingUser
GO