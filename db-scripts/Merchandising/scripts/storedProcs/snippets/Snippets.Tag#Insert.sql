USE [Merchandising]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Boucher
-- Create date: 5/20/2011
-- Description: Snippets.Tag#Insert
-- Adds a new tag to Snippets.Tag table
-- =============================================
IF object_id('Snippets.Tag#Insert') IS NOT NULL
	DROP PROCEDURE [Snippets].[Tag#Insert]
GO
CREATE PROCEDURE [Snippets].[Tag#Insert] 
	-- Add the parameters for the stored procedure here
	@Tag varchar(30),
	@NewId int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Snippets.Tag (description) VALUES ( @Tag );
	
	set @NewId = SCOPE_IDENTITY()

	SELECT tag.id, tag.description FROM Snippets.Tag tag WHERE (id = @NewId);

END
GO
GRANT EXECUTE ON [Snippets].[Tag#Insert] TO MerchandisingUser
GO