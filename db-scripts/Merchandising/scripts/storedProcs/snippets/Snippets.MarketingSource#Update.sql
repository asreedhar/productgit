SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Boucher
-- Create date: 5/20/2011
-- Description: Snippets.MarketingSource#Fetch
-- Returns all snippet sources or a single
-- snippet source if supplied with optional
-- @SourceId parameter
-- =============================================
IF object_id('Snippets.MarketingSource#Update') IS NOT NULL
	DROP PROCEDURE [Snippets].[MarketingSource#Update]
GO
CREATE PROCEDURE [Snippets].[MarketingSource#Update]
	-- Add the parameters for the stored procedure here
	@Id int,
	@SourceName varchar(50),
	@HasCopyright bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE Snippets.MarketingSource
	SET
		sourceName = @SourceName,
		hasCopyright = @HasCopyright
	WHERE
		id = @Id

	SELECT @Id

END
GO
GRANT EXECUTE ON [Snippets].[MarketingSource#Update] TO MerchandisingUser
GO