if exists (select * from dbo.sysobjects where id = object_id(N'[Snippets].[Marketing#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [Snippets].[Marketing#Fetch]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <9/24/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Snippets.Marketing#Fetch.sql,v 1.2 2010/05/06 17:59:37 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * Get the marketing information available on a vehicle and the tags for each marketing snippet
 * 
 * 
 * Parameters
 * ----------
 * 
 * @ChromeStyleId int - the chromestyleid of the vehicle 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Snippets].[Marketing#Fetch]
	
	@ChromeStyleId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT mkt.id as marketingId, 
		marketingText, 
		isVerbatim,
		ms.id as sourceId,
		ms.sourceName,
		ms.hasCopyright
	from snippets.marketing mkt
	join snippets.marketing_vehicle mv
	on mkt.id = mv.marketingId
	join snippets.marketingSource ms
	on ms.id = mkt.sourceId

	where chromeStyleId = @ChromeStyleId


	select mv.marketingId, 
		tg.description, 
		tg.id as tagId
	from snippets.tag tg
	join snippets.marketing_tag mt
	on mt.tagId = tg.id
	join snippets.marketing_vehicle mv
	on mv.marketingId = mt.marketingId
	where chromeStyleId = @ChromeStyleId

	

END
GO

GRANT EXECUTE ON [Snippets].[Marketing#Fetch] TO MerchandisingUser 
GO
