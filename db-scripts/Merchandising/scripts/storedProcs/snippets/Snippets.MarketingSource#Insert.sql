SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Boucher
-- Create date: 5/20/2011
-- Description: Snippets.MarketingSource#Insert
-- Adds a new tag to Snippets.MarketingSource table
-- =============================================
IF object_id('Snippets.MarketingSource#Insert') IS NOT NULL
	DROP PROCEDURE [Snippets].[MarketingSource#Insert]
GO
CREATE PROCEDURE [Snippets].[MarketingSource#Insert] 
	-- Add the parameters for the stored procedure here
	@SourceName varchar(50), 
	@HasCopyright bit = false
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO Snippets.MarketingSource (sourceName, hasCopyright) VALUES (@SourceName, @HasCopyright);
	SELECT id, sourceName, hasCopyright FROM Snippets.MarketingSource WHERE (id = SCOPE_IDENTITY())
END
GO
GRANT EXECUTE ON [Snippets].[MarketingSource#Insert] TO MerchandisingUser
GO