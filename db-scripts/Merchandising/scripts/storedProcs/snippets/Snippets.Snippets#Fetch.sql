USE [Merchandising]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Kevin Boucher
-- Create date:		5/27/2011
-- Description:		Snippets.Snippet#Fetch
-- Get snippets based on snippet ID, chrome Id
-- or keywords
-- =============================================
IF object_id('Snippets.Snippets#Fetch') IS NOT NULL
      DROP PROCEDURE [Snippets].[Snippets#Fetch]
GO
CREATE PROCEDURE [Snippets].[Snippets#Fetch] 
      
      @MarketingId int = null

AS
BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;

	  -- Insert statements for procedure here
      SELECT
            marketing.id AS marketingId,
            marketing.marketingText,
            marketing.sourceId,
			marketing.isVerbatim,
            source.sourceName,
            [Merchandising].[Snippets].[GetSnippetTags] (marketing.id) AS tags,
			[Merchandising].[Snippets].[GetSnippetStyles] (marketing.id) AS styles
      FROM Snippets.Marketing marketing
            --LEFT JOIN Snippets.Marketing_Parent parent ON marketing.id = parent.marketingId
            LEFT JOIN Snippets.MarketingSource source ON marketing.sourceId = source.id
      WHERE
            @MarketingId IS NULL OR @MarketingId = marketing.id
	  ORDER BY marketingId DESC
            

END
GO
GRANT EXECUTE ON [Snippets].[Snippets#Fetch] TO MerchandisingUser
GO
