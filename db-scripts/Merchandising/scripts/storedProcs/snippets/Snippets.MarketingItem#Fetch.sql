IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[MarketingItem#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Snippets].[MarketingItem#Fetch]
GO

CREATE PROCEDURE [Snippets].[MarketingItem#Fetch]
	
	@MarketingId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT mkt.id as marketingId, 
		marketingText, 
		isVerbatim,
		ms.id as sourceId,
		ms.sourceName,
		ms.hasCopyright
	from Snippets.marketing mkt
	join Snippets.marketingSource ms
	on ms.id = mkt.sourceId
	where mkt.id = @MarketingId


--select tags
	select 
		tg.id as tagId
	from Snippets.tag tg
	join Snippets.marketing_tag mt
	on mt.tagId = tg.id
	where mt.marketingId = @MarketingId

--get year make model and styles
select mdl.modelId, div.divisionId, mv.marketingId, mv.chromeStyleId
	from Snippets.marketing_vehicle mv
	join vehiclecatalog.chrome.styles sty 
	on mv.chromeStyleId = sty.styleID
	join vehicleCatalog.chrome.models mdl
	ON mdl.modelId = sty.modelId
	join vehiclecatalog.chrome.divisions div
	on div.divisionId = mdl.divisionId
	where mv.marketingId = @MarketingId

select ratingScore
from Snippets.userRating
where businessUnitId = 100150
and marketingId = @MarketingId

--get parent if one exists
select parentId
FROM Snippets.Marketing_Parent
where marketingId = @MarketingId
	

END
GO

GRANT EXECUTE ON [Snippets].[MarketingItem#Fetch] TO MerchandisingUser 
GO
