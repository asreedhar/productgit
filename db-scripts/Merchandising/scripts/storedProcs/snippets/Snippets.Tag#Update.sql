SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Boucher
-- Create date: 5/20/2011
-- Description: Snippets.MarketingSource#Fetch
-- Returns all snippet sources or a single
-- snippet source if supplied with optional
-- @SourceId parameter
-- =============================================
IF object_id('Snippets.Tag#Update') IS NOT NULL
	DROP PROCEDURE [Snippets].[Tag#Update]
GO
CREATE PROCEDURE [Snippets].[Tag#Update]
	-- Add the parameters for the stored procedure here
	@Id int,
	@Description varchar(30)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE Snippets.Tag
	SET
		[Snippets].[Tag].[description] = @Description
	WHERE
		id = @Id

	SELECT @Id

END
GO
GRANT EXECUTE ON [Snippets].[Tag#Update] TO MerchandisingUser
GO