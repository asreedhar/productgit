USE [Merchandising]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Boucher
-- Create date: 5/25/2011
-- Description: Snippets.Models#Fetch
-- Get list of vehicle models for snippet assignemnt
-- =============================================
IF object_id('Snippets.Models#Fetch') IS NOT NULL
	DROP PROCEDURE [Snippets].[Models#Fetch]
GO
CREATE PROCEDURE [Snippets].[Models#Fetch] 

	-- Parameters
	@DivisionId int = null

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT cast(modelYear as varchar) + ' ' + modelName as modelNameAndYear, modelId, divisions.divisionId, modelYear, modelName
	FROM VehicleCatalog.Chrome.Models models JOIN VehicleCatalog.Chrome.Divisions divisions ON divisions.divisionid = models.divisionid
	WHERE (divisions.countrycode = 1 AND models.countrycode = 1) AND (@DivisionId IS NULL OR models.divisionid = @DivisionId)
	ORDER BY modelName ASC, modelYear ASC

END
GO
GRANT EXECUTE ON [Snippets].[Models#Fetch] TO MerchandisingUser;
GO