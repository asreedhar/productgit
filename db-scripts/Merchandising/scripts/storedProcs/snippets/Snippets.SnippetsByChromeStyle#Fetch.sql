USE [Merchandising]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:			Kevin Boucher
-- Create date:		6/14/2011
-- Description:		Snippets.SnippetsByChromeStyle#Fetch
-- Get snippets based on chromeStyleId
-- =============================================
IF object_id('Snippets.SnippetsByChromeStyle#Fetch') IS NOT NULL
      DROP PROCEDURE [Snippets].[SnippetsByChromeStyle#Fetch]
GO
CREATE PROCEDURE [Snippets].[SnippetsByChromeStyle#Fetch] 
      
      @ChromeStyleId varchar(max)

AS
BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;

	  -- Insert statements for procedure here
      SELECT DISTINCT
            marketing.id AS marketingId,
            marketing.marketingText,
            marketing.sourceId,
			marketing.isVerbatim,
            source.sourceName
      FROM Snippets.Marketing marketing
			JOIN Snippets.Marketing_Vehicle vehicle ON marketing.id = vehicle.marketingId
            --LEFT JOIN Snippets.Marketing_Parent parent ON marketing.id = parent.marketingId
            LEFT JOIN Snippets.MarketingSource source ON marketing.sourceId = source.id
      WHERE
            vehicle.chromeStyleId IN (SELECT Data FROM dbo.split(@ChromeStyleId, ','))
	  ORDER BY marketingId DESC
            

END
GO
GRANT EXECUTE ON [Snippets].[SnippetsByChromeStyle#Fetch] TO MerchandisingUser
GO
