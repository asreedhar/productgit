SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Boucher
-- Create date: 5/20/2011
-- Description: Snippets.MarketingSource#Fetch
-- Returns all snippet sources or a single
-- snippet source if supplied with optional
-- @SourceId parameter
-- =============================================
IF object_id('Snippets.MarketingSource#Fetch') IS NOT NULL
	DROP PROCEDURE [Snippets].[MarketingSource#Fetch]
GO
CREATE PROCEDURE [Snippets].[MarketingSource#Fetch]
	-- Add the parameters for the stored procedure here
	@SourceId int = null
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT id, sourceName, hasCopyright
	FROM Snippets.MarketingSource
	WHERE (id = @SourceId or @SourceId is null)
	ORDER BY sourceName ASC
END
GO
GRANT EXECUTE ON [Snippets].[MarketingSource#Fetch] TO MerchandisingUser
GO