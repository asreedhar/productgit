SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Boucher
-- Create date: 5/23/2011
-- Description: Snippets.Tag#UndoDelete
-- Undo deleted snippet tag
-- =============================================
IF object_id('Snippets.Tag#UndoDelete') IS NOT NULL
	DROP PROCEDURE [Snippets].[Tag#UndoDelete]
GO
CREATE PROCEDURE [Snippets].[Tag#UndoDelete] 
	-- Add the parameters for the stored procedure here
	@TagId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @errorNumber int
	DECLARE @errorSeverity int
	DECLARE @errorState int
	DECLARE @errorMessage nvarchar(126)	
	DECLARE @errorProc nvarchar(126)
	DECLARE @errorLine int

	BEGIN TRY

		BEGIN TRAN

		-- Create 'new' tag (and get new ID)
		DECLARE @Tag varchar(30)
		DECLARE @NewId int

		SET @Tag = (SELECT [description] FROM Snippets.Marketing_Tag_Delete WHERE TagId = @TagId)
		EXEC Snippets.Tag#Insert @Tag, @NewId OUTPUT


		-- Copy mappings back to mapping table
		INSERT INTO Snippets.Marketing_Tag (marketingId, tagId)
			SELECT MarketingId, @NewId
			FROM Snippets.Marketing_Tag_Delete
			WHERE TagId = @TagId;

		-- Remove records from Marketing_Tag_Delete
		DELETE FROM Snippets.Marketing_Tag_Delete WHERE tagId = @TagId

		-- Return text and new id for tag
		SELECT id, [description] FROM Snippets.Tag WHERE id = @NewId

		COMMIT

	END TRY

	BEGIN CATCH
    
		SELECT 
			@errorNumber = ERROR_NUMBER(),
			@errorSeverity= ERROR_SEVERITY(),
			@errorState = ERROR_STATE(),
			@errorProc = ERROR_PROCEDURE(),
			@errorLine = ERROR_LINE(),
			@errorMessage = ERROR_MESSAGE()

		IF @@TRANCOUNT > 0
			ROLLBACK TRAN;

		RAISERROR (N'Undo failed, error number: %d, stored proc: %s, line number: %d, error message: %s', -- Message text.
		   @errorSeverity, -- Severity,
		   @errorState, -- State,
		   @errorNumber,
		   @errorProc,
		   @errorLine,
		   @errorMessage)

	END CATCH

END
GO
GRANT EXECUTE ON [Snippets].[Tag#UndoDelete] TO MerchandisingUser;
GO