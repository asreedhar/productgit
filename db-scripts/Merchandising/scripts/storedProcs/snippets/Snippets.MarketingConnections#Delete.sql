IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[MarketingConnections#Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Snippets].[MarketingConnections#Delete]
GO

CREATE Procedure [Snippets].[MarketingConnections#Delete]
(
	@MarketingId int
)
AS
	DELETE FROM Snippets.Marketing_Category
	WHERE MarketingId = @MarketingId

--delete, but don't bother w/ chained deletes so we keep children
	DELETE FROM Snippets.Marketing_Parent
	WHERE MarketingId = @MarketingId  OR parentId = @MarketingId
	
	DELETE FROM Snippets.Marketing_Tag
	WHERE MarketingId = @MarketingId
	
	DELETE FROM Snippets.Marketing_Vehicle
	WHERE MarketingId = @MarketingId

	DELETE FROM Snippets.MarketingNote
	WHERE MarketingId = @MarketingId

	DELETE FROM Snippets.UserRating
	WHERE MarketingId = @MarketingId
GO

GRANT EXECUTE ON [Snippets].[MarketingConnections#Delete] TO MerchandisingUser 
GO


