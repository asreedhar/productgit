USE [Merchandising]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Boucher
-- Create date: 5/31/2011
-- Description: Snippets.MakesByModels#Fetch
-- Get list of vehicle models from ChromeStyleIds
-- =============================================
IF object_id('Snippets.MakesByModels#Fetch') IS NOT NULL
	DROP PROCEDURE [Snippets].[MakesByModels#Fetch]
GO
CREATE PROCEDURE [Snippets].[MakesByModels#Fetch] 

	-- Parameters
	@ModelIds varchar(max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT makes.DivisionId, makes.ManufacturerId, makes.DivisionName
	FROM VehicleCatalog.Chrome.Divisions makes JOIN VehicleCatalog.Chrome.Models models ON models.DivisionId = makes.DivisionId
	WHERE models.ModelID in (SELECT Data FROM dbo.split(@ModelIds, ','))
	ORDER BY DivisionName ASC

END
GO
GRANT EXECUTE ON [Snippets].[MakesByModels#Fetch] TO MerchandisingUser;
GO
