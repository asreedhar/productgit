IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[VehicleCategory#Assign]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Snippets].[VehicleCategory#Assign]
GO

CREATE PROCEDURE [Snippets].[VehicleCategory#Assign] (
	@marketingId int,
	@categoryId int
)
AS

	INSERT INTO Snippets.Marketing_Category
	(marketingId,categoryId) VALUES (@marketingId, @categoryId)	

GO

GRANT EXECUTE ON [Snippets].[VehicleCategory#Assign] TO MerchandisingUser
GO
