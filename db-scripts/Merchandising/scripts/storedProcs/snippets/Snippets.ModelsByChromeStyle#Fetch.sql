USE [Merchandising]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kevin Boucher
-- Create date: 5/31/2011
-- Description: Snippets.ModelsByChromeStyle#Fetch
-- Get list of vehicle models from ChromeStyleIds
-- =============================================
IF object_id('Snippets.ModelsByChromeStyle#Fetch') IS NOT NULL
	DROP PROCEDURE [Snippets].[ModelsByChromeStyle#Fetch]
GO
CREATE PROCEDURE [Snippets].[ModelsByChromeStyle#Fetch] 

	-- Parameters
	@ChromeStyleIds varchar(max)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT cast(models.modelYear as varchar) + ' ' + modelName as modelNameAndYear, models.modelId, models.modelYear, models.modelName
	FROM VehicleCatalog.Chrome.Models models JOIN VehicleCatalog.Chrome.Styles styles ON styles.ModelID = models.ModelID
	WHERE styles.StyleID in (SELECT Data FROM dbo.split(@ChromeStyleIds, ','))
	ORDER BY modelName ASC, modelYear ASC

END
GO
GRANT EXECUTE ON [Snippets].[ModelsByChromeStyle#Fetch] TO MerchandisingUser;
GO