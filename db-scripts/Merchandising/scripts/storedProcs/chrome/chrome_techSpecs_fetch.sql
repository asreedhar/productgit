if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[techSpecs#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[techSpecs#Fetch]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_techSpecs_fetch.sql,v 1.1 2009/01/05 21:23:51 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets all tech specs for a chrome style id
 * 
 * 
 * Parameters
 * ----------
 * @ChromeStyleId
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Chrome].[techSpecs#Fetch]
	-- Add the parameters for the stored procedure here
	@ChromeStyleId int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT th.techTitleHeaderText, tt.Title, tt.titleId, ts.Sequence, ts.Text, ts.Condition
FROM vehicleCatalog.chrome.techSpecs ts
INNER JOIN vehiclecatalog.chrome.techTitles tt
ON tt.titleId = ts.titleId
INNER JOIN vehiclecatalog.chrome.techtitleheader th
ON th.techTitleHeaderId = tt.techTitleHeaderId
WHERE ts.styleId = @ChromeStyleId
AND ts.countryCode = 1 
AND tt.countryCode = 1
AND th.countryCode = 1
ORDER BY ts.sequence ASC

END
GO

GRANT EXECUTE ON [Chrome].[techSpecs#Fetch] TO MerchandisingUser 
GO