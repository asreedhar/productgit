if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getAllCategories]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getAllCategories]
GO


/****** Object:  StoredProcedure [Chrome].[getAllCategories]    Script Date: 10/30/2008 13:02:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_getAllCategories.sql,v 1.3 2008/11/20 20:01:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets all categories from chrome
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Chrome].[getAllCategories]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		

	SELECT ct.CategoryID, ct.CategoryTypeFilter, 
		ct.UserFriendlyName, ch.CategoryHeaderID, ch.CategoryHeader, ct.CategoryTypeFilter, 'C' as state
	FROM VehicleCatalog.Chrome.Categories AS ct 
		INNER JOIN VehicleCatalog.Chrome.CategoryHeaders AS ch 
		ON ch.CategoryHeaderID = ct.CategoryHeaderID 
	WHERE (ch.CountryCode = 1) AND (ct.CountryCode = 1) 
	ORDER BY ch.Sequence

END
GO

GRANT EXECUTE ON [Chrome].[getAllCategories] TO MerchandisingUser 
GO