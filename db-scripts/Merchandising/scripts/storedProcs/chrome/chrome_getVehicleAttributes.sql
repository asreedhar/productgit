if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getVehicleAttributes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getVehicleAttributes]
GO

/****** Object:  StoredProcedure [Chrome].[getVehicleAttributes]    Script Date: 10/29/2008 11:14:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_getVehicleAttributes.sql,v 1.5 2009/11/13 15:54:38 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the attributes for the vinpattern and styleid
 * 
 * 
 * Parameters
 * ----------
 * 
 * @ChromeStyleID - chromestyleid to use for vehicle attributes
 * @VinPatternID - vin pattern ID from chrome to use
 * 
 * History
 * ----------
 * WGH		02/13/2012	Optimization and formatting
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Chrome].[getVehicleAttributes]
	-- Add the parameters for the stored procedure here
	@ChromeStyleID int,
	@VinPatternID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @vehInfo TABLE (	modelID INT,
					bodyType VARCHAR(255)
   					)

	INSERT 
	INTO	@vehInfo (modelID, bodyType)
	
	SELECT	modelID, CFBodyType 
	FROM	VehicleCatalog.Chrome.Styles 
	WHERE	StyleID = @ChromeStyleID AND countrycode = 1

	SELECT  vp.year,
		vindivisionname,
		vinmodelname,
		cfbodytype,
		
		CASE WHEN COUNT(DISTINCT EngineTypeCategoryID) > 1 THEN 1 ELSE 0 END AS multiEngineFlag,
		CASE WHEN COUNT(DISTINCT engineSize) > 1 THEN 1 ELSE 0 END AS multiEngineSizeFlag,
		CASE WHEN COUNT(DISTINCT AutoTrans + ManualTrans) > 1 THEN 1 ELSE 0 END AS multiTransFlag,
		CASE WHEN COUNT(DISTINCT fuelTypeCategoryID) > 1 THEN 1 ELSE 0 END AS multiFuelFlag,
		CASE WHEN COUNT(DISTINCT forcedInductionCategoryID) > 1 THEN 1 ELSE 0 END AS multiForcedInducsFlag,
		CASE WHEN COUNT(DISTINCT frontWD + rearWD + AllWD + FourWD) > 1 THEN 1 ELSE 0 END AS multiDrivetrainFlag,
		CASE WHEN COUNT(DISTINCT passengerDoors) > 1 THEN 1 ELSE 0 END AS multiDoorsFlag,
		CASE WHEN COUNT(DISTINCT passengerCapacity) > 1 THEN 1 ELSE 0 END AS multiCapacityFlag
		
	FROM    @vehInfo vi
		INNER JOIN VehicleCatalog.chrome.styles sty ON vi.modelid = sty.modelID
							   AND vi.bodyType = sty.CFBodyType
		INNER JOIN VehicleCatalog.chrome.vinpatternstylemapping sm ON sm.chromestyleid = sty.styleid
		INNER JOIN VehicleCatalog.chrome.vinpattern vp ON vp.vinpatternid = sm.vinpatternid
	WHERE   vp.countrycode = 1
		AND sm.countrycode = 1
		AND sty.countrycode = 1
	GROUP 
	BY	vp.year,
		vindivisionname,
		vinmodelname,
		cfbodytype; 

	SELECT	sty.autotrans, sty.manualtrans, 
		sty.MSRP, 
		sty.PassengerDoors, 
		sty.PassengerCapacity, 
		sty.CFDrivetrain, 
		CASE WHEN autoTrans = 'S' THEN 'Auto' ELSE 'Manual' END AS VehicleTransmission, 
		e.UserFriendlyName AS VehicleEngine,  
		f.UserFriendlyName AS FuelType
		
	FROM   	VehicleCatalog.chrome.styles sty
		INNER JOIN VehicleCatalog.chrome.vinpatternstylemapping sm ON sm.chromestyleid = sty.styleid
		INNER JOIN VehicleCatalog.chrome.vinpattern vp ON vp.vinpatternid = sm.vinpatternid
		LEFT JOIN VehicleCatalog.chrome.categories e ON e.categoryid = vp.engineTypeCategoryID
	                	                                AND e.countrycode = 1
		LEFT JOIN VehicleCatalog.chrome.categories f ON f.categoryid = vp.fuelTypeCategoryID
	        	                                        AND f.countrycode = 1
	WHERE  styleID = @ChromeStyleID
	        AND vp.vinpatternid = @VinPatternID
		AND sty.countrycode = 1
		AND vp.countrycode = 1   
		AND sm.CountryCode = 1         

END
GO

GRANT EXECUTE ON [Chrome].[getVehicleAttributes] TO MerchandisingUser 
GO
