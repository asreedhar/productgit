if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getDifferentiatedCategories]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getDifferentiatedCategories]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_getDifferentiatedCategories.sql,v 1.4 2008/12/09 18:24:16 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the categories from chrome that are distinct for a vin pattern
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Chrome].[getDifferentiatedCategories]
	-- Add the parameters for the stored procedure here
	@VinPatternId int
	
AS

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		declare @styleIDs table (
		id int identity(1,1),
		styleID int,
		CFStyleName varchar(500)
	)

	INSERT INTO @styleIDs (styleID, CFStyleName)
		SELECT chromeStyleID, Trim + ' - ' + StyleNameWOTrim
		FROM VehicleCatalog.chrome.vinPatternStyleMapping sm
			INNER JOIN vehicleCatalog.chrome.styles sty
				ON sm.chromestyleid = sty.styleid
		WHERE vinPatternID = @VinPatternId 
			AND sm.countrycode = 1 
			AND sty.countrycode = 1
	
	--get number of styles
	declare @styleCt int
	select @styleCt = @@rowcount

	CREATE TABLE #Categories (
		categoryId int,
		userFriendlyName varchar(255),
		styleId int
	)
	
	INSERT INTO #Categories (categoryId, userFriendlyName, styleId)
	SELECT distinct s.categoryID, c.UserFriendlyName, s.styleId
        FROM VehicleCatalog.chrome.styleCats s
        INNER JOIN VehicleCatalog.chrome.categories c
        ON c.categoryid = s.categoryid
        
        WHERE s.styleID IN (SELECT styleID FROM @styleIDs)
        AND featureType = 'S'
        AND s.countrycode = 1
        AND c.countrycode = 1
        AND s.state <> 'D'
	
	DELETE FROM #Categories
	WHERE categoryId NOT IN (
	select categoryId FROM (
		select count(*) as ct, categoryId
		FROM #Categories
		GROUP BY categoryId) as a
	WHERE ct <> @styleCt)

	declare @i int, @currId int, @currStyleName nvarchar(255)
	set @i = 1
	declare @sqlStr nvarchar(max), @endSqlStr nvarchar(max)

	set @sqlStr = 'SELECT userFriendlyName'
	set @endSqlStr = 'FROM (SELECT categoryId, userFriendlyName, styleId
			FROM #Categories) p PIVOT (COUNT(CategoryId) FOR StyleId IN ('
	while @i <= @styleCt
	BEGIN
		--select next styleId
		select @currId = styleId, @currStyleName = CAST(CFStyleName as nvarchar) FROM @styleIds WHERE id = @i
		
		select @sqlStr = @sqlStr + ', [' + CAST(@currId AS nvarchar) + '] as [' + @currStyleName + ']', 
				@endSqlStr = @endSqlStr + '[' + CAST(@currId as nvarchar) + '],'
			
		select @i = @i + 1
	END
	--finish off dynamic sql segment...
	select @sqlStr = @sqlStr + ' ' + substring(@endSqlStr, 1, len(@endSqlStr)-1) + ') ) as pvt'

EXEC(@sqlStr)

DROP TABLE #Categories
GO

GRANT EXECUTE ON [Chrome].[getDifferentiatedCategories] TO MerchandisingUser 
GO