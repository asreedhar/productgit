if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getVinDecodedVehicleAttributes]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getVinDecodedVehicleAttributes]
GO

/****** Object:  StoredProcedure [Chrome].[getVinDecodedVehicleAttributes]    Script Date: 11/06/2008 11:52:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_getVinDecodedVehicleAttributes.sql,v 1.3 2008/11/20 20:01:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the attributes for the vinpattern
 * 
 * 
 * Parameters
 * ----------
 * 
 * @VinPatternID - vin pattern ID from chrome to use
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Chrome].[getVinDecodedVehicleAttributes]
	-- Add the parameters for the stored procedure here
	@VinPatternID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	


    select 
	--sty.autotrans, sty.manualtrans, sty.MSRP, sty.PassengerDoors, 
	--sty.PassengerCapacity, sty.CFDrivetrain, 
	--case when autoTrans = 'S' then 'A/T' else 'M/T' end as VehicleTransmission, 
	e.UserFriendlyName as VehicleEngine,  
	f.UserFriendlyName as FuelType
    --FROM chrome.styles sty
    --INNER JOIN chrome.vinpatternstylemapping sm on sm.chromestyleid = sty.styleid
    --INNER JOIN chrome.vinpattern vp on vp.vinpatternid = sm.vinpatternid
    FROM VehicleCatalog.chrome.vinpattern vp
    LEFT JOIN VehicleCatalog.chrome.categories e on e.categoryid = vp.engineTypeCategoryID and e.countrycode = 1
    LEFT JOIN VehicleCatalog.chrome.categories f on f.categoryid = vp.fuelTypeCategoryID and f.countrycode = 1
    --where styleID = @ChromeStyleID and vp.vinpatternid = @VinPatternID and sty.countrycode = 1 and vp.countrycode = 1
	where vp.vinpatternid = @VinPatternID AND vp.countrycode = 1                
                

END

GO

GRANT EXECUTE ON [Chrome].[getVinDecodedVehicleAttributes] TO MerchandisingUser 
GO