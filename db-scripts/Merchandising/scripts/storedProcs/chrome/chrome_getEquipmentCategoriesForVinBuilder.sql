if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getEquipmentCategoriesForVinBuilder]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getEquipmentCategoriesForVinBuilder]
GO

/****** Object:  StoredProcedure [Chrome].[getEquipmentCategoriesForVinBuilder]    Script Date: 11/03/2008 15:04:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_getEquipmentCategoriesForVinBuilder.sql,v 1.4 2008/11/25 14:20:50 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the categories from chrome are non-standard for one of the chrome styles that match
 * the vin pattern - this is used to create a master set of possible option categories
 * for build print sheets
 * 
 * Parameters
 * ----------
 * @Vin,
 * @BothInteriorOrExteriorFlag - 0 for both, 1 for interior, 2 for exterior
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Chrome].[getEquipmentCategoriesForVinBuilder]
	-- Add the parameters for the stored procedure here
	@Vin varchar(17),
	@BothInteriorOrExteriorFlag int = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--get the vin pattern id for this vehicle
	DECLARE @VinPatternId int
	EXECUTE @VinPatternId = chrome.getVinPattern @Vin

	--get all chrome styles that match this vin pattern
	DECLARE @StyleIDs TABLE (
		styleID int
	)
	INSERT INTO @StyleIDs (styleID)
		SELECT chromeStyleID from VehicleCatalog.chrome.vinPatternStyleMapping 
			WHERE vinPatternId = @VinPatternId AND countrycode = 1
	
    --get the proper category headers (interior/exterior/both)
	DECLARE @headerIDs TABLE (
		headerID int
	)

	INSERT INTO @headerIDs (headerID)
		SELECT categoryHeaderID 
			FROM merchandising.keyDisplayCategories
			WHERE @BothInteriorOrExteriorFlag = 0 
				OR categoryDisplayTypeId = @BothInteriorOrExteriorFlag

	--select categories from stylecats where equipment was NOT standard on one
	--of the styles
	SELECT DISTINCT s.categoryID, c.UserFriendlyName, ch.categoryHeaderID, ch.CategoryHeader, c.CategoryTypeFilter, 'C' as [state]
    FROM
    VehicleCatalog.chrome.stylecats s
    LEFT JOIN (
        --select all standard equip for each vehicle where not delete, is Standard, and country code is US
        select styleid, categoryID 
        from VehicleCatalog.chrome.stylecats 
        where featureType = 'S' 
            and state <> 'D'
            and countrycode = 1
    ) as stds on stds.styleid = s.styleid and stds.categoryid = s.categoryid

    INNER JOIN VehicleCatalog.chrome.categories c
		on c.categoryid = s.categoryid
    INNER JOIN VehicleCatalog.chrome.categoryheaders ch
		on ch.categoryheaderid = c.categoryheaderid
    INNER JOIN @headerIDs hh 
		ON hh.headerID = ch.categoryheaderid
    INNER JOIN @styleIDs stid
		ON s.styleID = stid.styleID
    where 
    
	featureType = 'O'
    AND s.state <> 'D'
    AND stds.categoryID is null  -- we only want the categories where it was non standard for a vehicle
        and s.countrycode = 1
        and c.countrycode = 1
        and ch.countrycode = 1
    
    order by ch.categoryheaderid

END
GO

GRANT EXECUTE ON [Chrome].[getEquipmentCategoriesForVinBuilder] TO MerchandisingUser 
GO