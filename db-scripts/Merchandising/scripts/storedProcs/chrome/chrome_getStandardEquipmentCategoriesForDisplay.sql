if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getStandardEquipmentCategoriesForDisplay]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getStandardEquipmentCategoriesForDisplay]
GO

/****** Object:  StoredProcedure [Chrome].[getStandardEquipmentCategories]    Script Date: 10/30/2008 13:05:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_getStandardEquipmentCategoriesForDisplay.sql,v 1.4 2010/05/06 17:59:37 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * gets the categories from chrome for standard equipment for a styleID with display priority from business unit rankings
 * 
 * 
 * Parameters
 * ----------
 * @BusinessUnitId
 * @ChromeStyleID,
 * @BothInteriorOrExteriorFlag - 0 for both, 1 for interior, 2 for exterior
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Chrome].[getStandardEquipmentCategoriesForDisplay]
	-- Add the parameters for the stored procedure here
	@BusinessUnitId int,
	@ChromeStyleId int,
	@BothInteriorOrExteriorFlag int = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

declare @rankings table (
	categoryID int,
	tierNumber int,
	displayPriority int
)		

IF EXISTS (SELECT 1 FROM settings.EquipmentDisplayRankings WHERE businessUnitId = @BusinessUnitId)
BEGIN
	INSERT INTO @rankings (categoryID, tierNumber, displayPriority)
		SELECT categoryId, tierNumber, displayPriority
		FROM settings.EquipmentDisplayRankings 
		WHERE businessUnitId = @BusinessUnitId
END
ELSE
BEGIN
	INSERT INTO @rankings (categoryID, tierNumber, displayPriority)
		SELECT categoryId, tierNumber, displayPriority
		FROM settings.EquipmentDisplayRankings 
		WHERE businessUnitId = 100150
END

	SELECT cats.*, '' as detailText, '' as optionText, '' as optionCode, r.tierNumber, r.displayPriority, cast(1 as bit) as isStandardEquipment FROM (
	SELECT distinct 
		s.categoryID, c.UserFriendlyName, 
		ch.categoryheaderid, ch.categoryheader, c.categoryTypeFilter, s.state
        FROM VehicleCatalog.chrome.styleCats s
        INNER JOIN VehicleCatalog.chrome.categories c
        ON c.categoryid = s.categoryid
        INNER JOIN VehicleCatalog.chrome.categoryheaders ch
        ON ch.categoryheaderid = c.categoryheaderid
			
        WHERE s.styleID = @ChromeStyleID
        AND featureType = 'S'
        AND s.countrycode = 1
        AND c.countrycode = 1
        AND ch.countrycode = 1
        AND c.categoryTypeFilter <> 'Transmission - Type'

        AND ch.categoryheaderid IN (
			SELECT categoryHeaderID 
				from merchandising.keyDisplayCategories
				WHERE (@BothInteriorOrExteriorFlag = 0 OR categoryDisplayTypeId = @BothInteriorOrExteriorFlag))
        AND s.state <> 'D'
       
   ) as cats
   INNER JOIN @rankings r 
			ON r.categoryId = cats.categoryId
	 order by cats.categoryheaderid	

END
GO

GRANT EXECUTE ON [Chrome].[getStandardEquipmentCategoriesForDisplay] TO MerchandisingUser 
GO