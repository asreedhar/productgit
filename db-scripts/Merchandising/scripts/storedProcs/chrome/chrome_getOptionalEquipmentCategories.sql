if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getOptionalEquipmentCategories]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getOptionalEquipmentCategories]
GO

/****** Object:  StoredProcedure [Chrome].[getOptionalEquipmentCategories]    Script Date: 10/30/2008 13:04:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_getOptionalEquipmentCategories.sql,v 1.6 2009/05/01 23:03:21 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the categories from chrome are from optional equipment ONLY for a styleID
 * This means that if the category was in the standard equipment, it will not appear
 * in this optional return list
 * 
 * Parameters
 * ----------
 * @ChromeStyleID,
 * @BothInteriorOrExteriorFlag - 0 for both, 1 for interior, 2 for exterior
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Chrome].[getOptionalEquipmentCategories]
	-- Add the parameters for the stored procedure here
	@ChromeStyleID int,
	@BothInteriorOrExteriorFlag int = 0
	
AS
BEGIN
	SET NOCOUNT ON;


	WITH headerIDs (headerID)
	AS
	(
        SELECT  categoryHeaderID
        FROM    merchandising.keyDisplayCategories
        WHERE   (@BothInteriorOrExteriorFlag = 0
                 OR categoryDisplayTypeId = @BothInteriorOrExteriorFlag
                )
                AND showOnOptionForm = 1
	)
        SELECT DISTINCT
                s.categoryID,
                c.UserFriendlyName,
                ch.categoryheaderid,
                ch.categoryheader,
                c.CategoryTypeFilter,
                'C' AS [state],
                CAST(0 AS BIT) AS isStandardEquipment
        FROM    VehicleCatalog.chrome.styleCats s
                INNER JOIN VehicleCatalog.chrome.categories c ON c.categoryid = s.categoryid
                INNER JOIN VehicleCatalog.chrome.categoryheaders ch ON ch.categoryheaderid = c.categoryheaderid
                INNER JOIN headerIDs hh ON hh.headerID = ch.categoryheaderid
        WHERE   s.styleID = @ChromeStyleID
                AND featureType = 'O'
                AND s.countrycode = 1
                AND c.countrycode = 1
                AND ch.countrycode = 1
                AND s.state <> 'D'
		--ELIMINATE ALL STANDARD EQUIPMENT W/ THIS AND CLAUSE
                AND NOT c.categoryID IN (
                SELECT DISTINCT
                        s.categoryID
                FROM    VehicleCatalog.chrome.styleCats s
                        INNER JOIN VehicleCatalog.chrome.categories c ON c.categoryid = s.categoryid
                        INNER JOIN VehicleCatalog.chrome.categoryheaders ch ON ch.categoryheaderid = c.categoryheaderid
                        INNER JOIN headerIDs hh ON hh.headerID = ch.categoryheaderid
                WHERE   s.styleID = @ChromeStyleID
                        AND featureType = 'S'
                        AND s.countrycode = 1
                        AND c.countrycode = 1
                        AND ch.countrycode = 1
                        AND s.state <> 'D')
        ORDER BY ch.categoryheaderid
END
GO

GRANT EXECUTE ON [Chrome].[getOptionalEquipmentCategories] TO MerchandisingUser 
GO