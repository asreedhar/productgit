if object_id('Chrome.getVinPattern') is not null
	drop procedure Chrome.getVinPattern
go

/****** Object:  StoredProcedure [Chrome].[getVinPattern]    Script Date: 10/29/2008 09:44:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_getVinPattern.sql,v 1.3 2008/11/20 20:01:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the vin pattern id for a particular vin
 * 
 * 
 
 Dave Speer:  this proc was taking way too many reads (291k and duration (1/3 second)
 due to original order by clause being applied against whole table... breaking out
 to table variable saves us by ordering a small set in an inefficient way vs. large set.
 
 * Parameters
 * ----------
 * 
 * @Vin     - Vin to use for search
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

create procedure Chrome.getVinPattern
	@Vin varchar(17)
as
set nocount on
set transaction isolation level read uncommitted

declare @vinPatternID int

declare @patterns table
(
	VINPatternID int,
	MatchingScore tinyint
)

insert @patterns
(
	VINPatternID,
	MatchingScore
)
select 
	VINPatternID,
	MatchingScore = 
		case when substring(VinPattern,11,1) = substring(@VIN,11,1) then 1 else 0 end +
		case when substring(VinPattern,12,1) = substring(@VIN,12,1) then 1 else 0 end + 
		case when substring(VinPattern,13,1) = substring(@VIN,13,1) then 1 else 0 end +
		case when substring(VinPattern,14,1) = substring(@VIN,14,1) then 1 else 0 end +
		case when substring(VinPattern,15,1) = substring(@VIN,15,1) then 1 else 0 end +
		case when substring(VinPattern,16,1) = substring(@VIN,16,1) then 1 else 0 end +
		case when substring(VinPattern,17,1) = substring(@VIN,17,1) then 1 else 0 end
from VehicleCatalog.Chrome.VINPattern
where
	CountryCode = 1
	and VINPattern_Prefix = substring(@VIN, 1, 8) + substring(@VIN, 10, 1)
	and substring(@VIN, 11, 7) like replace(substring(VINPattern, 11, 7), '*', '_')

-- NOTE: this proc should be avoided because multiple matches with the same rank could be found, but only the first top ranked match is returned.
select top 1 @vinPatternId = VINPatternID from @patterns order by MatchingScore desc, VINPatternID asc


select @vinPatternId as vinPatternId

return @vinPatternId

GO

GRANT EXECUTE ON [Chrome].[getVinPattern] TO MerchandisingUser 
GO