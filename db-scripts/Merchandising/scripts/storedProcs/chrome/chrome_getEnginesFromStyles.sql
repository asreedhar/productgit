if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getEnginesFromStyles]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getEnginesFromStyles]
GO



--- Get available std and optional engines given a list of style ids


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [Chrome].[getEnginesFromStyles]
	-- Add the parameters for the stored procedure here
	@StyleIds varchar(2000) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

;WITH stct AS (
	SELECT styleId, categoryId, featureType, [sequence], [state]
	FROM vehicleCatalog.chrome.stylecats sc
	WHERE countryCode = 1
		and styleId IN (SELECT * FROM dbo.iter$simple_intlist_to_tbl(@StyleIds))
		and categoryId IN (
			1045,
			1046,
			1047,
			1048,
			1049,
			1050,
			1051,
			1052,
			1135,
			1165)
		AND [state] <> 'D'
)
-- Get the list of standard engine descriptions from the available list of chrome style ids
SELECT 
	'' as optionCode,
	[standard] as engineDescription,
	sc.categoryId as engineCategoryId,
	categoryList,
	sc.styleId,
	1 as std
FROM stct sc
INNER JOIN
	vehicleCatalog.chrome.standards std on
	std.styleId = sc.styleId
	AND sc.featureType = 'S'
	AND std.sequence = sc.sequence
	AND std.countrycode = 1
UNION 
-- add the list of optional engines for the chrome styles selected
SELECT 
	optionCode,
	PON as engineDescription,
	sc.categoryId as engineCategoryId,
	categoryList,
	sc.styleId,
	0 as std
	
FROM stct sc
INNER JOIN
	vehicleCatalog.chrome.options op on
	op.styleid = sc.styleId
	and sc.featureType = 'O'
	and op.sequence = sc.sequence
	and op.countrycode = 1
	and NOT op.OptionDesc LIKE '%std%'

ORDER BY styleId

END
GO

GRANT EXECUTE ON [Chrome].[getEnginesFromStyles] TO MerchandisingUser 
GO

