if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getChromeStylesByOEMCode]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getChromeStylesByOEMCode]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- ZB, 12/30/2011
-- Get the chrome style ids associated with the speicified VIN and oem style code
CREATE PROCEDURE [Chrome].[getChromeStylesByOEMCode]
	@Vin CHAR(17),
	@OemFullStyle VARCHAR(20)	
AS
BEGIN
		
	SELECT S.StyleID
	FROM VehicleCatalog.Chrome.VINPatternStyleMapping VSM 
		JOIN VehicleCatalog.Chrome.VINPattern VP ON VSM.CountryCode = VP.CountryCode AND VSM.VINPatternID = VP.VINPatternID
		JOIN VehicleCatalog.Chrome.Styles S ON VSM.ChromeStyleID= S.StyleID	
	WHERE @vin LIKE LEFT(REPLACE(VP.VINPattern, '*','_'),17)
		AND S.CountryCode = 1
		-- maybe this should be LIKE or CONTAINS
		AND S.FullStyleCode = @OemFullStyle
		
END
GO

GRANT EXECUTE ON [Chrome].[getChromeStylesByOEMCode] TO MerchandisingUser 
GO