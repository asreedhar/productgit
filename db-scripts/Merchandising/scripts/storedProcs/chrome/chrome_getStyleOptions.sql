if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getStyleOptions]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getStyleOptions]
GO

/****** Object:  StoredProcedure [Chrome].[getStyleOptions]    Script Date: 11/07/2008 08:50:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_getStyleOptions.sql,v 1.6 2010/01/28 23:12:43 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the alerts for all vehicles that are posted on the internet 
 * for a businessUnit
 * 
 * 
 * Parameters
 * ----------
 * 
 * @ChromeStyleId     - style id of vehicle
 * @FilterPrice  - lowest MSRP price for options to show
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Chrome].[getStyleOptions]
	-- Add the parameters for the stored procedure here
	@ChromeStyleID int,
	@FilterPrice decimal = -1.0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	

SELECT DISTINCT opt.Sequence, opt.OptionCode, opt.CategoryList, opt.PON as description, 
        opt.ExtDescription, ohs.Header as header, cast(0 as bit) as isCustomized,
        opt.TypeFilter, 
        pr.Invoice, pr.MSRP, pr.PriceState, pr.Condition, oks.OptionKindID, oks.OptionKind 
    FROM VehicleCatalog.Chrome.StyleCats sc 
	INNER JOIN VehicleCatalog.Chrome.Options AS opt ON opt.sequence = sc.sequence AND opt.styleID = sc.styleID
    INNER JOIN VehicleCatalog.Chrome.OptHeaders AS ohs ON ohs.HeaderID = opt.HeaderID 
    INNER JOIN VehicleCatalog.Chrome.OptKinds AS oks ON oks.OptionKindID = opt.OptionKindID 
    INNER JOIN VehicleCatalog.Chrome.Prices AS pr ON pr.OptionCode = opt.OptionCode AND pr.StyleID = opt.StyleID 
    WHERE (sc.CountryCode = 1) 
		AND (opt.CountryCode = 1) 
        AND (ohs.CountryCode = 1) 
        AND (oks.CountryCode = 1) 
        AND (pr.CountryCode = 1) 
        AND opt.StyleID = @chromeStyleID 
        AND opt.Availability IN ('R', ' ', 'F') 
		AND sc.FeatureType = 'O'
		AND sc.State <> 'D'
        AND pr.MSRP >= @filterPrice 
		AND extDescription NOT LIKE '%STD%'    

END
GO

GRANT EXECUTE ON [Chrome].[getStyleOptions] TO MerchandisingUser 
GO