if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getStandardEquipmentCategories]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getStandardEquipmentCategories]
GO

/****** Object:  StoredProcedure [Chrome].[getStandardEquipmentCategories]    Script Date: 10/30/2008 13:05:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_getStandardEquipmentCategories.sql,v 1.4 2009/05/01 23:03:21 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the categories from chrome are from standard equipment for a styleID
 * 
 * 
 * Parameters
 * ----------
 * @ChromeStyleID,
 * @BothInteriorOrExteriorFlag - 0 for both, 1 for interior, 2 for exterior
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Chrome].[getStandardEquipmentCategories]
	-- Add the parameters for the stored procedure here
	@ChromeStyleID int,
	@BothInteriorOrExteriorFlag int = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT distinct s.categoryID, c.UserFriendlyName, ch.categoryheaderid, ch.categoryheader, c.categoryTypeFilter, s.state, cast(1 as bit) as isStandardEquipment
        FROM VehicleCatalog.chrome.styleCats s
        INNER JOIN VehicleCatalog.chrome.categories c
        ON c.categoryid = s.categoryid
        INNER JOIN VehicleCatalog.chrome.categoryheaders ch
        ON ch.categoryheaderid = c.categoryheaderid

        WHERE s.styleID = @ChromeStyleID
        AND featureType = 'S'
        AND s.countrycode = 1
        AND c.countrycode = 1
        AND ch.countrycode = 1

        AND ch.categoryheaderid IN (
			SELECT categoryHeaderID 
				from merchandising.keyDisplayCategories
				WHERE (@BothInteriorOrExteriorFlag = 0 OR categoryDisplayTypeId = @BothInteriorOrExteriorFlag))
        AND s.state <> 'D'
        order by ch.categoryheaderid

END
GO

GRANT EXECUTE ON [Chrome].[getStandardEquipmentCategories] TO MerchandisingUser 
GO