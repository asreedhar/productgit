if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[Trims#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[Trims#Fetch]
GO


/****** Object:  StoredProcedure [Chrome].[getAllCategories]    Script Date: 10/30/2008 13:02:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_trims_fetch.sql,v 1.1 2009/06/22 17:22:10 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets all models that match the given divisions from chrome
 * 
 * 
 * Parameters
 * ----------
 * @ModelList - list of chrome divisions
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Chrome].[Trims#Fetch]
	-- Add the parameters for the stored procedure here
	@ModelList varchar(2000) = '',
	@YearList varchar(2000) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	SELECT DISTINCT Trim FROM vehicleCatalog.chrome.styles sty
	INNER JOIN vehicleCatalog.chrome.models mdl
	ON mdl.modelId = sty.modelId
	WHERE 
		mdl.modelName --= 'MDX'
		IN 	(SELECT * FROM dbo.iter$simple_stringlist_to_tbl(@ModelList))
	AND (
		@YearList = '' OR 
		sty.modelYear IN (SELECT * FROM dbo.iter$simple_intlist_to_tbl(@YearList))
	)
	ORDER BY Trim

END
GO

GRANT EXECUTE ON [Chrome].[Trims#Fetch] TO MerchandisingUser 
GO