if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getUnfilteredOptions]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getUnfilteredOptions]
GO

/****** Object:  StoredProcedure [Chrome].[getUnfilteredOptions]    Script Date: 11/07/2008 08:50:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_getUnfilteredOptions.sql,v 1.3 2010/01/28 23:12:43 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the alerts for all vehicles that are posted on the internet 
 * for a businessUnit
 * 
 * 
 * Parameters
 * ----------
 * 
 * @ChromeStyleId     - style id of vehicle
 * @ModelId int=NULL - the chrome modelId to use for a search.  only matters if chromeStyleId is NULL
 *
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Chrome].[getUnfilteredOptions]
	-- Add the parameters for the stored procedure here
	@ChromeStyleID int,
	@ModelId int = NULL,
	@filterPrice decimal = NULL,
	@WhiteListOnly bit = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if (@ChromeStyleID IS NOT NULL)
		begin
			-- if by chome style, set the model id to keep from making the where totally ugly
			SELECT @ModelId = ModelId
				FROM VehicleCatalog.Chrome.Styles
				WHERE StyleID = @ChromeStyleID
				AND CountryCode = 1
		end
		
	-- create a table variable to hold the results prior to whiteList
	DECLARE @unfiltered TABLE
	(
		CountryCode	tinyint not null,
		StyleID int null,
		OptionCode varchar(20) null,
		PON varchar(2000) null,
		MSRP money null
	)
	
	-- create a table variable to hold the results to be returned
	DECLARE @result TABLE
	(
		CountryCode	tinyint not null,
		StyleID int null,
		OptionCode varchar(20) null,
		PON varchar(2000) null,
		MSRP money null
	)
		
	-- get options based on style/model/price
	INSERT INTO @unfiltered
		SELECT options.CountryCode, options.StyleID, options.OptionCode, options.PON, prices.MSRP
			FROM vehiclecatalog.chrome.options
				INNER JOIN VehicleCatalog.Chrome.OptHeaders ON OptHeaders.HeaderID = options.HeaderID 
																AND OptHeaders.CountryCode = 1
				INNER JOIN VehicleCatalog.Chrome.Prices ON Prices.OptionCode = options.OptionCode 
														AND Prices.StyleID = options.StyleID 
														AND Prices.CountryCode = 1
				INNER JOIN VehicleCatalog.Chrome.Styles ON options.StyleID = styles.StyleID 
														AND options.CountryCode = styles.CountryCode
			WHERE 
				options.CountryCode = 1
				AND styles.ModelID = @ModelId
				AND (@ChromeStyleID IS NULL OR options.styleId = @ChromeStyleId)   
				AND (@filterPrice IS NULL OR Prices.MSRP > @filterPrice)
				AND options.Availability IN ('R', ' ', 'F') 
	
	-- check white list
	if @WhiteListOnly = 0
		begin
			-- no white list ... just return what you have!!
			INSERT INTO @result
				SELECT *
					FROM @unfiltered
		end
	else
		begin

			-- find the manufacturer and divisions for this model (style)
			DECLARE @ManufacturerId int, @DivisionId int
			
			SELECT @ManufacturerId = Divisions.ManufacturerID, @DivisionId = Divisions.DivisionID
				FROM VehicleCatalog.Chrome.Models
				INNER JOIN VehicleCatalog.Chrome.Divisions ON Models.DivisionID = Divisions.DivisionID
															AND Models.CountryCode = Divisions.CountryCode
				WHERE Models.ModelID = @ModelId
				AND models.CountryCode = 1
				
			-- to keep this backwardly compatible, if no whitelist and no MSRP or include filters ... load @result with all options 
			if NOT EXISTS (SELECT TOP 1 *
								FROM dbo.ManufacturerVehicleOptionCodeWhiteList 
								WHERE VehicleManufacturerId = @ManufacturerId 
								AND VehicleMakeId = @DivisionId)
				AND NOT EXISTS (SELECT *
								FROM settings.OptionFilter
								WHERE ManufactureId = @ManufacturerId
								)
				AND NOT EXISTS (SELECT *
								FROM settings.OptionFilterIncludePhrase
								WHERE ManufactureId = @ManufacturerId
								)
					begin
						-- no whiteList and or include filters  ... populated @result with all options 
						-- sending throught process incase of exclude filter
						INSERT INTO @result
							SELECT *
								FROM @unfiltered
					end
				else
					begin
						
						-- insert for include phrases
						INSERT INTO @result
							SELECT unfiltered.*
								FROM @unfiltered unfiltered
								CROSS JOIN settings.OptionFilterIncludePhrase
								WHERE OptionFilterIncludePhrase.ManufactureId = @ManufacturerId
								AND unfiltered.PON LIKE '%' + OptionFilterIncludePhrase.IncludePhrase + '%'	
							
						-- insert for MSRP
						if EXISTS (SELECT *
										FROM settings.OptionFilter
										WHERE ManufactureId = @ManufacturerId)
							begin

								INSERT INTO @result
									SELECT *
										FROM @unfiltered unfiltered
										WHERE unfiltered.MSRP >= (SELECT OptionFilter.MinimumMsrp
																		FROM settings.OptionFilter
																		WHERE ManufactureId = @ManufacturerId)
							end
								
						-- delete for exclude phrases
						DELETE result
							FROM @result result
							CROSS JOIN settings.OptionFilterExcludePhrase
							WHERE OptionFilterExcludePhrase.ManufactureId = @ManufacturerId
							AND result.PON LIKE '%' + OptionFilterExcludePhrase.ExcludePhrase + '%'
							
						-- insert for white list
						INSERT INTO @result
							SELECT *
								FROM @unfiltered unfiltered
								WHERE EXISTS (SELECT *
												FROM dbo.ManufacturerVehicleOptionCodeWhiteList 
												WHERE VehicleManufacturerId = @ManufacturerId 
												AND VehicleMakeId = @DivisionId
												AND ManufacturerVehicleOptionCodeWhiteList.OptionCode = unfiltered.OptionCode
												)
							
					end
		end
		
	-- return the results (taken from original SELECT before FB 32397 updates) tureen 3/3/2015
	SELECT DISTINCT options.Sequence, options.OptionCode, options.CategoryList, options.PON as description, 
						options.ExtDescription, cast(0 AS BIT) AS isCustomized,
						OptHeaders.Header AS header, options.TypeFilter, 
						Prices.Invoice, Prices.MSRP, Prices.PriceState, Prices.Condition, OptKinds.OptionKindID, OptKinds.OptionKind
		FROM @result results
		INNER JOIN vehiclecatalog.chrome.options ON results.CountryCode = Options.CountryCode
													AND results.StyleID = Options.StyleID
													AND results.OptionCode = Options.OptionCode
													AND results.PON = Options.PON
		INNER JOIN VehicleCatalog.Chrome.Prices ON results.CountryCode = Prices.CountryCode
													AND results.StyleID = Prices.StyleID
													AND results.OptionCode = Prices.OptionCode
													AND results.MSRP = Prices.MSRP
		INNER JOIN VehicleCatalog.Chrome.OptHeaders ON options.HeaderID = OptHeaders.HeaderID 
													AND options.CountryCode = OptHeaders.CountryCode
		INNER JOIN VehicleCatalog.Chrome.OptKinds ON options.OptionKindID = OptKinds.OptionKindID 
													AND options.CountryCode = OptKinds.CountryCode
		INNER JOIN VehicleCatalog.Chrome.Styles ON options.StyleID = styles.StyleID 
													AND options.CountryCode = styles.CountryCode
		ORDER BY OptHeaders.header, Prices.msrp DESC	
	
	
END
GO

GRANT EXECUTE ON [Chrome].[getUnfilteredOptions] TO MerchandisingUser 
GO