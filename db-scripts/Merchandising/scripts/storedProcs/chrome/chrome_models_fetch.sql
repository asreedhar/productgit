if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[Models#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[Models#Fetch]
GO


/****** Object:  StoredProcedure [Chrome].[getAllCategories]    Script Date: 10/30/2008 13:02:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_models_fetch.sql,v 1.2 2009/06/22 17:22:10 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets all models that match the given divisions from chrome
 * 
 * 
 * Parameters
 * ----------
 * @MakeList - list of chrome divisions
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Chrome].[Models#Fetch]
	-- Add the parameters for the stored procedure here
	@MakeList varchar(2000) = '',
	@YearList varchar(2000) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
	SELECT DISTINCT ModelName FROM vehicleCatalog.chrome.models mdl
	INNER JOIN vehicleCatalog.chrome.divisions div
	ON div.divisionId = mdl.divisionId
	WHERE 
	(
		@MakeList = '' OR 
		div.DivisionName IN 
		(
			SELECT * FROM dbo.iter$simple_stringlist_to_tbl(@MakeList)
		)
	)
	AND 
	(
		@YearList = '' OR 
		modelYear IN 
		(
			SELECT * FROM dbo.iter$simple_intlist_to_tbl(@YearList)
		)
	)
	ORDER BY ModelName

END
GO

GRANT EXECUTE ON [Chrome].[Models#Fetch] TO MerchandisingUser 
GO