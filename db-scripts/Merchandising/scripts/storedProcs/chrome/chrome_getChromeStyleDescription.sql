if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getChromeStyleDescription]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getChromeStyleDescription]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_getChromeStyleDescription.sql,v 1.2 2008/11/20 20:01:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the alerts for all vehicles that are posted on the internet 
 * for a businessUnit
 * 
 * 
 * Parameters
 * ----------
 * 
 * @ChromeStyleID     - integer id of the chrome style for which to get the description info
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Chrome].[getChromeStyleDescription]
	-- Add the parameters for the stored procedure here
	@ChromeStyleID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT sty.CFStyleName, sty.stylenamewotrim, sty.trim 
    FROM VehicleCatalog.Chrome.Styles AS sty 
		
    WHERE (sty.CountryCode = 1) AND
    sty.StyleID = @chromeStyleID

END


GO

GRANT EXECUTE ON [Chrome].[getChromeStyleDescription] TO MerchandisingUser 
GO