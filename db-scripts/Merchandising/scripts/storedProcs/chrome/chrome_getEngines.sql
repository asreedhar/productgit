if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getEngines]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getEngines]
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <3/7/09>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_getEngines.sql,v 1.1 2009/03/13 21:22:35 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets all engines from chrome for a styleId
 * 
 * 
 * Parameters
 * ----------
 * 
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Chrome].[getEngines]
	-- Add the parameters for the stored procedure here
	@StyleId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
declare @stct table (
	styleId int,
	categoryId int,
	featureType varchar(1),
	[sequence] int,
	[state] varchar(1)
)

INSERT INTO @stct (styleId, categoryId, featureType, [sequence], [state])
SELECT styleId, categoryId, featureType, [sequence], [state]
FROM vehicleCatalog.chrome.stylecats sc
WHERE countryCode = 1
	and styleId = @StyleId
	and categoryId IN (
		1045,
		1046,
		1047,
		1048,
		1049,
		1050,
		1051,
		1052,
		1135,
		1165)
	AND [state] <> 'D'

SELECT 
	'' as optionCode,
	[standard] as engineDescription,
	sc.categoryId as engineCategoryId,
	categoryList
FROM @stct sc
INNER JOIN
	vehicleCatalog.chrome.standards std on
	std.styleId = sc.styleId
	AND sc.featureType = 'S'
	AND std.sequence = sc.sequence
	AND std.countrycode = 1
UNION 
SELECT 
	optionCode,
	PON as engineDescription,
	sc.categoryId as engineCategoryId,
	categoryList
FROM @stct sc
INNER JOIN
	vehicleCatalog.chrome.options op on
	op.styleid = sc.styleId
	and sc.featureType = 'O'
	and op.sequence = sc.sequence
	and op.countrycode = 1
	and NOT op.OptionDesc LIKE '%std%'

END
GO

GRANT EXECUTE ON [Chrome].[getEngines] TO MerchandisingUser 
GO