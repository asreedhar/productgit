use Merchandising

if object_id('Chrome.MappingData#Fetch') is not null
	drop procedure Chrome.MappingData#Fetch
go

create procedure Chrome.MappingData#Fetch
as

set nocount on
set transaction isolation level read uncommitted

-- Pull unambiguous VehicleCatalogId => ChromeStyleId mappings

select 	VehicleCatalogId, ChromeStyleId
from	VehicleCatalog.Firstlook.VehicleCatalog#ChromeStyleID

-- Pull VIN Pattern => ChromeStyleId mappings. Mark ambiguous mappings as ChromeStyleId = null
SELECT	[VINPattern_Prefix] as Prefix, substring(VINPattern, 11, 7) as Suffix,
	VPSM.ChromeStyleId,
	VP.VINPatternID
FROM	VehicleCatalog.Chrome.VINPattern VP
	LEFT JOIN (	SELECT	VINPatternID, MAX(ChromeStyleID) ChromeStyleID
			FROM	VehicleCatalog.Chrome.VINPatternStyleMapping
			WHERE	CountryCode = 1
			GROUP
			BY	VINPatternID
			HAVING	COUNT(DISTINCT ChromeStyleID) = 1
			) VPSM ON VP.VINPatternID = VPSM.VINPatternID
WHERE	VP.CountryCode = 1
ORDER BY VINPattern_Prefix -- NOTE: This order is required by the consuming application.

    
GO

GRANT EXECUTE ON Chrome.MappingData#Fetch TO MerchandisingUser 
GO