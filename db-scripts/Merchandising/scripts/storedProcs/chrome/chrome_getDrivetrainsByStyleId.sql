USE [Merchandising]
if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getDrivetrainsByStyleId]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getDrivetrainsByStyleId]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [chrome].[getDrivetrainsByStyleId]
	@ChromeStyleID int	
AS
BEGIN
	SET NOCOUNT ON;
	SELECT DISTINCT CS.CategoryTypeFilter, 
	                coalesce(MSCD.description, CS.UserFriendlyName) as UserFriendlyName, 
	                CS.CategoryID, 
	                CAST(CASE SGE.StyleAvailability
		                WHEN 'Standard' THEN 1
		                ELSE 0
	                END AS BIT) IsStandard,
	                CAST(CASE SGE.StyleAvailability
		                WHEN 'Optional' THEN 1
		                ELSE 0
	                END AS BIT) IsOptional
     FROM VehicleCatalog.Chrome.Categories CS
     LEFT JOIN VehicleCatalog.Chrome.StyleGenericEquipment SGE
	            ON SGE.CountryCode = CS.CountryCode
	            AND SGE.ChromeStyleID = @ChromeStyleId
	            AND SGE.CategoryID = CS.CategoryID
	 LEFT JOIN Merchandising.chrome.ManufacturerSpecificCategoryDescriptions MSCD
				ON MSCD.ChromeStyleID = SGE.ChromeStyleID
				and MSCD.CategoryID = CS.CategoryID
     WHERE CS.CountryCode = 1
           AND CS.CategoryTypeFilter = 'Drivetrain'
           ORDER BY UserFriendlyName

END
GO

GRANT EXECUTE ON [Chrome].[getDrivetrainsByStyleId] TO MerchandisingUser 
GO