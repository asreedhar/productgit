if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getMarketClass]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getMarketClass]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/5/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_getMarketClass.sql,v 1.3 2008/11/20 20:01:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * get market class for a vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @ChromeStyleID     - integer id of the vehicle chrome style
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [chrome].[getMarketClass]
	
	@ChromeStyleID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT mc.MarketClass
        FROM VehicleCatalog.Chrome.Styles as sty
        INNER JOIN VehicleCatalog.Chrome.MktClass as mc ON mc.MktClassID = sty.MktClassID
        WHERE (mc.CountryCode = 1) AND 
        (sty.CountryCode = 1) AND sty.StyleID = @ChromeStyleID

END
GO

GRANT EXECUTE ON [Chrome].[getMarketClass] TO MerchandisingUser 
GO