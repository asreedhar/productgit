if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getChromeStyles]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getChromeStyles]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_getChromeStyles.sql,v 1.3 2009/12/09 20:02:04 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the styles for a given vin pattern id
 * 
 * 
 * Parameters
 * ----------
 * 
 * @VinPatternID     - integer id of the VIN Pattern
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Chrome].[getChromeStyles]
	-- Add the parameters for the stored procedure here
	@VinPatternID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    SELECT 
		case when sty.trim = '' then 'base' else sty.trim end as trim, 
		sty.styleNameWOTrim, 
		styleID as styleID, 
		styleCode as modelCode,
		[sequence] as seq,
		sty.manualTrans, sty.AutoTrans, sty.FrontWD, sty.RearWD, sty.AllWD, sty.FourWD
        FROM VehicleCatalog.Chrome.VINPatternStyleMapping AS sm 
        INNER JOIN VehicleCatalog.Chrome.Styles as sty ON sty.StyleID = sm.ChromeStyleID 
        WHERE (sm.CountryCode = 1) AND (sty.CountryCode = 1) AND                 
        sm.vinpatternID = @VinPatternID
    group by styleID, styleCode, sequence, trim, stylenamewotrim, sty.manualTrans, sty.AutoTrans, sty.FrontWD, sty.RearWD, sty.AllWD, sty.FourWD
    order by seq

END


GO

GRANT EXECUTE ON [Chrome].[getChromeStyles] TO MerchandisingUser 
GO