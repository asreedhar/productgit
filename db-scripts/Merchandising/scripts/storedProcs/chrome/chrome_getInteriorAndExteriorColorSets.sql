if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getInteriorAndExteriorColorSets]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getInteriorAndExteriorColorSets]
GO

/****** Object:  StoredProcedure [Chrome].[getInteriorAndExteriorColorSets]    Script Date: 10/26/2008 15:34:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_getInteriorAndExteriorColorSets.sql,v 1.3 2008/11/20 20:01:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the alerts for all vehicles that are posted on the internet 
 * for a businessUnit
 * 
 * 
 * Parameters
 * ----------
 * 
 * @styleID     - integer id chrome style to lookup
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Chrome].[getInteriorAndExteriorColorSets]
	-- Add the parameters for the stored procedure here
	@styleID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT DISTINCT IntDesc, IntCode, IntManCode 
    FROM VehicleCatalog.Chrome.Colors 
    WHERE (CountryCode = 1) AND (StyleID = @styleID);

SELECT DISTINCT Ext1Desc, Ext1Code, Ext1ManCode, 
		GenericExtColor, Ext1RGBHex, Ext2Desc, Ext2Code, 
		Ext2ManCode, GenericExt2Color, Ext2RGBHex 
	FROM VehicleCatalog.Chrome.Colors 
	WHERE (CountryCode = 1) AND (StyleID = @styleID)
	

END
GO

GRANT EXECUTE ON [Chrome].[getInteriorAndExteriorColorSets] TO MerchandisingUser 
GO