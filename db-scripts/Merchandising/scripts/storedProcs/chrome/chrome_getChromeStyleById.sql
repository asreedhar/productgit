if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getChromeStyleById]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getChromeStyleById]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- ZB, 12/30/2011
-- Get the chrome style by style id. 
CREATE PROCEDURE [Chrome].[getChromeStyleById]
	@StyleId int
AS
BEGIN

SELECT 
	case when sty.trim = '' then 'base' else sty.trim end as trim, 
	sty.styleNameWOTrim, 
	styleID as styleID, 
	styleCode as modelCode,
	[sequence] as seq,
	sty.manualTrans, 
	sty.AutoTrans, 
	sty.FrontWD, 
	sty.RearWD, 
	sty.AllWD, 
	sty.FourWD

FROM 
	VehicleCatalog.Chrome.Styles as sty
WHERE 
	sty.StyleID = @StyleId
	
END
GO

GRANT EXECUTE ON [Chrome].[getChromeStyleById] TO MerchandisingUser 
GO