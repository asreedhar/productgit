IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'chrome.ColorOptionCode#LookupByVin') AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE chrome.ColorOptionCode#LookupByVin
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE chrome.ColorOptionCode#LookupByVin

	@vin char(17) = NULL
	
AS
BEGIN

	SET NOCOUNT ON 
	
	DECLARE @styleID int

	SELECT @styleID = ChromeStyleID
	FROM FLDW.dbo.Vehicle v
		INNER JOIN IMT.dbo.Inventory i ON i.VehicleID = v.VehicleID
		INNER JOIN builder.OptionsConfiguration oc ON oc.InventoryID = i.InventoryID AND oc.BusinessUnitID = i.BusinessUnitID
		INNER JOIN vehiclecatalog.chrome.options opt ON opt.StyleID = oc.ChromeStyleID AND opt.CountryCode = 1
	WHERE v.VIN = @vin

	SELECT DISTINCT * FROM chrome.ColorOptionCode#LookupByManufacturer WHERE StyleID = @styleID and CountryCode = 1

END
GO


GRANT EXECUTE ON chrome.ColorOptionCode#LookupByVin TO MerchandisingUser
GO
