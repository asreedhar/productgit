if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getStyleCategories]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getStyleCategories]
GO

/****** Object:  StoredProcedure [Chrome].[getStyleCategories]    Script Date: 10/30/2008 13:04:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * Altered by <Fitzpatrick, Benin> for the use of excluding certain equipment options
 * works in conjuction with the VehicleVinDataEditor User Control.
 * $Id: chrome_getStyleCategories.sql,v 1.4 2010/03/10 21:29:51 bfitzpatrick Exp $
 * 
 * Summary
 * -------
 * 
 * gets the categories from chrome are from optional equipment ONLY for a styleID
 * This means that if the category was in the standard equipment, it will not appear
 * in this optional return list
 * 
 * Parameters
 * ----------
 * @ChromeStyleID,
 * @BothInteriorOrExteriorFlag - 0 for both, 1 for interior, 2 for exterior
 * 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Chrome].[getStyleCategories]
	-- Add the parameters for the stored procedure here
	@ChromeStyleID int,
	@BothInteriorOrExteriorFlag int = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @headerIDs TABLE (
		headerID int
	)

	INSERT INTO @headerIDs (headerID)
		SELECT categoryHeaderID 
			FROM merchandising.keyDisplayCategories
			WHERE (@BothInteriorOrExteriorFlag = 0 
				OR categoryDisplayTypeId = @BothInteriorOrExteriorFlag)
				AND showOnOptionForm = 1
	declare @equip table (
		categoryId int,
		userFriendlyName varchar(100),
		categoryHeader varchar(100),
		isStandardEquipment bit
	)
	INSERT INTO @equip
	SELECT DISTINCT s.categoryID, 
			c.UserFriendlyName,
			ch.categoryheader,
			1 as isStandardEquipment
            FROM VehicleCatalog.chrome.styleCats s
            INNER JOIN VehicleCatalog.chrome.categories c
            ON c.categoryid = s.categoryid
            INNER JOIN VehicleCatalog.chrome.categoryheaders ch
            ON ch.categoryheaderid = c.categoryheaderid
--takes only the interior/exterior (or both) categories
			INNER JOIN @headerIDs hh ON hh.headerID = ch.categoryheaderid
            
		WHERE s.styleID = @ChromeStyleID
            AND featureType = 'S'
            AND s.countrycode = 1
            AND c.countrycode = 1
            AND ch.countrycode = 1
            AND c.CategoryTypeFilter != 'Transmission - Type'
            AND s.state <> 'D'
    
    INSERT INTO @equip
	SELECT DISTINCT s.categoryID, 
		c.UserFriendlyName,
		ch.categoryheader,
		0 as isStandardEquipment
		FROM VehicleCatalog.chrome.styleCats s
        INNER JOIN VehicleCatalog.chrome.categories c
        ON c.categoryid = s.categoryid
        INNER JOIN VehicleCatalog.chrome.categoryheaders ch
        ON ch.categoryheaderid = c.categoryheaderid
		INNER JOIN @headerIDs hh ON hh.headerID = ch.categoryheaderid

        WHERE s.styleID = @ChromeStyleID
        AND featureType = 'O'
        AND s.countrycode = 1
        AND c.countrycode = 1
        AND ch.countrycode = 1
        AND c.CategoryTypeFilter != 'Transmission - Type'
        AND s.state <> 'D'
		--ELIMINATE ALL STANDARD EQUIPMENT W/ THIS AND CLAUSE
		AND NOT c.categoryID IN (
				select categoryId from @equip
            )
        
        
   select *
   FROM @equip
   WHERE categoryHeader != 'Engine'
	AND categoryHeader != 'Drivetrain'
	AND categoryHeader != 'Fuel'
	AND categoryHeader != 'Transmission'
   order by categoryHeader

END
GO

GRANT EXECUTE ON [Chrome].[getStyleCategories] TO MerchandisingUser 
GO