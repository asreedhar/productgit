if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getUniqueOptionsAmongChromeStyles]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getUniqueOptionsAmongChromeStyles]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- ZB, 1/5/2012
-- Given a list of style ids, determine which options exist on only _one_ of the styles.
-- return such options, along with the correct style.
-- WGH 06/25/2013 Optimized, as query was prone to stalling

CREATE PROCEDURE [Chrome].[getUniqueOptionsAmongChromeStyles]
	@styles VARCHAR(255)
AS
BEGIN
		
	CREATE TABLE #Styles (StyleID INT)
	INSERT
	INTO	#Styles
	SELECT	CAST(Value AS INT)
	FROM	Utility.String.Split(@styles,',')
	
	SELECT	OptionCode, COUNT(DISTINCT O.StyleID) OptionCount , MAX(O.StyleID) AS StyleId
	FROM	VehicleCatalog.Chrome.Options O
	 	inner join #Styles S on O.StyleId = S.StyleID
	WHERE 
		CountryCode = 1
	GROUP 
	BY	OptionCode
	HAVING	COUNT(DISTINCT O.StyleID) = 1
END
GO

GRANT EXECUTE ON [Chrome].[getUniqueOptionsAmongChromeStyles] TO MerchandisingUser 
GO