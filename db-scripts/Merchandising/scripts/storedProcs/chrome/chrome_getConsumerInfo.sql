if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getConsumerInfo]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getConsumerInfo]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_getConsumerInfo.sql,v 1.2 2008/11/20 20:01:20 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * gets the consumer info entries from chrome base on id
 * 
 * 
 * Parameters
 * ----------
 * 
 * @ChromeStyleID     - integer id of the businesUnit for which to get the alert list
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Chrome].[getConsumerInfo]
	-- Add the parameters for the stored procedure here
	@ChromeStyleID int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT ci.Text, ct.Type, ci.TypeID from VehicleCatalog.Chrome.ConsInfo as ci 
		INNER JOIN VehicleCatalog.Chrome.CITypes as ct ON ci.TypeID = ct.TypeID 
		WHERE ci.StyleID = @ChromeStyleID 
		AND ci.CountryCode = 1 AND ct.CountryCode = 1 AND ci.TypeID IN (2,4, 5, 8, 11, 12)
		ORDER BY ci.TypeID

END
GO

GRANT EXECUTE ON [Chrome].[getConsumerInfo] TO MerchandisingUser 
GO