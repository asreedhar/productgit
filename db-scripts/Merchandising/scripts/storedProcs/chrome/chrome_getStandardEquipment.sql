if exists (select * from dbo.sysobjects where id = object_id(N'[chrome].[getStandardEquipment]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [chrome].[getStandardEquipment]
GO

/****** Object:  StoredProcedure [Chrome].[getStandardEquipment]    Script Date: 10/26/2008 15:36:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: chrome_getStandardEquipment.sql,v 1.5 2010/01/28 23:12:43 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * Get all standard equipment from chrome for a given styleid and equipment type
 * 
 * 
 * Parameters
 * ----------
 * 
 * @ChromeStyleID 
 * @StandardHeaderType - the header type as defined in merchandising.standardEquipmentTypes
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [Chrome].[getStandardEquipment]
	-- Add the parameters for the stored procedure here
	@ChromeStyleID int,
	@StandardHeaderType int = -1
	
AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	

	SELECT st.Sequence, st.Standard as description, st.categoryList, sh.header, cast(0 as bit) as isCustomized
    FROM VehicleCatalog.Chrome.Standards AS st
    INNER JOIN VehicleCatalog.Chrome.StdHeaders AS sh ON sh.HeaderID = st.HeaderID
    WHERE st.CountryCode = 1 
		AND sh.CountryCode = 1
		AND st.StyleID = @ChromeStyleID 
		AND (@StandardHeaderType = -1 OR
			sh.headerID IN 
			(SELECT headerID 
				FROM VehicleCatalog.chrome.stdHeaders h
				INNER JOIN merchandising.standardEquipmentMapping m
				ON m.standardEquipmentHeaderId = h.headerId
				WHERE m.standardEquipmentTypeID = @StandardHeaderType)
			)
    ORDER BY st.sequence
	

END
GO

GRANT EXECUTE ON [Chrome].[getStandardEquipment] TO MerchandisingUser 
GO