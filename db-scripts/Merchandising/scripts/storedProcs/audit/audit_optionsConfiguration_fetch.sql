if exists (select * from dbo.sysobjects where id = object_id(N'[audit].[OptionsConfiguration#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [audit].[OptionsConfiguration#Fetch]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: audit_optionsConfiguration_fetch.sql,v 1.1 2009/03/13 21:22:35 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch the audit history for vehicle config information
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId- vehicle inventory id
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [audit].[OptionsConfiguration#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@InventoryId int
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	
	SELECT 
		*
		FROM audit.OptionsConfiguration 
			
		WHERE
		inventoryId = @InventoryId
		AND businessUnitId = @BusinessUnitId
		
		
END
GO

GRANT EXECUTE ON [audit].[OptionsConfiguration#Fetch] TO MerchandisingUser 
GO