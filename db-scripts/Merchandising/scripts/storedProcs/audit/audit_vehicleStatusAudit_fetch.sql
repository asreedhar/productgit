if exists (select * from dbo.sysobjects where id = object_id(N'[audit].[VehicleStatusAudit#Fetch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [audit].[VehicleStatusAudit#Fetch]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <10/22/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: audit_vehicleStatusAudit_fetch.sql,v 1.1 2008/12/11 19:23:38 jkelley Exp $
 * 
 * Summary
 * -------
 * 
 * fetch the audit history for vehicle status record flags for a build step for a given vehicle
 * 
 * 
 * Parameters
 * ----------
 * 
 * @BusinessUnitId     - integer id of the businesUnit for which to get the alert list
 * @InventoryId- vehicle inventory id
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

CREATE PROCEDURE [audit].[VehicleStatusAudit#Fetch]
	-- Add the parameters for the stored procedure here
	@BusinessUnitID int,
	@InventoryId int,
	@StatusTypeId int = NULL
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
	
	SELECT 
		inventoryId,
		actionType,
		statusType, 
		vs.statusTypeId,
		statusLevel, --RETURN NULL TO INDICATE LACK OF EXISTENCE 
		createdBy, 
		createdOn, 
		lastUpdatedBy, 
		lastUpdatedOn, 
		version
		FROM audit.VehicleStatus vs
			INNER JOIN builder.VehicleStatusTypes vst
				ON vst.statusTypeId = vs.statusTypeId
	
		WHERE
		inventoryId = @InventoryId
		AND businessUnitId = @BusinessUnitId
		AND (@StatusTypeId IS NULL OR vs.statusTypeId = @StatusTypeId)
		ORDER BY auditStatusId
		
END
GO

GRANT EXECUTE ON [audit].[VehicleStatusAudit#Fetch] TO MerchandisingUser 
GO