if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[BlurbSampleText]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [templates].[BlurbSampleText]
GO

/****** Object:  Table [templates].[BlurbSampleText]    Script Date: 10/26/2008 13:03:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [templates].[BlurbSampleText](
	[blurbTextID] [int] IDENTITY(1,1) NOT NULL,
	[blurbPreText] [varchar](2000) NOT NULL CONSTRAINT [DF_blurbSampleText_blurbPreText]  DEFAULT (''),
	[blurbPostText] [varchar](2000) NOT NULL CONSTRAINT [DF_blurbSampleText_blurbPostText]  DEFAULT (''),
	[blurbId] [int] NOT NULL,
	[vehicleProfileId] [int] NULL,
	[businessUnitId] [int] NOT NULL CONSTRAINT [DF_blurbSampleText_businessUnitId]  DEFAULT ((0)),
	[active] [bit] NOT NULL DEFAULT((1)),
	CONSTRAINT [PK_BlurbSampleText] PRIMARY KEY CLUSTERED 
(
	[blurbTextID] ASC
)
)

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [templates].[BlurbSampleText]  WITH CHECK ADD  CONSTRAINT [FK_BlurbSampleText_BlurbTemplates] FOREIGN KEY([blurbId])
REFERENCES [templates].[BlurbTemplates] ([blurbId])
GO
ALTER TABLE [templates].[BlurbSampleText] CHECK CONSTRAINT [FK_BlurbSampleText_BlurbTemplates]