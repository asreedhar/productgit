if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[LiquidusMakes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[LiquidusMakes]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[LiquidusMakes](
	
	[liquidusMakeId] [int] NOT NULL,
	[chromeMakeName] [varchar](30) NOT NULL
	
)

GO
SET ANSI_PADDING OFF