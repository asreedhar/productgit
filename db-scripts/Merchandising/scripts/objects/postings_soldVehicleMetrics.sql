if exists (select * from dbo.sysobjects where id = object_id(N'[postings].[SoldVehicleMetrics]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [postings].[soldVechicleMetrics]
GO
/****** Object:  Table [postings].[SoldVehicleMetrics]    Script Date: 10/26/2008 12:57:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [postings].[SoldVehicleMetrics](
	[metricID] [int] IDENTITY(1,1) NOT NULL,
	[sourceID] [int] NOT NULL,
	[businessUnitID] [int] NOT NULL CONSTRAINT [DF_soldVehicleMetrics_businessUnitID]  DEFAULT ((-1)),
	[inventoryId] [int] NULL,
	[stockNumber] [varchar](20) NOT NULL,
	[removedDate] [datetime] NOT NULL,
	[lastLeadDate] [datetime] NULL,
	[lastLeadType] [varchar](80) NOT NULL CONSTRAINT [DF_soldVehicleMetrics_lastLeadType]  DEFAULT (''),
	[adsPrinted] [int] NOT NULL CONSTRAINT [DF_soldVehicleMetrics_adsPrinted]  DEFAULT ((0)),
	[daysLive] [int] NOT NULL CONSTRAINT [DF_soldVehicleMetrics_daysLive]  DEFAULT ((0)),
	[searchViewed] [int] NOT NULL CONSTRAINT [DF_soldVehicleMetrics_searchedViewd]  DEFAULT ((0)),
	[detailsViewed] [int] NOT NULL CONSTRAINT [DF_soldVehicleMetrics_detailsViewed]  DEFAULT ((0)),
	[emails] [int] NOT NULL CONSTRAINT [DF_soldVehicleMetrics_emails]  DEFAULT ((0)),
	[mapsViewed] [int] NOT NULL CONSTRAINT [DF_soldVehicleMetrics_mapsViewed]  DEFAULT ((0)),
	[websiteClickthrus] [int] NOT NULL CONSTRAINT [DF_soldVehicleMetrics_websiteClickthrus]  DEFAULT ((0)),
	[modelYear] [int] NOT NULL CONSTRAINT [DF_soldVehicleMetrics_year]  DEFAULT ((-1)),
	[make] [varchar](50) NOT NULL CONSTRAINT [DF_soldVehicleMetrics_make]  DEFAULT (''),
	[model] [varchar](50) NOT NULL CONSTRAINT [DF_soldVehicleMetrics_model]  DEFAULT (''),
	[usedOrNew] [varchar](1) NOT NULL CONSTRAINT [DF_soldVehicleMetrics_usedOrNew]  DEFAULT (''),
 CONSTRAINT [PK_soldVehicleMetrics] PRIMARY KEY CLUSTERED 
(
	[businessUnitID] ASC,
	[sourceID] ASC,
	[stockNumber] ASC,
	[removedDate] ASC
)
)

GO
SET ANSI_PADDING OFF