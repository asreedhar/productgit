if exists (select * from dbo.sysobjects where id = object_id(N'[alerts].[Reports]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [alerts].[Reports]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [alerts].[Reports](
	
	[reportId] [int] NOT NULL,
	[title] [nvarchar](256) NOT NULL
	
)

GO
SET ANSI_PADDING OFF