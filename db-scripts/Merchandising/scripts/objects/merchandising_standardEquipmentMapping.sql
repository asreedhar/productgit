if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[StandardEquipmentMapping]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [merchandising].[StandardEquipmentMapping]
GO

/****** Object:  Table [merchandising].[StandardEquipmentMapping]    Script Date: 10/26/2008 15:26:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [merchandising].[StandardEquipmentMapping](
	[standardEquipmentTypeID] [int] NOT NULL,
	[standardEquipmentHeaderId] [int] NOT NULL
)
