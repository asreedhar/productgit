if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[BlurbTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [templates].[BlurbTypes]
GO

/****** Object:  Table [templates].[BlurbTypes]    Script Date: 10/26/2008 13:03:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [templates].[BlurbTypes](
	[blurbTypeId] [int] IDENTITY(1,1) NOT NULL,
	[blurbTypeName] [varchar](50) NOT NULL CONSTRAINT [DF_blurbTypes_blurbTypeName]  DEFAULT ('')
CONSTRAINT [PK_BlurbTypes] PRIMARY KEY CLUSTERED 
(
	[blurbTypeId] ASC
)
)

GO
SET ANSI_PADDING OFF