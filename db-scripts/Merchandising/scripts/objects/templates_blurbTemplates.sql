if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[BlurbTemplates]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [templates].[BlurbTemplates]
GO

/****** Object:  Table [templates].[BlurbTemplates]    Script Date: 10/26/2008 13:03:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [templates].[BlurbTemplates](
	[blurbId] [int] IDENTITY(1,1) NOT NULL,
	[blurbName] [varchar](50) NOT NULL CONSTRAINT [DF_blurbTemplates_blurbTemplateName]  DEFAULT (''),
	[blurbTemplate] [varchar](500) NOT NULL CONSTRAINT [DF_blurbTemplates_blurbTemplateText]  DEFAULT (''),
	[blurbCondition] [varchar](500) NOT NULL CONSTRAINT [DF_blurbTemplates_blurbTemplateCondition]  DEFAULT (''),
	[blurbTypeId] [int] NOT NULL,
	[businessUnitId] [int] NOT NULL CONSTRAINT [DF_blurbTemplates_businessUnitId]  DEFAULT ((0))
CONSTRAINT [PK_BlurbTemplates] PRIMARY KEY CLUSTERED 
(
	[blurbId] ASC
)
)

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [templates].[BlurbTemplates]  WITH CHECK ADD  CONSTRAINT [FK_BlurbTemplates_BlurbTypes1] FOREIGN KEY([blurbTypeId])
REFERENCES [templates].[BlurbTypes] ([blurbTypeId])
GO
ALTER TABLE [templates].[BlurbTemplates] CHECK CONSTRAINT [FK_BlurbTemplates_BlurbTypes1]