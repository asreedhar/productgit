if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[KeyDisplayCategories]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [merchandising].[KeyDisplayCategories]
GO
/****** Object:  Table [merchandising].[KeyDisplayCategories]    Script Date: 11/10/2008 08:00:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [merchandising].[KeyDisplayCategories](
	[categoryDisplayTypeId] [int] NOT NULL,
	[categoryHeaderId] [int] NOT NULL,
	[showOnOptionForm] [bit] NOT NULL
)
