if exists (select * from dbo.sysobjects where id = object_id(N'[postings].[VehicleAdScheduling]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [postings].[VehicleAdScheduling]
GO

/****** Object:  Table [postings].[VehicleAdScheduling]    Script Date: 10/26/2008 12:58:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [postings].[VehicleAdScheduling](
	[schedulingID] [int] IDENTITY(1,1) NOT NULL,
	[businessUnitID] [int] NOT NULL,
	[inventoryId] [int] NOT NULL,
	[destinationID] [int] NOT NULL,
	[scheduledReleaseTime] [datetime] NULL,
	[destinationEnabled] [tinyint] NOT NULL CONSTRAINT [DF_VehicleAdScheduling_destinationEnabled]  DEFAULT ((1)),
	[actualReleaseTime] [datetime] NULL,
	[expiresOn] [datetime] NULL,
	[merchandisingDescription] [varchar](max) NOT NULL,
	[equipmentList] [varchar](max) NOT NULL,
	[listPrice] [decimal](9,2) NULL,
	[advertisementStatus] [int] NOT NULL,
	[approvedByMemberLogin] [varchar](30) NOT NULL,
	[approvedTimeStamp] [datetime] NOT NULL DEFAULT ((getdate())),
 CONSTRAINT [PK_VehicleAdScheduling] PRIMARY KEY CLUSTERED 
(
	[schedulingID] ASC
)
)

GO
SET ANSI_PADDING OFF