if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[WindowStickerTemplates]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[WindowStickerTemplates]
GO
/****** Object:  Table [settings].[WindowStickerTemplates]    Script Date: 10/26/2008 13:02:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[WindowStickerTemplates](
	[windowStickerTemplateID] [int] IDENTITY(1,1) NOT NULL,
	[styleSheetPath] [varchar](200) NOT NULL
)

GO
SET ANSI_PADDING OFF