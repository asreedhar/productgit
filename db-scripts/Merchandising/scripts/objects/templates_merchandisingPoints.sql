if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[MerchandisingPoints]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [templates].[MerchandisingPoints]
GO

/****** Object:  Table [templates].[MerchandisingPoints]    Script Date: 10/26/2008 13:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [templates].[MerchandisingPoints](
	[BusinessUnitID] [int] NOT NULL,
	[pointID] [int] IDENTITY(1,1) NOT NULL,
	[ruleType] [smallint] NOT NULL,
	[ruleParameter] [varchar](10) NOT NULL CONSTRAINT [DF_MerchandisingPoints_ruleParameter]  DEFAULT (''),
	[pointTypeID] [int] NOT NULL CONSTRAINT [DF_MerchandisingPoints_pointTypeID]  DEFAULT ((1)),
	[templateDisplayText] [varchar](50) NOT NULL CONSTRAINT [DF_MerchandisingPoints_templateDisplayText]  DEFAULT (''),
	[dataAccessString] [varchar](100) NOT NULL CONSTRAINT [DF_MerchandisingPoints_dataAccessString]  DEFAULT (''),
 CONSTRAINT [PK_MerchandisingTemplates] PRIMARY KEY CLUSTERED 
(
	[pointID] ASC
)
)

GO
SET ANSI_PADDING OFF