if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[descriptionContents]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[descriptionContents]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[descriptionContents](
	[businessUnitId] [int] NOT NULL,
	[inventoryId] [int] NOT NULL,
	[descriptionItemTypeId] [int] NOT NULL
)

GO
SET ANSI_PADDING OFF