if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[WebMetricScheduling]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[WebMetricScheduling]
GO
/****** Object:  Table [settings].[WebMetricScheduling]    Script Date: 10/26/2008 13:01:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [settings].[WebMetricScheduling](
	[sourceID] [int] NOT NULL,
	[collectionFrequency] [int] NOT NULL,
	[businessUnitID] [int] NOT NULL,
	[collectSearchData] [tinyint] NOT NULL,
	[collectClickData] [tinyint] NOT NULL,
	[collectActionData] [tinyint] NOT NULL,
	[activeFlag] [tinyint] NOT NULL CONSTRAINT [DF_WebMetricScheduling_activeFlag]  DEFAULT ((0)),
 CONSTRAINT [PK_WebMetricScheduling] PRIMARY KEY CLUSTERED 
(
	[sourceID] ASC,
	[businessUnitID] ASC
)
)
