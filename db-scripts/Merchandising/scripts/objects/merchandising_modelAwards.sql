if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[ModelAwards]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [merchandising].[ModelAwards]
GO

/****** Object:  Table [merchandising].[StandardEquipmentMapping]    Script Date: 10/26/2008 15:26:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [merchandising].[ModelAwards](
	[modelAwardId] int IDENTITY(1,1),
	[importanceRanking] int NOT NULL DEFAULT(100000),
	[make] [varchar](30) NOT NULL,
	[model] [varchar](30) NOT NULL,
	[trim] [varchar](30) NOT NULL,
	[modelYear] int NULL,
	[awardText] varchar(200)
)
GO