if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[GetAutoImages]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [merchandising].[GetAutoImages]
GO

/****** Object:  Table [merchandising].[GetAutoImages]    Script Date: 10/26/2008 13:05:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [merchandising].[GetAutoImages](
	[photoItemID] [int] IDENTITY(1,1) NOT NULL,
	[DealerID] [nvarchar](50) NULL,
	[VIN] [nvarchar](50) NULL,
	[Vevo] [nvarchar](50) NULL,
	[AltVideo] [nvarchar](50) NULL,
	[PhotoUrls] [nvarchar](4000) NULL,
	[WindowSticker] [nvarchar](50) NULL,
	[BuyersGuide] [nvarchar](50) NULL,
	[AuctionLink] [nvarchar](50) NULL,
	[VehicleHistoryReport] [nvarchar](50) NULL,
	[Reserved1] [nvarchar](50) NULL,
	[Reserved2] [nvarchar](50) NULL,
	[Reserved3] [nvarchar](50) NULL,
	[Reserved4] [nvarchar](50) NULL,
	[Reserved5] [nvarchar](50) NULL,
	[Reserved6] [nvarchar](50) NULL,
 CONSTRAINT [PK_GetAutoImages] PRIMARY KEY CLUSTERED 
(
	[photoItemID] ASC
)
)
