if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleConditionTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[VehicleConditionTypes]
GO
/****** Object:  Table [builder].[VehicleConditionTypes]    Script Date: 10/26/2008 12:54:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[VehicleConditionTypes](
	[vehicleConditionTypeID] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](30) NOT NULL,
	[category] int NOT NULL
	
)

GO
SET ANSI_PADDING OFF