if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[Descriptions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[Descriptions]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[Descriptions](
	[businessUnitID] [int] NOT NULL,
	[inventoryId] [int] NOT NULL,
	[acceleratorAdvertisementId] [int] NULL,
	[merchandisingDescription] [varchar](max) NOT NULL DEFAULT(''),
	[footer] [varchar](2000) NOT NULL DEFAULT(''),
	[expiresOn] [datetime] NULL,
	[createdOn] [datetime] NOT NULL DEFAULT (getdate()),
	[createdBy] [varchar](30) NOT NULL DEFAULT(''),
	[lastUpdatedOn] [datetime] NOT NULL DEFAULT(getdate()),
	[lastUpdatedBy] [varchar](30) NOT NULL DEFAULT(''),
	[version] [timestamp] NOT NULL,
CONSTRAINT [PK_Descriptions] PRIMARY KEY CLUSTERED 
(
	[businessUnitID] ASC,
	[inventoryID] ASC
)
)

GO
SET ANSI_PADDING OFF