if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[MerchandisingDescriptionPreferences]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[MerchandisingDescriptionPreferences]
GO

/****** Object:  Table [settings].[MerchandisingDescriptionPreferences]    Script Date: 11/13/2008 15:46:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [settings].[MerchandisingDescriptionPreferences](
	[businessUnitId] [int] NOT NULL,
	[maxOptionsTier0] [int] NOT NULL,
	[maxOptionsTier1] [int] NOT NULL,
	[maxOptionsTier2] [int] NOT NULL,
	[maxOptionsTier3] [int] NOT NULL,
	[goodBookDifferential] [decimal](18, 0) NOT NULL,
	[goodMsrpDifferential] [decimal](18, 0) NOT NULL,
	[minNumberForCountBelowBookDisplay] [int] NOT NULL,
	[minNumberForCountBelowPriceDisplay] [int] NOT NULL,
	[goodWarrantyMileageRemaining] [int] NOT NULL,
	[maxWarrantiesToDisplay] [int] NOT NULL,
	[goodCrashTestRating] [int] NOT NULL,
	[maxCrashTestRatingsToDisplay] [int] NOT NULL,
	[countTier1ForLoaded] [int] NOT NULL,
	[textSeparator] [varchar](10) NOT NULL DEFAULT(('~')),
	[preferredBookoutCategoryId] [int] NOT NULL DEFAULT((9))
	
)
