if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[AdditionalInfoCategories]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[AdditionalInfoCategories]
GO
/****** Object:  Table [builder].[AdditionalInfoCategories]    Script Date: 11/10/2008 07:46:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[AdditionalInfoCategories](
	[infoBuilderDisplay] [varchar](80) NOT NULL,
	[infoId] [int] IDENTITY(1,1) NOT NULL,
	[businessUnitID] [int] NOT NULL,
	[infoAdDisplay] [varchar](80) NOT NULL CONSTRAINT [DF_AdditionalInfoCategories_infoAdDisplay]  DEFAULT (''),
	[enabled] [int] NOT NULL DEFAULT((1)),
 CONSTRAINT [PK_AdditionalInfoCategories] PRIMARY KEY CLUSTERED 
(
	[infoId] ASC
)
)

GO
SET ANSI_PADDING OFF