if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleVideos]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[VehicleVideos]
GO
/****** Object:  Table [builder].[VehicleVideos]    Script Date: 10/26/2008 12:55:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[VehicleVideos](
	[inventoryID] [int] NOT NULL,
	[businessUnitID] [int] NOT NULL,
	[dateCreated] [datetime] NOT NULL,
	[basicURL] [varchar](100) NOT NULL,
	[enhancedURL] [varchar](100) NOT NULL,
 CONSTRAINT [PK_VehicleVideos] PRIMARY KEY CLUSTERED 
(
	[inventoryID] ASC,
	[businessUnitID] ASC,
	[dateCreated] ASC
)
)

GO
SET ANSI_PADDING OFF