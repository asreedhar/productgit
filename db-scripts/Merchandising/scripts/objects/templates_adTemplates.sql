if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[AdTemplates]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [templates].[AdTemplates]
GO
/****** Object:  Table [templates].[AdTemplates]    Script Date: 10/26/2008 13:02:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [templates].[AdTemplates](
	[templateID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL CONSTRAINT [DF_AdTemplates_name]  DEFAULT (''),
	[businessUnitID] [int] NOT NULL,
	[vehicleProfileId] [int] NULL,
	[parent] [int] NULL,
 CONSTRAINT [PK_AdTemplates] PRIMARY KEY CLUSTERED 
(
	[templateID] ASC
)
)

GO
SET ANSI_PADDING OFF