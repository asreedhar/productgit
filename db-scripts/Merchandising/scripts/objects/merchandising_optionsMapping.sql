if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[OptionsMapping]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [merchandising].[OptionsMapping]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [merchandising].[OptionsMapping](
	
	[description] [varchar](200) NOT NULL,
	[optionid] [int] NULL,
	[providerid] [tinyint] NULL,
	[chromeCategoryId] [int] NULL,
	[chromeDescription] [varchar](200) NULL,
	[matchPattern] [varchar](200) NULL,
	
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF