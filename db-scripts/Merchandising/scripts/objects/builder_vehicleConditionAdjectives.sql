if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleConditionAdjectives]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[VehicleConditionAdjectives]
GO
/****** Object:  Table [builder].[VehicleConditionAdjectives]    Script Date: 11/06/2008 18:34:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[VehicleConditionAdjectives](
	[conditionId] [int] IDENTITY(1,1) NOT NULL,
	[ConditionDescription] [varchar](50) NOT NULL,
	[ConditionLevel] [int] NOT NULL,
	[businessUnitID] [int] NOT NULL
)

GO
SET ANSI_PADDING OFF