if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[AdTemplateItems]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [templates].[AdTemplateItems]
GO
/****** Object:  Table [templates].[AdTemplateItems]    Script Date: 10/26/2008 13:02:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [templates].[AdTemplateItems](
	[templateID] [int] NOT NULL,
	[blurbID] [int] NOT NULL,
	[sequence] [int] NOT NULL,
	[priority] [int] NOT NULL,
	[required] [tinyint] NOT NULL,
	[parentID] [int] NULL,
 CONSTRAINT [PK_AdTemplateItems] PRIMARY KEY CLUSTERED 
(
	[templateID] ASC,
	[blurbID] ASC
)
)

GO
ALTER TABLE [templates].[AdTemplateItems]  WITH CHECK ADD  CONSTRAINT [FK_AdTemplateItems_AdTemplates] FOREIGN KEY([templateID])
REFERENCES [templates].[AdTemplates] ([templateID])
GO
ALTER TABLE [templates].[AdTemplateItems] CHECK CONSTRAINT [FK_AdTemplateItems_AdTemplates]
GO
ALTER TABLE [templates].[AdTemplateItems]  WITH CHECK ADD  CONSTRAINT [FK_AdTemplateItems_BlurbTemplates] FOREIGN KEY([blurbID])
REFERENCES [templates].[BlurbTemplates] ([blurbId])
GO
ALTER TABLE [templates].[AdTemplateItems] CHECK CONSTRAINT [FK_AdTemplateItems_BlurbTemplates]