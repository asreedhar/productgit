if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[AlertingPreferences]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[AlertingPreferences]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [settings].[AlertingPreferences](
	[alertingId] [int] identity(1,1),
	[businessUnitId] [int] NOT NULL,
	[memberId] [int] NOT NULL,
	[daysToEquipmentComplete] [int] NOT NULL,
	[daysToFirstPhoto] [int] NOT NULL,
	[daysToPhotosComplete] [int] NOT NULL,
	[daysToPricingComplete] [int] NOT NULL,
	[daysToApproval] [int] NOT NULL,
	[maxDaysToApprovedAfterPending] [int] NOT NULL,
	[daysToIncludeForActivityCalculations] [int] NOT NULL
)
