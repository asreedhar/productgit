if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[OptionsConfiguration]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[OptionsConfiguration]
GO

/****** Object:  Table [builder].[OptionsConfiguration]    Script Date: 10/26/2008 12:53:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[OptionsConfiguration](
	[businessUnitID] [int] NOT NULL,
	[stockNumber] [varchar](50) NULL,
	[inventoryId] [int] NOT NULL,
	[isLegacy] [bit] NOT NULL DEFAULT((0)),
	[chromeStyleID] [int] NOT NULL CONSTRAINT [DF_OptionsConfiguration_chromeStyleID] DEFAULT ((-1)),
	[selectedTemplateID] [int] NOT NULL CONSTRAINT [DF_OptionsConfiguration_selectedTemplateID] DEFAULT ((-1)),
	[lotLocationID] int NULL,
	[afterMarketTrim] [varchar](50) NOT NULL CONSTRAINT [DF_OptionsConfiguration_afterMarketTrim] DEFAULT (''),
	[vehicleCondition] [varchar](50) NOT NULL CONSTRAINT [DF_OptionsConfiguration_vehicleCondition] DEFAULT (''),
	[tireTread] [int] NULL,
	[reconditioningText] [varchar](250) NOT NULL DEFAULT(''),
	[reconditioningValue] decimal(9,2) NOT NULL DEFAULT(0),
	[extColor1] [varchar](50) NOT NULL CONSTRAINT [DF_OptionsConfiguration_extColor1] DEFAULT (''),
	[extColor2] [varchar](50) NOT NULL CONSTRAINT [DF_OptionsConfiguration_extColor2] DEFAULT (''),
	[intColor] [varchar](50) NOT NULL CONSTRAINT [DF_OptionsConfiguration_intColor] DEFAULT (''),
	[certificationTypeId] [int] NULL CONSTRAINT [DF_OptionsConfiguration_certificationType]  DEFAULT ((-1)),
	[specialId] [int] NULL DEFAULT((-1)),
	[afterMarketCategoryString] [varchar](max) NOT NULL CONSTRAINT [DF_OptionsConfiguration_afterMarketCategoryString]  DEFAULT (''),
	[additionalInfoText] [varchar](max) NOT NULL CONSTRAINT [DF_OptionsConfiguration_additionalInfoText]  DEFAULT (''),	
	[postingStatus] [smallint] NOT NULL CONSTRAINT [DF_OptionsConfiguration_postingStatus]  DEFAULT ((0)),
	[onlineFlag] [smallint] NOT NULL CONSTRAINT [DF_OptionsConfiguration_onlineFlag] DEFAULT((0)),
	[createdOn] [datetime] NOT NULL CONSTRAINT [DF_OptionsConfiguration_dateSaved]  DEFAULT (getdate()),
	[createdBy] [varchar](30) NOT NULL DEFAULT(''),
	[lastUpdatedOn] [datetime] NOT NULL DEFAULT(getdate()),
	[lastUpdatedBy] [varchar](30) NOT NULL DEFAULT(''),
	[version] [timestamp] NOT NULL,
CONSTRAINT [PK_OptionsConfiguration] PRIMARY KEY CLUSTERED 
(
	[businessUnitID] ASC,
	[inventoryID] ASC
)
)

GO
SET ANSI_PADDING OFF