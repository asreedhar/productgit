if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[cpoGroups]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[cpoGroups]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[cpoGroups](
	[cpoGroupId] [int] IDENTITY(1,1),
	[name] [varchar](30) NOT NULL
)

GO
SET ANSI_PADDING OFF