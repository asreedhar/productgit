if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehiclePhotos]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[VehiclePhotos]
GO
/****** Object:  Table [builder].[VehiclePhotos]    Script Date: 10/26/2008 12:55:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[VehiclePhotos](
	[businessUnitID] [int] NOT NULL,
	[inventoryID] [int] NOT NULL,
	[imageSequenceNumber] [int] NOT NULL,
	[imageComment] [varchar](50) NOT NULL,
	[imageUrl] [varchar](100) NOT NULL,
	[photoItemID] [int] IDENTITY(1,1) NOT NULL,
	[photoTypeID] [int] NOT NULL CONSTRAINT [DF_VehiclePhotos_photoTypeID]  DEFAULT ((1)),
	[imageThumbnailUrl] [varchar](100) NOT NULL CONSTRAINT [DF_VehiclePhotos_imageThumbnailUrl]  DEFAULT (''),
	[includeInVideo] [tinyint] NOT NULL CONSTRAINT [DF_VehiclePhotos_includeInVideo]  DEFAULT ((0)),
	[photoFileCreated] [datetime] NOT NULL,
	[pendingReorder] [bit] NOT NULL DEFAULT((0)),
	[photoActive] [tinyint] NOT NULL CONSTRAINT [DF_VehiclePhotos_photoActive]  DEFAULT ((0)),
	[dateInactive] [datetime] NULL,
 CONSTRAINT [PK_VehiclePhotos_1] PRIMARY KEY CLUSTERED 
(
	[photoItemID] ASC
)
)

GO
SET ANSI_PADDING OFF