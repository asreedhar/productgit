if exists (select * from dbo.sysobjects where id = object_id(N'[postings].[RawVehicleMetrics]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [postings].[RawVehicleMetrics]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [postings].[RawVehicleMetrics](
	[metricID] [int] IDENTITY(1,1) NOT NULL,
	[sourceID] [int] NOT NULL,
	[businessUnitID] [int] NOT NULL CONSTRAINT [DF_rawVehicleMetrics_businessUnitID]  DEFAULT ((-1)),
	[vin] [varchar](17) NOT NULL CONSTRAINT [DF_ravVehicleMetrics_vin]  DEFAULT (''),
	[stockNumber] [varchar](17) NOT NULL CONSTRAINT [DF_rawVehicleMetrics_stockNumber]  DEFAULT (''),
	[startDate] [datetime] NULL,
	[endDate] [datetime] NULL,
	[adsPrinted] [int] NOT NULL CONSTRAINT [DF_rawVehicleMetrics_adsPrinted]  DEFAULT ((0)),
	[currentPrice] [int] NOT NULL CONSTRAINT [DF_rawVehicleMetrics_currentPrice]  DEFAULT ((0)),
	[mktAvgPrice] [int] NOT NULL CONSTRAINT [DF_rawVehicleMetrics_mktAvgPrice]  DEFAULT ((0)),
	[daysLive] [int] NOT NULL CONSTRAINT [DF_rawVehicleMetrics_daysLive]  DEFAULT ((0)),
	[searchViewed] [int] NOT NULL CONSTRAINT [DF_rawVehicleMetrics_searchViewed]  DEFAULT ((0)),
	[detailsViewed] [int] NOT NULL CONSTRAINT [DF_rawVehicleMetrics_detailsViewed]  DEFAULT ((0)),
	[emails] [int] NOT NULL CONSTRAINT [DF_rawVehicleMetrics_emails]  DEFAULT ((0)),
	[mapsViewed] [int] NOT NULL CONSTRAINT [DF_rawVehicleMetrics_mapsViewed]  DEFAULT ((0)),
	[websiteClickthrus] [int] NOT NULL CONSTRAINT [DF_rawVehicleMetrics_websiteClickthrus]  DEFAULT ((0)),
 CONSTRAINT [PK_rawVehicleMetrics] PRIMARY KEY CLUSTERED 
(
	[metricID] ASC
)
)

GO
SET ANSI_PADDING OFF