if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[dealerCertificationConfiguration]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[dealerCertificationConfiguration]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[dealerCertificationConfiguration](
	[businessUnitId] [int] NOT NULL,
	[certificationTypeId] int NOT NULL,
	[certificationFeatureId] int NOT NULL,
	[featureDisplayFlag] bit NOT NULL
)

GO
SET ANSI_PADDING OFF