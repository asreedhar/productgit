if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[certificationFeatures]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[certificationFeatures]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[certificationFeatures](
	[certificationFeatureId] [int] IDENTITY(1,1),
	[businessUnitId] [int] NOT NULL,
	[certificationTypeId] [int] NOT NULL,
	[certificationFeatureTypeId] [int] NOT NULL,
	[description] [varchar](300) NOT NULL,
	[active] [bit] NOT NULL DEFAULT((1)),
	CONSTRAINT [PK_CertFeatures] PRIMARY KEY CLUSTERED 
	(
		[certificationTypeId] ASC,
		[certificationFeatureId] ASC
	)
)

GO
SET ANSI_PADDING OFF