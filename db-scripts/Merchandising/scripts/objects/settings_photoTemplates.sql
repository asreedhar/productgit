if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[PhotoTemplates]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[PhotoTemplates]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[PhotoTemplates](
	[photoTemplateItemId] [int] IDENTITY(1,1),
	[businessUnitId] [int] NOT NULL,
	[destinationId] [int] NOT NULL, --the template purpose (could be -1 for autoupload, -2 for video or a positive number indicating destination)
	[photoTemplateItemTypeId] INT NOT NULL DEFAULT((1)),
	[photoTemplateItemParameter] [int] NOT NULL,  --requiredPhotoId if requiredPhoto, dealerPhotoId if dealerPhoto, 
	[sequenceNumber] [int] NOT NULL,
	
	
	CONSTRAINT [PK_PhotoTemplateItems] PRIMARY KEY
	(
		[photoTemplateItemId] ASC
		--[destinationId] ASC,
		--[photoTemplateItemTypeId] ASC,
		--[photoTemplateItemParameter] ASC
		
	)
	
)

GO
SET ANSI_PADDING OFF