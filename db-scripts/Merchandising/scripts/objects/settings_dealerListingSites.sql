if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[DealerListingSites]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[DealerListingSites]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [settings].[DealerListingSites](
	[destinationID] [int] NOT NULL,
	[activeFlag] [bit] NOT NULL CONSTRAINT [DF_AdvertScheduling_activeFlag]  DEFAULT ((0)),
	[pushFrequency] [int] NOT NULL,
	[businessUnitID] [int] NOT NULL,
	[sendPhotos] [bit] NOT NULL,
	[sendOptions] [bit] NOT NULL,
	[sendDescription] [bit] NOT NULL,
	[sendPricing] [bit] NOT NULL,
	[sendVideos] [bit] NOT NULL,
	[sendNewCars] [bit] NOT NULL,
	[sendUsedCars] [bit] NOT NULL,
	[userName] [varchar](50) NOT NULL DEFAULT(''),
	[password] [varchar](50) NOT NULL DEFAULT(''), 
	[releaseEnabled] [int] NOT NULL DEFAULT((0)),
	[collectionEnabled] [int] NOT NULL DEFAULT((0)),
	[lastActiveInventoryCollection] datetime NULL,
	[lastSoldInventoryCollection] datetime NULL,
	
 CONSTRAINT [PK_AdvertScheduling] PRIMARY KEY CLUSTERED 
(
	[destinationID] ASC,
	[businessUnitID] ASC
)
)
