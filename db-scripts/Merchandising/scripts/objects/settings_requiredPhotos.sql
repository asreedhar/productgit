if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[RequiredPhotos]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[RequiredPhotos]
GO

/****** Object:  Table [settings].[RequiredPhotos]    Script Date: 10/26/2008 13:01:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[RequiredPhotos](
	[requiredPhotoId] [int] IDENTITY(1,1),
	[photoTitle] [varchar](30) NOT NULL,
	[photoTypeId] [int] NOT NULL,
	[businessUnitId] [int] NOT NULL,
	[sequenceNumber] [int] NOT NULL CONSTRAINT [DF_requiredPhotos_sequenceNumber]  DEFAULT ((999)),
	[useInVideo] [tinyint] NOT NULL CONSTRAINT [DF_requiredPhotos_useInVideo]  DEFAULT ((0)),
	[autoUploadSequenceNumber] [int] NULL,
	[storyBoardIconUrl] [varchar](50) NOT NULL
)

GO
SET ANSI_PADDING OFF