if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[financingSpecials]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[financingSpecials]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[financingSpecials](
	[specialId] [int] IDENTITY(1,1),
	[ownerEntityTypeId] [int] NOT NULL DEFAULT(1),
	[ownerEntityId] [int] NOT NULL,
	[title] [varchar](50) NOT NULL,
	[description] [varchar](300) NOT NULL,
	[expiresOn] datetime NULL,
	[vehicleProfileId] int NULL,
	[onlyCertifieds] bit NOT NULL DEFAULT(0)
	
)

GO
SET ANSI_PADDING OFF