if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[PhotoTemplateItemTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[PhotoTemplateItemTypes]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[PhotoTemplateItemTypes](
	
	[photoTemplateItemTypeId] [int] NOT NULL,
	[description] [varchar](30) NOT NULL, 
	
	CONSTRAINT [PK_PhotoTemplateItemTypes] PRIMARY KEY CLUSTERED 
	(
		[photoTemplateItemTypeId] ASC
	)
	
)

GO
SET ANSI_PADDING OFF