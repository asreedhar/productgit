if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[EdtDestinations]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[EdtDestinations]
GO

/****** Object:  Table [settings].[EdtDestinations]    Script Date: 10/26/2008 12:59:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[EdtDestinations](
	[destinationID] [int] IDENTITY(1,1) NOT NULL,
	[description] [varchar](50) NOT NULL,
	[maxDescriptionLength] [int] NOT NULL,
	[maxPhotoCount] [int] NOT NULL,
	[acceptsDescriptions] [tinyint] NOT NULL,
	[acceptsOptions] [tinyint] NOT NULL CONSTRAINT [DF_EdtDestinations_acceptsOptions]  DEFAULT ((1)),
	[acceptsPhotos] [tinyint] NOT NULL,
	[acceptsVideos] [tinyint] NOT NULL,
 CONSTRAINT [PK_EdtDestinations] PRIMARY KEY CLUSTERED 
(
	[destinationID] ASC
)
)

GO
SET ANSI_PADDING OFF