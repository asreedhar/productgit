
create table #cats (
categoryid int,
[testDescription] varchar(100),
[originalDescription] varchar(100) 
)

create table #edrs (
categoryid int,
displaypriority int 
)
insert into #edrs (categoryid, displaypriority) 
select categoryid, displaypriority 
from merchandising.settings.equipmentdisplayrankings
where businessunitid = 100150
order by displayPriority asc

insert into #cats (categoryid, [testDescription], [originalDescription])
select ed.categoryid, userfriendlyname, userfriendlyname
from #edrs ed
left join vehiclecatalog.chrome.categories ct
on ed.categoryid = ct.categoryid
where ct.countrycode = 1 and userfriendlyname <> ''


declare @i int
set @i = 1


------NEED A WAY TO COUNT MATCHING WORDS, NOT JUST TRIM OFF THE END OF WORDS
while @i <= 2
begin
	update om
	set chromeCategoryId = ct.categoryid, chromeDescription = ct.originalDescription, matchPattern = ct.testDescription
	from merchandising.optionsMapping om
	left join #cats ct
	on om.description like '% '+ct.testDescription+' %'
		OR om.description like ct.testDescription + ' %'
		OR om.description like '% ' + ct.testDescription
		OR om.description = ct.testDescription
	where chromecategoryid is null

	--remove everything after last underscore
	update #cats
	--set [testDescription] = substring([testDescription],1,len([testDescription])-charindex(' ',reverse([testDescription])))
	set [testDescription] = substring([testDescription],1,len([testDescription])-1)
	
	--delete from #cats where len([testDescription]) <= 2
	select * from #cats

	select @i = @i+1
end
drop table #cats
drop table #edrs 