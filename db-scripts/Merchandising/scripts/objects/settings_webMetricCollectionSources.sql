if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[WebMetricCollectionSources]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[WebMetricCollectionSources]
GO

/****** Object:  Table [settings].[WebMetricCollectionSources]    Script Date: 10/26/2008 13:01:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[WebMetricCollectionSources](
	[sourceID] [int] NOT NULL,
	[description] [varchar](50) NOT NULL,
	[hasSearchData] [tinyint] NOT NULL CONSTRAINT [DF_WebMetricCollectionSources_hasSearchData]  DEFAULT ((0)),
	[hasClickData] [tinyint] NOT NULL CONSTRAINT [DF_WebMetricCollectionSources_hasClickData]  DEFAULT ((0)),
	[hasActivityData] [tinyint] NOT NULL CONSTRAINT [DF_WebMetricCollectionSources_hasActivityData]  DEFAULT ((0)),
	[hasSoldData] [tinyint] NOT NULL CONSTRAINT [DF_WebMetricCollectionSources_hasSoldData]  DEFAULT ((0)),
	[hasActiveInventoryData] [tinyint] NOT NULL CONSTRAINT [DF_WebMetricCollectionSources_hasActiveInventoryData]  DEFAULT ((0)),
 CONSTRAINT [PK_WebMetricCollectionSources] PRIMARY KEY CLUSTERED 
(
	[sourceID] ASC
)
)

GO
SET ANSI_PADDING OFF