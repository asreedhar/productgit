if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[LoaderNotes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[LoaderNotes]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[LoaderNotes](
	
	[noteId] [int] IDENTITY(1,1) NOT NULL,
	[businessUnitID] [int] NOT NULL,
	[inventoryID] int NOT NULL,
	[noteText] [varchar](500) NOT NULL DEFAULT(('')),
	[writtenOn] [datetime] NOT NULL,
	[writtenBy] [varchar](30) NOT NULL,
	
 CONSTRAINT [PK_LoaderNotes] PRIMARY KEY CLUSTERED 
(
	[inventoryId] ASC,
	[noteId] ASC
)
)

GO
SET ANSI_PADDING OFF