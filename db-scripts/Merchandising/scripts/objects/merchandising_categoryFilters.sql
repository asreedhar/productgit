if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[CategoryFilters]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [merchandising].[CategoryFilters]
GO

/****** Object:  Table [merchandising].[CategoryFilters]    Script Date: 10/26/2008 13:06:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [merchandising].[CategoryFilters](
	[categoryID] [int] NOT NULL CONSTRAINT [DF_categoryFilters_categoryID]  DEFAULT ((-1)),
	[categoryDescription] [varchar](50) NOT NULL,
	[header] [varchar](50) NOT NULL CONSTRAINT [DF_categoryFilters_header]  DEFAULT (''),
	[businessUnitID] [int] NOT NULL CONSTRAINT [DF_categoryFilters_businessUnitID]  DEFAULT ((0)),
 CONSTRAINT [PK_categoryFilters] PRIMARY KEY CLUSTERED 
(
	[categoryID] ASC,
	[businessUnitID] ASC
)
)

GO
SET ANSI_PADDING OFF