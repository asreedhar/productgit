if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleStatusTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[VehicleStatusTypes]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[VehicleStatusTypes](
	
	[statusTypeId] [int] NOT NULL,
	[statusType] [varchar](30) NOT NULL
	
)

GO
SET ANSI_PADDING OFF