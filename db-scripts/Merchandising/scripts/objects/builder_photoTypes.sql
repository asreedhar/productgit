if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[photoTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[photoTypes]
GO
/****** Object:  Table [builder].[photoTypes]    Script Date: 10/26/2008 12:54:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[photoTypes](
	[imageTypeId] [int] NOT NULL,
	[imageTypeDescription] [varchar](20) NOT NULL
)

GO
SET ANSI_PADDING OFF