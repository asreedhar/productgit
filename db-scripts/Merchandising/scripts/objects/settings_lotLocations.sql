if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[LotLocations]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[LotLocations]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[LotLocations](
	[businessUnitID] [int] NOT NULL,
	[lotLocationId] [int] identity(1,1),
	[lotLocation] [varchar](50) NOT NULL,
CONSTRAINT [PK_LotLocations] PRIMARY KEY CLUSTERED 
(
	[businessUnitID] ASC,
	[lotLocationID] ASC
)
)

GO
SET ANSI_PADDING OFF 