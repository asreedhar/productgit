if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[VehicleProfiles]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [templates].[VehicleProfiles]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [templates].[VehicleProfiles](
	[vehicleProfileID] [int] IDENTITY(1,1) NOT NULL,
	[businessUnitId] [int] NOT NULL,
	[title] [varchar](30) NOT NULL,
	[makeIDList] [varchar](max) NOT NULL DEFAULT (''),
	[modelList] [varchar](max) NOT NULL DEFAULT (''),
	[marketClassIdList] [varchar](max) NOT NULL DEFAULT (''),
	[startYear] [int] NULL,
	[endYear] [int] NULL,
	[minAge] [int] NULL,
	[maxAge] [int] NULL,
	[minPrice] [decimal](18, 0) NULL,
	[maxPrice] [decimal](18, 0) NULL,
 CONSTRAINT [PK_VehicleProfiles] PRIMARY KEY CLUSTERED 
(
	[vehicleProfileID] ASC,
	[businessUnitID] ASC
)
)

GO
SET ANSI_PADDING OFF
GO
