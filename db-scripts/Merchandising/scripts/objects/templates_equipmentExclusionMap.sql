if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[EquipmentExclusionMap]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [templates].[EquipmentExclusionMap]
GO

/****** Object:  Table [templates].[EquipmentExclusionMap]    Script Date: 10/26/2008 13:04:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [templates].[EquipmentExclusionMap](
	[displayCategoryID] [int] NOT NULL,
	[eliminateCategoryID] [int] NOT NULL
)
