if exists (select * from dbo.sysobjects where id = object_id(N'[postings].[Publications]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [postings].[publications]
GO
/****** Object:  Table [postings].[Publications]    Script Date: 10/26/2008 12:57:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [postings].[Publications](
	[postingID] [int] IDENTITY(1,1) NOT NULL,
	[businessUnitID] [int] NOT NULL,
	[inventoryId] [int] NOT NULL,
	[datePosted] [datetime] NOT NULL,
	[destinationID] [int] NOT NULL,
	[merchandisingDescription] [varchar](max) NOT NULL,
	[photoCount] [int] NOT NULL,
	[hasVideo] [tinyint] NOT NULL,
	[equipmentCount] [int] NOT NULL,
	[pricePosted] [int] NOT NULL,
	[templateID] [int] NOT NULL,
	[approvedByMemberLogin] [varchar](30) NOT NULL,
	[dateRemoved] [datetime] NULL,
	[expiresOn] [datetime] NULL
)

GO
SET ANSI_PADDING OFF