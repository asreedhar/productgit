if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[MemberMapping]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[MemberMapping]
GO

/****** Object:  Table [settings].[MemberMapping]    Script Date: 10/26/2008 13:00:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[MemberMapping](
	[merchandisingMemberLogin] [varchar](50) NOT NULL,
	[firstlookMemberId] [int] NOT NULL,
	[firstlookMemberLogin] [varchar](50) NOT NULL
)

GO
SET ANSI_PADDING OFF