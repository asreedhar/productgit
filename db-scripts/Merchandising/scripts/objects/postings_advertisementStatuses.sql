if exists (select * from dbo.sysobjects where id = object_id(N'[postings].[AdvertisementStatuses]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [postings].[AdvertisementStatuses]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [postings].[AdvertisementStatuses](
	[statusId] [int] NOT NULL,
	[statusDescription] [varchar](30) NOT NULL
)

GO
SET ANSI_PADDING OFF