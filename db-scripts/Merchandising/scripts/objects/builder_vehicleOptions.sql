if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleOptions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[VehicleOptions]
GO
/****** Object:  Table [builder].[VehicleOptions]    Script Date: 10/26/2008 12:54:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[VehicleOptions](
	[vehicleOptionID] [int] IDENTITY(1,1) NOT NULL,
	[businessUnitID] [int] NOT NULL,
	[inventoryID] [int] NOT NULL,
	[optionID] [int] NOT NULL,
	[optionCode] [varchar](20) NULL,
	[detailText] [varchar](300) NULL,
	[optionText] [varchar](300) NOT NULL,
	[isStandardEquipment] [bit] NOT NULL,
 CONSTRAINT [PK_VehicleOptions] PRIMARY KEY CLUSTERED 
(
	[vehicleOptionID] ASC
)
)

GO
SET ANSI_PADDING OFF