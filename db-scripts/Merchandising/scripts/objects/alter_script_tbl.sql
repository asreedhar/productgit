

/****** Object:  Table [dbo].[Alter_Script]    Script Date: 11/07/2008 09:59:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Alter_Script](
	[AlterScriptName] [varchar](100) NOT NULL,
	[ApplicationDate] [datetime] NOT NULL CONSTRAINT [DF_Alter_Script_ApplicationDate]  DEFAULT (getdate())
)

GO
SET ANSI_PADDING OFF