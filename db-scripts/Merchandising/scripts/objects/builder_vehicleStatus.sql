if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleStatus]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[VehicleStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[VehicleStatus](
	
	[statusId] [int] IDENTITY(1,1) NOT NULL,
	[businessUnitID] [int] NOT NULL,
	[inventoryID] int NOT NULL,
	[statusLevel] [int] NOT NULL DEFAULT((0)),
	[statusTypeId] [int] NOT NULL,
	[createdOn] [datetime] NOT NULL,
	[createdBy] [varchar](30) NOT NULL,
	[lastUpdatedOn] [datetime] NOT NULL,
	[lastUpdatedBy] [varchar](30) NOT NULL,
	[version] [timestamp] NOT NULL,
 CONSTRAINT [PK_VehicleStatus] PRIMARY KEY CLUSTERED 
(
	[inventoryId] ASC,
	[statusId] ASC
)
)

GO
SET ANSI_PADDING OFF