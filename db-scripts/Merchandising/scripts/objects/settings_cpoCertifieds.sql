if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[cpoCertifieds]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[cpoCertifieds]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[cpoCertifieds](
	[cpoGroupId] [int] NOT NULL,
	[certificationTypeId] int NOT NULL,
	CONSTRAINT [PK_cpoCertifieds] PRIMARY KEY CLUSTERED 
	(
		[cpoGroupId] ASC,
		[certificationTypeId] ASC
	) 
	
)

GO
SET ANSI_PADDING OFF