if exists (select * from dbo.sysobjects where id = object_id(N'[postings].[VehicleMetrics]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [postings].[VehicleMetrics]
GO

/****** Object:  Table [postings].[VehicleMetrics]    Script Date: 10/26/2008 12:58:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [postings].[VehicleMetrics](
	[metricID] [int] IDENTITY(1,1) NOT NULL,
	[sourceID] [int] NOT NULL,
	[businessUnitID] [int] NOT NULL CONSTRAINT [DF_vehicleMetrics_businessUnitID]  DEFAULT ((-1)),
	[vin] [varchar](17) NOT NULL CONSTRAINT [DF_vehicleMetrics_vin]  DEFAULT (''),
	[inventoryId] [int] NULL,
	[stockNumber] [varchar](17) NOT NULL CONSTRAINT [DF_vehicleMetrics_stockNumber]  DEFAULT (''),
	[startDate] [datetime] NULL,
	[adsPrinted] [int] NOT NULL CONSTRAINT [DF_vehicleMetrics_adsPrinted]  DEFAULT ((0)),
	[currentPrice] [int] NOT NULL CONSTRAINT [DF_vehicleMetrics_currentPrice]  DEFAULT ((0)),
	[mktAvgPrice] [int] NOT NULL CONSTRAINT [DF_vehicleMetrics_mktAvgPrice]  DEFAULT ((0)),
	[daysLive] [int] NOT NULL CONSTRAINT [DF_vehicleMetrics_daysLive]  DEFAULT ((0)),
	[searchViewed] [int] NOT NULL CONSTRAINT [DF_vehicleMetrics_searchViewed]  DEFAULT ((0)),
	[detailsViewed] [int] NOT NULL CONSTRAINT [DF_vehicleMetrics_detailsViewed]  DEFAULT ((0)),
	[emails] [int] NOT NULL CONSTRAINT [DF_vehicleMetrics_emails]  DEFAULT ((0)),
	[mapsViewed] [int] NOT NULL CONSTRAINT [DF_vehicleMetrics_mapsViewed]  DEFAULT ((0)),
	[websiteClickthrus] [int] NOT NULL CONSTRAINT [DF_vehicleMetrics_websiteClickthrus]  DEFAULT ((0)),
 CONSTRAINT [PK_vehicleMetrics] PRIMARY KEY CLUSTERED 
(
	[metricID] ASC
)
)

GO
SET ANSI_PADDING OFF