if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[manufacturerCertifications]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[manufacturerCertifications]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[manufacturerCertifications](
	[certificationTypeId] [int] identity(1,1),
	[name] [varchar](30)
)

GO
SET ANSI_PADDING OFF