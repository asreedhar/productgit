if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleConditions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[VehicleConditions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[VehicleConditions](
	[vehicleConditionID] [int] IDENTITY(1,1) NOT NULL,
	[businessUnitID] [int] NOT NULL,
	[inventoryID] [int] NOT NULL,
	[vehicleConditionTypeID] [int] NOT NULL,
	[conditionLevel] [int] NOT NULL,
	[description] [varchar](30) NOT NULL
 CONSTRAINT [PK_VehicleConditions] PRIMARY KEY CLUSTERED 
(
	[vehicleConditionID] ASC
)
)

GO
SET ANSI_PADDING OFF