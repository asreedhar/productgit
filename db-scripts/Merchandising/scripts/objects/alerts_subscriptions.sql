if exists (select * from dbo.sysobjects where id = object_id(N'[alerts].[Subscriptions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [alerts].[Subscriptions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [alerts].[Subscriptions](
	
	[reportId] [int] NOT NULL,
	[businessUnitId] [int] NOT NULL,
	[memberId] [int] NOT NULL,
	[memberLogin] [varchar](80) NOT NULL,
	[emailAddress] [varchar](100) NOT NULL,
	[dayOfWeek] [int] NOT NULL,
	[timeOfDay] [int] NOT NULL
	
	
)

GO
SET ANSI_PADDING OFF