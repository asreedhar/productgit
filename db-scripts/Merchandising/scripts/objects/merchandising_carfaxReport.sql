if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[CarfaxReport]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [merchandising].[CarfaxReport]
GO
/****** Object:  Table [merchandising].[CarfaxReport]    Script Date: 10/26/2008 13:44:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [merchandising].[carfaxReport](
	[vin] [varchar](50) NOT NULL,
	[Problem] [varchar](50) NULL,
	[OwnershipText] [varchar](50) NULL,
	[BBGText] [varchar](50) NULL,
	[TotalLossText] [varchar](50) NULL,
	[FrameDamageText] [varchar](50) NULL,
	[AirbagDeploymentText] [varchar](50) NULL,
	[OdometerRollbackText] [varchar](50) NULL,
	[AccidentIndicatorsText] [varchar](50) NULL,
	[ManufacturerRecallText] [varchar](50) NULL,
	[expiresOn] [datetime] NOT NULL,
 CONSTRAINT [PK_carfaxReport] PRIMARY KEY CLUSTERED 
(
	[vin] ASC
)
)

GO
SET ANSI_PADDING OFF