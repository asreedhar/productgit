if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[GasMileagePreferences]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[GasMileagePreferences]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [settings].[GasMileagePreferences](
	[businessUnitId] [int] NOT NULL,
	[segmentId] [int] NOT NULL,
	[goodHighwayGasMileage] [int] NOT NULL,
	[goodCityGasMileage] [int] NOT NULL
	
)
