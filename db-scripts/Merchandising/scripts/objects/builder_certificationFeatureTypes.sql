if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[certificationFeatureTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[certificationFeatureTypes]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[certificationFeatureTypes](
	[certificationFeatureTypeId] [int] NOT NULL,
	[title] [varchar](30) NOT NULL
	
)

GO
SET ANSI_PADDING OFF