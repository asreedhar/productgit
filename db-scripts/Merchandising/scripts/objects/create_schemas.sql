CREATE SCHEMA [builder]
GO
GRANT SELECT ON SCHEMA ::  [builder] TO MerchandisingUser
GO

CREATE SCHEMA [chrome]
GO
GRANT SELECT ON SCHEMA ::  [chrome] TO MerchandisingUser
GO

CREATE SCHEMA [merchandising]
GO
GRANT SELECT ON SCHEMA ::  [merchandising] TO MerchandisingUser
GO 

CREATE SCHEMA [postings]
GO
GRANT SELECT ON SCHEMA ::  [postings] TO MerchandisingUser
GO

CREATE SCHEMA [settings]
GO
GRANT SELECT ON SCHEMA ::  [settings] TO MerchandisingUser
GO

CREATE SCHEMA [templates]
GO
GRANT SELECT ON SCHEMA ::  [templates] TO MerchandisingUser
GO

CREATE SCHEMA [Interface]
GO
GRANT SELECT ON SCHEMA ::  [Interface] TO MerchandisingUser
GO

CREATE SCHEMA [alerts]
GO
GRANT SELECT ON SCHEMA ::  [alerts] TO MerchandisingUser
GO

CREATE SCHEMA [audit]
GO
GRANT SELECT ON SCHEMA ::  [audit] TO MerchandisingUser
GO

CREATE SCHEMA [staging]
GO
GRANT SELECT ON SCHEMA ::  [builder] TO MerchandisingUser
GO