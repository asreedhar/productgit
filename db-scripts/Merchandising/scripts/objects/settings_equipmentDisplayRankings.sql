if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[EquipmentDisplayRankings]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[EquipmentDisplayRankings]
GO

/****** Object:  Table [settings].[EquipmentDisplayRankings]    Script Date: 10/26/2008 13:00:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[EquipmentDisplayRankings](
	[rankingID] [int] IDENTITY(1,1) NOT NULL,
	[businessUnitID] [int] NOT NULL,
	[categoryID] [int] NOT NULL,
	[displayPriority] [int] NOT NULL CONSTRAINT [DF_EquipmentDisplayRankings_displayPriority]  DEFAULT ((0)),
	[tierNumber] [int] NOT NULL CONSTRAINT [DF_EquipmentDisplayRankings_tierNumber]  DEFAULT ((2)),
	[typeID] [int] NOT NULL CONSTRAINT [DF_EquipmentDisplayRankings_typeID]  DEFAULT ((0)),
	[categoryDesc] [varchar](80) NOT NULL,
	[displayDescription] [varchar](80) NOT NULL,
	[isSticky] [bit] NOT NULL DEFAULT((0)),
 CONSTRAINT [PK_EquipmentDisplayRankings] PRIMARY KEY CLUSTERED 
(
	[rankingID] ASC
)
)

GO
SET ANSI_PADDING OFF