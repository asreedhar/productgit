if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[StandardEquipmentTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [merchandising].[StandardEquipmentTypes]
GO

/****** Object:  Table [merchandising].[StandardEquipmentTypes]    Script Date: 10/26/2008 15:23:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [merchandising].[StandardEquipmentTypes](
	[standardEquipmentType] [int] NOT NULL,
	[standardEquipmentTypeName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_StandardEquipmentTypes] PRIMARY KEY CLUSTERED 
(
	[standardEquipmentType] ASC
)
)

GO
SET ANSI_PADDING OFF