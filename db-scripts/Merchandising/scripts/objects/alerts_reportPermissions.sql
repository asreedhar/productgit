if exists (select * from dbo.sysobjects where id = object_id(N'[alerts].[ReportPermissions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [alerts].[ReportPermissions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [alerts].[ReportPermissions](
	
	[reportId] [int] NOT NULL,
	[roleName] [nvarchar](256) NOT NULL
	
)

GO
SET ANSI_PADDING OFF