if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[DealerCustomImages]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[DealerCustomImages]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[DealerCustomImages](
	
	[dealerCustomImageId] [int] IDENTITY(1,1),
	[businessUnitId] [int] NOT NULL,
	[title] [varchar](20) NOT NULL, 
	[imageUrl] [varchar](100) NOT NULL,
	
	CONSTRAINT [PK_DealerCustomImage] PRIMARY KEY CLUSTERED 
	(
		[dealerCustomImageId] ASC
	)
	
)

GO
SET ANSI_PADDING OFF