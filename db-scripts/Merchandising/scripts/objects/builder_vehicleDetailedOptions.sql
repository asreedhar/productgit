if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleDetailedOptions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[VehicleDetailedOptions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[VehicleDetailedOptions](
	[vehicleDetailedOptionID] [int] IDENTITY(1,1) NOT NULL,
	[businessUnitID] [int] NOT NULL,
	[inventoryID] [int] NOT NULL,
	[optionCode] [varchar](20) NULL,
	[detailText] [varchar](300) NULL,
	[optionText] [varchar](300) NOT NULL,
	[categoryList] [varchar](300) NOT NULL,
	[isStandardEquipment] [bit] NOT NULL,
 CONSTRAINT [PK_VehicleDetailedOptions] PRIMARY KEY CLUSTERED 
(
	[vehicleDetailedOptionID] ASC
)
)

GO
SET ANSI_PADDING OFF