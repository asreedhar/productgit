/*
PSEUDOCODE FOR GETAUTO PHOTO PROCESSING

NEED: inventoryId, VIN, GetAuto photo code for all active inventory of getauto dealers

SELECT ALL DEALERS USING THE GETAUTO PHOTO FEED

SELECT ALL ACTIVE INVENTORY FOR THESE DEALERS INNER JOINED TO MarketStaging.GetAuto.Vehicle Photos ON THE VIN
WHERE the photos column is not null or empty

--Now we have all active inventory that actually have a photo...

INSERT INTO IMT.dbo.Photos 
SELECT the photoUrl FROM ACTIVE INVENTORY JOIN ConstructGetAutoPhotoUrls(MarketStaging.GetAuto.Vehicle Photos)
--Joining on the VIN
--Use normal photos (not thumbnail or supersize)

--EXPLODE THE URLS TO INSERT ONE ROW FOR EVERY URL
imageUrl = url, originalImageUrl, photoStatusCode = remote, isPrimaryPhoto = 0, 
--set first photo to primary?
--how to account for photos already present in the database?
Could use left join on current photos on the originalUrl WHERE there is no match to determine which to insert


*/ 

SELECT inventoryId, url FROM (
SELECT inv.inventoryId, veh.VIN, ms.photos
FROM FLDW.inventoryActive inv
JOIN imt.dbo.tbl_vehicle veh
ON veh.vehicleId = inv.vehicleId
JOIN MarketStaging.GetAuto.Vehicle gv
ON gv.VIN = veh.VIN
WHERE photos > '') as activePhotos
JOIN ConstructGetAutoPhotoUrls(photos) pu
ON 