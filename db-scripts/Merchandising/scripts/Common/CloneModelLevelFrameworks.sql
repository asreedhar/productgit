set nocount on

/*
	WARNING! this script must delete VehicleProfiles, AdTemplates, and AdTemplate items to work correctly.  That means
	if a dealer has made a custom edit to a framework/blurb, the changes will be lost.
	
	
	WARNING WARNING WARNING!!!!!!  this script will destroy data if the business unit from which you want to clone 
	also exists in the list of businesses to clone TO!!!!!!!
	
	Check the templateId list, make sure it only comes from one distinct business unit, and then make sure that business unit
	DOES NOT EXIST in the CLONE TO list!!!!!!!
	
	
	
	!!!!!!!
	
	
	
	
	
	this script will clone templates, but it must be edited to properly populate the
	
		@stores table variable with a list of stores to clone to
		@templates table with a list of pre-existing templateids to copy
		
	this script is smart enough to not duplicate templates, in other words, if templateID 10 is in the @templates list,
	and it already exists for businessUnit 165900 in adTemplates as evidenced by having a record in there for that businessUnitID
	and that SourceTemplateId (10), it will still function properly.
	
	in other words, this script should always clone the @templates to the other @stores, even if they already exist.
	
	once the templates are created, it will also create the associated AdTemplateItems (by deleteing any that already exist and then
	inserting the ones associated with the original @templates list)
	
	this script also deep copies the VehicleProfile records.
	
*/

declare @stores table (BusinessUnitID int not null)
declare @templates table (templateID int not null)


insert @stores (BusinessUnitID)
select distinct BusinessUnitID
from IMT.dbo.BusinessUnit
where
	BusinessUnitTypeID = 4
	and	BusinessUnitCode in (
		 'F.H.DAIL02 '
		, 'TOMBELLC01'
		, 'DIVERCHE02'
		, 'HEYWARDA05'
	)


insert @templates (templateID)
select templateID from Merchandising.templates.AdTemplates where templateID in
(
	select templateId from
	(
		          select Brand = 'BMW', Framework = 'BMW 3 Series', templateId = 7212
		union all select Brand = 'BMW', Framework = 'BMW 3 Series New', templateId = 7548
		union all select Brand = 'BMW', Framework = 'BMW 5 Series', templateId = 7229
		union all select Brand = 'BMW', Framework = 'BMW 5 Series New', templateId = 7566
		union all select Brand = 'BMW', Framework = 'BMW 7 Series', templateId = 7265
		union all select Brand = 'BMW', Framework = 'BMW 7 Series New', templateId = 7717
		union all select Brand = 'BMW', Framework = 'BMW X3', templateId = 7323
		union all select Brand = 'BMW', Framework = 'BMW X3 New', templateId = 7684
		union all select Brand = 'BMW', Framework = 'BMW X5', templateId = 7324
		union all select Brand = 'BMW', Framework = 'BMW X5 New', templateId = 7565
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Chrysler 200', templateId = 8630
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Chrysler 200 New', templateId = 8629
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Chrysler 300', templateId = 8643
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Chrysler 300 New', templateId = 8644
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Chrysler 300C', templateId = 8651
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Chrysler 300C New', templateId = 8652
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Chrysler Town & Country', templateId = 8627
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Chrysler Town & Country New', templateId = 8564
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Charger', templateId = 8854
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Charger New', templateId = 8855
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Durango', templateId = 8824
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Durango 2009-2006', templateId = 8826
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Durango New', templateId = 8825
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Grand Caravan', templateId = 8792
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Grand Caravan New', templateId = 8793
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Journey', templateId = 8845
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Journey New', templateId = 8852
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Nitro', templateId = 8858
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Dodge Nitro New', templateId = 8859
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Compass', templateId = 8748
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Compass New', templateId = 8749
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Grand Cherokee', templateId = 8697
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Grand Cherokee New', templateId = 8698
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Liberty', templateId = 8746
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Liberty New', templateId = 8747
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Patriot', templateId = 8699
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Patriot New', templateId = 8700
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Wrangler', templateId = 8681
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Wrangler New', templateId = 8682
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Wrangler Rubicon', templateId = 8683
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Wrangler Rubicon New', templateId = 8684
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Wrangler Unlimited', templateId = 8677
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Wrangler Unlimited New', templateId = 8678
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Wrangler Unlimited Rubico', templateId = 8679
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Jeep Wrangler Unlimited RubNew', templateId = 8680
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Ram Dakota', templateId = 9314
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Ram Dakota New', templateId = 9315
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Ram Truck', templateId = 9312
		union all select Brand = 'Chrysler Jeep Dodge', Framework = 'Ram Truck New', templateId = 9313
		union all select Brand = 'GM', Framework = 'Buick Enclave', templateId = 8458
		union all select Brand = 'GM', Framework = 'Buick Enclave New', templateId = 8459
		union all select Brand = 'GM', Framework = 'Buick LaCrosse', templateId = 8506
		union all select Brand = 'GM', Framework = 'Buick LaCrosse New', templateId = 8507
		union all select Brand = 'GM', Framework = 'Cadillac CTS', templateId = 8464
		union all select Brand = 'GM', Framework = 'Cadillac CTS New', templateId = 8465
		union all select Brand = 'GM', Framework = 'Cadillac Escalade', templateId = 8462
		union all select Brand = 'GM', Framework = 'Cadillac Escalade New', templateId = 8463
		union all select Brand = 'GM', Framework = 'Chevy Cruze', templateId = 8401
		union all select Brand = 'GM', Framework = 'Chevy Cruze New', templateId = 8402
		union all select Brand = 'GM', Framework = 'Chevy Malibu', templateId = 8382
		union all select Brand = 'GM', Framework = 'Chevy Malibu New', templateId = 8466
		union all select Brand = 'GM', Framework = 'Chevy Silverado', templateId = 8701
		union all select Brand = 'GM', Framework = 'Chevy Silverado New', templateId = 8702
		union all select Brand = 'GM', Framework = 'Chevy Suburban', templateId = 8496
		union all select Brand = 'GM', Framework = 'Chevy Suburban New', templateId = 8497
		union all select Brand = 'GM', Framework = 'Chevy Tahoe', templateId = 8403
		union all select Brand = 'GM', Framework = 'Chevy Tahoe New', templateId = 8404
		union all select Brand = 'GM', Framework = 'Chevy Traverse', templateId = 8456
		union all select Brand = 'GM', Framework = 'Chevy Traverse New', templateId = 8457
		union all select Brand = 'GM', Framework = 'GMC Acadia', templateId = 8460
		union all select Brand = 'GM', Framework = 'GMC Acadia New', templateId = 8461
		union all select Brand = 'GM', Framework = 'GMC Yukon', templateId = 8405
		union all select Brand = 'GM', Framework = 'GMC Yukon New', templateId = 8406
		union all select Brand = 'GM', Framework = 'GMC Yukon New Car', templateId = 8407
		union all select Brand = 'GM', Framework = 'GMC Yukon XL', templateId = 8498
		union all select Brand = 'GM', Framework = 'GMC Yukon XL New', templateId = 8499
		union all select Brand = 'Honda', Framework = 'Honda Accord', templateId = 7393
		union all select Brand = 'Honda', Framework = 'Honda Accord New', templateId = 7774
		union all select Brand = 'Honda', Framework = 'Honda Civic', templateId = 7392
		union all select Brand = 'Honda', Framework = 'Honda Civic New', templateId = 7693
		union all select Brand = 'Honda', Framework = 'Honda CR-V', templateId = 7466
		union all select Brand = 'Honda', Framework = 'Honda CR-V New', templateId = 7815
		union all select Brand = 'Honda', Framework = 'Honda Odyssey', templateId = 7416
		union all select Brand = 'Honda', Framework = 'Honda Odyssey New', templateId = 7806
		union all select Brand = 'Honda', Framework = 'Honda Pilot', templateId = 7432
		union all select Brand = 'Honda', Framework = 'Honda Pilot New', templateId = 7814
	) a
)

--- do not edit script below this line, only edit the stores and templates list above

set xact_abort on
set transaction isolation level serializable
begin transaction

declare
	@rows int,
	@businessUnitId int,
	@sourceTemplateId int,
	@vehicleProfileId int


declare @storeProfiles table
(
	BusinessUnitID int not null,
	sourceTemplateId int not null,
	vehicleProfileId int null,
	processed bit not null
	
)

insert @storeProfiles
(
	BusinessUnitID,
	sourceTemplateId,
	processed
)
select
	s.BusinessUnitID,
	t.templateID,
	0
from
	@stores s
	cross join @templates t


declare @vpToDelete table
(
	vehicleProfileID int
)

-- delete all VehicleProfile, AdTemplate, and AdTemplateItems for the new stores 
-- that are associated with the "master" templates we are about to clone.  these
-- records might not exist, but if they do this script will not correctly map the
-- deltas that may occur if a store only has a partial framework frmo the masters.

insert @vpToDelete
(
	vehicleProfileID
)
select
	vp.vehicleProfileID
from
	Merchandising.templates.VehicleProfiles vp
	join Merchandising.templates.AdTemplates at
		on vp.vehicleProfileID = at.vehicleProfileId
	join @storeProfiles sp
		on at.SourceTemplateId = sp.sourceTemplateId
		and at.businessUnitID = sp.BusinessUnitID
		and vp.businessUnitId = sp.BusinessUnitID
	
set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' pre-exising VehicleProfiles staged for delete.'

delete ati
from
	Merchandising.templates.AdTemplateItems ati
	join Merchandising.templates.AdTemplates at
		on ati.templateID = at.templateID
	join @storeProfiles sp
		on at.businessUnitID = sp.BusinessUnitID
		and at.SourceTemplateId = sp.sourceTemplateId

set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' pre-exising AdTemplateItems deleted.'

delete at
from
	Merchandising.templates.AdTemplates at
	join @storeProfiles sp
		on at.SourceTemplateId = sp.sourceTemplateId
		and at.businessUnitID = sp.BusinessUnitID

set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' pre-exising AdTemplates deleted.'

delete vp
from
	Merchandising.templates.VehicleProfiles vp
	join @vpToDelete d
		on vp.vehicleProfileID = d.vehicleProfileID

set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' pre-exising VehicleProfiles deleted.'
		

while (1=1)
begin
	select top 1
		@businessUnitId = BusinessUnitID,
		@sourceTemplateId = sourceTemplateId
	from @storeProfiles
	where processed = 0
	
	if (@@rowcount <> 1) break;
	
	insert Merchandising.templates.VehicleProfiles
	( 
		businessUnitId,
		title,
		makeIDList,
		modelList,
		marketClassIdList,
		startYear,
		endYear,
		minAge,
		maxAge,
		minPrice,
		maxPrice,
		certifiedStatusFilter,
		trimList,
		segmentIdList,
		vehicleType
	)
	select
		@businessUnitID,
		vp.title,
		vp.makeIDList,
		vp.modelList,
		vp.marketClassIdList,
		vp.startYear,
		vp.endYear,
		vp.minAge,
		vp.maxAge,
		vp.minPrice,
		vp.maxPrice,
		vp.certifiedStatusFilter,
		vp.trimList,
		vp.segmentIdList,
		vp.vehicleType	
	from
		Merchandising.templates.AdTemplates at
		join Merchandising.templates.VehicleProfiles vp
			on at.vehicleProfileId = vp.vehicleProfileID
	where at.templateID = @sourceTemplateId
	
	select @rows = @@rowcount, @vehicleProfileId = scope_identity()
	
	if (@rows > 1) raiserror('incorrect rows inserted into vehicle profiles!', 16, 1)
	
	update @storeProfiles set
		vehicleProfileId = @vehicleProfileId,
		processed = 1
	where
		BusinessUnitID = @businessUnitId
		and sourceTemplateId = @sourceTemplateId
	
	set @rows = @@rowcount
	if (@rows <> 1) raiserror('incorrect rows updated in local table!', 16, 1)
	
end	-- VehicleProfile insert loop


print ''
print ''

select @rows = count(*) from @storeProfiles
print convert(varchar(10), @rows) + ' new VehicleProfiles added.'




-- insert the new templates
insert Merchandising.templates.AdTemplates
(
	name,
	businessUnitID,
	vehicleProfileId,
	parent,
	active,
	SourceTemplateId
)
select
	at.name,
	sp.BusinessUnitID,
	sp.vehicleProfileId,
	at.parent,
	at.active,
	SourceTemplateId = at.templateID
from
	Merchandising.templates.AdTemplates at
	join @storeProfiles sp
		on at.templateID = sp.sourceTemplateId
	
set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' new templates added.'
	
	
	
	
	

-- ok go clone the AdTemplateItems tied to @templates list to the new @stores list
insert Merchandising.templates.AdTemplateItems
(
	templateID,
	blurbID,
	sequence,
	priority,
	required,
	parentID
)
select
	newTemplates.templateID,
	ati.blurbID,
	ati.sequence,
	ati.priority,
	ati.required,
	ati.parentID
from
	Merchandising.templates.AdTemplateItems ati
	join Merchandising.templates.AdTemplates at
		on ati.templateID = at.templateID
	join @templates t
		on at.templateID = t.templateID
	join Merchandising.templates.AdTemplates newTemplates
		on newTemplates.SourceTemplateId = at.templateID
	join @stores s
		on newTemplates.businessUnitID = s.BusinessUnitID


set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' new template items added.'

commit
set xact_abort off
set transaction isolation level read uncommitted



