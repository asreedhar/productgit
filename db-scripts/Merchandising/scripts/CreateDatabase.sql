--  EXECUTION  ----------------------------------------------------------------------------------------------

SET NOCOUNT on

IF db_id('__DATABASE__') is not null
    DROP DATABASE [__DATABASE__]

--  Create the database
CREATE DATABASE __DATABASE__
 on ( name       = '__DATABASE__'
     ,filename   = '__SQL_SERVER_DATA__\__DATABASE__.mdf'
     ,size       = 100MB
     ,filegrowth = 200MB)
 log on ( name       = '__DATABASE___log'
         ,filename   = '__SQL_SERVER_LOG__\__DATABASE___log.ldf'
         ,size       = 200MB
        ,filegrowth = 20%)

ALTER DATABASE __DATABASE__
SET
   auto_create_statistics ON
  ,auto_update_statistics ON
  ,recovery simple


GO

CREATE TABLE __DATABASE__.dbo.Alter_Script
 (
   AlterScriptName varchar(50) not null
  ,ApplicationDate datetime    not null 
    constraint DF_Alter_Script_ApplicationDate
     default (getdate())
 )

INSERT __DATABASE__.dbo.Alter_Script (AlterScriptName) values ('Database Created')
GO

USE __DATABASE__
GO