if object_id('settings.DealerListingSitesAudit') is not null
	drop trigger settings.DealerListingSitesAudit
go


create trigger settings.DealerListingSitesAudit on settings.DealerListingSites
for insert,update,delete
as

declare
	@type char(1),
	@user nvarchar(128),
	@stamp datetime
	

select
	@type = 
		case
			when exists(select * from inserted) and not exists(select * from deleted) then 'I'
			when exists(select * from inserted) and exists(select * from deleted) then 'U'
			else 'D'
		end,
	@user = suser_sname(),
	@stamp = getdate()


if @type in ('I', 'U')
	insert audit.DealerListingSites (actionType, actionDate, suser_sname, destinationID, activeFlag, pushFrequency, businessUnitID, sendPhotos, sendOptions, sendDescription, sendPricing, sendVideos, sendNewCars, sendUsedCars, userName, password, releaseEnabled, collectionEnabled, lastActiveInventoryCollection, lastSoldInventoryCollection, ListingSiteDealerId, dateLoginFailure, dateLoginFailureCleared)
	select @type, @stamp, @user, destinationID, activeFlag, pushFrequency, businessUnitID, sendPhotos, sendOptions, sendDescription, sendPricing, sendVideos, sendNewCars, sendUsedCars, userName, password, releaseEnabled, collectionEnabled, lastActiveInventoryCollection, lastSoldInventoryCollection, ListingSiteDealerId, dateLoginFailure, dateLoginFailureCleared
	from inserted
else
	insert audit.DealerListingSites (actionType, actionDate, suser_sname, destinationID, activeFlag, pushFrequency, businessUnitID, sendPhotos, sendOptions, sendDescription, sendPricing, sendVideos, sendNewCars, sendUsedCars, userName, password, releaseEnabled, collectionEnabled, lastActiveInventoryCollection, lastSoldInventoryCollection, ListingSiteDealerId, dateLoginFailure, dateLoginFailureCleared)
	select @type, @stamp, @user, destinationID, activeFlag, pushFrequency, businessUnitID, sendPhotos, sendOptions, sendDescription, sendPricing, sendVideos, sendNewCars, sendUsedCars, userName, password, releaseEnabled, collectionEnabled, lastActiveInventoryCollection, lastSoldInventoryCollection, ListingSiteDealerId, dateLoginFailure, dateLoginFailureCleared
	from deleted

go
