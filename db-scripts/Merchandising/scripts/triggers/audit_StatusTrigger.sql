IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[builder].[VehicleStatus_ChangeTracking]'))
DROP TRIGGER [builder].[VehicleStatus_ChangeTracking]
GO


CREATE TRIGGER VehicleStatus_ChangeTracking ON [builder].[VehicleStatus]
FOR INSERT, UPDATE, DELETE
AS


IF EXISTS (select 1 from INSERTED)	
BEGIN
	declare @actionType varchar(1)
	IF EXISTS (select 1 FROM DELETED)
		set @actionType = 'U'
	ELSE
		set @actionType = 'I'

	INSERT INTO audit.VehicleStatus
		(actionType, statusId, businessUnitID, inventoryId, statusLevel,
		statusTypeId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy)
		SELECT @actionType as actionType, 
				statusId, businessUnitID, inventoryId, statusLevel,
		statusTypeId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy
		FROM inserted
		WHERE lastUpdatedBy <> 'PhotoSynch'
END
ELSE --if not inserted, it was deleted
BEGIN
INSERT INTO audit.VehicleStatus
	(actionType, statusId, businessUnitID, inventoryId, statusLevel,
	statusTypeId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy)
	SELECT 'D' as actionType, 
			statusId, businessUnitID, inventoryId, statusLevel,
	statusTypeId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy
	FROM deleted
	WHERE lastUpdatedBy <> 'PhotoSynch'
END

GO


	 