IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[builder].[Descriptions_ChangeTracking]'))
DROP TRIGGER [builder].[Descriptions_ChangeTracking]
GO


CREATE TRIGGER Descriptions_ChangeTracking ON [builder].[Descriptions]
FOR INSERT, UPDATE, DELETE
AS


IF EXISTS (select 1 from INSERTED)	
BEGIN
	declare @actionType varchar(1)
	IF EXISTS (select 1 FROM DELETED)
		set @actionType = 'U'
	ELSE
		set @actionType = 'I'

	INSERT INTO audit.Descriptions
		(actionType, 
	[businessUnitID],
	[inventoryId],
	[acceleratorAdvertisementId],
	[merchandisingDescription],
	[footer],		
	[expiresOn],
	[selectedTemplateId],
	[themeId],
	[createdOn],
	[createdBy],
	[lastUpdatedOn],
	[lastUpdatedBy]
	)
		SELECT @actionType as actionType, 
				
	[businessUnitID],
	[inventoryId],
	[acceleratorAdvertisementId],
	[merchandisingDescription],
	[footer],	
	[expiresOn],
	ISNULL([templateId],-1),
	[themeId],
	[createdOn],
	[createdBy],
	[lastUpdatedOn],
	[lastUpdatedBy]
		
		FROM INSERTED
END
ELSE --if not inserted, it was deleted
BEGIN
INSERT INTO audit.Descriptions
	(actionType, 
	[businessUnitID],
	[inventoryId],
	[acceleratorAdvertisementId],
	[merchandisingDescription],
	[footer],	
	[expiresOn],
	[selectedTemplateId],
	[themeId],
	[createdOn],
	[createdBy],
	[lastUpdatedOn],
	[lastUpdatedBy]
	
	)
	SELECT 'D' as actionType, 
	[businessUnitID],
	[inventoryId],
	[acceleratorAdvertisementId],
	[merchandisingDescription],
	[footer],	
	[expiresOn],
	isnull([templateId],-1),
	[themeId],
	[createdOn],
	[createdBy],
	[lastUpdatedOn],
	[lastUpdatedBy]
	
	
	FROM DELETED
END

GO


	 