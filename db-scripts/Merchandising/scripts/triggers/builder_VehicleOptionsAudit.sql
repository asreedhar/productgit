if object_id('builder.VehicleOptionsAudit') is not null
	drop trigger [builder].[VehicleOptionsAudit]
go


CREATE trigger [builder].[VehicleOptionsAudit] on [builder].[VehicleOptions]
for insert,update,delete
as

	/* trigger is used to populate audit.VehicleOptions, basically just a compare between insert and deleted to save row changes */

	DECLARE @user nvarchar(128), @stamp datetime

	-- save the user and time stamp so they are consistant (can group insert, updates and delete from a transaction)
	SET @user = suser_sname()
	SET @stamp = getdate()

	-- insert new rows
	INSERT INTO audit.VehicleOptions 
					(	
						actionType, 
						actionDate, 
						suser_sname, 
						vehicleOptionID, 
						businessUnitID, 
						inventoryID, 
						optionID, 
						optionCode, 
						detailText, 
						optionText, 
						isStandardEquipment
					)
		SELECT 'I', 
				@stamp, 
				@user, 
				vehicleOptionID, 
				businessUnitID, 
				inventoryID, 
				optionID, 
				optionCode, 
				detailText, 
				optionText, 
				isStandardEquipment
			FROM inserted
			WHERE NOT EXISTS (SELECT *
								FROM deleted
								WHERE inserted.vehicleOptionID = deleted.vehicleOptionID)
	
	
	-- insert deleted row
	INSERT INTO audit.VehicleOptions 
					(	
						actionType, 
						actionDate, 
						suser_sname, 
						vehicleOptionID, 
						businessUnitID, 
						inventoryID, 
						optionID, 
						optionCode, 
						detailText, 
						optionText, 
						isStandardEquipment
					)
		SELECT 'D', 
				@stamp, 
				@user, 
				vehicleOptionID, 
				businessUnitID, 
				inventoryID, 
				optionID, 
				optionCode, 
				detailText, 
				optionText, 
				isStandardEquipment
			FROM deleted
			WHERE NOT EXISTS (SELECT *
								FROM inserted
								WHERE deleted.vehicleOptionID = inserted.vehicleOptionID)
	
	
	-- insert updates
	INSERT INTO audit.VehicleOptions 
					(	
						actionType, 
						actionDate, 
						suser_sname, 
						vehicleOptionID, 
						businessUnitID, 
						inventoryID, 
						optionID, 
						optionCode, 
						detailText, 
						optionText, 
						isStandardEquipment
					)
		SELECT 'U', 
				@stamp, 
				@user, 
				vehicleOptionID, 
				businessUnitID, 
				inventoryID, 
				optionID, 
				optionCode, 
				detailText, 
				optionText, 
				isStandardEquipment
			FROM deleted
			WHERE EXISTS (SELECT *
							FROM inserted
							WHERE deleted.vehicleOptionID = inserted.vehicleOptionID)
	

GO