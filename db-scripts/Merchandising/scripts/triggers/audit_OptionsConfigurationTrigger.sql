IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[builder].[OptionsConfiguration_ChangeTracking]'))
DROP TRIGGER [builder].[OptionsConfiguration_ChangeTracking]
GO


CREATE TRIGGER OptionsConfiguration_ChangeTracking ON [builder].[OptionsConfiguration]
FOR INSERT, UPDATE, DELETE
AS


IF EXISTS (select 1 from INSERTED)	
BEGIN
	declare @actionType varchar(1)
	IF EXISTS (select 1 FROM DELETED)
		set @actionType = 'U'
	ELSE
		set @actionType = 'I'

	INSERT INTO audit.OptionsConfiguration
		(actionType, 
	[businessUnitID],
	[stockNumber],
	[inventoryId],
	[isLegacy],
	[chromeStyleID],
	[lotLocationID],
	[afterMarketTrim],
	[vehicleCondition],
	[tireTread],
	[reconditioningText],
	[reconditioningValue],
	[extColor1],
	[extColor2],
	[intColor],
	[certificationTypeId],
	[specialId],
	[afterMarketCategoryString],
	[additionalInfoText],
	[doNotPostFlag],
	[lotDataImportLock],
	[postingStatus],
	[onlineFlag],
	[lowActivityFlag],
	[createdOn],
	[createdBy],
	[lastUpdatedOn],
	[lastUpdatedBy]
	)
		SELECT @actionType as actionType, 
				
[businessUnitID],
	[stockNumber],
	[inventoryId],
	[isLegacy],
	[chromeStyleID],
	[lotLocationID],
	[afterMarketTrim],
	[vehicleCondition],
	[tireTread],
	[reconditioningText],
	[reconditioningValue],
	[extColor1],
	[extColor2],
	[intColor],
	[certificationTypeId],
	[specialId],
	[afterMarketCategoryString],
	[additionalInfoText],
	[doNotPostFlag],
	[lotDataImportLock],
	[postingStatus],
	[onlineFlag],
	[lowActivityFlag],
	[createdOn],
	[createdBy],
	[lastUpdatedOn],
	[lastUpdatedBy]
	
		FROM INSERTED
END
ELSE --if not inserted, it was deleted
BEGIN
INSERT INTO audit.OptionsConfiguration
	(actionType, 
	[businessUnitID],
	[stockNumber],
	[inventoryId],
	[isLegacy],
	[chromeStyleID],
	[lotLocationID],
	[afterMarketTrim],
	[vehicleCondition],
	[tireTread],
	[reconditioningText],
	[reconditioningValue],
	[extColor1],
	[extColor2],
	[intColor],
	[certificationTypeId],
	[specialId],
	[afterMarketCategoryString],
	[additionalInfoText],
	[doNotPostFlag],
	[lotDataImportLock],
	[postingStatus],
	[onlineFlag],
	[lowActivityFlag],
	[createdOn],
	[createdBy],
	[lastUpdatedOn],
	[lastUpdatedBy]
	)
	SELECT 'D' as actionType, 
		[businessUnitID],
		[stockNumber],
		[inventoryId],
		[isLegacy],
		[chromeStyleID],
		[lotLocationID],
		[afterMarketTrim],
		[vehicleCondition],
		[tireTread],
		[reconditioningText],
		[reconditioningValue],
		[extColor1],
		[extColor2],
		[intColor],
		[certificationTypeId],
		[specialId],
		[afterMarketCategoryString],
		[additionalInfoText],
		[doNotPostFlag],
		[lotDataImportLock],
		[postingStatus],
		[onlineFlag],
		[lowActivityFlag],
		[createdOn],
		[createdBy],
		[lastUpdatedOn],
		[lastUpdatedBy]
	
	FROM DELETED
END

GO


	 