USE [Merchandising]
GO

-- DROP existing user; should be associated with a server login!
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'merchandisingWebsite')
DROP USER [merchandisingWebsite]
GO

IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'merchandisingWebsite')
BEGIN
	IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'merchandisingWebsite')
	BEGIN
		CREATE USER [merchandisingWebsite] FOR LOGIN [merchandisingWebsite]
	END

	EXEC sp_addrolemember N'aspnet_Membership_BasicAccess', N'merchandisingWebsite'

	EXEC sp_addrolemember N'aspnet_Membership_FullAccess', N'merchandisingWebsite'

	EXEC sp_addrolemember N'aspnet_Membership_ReportingAccess', N'merchandisingWebsite'

	EXEC sp_addrolemember N'aspnet_Personalization_BasicAccess', N'merchandisingWebsite'

	EXEC sp_addrolemember N'aspnet_Personalization_FullAccess', N'merchandisingWebsite'

	EXEC sp_addrolemember N'aspnet_Personalization_ReportingAccess', N'merchandisingWebsite'

	EXEC sp_addrolemember N'aspnet_Profile_BasicAccess', N'merchandisingWebsite'

	EXEC sp_addrolemember N'aspnet_Profile_FullAccess', N'merchandisingWebsite'

	EXEC sp_addrolemember N'aspnet_Profile_ReportingAccess', N'merchandisingWebsite'

	EXEC sp_addrolemember N'aspnet_Roles_BasicAccess', N'merchandisingWebsite'

	EXEC sp_addrolemember N'aspnet_Roles_FullAccess', N'merchandisingWebsite'

	EXEC sp_addrolemember N'aspnet_Roles_ReportingAccess', N'merchandisingWebsite'

	EXEC sp_addrolemember N'aspnet_WebEvent_FullAccess', N'merchandisingWebsite'

	EXEC sp_addrolemember N'db_datareader', N'merchandisingWebsite'

	EXEC sp_addrolemember N'db_datawriter', N'merchandisingWebsite'

	EXEC sp_addrolemember N'MerchandisingUser', N'merchandisingWebsite'
END
GO

/****** Object:  Login [FIRSTLOOK\bfitzpatrick]    Script Date: 03/10/2010 10:48:19 ******/
--only if this person exists as a Login for the database server!
IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'FIRSTLOOK\bfitzpatrick')
BEGIN
	IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'FIRSTLOOK\bfitzpatrick')
	BEGIN
		CREATE USER [FIRSTLOOK\bfitzpatrick] FOR LOGIN [FIRSTLOOK\bfitzpatrick]
	END
	
	EXEC sp_addrolemember N'db_datareader', N'FIRSTLOOK\bfitzpatrick'

	EXEC sp_addrolemember N'db_datawriter', N'FIRSTLOOK\bfitzpatrick'

	EXEC sp_addrolemember N'db_ddladmin', N'FIRSTLOOK\bfitzpatrick'

	EXEC sp_addrolemember N'db_owner', N'FIRSTLOOK\bfitzpatrick'
END
GO

/****** Object:  Login [FIRSTLOOK\bfitzpatrick]    Script Date: 03/10/2010 10:48:19 ******/
--only if this person exists as a Login for the database server!
IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'FIRSTLOOK\bsombke')
BEGIN
	IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'FIRSTLOOK\bsombke')
	BEGIN
		CREATE USER [FIRSTLOOK\bsombke] FOR LOGIN [FIRSTLOOK\bsombke]
	END
	
	EXEC sp_addrolemember N'db_datareader', N'FIRSTLOOK\bsombke'

	EXEC sp_addrolemember N'db_datawriter', N'FIRSTLOOK\bsombke'

	EXEC sp_addrolemember N'db_ddladmin', N'FIRSTLOOK\bsombke'

	EXEC sp_addrolemember N'db_owner', N'FIRSTLOOK\bsombke'
END
GO
