--Blow up if the server login doesn't exist.  don't want to silently swallow.
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'MarketingReviews' AND type = 'R')
CREATE ROLE [MarketingReviews] AUTHORIZATION [dbo]
GO

GRANT DELETE ON SCHEMA::[Snippets] TO [MarketingReviews]
GO
GRANT EXECUTE ON SCHEMA::[Snippets] TO [MarketingReviews]
GO
GRANT INSERT ON SCHEMA::[Snippets] TO [MarketingReviews]
GO
GRANT SELECT ON SCHEMA::[Snippets] TO [MarketingReviews]
GO
GRANT UPDATE ON SCHEMA::[Snippets] TO [MarketingReviews]
GO

IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'FIRSTLOOK\MarketingReviewsUsers')
BEGIN
	CREATE USER [FIRSTLOOK\MarketingReviewsUsers] FOR LOGIN [FIRSTLOOK\MarketingReviewsUsers]
END
GO

EXEC sp_addrolemember N'MarketingReviews', N'FIRSTLOOK\MarketingReviewsUsers'
GO
