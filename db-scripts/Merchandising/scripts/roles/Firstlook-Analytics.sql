IF NOT EXISTS (	SELECT	*
		FROM	sys.database_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'FIRSTLOOK\Analytics'
		)
	CREATE USER [FIRSTLOOK\Analytics] FOR LOGIN [FIRSTLOOK\Analytics]
GO

EXEC sp_addrolemember N'db_datareader', N'FIRSTLOOK\Analytics'
GO
