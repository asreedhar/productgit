IF EXISTS (	SELECT	1
		FROM	sys.server_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'Firstlook\QualityAssurance'
		) BEGIN
		

	IF NOT EXISTS (	SELECT	1
			FROM	sys.database_principals
			WHERE	type_desc = 'WINDOWS_GROUP'
				AND name = 'Firstlook\QualityAssurance'
			) 

		CREATE USER [FIRSTLOOK\QualityAssurance] FOR LOGIN [FIRSTLOOK\QualityAssurance]

	EXEC sp_addrolemember N'db_datareader', N'FIRSTLOOK\QualityAssurance'
	

end