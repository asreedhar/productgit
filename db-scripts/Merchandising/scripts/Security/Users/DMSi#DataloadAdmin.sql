IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'WINDOWS_GROUP'
			AND name = 'FIRSTLOOK\DMSi#DataloadAdmin'
		)

	CREATE USER [FIRSTLOOK\DMSi#DataloadAdmin] FOR LOGIN [FIRSTLOOK\DMSi#DataloadAdmin]

GO


GRANT SELECT ON Settings.DealerListingSites TO [FIRSTLOOK\DMSi#DataloadAdmin]