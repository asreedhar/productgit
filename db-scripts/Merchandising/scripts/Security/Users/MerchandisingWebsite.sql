IF NOT EXISTS (
	SELECT 1
	FROM  sys.database_principals
	WHERE type_desc = 'SQL_USER'
		AND name = 'MerchandisingWebsite'
)
	CREATE USER [MerchandisingWebsite] FOR LOGIN [MerchandisingWebsite]

GO
