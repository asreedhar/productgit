use Merchandising

if not exists (	select	1
		from	sys.database_principals
		where	type_desc = 'SQL_USER'
			and name = 'SalesForceExtractorService'
		)

	create user SalesForceExtractorService for login SalesForceExtractorService with default_schema=[dbo]
GO

grant select on settings.Merchandising to SalesForceExtractorService
grant select on settings.AutoApprove to SalesForceExtractorService
grant select on settings.Franchise to SalesForceExtractorService

GO