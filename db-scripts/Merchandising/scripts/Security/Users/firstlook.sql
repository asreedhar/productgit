IF NOT EXISTS (	SELECT	*
		FROM	sys.database_principals
		WHERE	type_desc = 'SQL_USER'
			AND name = 'firstlook'
		)

	CREATE USER [firstlook] FOR LOGIN [firstlook] WITH DEFAULT_SCHEMA=[dbo]

GO


GRANT SELECT ON [builder].[NewCarPricing] TO [firstlook]
GRANT EXECUTE ON [builder].[GetInventoryManufacturerMake] TO [firstlook]
GRANT SELECT ON [builder].[OptionsConfiguration] TO [firstlook]
GRANT SELECT ON [builder].[VehicleReferenceData] TO [firstlook]
GRANT SELECT ON [workflow].[InventoryMake] TO [firstlook]
GRANT CONNECT TO [firstlook]
