USE [Merchandising]
GO

/****** Object:  Table [demo].[BusinessUnitRelationship]    Script Date: 07/18/2013 11:32:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[Merchandising].[demo].[BusinessUnitRelationship]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
CREATE TABLE [demo].[BusinessUnitRelationship](
	[BusinessUnitID] [int] NOT NULL,
	[ParentID] [int] NOT NULL,
 CONSTRAINT [PK_Demo_BusinessUnitRelationship] PRIMARY KEY CLUSTERED 
(
	[BusinessUnitID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)

END
GO

ALTER TABLE [demo].[BusinessUnitRelationship]  WITH CHECK ADD  CONSTRAINT [FK_Demo_BusinessUnitRelationship_BusinessUnitID] FOREIGN KEY([BusinessUnitID])
REFERENCES [demo].[BusinessUnit] ([BusinessUnitID])
GO

ALTER TABLE [demo].[BusinessUnitRelationship] CHECK CONSTRAINT [FK_Demo_BusinessUnitRelationship_BusinessUnitID]
GO

ALTER TABLE [demo].[BusinessUnitRelationship]  WITH CHECK ADD  CONSTRAINT [FK_Demo_BusinessUnitRelationship_ParentID] FOREIGN KEY([ParentID])
REFERENCES [demo].[BusinessUnit] ([BusinessUnitID])
GO

ALTER TABLE [demo].[BusinessUnitRelationship] CHECK CONSTRAINT [FK_Demo_BusinessUnitRelationship_ParentID]
GO


