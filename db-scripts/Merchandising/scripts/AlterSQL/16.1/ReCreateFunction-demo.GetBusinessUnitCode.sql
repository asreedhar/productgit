USE [Merchandising]
GO

/** Remove the old function, if it exists. We don't use this any more. **/
IF ( EXISTS(SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'demo.RemovePatternFromString') AND xtype IN (N'FN', N'IF', N'TF')))
	DROP FUNCTION [demo].[RemovePatternFromString]
GO

/****** Object:  UserDefinedFunction [demo].[GetBusinessUnitCode]    Script Date: 07/18/2013 14:20:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[demo].[GetBusinessUnitCode]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
	DROP FUNCTION [demo].[GetBusinessUnitCode]
GO

USE [Merchandising]
GO

/****** Object:  UserDefinedFunction [demo].[GetBusinessUnitCode]    Script Date: 07/18/2013 14:20:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [demo].[GetBusinessUnitCode] 
(
	-- Add the parameters for the function here
	@BusinessUnitName nvarchar(MAX)
)
RETURNS nvarchar(20)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result nvarchar(20);
	DECLARE @Prefix varchar(8);
	DECLARE @Suffix varchar(2);
	DECLARE @DPrefix nvarchar(12);

	
	DECLARE @PrefixLength int;
	DECLARE @SuffixLength int;
	DECLARE @KeepPattern varchar(128);
	DECLARE @MaxPrefix varchar(20);
	DECLARE @MaxSuffix varchar(2);
	
	SET @PrefixLength = 10;
	SET @SuffixLength = 2;
	SET @KeepPattern = '[A-Za-z0-9]';
	SET @Suffix = '01';
	
	
	SET @Prefix = SUBSTRING(UPPER(demo.CleanString(@BusinessUnitName, @KeepPattern)),1, @PrefixLength);
	SET @DPrefix = 'DM' + @Prefix;

	SELECT @MaxPrefix = MAX(BusinessUnitCode) FROM demo.BusinessUnit WHERE SUBSTRING(BusinessUnitCode,1,LEN(@DPrefix)) = @DPrefix;

	-- If this prefix already exists
	IF( EXISTS (SELECT @MaxPrefix) AND @MaxPrefix IS NOT NULL)
	BEGIN
		SET @Suffix = SUBSTRING(@MaxPrefix, LEN(@MaxPrefix) - 1, 2);
		SET @Suffix = CONVERT(varchar(2), (CONVERT(int, @Suffix) + 1) )
		IF(LEN(@Suffix) < @SuffixLength)
			SET @Suffix = '0' + @Suffix

	END

	SET @Result = @DPrefix + @Suffix;
	
	-- Return the result of the function
	RETURN @Result

END

GO


