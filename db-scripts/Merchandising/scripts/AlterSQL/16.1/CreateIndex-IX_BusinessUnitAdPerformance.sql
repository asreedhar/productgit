USE [Merchandising]
GO

/****** Object:  Index [IX_BusinessUnitAdPerformance]    Script Date: 08/05/2013 14:19:36 ******/
CREATE NONCLUSTERED INDEX [IX_BusinessUnitAdPerformance] ON [dashboard].[InventoryAdPerformance] 
(
	[BusinessUnitId] ASC,
	[DestinationId] ASC,
	[Date] ASC
)
GO

-- Executed in 5:47 on THUBER01


