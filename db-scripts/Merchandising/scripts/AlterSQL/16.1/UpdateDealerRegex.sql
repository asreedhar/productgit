update merchandising.settings.GoogleQuery
set filter = 'ga:pagePath=~[/]inventory-details[/][0-9]{4\,4}[/]'
where Queryname = 'drivedominion-VDP-Both'

update merchandising.settings.GoogleQuery 
set filter = 'ga:pagePath=~[/]inventory-details[/][0-9]{4\,4}[/];ga:pageTitle=~^new'
where Queryname = 'drivedominion-VDP-New'

update merchandising.settings.GoogleQuery
set filter = 'ga:pagePath=~[/]inventory-details[/][0-9]{4\,4}[/];ga:pageTitle=~^used'
where Queryname = 'drivedominion-VDP-Used';

update merchandising.settings.GoogleQuery
set filter = 'ga:pagePath=~[/]new[/]index\.cfm[/]action[/]view|[/]preowned[/]index\.cfm[/]action[/]view'
where Queryname = 'SOKAL-VDP-Both';

update merchandising.settings.GoogleQuery
set filter = 'ga:pagePath=~[/]new[/]index\.cfm[/]action[/]view'
where Queryname = 'SOKAL-VDP-New';

update merchandising.settings.GoogleQuery
set filter = 'ga:pagePath=~[/]preowned[/]index\.cfm[/]action[/]view'
where Queryname = 'SOKAL-VDP-Used';

update merchandising.settings.GoogleQuery
set filter = 'ga:pagePath=~[/]Vehicle Details[/]New[/][0-9]{4\,4}[/]|[/]Vehicle Details[/]Pre-Owned[/][0-9]{4\,4}[/]'
where Queryname = 'Stpventures-VDP-Both';

update merchandising.settings.GoogleQuery
set filter = 'ga:pagePath=~[/]Vehicle Details[/]New[/][0-9]{4\,4}[/]'
where Queryname = 'Stpventures-VDP-New';

update merchandising.settings.GoogleQuery
set filter = 'ga:pagePath=~[/]Vehicle Details[/]Pre-Owned[/][0-9]{4\,4}[/]'
where Queryname = 'Stpventures-VDP-Used';

update merchandising.settings.GoogleQuery
set filter = 'ga:pagePath=~[/][0-9]{4\,4}.+[/]vd[/][0-9]{4\,}'
where Queryname = 'VinSolutions-VDP-Both';

update merchandising.settings.GoogleQuery
set filter = 'ga:pagePath=~[/][0-9]{4\,4}.+[/]vd[/][0-9]{4\,};ga:pageTitle=~New'
where Queryname = 'VinSolutions-VDP-New';

update merchandising.settings.GoogleQuery
set filter = 'ga:pagePath=~[/][0-9]{4\,4}.+[/]vd[/][0-9]{4\,};ga:pageTitle=~Used|Certified'
where Queryname = 'VinSolutions-VDP-Used';
