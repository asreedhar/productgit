declare @googleQuery table (
	QueryId int not null,
	Provider varchar(50) not null,
	QueryType int not null,
	QueryName varchar(50) not null,
	Dimensions varchar(255) null,
	Filter varchar(255) null,
	Limit int null,
	Metrics varchar(255) null,
	Segment varchar(255) null,
	Sort varchar(255) null
	)

declare @edtDestinations table (
	destinationId int not null,
	description varchar(50) not null,
	maxDescriptionLength int not null,
	maxPhotoCount int not null,
	acceptsDescriptions tinyint not null,
	acceptsOptions tinyint not null,
	acceptsPhotos tinyint not null,
	acceptsVideos tinyint not null,
	canReleaseTo bit not null,
	canCollectMetricsFrom bit not null,
	active bit not null,
	isDealerWebSite bit not null,
	hasGoogleVdps bit not null
)

insert into @edtDestinations
select
-1, 'Stpventures', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1
union select
-1, 'drivedominion', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1
union select
-1, 'SOKAL', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1
union select
-1, 'worlddealer.net', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1
union select
-1, 'VinSolutions', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1

insert @googleQuery
select 
-1, 'Stpventures', 2, 'Stpventures-VDP-New', 'ga:year,ga:month', 'ga:pagePath=~[/]Vehicle Details[/]New[/][0-9]{4,4}[/]', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'Stpventures', 3, 'Stpventures-VDP-Used', 'ga:year,ga:month', 'ga:pagePath=~[/]Vehicle Details[/]Pre-Owned[/][0-9]{4,4}[/]', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'Stpventures', 4, 'Stpventures-VDP-Both', 'ga:year,ga:month', 'ga:pagePath=~[/]Vehicle Details[/]New[/][0-9]{4,4}[/]|[/]Vehicle Details[/]Pre-Owned[/][0-9]{4,4}[/]', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'drivedominion', 2, 'drivedominion-VDP-New', 'ga:year,ga:month', 'ga:pagePath=~[/]inventory-details[/][0-9]{4,4}[/];ga:pageTitle=~^new', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'drivedominion', 3, 'drivedominion-VDP-Used', 'ga:year,ga:month', 'ga:pagePath=~[/]inventory-details[/][0-9]{4,4}[/];ga:pageTitle=~^used', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'drivedominion', 4, 'drivedominion-VDP-Both', 'ga:year,ga:month', 'ga:pagePath=~[/]inventory-details[/][0-9]{4,4}[/]', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'SOKAL', 2, 'SOKAL-VDP-New', 'ga:year,ga:month', 'ga:pagePath=~[/]new[/]index\.cfm[/]action[/]view', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'SOKAL', 3, 'SOKAL-VDP-Used', 'ga:year,ga:month', 'ga:pagePath=~[/]preowned[/]index\.cfm[/]action[/]view', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'SOKAL', 4, 'SOKAL-VDP-Both', 'ga:year,ga:month', 'ga:pagePath=~[/]new[/]index\.cfm[/]action[/]view|[/]preowned[/]index\.cfm[/]action[/]view', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'worlddealer.net', 2, 'worlddealer.net-VDP-New', 'ga:year,ga:month', 'ga:pagePath=~[/]New[/].+-[0-9A-Z]{17}-wdi\.cfm', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'worlddealer.net', 3, 'worlddealer.net-VDP-Used', 'ga:year,ga:month', 'ga:pagePath=~[/]Used[/].+-[0-9A-Z]{17}-wdi\.cfm|[/]Certified[/].+-[0-9A-Z]{17}-wdi\.cfm', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'worlddealer.net', 4, 'worlddealer.net-VDP-Both', 'ga:year,ga:month', 'ga:pagePath=~[/]New[/].+-[0-9A-Z]{17}-wdi\.cfm|[/]Used[/].+-[0-9A-Z]{17}-wdi\.cfm|[/]Certified[/].+-[0-9A-Z]{17}-wdi\.cfm', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'VinSolutions', 2, 'VinSolutions-VDP-New', 'ga:year,ga:month', 'ga:pagePath=~[/][0-9]{4,4}.+[/]vd[/][0-9]{4,};ga:pagePath=~New', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'VinSolutions', 3, 'VinSolutions-VDP-Used', 'ga:year,ga:month', 'ga:pagePath=~[/][0-9]{4,4}.+[/]vd[/][0-9]{4,};ga:pagePath=~Used', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'VinSolutions', 4, 'VinSolutions-VDP-Both', 'ga:year,ga:month', 'ga:pagePath=~[/][0-9]{4,4}.+[/]vd[/][0-9]{4,}', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'

--Insert google query
insert into merchandising.settings.GoogleQuery
select QueryType, QueryName, Dimensions, Filter, Limit, Metrics, Segment, Sort
from @googleQuery
order by QueryName

Update @googleQuery
SET q.QueryId = g.QueryId
FROM merchandising.settings.GoogleQuery g
JOIN @googleQuery q on g.QueryName = q.QueryName


--insert EdtDestinations
insert into merchandising.settings.EdtDestinations
select description, maxDescriptionLength, maxPhotoCount, acceptsDescriptions,
acceptsOptions, acceptsPhotos, acceptsVideos, canReleaseTo, canCollectMetricsFrom, active, isDealerWebsite, hasGoogleVdps
from @edtDestinations
order by description

update @edtDestinations
set edt.destinationid = ed.destinationid
from merchandising.settings.EdtDestinations ed
join @edtDestinations edt on edt.description = ed.description


--insert EdtDestinationQueries
insert into merchandising.settings.EdtDestinationQueries
select edt.destinationid, gq.QueryId
from @edtDestinations edt
join @googleQuery gq on edt.description = gq.Provider
