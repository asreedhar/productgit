-- These IDs were pulled from proddb02sql
UPDATE Merchandising.templates.AdTemplates 
SET name = 'Chrysler Town & Country New 2013'
WHERE businessUnitID = 101621
AND templateID = 78334

GO

UPDATE Merchandising.templates.AdTemplates 
SET name = 'Chrysler Town & Country Used 2013'
WHERE businessUnitID = 101621
AND templateID = 78335

GO

