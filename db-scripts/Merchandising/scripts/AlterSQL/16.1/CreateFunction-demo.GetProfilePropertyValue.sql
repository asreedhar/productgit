USE [Merchandising]
GO

/****** Object:  UserDefinedFunction [demo].[RemovePatternFromString]    Script Date: 07/18/2013 10:44:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Create date: 08/01/2013
-- Description: Gets the property value from
--              the Asp.Net profile.
--              @PropertyName � The property to
--              be found.
--              @PropertyNamesString � The
--              property names information.
--              @PropertyValuesString � The
--              property values information
-- =============================================

IF ( EXISTS(SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'demo.GetProfilePropertyValue') AND xtype IN (N'FN', N'IF', N'TF')))
	DROP FUNCTION [demo].[GetProfilePropertyValue]
GO

CREATE FUNCTION [demo].[GetProfilePropertyValue] (  
    @PropertyName as varchar(max)
    , @PropertyNamesString as varchar(max)
    , @PropertyValuesString as varchar(max)) 
RETURNS varchar(max)
AS
BEGIN
    DECLARE @StartIndex int
    DECLARE @EndIndex int
    DECLARE @StartPos int
    DECLARE @Length int
    
    -- First we find the starting position
    Set @StartIndex = PatIndex('%' + @PropertyName + ':%', @PropertyNamesString) + LEN(RTRIM(@PropertyName)) + 3
    Set @EndIndex = PatIndex('%:%', Right(@PropertyNamesString, LEN(@PropertyNamesString) - @StartIndex))
    Set @StartPos = Cast(Substring(@PropertyNamesString, @StartIndex, @EndIndex) As Int)
    
    -- Now we need to know how long it is
    Set @StartIndex = @StartIndex + @EndIndex + 1
    Set @EndIndex = PatIndex('%:%', Right(@PropertyNamesString, LEN(@PropertyNamesString) - @StartIndex))
    Set @Length = Cast(Substring(@PropertyNamesString, @StartIndex, @EndIndex) As int)
    
    -- Now we get the value we want
    RETURN SUBSTRING(@PropertyValuesString, @StartPos + 1, @Length)
END
GO

GRANT EXEC ON [demo].[GetProfilePropertyValue] TO MerchandisingUser
GO