USE [Merchandising]
GO

/****** Object:  UserDefinedFunction [demo].[CleanString]    Script Date: 08/12/2013 10:44:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-08-12
-- Description:	Remove characters from a string
--				that do not match the provided
--				pattern
-- =============================================
IF ( EXISTS(SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'demo.CleanString') AND xtype IN (N'FN', N'IF', N'TF')))
	DROP FUNCTION [demo].[CleanString]
GO

CREATE FUNCTION [demo].[CleanString]
(
	@BUFFER VARCHAR(MAX), 
	@ALLOWED VARCHAR(128)
) 
RETURNS VARCHAR(MAX)
AS
BEGIN
  DECLARE @p INT = 1, @n VARCHAR(255) = '', @c CHAR(1);
  WHILE @p <= LEN(@BUFFER)
  BEGIN
	SELECT @c = SUBSTRING(@BUFFER, @p, 1);
    IF @c LIKE @ALLOWED
    BEGIN
      SET @n += @c;
    END 
    SET @p += 1;
  END
  RETURN(@n);
END

GO

GRANT EXEC ON [demo].[CleanString] TO MerchandisingUser
GO

