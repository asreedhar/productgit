USE [Merchandising]
GO

/****** Object:  Table [demo].[BusinessUnitRelationship]    Script Date: 07/18/2013 11:32:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[Merchandising].[demo].[BusinessUnitRelationship]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
--DROP TABLE demo.BusinessUnitRelationship
--IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[Merchandising].[demo].[BusinessUnit]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
--DROP TABLE demo.BusinessUnit
	
IF NOT EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[Merchandising].[demo].[BusinessUnit]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [Merchandising].[demo].[BusinessUnit](

	[BusinessUnitID] [int] IDENTITY(10000000,1) NOT NULL,
	[BusinessUnitTypeID] [int] NOT NULL,
	[BusinessUnit] [varchar](40) NOT NULL,
	[BusinessUnitCode] [varchar](20) NULL,
	[Active] [tinyint] NOT NULL DEFAULT (1),
	CONSTRAINT [PK_Demo_BusinessUnit] PRIMARY KEY NONCLUSTERED 
	(
		[BusinessUnitID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80),

	CONSTRAINT [UQ_Demo_BusinessUnit_BusinessUnitCode] UNIQUE NONCLUSTERED 
	(
		[BusinessUnitCode] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80)
)
END

GO


