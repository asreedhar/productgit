	declare @stores table (BusinessUnitID int not null)
	declare @templates table (templateID int not null)

	insert @stores (BusinessUnitID)
	select b.businessunitid
	from merchandising.settings.merchandising s
	join imt.dbo.businessunit b on b.businessunitid = s.businessunitid
	join imt.dbo.DealerUpgrade u on u.businessunitid = b.businessunitid and u.dealerUpgradeCD = 24
	where ModelLevelFrameworksEnabled = 1 and b.Active = 1


	insert @templates (templateID)
	select templateID
	from Merchandising.templates.AdTemplates at
	join merchandising.templates.vehicleProfiles vp on at.vehicleProfileID = vp.vehicleprofileid  
	where at.businessunitid = 101621
	and templateID in (74805,74818,67593,78336,78332,78334,78326,
	78387,78383,78385,78381,78379,78320,78324,78389,
	78393,78391,78343,78341,78338,78357,78355,78373,78359)

	--- do not edit script below this line, only edit the stores and templates list above

	set xact_abort on
	set transaction isolation level serializable
	begin transaction

	declare
		@rows int,
		@businessUnitId int,
		@sourceTemplateId int,
		@vehicleProfileId int


	declare @storeProfiles table
	(
		BusinessUnitID int not null,
		sourceTemplateId int not null,
		vehicleProfileId int null,
		processed bit not null
		
	)

	insert @storeProfiles
	(
		BusinessUnitID,
		sourceTemplateId,
		processed
	)
	select
		s.BusinessUnitID,
		t.templateID,
		0
	from
		@stores s
		cross join @templates t


	declare @vpToDelete table
	(
		vehicleProfileID int
	)

	-- delete all VehicleProfile, AdTemplate, and AdTemplateItems for the new stores 
	-- that are associated with the "master" templates we are about to clone.  these
	-- records might not exist, but if they do this script will not correctly map the
	-- deltas that may occur if a store only has a partial framework frmo the masters.

	insert @vpToDelete
	(
		vehicleProfileID
	)
	select
		vp.vehicleProfileID
	from
		Merchandising.templates.VehicleProfiles vp
		join Merchandising.templates.AdTemplates at
			on vp.vehicleProfileID = at.vehicleProfileId
		join @storeProfiles sp
			on at.SourceTemplateId = sp.sourceTemplateId
			and at.businessUnitID = sp.BusinessUnitID
			and vp.businessUnitId = sp.BusinessUnitID
		
	delete ati
	from
		Merchandising.templates.AdTemplateItems ati
		join Merchandising.templates.AdTemplates at
			on ati.templateID = at.templateID
		join @storeProfiles sp
			on at.businessUnitID = sp.BusinessUnitID
			and at.SourceTemplateId = sp.sourceTemplateId

	delete at
	from
		Merchandising.templates.AdTemplates at
		join @storeProfiles sp
			on at.SourceTemplateId = sp.sourceTemplateId
			and at.businessUnitID = sp.BusinessUnitID

	delete vp
	from
		Merchandising.templates.VehicleProfiles vp
		join @vpToDelete d
			on vp.vehicleProfileID = d.vehicleProfileID

	while (1=1)
	begin
		select top 1
			@businessUnitId = BusinessUnitID,
			@sourceTemplateId = sourceTemplateId
		from @storeProfiles
		where processed = 0
		
		if (@@rowcount <> 1) break;
		
		insert Merchandising.templates.VehicleProfiles
		( 
			businessUnitId,
			title,
			makeIDList,
			modelList,
			marketClassIdList,
			startYear,
			endYear,
			minAge,
			maxAge,
			minPrice,
			maxPrice,
			certifiedStatusFilter,
			trimList,
			segmentIdList,
			vehicleType
		)
		select
			@businessUnitID,
			vp.title,
			vp.makeIDList,
			vp.modelList,
			vp.marketClassIdList,
			vp.startYear,
			vp.endYear,
			vp.minAge,
			vp.maxAge,
			vp.minPrice,
			vp.maxPrice,
			vp.certifiedStatusFilter,
			vp.trimList,
			vp.segmentIdList,
			vp.vehicleType	
		from
			Merchandising.templates.AdTemplates at
			join Merchandising.templates.VehicleProfiles vp
				on at.vehicleProfileId = vp.vehicleProfileID
		where at.templateID = @sourceTemplateId
		
		select @rows = @@rowcount, @vehicleProfileId = scope_identity()
		
		if (@rows > 1) raiserror('incorrect rows inserted into vehicle profiles!', 16, 1)
		
		update @storeProfiles set
			vehicleProfileId = @vehicleProfileId,
			processed = 1
		where
			BusinessUnitID = @businessUnitId
			and sourceTemplateId = @sourceTemplateId
		
		set @rows = @@rowcount
		if (@rows <> 1) raiserror('incorrect rows updated in local table!', 16, 1)
		
	end	-- VehicleProfile insert loop


	print ''
	print ''

	select @rows = count(*) from @storeProfiles
	print convert(varchar(10), @rows) + ' new VehicleProfiles added.'




	-- insert the new templates
	insert Merchandising.templates.AdTemplates
	(
		name,
		businessUnitID,
		vehicleProfileId,
		parent,
		active,
		SourceTemplateId
	)
	select
		at.name,
		sp.BusinessUnitID,
		sp.vehicleProfileId,
		at.parent,
		at.active,
		SourceTemplateId = at.templateID
	from
		Merchandising.templates.AdTemplates at
		join @storeProfiles sp
			on at.templateID = sp.sourceTemplateId
		

	-- ok go clone the AdTemplateItems tied to @templates list to the new @stores list
	insert Merchandising.templates.AdTemplateItems
	(
		templateID,
		blurbID,
		sequence,
		priority,
		required,
		parentID
	)
	select
		newTemplates.templateID,
		ati.blurbID,
		ati.sequence,
		ati.priority,
		ati.required,
		ati.parentID
	from
		Merchandising.templates.AdTemplateItems ati
		join Merchandising.templates.AdTemplates at
			on ati.templateID = at.templateID
		join @templates t
			on at.templateID = t.templateID
		join Merchandising.templates.AdTemplates newTemplates
			on newTemplates.SourceTemplateId = at.templateID
		join @stores s
			on newTemplates.businessUnitID = s.BusinessUnitID

	commit
	set xact_abort off
	set transaction isolation level read uncommitted