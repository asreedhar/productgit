declare @otherOptions table (
	businessunitid int not null,
	inventoryid int not null,
	optionid int not null
 )
 
 insert into @otherOptions
 select distinct b.businessunitid, i.inventoryid, vo.optionid
 from imt.dbo.businessunit b
 join imt.dbo.DealerUpgrade u on u.businessunitid = b.businessunitid
 join imt.dbo.inventory i on b.businessunitid = i.businessunitid
 join merchandising.settings.merchandising m on m.businessunitid = b.businessunitid
 join merchandising.builder.OptionsConfiguration oc on oc.businessunitid = i.businessunitid and oc.inventoryid = i.inventoryid and oc.chromeStyleID != -1
 join merchandising.builder.VehicleOptions vo on vo.businessunitid = i.businessunitid and vo.inventoryid = i.inventoryid
 join VehicleCatalog.chrome.categories cat ON vo.optionID = cat.categoryid and cat.countrycode = 1
 left join    (  SELECT DISTINCT s.styleID, s.categoryID, c.UserFriendlyName
                 FROM VehicleCatalog.chrome.styleCats s
                 JOIN VehicleCatalog.chrome.categories c ON c.categoryid = s.categoryid 
                WHERE s.countrycode = 1 and c.countrycode = 1
             ) c on vo.optionID = c.categoryID and c.styleid = oc.chromeStyleID
 where i.inventoryActive = 1 and b.Active = 1 and b.businessunitid = 106292
 and c.styleid is null
 and exists    ( SELECT *
                 FROM VehicleCatalog.chrome.styleCats s
                 WHERE s.countrycode = 1 and s.styleid = oc.chromeStyleID
             )
 
 
 delete vo
 from @otherOptions oo
 join merchandising.builder.VehicleOptions vo on
	vo.businessunitid = oo.businessunitid and vo.inventoryid = oo.inventoryid and vo.optionid = oo.optionid