-- populate settings.Merchandising sendOptimalFormat column from settings.DealerDatafeedPreferences

update m set
	sendOptimalFormat = convert(bit, ddp.flag)
from
	Merchandising.settings.Merchandising m
	join
	(
		select
			BusinessUnitID,
			flag = max(convert(int, SendHTMLDescription))
		from Merchandising.settings.DealerDatafeedPreferences
		group by BusinessUnitID
	) ddp
		on m.businessUnitID = ddp.BusinessUnitID