-- drop the columns about optimal format from the tables where they don't belong
	if object_id('settings.DF_Settings_DealerListingSites_sendOptimalFormat') is not null
		alter table settings.DealerListingSites drop constraint DF_Settings_DealerListingSites_sendOptimalFormat
	go

	if exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'settings' and TABLE_NAME = 'DealerListingSites' and COLUMN_NAME = 'sendOptimalFormat')
		alter table settings.DealerListingSites drop column sendOptimalFormat
	go

	if object_id('settings.DF_Settings_EdtDestinations_acceptsOptimalFormat') is not null
		alter table settings.EdtDestinations drop constraint DF_Settings_EdtDestinations_acceptsOptimalFormat
	go

	if exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'settings' and TABLE_NAME = 'EdtDestinations' and COLUMN_NAME = 'acceptsOptimalFormat')
		alter table settings.EdtDestinations drop column acceptsOptimalFormat
	go




-- add the flag to settings.Merchandising
	if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'settings' and TABLE_NAME = 'Merchandising' and COLUMN_NAME = 'sendOptimalFormat')
		alter table settings.Merchandising add sendOptimalFormat bit not null constraint DF_settings_Merchandising_sendOptimalFormat default (0)
	go



