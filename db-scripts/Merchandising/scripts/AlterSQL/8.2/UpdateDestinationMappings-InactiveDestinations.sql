--Inactivates the mappings for the inactive destinations for dealers.  
--This fixes all the errors that are logged by the release processor trying to release to inactive destiantions.

update merchandising.settings.dealerListingsites 

set releaseenabled = 0 , activeflag = 0  

where destinationid in
(
	select destinationid from merchandising.settings.EdtDestinations dealers 
	where active = 0
)

and (releaseenabled = 1 or activeflag = 1)