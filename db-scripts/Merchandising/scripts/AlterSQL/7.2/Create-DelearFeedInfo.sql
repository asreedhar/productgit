if  exists (select * from sys.objects where object_id = object_id(N'[merchandising].[DealerFeedInfo]') and type in (N'U'))
drop table [merchandising].[DealerFeedInfo]
GO

-- Create the DealerFeedInfo table which will store useful statistics about
-- each feed by business unit id

create table merchandising.DealerFeedInfo (
	BusinessUnitId int not null,
	DestinationId int not null,
	DatasourceInterfaceId int not null,
	LastSuccessfulDateLoaded datetime null,
	LastSuccessfulRequestId int null,
	constraint PK_DealerFeedInfo 
		primary key clustered (BusinessUnitId, DestinationId, DatasourceInterfaceId)
)
GO