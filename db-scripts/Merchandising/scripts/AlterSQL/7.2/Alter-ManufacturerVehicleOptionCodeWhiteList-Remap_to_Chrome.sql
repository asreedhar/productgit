IF (OBJECT_ID('dbo.FK_ManufacturerVehicleOptionCodeWhiteList_cpoGroups') IS NOT NULL)
BEGIN

ALTER TABLE dbo.ManufacturerVehicleOptionCodeWhiteList
DROP FK_ManufacturerVehicleOptionCodeWhiteList_cpoGroups

ALTER TABLE dbo.ManufacturerVehicleOptionCodeWhiteList
DROP FK_ManufacturerVehicleOptionCodeWhiteList_manufacturerCertifications

UPDATE dbo.ManufacturerVehicleOptionCodeWhiteList
SET VehicleManufacturerID = 6
WHERE VehicleManufacturerID = 5

UPDATE dbo.ManufacturerVehicleOptionCodeWhiteList
SET VehicleMakeID = d.DivisionID 
FROM VehicleCatalog.Chrome.Divisions d
	JOIN builder.manufacturerCertifications mc ON mc.[Name] = d.DivisionName
	JOIN dbo.ManufacturerVehicleOptionCodeWhiteList mvocw ON mvocw.VehicleMakeId = mc.certificationTypeId
WHERE VehicleMakeId = mvocw.VehicleMakeId

END