IF(NOT EXISTS(SELECT 1 FROM Merchandising.settings.CredentialedSites WHERE siteId = 3))
BEGIN
SET IDENTITY_INSERT Merchandising.settings.CredentialedSites ON

INSERT INTO Merchandising.settings.CredentialedSites
           (siteId,siteName)
     VALUES
           (3,'Chrysler Dealer Connect')

SET IDENTITY_INSERT Merchandising.settings.CredentialedSites OFF
END
