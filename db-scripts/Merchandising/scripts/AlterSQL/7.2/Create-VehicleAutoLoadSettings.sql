IF (OBJECT_ID('merchandising.VehicleAutoLoadSettings') IS NOT NULL)
   DROP TABLE merchandising.VehicleAutoLoadSettings
GO

CREATE TABLE merchandising.VehicleAutoLoadSettings(
	ManufacturerID int NOT NULL,
	IsAutoLoadEnabled bit NOT NULL,
	CONSTRAINT PK_VehicleAutoLoadSettings PRIMARY KEY (ManufacturerID)
)


INSERT INTO merchandising.VehicleAutoLoadSettings 
		(ManufacturerID, IsAutoLoadEnabled)
SELECT	ManufacturerID, 0 
  FROM	VehicleCatalog.Chrome.Manufacturers 
 WHERE	CountryCode = 1

UPDATE	merchandising.VehicleAutoLoadSettings 
   SET	IsAutoLoadEnabled = 1
 WHERE	ManufacturerID IN (
		SELECT	ManufacturerID 
		FROM	VehicleCatalog.Chrome.Manufacturers 
		WHERE	ManufacturerName IN ('BMW', 'General Motors', 'Chrysler')
				AND CountryCode = 1
		)
