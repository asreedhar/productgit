-- Add the DateLoaded column and populate it with the current timestamp.
if not exists(select * 
	from Information_Schema.Columns 
	where Table_Catalog = N'Merchandising' 
		and Table_Schema = N'merchandising' 
		and Table_Name = N'AutoTraderVehOnline'
		and Column_Name = N'DateLoaded')
begin
	alter table merchandising.AutoTraderVehOnline
		add DateLoaded datetime default getdate() not null
end