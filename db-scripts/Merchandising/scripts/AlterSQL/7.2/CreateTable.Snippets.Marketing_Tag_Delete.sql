/****** Object:  Table [Snippets].[Marketing_Tag_Delete]    Script Date: 05/23/2011 16:03:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Snippets].[Marketing_Tag_Delete] (
	[TagId] [int] NOT NULL,
	[MarketingId] [int] NOT NULL,
	[Description] [varchar](30) NOT NULL,
	[Expires] [datetime] NOT NULL CONSTRAINT [DF_Marketing_Tag_Delete_Expires] DEFAULT (dateadd(day,(1),getdate()))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF