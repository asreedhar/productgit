IF( EXISTS( SELECT 1 FROM Merchandising.settings.cpoCertifieds WHERE cpoGroupID = 8 AND certificationTypeID != 39 ) )
UPDATE Merchandising.settings.cpoCertifieds
SET certificationTypeID = 39
WHERE cpoGroupID = 8