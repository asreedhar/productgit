ALTER TABLE builder.optionsConfiguration
ADD doNotPostFlag [bit] NOT NULL DEFAULT(0)

ALTER TABLE audit.optionsConfiguration
ADD doNotPostFlag [bit] NOT NULL DEFAULT(0)

ALTER TABLE audit.optionsConfiguration
ALTER COLUMN doNotPostFlag [bit] NOT NULL

 