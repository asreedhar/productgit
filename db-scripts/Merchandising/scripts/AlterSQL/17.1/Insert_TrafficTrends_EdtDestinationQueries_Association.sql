-------- Begin ADD EdtDestinationQueries Section ----------------------------------
IF NOT EXISTS(
	SELECT * FROM Merchandising.settings.EdtDestinationQueries e
	  JOIN Merchandising.settings.GoogleQuery q ON e.[GoogleQueryId] = q.[QueryId]
	 WHERE e.[EdtDestinationId] = 0
	   AND q.[QueryName] = 'Trends'
)
BEGIN	
	INSERT INTO [Merchandising].[settings].[EdtDestinationQueries]
		([EdtDestinationId],[GoogleQueryId])
	VALUES
		(0, (SELECT [QueryId] FROM [Merchandising].[settings].[GoogleQuery]
			WHERE [QueryName] = 'Trends'))
END
GO
-------- End ADD EdtDestinationQueries Section ------------------------------------
