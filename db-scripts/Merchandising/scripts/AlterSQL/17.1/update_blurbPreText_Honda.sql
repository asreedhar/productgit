use Merchandising
GO

UPDATE templates.blurbSampleText
	SET blurbPreText = REPLACE(blurbPreText, 'HONDA', 'Honda')	
	FROM templates.blurbSampleText
	WHERE blurbPreText COLLATE Latin1_General_CS_AS LIKE '%HONDA%'


/* test after update
SELECT TOP 1000 blurbPreText, newBlurbTest = REPLACE(blurbPreText, 'HONDA', 'Honda')	
	FROM templates.blurbSampleText
	WHERE blurbPreText COLLATE Latin1_General_CS_AS LIKE '%HONDA%'
	
*/