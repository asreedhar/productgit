-------- Begin ADD GoogleQuery Section ----------------------------------
IF NOT EXISTS(
	SELECT [QueryName] FROM Merchandising.settings.GoogleQuery WHERE [QueryName] = 'Trends'
)
BEGIN	
	INSERT INTO [Merchandising].[settings].[GoogleQuery]
		([QueryType],[QueryName],[Dimensions],[Filter],[Limit],[Metrics],[Segment],[Sort])
	VALUES
		-- Referrals only
		(1, 'Trends', 'ga:medium,ga:socialNetwork', NULL, 50, 'ga:visits', NULL, '-ga:visits')
END
GO
-------- End ADD GoogleQuery Section ------------------------------------
