BEGIN TRY
	DECLARE @VehicleManufacturerId int, @VehicleMakeId int, @OptionCode varchar(20)

	SELECT @VehicleManufacturerId = ManufacturerID
		FROM VehicleCatalog.Chrome.Manufacturers Manufacturers
		WHERE CountryCode = 1
		AND Manufacturers.ManufacturerName = 'General Motors'
		
	if @@ROWCOUNT <> 1
		RAISERROR('Cannot find General Motors in VehicleCatalog.Chrome.Manufacturers!', 16, 1)
		
	SELECT @VehicleMakeId = DivisionID
		FROM VehicleCatalog.Chrome.Divisions
		WHERE CountryCode = 1
		and ManufacturerID = @VehicleManufacturerId
		AND DivisionName = 'Chevrolet'
		
	if @@ROWCOUNT <> 1
		RAISERROR('Cannot find Chevrolet in VehicleCatalog.Chrome.Divisions for Manufacturer ID: %d!', 16, 1, @VehicleManufacturerId)
		
		-- loop through missing engine code options for 2014 silverados
		DECLARE silveradoEngine2014 CURSOR FOR
			SELECT DISTINCT options.OptionCode 
				FROM VehicleCatalog.Chrome.Options options
				INNER JOIN VehicleCatalog.Chrome.Styles styles ON options.CountryCode = styles.CountryCode
																	AND options.StyleID = styles.StyleID
				INNER JOIN VehicleCatalog.Chrome.YearMakeModelStyle yearMakeModelStyle ON styles.CountryCode = yearMakeModelStyle.CountryCode
																							AND styles.StyleID = yearMakeModelStyle.ChromeStyleID
				INNER JOIN VehicleCatalog.Chrome.Divisions divisions ON yearMakeModelStyle.DivisionID = divisions.DivisionID
																		AND yearMakeModelStyle.CountryCode = divisions.CountryCode
				INNER JOIN VehicleCatalog.Chrome.OptHeaders optHeaders ON options.HeaderID = optHeaders.HeaderID
																		AND options.CountryCode = optHeaders.CountryCode
				WHERE options.CountryCode = 1
				AND NOT EXISTS(SELECT manufacturerVehicleOptionCodeWhiteList.OptionCode 
									FROM Merchandising.dbo.ManufacturerVehicleOptionCodeWhiteList manufacturerVehicleOptionCodeWhiteList 
									WHERE manufacturerVehicleOptionCodeWhiteList.VehicleManufacturerId = divisions.ManufacturerID
									AND manufacturerVehicleOptionCodeWhiteList.VehicleMakeId = divisions.DivisionID
									AND manufacturerVehicleOptionCodeWhiteList.OptionCode = options.OptionCode
									)
				AND yearMakeModelStyle.DivisionID = @VehicleMakeId
				AND divisions.ManufacturerID = @VehicleManufacturerId
				AND yearMakeModelStyle.Year = 2014
				AND ModelName LIKE '%silverado%'
				AND optHeaders.Header = 'ENGINE'
				ORDER BY OptionCode
				
				
		OPEN silveradoEngine2014

		FETCH NEXT FROM silveradoEngine2014
			INTO @optionCode


		WHILE @@FETCH_STATUS = 0
			BEGIN
				if NOT EXISTS (SELECT *
									FROM dbo.ManufacturerVehicleOptionCodeWhiteList
									WHERE VehicleManufacturerId = @VehicleManufacturerId
									AND VehicleMakeId = @VehicleMakeId
									AND optionCode = @OptionCode
									)
						BEGIN
							--SELECT @VehicleManufacturerId, @VehicleMakeId, @OptionCode
							
							INSERT merchandising.dbo.ManufacturerVehicleOptionCodeWhiteList
								( VehicleManufacturerId, VehicleMakeId, OptionCode )
									VALUES (@VehicleManufacturerId, @VehicleMakeId, @OptionCode)
							
						END
			
				-- loop!!!
				FETCH NEXT FROM silveradoEngine2014
					INTO @optionCode
			END

		CLOSE silveradoEngine2014
		DEALLOCATE silveradoEngine2014
	
			
		

	if NOT EXISTS (SELECT *
						FROM dbo.ManufacturerVehicleOptionCodeWhiteList
						WHERE VehicleManufacturerId = @VehicleManufacturerId
						AND VehicleMakeId = @VehicleMakeId
						AND optionCode = 'L83'
						)
			BEGIN
				--SELECT @VehicleManufacturerId, @VehicleMakeId, @OptionCode
				
				INSERT merchandising.dbo.ManufacturerVehicleOptionCodeWhiteList
					( VehicleManufacturerId, VehicleMakeId, OptionCode )
						VALUES (@VehicleManufacturerId, @VehicleMakeId, @OptionCode)
				
			END

END TRY
BEGIN CATCH
	
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               )

END CATCH



/*
DECLARE @ManufacturerID varchar(100), @MakeID varchar(100)
SET @ManufacturerID = (SELECT cpoGroupId FROM [Merchandising].settings.cpoGroups WHERE [name] = 'GM')


SELECT *FROM [Merchandising].settings.cpoGroups WHERE [name] = 'GM'

SELECT *
	FROM VehicleCatalog.Chrome.Manufacturers
	WHERE 
	

sp_help ManufacturerVehicleOptionCodeWhiteList


INSERT [Merchandising].[dbo].[ManufacturerVehicleOptionCodeWhiteList] 
	( [VehicleManufacturerId], [VehicleMakeId], [OptionCode] )
VALUES (6, 8, 'L83')
*/












/*

DECLARE @ManufacturerID varchar(100), @MakeID varchar(100)
SET @ManufacturerID = (SELECT cpoGroupId FROM [Merchandising].settings.cpoGroups WHERE [name] = 'GM')

SET @MakeID = (SELECT certificationTypeId FROM [Merchandising].builder.manufacturerCertifications WHERE [name] like '%Buick%')
INSERT [Merchandising].[dbo].[ManufacturerVehicleOptionCodeWhiteList] ( [VehicleManufacturerId], [VehicleMakeId], [OptionCode] )
VALUES (@ManufacturerID, @MakeID, 'BTV')
INSERT [Merchandising].[dbo].[ManufacturerVehicleOptionCodeWhiteList] ( [VehicleManufacturerId], [VehicleMakeId], [OptionCode] )
VALUES (@ManufacturerID, @MakeID, 'KB6')

SET @MakeID = (SELECT certificationTypeId FROM [Merchandising].builder.manufacturerCertifications WHERE [name] like '%GMC%') 
INSERT [Merchandising].[dbo].[ManufacturerVehicleOptionCodeWhiteList] ( [VehicleManufacturerId], [VehicleMakeId], [OptionCode] )
VALUES (@ManufacturerID, @MakeID, 'VR4')
INSERT [Merchandising].[dbo].[ManufacturerVehicleOptionCodeWhiteList] ( [VehicleManufacturerId], [VehicleMakeId], [OptionCode] )
VALUES (@ManufacturerID, @MakeID, 'PCY')
INSERT [Merchandising].[dbo].[ManufacturerVehicleOptionCodeWhiteList] ( [VehicleManufacturerId], [VehicleMakeId], [OptionCode] )
VALUES (@ManufacturerID, @MakeID, 'PY9')
INSERT [Merchandising].[dbo].[ManufacturerVehicleOptionCodeWhiteList] ( [VehicleManufacturerId], [VehicleMakeId], [OptionCode] )
VALUES (@ManufacturerID, @MakeID, 'K34')
INSERT [Merchandising].[dbo].[ManufacturerVehicleOptionCodeWhiteList] ( [VehicleManufacturerId], [VehicleMakeId], [OptionCode] )
VALUES (@ManufacturerID, @MakeID, 'AE7')

*/

/*


sp_helptext 'Chrome.getUnfilteredOptions'


sp_helptext 'Chrome.getUnfilteredOptions'

EXEC Chrome.getUnfilteredOptions @ChromeStyleID = 357152, @whiteLIstOnly = 0

EXEC chrome.getStyleOptions 357152


sp_help 'builder.Vehicle_DetailedEquipmentDescriptions'

SP_HELP manufacturerVehicleOptionCodeWhiteList


*/