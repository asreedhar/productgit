BEGIN TRY
	DECLARE @VehicleManufacturerId int, @VehicleMakeId int, @OptionCode varchar(20)

	SELECT @VehicleManufacturerId = ManufacturerID
		FROM VehicleCatalog.Chrome.Manufacturers Manufacturers
		WHERE CountryCode = 1
		AND Manufacturers.ManufacturerName = 'General Motors'
		
	if @@ROWCOUNT <> 1
		RAISERROR('Cannot find General Motors in VehicleCatalog.Chrome.Manufacturers!', 16, 1)
		
	SELECT @VehicleMakeId = DivisionID
		FROM VehicleCatalog.Chrome.Divisions
		WHERE CountryCode = 1
		and ManufacturerID = @VehicleManufacturerId
		AND DivisionName = 'Chevrolet'
		
	if @@ROWCOUNT <> 1
		RAISERROR('Cannot find Chevrolet in VehicleCatalog.Chrome.Divisions for Manufacturer ID: %d!', 16, 1, @VehicleManufacturerId)
		
	-- insert the missing codes for the white list				
	INSERT merchandising.dbo.ManufacturerVehicleOptionCodeWhiteList
		( VehicleManufacturerId, VehicleMakeId, OptionCode )
	SELECT DISTINCT divisions.ManufacturerID, yearMakeModelStyle.DivisionID, options.OptionCode 
		FROM VehicleCatalog.Chrome.Options options
		INNER JOIN VehicleCatalog.Chrome.Styles styles ON options.CountryCode = styles.CountryCode
															AND options.StyleID = styles.StyleID
		INNER JOIN VehicleCatalog.Chrome.YearMakeModelStyle yearMakeModelStyle ON styles.CountryCode = yearMakeModelStyle.CountryCode
																					AND styles.StyleID = yearMakeModelStyle.ChromeStyleID
		INNER JOIN VehicleCatalog.Chrome.Divisions divisions ON yearMakeModelStyle.DivisionID = divisions.DivisionID
																AND yearMakeModelStyle.CountryCode = divisions.CountryCode
		INNER JOIN VehicleCatalog.Chrome.OptHeaders optHeaders ON options.HeaderID = optHeaders.HeaderID
																AND options.CountryCode = optHeaders.CountryCode
		WHERE options.CountryCode = 1
		AND NOT EXISTS(SELECT manufacturerVehicleOptionCodeWhiteList.OptionCode 
							FROM Merchandising.dbo.ManufacturerVehicleOptionCodeWhiteList manufacturerVehicleOptionCodeWhiteList 
							WHERE manufacturerVehicleOptionCodeWhiteList.VehicleManufacturerId = divisions.ManufacturerID
							AND manufacturerVehicleOptionCodeWhiteList.VehicleMakeId = divisions.DivisionID
							AND manufacturerVehicleOptionCodeWhiteList.OptionCode = options.OptionCode
							)
		AND yearMakeModelStyle.DivisionID = @VehicleMakeId
		AND divisions.ManufacturerID = @VehicleManufacturerId
		--AND yearMakeModelStyle.Year = 2014
		AND ModelName LIKE '%silverado%'
		AND optHeaders.Header = 'ENGINE'
		AND options.OptionCode NOT LIKE '%-f'
		ORDER BY OptionCode
	

END TRY
BEGIN CATCH
	
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               )

END CATCH
