DECLARE @badOption
TABLE(vehicleOptionID int, chromeStyleID int
)

-- find option not in chrome for that vehicles chrome style
INSERT INTO @badOption
SELECT VehicleOptions.vehicleOptionID, OptionsConfiguration.chromeStyleID
	FROM Merchandising.builder.VehicleOptions vehicleOptions
	INNER JOIN imt.dbo.BusinessUnit businessUnit ON vehicleOptions.businessUnitID = businessUnit.BusinessUnitID
													AND businessUnit.Active = 1 -- active business units
	INNER JOIN imt.dbo.Inventory inventory ON vehicleOptions.inventoryId = inventory.InventoryID
												AND vehicleOptions.businessUnitID = inventory.businessUnitID
	INNER JOIN imt.dbo.DealerUpgrade DealerUpgrade ON BusinessUnit.businessunitid = DealerUpgrade.businessunitid
													AND DealerUpgradeCD = 24 -- max!
	INNER JOIN merchandising.settings.merchandising merchandising ON businessUnit.BusinessUnitID = merchandising.businessUnitID
																		AND (merchandising.lotProviderId = 8 OR merchandising.lotProviderId IS NULL) -- AULTec or no lot provider
	INNER JOIN merchandising.builder.OptionsConfiguration OptionsConfiguration ON inventory.InventoryID = OptionsConfiguration.inventoryId
																				AND inventory.BusinessUnitID = OptionsConfiguration.BusinessUnitID
																				and OptionsConfiguration.chromeStyleID > -1
	INNER JOIN VehicleCatalog.chrome.categories categories2 ON vehicleOptions.optionID = categories2.categoryid
															AND categories2.CountryCode = 1
														--	AND categories2.CategoryTypeFilter != 'Transmission - Type'
	INNER JOIN VehicleCatalog.chrome.categoryheaders categoryHeaders2 ON categories2.categoryheaderid = categoryheaders2.CategoryHeaderID
																		AND categories2.CountryCode = categoryheaders2.CountryCode
														---				AND categoryHeaders2.CategoryHeader NOT IN ('Engine', 'Drivetrain', 'Fuel', 'Transmission')
	WHERE NOT EXISTS (

					SELECT DISTINCT categories.*, CategoryHeaders.CategoryHeader, styleCats.FeatureType
						FROM VehicleCatalog.chrome.styleCats styleCats
						INNER JOIN VehicleCatalog.chrome.categories categories ON styleCats.categoryid = categories.categoryid
																				AND styleCats.CountryCode = categories.CountryCode
						INNER JOIN VehicleCatalog.chrome.categoryheaders categoryHeaders ON categories.categoryheaderid = categoryheaders.CategoryHeaderID
																							AND categories.CountryCode = categoryheaders.CountryCode
						--INNER JOIN merchandising.keyDisplayCategories keyDisplayCategories ON categoryheaders.categoryheaderid = keyDisplayCategories.categoryHeaderId
						--																		AND showOnOptionForm = 1
						WHERE styleCats.countrycode = 1
						--AND styleCats.state <> 'D'
						AND styleCats.styleID = OptionsConfiguration.chromeStyleID
						AND categories.CategoryID = vehicleOptions.optionID
				)
				-- no RI between chrome and optionConfiguration, bad chrome ids ... but might be good options
	AND  EXISTS ( SELECT *
					 FROM VehicleCatalog.chrome.styleCats s
					 WHERE s.countrycode = 1 and s.styleid = OptionsConfiguration.chromeStyleID
				)

	-- AND inventory.businessUnitID = 105239
	--AND inventory.inventoryId = 29155460
	AND inventory.InventoryActive = 1
	
	
-- delete!!!!
DELETE vehicleOptions
	FROM builder.VehicleOptions vehicleOptions
	INNER JOIN imt.dbo.BusinessUnit businessUnit ON vehicleOptions.businessUnitID = businessUnit.BusinessUnitID
	INNER JOIN imt.dbo.Inventory inventory ON vehicleOptions.inventoryId = inventory.InventoryID
												AND vehicleOptions.businessUnitID = inventory.businessUnitID
	INNER JOIN VehicleCatalog.chrome.categories categories ON vehicleOptions.optionID = categories.categoryid
															AND categories.CountryCode = 1
	INNER JOIN VehicleCatalog.chrome.categoryheaders categoryHeaders ON categories.categoryheaderid = categoryheaders.CategoryHeaderID
																		AND categories.CountryCode = categoryheaders.CountryCode
	INNER JOIN @badOption badOption ON vehicleOptions.vehicleOptionID = badOption.vehicleOptionID
	

/*
-- return options for testing
SELECT businessUnit.BusinessUnit, inventory.StockNumber, categoryHeaders.CategoryHeader, categories.UserFriendlyName, inventory.InventoryActive, vehicleOptions.optionID, badOption.chromeStyleID, vehicleOptions.businessUnitID, vehicleOptions.inventoryId
	FROM builder.VehicleOptions vehicleOptions
	INNER JOIN imt.dbo.BusinessUnit businessUnit ON vehicleOptions.businessUnitID = businessUnit.BusinessUnitID
	INNER JOIN imt.dbo.Inventory inventory ON vehicleOptions.inventoryId = inventory.InventoryID
												AND vehicleOptions.businessUnitID = inventory.businessUnitID
	INNER JOIN VehicleCatalog.chrome.categories categories ON vehicleOptions.optionID = categories.categoryid
															AND categories.CountryCode = 1
	INNER JOIN VehicleCatalog.chrome.categoryheaders categoryHeaders ON categories.categoryheaderid = categoryheaders.CategoryHeaderID
																		AND categories.CountryCode = categoryheaders.CountryCode
	INNER JOIN @badOption badOption ON vehicleOptions.vehicleOptionID = badOption.vehicleOptionID
	ORDER BY businessUnit.BusinessUnit, inventory.StockNumber, categoryHeaders.CategoryHeader, categories.UserFriendlyName
	
*/