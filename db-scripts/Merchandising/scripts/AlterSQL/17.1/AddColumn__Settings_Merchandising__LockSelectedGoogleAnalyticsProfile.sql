IF NOT EXISTS (SELECT * FROM syscolumns 
	WHERE id=OBJECT_ID('settings.Merchandising') 
	AND name='LockSelectedGoogleAnalyticsProfile')
BEGIN
	ALTER TABLE settings.Merchandising
	ADD LockSelectedGoogleAnalyticsProfile BIT CONSTRAINT DF_LockSelectedGoogleAnalyticsProfile DEFAULT 0 NOT NULL
END
GO
