CREATE INDEX
IX_LoaderNotes_writtenOn
ON builder.LoaderNotes (writtenOn ASC)