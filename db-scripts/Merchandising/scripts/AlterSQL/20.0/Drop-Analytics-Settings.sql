
alter table settings.Merchandising drop constraint DF_EnableMaxAnalyticsVehicleActivity

alter table Merchandising.settings.Merchandising
drop column EnableMaxAnalyticsVehicleActivity

alter table settings.Merchandising drop constraint DF_EnableMaxAnalyticsOnline

alter table Merchandising.settings.Merchandising
drop column EnableMaxAnalyticsOnline

alter table settings.Merchandising drop constraint DF_EnableMaxAnalyticsPerformanceSummary

alter table Merchandising.settings.Merchandising
drop column EnableMaxAnalyticsPerformanceSummary



