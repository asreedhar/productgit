

-- turn off Toyota and Ford from fetch autoload

UPDATE merchandising.VehicleAutoLoadSettings 
	SET IsAutoLoadEnabled = 0
	WHERE ManufacturerID IN (9, 16) -- ford and toyota

