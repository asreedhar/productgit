ALTER TABLE builder.optionsConfiguration
ADD lotDataImportLock [bit] NOT NULL DEFAULT(0)

ALTER TABLE audit.optionsConfiguration
ADD lotDataImportLock [bit] NOT NULL DEFAULT(0)

ALTER TABLE audit.optionsConfiguration
ALTER COLUMN lotDataImportLock [bit] NOT NULL

 