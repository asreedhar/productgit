
-----additional info categories
ALTER TABLE builder.AdditionalInfoCategories
DROP CONSTRAINT PK_AdditionalInfoCategories

ALTER TABLE builder.AdditionalInfoCategories
ADD CONSTRAINT PK_AdditionalInfoCategories
PRIMARY KEY NONCLUSTERED (infoId)

CREATE CLUSTERED 
INDEX NDX_AdditionalInfoCategories
ON builder.AdditionalInfoCategories (businessUnitId)

--default cert features

ALTER TABLE builder.CertificationFeatures
DROP CONSTRAINT PK_CertFeatures

ALTER TABLE builder.CertificationFeatures
ADD CONSTRAINT PK_CertificationFeatures
PRIMARY KEY CLUSTERED (businessUnitId, certificationTypeId, certificationFeatureId)

--all certified programs
ALTER TABLE builder.ManufacturerCertifications
ADD CONSTRAINT PK_ManufacturerCertifications
PRIMARY KEY CLUSTERED (certificationTypeId)

--types of features available
ALTER TABLE builder.CertificationFeatureTypes
ADD CONSTRAINT PK_CertificationFeatureTypes
PRIMARY KEY CLUSTERED (certificationFeatureTypeId)

--dealer cert feature settings
ALTER TABLE settings.DealerCertificationConfiguration
ADD CONSTRAINT PK_DealerCertificationConfiguration
PRIMARY KEY CLUSTERED (businessUnitId, certificationTypeId, certificationFeatureId)


------------certified foreign keys
DELETE dcc FROM 
settings.DealerCertificationConfiguration dcc
LEFT JOIN builder.certificationFeatures cf
ON cf.certificationFeatureId = dcc.certificationFeatureId
WHERE
cf.certificationFeatureId IS NULL

ALTER TABLE builder.CertificationFeatures
ADD CONSTRAINT FK_CertificationFeatures_CertificationTypeId
FOREIGN KEY (certificationTypeId)
REFERENCES builder.manufacturerCertifications (certificationTypeId)

ALTER TABLE builder.CertificationFeatures
ADD CONSTRAINT FK_CertificationFeatures_CertificationFeatureTypeId
FOREIGN KEY (certificationFeatureTypeId)
REFERENCES builder.certificationFeatureTypes (certificationFeatureTypeId)

-------dealer custom images
ALTER TABLE settings.DealerCustomImages
DROP CONSTRAINT PK_DealerCustomImage

ALTER TABLE settings.DealerCustomImages
ADD CONSTRAINT PK_DealerCustomImage
PRIMARY KEY NONCLUSTERED (dealerCustomImageId)

CREATE CLUSTERED 
INDEX NDX_DealerCustomImages
ON settings.DealerCustomImages (businessUnitId)

------equipment display rankings
ALTER TABLE settings.EquipmentDisplayRankings
DROP CONSTRAINT PK_EquipmentDisplayRankings

ALTER TABLE settings.EquipmentDisplayRankings
ADD CONSTRAINT PK_EquipmentDisplayRankings
PRIMARY KEY NONCLUSTERED (rankingID)

--cluster on buid and categoryId, b/c most searches are a join on these.  some searches only on buid
CREATE CLUSTERED 
INDEX NDX_EquipmentDisplayRankings_BusinessUnitIdCategoryId
ON settings.EquipmentDisplayRankings (businessUnitId,categoryId)

------
CREATE CLUSTERED 
INDEX NDX_GasMileagePreferences
ON settings.GasMileagePreferences (businessUnitId)

----------lot locations
ALTER TABLE settings.LotLocations
DROP CONSTRAINT PK_LotLocations

ALTER TABLE settings.LotLocations
ADD CONSTRAINT PK_LotLocations
PRIMARY KEY NONCLUSTERED (lotLocationId)

CREATE CLUSTERED INDEX
NDX_LotLocations_BusinessUnitId
on settings.LotLocations (businessUnitId)

------------photo templates
-------photo templates
ALTER TABLE settings.PhotoTemplates
DROP CONSTRAINT PK_PhotoTemplateItems

ALTER TABLE settings.PhotoTemplates
ADD CONSTRAINT PK_PhotoTemplateItems
PRIMARY KEY NONCLUSTERED (photoTemplateItemId)

ALTER TABLE settings.PhotoTemplates
ADD CONSTRAINT FK_PhotoTemplateItems_ItemId
FOREIGN KEY (photoTemplateItemTypeId)
REFERENCES settings.PhotoTemplateItemTypes (photoTemplateItemTypeId)

CREATE CLUSTERED INDEX
NDX_PhotoTemplates_BusinessUnitId
ON settings.PhotoTemplates (businessUnitId)

------required photos
--NO PK by default
--ALTER TABLE settings.RequiredPhotos
--DROP CONSTRAINT PK_RequiredPhotos

ALTER TABLE settings.RequiredPhotos
ADD CONSTRAINT PK_RequiredPhotos
PRIMARY KEY NONCLUSTERED (requiredPhotoId)

CREATE CLUSTERED INDEX
NDX_RequiredPhotos_BusinessUnitId
on settings.RequiredPhotos (businessUnitId)

-----------
