ALTER TABLE builder.VehicleDetailedOptions
ALTER COLUMN detailText varchar(800) NULL

--update the detail text to include the option text per the new way the system operates...
update builder.vehicledetailedoptions
set detailText = ltrim(optionText + ' ') + detailText