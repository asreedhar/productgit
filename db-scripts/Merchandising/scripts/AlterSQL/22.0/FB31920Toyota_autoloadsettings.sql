-- FB 31920 toyota site and autoload

if EXISTS (
		SELECT * 
			FROM settings.credentialedSites
			WHERE siteId = 10
		)
	begin
		UPDATE merchandising.VehicleAutoLoadSettings
			SET siteID = null, isAutoLoadEnabled = 0
			WHERE siteID = 10
			
		DELETE builder.BuildRequestLog
			WHERE siteID = 10

		DELETE settings.Dealer_SiteCredentials
			WHERE siteID = 10
	
		DELETE settings.credentialedSites
			WHERE siteId = 10
	end
go
-- insert new site
SET IDENTITY_INSERT settings.credentialedSites ON

INSERT INTO settings.credentialedSites
	(siteId, siteName, DealerCodeRequired)
	VALUES(10, 'Dealer Daily', 0)
go
	
SET IDENTITY_INSERT settings.credentialedSites OFF
go
	
-- set VehicleAutoLoadSettings site for audi
UPDATE merchandising.VehicleAutoLoadSettings
	SET siteId = 10, isAutoLoadEnabled = 1
	WHERE ManufacturerID = 16
