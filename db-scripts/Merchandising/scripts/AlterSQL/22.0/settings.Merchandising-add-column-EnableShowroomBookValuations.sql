-- if column default ... remove it
if EXISTS (SELECT * 
				FROM sysobjects
				WHERE TYPE = 'D'
				AND NAME = 'DF_merchandising_EnableShowroomBookValuationsr')
	begin
		ALTER TABLE settings.merchandising
			DROP CONSTRAINT DF_merchandising_EnableShowroomBookValuations
	end
	
-- if column exists ... remove it
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'EnableShowroomBookValuations'
				AND object_id = object_id('settings.merchandising'))
	begin
		ALTER TABLE settings.merchandising
			DROP COLUMN EnableShowroomBookValuations
	end

-- add column to allow Showroom to enable book valuations
ALTER TABLE settings.merchandising
	ADD EnableShowroomBookValuations bit NOT NULL CONSTRAINT DF_merchandising_EnableShowroomBookValuations DEFAULT 0
GO

