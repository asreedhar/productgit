alter table settings.DealerProfilePublishedSnapshot
	add HasShowroomBookValuations bit not null constraint DF_settings_HasShowroomBookValuations default 0
go
