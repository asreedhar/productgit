IF NOT EXISTS (SELECT *
					FROM builder.AutoloadStatusTypes
					WHERE statusTypeId = -9)
	begin
		INSERT INTO builder.AutoloadStatusTypes
			(statusTypeId, statusType, statusTypeDescription, requeueMinutes)
			VALUES('-9', 'Delayed', 'Delayed for batch processing', -30)
	end
