
-- drop default constraint on batchProcessOnly
IF EXISTS (SELECT * 
				FROM sysobjects
				WHERE NAME ='DF_credentialedSites_batchProcessOnly'
				AND xtype = 'd')
	begin
		ALTER TABLE settings.credentialedSites
			DROP CONSTRAINT DF_credentialedSites_batchProcessOnly
	end
go


-- drop default constraint on maxRequestPerCall
IF EXISTS (SELECT * 
				FROM sysobjects
				WHERE NAME ='DF_credentialedSites_maxRequestsPerCall'
				AND xtype = 'd')
	begin
		ALTER TABLE settings.credentialedSites
			DROP CONSTRAINT DF_credentialedSites_maxRequestsPerCall
	end
go


-- drop column batchProcessOnly
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'batchProcessOnly'
				AND object_id = object_id('settings.credentialedSites'))
	begin
		ALTER TABLE settings.credentialedSites
			DROP COLUMN batchProcessOnly
	end
go

-- drop column maxRequestsPerCall
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'maxRequestsPerCall'
				AND object_id = object_id('settings.credentialedSites'))
	begin
		ALTER TABLE settings.credentialedSites
			DROP COLUMN maxRequestsPerCall
	end
go

-- drop column maxCallsPerBatch
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'maxCallsPerBatch'
				AND object_id = object_id('settings.credentialedSites'))
	begin
		ALTER TABLE settings.credentialedSites
			DROP COLUMN maxCallsPerBatch
	end
go


-- add column for buildRequestType
ALTER TABLE settings.credentialedSites
	ADD batchProcessOnly BIT NOT NULL
	CONSTRAINT DF_credentialedSites_batchProcessOnly DEFAULT 0
GO

-- add column for maxRequestsPerCall
ALTER TABLE settings.credentialedSites
	ADD maxRequestsPerCall int not null
	CONSTRAINT DF_credentialedSites_maxRequestsPerCall DEFAULT 1
GO

-- add column for maxCallsPerBatch
ALTER TABLE settings.credentialedSites
	ADD maxCallsPerBatch int null
	
GO

UPDATE settings.credentialedSites
	SET maxRequestsPerCall = 20, maxCallsPerBatch = 1
	WHERE siteId = 9 -- hyundai

UPDATE settings.credentialedSites
	SET maxRequestsPerCall = 20, maxCallsPerBatch = 1, batchProcessOnly = 1
	WHERE siteId = 10 -- toyota (dealer daily)
GO


