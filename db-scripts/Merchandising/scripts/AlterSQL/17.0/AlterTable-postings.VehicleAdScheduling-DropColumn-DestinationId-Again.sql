if exists(select * from sys.columns 
            where Name = N'destinationId' and Object_ID = Object_ID(N'Merchandising.postings.VehicleAdScheduling'))
	BEGIN
		ALTER TABLE Merchandising.postings.VehicleAdScheduling
		DROP COLUMN [destinationID]
	END
GO


EXEC sp_refreshview 'workflow.Inventory'
GO

EXEC sp_refreshview 'Reports.VehiclesOnline'
GO

EXEC sp_refreshview 'Reports.VehiclesOnlineInfo'
GO
