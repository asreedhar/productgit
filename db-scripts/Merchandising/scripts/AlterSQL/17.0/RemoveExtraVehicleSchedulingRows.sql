; with lastAd as
(
      select      
            businessUnitID,
            inventoryId,
            max(schedulingID) as lastSchedulingId
      from Merchandising.postings.VehicleAdScheduling
      group by
            businessUnitID,
            inventoryId
)

delete ads
from Merchandising.postings.VehicleAdScheduling ads
where not exists
      (
            select * from lastAd
            where
                  businessUnitID = ads.businessUnitID
                  and inventoryId = ads.inventoryId
                  and lastSchedulingId = ads.schedulingID
      )
