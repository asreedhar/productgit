-- Add LandRover to the MaxAd CPO system
-- FB 14145 - 2011-02-21 - dh

DECLARE @cpoGroupId INT
DECLARE @certificationTypeId INT

-- create the LandRover cpoGroup (all programs must be in a group)
INSERT INTO settings.cpoGroups
    ( name )
VALUES  ( 'LandRover'  )

SET @cpoGroupId = SCOPE_IDENTITY()

-- create the certified program itself
INSERT INTO builder.manufacturerCertifications
    ( name )
VALUES  ( 'LandRover' )

SET @certificationTypeId = SCOPE_IDENTITY()

INSERT INTO settings.cpoCertifieds
    ( cpoGroupId, certificationTypeId )
VALUES  ( @cpoGroupId, @certificationTypeId )
