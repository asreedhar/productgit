-- Must insert a zero record in order for the following constraints to work.
SET IDENTITY_INSERT Merchandising.settings.EdtDestinations ON
INSERT INTO Merchandising.settings.EdtDestinations (destinationID, description, maxDescriptionLength, maxPhotoCount, acceptsDescriptions, acceptsOptions, acceptsPhotos, acceptsVideos, canReleaseTo, canCollectMetricsFrom, active, isDealerWebsite)
VALUES(0,'Undefined',0,0,0,0,0,0,0,0,0,0)
SET IDENTITY_INSERT Merchandising.settings.EdtDestinations OFF

ALTER TABLE [settings].[GoogleAnalyticsProfile]  WITH CHECK ADD  CONSTRAINT [FK_GoogleAnalyticsProfile_DestinationId] FOREIGN KEY([DestinationId])
REFERENCES [settings].[EdtDestinations] ([destinationID])
GO

ALTER TABLE [settings].[GoogleAnalyticsProfile] CHECK CONSTRAINT [FK_GoogleAnalyticsProfile_DestinationId]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Link to settings.EdtDestinations destinationID' , @level0type=N'SCHEMA',@level0name=N'settings', @level1type=N'TABLE',@level1name=N'GoogleAnalyticsProfile', @level2type=N'CONSTRAINT',@level2name=N'FK_GoogleAnalyticsProfile_DestinationId'
GO

