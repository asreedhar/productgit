TRUNCATE TABLE [Merchandising].[settings].[EdtDestinationQueries]
GO


INSERT INTO [Merchandising].[settings].[EdtDestinationQueries]
           ([EdtDestinationId]
           ,[GoogleQueryId])
     VALUES
           (4,  (SELECT QueryId FROM settings.GoogleQuery WHERE QueryName = 'Dealer.com-VDP-New')),
           (4,  (SELECT QueryId FROM settings.GoogleQuery WHERE QueryName = 'Dealer.com-VDP-Both')),
           (4,  (SELECT QueryId FROM settings.GoogleQuery WHERE QueryName = 'Dealer.com-VDP-Used'))
GO
