if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[FordAutoLoadNew_DealerCodes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[FordAutoLoadNew_DealerCodes]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[FordAutoLoadNew_DealerCodes](
	[businessUnitID] [int] NOT NULL,
	[dealerPaCode] [varchar] (20) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [varchar](32) NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [varchar](32) NULL,

	CONSTRAINT [PK_FordAutoLoadNew_DealerCodes] PRIMARY KEY CLUSTERED 
	(
		[businessUnitID] ASC
	)
)

GO
SET ANSI_PADDING OFF 