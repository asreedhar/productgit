-------- Begin ADD GoogleQuery Section ----------------------------------
IF (
	(SELECT COUNT(*) FROM Merchandising.settings.GoogleQuery WHERE [QueryName] = 'Cobalt-VDP-New' OR [QueryName] = 'Cobalt-VDP-Used' OR [QueryName] = 'CObalt-VDP-Both') = 0
)
BEGIN	
	INSERT INTO [Merchandising].[settings].[GoogleQuery]
           ([QueryType],[QueryName],[Dimensions],[Filter],[Limit],[Metrics],[Segment],[Sort])
     VALUES
           ( (SELECT QueryTypeId FROM settings.GoogleQueryType WHERE Name='VDP-New'), 'Cobalt-VDP-New' , 'ga:year,ga:month', 'ga:pagePath=~[/]new-.+[/][0-9]{4\,}', 25, 'ga:visits','gaid::-1', 'ga:year,ga:month'),
           ( (SELECT QueryTypeId FROM settings.GoogleQueryType WHERE Name='VDP-Used'), 'Cobalt-VDP-Used', 'ga:year,ga:month', 'ga:pagePath=~[/]used-.+[/][0-9]{4\,}|[/]certified-.+[/][0-9]{4\,}', 25, 'ga:visits','gaid::-1', 'ga:year,ga:month'),
           ( (SELECT QueryTypeId FROM settings.GoogleQueryType WHERE Name='VDP-Both'), 'Cobalt-VDP-Both', 'ga:year,ga:month', 'ga:pagePath=~[/]new-.+[/][0-9]{4\,}|[/]used-.+[/][0-9]{4\,}|[/]certified-.+[/][0-9]{4\,}', 25, 'ga:visits','gaid::-1', 'ga:year,ga:month')
END
GO
-------- End ADD GoogleQuery Section ------------------------------------

-------- Begin ADD EdtDestinationQueries Section ------------------------

DECLARE @DestId int;
SELECT @DestId = destinationID FROM settings.EdtDestinations WHERE description = 'Cobalt';

IF (
	(SELECT COUNT(*) FROM Merchandising.settings.EdtDestinationQueries WHERE GoogleQueryId IN (
		SELECT QueryId FROM Merchandising.settings.GoogleQuery WHERE [QueryName] LIKE 'Cobalt-VDP%'
	)) = 0
)
BEGIN
	INSERT INTO [Merchandising].[settings].[EdtDestinationQueries]
           ([EdtDestinationId]
           ,[GoogleQueryId])
     VALUES
           (@DestId,  (SELECT QueryId FROM settings.GoogleQuery WHERE QueryName = 'Cobalt-VDP-New')),
           (@DestId,  (SELECT QueryId FROM settings.GoogleQuery WHERE QueryName = 'Cobalt-VDP-Both')),
           (@DestId,  (SELECT QueryId FROM settings.GoogleQuery WHERE QueryName = 'Cobalt-VDP-Used'))
END
GO

-------- End ADD EdtDestinationQueries Section --------------------------


UPDATE settings.EdtDestinations SET hasGoogleVdps = 1 WHERE description = 'Cobalt';
GO
