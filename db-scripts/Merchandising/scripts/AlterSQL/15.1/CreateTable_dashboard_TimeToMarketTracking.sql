IF NOT EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[dashboard].[TimeToMarketTracking]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [Merchandising].[dashboard].[TimeToMarketTracking](
    [BusinessUnitId] [int] NOT NULL,
    [InventoryId] [int] NOT NULL,
    [Inserted]	[DateTime] NOT NULL CONSTRAINT [Default_dashboard.TimeToMarketInserted] DEFAULT CURRENT_TIMESTAMP,
    [Updated]	[DateTime] NOT NULL CONSTRAINT [Default_dashboard.TimeToMarketUpdated] DEFAULT CURRENT_TIMESTAMP,
    [GIDFirstDate]         [DateTime] NULL,
    [PhotosFirstDate]      [DateTime] NULL,
    [DescriptionFirstDate] [DateTime] NULL,
    [PriceFirstDate]       [DateTime] NULL,
    [AdApprovalFirstDate]  [DateTime] NULL,
    [CompleteAdFirstDate]  [DateTime] NULL,
    CONSTRAINT [PK_dashboard.TimeToMarketTracking] PRIMARY KEY CLUSTERED
    (
        [BusinessUnitId] ASC,
        [InventoryId] ASC
    )
) ON [PRIMARY]

END
