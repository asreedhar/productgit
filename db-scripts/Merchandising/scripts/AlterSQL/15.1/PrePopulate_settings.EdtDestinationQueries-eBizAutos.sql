-------- Begin ADD GoogleQuery Section ----------------------------------
IF NOT EXISTS(
	SELECT COUNT(*) FROM Merchandising.settings.GoogleQuery WHERE [QueryName] = 'eBizAutos-VDP-New' OR [QueryName] = 'eBizAutos-VDP-Used' OR [QueryName] = 'eBizAutos-VDP-Both'
)
BEGIN	
	INSERT INTO [Merchandising].[settings].[GoogleQuery]
           ([QueryType],[QueryName],[Dimensions],[Filter],[Limit],[Metrics],[Segment],[Sort])
     VALUES
           ( (SELECT QueryTypeId FROM settings.GoogleQueryType WHERE Name='VDP-New'), 'eBizAutos-VDP-New' , 'ga:year,ga:month', 'ga:pagePath=~[/]detail-.+new-[0-9]{4\,}\.html', 25, 'ga:visitors','gaid::-1', 'ga:year,ga:month'),
           ( (SELECT QueryTypeId FROM settings.GoogleQueryType WHERE Name='VDP-Used'), 'eBizAutos-VDP-Used', 'ga:year,ga:month', 'ga:pagePath=~[/]detail-.+used-[0-9]{4\,}\.html', 25, 'ga:visitors','gaid::-1', 'ga:year,ga:month'),
           ( (SELECT QueryTypeId FROM settings.GoogleQueryType WHERE Name='VDP-Both'), 'eBizAutos-VDP-Both', 'ga:year,ga:month', 'ga:pagePath=~[/]detail-.+new-[0-9]{4\,}\.html|[/]detail-.+used-[0-9]{4\,}\.html', 25, 'ga:visitors','gaid::-1', 'ga:year,ga:month')
END
GO
-------- End ADD GoogleQuery Section ------------------------------------

-------- Begin ADD EdtDestinationQueries Section ------------------------

DECLARE @DestId int;
SELECT @DestId = destinationID FROM settings.EdtDestinations WHERE description = 'eBizAutos';

IF NOT EXISTS(
	SELECT COUNT(*) FROM Merchandising.settings.EdtDestinationQueries WHERE GoogleQueryId IN (
		SELECT QueryId FROM Merchandising.settings.GoogleQuery WHERE [QueryName] LIKE 'eBizAutos-VDP%'
	)
)
BEGIN
	INSERT INTO [Merchandising].[settings].[EdtDestinationQueries]
           ([EdtDestinationId]
           ,[GoogleQueryId])
     VALUES
           (@DestId,  (SELECT QueryId FROM settings.GoogleQuery WHERE QueryName = 'eBizAutos-VDP-New')),
           (@DestId,  (SELECT QueryId FROM settings.GoogleQuery WHERE QueryName = 'eBizAutos-VDP-Both')),
           (@DestId,  (SELECT QueryId FROM settings.GoogleQuery WHERE QueryName = 'eBizAutos-VDP-Used'))
END
GO

-------- End ADD EdtDestinationQueries Section --------------------------


UPDATE settings.EdtDestinations SET hasGoogleVdps = 1 WHERE description = 'eBizAutos';
GO
