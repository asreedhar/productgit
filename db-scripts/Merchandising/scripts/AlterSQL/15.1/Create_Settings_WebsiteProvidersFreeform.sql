USE [Merchandising]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [settings].[WebsiteProvidersFreeform](
	[WebsiteProviderID] [int] IDENTITY(-1000,-1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_settings_WebsiteProvidersFreeform] PRIMARY KEY CLUSTERED 
(
	[WebsiteProviderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



