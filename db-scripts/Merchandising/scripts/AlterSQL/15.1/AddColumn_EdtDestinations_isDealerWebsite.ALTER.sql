ALTER TABLE settings.EdtDestinations
ADD isDealerWebsite bit NOT NULL
CONSTRAINT DV_settings_EdtDestinations_isDealerWebsite DEFAULT 0
GO

UPDATE settings.EdtDestinations SET isDealerWebsite = 1 WHERE destinationID IN (4,5)
GO

INSERT INTO settings.EdtDestinations
([description],[maxDescriptionLength],[maxPhotoCount],[acceptsDescriptions],[acceptsOptions],[acceptsPhotos],[acceptsVideos],[canReleaseTo],[canCollectMetricsFrom],[active],[isDealerWebsite])
VALUES
('Cobalt',     0,0,0,0,0,0,0,0,0,1)
GO

INSERT INTO settings.EdtDestinations
([description],[maxDescriptionLength],[maxPhotoCount],[acceptsDescriptions],[acceptsOptions],[acceptsPhotos],[acceptsVideos],[canReleaseTo],[canCollectMetricsFrom],[active],[isDealerWebsite])
VALUES
('ClickMotive',0,0,0,0,0,0,0,0,0,1)
GO

