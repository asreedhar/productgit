IF NOT EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'DestinationId' AND Object_ID = Object_ID(N'settings.GoogleAnalyticsProfile'))
BEGIN    
ALTER TABLE settings.GoogleAnalyticsProfile
	ADD DestinationId INT CONSTRAINT DF_DestinationId DEFAULT 0 NOT NULL
END
GO
