USE [Merchandising]
GO
/****** Object:  StoredProcedure [settings].[MiscSettings#Update]    Script Date: 04/23/2013 09:06:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER procedure [settings].[MiscSettings#Update]
	@businessUnitID int,
	@sendOptimalFormat bit,
	@franchiseId int,
	@priceNewCars bit,
	@showDashboard bit,
	@showGroupLevelDashboard bit,
	@analyticsSuite bit,
	@batchAutoload bit,
	@MaxVersion tinyint = 2,
	@WebLoaderEnabled bit = 0,
	@ModelLevelFrameworksEnabled bit = 0,
	@AutoOffline_WholesalePlanTrigger bit = null,
	@ShowCtrGraph bit,
	@DealershipSegment tinyint = 0
as

set nocount on
set transaction isolation level read uncommitted

if not exists(select * from Merchandising.settings.Merchandising where businessUnitID = @businessUnitID)
begin
	raiserror('settings.MiscSettings#Update proc: Dealer not set up!', 16, 1)
	return 1
end

update Merchandising.settings.Merchandising
set sendOptimalFormat = @sendOptimalFormat, 
    franchiseId = @franchiseId, 
    priceNewCars = @priceNewCars,
    showDashboard = @showDashboard,
    showGroupLevelDashboard = @showGroupLevelDashboard,
    analyticsSuite = @analyticsSuite,
    batchAutoload = @batchAutoload,
    MaxVersion = @MaxVersion,
    WebloaderEnabled = @WebloaderEnabled,
    ModelLevelFrameworksEnabled = @ModelLevelFrameworksEnabled,
    AutoOffline_WholesalePlanTrigger = isnull(@AutoOffline_WholesalePlanTrigger, AutoOffline_WholesalePlanTrigger),
	ShowCtrGraph = @ShowCtrGraph,
	DealershipSegment = @DealershipSegment
    
where businessUnitID = @businessUnitID

return 0

