IF EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'VehicleType' AND Object_ID = Object_ID(N'settings.GoogleQuery'))
BEGIN    

ALTER TABLE [Merchandising].[settings].[GoogleQuery]
DROP COLUMN VehicleType

END

