
IF NOT EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'hasGoogleVdps' AND Object_ID = Object_ID(N'settings.EdtDestinations'))
    BEGIN
        ALTER TABLE settings.EdtDestinations
        ADD hasGoogleVdps bit NOT NULL
        CONSTRAINT DV_settings_EdtDestinations_hasGoogleVdps DEFAULT 0
END
GO
UPDATE settings.EdtDestinations SET hasGoogleVdps = 1 WHERE destinationID IN (4)
GO


