if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[FordAutoLoadNew_YearMakeModel]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[FordAutoLoadNew_YearMakeModel]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[FordAutoLoadNew_YearMakeModel](
	[Year] [int] NOT NULL,
	[Make] [varchar](20) NOT NULL,
	[Model] [varchar] (30) NOT NULL,	
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [varchar](32) NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[UpdatedBy] [varchar](32) NULL,

	CONSTRAINT [PK_FordAutoLoadNew_YearMakeModel] PRIMARY KEY CLUSTERED 
	(
		[Year] ASC,[Make],[Model]
	)
)

GO
SET ANSI_PADDING OFF 