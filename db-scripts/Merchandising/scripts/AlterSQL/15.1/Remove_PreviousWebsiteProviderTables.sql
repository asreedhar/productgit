IF OBJECT_ID('[dashboard].[WebsiteMetrics]','U') IS NOT NULL
DROP TABLE [dashboard].[WebsiteMetrics]
GO

IF OBJECT_ID('[settings].[WebsiteProviders]','U') IS NOT NULL
DROP TABLE [settings].[WebsiteProviders]
GO

IF OBJECT_ID('[settings].[WebsiteProviderCredentials]','U') IS NOT NULL
DROP TABLE [settings].[WebsiteProviderCredentials]
GO

