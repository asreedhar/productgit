USE [Merchandising]
GO

/****** Object:  Table [settings].[GoogleQueryType]    Script Date: 05/01/2013 13:57:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [settings].[GoogleQueryType](
	[QueryTypeId] [int] IDENTITY (1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_GoogleQueryType] PRIMARY KEY CLUSTERED 
(
	[QueryTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


