USE [Merchandising]
GO

/****** Object:  Table [settings].[GoogleQuery]    Script Date: 05/01/2013 13:57:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [settings].[GoogleQuery](
	[QueryId] [int] IDENTITY(1,1) NOT NULL,
	[QueryType] [int] NOT NULL,
	[QueryName] [varchar](50) NOT NULL,
	[Dimensions] [varchar](255) NULL,
	[Filter] [varchar](255) NULL,
	[Limit] [int] NULL,
	[Metrics] [varchar](255) NULL,
	[Segment] [varchar](255) NULL,
	[Sort] [varchar](255) NULL,
 CONSTRAINT [PK_settings.GoogleQuery] PRIMARY KEY CLUSTERED 
(
	[QueryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [settings].[GoogleQuery]  WITH CHECK ADD  CONSTRAINT [FK_settings.GoogleQuery_settings.GoogleQuery] FOREIGN KEY([QueryType])
REFERENCES [settings].[GoogleQueryType] ([QueryTypeId])
GO

ALTER TABLE [settings].[GoogleQuery] CHECK CONSTRAINT [FK_settings.GoogleQuery_settings.GoogleQuery]
GO


