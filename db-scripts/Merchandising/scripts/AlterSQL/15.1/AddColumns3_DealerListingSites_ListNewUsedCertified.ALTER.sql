-- USE Merchandising

IF COL_LENGTH('settings.DealerListingSites','ListNew') IS NULL
	ALTER TABLE settings.DealerListingSites
	ADD ListNew bit NOT NULL
	CONSTRAINT DV_ListNew DEFAULT 1;

IF COL_LENGTH('settings.DealerListingSites','ListUsed') IS NULL
	ALTER TABLE settings.DealerListingSites
	ADD ListUsed bit NOT NULL
	CONSTRAINT DV_ListUsed DEFAULT 1;

IF COL_LENGTH('settings.DealerListingSites','ListCertified') IS NULL
	ALTER TABLE settings.DealerListingSites
	ADD ListCertified bit NOT NULL
	CONSTRAINT DV_ListCertified DEFAULT 1;

