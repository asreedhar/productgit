USE [Merchandising]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[settings].[FK_EdtDestinationQueries_Destinations]') AND parent_object_id = OBJECT_ID(N'[settings].[EdtDestinationQueries]'))
    ALTER TABLE [settings].[EdtDestinationQueries] DROP CONSTRAINT [FK_EdtDestinationQueries_Destinations]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[settings].[FK_EdtDestinationQueries_GoogleQuery]') AND parent_object_id = OBJECT_ID(N'[settings].[EdtDestinationQueries]'))
    ALTER TABLE [settings].[EdtDestinationQueries] DROP CONSTRAINT [FK_EdtDestinationQueries_GoogleQuery]
GO

/****** Object:  Table [settings].[EdtDestinationQueries]    Script Date: 05/08/2013 10:43:22 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[settings].[EdtDestinationQueries]') AND type in (N'U'))
    DROP TABLE [settings].[EdtDestinationQueries]
GO

/****** Object:  Table [settings].[EdtDestinationQueries]    Script Date: 05/01/2013 13:59:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [settings].[EdtDestinationQueries](
	[EdtDestinationId] [int] NOT NULL,
	[GoogleQueryId] [int] NOT NULL,
 CONSTRAINT [PK_EdtDestinationQueries] PRIMARY KEY CLUSTERED 
(
	[EdtDestinationId] ASC,
	[GoogleQueryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [settings].[EdtDestinationQueries]  WITH CHECK ADD  CONSTRAINT [FK_EdtDestinationQueries_Destinations] FOREIGN KEY([EdtDestinationId])
REFERENCES [settings].[EdtDestinations] ([destinationID])
GO

ALTER TABLE [settings].[EdtDestinationQueries] CHECK CONSTRAINT [FK_EdtDestinationQueries_Destinations]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Link to settings.EdtDestinations' , @level0type=N'SCHEMA',@level0name=N'settings', @level1type=N'TABLE',@level1name=N'EdtDestinationQueries', @level2type=N'CONSTRAINT',@level2name=N'FK_EdtDestinationQueries_Destinations'
GO

ALTER TABLE [settings].[EdtDestinationQueries]  WITH CHECK ADD  CONSTRAINT [FK_EdtDestinationQueries_GoogleQuery] FOREIGN KEY([GoogleQueryId])
REFERENCES [settings].[GoogleQuery] ([QueryId])
GO

ALTER TABLE [settings].[EdtDestinationQueries] CHECK CONSTRAINT [FK_EdtDestinationQueries_GoogleQuery]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Link to settings.GoogleQuery Id' , @level0type=N'SCHEMA',@level0name=N'settings', @level1type=N'TABLE',@level1name=N'EdtDestinationQueries', @level2type=N'CONSTRAINT',@level2name=N'FK_EdtDestinationQueries_GoogleQuery'
GO


