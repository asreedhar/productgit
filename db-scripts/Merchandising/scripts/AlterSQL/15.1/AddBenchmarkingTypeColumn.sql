IF NOT EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'benchmarkingType' AND Object_ID = Object_ID(N'settings.Merchandising'))
BEGIN    
ALTER TABLE settings.Merchandising 
	ADD benchmarkingType TINYINT CONSTRAINT DF_BenchmarkingType DEFAULT 0 NOT NULL
END
GO
