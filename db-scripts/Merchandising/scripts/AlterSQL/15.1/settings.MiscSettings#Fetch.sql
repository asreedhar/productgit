USE [Merchandising]
GO
/****** Object:  StoredProcedure [settings].[MiscSettings#Fetch]    Script Date: 04/22/2013 17:05:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER procedure [settings].[MiscSettings#Fetch]
	@businessUnitID int
as

set nocount on
set transaction isolation level read uncommitted

select sendOptimalFormat, franchiseId, priceNewCars, showDashboard, ShowGroupLevelDashboard, analyticsSuite, batchAutoload,
    MaxVersion, WebLoaderEnabled, ModelLevelFrameworksEnabled, AutoOffline_WholesalePlanTrigger, ShowCtrGraph, DealershipSegment
from Merchandising.settings.Merchandising
where businessUnitID = @businessUnitID

