IF NOT EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'SelectedWebsiteProvider' AND Object_ID = Object_ID(N'settings.Merchandising'))
BEGIN    

ALTER TABLE [Merchandising].[settings].[Merchandising]
ADD SelectedWebsiteProvider int NULL

END