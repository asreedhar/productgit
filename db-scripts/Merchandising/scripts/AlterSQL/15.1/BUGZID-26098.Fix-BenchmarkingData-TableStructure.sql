USE Merchandising
GO

IF EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[dbo].[BenchmarkingData]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN
	ALTER TABLE [dbo].[BenchmarkingData]
	ALTER COLUMN [ImpressionCost] decimal(18,2) NULL;
	ALTER TABLE [dbo].[BenchmarkingData]
	ALTER COLUMN [LeadCost] decimal(18,2) NULL
END
GO
