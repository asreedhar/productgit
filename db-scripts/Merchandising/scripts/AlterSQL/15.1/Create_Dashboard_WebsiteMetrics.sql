USE [Merchandising]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dashboard].[WebsiteMetrics](
	[BusinessUnitID] [int] NOT NULL,
	[WebsiteProviderID] [int] NOT NULL,
	[MonthApplicable] [datetime] NOT NULL,
	[UsedVDPs] [int] NOT NULL,
	[NewVDPs] [int] NOT NULL
 CONSTRAINT [PK_dashboard_WebsiteMetrics] PRIMARY KEY CLUSTERED 
(
	[BusinessUnitID] ASC,
	[WebsiteProviderID] ASC,
	[MonthApplicable] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


