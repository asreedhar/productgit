IF NOT EXISTS (SELECT * FROM sys.types WHERE name = 'StringValTableType' AND is_table_type = 1)
BEGIN
    CREATE TYPE dbo.StringValTableType AS TABLE
    (
        StringValue varchar(max)
    )
END
GO
GRANT EXECUTE ON TYPE::dbo.StringValTableType TO MerchandisingUser
GO
