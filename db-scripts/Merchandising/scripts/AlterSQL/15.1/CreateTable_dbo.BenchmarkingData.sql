USE Merchandising
GO

IF NOT EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[dbo].[BenchmarkingData]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[BenchmarkingData](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Year] int NOT NULL,
	[Month] int NOT NULL,
	[ImpressionCost] decimal(18,2) NULL,
	[LeadCost] decimal(18,2) NULL,
	[SiteId] tinyint NOT NULL,
	[DealershipSegmentType] tinyint NOT NULL,
	[VehicleType] tinyint NOT NULL
 CONSTRAINT [PK_x] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

END
