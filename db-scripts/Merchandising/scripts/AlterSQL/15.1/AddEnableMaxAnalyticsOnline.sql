ALTER TABLE settings.Merchandising
ADD EnableMaxAnalyticsOnline BIT CONSTRAINT DF_EnableMaxAnalyticsOnline DEFAULT 0 NOT NULL
GO