
INSERT INTO [Merchandising].[settings].[GoogleQuery]
    ([QueryType],[QueryName],[Dimensions],[Filter],[Limit],[Metrics],[Segment],[Sort])
VALUES
	(2,'Dealer.com-VDP-New','ga:year,ga:month','ga:pagePath=~^[/]new[/].+htm',25,'ga:visits','gaid::-1','ga:year,ga:month'),
	(3,'Dealer.com-VDP-Used','ga:year,ga:month','ga:pagePath=~^[/]used[/].+htm|^[/]certified[/].+htm',25,'ga:visits','gaid::-1','ga:year,ga:month'),
	(4,'Dealer.com-VDP-Both','ga:year,ga:month','ga:pagePath=~^[/]new[/].+htm|^[/]used[/].+htm|^[/]certified[/].+htm',25,'ga:visits','gaid::-1','ga:year,ga:month')
GO

