-------- Begin ADD GoogleQuery Section ----------------------------------
IF NOT EXISTS(
	SELECT COUNT(*) FROM Merchandising.settings.GoogleQuery WHERE [QueryName] = 'ClickMotive-VDP-New' OR [QueryName] = 'ClickMotive-VDP-Used' OR [QueryName] = 'ClickMotive-VDP-Both'
)
BEGIN	
	INSERT INTO [Merchandising].[settings].[GoogleQuery]
           ([QueryType],[QueryName],[Dimensions],[Filter],[Limit],[Metrics],[Segment],[Sort])
     VALUES
           ( (SELECT QueryTypeId FROM settings.GoogleQueryType WHERE Name='VDP-New'), 'ClickMotive-VDP-New' , 'ga:year,ga:month', 'ga:pagePath=~[/]For-Sale[/]New[/].+[/][0-9]{4\,}', 25, 'ga:visitors','gaid::-1', 'ga:year,ga:month'),
           ( (SELECT QueryTypeId FROM settings.GoogleQueryType WHERE Name='VDP-Used'), 'ClickMotive-VDP-Used', 'ga:year,ga:month', 'ga:pagePath=~[/]For-Sale[/]Used[/].+[/][0-9]{4\,}', 25, 'ga:visitors','gaid::-1', 'ga:year,ga:month'),
           ( (SELECT QueryTypeId FROM settings.GoogleQueryType WHERE Name='VDP-Both'), 'ClickMotive-VDP-Both', 'ga:year,ga:month', 'ga:pagePath=~[/]For-Sale[/]Used[/].+[/][0-9]{4\,}|[/]For-Sale[/]New[/].+[/][0-9]{4\,}', 25, 'ga:visitors','gaid::-1', 'ga:year,ga:month')
END
GO
-------- End ADD GoogleQuery Section ------------------------------------

-------- Begin ADD EdtDestinationQueries Section ------------------------

DECLARE @DestId int;
SELECT @DestId = destinationID FROM settings.EdtDestinations WHERE description = 'ClickMotive';

IF NOT EXISTS(
	SELECT COUNT(*) FROM Merchandising.settings.EdtDestinationQueries WHERE GoogleQueryId IN (
		SELECT QueryId FROM Merchandising.settings.GoogleQuery WHERE [QueryName] LIKE 'ClickMotive-VDP%'
	)
)
BEGIN
	INSERT INTO [Merchandising].[settings].[EdtDestinationQueries]
           ([EdtDestinationId]
           ,[GoogleQueryId])
     VALUES
           (@DestId,  (SELECT QueryId FROM settings.GoogleQuery WHERE QueryName = 'ClickMotive-VDP-New')),
           (@DestId,  (SELECT QueryId FROM settings.GoogleQuery WHERE QueryName = 'ClickMotive-VDP-Both')),
           (@DestId,  (SELECT QueryId FROM settings.GoogleQuery WHERE QueryName = 'ClickMotive-VDP-Used'))
END
GO

-------- End ADD EdtDestinationQueries Section --------------------------


UPDATE settings.EdtDestinations SET hasGoogleVdps = 1 WHERE description = 'ClickMotive';
GO
