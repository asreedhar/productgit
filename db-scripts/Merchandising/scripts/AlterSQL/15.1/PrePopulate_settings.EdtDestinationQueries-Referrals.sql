
IF NOT EXISTS ( SELECT * FROM Merchandising.settings.GoogleQuery WHERE [QueryName] IN ('Sources','Keywords','Mobile'))
BEGIN
	INSERT [settings].[GoogleQuery] ([QueryType], [QueryName], [Dimensions], [Filter], [Limit], [Metrics], [Segment], [Sort]) VALUES (1, N'Sources', N'ga:source,ga:medium', NULL, 50, N'ga:visits,ga:percentNewVisits,ga:visitBounceRate,ga:avgTimeOnSite,ga:pageviewsPerVisit', NULL, N'-ga:visits')
	INSERT [settings].[GoogleQuery] ([QueryType], [QueryName], [Dimensions], [Filter], [Limit], [Metrics], [Segment], [Sort]) VALUES (1, N'Keywords', N'ga:keyword', NULL, 50, N'ga:visits,ga:percentNewVisits,ga:visitBounceRate,ga:avgTimeOnSite,ga:pageviewsPerVisit', NULL, N'-ga:visits')
	INSERT [settings].[GoogleQuery] ([QueryType], [QueryName], [Dimensions], [Filter], [Limit], [Metrics], [Segment], [Sort]) VALUES (1, N'Mobile', N'ga:mobileDeviceInfo', NULL, 50, N'ga:visits,ga:percentNewVisits,ga:visitBounceRate,ga:avgTimeOnSite,ga:pageviewsPerVisit', NULL, N'-ga:visits')
END
GO

IF NOT EXISTS (
	SELECT * FROM settings.[EdtDestinationQueries] WHERE GoogleQueryId IN (
		SELECT QueryId FROM settings.[GoogleQuery] WHERE [QueryName] IN  ('Sources','Keywords','Mobile')
	)
)
BEGIN
	INSERT [settings].[EdtDestinationQueries] (EdtDestinationId, GoogleQueryId) VALUES (0, (SELECT [QueryId] FROM [settings].[GoogleQuery] WHERE [QueryName] = 'Sources'))
	INSERT [settings].[EdtDestinationQueries] (EdtDestinationId, GoogleQueryId) VALUES (0, (SELECT [QueryId] FROM [settings].[GoogleQuery] WHERE [QueryName] = 'Keywords'))
	INSERT [settings].[EdtDestinationQueries] (EdtDestinationId, GoogleQueryId) VALUES (0, (SELECT [QueryId] FROM [settings].[GoogleQuery] WHERE [QueryName] = 'Mobile'))
END
GO

