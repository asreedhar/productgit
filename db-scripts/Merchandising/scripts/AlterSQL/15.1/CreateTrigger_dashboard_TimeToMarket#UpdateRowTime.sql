USE Merchandising
GO

IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dashboard].[TimeToMarket#UpdateRowTime]'))
DROP TRIGGER [dashboard].[TimeToMarket#UpdateRowTime]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Travis Huber
-- Create date: 2013-04-18
-- Description:	Set the update timestamp
-- =============================================
CREATE TRIGGER TimeToMarket#UpdateRowTime 
   ON dashboard.TimeToMarketTracking 
   AFTER INSERT,UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    UPDATE 
      Merchandising.dashboard.TimeToMarketTracking
	SET 
      Updated = GETDATE()
    FROM 
      Inserted Ins
   WHERE
      Merchandising.dashboard.TimeToMarketTracking.BusinessUnitId = Ins.BusinessUnitId 
      AND Merchandising.dashboard.TimeToMarketTracking.InventoryId = Ins.InventoryId

END
GO
