declare @default_name varchar(50), @tbl varchar(50), @col varchar(50), @dynSql varchar(200)
select @tbl = 'DealerListingSites', @col = 'collectionEnabled'
SELECT @default_name = o2.name
FROM syscolumns c
JOIN sysobjects o ON c.id = o.id
JOIN sysobjects o2 ON c.cdefault = o2.id
WHERE o.name = @tbl
AND c.name = @col

--drop the existing default constraint for column
set @dynSql = 'ALTER TABLE settings.dealerListingSites
DROP CONSTRAINT ' + @default_name

exec(@dynSql)

--change to bit
ALTER TABLE settings.dealerListingSites
ALTER COLUMN collectionEnabled bit NOT NULL

--add new default
ALTER TABLE settings.dealerListingSites
ADD CONSTRAINT [DF__DealerListingSites__CollectionEnabled]
DEFAULT (0) FOR [collectionEnabled]


select @tbl = 'DealerListingSites', @col = 'releaseEnabled'
SELECT @default_name = o2.name
FROM syscolumns c
JOIN sysobjects o ON c.id = o.id
JOIN sysobjects o2 ON c.cdefault = o2.id
WHERE o.name = @tbl
AND c.name = @col

--drop the existing default constraint for column
set @dynSql = 'ALTER TABLE settings.dealerListingSites
DROP CONSTRAINT ' + @default_name
exec(@dynSql)

--change to bit
ALTER TABLE settings.dealerListingSites
ALTER COLUMN releaseEnabled bit NOT NULL

--add new default
ALTER TABLE settings.dealerListingSites
ADD CONSTRAINT [DF__DealerListingSites__ReleaseEnabled]
DEFAULT (0) FOR [releaseEnabled]
