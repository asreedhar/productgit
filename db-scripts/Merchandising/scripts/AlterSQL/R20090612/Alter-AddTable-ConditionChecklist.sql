if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[ConditionChecklist]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[ConditionChecklist]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [builder].[ConditionChecklist](
	[businessUnitId] [int] NOT NULL,
	[inventoryId] [int] NOT NULL,
	isDealerMaintained [bit] NOT NULL,
	isFullyDetailed [bit] NOT NULL,
	hasNoPanelScratches [bit] NOT NULL,
	hasNoVisibleDents [bit] NOT NULL,
	hasNoVisibleRust [bit] NOT NULL,
	hasNoKnownAccidents [bit] NOT NULL,
	hasNoKnownBodyWork [bit] NOT NULL,
	hasPassedDealerInspection [bit] NOT NULL,
	haveAllKeys [bit] NOT NULL,
	noKnownMechanicalProblems [bit] NOT NULL,
	hadAllRequiredScheduledMaintenancePerformed [bit] NOT NULL,
	haveServiceRecords [bit] NOT NULL,
	haveOriginalManuals [bit] NOT NULL,
	CONSTRAINT [PK_ConditionChecklist] PRIMARY KEY 
	(
		[businessUnitID],
		[inventoryID]	
	)
)

