ALTER TABLE postings.VehicleAdScheduling
ADD ExteriorColor VARCHAR(50) NULL
GO

ALTER TABLE postings.VehicleAdScheduling
ADD InteriorColor VARCHAR(50) NULL
GO

ALTER TABLE postings.VehicleAdScheduling
ADD InteriorType VARCHAR(50) NULL
GO

ALTER TABLE postings.VehicleAdScheduling
ADD MappedOptionIds VARCHAR(1000) NULL
GO
