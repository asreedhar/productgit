USE [Merchandising]
GO
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'Listing')
EXEC sys.sp_executesql N'CREATE SCHEMA [Listing] AUTHORIZATION [dbo]'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[StockType]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[StockType](
	[StockTypeID] [tinyint] NOT NULL,
	[Description] [varchar](20) NOT NULL,
 CONSTRAINT [PK_ListingStockType] PRIMARY KEY CLUSTERED 
(
	[StockTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[VehicleOption_Interface]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[VehicleOption_Interface](
	[ProviderID] [tinyint] NOT NULL,
	[SourceRowID] [int] NOT NULL,
	[VIN] [varchar](17) NOT NULL,
	[OptionDescription] [varchar](200) NOT NULL,
	[VehicleID] [int] NULL,
	[OptionID] [int] NULL
) 
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[StandardColor]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[StandardColor](
	[StandardColorID] [int] IDENTITY(1,1) NOT NULL,
	[StandardColor] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ListingStandardColor] PRIMARY KEY CLUSTERED 
(
	[StandardColorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[Make]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[Make](
	[MakeID] [smallint] IDENTITY(1,1) NOT NULL,
	[Make] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ListingMake] PRIMARY KEY CLUSTERED 
(
	[MakeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[Model]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[Model](
	[ModelID] [smallint] IDENTITY(1,1) NOT NULL,
	[Model] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ListingModel] PRIMARY KEY CLUSTERED 
(
	[ModelID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[Trim]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[Trim](
	[TrimID] [int] IDENTITY(1,1) NOT NULL,
	[Trim] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ListingTrim] PRIMARY KEY CLUSTERED 
(
	[TrimID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[VehicleType]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[VehicleType](
	[VehicleTypeID] [int] IDENTITY(1,1) NOT NULL,
	[MakeID] [smallint] NOT NULL,
	[ModelID] [smallint] NOT NULL,
	[ModelYear] [smallint] NOT NULL,
	[TrimID] [int] NOT NULL,
	[DriveTrainID] [tinyint] NOT NULL,
	[EngineID] [smallint] NOT NULL,
	[TransmissionID] [smallint] NOT NULL,
	[FuelTypeID] [tinyint] NOT NULL,
 CONSTRAINT [PK_VehicleType] PRIMARY KEY CLUSTERED 
(
	[VehicleTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[Vehicle_SellerDescription]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[Vehicle_SellerDescription](
	[VehicleID] [int] NOT NULL,
	[SellerDescription] [varchar](2000) NULL,
 CONSTRAINT [PK_Vehicle_SellerDescription] PRIMARY KEY CLUSTERED 
(
	[VehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[DriveTrain]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[DriveTrain](
	[DriveTrainID] [tinyint] IDENTITY(1,1) NOT NULL,
	[DriveTrain] [varchar](50) NOT NULL,
	[DriveTypeCode] [varchar](3) NULL,
 CONSTRAINT [PK_ListingDriveTrain] PRIMARY KEY CLUSTERED 
(
	[DriveTrainID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[Engine]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[Engine](
	[EngineID] [smallint] IDENTITY(1,1) NOT NULL,
	[Engine] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ListingEngine] PRIMARY KEY CLUSTERED 
(
	[EngineID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[OptionsList]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[OptionsList](
	[OptionID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](200) NOT NULL,
 CONSTRAINT [PK_ListingOptionsList] PRIMARY KEY CLUSTERED 
(
	[OptionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[FuelType]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[FuelType](
	[FuelTypeID] [tinyint] IDENTITY(1,1) NOT NULL,
	[FuelType] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ListingFuelType] PRIMARY KEY CLUSTERED 
(
	[FuelTypeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[Seller]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[Seller](
	[SellerID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](64) NOT NULL,
	[Address1] [varchar](100) NULL,
	[Address2] [varchar](100) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](3) NULL,
	[ZipCode] [char](5) NULL,
	[PhoneNumber] [varchar](50) NULL,
	[HomePageURL] [varchar](200) NULL,
	[SourceID] [tinyint] NOT NULL,
	[DealerType] [varchar](30) NULL,
	[EMail] [varchar](100) NULL,
 CONSTRAINT [PK_ListingSeller] PRIMARY KEY CLUSTERED 
(
	[SellerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[Source]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[Source](
	[SourceID] [tinyint] NOT NULL,
	[Description] [varchar](50) NOT NULL,
 CONSTRAINT [PK_ListingSource] PRIMARY KEY CLUSTERED 
(
	[SourceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[Vehicle_Interface]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[Vehicle_Interface](
	[SourceRowID] [int] NOT NULL,
	[Source] [varchar](20) NULL,
	[SellerName] [varchar](64) NULL,
	[SellerAddress1] [varchar](100) NULL,
	[SellerAddress2] [varchar](100) NULL,
	[SellerCity] [varchar](50) NULL,
	[SellerState] [varchar](50) NULL,
	[SellerZipCode] [varchar](10) NULL,
	[SellerURL] [varchar](200) NULL,
	[SellerPhoneNumber] [varchar](50) NULL,
	[SellerID] [int] NULL,
	[VIN] [varchar](17) NULL,
	[Make] [varchar](50) NULL,
	[Model] [varchar](50) NULL,
	[ModelYear] [smallint] NULL,
	[Trim] [varchar](50) NULL,
	[ExteriorColor] [varchar](50) NULL,
	[DriveTrain] [varchar](50) NULL,
	[Engine] [varchar](50) NULL,
	[Transmission] [varchar](50) NULL,
	[FuelType] [varchar](50) NULL,
	[ListPrice] [int] NULL,
	[ListingDate] [smallDATETIME] NULL,
	[Mileage] [int] NULL,
	[MSRP] [int] NULL,
	[StockNumber] [varchar](30) NULL,
	[Certified] [char](1) NULL,
	[ProviderID] [tinyint] NOT NULL,
	[DeltaFlag] [tinyint] NOT NULL,
	[SourceID] [tinyint] NULL,
	[VehicleID] [int] NULL,
	[Options] [varchar](8000) NULL,
	[SellerDescription] [varchar](2000) NULL,
	[DealerType] [varchar](30) NULL,
	[StockType] [varchar](20) NULL,
	[SellerEMail] [varchar](100) NULL
) 
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[ProviderStatus]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[ProviderStatus](
	[ProviderStatusID] [tinyint] NOT NULL,
	[Description] [varchar](20) NOT NULL,
 CONSTRAINT [PK_ListingProviderStatus] PRIMARY KEY CLUSTERED 
(
	[ProviderStatusID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[Vehicle]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[Vehicle](
	[VehicleID] [int] IDENTITY(1,1) NOT NULL,
	[ProviderID] [tinyint] NOT NULL,
	[SellerID] [int] NOT NULL,
	[SourceID] [tinyint] NOT NULL,
	[SourceRowID] [int] NOT NULL,
	[VehicleCatalogID] [int] NOT NULL,
	[ColorID] [int] NOT NULL,
	[MakeID] [smallint] NOT NULL,
	[ModelID] [smallint] NOT NULL,
	[ModelYear] [smallint] NOT NULL,
	[TrimID] [int] NOT NULL,
	[DriveTrainID] [tinyint] NOT NULL,
	[EngineID] [smallint] NOT NULL,
	[TransmissionID] [smallint] NOT NULL,
	[FuelTypeID] [tinyint] NOT NULL,
	[VIN] [varchar](17) NULL,
	[ListPrice] [int] NOT NULL,
	[ListingDate] [smallDATETIME] NULL,
	[Mileage] [int] NOT NULL,
	[MSRP] [int] NULL,
	[StockNumber] [varchar](30) NULL,
	[Certified] [tinyint] NULL,
	[StockTypeID] [tinyint] NOT NULL CONSTRAINT [DF_ListingVehicle__StockTypeID]  DEFAULT ((0)),
 CONSTRAINT [PK_ListingVehicle] PRIMARY KEY CLUSTERED 
(
	[VehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[Color]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[Color](
	[ColorID] [int] IDENTITY(1,1) NOT NULL,
	[Color] [varchar](50) NOT NULL,
	[StandardColorID] [int] NULL,
 CONSTRAINT [PK_ListingColor] PRIMARY KEY CLUSTERED 
(
	[ColorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 
END
GO
SET ANSI_PADDING OFF
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[VehicleOption]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[VehicleOption](
	[VehicleID] [int] NOT NULL,
	[OptionID] [int] NOT NULL,
 CONSTRAINT [PK_VehicleOption] PRIMARY KEY CLUSTERED 
(
	[VehicleID] ASC,
	[OptionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[Provider]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[Provider](
	[ProviderID] [tinyint] NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[Priority] [tinyint] NOT NULL CONSTRAINT [DF_ListingProvider__Priority]  DEFAULT ((4)),
	[ProviderStatusID] [tinyint] NOT NULL,
	[DataloadID] [int] NULL,
	[LastStatusChange] [smallDATETIME] NULL,
	[ChangedByUser] [varchar](128) NULL,
	[OptionDelimiter] [char](1) NULL,
 CONSTRAINT [PK_ListingProvider] PRIMARY KEY CLUSTERED 
(
	[ProviderID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 
END
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[Listing].[GetStandardizedTransmission]') and xtype in (N'FN', N'IF', N'TF'))
DROP FUNCTION [Listing].[GetStandardizedTransmission]
GO

--------------------------------------------------------------------------------------------------------
--	NEED TO CREATE THE FUNCTION IN THE ALTER OTHERWISE IT WON'T EXIST
--	WHEN WE CREATE THE CAL'D COLUMN.
--	SINCE THERE AREN'T A LOT OF COLUMNS, WE CAN FORGET ABOUT THE OVERHEAD OF THE
--	CAL'D COLUMN.   
--
--	04/27/2009	MAK	Added several other clauses in effort to map transmission.
--
--------------------------------------------------------------------------------------------------------	

CREATE FUNCTION [Listing].[GetStandardizedTransmission] (@Transmission VARCHAR(50)) RETURNS VARCHAR(10)
AS
BEGIN
	
	RETURN (CASE WHEN @Transmission LIKE '%MANUAL%' THEN 'MANUAL'
			WHEN @Transmission LIKE '%AUTO%' THEN 'AUTOMATIC'
			WHEN @Transmission LIKE '%AT%' THEN 'AUTOMATIC'
			WHEN @Transmission LIKE '%AU%' THEN 'AUTOMATIC'
			WHEN @Transmission LIKE '%MAN%' THEN 'MANUAL'
			WHEN @Transmission LIKE '%VAR%' THEN 'AUTOMATIC'
			WHEN @Transmission LIKE '%CVT%' THEN 'AUTOMATIC'
			WHEN @Transmission LIKE '%Standard%' THEN 'MANUAL'
			WHEN @Transmission LIKE '%STND%' THEN 'MANUAL'
			WHEN @Transmission LIKE '%STD%' THEN 'MANUAL'
			WHEN @Transmission LIKE '%Stick%' THEN 'MANUAL'
			WHEN @Transmission Like '%A/T%' THEN 'AUTOMATIC'
			WHEN @Transmission Like '%A\T%' THEN 'AUTOMATIC'
			WHEN @Transmission Like '%MT%' THEN 'MANUAL'
			ELSE 'UNKNOWN'
		END 
		)				    

END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[Transmission]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[Transmission](
	[TransmissionID] [smallint] IDENTITY(1,1) NOT NULL,
	[Transmission] [varchar](50) NOT NULL,
	[StandardizedTransmission]  AS ([Listing].[GetStandardizedTransmission]([Transmission])),
 CONSTRAINT [PK_ListingTransmission] PRIMARY KEY CLUSTERED 
(
	[TransmissionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Listing].[Vehicle_History]') AND type in (N'U'))
BEGIN
CREATE TABLE [Listing].[Vehicle_History](
	[VehicleID] [int] NOT NULL,
	[ProviderID] [tinyint] NOT NULL,
	[SellerID] [int] NOT NULL,
	[SourceID] [tinyint] NOT NULL,
	[SourceRowID] [int] NOT NULL,
	[VehicleCatalogID] [int] NOT NULL,
	[ColorID] [int] NOT NULL,
	[MakeID] [int] NOT NULL,
	[ModelID] [int] NOT NULL,
	[ModelYear] [int] NOT NULL,
	[TrimID] [int] NOT NULL,
	[DriveTrainID] [tinyint] NOT NULL,
	[EngineID] [smallint] NOT NULL,
	[TransmissionID] [smallint] NOT NULL,
	[FuelTypeID] [tinyint] NOT NULL,
	[VIN] [varchar](17) NULL,
	[ListPrice] [int] NOT NULL,
	[ListingDate] [smallDATETIME] NULL,
	[Mileage] [int] NOT NULL,
	[MSRP] [int] NULL,
	[StockNumber] [varchar](30) NULL,
	[Certified] [tinyint] NULL,
	[DateCreated] [smallDATETIME] NOT NULL CONSTRAINT [DF_ListingVehicleHistory__DateCreated]  DEFAULT (dateadd(day,datediff(day,(0),getdate()),(0))),
	[SellerDescription] [varchar](2000) NULL,
 CONSTRAINT [PK_ListingVehicle_History] PRIMARY KEY NONCLUSTERED 
(
	[VehicleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Listing].[DataSet](
	[DataSetID] [int] IDENTITY(1,1) NOT NULL,
	[DataFileNames] [varchar](8000) NULL,
	[JobStartDate] [DATETIME] NULL,
	[JobEndDate] [DATETIME] NULL,
	[TotalSearches] [int] NULL,
	[TotalListings] [int] NULL,
	[TotalRecordsInHistory] [int] NULL,
	[TotalRecordsInHistoryPast90Days] [int] NULL,
	[TotalRecordsInSalesU] [int] NULL,
	[TotalRecordsInSalesR] [int] NULL,
	[TotalRecordsInSalesW] [int] NULL,
	[TotalRecordsInSalesUPast90Days] [int] NULL,
	[TotalRecordsInSalesRPast90Days] [int] NULL,
	[TotalRecordsInSalesWPast90Days] [int] NULL,
 CONSTRAINT [PK_DataSet] PRIMARY KEY CLUSTERED 
(
	[DataSetID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) 
) 

GO

IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Listing].[FK_ListingVehicleOption_ListingOption]') AND parent_object_id = OBJECT_ID(N'[Listing].[VehicleOption]'))
ALTER TABLE [Listing].[VehicleOption]  WITH CHECK ADD  CONSTRAINT [FK_ListingVehicleOption_ListingOption] FOREIGN KEY([OptionID])
REFERENCES [Listing].[OptionsList] ([OptionID])
GO
ALTER TABLE [Listing].[VehicleOption] CHECK CONSTRAINT [FK_ListingVehicleOption_ListingOption]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Listing].[FK_ListingVehicleOption_ListingVehicle]') AND parent_object_id = OBJECT_ID(N'[Listing].[VehicleOption]'))
ALTER TABLE [Listing].[VehicleOption]  WITH CHECK ADD  CONSTRAINT [FK_ListingVehicleOption_ListingVehicle] FOREIGN KEY([VehicleID])
REFERENCES [Listing].[Vehicle] ([VehicleID])
GO
ALTER TABLE [Listing].[VehicleOption] CHECK CONSTRAINT [FK_ListingVehicleOption_ListingVehicle]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Listing].[FK_ListingVehicle__StockTypeID]') AND parent_object_id = OBJECT_ID(N'[Listing].[Vehicle]'))
ALTER TABLE [Listing].[Vehicle]  WITH CHECK ADD  CONSTRAINT [FK_ListingVehicle__StockTypeID] FOREIGN KEY([StockTypeID])
REFERENCES [Listing].[StockType] ([StockTypeID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_ListingVehicle__StockTypeID]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Listing].[FK_ListingVehicle_ListingColor]') AND parent_object_id = OBJECT_ID(N'[Listing].[Vehicle]'))
ALTER TABLE [Listing].[Vehicle]  WITH NOCHECK ADD  CONSTRAINT [FK_ListingVehicle_ListingColor] FOREIGN KEY([ColorID])
REFERENCES [Listing].[Color] ([ColorID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_ListingVehicle_ListingColor]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Listing].[FK_ListingVehicle_ListingDriveTrain]') AND parent_object_id = OBJECT_ID(N'[Listing].[Vehicle]'))
ALTER TABLE [Listing].[Vehicle]  WITH NOCHECK ADD  CONSTRAINT [FK_ListingVehicle_ListingDriveTrain] FOREIGN KEY([DriveTrainID])
REFERENCES [Listing].[DriveTrain] ([DriveTrainID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_ListingVehicle_ListingDriveTrain]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Listing].[FK_ListingVehicle_ListingEngine]') AND parent_object_id = OBJECT_ID(N'[Listing].[Vehicle]'))
ALTER TABLE [Listing].[Vehicle]  WITH NOCHECK ADD  CONSTRAINT [FK_ListingVehicle_ListingEngine] FOREIGN KEY([EngineID])
REFERENCES [Listing].[Engine] ([EngineID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_ListingVehicle_ListingEngine]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Listing].[FK_ListingVehicle_ListingFuelType]') AND parent_object_id = OBJECT_ID(N'[Listing].[Vehicle]'))
ALTER TABLE [Listing].[Vehicle]  WITH NOCHECK ADD  CONSTRAINT [FK_ListingVehicle_ListingFuelType] FOREIGN KEY([FuelTypeID])
REFERENCES [Listing].[FuelType] ([FuelTypeID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_ListingVehicle_ListingFuelType]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Listing].[FK_ListingVehicle_ListingMake]') AND parent_object_id = OBJECT_ID(N'[Listing].[Vehicle]'))
ALTER TABLE [Listing].[Vehicle]  WITH NOCHECK ADD  CONSTRAINT [FK_ListingVehicle_ListingMake] FOREIGN KEY([MakeID])
REFERENCES [Listing].[Make] ([MakeID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_ListingVehicle_ListingMake]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Listing].[FK_ListingVehicle_ListingModel]') AND parent_object_id = OBJECT_ID(N'[Listing].[Vehicle]'))
ALTER TABLE [Listing].[Vehicle]  WITH NOCHECK ADD  CONSTRAINT [FK_ListingVehicle_ListingModel] FOREIGN KEY([ModelID])
REFERENCES [Listing].[Model] ([ModelID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_ListingVehicle_ListingModel]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Listing].[FK_ListingVehicle_ListingProvider]') AND parent_object_id = OBJECT_ID(N'[Listing].[Vehicle]'))
ALTER TABLE [Listing].[Vehicle]  WITH NOCHECK ADD  CONSTRAINT [FK_ListingVehicle_ListingProvider] FOREIGN KEY([ProviderID])
REFERENCES [Listing].[Provider] ([ProviderID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_ListingVehicle_ListingProvider]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Listing].[FK_ListingVehicle_ListingSeller]') AND parent_object_id = OBJECT_ID(N'[Listing].[Vehicle]'))
ALTER TABLE [Listing].[Vehicle]  WITH NOCHECK ADD  CONSTRAINT [FK_ListingVehicle_ListingSeller] FOREIGN KEY([SellerID])
REFERENCES [Listing].[Seller] ([SellerID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_ListingVehicle_ListingSeller]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Listing].[FK_ListingVehicle_ListingSource]') AND parent_object_id = OBJECT_ID(N'[Listing].[Vehicle]'))
ALTER TABLE [Listing].[Vehicle]  WITH NOCHECK ADD  CONSTRAINT [FK_ListingVehicle_ListingSource] FOREIGN KEY([SourceID])
REFERENCES [Listing].[Source] ([SourceID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_ListingVehicle_ListingSource]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Listing].[FK_ListingVehicle_ListingTransmission]') AND parent_object_id = OBJECT_ID(N'[Listing].[Vehicle]'))
ALTER TABLE [Listing].[Vehicle]  WITH NOCHECK ADD  CONSTRAINT [FK_ListingVehicle_ListingTransmission] FOREIGN KEY([TransmissionID])
REFERENCES [Listing].[Transmission] ([TransmissionID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_ListingVehicle_ListingTransmission]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Listing].[FK_ListingVehicle_ListingTrim]') AND parent_object_id = OBJECT_ID(N'[Listing].[Vehicle]'))
ALTER TABLE [Listing].[Vehicle]  WITH NOCHECK ADD  CONSTRAINT [FK_ListingVehicle_ListingTrim] FOREIGN KEY([TrimID])
REFERENCES [Listing].[Trim] ([TrimID])
GO
ALTER TABLE [Listing].[Vehicle] CHECK CONSTRAINT [FK_ListingVehicle_ListingTrim]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Listing].[FK_ListingProvider__ProviderStatusID]') AND parent_object_id = OBJECT_ID(N'[Listing].[Provider]'))
ALTER TABLE [Listing].[Provider]  WITH CHECK ADD  CONSTRAINT [FK_ListingProvider__ProviderStatusID] FOREIGN KEY([ProviderStatusID])
REFERENCES [Listing].[ProviderStatus] ([ProviderStatusID])
GO
ALTER TABLE [Listing].[Provider] CHECK CONSTRAINT [FK_ListingProvider__ProviderStatusID]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Listing].[FK_ListingColor_ListingStandardColor]') AND parent_object_id = OBJECT_ID(N'[Listing].[Color]'))
ALTER TABLE [Listing].[Color]  WITH CHECK ADD  CONSTRAINT [FK_ListingColor_ListingStandardColor] FOREIGN KEY([StandardColorID])
REFERENCES [Listing].[StandardColor] ([StandardColorID])
GO
ALTER TABLE [Listing].[Color] CHECK CONSTRAINT [FK_ListingColor_ListingStandardColor]
GO

INSERT INTO Listing.ProviderStatus
VALUES (0, 'Inactive')
INSERT INTO Listing.ProviderStatus
VALUES (1, 'Ready')
INSERT INTO Listing.ProviderStatus
VALUES (2, 'Staging Lock')


INSERT
INTO	Listing.Provider (ProviderID, Description, Priority, ProviderStatusID, OptionDelimiter)
SELECT	5, 'eBizAutos', 0, 1, ','
GO


UPDATE	Listing.Provider
SET	ProviderStatusID = 0

UPDATE	Listing.Provider
SET	ProviderStatusID = 1
WHERE	ProviderID = 5

GO