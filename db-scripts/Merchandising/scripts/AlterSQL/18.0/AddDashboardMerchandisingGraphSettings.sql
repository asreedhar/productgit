use Merchandising
GO

if not exists(select * 
              from INFORMATION_SCHEMA.columns 
              where TABLE_SCHEMA = 'settings' and TABLE_NAME = 'Merchandising' and COLUMN_NAME = 'MerchandisingGraphDefault')
begin
    print 'Adding MerchandisingGraphDefault field to settings.Merchandising'
    alter table settings.Merchandising
	    add MerchandisingGraphDefault tinyint not null constraint DF_Merchandising_MerchandisingGraphDefault default (0)
    print 'Done.'
end
GO
