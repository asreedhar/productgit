-- if column default ... remove it
if EXISTS (SELECT * 
				FROM sysobjects
				WHERE TYPE = 'D'
				AND NAME = 'DF_merchandising_maxForSmartphone')
	begin
		ALTER TABLE settings.merchandising
			DROP CONSTRAINT DF_merchandising_maxForSmartphone
	end
	
-- if column exists ... remove it
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'maxForSmartphone'
				AND object_id = object_id('settings.merchandising'))
	begin
		ALTER TABLE settings.merchandising
			DROP COLUMN maxForSmartphone
	end

-- add column for Max for smartphone, FB:28289
ALTER TABLE settings.merchandising
	ADD maxForSmartphone bit NOT NULL CONSTRAINT DF_merchandising_maxForSmartphone DEFAULT 0
GO

