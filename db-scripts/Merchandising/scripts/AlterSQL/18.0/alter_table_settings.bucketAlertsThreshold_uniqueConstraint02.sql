
-- get duplicate buckets with highest id
WITH duplicateBucketAlert AS (SELECT id = MAX(id), bucketAlertsThreshold.BusinessUnitId, bucketAlertsThreshold.Bucket
	FROM merchandising.settings.BucketAlertsThreshold bucketAlertsThreshold
	WHERE EXISTS (
					SELECT BusinessUnitId, Bucket
						FROM merchandising.settings.BucketAlertsThreshold bat02
						WHERE bucketAlertsThreshold.BusinessUnitId = bat02.BusinessUnitId
						AND bucketAlertsThreshold.Bucket = bat02.Bucket
						GROUP BY BusinessUnitId, Bucket
						HAVING COUNT(*) > 1
					)
	GROUP BY bucketAlertsThreshold.BusinessUnitId, bucketAlertsThreshold.Bucket)

DELETE bucketAlertsThreshold
	FROM merchandising.settings.BucketAlertsThreshold bucketAlertsThreshold
	WHERE EXISTS (SELECT *
					FROM duplicateBucketAlert
					WHERE duplicateBucketAlert.BusinessUnitId = bucketAlertsThreshold.BusinessUnitId
					AND  duplicateBucketAlert.bucket = bucketAlertsThreshold.bucket)
	AND NOT EXISTS (SELECT *
					FROM duplicateBucketAlert
					WHERE duplicateBucketAlert.ID = bucketAlertsThreshold.ID)
	
GO
-- create the unique index
if EXISTS(SELECT *
				FROM sys.indexes
				WHERE name = 'IX_BusinessUnitID_Bucket'
				AND object_id = object_id('settings.BucketAlertsThreshold'))
	begin
		DROP INDEX IX_BusinessUnitID_Bucket ON settings.BucketAlertsThreshold	
	end

CREATE UNIQUE INDEX IX_BusinessUnitID_Bucket ON settings.BucketAlertsThreshold(BusinessUnitId, Bucket)
GO