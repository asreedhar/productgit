-- if column default ... remove it
if EXISTS (SELECT * 
				FROM sysobjects
				WHERE TYPE = 'D'
				AND NAME = 'DF_merchandising_showTimeToMarket')
	begin
		ALTER TABLE settings.merchandising
			DROP CONSTRAINT DF_merchandising_showTimeToMarket
	end
	
-- if column exists ... remove it
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'showTimeToMarket'
				AND object_id = object_id('settings.merchandising'))
	begin
		ALTER TABLE settings.merchandising
			DROP COLUMN showTimeToMarket
	end

-- add column, FB:28466
ALTER TABLE settings.merchandising
	ADD showTimeToMarket bit NOT NULL CONSTRAINT DF_merchandising_showTimeToMarket DEFAULT 1
GO

