-- if column default ... remove it
if EXISTS (SELECT * 
				FROM sysobjects
				WHERE TYPE = 'D'
				AND NAME = 'DF_merchandising_showOnlineClassifiedOverview')
	begin
		ALTER TABLE settings.merchandising
			DROP CONSTRAINT DF_merchandising_showOnlineClassifiedOverview
	end
	
-- if column exists ... remove it
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'showOnlineClassifiedOverview'
				AND object_id = object_id('settings.merchandising'))
	begin
		ALTER TABLE settings.merchandising
			DROP COLUMN showOnlineClassifiedOverview
	end

-- add column for showing online classified overview , FB:28329
ALTER TABLE settings.merchandising
	ADD showOnlineClassifiedOverview bit NOT NULL CONSTRAINT DF_merchandising_showOnlineClassifiedOverview DEFAULT 0
GO



