ALTER TABLE settings.Merchandising DROP CONSTRAINT DF_EnableMaxAnalyticsOnline
GO
ALTER TABLE settings.Merchandising DROP COLUMN EnableMaxAnalyticsOnline
GO

ALTER TABLE settings.Merchandising DROP CONSTRAINT DF_EnableMaxAnalyticsPerformanceSummary
GO
ALTER TABLE settings.Merchandising DROP COLUMN EnableMaxAnalyticsPerformanceSummary
GO

ALTER TABLE settings.Merchandising
ADD EnableMaxAnalyticsOnline BIT CONSTRAINT DF_EnableMaxAnalyticsOnline DEFAULT 1 NOT NULL
GO

ALTER TABLE settings.Merchandising
ADD EnableMaxAnalyticsPerformanceSummary BIT CONSTRAINT DF_EnableMaxAnalyticsPerformanceSummary DEFAULT 1 NOT NULL
GO