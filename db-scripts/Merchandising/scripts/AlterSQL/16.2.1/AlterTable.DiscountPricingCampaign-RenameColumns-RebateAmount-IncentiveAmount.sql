USE Merchandising
GO
IF EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'RebateAmount' AND Object_ID = Object_ID(N'merchandising.DiscountPricingCampaign'))
BEGIN
	exec sp_rename 'Merchandising.merchandising.DiscountPricingCampaign.RebateAmount', 'ManufacturerRebate', 'COLUMN'
END
GO


IF EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'DiscountAmount' AND Object_ID = Object_ID(N'merchandising.DiscountPricingCampaign'))
BEGIN
	exec sp_rename 'Merchandising.merchandising.DiscountPricingCampaign.DiscountAmount', 'DealerDiscount', 'COLUMN'
END
GO

