IF EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'RebateAmount' AND Object_ID = Object_ID(N'postings.VehicleAdScheduling'))
BEGIN
	exec sp_rename 'Merchandising.postings.VehicleAdScheduling.RebateAmount', 'ManufacturerRebate', 'COLUMN'
END
GO


IF EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'IncentiveAmount' AND Object_ID = Object_ID(N'postings.VehicleAdScheduling'))
BEGIN
	exec sp_rename 'Merchandising.postings.VehicleAdScheduling.IncentiveAmount', 'DealerDiscount', 'COLUMN'
END
GO

