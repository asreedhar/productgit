declare @templates table
(
	id int not null,
	series varchar(100) not null,
	NewTemplateID int not null,
	UsedTemplateID int not null,
	NewVPID int not null,
	UsedVPID int null
)

insert @templates
(
	id,
	series,
	NewTemplateID,
	UsedTemplateID,
	NewVPID,
	UsedVPID
)
          select id = 0, series = 'BMW 3 Series', NewTemplateID = 7548, UsedTemplateID = 7212, NewVPID = 264, UsedVPID = null
union all select id = 1, series = 'BMW 5 Series', NewTemplateID = 7566, UsedTemplateID = 7229, NewVPID = 265, UsedVPID = null
union all select id = 2, series = 'BMW 7 Series', NewTemplateID = 7717, UsedTemplateID = 7265, NewVPID = 266, UsedVPID = null
union all select id = 3, series = 'BMW X3', NewTemplateID = 7684, UsedTemplateID = 7323, NewVPID = 275, UsedVPID = null
union all select id = 4, series = 'BMW X5', NewTemplateID = 7565, UsedTemplateID = 7324, NewVPID = 267, UsedVPID = null


select 'debug before', * from @templates

declare
	@i int,
	@rows int,
	@NewTemplateID int,
	@UsedTemplateID int,
	@NewVPID int,
	@UsedVPID int


begin try
	begin tran

	set @i = 0
	while (@i < 5)
	begin
		select top 1
			@NewTemplateID = NewTemplateID,
			@UsedTemplateID = UsedTemplateID,
			@NewVPID = NewVPID,
			@UsedVPID = UsedVPID
		from @templates
		where id = @i
		

		insert Merchandising.templates.VehicleProfiles
		(
			businessUnitId,
			title,
			makeIDList,
			modelList,
			marketClassIdList,
			startYear,
			endYear,
			minAge,
			maxAge,
			minPrice,
			maxPrice,
			certifiedStatusFilter,
			trimList,
			segmentIdList,
			vehicleType
		)
		select
			businessUnitId,
			title,
			makeIDList,
			modelList,
			marketClassIdList,
			startYear,
			endYear,
			minAge,
			maxAge,
			minPrice,
			maxPrice,
			certifiedStatusFilter,
			trimList,
			segmentIdList,
			2 -- used
		from Merchandising.templates.VehicleProfiles
		where vehicleProfileID = @NewVPID
		
		select @rows = @@ROWCOUNT, @UsedVPID = scope_identity()
		if (@rows <> 1) raiserror('invalid profile insert', 16, 1)
		
		-- set the new type on the old profile record 
		update Merchandising.templates.VehicleProfiles set vehicleType = 1 where vehicleProfileID = @NewVPID
		
		-- copy over our id into the pre-existing AdTemplate row
		update Merchandising.templates.AdTemplates set vehicleProfileId = @UsedVPID where templateID = @UsedTemplateID
		
		-- save off to our temp table for debugging
		update @templates set UsedVPID = @UsedVPID where id = @i

			
		set @i = @i + 1
	end

	commit
end try
begin catch
	rollback
	declare @error varchar(1000)
	
	set @error =
		'ErrorNumber ' + isnull(convert(varchar(11), error_number()), '<null>') +
		', ErrorSeverity ' + isnull(convert(varchar(11), error_severity()), '<null>') +
		', ErrorState ' + isnull(convert(varchar(11), error_state()), '<null>') +
		', ErrorProcedure ' + isnull(error_procedure(), '<null>') +
		', ErrorLine ' + isnull(convert(varchar(11), error_line()), '<null>') +
		', ErrorMessage ' + isnull(error_message(), '<null>')
	
	raiserror(@error, 16, 1)
end catch



select 'debug after', * from @templates