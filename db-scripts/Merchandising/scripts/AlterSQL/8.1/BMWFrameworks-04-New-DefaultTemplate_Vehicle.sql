insert Merchandising.templates.DefaultTemplate_Vehicle
(
	BusinessUnitId,
	ChromeStyleId,
	templateId,
	themeSetId,
	vehicleType
)
select
	BusinessUnitId,
	ChromeStyleId,
	templateId = 
		case
			when templateId = 7212 then 7548
			when templateId = 7229 then 7566
			when templateId = 7265 then 7717
			when templateId = 7323 then 7684
			when templateId = 7324 then 7565
			else null
		end,
	themeSetId,
	1
from Merchandising.templates.DefaultTemplate_Vehicle
where templateId in (7212, 7229, 7265, 7323, 7324)


update Merchandising.templates.DefaultTemplate_Vehicle set vehicleType = 2 
where templateId in (7212, 7229, 7265, 7323, 7324)