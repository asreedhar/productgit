if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'templates' and TABLE_NAME = 'EquipmentExclusionMap' and COLUMN_NAME = 'EquipmentExclusionMapId')
	alter table templates.EquipmentExclusionMap add EquipmentExclusionMapId int identity(1,1) not null
go


if object_id('templates.PK_EquipmentExclusionMap') is null
	alter table templates.EquipmentExclusionMap add constraint PK_EquipmentExclusionMap primary key clustered (EquipmentExclusionMapId) on [PRIMARY]
go