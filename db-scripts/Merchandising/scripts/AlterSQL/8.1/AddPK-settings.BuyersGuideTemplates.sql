if object_id('settings.PK_BuyersGuideTemplates') is null
	alter table settings.BuyersGuideTemplates add constraint PK_BuyersGuideTemplates primary key clustered (buyersGuideTemplateID) on [PRIMARY]
go