if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'alerts' and TABLE_NAME = 'Subscriptions' and COLUMN_NAME = 'subscriptionId')
	alter table alerts.Subscriptions add subscriptionId int identity(1,1) not null
go


if object_id('alerts.PK_Subscriptions') is null
	alter table alerts.Subscriptions add constraint PK_Subscriptions primary key clustered (subscriptionId) on [PRIMARY]
go