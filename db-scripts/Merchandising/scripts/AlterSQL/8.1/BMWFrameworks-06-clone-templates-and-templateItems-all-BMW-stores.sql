set nocount on

/*
	this script will clone templates, but it must be edited to properly populate the
	
		@stores table variable with a list of stores to clone to
		@templates table with a list of pre-existing templateids to copy
		
	this script is smart enough to not duplicate templates, in other words, if templateID 10 is in the @templates list,
	and it already exists for businessUnit 165900 in adTemplates as evidenced by having a record in there for that businessUnitID
	and that SourceTemplateId (10), it will still function properly.
	
	in other words, this script should always clone the @templates to the other @stores, even if they already exist.
	
	once the templates are created, it will also create the associated AdTemplateItems (by deleteing any that already exist and then
	inserting the ones associated with the original @templates list)
	
*/

declare @stores table (BusinessUnitID int not null)
declare @templates table (templateID int not null)


insert @stores (BusinessUnitID)
select BusinessUnitID
from IMT.dbo.BusinessUnit
where
	BusinessUnitTypeID = 4
	and	BusinessUnit in (
		'Motorwerks BMW',
		'Windy City BMW'
	)


insert @templates (templateID)
select templateID from Merchandising.templates.AdTemplates where templateID in (7212, 7229, 7265, 7323, 7324, 7548, 7565, 7566, 7684, 7717)


--- do not edit script below this line, only edit the stores and templates list above

declare @rows int


-- insert the new templates
insert Merchandising.templates.AdTemplates
(
	name,
	businessUnitID,
	vehicleProfileId,
	parent,
	active,
	SourceTemplateId
)
select
	at.name,
	s.BusinessUnitID,
	at.vehicleProfileId,
	at.parent,
	at.active,
	SourceTemplateId = at.templateID
from
	Merchandising.templates.AdTemplates at
	join @templates t
		on at.templateID = t.templateID
	cross join @stores s
where
	not exists(
		-- make sure we're not cloning templates that already exist
		select * from Merchandising.templates.AdTemplates
		where
			businessUnitID = s.BusinessUnitID
			and SourceTemplateId = at.templateID
	)
	
set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' new templates added.'
	
	
	
	
	
-- delete any existing AdTemplateItems in order to reset them (shouldn't be any if the goal of running this script was to clone templates,
-- but this script could also be used to re-set templates for all stores back to a set of master templates)
	
delete ati
from
	Merchandising.templates.AdTemplateItems ati
	join Merchandising.templates.AdTemplates at
		on ati.templateID = at.templateID
	join @templates t
		on at.SourceTemplateId = t.templateID
	join @stores s
		on at.businessUnitID = s.BusinessUnitID

set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' pre-exising AdTemplateItems deleted.'




-- ok go clone the AdTemplateItems tied to @templates list to the new @stores list
insert Merchandising.templates.AdTemplateItems
(
	templateID,
	blurbID,
	sequence,
	priority,
	required,
	parentID
)
select
	newTemplates.templateID,
	ati.blurbID,
	ati.sequence,
	ati.priority,
	ati.required,
	ati.parentID
from
	Merchandising.templates.AdTemplateItems ati
	join Merchandising.templates.AdTemplates at
		on ati.templateID = at.templateID
	join @templates t
		on at.templateID = t.templateID
	join Merchandising.templates.AdTemplates newTemplates
		on newTemplates.SourceTemplateId = at.templateID



set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' new template items added.'
