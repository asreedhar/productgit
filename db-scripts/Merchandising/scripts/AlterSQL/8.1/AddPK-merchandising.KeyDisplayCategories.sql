if object_id('merchandising.PK_KeyDisplayCategories') is null
	alter table merchandising.KeyDisplayCategories add constraint PK_KeyDisplayCategories primary key clustered (categoryDisplayTypeId, categoryHeaderId) on [PRIMARY]
go