if object_id('alerts.PK_ReportPermissions') is null
	alter table alerts.ReportPermissions add constraint PK_ReportPermissions primary key clustered (reportId, roleName) on [PRIMARY]
go