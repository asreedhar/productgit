-- drop old nonclustered pk
if object_id('settings.PK_InventoryObjectiveMapping') is not null
	alter table settings.InventoryObjectiveMapping drop constraint PK_InventoryObjectiveMapping
go

if object_id('settings.PK_InventoryObjectiveMapping') is null
	alter table settings.InventoryObjectiveMapping add constraint PK_InventoryObjectiveMapping primary key clustered (businessUnitId, inventoryStatusCD) on [PRIMARY]
go