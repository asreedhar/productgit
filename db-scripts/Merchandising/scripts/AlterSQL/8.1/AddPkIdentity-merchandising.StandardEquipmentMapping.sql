if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'merchandising' and TABLE_NAME = 'StandardEquipmentMapping' and COLUMN_NAME = 'StandardEquipmentMappingId')
	alter table merchandising.StandardEquipmentMapping add StandardEquipmentMappingId int identity(1,1) not null
go


if object_id('merchandising.PK_StandardEquipmentMapping') is null
	alter table merchandising.StandardEquipmentMapping add constraint PK_StandardEquipmentMapping primary key clustered (StandardEquipmentMappingId) on [PRIMARY]
go