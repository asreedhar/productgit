if object_id('settings.PK_WindowStickerTemplates') is null
	alter table settings.WindowStickerTemplates add constraint PK_WindowStickerTemplates primary key clustered (windowStickerTemplateID) on [PRIMARY]
go