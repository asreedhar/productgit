if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'merchandising' and TABLE_NAME = 'GetAutoMapping' and COLUMN_NAME = 'MappingId')
	alter table merchandising.GetAutoMapping add MappingId int identity(1,1) not null
go


if object_id('merchandising.PK_GetAutoMapping') is null
	alter table merchandising.GetAutoMapping add constraint PK_GetAutoMapping primary key clustered (MappingId) on [PRIMARY]
go