insert Merchandising.templates.DefaultTemplate_Model
(
	BusinessUnitId,
	ModelId,
	templateId,
	themeSetId,
	specificityLevel,
	vehicleType
)
select
	100150,
	ModelId,
	templateId,
	themeSetId,
	specificityLevel,
	vehicleType
from Merchandising.templates.DefaultTemplate_Model
where templateId in (7212, 7229, 7265, 7323, 7324, 7548, 7565, 7566, 7684, 7717)

insert Merchandising.templates.DefaultTemplate_Vehicle
(
	BusinessUnitId,
	ChromeStyleId,
	templateId,
	themeSetId,
	vehicleType
)
select
	100150,
	ChromeStyleId,
	templateId,
	themeSetId,
	vehicleType
from Merchandising.templates.DefaultTemplate_Vehicle
where templateId in (7212, 7229, 7265, 7323, 7324, 7548, 7565, 7566, 7684, 7717)