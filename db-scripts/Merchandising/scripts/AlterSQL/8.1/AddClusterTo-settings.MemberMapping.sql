-- fix table settings.MemberMapping

if exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'settings' and TABLE_NAME = 'MemberMapping' and COLUMN_NAME = 'firstlookMemberLogin')
	alter table settings.MemberMapping drop column firstlookMemberLogin
go

if object_id('tempdb.dbo.#dupeMembers') is not null
	drop table #dupeMembers
go

if object_id('settings.PK_MemberMapping') is null
begin
	-- got a lot of duplicate rows in the table, clean them up:
	select
		merchandisingMemberLogin,
		firstlookMemberId = min(firstlookMemberId)
	into #dupeMembers
	from settings.MemberMapping
	group by merchandisingMemberLogin
	having count(*) > 1

	begin try
		begin tran
		
		delete members
		from
			settings.MemberMapping members
			join #dupeMembers dupes
				on members.merchandisingMemberLogin = dupes.merchandisingMemberLogin
				and members.firstlookMemberId = dupes.firstlookMemberId
		
		insert settings.MemberMapping
		(
			merchandisingMemberLogin,
			firstlookMemberId
		)
		select
			merchandisingMemberLogin,
			firstlookMemberId
		from #dupeMembers
		
		if
		(
			select count(*) from #dupeMembers
		) <>
		(
			select count(*)
			from
				settings.MemberMapping members
				join #dupeMembers dupes
					on members.merchandisingMemberLogin = dupes.merchandisingMemberLogin
					and members.firstlookMemberId = dupes.firstlookMemberId
		)
		begin
			raiserror('problem cleaning up duplicate members!', 16, 1)
		end
		
		commit
		
		drop table #dupeMembers
	end try
	begin catch
		rollback
		declare @error varchar(1000)
		
		set @error =
			'ErrorNumber ' + isnull(convert(varchar(11), error_number()), '<null>') +
			', ErrorSeverity ' + isnull(convert(varchar(11), error_severity()), '<null>') +
			', ErrorState ' + isnull(convert(varchar(11), error_state()), '<null>') +
			', ErrorProcedure ' + isnull(error_procedure(), '<null>') +
			', ErrorLine ' + isnull(convert(varchar(11), error_line()), '<null>') +
			', ErrorMessage ' + isnull(error_message(), '<null>')
		
		raiserror(@error, 16, 1)
	end catch
		
	alter table settings.MemberMapping add constraint PK_MemberMapping primary key clustered (merchandisingMemberLogin) on [PRIMARY]
end
go
