use Merchandising
go



-- drop temporary data no longer needed

if object_id('temp.bug#16107#backupVehicleAdScheduling#20110901') is not null
	drop table temp.bug#16107#backupVehicleAdScheduling#20110901
go


if object_id('temp.bug#14270#postings_VehicleAdScheduling#20110401') is not null
	drop table temp.bug#14270#postings_VehicleAdScheduling#20110401
go


if object_id('temp.bug#15163#postings_VehicleAdScheduling#20110801') is not null
	drop table temp.bug#15163#postings_VehicleAdScheduling#20110801
go

if object_id('temp.bug#14987#postings_VehicleAdScheduling#20110801') is not null
	drop table temp.bug#14987#postings_VehicleAdScheduling#20110801
go


if object_id('temp.bug#14472#postings_VehicleAdScheduling#20110501') is not null
	drop table temp.bug#14472#postings_VehicleAdScheduling#20110501
go


if object_id('temp.bug#14408#Snippets_Marketing_Vehicle#20110901') is not null
	drop table temp.bug#14408#Snippets_Marketing_Vehicle#20110901
go


if object_id('temp.bug#14408#Snippets_Marketing#20110901') is not null
	drop table temp.bug#14408#Snippets_Marketing#20110901
go


if object_id('temp.bug#14408#Snippets_MarketingSource#20110901') is not null
	drop table temp.bug#14408#Snippets_MarketingSource#20110901
go

if object_id('temp.bug#13858#builder_OptionsConfiguration#20110601') is not null
	drop table temp.bug#13858#builder_OptionsConfiguration#20110601
go


if object_id('dbo.PK_Alter_Script') is null
begin
	delete dbo.Alter_Script where AlterScriptName = '5.21\P0-Fix-FB13469-RecreateAdTemplates.sql' and ApplicationDate = '2010-12-14 23:31:44.707' -- this is a dupe, have no idea how it happened
	alter table dbo.Alter_Script add constraint PK_Alter_Script primary key clustered (AlterScriptName)
end
go


if object_id('dbo.bug#AdminTemplateManagerError#templates_AdTemplateItems#20110801') is not null
	drop table dbo.bug#AdminTemplateManagerError#templates_AdTemplateItems#20110801
go


if object_id('dbo.bug#AdminTemplateManagerError#templates_AdTemplates#20110801') is not null
	drop table dbo.bug#AdminTemplateManagerError#templates_AdTemplates#20110801
go








