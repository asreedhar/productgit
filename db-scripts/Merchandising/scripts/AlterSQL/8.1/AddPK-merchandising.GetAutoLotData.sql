if object_id('merchandising.PK_GetAutoLotData') is null
	alter table merchandising.GetAutoLotData add constraint PK_GetAutoLotData primary key clustered (DealerID) on [PRIMARY]
go