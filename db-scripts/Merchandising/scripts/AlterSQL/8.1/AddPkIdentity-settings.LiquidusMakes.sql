if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'settings' and TABLE_NAME = 'LiquidusMakes' and COLUMN_NAME = 'id')
	alter table settings.LiquidusMakes add id int identity(1,1) not null
go


if object_id('settings.PK_LiquidusMakes') is null
	alter table settings.LiquidusMakes add constraint PK_LiquidusMakes primary key clustered (id) on [PRIMARY]
go