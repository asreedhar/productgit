if not exists(
	select * 
	from Merchandising.information_schema.columns
	where table_name = 'AutoTraderVehSummary'
	and column_name = 'LogicalDate'
)
	alter table Merchandising.merchandising.AutoTraderVehSummary
	add LogicalDate  as (
			case 
				when datediff(month,[Month],coalesce(FetchDateInserted,DateLoaded))>(0) 
				then dateadd(day,(-1),dateadd(month,(1),[Month])) 
				else dateadd(day,datediff(day,(0),coalesce(FetchDateInserted,DateLoaded)),(0)) 
			end
		)



go

DECLARE 
    @ErrorMessage    NVARCHAR(4000),
    @ErrorNumber     INT,
    @ErrorSeverity   INT,
    @ErrorState      INT,
    @ErrorLine       INT,
    @ErrorProcedure  NVARCHAR(200)

begin try

-- add new column to table (used to properly sort the rows for insert into new table)

	begin tran



	-- create the new table
	create table Merchandising.AutoTraderVehSummaryNew(
		BusinessUnitID int not null,
		[Month] datetime not null,
		VIN varchar(50) not null,
		[Type] varchar(50) null,
		YearMakeModel varchar(75) null,
		StockNumber varchar(50) not null,
		Price money null,
		AppearedInSearch int null,
		DetailsPageViews int null,
		MapViews int null,
		EmailsSent int null,
		DetailsPagePrinted int null,
		HasSpotLightAds bit null,
		RequestID int not null,
		Load_ID int not null,
		DateLoaded datetime not null,
		FetchDateInserted datetime null,
		InventoryId int null,
		LogicalDate  as (
			case 
				when datediff(month,[Month],coalesce(FetchDateInserted,DateLoaded))>(0) 
				then dateadd(day,(-1),dateadd(month,(1),[Month])) 
				else dateadd(day,datediff(day,(0),coalesce(FetchDateInserted,DateLoaded)),(0)) 
			end
		),
		DbTimestamp rowversion not null,
	 constraint PK_AutoTraderVehSummaryNew primary key clustered 
	(
		BusinessUnitID asc,
		RequestID asc,
		[Month] asc,
		VIN asc,
		StockNumber asc
	)with (pad_index  = off, statistics_norecompute  = off, ignore_dup_key = off, allow_row_locks  = on, allow_page_locks  = on, fillfactor = 80) on [primary]
	) on [primary]

	alter table Merchandising.AutoTraderVehSummaryNew  with check add  constraint FK_AutoTraderVehSummaryNew foreign key(BusinessUnitID)
	references settings.merchandising (BusinessUnitID)
		
	alter table Merchandising.AutoTraderVehSummaryNew 
	check constraint FK_AutoTraderVehSummaryNew

	alter table Merchandising.AutoTraderVehSummaryNew 
	add constraint DF_AutoTraderVehSummary_DateLoadedNew  default (getdate()) for DateLoaded


	-- load the new table in a specific order to properly set the DBTimestamp value
	insert Merchandising.AutoTraderVehSummaryNew ( 
		businessUnitID, month, VIN, Type, YearMakeModel, StockNumber, Price, AppearedInSearch, DetailsPageViews, MapViews, EmailsSent, DetailsPagePrinted, HasSpotLightAds, RequestId, Load_ID, DateLoaded, FetchDateInserted, InventoryId
		)
	select
		businessUnitID, month, VIN, Type, YearMakeModel, StockNumber, Price, AppearedInSearch, DetailsPageViews, MapViews, EmailsSent, DetailsPagePrinted, HasSpotLightAds, RequestId, Load_ID, DateLoaded, FetchDateInserted, InventoryId
	from Merchandising.AutoTraderVehSummary
	order by 
		VIN,
		StockNumber,
		LogicalDate,
		Fetchdateinserted,
		DateLoaded


	-- get rid of the old table
	alter table merchandising.AutoTraderVehSummary 
	drop constraint FK_AutoTraderVehSummary

	alter table merchandising.AutoTraderVehSummary 
	drop constraint DF_AutoTraderVehSummary_DateLoaded

	drop table Merchandising.AutoTraderVehSummary


	-- rename the new table to the original name

	exec sp_rename 'merchandising.PK_AutoTraderVehSummaryNew', 'PK_AutoTraderVehSummary'
	exec sp_rename 'merchandising.FK_AutoTraderVehSummaryNew', 'FK_AutoTraderVehSummary'
	exec sp_rename 'merchandising.DF_AutoTraderVehSummary_DateLoadedNew', 'DF_AutoTraderVehSummary_DateLoaded'
	exec sp_rename 'merchandising.AutoTraderVehSummaryNew', 'AutoTraderVehSummary'


	create nonclustered index IX_AutoTraderVehSummary_InventoryLogicalDateTimestamp 
	on Merchandising.merchandising.AutoTraderVehSummary ( InventoryId, LogicalDate, DbTimestamp )
	with (pad_index  = off, statistics_norecompute  = off, sort_in_tempdb = off, ignore_dup_key = off, drop_existing = off, online = off, allow_row_locks  = on, allow_page_locks  = on) on [primary]
	
	-- create original indexes
	create nonclustered index IX_BusinessUnitId_InventoryId on Merchandising.merchandising.AutoTraderVehSummary
	(
		businessUnitID ASC,
		InventoryId ASC
	)with (pad_index  = off, statistics_norecompute  = off, sort_in_tempdb = off, ignore_dup_key = off, drop_existing = off, online = off, allow_row_locks  = on, allow_page_locks  = on) on [primary]

	create nonclustered index IX_LoadID on Merchandising.merchandising.AutoTraderVehSummary
	(
		Load_ID ASC
	)with (pad_index  = off, statistics_norecompute  = off, sort_in_tempdb = off, ignore_dup_key = off, drop_existing = off, online = off, allow_row_locks  = on, allow_page_locks  = on) on [primary]
	
	--print 'no errors, rolling back anyhow...'
	--rollback
	commit
end try
begin catch

    SELECT 
        @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');
    
    SELECT @ErrorMessage = 
        N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
            'Message: '+ ERROR_MESSAGE();
            
	
	if @@trancount > 0 rollback
	print 'error encountered , transaction rolled back'

    RAISERROR 
        (
        @ErrorMessage, 
        @ErrorSeverity, 
        1,               
        @ErrorNumber,    -- parameter: original error number.
        @ErrorSeverity,  -- parameter: original error severity.
        @ErrorState,     -- parameter: original error state.
        @ErrorProcedure, -- parameter: original error procedure name.
        @ErrorLine       -- parameter: original error line number.
        );
	
end catch
