DECLARE 
    @ErrorMessage    NVARCHAR(4000),
    @ErrorNumber     INT,
    @ErrorSeverity   INT,
    @ErrorState      INT,
    @ErrorLine       INT,
    @ErrorProcedure  NVARCHAR(200)

begin try

	begin tran
	-- this table won't get this column, we already have feeddate which is basically the same thing as logicaldate
	
	-- add new column to table (used to properly sort the rows for insert into new table)
	--alter table Merchandising.AutoTraderHendrickVehicleSummary
	--add LogicalDate  as (
	--		case 
	--			when datediff(month,[Month],coalesce(FetchDateInserted,DateLoaded))>(0) 
	--			then dateadd(day,(-1),dateadd(month,(1),[Month])) 
	--			else dateadd(day,datediff(day,(0),coalesce(FetchDateInserted,DateLoaded)),(0)) 
	--		end
	--	)

	-- create the new table
	create table Merchandising.AutoTraderHendrickVehicleSummaryNew(
		VIN varchar(17) NOT NULL,
		StockNumber varchar(15) NOT NULL,
		FeedDate datetime NOT NULL,
		SearchPageViews int NULL,
		DetailPageViews int NULL,
		EmailLeads int NULL,
		MapsViewed int NULL,
		AdPrints int NULL,
		LoadId int NOT NULL,
		PhoneRequests int NULL,
		WebsiteClicks int NULL,
		ChatRequests int NULL,
		SpotlightAds int NULL,
		Price int NULL,
		ImageCount smallint NULL,
		SellerNotes char(1) NULL,
		DaysLive smallint NULL,
		BusinessUnitId int NULL,
		InventoryId int NULL,
		[Month] datetime NOT NULL,
		--LogicalDate  as (
		--	case 
		--		when datediff(month,[Month],coalesce(FetchDateInserted,DateLoaded))>(0) 
		--		then dateadd(day,(-1),dateadd(month,(1),[Month])) 
		--		else dateadd(day,datediff(day,(0),coalesce(FetchDateInserted,DateLoaded)),(0)) 
		--	end
		--),
		DbTimestamp rowversion not null,
	 constraint PK_AutoTraderHendrickVehicleSummaryNew primary key clustered 
	(
		VIN ASC,
		StockNumber ASC,
		FeedDate ASC
	)with (pad_index  = off, statistics_norecompute  = off, ignore_dup_key = off, allow_row_locks  = on, allow_page_locks  = on, fillfactor = 80) on [primary]
	) on [primary]

	-- leaving this out (not there currently, playing it safe with this Hendrick table)
	--alter table Merchandising.AutoTraderHendrickVehicleSummaryNew  with check add  constraint FK_AutoTraderHendrickVehicleSummaryNew foreign key(BusinessUnitID)
	--references settings.merchandising (BusinessUnitID)
		
	--alter table Merchandising.AutoTraderHendrickVehicleSummaryNew 
	--check constraint FK_AutoTraderHendrickVehicleSummaryNew

	alter table Merchandising.AutoTraderHendrickVehicleSummaryNew 
	add constraint DF_AutoTraderHendrickVehicleSummaryNew_Month  default (getdate()) for [Month]

	-- load the new table in a specific order to properly set the DBTimestamp value
	insert Merchandising.AutoTraderHendrickVehicleSummaryNew ( 
		VIN, StockNumber, FeedDate, SearchPageViews, DetailPageViews, EmailLeads, MapsViewed, AdPrints, LoadId, PhoneRequests, WebsiteClicks, ChatRequests, SpotlightAds, Price, ImageCount, SellerNotes, DaysLive, BusinessUnitId, InventoryId, Month
		)
	select 
		VIN, StockNumber, FeedDate, SearchPageViews, DetailPageViews, EmailLeads, MapsViewed, AdPrints, LoadId, PhoneRequests, WebsiteClicks, ChatRequests, SpotlightAds, Price, ImageCount, SellerNotes, DaysLive, BusinessUnitId, InventoryId, Month
	from Merchandising.AutoTraderHendrickVehicleSummary
	order by 
		VIN,
		StockNumber,
		FeedDate,
		LoadId


	-- get rid of the old table
	--alter table merchandising.AutoTraderHendrickVehicleSummary 
	--drop constraint FK_AutoTraderHendrickVehicleSummary

	alter table merchandising.AutoTraderHendrickVehicleSummary 
	drop constraint DF_AutoTraderHendrickVehicleSummary_Month

	drop table Merchandising.AutoTraderHendrickVehicleSummary


	-- rename the new table to the original name
	exec sp_rename 'merchandising.PK_AutoTraderHendrickVehicleSummaryNew', 'PK_AutoTraderHendrickVehicleSummary'
	exec sp_rename 'merchandising.DF_AutoTraderHendrickVehicleSummaryNew_Month', 'DF_AutoTraderHendrickVehicleSummary_Month'
	exec sp_rename 'merchandising.AutoTraderHendrickVehicleSummaryNew', 'AutoTraderHendrickVehicleSummary'


	create nonclustered index IX_AutoTraderHendrickVehicleSummary_InventoryFeedDateTimestamp 
	on merchandising.AutoTraderHendrickVehicleSummary ( InventoryId, FeedDate, DbTimestamp )
	with (pad_index  = off, statistics_norecompute  = off, sort_in_tempdb = off, ignore_dup_key = off, drop_existing = off, online = off, allow_row_locks  = on, allow_page_locks  = on) on [primary]

	-- create original indexes
	create nonclustered index IX_BusinessUnitId_InventoryId_FeedDate ON merchandising.AutoTraderHendrickVehicleSummary 
	(
		BusinessUnitId asc,
		InventoryId asc,
		FeedDate asc
	)with (pad_index  = off, statistics_norecompute  = off, sort_in_tempdb = off, ignore_dup_key = off, drop_existing = off, online = off, allow_row_locks  = on, allow_page_locks  = on) on [primary]


	
	commit
end try
begin catch
	SELECT 
        @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');
    
    SELECT @ErrorMessage = 
        N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
            'Message: '+ ERROR_MESSAGE();
            
	
	if @@trancount > 0 rollback
	print 'error encountered , transaction rolled back'

    RAISERROR 
        (
        @ErrorMessage, 
        @ErrorSeverity, 
        1,               
        @ErrorNumber,    -- parameter: original error number.
        @ErrorSeverity,  -- parameter: original error severity.
        @ErrorState,     -- parameter: original error state.
        @ErrorProcedure, -- parameter: original error procedure name.
        @ErrorLine       -- parameter: original error line number.
        );
	
end catch
