alter table Merchandising.dashboard.VehicleActivity drop constraint PK_VehicleActivity
go

alter table Merchandising.dashboard.VehicleActivity add 
	constraint PK_VehicleActivity primary key clustered 
	(
		BusinessUnitId,
		InventoryId,
		DestinationId,
		LogicalDate
	)
	
go