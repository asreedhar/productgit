/*
beta averages
(No column name)	@AvgSearchPageViewsRatio	@AvgDetailPageViewsRatio	@AvgMapsViewedRatio	@AvgAdPrintsRatio
debug store average ratios	0.6508494	0.6868289	0.5683301	0.571697

second run on beta:
(No column name)	@AvgSearchPageViewsRatio	@AvgDetailPageViewsRatio	@AvgMapsViewedRatio	@AvgAdPrintsRatio
debug store average ratios	0.6658109	0.7028525	0.6230506	0.6223153

*/

if not exists
(
	select *
	from Merchandising.INFORMATION_SCHEMA.columns
	where
		TABLE_SCHEMA = 'dashboard'
		and TABLE_NAME = 'PerformanceSummary'
		and COLUMN_NAME = 'UsedSearchPageViewsRatio'
)
begin
	alter table Merchandising.dashboard.PerformanceSummary add
		UsedSearchPageViewsRatio float(24) null,
		UsedDetailPageViewsRatio float(24) null,
		UsedMapsViewedRatio float(24) null,
		UsedAdPrintsRatio float(24) null
end

go


set nocount on
set transaction isolation level read uncommitted


-- step 1: get a list of all the perfSummary records we need to process (max Scraped by businessUnitId, Month)

declare @perfSummaryQueue table
(
	businessUnitId int not null,
	ReportMonth datetime,
	Scraped datetime
)

insert @perfSummaryQueue
(
	businessUnitId,
	ReportMonth,
	Scraped
)
select
	businessUnitID,
	Month,
	max(isnull(FetchDateInserted, DateLoaded))
from Merchandising.merchandising.CarsDotComPerfSummary
group by
	businessUnitID,
	Month
	



-- step 2: collect ratios for SPV, DPV, Email, Maps, AdPrints for all cars.com destinations

declare @ratios table
(
	BusinessUnitId int not null,
	ReportMonth datetime not null,
	UsedSearchPageViewsRatio float(24) not null,
	UsedDetailPageViewsRatio float(24) not null,
	UsedMapsViewedRatio float(24) not null,
	UsedAdPrintsRatio float(24) not null
)


		
declare @BusinessUnitMonth table
(
	BusinessUnitId int,
	month datetime
)		


insert @BusinessUnitMonth
(
	BusinessUnitId,
	month
)
select
	BusinessUnitId,
	Month = dateadd(month, datediff(month, 0, Date), 0)
from Merchandising.dashboard.InventoryAdPerformance
where
	DestinationId = 1
group by
	BusinessUnitId,
	dateadd(month, datediff(month, 0, Date), 0)
	


			-- replace the rows we just deleted
			;with MinMaxReportDatePerMonth
			as
			(	-- Get the max report date (scraped date) for every bu, inv, destination, month
				select
					BusinessUnitId
					,InventoryId
					,DestinationId
					,ReportMonth = dateadd(month, datediff(month, 0, Date), 0)
					,MinReportDate = min(Date)
					,MaxReportDate = max(Date)
					,RecCount = count(*)
				from Merchandising.dashboard.InventoryAdPerformance
				group by 
					BusinessUnitId
					,InventoryId
					,DestinationId
					,dateadd(month, datediff(month, 0, Date), 0) -- ReportMonth
			)
			,InvAdPerfByMonth as
			( -- Get the last set of stats for the max date for each bu, inv, destination, month
				select 
					mm.BusinessUnitId
					,mm.InventoryId
					,mm.DestinationId
					,mm.ReportMonth
					,SearchPageViews = case RecCount when 1 then  maxiap.SearchPageViews else maxiap.SearchPageViews - miniap.SearchPageViews end
					,DetailPageViews = case RecCount when 1 then  maxiap.DetailPageViews else maxiap.DetailPageViews - miniap.DetailPageViews end
					,EmailLeads = case RecCount when 1 then  maxiap.EmailLeads else maxiap.EmailLeads - miniap.EmailLeads end
					,MapsViewed = case RecCount when 1 then  maxiap.MapsViewed else maxiap.MapsViewed - miniap.MapsViewed end
					,AdPrints = case RecCount when 1 then  maxiap.AdPrints else maxiap.AdPrints - miniap.AdPrints end
				from MinMaxReportDatePerMonth mm
				join Merchandising.dashboard.InventoryAdPerformance miniap
					on mm.BusinessUnitId = miniap.BusinessUnitId
					and mm.InventoryId = miniap.InventoryId
					and mm.DestinationId = miniap.DestinationId
					and mm.MinReportDate = miniap.Date
				join Merchandising.dashboard.InventoryAdPerformance maxiap
					on mm.BusinessUnitId = maxIAP.BusinessUnitId
					and mm.InventoryId = maxIAP.InventoryId
					and mm.DestinationId = maxIAP.DestinationId
					and mm.MaxReportDate = maxIAP.Date
			)
			,PerformanceSummaryRollup
			as 
			(	-- Roll up the ad performance by bu, report month, destination, and inventory type (used/new)
				select 
					iap.BusinessUnitId
					,ReportMonth
					,DestinationId
					,InventoryType
					,SearchPageViews = sum(iap.SearchPageViews) 
					,DetailPageViews = sum(iap.DetailPageViews)
					,EmailLeads = sum(iap.EmailLeads)
					,MapsViewed = sum(iap.MapsViewed)
					,AdPrints = sum(iap.AdPrints)
				from InvAdPerfByMonth iap
				join IMT.dbo.Inventory inv
					on inv.InventoryId = iap.InventoryId
				group by iap.BusinessUnitId, ReportMonth, DestinationId, InventoryType
			) -- end of CTEs

			insert @ratios
			(
				BusinessUnitId,
				ReportMonth,
				UsedSearchPageViewsRatio,
				UsedDetailPageViewsRatio,
				UsedMapsViewedRatio,
				UsedAdPrintsRatio
			)
			select 
				BusinessUnitId
				, ReportMonth
				
				, UsedSearchPageViewsRatio = sum(case InventoryType when 2 then SearchPageViews else 0 end) / isnull(nullif((1.0 * sum(SearchPageViews)), 0), 1)
				, UsedDetailPageViewsRatio = sum(case InventoryType when 2 then DetailPageViews else 0 end) / isnull(nullif((1.0 * sum(DetailPageViews)), 0), 1)
				, UsedMapsViewedRatio = sum(case InventoryType when 2 then MapsViewed else 0 end) / isnull(nullif((1.0 * sum(MapsViewed)), 0), 1)
				, UsedAdPrintsRatio = sum(case InventoryType when 2 then AdPrints else 0 end) / isnull(nullif((1.0 * sum(AdPrints)), 0), 1)
			from PerformanceSummaryRollup ps
			where
				DestinationId = 1 -- cars.com
				and exists 
				(	-- only get rows in our input
					select *
					from @BusinessUnitMonth
					where 
						BusinessUnitId = ps.BusinessUnitId 
						and Month = ps.ReportMonth
				)
			group by
				BusinessUnitId,
				ReportMonth,
				DestinationId
			order by
				BusinessUnitId,
				ReportMonth,
				DestinationId





-- step 3:  process the perfSummary queue and apply the ratios to the numbers

declare @PerformanceSummary table
(

	BusinessUnitId int not null,
	DestinationId int not null,
	Month datetime not null,
	NewSearchPageViews int not null,
	NewDetailPageViews int not null,
	NewEmailLeads int not null,
	NewMapsViewed int not null,
	NewAdPrints int not null,
	
	UsedSearchPageViews int not null,
	UsedDetailPageViews int not null,
	UsedEmailLeads int not null,
	UsedMapsViewed int not null,
	UsedAdPrints int not null,
	
	LastLoadId int not null,
	
	RequestId int null,
	DateCollected datetime not null,
	DateLoaded datetime not null,
	
	UsedSearchPageViewsRatio float(24) not null,
	UsedDetailPageViewsRatio float(24) not null,
	UsedMapsViewedRatio float(24) not null,
	UsedAdPrintsRatio float(24) not null
	
) 






declare
	@businessUnitId int,
	@ReportMonth datetime,
	@Scraped datetime,

	@UsedSearchPageViewsRatio float(24),
	@UsedDetailPageViewsRatio float(24),
	@UsedMapsViewedRatio float(24),
	@UsedAdPrintsRatio float(24),

	@AvgSearchPageViewsRatio float(24),
	@AvgDetailPageViewsRatio float(24),
	@AvgMapsViewedRatio float(24),
	@AvgAdPrintsRatio float(24),
	
	@foundRatios int
	


select
	@AvgSearchPageViewsRatio = avg(UsedSearchPageViewsRatio),
	@AvgDetailPageViewsRatio = avg(UsedDetailPageViewsRatio),
	@AvgMapsViewedRatio = avg(UsedMapsViewedRatio),
	@AvgAdPrintsRatio = avg(UsedAdPrintsRatio)
from @ratios


	select 'debug store average ratios',
		[@AvgSearchPageViewsRatio] = @AvgSearchPageViewsRatio,
		[@AvgDetailPageViewsRatio] = @AvgDetailPageViewsRatio,
		[@AvgMapsViewedRatio] = @AvgMapsViewedRatio,
		[@AvgAdPrintsRatio] = @AvgAdPrintsRatio
			
			

while (1=1)
begin
	select top 1
		@businessUnitId = businessUnitId,
		@ReportMonth = ReportMonth,
		@Scraped = Scraped,
		@foundRatios = 0
	from @perfSummaryQueue
	
	if (@@rowcount <> 1) break
	
	delete @perfSummaryQueue
	where
		businessUnitId = @businessUnitId
		and ReportMonth = @ReportMonth
		and Scraped = @Scraped


	select
		@UsedSearchPageViewsRatio = UsedSearchPageViewsRatio,
		@UsedDetailPageViewsRatio = UsedDetailPageViewsRatio,
		@UsedMapsViewedRatio = UsedMapsViewedRatio,
		@UsedAdPrintsRatio = UsedAdPrintsRatio
	from @ratios
	where
		businessUnitId = @businessUnitId
		and ReportMonth = @ReportMonth
	
	set @foundRatios = @@rowcount
		
	if (@foundRatios = 0)
	begin
		-- use previous month for ratio
		select top 1
			@UsedSearchPageViewsRatio = UsedSearchPageViewsRatio,
			@UsedDetailPageViewsRatio = UsedDetailPageViewsRatio,
			@UsedMapsViewedRatio = UsedMapsViewedRatio,
			@UsedAdPrintsRatio = UsedAdPrintsRatio
		from @ratios
		where
			businessUnitId = @businessUnitId
			and ReportMonth < @ReportMonth
		order by ReportMonth desc

		set @foundRatios = @@rowcount
	end



	if
	(
		@foundRatios = 0
		or @UsedSearchPageViewsRatio in (0,1) or @UsedSearchPageViewsRatio not between 0 and 1
		or @UsedDetailPageViewsRatio in (0,1) or @UsedDetailPageViewsRatio not between 0 and 1
		or @UsedMapsViewedRatio in (0,1) or @UsedMapsViewedRatio not between 0 and 1
		or @UsedAdPrintsRatio in (0,1) or @UsedAdPrintsRatio not between 0 and 1
	)
	begin
		-- use average of all stores for the ratio
		select
			@UsedSearchPageViewsRatio = @AvgSearchPageViewsRatio,
			@UsedDetailPageViewsRatio = @AvgDetailPageViewsRatio,
			@UsedMapsViewedRatio = @AvgMapsViewedRatio,
			@UsedAdPrintsRatio = @AvgAdPrintsRatio
		from @ratios
	end	
		
	-- now that we have the proper ratio, apply it to the perf summary row	
	
	insert @PerformanceSummary 
	(

		BusinessUnitId,
		DestinationId,
		Month,
		
		NewSearchPageViews,
		NewDetailPageViews,
		NewEmailLeads,
		NewMapsViewed,
		NewAdPrints,
		
		UsedSearchPageViews,
		UsedDetailPageViews,
		UsedEmailLeads,
		UsedMapsViewed,
		UsedAdPrints,
		
		LastLoadId,
		
		RequestId,
		DateCollected,
		DateLoaded,
		
		UsedSearchPageViewsRatio,
		UsedDetailPageViewsRatio,
		UsedMapsViewedRatio,
		UsedAdPrintsRatio
	)
	select
		p.businessUnitID,
		1,
		p.Month,
		
		NewSearchPageViews = p.InSearchResult - convert(int, p.InSearchResult * @UsedSearchPageViewsRatio),
		NewDetailPageViews = p.RequestDetail - convert(int, p.RequestDetail * @UsedDetailPageViewsRatio),
		NewEmailLeads = p.EmailDealerNew,
		NewMapsViewed = p.ViewMap - convert(int, p.ViewMap * @UsedMapsViewedRatio),
		NewAdPrints = p.PrintAD - convert(int, p.PrintAD * @UsedAdPrintsRatio),
		
		UsedSearchPageViews = convert(int, p.InSearchResult * @UsedSearchPageViewsRatio),
		UsedDetailPageViews = convert(int, p.RequestDetail * @UsedDetailPageViewsRatio),
		UsedEmailLeads = p.EmailDealerUsed,
		UsedMapsViewed = convert(int, p.ViewMap * @UsedMapsViewedRatio),
		UsedAdPrints = convert(int, p.PrintAD * @UsedAdPrintsRatio),
				
		LastLoadId = p.Load_ID,
		RequestId = p.RequestId,
		DateCollected = coalesce(p.FetchDateInserted, p.DateLoaded, '1900-01-01'),
		p.DateLoaded,
		
		@UsedSearchPageViewsRatio,
		@UsedDetailPageViewsRatio,
		@UsedMapsViewedRatio,
		@UsedAdPrintsRatio
	from
		Merchandising.merchandising.CarsDotComPerfSummary p
	where
		p.businessUnitId = @businessUnitId
		and p.month = @ReportMonth
		and isnull(p.FetchDateInserted, p.DateLoaded) = @Scraped

	

	
end




declare @error varchar(max)



--select
--	BusinessUnitId,
--	DestinationId,
--	month,
--	count(*)
--from @PerformanceSummary t
--group by
--	BusinessUnitId,
--	DestinationId,
--	Month
--having count(*) > 1



begin try

		begin tran
		


		update ps set
			NewSearchPageViews = t.NewSearchPageViews,
			NewDetailPageViews = t.NewDetailPageViews,
			NewEmailLeads = t.NewEmailLeads,
			NewMapsViewed = t.NewMapsViewed,
			NewAdPrints = t.NewAdPrints,

			UsedSearchPageViews = t.UsedSearchPageViews,
			UsedDetailPageViews = t.UsedDetailPageViews,
			UsedEmailLeads = t.UsedEmailLeads,
			UsedMapsViewed = t.UsedMapsViewed,
			UsedAdPrints = t.UsedAdPrints,

			LastLoadId = t.LastLoadId,

			RequestId = t.RequestId,
			DateCollected = t.DateCollected,
			DateLoaded = t.DateLoaded,

			UsedSearchPageViewsRatio = t.UsedSearchPageViewsRatio,
			UsedDetailPageViewsRatio = t.UsedDetailPageViewsRatio,
			UsedMapsViewedRatio = t.UsedMapsViewedRatio,
			UsedAdPrintsRatio = t.UsedAdPrintsRatio
		from
			Merchandising.dashboard.PerformanceSummary ps
			join @PerformanceSummary t
				on ps.BusinessUnitId = t.BusinessUnitId
				and ps.DestinationId = t.DestinationId
				and ps.Month = t.Month
		
		print convert(varchar(10), @@rowcount) +  ' rows updated in PerformanceSummary'
		
		insert Merchandising.dashboard.PerformanceSummary
		(
			BusinessUnitId,
			DestinationId,
			Month,
			NewSearchPageViews,
			NewDetailPageViews,
			NewEmailLeads,
			NewMapsViewed,
			NewAdPrints,

			UsedSearchPageViews,
			UsedDetailPageViews,
			UsedEmailLeads,
			UsedMapsViewed,
			UsedAdPrints,

			LastLoadId,

			RequestId,
			DateCollected,
			DateLoaded,

			UsedSearchPageViewsRatio,
			UsedDetailPageViewsRatio,
			UsedMapsViewedRatio,
			UsedAdPrintsRatio
		)
		select
			BusinessUnitId,
			DestinationId,
			Month,
			NewSearchPageViews,
			NewDetailPageViews,
			NewEmailLeads,
			NewMapsViewed,
			NewAdPrints,

			UsedSearchPageViews,
			UsedDetailPageViews,
			UsedEmailLeads,
			UsedMapsViewed,
			UsedAdPrints,

			LastLoadId,

			RequestId,
			DateCollected,
			DateLoaded,

			UsedSearchPageViewsRatio,
			UsedDetailPageViewsRatio,
			UsedMapsViewedRatio,
			UsedAdPrintsRatio
		from @PerformanceSummary t
		where
			not exists
			(
				select *
				from Merchandising.dashboard.PerformanceSummary
				where
					BusinessUnitId = t.BusinessUnitId 
					and DestinationId = t.DestinationId
					and Month = t.Month
			)
		print convert(varchar(10), @@rowcount) +  ' rows inserted in PerformanceSummary'
		
		commit
	end try
	begin catch
		rollback
		
		
		set @error =
			'Failed to load data into PerformanceSummary! ' +
			' ErrorNumber ' + isnull(convert(varchar(11), error_number()), '<null>') +
			', ErrorSeverity ' + isnull(convert(varchar(11), error_severity()), '<null>') +
			', ErrorState ' + isnull(convert(varchar(11), error_state()), '<null>') +
			', ErrorProcedure ' + isnull(error_procedure(), '<null>') +
			', ErrorLine ' + isnull(convert(varchar(11), error_line()), '<null>') +
			', ErrorMessage ' + isnull(error_message(), '<null>')
		
		raiserror(@error, 16, 1)
		return
	end catch 
	
	