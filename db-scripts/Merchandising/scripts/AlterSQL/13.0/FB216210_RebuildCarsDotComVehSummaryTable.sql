if not exists(
	select * 
	from Merchandising.information_schema.columns
	where table_name = 'CarsDotComVehSummary'
	and column_name = 'LogicalDate'
)
	-- add new column to table (used to properly sort the rows for insert into new table)
	alter table Merchandising.merchandising.CarsDotComVehSummary
	add LogicalDate  as (
			case 
				when datediff(month,[Month],coalesce(FetchDateInserted,DateLoaded))>(0) 
				then dateadd(day,(-1),dateadd(month,(1),[Month])) 
				else dateadd(day,datediff(day,(0),coalesce(FetchDateInserted,DateLoaded)),(0)) 
			end
		)
		
		go



DECLARE 
    @ErrorMessage    NVARCHAR(4000),
    @ErrorNumber     INT,
    @ErrorSeverity   INT,
    @ErrorState      INT,
    @ErrorLine       INT,
    @ErrorProcedure  NVARCHAR(200)

begin try

	begin tran



	-- create the new table
	create table Merchandising.CarsDotComVehSummaryNew(
		BusinessUnitID int NOT NULL,
		[Month] datetime NOT NULL,
		StockNumber varchar(25) NOT NULL,
		[Year] smallint NULL,
		Make varchar(150) NULL,
		VIN varchar(25) NOT NULL,
		[Type] varchar(5) NULL,
		Price int NULL,
		Age int NULL,
		InSearchResult int NULL,
		DetailViewed int NULL,
		VideoViewed int NULL,
		AdsPrinted int NULL,
		MapViewed int NULL,
		ClickThru int NULL,
		EmailSent int NULL,
		Chat int NULL,
		Total int NULL,
		PhotoCount int NULL,
		Load_ID int NOT NULL,
		RequestId int NOT NULL,
		DateLoaded datetime NOT NULL,
		FetchDateInserted datetime NULL,
		InventoryId int NULL,
		LogicalDate  as (
			case 
				when datediff(month,[Month],coalesce(FetchDateInserted,DateLoaded))>(0) 
				then dateadd(day,(-1),dateadd(month,(1),[Month])) 
				else dateadd(day,datediff(day,(0),coalesce(FetchDateInserted,DateLoaded)),(0)) 
			end
		),
		DbTimestamp rowversion not null,
	 constraint PK_CarsDotComVehSummaryNew primary key clustered 
	(
		BusinessUnitID asc,
		RequestID asc,
		[Month] asc,
		VIN asc,
		StockNumber asc
	)with (pad_index  = off, statistics_norecompute  = off, ignore_dup_key = off, allow_row_locks  = on, allow_page_locks  = on, fillfactor = 80) on [primary]
	) on [primary]

	alter table Merchandising.CarsDotComVehSummaryNew  with check add  constraint FK_CarsDotComVehSummaryNew foreign key(BusinessUnitID)
	references settings.merchandising (BusinessUnitID)
		
	alter table Merchandising.CarsDotComVehSummaryNew 
	check constraint FK_CarsDotComVehSummaryNew

	alter table Merchandising.CarsDotComVehSummaryNew 
	add constraint DF_CarsDotComVehSummary_DateLoadedNew  default (getdate()) for DateLoaded

	-- load the new table in a specific order to properly set the DBTimestamp value
	insert Merchandising.CarsDotComVehSummaryNew ( 
		businessUnitID, month, StockNumber, Year, Make, VIN, Type, Price, Age, InSearchResult, DetailViewed, VideoViewed, AdsPrinted, MapViewed, ClickThru, EmailSent, Chat, Total, PhotoCount, Load_ID, RequestId, DateLoaded, FetchDateInserted, InventoryId)
	select 
		businessUnitID, month, StockNumber, Year, Make, VIN, Type, Price, Age, InSearchResult, DetailViewed, VideoViewed, AdsPrinted, MapViewed, ClickThru, EmailSent, Chat, Total, PhotoCount, Load_ID, RequestId, DateLoaded, FetchDateInserted, InventoryId
	from Merchandising.CarsDotComVehSummary
	order by 
		VIN,
		StockNumber,
		LogicalDate,
		Fetchdateinserted,
		DateLoaded


	-- get rid of the old table (this didn't already exist for cars.com)
	--alter table merchandising.CarsDotComVehSummary 
	--drop constraint FK_CarsDotComVehSummary

	alter table merchandising.CarsDotComVehSummary 
	drop constraint DF_CarsDotComVehSummary_DateLoaded

	drop table Merchandising.CarsDotComVehSummary


	-- rename the new table to the original name
	exec sp_rename 'merchandising.PK_CarsDotComVehSummaryNew', 'PK_CarsDotComVehSummary'
	exec sp_rename 'merchandising.FK_CarsDotComVehSummaryNew', 'FK_CarsDotComVehSummary'
	exec sp_rename 'merchandising.DF_CarsDotComVehSummary_DateLoadedNew', 'DF_CarsDotComVehSummary_DateLoaded'
	exec sp_rename 'merchandising.CarsDotComVehSummaryNew', 'CarsDotComVehSummary'

	exec sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maps to Server01.DBAStat.dbo.Dataload_History.Load_ID' , @level0type=N'SCHEMA',@level0name=N'merchandising', @level1type=N'TABLE',@level1name=N'CarsDotComVehSummary', @level2type=N'COLUMN',@level2name=N'Load_ID'
	exec sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maps to Server03.Datafeeds.Extract.FetchDotCom#Request.RequestID' , @level0type=N'SCHEMA',@level0name=N'merchandising', @level1type=N'TABLE',@level1name=N'CarsDotComVehSummary', @level2type=N'COLUMN',@level2name=N'RequestId'
	exec sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date this record was loaded into Merchandising.' , @level0type=N'SCHEMA',@level0name=N'merchandising', @level1type=N'TABLE',@level1name=N'CarsDotComVehSummary', @level2type=N'COLUMN',@level2name=N'DateLoaded'


	create nonclustered index IX_CarsDotComVehSummary_InventoryLogicalDateTimestamp 
	on merchandising.CarsDotComVehSummary ( InventoryId, LogicalDate, DbTimestamp )
	with (pad_index  = off, statistics_norecompute  = off, sort_in_tempdb = off, ignore_dup_key = off, drop_existing = off, online = off, allow_row_locks  = on, allow_page_locks  = on) on [primary]
	
	-- create the original indexes
	create nonclustered index IX_BusinessUnitId_InventoryId on merchandising.CarsDotComVehSummary 
	(
		businessUnitID ASC,
		InventoryId ASC
	)with (pad_index  = off, statistics_norecompute  = off, sort_in_tempdb = off, ignore_dup_key = off, drop_existing = off, online = off, allow_row_locks  = on, allow_page_locks  = on) on [primary]

	create nonclustered index IX_LoadID on merchandising.CarsDotComVehSummary 
	(
		Load_ID ASC
	)
	include ( 
		BusinessUnitID,
		[Month],
		StockNumber,
		VIN,
		InSearchResult,
		DetailViewed,
		AdsPrinted,
		MapViewed,
		EmailSent,
		RequestId,
		DateLoaded,
		FetchDateInserted,
		LogicalDate,
		DbTimestamp
	)with (pad_index  = off, statistics_norecompute  = off, sort_in_tempdb = off, ignore_dup_key = off, drop_existing = off, online = off, allow_row_locks  = on, allow_page_locks  = on) on [primary]


	
	commit
end try
begin catch
	SELECT 
        @ErrorNumber = ERROR_NUMBER(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE(),
        @ErrorLine = ERROR_LINE(),
        @ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');
    
    SELECT @ErrorMessage = 
        N'Error %d, Level %d, State %d, Procedure %s, Line %d, ' + 
            'Message: '+ ERROR_MESSAGE();
            
	
	if @@trancount > 0 rollback
	print 'error encountered , transaction rolled back'

    RAISERROR 
        (
        @ErrorMessage, 
        @ErrorSeverity, 
        1,               
        @ErrorNumber,    -- parameter: original error number.
        @ErrorSeverity,  -- parameter: original error severity.
        @ErrorState,     -- parameter: original error state.
        @ErrorProcedure, -- parameter: original error procedure name.
        @ErrorLine       -- parameter: original error line number.
        );
	
end catch
