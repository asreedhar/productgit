use merchandising;

--select ma.manufacturerId, di.divisionId, op.optionCode, ma.manufacturerName, di.divisionName, st.modelYear, st.styleName, op.optionDesc
insert into dbo.ManufacturerVehicleOptionCodeWhiteList (vehicleManufacturerId, vehicleMakeId, optionCode)
select distinct ma.manufacturerId, di.divisionId, op.optionCode
from vehicleCatalog.chrome.options op
join vehicleCatalog.chrome.styles st on st.styleID = op.styleId
join vehicleCatalog.chrome.models mo on mo.modelId = st.modelId
join vehicleCatalog.chrome.divisions di on di.divisionId = mo.divisionId
join vehicleCatalog.chrome.manufacturers ma on ma.manufacturerId = di.manufacturerId
where
	ma.countryCode=1
	and di.countryCode=1
	and op.countryCode=1
	and st.countryCode=1

	and ma.manufacturerId=7
    and op.optionCode in (
		'22E-F','22E-R','23V','25S-F','25S-R','27R','29K-F','29K-R','29N-F','29N-R','2BB-F','2BB-R','2FM','ACF-1','AEH-F','AGF-F','AGF-R','AJJ3','AJV-F','AJV-R',
		'ALS-F','ALS-R','ALXV','ALY','AT4','AT5','AW8','AWX-F','AWX-R','AY7','AYE','AYH','AYM','AYN','AYP','AZC','AZH','AZZ','BLX9','BN9','BNB','CBK',
		'CFU-F','CFU-R','CJ4','CJJ1','CJX9','CMP','CUG','CW9','CWC','CXQ','D7XL','DA1','DA4','DAW','DD5','DD7','DE1','DED','DEG','DEH','DEK','DF2',
		'DF9','DFD','DFF','DFG','DFL','DFP','DG1','DG1-F','DG1-R','DG3','DG7','DG8','DGJ','DGQ','DGQ-A','DGQ-F','DGU','DGV','DGV-F','DGV-R','DGZ',
		'DLXR','DL__-R','EAF','EBA','ECK','ECN','ED3','ED4','EDG','EDT','EDZ','EER-E','EER-G','EGF','EGG','EGH','EGL','EGL-F','EGL-R','EGQ','EGS',
		'EGT','EGV','EGV-E','EGV-F','EGV-R','EGX','EKG','ERB-F','ERB-R','ESG','ESH','ETJ','ETK','EVE-E','EVE-FA','EVE-FF','EVE-G','EVE-NA',
		'EVE-NF','EVE-R','EWE','EWG','EXM','EZA','EZB-1','EZC','EZE','EZF','EZH-F','EZH-R','GC2','GC3','GC4','GJU1','HAK-R','HBD','JBE','JBK',
		'JJK','LBQ','LCQ','LM2','LN3','LN4','LN5','M91','M92','M93','M94','M95','M97','M99','M9U1','M9X8','MGB','MJKT-O','MJU1-O','MJV3-O',
		'MJX8-O','MRA','MU4','MWJ','MX1','MX2','MX3','MX4','MXJ','MXS','NXF','P__-4','RA1','RA2','RA3','RA4','RBZ-F','RBZ-R','RC5','RCA',
		'RE2','RES','RHR-F','RHR-O-F','RHR-O-R','RHR-S','RSP-X','VKD','VKE','VKF','VKG','VKK','VKK-4','VKM','VKP','VKR','VKT','VKU',
		'VKX','VKX-3','VKY','VKZ','VRA','VRA-7','VRB','VRC-3','VRD','VRF','VRH','VRH-4','VRJ','VRK','VRK-6','VRL','VRM','VRR',
		'VRR-5','VRS','VRS-1','VRU','VRV','VRW','VRW-8','VRX','VRX-2','VRY','VRZ','VR_','WPK','WRE','WSJ','XB9','XB9-F',
		'XB9-R','XBQ','XCK','XE5','XG5','XJ1','XJY','XSQ','XZK'
	)

