
update dls
	set ListingSiteDealerId = listingSite.ListingSiteId
from
	Merchandising.settings.DealerListingSites dls
	join
	(
		select BusinessUnitId = 102395, ListingSiteId = '2244558', destinationId = 1
		union all select BusinessUnitId = 105788, ListingSiteId = '183707', destinationId = 1
		union all select BusinessUnitId = 100261, ListingSiteId = '182687', destinationId = 1
		union all select BusinessUnitId = 100945, ListingSiteId = '155010', destinationId = 1
		union all select BusinessUnitId = 105787, ListingSiteId = '165955', destinationId = 1
		union all select BusinessUnitId = 102392, ListingSiteId = '2244648', destinationId = 1
		union all select BusinessUnitId = 105213, ListingSiteId = '151793', destinationId = 1
		union all select BusinessUnitId = 105214, ListingSiteId = '20232', destinationId = 1
		union all select BusinessUnitId = 105218, ListingSiteId = '201788', destinationId = 1
		union all select BusinessUnitId = 104163, ListingSiteId = '180322', destinationId = 1
		union all select BusinessUnitId = 104162, ListingSiteId = '210290', destinationId = 1
		union all select BusinessUnitId = 101429, ListingSiteId = '157641', destinationId = 1
		union all select BusinessUnitId = 103269, ListingSiteId = '26747', destinationId = 1
		union all select BusinessUnitId = 103132, ListingSiteId = '156449', destinationId = 1
		union all select BusinessUnitId = 103129, ListingSiteId = '156867', destinationId = 1
		union all select BusinessUnitId = 103130, ListingSiteId = '156893', destinationId = 1
		union all select BusinessUnitId = 102175, ListingSiteId = '24055', destinationId = 1
		union all select BusinessUnitId = 102394, ListingSiteId = '209721', destinationId = 1
		union all select BusinessUnitId = 105204, ListingSiteId = '3187', destinationId = 1
		union all select BusinessUnitId = 105209, ListingSiteId = '156030', destinationId = 1
		union all select BusinessUnitId = 105208, ListingSiteId = '88895', destinationId = 1
		union all select BusinessUnitId = 105340, ListingSiteId = '201267', destinationId = 1
		union all select BusinessUnitId = 101471, ListingSiteId = '155185', destinationId = 1
		union all select BusinessUnitId = 105442, ListingSiteId = '154937', destinationId = 1
		union all select BusinessUnitId = 102390, ListingSiteId = '149690', destinationId = 1
		union all select BusinessUnitId = 102388, ListingSiteId = '149691', destinationId = 1
		union all select BusinessUnitId = 102389, ListingSiteId = '183439', destinationId = 1
		union all select BusinessUnitId = 105339, ListingSiteId = '81087', destinationId = 1
		union all select BusinessUnitId = 100414, ListingSiteId = '107881', destinationId = 1
		union all select BusinessUnitId = 100415, ListingSiteId = '20375', destinationId = 1
		union all select BusinessUnitId = 102899, ListingSiteId = '154948', destinationId = 1
		union all select BusinessUnitId = 103433, ListingSiteId = '4601', destinationId = 1
		union all select BusinessUnitId = 103969, ListingSiteId = '11238', destinationId = 1
		union all select BusinessUnitId = 105894, ListingSiteId = '202956', destinationId = 1
		union all select BusinessUnitId = 105500, ListingSiteId = '195668', destinationId = 1
		union all select BusinessUnitId = 100388, ListingSiteId = '154910', destinationId = 1
		union all select BusinessUnitId = 105619, ListingSiteId = '209498', destinationId = 1
		union all select BusinessUnitId = 100416, ListingSiteId = '201601', destinationId = 1
		union all select BusinessUnitId = 100389, ListingSiteId = '109032', destinationId = 1
		union all select BusinessUnitId = 100417, ListingSiteId = '109030', destinationId = 1
		union all select BusinessUnitId = 101071, ListingSiteId = '164876', destinationId = 1
		union all select BusinessUnitId = 103479, ListingSiteId = '80423', destinationId = 1
		union all select BusinessUnitId = 105800, ListingSiteId = '150225', destinationId = 1
		union all select BusinessUnitId = 102430, ListingSiteId = '109551', destinationId = 1
		union all select BusinessUnitId = 105219, ListingSiteId = '82341', destinationId = 1
		union all select BusinessUnitId = 102393, ListingSiteId = '2244770', destinationId = 1
		union all select BusinessUnitId = 103131, ListingSiteId = '16096', destinationId = 1
		union all select BusinessUnitId = 100389, ListingSiteId = '91887', destinationId = 2
		union all select BusinessUnitId = 104485, ListingSiteId = '57275872', destinationId = 2
		union all select BusinessUnitId = 105768, ListingSiteId = '69346', destinationId = 2
		union all select BusinessUnitId = 105363, ListingSiteId = '620736', destinationId = 2
		union all select BusinessUnitId = 105356, ListingSiteId = '1328111', destinationId = 2
		union all select BusinessUnitId = 105357, ListingSiteId = '670936', destinationId = 2
		union all select BusinessUnitId = 105358, ListingSiteId = '94841', destinationId = 2
	) listingSite
		on dls.businessUnitID = listingSite.BusinessUnitId
		and dls.destinationID = listingSite.destinationId
		