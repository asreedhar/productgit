
--alter table Merchandising.postings.PhoneLeads add InventoryType tinyint not null

if object_id('postings.PhoneLeads') is not null
	drop table postings.PhoneLeads



CREATE TABLE [postings].[PhoneLeads]
(
	[businessUnitId] [int] not NULL,
	[Month] [datetime] not NULL,
	[Load_ID] [int] NOT NULL,
	[RequestId] [int] NOT NULL,
	NewPhoneLeads int not null,
	UsedPhoneLeads int not null,
	[DateLoaded] [datetime] NOT NULL CONSTRAINT [DF_PhoneLeads_DateLoaded] DEFAULT (getdate()),
) ON [PRIMARY]
GO

ALTER TABLE [postings].[PhoneLeads] ADD CONSTRAINT [PK_phoneLeads] PRIMARY KEY clustered  (businessUnitId, Month, Load_ID, RequestId) WITH (FILLFACTOR=90) ON [PRIMARY]
GO

create nonclustered index IX_LoadId on postings.PhoneLeads (Load_ID)
go