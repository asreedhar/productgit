-- deletes for options filters!!!!
DELETE FROM settings.OptionFilter WHERE ManufactureId = 6
DELETE FROM settings.OptionFilterIncludePhrase WHERE ManufactureId = 6
DELETE FROM settings.OptionFilterExcludePhrase WHERE ManufactureId = 6
DELETE FROM dbo.ManufacturerVehicleOptionCodeWhiteList WHERE VehicleManufacturerId = 6
 
-- insert for settings.OptionFilter

----------------------------------------------------------------
INSERT INTO settings.OptionFilter VALUES(6, 199.00)

 
-- insert for settings.OptionFilterIncludePhrase

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'audio')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'bluetooth')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'CARBON FIBER')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'collision')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'DANA,')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'DIFFERENTIAL,')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'EDITION')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'engine,')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'ENTERTAINMENT SYSTEM')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'EQUIPMENT GROUP')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'EXHAUST TIP')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'GVWR,')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'heated')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'keyless')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'lane,')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'navigation')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'ONSTAR WITH 4G LTE')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'PACKAGE')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'POWER OUTLETS,')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'REAR AXLE,')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'recaro')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'REMOTE VEHICLE START')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'remote,')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'SPOILER,')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'SPORT PEDALS')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'STABILITRAK,')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'steering,')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'suspension package,')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'SUSPENSION,')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'TRANSMISSION,')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'UNIVERSAL HOME REMOTE')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(6, 'WINDOWS,')

 
-- insert for settings.OptionFilterExcludePhrase

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO settings.OptionFilterExcludePhrase VALUES(6, 'BASE PACKAGE')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(6, 'CARGO CONVENIENCE')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(6, 'cargo mat')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(6, 'COMMERCIAL PREFERRED EQUIPMENT GROUP')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(6, 'DISCOUNT')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(6, 'FLEET PACKAGE')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(6, 'floor mat')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(6, 'GAS GUZZLER')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(6, 'NO REMOTE KEYLESS ENTRY')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(6, 'POLICE PREFERRED')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(6, 'RADIO PROVISIONS DELETE')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(6, 'SHIP THRU,')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(6, 'SMOKER')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(6, 'SPECIAL SERVICE PREFERRED')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(6, 'TIRE PACKAGE')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(6, 'TIRE,')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(6, 'TIRES,')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(6, 'WINDOWS, POWER, DELETE')

 
-- insert for dbo.ManufacturerVehicleOptionCodeWhiteList

-------------------------------------------------------------------------------------------------------------
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(6, 8, 'G80')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(6, 15, 'G80')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(6, 8, 'UY2')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(6, 15, 'UY2')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(6, 8, 'Z71')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(6, 8, 'Z71-F')

