IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[settings].[DealerProfilePublishedSnapshot]') 
         AND name = 'HasNewVehiclesEnabled'
)
BEGIN
alter table settings.DealerProfilePublishedSnapshot
	add HasNewVehiclesEnabled bit not null constraint DF_settings_HasNewVehiclesEnabled default 0
END

GO

IF NOT EXISTS (
  SELECT * 
  FROM   sys.columns 
  WHERE  object_id = OBJECT_ID(N'[settings].[DealerProfilePublishedSnapshot]') 
         AND name = 'HasUsedVehiclesEnabled'
)
BEGIN

alter table settings.DealerProfilePublishedSnapshot
	add HasUsedVehiclesEnabled bit not null constraint DF_settings_HasUsedVehiclesEnabled default 0
END


GO