-- if column default ... remove it
if EXISTS (SELECT * 
				FROM sysobjects
				WHERE TYPE = 'D'
				AND NAME = 'DF_merchandising_vehiclesInShowroom')
	begin
		ALTER TABLE settings.merchandising
			DROP CONSTRAINT DF_merchandising_vehiclesInShowroom
	end
	
-- if column exists ... remove it
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'vehiclesInShowroom'
				AND object_id = object_id('settings.merchandising'))
	begin
		ALTER TABLE settings.merchandising
			DROP COLUMN vehiclesInShowroom
	end

-- add column for Max for smartphone, FB:28289
ALTER TABLE settings.merchandising
	ADD vehiclesInShowroom tinyint NOT NULL CONSTRAINT DF_merchandising_vehiclesInShowroom DEFAULT 2
GO

