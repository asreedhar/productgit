-- FB 32397 - data driven whitelist

-- deletes for options filters!!!!
DELETE FROM settings.OptionFilter WHERE ManufactureId = 16
DELETE FROM settings.OptionFilterIncludePhrase WHERE ManufactureId = 16
DELETE FROM settings.OptionFilterExcludePhrase WHERE ManufactureId = 16
DELETE FROM dbo.ManufacturerVehicleOptionCodeWhiteList WHERE VehicleManufacturerId = 16
 
-- insert for settings.OptionFilter
INSERT INTO settings.OptionFilter VALUES(16, 199.00)

 
-- insert for settings.OptionFilterIncludePhrase
INSERT INTO settings.OptionFilterIncludePhrase VALUES(16, 'collection')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(16, 'exhaust tip')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(16, 'group')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(16, 'heated')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(16, 'keyless')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(16, 'leather seat')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(16, 'navigation')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(16, 'pack')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(16, 'pkg')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(16, 'spoiler')

 
-- insert for settings.OptionFilterExcludePhrase
INSERT INTO settings.OptionFilterExcludePhrase VALUES(16, 'cargo')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(16, 'discount')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(16, 'floor mat')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(16, 'PREFERRED ACCESSORY')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(16, 'savings')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(16, 'wheel lock')

 
-- insert for dbo.ManufacturerVehicleOptionCodeWhiteList
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 52, '32')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 24, '37-0')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 39, 'AA')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 39, 'AH')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 39, 'AL')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 39, 'AW')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 24, 'EK')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 39, 'EL')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 52, 'L3')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 39, 'L7')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 24, 'LW')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 24, 'SE')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 52, 'SE')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 39, 'VF')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 39, 'VP')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 39, 'VQ')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 39, 'WQ6')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 52, 'YA1')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 52, 'YANR2')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 52, 'Z1')
INSERT INTO dbo.ManufacturerVehicleOptionCodeWhiteList VALUES(16, 52, 'ZSVP1')