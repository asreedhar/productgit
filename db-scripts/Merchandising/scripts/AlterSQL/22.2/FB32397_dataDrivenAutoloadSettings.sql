-- FB 32397 - data driven autoload settings

-- drop optionFilter
if EXISTS (SELECT * 
			FROM dbo.sysobjects 
			WHERE id = object_id(N'[settings].[OptionFilter]') 
			AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [settings].[OptionFilter]
GO

-- drop OptionFilterIncludePhrase
if EXISTS (SELECT * 
			FROM dbo.sysobjects 
			WHERE id = object_id(N'[settings].[OptionFilterIncludePhrase]') 
			AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [settings].[OptionFilterIncludePhrase]
GO

-- drop OptionFilterExcludePhrase
if EXISTS (SELECT * 
			FROM dbo.sysobjects 
			WHERE id = object_id(N'[settings].[OptionFilterExcludePhrase]') 
			AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [settings].[OptionFilterExcludePhrase]
GO

-- create optionFilter
CREATE TABLE settings.OptionFilter
(
	ManufactureId		int NOT NULL,
	MinimumMsrp	money NOT NULL
	CONSTRAINT PK_settings_OptionFilter PRIMARY KEY (ManufactureId)
 )
 
-- create OptionFilterIncludePhrase
CREATE TABLE settings.OptionFilterIncludePhrase
(
	ManufactureId		int NOT NULL,
	IncludePhrase		varchar(100) NOT NULL
	CONSTRAINT PK_settings_OptionFilterIncludePhrase PRIMARY KEY (ManufactureId, IncludePhrase)
 )

-- create OptionFilterExcludePhrase
CREATE TABLE settings.OptionFilterExcludePhrase
(
	ManufactureId		int NOT NULL,
	ExcludePhrase		varchar(100) NOT NULL
	CONSTRAINT PK_settings_OptionFilterExcludePhrase PRIMARY KEY (ManufactureId, ExcludePhrase)
 )
 
-- settings for BMW
INSERT INTO settings.OptionFilter
	VALUES(5, .01)

-- settings for Audi
INSERT INTO settings.OptionFilter
	VALUES(4, 199.00)

INSERT INTO settings.OptionFilterIncludePhrase
	VALUES(4, 'package')
INSERT INTO settings.OptionFilterIncludePhrase
	VALUES(4, 'collection')
INSERT INTO settings.OptionFilterIncludePhrase
	VALUES(4, 'group')
INSERT INTO settings.OptionFilterIncludePhrase
	VALUES(4, 'pack')
INSERT INTO settings.OptionFilterIncludePhrase
	VALUES(4, 'pkg')

-- settings for Hyundai
INSERT INTO settings.OptionFilter
	VALUES(10, 199.00)

INSERT INTO settings.OptionFilterIncludePhrase
	VALUES(10, 'package')
INSERT INTO settings.OptionFilterIncludePhrase
	VALUES(10, 'collection')
INSERT INTO settings.OptionFilterIncludePhrase
	VALUES(10, 'group')
INSERT INTO settings.OptionFilterIncludePhrase
	VALUES(10, 'pack')
INSERT INTO settings.OptionFilterIncludePhrase
	VALUES(10, 'pkg')


-- settings for Toyota
INSERT INTO settings.OptionFilter
	VALUES(16, 199.00)

INSERT INTO settings.OptionFilterIncludePhrase
	VALUES(16, 'package')
INSERT INTO settings.OptionFilterIncludePhrase
	VALUES(16, 'collection')
INSERT INTO settings.OptionFilterIncludePhrase
	VALUES(16, 'group')
INSERT INTO settings.OptionFilterIncludePhrase
	VALUES(16, 'pack')
INSERT INTO settings.OptionFilterIncludePhrase
	VALUES(16, 'pkg')
	
INSERT INTO settings.OptionFilterExcludePhrase
	VALUES(16, 'wheel lock')
INSERT INTO settings.OptionFilterExcludePhrase
	VALUES(16, 'cargo')
INSERT INTO settings.OptionFilterExcludePhrase
	VALUES(16, 'floor mat')
INSERT INTO settings.OptionFilterExcludePhrase
	VALUES(16, 'PREFERRED ACCESSORY')



