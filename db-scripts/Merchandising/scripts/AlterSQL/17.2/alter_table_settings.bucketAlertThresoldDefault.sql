

-- if column exists ... remove it
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'emailActive'
				AND object_id = object_id('settings.BucketAlertThresholdDefault'))
	begin
		ALTER TABLE settings.BucketAlertThresholdDefault
			DROP COLUMN emailActive
	end

-- add column for bucket alert email default
ALTER TABLE settings.BucketAlertThresholdDefault
	ADD emailActive bit NULL
GO
	
UPDATE settings.BucketAlertThresholdDefault
	SET emailActive = active
GO

ALTER TABLE settings.BucketAlertThresholdDefault
	ALTER COLUMN emailActive bit NOT NULL
GO


