INSERT INTO Merchandising.dbo.ManufacturerVehicleOptionCodeWhiteList (vehicleManufacturerId, vehicleMakeId, optionCode)
SELECT DISTINCT ma.manufacturerId, di.divisionId, op.optionCode
FROM vehicleCatalog.chrome.options op
JOIN vehicleCatalog.chrome.styles st on st.styleID = op.styleId
JOIN vehicleCatalog.chrome.models mo on mo.modelId = st.modelId
JOIN vehicleCatalog.chrome.divisions di on di.divisionId = mo.divisionId
JOIN vehicleCatalog.chrome.manufacturers ma on ma.manufacturerId = di.manufacturerId
WHERE ma.countryCode=1
	AND ma.manufacturerid = 6 -- GM
	AND di.countryCode=1
	AND op.countryCode=1
	AND st.countryCode=1
	AND op.OptionCode IN (
		'LFX'
	)
