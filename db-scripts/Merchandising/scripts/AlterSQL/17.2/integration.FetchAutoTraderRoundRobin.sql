if object_id('Merchandising.integration.FetchAutoTraderRoundRobin') is not null
	drop table Merchandising.integration.FetchAutoTraderRoundRobin
go

create table Merchandising.integration.FetchAutoTraderRoundRobin
(
	RequestTypeId tinyint not null,
	BusinessUnitId int not null,
	ReportMonth datetime not null,
	DateQueued datetime not null,
	DateLastRequested datetime null
)
go

alter table Merchandising.integration.FetchAutoTraderRoundRobin add constraint PK_FetchAutoTraderRoundRobin primary key clustered
	 (RequestTypeId, BusinessUnitId, ReportMonth)
go
