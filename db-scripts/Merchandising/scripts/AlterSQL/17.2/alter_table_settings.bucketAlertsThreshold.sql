-- if column exists ... remove it
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'emailActive'
				AND object_id = object_id('settings.bucketAlertsThreshold'))
	begin
		ALTER TABLE settings.bucketAlertsThreshold
			DROP COLUMN emailActive
	end

-- add column for separate email alerts, FB:27993
ALTER TABLE settings.bucketAlertsThreshold
	ADD EmailActive bit NULL 
GO
	
UPDATE settings.bucketAlertsThreshold
	SET EmailActive = Active
GO

ALTER TABLE settings.bucketAlertsThreshold
	ALTER COLUMN EmailActive bit NOT NULL
GO
