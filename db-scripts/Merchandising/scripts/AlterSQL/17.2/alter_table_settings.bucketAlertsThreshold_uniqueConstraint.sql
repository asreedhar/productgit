if EXISTS(SELECT *
				FROM sys.indexes
				WHERE name = 'IX_BusinessUnitID_Bucket'
				AND object_id = object_id('settings.BucketAlertsThreshold'))
	begin
		DROP INDEX IX_BusinessUnitID_Bucket ON settings.BucketAlertsThreshold	
	end

CREATE UNIQUE INDEX IX_BusinessUnitID_Bucket ON settings.BucketAlertsThreshold(BusinessUnitId, Bucket)
GO