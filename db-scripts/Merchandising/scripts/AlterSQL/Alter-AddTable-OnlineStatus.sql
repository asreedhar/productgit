if exists (select * from dbo.sysobjects where id = object_id(N'[postings].[OnlineStatus]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [postings].[OnlineStatus]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [postings].[OnlineStatus](
	[businessUnitId] [int] NOT NULL,
	[inventoryId] [int] NOT NULL,
	[destinationId] [int] NOT NULL,
	[mileageMatch] [bit] NOT NULL,
	[listPriceMatch] [bit] NOT NULL,
	[descriptionMatch] [bit] NOT NULL,
	[descriptionEmpty] [bit] NOT NULL,
	CONSTRAINT [PK_OnlineStatus] PRIMARY KEY NONCLUSTERED 
	(
		[businessUnitID],
		[inventoryID],
		[destinationId]
	)
)


CREATE CLUSTERED 
INDEX IX_OnlineStatus
ON [postings].[OnlineStatus] (businessUnitId, inventoryId)
