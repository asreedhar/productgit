-- add destinationid to PK
if  object_id(N'dashboard.InventoryAdPerformanceByMonth') is not null 
	alter table dashboard.InventoryAdPerformanceByMonth drop constraint PK_InventoryAdPerformanceByMonth
go

alter table dashboard.InventoryAdPerformanceByMonth add constraint PK_InventoryAdPerformanceByMonth 
primary key clustered ( BusinessUnitId, InventoryId, Month, DestinationId )

