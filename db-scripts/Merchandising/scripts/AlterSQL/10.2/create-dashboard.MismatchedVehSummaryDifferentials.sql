if object_id('dashboard.MismatchedVehSummaryDifferentials') is null

	create table dashboard.MismatchedVehSummaryDifferentials
	(
		LoadId int not null,
		TotalUnits int not null,
		AuditedOn datetime not null,
		BusinessUnitId int not null,
		InventoryId int not null,
		Month datetime not null,

		InputSearchPageViews int not null,
		InputDetailPageViews int not null,
		InputEmailLeads int not null,
		InputMapsViewed int not null,
		InputAdPrints int not null,


		OutputSearchPageViews int not null,
		OutputDetailPageViews int not null,
		OutputEmailLeads int not null,
		OutputMapsViewed int not null,
		OutputAdPrints int not null
	)
	
go

if object_id('dashboard.PK_MismatchedVehSummaryDifferentials') is null
	alter table dashboard.MismatchedVehSummaryDifferentials add constraint PK_MismatchedVehSummaryDifferentials primary key clustered
		(
			LoadId,
			BusinessUnitId,
			InventoryId,
			Month
		)
go