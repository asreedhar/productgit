if object_id('Merchandising.dashboard.SiteMetricsByStore') is not null
	drop table Merchandising.dashboard.SiteMetricsByStore

if object_id('Merchandising.dashboard.SiteMetricsByStoreDate') is not null
	drop table Merchandising.dashboard.SiteMetricsByStoreDate
	
if object_id('Merchandising.dashboard.SiteMetricsByStoreDateInventory') is not null
	drop table Merchandising.dashboard.SiteMetricsByStoreDateInventory

if object_id('Merchandising.dashboard.SiteMetricsByStoreInventory') is not null
	drop table Merchandising.dashboard.SiteMetricsByStoreInventory