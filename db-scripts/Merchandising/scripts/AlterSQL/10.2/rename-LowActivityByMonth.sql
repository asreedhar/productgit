if object_id('Merchandising.dashboard.LowActivityByMonth') is not null
	exec Merchandising.sys.sp_rename @objname = 'dashboard.LowActivityByMonth', @newname = 'InventoryAdPerformanceByMonth'
go

if object_id('Merchandising.dashboard.PK_LowActivityByMonth') is not null
	exec Merchandising.sys.sp_rename @objname = 'dashboard.PK_LowActivityByMonth', @newname = 'PK_InventoryAdPerformanceByMonth'
go

if object_id('Merchandising.dashboard.FK_LowActivityByMonth_EdtDestinations') is not null
	exec Merchandising.sys.sp_rename @objname = 'dashboard.FK_LowActivityByMonth_EdtDestinations', @newname = 'FK_InventoryAdPerformanceByMonth_EdtDestinations'
go