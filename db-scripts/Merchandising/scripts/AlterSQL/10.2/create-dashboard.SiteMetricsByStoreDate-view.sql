if object_id('dashboard.SiteMetricsByStoreDate') is not null
	drop view dashboard.SiteMetricsByStoreDate
go

create view dashboard.SiteMetricsByStoreDate
as

select
	Date = Month,
	BusinessUnitId,
	DestinationId
from Merchandising.dashboard.RecentPerformanceSummary

go


grant select on dashboard.SiteMetricsByStoreDate to MerchandisingUser
go