-- drop table dashboard.LowActivity

if object_id('dashboard.LowActivity') is null
	create table dashboard.LowActivity
	(
		BusinessUnitId int not null
		, InventoryId int not null
		, DestinationId int not null	

		-- metrics
		, SearchPageViews int not null
		, DetailPageViews int not null
		, EmailLeads int not null
		, MapsViewed int not null
		, AdPrints int not null
	)
go

if object_id('dashboard.PK_LowActivity') is null
	alter table dashboard.LowActivity add constraint PK_LowActivity
		primary key clustered (BusinessUnitId, InventoryId, DestinationId)
go


if object_id('dashboard.FK_LowActivity_EdtDestinations') is null
	alter table dashboard.LowActivity add constraint FK_LowActivity_EdtDestinations
		foreign key (DestinationID) references settings.EdtDestinations (destinationID)
go