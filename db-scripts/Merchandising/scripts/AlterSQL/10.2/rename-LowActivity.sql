if object_id('Merchandising.dashboard.LowActivity') is not null
	exec Merchandising.sys.sp_rename @objname = 'dashboard.LowActivity', @newname = 'InventoryAdPerformance'
go

if object_id('Merchandising.dashboard.PK_LowActivity') is not null
	exec Merchandising.sys.sp_rename @objname = 'dashboard.PK_LowActivity', @newname = 'PK_InventoryAdPerformance'
go

if object_id('Merchandising.dashboard.FK_LowActivity_EdtDestinations') is not null
	exec Merchandising.sys.sp_rename @objname = 'dashboard.FK_LowActivity_EdtDestinations', @newname = 'FK_InventoryAdPerformance_EdtDestinations'
go
