-- cleanup Cars.com excess records



if object_id('tempdb.dbo.##request') is not null
	drop table ##request
go


declare
	@requestId int
	
	
declare @requestDealer table
(
	requestId int not null,
	monthRequested datetime not null,
	businessUnitId int not null,
	month datetime not null
)

create table ##request
(
	requestId int not null primary key clustered,
	startedOn datetime null
)


insert @requestDealer
(
	requestId,
	monthRequested,
	businessUnitId,
	month
)
select
	r.RequestID,
	monthRequested = dateadd(month, datediff(month, 0, r.DateCreated), 0),
	rd.BusinessUnitId,
	rd.Month
from
	STAGINGLINK.Datafeeds.Extract.FetchDotCom#Request r
	join STAGINGLINK.Datafeeds.Extract.FetchDotCom#RequestDealer rd
		on rd.RequestID = r.RequestID
where r.RequestTypeId = 3


insert ##request (requestId) select distinct(requestId) from @requestDealer


while (1=1)
begin
	select top 1 @requestId = requestId from ##request where startedOn is null order by requestId
	if (@@rowcount <> 1) break;
	update ##request set startedOn = getdate() where requestId = @requestId
	
	
	delete v
	from
		Merchandising.merchandising.CarsDotComVehSummary v
	where
		v.RequestId = @requestId
		and exists
		(
			-- we requested this dealer / month combo in the past, and the month is not the same as the month of the dateRequested
			select *
			from @requestDealer rd
			where
				businessUnitId = v.businessUnitID
				and month = v.month
				and requestId < @requestId
				and monthRequested <> v.month
		)
	
end


/*


select
	RequestId,
	RecordsScraped = count(*)
from Merchandising.merchandising.CarsDotComVehSummary
group by RequestId
order by 1

*/
