if object_id('Merchandising.dashboard.RecentPerformanceSummary') is not null
	exec Merchandising.sys.sp_rename @objname = 'dashboard.RecentPerformanceSummary', @newname = 'PerformanceSummary'
go
    
if object_id('Merchandising.dashboard.PK_RecentPerformanceSummary') is not null
	exec Merchandising.sys.sp_rename @objname = 'dashboard.PK_RecentPerformanceSummary', @newname = 'PK_PerformanceSummary'
go

if object_id('Merchandising.dashboard.FK_RecentPerformanceSummary_EdtDestinations') is not null
	exec Merchandising.sys.sp_rename @objname = 'dashboard.FK_RecentPerformanceSummary_EdtDestinations', @newname = 'FK_PerformanceSummary_EdtDestinations'
go
