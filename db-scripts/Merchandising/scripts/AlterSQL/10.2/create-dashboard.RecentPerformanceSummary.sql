-- drop table dashboard.RecentPerformanceSummary

if object_id('dashboard.RecentPerformanceSummary') is null
	create table dashboard.RecentPerformanceSummary
	(
		BusinessUnitId int not null
		, DestinationId int not null
		, Month datetime not null	

		-- metrics
		, NewSearchPageViews int not null
		, NewDetailPageViews int not null
		, NewEmailLeads int not null
		, NewMapsViewed int not null
		, NewAdPrints int not null

		, UsedSearchPageViews int not null
		, UsedDetailPageViews int not null
		, UsedEmailLeads int not null
		, UsedMapsViewed int not null
		, UsedAdPrints int not null
		
		-- misc
		
		, LastLoadId int not null
	)
go

if object_id('dashboard.PK_RecentPerformanceSummary') is null
	alter table dashboard.RecentPerformanceSummary add constraint PK_RecentPerformanceSummary
		primary key clustered (BusinessUnitId, DestinationId, Month)
go

if object_id('dashboard.FK_RecentPerformanceSummary_EdtDestinations') is null
	alter table dashboard.RecentPerformanceSummary add constraint FK_RecentPerformanceSummary_EdtDestinations
		foreign key (DestinationID) references settings.EdtDestinations (destinationID)
go