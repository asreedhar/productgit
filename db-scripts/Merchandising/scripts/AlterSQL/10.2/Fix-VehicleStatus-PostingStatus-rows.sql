use Merchandising
GO 

-- Reset Posting Status to correct values.
-- For each vehicle:
--  if DoNotPost is 0 and 
--       (there exists a Publications record with a null dateRemoved 
--        or there exists a VehicleAdScheduling record with advertisementStatus in 0, 1, 2)
--     then set statusLevel
--  otherwise set the statusLevel to the maximum of 2 or the existing PostingStatus statusLevel.

-- NOTE: This query took about 10 minutes on Int-B

with    AdState
          as (
               select   VS.BusinessUnitID ,
                        VS.InventoryID ,
                        max(isnull(VS.statusLevel, 0)) as StatusLevel ,
                        max(cast(isnull(OC.doNotPostFlag, 0) as int)) as DoNotPostFlag ,
                        cast(case when exists ( select  *
                                                from    Merchandising.postings.VehicleAdScheduling VAS
                                                where   businessUnitID = VS.businessUnitID
                                                        and inventoryId = VS.inventoryID
                                                        and advertisementStatus in (
                                                        0, 1, 2 ) ) then 1
                                  else 0
                             end as bit) as HasVehicleAdScheduling ,
                        cast(case when exists ( select  *
                                                from    Merchandising.postings.Publications PUB
                                                where   businessUnitID = VS.businessUnitID
                                                        and inventoryId = VS.inventoryID
                                                        and dateRemoved is null )
                                  then 1
                                  else 0
                             end as bit) as HasPublication
               from     Merchandising.builder.VehicleStatus VS
                        left join Merchandising.builder.OptionsConfiguration OC
                            on VS.businessUnitID = OC.businessUnitID
                               and VS.inventoryID = OC.inventoryId
               where    VS.statusTypeId = 5 -- Posting Status
               group by VS.businessUnitID ,
                        VS.inventoryID
             ),
        PostingStatus
          as (
               select   BusinessUnitId ,
                        InventoryId ,
                        StatusLevel as ExistingStatusLevel ,
                        case when DoNotPostFlag = 0
                                  and ( HasPublication = 1
                                        or HasVehicleAdScheduling = 1
                                      ) then 4
                             when StatusLevel < 2 then StatusLevel
                             else 2
                        end as DesiredStatusLevel ,
                        DoNotPostFlag ,
                        HasVehicleAdScheduling ,
                        HasPublication
               from     AdState
             )
    update  VS
    set     lastUpdatedBy = 'FB_20261' ,
            lastUpdatedOn = getdate() ,
            statusLevel = PS.DesiredStatusLevel -- NoActionRequired
    from    Merchandising.builder.VehicleStatus VS
            inner join PostingStatus PS
                on VS.businessUnitID = PS.businessUnitID
                   and VS.inventoryID = PS.InventoryID
    where   VS.statusTypeId = 5 -- Posting Status
            and VS.statusLevel <> PS.DesiredStatusLevel
