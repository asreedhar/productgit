if object_id('Merchandising.dashboard.AutoTraderLowActivity') is not null
	exec Merchandising.sys.sp_rename @objname = 'dashboard.AutoTraderLowActivity', @newname = 'LowActivityByMonth'
go
    
if object_id('Merchandising.dashboard.PK_AutoTraderLowActivity') is not null
	exec Merchandising.sys.sp_rename @objname = 'dashboard.PK_AutoTraderLowActivity', @newname = 'PK_LowActivityByMonth'
go

    
if not exists(select * from Merchandising.sys.columns where object_id = object_id('Merchandising.dashboard.LowActivityByMonth') and name = 'destinationId')
begin
	alter table Merchandising.dashboard.LowActivityByMonth add destinationId int not null constraint DF_LowActivityByMonth default (2)
	
	alter table Merchandising.dashboard.LowActivityByMonth drop constraint DF_LowActivityByMonth
	
	alter table Merchandising.dashboard.LowActivityByMonth add constraint FK_LowActivityByMonth_EdtDestinations foreign key
		(destinationId) references Merchandising.settings.EdtDestinations (destinationID)
end



if object_id('dashboard.AggregateAutoTraderVehSummary') is not null
	drop procedure dashboard.AggregateAutoTraderVehSummary
go