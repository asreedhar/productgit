-- drop table dashboard.AutoTraderLowActivity

if object_id('dashboard.AutoTraderLowActivity') is null
	create table dashboard.AutoTraderLowActivity
	(
		BusinessUnitId int not null
		, InventoryId int not null
		, Month datetime not null	

		-- metrics
		, SearchPageViews int not null
		, DetailPageViews int not null
		, EmailLeads int not null
		, MapsViewed int not null
		, AdPrints int not null

		, DateScraped datetime not null		
		, LastLoadId int not null
	)
go

if object_id('dashboard.PK_AutoTraderLowActivity') is null
	alter table dashboard.AutoTraderLowActivity add constraint PK_AutoTraderLowActivity
		primary key clustered (BusinessUnitId, InventoryId, Month)
go

go