ALTER TABLE [templates].[DefaultTemplate_Model] DROP CONSTRAINT [PK_DefaultTemplate_Model]
GO

ALTER TABLE [templates].[DefaultTemplate_Model] 
ADD  CONSTRAINT [PK_DefaultTemplate_Model] 
PRIMARY KEY CLUSTERED (	[BusinessUnitId] ASC, [ModelId] ASC, [specificityLevel] ASC, [vehicleType] ASC)
GO

ALTER TABLE [templates].[DefaultTemplate_Vehicle] DROP CONSTRAINT [PK_DefaultTemplate_Vehicle]
GO

ALTER TABLE [templates].[DefaultTemplate_Vehicle] 
ADD  CONSTRAINT [PK_DefaultTemplate_Vehicle] 
PRIMARY KEY CLUSTERED (	[BusinessUnitId] ASC, [ChromeStyleId] ASC, [vehicleType] ASC)
GO