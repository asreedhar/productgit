ALTER TABLE templates.BlurbTemplates
ADD blurbCategory INT CONSTRAINT DF_BlurbCategory DEFAULT 1 NOT NULL
GO


ALTER TABLE templates.BlurbTemplates
ADD CONSTRAINT FK_BlurbCategory
FOREIGN KEY (blurbCategory)
REFERENCES templates.BlurbCategories(categoryID)

GO

UPDATE templates.BlurbTemplates
SET blurbCategory = 2
WHERE blurbID IN (111,20,28)