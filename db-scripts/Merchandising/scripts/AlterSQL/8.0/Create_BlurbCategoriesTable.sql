
CREATE TABLE [templates].[BlurbCategories](
	[categoryID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_BlurbCategories] PRIMARY KEY CLUSTERED 
(
	[categoryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


INSERT INTO [templates].[BlurbCategories] (Name)
VALUES ('Regular')

GO

INSERT INTO [templates].[BlurbCategories] (Name)
VALUES ('Price')

GO