if exists(select * from msdb.dbo.sysjobs where name = 'Integration: Merchandising Photo Synch')
	exec msdb.dbo.sp_delete_job
		@job_id = null,
	    @job_name = 'Integration: Merchandising Photo Synch',
	    @originating_server = null,
	    @delete_history = 1,
	    @delete_unused_schedule = 1
	