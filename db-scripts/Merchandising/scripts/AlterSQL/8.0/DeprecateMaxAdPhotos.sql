use Merchandising -- for drop procedure which can not span dbs
go


-- Section One:  drop tables

if object_id('builder.photoTypes') is not null drop table builder.photoTypes

if object_id('builder.VehicleVideos') is not null drop table builder.VehicleVideos
if object_id('merchandising.GetAutoImages') is not null drop table merchandising.GetAutoImages
if object_id('settings.dealerCustomImages') is not null drop table settings.dealerCustomImages 
if object_id('settings.EdtDestinationPhotoProvider') is not null drop table settings.EdtDestinationPhotoProvider

if object_id('settings.PhotoTemplates') is not null drop table settings.PhotoTemplates
if object_id('settings.PhotoTemplateItemTypes') is not null drop table settings.PhotoTemplateItemTypes

if object_id('settings.RequiredPhotos') is not null drop table settings.RequiredPhotos



-- Section Two:  drop procs

if object_id('builder.createPhotoItem') is not null drop procedure builder.createPhotoItem
if object_id('builder.createVideo') is not null drop procedure builder.createVideo
if object_id('builder.deletePhoto') is not null drop procedure builder.deletePhoto
if object_id('builder.deleteVideo') is not null drop procedure builder.deleteVideo
if object_id('builder.getActivePhotoItemCount') is not null drop procedure builder.getActivePhotoItemCount
if object_id('builder.getBuildMerchandisingGuidance') is not null drop procedure builder.getBuildMerchandisingGuidance
if object_id('builder.getInventoryPhotos') is not null drop procedure builder.getInventoryPhotos
if object_id('builder.getPhoto') is not null drop procedure builder.getPhoto
if object_id('builder.getPhotoItemList') is not null drop procedure builder.getPhotoItemList
if object_id('builder.getPhotoItemsForReorder') is not null drop procedure builder.getPhotoItemsForReorder
if object_id('builder.getStoryboardItems') is not null drop procedure builder.getStoryboardItems
if object_id('builder.getThumbnails') is not null drop procedure builder.getThumbnails
if object_id('builder.getUploadSequenceNumber') is not null drop procedure builder.getUploadSequenceNumber
if object_id('builder.getVideo') is not null drop procedure builder.getVideo
if object_id('builder.photo#assign') is not null drop procedure builder.photo#assign
if object_id('builder.photoPlacement#Create') is not null drop procedure builder.photoPlacement#Create
if object_id('builder.photoPlacement#Delete') is not null drop procedure builder.photoPlacement#Delete
if object_id('builder.photoPlacements#Fetch') is not null drop procedure builder.photoPlacements#Fetch
if object_id('builder.photoPlacements#Update') is not null drop procedure builder.photoPlacements#Update
if object_id('builder.PhotoStatus#Update') is not null drop procedure builder.PhotoStatus#Update
if object_id('builder.photosThirdParty#Fetch') is not null drop procedure builder.photosThirdParty#Fetch
if object_id('builder.updatePhotoItem') is not null drop procedure builder.updatePhotoItem
if object_id('builder.updatePhotoSequence') is not null drop procedure builder.updatePhotoSequence
if object_id('builder.VehiclePhotosChanged_ProcessorQueue#Run') is not null drop procedure builder.VehiclePhotosChanged_ProcessorQueue#Run
if object_id('integration.MaxAdPhotosToIMS') is not null drop procedure integration.MaxAdPhotosToIMS
if object_id('integration.MerchandisingPhotos#Synchronize') is not null drop procedure integration.MerchandisingPhotos#Synchronize
if object_id('postings.getUnderMerchandisedAlerts') is not null drop procedure postings.getUnderMerchandisedAlerts
if object_id('postings.PhotoPosting#Fetch') is not null drop procedure postings.PhotoPosting#Fetch
if object_id('settings.PhotoTemplate#Fetch') is not null drop procedure settings.PhotoTemplate#Fetch
if object_id('settings.PhotoTemplateItem#Delete') is not null drop procedure settings.PhotoTemplateItem#Delete
if object_id('settings.PhotoTemplateItem#Insert') is not null drop procedure settings.PhotoTemplateItem#Insert
if object_id('settings.PhotoTemplateItem#UpdateSequence') is not null drop procedure settings.PhotoTemplateItem#UpdateSequence
if object_id('Utility.builderVehiclePhotos#CleanupInactiveInventory') is not null drop procedure Utility.builderVehiclePhotos#CleanupInactiveInventory
if object_id('settings.DealerCustomImage#Delete') is not null drop procedure settings.DealerCustomImage#Delete
if object_id('settings.DealerCustomImage#Insert') is not null drop procedure settings.DealerCustomImage#Insert
if object_id('settings.DealerCustomImages#Fetch') is not null drop procedure settings.DealerCustomImages#Fetch
if object_id('settings.PhotoTemplateItem#Upsert') is not null drop procedure settings.PhotoTemplateItem#Upsert

 
-- Section Three: drop views

if object_id('builder.ExportVehiclePhotos') is not null drop view builder.ExportVehiclePhotos



-- Section Four: drop functions

if object_id('builder.GetInventoryMaxAdPhotoList') is not null drop function builder.GetInventoryMaxAdPhotoList


-- Section Five: drop triggers

if object_id('builder.VehiclePhotosChanged') is not null drop trigger builder.VehiclePhotosChanged



-- Section Six: drop columns

if exists(
	select *
	from MerchandisingInterface.INFORMATION_SCHEMA.columns
	where
		TABLE_SCHEMA = 'Interface' 
		and TABLE_NAME = 'AdReleases'
		and COLUMN_NAME = 'photoCount'
)
	alter table MerchandisingInterface.Interface.AdReleases drop column photoCount

	
if exists(
	select *
	from MerchandisingInterface.INFORMATION_SCHEMA.columns
	where
		TABLE_SCHEMA = 'Interface' 
		and TABLE_NAME = 'AdReleases'
		and COLUMN_NAME = 'photoUrls'
)
	alter table MerchandisingInterface.Interface.AdReleases drop column photoUrls


if exists(
	select *
	from MerchandisingInterface.INFORMATION_SCHEMA.columns
	where
		TABLE_SCHEMA = 'Interface' 
		and TABLE_NAME = 'AdReleases'
		and COLUMN_NAME = 'VideoUrl'
)
	alter table MerchandisingInterface.Interface.AdReleases drop column VideoUrl
	
	


if exists(
	select *
	from Merchandising.INFORMATION_SCHEMA.columns
	where
		TABLE_SCHEMA = 'postings' 
		and TABLE_NAME = 'Publications'
		and COLUMN_NAME = 'photoCount'
)
	alter table Merchandising.postings.Publications drop column photoCount	
	
	
	
	
if exists(
	select *
	from Merchandising.INFORMATION_SCHEMA.columns
	where
		TABLE_SCHEMA = 'postings' 
		and TABLE_NAME = 'Publications'
		and COLUMN_NAME = 'hasVideo'
)
	alter table Merchandising.postings.Publications drop column hasVideo
	
go
use Reports
go
	
if object_id('Reports.Operations.InventoryPhotoCount#Fetch') is not null drop procedure Operations.InventoryPhotoCount#Fetch
if object_id('Reports.Operations.InventoryPhotoDetail_MAX#Fetch') is not null drop procedure Operations.InventoryPhotoDetail_MAX#Fetch
