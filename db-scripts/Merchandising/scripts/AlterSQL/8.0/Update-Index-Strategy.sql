if exists(select * from sys.indexes where object_id = object_id('merchandising.OptionsMapping') and name = 'OptionMap_OptionIdNDX')
	DROP INDEX merchandising.OptionsMapping.OptionMap_OptionIdNDX
go

if exists(select * from sys.indexes where object_id = object_id('merchandising.OptionsMapping') and name = 'IX_optionid_chromeCategoryId')
	DROP INDEX merchandising.OptionsMapping.IX_optionid_chromeCategoryId
go

if not exists(select * from sys.indexes where object_id = object_id('merchandising.OptionsMapping') and name = 'IX_optionid_chromeCategoryId')
	CREATE CLUSTERED INDEX IX_optionid_chromeCategoryId ON merchandising.OptionsMapping (optionid, chromeCategoryId) ON [PRIMARY]
go

