ALTER TABLE templates.VehicleProfiles
ADD vehicleType int NOT NULL constraint DF__VehicleProfiles__vehicleType default (0)
GO

ALTER TABLE templates.DefaultTemplate_Model
ADD vehicleType int NOT NULL constraint DF__DefaultTemplate_Model__vehicleType default (0)
GO

ALTER TABLE templates.DefaultTemplate_Vehicle
ADD vehicleType int NOT null constraint DF__DefaultTemplate_Vehicle__vehicleType default (0)
GO
