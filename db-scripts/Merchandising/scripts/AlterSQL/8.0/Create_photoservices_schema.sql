if not exists(
    select * 
    from information_schema.schemata 
    where Catalog_Name = 'Merchandising' and Schema_Name = 'photoservices')
begin
    exec ('create schema photoservices authorization dbo')
end
GO
