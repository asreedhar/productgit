USE [Merchandising]
GO
-- =========================================================
-- Author:			Kevin Boucher
-- Create date:		6/28/2011
-- Description:		SnippetSchemaMaintenance_06-28-2010.sql
-- This script is designed to cleanup the extranneous tables,
-- procedures and rows related to Reviews & Awards snippets.
-- =========================================================

-- Backup objects to be dropped
SELECT m.*
INTO Snippets.UserRating_Backup
FROM Snippets.UserRating m

SELECT m.*
INTO Snippets.MarketingNote_Backup
FROM Snippets.MarketingNote m

SELECT m.*
INTO Snippets.Marketing_Category_Backup
FROM Snippets.Marketing_Category m

SELECT m.*
INTO Snippets.Marketing_Parent_Backup
FROM Snippets.Marketing_Parent m

-- Load all child snippet IDs into temp table
DECLARE @ids TABLE(idx int identity(1,1), MarketingId int)
INSERT INTO @ids SELECT MarketingId FROM Snippets.Marketing_Parent

-- Drop objects
DROP TABLE Snippets.UserRating
DROP TABLE Snippets.MarketingNote
DROP TABLE Snippets.Marketing_Category
DROP TABLE Snippets.Marketing_Parent

-- Backup rows to be deleted
SELECT m.*
INTO Snippets.Marketing_Tag_Backup
FROM Snippets.Marketing_Tag m
JOIN @ids del ON del.MarketingId = m.MarketingId

SELECT m.*
INTO Snippets.Marketing_Vehicle_Backup
FROM Snippets.Marketing_Vehicle m
JOIN @ids del ON del.MarketingId = m.MarketingId

SELECT m.*
INTO Snippets.Marketing_Backup
FROM Snippets.Marketing m
JOIN @ids del ON del.MarketingId = m.Id

-- Remove child snippet rows from relevant tables
DELETE m 
FROM Snippets.Marketing_Tag m
JOIN @ids del ON del.MarketingId = m.MarketingId

DELETE m 
FROM Snippets.Marketing_Vehicle m
JOIN @ids del ON del.MarketingId = m.MarketingId

DELETE m 
FROM Snippets.Marketing m
JOIN @ids del ON del.MarketingId = m.Id

