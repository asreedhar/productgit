IF OBJECT_ID ('builder.VehiclePhotosChanged','TR') IS NOT NULL
DROP TRIGGER builder.VehiclePhotosChanged

IF (OBJECT_ID('builder.VehiclePhotosChanged_ProcessorQueue#Run') IS NOT NULL) 
DROP PROCEDURE builder.VehiclePhotosChanged_ProcessorQueue#Run

IF (OBJECT_ID('builder.VehiclePhotosChanged_ProcessorQueue') IS NOT NULL)
DROP TABLE builder.VehiclePhotosChanged_ProcessorQueue

IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'Processor: Push Ads for Changes to Photos')
BEGIN
   DECLARE @jobId varchar(100)
   SELECT @jobId = job_id FROM msdb.dbo.sysjobs_view WHERE name = N'Processor: Push Ads for Changes to Photos'
   EXEC msdb.dbo.sp_delete_job @job_id=@jobId, @delete_unused_schedule=1
END
