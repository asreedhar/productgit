USE Merchandising
IF NOT EXISTS(SELECT * FROM sys.columns WHERE [name] LIKE 'WebloaderVersion' AND [OBJECT_ID] = object_id('merchandising.webloaderphotos'))
BEGIN
	ALTER TABLE [merchandising].[webloaderphotos]
		ADD WebloaderVersion VARCHAR(20)
END
GO