
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[merchandising].[DiscountPricingCampaignInventory]') AND name = N'IXC_merchandising_DiscountPricingCampaignInventory_inventoryId')
	DROP INDEX [IXC_merchandising_DiscountPricingCampaignInventory_inventoryId] ON [merchandising].[DiscountPricingCampaignInventory] WITH ( ONLINE = OFF )
GO

CREATE CLUSTERED INDEX [IXC_merchandising_DiscountPricingCampaignInventory_inventoryId] ON [merchandising].[DiscountPricingCampaignInventory] 
(
       [InventoryId], [CampaignID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[merchandising].[DiscountPricingCampaign]') AND name = N'IXC_merchandising_DiscountPricingCampaign_businessUnitID')
DROP INDEX [IXC_merchandising_DiscountPricingCampaign_businessUnitID] ON [merchandising].[DiscountPricingCampaign] WITH ( ONLINE = OFF )
GO

CREATE CLUSTERED INDEX [IXC_merchandising_DiscountPricingCampaign_businessUnitID] ON [merchandising].[DiscountPricingCampaign] 
(
       [BusinessUnitId], [CampaignID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
