ALTER TABLE builder.optionsConfiguration
ADD lowActivityFlag [bit] NOT NULL DEFAULT(0)

ALTER TABLE audit.optionsConfiguration
ADD lowActivityFlag [bit] NOT NULL DEFAULT(0)

ALTER TABLE audit.optionsConfiguration
ALTER COLUMN lowActivityFlag [bit] NOT NULL

  