create type settings.DealerProfiles as table(
	BusinessUnitID int not null,
	BusinessUnitName varchar(40) not null,
	BusinessUnitCode varchar(20) null,
	OwnerHandle uniqueidentifier null,
	GroupId int not null,
	[Address] varchar(500) not null,
	City varchar(500) not null,
	[Description] varchar(2000) not null,
	Email varchar(500) not null,
	Phone varchar(14) not null,
	Url varchar(500) not null,
	[State] varchar(2) not null,
	ZipCode varchar(5) not null,
	HasMaxForWebsite1 bit null,
	HasMaxForWebsite2 bit null,
	HasMobileShowroom bit null,
	HasShowroom bit null
)
go

grant execute on type::settings.DealerProfiles to MerchandisingUser
go

