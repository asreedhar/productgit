-- FB 30350 - MAX Digital Showroom upgrade
alter table settings.merchandising
	add MaxDigitalShowroom bit not null constraint DF_settings_MaxDigitalShowroom default 0
go
