IF EXISTS (SELECT * FROM sys.types WHERE name = N'vehicleOption' AND is_table_type = 1)
	DROP TYPE builder.vehicleOption
GO

BEGIN
    CREATE TYPE builder.vehicleOption AS TABLE
    (
	OptionID INT NOT NULL,
	OptionCode VARCHAR(20) NULL,
	OptionText VARCHAR(300) NULL,
	DetailText VARCHAR(300) NOT NULL,
	IsStandardEquipment BIT NOT NULL DEFAULT 0
    )
END
GO
GRANT EXECUTE ON TYPE::builder.vehicleOption TO MerchandisingUser
GO
