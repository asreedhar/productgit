-- Update the Active flag on records that do have a weekly email set.
UPDATE	Merchandising.settings.BucketAlerts
SET	Active = 1
WHERE	Active = 0
	AND	LEN(RTRIM(ISNULL(WeeklyEmail, ''))) > 0
