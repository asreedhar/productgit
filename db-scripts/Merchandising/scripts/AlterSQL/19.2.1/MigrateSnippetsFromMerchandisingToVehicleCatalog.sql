-- First migrate all the new marketing text sources to vehicleCatalog
DECLARE @rankSeed INT
SELECT @rankSeed = max(DefaultRank) from VehicleCatalog.FirstLook.MarketingTextSource
INSERT INTO VehicleCatalog.FirstLook.MarketingTextSource (MarketingTextSourceID, Name, HasCopyright, DefaultRank)
SELECT id, sourceName, HasCopyright, row_number() over(order by (select NULL))+@rankSeed AS DefaultRank from Merchandising.Snippets.MarketingSource
WHERE id NOT IN (Select MarketingTextSourceID from VehicleCatalog.FirstLook.MarketingTextSource)
-- Fix the one id that was changed when we moved to merchandising.  This is ok because there are no references to that id.
DELETE FROM VehicleCatalog.FirstLook.MarketingTextSource WHERE MarketingTextSourceID = 13

-- Copy all the marketing text from Merchandising.Snippets.Marketing to VehicleCatalog.Firstlook.MarketingText
INSERT INTO VehicleCatalog.Firstlook.MarketingText (MarketingText, SourceID) 
( 
	SELECT DISTINCT marketingText, sourceId  FROM Merchandising.Snippets.Marketing sm 
	WHERE NOT EXISTS 
	(
		SELECT * from VehicleCatalog.FirstLook.MarketingText mt WHERE sm.marketingText = mt.MarketingText AND sm.sourceId = mt.SourceID
	) 
)

-- Create temp table with VehicleCatalogID to ChromeStyleID relationship
 SELECT  VehicleCatalogId, ChromeStyleId
 INTO #C2VC
 FROM    VehicleCatalog.Firstlook.VehicleCatalog_ChromeMapping cm
        INNER JOIN VehicleCatalog.Chrome.VINPatternStyleMapping vpsm ON cm.Chrome_VinMappingId = vpsm.VinMappingId
        
-- Create temp table with relationship between marketing text ids in FirstLook.MarketingText to Snippets.Marketing
 Select DISTINCT id , MarketingTextId 
 INTO #Snip2MT
 FROM Merchandising.Snippets.Marketing sm
 INNER JOIN VehicleCatalog.FirstLook.MarketingText vc ON sm.marketingText = vc.MarketingText AND sm.sourceId = vc.SourceID       
 
-- Insert missing relationships
INSERT INTO VehicleCatalog.Firstlook.MarketingTextVehicleCatalog (VehicleCatalogID, MarketingTextID)
(
	SELECT DISTINCT VehicleCatalogID, MarketingTextId  
	FROM Merchandising.Snippets.Marketing_Vehicle mv
	INNER JOIN #C2VC vc 
	ON vc.ChromeStyleid = mv.chromeStyleId
	INNER JOIN #Snip2MT vcm
	ON vcm.id = mv.marketingId
	WHERE NOT EXISTS 
	(
		SELECT * FROM VehicleCatalog.Firstlook.MarketingTextVehicleCatalog mtvc 
		WHERE mtvc.MarketingTextID = vcm.marketingTextId AND mtvc.VehicleCatalogID=vc.VehicleCatalogID
	)
)

DROP TABLE #C2VC
DROP TABLE #Snip2MT

GO