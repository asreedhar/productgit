ALTER TABLE builder.certificationFeatures
ADD MinYear INT NULL, 
MaxYear INT NULL,
CONSTRAINT CK_CertificationFeature_MinYearIsNotGreaterThanMaxYear CHECK (MinYear <= MaxYear)
GO


