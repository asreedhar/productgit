SET IDENTITY_INSERT [Merchandising].[settings].[CredentialedSites] ON

INSERT INTO [Merchandising].[settings].[CredentialedSites]
           ([siteId],[siteName])
     VALUES
           (2,'GM Global Connect')

SET IDENTITY_INSERT [Merchandising].[settings].[CredentialedSites] OFF
