--Data mappings between chrome and log provider ebiz updated

-- ChromeCategoryId was 1067
UPDATE Merchandising.Merchandising.OptionsMapping
SET ChromeCategoryId = 1132
WHERE optionid = 1287 AND providerid = 5

--ChromeCategoryId was 1068 
UPDATE Merchandising.Merchandising.OptionsMapping
SET ChromeCategoryId = 1132
WHERE optionid = 1514 AND providerid = 5

--ChromeCategoryId was null
UPDATE Merchandising.Merchandising.OptionsMapping
SET ChromeCategoryId = 1124
WHERE optionid = 20551 AND providerid = 5



