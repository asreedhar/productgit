if not exists(select * from Merchandising.INFORMATION_SCHEMA.COLUMNS where TABLE_SCHEMA = 'builder' and TABLE_NAME = 'VehiclePhotos' and COLUMN_NAME = 'IMSPhotoId')
	alter table Merchandising.builder.VehiclePhotos add IMSPhotoId int not null constraint DF_VehiclePhotos_IMSPhotoId default (-1)
go
	
if not exists(select * from Merchandising.builder.VehiclePhotos where IMSPhotoId > -1)
begin
	-- set the IMSPhotoId
	
	; WITH ActiveInventoryPhotos AS (
		SELECT 
			I.BusinessUnitID, 
			I.InventoryID, 
			P.PhotoID, 
			'https://www.firstlook.biz/digitalimages/' + P.PhotoURL AS Url
		FROM Merchandising.settings.Merchandising M
		JOIN IMT.dbo.Inventory I
			ON M.BusinessUnitID = I.BusinessUnitID
		JOIN IMT.dbo.InventoryPhotos IP
			ON I.InventoryID = IP.InventoryID
		JOIN IMT.dbo.Photos P
			ON IP.PhotoID = P.PhotoID
		WHERE I.InventoryActive = 1
	)
	update maxPix set
		IMSPhotoId = ap.PhotoID
	from
		ActiveInventoryPhotos ap
		join Merchandising.builder.VehiclePhotos maxPix
			on ap.BusinessUnitID = maxPix.businessUnitID
			and ap.InventoryID = maxPix.inventoryId
			and ap.Url = maxPix.imageUrl

	-- set everything to 0 that came from IMS but we couldn't match so that we don't try to synch it back to IMS!
	update Merchandising.builder.VehiclePhotos set
		IMSPhotoId = 0
	where
		imageUrl like 'https://www.firstlook.biz/digitalimages/%'
		and IMSPhotoId = -1
		
	update vp set
		IMSPhotoId = 0
	from Merchandising.builder.VehiclePhotos vp
	where
		IMSPhotoId = -1
		and not exists(select * from IMT.dbo.Inventory where InventoryID = vp.inventoryId and InventoryActive = 1)
end