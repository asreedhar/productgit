if object_id('audit.VehicleOptions') is null

	create table audit.VehicleOptions
	(
		rowId int identity(0,1) not null,
		actionType char(1) not null, -- [I]nsert, [U]pdate, [D]elete, [O]riginal Record/Unknown
		actionDate datetime not null,
		suser_sname nvarchar(128) not null,
		--- base table settings.VehicleOptions schema below but without nullability constraints (no need to break things to audit)

		vehicleOptionID int null,
		businessUnitID int null,
		inventoryID int null,
		optionID int null,
		optionCode varchar(20) null,
		detailText varchar(300) null,
		optionText varchar(300) null,
		isStandardEquipment bit null,
		CONSTRAINT [PK_VehicleOptions_RowID] PRIMARY KEY (rowId)
	)
	
go



-- actionType 'O' is for Original/Unknown

if not exists(select * from audit.VehicleOptions)

	insert audit.VehicleOptions (actionType, actionDate, suser_sname, vehicleOptionID, businessUnitID, inventoryID, optionID, optionCode, detailText, optionText, isStandardEquipment)
	select 'O', getdate(), suser_sname(), OP.vehicleOptionID, OP.businessUnitID, OP.inventoryID, OP.optionID, OP.optionCode, OP.detailText, OP.optionText, OP.isStandardEquipment
	from builder.VehicleOptions OP
	JOIN FLDW.dbo.InventoryActive I ON I.InventoryID = OP.InventoryID 

go
