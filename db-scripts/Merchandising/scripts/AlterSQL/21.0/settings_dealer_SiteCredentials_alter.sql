-- FB: 31248 - slurpee request workflow changes

-- drop default constraint on build request type
IF EXISTS (SELECT * 
				FROM sysobjects
				WHERE NAME ='DF_dealer_SiteCredentials_buildRequestType'
				AND xtype = 'd')
	begin
		ALTER TABLE settings.dealer_SiteCredentials
			DROP CONSTRAINT DF_dealer_SiteCredentials_buildRequestType
	end
go

-- drop column buildRequestType
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'buildRequestType'
				AND object_id = object_id('settings.dealer_SiteCredentials'))
	begin
		ALTER TABLE settings.dealer_SiteCredentials
			DROP COLUMN buildRequestType
	end
go

-- add column for buildRequestType
ALTER TABLE settings.dealer_SiteCredentials
	ADD buildRequestType TINYINT NOT NULL
	CONSTRAINT DF_dealer_SiteCredentials_buildRequestType DEFAULT 1
GO

