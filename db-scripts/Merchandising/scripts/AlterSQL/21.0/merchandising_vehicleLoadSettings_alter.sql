-- FB 31248 - surplee request workflow change

-- check for constraint
if EXISTS (SELECT * 
				FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS 
				WHERE CONSTRAINT_NAME ='FK_VehicleAutoLoadSettings_SiteCredentials')
begin
    ALTER TABLE merchandising.VehicleAutoLoadSettings
		DROP CONSTRAINT FK_VehicleAutoLoadSettings_SiteCredentials
end
go

-- check for siteID column
IF EXISTS(SELECT * FROM sys.columns 
        WHERE [name] = N'siteId' AND [object_id] = OBJECT_ID(N'merchandising.VehicleAutoLoadSettings'))
BEGIN
    ALTER TABLE merchandising.VehicleAutoLoadSettings
		DROP COLUMN siteId
END
go

-- add new column
ALTER TABLE merchandising.VehicleAutoLoadSettings
	ADD siteId int null
go
	
-- add FK constraint	
ALTER TABLE merchandising.VehicleAutoLoadSettings
	ADD CONSTRAINT FK_VehicleAutoLoadSettings_SiteCredentials
	FOREIGN KEY (siteId)
	REFERENCES settings.CredentialedSites(siteId)
go

-- see values
UPDATE merchandising.VehicleAutoLoadSettings 
	SET siteId = 1 -- dealer speed
	WHERE ManufacturerID = 5 -- bmw

UPDATE merchandising.VehicleAutoLoadSettings 
	SET siteId = 2 -- gm global connect
	WHERE ManufacturerID = 6 -- gm

UPDATE merchandising.VehicleAutoLoadSettings 
	SET siteId = 3 -- chrysler dealer connect
	WHERE ManufacturerID = 7 -- chrysler
	
UPDATE merchandising.VehicleAutoLoadSettings 
	SET siteId = 4 -- vw hub
	WHERE ManufacturerID = 24 -- vw
	
UPDATE merchandising.VehicleAutoLoadSettings 
	SET siteId = 5 -- mazda usa
	WHERE ManufacturerID = 17 -- mazda
	
