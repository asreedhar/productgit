-- drop column requeueMinutes
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'requeueMinutes'
				AND object_id = object_id('builder.AutoLoadStatusTypes'))
	begin
		ALTER TABLE builder.AutoLoadStatusTypes
			DROP COLUMN requeueMinutes
	end
go

-- add column requeueMinutes
ALTER TABLE builder.AutoLoadStatusTypes
	ADD requeueMinutes int null
go
	
-- set minutes to reprocess
UPDATE builder.AutoLoadStatusTypes
	SET requeueMinutes = 240
	WHERE statusTypeId = -100 -- unknown

UPDATE builder.AutoLoadStatusTypes
	SET requeueMinutes = 240
	WHERE statusTypeId = -99 -- invalidCredentials

UPDATE builder.AutoLoadStatusTypes
	SET requeueMinutes = 240
	WHERE statusTypeId = -98 -- Vehicle not found
	
UPDATE builder.AutoLoadStatusTypes
	SET requeueMinutes = 240
	WHERE statusTypeId = -97 -- Network error

UPDATE builder.AutoLoadStatusTypes
	SET requeueMinutes = 240
	WHERE statusTypeId = -96 -- Insufficient Permission

UPDATE builder.AutoLoadStatusTypes
	SET requeueMinutes = 240
	WHERE statusTypeId = 0 -- pending

