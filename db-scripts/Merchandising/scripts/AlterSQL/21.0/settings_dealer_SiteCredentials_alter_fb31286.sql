-- FB: 31286 - concurrency check and hash for credentials

-- drop default constraint on credential hash
IF EXISTS (SELECT * 
				FROM sysobjects
				WHERE NAME ='DF_dealer_SiteCredentials_credentialHash'
				AND xtype = 'd')
	begin
		ALTER TABLE settings.dealer_SiteCredentials
			DROP CONSTRAINT DF_dealer_SiteCredentials_credentialHash
	end
go

-- drop column credentialHash
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'credentialHash'
				AND object_id = object_id('settings.dealer_SiteCredentials'))
	begin
		ALTER TABLE settings.dealer_SiteCredentials
			DROP COLUMN credentialHash
	end
go

-- drop column credentialVersion
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'credentialVersion'
				AND object_id = object_id('settings.dealer_SiteCredentials'))
	begin
		ALTER TABLE settings.dealer_SiteCredentials
			DROP COLUMN credentialVersion
	end
go

-- add column credentialVersion
ALTER TABLE settings.dealer_siteCredentials
	ADD credentialVersion rowversion not null


-- add column for credentialHash
ALTER TABLE settings.dealer_siteCredentials
	ADD credentialHash uniqueidentifier NOT NULL
	CONSTRAINT DF_dealer_SiteCredentials_credentialHash DEFAULT newid()
GO

