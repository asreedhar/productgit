if not exists(select * from sys.indexes where object_id = object_id('dashboard.VehicleOnline') and name = 'IX_LoadId')
	create nonclustered index IX_LoadId on dashboard.VehicleOnline (LoadId)