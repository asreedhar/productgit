-- FB 24179
-- update: remove these from the whitelist
delete Merchandising.dbo.ManufacturerVehicleOptionCodeWhiteList
where VehicleManufacturerId = 6
and VehicleMakeId in (6,7,8,15)
and OptionCode in ('FE9', 'VK3')

