
-- add options to whitelist FB Case 24179

INSERT INTO [Merchandising].[dbo].[ManufacturerVehicleOptionCodeWhiteList] ([VehicleManufacturerId],[VehicleMakeId],[OptionCode])
select '6','6','S3U' 
union all
select '6','7','S3U' 
union all
select '6','8','S3U' 
union all
select '6','15','S3U'
union all
select '6','6','FE9' 
union all
select '6','7','FE9' 
union all
select '6','8','FE9' 
union all
select '6','15','FE9'union all
select '6','6','LUJ' 
union all
select '6','7','LUJ' 
union all
select '6','8','LUJ' 
union all
select '6','15','LUJ'union all
select '6','6','MF3' 
union all
select '6','7','MF3' 
union all
select '6','8','MF3' 
union all
select '6','15','MF3'union all
select '6','6','VK3' 
union all
select '6','7','VK3' 
union all
select '6','8','VK3' 
union all
select '6','15','VK3'