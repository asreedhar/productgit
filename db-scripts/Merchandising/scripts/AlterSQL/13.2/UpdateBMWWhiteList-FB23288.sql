-- add options to whitelist FB Case 23288

declare @temp table(manufacturerId int, makeId int, optionCode varchar(20))

insert into @temp( manufacturerId, makeId, optionCode )
          select 5,5,'-268'
union select 5,5,'-986'
union select 5,5,'-ASC'
union select 5,5,'-ASC+T'
union select 5,5,'-AUTO'
union select 5,5,'-AUTO-2'
union select 5,5,'-EDC'
union select 5,5,'-EP'
union select 5,5,'-LC__'
union select 5,5,'-LTD'
union select 5,5,'-LTPOP'
union select 5,5,'-SPEC'
union select 5,5,'-WHL'
union select 5,5,'072'
union select 5,5,'200'
union select 5,5,'261'
union select 5,5,'263'
union select 5,5,'268'
union select 5,5,'298'
union select 5,5,'2M8'
union select 5,5,'2MK'
union select 5,5,'2TB'
union select 5,5,'2TC'
union select 5,5,'302'
union select 5,5,'322'
union select 5,5,'323'
union select 5,5,'328'
union select 5,5,'330'
union select 5,5,'354'
union select 5,5,'359'
union select 5,5,'383'
union select 5,5,'403'
union select 5,5,'416'
union select 5,5,'456'
union select 5,5,'475'
union select 5,5,'488'
union select 5,5,'490'
union select 5,5,'496'
union select 5,5,'508'
union select 5,5,'522'
union select 5,5,'555'
union select 5,5,'609'
union select 5,5,'655'
union select 5,5,'677'
union select 5,5,'761'
union select 5,5,'850'
union select 5,5,'911'
union select 5,5,'A17'
union select 5,5,'A52'
union select 5,5,'A72'
union select 5,5,'A76'
union select 5,5,'A82'
union select 5,5,'A92'
union select 5,5,'A96'
union select 5,5,'B06'
union select 5,5,'B07'
union select 5,5,'B09'
union select 5,5,'B44'
union select 5,5,'Z8B'
union select 5,5,'ZAE'
union select 5,5,'ZAF'
union select 5,5,'ZAG'
union select 5,5,'ZAZ'
union select 5,5,'ZCQ'
union select 5,5,'ZFQ'
union select 5,5,'ZI1'
union select 5,5,'ZJH'
union select 5,5,'ZKD'
union select 5,5,'ZOX'
union select 5,5,'ZRU'
union select 5,5,'ZWI'
union select 5,5,'ZWV'

-- only add the rows that don't yet exist...

insert Merchandising.dbo.ManufacturerVehicleOptionCodeWhiteList(VehicleManufacturerId, VehicleMakeId, OptionCode)

select manufacturerId, makeId, optionCode
from @temp t
where not exists
(
	select *
	from Merchandising.dbo.ManufacturerVehicleOptionCodeWhiteList wl
	where t.manufacturerId = wl.VehicleManufacturerId
	and t.makeId = wl.VehicleMakeId
	and t.optionCode = wl.OptionCode
)
