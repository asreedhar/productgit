INSERT INTO alerts.Reports
(reportId, title)
VALUES
(6, 'Summary Alerts') 

INSERT into alerts.ReportPermissions (reportId, roleName) VALUES (6, 'Manager')
INSERT into alerts.ReportPermissions (reportId, roleName) VALUES (6, 'Approver')
INSERT into alerts.ReportPermissions (reportId, roleName) VALUES (6, 'Loader')
INSERT into alerts.ReportPermissions (reportId, roleName) VALUES (6, 'Photographer')
