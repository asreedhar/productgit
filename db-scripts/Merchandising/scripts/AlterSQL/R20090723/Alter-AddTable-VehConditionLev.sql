if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleConditionLevels]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[VehicleConditionLevels]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [builder].[VehicleConditionLevels](
	[levelId] int,
	[description] varchar(10),
	CONSTRAINT [PK_ConditionLevel] PRIMARY KEY 
	(
		[levelId]		
	)
)
GO

INSERT INTO builder.VehicleConditionLevels (levelId, description) VALUES (1,'Excellent')
INSERT INTO builder.VehicleConditionLevels (levelId, description) VALUES (2,'Good')
INSERT INTO builder.VehicleConditionLevels (levelId, description) VALUES (3,'Fair')
INSERT INTO builder.VehicleConditionLevels (levelId, description) VALUES  (4,'Poor')
GO