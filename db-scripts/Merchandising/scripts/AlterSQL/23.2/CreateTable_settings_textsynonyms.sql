--DROP TABLE Merchandising.settings.TextSynonyms

CREATE TABLE Merchandising.settings.TextSynonyms
(
	TextValue VARCHAR(100) NOT NULL,
	TextSynonym VARCHAR(100) NOT NULL,
	LastModified DATETIME NOT NULL CONSTRAINT DF_Merchandising_Settings_TextSynonyms_LastModified DEFAULT (GETDATE())
)
GO

ALTER TABLE Merchandising.settings.TextSynonyms
	ADD CONSTRAINT PK_Merchandising_Settings_TextSynonyms
	PRIMARY KEY CLUSTERED (TextValue,TextSynonym)
GO

INSERT INTO Merchandising.settings.TextSynonyms(TextValue,TextSynonym)VALUES('A/C', 'Air Conditioning');
INSERT INTO Merchandising.settings.TextSynonyms(TextValue,TextSynonym)VALUES('A/C', 'AC');
INSERT INTO Merchandising.settings.TextSynonyms(TextValue,TextSynonym)VALUES('FWD', 'Front Wheel Drive');
INSERT INTO Merchandising.settings.TextSynonyms(TextValue,TextSynonym)VALUES('RWD', 'Rear Wheel Drive');
INSERT INTO Merchandising.settings.TextSynonyms(TextValue,TextSynonym)VALUES('4WD', 'Four Wheel Drive');
INSERT INTO Merchandising.settings.TextSynonyms(TextValue,TextSynonym)VALUES('4WD', '4x4');



