use Merchandising

update settings.Franchise set name = 'BMW' where franchiseId = 1
update settings.Franchise set name = 'Buick' where franchiseId = 2
update settings.Franchise set name = 'Cadillac' where franchiseId = 3
update settings.Franchise set name = 'Chevrolet' where franchiseId = 4
update settings.Franchise set name = 'GMC' where franchiseId = 5
update settings.Franchise set name = 'Honda' where franchiseId = 6
update settings.Franchise set name = 'Chrysler' where franchiseId = 7
update settings.Franchise set name = 'Ford' where franchiseId = 8

delete from settings.Franchise where franchiseId = 9