use Merchandising
GO

if not exists(select * 
              from INFORMATION_SCHEMA.columns 
              where TABLE_SCHEMA = 'settings' and TABLE_NAME = 'Merchandising' and COLUMN_NAME = 'MaxVersion')
begin
    print 'Adding MaxVersion, WebLoaderEnabled, and ModelLevelFrameworksEnabled fields to settings.Merchandising'
    alter table settings.Merchandising
	    add MaxVersion tinyint not null constraint DF_Merchandising_MaxVersion default (2),
	        WebLoaderEnabled bit not null constraint DF_Merchandising_WebLoaderEnabled default (0),
	        ModelLevelFrameworksEnabled bit not null constraint DF_Merchandising_ModelLevelFrameworksEnabled default (0)
    print 'Done.'
end
GO