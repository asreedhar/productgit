set nocount on

/*
	WARNING! this script must delete VehicleProfiles, AdTemplates, and AdTemplate items to work correctly.  That means
	if a dealer has made a custom edit to a framework/blurb, the changes will be lost.
	
	
	WARNING WARNING WARNING!!!!!!  this script will destroy data if the business unit from which you want to clone 
	also exists in the list of businesses to clone TO!!!!!!!
	
	Check the templateId list, make sure it only comes from one distinct business unit, and then make sure that business unit
	DOES NOT EXIST in the CLONE TO list!!!!!!!
	
	
	
	!!!!!!!
	
	
	
	
	
	this script will clone templates, but it must be edited to properly populate the
	
		@stores table variable with a list of stores to clone to
		@templates table with a list of pre-existing templateids to copy
		
	this script is smart enough to not duplicate templates, in other words, if templateID 10 is in the @templates list,
	and it already exists for businessUnit 165900 in adTemplates as evidenced by having a record in there for that businessUnitID
	and that SourceTemplateId (10), it will still function properly.
	
	in other words, this script should always clone the @templates to the other @stores, even if they already exist.
	
	once the templates are created, it will also create the associated AdTemplateItems (by deleteing any that already exist and then
	inserting the ones associated with the original @templates list)
	
	this script also deep copies the VehicleProfile records.
	
*/

declare @stores table (BusinessUnitID int not null)
declare @templates table (templateID int not null)


insert @stores (BusinessUnitID)
select distinct BusinessUnitID
from IMT.dbo.BusinessUnit
where
	BusinessUnitTypeID = 4
	and	BusinessUnitCode in (
		 'ALLSTARD01'
		, 'ALLSTARF01'
		, 'ANDERSON10'
		, 'STANLEYF01'
		, 'AUTOFAIR04'
		, 'PERFORMA10'
		, 'BILLPIER02'
		, 'BLUEBONN05'
		, 'STANLEYF02'
		, 'CHAMPION13'
		, 'CHARLIEG02'
		, 'STANLEYF03'
		, 'COLESTOR01'
		, 'DELRIOFO01'
		, 'STANLEYF04'
		, 'EDSHULTS03'
		, 'FAULKNER02'
		, 'FAULKNER03'
		, 'ANCIRAFO01'
		, 'SUNCOAST01'
		, 'FREEDOMF02'
		, 'FREEWAYF01'
		, 'HARRISFO01'
		, 'HOBLITMO01'
		, 'HONDAOFO01'
		, 'HUDSONFO01'
		, 'IMPERIAL03'
		, 'JENKINSA02'
		, 'JIMBUTLE02'
		, 'KENNYROS04'
		, 'KENNYROS05'
		, 'KRIEGERM02'
		, 'LAKEWOOD02'
		, 'LANDERSF03'
		, 'LANDMARK09'
		, 'LEWISFOR02'
		, 'LUTHERFA02'
		, 'MACKENZI02'
		, 'STANLEYF05'
		, 'METROPOL02'
		, 'METROFOR03'
		, 'MIKEBARN02'
		, 'MIKEGRAI01'
		, 'MIKEDAVI02'
		, 'MIKERAIS04'
		, 'NEWPRAGU02'
		, 'NORTHCAS02'
		, 'NORTHCOU01'
		, 'PASOROBL02'
		, 'PERFORMA22'
		, 'PHILLONG14'
		, 'PHILLONG04'
		, 'PHILLONG02'
		, 'PHILLONG01'
		, 'PHILLONG13'
		, 'PRIMEFLM01'
		, 'PUNDMANN04'
		, 'RICKRIDI02'
		, 'RUSSELLB02'
		, 'SANDERSF02'
		, 'SKALNEKF02'
		, 'SUPERIOR10'
		, 'STANLEYF06'
		, 'WORDENMA03'
		, 'WRAYFORD02'
	)

insert @templates (templateID)
select at.templateID
from
	Merchandising.templates.AdTemplates at
	join 
	(
		select templateID = 9337, name = 'Ford Edge'
		union all select templateID = 9338, name = 'Ford Edge New'
		union all select templateID = 9328, name = 'Ford Escape'
		union all select templateID = 9687, name = 'Ford Escape Early Model'
		union all select templateID = 9329, name = 'Ford Escape New'
		union all select templateID = 9609, name = 'Ford Expedition'
		union all select templateID = 9610, name = 'Ford Expedition New'
		union all select templateID = 9339, name = 'Ford Explorer'
		union all select templateID = 9376, name = 'Ford Explorer 2011, 2012'
		union all select templateID = 9688, name = 'Ford Explorer Early Model'
		union all select templateID = 9340, name = 'Ford Explorer New'
		union all select templateID = 9613, name = 'Ford F-150'
		union all select templateID = 9614, name = 'Ford F-150 New'
		union all select templateID = 9316, name = 'Ford Fiesta'
		union all select templateID = 9317, name = 'Ford Fiesta New'
		union all select templateID = 9319, name = 'Ford Fiesta New Sedan'
		union all select templateID = 9318, name = 'Ford Fiesta Sedan'
		union all select templateID = 9606, name = 'Ford Flex'
		union all select templateID = 9607, name = 'Ford Flex New'
		union all select templateID = 9320, name = 'Ford Focus'
		union all select templateID = 9685, name = 'Ford Focus Early Model'
		union all select templateID = 9321, name = 'Ford Focus New'
		union all select templateID = 9322, name = 'Ford Fusion'
		union all select templateID = 9323, name = 'Ford Fusion New'
		union all select templateID = 9324, name = 'Ford Mustang'
		union all select templateID = 9325, name = 'Ford Mustang New'
		union all select templateID = 9611, name = 'Ford Ranger'
		union all select templateID = 9612, name = 'Ford Ranger New'
		union all select templateID = 9326, name = 'Ford Taurus'
		union all select templateID = 9686, name = 'Ford Taurus Early Model'
		union all select templateID = 9327, name = 'Ford Taurus New'
		union all select templateID = 9619, name = 'Lincoln MKS'
		union all select templateID = 9620, name = 'Lincoln MKS New'
		union all select templateID = 9621, name = 'Lincoln MKT'
		union all select templateID = 9622, name = 'Lincoln MKT New'
		union all select templateID = 9617, name = 'Lincoln MKX'
		union all select templateID = 9618, name = 'Lincoln MKX New'
		union all select templateID = 9615, name = 'Lincoln MKZ'
		union all select templateID = 9616, name = 'Lincoln MKZ New'
		union all select templateID = 9647, name = 'Lincoln Navigator'
		union all select templateID = 9649, name = 'Lincoln Navigator L'
		union all select templateID = 9650, name = 'Lincoln Navigator L New'
		union all select templateID = 9648, name = 'Lincoln Navigator New'
		union all select templateID = 9630, name = 'Lincoln Town Car'
		union all select templateID = 9631, name = 'Lincoln Town Car New'
	) list
	on at.templateID = list.templateID


--- do not edit script below this line, only edit the stores and templates list above

set xact_abort on
set transaction isolation level serializable
begin transaction

declare
	@rows int,
	@businessUnitId int,
	@sourceTemplateId int,
	@vehicleProfileId int


declare @storeProfiles table
(
	BusinessUnitID int not null,
	sourceTemplateId int not null,
	vehicleProfileId int null
)

insert @storeProfiles
(
	BusinessUnitID,
	sourceTemplateId
)
select
	s.BusinessUnitID,
	t.templateID
from
	@stores s
	cross join @templates t


declare @vpToDelete table
(
	vehicleProfileID int
)

-- delete all VehicleProfile, AdTemplate, and AdTemplateItems for the new stores 
-- that are associated with the "master" templates we are about to clone.  these
-- records might not exist, but if they do this script will not correctly map the
-- deltas that may occur if a store only has a partial framework frmo the masters.

insert @vpToDelete
(
	vehicleProfileID
)
select
	vp.vehicleProfileID
from
	Merchandising.templates.VehicleProfiles vp
	join Merchandising.templates.AdTemplates at
		on vp.vehicleProfileID = at.vehicleProfileId
	join @storeProfiles sp
		on at.SourceTemplateId = sp.sourceTemplateId
		and at.businessUnitID = sp.BusinessUnitID
		and vp.businessUnitId = sp.BusinessUnitID
	
set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' pre-exising VehicleProfiles staged for delete.'

delete ati
from
	Merchandising.templates.AdTemplateItems ati
	join Merchandising.templates.AdTemplates at
		on ati.templateID = at.templateID
	join @storeProfiles sp
		on at.businessUnitID = sp.BusinessUnitID
		and at.SourceTemplateId = sp.sourceTemplateId

set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' pre-exising AdTemplateItems deleted.'

delete at
from
	Merchandising.templates.AdTemplates at
	join @storeProfiles sp
		on at.SourceTemplateId = sp.sourceTemplateId
		and at.businessUnitID = sp.BusinessUnitID

set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' pre-exising AdTemplates deleted.'

delete vp
from
	Merchandising.templates.VehicleProfiles vp
	join @vpToDelete d
		on vp.vehicleProfileID = d.vehicleProfileID

set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' pre-exising VehicleProfiles deleted.'
		

while (1=1)
begin
	select top 1
		@businessUnitId = BusinessUnitID,
		@sourceTemplateId = sourceTemplateId
	from @storeProfiles
	where vehicleProfileId is null
	
	if (@@rowcount <> 1) break;
	
	insert Merchandising.templates.VehicleProfiles
	( 
		businessUnitId,
		title,
		makeIDList,
		modelList,
		marketClassIdList,
		startYear,
		endYear,
		minAge,
		maxAge,
		minPrice,
		maxPrice,
		certifiedStatusFilter,
		trimList,
		segmentIdList,
		vehicleType
	)
	select
		@businessUnitID,
		vp.title,
		vp.makeIDList,
		vp.modelList,
		vp.marketClassIdList,
		vp.startYear,
		vp.endYear,
		vp.minAge,
		vp.maxAge,
		vp.minPrice,
		vp.maxPrice,
		vp.certifiedStatusFilter,
		vp.trimList,
		vp.segmentIdList,
		vp.vehicleType	
	from
		Merchandising.templates.AdTemplates at
		join Merchandising.templates.VehicleProfiles vp
			on at.vehicleProfileId = vp.vehicleProfileID
	where at.templateID = @sourceTemplateId
	
	select @rows = @@rowcount, @vehicleProfileId = scope_identity()
	
	if (@rows <> 1) raiserror('incorrect rows inserted into vehicle profiles!', 16, 1)
	
	update @storeProfiles set
		vehicleProfileId = @vehicleProfileId
	where
		BusinessUnitID = @businessUnitId
		and sourceTemplateId = @sourceTemplateId
	
	set @rows = @@rowcount
	if (@rows <> 1) raiserror('incorrect rows updated in local table!', 16, 1)
	
end	-- VehicleProfile insert loop


print ''
print ''

select @rows = count(*) from @storeProfiles
print convert(varchar(10), @rows) + ' new VehicleProfiles added.'



-- insert the new templates
insert Merchandising.templates.AdTemplates
(
	name,
	businessUnitID,
	vehicleProfileId,
	parent,
	active,
	SourceTemplateId
)
select
	at.name,
	sp.BusinessUnitID,
	sp.vehicleProfileId,
	at.parent,
	at.active,
	SourceTemplateId = at.templateID
from
	Merchandising.templates.AdTemplates at
	join @storeProfiles sp
		on at.templateID = sp.sourceTemplateId
	
set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' new templates added.'
	
	
	
	
	

-- ok go clone the AdTemplateItems tied to @templates list to the new @stores list
insert Merchandising.templates.AdTemplateItems
(
	templateID,
	blurbID,
	sequence,
	priority,
	required,
	parentID
)
select
	newTemplates.templateID,
	ati.blurbID,
	ati.sequence,
	ati.priority,
	ati.required,
	ati.parentID
from
	Merchandising.templates.AdTemplateItems ati
	join Merchandising.templates.AdTemplates at
		on ati.templateID = at.templateID
	join @templates t
		on at.templateID = t.templateID
	join Merchandising.templates.AdTemplates newTemplates
		on newTemplates.SourceTemplateId = at.templateID
	join @stores s
		on newTemplates.businessUnitID = s.BusinessUnitID


set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' new template items added.'

commit
set xact_abort off
set transaction isolation level read uncommitted

