ALTER TABLE settings.Merchandising
ADD franchiseId INT NOT NULL CONSTRAINT DF_settings_Merchandising_franchiseId DEFAULT (0)

ALTER TABLE settings.Merchandising
ADD CONSTRAINT FK__settings_Merchandising_franchiseId FOREIGN KEY (franchiseId) REFERENCES settings.Franchise(franchiseId)