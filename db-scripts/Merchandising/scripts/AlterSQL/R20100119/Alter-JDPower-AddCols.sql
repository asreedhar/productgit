ALTER TABLE merchandising.JdPowerRatings 
ADD Predicted_Reliability float NULL
GO

ALTER TABLE merchandising.JdPowerRatings 
ADD Auto_Ratings_Page_Url varchar(200) NULL
GO
