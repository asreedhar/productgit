

CREATE TABLE templates.BlurbSampleText_DealerOverride (
	overrideId int identity(1,1),
	businessUnitId int,
	blurbTextId int,
	enabled bit,
	CONSTRAINT PK_BlurbSampleText_DealerOverride PRIMARY KEY (overrideId)	
)

--could create foreign key on disableSampleTextId, but if FirstLook library loses
--a sampleTextItem from deletion, the meaning of this is unchanged