--86  AFFORDABILITY HEADER
--87  T3 Mission HEADER
--88  Risk HEADER
--89  Accolades and Awards HEADER
--90  Key Features HEADER
--92  Dealer Differentiator HEADER

insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'AFFORDABLE: ', '', 86, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'AFFORDABILITY: ', '', 86, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'AFFORDABLE TO OWN: ', '', 86, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'GREAT OFFERS: ', '', 86, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'SHOP SMART: ', '', 86, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'GET A BARGAIN: ', '', 86, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'A GREAT TIME TO BUY: ', '', 86, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'A GREAT VALUE: ', '', 86, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'EXCELLENT VALUE: ', '', 86, NULL, 1, 1, NULL)

insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'SET YOUR MIND AT EASE: ', '', 88, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'PURCHASE WITH CONFIDENCE: ', '', 88, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'INVEST WISELY: ', '', 88, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'A WISE INVESTMENT: ', '', 88, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'BUY WITH CONFIDENCE: ', '', 88, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'SHOP WITH CONFIDENCE: ', '', 88, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'NO MORE WORRIES: ', '', 88, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'NO NEED TO WORRY: ', '', 88, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'WORRY FREE: ', '', 88, NULL, 1, 1, NULL)

insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'EXPERTS AGREE: ', '', 89, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'EXPERTS CONCLUDE: ', '', 89, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'EXPERTS ARE SAYING: ', '', 89, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'VEHICLE REVIEWS: ', '', 89, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, '[Model] Reviews: ', '', 89, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, '[Model] Accolades: ', '', 89, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'REVIEWERS SAY: ', '', 89, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'EXPERTS REPORT: ', '', 89, NULL, 1, 1, NULL)

insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'FEATURES: ', '', 90, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'EQUIPPED WITH: ', '', 90, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'FEATURES INCLUDE: ', '', 90, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'COMES WITH: ', '', 90, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'LOADED WITH: ', '', 90, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'GREAT EQUIPMENT: ', '', 90, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'WELL EQUIPPED: ', '', 90, NULL, 1, 1, NULL)

insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'ABOUT US: ', '', 92, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'WHO WE ARE: ', '', 92, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'ABOUT OUR DEALERSHIP: ', '', 92, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'OUR DEALERSHIP: ', '', 92, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'WHY BUY FROM US: ', '', 92, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'MORE ABOUT US: ', '', 92, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'VISIT US TODAY:  ', '', 92, NULL, 1, 1, NULL)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'OUR OFFERINGS: ', '', 92, NULL, 1, 1, NULL) 