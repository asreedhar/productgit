if not exists (select * from [Merchandising].[builder].[AutoloadStatusTypes] where [statusTypeId] = -96)
BEGIN
INSERT INTO [Merchandising].[builder].[AutoloadStatusTypes]
           ([statusTypeId]
           ,[statusType]
           ,[statusTypeDescription])     
VALUES('-96' ,'InsufficientPermissions','The credentials supplied do not have sufficient permissions.');
END
GO