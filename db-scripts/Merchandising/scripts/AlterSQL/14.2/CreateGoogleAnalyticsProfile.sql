CREATE TABLE [settings].[GoogleAnalyticsProfile](
	[BusinessUnitId] [int] NOT NULL,
	[Type] [int] NOT NULL,
	[ProfileId] [int] NOT NULL,
	[ProfileName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_settings.GoogleAnalyticsProfile] PRIMARY KEY CLUSTERED 
(
	[BusinessUnitId] ASC,
	[Type] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


