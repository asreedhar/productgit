CREATE TABLE [settings].[GoogleAnalyticsToken](
	[BusinessUnitId] [int] NOT NULL,
	[ClientId] [varchar](150) NOT NULL,
	[ClientSecret] [varchar](100) NOT NULL,
	[RefreshToken] [varchar](100) NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_settings.GoogleAnalyticsToken] PRIMARY KEY CLUSTERED 
(
	[BusinessUnitId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


