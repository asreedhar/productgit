IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[settings].[Dealer_SiteCredentials_20131220_after]') AND type in (N'U'))
DROP TABLE [settings].[Dealer_SiteCredentials_20131220_after]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[settings].[Dealer_SiteCredentials_20131220_before]') AND type in (N'U'))
DROP TABLE [settings].[Dealer_SiteCredentials_20131220_before]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[settings].[Dealer_SiteCredentials_20131224_after]') AND type in (N'U'))
DROP TABLE [settings].[Dealer_SiteCredentials_20131224_after]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[settings].[Dealer_SiteCredentials_20131224_before]') AND type in (N'U'))
DROP TABLE [settings].[Dealer_SiteCredentials_20131224_before]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[settings].[Dealer_SiteCredentials_20140203_after]') AND type in (N'U'))
DROP TABLE [settings].[Dealer_SiteCredentials_20140203_after]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[settings].[Dealer_SiteCredentials_20140203_before]') AND type in (N'U'))
DROP TABLE [settings].[Dealer_SiteCredentials_20140203_before]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[settings].[Dealer_SiteCredentials_20140313_after]') AND type in (N'U'))
DROP TABLE [settings].[Dealer_SiteCredentials_20140313_after]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[settings].[Dealer_SiteCredentials_20140313_before]') AND type in (N'U'))
DROP TABLE [settings].[Dealer_SiteCredentials_20140313_before]

