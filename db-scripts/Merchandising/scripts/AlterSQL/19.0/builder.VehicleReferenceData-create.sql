/****** Object:  Table [builder].[VehicleReferenceData]    Script Date: 04/02/2014 16:29:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [builder].[VehicleReferenceData](
	[VehicleId] [int] NOT NULL,
	[DataTypeId] [int] NOT NULL,
	[DataSource] [nvarchar](500) NOT NULL,
	[Value] [nvarchar](250) NULL
) ON [PRIMARY]

GO

ALTER TABLE [builder].[VehicleReferenceData]  WITH CHECK ADD  CONSTRAINT [FK_VehicleReferenceData_ReferenceDataTypes] FOREIGN KEY([DataTypeId])
REFERENCES [builder].[ReferenceDataTypes] ([DataTypeId])
GO

ALTER TABLE [builder].[VehicleReferenceData] CHECK CONSTRAINT [FK_VehicleReferenceData_ReferenceDataTypes]
GO


