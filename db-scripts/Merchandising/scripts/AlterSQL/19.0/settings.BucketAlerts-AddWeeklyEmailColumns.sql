alter table settings.BucketAlerts
add 
	WeeklyEmail [varchar](1000) NULL,
	WeeklyEmailBounced bit not null CONSTRAINT DF_settings_BucketAlerts_WeeklyEmailBounced DEFAULT (0)
GO
