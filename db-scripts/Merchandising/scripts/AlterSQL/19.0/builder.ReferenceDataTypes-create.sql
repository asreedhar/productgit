/****** Object:  Table [builder].[ReferenceDataTypes]    Script Date: 04/02/2014 16:30:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [builder].[ReferenceDataTypes](
	[DataTypeId] [int] IDENTITY(1,1) NOT NULL,
	[DataType] [nvarchar](50) NOT NULL,
	CONSTRAINT [PK_ReferenceDataTypes] PRIMARY KEY CLUSTERED 
	(
		[DataTypeId] ASC
	)) ON [PRIMARY]

GO


