IF NOT EXISTS (
	SELECT *
	FROM Merchandising.INFORMATION_SCHEMA.COLUMNS
	WHERE TABLE_SCHEMA = 'settings'
	AND TABLE_NAME = 'DealerProfilePublishedSnapshot'
	AND COLUMN_NAME = 'HasProfitMAX'
)
BEGIN
	ALTER TABLE Merchandising.settings.DealerProfilePublishedSnapshot
	ADD HasProfitMAX BIT
END