UPDATE Merchandising.settings.MostSearchedEquipment
SET active = 0
WHERE searchKeywords LIKE '%Overhead Airbag%'

UPDATE Merchandising.settings.EquipmentDisplayRankings
SET tierNumber = 4
WHERE categoryID IN (1007,1008)

UPDATE Merchandising.settings.EquipmentDisplayRankings
SET displayPriority = 999999
WHERE categoryID IN (1007,1008)