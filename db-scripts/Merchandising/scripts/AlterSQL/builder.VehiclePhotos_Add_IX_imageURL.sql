if not exists(select * from Merchandising.sys.indexes where object_id = object_id('builder.VehiclePhotos') and name = 'IX_imageUrl')
	create nonclustered index IX_imageUrl on Merchandising.builder.VehiclePhotos (imageUrl)
go