-- if column default ... remove it
if EXISTS (SELECT * 
				FROM sysobjects
				WHERE TYPE = 'D'
				AND NAME = 'DF_merchandising_MAXForWebsite20')
	begin
		ALTER TABLE settings.merchandising
			DROP CONSTRAINT DF_merchandising_MAXForWebsite20
	end
	
-- if column exists ... remove it
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'MAXForWebsite20'
				AND object_id = object_id('settings.merchandising'))
	begin
		ALTER TABLE settings.merchandising
			DROP COLUMN MAXForWebsite20
	end

-- add column for Max for smartphone, FB:30138
ALTER TABLE settings.merchandising
	ADD MAXForWebsite20 bit NOT NULL CONSTRAINT DF_merchandising_MAXForWebsite20 DEFAULT 0
GO

