

-----Loader notes
ALTER TABLE builder.loaderNotes
DROP CONSTRAINT PK_LoaderNotes

ALTER TABLE builder.loaderNotes
ADD CONSTRAINT PK_LoaderNotes
PRIMARY KEY NONCLUSTERED (noteId)

CREATE CLUSTERED 
INDEX NDX_LoaderNotes
ON builder.LoaderNotes (businessUnitId, inventoryId)

ALTER TABLE builder.LoaderNotes
ADD CONSTRAINT FK_LoaderNotes_OptionsConfiguration
FOREIGN KEY (businessUnitId, inventoryId)
REFERENCES builder.optionsConfiguration (businessUnitId, inventoryId)
------end loder notes

ALTER TABLE builder.financingSpecials
ADD CONSTRAINT PK_FinancingSpecials
PRIMARY KEY NONCLUSTERED (specialId)

CREATE CLUSTERED 
INDEX NDX_FinancingSpecials_OwnerEntity
ON builder.financingSpecials(ownerEntityTypeId, ownerEntityId)

---------NO FK NEEDED, BECAUSE THE FINANCING SPECIAL's VEHICLE PROFILE MAY NOT BELONG TO THE BUSINESS UNIT IN THIS TABLE
--ALTER TABLE builder.financingSpecials
--ADD CONSTRAINT FK_FinancingSpecials_VehicleProfileId
--FOREIGN KEY (vehicleProfileId)
--REFERENCES templates.VehicleProfiles (vehicleProfileId)


-------conditions---------
ALTER TABLE builder.vehicleConditionTypes
ADD CONSTRAINT PK_VehicleConditionTypes
PRIMARY KEY (vehicleConditionTypeId)

ALTER TABLE builder.vehicleConditions
DROP CONSTRAINT PK_VehicleConditions

ALTER TABLE builder.vehicleConditions
ADD CONSTRAINT PK_VehicleConditions
PRIMARY KEY NONCLUSTERED (vehicleConditionID)

CREATE CLUSTERED
INDEX NDX_VehicleConditions
ON builder.vehicleConditions (businessUnitId, inventoryId)

ALTER TABLE builder.vehicleConditions
ADD CONSTRAINT FK_VehicleConditions_VehicleConditionTypes
FOREIGN KEY (vehicleConditionTypeId)
REFERENCES builder.vehicleConditionTypes (vehicleConditionTypeId)

DELETE vc FROM 
builder.vehicleConditions vc
LEFT JOIN builder.optionsConfiguration oc
ON oc.businessUnitId = vc.businessUnitId
AND oc.inventoryId = vc.inventoryId
WHERE
oc.inventoryId IS NULL

ALTER TABLE builder.vehicleConditions
ADD CONSTRAINT FK_VehicleConditions_OptionsConfiguration
FOREIGN KEY (businessUnitId, inventoryId)
REFERENCES builder.optionsConfiguration (businessUnitId, inventoryId)

ALTER TABLE builder.vehicleConditionAdjectives
ADD CONSTRAINT PK_VehicleConditionAdjectives
PRIMARY KEY NONCLUSTERED (conditionId)

CREATE CLUSTERED
INDEX NDX_VehicleConditionAdjectives_BusinessUnitId
ON builder.vehicleConditionAdjectives (businessUnitId)


--------------------------
ALTER TABLE builder.VehiclePhotos
DROP CONSTRAINT PK_VehiclePhotos_1

ALTER TABLE builder.VehiclePhotos
ADD CONSTRAINT PK_VehiclePhotos
PRIMARY KEY NONCLUSTERED (photoItemId)

--change inventoryid to not null...why was it?
DELETE FROM builder.VehiclePhotos WHERE inventoryId IS NULL

ALTER TABLE builder.VehiclePhotos
ALTER COLUMN inventoryId INT NOT NULL

CREATE CLUSTERED  
INDEX NDX_VehiclePhotos
ON builder.vehiclePhotos (businessUnitId, inventoryId)

DELETE vp FROM 
builder.vehiclePhotos vp
LEFT JOIN builder.optionsConfiguration oc
ON oc.businessUnitId = vp.businessUnitId
AND oc.inventoryId = vp.inventoryId
WHERE
oc.inventoryId IS NULL

--Logic for FK is faulty; OptionConfiguration may be missing Active Inventory items
--However, Wanamaker.WebApp always goes to fetch Active Inventory not from OptionConfiguration
--This prevents VehiclesPhotos from being populated correctly, as percieved by the end user
--BUGZID: 12598
ALTER TABLE builder.VehiclePhotos
ADD CONSTRAINT FK_VehiclePhotos_OptionsConfiguration
FOREIGN KEY (businessUnitId, inventoryId)
REFERENCES builder.OptionsConfiguration (businessUnitId, inventoryId)

ALTER TABLE builder.PhotoTypes
ADD CONSTRAINT PK_PhotoTypes
PRIMARY KEY (imageTypeId)

---------statuses-------------
ALTER TABLE builder.VehicleStatus
DROP CONSTRAINT PK_VehicleStatus

ALTER TABLE builder.VehicleStatus
ADD CONSTRAINT PK_VehicleStatus
PRIMARY KEY NONCLUSTERED (statusId)

CREATE CLUSTERED  
INDEX NDX_VehicleStatus
ON builder.vehicleStatus (businessUnitId, inventoryId)


DELETE vs FROM 
builder.vehicleStatus vs
LEFT JOIN builder.optionsConfiguration oc
ON oc.businessUnitId = vs.businessUnitId
AND oc.inventoryId = vs.inventoryId
WHERE
oc.inventoryId IS NULL

ALTER TABLE builder.VehicleStatus
ADD CONSTRAINT FK_VehicleStatus_OptionsConfiguration
FOREIGN KEY (businessUnitId, inventoryId)
REFERENCES builder.OptionsConfiguration (businessUnitId, inventoryId)



