--now add jdpower blurbId to the merchandisingpoints table
declare @blurbId int

insert into templates.BlurbTemplates (blurbName, blurbTemplate, blurbCondition, blurbTypeId, businessUnitId) VALUES ('JD Power Ratings','<JD Power Ratings>','<HasGoodJdPowerRatings>',3,100150)

select @blurbId = scope_identity()

insert into templates.BlurbSampleText (blurbPreText, blurbPostText, blurbId, businessUnitID) VALUES ('','',@blurbId,100150)

insert into templates.MerchandisingPoints (businessUnitId, ruleType, ruleParameter,pointTypeId,templateDisplayText,dataAccessString) VALUES (100150,4,32,1,'JD Power Ratings','$base.GoodJdPowerRatings$')

update templates.AdTemplateItems set [sequence] = [sequence]+1 
where
templateId = 10
AND sequence >= 6 --anything above 5 (carfax one owner)

insert into templates.AdTemplateItems (templateID, blurbId, sequence, priority, required, parentID) VALUES (10,@blurbId,5,6,0,NULL) 



---Now, create the jdpower