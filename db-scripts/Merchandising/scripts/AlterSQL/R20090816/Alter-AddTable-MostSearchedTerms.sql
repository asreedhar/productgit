if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[MostSearchedEquipment]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[MostSearchedEquipment]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [settings].[MostSearchedEquipment](
	[id] [int] IDENTITY(1,1),
	[categoryId] [int] NOT NULL,		
	[mostSearchedRanking] [int] NOT NULL,
	[searchKeywords] varchar(100) NOT NULL
	CONSTRAINT [PK_MostSearchedEquipment] PRIMARY KEY  (
		categoryId
	)
)
GO


INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1078, 1, 'Leather Seats,Leather Interior,Leather')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1066, 2, 'NAV,Navigation,Nav System')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1144, 3, 'Sunroof,Moonroof')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1069, 3, 'Sunroof,Moonroof')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1132, 3, 'Sunroof,Moonroof')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1067, 3, 'Sunroof,Moonroof')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1068, 3, 'Sunroof,Moonroof')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1073, 4, '3rd Row Seat,Third Row Seat')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1156, 5, 'Seat Heaters,Heated Seats')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1157, 5, 'Seat Heaters,Heated Seats')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1018, 6, 'ABS Brakes,ABS')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1019, 6, 'ABS Brakes,ABS')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1131, 7, 'Manual Transmission,Manual,Stick')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1215, 8, 'DVD,DVD Entertainment System,Entertainment System')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1074, 9, 'Power Seats')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1075, 9, 'Power Seats')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1199, 10, 'Tow Hitch,Trailer Hitch,Hitch')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1216, 11, 'Bed Liner')
--INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (, 12, 'long bed')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1123, 13, 'Alloy Wheels,Aluminum Wheels')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1124, 13, 'Alloy Wheels,Chrome Wheels')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1141, 14, 'Quad Seats,Captains Chairs,Quad Bucket Seats')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1054, 15, 'Turbo,Turbo Charged,Turbo Charged Engine')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1007, 16, 'Overhead Airbag,Head Airbag')
INSERT INTO settings.MostSearchedEquipment (categoryId, mostSearchedRanking, searchKeywords) VALUES (1008, 16, 'Overhead Airbag,Head Airbag')

