if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[DealerContact]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[DealerContact]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [settings].[DealerContact](
	[businessUnitId] [int],
	[leadEmailAddress] [varchar](100),		
	[leadPhoneNumber] [varchar](20),
	[leadManagerFirstName] [varchar](50),
	[leadManagerLastName] [varchar](50),
	
	
	CONSTRAINT [PK_DealerContact] PRIMARY KEY  (
		businessUnitId
	)
)
GO
