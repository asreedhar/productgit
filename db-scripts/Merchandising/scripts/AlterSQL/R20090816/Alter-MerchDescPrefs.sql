ALTER TABLE Settings.MerchandisingDescriptionPreferences
ADD useLongDescriptionAsDefault bit NOT NULL default(0)
GO

ALTER TABLE Settings.MerchandisingDescriptionPreferences
ADD desiredPreviewLength int NOT NULL default(150)
GO
