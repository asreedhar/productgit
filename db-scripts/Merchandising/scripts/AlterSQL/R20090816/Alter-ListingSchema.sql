ALTER TABLE Listing.Vehicle_Interface
ADD ChromeStyleId int NULL
GO

ALTER TABLE Listing.Vehicle_Interface
ADD OptionCodes varchar(300) NULL
GO

ALTER TABLE Listing.Vehicle
ADD ChromeStyleId int NULL
GO

ALTER TABLE Listing.Vehicle
ADD OptionCodes varchar(300) NULL
GO

ALTER TABLE Listing.Vehicle_History
ADD ChromeStyleId int NULL
GO

ALTER TABLE Listing.Vehicle_History
ADD OptionCodes varchar(300) NULL
GO
