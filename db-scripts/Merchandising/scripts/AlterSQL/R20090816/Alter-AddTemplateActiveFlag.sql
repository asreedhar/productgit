ALTER TABLE templates.AdTemplates
ADD active bit NOT NULL DEFAULT((1))
GO
UPDATE templates.AdTemplates
SET active = 1
GO