set nocount on

declare @tables table
(
	id int identity(0,1) not null,
	schemaName sysname,
	tableName sysname
)

--select 
--	'union all select schemaName = ''' + TABLE_SCHEMA + ''', tableName = ''' + TABLE_NAME + ''''
--from INFORMATION_SCHEMA.tables where TABLE_SCHEMA = 'merchandising' and TABLE_NAME like 'Cars%'

insert @tables
(
	schemaName,
	tableName
)
select schemaName = 'merchandising', tableName = 'AutoTraderExSummary'
union all select schemaName = 'merchandising', tableName = 'AutoTraderPerfSummary'
union all select schemaName = 'merchandising', tableName = 'AutoTraderVehOnline'
union all select schemaName = 'merchandising', tableName = 'AutoTraderVehSummary'
union all select schemaName = 'merchandising', tableName = 'CarsDotComExSummary'
union all select schemaName = 'merchandising', tableName = 'CarsDotComPerfSummary'
union all select schemaName = 'merchandising', tableName = 'CarsDotComVehOnline'
union all select schemaName = 'merchandising', tableName = 'CarsDotComVehSummary'
union all select schemaName = 'postings', tableName = 'CarsDotComPhoneLeads'
union all select schemaName = 'postings', tableName = 'PhoneLeads'



declare
	@id int,
	@cmd varchar(max)


while (1=1)
begin
	select top 1
		@id = id,
		@cmd = 
			replace(
				replace
				(
					'
					if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = ''$s'' and TABLE_NAME = ''$t'' and COLUMN_NAME = ''FetchDateInserted'')
						alter table $s.$t add FetchDateInserted datetime null
					'
					, '$s'
					, schemaName
				)
				, '$t'
				, tableName
			)
	from @tables
	
	if (@@rowcount <> 1) break
	
	delete @tables where id = @id
	
	exec(@cmd)
end

