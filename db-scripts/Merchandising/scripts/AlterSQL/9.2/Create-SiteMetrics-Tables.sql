/*

drop table dashboard.SiteMetricsByStoreDateInventory
drop table dashboard.SiteMetricsByStoreInventory
drop table dashboard.SiteMetricsByStoreDate
drop table dashboard.SiteMetricsByStore

*/

create table dashboard.SiteMetricsByStoreDateInventory
(
	id int identity (1,1) not null
	, BusinessUnitId int not null
	, InventoryId int not null
	, destinationID int not null
	, Date datetime not null

	-- metrics
	, SearchPageViews int not null
	, DetailPageViews int not null
	, EmailLeads int not null
	, MapsViewed int not null
	, AdPrints int not null

)
go

alter table dashboard.SiteMetricsByStoreDateInventory add constraint PK_SiteMetricsByStoreDateInventory
	primary key clustered (BusinessUnitId, InventoryId, destinationID, Date)
go

alter table dashboard.SiteMetricsByStoreDateInventory add constraint FK_SiteMetricsByStoreDateInventory_EdtDestinations
	foreign key (destinationID) references settings.EdtDestinations (destinationID)
go	




create table dashboard.SiteMetricsByStoreInventory
(
	id int identity (1,1) not null
	, BusinessUnitId int not null
	, InventoryId int not null
	, destinationID int not null

	-- metrics
	, SearchPageViews int not null
	, DetailPageViews int not null
	, EmailLeads int not null
	, MapsViewed int not null
	, AdPrints int not null
)
go

alter table dashboard.SiteMetricsByStoreInventory add constraint PK_SiteMetricsByStoreInventory
	primary key clustered (BusinessUnitId, InventoryId, destinationID)
go

alter table dashboard.SiteMetricsByStoreInventory add constraint FK_SiteMetricsByStoreInventory_EdtDestinations
	foreign key (destinationID) references settings.EdtDestinations (destinationID)
go	


create table dashboard.SiteMetricsByStoreDate
(
	id int identity (1,1) not null
	, BusinessUnitId int not null
	, InventoryType tinyint not null
	, destinationID int not null
	, Date datetime not null
	-- metrics

	, SearchPageViews int not null
	, DetailPageViews int not null
	, EmailLeads int not null
	, MapsViewed int not null
	, AdPrints int not null
)
go

alter table dashboard.SiteMetricsByStoreDate add constraint PK_SiteMetricsByStoreDate
	primary key clustered (BusinessUnitId, InventoryType, destinationID, Date)
go

alter table dashboard.SiteMetricsByStoreDate add constraint FK_SiteMetricsByStoreDate_EdtDestinations
	foreign key (destinationID) references settings.EdtDestinations (destinationID)
go






create table dashboard.SiteMetricsByStore
(
	id int identity (1,1) not null
	, BusinessUnitId int not null
	, InventoryType tinyint not null
	, destinationID int not null	

	-- metrics
	, SearchPageViews int not null
	, DetailPageViews int not null
	, EmailLeads int not null
	, MapsViewed int not null
	, AdPrints int not null
	
)
go

alter table dashboard.SiteMetricsByStore add constraint PK_SiteMetricsByStore
	primary key clustered (BusinessUnitId, InventoryType, destinationID)
go

alter table dashboard.SiteMetricsByStore add constraint FK_SiteMetricsByStore_EdtDestinations
	foreign key (destinationID) references settings.EdtDestinations (destinationID)
go