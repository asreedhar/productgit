

-- AutoTraderExSummary
	if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'merchandising' and TABLE_NAME = 'AutoTraderExSummary' and COLUMN_NAME = 'Load_ID')
		alter table merchandising.AutoTraderExSummary add Load_ID int not null constraint DF_AutoTraderExSummary_LoadID default (-1)
	go

	if object_id('merchandising.DF_AutoTraderExSummary_LoadID') is not null
		alter table merchandising.AutoTraderExSummary drop constraint DF_AutoTraderExSummary_LoadID
	go

	if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'merchandising' and TABLE_NAME = 'AutoTraderExSummary' and COLUMN_NAME = 'DateLoaded')
	begin
		
		alter table merchandising.AutoTraderExSummary add DateLoaded datetime not null constraint DF_AutoTraderExSummary_DateLoaded default (getdate())
		
		-- must be in exec since the column didn't exist yet in this query batch:
		exec('
		update tbl set 
			DateLoaded = r.DateCreated
		from
			merchandising.AutoTraderExSummary tbl
			join STAGINGLINK.Datafeeds.Extract.FetchDotCom#Request r
				on tbl.RequestId = r.RequestId
		')
	end
	go

	if not exists(select * from sys.indexes where object_id = object_id('merchandising.AutoTraderExSummary') and name = 'IX_LoadID')
	begin
		create nonclustered index IX_LoadID on merchandising.AutoTraderExSummary (Load_ID)
	end
	go
	
	
	
	
	
-- AutoTraderPerfSummary

	if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'merchandising' and TABLE_NAME = 'AutoTraderPerfSummary' and COLUMN_NAME = 'Load_ID')
		alter table merchandising.AutoTraderPerfSummary add Load_ID int not null constraint DF_AutoTraderPerfSummary_LoadID default (-1)
	go

	if object_id('merchandising.DF_AutoTraderPerfSummary_LoadID') is not null
		alter table merchandising.AutoTraderPerfSummary drop constraint DF_AutoTraderPerfSummary_LoadID
	go

	if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'merchandising' and TABLE_NAME = 'AutoTraderPerfSummary' and COLUMN_NAME = 'DateLoaded')
	begin
		
		alter table merchandising.AutoTraderPerfSummary add DateLoaded datetime not null constraint DF_AutoTraderPerfSummary_DateLoaded default (getdate())
		
		-- must be in exec since the column didn't exist yet in this query batch:
		exec('
		update tbl set 
			DateLoaded = r.DateCreated
		from
			merchandising.AutoTraderPerfSummary tbl
			join STAGINGLINK.Datafeeds.Extract.FetchDotCom#Request r
				on tbl.RequestId = r.RequestId
		')
	end
	go
	
	if not exists(select * from sys.indexes where object_id = object_id('merchandising.AutoTraderPerfSummary') and name = 'IX_LoadID')
	begin
		create nonclustered index IX_LoadID on merchandising.AutoTraderPerfSummary (Load_ID)
	end
	go
	
	
	
	
-- AutoTraderVehOnline

	if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'merchandising' and TABLE_NAME = 'AutoTraderVehOnline' and COLUMN_NAME = 'Load_ID')
		alter table merchandising.AutoTraderVehOnline add Load_ID int not null constraint DF_AutoTraderVehOnline_LoadID default (-1)
	go

	if object_id('merchandising.DF_AutoTraderVehOnline_LoadID') is not null
		alter table merchandising.AutoTraderVehOnline drop constraint DF_AutoTraderVehOnline_LoadID
	go

	if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'merchandising' and TABLE_NAME = 'AutoTraderVehOnline' and COLUMN_NAME = 'DateLoaded')
	begin
		
		alter table merchandising.AutoTraderVehOnline add DateLoaded datetime not null constraint DF_AutoTraderVehOnline_DateLoaded default (getdate())
		
		-- must be in exec since the column didn't exist yet in this query batch:
		exec('
		update tbl set 
			DateLoaded = r.DateCreated
		from
			merchandising.AutoTraderVehOnline tbl
			join STAGINGLINK.Datafeeds.Extract.FetchDotCom#Request r
				on tbl.RequestId = r.RequestId
		')
	end
	go

	if not exists(select * from sys.indexes where object_id = object_id('merchandising.AutoTraderVehOnline') and name = 'IX_LoadID')
	begin
		create nonclustered index IX_LoadID on merchandising.AutoTraderVehOnline (Load_ID)
	end
	go
	
	
	
	
-- AutoTraderVehSummary

	if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'merchandising' and TABLE_NAME = 'AutoTraderVehSummary' and COLUMN_NAME = 'Load_ID')
		alter table merchandising.AutoTraderVehSummary add Load_ID int not null constraint DF_AutoTraderVehSummary_LoadID default (-1)
	go

	if object_id('merchandising.DF_AutoTraderVehSummary_LoadID') is not null
		alter table merchandising.AutoTraderVehSummary drop constraint DF_AutoTraderVehSummary_LoadID
	go

	if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'merchandising' and TABLE_NAME = 'AutoTraderVehSummary' and COLUMN_NAME = 'DateLoaded')
	begin
		
		alter table merchandising.AutoTraderVehSummary add DateLoaded datetime not null constraint DF_AutoTraderVehSummary_DateLoaded default (getdate())
		
		-- must be in exec since the column didn't exist yet in this query batch:
		exec('
		update tbl set 
			DateLoaded = r.DateCreated
		from
			merchandising.AutoTraderVehSummary tbl
			join STAGINGLINK.Datafeeds.Extract.FetchDotCom#Request r
				on tbl.RequestId = r.RequestId
		')
	end
	go
	
	if not exists(select * from sys.indexes where object_id = object_id('merchandising.AutoTraderVehSummary') and name = 'IX_LoadID')
	begin
		create nonclustered index IX_LoadID on merchandising.AutoTraderVehSummary (Load_ID)
	end
	go
	
	
	
	
-- PhoneLeads

	if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'postings' and TABLE_NAME = 'PhoneLeads' and COLUMN_NAME = 'Load_ID')
		alter table postings.PhoneLeads add Load_ID int not null constraint DF_PhoneLeads_LoadID default (-1)
	go

	if object_id('postings.DF_PhoneLeads_LoadID') is not null
		alter table postings.PhoneLeads drop constraint DF_PhoneLeads_LoadID
	go

	if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'postings' and TABLE_NAME = 'PhoneLeads' and COLUMN_NAME = 'DateLoaded')
	begin
		
		alter table postings.PhoneLeads add DateLoaded datetime not null constraint DF_PhoneLeads_DateLoaded default (getdate())
		
		-- must be in exec since the column didn't exist yet in this query batch:
		exec('
		update tbl set 
			DateLoaded = r.DateCreated
		from
			postings.PhoneLeads tbl
			join STAGINGLINK.Datafeeds.Extract.FetchDotCom#Request r
				on tbl.RequestId = r.RequestId
		')
	end
	go
			
	if not exists(select * from sys.indexes where object_id = object_id('postings.PhoneLeads') and name = 'IX_LoadID')
	begin
		create nonclustered index IX_LoadID on postings.PhoneLeads (Load_ID)
	end
	go