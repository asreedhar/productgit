-- NOTE: This script sets the 'Ad Review' state to 'Complete' for all inventory that is online
-- BUGZID 18963

-- Build list of inventory already online
declare @vehiclesWithOnlineAds table (BusinessUnitID int, InventoryID int)

insert into @vehiclesWithOnlineAds
select IA.BusinessUnitID, IA.InventoryID
from FLDW.dbo.InventoryActive IA
    inner join Merchandising.Builder.OptionsConfiguration oc
    on IA.BusinessUnitID = oc.BusinessUnitID and IA.InventoryID = oc.InventoryID
    left join Merchandising.Builder.VehicleStatus vs
    on IA.BusinessUnitID = vs.BusinessUnitID and IA.InventoryID = vs.InventoryID and vs.StatusTypeID = 5 -- Posting
where IA.InventoryActive = 1
    and oc.DoNotPostFlag = 0
    and coalesce(vs.StatusLevel, 0) <> 2 -- PendingApproval
    and (oc.onlineFlag = 1 
        or coalesce(vs.StatusLevel, 0) >= 3) -- CurrentlyReleasing (3) or NoActionNeeded (4)
        
-- Upsert into VehicleStatus
update Merchandising.builder.VehicleStatus
    set statusLevel = 2, -- Complete
        lastUpdatedOn = getdate(), lastUpdatedBy = 'Set-AdReviewComplete-Status'
    from Merchandising.builder.VehicleStatus vs
        inner join @vehiclesWithOnlineAds o
        on vs.BusinessUnitID = o.BusinessUnitID
            and vs.InventoryID = o.InventoryID
    where vs.StatusTypeID = 6 -- Ad Review
        and vs.statusLevel <> 2 -- Not Complete
        
insert into Merchandising.builder.VehicleStatus 
    (businessUnitID, inventoryID, statusLevel, statusTypeId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy)
    select o.BusinessUnitID, o.InventoryID, 
        2, -- Complete
        6, -- Ad Review
        getdate(), 'Set-AdReviewComplete-Status', getdate(), 'Set-AdReviewComplete-Status'
    from @vehiclesWithOnlineAds o
    where not exists (select 1 
                      from Merchandising.builder.VehicleStatus vs 
                      where o.BusinessUnitId = vs.BusinessUnitId
                        and o.InventoryId = vs.InventoryId
                        and vs.StatusTypeId = 6) -- Ad Review