set nocount on

/*
	WARNING! this script must delete VehicleProfiles, AdTemplates, and AdTemplate items to work correctly.  That means
	if a dealer has made a custom edit to a framework/blurb, the changes will be lost.
	
	
	WARNING WARNING WARNING!!!!!!  this script will destroy data if the business unit from which you want to clone 
	also exists in the list of businesses to clone TO!!!!!!!
	
	Check the templateId list, make sure it only comes from one distinct business unit, and then make sure that business unit
	DOES NOT EXIST in the CLONE TO list!!!!!!!
	
	
	
	!!!!!!!
	
	
	
	
	
	this script will clone templates, but it must be edited to properly populate the
	
		@stores table variable with a list of stores to clone to
		@templates table with a list of pre-existing templateids to copy
		
	this script is smart enough to not duplicate templates, in other words, if templateID 10 is in the @templates list,
	and it already exists for businessUnit 165900 in adTemplates as evidenced by having a record in there for that businessUnitID
	and that SourceTemplateId (10), it will still function properly.
	
	in other words, this script should always clone the @templates to the other @stores, even if they already exist.
	
	once the templates are created, it will also create the associated AdTemplateItems (by deleteing any that already exist and then
	inserting the ones associated with the original @templates list)
	
	this script also deep copies the VehicleProfile records.
	
*/

declare @stores table (BusinessUnitID int not null)
declare @templates table (templateID int not null)


insert @stores (BusinessUnitID)
select distinct BusinessUnitID
from IMT.dbo.BusinessUnit
where
	BusinessUnitTypeID = 4
	and	BusinessUnitCode in (
		  'ADAMSPBG02'
		, 'AIRPORTC01'
		, 'ALLSTARC01'
		, 'ALLSTARC02'
		, 'ALMAPONT01'
		, 'AMERICAN02'
		, 'ANCIRAGM01'
		, 'ANCIRAVO01'
		, 'ANCIRAWI01'
		, 'ANDERSON08'
		, 'ANTHEMCH02'
		, 'APPLEVAL01'
		, 'ARNIEBAU02'
		, 'ATWOODCH02'
		, 'ATZENHOF02'
		, 'AUFFENBE01'
		, 'AXELRODB01'
		, 'BEARDMOR02'
		, 'BELLHOWA01'
		, 'BENMYNAT02'
		, 'BENMYNAT04'
		, 'BERGERCH01'
		, 'BERTERAC01'
		, 'BILLESTE04'
		, 'BILLKAYC03'
		, 'BILLPIER03'
		, 'BMWOFSIL01'
		, 'BOBKINGB01'
		, 'BOTHUNJK01'
		, 'BRALEYAN01'
		, 'BROOKDAL02'
		, 'BROWNAND02'
		, 'BURTNESS01'
		, 'BURTNESS03'
		, 'BURTWATS02'
		, 'CADILLAC02'
		, 'CADILLAC03'
		, 'CAPITOLC02'
		, 'CARGILLC02'
		, 'CARLCANN02'
		, 'CARLHOGA02'
		, 'CHAMPION10'
		, 'CHANDLER02'
		, 'CHEVROLE04'
		, 'CLASSICC03'
		, 'CLINKSCA02'
		, 'COASTALC02'
		, 'COASTBUI02'
		, 'COLEKRUM01'
		, 'COLLEGEC02'
		, 'COLONIAL03'
		, 'COLONIAL10'
		, 'COLUMBIA06'
		, 'COMMUNIT04'
		, 'CONNELLC02'
		, 'COYLECHE02'
		, 'CRITZINC01'
		, 'CROWNCAD01'
		, 'DAHLSTRO02'
		, 'DANIELSC03'
		, 'DAVENPOR03'
		, 'DAVISCHE01'
		, 'DELAFUEN01'
		, 'DEVOECAD01'
		, 'DEVOEPB-01'
		, 'DICKGENT03'
		, 'DONTHORN02'
		, 'DUNNPONT02'
		, 'DUPLESSI05'
		, 'EDSHULTS01'
		, 'ESTEROBA02'
		, 'ETTLESON02'
		, 'EVERETTE02'
		, 'FARNSWOR02'
		, 'FAULKNER08'
		, 'FAULKNER16'
		, 'FAULKNER20'
		, 'FAULKNER26'
		, 'FAULKNER28'
		, 'FERRISCH01'
		, 'FIOREBUI02'
		, 'FISHERCH03'
		, 'FOXNEGAU01'
		, 'GALLESCH02'
		, 'GANLEYCH02'
		, 'GATEWAYC01'
		, 'GENTILIN02'
		, 'GEORGEWE02'
		, 'GERRYLAN01'
		, 'GINMOTOR02'
		, 'GOLLINGP02'
		, 'GORDONCH02'
		, 'GRANDALL01'
		, 'GRANDBUI03'
		, 'GREENBRI01'
		, 'GREGGYOU02'
		, 'GUARANTY02'
		, 'GWINNETT01'
		, 'HARECHEV01'
		, 'HARRELDC01'
		, 'HARRYBRO02'
		, 'HASELWOO02'
		, 'HENDRICK01'
		, 'HENDRICK04'
		, 'HENDRICK05'
		, 'HENDRICK07'
		, 'HENDRICK17'
		, 'HERBCHAM18'
		, 'HERITAGE07'
		, 'HEYWARDA03'
		, 'HIDDENVA02'
		, 'HILTONHE02'
		, 'HOLMAUTO02'
		, 'HOLMESCH02'
		, 'HOLZMOTO04'
		, 'HOODNORT02'
		, 'HOOVERBU01'
		, 'HUFFINES04'
		, 'HUGCHEVR02'
		, 'IMPERIAL02'
		, 'JEFFGORD01'
		, 'JEFFREYA02'
		, 'JERRYBIG02'
		, 'JIMHARDM02'
		, 'JIMPRICE02'
		, 'JOHNHIES02'
		, 'JOHNHIRS01'
		, 'JOHNMEGE02'
		, 'JOHNVANC04'
		, 'JPCHEVRO03'
		, 'KEARNYME02'
		, 'KENNYROS03'
		, 'KENNYROS06'
		, 'KENNYROS07'
		, 'KINGDOMC02'
		, 'KOOLCHEV01'
		, 'KRIEGERM02'
		, 'LAFONTAI02'
		, 'LAKEBUIC01'
		, 'LAKECHEV01'
		, 'LANDERSC03'
		, 'LEVALLEY04'
		, 'LIBERTYB02'
		, 'LIBERTYP01'
		, 'LIBERTYV03'
		, 'LIGHTHOU02'
		, 'LONGOFCH01'
		, 'LUTHERFA01'
		, 'LUTHERHU02'
		, 'LYNCHCHE02'
		, 'MAINMOTO02'
		, 'MCCARTHY01'
		, 'MCCLINTO02'
		, 'MIKERAIS03'
		, 'MIKEREED02'
		, 'MILLERST02'
		, 'MILLSCHE02'
		, 'MITCHELL02'
		, 'MONTROSE07'
		, 'MOORECAD03'
		, 'MTNVIEWC01'
		, 'NASHCHEV01'
		, 'NEWROGER01'
		, 'NIMNICHT02'
		, 'NIMNICHT03'
		, 'NORTHSTA02'
		, 'NORTHSTA03'
		, 'OBAUGHCH01'
		, 'OBAUGHPO01'
		, 'PADDOCKC02'
		, 'PARADISE02'
		, 'PARKWAYO02'
		, 'PENSKECH01'
		, 'PERFORMA13'
		, 'PHILLIPS02'
		, 'PINEBELT10'
		, 'PINEBELT11'
		, 'POINTEPO02'
		, 'RANDYHOS02'
		, 'RAYDENNI02'
		, 'RAYHUFFI01'
		, 'REDWINGC01'
		, 'RICKHEND03'
		, 'RICKHEND04'
		, 'RICKHEND07'
		, 'RICKMATT02'
		, 'RIOVISTA02'
		, 'ROBERTLE02'
		, 'ROCKENBA03'
		, 'ROMEROMO01'
		, 'ROSEDALE01'
		, 'ROYALCHE04'
		, 'ROYALOAK02'
		, 'SANTAPAU02'
		, 'SAPAUGHM02'
		, 'SCHEPELC01'
		, 'SEACOAST02'
		, 'SHAKOPEE02'
		, 'SIMSBUIC01'
		, 'STANLEYC02'
		, 'STANLEYC04'
		, 'STANLEYC05'
		, 'STANLEYC07'
		, 'STEWARTC02'
		, 'STUARTCO01'
		, 'SUBURBAN05'
		, 'SUDBAYCH01'
		, 'SUPERIOR03'
		, 'SUPERIOR08'
		, 'SUPERIOR18'
		, 'SWEENEYB02'
		, 'TEAMONEA02'
		, 'TERRYLAB01'
		, 'TIMONEIL02'
		, 'TINNEYAU02'
		, 'TOMBELLC01'
		, 'TOMGIBBS02'
		, 'TOWNE-CA01'
		, 'TULLEYBP01'
		, 'TUSTINCA02'
		, 'VELDECAD02'
		, 'VERMILLI01'
		, 'VOSSCHEV02'
		, 'VOSSVILL02'
		, 'WADERAUL02'
		, 'WARSAWBU02'
		, 'WATSONCH02'
		, 'WENTWORT02'
		, 'WIGDERCH02'
		, 'WILKINSB01'
		, 'WILSONCO02'
		, 'WILSONVI01'
		, 'WOODRUFF01'
		, 'WORDENMA04'
		, 'ZIMBRICK03'
	)

insert @templates (templateID)
select templateID from Merchandising.templates.AdTemplates where templateID in (8458,8459,8506,8507,8462,8463,8464,8465,8382,8401,8402,8403,8404,8456,8457,8466,8496,8497,8701,8702,8405,8406,8407,8460,8461,8498,8499)


--- do not edit script below this line, only edit the stores and templates list above

set xact_abort on
set transaction isolation level serializable
begin transaction

declare
	@rows int,
	@businessUnitId int,
	@sourceTemplateId int,
	@vehicleProfileId int


declare @storeProfiles table
(
	BusinessUnitID int not null,
	sourceTemplateId int not null,
	vehicleProfileId int null
)

insert @storeProfiles
(
	BusinessUnitID,
	sourceTemplateId
)
select
	s.BusinessUnitID,
	t.templateID
from
	@stores s
	cross join @templates t


declare @vpToDelete table
(
	vehicleProfileID int
)

-- delete all VehicleProfile, AdTemplate, and AdTemplateItems for the new stores 
-- that are associated with the "master" templates we are about to clone.  these
-- records might not exist, but if they do this script will not correctly map the
-- deltas that may occur if a store only has a partial framework frmo the masters.

insert @vpToDelete
(
	vehicleProfileID
)
select
	vp.vehicleProfileID
from
	Merchandising.templates.VehicleProfiles vp
	join Merchandising.templates.AdTemplates at
		on vp.vehicleProfileID = at.vehicleProfileId
	join @storeProfiles sp
		on at.SourceTemplateId = sp.sourceTemplateId
		and at.businessUnitID = sp.BusinessUnitID
		and vp.businessUnitId = sp.BusinessUnitID
	
set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' pre-exising VehicleProfiles staged for delete.'

delete ati
from
	Merchandising.templates.AdTemplateItems ati
	join Merchandising.templates.AdTemplates at
		on ati.templateID = at.templateID
	join @storeProfiles sp
		on at.businessUnitID = sp.BusinessUnitID
		and at.SourceTemplateId = sp.sourceTemplateId

set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' pre-exising AdTemplateItems deleted.'

delete at
from
	Merchandising.templates.AdTemplates at
	join @storeProfiles sp
		on at.SourceTemplateId = sp.sourceTemplateId
		and at.businessUnitID = sp.BusinessUnitID

set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' pre-exising AdTemplates deleted.'

delete vp
from
	Merchandising.templates.VehicleProfiles vp
	join @vpToDelete d
		on vp.vehicleProfileID = d.vehicleProfileID

set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' pre-exising VehicleProfiles deleted.'
		

while (1=1)
begin
	select top 1
		@businessUnitId = BusinessUnitID,
		@sourceTemplateId = sourceTemplateId
	from @storeProfiles
	where vehicleProfileId is null
	
	if (@@rowcount <> 1) break;
	
	insert Merchandising.templates.VehicleProfiles
	( 
		businessUnitId,
		title,
		makeIDList,
		modelList,
		marketClassIdList,
		startYear,
		endYear,
		minAge,
		maxAge,
		minPrice,
		maxPrice,
		certifiedStatusFilter,
		trimList,
		segmentIdList,
		vehicleType
	)
	select
		@businessUnitID,
		vp.title,
		vp.makeIDList,
		vp.modelList,
		vp.marketClassIdList,
		vp.startYear,
		vp.endYear,
		vp.minAge,
		vp.maxAge,
		vp.minPrice,
		vp.maxPrice,
		vp.certifiedStatusFilter,
		vp.trimList,
		vp.segmentIdList,
		vp.vehicleType	
	from
		Merchandising.templates.AdTemplates at
		join Merchandising.templates.VehicleProfiles vp
			on at.vehicleProfileId = vp.vehicleProfileID
	where at.templateID = @sourceTemplateId
	
	select @rows = @@rowcount, @vehicleProfileId = scope_identity()
	
	if (@rows <> 1) raiserror('incorrect rows inserted into vehicle profiles!', 16, 1)
	
	update @storeProfiles set
		vehicleProfileId = @vehicleProfileId
	where
		BusinessUnitID = @businessUnitId
		and sourceTemplateId = @sourceTemplateId
	
	set @rows = @@rowcount
	if (@rows <> 1) raiserror('incorrect rows updated in local table!', 16, 1)
	
end	-- VehicleProfile insert loop


print ''
print ''

select @rows = count(*) from @storeProfiles
print convert(varchar(10), @rows) + ' new VehicleProfiles added.'



-- insert the new templates
insert Merchandising.templates.AdTemplates
(
	name,
	businessUnitID,
	vehicleProfileId,
	parent,
	active,
	SourceTemplateId
)
select
	at.name,
	sp.BusinessUnitID,
	sp.vehicleProfileId,
	at.parent,
	at.active,
	SourceTemplateId = at.templateID
from
	Merchandising.templates.AdTemplates at
	join @storeProfiles sp
		on at.templateID = sp.sourceTemplateId
	
set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' new templates added.'
	
	
	
	
	

-- ok go clone the AdTemplateItems tied to @templates list to the new @stores list
insert Merchandising.templates.AdTemplateItems
(
	templateID,
	blurbID,
	sequence,
	priority,
	required,
	parentID
)
select
	newTemplates.templateID,
	ati.blurbID,
	ati.sequence,
	ati.priority,
	ati.required,
	ati.parentID
from
	Merchandising.templates.AdTemplateItems ati
	join Merchandising.templates.AdTemplates at
		on ati.templateID = at.templateID
	join @templates t
		on at.templateID = t.templateID
	join Merchandising.templates.AdTemplates newTemplates
		on newTemplates.SourceTemplateId = at.templateID
	join @stores s
		on newTemplates.businessUnitID = s.BusinessUnitID


set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' new template items added.'

commit
set xact_abort off
set transaction isolation level read uncommitted

