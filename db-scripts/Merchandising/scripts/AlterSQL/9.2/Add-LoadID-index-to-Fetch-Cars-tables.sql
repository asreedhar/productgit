
-- CarsDotComExSummary
	if not exists(select * from sys.indexes where object_id = object_id('merchandising.CarsDotComExSummary') and name = 'IX_LoadID')
	begin
		create nonclustered index IX_LoadID on merchandising.CarsDotComExSummary (Load_ID)
	end
	go
	

-- CarsDotComPerfSummary
	if not exists(select * from sys.indexes where object_id = object_id('merchandising.CarsDotComPerfSummary') and name = 'IX_LoadID')
	begin
		create nonclustered index IX_LoadID on merchandising.CarsDotComPerfSummary (Load_ID)
	end
	go
	

-- CarsDotComPhoneLeads
	if not exists(select * from sys.indexes where object_id = object_id('postings.CarsDotComPhoneLeads') and name = 'IX_LoadID')
	begin
		create nonclustered index IX_LoadID on postings.CarsDotComPhoneLeads (Load_ID)
	end
	go
	

-- CarsDotComVehOnline
	if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'merchandising' and TABLE_NAME = 'CarsDotComVehOnline' and COLUMN_NAME = 'Load_ID')
		alter table merchandising.CarsDotComVehOnline add Load_ID int not null constraint DF_CarsDotComVehOnline_LoadID default (-1)
	go

	if object_id('merchandising.DF_CarsDotComVehOnline_LoadID') is not null
		alter table merchandising.CarsDotComVehOnline drop constraint DF_CarsDotComVehOnline_LoadID
	go
	
	if not exists(select * from sys.indexes where object_id = object_id('merchandising.CarsDotComVehOnline') and name = 'IX_LoadID')
	begin
		create nonclustered index IX_LoadID on merchandising.CarsDotComVehOnline (Load_ID)
	end
	go
	

-- CarsDotComVehSummary
	if not exists(select * from sys.indexes where object_id = object_id('merchandising.CarsDotComVehSummary') and name = 'IX_LoadID')
	begin
		create nonclustered index IX_LoadID on merchandising.CarsDotComVehSummary (Load_ID)
	end
	go
	
