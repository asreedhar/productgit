USE [Merchandising]
GO
-- =============================================
-- Author:		Kevin Boucher
-- Create date: 11/15/2011
-- Description: settings.PreApprove
-- Creates table to store MAX AD Pre-Approve With Review Settings
-- =============================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[PreApprove] (
	[businessUnitID] int NOT NULL,
	[FirstName] VARCHAR(32) NOT NULL,
	[LastName] VARCHAR(32) NOT NULL,
	[Email] VARCHAR(128) NOT NULL,
	[Status] int NOT NULL,
	[CreatedOn] datetime NOT NULL,
	[CreatedBy] VARCHAR(32) NOT NULL,
	[UpdatedOn] datetime NULL,
	[UpdatedBy] VARCHAR(32) NULL

	CONSTRAINT [PK_PreApprove] PRIMARY KEY CLUSTERED
	(
		[businessUnitID] ASC
 	)	WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

) ON [PRIMARY]

GO
SET ANSI_PADDING OFF