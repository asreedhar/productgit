if object_id('Merchandising.Merchandising.AutoTraderHendrickVehicleSummary') is null
begin
	-- drop table Merchandising.Merchandising.AutoTraderHendrickVehicleSummary

	create table Merchandising.Merchandising.AutoTraderHendrickVehicleSummary
	(
		-- pk
		VIN varchar(17) not null,
		StockNumber varchar(15) not null,
		FeedDate datetime not null,

		-- stats
		SearchPageViews int,
		DetailPageViews int,
		EmailLeads int,
		MapsViewed int,
		AdPrints int,

		-- audit
		LoadId int not null,
		
		-- extra stats
		PhoneRequests int,
		WebsiteClicks int,
		ChatRequests int,
		SpotlightAds int,
		Price int,
		ImageCount smallint,
		SellerNotes char(1),
		DaysLive smallint
	)

end

if object_id('Merchandising.Merchandising.PK_AutoTraderHendrickVehicleSummary') is null
	alter table Merchandising.Merchandising.AutoTraderHendrickVehicleSummary add constraint PK_AutoTraderHendrickVehicleSummary primary key clustered (VIN, StockNumber, FeedDate)

