-- drop table Merchandising.merchandising.AutoTraderHendrickPerformanceSummary


if object_id('Merchandising.merchandising.AutoTraderHendrickPerformanceSummary') is null
begin

	create table Merchandising.merchandising.AutoTraderHendrickPerformanceSummary
	(
		BusinessUnitId int not null,
		ReportMonth datetime not null,
		FeedDate datetime not null,

		NewSearchPageViews int,
		UsedSearchPageViews int,

		NewDetailPageViews int,
		UsedDetailPageViews int,

		NewEmailLeads int,
		UsedEmailLeads int,

		NewMapsViewed int,
		UsedMapsViewed int,

		NewAdPrints int,
		UsedAdPrints int,

		FydMaps int,
		FydEmails int,
		NewPhoneLeads int,
		UsedPhoneLeads int,
		NewClicksToDealerWebsite int,
		UsedClicksToDealerWebsite int,
		NewVideoPlays int,
		UsedVideoPlays int,
		TotalChatRequests int,
		TotalTradeInMarketProspects int,
		LoadId int
		
	)

	alter table Merchandising.merchandising.AutoTraderHendrickPerformanceSummary add constraint PK_AutoTraderHendrickPerformanceSummary primary key clustered (BusinessUnitId, ReportMonth, FeedDate)
end