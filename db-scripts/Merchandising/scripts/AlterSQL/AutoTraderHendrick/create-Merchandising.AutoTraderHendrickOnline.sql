-- drop table Merchandising.merchandising.AutoTraderHendrickOnline
if object_id('Merchandising.merchandising.AutoTraderHendrickOnline') is null
begin
	create table Merchandising.merchandising.AutoTraderHendrickOnline
	(
		Vin varchar(17) not null,
		StockNumber varchar(15) not null,
		FeedDate datetime not null,
		LoadId int not null,
		BusinessUnitId int,
		InventoryId int,
		InventoryType tinyint,
		HasPhoto bit,
		PhotoCount smallint,
		DaysLive smallint,
		Make varchar(30),
		Model varchar(50),
		Year smallint,
		Price int,
		Mileage int,
		ExpirationDate datetime,
		Source varchar(30),
	    
		constraint PK_AutoTraderHendrickOnline primary key clustered
		(
			Vin,
			StockNumber,
			FeedDate
		)
	)
	
	create nonclustered index IX_LoadId on Merchandising.merchandising.AutoTraderHendrickOnline (LoadId)
	
	create nonclustered index IX_BusinessUnitId_LoadId_Vin on Merchandising.merchandising.AutoTraderHendrickOnline (BusinessUnitId, LoadId, Vin)
end
