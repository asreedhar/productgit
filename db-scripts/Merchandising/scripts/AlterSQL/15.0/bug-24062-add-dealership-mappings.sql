--IVORYMAZ01, 106248, remote: 2172862
--KEARNYME02, 101439, remote: 165997


select *
from DBASTAT.dbo.DealershipIDConversion
where
	ManagementIdentifier = 'CarsDotCom'
	and BusinessUnitCode in ('IVORYMAZ01', 'KEARNYME02')
	


; with NewStores as
(
select
	ManagementIdentifier = 'CarsDotCom',
	DealershipIdentifier = '2172862',
	BusinessUnitCode = 'IVORYMAZ01',
	DataSourceId = 111,
	InsertDate = getdate(),
	InsertUser = 'FIRSTLOOK\dspeer',
	UpdateDate = getdate(),
	UpdateUser = 'FIRSTLOOK\dspeer'
	
union all	

select
	ManagementIdentifier = 'CarsDotCom',
	DealershipIdentifier = '165997',
	BusinessUnitCode = 'KEARNYME02',
	DataSourceId = 111,
	InsertDate = getdate(),
	InsertUser = 'FIRSTLOOK\dspeer',
	UpdateDate = getdate(),
	UpdateUser = 'FIRSTLOOK\dspeer'
)
insert DBASTAT.dbo.DealershipIDConversion
(
	ManagementIdentifier,
	DealershipIdentifier,
	BusinessUnitCode,
	DataSourceId,
	InsertDate,
	InsertUser,
	UpdateDate,
	UpdateUser
)
select
	ManagementIdentifier,
	DealershipIdentifier,
	BusinessUnitCode,
	DataSourceId,
	InsertDate,
	InsertUser,
	UpdateDate,
	UpdateUser
from NewStores ns
where
	not exists
	(
		select *
		from DBASTAT.dbo.DealershipIDConversion
		where
			ManagementIdentifier = ns.ManagementIdentifier
			and DealershipIdentifier = ns.DealershipIdentifier
	)
	