if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[CampaignEvents]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[CampaignEvents]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [settings].[CampaignEvents](
	[campaignEventId] [int] identity(1,1),
	[businessUnitId] [int],
	[vehicleAgeInDaysForAction] [int],
	[regenerateDescription] [bit],
	[templateId] [int] NULL,
	[themeId] [int] NULL,
	[repriceTypeId] [int],
	[repricePercentage] [int] NULL,	
	CONSTRAINT [PK_MerchandisingCampaignEvents] PRIMARY KEY (
		businessUnitId, vehicleAgeInDaysForAction
	)
)
GO

SET ANSI_PADDING OFF
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[CampaignRepriceType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[CampaignRepriceType]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [settings].[CampaignRepriceType](
	[repriceTypeId] [int] identity(1,1),
	[description] [varchar](50),
	CONSTRAINT [PK_CampaignRepriceType] PRIMARY KEY  (
		repriceTypeId		
	)
)
GO

INSERT INTO [settings].[CampaignRepriceType] (description) VALUES ('Percentage of Current Price')
INSERT INTO [settings].[CampaignRepriceType] (description) VALUES ('Percentage of Market Average')
GO

SET ANSI_PADDING OFF
GO
