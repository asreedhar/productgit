
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [merchandising].[SystemMessages](
	[messageId] [int] identity(1,1),
	[messageText] [varchar](2000) NOT NULL,	
	[active] [bit] NOT NULL,
	[expiresOn] [datetime] NOT NULL,
	CONSTRAINT [PK_SystemMessages] PRIMARY KEY  (
		messageId
	)
)
GO

SET ANSI_NULLS OFF
GO
SET ANSI_PADDING OFF
GO