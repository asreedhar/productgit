
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [builder].[Inventory_KeyInformation](
	[businessUnitId] [int],
	[inventoryId] [int],
	[infoId] [int],
	CONSTRAINT [PK_KeyInformation] PRIMARY KEY  (
		businessUnitId, 
		inventoryId, 
		infoId
	)
)
GO

SET ANSI_NULLS OFF
GO
SET ANSI_PADDING OFF
GO


--------------migrate the old poorly normalized data--------------

--THIS REQUIRES AN UPDATE TO THIS FUNCTION...
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[iter$simple_stringlist_to_tbl]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[iter$simple_stringlist_to_tbl]
GO
/****** Object:  UserDefinedFunction [dbo].[iter$simple_stringlist_to_tbl]    Script Date: 10/26/2008 13:11:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[iter$simple_stringlist_to_tbl] (@list nvarchar(MAX))
   RETURNS @tbl TABLE (itemName varchar(150) NOT NULL) AS
BEGIN
   DECLARE @pos        int,
           @nextpos    int,
           @valuelen   int

   SELECT @pos = 0, @nextpos = 1

   WHILE @nextpos > 0
   BEGIN
      SELECT @nextpos = charindex(',', @list, @pos + 1)
      SELECT @valuelen = CASE WHEN @nextpos > 0
                              THEN @nextpos
                              ELSE len(@list) + 1
                         END - @pos - 1
      INSERT @tbl (itemName)
         VALUES (substring(@list, @pos + 1, @valuelen))
      SELECT @pos = @nextpos
   END
  RETURN
END

GO	
--------------------

declare @invIds table(
	rowId int identity(1,1),
	inventoryId int,
	businessUnitId int,
	additionalInfoText varchar(max)
)
insert into @invIds (inventoryId, businessUnitId, additionalInfoText)
select inventoryId, businessUnitId, additionalInfoText
FROM builder.optionsConfiguration
where additionalInfoText <> ''

declare @ctr int, @totalCt int
select @ctr = 1, @totalCt = count(*)
from @invIds

declare @addlTxt varchar(max), @currId int, @currBuid int

while @ctr <= @totalCt
begin

	SELECT @addlTxt = additionalInfoText, @currId = inventoryId, @currBuid = businessUnitId
	FROM @invIds
	WHERE rowId = @ctr

	--insert rows for each item in the list that matches an infoAdDisplay
	INSERT INTO builder.Inventory_KeyInformation (businessUnitId, inventoryId, infoId)
	SELECT businessUnitId, @currId, aic.infoId
	from dbo.iter$simple_stringlist_to_tbl(@addlTxt) as phrases
	JOIN builder.additionalInfoCategories aic
	ON aic.businessUnitId = @currBuid
	AND rtrim(ltrim(aic.infoAdDisplay)) = rtrim(ltrim(phrases.itemName))
	AND rtrim(ltrim(aic.infoAdDisplay)) <> ''

	select @ctr = @ctr + 1
end