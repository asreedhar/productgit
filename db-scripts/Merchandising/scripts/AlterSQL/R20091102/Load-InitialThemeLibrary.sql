--1 = Family
--2'Green/Eco-Friendly'
--3'Student/Teen'
--4'Urban'
--5'Work/Utility'
--6'Recreation/Travelling'
--7'Sport'
--8'Value'
--9'Luxury'

insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'GREAT SAFETY FOR YOUR FAMILY: ', '', 9, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Keep the planet clean in an Earth friendly vehicle. ', '', 36, NULL, 1, 1, 2)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Features eco-friendly items: ', ' !!', 46, NULL, 1, 0, 2)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Save more for college by buying your [Model] from us! ', '', 45, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Take this opportunity to pick up a great deal for your family! ', '', 45, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Get your hands on the perfect family mover today at an affordable price. ', '', 45, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Fill the car with the whole team without emptying your wallet. ', '', 45, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Your family deserves a vehicle that is affordable to own! ', '', 45, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Get more for your family! ', '', 45, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'This [Make] is a smart investment for your family! ', '', 47, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Buy your family''s next car with total confidence. ', '', 47, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'You deserve reliability - one of our [Make]''s is the way to go... ', '', 47, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'It''s time to own a reliable [Make].', '', 47, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Make a great purchase for your family''s future - ', '', 47, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'This deal is really a no brainer - ', '', 47, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Long term cost of ownership is a critical decision for your family. ', '', 47, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Make long trips more enjoyable with ', '', 46, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Even the kids win - ', '', 46, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Win major brownie points with ', '', 46, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Packed with key equipment the whole family can love - ', '', 46, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Even the family pet will enjoy ', '', 46, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'We know your family comes first. ', '', 36, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'You want a car that fits everyone in your family. ', '', 36, NULL, 1, 1, 1)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'With a footprint like this, you''ll be able to fit in any parking space, any time. ', '', 36, NULL, 1, 1, 4)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'No more hunting for a bigger space - now you can park anywhere. ', '', 36, NULL, 1, 1, 4)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'You drive in town, get a versatile car that''s easy to maneuver. ', '', 36, NULL, 1, 1, 4)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Driving in town doesn''t have to be a squeeze. ', '', 36, NULL, 1, 1, 4)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Reduce your footprint on the streets and the planet. ', '', 36, NULL, 1, 1, 4)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Ever wonder how to make city driving easier? How about a better urban machine... ', '', 36, NULL, 1, 1, 4)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Now you can afford a condo and a great city car. ', '', 45, NULL, 1, 1, 4)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Living in the city is not cheap - get into a car that''s affordable to own. ', '', 45, NULL, 1, 1, 4)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Don''t settle for unaffordable - get into a car that works with your budget. ', '', 45, NULL, 1, 1, 4)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Get a car that leaves enough money in your wallet to enjoy living in the city. ', '', 45, NULL, 1, 1, 4)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Living big doesn''t have to mean an expensive ride. ', '', 45, NULL, 1, 1, 4)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Make waves in the city without big ripples in your bank account. ', '', 45, NULL, 1, 1, 4)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Live in the city? Size matters. ', '', 36, NULL, 1, 1, 4)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'City traffic is much more pleasant with ', '', 46, NULL, 1, 1, 4)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Love driving in the city with ', '', 46, NULL, 1, 1, 4)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Your urban machine includes ', '', 46, NULL, 1, 1, 4)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Why not enjoy zipping through town with ', '', 46, NULL, 1, 1, 4)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Your [Model] city cruiser features: ', '', 46, NULL, 1, 1, 4)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Safety comes first - ', '', 9, NULL, 1, 1, 1)

insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'While you save the planet, you can enjoy:', '', 46, NULL, 1, 1, 2)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'You need reliable transportation to get around town - we can help.', '', 36, NULL, 1, 1, 8)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Great bargains across the board.', '', 36, NULL, 1, 1, 8)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Get a great [Make] at a great price!', '', 36, NULL, 1, 1, 8)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Buying a pre-owned is all about the value you get for the price.', '', 36, NULL, 1, 1, 8)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Get great transportation at a great value.', '', 36, NULL, 1, 1, 8)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'You CAN afford reliable transportation.', '', 45, NULL, 1, 1, 8)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Dependability can be so affordable.', '', 45, NULL, 1, 1, 8)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'A reliable [Make], and yet so affordable.', '', 45, NULL, 1, 1, 8)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Get a dependable ride without breaking the bank.', '', 45, NULL, 1, 1, 8)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'You don''t have to empty your wallet to get around.', '', 45, NULL, 1, 1, 8)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Buy a pre-owned [Make] that''s a joy to drive with an easy to stomach price tag.', '', 45, NULL, 1, 1, 8)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'A great first car and a great value.', '', 45, NULL, 1, 1, 3)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'An affordable [Make] - perfect for students.', '', 45, NULL, 1, 1, 3)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Perfect first car with a perfect cost to own.', '', 45, NULL, 1, 1, 3)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'This [Model] comes with a very reasonable cost of ownership.', '', 45, NULL, 1, 1, 3)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'A bargain to own for you or your kids!', '', 45, NULL, 1, 1, 3)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Get your teen a practical value!', '', 45, NULL, 1, 1, 3)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Get your work done at a reasonable price.', '', 45, NULL, 1, 1, 5)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'All the utility, not all the price.', '', 45, NULL, 1, 1, 5)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'All the toughness without breaking the bank.', '', 45, NULL, 1, 1, 5)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'You need a utility to get work done - do it affordably.', '', 45, NULL, 1, 1, 5)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Do your work at a great cost of ownership.', '', 45, NULL, 1, 1, 5)


--affordability 45
--work (5)
--rec/travel (6)
--value (8)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, '', '', 45, NULL, 1, 1, 1)


--risk reduction [47]
--family (1)
--student (3)
--work (5)
--rec/travel (6)

--student (3)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Get your teen a car that you both can rely on. ','',47,NULL,1,1,3)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Students cannot afford to worry about reliability. ','',47,NULL,1,1,3)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'As a student, reliability is king. ','',47,NULL,1,1,3)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'You may not know it yet, but you''ll find this [Model]''s dependability indispensible. ','',47,NULL,1,1,3)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Your teen deserves a vehicle that doesn''t make either of you worry. ','',47,NULL,1,1,3)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'No worries - you teen can drive a ','',47,NULL,1,1,3)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Get your A+ student a dependable car ','',47,NULL,1,1,3)
-- work (5)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'When your work depends on your vehicle, you should have ','',47,NULL,1,1,5)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'A utility vehicle has to perform whenever you need it - ','',47,NULL,1,1,5)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Work hard and play hard without concern ','',47,NULL,1,1,5)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'No worries - your work can depend on this [Model]. ','',47,NULL,1,1,5)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Enjoy peace of mind for your business - ','',47,NULL,1,1,5)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Drive a [Model] that your business can count on - ','',47,NULL,1,1,5)
-- recreation (6)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Enjoy the thrill of the road, without a moment of worry - ','',47,NULL,1,1,6)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'You can travel across the country in this [Model] with full confidence. ','',47,NULL,1,1,6)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Travel carefree in this dependable [Model]. ','',47,NULL,1,1,6)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'You''ll never worry about getting there in this dependable [Make] .','',47,NULL,1,1,6)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Buy a vehicle that your travelling bone can depend on - ','',47,NULL,1,1,6)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'On the road, comfort is second only to dependability, so shop wisely - ','',47,NULL,1,1,6)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Hit the road with complete confidence in your [Model] ','',47,NULL,1,1,6)
--


---freatures [46]
--5'Work/Utility'
--6'Recreation/Travelling'
--7'Sport'
--8'Value'

--work
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Enjoy a tough vehicle with great features like ','',46,NULL,1,1,5)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Your [Model] can do business and pleasure with ','',46,NULL,1,1,5)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'You work hard, and your [Model] can make that more enjoyable with ','',46,NULL,1,1,5)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Tough jobs will seem easier because this [Model] sports ','',46,NULL,1,1,5)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Make those dirty jobs seem like a piece of cake with ','',46,NULL,1,1,5)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Equipment like ', ' will make your toughest test a breeze. ',46,NULL,1,1,5)
--recreation
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Enjoy the ride with ','',46,NULL,1,1,6)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'The road is more friendly when your [Model] has ','',46,NULL,1,1,6)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Discover the joys of travel - includes ','',46,NULL,1,1,6)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'You can have this [Model] with ','',46,NULL,1,1,6)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Your [Model] is ready to ramble - ','',46,NULL,1,1,6)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Explore the country in comfort with features like ','',46,NULL,1,1,6)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'You can have this nicely optioned [Model] that includes ','',46,NULL,1,1,6)
--sport
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'It''s all about performance.  This [Model] does not dissapoint, and it comes with ','',46,NULL,1,1,7)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'This [Model] is HOT! It features ','',46,NULL,1,1,7)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Wow - you will turn heads and enjoy doing it with ','',46,NULL,1,1,7)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'This [Model] is perfect.  Start cruising along with ','',46,NULL,1,1,7)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'You want a sporty [Model], and you want it to have ','',46,NULL,1,1,7)
--Value
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Your [Model] also has great equipment like ','',46,NULL,1,1,8)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'This fantastic bargain even gets better - ','',46,NULL,1,1,8)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'And the deal is even sweeter with ','',46,NULL,1,1,8)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'This deal of a [Model] gets even sweeter - ','',46,NULL,1,1,8)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'It''s hard to find a better deal on a [Model], especially one with ','',46,NULL,1,1,8)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'We offer lots of great deals, but this one also has ','',46,NULL,1,1,8)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Amazing Value without sacrificing features - ','',46,NULL,1,1,8)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Great bang for your buck! Comes with ','',46,NULL,1,1,8)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Unbeatable [Model] value - comes with ','',46,NULL,1,1,8)
--Green (2)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Your green machine also has great equipment like ','',46,NULL,1,1,2)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Greener driving can also be more fun driving with ','',46,NULL,1,1,2)
insert into templates.blurbSampleText (businessUnitId, blurbPreText, blurbPostText, blurbId, vehicleProfileId, active, isLongForm, themeId) VALUES (100150, 'Get this green [Make] with great features ','',46,NULL,1,1,2)