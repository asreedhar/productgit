UPDATE templates.MerchandisingPoints 
set dataAccessString = ''
WHERE templateDisplayText = ''

UPDATE templates.MerchandisingPoints 
set dataAccessString = '$if(rest(base.HighlightedPackages))$$trunc(base.HighlightedPackages):{n| $n$, }$$last(base.HighlightedPackages):{n| and $n$}$$else$$base.HighlightedPackages$$endif$'
WHERE templateDisplayText = 'Highlighted Packages'

UPDATE templates.MerchandisingPoints 
set dataAccessString = '$if(rest(base.Tier1Equipment))$$trunc(base.Tier1Equipment):{n| $n$, }$$last(base.Tier1Equipment):{n| and $n$}$$else$$base.Tier1Equipment$$endif$'
WHERE templateDisplayText = 'Tier One Equipment'

UPDATE templates.MerchandisingPoints 
set dataAccessString = '$if(rest(base.Tier2Equipment))$$trunc(base.Tier2Equipment):{n| $n$, }$$last(base.Tier2Equipment):{n| and $n$}$$else$$base.Tier2Equipment$$endif$'
WHERE templateDisplayText = 'Tier Two Equipment'


UPDATE templates.MerchandisingPoints 
set dataAccessString = '$if(rest(base.MajorEquipmentOfInterest))$$trunc(base.MajorEquipmentOfInterest):{n| $n$, }$$last(base.MajorEquipmentOfInterest):{n| and $n$}$$else$$base.MajorEquipmentOfInterest$$endif$'
WHERE templateDisplayText = 'Unique Major Equipment'


UPDATE templates.MerchandisingPoints 
set dataAccessString = '$if(rest(base.SafetyOptions))$$trunc(base.SafetyOptions):{n| $n$, }$$last(base.SafetyOptions):{n| and $n$}$$else$$base.SafetyOptions$$endif$'
WHERE templateDisplayText = 'Top Safety Equipment'


UPDATE templates.MerchandisingPoints 
set dataAccessString = '$if(rest(base.LuxuryOptions))$$trunc(base.LuxuryOptions):{n| $n$, }$$last(base.LuxuryOptions):{n| and $n$}$$else$$base.LuxuryOptions$$endif$'
WHERE templateDisplayText = 'Top Luxury Equipment'


UPDATE templates.MerchandisingPoints 
set dataAccessString = '$if(rest(base.ConvenienceOptions))$$trunc(base.ConvenienceOptions):{n| $n$, }$$last(base.ConvenienceOptions):{n| and $n$}$$else$$base.ConvenienceOptions$$endif$'
WHERE templateDisplayText = 'Top Convenience Equipment'


UPDATE templates.MerchandisingPoints 
set dataAccessString = '$if(rest(base.TruckOptions))$$trunc(base.TruckOptions):{n| $n$, }$$last(base.TruckOptions):{n| and $n$}$$else$$base.TruckOptions$$endif$'
WHERE templateDisplayText = 'Top Truck Equipment'


UPDATE templates.MerchandisingPoints 
set dataAccessString = '$if(rest(base.SportyOptions))$$trunc(base.SportyOptions):{n| $n$, }$$last(base.SportyOptions):{n| and $n$}$$else$$base.SportyOptions$$endif$'
WHERE templateDisplayText = 'Top Sporty Equipment'


UPDATE templates.MerchandisingPoints 
set dataAccessString = '$if(rest(base.ValuedOptions))$$trunc(base.ValuedOptions):{n| $n$, }$$last(base.ValuedOptions):{n| and $n$}$$else$$base.ValuedOptions$$endif$ '
WHERE templateDisplayText = 'Top Valued Equipment'
