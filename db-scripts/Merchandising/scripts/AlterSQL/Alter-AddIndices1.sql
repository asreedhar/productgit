ALTER TABLE merchandising.OptionsMapping
ALTER COLUMN optionId INT NOT NULL

CREATE CLUSTERED
    INDEX OptionMap_OptionIdNDX ON merchandising.OptionsMapping (optionCount)
    
ALTER TABLE settings.MerchandisingDescriptionPreferences
ADD CONSTRAINT PK_MerchandisingDescriptionPreferences
PRIMARY KEY CLUSTERED (businessUnitId)

CREATE CLUSTERED  
INDEX NDX_DescriptionContents
ON builder.descriptionContents (businessUnitId, inventoryId)

ALTER TABLE builder.descriptionContents
ADD CONSTRAINT FK_DescriptionContents_Descriptions
FOREIGN KEY (businessUnitId,inventoryId)
REFERENCES builder.Descriptions (businessUnitId, inventoryId)


--VEHICLE OPTIONS!-------------------------------------------------------------------------------
-----REMOVE INVALID OPTIONS FOR FK CONSTRAINT--------
DELETE vo FROM 
builder.vehicleOptions vo
LEFT JOIN builder.optionsConfiguration oc
ON oc.businessUnitId = vo.businessUnitId
AND oc.inventoryId = vo.inventoryId
WHERE
oc.inventoryId IS NULL

ALTER TABLE builder.VehicleOptions
DROP CONSTRAINT PK_VehicleOptions

ALTER TABLE builder.VehicleOptions
ADD CONSTRAINT PK_VehicleOptions
PRIMARY KEY NONCLUSTERED (vehicleOptionId)

ALTER TABLE builder.vehicleOptions
ADD CONSTRAINT FK_VehicleOption_OptionsConfiguration
FOREIGN KEY (businessUnitId, inventoryId)
REFERENCES builder.optionsConfiguration (businessUnitId, inventoryId)

CREATE CLUSTERED
INDEX IX_VehicleOptions
ON builder.VehicleOptions (businessUnitId, inventoryId, optionId)
--END VEHICLE OPTIONS!-------------------------------------------------------------------------------

-----DETAILED VEHICLE OPTIONS!!!!!!_-------

ALTER TABLE builder.VehicleDetailedOptions
DROP CONSTRAINT PK_VehicleDetailedOptions

ALTER TABLE builder.VehicleDetailedOptions
ADD CONSTRAINT PK_VehicleDetailedOptions
PRIMARY KEY NONCLUSTERED (vehicleDetailedOptionId)

ALTER TABLE builder.vehicleDetailedOptions
ADD CONSTRAINT FK_VehicleDetailedOption_OptionsConfiguration
FOREIGN KEY (businessUnitId, inventoryId)
REFERENCES builder.optionsConfiguration (businessUnitId, inventoryId)

CREATE CLUSTERED
INDEX NDX_VehicleDetailedOptions
ON builder.VehicleDetailedOptions (businessUnitId, inventoryId)

----END DETAILE OPTIONS
