IF(EXISTS(SELECT 1 FROM Merchandising.settings.CredentialedSites WHERE siteId = 1))
BEGIN
	UPDATE Merchandising.settings.CredentialedSites
	SET siteName = 'BMW/MINI'
	WHERE siteId = 1
END

IF(EXISTS(SELECT 1 FROM Merchandising.settings.CredentialedSites WHERE siteId = 2))
BEGIN
	UPDATE Merchandising.settings.CredentialedSites
	SET siteName = 'General Motors'
	WHERE siteId = 2
END

IF(EXISTS(SELECT 1 FROM Merchandising.settings.CredentialedSites WHERE siteId = 3))
BEGIN
	UPDATE Merchandising.settings.CredentialedSites
	SET siteName = 'Chrysler'
	WHERE siteId = 3
END

IF(EXISTS(SELECT 1 FROM Merchandising.settings.CredentialedSites WHERE siteId = 4))
BEGIN
	UPDATE Merchandising.settings.CredentialedSites
	SET siteName = 'Volkswagen'
	WHERE siteId = 4
END

IF(EXISTS(SELECT 1 FROM Merchandising.settings.CredentialedSites WHERE siteId = 5))
BEGIN
	UPDATE Merchandising.settings.CredentialedSites
	SET siteName = 'Mazda'
	WHERE siteId = 5
END

IF(EXISTS(SELECT 1 FROM Merchandising.settings.CredentialedSites WHERE siteId = 6))
BEGIN
	UPDATE Merchandising.settings.CredentialedSites
	SET siteName = 'Ford'
	WHERE siteId = 6
END

IF(EXISTS(SELECT 1 FROM Merchandising.settings.CredentialedSites WHERE siteId = 8))
BEGIN
	UPDATE Merchandising.settings.CredentialedSites
	SET siteName = 'Audi'
	WHERE siteId = 8
END

IF(EXISTS(SELECT 1 FROM Merchandising.settings.CredentialedSites WHERE siteId = 9))
BEGIN
	UPDATE Merchandising.settings.CredentialedSites
	SET siteName = 'Hyundai'
	WHERE siteId = 9
END

IF(EXISTS(SELECT 1 FROM Merchandising.settings.CredentialedSites WHERE siteId = 10))
BEGIN
	UPDATE Merchandising.settings.CredentialedSites
	SET siteName = 'Lexus/Toyota National'
	WHERE siteId = 10
END

IF(EXISTS(SELECT 1 FROM Merchandising.settings.CredentialedSites WHERE siteId = 11))
BEGIN
	UPDATE Merchandising.settings.CredentialedSites
	SET siteName = 'Toyota SET'
	WHERE siteId = 11
END

IF(EXISTS(SELECT 1 FROM Merchandising.settings.CredentialedSites WHERE siteId = 12))
BEGIN
	UPDATE Merchandising.settings.CredentialedSites
	SET siteName = 'Nissan/Infiniti'
	WHERE siteId = 12
END
