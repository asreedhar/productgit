ALTER TABLE Merchandising.settings.MerchandisingDescriptionPreferences ADD Threshold_ProofPoint  INT DEFAULT 0 NOT NULL
ALTER TABLE Merchandising.settings.MerchandisingDescriptionPreferences ADD KBB_ProofPoint  INT DEFAULT 0 NOT NULL
ALTER TABLE Merchandising.settings.MerchandisingDescriptionPreferences ADD NADA_ProofPoint  INT DEFAULT 0 NOT NULL
ALTER TABLE Merchandising.settings.MerchandisingDescriptionPreferences ADD MktAvg_ProofPoint  INT DEFAULT 0 NOT NULL
ALTER TABLE Merchandising.settings.MerchandisingDescriptionPreferences ADD MSRP_ProofPoint  INT DEFAULT 0 NOT NULL
ALTER TABLE Merchandising.settings.MerchandisingDescriptionPreferences ADD Edmunds_ProofPoint INT DEFAULT 0  NOT NULL