-- if column exists ... remove it
IF NOT EXISTS(SELECT * FROM sys.columns WHERE [name] LIKE 'TimeToMarketGoalDays' AND [OBJECT_ID] = object_id('settings.merchandising'))
BEGIN
	ALTER TABLE [settings].[merchandising]
		ADD [TimeToMarketGoalDays] INT
END
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE [name] LIKE 'DMSName' AND [OBJECT_ID] = object_id('settings.merchandising'))
BEGIN
	ALTER TABLE [settings].[merchandising]
		ADD [DMSName] VARCHAR(255)
END
GO

IF NOT EXISTS(SELECT * FROM sys.columns WHERE [name] LIKE 'DMSEmail' AND [OBJECT_ID] = object_id('settings.merchandising'))
BEGIN
	ALTER TABLE [settings].[merchandising]
		ADD [DMSEmail] VARCHAR(255)
END
GO
