--DROP TABLE [BucketAlertMonthlyEmail_backup_201409] 
IF NOT EXISTS(SELECT * FROM sys.tables WHERE [name] LIKE 'BucketAlertMonthlyEmail_backup_201409')
BEGIN
	CREATE TABLE [BucketAlertMonthlyEmail_backup_201409] (
		[BusinessUnitID] INT NOT NULL,
		[Email] VARCHAR(128)
	)
END

;WITH [emailList]([BusinessUnitId], [Email], [MonthlyEmail]) AS (
	SELECT	[BusinessUnitId],
			[Email] = CAST(LEFT([MonthlyEmail], CHARINDEX(',', [MonthlyEmail] + ',') - 1) AS VARCHAR(1000)),
			[MonthlyEmail] = STUFF([MonthlyEmail], 1, CHARINDEX(',', [MonthlyEmail] + ','), '')
	FROM	[settings].[BucketAlerts]
	WHERE	[MonthlyEmail] <> ''
		AND	[MonthlyEmail] <> 'DigitalMarketingSpecialist@maxdigital.com'
	UNION ALL
	SELECT	[BusinessUnitId],
			[Email] = CAST(LEFT([MonthlyEmail], CHARINDEX(',', [MonthlyEmail] + ',') - 1) AS VARCHAR(1000)),
			[MonthlyEmail] = STUFF([MonthlyEmail], 1, CHARINDEX(',', [MonthlyEmail] + ','), '')
	FROM	[emailList]
	WHERE	[MonthlyEmail] > ''
)
INSERT	[BucketAlertMonthlyEmail_backup_201409]
SELECT	[BusinessUnitId],
		[Email]
FROM	[emailList]
WHERE	NOT EXISTS (SELECT [BusinessUnitId] FROM [BucketAlertMonthlyEmail_backup_201409] BA WHERE BA.[BusinessUnitId] = emailList.[BusinessUnitId])

UPDATE	[settings].[BucketAlerts]
SET	[MonthlyEmail] = ''
WHERE	[MonthlyEmail] <> ''
	AND	[MonthlyEmail] <> 'DigitalMarketingSpecialist@maxdigital.com'

UPDATE	[settings].[BucketAlerts]
SET	[MonthlyEmail] = 'DigitalMarketingSpecialist@maxdigital.com'
WHERE [BusinessUnitId] IN (
	105834, 105967,105778,105900,106905,106076,105837,106676,106071,106491,104656,105190,104777,106255,106898,104575,
	106235,106093,106820,106659,106395,106301,106531,106530,106532,102410,106008,105898,106360,106226,105323,105535,
	106698,106697,105369,105991,105992,105986,106087,106088,100234,100227,106182,106184,106183,106180,105228,106712,
	106347,106028,106515,106537,100176,100325,100371,100428,100666,100833,100834,100835,100837,100983,100984,101161,
	101323,101324,101325,101326,101339,101424,101425,101426,101427,101514,101592,101594,101595,101636,101642,101661)

--SELECT * FROM [BucketAlertMonthlyEmail_backup_201409]