-- if column exists ... remove it
USE Merchandising
IF NOT EXISTS(SELECT * FROM sys.columns WHERE [name] LIKE 'PositionOffset' AND [OBJECT_ID] = object_id('merchandising.webloaderphotos'))
BEGIN
	ALTER TABLE [merchandising].[webloaderphotos]
		ADD [PositionOffset] INT
END
GO