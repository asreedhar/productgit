ALTER TABLE postings.Publications
ADD ExteriorColor VARCHAR(50) NULL
GO

ALTER TABLE postings.Publications
ADD InteriorColor VARCHAR(50) NULL
GO

ALTER TABLE postings.Publications
ADD InteriorType VARCHAR(50) NULL
GO

ALTER TABLE postings.Publications
ADD MappedOptionIds VARCHAR(1000) NULL
GO
