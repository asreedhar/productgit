/****** Object:  Table [BuyersGuideTemplates] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[settings].[BuyersGuideTemplates]') AND type in (N'U'))
BEGIN

CREATE TABLE [Merchandising].[settings].[BuyersGuideTemplates](
	[buyersGuideTemplateID] [int] IDENTITY(1,1) NOT NULL,
	[styleSheetPath] [varchar](200) NOT NULL
)
END
GO
SET ANSI_PADDING OFF