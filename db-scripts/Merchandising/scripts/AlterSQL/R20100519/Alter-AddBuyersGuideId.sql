ALTER TABLE Merchandising.settings.Merchandising
ADD [buyersGuideTemplateId] int NULL DEFAULT(1)
GO

INSERT INTO Merchandising.settings.BuyersGuideTemplates (styleSheetPath)
VALUES ('~/Public/Media/WindowStickers/buyersGuide.css')
GO

UPDATE Merchandising.settings.Merchandising
SET [buyersGuideTemplateId]=1
GO

INSERT INTO Merchandising.settings.BuyersGuideTemplates (styleSheetPath)
VALUES ('~/Public/Media/WindowStickers/buyersGuide-HondaOfTheAvenues.css')
GO

UPDATE Merchandising.settings.Merchandising
SET [buyersGuideTemplateId]=2
WHERE [businessUnitId]=104286
GO

INSERT INTO Merchandising.settings.WindowStickerTemplates (styleSheetPath)
VALUES ('~/Public/Media/WindowStickers/HondaOfTheAvenues.css')
GO

UPDATE Merchandising.settings.Merchandising
SET [windowStickerTemplateId]=4
WHERE [businessUnitId]=104286
GO