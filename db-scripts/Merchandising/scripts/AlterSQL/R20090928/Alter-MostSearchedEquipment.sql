ALTER TABLE Settings.MostSearchedEquipment
ADD active bit NOT NULL DEFAULT(1)
GO

UPDATE Settings.MostSearchedEquipment
SET active = 0
WHERE categoryid IN (1018,1019, 1074,1075)
GO