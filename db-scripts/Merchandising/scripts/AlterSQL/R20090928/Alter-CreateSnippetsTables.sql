create schema Snippets
GO
------NOTE DO NOT MAKE THE SNIPPETS SELF DROPPING!!!!-----


CREATE TABLE Snippets.Marketing (
	id int IDENTITY(1,1),
	marketingType int,
	marketingText varchar(500),
	sourceId int,
	isVerbatim bit,
	CONSTRAINT PK_Marketing PRIMARY KEY (
		id
	)	
)
GO

CREATE TABLE Snippets.MarketingNote (
	id int IDENTITY(1,1),
	marketingId int,
	notesText varchar(500),
	CONSTRAINT PK_MarketingNote PRIMARY KEY NONCLUSTERED (id)
)
GO

CREATE CLUSTERED
    INDEX IX__MarketingNotes_MarketingId ON Snippets.MarketingNote (marketingId)
GO

create TABLE Snippets.MarketingSource (
	id int IDENTITY(1,1),
	sourceName varchar(50),
	hasCopyright bit,
	CONSTRAINT PK_MarketingSource PRIMARY KEY (
		id
	)
)
GO

CREATE TABLE Snippets.Tag (
	id int IDENTITY(1,1),
	description varchar(30),
	CONSTRAINT PK_Tag PRIMARY KEY (
		id
	)
)
GO

CREATE TABLE Snippets.Marketing_Tag (
	joinId int IDENTITY(1,1),
	marketingId int,
	tagId int,
	CONSTRAINT PK_Marketing_Tag PRIMARY KEY (marketingId, tagId)
)
GO

CREATE INDEX IX__Marketing_Tag_MarketingId_TagId
	ON Snippets.Marketing_Tag (tagId, marketingId)	
GO

CREATE TABLE Snippets.Marketing_Vehicle (
	joinId int IDENTITY(1,1),
	marketingId int,
	chromeStyleId int,
	CONSTRAINT PK_Marketing_Vehicle PRIMARY KEY (marketingId, chromeStyleId)
)
GO
CREATE INDEX IX__Marketing_Vehicle_MarketingId_TagId
	ON Snippets.Marketing_Vehicle (chromeStyleId, marketingId)	
GO
    

CREATE TABLE Snippets.Marketing_Category (
	joinId int IDENTITY(1,1),
	marketingId int,
	categoryId int,
	CONSTRAINT PK_Marketing_Category PRIMARY KEY (marketingId, categoryId)
)
GO
CREATE INDEX IX__Marketing_Category_MarketingId_CategoryId
	ON Snippets.Marketing_Category (categoryId, marketingId)	
GO
 
--FKs for Marketing_Tag
ALTER TABLE Snippets.Marketing_Tag
ADD CONSTRAINT FK_Marketing_Tag_Marketing
FOREIGN KEY (marketingId)
REFERENCES Snippets.Marketing (id)

ALTER TABLE Snippets.Marketing_Tag
ADD CONSTRAINT FK_Marketing_Tag_Tags
FOREIGN KEY (tagId)
REFERENCES Snippets.Tag (id)

--FKs for Marketing_Category (SHOULD ALSO INCLUDE ONE TO POINT TO CHROME CATEGORIES)
ALTER TABLE Snippets.Marketing_Category
ADD CONSTRAINT FK_Marketing_Category_Marketing
FOREIGN KEY (marketingId)
REFERENCES Snippets.Marketing (id)

--FKs for Marketing_Vehicle (SHOULD ALSO INCLUDE ONE TO POINT TO CHROME CATEGORIES)
ALTER TABLE Snippets.Marketing_Vehicle
ADD CONSTRAINT FK_Marketing_Vehicle_Marketing
FOREIGN KEY (marketingId)
REFERENCES Snippets.Marketing (id)

--FK for marketing Note
ALTER TABLE Snippets.MarketingNote
ADD CONSTRAINT FK_MarketingNotes_Marketing
FOREIGN KEY (marketingId)
REFERENCES Snippets.Marketing (id)


--FKs for Marketing
ALTER TABLE Snippets.Marketing
ADD CONSTRAINT FK_Marketing_Source
FOREIGN KEY (sourceId)
REFERENCES Snippets.MarketingSource (id)
