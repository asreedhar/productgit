if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[Themes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [templates].[Themes]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

create table [templates].[Themes] (
	id int IDENTITY(1,1),
	businessUnitId int NOT NULL,
	[name] varchar(50)
)
GO



insert into templates.Themes (businessUnitId, name) VALUES (100150, 'Family')
insert into templates.Themes (businessUnitId, name) VALUES (100150, 'Green/Eco-Friendly')
insert into templates.Themes (businessUnitId, name) VALUES (100150, 'Student/Teen')
insert into templates.Themes (businessUnitId, name) VALUES (100150, 'Urban')
insert into templates.Themes (businessUnitId, name) VALUES (100150, 'Work/Utility')
insert into templates.Themes (businessUnitId, name) VALUES (100150, 'Recreation/Travelling')
insert into templates.Themes (businessUnitId, name) VALUES (100150, 'Sport')
insert into templates.Themes (businessUnitId, name) VALUES (100150, 'Value')
insert into templates.Themes (businessUnitId, name) VALUES (100150, 'Luxury')
GO


ALTER TABLE [templates].[blurbSampleText]
ADD themeId int NULL
GO

ALTER TABLE [builder].[descriptions]
ADD templateId int NULL
GO
ALTER TABLE [builder].[descriptions]
ADD themeId int NULL
GO

--already had "selectedtemplateId" for some reason
ALTER TABLE [audit].[descriptions]
ADD themeId int NULL
GO

--go ahead and set the templateId here...
UPDATE de
set de.templateId = oc.selectedTemplateId
FROM builder.descriptions de
JOIN builder.optionsConfiguration oc
ON oc.businessUnitId = de.businessUnitId
and oc.inventoryId = de.inventoryId
GO
ALTER TABLE [builder].[OptionsConfiguration] DROP CONSTRAINT DF_OptionsConfiguration_selectedTemplateID

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[builder].[DF_OptionsConfiguration_selectedTemplateID]') AND type in (N'D'))
BEGIN
	ALTER TABLE [builder].[OptionsConfiguration]
	DROP CONSTRAINT [DF_OptionsConfiguration_selectedTemplateID]
END

ALTER TABLE [builder].[OptionsConfiguration]
DROP COLUMN selectedTemplateId
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[audit].[DF_OptionsConfiguration_selectedTemplateID]') AND type in (N'D'))
BEGIN
	ALTER TABLE [audit].[OptionsConfiguration]
	DROP CONSTRAINT [DF_OptionsConfiguration_selectedTemplateID]
END

ALTER TABLE [audit].[OptionsConfiguration]
DROP COLUMN selectedTemplateId
GO
