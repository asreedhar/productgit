CREATE TABLE Snippets.UserRating (
	marketingId int,
	ratingScore int,
	businessUnitId int
)
GO

--FKs for Marketing_Tag
ALTER TABLE Snippets.UserRating
ADD CONSTRAINT FK_Marketing_UserRating	
FOREIGN KEY (marketingId)
REFERENCES Snippets.Marketing (id)
GO

--index for userRatings
CREATE CLUSTERED INDEX IX__UserRating_MarketingId
	ON Snippets.UserRating (marketingId)	
GO

CREATE TABLE Snippets.Marketing_Parent (
	marketingId int,
	parentId int,
	CONSTRAINT PK_Marketing_Parent PRIMARY KEY (marketingId)
)
GO

ALTER TABLE Snippets.Marketing_Parent
ADD CONSTRAINT FK_Marketing_Parent_Marketing
FOREIGN KEY (parentId)
REFERENCES Snippets.Marketing (id)
GO
