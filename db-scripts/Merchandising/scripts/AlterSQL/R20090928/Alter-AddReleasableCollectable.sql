ALTER TABLE settings.EdtDestinations
ADD canReleaseTo bit NOT NULL DEFAULT((1))
GO
ALTER TABLE settings.EdtDestinations
ADD canCollectMetricsFrom bit NOT NULL DEFAULT((1))
GO

UPDATE settings.EdtDestinations
SET canCollectMetricsFrom = 0
WHERE destinationID IN (3,4,5)
GO