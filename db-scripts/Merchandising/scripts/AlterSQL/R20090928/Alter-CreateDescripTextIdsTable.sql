
CREATE TABLE builder.Description_SampleTextItems (
	id int IDENTITY(1,1),
	businessUnitId int NOT NULL,
	inventoryId int NOT NULL,
	blurbTextId int,
	textSequence int,
	CONSTRAINT PK_Description_SampleTextItems PRIMARY KEY NONCLUSTERED (
		id
	)	
)
GO

CREATE CLUSTERED INDEX IX__Description_SampleTextItems_businessUnitId_inventoryId
ON builder.Description_SampleTextItems (businessUnitID, inventoryId)	
GO

--foreign key for blurb sample text  
--ACTUALLY IGNORE THIS FOR NOW, AS THE APP WON'T SUPPORT THE FK WHEN DELETING SampleText
--ALTER TABLE builder.Description_SampleTextItems
--ADD CONSTRAINT FK_Marketing_Vehicle_Marketing
--FOREIGN KEY (marketingId)
--REFERENCES Snippets.Marketing (id)
