-- FB: 30601 - auto window sticker minimum days

-- drop default constraint on Minimum days
IF EXISTS (SELECT * 
				FROM sysobjects
				WHERE NAME ='DF_AutoWindowSticker_MinumumDaysDefault'
				AND xtype = 'd')
	begin
		ALTER TABLE settings.AutoWindowSticker
			DROP CONSTRAINT DF_AutoWindowSticker_MinumumDaysDefault
	end
go

-- drop column Minimum days
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'MinimumDays'
				AND object_id = object_id('settings.AutoWindowSticker'))
	begin
		ALTER TABLE settings.AutoWindowSticker
			DROP COLUMN MinimumDays
	end
go


-- add column for minimum days
ALTER TABLE settings.AutoWindowSticker
	ADD MinimumDays smallint NOT NULL 
	CONSTRAINT DF_AutoWindowSticker_MinumumDaysDefault DEFAULT 0
GO

