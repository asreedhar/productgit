-- if column default ... remove it
if EXISTS (SELECT * 
				FROM sysobjects
				WHERE TYPE = 'D'
				AND NAME = 'DF_merchandising_UsePhoneMapping')
	begin
		ALTER TABLE settings.merchandising
			DROP CONSTRAINT DF_merchandising_UsePhoneMapping
	end
	
-- if column exists ... remove it
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'UsePhoneMapping'
				AND object_id = object_id('settings.merchandising'))
	begin
		ALTER TABLE settings.merchandising
			DROP COLUMN UsePhoneMapping
	end

-- add column for Max for smartphone, FB:30138
ALTER TABLE settings.merchandising
	ADD UsePhoneMapping bit NOT NULL CONSTRAINT DF_merchandising_UsePhoneMapping DEFAULT 0
GO

