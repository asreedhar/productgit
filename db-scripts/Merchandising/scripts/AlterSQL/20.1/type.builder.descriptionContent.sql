-- must drop all dependencies first!!!!
if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[descriptionItem#Merge]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [builder].descriptionItem#Merge
GO


IF EXISTS (SELECT * FROM sys.types WHERE name = N'descriptionContent' AND is_table_type = 1)
	DROP TYPE builder.descriptionContent
GO

BEGIN

    CREATE TYPE builder.descriptionContent AS TABLE
    (
		descriptionItemTypeId int
    )

END
GO

GRANT EXECUTE ON TYPE::builder.descriptionContent TO MerchandisingUser
GO


