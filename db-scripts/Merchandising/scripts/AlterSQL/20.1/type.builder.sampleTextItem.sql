-- must drop all dependencies first!!!!
if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[DescriptionSampleTextItem#Merge]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	DROP PROCEDURE [builder].[DescriptionSampleTextItem#Merge]
GO


IF EXISTS (SELECT * FROM sys.types WHERE name = N'sampleTextItem' AND is_table_type = 1)
	DROP TYPE builder.sampleTextItem
GO

BEGIN

    CREATE TYPE builder.sampleTextItem AS TABLE
    (
		blurbTextId int,
		textSequence int
		
    )

END
GO

GRANT EXECUTE ON TYPE::builder.sampleTextItem TO MerchandisingUser
GO

