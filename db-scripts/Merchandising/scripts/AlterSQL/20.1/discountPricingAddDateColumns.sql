-- add column for activateddate and deactivated date 30372
ALTER TABLE merchandising.DiscountPricingCampaignInventory
	ADD ActivatedDate DATETIME NULL,
		DeactivatedDate DATETIME NULL
GO

