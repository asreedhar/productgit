if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[BuildRequestLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table [builder].[BuildRequestLog]
GO

CREATE TABLE builder.BuildRequestLog(
	BuildRequestLogId int IDENTITY(1,1) NOT NULL,
	businessUnitId	int not null,
	vin varchar(17) not null,
	statusTypeId int not null,
	siteId	int not null,
	credentialBusinessUnit int null,
	requestedOn	smalldatetime not null default getdate(),
	requestedBy	varchar(30)	
)
GO 

ALTER TABLE builder.BuildRequestLog
	ADD CONSTRAINT PK_BuildRequestLog
	PRIMARY KEY NONCLUSTERED (BuildRequestLogId) 
GO

ALTER TABLE builder.BuildRequestLog
	ADD CONSTRAINT FK_BuildRequestLog_statusTypeId
	FOREIGN KEY (statusTypeId) 
	REFERENCES builder.AutoloadStatusTypes(statusTypeId)
GO

ALTER TABLE builder.BuildRequestLog
	ADD CONSTRAINT FK_BuildRequestLog_siteId
	FOREIGN KEY (siteId) 
	REFERENCES settings.CredentialedSites (siteId)
GO

CREATE INDEX IX_BuildRequestLog_businessUnitId_vin ON builder.BuildRequestLog(businessUnitId, vin)
go

CREATE INDEX IX_BuildRequestLog_vin ON builder.BuildRequestLog(vin)
go
