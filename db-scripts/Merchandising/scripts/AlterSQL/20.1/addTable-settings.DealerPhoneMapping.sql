IF(NOT EXISTS(SELECT 1 FROM [sys].[tables] where name = 'DealerPhoneMapping'))
BEGIN
	--DROP TABLE [settings].[DealerPhoneMapping]
	CREATE TABLE [settings].[DealerPhoneMapping] (
		[BusinessUnitID] [INT] NOT NULL,
		[MappedPhone] [VARCHAR](14) NOT NULL,
		[ProvisionedPhone] [VARCHAR](14) NOT NULL,
		[Purchased] [BIT] NOT NULL  CONSTRAINT DF_DealerPhoneMapping_Purchased DEFAULT 0

		CONSTRAINT [PK_DealerPhoneMapping] PRIMARY KEY CLUSTERED ([BusinessUnitID] ASC) ON [PRIMARY]
	) ON [PRIMARY]
END
GO