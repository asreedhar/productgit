IF NOT EXISTS (
	SELECT	1
	FROM	[sys].[columns]
	WHERE	[name] LIKE 'MonthlyEmail'
		AND	[object_id] = OBJECT_ID('[settings].[BucketAlerts]')
)
	ALTER TABLE [settings].[BucketAlerts]
		ADD	[MonthlyEmail] [VARCHAR](1000) NOT NULL CONSTRAINT DF_settings_BucketAlerts_MonthlyEmail DEFAULT ('')

IF NOT EXISTS (
	SELECT	1
	FROM	[sys].[columns]
	WHERE	[name] LIKE 'MonthlyEmailBounced'
		AND	[object_id] = OBJECT_ID('[settings].[BucketAlerts]')
)
	ALTER TABLE [settings].[BucketAlerts]
		ADD [MonthlyEmailBounced] [BIT] NOT NULL CONSTRAINT DF_settings_BucketAlerts_MonthlyEmailBounced DEFAULT (0)

IF NOT EXISTS (
	SELECT	*
	FROM	[sysobjects]
	WHERE	[type] = 'D'
		AND	[name] = 'DF_settings_BucketAlerts_MonthlyEmail'
)
	ALTER TABLE [settings].[BucketAlerts]
		ADD CONSTRAINT DF_settings_BucketAlerts_MonthlyEmail DEFAULT ('') FOR [MonthlyEmail]

IF NOT EXISTS (
	SELECT	*
	FROM	[sysobjects]
	WHERE	[type] = 'D'
		AND	[name] = 'DF_settings_BucketAlerts_MonthlyEmailBounced'
)
	ALTER TABLE [settings].[BucketAlerts]
		ADD CONSTRAINT DF_settings_BucketAlerts_MonthlyEmailBounced DEFAULT (0) FOR [MonthlyEmailBounced]

IF NOT EXISTS (
	SELECT	*
	FROM	[sysobjects]
	WHERE	[type] = 'D'
		AND	[name] = 'DF_settings_BucketAlerts_WeeklyEmail'
)
	ALTER TABLE [settings].[BucketAlerts]
		ADD CONSTRAINT DF_settings_BucketAlerts_WeeklyEmail DEFAULT ('') FOR [WeeklyEmail]

IF NOT EXISTS (
	SELECT  1
	FROM	[sys].[columns] 
	WHERE   [name] = 'WeeklyEmail'
		AND	[object_id] = OBJECT_ID('[settings].[BucketAlerts]')
		AND	[is_nullable] = 0
)
BEGIN
	UPDATE [settings].[BucketAlerts]
	SET [WeeklyEmail] = ''
	WHERE [WeeklyEmail] IS NULL

	ALTER TABLE [settings].[BucketAlerts]
		ALTER COLUMN [WeeklyEmail] [VARCHAR](1000) NOT NULL
END
GO