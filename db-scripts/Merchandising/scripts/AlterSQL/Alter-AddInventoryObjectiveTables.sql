if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[InventoryObjectives]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[InventoryObjectives]
GO
if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[InventoryObjectiveMapping]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[InventoryObjectiveMapping]
GO




SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[InventoryObjectives] (
	[objectiveTypeId] [int] NOT NULL,
	[description] [varchar](30) NOT NULL,
	[shortDescription] [varchar](1) NOT NULL,
	 CONSTRAINT [PK_InventoryObjectives] PRIMARY KEY CLUSTERED 
	(
		[objectiveTypeId] ASC
	)
 )

CREATE TABLE [settings].[InventoryObjectiveMapping](
	[businessUnitId] [int] NOT NULL,
	[objectiveTypeId] [int] NOT NULL,
	[inventoryStatusCD] [int] NOT NULL,
 CONSTRAINT [PK_InventoryObjectiveMapping] PRIMARY KEY NONCLUSTERED 
	(
	[businessUnitId] ASC,
	[inventoryStatusCD] ASC
	)
)

GO
ALTER TABLE [settings].[InventoryObjectiveMapping]  
ADD CONSTRAINT [FK_InventoryObjectiveMapping_InventoryObjectives] 
FOREIGN KEY([objectiveTypeId])
REFERENCES [settings].[InventoryObjectives] ([objectiveTypeId])
GO

insert into settings.inventoryObjectives (objectiveTypeId, description, shortDescription) VALUES (1, 'Retail', 'R')
insert into settings.inventoryObjectives (objectiveTypeId, description, shortDescription) VALUES (2, 'Wholesale', 'W')
insert into settings.inventoryObjectives (objectiveTypeId, description, shortDescription) VALUES (3, 'Other', 'O')

insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,3,0)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,3,2)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,3,3)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,3,4)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,3,5)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,3,6)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,3,7)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,3,8)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,3,9)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,3,10)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,3,11)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,3,12)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,3,13)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,3,14)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,1,16)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,2,17)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,3,18)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,3,19)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,1,20)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,1,21)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,3,22)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,2,23)
insert into settings.inventoryObjectiveMapping (businessUnitId, objectiveTypeId, inventoryStatusCD) VALUES (100150,2,24)


GO
SET ANSI_PADDING OFF

