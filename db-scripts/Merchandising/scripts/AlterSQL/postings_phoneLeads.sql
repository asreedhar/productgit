if exists (select * from dbo.sysobjects where id = object_id(N'[postings].[PhoneLeads]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [postings].[PhoneLeads]
GO

/****** Object:  Table [postings].[PhoneLeads]    Script Date: 10/26/2008 12:58:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [postings].[PhoneLeads](
	leadId int IDENTITY(1,1),
	businessUnitId int,
	sourceId int,
	LeadDate datetime, 
	CallerPhone varchar(20),
	CallerZip int, 
	CallStatus varchar(50),
    CallDurationMinutes int, 
	CallDurationSeconds int
	
 CONSTRAINT [PK_phoneLeads] PRIMARY KEY NONCLUSTERED 
(
	[leadID] ASC
)
)

GO


CREATE CLUSTERED  
INDEX NDX_PhoneLeads
ON postings.PhoneLeads (businessUnitId, sourceId, leadDate)
GO 

SET ANSI_PADDING OFF