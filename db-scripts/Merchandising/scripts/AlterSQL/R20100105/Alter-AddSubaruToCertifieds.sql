declare @subaruId int, @subaruGroupId int

INSERT INTO builder.manufacturerCertifications ([name]) VALUES ('Subaru')
select @subaruId = scope_identity()


INSERT INTO settings.cpoGroups ([name]) VALUES ('Subaru')
select @subaruGroupId = scope_identity()

insert into settings.cpoCertifieds (cpoGroupId, certificationTypeId) VALUES (@subaruGroupId, @subaruId)



--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,@subaruId, 4,'Qualifying vehicles under the original 3-year/36,000-mile new car warranty come standard with a 6-year/100,000-mile powertrain warranty')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,@subaruId, 10,'No Deductible')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,@subaruId, 14,'152-Point Inspection')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,@subaruId, 13,'Carfax Vehicle History Report')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,@subaruId, 17,'24-hour Roadside Assistance')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,@subaruId, 21,'Trip Planning Services')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,@subaruId, 8,'Qualifying vehicles are eligible for a 6-year/60,000-mile Classic or Gold Plus plan warranty')
GO



