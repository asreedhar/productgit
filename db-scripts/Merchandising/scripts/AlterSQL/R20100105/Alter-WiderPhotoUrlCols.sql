ALTER TABLE builder.vehiclePhotos
ALTER COLUMN imageUrl varchar(200) NOT NULL 
GO

ALTER TABLE builder.vehiclePhotos
ALTER COLUMN imageThumbnailUrl varchar(200) NOT NULL 
GO