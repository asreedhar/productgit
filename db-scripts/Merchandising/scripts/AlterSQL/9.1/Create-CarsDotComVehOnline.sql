
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [merchandising].[CarsDotComVehOnline](
	[BusinessUnitID] [int] NOT NULL,
	[VIN] [varchar](50) NOT NULL,
	[Type] [varchar](50) NULL,
	[YearMakeModel] [varchar](75) NULL,
	[StockNumber] [varchar](50) NOT NULL,
	[HasPhoto] [bit] NULL,
	[Price] [money] NULL,
	[MarketAvgPrice] [money] NULL,
	[Mileage] [int] NULL,
	[MarketAvgMileage] [int] NULL,
	[ExpirationDate] [datetime] NULL,
	[Source] [varchar](100) NULL,
	[PhotoCount] [int] NULL,
	[DaysLive] [int] NULL,
	[RequestId] [int] NOT NULL,
	[DateLoaded] [datetime] NOT NULL constraint DF_CarsDotComVehOnline_DateLoaded default (getdate()),
 CONSTRAINT [PK_CarsDotComVehOnline] PRIMARY KEY CLUSTERED 
(
	[BusinessUnitID] ASC,
	[RequestId] ASC,
	[VIN] ASC,
	[StockNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF