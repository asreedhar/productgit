CREATE TABLE [Merchandising].[CarsDotComPerfSummary]
(
	[businessUnitID] [int] NOT NULL,
	[month] [datetime] NOT NULL,
	[InSearchResult] [int] NULL,
	[RequestDetail] [int] NULL,
	[SearchDealer] [int] NULL,
	[ViewVideo] [int] NULL,
	[EmailDealerNew] [int] NULL,
	[EmailDealerUsed] [int] NULL,
	[PhoneProspectNew] [int] NULL,
	[PhoneProspectsUsed] [int] NULL,
	[ChatProspectsNew] [int] NULL,
	[ChatProspectsUsed] [int] NULL,
	[ClickThru] [int] NULL,
	[PrintAD] [int] NULL,
	[ViewMap] [int] NULL,
	[TotalContacts] [int] NULL,
	[PctListPriceUsed] [int] NULL,
	[PctListCommentUsed] [int] NULL,
	[PctListPhotoUsed] [int] NULL,
	[PctSinglePhotoUsed] [int] NULL,
	[pctMultiPhotoUsed] [int] NULL,
	[PctListVideoUsed] [int] NULL,
	[PctListPriceNew] [int] NULL,
	[PctListCommentNew] [int] NULL,
	[PctListPhotoNew] [int] NULL,
	[PctSinglePhotoNew] [int] NULL,
	[pctMultiPhotoNew] [int] NULL,
	[PctListVideoNew] [int] NULL,
	[Load_ID] [int] NOT NULL,
	[RequestId] [int] NOT NULL,
	[DateLoaded] [datetime] NOT NULL CONSTRAINT [DF_CarsDotComPerfSummary_DateLoaded] DEFAULT (getdate())
) ON [PRIMARY]


ALTER TABLE [merchandising].[CarsDotComPerfSummary] ADD CONSTRAINT [PK_CarsDotComPerfSummary] PRIMARY KEY CLUSTERED  ([businessUnitID], [month], [RequestId]) ON [PRIMARY]

