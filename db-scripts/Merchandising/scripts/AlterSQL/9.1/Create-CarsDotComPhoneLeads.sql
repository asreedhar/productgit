-- drop table postings.carsdotcomphoneleads
CREATE TABLE [postings].[CarsDotComPhoneLeads]
(
	[leadId] [int] NOT NULL IDENTITY(1, 1),
	[businessUnitId] [int] NULL,
	[LeadDate] [datetime] NULL,
	[CallerPhone] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CallerZip] [int] NULL,
	[CallStatus] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[CallDurationMinutes] [int] NULL,
	[CallDurationSeconds] [int] NULL,
	[Load_ID] [int] NOT NULL,
	[RequestId] [int] NOT NULL,
	[DateLoaded] [datetime] NOT NULL CONSTRAINT [DF_CarsDotComPhoneLeads_DateLoaded] DEFAULT (getdate())
) ON [PRIMARY]

CREATE CLUSTERED INDEX [NDX_PhoneLeads] ON [postings].[CarsDotComPhoneLeads] ([businessUnitId], [LeadDate]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
ALTER TABLE [postings].[CarsDotComPhoneLeads] ADD CONSTRAINT [PK_CarsDotComPhoneLeads] PRIMARY KEY NONCLUSTERED  ([leadId]) WITH (FILLFACTOR=90) ON [PRIMARY]
GO
