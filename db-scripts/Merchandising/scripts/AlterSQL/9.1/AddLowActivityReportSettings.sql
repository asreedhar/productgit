
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER TABLE settings.Merchandising ADD
  lowActivityThreshold real NULL,
  highActivityThreshold real NULL ,   
  minSearchCountThreshold int NULL
