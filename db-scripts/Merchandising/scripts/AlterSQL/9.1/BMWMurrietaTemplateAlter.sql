
declare @stores table (BusinessUnitID int not null)
declare @templates table (templateID int not null)


insert @stores (BusinessUnitID)
select distinct BusinessUnitID
from IMT.dbo.BusinessUnit
where
	BusinessUnitTypeID = 4
	and	BusinessUnitCode in (
		'BMWOFMUR01'
	)


insert @templates (templateID)
select templateID from Merchandising.templates.AdTemplates where templateID in (8565,8566,8567,8568,8569,8570,8571,8572,8573,8574)


--- do not edit script below this line, only edit the stores and templates list above

set xact_abort on
set transaction isolation level serializable
begin transaction

declare	@rows int
	
declare @storeProfiles table
(
	BusinessUnitID int not null,
	sourceTemplateId int not null,
	vehicleProfileId int null
)

insert @storeProfiles
(
	BusinessUnitID,
	sourceTemplateId
)
select
	s.BusinessUnitID,
	t.templateID
from
	@stores s
	cross join @templates t


declare @vpToDelete table
(
	vehicleProfileID int
)

-- delete all VehicleProfile, AdTemplate, and AdTemplateItems for the stores 

insert @vpToDelete
(
	vehicleProfileID
)
select
	vp.vehicleProfileID
from
	Merchandising.templates.VehicleProfiles vp
	join Merchandising.templates.AdTemplates at
		on vp.vehicleProfileID = at.vehicleProfileId
	join @storeProfiles sp
		on at.TemplateId = sp.sourceTemplateId
		and at.businessUnitID = sp.BusinessUnitID
		and vp.businessUnitId = sp.BusinessUnitID
	
set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' pre-exising VehicleProfiles staged for delete.'

delete ati
from
	Merchandising.templates.AdTemplateItems ati
	join Merchandising.templates.AdTemplates at
		on ati.templateID = at.templateID
	join @storeProfiles sp
		on at.businessUnitID = sp.BusinessUnitID
		and at.TemplateId = sp.sourceTemplateId

set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' pre-exising AdTemplateItems deleted.'

delete at
from
	Merchandising.templates.AdTemplates at
	join @storeProfiles sp
		on at.TemplateId = sp.sourceTemplateId
		and at.businessUnitID = sp.BusinessUnitID

set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' pre-exising AdTemplates deleted.'

delete vp
from
	Merchandising.templates.VehicleProfiles vp
	join @vpToDelete d
		on vp.vehicleProfileID = d.vehicleProfileID

set @rows = @@ROWCOUNT
print convert(varchar(10), @rows) + ' pre-exising VehicleProfiles deleted.'


commit
set xact_abort off
set transaction isolation level read uncommitted

		