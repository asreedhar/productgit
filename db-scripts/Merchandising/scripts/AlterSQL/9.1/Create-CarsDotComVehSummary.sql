-- DROP TABLE [merchandising].[CarsDotComVehSummary]
CREATE TABLE [merchandising].[CarsDotComVehSummary](
                [businessUnitID] [int] NOT NULL,
                [month] [datetime] NOT NULL
				, StockNumber varchar(25) not null
				, Year smallint
				, Make varchar(150)
				, VIN varchar(25) not null
				, Type varchar(5)
				, Price int
				, Age int
				, InSearchResult int
				, DetailViewed int
				, VideoViewed int
				, AdsPrinted int
				, MapViewed int
				, ClickThru int
				, EmailSent int
				, Chat int
				, Total int
				, PhotoCount int ,
                [Load_ID] int NOT NULL, -- Server01.DBAStat.dbo.Dataload_History.Load_ID
                [RequestId] [int] NOT NULL,
				[DateLoaded] [datetime] NOT NULL CONSTRAINT [DF_CarsDotComVehSummary_DateLoaded] DEFAULT (getdate())
) ON [PRIMARY]

GO
ALTER TABLE [merchandising].[CarsDotComVehSummary] ADD CONSTRAINT [PK_CarsDotComVehSummary] PRIMARY KEY CLUSTERED  ([businessUnitID], [RequestId], [month], [VIN], [StockNumber]) ON [PRIMARY]
GO



GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Merchandising.Merchandising.CarsDotComVehSummary.Load_ID',
	@Description=N'Maps to Server01.DBAStat.dbo.Dataload_History.Load_ID'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Merchandising.Merchandising.CarsDotComVehSummary.RequestID',
	@Description=N'Maps to Server03.Datafeeds.Extract.FetchDotCom#Request.RequestID'
GO


EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Merchandising.Merchandising.CarsDotComVehSummary.DateLoaded',
	@Description=N'The date this record was loaded into Merchandising.'
go

