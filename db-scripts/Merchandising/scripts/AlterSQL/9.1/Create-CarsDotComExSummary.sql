CREATE TABLE [Merchandising].[CarsDotComExSummary]
(
	[businessUnitID] [int] NOT NULL,
	[month] [datetime] NOT NULL,
	[InSearchResult] [int] NOT NULL,
	[SearchToDetailRate] [tinyint] NOT NULL,
	[ViewDetail] [int] NOT NULL,
	[Contacts] [int] NOT NULL,
	[Sold] [int] NOT NULL,
	[Load_ID] [int] NOT NULL,
	[RequestId] [int] NOT NULL,
	[DateLoaded] [datetime] NOT NULL CONSTRAINT [DF_CarsDotComExSummary_DateLoaded] DEFAULT (getdate())
) ON [PRIMARY]

ALTER TABLE [merchandising].[CarsDotComExSummary] ADD CONSTRAINT [PK_CarsDotComExSummary] PRIMARY KEY CLUSTERED  ([businessUnitID], [month], [RequestId]) ON [PRIMARY]

