CREATE TABLE settings.DealerDatafeedPreferences
(
	BusinessUnitID	INT NOT NULL,
	DatafeedCode	VARCHAR(20) NOT NULL,
	SendHTMLDescription	BIT NOT NULL CONSTRAINT DF_DealerDatafeedPreferences_SENDHTML DEFAULT (0)
	
	CONSTRAINT PK_DealerDatafeedPreferences PRIMARY KEY (BusinessUnitID, DatafeedCode)
)

INSERT INTO settings.DealerDatafeedPreferences (BusinessUnitID, DatafeedCode, SendHTMLDescription)
SELECT 102190, 'AULtec', 1 -- Bloomington Acura Subaru
UNION
SELECT 102489, 'AULtec', 1 -- White Bear Acura Subaru
