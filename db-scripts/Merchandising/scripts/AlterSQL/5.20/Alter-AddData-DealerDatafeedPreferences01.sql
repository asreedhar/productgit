DECLARE @BUID INT

--SELECT * FROM settings.DealerDatafeedPreferences DDP

UPDATE settings.DealerDatafeedPreferences
SET SendHTMLDescription=0
WHERE DatafeedCode='AULtec'

--SELECT * FROM settings.DealerDatafeedPreferences DDP

SET @BUID=104522 -- Snyder Pontiac Buick
IF (SELECT COUNT(*) FROM settings.DealerDatafeedPreferences WHERE BusinessUnitID=@BUID AND DatafeedCode='AULtec')=0
	INSERT INTO settings.DealerDatafeedPreferences
	        (BusinessUnitID ,
	         DatafeedCode ,
	         SendHTMLDescription)
	VALUES  (@BUID , -- BusinessUnitID - int
	         'AULtec' , -- DatafeedCode - varchar(20)
	         0  -- SendHTMLDescription - bit
	         )
	         
UPDATE settings.DealerDatafeedPreferences
SET SendHTMLDescription=1
WHERE BusinessUnitID=@BUID
AND DatafeedCode='AULtec'



SET @BUID=104523 -- Snyder Chevrolet
IF (SELECT COUNT(*) FROM settings.DealerDatafeedPreferences WHERE BusinessUnitID=@BUID AND DatafeedCode='AULtec')=0
	INSERT INTO settings.DealerDatafeedPreferences
	        (BusinessUnitID ,
	         DatafeedCode ,
	         SendHTMLDescription)
	VALUES  (@BUID , -- BusinessUnitID - int
	         'AULtec' , -- DatafeedCode - varchar(20)
	         0  -- SendHTMLDescription - bit
	         )
	         
UPDATE settings.DealerDatafeedPreferences
SET SendHTMLDescription=1
WHERE BusinessUnitID=@BUID
AND DatafeedCode='AULtec'

--SELECT * FROM settings.DealerDatafeedPreferences DDP
