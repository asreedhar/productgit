CREATE TABLE settings.EdtDestinationPhotoProvider (
	EdtDestinationId INT NOT NULL,
	PhotoProviderId INT NOT NULL,
	CONSTRAINT PK_EdtDestinationPhotoProvider PRIMARY KEY NONCLUSTERED (
		PhotoProviderId, EdtDestinationId
	),
	CONSTRAINT FK__EdtDestination_Id 
		FOREIGN KEY (EdtDestinationId)
		REFERENCES settings.EdtDestinations(destinationID)
)
GO

INSERT INTO settings.EdtDestinationPhotoProvider (
	EdtDestinationId,
	PhotoProviderId
)
SELECT 7 AS EdtDestinationId, 6070 AS PhotoProviderId --auto uplink usa
UNION ALL SELECT 5, 7962 --ebiz
UNION ALL SELECT 3, 8627 --getauto/dealer specialties
GO

--get rid of function that does what the table above does.
if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[GetEdtDestinationIdFromPhotoProviderId]') and xtype in (N'FN', N'IF', N'TF'))
drop function [builder].[GetEdtDestinationIdFromPhotoProviderId]
GO