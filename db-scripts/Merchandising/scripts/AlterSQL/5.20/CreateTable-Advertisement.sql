if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[Advertisement]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[Advertisement]
GO

CREATE TABLE [builder].[Advertisement](
	businessUnitId int NOT null,
	inventoryId int NOT NULL,
	advertisementXml varchar(MAX) NOT NULL,
	createdOn datetime NOT NULL,
	createdBy varchar(30) NOT NULL,
	lastUpdatedOn datetime,
	lastUpdatedBy varchar(30),
	CONSTRAINT [PK_Advertisement] PRIMARY KEY CLUSTERED 
	(
		businessUnitId, inventoryId
	) 
)

GO