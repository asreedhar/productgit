--Constraint added in Alter-AddIndices2.sql
--Logic for FK is faulty; OptionConfiguration may be missing Active Inventory items
--However, Wanamaker.WebApp always goes to fetch Active Inventory not from OptionConfiguration
--This prevents VehiclesPhotos from being populated correctly, as percieved by the end user
--BUGZID: 12598

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[builder].[FK_VehiclePhotos_OptionsConfiguration]') AND parent_object_id = OBJECT_ID(N'[builder].[VehiclePhotos]'))
ALTER TABLE [builder].[VehiclePhotos] DROP CONSTRAINT [FK_VehiclePhotos_OptionsConfiguration]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[builder].[FK_VehicleStatus_OptionsConfiguration]') AND parent_object_id = OBJECT_ID(N'[builder].[VehicleStatus]'))
ALTER TABLE [builder].[VehicleStatus] DROP CONSTRAINT [FK_VehicleStatus_OptionsConfiguration]
GO