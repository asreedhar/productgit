--Add some documentation since the FK's are impossible w/the current table structure.
--Also, the C# code passes around int's, so we have no clue really what these #'s mean.
ALTER TABLE builder.VehicleStatus
ADD StatusTypeStatusLevelDoc AS
	CASE WHEN StatusTypeId = 0 AND StatusLevel = 0 THEN 'VehicleStatusTypes: undefined, ActivityStatusCode: Untouched'
		 WHEN StatusTypeId = 0 AND StatusLevel = 1 THEN 'VehicleStatusTypes: undefined, ActivityStatusCode: Incomplete'
		 WHEN StatusTypeId = 0 AND StatusLevel = 2 THEN 'VehicleStatusTypes: undefined, ActivityStatusCode: Complete'
		 WHEN StatusTypeId = 1 AND StatusLevel = 0 THEN 'VehicleStatusTypes: ExteriorEquipment, ActivityStatusCode: Untouched'
		 WHEN StatusTypeId = 1 AND StatusLevel = 1 THEN 'VehicleStatusTypes: ExteriorEquipment, ActivityStatusCode: Incomplete'
		 WHEN StatusTypeId = 1 AND StatusLevel = 2 THEN 'VehicleStatusTypes: ExteriorEquipment, ActivityStatusCode: Complete'
		 WHEN StatusTypeId = 2 AND StatusLevel = 0 THEN 'VehicleStatusTypes: InteriorEquipment, ActivityStatusCode: Untouched'
		 WHEN StatusTypeId = 2 AND StatusLevel = 1 THEN 'VehicleStatusTypes: InteriorEquipment, ActivityStatusCode: Incomplete'
		 WHEN StatusTypeId = 2 AND StatusLevel = 2 THEN 'VehicleStatusTypes: InteriorEquipment, ActivityStatusCode: Complete'
		 WHEN StatusTypeId = 3 AND StatusLevel = 0 THEN 'VehicleStatusTypes: Photos, ActivityStatusCode: Untouched'
		 WHEN StatusTypeId = 3 AND StatusLevel = 1 THEN 'VehicleStatusTypes: Photos, ActivityStatusCode: Incomplete'
		 WHEN StatusTypeId = 3 AND StatusLevel = 2 THEN 'VehicleStatusTypes: Photos, ActivityStatusCode: Complete'
		 WHEN StatusTypeId = 4 AND StatusLevel = 0 THEN 'VehicleStatusTypes: Pricing, ActivityStatusCode: Untouched'
		 WHEN StatusTypeId = 4 AND StatusLevel = 1 THEN 'VehicleStatusTypes: Pricing, ActivityStatusCode: Incomplete'
		 WHEN StatusTypeId = 4 AND StatusLevel = 2 THEN 'VehicleStatusTypes: Pricing, ActivityStatusCode: Complete'
		 WHEN StatusTypeId = 5 AND StatusLevel = 0 THEN 'VehicleStatusTypes: Posting, AdvertisementPostingStatus: Error'
		 WHEN StatusTypeId = 5 AND StatusLevel = 1 THEN 'VehicleStatusTypes: Posting, AdvertisementPostingStatus: NeedsAction'
		 WHEN StatusTypeId = 5 AND StatusLevel = 2 THEN 'VehicleStatusTypes: Posting, AdvertisementPostingStatus: PendingApproval'
		 WHEN StatusTypeId = 5 AND StatusLevel = 3 THEN 'VehicleStatusTypes: Posting, AdvertisementPostingStatus: CurrentlyReleasing'
		 WHEN StatusTypeId = 5 AND StatusLevel = 4 THEN 'VehicleStatusTypes: Posting, AdvertisementPostingStatus: NoActionRequired'
		 ELSE 'Undefined, or could not reverse engineer from c# code'
	END
GO

ALTER TABLE builder.VehicleStatusTypes
ADD CONSTRAINT [PK__builder_VehicleStatusTypes] PRIMARY KEY CLUSTERED (
	statusTypeId
)
GO

ALTER TABLE builder.VehicleStatus
ADD CONSTRAINT [FK__VehicleStatus_VehicleStatusTypes] 
FOREIGN KEY (statusTypeId)
REFERENCES builder.VehicleStatusTypes(statusTypeId)
GO