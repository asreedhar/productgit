if object_id('Merchandising.settings.Environment') is not null
	drop table Merchandising.settings.Environment
go


create table Merchandising.settings.Environment
(
	SettingName varchar(100) not null,
	SettingValue varchar(1000) null
)
go

alter table Merchandising.settings.Environment add constraint PK_Environment primary key clustered (SettingName)
go
