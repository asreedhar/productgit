declare
	@LoadID int,
	@DatasourceInterfaceID int

set nocount on
set transaction isolation level read uncommitted


declare @loads table
(
	LoadId int not null,
	DatasourceInterfaceId int not null
)

insert @loads
(
	LoadId,
	DatasourceInterfaceId
)
select top 100
	Load_ID,
	DatasourceInterfaceId = 14
from Merchandising.merchandising.AutoTraderVehOnline
group by Load_ID
order by Load_ID desc



insert @loads
(
	LoadId,
	DatasourceInterfaceId
)
select top 100
	Load_ID,
	DatasourceInterfaceId = 2000
from Merchandising.merchandising.CarsDotComVehOnline
group by Load_ID
order by Load_ID desc

insert @loads
(
	LoadId,
	DatasourceInterfaceId
)
select top 100
	LoadId,
	DatasourceInterfaceId = 2009
from Merchandising.merchandising.AutoTraderHendrickOnline
group by LoadId
order by LoadId desc

insert @loads
(
	LoadId,
	DatasourceInterfaceId
)
select top 100
	LoadId,
	DatasourceInterfaceId = 2010
from Merchandising.dashboard.VehicleOnline
group by LoadId
order by LoadId desc


set @LoadId = -1000
while (1=1)
begin
	select top 1
		@LoadId = LoadId,
		@DatasourceInterfaceID = DatasourceInterfaceId
	from @loads
	where LoadId > @LoadID
	order by LoadId asc
	
	if (@@rowcount <> 1) break
	
	delete @loads where LoadId = @LoadID
	
	
	
	exec Merchandising.dashboard.SaveLastOnlineSnapshot 
	    @LoadID = @LoadId, -- int
	    @DatasourceInterfaceID = @DatasourceInterfaceID -- int
	
end




