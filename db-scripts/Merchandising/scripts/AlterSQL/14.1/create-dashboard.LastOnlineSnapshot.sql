if object_id('Merchandising.dashboard.LastOnlineSnapshot') is null
begin
	create table dashboard.LastOnlineSnapshot
	(
		BusinessUnitId int not null,
		FetchAtRequest int null,
		FetchAtDateLoaded datetime null,
		FetchCarsRequest int null,
		FetchCarsDateLoaded datetime null,
		AtDirectLoad int null,
		AtDirectDateLoaded datetime null,
		CarsDirectLoad int null,
		CarsDirectDateLoaded datetime null
	)
end

if object_id('Merchandising.dashboard.PK_LastOnlineSnapshot') is null
begin
	alter table dashboard.LastOnlineSnapshot add constraint PK_LastOnlineSnapshot primary key clustered (BusinessUnitId)
end



