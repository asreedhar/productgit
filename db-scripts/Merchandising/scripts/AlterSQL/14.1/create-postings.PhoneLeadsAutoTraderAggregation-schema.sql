

if object_id('postings.PhoneLeadsAutoTraderAggregation') is not null
	drop table postings.PhoneLeadsAutoTraderAggregation



CREATE TABLE [postings].[PhoneLeadsAutoTraderAggregation]
(
	[businessUnitId] [int] not NULL,
	[Month] [datetime] not NULL,
	NewPhoneLeads int not null,
	UsedPhoneLeads int not null
) 
GO

ALTER TABLE [postings].[PhoneLeadsAutoTraderAggregation] ADD CONSTRAINT [PK_PhoneLeadsAutoTraderAggregation] PRIMARY KEY clustered  (businessUnitId, Month) WITH (FILLFACTOR=90) 
GO

