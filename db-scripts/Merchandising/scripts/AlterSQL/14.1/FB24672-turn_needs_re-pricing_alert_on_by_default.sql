update Merchandising.settings.BucketAlertsThreshold
set Active = 1
where Bucket = 16

update Merchandising.settings.BucketAlertThresholdDefault
set Limit = 20, Active = 1
where Bucket = 16