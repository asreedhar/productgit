if not exists
(
	select * from Merchandising.settings.Environment
	where SettingName = 'VehicleOnlineStaleDataDays'
)
begin
	insert Merchandising.settings.Environment
	(
		SettingName,
		SettingValue
	)
	values
	(
		'VehicleOnlineStaleDataDays',
		9
	)
end