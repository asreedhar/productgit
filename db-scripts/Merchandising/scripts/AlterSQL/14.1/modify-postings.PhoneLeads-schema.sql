
--alter table Merchandising.postings.PhoneLeads add InventoryType tinyint not null

if object_id('postings.PhoneLeads') is not null
	drop table postings.PhoneLeads



CREATE TABLE [postings].[PhoneLeads]
(
	[Load_ID] [int] NOT NULL,
	[businessUnitId] [int] not NULL,
	[LeadDate] [datetime] not NULL,
	[CallerPhone] [varchar] (20) not NULL,
	[InventoryType] [tinyint] NOT NULL,
	[RequestId] [int] NOT NULL,
	[DateLoaded] [datetime] NOT NULL CONSTRAINT [DF_PhoneLeads_DateLoaded] DEFAULT (getdate()),
) 
GO

ALTER TABLE [postings].[PhoneLeads] ADD CONSTRAINT [PK_phoneLeads] PRIMARY KEY clustered  (businessUnitId, LeadDate, CallerPhone) WITH (FILLFACTOR=90)
GO


create nonclustered index IX_LoadID on postings.PhoneLeads (Load_ID)
go