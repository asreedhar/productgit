
declare @input table(businessUnitId int, listingSiteDealerId int, siteDesc varchar(10), destinationId int)

-- from Chad's spreadsheet in FB 24349:
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(102395, 2244558, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105788, 183707, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(100261, 182687, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(100945, 155010, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105787, 165955, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(102392, 2244648, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105213, 151793, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105214, 20232, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105218, 201788, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(104163, 180322, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(104162, 210290, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(101429, 157641, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(103269, 26747, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(103132, 156449, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(103129, 156867, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(103130, 156893, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(102175, 24055, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(102394, 209721, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105204, 3187, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105209, 156030, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105208, 88895, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105340, 201267, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(101471, 155185, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105442, 154937, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(102390, 149690, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(102388, 149691, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(102389, 183439, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105339, 81087, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(100414, 107881, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(100415, 20375, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(102899, 154948, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(103433, 4601, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(103969, 11238, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105894, 202956, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105500, 195668, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(100388, 154910, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105619, 209498, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(100416, 201601, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(100389, 109032, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(100417, 109030, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(101071, 164876, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(103479, 80423, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105800, 150225, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(102430, 109551, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105219, 82341, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(102393, 2244770, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(103131, 16096, 'cars')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(100389, 91887, 'AT')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(104485, 57275872, 'AT')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105768, 69346, 'AT')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105363, 620736, 'AT')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105356, 1328111, 'AT')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105357, 670936, 'AT')
insert @input(businessUnitId, listingSiteDealerId, siteDesc)values(105358, 94841, 'AT')

update @input
set DestinationId = 
	case 
		when siteDesc = 'cars' 
			then 1 
		when siteDesc = 'AT' 
			then 2 
		else null 
	end

/*
select 'not in DLS table: ', * 
from @input i
where not exists
(
	select *
	from Merchandising.settings.DealerListingSites dls
	where i.businessUnitID = dls.businessUnitID
	and i.destinationId = dls.destinationID
)
*/

update dls
set ListingSiteDealerId = cast(input.listingSiteDealerId as varchar)
from Merchandising.settings.DealerListingSites dls
join @input input 
	on input.businessUnitId = dls.businessUnitID
	and input.destinationId = dls.destinationID


--select * into Merchandising.settings.DealerListingSites#BackupAfter24439 from Merchandising.settings.DealerListingSites
--select * from Merchandising.settings.DealerListingSites where ListingSiteDealerId is not null
--select * from @input


