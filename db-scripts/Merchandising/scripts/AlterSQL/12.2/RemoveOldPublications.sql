USE Merchandising    
GO
SET NOCOUNT ON
DECLARE @TargetDate AS SMALLDATETIME
SET @TargetDate = DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0)

/*
Grab all Publication records that either:
	The Inventory record doesn't exist for that InventoryId
	The Inventory record exists, but is Inactive and the DeleteDt is past the UnwindDays
*/
PRINT N'Writing records to keep to a temporary table.' ;    
SELECT  DISTINCT
        a.postingID
      , a.businessUnitID
      , a.inventoryId
      , a.datePosted
      , a.destinationID
      , a.merchandisingDescription
      , a.equipmentCount
      , a.pricePosted
      , a.templateID
      , a.approvedByMemberLogin
      , a.dateRemoved
      , a.expiresOn
      , a.ExteriorColor
      , a.InteriorColor
      , a.InteriorType
      , a.MappedOptionIds
      , a.ChromeStyleId
      , a.OptionCodes
INTO    #recordsToKeep
FROM    postings.Publications a
        INNER JOIN IMT.dbo.Inventory b ON a.inventoryId = b.InventoryID
                                          AND a.businessUnitID = b.BusinessUnitID
        LEFT OUTER JOIN IMT.dbo.DealerPreference c ON b.BusinessUnitID = c.BusinessUnitID
WHERE   b.InventoryActive = 1
        OR (b.InventoryActive = 0 AND b.DeleteDt >= DATEADD(DAY, ISNULL(-UnwindDaysThreshold, -45), @TargetDate) )
		
PRINT N'Writing count of records to keep.' ;    
SELECT  COUNT(1) AS Keeping
FROM    #recordsToKeep

PRINT N'Writing record count of source table.' ;           	
SELECT  COUNT(1) AS Total
FROM    postings.Publications
    

BEGIN TRANSACTION 
PRINT N'Truncating source table.';
TRUNCATE TABLE postings.Publications
PRINT N'Remove indexes for faster load.';
DROP INDEX [NDX_Publication] ON [postings].[Publications] WITH ( ONLINE = OFF )
PRINT N'Adding back records to keep to source table with original identity values.' ;
SET IDENTITY_INSERT postings.Publications  ON
INSERT  INTO postings.Publications
        ( postingID
        , businessUnitID
        , inventoryId
        , datePosted
        , destinationID
        , merchandisingDescription
        , equipmentCount
        , pricePosted
        , templateID
        , approvedByMemberLogin
        , dateRemoved
        , expiresOn
        , ExteriorColor
        , InteriorColor
        , InteriorType
        , MappedOptionIds
        , ChromeStyleId
        , OptionCodes )
        SELECT  postingID
              , businessUnitID
              , inventoryId
              , datePosted
              , destinationID
              , merchandisingDescription
              , equipmentCount
              , pricePosted
              , templateID
              , approvedByMemberLogin
              , dateRemoved
              , expiresOn
              , ExteriorColor
              , InteriorColor
              , InteriorType
              , MappedOptionIds
              , ChromeStyleId
              , OptionCodes
        FROM    #recordsToKeep
SET IDENTITY_INSERT postings.Publications  OFF
DBCC CHECKIDENT('postings.Publications', RESEED)
PRINT N'Recreating indexes.';
CREATE CLUSTERED INDEX [NDX_Publication] ON [postings].[Publications] 
(
	[businessUnitID] ASC,
	[inventoryId] ASC
)
COMMIT TRANSACTION

DROP TABLE #recordsToKeep
DBCC SHRINKFILE (N'Merchandising_log' , 2000)
SET NOCOUNT OFF