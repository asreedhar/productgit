INSERT INTO Merchandising.dbo.ManufacturerVehicleOptionCodeWhiteList (VehicleManuFacturerID, VehicleMakeId, OptionCode)
SELECT MA.ManufacturerID, D.DivisionID, O.OptionCode
FROM VehicleCatalog.Chrome.Manufacturers MA
JOIN VehicleCatalog.Chrome.Divisions D ON D.ManufacturerID = MA.ManufacturerID AND D.CountryCode = 1
JOIN VehicleCatalog.Chrome.Models MO ON MO.DivisionID = D.DivisionID AND MO.CountryCode = 1
JOIN VehicleCatalog.Chrome.Styles S ON S.ModelID = MO.ModelID
JOIN VehicleCatalog.Chrome.Options O ON O.StyleID = S.StyleID
WHERE MA.ManufacturerID = 9 AND MA.CountryCode = 1 
AND ((O.OptionDesc Like '%Leather%') 
OR (O.OptionDesc Like '%Leatherette%')
OR (O.OptionDesc Like '%dvd%')
OR (O.OptionDesc Like '%moonroof%')
OR (O.OptionDesc Like '%camera%')
OR (O.OptionDesc Like '%navigation%')
OR (O.OptionDesc Like '%nav%')
OR (O.OptionDesc Like '%third row%')
OR (O.OptionDesc Like '%trailer%')
OR (O.OptionDesc Like '%sunroof%')
OR (O.OptionDesc Like '%sun%')
OR (O.OptionDesc Like '%moon%')
OR (O.OptionDesc Like '%bluetooth%')
OR (O.OptionDesc Like '%4x4%')
OR (O.OptionDesc Like '%hands free%')
OR (O.OptionDesc Like '%handsfree%')
OR (O.OptionDesc Like '%hands-free%')
OR (O.OptionDesc Like '%premium%')
OR (O.OptionDesc Like '%iphone%')
OR (O.OptionDesc Like '%usb%')
OR (O.OptionDesc Like '%anit-collision%')
OR (O.OptionDesc Like '%winter pkg%')
OR (O.OptionDesc Like '%winter package%')
OR (O.OptionDesc Like '%cold weather%')
OR (O.OptionDesc Like '%off-road%')
OR (O.OptionDesc Like '%offroad%')
OR (O.OptionDesc Like '%off road%')
OR (O.OptionDesc Like '%aniversary%')
OR (O.OptionDesc Like '%heated%')
OR (O.OptionDesc Like '%cooled%')
OR (O.OptionDesc Like '%memory%')
OR (O.OptionDesc Like '%entertainment%')
OR (O.OptionDesc Like '%satellite%')
OR (O.OptionDesc Like '%blind spot%')
OR (O.OptionDesc Like '%4WD%')
OR (O.OptionDesc Like '%cd changer%')
OR (O.OptionDesc Like '%6*pass* seating%')
OR (O.OptionDesc Like '%7*pass* seating%')
OR (O.OptionDesc Like '%8*pass* seating%')
OR (O.OptionDesc Like '%9*pass* seating%')
OR (O.OptionDesc Like '%11*pass* seating%')
OR (O.OptionDesc Like '%12*pass* seating%')
OR (O.OptionDesc Like '%15*pass* seating%')
OR (O.OptionDesc Like '%ambulance%')
OR (O.OptionDesc Like '%rear air condition%')
OR (O.OptionDesc Like '%AWD%')
OR (O.OptionDesc Like '%SIDE*IMPACT AIR BAGS %')
OR (O.OptionDesc Like '%3rd row%')
OR (O.OptionDesc Like '%third-row%')
OR (O.OptionDesc Like '%skid plates%')
OR (O.OptionDesc Like '%VISTA ROOF%')
OR (O.OptionDesc Like '%PARK ASSIST%')
OR (O.OptionDesc Like '%bed liner%')
OR (O.OptionDesc Like '%ADAPTIVE CRUISE CONTROL%')
OR (O.OptionDesc Like '%TOW HOOKS%')
OR (O.OptionDesc Like '%TRACTION CONTROL%')
OR (O.OptionDesc Like '%TRACTION ASSIST %')
OR (O.OptionDesc Like '%CURTAIN AIRBAG%')
OR (O.OptionDesc Like '%REMOTE START%')
OR (O.OptionDesc Like '%MP3%')
OR (O.OptionDesc Like '%GLASS ROOF%')
OR (O.OptionDesc Like '%SIDE-IMPACT AIRBAGS %')
OR (O.OptionDesc Like '%SUPER ENGINE COOLING%')
OR (O.OptionDesc Like '%ADJUSTABLE PEDALS%')
OR (O.OptionDesc Like '%ADJUSTABLE GAS & BRAKE PEDALS%')
OR (O.OptionDesc Like '%SPORT APPEARANCE%')
OR (O.OptionDesc Like '%OPEN AIR ROOF%')
OR (O.OptionDesc Like '%sport-tuned%')
OR (O.OptionDesc Like '%TOW* PKG%'))

AND O.OptionDesc NOT IN (Select Distinct(O.OptionCode)
						WHERE ((O.OptionDesc LIKE '%processing code%')
								OR (O.OptionDesc LIKE '%discount%')
								OR (O.OptionDesc LIKE '%credit%')
								OR (O.OptionDesc LIKE '%AXLE RATIO%'))
						)
GROUP BY MA.ManufacturerID, D.DivisionID, O.OptionCode
ORDER BY O.OptionCode

