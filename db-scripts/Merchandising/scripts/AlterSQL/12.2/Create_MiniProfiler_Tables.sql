
use Merchandising
go

-- Add table structure needed to support the mini-profiler
-- See .\FirstLook.NET\MasterSolution\MvcMiniProfiler\Storage\SqlServerStorage.cs

if object_id('dbo.MiniProfilers') is null
	create table dbo.MiniProfilers
	  (
		 Id                                   uniqueidentifier not null primary key,
		 Name                                 nvarchar(200) not null,
		 Started                              datetime not null,
		 MachineName                          nvarchar(100) null,
		 [User]                               nvarchar(100) null,
		 Level                                tinyint null,
		 RootTimingId                         uniqueidentifier null,
		 DurationMilliseconds                 decimal(7, 1) not null,
		 DurationMillisecondsInSql            decimal(7, 1) null,
		 HasSqlTimings                        bit not null,
		 HasDuplicateSqlTimings               bit not null,
		 HasTrivialTimings                    bit not null,
		 HasAllTrivialTimings                 bit not null,
		 TrivialDurationThresholdMilliseconds decimal(5, 1) null,
		 HasUserViewed                        bit not null
	  );

if object_id('dbo.MiniProfilerTimings') is NULL
	create table dbo.MiniProfilerTimings
	  (
		 RowId                               integer primary key identity,
		 Id                                  uniqueidentifier not null,
		 MiniProfilerId                      uniqueidentifier not null,
		 ParentTimingId                      uniqueidentifier null,
		 Name                                nvarchar(200) not null,
		 Depth                               smallint not null,
		 StartMilliseconds                   decimal(7, 1) not null,
		 DurationMilliseconds                decimal(7, 1) not null,
		 DurationWithoutChildrenMilliseconds decimal(7, 1) not null,
		 SqlTimingsDurationMilliseconds      decimal(7, 1) null,
		 IsRoot                              bit not null,
		 HasChildren                         bit not null,
		 IsTrivial                           bit not null,
		 HasSqlTimings                       bit not null,
		 HasDuplicateSqlTimings              bit not null,
		 ExecutedReaders                     smallint not null,
		 ExecutedScalars                     smallint not null,
		 ExecutedNonQueries                  smallint not null
	  );
if not exists (select * from sys.indexes 
               where object_id = object_id('dbo.MiniProfilerTimings') 
					and name = 'IX__MiniProfilerTimings_MiniProfiler')
	create index IX__MiniProfilerTimings_MiniProfiler on dbo.MiniProfilerTimings (MiniProfilerId)


if object_id('dbo.MiniProfilerSqlTimings') is null
	create table dbo.MiniProfilerSqlTimings
	  (
		 RowId                          integer primary key identity,
		 Id                             uniqueidentifier not null,
		 MiniProfilerId                 uniqueidentifier not null,
		 ParentTimingId                 uniqueidentifier not null,
		 ExecuteType                    tinyint not null,
		 StartMilliseconds              decimal(7, 1) not null,
		 DurationMilliseconds           decimal(7, 1) not null,
		 FirstFetchDurationMilliseconds decimal(7, 1) null,
		 IsDuplicate                    bit not null,
		 StackTraceSnippet              nvarchar(200) not null,
		 CommandString                  nvarchar(max) not null
	  );

if not exists (select * from sys.indexes 
               where object_id = object_id('dbo.MiniProfilerSqlTimings') 
					and name = 'IX__MiniProfilerSqlTimings_MiniProfiler')
	create index IX__MiniProfilerSqlTimings_MiniProfiler on dbo.MiniProfilerSqlTimings (MiniProfilerId)

if object_id('dbo.MiniProfilerSqlTimingParameters') is null
	create table dbo.MiniProfilerSqlTimingParameters
	  (
		 MiniProfilerId    uniqueidentifier not null,
		 ParentSqlTimingId uniqueidentifier not null,
		 Name              nvarchar(130) not null,
		 DbType            nvarchar(50) null,
		 Size              int null,
		 Value             nvarchar(max) null
	  );

if not exists (select * from sys.indexes 
               where object_id = object_id('dbo.MiniProfilerSqlTimingParameters') 
					and name = 'IX__MiniProfilerSqlTimingParameters_MiniProfiler')
	create index IX__MiniProfilerSqlTimingParameters_MiniProfiler on dbo.MiniProfilerSqlTimingParameters (MiniProfilerId)