USE Merchandising    
GO

DECLARE @TargetDate AS SMALLDATETIME
SET @TargetDate = DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0)

/*
Grab all VehicleAdScheduling records that either:
	The Inventory record doesn't exist for that InventoryId
	The Inventory record exists, but is Inactive and the DeleteDt is past the UnwindDays
*/
PRINT N'Writing records to keep to a temporary table.' ;    
SELECT  DISTINCT
        a.schedulingID
      , a.businessUnitID
      , a.inventoryId
      , a.destinationID
      , a.scheduledReleaseTime
      , a.destinationEnabled
      , a.actualReleaseTime
      , a.expiresOn
      , a.merchandisingDescription
      , a.equipmentList
      , a.listPrice
      , a.advertisementStatus
      , a.approvedByMemberLogin
      , a.approvedTimeStamp
      , a.ExteriorColor
      , a.InteriorColor
      , a.InteriorType
      , a.MappedOptionIds
      , a.footer
      , a.OptionCodes
      , a.highlightCalloutText
      , a.seoKeywords
      , a.SpecialPrice
      , a.autoRegenerate
INTO    #recordsToKeep
FROM    postings.VehicleAdScheduling a
        INNER JOIN IMT.dbo.Inventory b ON a.inventoryId = b.InventoryID
                                          AND a.businessUnitID = b.BusinessUnitID
        LEFT OUTER JOIN IMT.dbo.DealerPreference c ON b.BusinessUnitID = c.BusinessUnitID
WHERE   b.InventoryActive = 1
        OR (b.InventoryActive = 0 AND b.DeleteDt >= DATEADD(DAY, ISNULL(-UnwindDaysThreshold, -45), @TargetDate) )
		
PRINT N'Writing count of records to keep.' ;    
SELECT  COUNT(1)
FROM    #recordsToKeep

PRINT N'Writing record count of source table.' ;           	
SELECT  COUNT(1)
FROM    postings.VehicleAdScheduling
    
BEGIN TRANSACTION 
PRINT N'Truncating source table.' ;
TRUNCATE TABLE postings.VehicleAdScheduling
PRINT N'Adding back records to keep to source table with original identity values.' ;
SET IDENTITY_INSERT postings.VehicleAdScheduling  ON
INSERT  INTO postings.VehicleAdScheduling
        ( schedulingID
        , businessUnitID
        , inventoryId
        , destinationID
        , scheduledReleaseTime
        , destinationEnabled
        , actualReleaseTime
        , expiresOn
        , merchandisingDescription
        , equipmentList
        , listPrice
        , advertisementStatus
        , approvedByMemberLogin
        , approvedTimeStamp
        , ExteriorColor
        , InteriorColor
        , InteriorType
        , MappedOptionIds
        , footer
        , OptionCodes
        , highlightCalloutText
        , seoKeywords
        , SpecialPrice
        , autoRegenerate )
        SELECT  schedulingID
              , businessUnitID
              , inventoryId
              , destinationID
              , scheduledReleaseTime
              , destinationEnabled
              , actualReleaseTime
              , expiresOn
              , merchandisingDescription
              , equipmentList
              , listPrice
              , advertisementStatus
              , approvedByMemberLogin
              , approvedTimeStamp
              , ExteriorColor
              , InteriorColor
              , InteriorType
              , MappedOptionIds
              , footer
              , OptionCodes
              , highlightCalloutText
              , seoKeywords
              , SpecialPrice
              , autoRegenerate
        FROM    #recordsToKeep
SET IDENTITY_INSERT postings.VehicleAdScheduling  OFF
DBCC CHECKIDENT('postings.VehicleAdScheduling', RESEED)
COMMIT TRANSACTION         

DROP TABLE #recordsToKeep

DBCC SHRINKFILE (N'Merchandising_log' , 2000)
