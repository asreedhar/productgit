if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[DetailedEquipmentDescriptions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[DetailedEquipmentDescriptions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [builder].[DetailedEquipmentDescriptions](
	[equipmentDescriptionId] int IDENTITY(1,1),
	[optionCode] varchar(20),
	[equipmentTitle] varchar(2000),
	[equipmentDescription] varchar(2000),
	[isStandard] bit NOT NULL,
	CONSTRAINT [PK_DetailedEquipmentDescription] PRIMARY KEY NONCLUSTERED 
	(
		equipmentDescriptionId
	)
)
GO

CREATE CLUSTERED 
INDEX IX_DetailedEquipmentDescriptions__Standard_OptionCode
ON [builder].[DetailedEquipmentDescriptions] (isStandard, optionCode)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[Vehicle_DetailedEquipmentDescriptions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table [builder].[Vehicle_DetailedEquipmentDescriptions]
GO

CREATE TABLE [builder].[Vehicle_DetailedEquipmentDescriptions] (
	[mappingId] int IDENTITY(1,1),
	[businessUnitId] int,
	[modelId] int,
	[chromeStyleId] int NULL,
	[equipmentDescriptionId] int,
	CONSTRAINT [PK_Vehicle_DetailedEquipmentDescriptions] PRIMARY KEY NONCLUSTERED
	(
		mappingId
	)
)
GO

CREATE CLUSTERED
INDEX IX_Vehicle_DetailedEquipmentDescriptions__businessUnitId_modelId_equipmentDescriptionId
ON [builder].[Vehicle_DetailedEquipmentDescriptions] (businessUnitId, modelId, equipmentDescriptionId)
GO
	

ALTER TABLE vehiclecatalog.chrome.prices
DROP CONSTRAINT PK_Prices
GO

ALTER TABLE vehicleCatalog.chrome.prices
ADD CONSTRAINT PK_Prices
PRIMARY KEY NONCLUSTERED (countryCode, styleId, sequence)
GO

CREATE CLUSTERED 
INDEX IX_Prices__countrycode_styleId_optionCode
ON vehicleCatalog.chrome.prices (countryCode, styleId, optionCode)
GO
