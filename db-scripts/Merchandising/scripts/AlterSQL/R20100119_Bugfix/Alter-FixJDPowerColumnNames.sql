if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[JdPowerRatings]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [merchandising].[JdPowerRatings]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [merchandising].[JdPowerRatings](
	[Year] [int] NULL,
	[Make] [nvarchar](50) NULL,
	[Model] [nvarchar](50) NULL,
	[OverallPerformanceAndDesign] [float] NULL,
	[Performance] [float] NULL,
	[Comfort] [float] NULL,
	[FeaturesAndInstrumentPanel] [float] NULL,
	[Style] [float] NULL,
	[OverallIQS] [float] NULL,
	[OverallQualityMechanical] [float] NULL,
	[PowertrainQualityMechanical] [float] NULL,
	[BodyAndInteriorQualityMechanical] [float] NULL,
	[FeaturesAndAccessoriesQualityMechanical] [float] NULL,
	[OverallQualityDesign] [float] NULL,
	[PowertrainQualityDesign] [float] NULL,
	[BodyAndInteriorQualityDesign] [float] NULL,
	[FeaturesAndAccessoriesQualityDesign] [float] NULL,
	[OverallDependability] [float] NULL,
	[PowertrainDependability] [float] NULL,
	[BodyAndInteriorDependability] [float] NULL,
	[FeatureAndAccessoryDependability] [float] NULL
) 
GO
SET ANSI_PADDING OFF
GO

CREATE CLUSTERED 
INDEX IX_JdPowerRatings
ON merchandising.JdPowerratings ([Year],Make,Model) 

ALTER TABLE merchandising.JdPowerRatings 
ADD PredictedReliability float NULL
GO

ALTER TABLE merchandising.JdPowerRatings 
ADD AutoRatingsPageUrl varchar(200) NULL
GO
