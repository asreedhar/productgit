 --Hyundai will be 18
 insert into settings.CpoGroups ([name]) VALUES ('Hyundai')
 insert into settings.CpoGroups ([name]) VALUES ('Mitsubishi')
 insert into settings.CpoGroups ([name]) VALUES ('Bentley')
 insert into settings.CpoGroups ([name]) VALUES ('Jaguar')
 insert into settings.CpoGroups ([name]) VALUES ('Ferrari')
 insert into settings.CpoGroups ([name]) VALUES ('Maserati')
 insert into settings.CpoGroups ([name]) VALUES ('Porsche')
 
 
 --hyundai and mitsubishi already existed
 insert into settings.cpoCertifieds (cpoGroupId, certificationTypeId) VALUES (18, 13)
 insert into settings.cpoCertifieds (cpoGroupId, certificationTypeId) VALUES (19, 22)
 
 --create certifieds for each...
 insert into builder.manufacturerCertifications ([name]) VALUES ('Bentley')  --34
 insert into builder.manufacturerCertifications ([name]) VALUES ('Jaguar')   --35
 insert into builder.manufacturerCertifications ([name]) VALUES ('Ferrari')  --36
 insert into builder.manufacturerCertifications ([name]) VALUES ('Maserati') --37
 insert into builder.manufacturerCertifications ([name]) VALUES ('Porsche')  --38
 
 --link just created groups and certifieds
 insert into settings.cpoCertifieds (cpoGroupId, certificationTypeId) VALUES (20, 34)
 insert into settings.cpoCertifieds (cpoGroupId, certificationTypeId) VALUES (21, 35)
 insert into settings.cpoCertifieds (cpoGroupId, certificationTypeId) VALUES (22, 36)
 insert into settings.cpoCertifieds (cpoGroupId, certificationTypeId) VALUES (23, 37)
 insert into settings.cpoCertifieds (cpoGroupId, certificationTypeId) VALUES (24, 38)
 
--hyundai...missing for some reason from original set of data
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,13,5,'6-year/75,000-Mile Bumper-to-Bumper Warranty')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,13,4,'Limited powertrain warranty')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,13,14,'120-Point Inspection and Reconditioning')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,13,20,'Trip-interruption services')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,13,17,'24/7 Roadside Assistance')


--bentley
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,34,5,'1 Year/Unliminted Mileage-Month/12,000-Mile Bumper-to-Bumper Warranty')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,34,8,'Option to extend warranty by either 12 or 24 months for cars up to 7 years old')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,34,14,'154-point inspection')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,34,13,'Vehicle History Report')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,34,17,'24/7 roadside assistance')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,34,20,'Trip-interruption services')

--jaguar
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,35,5,'Balance of original 4-year/50,000-mile warranty')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,35,6,'6-year/100,000-mile Select Edition premium warranty')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,35,14,'140-Point Inspection and Reconditioning')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,35,13,'Vehicle History Report')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,35,20,'Trip-interruption services')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,35,17,'24/7 roadside assistance')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,35,10,'No Deductible')

--Ferrari
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,36,6,'Powertrain Plus Service Contract covering engine, powertrain, major electrical and other components')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,36,14,'101-Point Inspection and Reconditioning')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,36,13,'Vehicle History Report')


--Maserati
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,37,5,'1-year/90,000-mile coverage')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,37,14,'120-Point Inspection')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,37,13,'Certified Maseratis have been maintained by Maserati''s authorized dealer network.')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,37,17,'24/7 roadside assistance')

 
 --Porsche
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,38,3,'Balance of original 4-year/50,000-mile warranty')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,38,6,'Additional 2-year/50,000-mile warranty, up to 6 years/100,000 miles total')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,38,14,'Over 100 Point Inspection and Reconditioning')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,38,20,'Trip-interruption services')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,38,17,'24/7 Roadside Assistance')
INSERT INTO builder.certificationFeatures (businessUnitId,certificationTypeId, certificationFeatureTypeId, description) 
	VALUES (100150,38,10,'No Deductible, transferrable to subsequent owners')

 
 