DECLARE @newRules TABLE
(
	ruleId	int, 
	textToReplace	varchar(150),
	textToReplaceWith	varchar(150)
)

INSERT INTO @newRules
		VALUES(15, 'HARMAN/KARDON', 'harman/kardon')

INSERT INTO @newRules
	VALUES(16, 'HARMAN KARDON', 'harman kardon')

INSERT INTO @newRules
	VALUES(17, 'HARMAN-KARDON', 'harman-kardon')

INSERT INTO @newRules
	VALUES(18, 'harmon/kardon', 'harman/kardon')

DECLARE @ruleID int, @textToReplace varchar(150), @textToReplaceWith varchar(150)
	
DECLARE packageRule CURSOR FOR
	SELECT ruleId, textToReplace, textToReplaceWith
		FROM @newRules
		
OPEN packageRule
FETCH NEXT FROM packageRule
	INTO @ruleID, @textToReplace, @textToReplaceWith

WHILE @@FETCH_STATUS = 0
	BEGIN
	
		-- delete this rule and mappings if it already exists in case changes were made to the replacement text
		DELETE
			FROM Merchandising.templates.OptionCodeRuleMappings
			WHERE ruleID = @ruleID

		DELETE
			FROM Merchandising.templates.optionCodeRules
			WHERE ruleID = @ruleID
			
		-- insert rule 
		INSERT INTO Merchandising.templates.optioncoderules
			VALUES(@ruleID, @textToReplace, @textToReplaceWith)
		
		
		-- insert mappings
		INSERT INTO Merchandising.templates.OptionCodeRuleMappings
			SELECT DISTINCT OptionCode, 100150, @ruleID, 0
				FROM VehicleCatalog.Chrome.Options
				WHERE CountryCode = 1
				AND OptionDesc COLLATE Latin1_General_CS_AS LIKE  '%' + @textToReplace + '%'
				ORDER BY OptionCode

	
		FETCH NEXT FROM packageRule
			INTO @ruleID, @textToReplace, @textToReplaceWith
	END

CLOSE packageRule
DEALLOCATE packageRule
	
	





