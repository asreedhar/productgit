	BEGIN TRY

		DECLARE @whiteList TABLE 
		(
			manufactureName varchar(50)	NOT NULL,
			optionCode VARCHAR(20) NOT NULL,
			caseID	varchar(20)
		)

		INSERT INTO @whiteList	VALUES('General Motors', 'FHV', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'A95', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'PYS', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'AZ3', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'PCM', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'VBP', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'VQT', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'PYS', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'VXH', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'VPB', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', '5JY', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'VZZ', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'PYV', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'PDC', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'VXJ', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'UQA', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'CGN', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'VBN', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'G80', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'VZX', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'TGK', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'AG1', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'Z71', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'VQG', '28268a') 
		INSERT INTO @whiteList	VALUES('General Motors', 'K76', '28268a') 
		

		INSERT INTO @whiteList	VALUES('General Motors', 'PCY', '28268b') 
		INSERT INTO @whiteList	VALUES('General Motors', 'GBN', '28268b') 
		INSERT INTO @whiteList	VALUES('General Motors', 'PCP', '28268b') 
		INSERT INTO @whiteList	VALUES('General Motors', 'VAT', '28268b') 
		INSERT INTO @whiteList	VALUES('General Motors', 'GBE', '28268b') 
		INSERT INTO @whiteList	VALUES('General Motors', 'SB2', '28268b') 
		INSERT INTO @whiteList	VALUES('General Motors', 'VEB', '28268b') 
		INSERT INTO @whiteList	VALUES('General Motors', 'VKU', '28268b') 
		INSERT INTO @whiteList	VALUES('General Motors', 'RVW', '28268b') 
		INSERT INTO @whiteList	VALUES('General Motors', 'SFE', '28268b') 


		-- make sure all of the manufactures can be found by name
		if EXISTS (
					SELECT *
						FROM @whiteList whiteList
						WHERE NOT EXISTS (SELECT *
											FROM VehicleCatalog.Chrome.Manufacturers Manufacturers
											WHERE whiteList.manufactureName = Manufacturers.ManufacturerName)
				)
				begin
					RAISERROR('One of the manufactures name in the white list alter is not valid', 16, 1)
				end


		-- insert into white list
		INSERT INTO manufacturerVehicleOptionCodeWhiteList
			(VehicleManufacturerId, VehicleMakeId, OptionCode)
			SELECT DISTINCT Manufacturers.ManufacturerID, YearMakeModelStyle.DivisionID, options.OptionCode
				FROM vehiclecatalog.chrome.options options
				INNER JOIN VehicleCatalog.Chrome.YearMakeModelStyle YearMakeModelStyle ON options.StyleID = YearMakeModelStyle.ChromeStyleID
																								AND options.CountryCode = YearMakeModelStyle.CountryCode
				INNER JOIN VehicleCatalog.Chrome.Divisions Divisions ON YearMakeModelStyle.DivisionID = Divisions.DivisionID
																			AND YearMakeModelStyle.CountryCode = Divisions.CountryCode
				INNER JOIN VehicleCatalog.Chrome.Manufacturers Manufacturers ON divisions.ManufacturerID = Manufacturers.ManufacturerID
																				AND divisions.CountryCode = Manufacturers.CountryCode
													
				INNER JOIN @whiteList whiteList ON options.OptionCode = whiteList.optionCode
													AND Manufacturers.ManufacturerName = whiteList.manufactureName
				WHERE options.CountryCode = 1
				AND NOT EXISTS(SELECT manufacturerVehicleOptionCodeWhiteList.OptionCode 
									FROM Merchandising.dbo.ManufacturerVehicleOptionCodeWhiteList manufacturerVehicleOptionCodeWhiteList 
									WHERE manufacturerVehicleOptionCodeWhiteList.VehicleManufacturerId = Manufacturers.ManufacturerID
									--AND manufacturerVehicleOptionCodeWhiteList.VehicleMakeId = yearMakeModelStyle.DivisionID -- doesn't matter for white list lookup
									AND ManufacturerVehicleOptionCodeWhiteList.OptionCode = options.OptionCode
									)

		/*
		-- good code to test alter			
		SELECT DISTINCT Manufacturers.ManufacturerID, YearMakeModelStyle.DivisionID, options.OptionCode, manufactureName, divisions.DivisionName, caseID
			FROM vehiclecatalog.chrome.options options
			INNER JOIN VehicleCatalog.Chrome.YearMakeModelStyle YearMakeModelStyle ON options.StyleID = YearMakeModelStyle.ChromeStyleID
																							AND options.CountryCode = YearMakeModelStyle.CountryCode
			INNER JOIN VehicleCatalog.Chrome.Divisions Divisions ON YearMakeModelStyle.DivisionID = Divisions.DivisionID
																		AND YearMakeModelStyle.CountryCode = Divisions.CountryCode
			INNER JOIN VehicleCatalog.Chrome.Manufacturers Manufacturers ON divisions.ManufacturerID = Manufacturers.ManufacturerID
																			AND divisions.CountryCode = Manufacturers.CountryCode
												
			INNER JOIN @whiteList whiteList ON options.OptionCode = whiteList.optionCode
												AND Manufacturers.ManufacturerName = whiteList.manufactureName
			WHERE options.CountryCode = 1
			AND NOT EXISTS(SELECT manufacturerVehicleOptionCodeWhiteList.OptionCode 
								FROM Merchandising.dbo.ManufacturerVehicleOptionCodeWhiteList manufacturerVehicleOptionCodeWhiteList 
								WHERE manufacturerVehicleOptionCodeWhiteList.VehicleManufacturerId = Manufacturers.ManufacturerID
								--AND manufacturerVehicleOptionCodeWhiteList.VehicleMakeId = yearMakeModelStyle.DivisionID -- doesn't matter for white list lookup
								AND ManufacturerVehicleOptionCodeWhiteList.OptionCode = options.OptionCode
								)
			ORDER BY options.OptionCode, manufactureName, divisions.DivisionName
		*/

	
	END TRY
	BEGIN CATCH

			DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE();

			-- Use RAISERROR inside the CATCH block to return error
			-- information about the original error that caused
			-- execution to jump to the CATCH block.
			RAISERROR (@ErrorMessage, -- Message text.
					   @ErrorSeverity, -- Severity.
					   @ErrorState -- State.
					   );
		
	END CATCH
