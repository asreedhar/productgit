ALTER TABLE settings.EdtDestinations
ADD acceptsOptimalFormat BIT NOT NULL CONSTRAINT [DF_Settings_EdtDestinations_acceptsOptimalFormat] DEFAULT(0)
GO

UPDATE settings.EdtDestinations
SET acceptsOptimalFormat = 1 WHERE destinationID = 2 -- AutoTrader.com

ALTER TABLE settings.DealerListingSites
ADD sendOptimalFormat BIT NOT NULL CONSTRAINT [DF_Settings_DealerListingSites_sendOptimalFormat] DEFAULT(0)