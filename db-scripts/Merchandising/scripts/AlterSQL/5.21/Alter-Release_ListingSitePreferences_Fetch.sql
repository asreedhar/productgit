USE [Merchandising]
GO
/****** Object:  StoredProcedure [Release].[ListingSitePreferences#Fetch]    Script Date: 12/07/2010 14:46:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/10/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Release.ListingSitePreferences_Fetch.sql,v 1.1 2010/08/04 19:13:43 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * retrieve listing site preferences for dealer
 * 
 * 
 * Parameters
 * ----------
 * 
 *
 * should be ignored by the rule
 * @BusinessUnitId - id of business unit
 * @DestinationId - id of destination --> NULL if you want all prefs
 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

ALTER PROCEDURE [Release].[ListingSitePreferences#Fetch]
	-- Add the parameters for the stored procedure here
	
	@BusinessUnitId int,
	@DestinationId int = NULL,
	@OnlyActiveReleaseSites bit = 0
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
	dls.destinationId,
	ed.description,
	pushFrequency,
	sendPhotos,
	sendOptions, 
	sendDescription, 
	sendPricing, 
	sendVideos, 
	sendNewCars, 
	sendUsedCars,
	activeFlag,
	username,
	password,
	CAST(releaseEnabled as bit) as releaseEnabled,
	CAST(collectionEnabled as bit) as collectionEnabled,
	CAST(acceptsOptimalFormat as bit) as acceptsOptimalFormat,
	isnull(listingSiteDealerId,'') as listingSiteDealerId,
	ed.canReleaseTo,
	ed.canCollectMetricsFrom
	
	FROM settings.DealerListingSites dls
	JOIN settings.EdtDestinations ed
		ON ed.destinationId = dls.destinationId
	WHERE 
		(
			@DestinationId IS NULL 
			OR dls.destinationId = @DestinationId
		)
		AND dls.businessUnitId = @BusinessUnitId
		AND 
		(
			@OnlyActiveReleaseSites = 0 
			OR (
				releaseEnabled = 1
                and activeFlag = 1
            )
        )
					
				
END 
