-- Set the model business unit, this BU will be used to update two tables:
-- 1.  settings.MerchandisingDescriptionPreferences
-- 2.  settings.EquipmentDisplayRankings
DECLARE @modelBusinessUnitId INT
SET @modelBusinessUnitId = 100506 -- Penske Honda (Production)

-- We will not update any businessUnits that are inserted into this table
DECLARE @excludeBusinessUnits TABLE(BusinessUnitId INT)
INSERT INTO @excludeBusinessUnits( BusinessUnitId )
SELECT bu.businessUnitID
FROM imt.dbo.BusinessUnit bu
JOIN imt.dbo.BusinessUnitRelationship br ON bu.BusinessUnitId = br.BusinessUnitID
WHERE br.ParentID = 101676 --Luther Auto Group (leave them out - they have special needs regarding ad-length)

/*

Update the settings.MerchandisingDescriptionPreferences table

*/
DECLARE @modelMaxOptionsTier1 INT
DECLARE @modelMaxOptionsTier2 INT

SELECT 
@modelMaxOptionsTier1 = maxOptionsTier1,
@modelMaxOptionsTier2 = maxOptionsTier2
FROM settings.MerchandisingDescriptionPreferences
WHERE BusinessUnitId = @modelBusinessUnitId 

-- do the update
UPDATE pref
SET maxOptionsTier1 = @modelMaxOptionsTier1,
maxOptionsTier2 = @modelMaxOptionsTier2
FROM settings.MerchandisingDescriptionPreferences pref
LEFT JOIN @excludeBusinessUnits exclude ON exclude.BusinessUnitId = pref.businessUnitId
WHERE exclude.BusinessUnitId IS NULL



/*

Now Update the settings.EquipmentDisplayRankings table

*/

-- Load the model EDR table
DECLARE @modelEDR TABLE( categoryId INT, typeId INT, categoryDesc VARCHAR(80), displayPriority INT, tierNumber INT, displayDescription VARCHAR(80) )
INSERT INTO @modelEDR( categoryId, typeId, categoryDesc, displayPriority, tierNumber, displayDescription )
SELECT categoryID, typeID, categoryDesc, displayPriority, tierNumber, displayDescription
FROM settings.EquipmentDisplayRankings 
WHERE businessUnitID = @modelBusinessUnitId

-- do the update
UPDATE edr
SET displayPriority = model.displayPriority,
tierNumber = model.tierNumber,
displayDescription = model.displayDescription
FROM settings.EquipmentDisplayRankings edr
JOIN @modelEDR model ON model.categoryId = edr.categoryID AND model.typeId = edr.typeID AND model.categoryDesc = edr.categoryDesc
LEFT JOIN @excludeBusinessUnits exclude ON exclude.BusinessUnitId = edr.businessUnitID
WHERE exclude.BusinessUnitId IS NULL
AND ( edr.displayPriority != model.displayPriority 
    OR edr.tierNumber != model.tierNumber 
    OR edr.displayDescription != model.displayDescription) -- only update those that need to change

