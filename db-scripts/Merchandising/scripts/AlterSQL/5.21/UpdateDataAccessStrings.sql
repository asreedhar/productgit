-- Update MerchandisingPoints that are list-based.

UPDATE templates.MerchandisingPoints
SET dataAccessString = '$base.SafetyOptions$'
WHERE pointID = 1 AND BusinessUnitID = 100150

UPDATE templates.MerchandisingPoints
SET dataAccessString = '$base.LuxuryOptions$'
WHERE pointID = 2 AND BusinessUnitID = 100150

UPDATE templates.MerchandisingPoints
SET dataAccessString = '$base.ConvenienceOptions$'
WHERE pointID = 3 AND BusinessUnitID = 100150

UPDATE templates.MerchandisingPoints
SET dataAccessString = '$base.TruckOptions$'
WHERE pointID = 4 AND BusinessUnitID = 100150

UPDATE templates.MerchandisingPoints
SET dataAccessString = '$base.Tier1Equipment$'
WHERE pointID = 29 AND BusinessUnitID = 100150

UPDATE templates.MerchandisingPoints
SET dataAccessString = '$base.Tier2Equipment$'
WHERE pointID = 30 AND BusinessUnitID = 100150

UPDATE templates.MerchandisingPoints
SET dataAccessString = '$base.MajorEquipmentOfInterest$'
WHERE pointID = 46 AND BusinessUnitID = 100150

UPDATE templates.MerchandisingPoints
SET dataAccessString = '$base.SportyOptions$'
WHERE pointID = 63 AND BusinessUnitID = 100150

UPDATE templates.MerchandisingPoints
SET dataAccessString = '$base.ValuedOptions$'
WHERE pointID = 64 AND BusinessUnitID = 100150

UPDATE templates.MerchandisingPoints
SET dataAccessString = '$base.HighlightedPackages$'
WHERE pointID = 67 AND BusinessUnitID = 100150

UPDATE templates.MerchandisingPoints
SET dataAccessString = '$base.ThemedEquipment$'
WHERE pointID = 75 AND BusinessUnitID = 100150

UPDATE templates.MerchandisingPoints
SET dataAccessString = '$base.Trim$'
WHERE pointID = 76 AND BusinessUnitID = 100150

UPDATE templates.MerchandisingPoints
SET dataAccessString = '$base.FuelHwyCity$'
WHERE pointID = 13 AND BusinessUnitID = 100150
