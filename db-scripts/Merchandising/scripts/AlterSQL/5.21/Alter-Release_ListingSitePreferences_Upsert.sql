USE [Merchandising]
GO
/****** Object:  StoredProcedure [Release].[ListingSitePreferences#Upsert]    Script Date: 12/07/2010 14:46:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <11/10/08>
-- =============================================
/* --------------------------------------------------------------------
 * 
 * $Id: Release.ListingSitePreferences_Upsert.sql,v 1.1 2010/08/04 19:13:43 bfung Exp $
 * 
 * Summary
 * -------
 * 
 * retrieve listing site preferences for dealer
 * 
 * 
 * Parameters
 * ----------
 * 
 *
 * should be ignored by the rule
 * @BusinessUnitId - id of business unit
 * @DestinationId - id of destination --> NULL if you want all prefs
 
 * 
 * Exceptions
 * ----------
 * 
 *
 * -------------------------------------------------------------------- */

ALTER PROCEDURE [Release].[ListingSitePreferences#Upsert]
	-- Add the parameters for the stored procedure here
	
	@BusinessUnitId int,
	@DestinationId int,
	@ActiveFlag bit = 1, 
	@PushFrequency int = 24, 
	@SendPhotos bit = 1, 
	@SendOptions bit = 1, 
	@SendDescription bit = 1, 
	@SendPricing bit = 0,
	@SendVideos bit = 1,
	@SendNewCars bit = 1,
	@SendUsedCars bit = 1,
	@Username varchar(50) = NULL,
	@Password varchar(50) = NULL,
	@ReleaseEnabled bit = NULL,
	@CollectionEnabled bit = NULL,
	@ListingSiteDealerId varchar(30) = NULL,
	@NewDestinationId int = NULL, --only required for updates that are changing the destinationid
	@SendOptimalFormat bit = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF EXISTS(SELECT 1 FROM settings.dealerListingSites WHERE businessUnitId = @BusinessUnitID AND destinationId = @DestinationId)
	BEGIN
		
		if (@NewDestinationID IS NULL)
		BEGIN
			select @NewDestinationID = destinationId
			from settings.dealerListingSites 
			WHERE businessUnitId = @BusinessUnitID AND destinationId = @DestinationId
		END
		
		UPDATE dls
		SET 			
			destinationID = @NewDestinationId, 
			activeFlag = @ActiveFlag, 
			pushFrequency = @PushFrequency, 
			sendPhotos = @SendPhotos, 
			sendOptions = @SendOptions, 
			sendDescription = @SendDescription, 
			sendPricing = @SendPricing, 
			sendVideos = @SendVideos, 
			sendNewCars = @SendNewCars, 
			sendUsedCars = @SendUsedCars,
			listingSiteDealerId = @ListingSiteDealerId,
			releaseEnabled = case when @ReleaseEnabled IS NULL then dls.releaseEnabled
								  when canReleaseTo = 0 then 0
								  else @ReleaseEnabled end, 
			collectionEnabled = case 
									when @CollectionEnabled IS NULL then dls.collectionEnabled
									when canCollectMetricsFrom = 0 then 0
									else @CollectionEnabled end,
			username = case when @Username IS NULL then dls.username else @Username end,
			password = case when @Password IS NULL then dls.password else @Password end,
			sendOptimalFormat = @SendOptimalFormat
		FROM settings.dealerListingSites dls
			JOIN settings.edtDestinations ed
			ON ed.destinationId = @NewDestinationId
		WHERE 
			(businessUnitID = @BusinessUnitID) 
			AND (dls.destinationID = @DestinationID)
	END
	ELSE
	BEGIN
		INSERT INTO settings.DealerListingSites 
		(
			businessUnitID, 
			destinationID,
			activeFlag, 
			pushFrequency, 
			sendPhotos, 
			sendOptions, 
			sendDescription, 
			sendPricing,
			sendVideos,
			sendNewCars,
			sendUsedCars, 
			username,
			[password],
			listingSiteDealerId,
			releaseEnabled,
			collectionEnabled,
			sendOptimalFormat
		) 
		SELECT 
		
			@BusinessUnitID, 
			@DestinationID,
			@ActiveFlag, 
			@PushFrequency, 
			@SendPhotos, 
			@SendOptions, 
			@SendDescription, 
			@SendPricing,
			@SendVideos,
			@SendNewCars,
			@SendUsedCars, 	
			ISNULL(@Username,''),
			ISNULL(@Password,''),
			@ListingSiteDealerId,
			case when @ReleaseEnabled IS NULL then canReleaseTo else @ReleaseEnabled end as releaseEnabled, 
			case when @CollectionEnabled IS NULL then canCollectMetricsFrom else @CollectionEnabled end as collectionEnabled,
			@SendOptimalFormat
		FROM settings.EdtDestinations 
		WHERE destinationId = @DestinationId
	END	
				
END
