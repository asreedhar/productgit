
-- create a table to hold the model templates that we will use to recreate the dealer templates
DECLARE @modelTemplates TABLE (modelTemplateId int, templateName varchar(50))

-- load the model templates
INSERT INTO @modelTemplates( modelTemplateId, templateName )
SELECT TemplateId, [Name]
FROM Merchandising.templates.AdTemplates
WHERE businessUnitID = 100150 -- FirstLook
AND name IN 
('Luxury/Key Features Enhanced+',
'Certified Enhanced+',
'Aggressive Pricing Enhanced+',
'Cash Car Enhanced+',
'Family Enhanced+',
'New Car - Risk Reduction',
'New Car - Key Differentiators',
'New Car - Affordability',
'Family Enhanced',
'Cash Car Enhanced',
'Aggressive Pricing Enhanced',
'Certified Enhanced',
'Luxury/Key Features Enhanced')

-- make sure we don't have duplicate models
IF EXISTS (
	SELECT templateName
	FROM @modelTemplates 
	GROUP BY templateName
	HAVING COUNT(templateName) > 1
)
RAISERROR('Not ready to run the rest of the script - dupes found.', 16, 1)

-- create a table to hold each template that needs updating (each row will be an item to fix)
DECLARE @businessUnitTemplate TABLE(btidx INT IDENTITY(1,1), businessUnitId INT, businessUnit VARCHAR(40), dealerTemplateId INT, templateName VARCHAR(50), modelTemplateId int)
-- load the dealer templates to change
INSERT INTO @businessUnitTemplate( businessUnitId, businessUnit, dealerTemplateId, templateName, modelTemplateId )
SELECT AT.businessUnitID, bu.BusinessUnit, AT.templateID, AT.[name], mt.modelTemplateId
FROM Merchandising.templates.AdTemplates AT
JOIN @modelTemplates mt ON AT.name = mt.templateName	-- get our model by matching 'template name'
JOIN imt.dbo.BusinessUnit bu ON bu.BusinessUnitId = AT.businessUnitID
JOIN imt.dbo.BusinessUnitRelationship br ON AT.BusinessUnitId = br.BusinessUnitID
WHERE br.ParentID != 101676 --Luther Auto Group (leave them out for some reason, per JS)
AND AT.BusinessUnitID != 100150	-- FirstLook (we do not want to recreate our models)
ORDER BY businessUnitID, at.name


/*  sanity check  */
IF EXISTS (
    SELECT * 
    FROM @businessUnitTemplate but
    JOIN Merchandising.templates.AdTemplates at ON at.templateID = but.modelTemplateId
    WHERE at.name != but.templateName
)
RAISERROR('model template mismatch - something is wrong.', 16,1)

DECLARE @insId INT -- our new templateId

-- set up some loop variables
DECLARE @i INT
DECLARE @max INT
-- initialize our loop variables
SELECT @i = 1, @max = MAX(btidx) 
FROM @businessUnitTemplate

-- set up some variables to use inside the loop for reporting (printing)
DECLARE @currBusinessUnitId INT, @currDealerTemplateId INT, @currModelTemplateId INT 
DECLARE @currBusinessUnit VARCHAR(40), @currTemplateName VARCHAR(50)

-- create the new templates
WHILE (@i <= @max)
BEGIN

    -- populate the current values we're working with
    SELECT 
    @currBusinessUnitId = businessUnitId, 
    @currBusinessUnit = businessUnit, 
    @currDealerTemplateId = dealerTemplateId, 
    @currTemplateName = templateName, 
    @currModelTemplateId = modelTemplateId
    FROM @businessUnitTemplate 
    WHERE btidx = @i

    -- log it
    PRINT CAST(@i AS VARCHAR)+ ' of ' + CAST(@max AS VARCHAR) 
    + ' executing fix for: BusinessUnitId ' + CAST(@currBusinessUnitId AS VARCHAR) 
    + ', ' + @currBusinessUnit + ' for TemplateId ' 
    + CAST(@currDealerTemplateId AS VARCHAR) + ', ' + @currTemplateName
    + ' - using model templateId ' + CAST(@currModelTemplateId AS VARCHAR)

    -- delete the old adTemplateItems    
    DELETE FROM Merchandising.templates.AdTemplateItems
    WHERE templateID = @currDealerTemplateId

    -- add the new adTemplateItems from the model
    INSERT INTO [templates].[AdTemplateItems]
    (templateID, blurbId, [sequence],priority, required, parentID)
    SELECT @currDealerTemplateId, blurbId, [sequence],priority,required,parentID
    FROM [templates].[AdTemplateItems]
    WHERE templateId = @currModelTemplateId

    -- increment our counter
    SET @i = @i + 1
    
END





