
-- Rename the existing user type because a proc relies on it's existence
exec sp_rename @objname='TimeToMarketTableType', @newname= 'TimeToMarketTableType_bak', @objtype='USERDATATYPE'
GO

-- Create the new user type with additional columns
/****** Object:  UserDefinedTableType [dbo].[TimeToMarketTableType]    Script Date: 02/26/2014 10:12:21 ******/
CREATE TYPE [dbo].[TimeToMarketTableType] AS TABLE(
	[BusinessUnitId] [int] NULL,
	[InventoryId] [int] NULL,
	[VIN] [varchar](max) NULL,
	[GIDFirstDate] [datetime] NULL,
	[PhotosFirstDate] [datetime] NULL,
	[DescriptionFirstDate] [datetime] NULL,
	[PriceFirstDate] [datetime] NULL,
	[AdApprovalFirstDate] [datetime] NULL,
	[CompleteAdFirstDate] [datetime] NULL,
	[MinimumPhotosFirstDate] [datetime] NULL,
	[MAXMinimumPhotosFirstDate] [datetime] NULL,
	[MAXPhotosFirstDate] [datetime] NULL,
	[MAXPriceFirstDate] [datetime] NULL,
	[MAXDescriptionFirstDate] [datetime] NULL
)
GO
GRANT EXECUTE ON TYPE::dbo.TimeToMarketTableType TO MerchandisingUser
GO

-- call refresh module to force the proc to point to the new table type
sp_refreshsqlmodule '[dashboard].[TimeToMarket#UpsertData]'
GO

-- Drop the backed up table type
IF EXISTS (SELECT * FROM sys.types WHERE name = 'TimeToMarketTableType_bak' AND is_table_type = 1)
	DROP TYPE [dbo].[TimeToMarketTableType_bak]
GO
