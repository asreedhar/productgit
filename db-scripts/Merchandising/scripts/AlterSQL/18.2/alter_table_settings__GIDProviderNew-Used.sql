-- if column default ... remove it
if EXISTS (SELECT * 
				FROM sysobjects
				WHERE TYPE = 'D'
				AND NAME = 'DF_merchandising_GIDProviderNew')
	begin
		ALTER TABLE settings.merchandising
			DROP CONSTRAINT DF_merchandising_GIDProviderNew
	end
	
if EXISTS (SELECT * 
				FROM sysobjects
				WHERE TYPE = 'D'
				AND NAME = 'DF_merchandising_GIDProviderUsed')
	begin
		ALTER TABLE settings.merchandising
			DROP CONSTRAINT DF_merchandising_GIDProviderUsed
	end

-- if column exists ... remove it
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'GIDProviderNew'
				AND object_id = object_id('settings.merchandising'))
	begin
		ALTER TABLE settings.merchandising
			DROP COLUMN GIDProviderNew
	end

if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'GIDProviderUsed'
				AND object_id = object_id('settings.merchandising'))
	begin
		ALTER TABLE settings.merchandising
			DROP COLUMN GIDProviderUsed
	end

-- add column, FB:29149
ALTER TABLE settings.merchandising
	ADD 
		GIDProviderNew tinyint NOT NULL CONSTRAINT DF_merchandising_GIDProviderNew DEFAULT 0,
		GIDProviderUsed tinyint NOT NULL CONSTRAINT DF_merchandising_GIDProviderUsed DEFAULT 0
GO

