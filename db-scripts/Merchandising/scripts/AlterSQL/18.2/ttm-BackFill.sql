-- Backfill the TimeToMarket/TimeToMax dates with existing data
UPDATE Merchandising.dashboard.TimeToMarketTracking
SET
	MAXDescriptionFirstDate = DescriptionFirstDate,
	MAXPhotosFirstDate = PhotosFirstDate,
	MAXPriceFirstDate = PriceFirstDate,
	MAXMinimumPhotosFirstDate = MinimumPhotosFirstDate
