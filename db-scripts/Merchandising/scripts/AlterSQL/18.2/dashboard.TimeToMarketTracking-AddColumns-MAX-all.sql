IF NOT EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'MAXMinimumPhotosFirstDate' AND Object_ID = Object_ID(N'dashboard.TimeToMarketTracking'))
BEGIN    
ALTER TABLE [dashboard].[TimeToMarketTracking] 
	ADD MAXMinimumPhotosFirstDate DateTime NULL
END

IF NOT EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'MAXPhotosFirstDate' AND Object_ID = Object_ID(N'dashboard.TimeToMarketTracking'))
BEGIN    
ALTER TABLE [dashboard].[TimeToMarketTracking] 
	ADD MAXPhotosFirstDate DateTime NULL
END

IF NOT EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'MAXPriceFirstDate' AND Object_ID = Object_ID(N'dashboard.TimeToMarketTracking'))
BEGIN    
ALTER TABLE [dashboard].[TimeToMarketTracking] 
	ADD MAXPriceFirstDate DateTime NULL
END

IF NOT EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'MAXDescriptionFirstDate' AND Object_ID = Object_ID(N'dashboard.TimeToMarketTracking'))
BEGIN    
ALTER TABLE [dashboard].[TimeToMarketTracking] 
	ADD MAXDescriptionFirstDate DateTime NULL
END


GO
