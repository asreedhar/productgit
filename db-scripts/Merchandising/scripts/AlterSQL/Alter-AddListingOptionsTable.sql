if exists (select * from dbo.sysobjects where id = object_id(N'[Merchandising].[ListingOptions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [Merchandising].[ListingOptions]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Merchandising].[ListingOptions](
	[inventoryId] [int] NOT NULL,
	[optionId] [int] NOT NULL,
	
 CONSTRAINT [PK_VehicleOptions] PRIMARY KEY CLUSTERED 
(
	[vehicleOptionID] ASC
)
)

GO
SET ANSI_PADDING OFF