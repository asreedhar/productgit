if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[NewEquipmentMapping]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[NewEquipmentMapping]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [builder].[NewEquipmentMapping](
	[inventoryID] int NOT NULL,
	[businessUnitID] int NOT NULL,
	[EngineTypeCategoryID] int NOT NULL,
	[TransmissionTypeCategoryID] int NOT NULL,
	[DriveTrainCategoryID] int NOT NULL,
	[FuelCategoryID] int NOT NULL,
	[createdBy] varchar(50) NULL,
	[createdOn] datetime NULL,
	[updatedBy] varchar(50) NULL,
	[updatedOn] datetime NULL,
	CONSTRAINT PK_NewEquipmentMapping PRIMARY KEY (businessUnitID, inventoryID)
)

GO
