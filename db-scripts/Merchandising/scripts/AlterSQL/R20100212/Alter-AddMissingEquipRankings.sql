INSERT INTO settings.EquipmentDisplayRankings ( BusinessUnitID, CategoryID, DisplayPriority, TierNumber, 
	TypeID, CategoryDesc, DisplayDescription, isSticky)
SELECT DISTINCT MR.BusinessUnitID, CT.CategoryID, CT.DisplayPriority, CT.TierNumber, CT.TypeID,
	CT.CategoryDesc, CT.DisplayDescription, CT.isSticky
FROM Settings.Merchandising as MR
CROSS JOIN
	(
	SELECT 1302 as CategoryID, 90050 as DisplayPriority, 4 as TierNumber, 
	13 as TypeID,'Electric Motor' as categoryDesc, 'Electric Motor' as DisplayDescription, 
	0 as isSticky 
	UNION
	SELECT 1301 as CategoryID, 9500 as DisplayPriority, 2 as TierNumber, 
	15 as TypeID, '1-Speed A/T' as categoryDesc, '1-Speed A/T' as DisplayDescription, 
	0 as isSticky) 
	as CT
LEFT JOIN settings.EquipmentDisplayRankings EDR ON
	EDR.businessUnitId = MR.businessUnitId
	AND EDR.categoryId = CT.categoryId
WHERE EDR.categoryId IS NULL  --make sure we're not duplicating

 