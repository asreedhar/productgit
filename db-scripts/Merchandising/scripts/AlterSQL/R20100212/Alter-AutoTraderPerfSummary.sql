if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[AutoTraderPerfSummary]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [merchandising].[AutoTraderPerfSummary]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [merchandising].[AutoTraderPerfSummary](
	[businessUnitID] int ,
	[month] datetime ,
	[CallsToDealership] int ,
	[ViewedWebsiteOrInventory] int ,
	[SecureCreditApplications] int ,
	[UsedVehiclesSearched] int ,
	[UsedDetailPagesViewed] int ,
	[UsedViewedMaps] int ,
	[UsedEmailsToDealership] int ,
	[UsedPrintableAds] int ,
	[NewVehiclesSearched] int ,
	[NewDetailPagesViewed] int ,
	[NewViewedMaps] int ,
	[NewEmailsToDealership] int ,
	[NewPrintableAds] int ,
	[SearchImpressions] int ,
	[ClickThrus] int ,
	[ViewedMaps] int ,
	[EmailsToDealership]	int ,
	[AskedForMoreDetails]	int ,
	[UsedAvgDailyVehicles]	int ,
	[UsedPctWithPrice]	decimal(6, 3) ,
	[UsedPctWithComments]		decimal(6, 3) ,
	[UsedPctWithPhotos]	decimal(6, 3) ,
	[UsedPctMultiplePhotos]	decimal(6, 3) ,
	[UsedPctSinglePhoto]		decimal(6, 3) ,
	[UsedPrimarySource]	varchar(50) ,
	[NewAvgDailyVehicles]		int ,
	[NewPctWithPrice]		decimal(6, 3) ,
	[NewPctWithComments]	decimal(6, 3) ,
	[NewPctWithPhotos]	decimal(6, 3) ,
	[NewPctMultiplePhotos]	decimal(6, 3) ,
	[NewPctSinglePhoto]	decimal(6, 3) ,
	[NewPrimarySource]	varchar(50) ,
	[PartnerSelectedMarkets]	varchar(50) ,
	[PartnerBannerAdImpressions1]	int ,
	[PartnerBannerAdImpressions2]	int ,
	[PartnerBannerAdClickThrus1]	int ,
	[PartnerBannerAdClickThrus2]	int 
	CONSTRAINT FK_AutoTraderPerfSummary FOREIGN KEY (businessUnitID) REFERENCES Merchandising.settings.Merchandising (businessUnitID)
	CONSTRAINT PK_AutoTraderPerfSummary PRIMARY KEY (businessUnitID, month)
)

GO