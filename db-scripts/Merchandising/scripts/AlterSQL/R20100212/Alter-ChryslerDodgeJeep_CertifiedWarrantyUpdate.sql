UPDATE CF
SET CF.Description = REPLACE(CF.Description, '8-Year', '6-Year')
FROM builder.certificationFeatures CF
JOIN builder.manufacturerCertifications MC
	ON CF.certificationTypeId = MC.certificationTypeId
WHERE MC.Name IN (
	'Chrysler',
	'Dodge',
	'Jeep'
)
AND CF.Description like '8-Year%'
GO