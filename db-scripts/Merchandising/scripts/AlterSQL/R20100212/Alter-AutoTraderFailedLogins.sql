SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [merchandising].[AutoTraderFailedLogins](
	[businessUnitID] int,
	[dateFailed] datetime
	
	CONSTRAINT PK_AutoTraderFailedLogins PRIMARY KEY (businessUnitID)
)

GO 