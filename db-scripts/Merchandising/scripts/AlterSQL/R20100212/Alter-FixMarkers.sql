
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

Update templates.BlurbSampleText
SET BlurbPreText = Replace(TBS.BlurbPreText, ':', '') 
from templates.AdtemplateItems 
TAI Join templates.Adtemplates TA 
ON TA.templateID = TAI.templateID 
Join templates.BlurbSampleText TBS 
ON TAI.blurbID = TBS.blurbID 
Where TBS.businessUnitID = 100150 
AND TAI.templateID IN (1264, 1265, 1266, 1267, 1268) 
AND TBS.BlurbPreText <>'' 
AND TBS.BlurbPreText LIKE '%:'


Update templates.BlurbSampleText 
SET BlurbPreText = Replace(TBS.BlurbPreText, '**', '') 
from templates.AdtemplateItems 
TAI Join templates.Adtemplates TA 
ON TA.templateID = TAI.templateID 
Join templates.BlurbSampleText TBS 
ON TAI.blurbID = TBS.blurbID 
Where TBS.businessUnitID = 100150 
AND TAI.templateID IN (1264, 1265, 1266, 1267, 1268) 
AND TBS.BlurbPreText <>'' 
AND TBS.BlurbPreText LIKE '**%-'
AND TBS.BlurbPreText not like '====%'


Update templates.BlurbSampleText 
SET BlurbPreText = Replace(TBS.BlurbPreText, '-', '') 
from templates.AdtemplateItems 
TAI Join templates.Adtemplates TA 
ON TA.templateID = TAI.templateID 
Join templates.BlurbSampleText TBS 
ON TAI.blurbID = TBS.blurbID 
Where TBS.businessUnitID = 100150 
AND TAI.templateID IN (1264, 1265, 1266, 1267, 1268) 
AND TBS.BlurbPreText <>'' 
AND TBS.BlurbPreText LIKE '%-'
AND TBS.BlurbPreText not like '====%'

Update templates.BlurbSampleText 
SET BlurbPreText = Replace(TBS.BlurbPreText, '**', '') 
from templates.AdtemplateItems 
TAI Join templates.Adtemplates TA 
ON TA.templateID = TAI.templateID 
Join templates.BlurbSampleText TBS 
ON TAI.blurbID = TBS.blurbID 
Where TBS.businessUnitID = 100150 
AND TAI.templateID IN (1264, 1265, 1266, 1267, 1268) 
AND TBS.BlurbPreText <>'' 
AND TBS.BlurbPreText LIKE '**%'
AND TBS.BlurbPreText not like '====%'

Update templates.BlurbSampleText
SET BlurbPreText = '====== ' + BlurbPreText
from templates.AdtemplateItems 
TAI Join templates.Adtemplates TA 
ON TA.templateID = TAI.templateID 
Join templates.BlurbSampleText TBS 
ON TAI.blurbID = TBS.blurbID 
Where TBS.businessUnitID = 100150 
AND TAI.templateID IN (1264, 1265, 1266, 1267, 1268) 
AND TBS.BlurbPreText <>'' 
AND TBS.BlurbPreText not like '====%'

GO