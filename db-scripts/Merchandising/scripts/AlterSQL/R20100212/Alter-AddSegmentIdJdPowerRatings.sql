 ALTER TABLE merchandising.JdPowerRatings
 ADD SegmentID tinyint NOT NULL DEFAULT (0)
 GO
 
 UPDATE merchandising.JdPowerRatings
 SET SegmentID=1
 WHERE AutoRatingsPageUrl LIKE '%Unknown%'
 GO
 
 UPDATE merchandising.JdPowerRatings
 SET SegmentID=2
 WHERE AutoRatingsPageUrl LIKE '%Truck%'
 OR AutoRatingsPageUrl LIKE '%Pickup%'
 GO
 
  UPDATE merchandising.JdPowerRatings
 SET SegmentID=3
 WHERE AutoRatingsPageUrl LIKE '%Sedan%'
 GO
 
  UPDATE merchandising.JdPowerRatings
 SET SegmentID=4
 WHERE AutoRatingsPageUrl LIKE '%Coupe%'
 GO
 
  UPDATE merchandising.JdPowerRatings
 SET SegmentID=5
 WHERE AutoRatingsPageUrl LIKE '%Van%'
 GO
 
  UPDATE merchandising.JdPowerRatings
 SET SegmentID=6
 WHERE AutoRatingsPageUrl LIKE '%SUV%'
 GO
 
  UPDATE merchandising.JdPowerRatings
 SET SegmentID=7
 WHERE AutoRatingsPageUrl LIKE '%Convertible%'
 GO
 
  UPDATE merchandising.JdPowerRatings
 SET SegmentID=8
 WHERE AutoRatingsPageUrl LIKE '%Wagon%'
 GO