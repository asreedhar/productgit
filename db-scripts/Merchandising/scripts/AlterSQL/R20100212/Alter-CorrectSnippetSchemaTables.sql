--===========================================================================--
--
-- Schema
--
--===========================================================================--
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'Snippets')
BEGIN
	DECLARE @cSchemaSQL VARCHAR(50)
	SET @cSchemaSQL = 'CREATE SCHEMA [Snippets]'
	PRINT @cSchemaSQL
	EXEC(@cSchemaSQL)
END
GO

--===========================================================================--
--
-- Tables
--
--===========================================================================--
/* [Snippets].[MarketingSource] */
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[MarketingSource]') AND type in (N'U'))
BEGIN

CREATE TABLE [Snippets].[MarketingSource](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[sourceName] [varchar](50) NULL,
	[hasCopyright] [bit] NULL,
	CONSTRAINT [PK_MarketingSource] PRIMARY KEY CLUSTERED (
		[id] ASC
	)
)

END

/* [Snippets].[Marketing] */
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[Marketing]') AND type in (N'U'))
BEGIN

CREATE TABLE [Snippets].[Marketing](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[marketingType] [int] NULL,
	[marketingText] [varchar](500) NULL,
	[sourceId] [int] NULL,
	[isVerbatim] [bit] NULL,
	CONSTRAINT [PK_Marketing] PRIMARY KEY CLUSTERED (
		[id] ASC
	)
)

ALTER TABLE [Snippets].[Marketing]  WITH NOCHECK ADD  CONSTRAINT [FK_Marketing_Source] FOREIGN KEY([sourceId])
REFERENCES [Snippets].[MarketingSource] ([id])

ALTER TABLE [Snippets].[Marketing] CHECK CONSTRAINT [FK_Marketing_Source]

END

/* [Snippets].[MarketingNote] */
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[MarketingNote]') AND type in (N'U'))
BEGIN

CREATE TABLE [Snippets].[MarketingNote](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[marketingId] [int] NULL,
	[notesText] [varchar](500) NULL,
	CONSTRAINT [PK_MarketingNote] PRIMARY KEY NONCLUSTERED (
		[id] ASC
	)
)

ALTER TABLE [Snippets].[MarketingNote]  WITH NOCHECK ADD  CONSTRAINT [FK_MarketingNotes_Marketing] FOREIGN KEY([marketingId])
REFERENCES [Snippets].[Marketing] ([id])

ALTER TABLE [Snippets].[MarketingNote] CHECK CONSTRAINT [FK_MarketingNotes_Marketing]


END

/* [Snippets].[Tag] */
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[Tag]') AND type in (N'U'))
BEGIN

CREATE TABLE [Snippets].[Tag](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [varchar](30) NULL,
	CONSTRAINT [PK_Tag] PRIMARY KEY CLUSTERED (
		[id] ASC
	)
)

END

/* [Snippets].[Marketing_Tag] */
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[Marketing_Tag]') AND type in (N'U'))
BEGIN

CREATE TABLE [Snippets].[Marketing_Tag](
	[joinId] [int] IDENTITY(1,1) NOT NULL,
	[marketingId] [int] NOT NULL,
	[tagId] [int] NOT NULL,
	CONSTRAINT [PK_Marketing_Tag] PRIMARY KEY CLUSTERED (
		[marketingId] ASC,
		[tagId] ASC
	)
)


ALTER TABLE [Snippets].[Marketing_Tag]  WITH NOCHECK ADD  CONSTRAINT [FK_Marketing_Tag_Marketing] FOREIGN KEY([marketingId])
REFERENCES [Snippets].[Marketing] ([id])

ALTER TABLE [Snippets].[Marketing_Tag] CHECK CONSTRAINT [FK_Marketing_Tag_Marketing]


ALTER TABLE [Snippets].[Marketing_Tag]  WITH NOCHECK ADD  CONSTRAINT [FK_Marketing_Tag_Tags] FOREIGN KEY([tagId])
REFERENCES [Snippets].[Tag] ([id])

ALTER TABLE [Snippets].[Marketing_Tag] CHECK CONSTRAINT [FK_Marketing_Tag_Tags]

END

/* [Snippets].[Marketing_Vehicle] */
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[Marketing_Vehicle]') AND type in (N'U'))
BEGIN

CREATE TABLE [Snippets].[Marketing_Vehicle](
	[joinId] [int] IDENTITY(1,1) NOT NULL,
	[marketingId] [int] NOT NULL,
	[chromeStyleId] [int] NOT NULL,
	CONSTRAINT [PK_Marketing_Vehicle] PRIMARY KEY CLUSTERED (
		[marketingId] ASC,
		[chromeStyleId] ASC
	)
)

ALTER TABLE [Snippets].[Marketing_Vehicle]  WITH NOCHECK ADD  CONSTRAINT [FK_Marketing_Vehicle_Marketing] FOREIGN KEY([marketingId])
REFERENCES [Snippets].[Marketing] ([id])

ALTER TABLE [Snippets].[Marketing_Vehicle] CHECK CONSTRAINT [FK_Marketing_Vehicle_Marketing]

END

/* [Snippets].[Marketing_Category] */
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[Marketing_Category]') AND type in (N'U'))
BEGIN

CREATE TABLE [Snippets].[Marketing_Category](
	[joinId] [int] IDENTITY(1,1) NOT NULL,
	[marketingId] [int] NOT NULL,
	[categoryId] [int] NOT NULL,
	CONSTRAINT [PK_Marketing_Category] PRIMARY KEY CLUSTERED (
		[marketingId] ASC,
		[categoryId] ASC
	)
)

ALTER TABLE [Snippets].[Marketing_Category]  WITH CHECK ADD  CONSTRAINT [FK_Marketing_Category_Marketing] FOREIGN KEY([marketingId])
REFERENCES [Snippets].[Marketing] ([id])

ALTER TABLE [Snippets].[Marketing_Category] CHECK CONSTRAINT [FK_Marketing_Category_Marketing]

END

/* [Snippets].[UserRating] */
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[UserRating]') AND type in (N'U'))
BEGIN

CREATE TABLE [Snippets].[UserRating](
	[marketingId] [int] NULL,
	[ratingScore] [int] NULL,
	[businessUnitId] [int] NULL
)

ALTER TABLE [Snippets].[UserRating]  WITH NOCHECK ADD  CONSTRAINT [FK_Marketing_UserRating] FOREIGN KEY([marketingId])
REFERENCES [Snippets].[Marketing] ([id])

ALTER TABLE [Snippets].[UserRating] CHECK CONSTRAINT [FK_Marketing_UserRating]

CREATE CLUSTERED INDEX [IX__UserRating_MarketingId] ON [Snippets].[UserRating] (
	[marketingId] ASC
)

END

/* [Snippets].[Marketing_Parent] */
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[Marketing_Parent]') AND type in (N'U'))
BEGIN

CREATE TABLE [Snippets].[Marketing_Parent](
	[marketingId] [int] NOT NULL,
	[parentId] [int] NULL,
	CONSTRAINT [PK_Marketing_Parent] PRIMARY KEY CLUSTERED (
		[marketingId] ASC
	)
)

ALTER TABLE [Snippets].[Marketing_Parent]  WITH NOCHECK ADD  CONSTRAINT [FK_Marketing_Parent_Marketing] FOREIGN KEY([parentId])
REFERENCES [Snippets].[Marketing] ([id])

ALTER TABLE [Snippets].[Marketing_Parent] CHECK CONSTRAINT [FK_Marketing_Parent_Marketing]

END