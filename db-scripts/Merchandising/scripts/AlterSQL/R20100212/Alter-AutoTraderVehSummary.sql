if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[AutoTraderVehSummary]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [merchandising].[AutoTraderVehSummary]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [merchandising].[AutoTraderVehSummary](
	[businessUnitID] int,
	[month] datetime,
	[VIN] varchar(50) ,
	[Type]	varchar(50) ,
	[YearMakeModel]	varchar(50) ,
	[StockNumber]	varchar(50) ,
	[Price]	money ,
	[AppearedInSearch]	int ,
	[DetailsPageViews]	int ,
	[MapViews]	int ,
	[EmailsSent]	int ,
	[DetailsPagePrinted]	int,
	[HasSpotLightAds] bit
	CONSTRAINT FK_AutoTraderVehSummary FOREIGN KEY (businessUnitID) REFERENCES Merchandising.settings.Merchandising (businessUnitID)
	CONSTRAINT PK_AutoTraderVehSummary PRIMARY KEY (businessUnitID, month, VIN)
)

GO