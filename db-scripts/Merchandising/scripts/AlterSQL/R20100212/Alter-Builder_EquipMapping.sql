if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[EquipmentMapping]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[EquipmentMapping]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [builder].[EquipmentMapping](
	[inventoryID] int NOT NULL,
	[businessUnitID] int NOT NULL,
	[EngineTypeCategoryID] int NOT NULL,
	[TransmissionTypeCategoryID] int NOT NULL,
	[DriveTrainCategoryID] int NOT NULL,
	[FuelCategoryID] int NOT NULL,
	[EngineAvailability] varchar(50) NOT NULL,
	[TransmissionAvailability] varchar(50) NOT NULL,
	[DriveTrainAvailability] varchar(50) NOT NULL,
	[FuelAvailability] varchar(50) NOT NULL,
	[createdBy] varchar(50) NULL,
	[createdOn] datetime NULL,
	[updatedBy] varchar(50) NULL,
	[updatedOn] datetime NULL,
	CONSTRAINT PK_EquipmentMapping PRIMARY KEY (businessUnitID, inventoryID)
)

GO