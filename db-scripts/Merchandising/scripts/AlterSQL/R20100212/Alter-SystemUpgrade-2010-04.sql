IF NOT EXISTS (SELECT 1 FROM Merchandising.merchandising.SystemMessages WHERE ExpiresOn = '04/13/2010 20:00:00')
INSERT INTO Merchandising.merchandising.SystemMessages (messageText,active,expiresOn)
VALUES ('Max Merchandising will be down for maintenance on Tuesday, 04/13/2010 from 6:00 PM until 8:00 PM CDT.  Thank you for your patience as we upgrade the system.'
      ,1
      ,'04/13/2010 20:00:00')