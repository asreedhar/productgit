ALTER TABLE settings.Merchandising
ADD InsertDate DATETIME NULL CONSTRAINT [DF__settings_Merchandising__InsertDate] DEFAULT getdate()
GO

UPDATE MS
SET MS.InsertDate = T.InsertDate
FROM settings.Merchandising MS
JOIN (
	SELECT D.BusinessUnitID AS BusinessUnitID, MIN(V.createdOn) AS InsertDate
	FROM settings.Merchandising D
	JOIN builder.OptionsConfiguration V
		ON D.BusinessUnitID = V.BusinessUnitID
	GROUP BY D.BusinessUnitID
) T ON MS.BusinessUnitID = T.BusinessUnitID
GO

--Add the insert date for FirstLook businessUnit
UPDATE settings.Merchandising
SET InsertDate = '2008-06-11 11:56:34'
WHERE BusinessUnitID = 100150
GO

ALTER TABLE settings.Merchandising
ADD LaunchedDate DATETIME NULL
GO

--Initialize Launch
UPDATE settings.Merchandising
SET LaunchedDate = InsertDate
WHERE InsertDate IS NOT NULL
GO
