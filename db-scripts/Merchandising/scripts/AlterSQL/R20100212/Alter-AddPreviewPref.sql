if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[PreviewPreferences]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[PreviewPreferences]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[PreviewPreferences](
	PreferenceId int IDENTITY(1,1),
	BusinessUnitId int NOT NULL,
	newOrUsed int NOT NULL,
    useMissionMarker bit NOT NULL DEFAULT(1),
    usePriceBelowBook bit NOT NULL DEFAULT(1),
    useOneOwner bit NOT NULL DEFAULT(1),
    usePriceReduced bit NOT NULL DEFAULT(1),
    useCertified bit NOT NULL DEFAULT(1),
    useVehicleCondition bit NOT NULL DEFAULT(1),
    useVehicleConditionStatements bit NOT NULL DEFAULT(1),
    useJdPowerRatings bit NOT NULL DEFAULT(1),
    useModelAwards bit NOT NULL DEFAULT(1),
    useExpertReviews bit NOT NULL DEFAULT(1),
    useCrashTestRatings bit NOT NULL DEFAULT(1),
    useWarranties bit NOT NULL DEFAULT(1),
    useFuelEconomy bit NOT NULL DEFAULT(1),
    useLowMilesPerYear bit NOT NULL DEFAULT(1),
    useFinancingSpecial bit NOT NULL DEFAULT(1),
    useTrim bit NOT NULL DEFAULT(1),
    useColors bit NOT NULL DEFAULT(1),
    useKeyInfoCheckboxes bit NOT NULL DEFAULT(1),
    useKeyVehicleEquipment bit NOT NULL DEFAULT(1),
    useDrivetrain bit NOT NULL DEFAULT(1),
	CONSTRAINT PK_PreviewPreferences PRIMARY KEY
	(
		businessUnitId, newOrUsed
	)
) 
GO

INSERT INTO settings.PreviewPreferences
(
	BusinessUnitId ,
	newOrUsed ,
    useMissionMarker ,
    usePriceBelowBook ,
    useOneOwner ,
    usePriceReduced ,
    useCertified ,
    useVehicleCondition ,
    useVehicleConditionStatements ,
    useJdPowerRatings ,
    useModelAwards ,
    useExpertReviews ,
    useCrashTestRatings ,
    useWarranties ,
    useFuelEconomy ,
    useLowMilesPerYear ,
    useFinancingSpecial ,
    useTrim ,
    useColors ,
    useKeyInfoCheckboxes ,
    useKeyVehicleEquipment,
    useDrivetrain
 )
 VALUES 
 (
 100150,1,
 1,1,1,1,1,1,1,1,1,1,
 1,1,1,1,1,1,1,1,1,1
 )
 GO
 
INSERT INTO settings.PreviewPreferences
(
	BusinessUnitId ,
	newOrUsed ,
    useMissionMarker ,
    usePriceBelowBook ,
    useOneOwner ,
    usePriceReduced ,
    useCertified ,
    useVehicleCondition ,
    useVehicleConditionStatements ,
    useJdPowerRatings ,
    useModelAwards ,
    useExpertReviews ,
    useCrashTestRatings ,
    useWarranties ,
    useFuelEconomy ,
    useLowMilesPerYear ,
    useFinancingSpecial ,
    useTrim ,
    useColors ,
    useKeyInfoCheckboxes ,
    useKeyVehicleEquipment,
    useDrivetrain
 )
 VALUES 
 (
 100150,2,
 1,1,1,1,1,1,1,1,1,1,
 1,1,1,1,1,1,1,1,1,1
 )
 GO