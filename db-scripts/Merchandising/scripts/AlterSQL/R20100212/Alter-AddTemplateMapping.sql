if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[DefaultTemplate_Vehicle]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [templates].[DefaultTemplate_Vehicle]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [templates].[DefaultTemplate_Vehicle](
	MappingId int IDENTITY(1,1),
	BusinessUnitId int NOT NULL,
	ChromeStyleId int NOT NULL,
	templateId int NOT NULL,
	themeSetId int NULL,	
	CONSTRAINT PK_DefaultTemplate_Vehicle PRIMARY KEY
	(
		businessUnitId, chromeStyleId
	)
) 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[DefaultTemplate_Model]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [templates].[DefaultTemplate_Model]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [templates].[DefaultTemplate_Model](
	MappingId int IDENTITY(1,1),
	BusinessUnitId int NOT NULL,
	ModelId int NOT NULL,
	templateId int NOT NULL,
	themeSetId int NULL,
	specificityLevel int NOT NULL,
	CONSTRAINT PK_DefaultTemplate_Model PRIMARY KEY
	(
		businessUnitId, modelId, specificityLevel
	)
) 
GO



if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[ThemeSet]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [templates].[ThemeSet]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [templates].[ThemeSet](
	ThemeSetId int NOT NULL,
	ThemeId int NOT NULL,
	CONSTRAINT PK_ThemeSet PRIMARY KEY
	(
		themeSetId
	)
) 
GO

ALTER TABLE templates.DefaultTemplate_Model
ADD CONSTRAINT FK_DefaultTemplate_Model_ThemeSet__themeSetId
FOREIGN KEY (themeSetId)
REFERENCES templates.ThemeSet (themeSetId)
GO

ALTER TABLE templates.DefaultTemplate_Vehicle
ADD CONSTRAINT FK_DefaultTemplate_Vehicle_ThemeSet__themeSetId
FOREIGN KEY (themeSetId)
REFERENCES templates.ThemeSet (themeSetId)
GO

--create default themeSets that consist of only the single theme
INSERT INTO templates.ThemeSet (themeSetId, themeId)
select id,id from templates.themes
GO

--alter the adtemplates table to include a reference to sourceTemplateId for whenever a template was copied from FirstLook frameworks
ALTER TABLE templates.AdTemplates
ADD SourceTemplateId int NULL
GO

--we'll need to create an update to set the source approrpriately


declare @tmpTemplateId int

--------Certified
select top(1) @tmpTemplateId = templateId FROM templates.adtemplates where [name] like '%certified%' and businessUnitId = 100150 and active = 1

if NOT @tmpTemplateId IS NULL
BEGIN
	update templates.AdTemplates 
	set sourceTemplateId = @tmpTemplateId
	where [name] like '%certified%' and businessUnitId <> 100150 and active = 1
END

---------Family
select @tmpTemplateId = NULL --clear it out
select top(1) @tmpTemplateId = templateId FROM templates.adtemplates where [name] like '%family%' and businessUnitId = 100150 and active = 1

if NOT @tmpTemplateId IS NULL
BEGIN
	update templates.AdTemplates 
	set sourceTemplateId = @tmpTemplateId
	where [name] like '%family%' and businessUnitId <> 100150 and active = 1
END

---------Cash/Value
select @tmpTemplateId = NULL --clear it out
select top(1) @tmpTemplateId = templateId FROM templates.adtemplates where [name] like '%cash%' and businessUnitId = 100150 and active = 1

if NOT @tmpTemplateId IS NULL
BEGIN
	update templates.AdTemplates 
	set sourceTemplateId = @tmpTemplateId
	where ([name] like '%cash%' or [name] like '%value%')  and businessUnitId <> 100150 and active = 1
END

---------Luxury
select @tmpTemplateId = NULL --clear it out
select top(1) @tmpTemplateId = templateId FROM templates.adtemplates where [name] like '%luxury%' and businessUnitId = 100150 and active = 1

if NOT @tmpTemplateId IS NULL
BEGIN
	update templates.AdTemplates 
	set sourceTemplateId = @tmpTemplateId
	where ([name] like '%luxury%')  and businessUnitId <> 100150 and active = 1
END

---------Aggressive Pricing
select @tmpTemplateId = NULL --clear it out
select top(1) @tmpTemplateId = templateId FROM templates.adtemplates where [name] like '%aggressive%' and businessUnitId = 100150 and active = 1

if NOT @tmpTemplateId IS NULL
BEGIN
	update templates.AdTemplates 
	set sourceTemplateId = @tmpTemplateId
	where ([name] like '%aggressive%')  and businessUnitId <> 100150 and active = 1
END

--

SET ANSI_PADDING OFF
GO 
