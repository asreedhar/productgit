
if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[CredentialedSites]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[CredentialedSites]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[CredentialedSites](
	siteId int IDENTITY(1,1),
	siteName varchar(30) NOT NULL,
	CONSTRAINT PK_CredentialedSites PRIMARY KEY
	(
		siteId
	)
)
GO

insert into settings.CredentialedSites (siteName) VALUES ('BMW Dealer Speed')
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[Dealer_SiteCredentials]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[Dealer_SiteCredentials]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[Dealer_SiteCredentials](
	credentialId int IDENTITY(1,1),
	siteId int NOT NULL,
	businessUnitId int NOT NULL,
	username varchar(50),
	encryptedPassword varchar(50),		
	reservedField varchar(50) NULL,
	isLockedOut bit NOT NULL DEFAULT(0),
	CONSTRAINT PK_Dealer_SiteCredentials PRIMARY KEY
	(
		businessUnitId, siteId
	)
) 
GO 


ALTER TABLE settings.Dealer_SiteCredentials
ADD CONSTRAINT FK_Dealer_SiteCredentials_CredentialedSites
FOREIGN KEY (siteId)
REFERENCES settings.CredentialedSites (siteId)
GO
