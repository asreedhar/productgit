if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[AutoTraderExSummary]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [merchandising].[AutoTraderExSummary]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [merchandising].[AutoTraderExSummary](
	[businessUnitID] int ,
	[month] datetime ,
	[AvgDayNew] int ,
	[AvgDayUsed] int,
	[AvgDayTotal] int ,
	[TimesSeenInSearch] int ,
	[BannerAdImpressions] int ,
	[FindDealerSearchImpressions] int ,
	[TotalExposure] int ,
	[DetailPagesViewed] int ,
	[ViewedMaps] int ,
	[PrintableAdsRequested] int ,
	[ViewedWebsiteOrInventory] int ,
	[FindDealerClickThrus] int ,
	[BannerAdClickThrus] int ,
	[TotalActivity] int ,
	[CallsToDealership] int ,
	[EmailsToDealership] int ,
	[SecureCreditApplications] int ,
	[TotalTrackableProspects] int ,
	[TotalShoppersInArea] int 
	CONSTRAINT FK_AutoTraderExSummary FOREIGN KEY (businessUnitID) REFERENCES Merchandising.settings.Merchandising (businessUnitID)
	CONSTRAINT PK_AutoTraderExSummary PRIMARY KEY (businessUnitID, month)
)

GO