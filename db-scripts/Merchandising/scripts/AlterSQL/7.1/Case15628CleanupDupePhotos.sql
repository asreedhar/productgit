use Merchandising
go

set nocount on
set transaction isolation level read uncommitted

create table temp.bug#15628#DupePhotosCleanupQueue#20110801
(
	PhotoID int not null,
	status int not null
)

insert temp.bug#15628#DupePhotosCleanupQueue#20110801
(
	PhotoId,
	status
)
select
	PhotoId,
	0
from
	IMT.dbo.Photos
where
	PhotoStatusCode = 2
	and PhotoProviderID = 11999
	and left(OriginalPhotoURL, 40) = 'https://www.firstlook.biz/digitalimages/'
	

alter table temp.bug#15628#DupePhotosCleanupQueue#20110801 add constraint PK_bug#15628#DupePhotosCleanupQueue#20110801 primary key clustered (PhotoID)
create nonclustered index ix_status on temp.bug#15628#DupePhotosCleanupQueue#20110801 (status)

	
declare
	@PhotoId int,
	@InventoryId int,
	@BusinessUnitId int,
	@CorrectPhotoId int,
	@FullURL varchar(200)
		
while (1=1)
begin
	select top 1 @PhotoId = PhotoID from temp.bug#15628#DupePhotosCleanupQueue#20110801 where status = 0
	if (@@rowcount <> 1) break;
	
	update temp.bug#15628#DupePhotosCleanupQueue#20110801 set status = 1 where PhotoId = @PhotoId
	
	-- get the id's to verify/tie back to builder.VehiclePhotos properly
	select
		@BusinessUnitID = i.BusinessUnitID,
		@InventoryId = ip.InventoryId,
		@FullURL = p.OriginalPhotoURL
	from
		IMT.dbo.Photos p
		join IMT.dbo.InventoryPhotos ip
			on p.PhotoID = ip.PhotoID
		join IMT.dbo.Inventory i
			on ip.InventoryID = i.InventoryID
	where
		p.PhotoID = @PhotoId
	
	if (@@rowcount <> 1)
	begin
		-- oh great, we have photo sharing over dupe photos
		update temp.bug#15628#DupePhotosCleanupQueue#20110801 set status = 2 where PhotoId = @PhotoId
		continue
	end
	
	-- find the original photo that created a row in builder.VehiclePhotos that was then incorrectly duped back over to IMT
	select
		@CorrectPhotoId = p.PhotoID
	from
		IMT.dbo.Photos p
		join IMT.dbo.InventoryPhotos ip
			on p.PhotoID = ip.PhotoID
	where
		ip.InventoryID = @InventoryId
		and p.PhotoID <> @PhotoId
		and p.PhotoURL = replace(@FullURL, 'https://www.firstlook.biz/digitalimages/', '')
		
	if (@@rowcount <> 1)
	begin
		-- hmmm....
		
--		select 'debug'
--		,CorrectPhotoId = @CorrectPhotoId
--		,PhotoId =@PhotoId
--		,InventoryId =@InventoryId
--		,FullURL = @FullURL
		 
		 
		 
		update temp.bug#15628#DupePhotosCleanupQueue#20110801 set status = 3 where PhotoId = @PhotoId
		
--		break
		
		
		continue
	end	
	
	begin try
		begin tran
		
		-- delete dupe row from IMT

		delete IMT.dbo.InventoryPhotos where InventoryID = @InventoryId and PhotoID = @PhotoId	
		if (@@rowcount <> 1)
		begin
			rollback
			update temp.bug#15628#DupePhotosCleanupQueue#20110801 set status = 4 where PhotoId = @PhotoId
			continue
		end
		

		delete IMT.dbo.Photos where PhotoID = @PhotoId
		if (@@rowcount <> 1)
		begin
			rollback
			update temp.bug#15628#DupePhotosCleanupQueue#20110801 set status = 5 where PhotoId = @PhotoId
			continue
		end




	
		-- re-assign IMSPhotoID in Max
		update Merchandising.builder.VehiclePhotos set
			IMSPhotoId = @CorrectPhotoId 
		where
			businessUnitID = @BusinessUnitId
			and inventoryId = @InventoryId
			and IMSPhotoId = @PhotoId
		if (@@rowcount <> 1)
		begin
			rollback
			update temp.bug#15628#DupePhotosCleanupQueue#20110801 set status = 6 where PhotoId = @PhotoId
			continue
		end

		-- yay cleanup complete:
		update temp.bug#15628#DupePhotosCleanupQueue#20110801 set status = 100 where PhotoId = @PhotoId
		
		commit
	end try
	begin catch
		rollback
		
		declare @error varchar(1000)
		
		set @error =
			'ErrorNumber ' + isnull(convert(varchar(11), error_number()), '<null>') +
			', ErrorSeverity ' + isnull(convert(varchar(11), error_severity()), '<null>') +
			', ErrorState ' + isnull(convert(varchar(11), error_state()), '<null>') +
			', ErrorProcedure ' + isnull(error_procedure(), '<null>') +
			', ErrorLine ' + isnull(convert(varchar(11), error_line()), '<null>') +
			', ErrorMessage ' + isnull(error_message(), '<null>')
		
		raiserror(@error, 16, 1)
		
		break
	end catch
	
	
end

	
	