BEGIN TRANSACTION
GO

-- Add dateLoginFailure & dateLoginFailureCleared to DealerListingSites
ALTER TABLE settings.DealerListingSites ADD
	dateLoginFailure datetime NULL,
	dateLoginFailureCleared datetime NULL
GO

-- Copy data from AutoTraderFailedLogins to DealerListingSites
UPDATE settings.DealerListingSites 
	SET 
		dateLoginFailure = src.dateFailed,
		dateLoginFailureCleared = src.dateCleared
	FROM
		settings.DealerListingSites as dst
		INNER JOIN merchandising.AutoTraderFailedLogins as src
		ON dst.businessUnitID = src.businessUnitID
	WHERE
		dst.destinationID = 2 -- 2 = AutoTrader.com

-- Drop table AutoTraderFailedLogins
DROP TABLE merchandising.AutoTraderFailedLogins

COMMIT