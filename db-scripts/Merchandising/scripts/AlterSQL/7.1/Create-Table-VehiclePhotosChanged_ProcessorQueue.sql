IF (OBJECT_ID('builder.VehiclePhotosChanged_ProcessorQueue') IS NOT NULL)
   DROP TABLE builder.VehiclePhotosChanged_ProcessorQueue
GO

CREATE TABLE builder.VehiclePhotosChanged_ProcessorQueue(
	BusinessUnitID int NOT NULL,
	InventoryID int NOT NULL,
	TS TimeStamp
)

IF (OBJECT_ID('builder.CI_VehiclePhotosChanged_ProcessorQueue_InventoryID_TimeStamp') IS NOT NULL)
   DROP INDEX builder.CI_VehiclePhotosChanged_ProcessorQueue_InventoryID_TimeStamp
GO

CREATE CLUSTERED INDEX CI_VehiclePhotosChanged_ProcessorQueue_InventoryID_TimeStamp
ON builder.VehiclePhotosChanged_ProcessorQueue(BusinessUnitID, InventoryID, TS) WITH FILLFACTOR=50
