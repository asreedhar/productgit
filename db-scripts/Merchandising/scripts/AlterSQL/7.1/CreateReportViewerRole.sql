DECLARE @RC int
DECLARE @ApplicationName nvarchar(256)
DECLARE @RoleName nvarchar(256)

SET @ApplicationName = 'Merchandising'
SET @RoleName = 'Report Viewer'

EXECUTE @RC = [Merchandising].[dbo].[aspnet_Roles_CreateRole] 
   @ApplicationName
  ,@RoleName
