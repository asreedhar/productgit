IF( NOT EXISTS(SELECT 1 FROM Merchandising.builder.manufacturerCertifications WHERE certificationTypeId = 41 ) )
BEGIN

SET IDENTITY_INSERT Merchandising.builder.manufacturerCertifications ON
INSERT INTO  Merchandising.builder.manufacturerCertifications( certificationTypeId, [name] )
VALUES(41, 'Ram')
SET IDENTITY_INSERT Merchandising.builder.manufacturerCertifications OFF

INSERT INTO Merchandising.settings.cpoCertifieds( certificationTypeId, cpoGroupId )
VALUES(41, 3)

END