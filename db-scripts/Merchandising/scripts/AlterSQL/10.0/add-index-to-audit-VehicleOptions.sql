

if exists(select * from sys.indexes where object_id = object_id('audit.VehicleOptions') and name = 'IX__BusinessUnitId_InventoryId')
	drop index audit.VehicleOptions.IX__BusinessUnitId_InventoryId
go

create nonclustered index IX__BusinessUnitId_InventoryId on audit.VehicleOptions (businessUnitID, inventoryID)
	
	
