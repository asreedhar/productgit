
DECLARE @intErrorCode INT
BEGIN TRAN
	delete from  [Merchandising].[templates].[OptionCodeRuleMappings]
	delete from  [Merchandising].[templates].[OptionCodeRules]
	--first add the rules
	INSERT INTO [Merchandising].[templates].[OptionCodeRules]
           ([ruleId]
           ,[textToReplace]
           ,[textToReplaceWith])
     
	
		select 0,'(1) year subscription',''     
		union all
		select 1, 'w/(1) year subscription',''

	
	SELECT @intErrorCode = @@ERROR
    IF (@intErrorCode <> 0) GOTO PROBLEM

	--then the mapping
	INSERT INTO [Merchandising].[templates].[OptionCodeRuleMappings]
           ([optionCode]
           ,[businessunitid]
           ,[ruleId]
           ,[inventoryType])
	 
		select '655', 100150 , 0 ,2
		union all 
		select 'ZPS', 100150 , 1 ,2
		union all 
		select '7HB', 100150 , 1 ,2

	SELECT @intErrorCode = @@ERROR
		IF (@intErrorCode <> 0) GOTO PROBLEM
Commit Tran

PROBLEM:
IF (@intErrorCode <> 0) BEGIN
PRINT 'Unexpected error occurred!'
    ROLLBACK TRAN
END