param(
	[Parameter(Mandatory = $true)]
	$InputCsv,
	
	[Parameter(Mandatory = $true)]
	$OutputSql = $null
)

function EscapeXML($val)
{
	$val = $val -replace '&', '&amp;'
	$val = $val -replace '"', '&quot;'
	$val = $val -replace "'", '&apos;'
	$val = $val -replace '<', '&lt;'
	$val = $val -replace '>', '&gt;'
	$val
}

function GenerateInsertData
{
	(Import-CSV $InputCsv |
		Foreach-Object -Begin { $c = 0 } -End { "';" }`
		{
			if($c % 100 -eq 0)
			{
				if($c -gt 0) { "';" }
				"set @xml = @xml + N'"
			}
			
			"<r s=`"$($_.StyleId)`" q=`"$($_.Sequence)`" o=`"$(EscapeXML $_.OptionCode)`" p=`"$(EscapeXML $_.PON)`" />"
			
			$c++
		}
		
	) -join "`r`n"
}

@"
use Merchandising

set nocount on
set transaction isolation level read uncommitted

declare @whitelist table (
	ChromeStyleId int, 
	Sequence int,
	OptionCode varchar(20),
	PON varchar(1000),
	primary key clustered (ChromeStyleId, Sequence))

print 'Loading data for ''$inputCsv'''
	
print 'Loading XML string...' + convert(varchar,getdate(),121)
declare @xml nvarchar(max), @hdoc int
set @xml = N''
$(GenerateInsertData)
set @xml = N'<rows>' + @xml + N'</rows>'

print 'Parsing XML...' + convert(varchar,getdate(),121)
exec sp_xml_preparedocument @hdoc OUTPUT, @xml

print 'Loading @whitelist...' + convert(varchar,getdate(),121)
insert into @whitelist (ChromeStyleId, Sequence, OptionCode, PON)
select s as ChromeStyleId, q as Sequence, o as OptionCode, p as PON
from OPENXML(@hdoc, '/rows/r', 1) WITH (s int, q int, o varchar(20), p varchar(1000))

exec sp_xml_removedocument @hdoc

select wl.ChromeStyleId, wl.OptionCode as WlOptionCode, o.OptionCode, wl.PON as WlPON, o.PON
from @whitelist wl
left outer join VehicleCatalog.chrome.Options o
on o.CountryCode = 1
    and wl.ChromeStyleId = o.StyleId
    and wl.Sequence = o.Sequence
where o.PON is null or wl.PON <> o.PON

print 'Done.' + convert(varchar,getdate(),121)
"@ | Set-Content $OutputSql 
