use Merchandising

if not exists (select * 
               from information_schema.schemata
               where catalog_name = 'Merchandising' and schema_name = 'workflow')
begin
    declare @cmd nvarchar(1000)
    set @cmd = N'create schema workflow authorization dbo'
    exec sp_executesql @cmd
end
GO

grant select on schema :: workflow TO MerchandisingUser
GO