--Add the VW Credential site
if not exists (select * from [Merchandising].[settings].[CredentialedSites] where SiteName = 'Volkswagen Hub')
INSERT INTO [Merchandising].[settings].[CredentialedSites]
           ([siteName])
     VALUES
           ('Volkswagen Hub')



--Add the Mazda Credential site
if not exists (select * from [Merchandising].[settings].[CredentialedSites] where SiteName = 'Mazda USA')
INSERT INTO [Merchandising].[settings].[CredentialedSites]
           ([siteName])
     VALUES
           ('Mazda USA')