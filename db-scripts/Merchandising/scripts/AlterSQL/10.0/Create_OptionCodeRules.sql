USE [Merchandising]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[templates].[FK_OptionCodeRuleMappings_OptionCodeRules]') AND parent_object_id = OBJECT_ID(N'[templates].[OptionCodeRuleMappings]'))
ALTER TABLE [templates].[OptionCodeRuleMappings] DROP CONSTRAINT [FK_OptionCodeRuleMappings_OptionCodeRules]
GO
if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[OptionCodeRules]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [templates].[OptionCodeRules]
GO
/****** Object:  Table [templates].[OptionCodeRules]    Script Date: 01/09/2012 13:39:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [templates].[OptionCodeRules](
	[ruleId] [int] NOT NULL,
	[textToReplace] [varchar](150) NOT NULL,
	[textToReplaceWith] [varchar](150) NOT NULL,
 CONSTRAINT [PK_OptionCodeRules_1] PRIMARY KEY CLUSTERED 
(
	[ruleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF