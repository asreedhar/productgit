
DECLARE @intErrorCode INT
BEGIN TRAN
	delete from  [Merchandising].[templates].[OptionCodeRuleMappings] where ruleid in (11)
	delete from  [Merchandising].[templates].[OptionCodeRules] where ruleid in (11)
	--fix the order of rules for 58N,  most verbose first
	delete from  [Merchandising].[templates].[OptionCodeRuleMappings] where optioncode = '58N' and ruleid in (9, 10)
	--first add the rules
	INSERT INTO [Merchandising].[templates].[OptionCodeRules]
           ([ruleId]
           ,[textToReplace]
           ,[textToReplaceWith])
     
		select 11,', (R6J) 1-year subscription of XM Radio (an additional 9 months of service after the 3 trial months)',''     
	
	SELECT @intErrorCode = @@ERROR
    IF (@intErrorCode <> 0) GOTO PROBLEM

	--then the mapping
	INSERT INTO [Merchandising].[templates].[OptionCodeRuleMappings]
           ([optionCode]
           ,[businessunitid]
           ,[ruleId]
           ,[inventoryType])
	 
		select 'PCK', 100150 , 11 ,2
		Union all
		select '58N', 100150 , 10 ,2
		Union all
		select '58N', 100150 , 9 ,2

	SELECT @intErrorCode = @@ERROR
		IF (@intErrorCode <> 0) GOTO PROBLEM
Commit Tran

PROBLEM:
IF (@intErrorCode <> 0) BEGIN
PRINT 'Unexpected error occurred!'
    ROLLBACK TRAN
END