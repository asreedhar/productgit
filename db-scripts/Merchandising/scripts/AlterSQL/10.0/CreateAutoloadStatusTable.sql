if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[AutoloadStatus]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[AutoloadStatus]
GO
GO
/****** Object:  Table [builder].[VehicleStatus]    Script Date: 12/28/2011 08:49:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[AutoloadStatus](
	[businessUnitID] [int] NOT NULL,
	[inventoryID] [int] NOT NULL,
	[statusTypeId] [int] NOT NULL,
	[startTime] [datetime] NOT NULL,
	[endTime] [datetime],
	[createdOn] [datetime] NOT NULL,
	[createdBy] [varchar](30) NOT NULL,
	[lastUpdatedOn] [datetime] NOT NULL,
	[lastUpdatedBy] [varchar](30) NOT NULL
	
  CONSTRAINT [PK_AutoloadStatus] PRIMARY KEY CLUSTERED 
(
	[businessUnitID] ASC,
	[inventoryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [builder].[AutoloadStatus]  WITH CHECK ADD  CONSTRAINT [FK__AutoloadStatus_AutoloadStatusTypes] FOREIGN KEY([statusTypeId])
REFERENCES [builder].[AutoloadStatusTypes] ([statusTypeId])
GO
ALTER TABLE [builder].[AutoloadStatus] CHECK CONSTRAINT [FK__AutoloadStatus_AutoloadStatusTypes]
