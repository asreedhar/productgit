if exists (select * from dbo.sysobjects where id = object_id(N'[templates].[OptionCodeRuleMappings]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [templates].[OptionCodeRuleMappings]


GO
/****** Object:  Table [templates].[OptionCodeRuleMappings]    Script Date: 01/09/2012 13:38:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [templates].[OptionCodeRuleMappings](
	[optionCode] [varchar](20) NOT NULL,
	[businessunitid] [int] NOT NULL,
	[ruleId] [int] NOT NULL,
	[inventoryType] [int] NOT NULL,
 CONSTRAINT [PK_OptionCodeRuleMappings] PRIMARY KEY CLUSTERED 
(
	[optionCode] ASC,
	[businessunitid] ASC,
	[ruleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [templates].[OptionCodeRuleMappings]  WITH CHECK ADD  CONSTRAINT [FK_OptionCodeRuleMappings_OptionCodeRules] FOREIGN KEY([ruleId])
REFERENCES [templates].[OptionCodeRules] ([ruleId])
GO
ALTER TABLE [templates].[OptionCodeRuleMappings] CHECK CONSTRAINT [FK_OptionCodeRuleMappings_OptionCodeRules]