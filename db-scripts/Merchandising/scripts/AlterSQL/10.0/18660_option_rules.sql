
DECLARE @intErrorCode INT
BEGIN TRAN

	--first add the rules
	INSERT INTO [Merchandising].[templates].[OptionCodeRules]
           ([ruleId]
           ,[textToReplace]
           ,[textToReplaceWith])
     
	
		select 2,'(6) month subscription',''     
		union all
		select 3, 'w/(6) month subscription',''

	
	SELECT @intErrorCode = @@ERROR
    IF (@intErrorCode <> 0) GOTO PROBLEM

	--then the mapping
	INSERT INTO [Merchandising].[templates].[OptionCodeRuleMappings]
           ([optionCode]
           ,[businessunitid]
           ,[ruleId]
           ,[inventoryType])
	 
		select '-TECH', 100150 , 3 ,2
		union all 
		select '000500', 100150 , 2 ,2
		union all 
		select '022', 100150 , 2 ,2
		union all 
		select '1EP', 100150 , 3 ,2
		union all 
		select '1SA', 100150 , 2 ,2
		union all 
		select '2PR', 100150 , 3 ,2
		union all
		select '320', 100150 , 3 ,2
		union all 
		select '420', 100150 , 3 ,2
		union all 
		select '536', 100150 , 2 ,2
		union all 
		select '58N', 100150 , 3 ,2
		union all 
		select '58Q', 100150 , 3 ,2
		union all 
		select '58Q-1', 100150 , 3 ,2
		union all
		select '58Q-F', 100150 , 3 ,2
		union all 
		select '58Q-R', 100150 , 3 ,2
		union all 
		select '608', 100150 , 3 ,2
		union all 
		select '609', 100150 , 3 ,2
		union all 
		select '60S', 100150 , 3 ,2
		union all 
		select '62S', 100150 , 2 ,2
		union all
		select '85B', 100150 , 2 ,2
		union all 
		select '91S', 100150 , 2 ,2
		union all 
		select '981', 100150 , 2 ,2
		union all 
		select 'P01', 100150 , 3 ,2
		union all 
		select 'P02', 100150 , 3 ,2
		union all 
		select 'P03', 100150 , 3 ,2
		union all
		select 'P1A', 100150 , 3 ,2
		union all
		select 'P2-1', 100150 , 3 ,2
		union all
		select 'P2A', 100150 , 3 ,2
		union all
		select 'P3A', 100150 , 3 ,2
		union all
		select 'SAT', 100150 , 2 ,2
		union all
		select 'WPS', 100150 , 3 ,2
		union all
		select 'WPT', 100150 , 3 ,2


	SELECT @intErrorCode = @@ERROR
		IF (@intErrorCode <> 0) GOTO PROBLEM
Commit Tran

PROBLEM:
IF (@intErrorCode <> 0) BEGIN
PRINT 'Unexpected error occurred!'
    ROLLBACK TRAN
END