param(
	[Parameter(Mandatory = $true)]
	$InputCsv,
	
	[Parameter(Mandatory = $true)]
	$OutputSql = $null
)

function GenerateInsertData
{
	(Import-CSV $InputCsv |
		Foreach-Object -Begin { $c = 0 } -End { "';" }`
		{
			if($c % 100 -eq 0)
			{
				if($c -gt 0) { "';" }
				"set @xml = @xml + N'"
			}
			
			"<r s=`"$($_.StyleId)`" q=`"$($_.Sequence)`"/>"
			
			$c++
		}
		
	) -join "`r`n"
}

@"
use Merchandising

set nocount on
set transaction isolation level read uncommitted

declare @whitelist table (
	ChromeStyleId int, 
	Sequence int,
	primary key clustered (ChromeStyleId, Sequence))

print 'Loading data for ''$inputCsv'''
	
print 'Loading XML string...' + convert(varchar,getdate(),121)
declare @xml nvarchar(max), @hdoc int
set @xml = N''
$(GenerateInsertData)
set @xml = N'<rows>' + @xml + N'</rows>'

print 'Parsing XML...' + convert(varchar,getdate(),121)
exec sp_xml_preparedocument @hdoc OUTPUT, @xml

print 'Loading @whitelist...' + convert(varchar,getdate(),121)
insert into @whitelist (ChromeStyleId, Sequence)
select s as ChromeStyleId, q as Sequence
from OPENXML(@hdoc, '/rows/r', 1) WITH (s int, q int)

exec sp_xml_removedocument @hdoc

print 'Deleting existing values from chrome.PackageOptionCodeWhitelist...' + convert(varchar,getdate(),121)
delete from PkgWL
from chrome.PackageOptionCodeWhitelist PkgWL
where exists (select * 
              from @whitelist WL 
              where WL.ChromeStyleId = PkgWL.ChromeStyleId)

print 'Inserting new values to chrome.PackageOptionCodeWhiteList...' + convert(varchar,getdate(),121)
insert into Merchandising.chrome.PackageOptionCodeWhitelist (ChromeStyleId, ChromeOptionCode)
select wl.ChromeStyleId, o.OptionCode 
from @whitelist wl
left outer join VehicleCatalog.chrome.Options o
on o.CountryCode = 1
    and wl.ChromeStyleId = o.StyleId
    and wl.Sequence = o.Sequence

print 'Done.' + convert(varchar,getdate(),121)
"@ | Set-Content $OutputSql 
