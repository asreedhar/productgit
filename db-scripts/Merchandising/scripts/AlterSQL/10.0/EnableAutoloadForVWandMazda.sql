--Enable autoload for Volkswagen -24
--Enable autoload for Mazda -17
UPDATE [Merchandising].[merchandising].[VehicleAutoLoadSettings]
   SET 
      [IsAutoLoadEnabled] = 1
 WHERE ManufacturerId in ('17','24')