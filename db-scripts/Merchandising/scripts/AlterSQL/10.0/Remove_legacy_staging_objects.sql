use Merchandising
GO

if object_id('staging.vehicle#Create', 'P') is not null
begin
    drop procedure staging.vehicle#Create
    print 'Procedure dropped'
end

if object_id('staging.getInventoryItem', 'P') is not null
begin
    drop procedure staging.getInventoryItem
    print 'Procedure dropped'
end

if object_id('staging.Inventory', 'U') is not null
begin
    drop table staging.Inventory
    print 'Table dropped'
end