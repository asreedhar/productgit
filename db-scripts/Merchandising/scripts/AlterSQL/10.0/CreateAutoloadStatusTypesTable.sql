if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[AutoloadStatusTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [builder].[AutoloadStatusTypes]
GO
GO
/****** Object:  Table [builder].[VehicleStatusTypes]    Script Date: 12/28/2011 08:27:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [builder].[AutoloadStatusTypes](
	[statusTypeId] [int] NOT NULL,
	[statusType] [varchar](30) NOT NULL,
	[statusTypeDescription] [varchar](70) NOT NULL,
 CONSTRAINT [PK__builder_AutoloadStatusTypes] PRIMARY KEY CLUSTERED 
(
	[statusTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF