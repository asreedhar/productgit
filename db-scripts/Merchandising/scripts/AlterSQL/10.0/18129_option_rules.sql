
DECLARE @intErrorCode INT
BEGIN TRAN
	--first add the rules
	INSERT INTO [Merchandising].[templates].[OptionCodeRules]
           ([ruleId]
           ,[textToReplace]
           ,[textToReplaceWith])
     
	
		select 4,'1 year of XM Radio and XM NavTraffic service, ',''     
		union all
		select 5, '1 year of XM Radio Service, ',''
		union all
		select 6, 'Also, includes 1 year of XM Radio Service',''

	
	SELECT @intErrorCode = @@ERROR
    IF (@intErrorCode <> 0) GOTO PROBLEM

	--then the mapping
	INSERT INTO [Merchandising].[templates].[OptionCodeRuleMappings]
           ([optionCode]
           ,[businessunitid]
           ,[ruleId]
           ,[inventoryType])
	 
		select 'PCJ', 100150 , 4 ,2
		union all 
		select 'PCU', 100150 , 5 ,2
		union all 
		select 'PDA', 100150 , 6 ,2
		union all
		select 'PEB', 100150 , 6 ,2

	SELECT @intErrorCode = @@ERROR
		IF (@intErrorCode <> 0) GOTO PROBLEM
Commit Tran

PROBLEM:
IF (@intErrorCode <> 0) BEGIN
PRINT 'Unexpected error occurred!'
    ROLLBACK TRAN
END