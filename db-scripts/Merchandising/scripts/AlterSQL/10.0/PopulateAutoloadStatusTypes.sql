delete from [Merchandising].[builder].[AutoloadStatusTypes]

INSERT INTO [Merchandising].[builder].[AutoloadStatusTypes]
           ([statusTypeId]
           ,[statusType]
           ,[statusTypeDescription])
     
select '100' ,'Success' ,'Autoload completed successfully.'
union all

select	'0' ,'Pending','Autoload is pending processing.'
union all


select '-100' ,'UnknownError' ,'An unknown error was encountered during processing.'
union all

select '-99' ,'InvalidCredentials' ,'The credentials supplied were invalid.'
union all

select	'-98' ,'NoVehicleFound','The vehicle was not found in the database.'
union all

select	'-97' ,'NetworkError','Error connecting to fetch.'
