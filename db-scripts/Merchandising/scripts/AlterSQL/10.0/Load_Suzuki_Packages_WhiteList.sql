use Merchandising

set nocount on
set transaction isolation level read uncommitted

declare @whitelist table (
	ChromeStyleId int, 
	Sequence int,
	primary key clustered (ChromeStyleId, Sequence))

print 'Loading data for ''C:\temp\19362\suzuki_Packages_v2.csv'''
	
print 'Loading XML string...' + convert(varchar,getdate(),121)
declare @xml nvarchar(max), @hdoc int
set @xml = N''
set @xml = @xml + N'
<r s="337985" q="18"/>
<r s="337986" q="18"/>
<r s="337987" q="18"/>
<r s="337988" q="18"/>
<r s="337990" q="18"/>
<r s="337991" q="18"/>
<r s="337951" q="20"/>
<r s="337952" q="20"/>
<r s="337953" q="20"/>
<r s="337954" q="20"/>
<r s="337955" q="20"/>
<r s="337956" q="20"/>
<r s="337957" q="20"/>
<r s="337958" q="20"/>
<r s="337959" q="20"/>
<r s="337960" q="20"/>
<r s="337961" q="20"/>
<r s="337962" q="20"/>
<r s="337963" q="20"/>
<r s="337964" q="20"/>
<r s="338552" q="20"/>
<r s="262457" q="3"/>
<r s="262458" q="3"/>
<r s="262459" q="3"/>
<r s="262460" q="3"/>
<r s="262463" q="3"/>
<r s="262465" q="3"/>
<r s="262466" q="3"/>
<r s="262469" q="3"/>
<r s="286140" q="3"/>
<r s="286145" q="3"/>
<r s="288204" q="3"/>
<r s="286928" q="3"/>
<r s="337732" q="13"/>
<r s="337733" q="13"/>
<r s="309306" q="12"/>
<r s="309307" q="12"/>
<r s="309309" q="12"/>
<r s="132782" q="1"/>
<r s="132783" q="2"/>
<r s="132784" q="2"/>
';
set @xml = N'<rows>' + @xml + N'</rows>'

print 'Parsing XML...' + convert(varchar,getdate(),121)
exec sp_xml_preparedocument @hdoc OUTPUT, @xml

print 'Loading @whitelist...' + convert(varchar,getdate(),121)
insert into @whitelist (ChromeStyleId, Sequence)
select s as ChromeStyleId, q as Sequence
from OPENXML(@hdoc, '/rows/r', 1) WITH (s int, q int)

exec sp_xml_removedocument @hdoc

print 'Deleting existing values from chrome.PackageOptionCodeWhitelist...' + convert(varchar,getdate(),121)
delete from PkgWL
from chrome.PackageOptionCodeWhitelist PkgWL
where exists (select * 
              from @whitelist WL 
              where WL.ChromeStyleId = PkgWL.ChromeStyleId)

print 'Inserting new values to chrome.PackageOptionCodeWhiteList...' + convert(varchar,getdate(),121)
insert into Merchandising.chrome.PackageOptionCodeWhitelist (ChromeStyleId, ChromeOptionCode)
select wl.ChromeStyleId, o.OptionCode 
from @whitelist wl
join VehicleCatalog.chrome.Options o
on o.CountryCode = 1
    and wl.ChromeStyleId = o.StyleId
    and wl.Sequence = o.Sequence

print 'Done.' + convert(varchar,getdate(),121)
