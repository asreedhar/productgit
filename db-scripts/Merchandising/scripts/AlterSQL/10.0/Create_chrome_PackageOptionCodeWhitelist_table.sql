
use Merchandising
GO

if object_id('chrome.PackageOptionCodeWhitelist', 'U') is null
begin
    print 'Creating table...'
    create table chrome.PackageOptionCodeWhitelist
    (
        ChromeManufacturerID int not null,
        ChromeDivisionID int not null,  -- Make
        ChromeOptionCode varchar(20) not null,
        constraint PK_PackageOptionCodeWhitelist primary key clustered (ChromeManufacturerID, ChromeDivisionID, ChromeOptionCode)
    )
    print 'Done.'
       
end
GO

