
DECLARE @intErrorCode INT
BEGIN TRAN
	delete from  [Merchandising].[templates].[OptionCodeRuleMappings] where ruleid in (7,8,9,10)
	delete from  [Merchandising].[templates].[OptionCodeRules] where ruleid in (7,8,9,10)
	--first add the rules
	INSERT INTO [Merchandising].[templates].[OptionCodeRules]
           ([ruleId]
           ,[textToReplace]
           ,[textToReplaceWith])
     
	
		select 7,'6 month subscription',''     
		union all
		select 8, 'w/6 month subscription',''
		union all
		select 9, '6-month subscription',''
		union all
		select 10, 'w/6-month subscription',''

	
	SELECT @intErrorCode = @@ERROR
    IF (@intErrorCode <> 0) GOTO PROBLEM

	--then the mapping
	INSERT INTO [Merchandising].[templates].[OptionCodeRuleMappings]
           ([optionCode]
           ,[businessunitid]
           ,[ruleId]
           ,[inventoryType])
	 
		select '17M', 100150 , 7 ,2
		union all 
		select '62S', 100150 , 7 ,2
		union all 
		select '96B', 100150 , 7 ,2

		union all
		select '1MC', 100150 , 8 ,2
		union all 
		select '502A', 100150 , 8 ,2
		union all 
		select '58N', 100150 , 8 ,2

		union all
		select '000500', 100150 , 9 ,2
		union all 
		select '1SA', 100150 , 9 ,2
		union all 
		select '58N', 100150 , 9 ,2
		union all
		select '62S', 100150 , 9 ,2
		union all 
		select '65R', 100150 , 9 ,2
		union all 
		select '91S', 100150 , 9 ,2
		union all
		select 'P1', 100150 , 9 ,2
		union all 
		select 'P2', 100150 , 9 ,2
		union all
		select 'RX6', 100150 , 9 ,2
		union all 
		select 'SAT', 100150 , 9 ,2
		union all

		select '-AP', 100150 , 10 ,2
 		union all 
 		select '-MP', 100150 , 10 ,2
 		union all 
 		select '-PP', 100150 , 10 ,2
 		union all 
 		select '-SEL', 100150 , 10 ,2
 		union all 
 		select '-TP', 100150 , 10 ,2
 		union all 
 		select '101A', 100150 , 10 ,2
 		union all 
 		select '102A-F', 100150 , 10 ,2
 		union all 
 		select '102A-R', 100150 , 10 ,2
 		union all 
 		select '103A', 100150 , 10 ,2
 		union all 
 		select '1MC', 100150 , 10 ,2
 		union all 
 		select '1MT', 100150 , 10 ,2
 		union all 
 		select '1NV', 100150 , 10 ,2
 		union all 
 		select '1PR', 100150 , 10 ,2
 		union all 
 		select '1TE', 100150 , 10 ,2
 		union all 
 		select '203A', 100150 , 10 ,2
 		union all 
 		select '2PR', 100150 , 10 ,2
 		union all 
 		select '301A', 100150 , 10 ,2
 		union all 
 		select '301A-F', 100150 , 10 ,2
 		union all 
 		select '301A-R', 100150 , 10 ,2
 		union all 
 		select '302A', 100150 , 10 ,2
 		union all 
 		select '303A', 100150 , 10 ,2
 		union all 
 		select '47E', 100150 , 10 ,2
 		union all 
 		select '52N', 100150 , 10 ,2
 		union all 
 		select '581', 100150 , 10 ,2
 		union all 
 		select '58N', 100150 , 10 ,2
 		union all 
 		select '58Q', 100150 , 10 ,2
 		union all 
 		select '58V', 100150 , 10 ,2
 		union all 
 		select '58X', 100150 , 10 ,2
 		union all 
 		select '61N', 100150 , 10 ,2
 		union all 
 		select '85N', 100150 , 10 ,2
 		union all 
 		select 'MCA61', 100150 , 10 ,2
 		union all 
 		select 'MCA69', 100150 , 10 ,2
 		union all 
 		select 'MCA89', 100150 , 10 ,2
 		union all 
 		select 'MCB06', 100150 , 10 ,2
 		union all 
 		select 'MCB17', 100150 , 10 ,2
 		union all 
 		select 'MCB58', 100150 , 10 ,2
 		union all 
 		select 'P3', 100150 , 10 ,2
 		union all 
 		select 'P3-1', 100150 , 10 ,2
 		union all 
 		select 'P4', 100150 , 10 ,2
 		union all 
 		select 'PS', 100150 , 10 ,2
		

	SELECT @intErrorCode = @@ERROR
		IF (@intErrorCode <> 0) GOTO PROBLEM
Commit Tran

PROBLEM:
IF (@intErrorCode <> 0) BEGIN
PRINT 'Unexpected error occurred!'
    ROLLBACK TRAN
END