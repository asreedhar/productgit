
use Merchandising
GO

if object_id('chrome.PackageOptionCodeWhitelist', 'U') is not null
begin
    print 'Dropping table...'
    drop table chrome.PackageOptionCodeWhitelist
    print 'Done.'
end

print 'Creating table...'
create table chrome.PackageOptionCodeWhitelist
(
    ChromeStyleID int not null,
    ChromeOptionCode varchar(20) not null,
    constraint PK_PackageOptionCodeWhitelist primary key clustered (ChromeStyleID, ChromeOptionCode)
)
print 'Done.'

GO

