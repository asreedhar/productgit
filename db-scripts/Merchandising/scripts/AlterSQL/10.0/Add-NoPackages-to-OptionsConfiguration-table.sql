use Merchandising

if not exists(select * 
              from INFORMATION_SCHEMA.columns 
              where TABLE_SCHEMA = 'builder' and TABLE_NAME = 'OptionsConfiguration' and COLUMN_NAME = 'NoPackages')
begin
	print 'Adding NoPackages field to OptionsConfiguration'
	alter table builder.OptionsConfiguration 
	    add NoPackages bit not null constraint DF_OptionsConfiguration_NoPackages default (0)
    print 'Done.'
end