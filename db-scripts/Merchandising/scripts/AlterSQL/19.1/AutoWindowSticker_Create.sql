if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[AutoWindowSticker]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table [settings].[AutoWindowSticker]
GO

CREATE TABLE [settings].[AutoWindowSticker](
	AutoWindowStickerId int IDENTITY(1,1) NOT NULL,
	BusinessUnitId	int not null,
	Email	varchar(1000),
	EmailBounced	bit not null default 0,
	CreatedOn	smalldatetime  not null default getdate(),
	CreatedBy	varchar(80),
	UpdatedOn	smalldatetime not null default getdate(),
	UpdatedBy	varchar(80)	
)
GO 

ALTER TABLE settings.AutoWindowSticker
	ADD CONSTRAINT PK_AutoWindowSticker
	PRIMARY KEY NONCLUSTERED (AutoWindowStickerId) 
GO

-- unique for now, only one per business unit
CREATE UNIQUE INDEX IX_AutoWindowSticker_BusinessUnitID ON settings.AutoWindowSticker(BusinessUnitID)
go
