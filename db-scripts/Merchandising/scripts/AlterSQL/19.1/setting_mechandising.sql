-- if column exists ... remove it
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'autoWindowStickerTemplateId'
				AND object_id = object_id('settings.merchandising'))
	begin
		ALTER TABLE settings.merchandising
			DROP COLUMN autoWindowStickerTemplateId
	end

-- add column for auto windowSticker, FB: 29694
ALTER TABLE settings.merchandising
	ADD autoWindowStickerTemplateId int NULL
GO
