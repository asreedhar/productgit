DECLARE @TemplateInfo TABLE (
    idx smallint Primary Key IDENTITY(1,1),
    blurbTextId int,
    businessUnitId int
)


INSERT INTO @TemplateInfo (blurbTextId, businessUnitId)
    SELECT MAX(blurbTextID), businessUnitId
    FROM templates.BlurbSampleText bst
    WHERE ((vehicleProfileId IS NULL) OR (vehicleProfileId = 5))
    GROUP BY businessUnitId


-- SELECT * FROM @TemplateInfo


DECLARE @i int,
        @numrows int,
        @BusinessUnitId int,
        @blurbPreText varchar(2000)

SET @i = 1
SET @numrows = (SELECT COUNT(*) FROM @TemplateInfo)
IF @numrows > 0
    WHILE (@i <= (SELECT MAX(idx) FROM @TemplateInfo))
    BEGIN
        -- get the next employee primary key
        SELECT @BusinessUnitId = (SELECT businessUnitId FROM @TemplateInfo WHERE idx = @i),
                @blurbPreText = (
                    SELECT blurbPreText FROM templates.BlurbSampleText WHERE blurbTextID = ( 
                        SELECT blurbTextID FROM @TemplateInfo WHERE idx = @i
                    )
                )

        --
        -- do something with this bu
        --

		IF EXISTS(SELECT 1 FROM Market.Pricing.Owner O WHERE OwnerTypeID = 1 AND OwnerEntityID = @BusinessUnitID)
		BEGIN
			Declare @Results Table (
				[Id]                    INT             NOT NULL,
				[Handle]                VARCHAR(36)     NOT NULL,
				[Name]                  VARCHAR(200)    NOT NULL,
				[OwnerEntityTypeId]     INT             NOT NULL,
				[OwnerEntityId]         INT             NOT NULL
			);

			DECLARE @OwnerId int, 
					@OwnerHandle varchar(36);

			INSERT INTO @Results
			EXEC [Market].[Pricing].[Owner#FetchByDealerId] @BusinessUnitId

			SELECT @OwnerId = Id, @OwnerHandle = Handle FROM @Results

			IF(@OwnerId > 0)
			BEGIN
				IF NOT EXISTS ( SELECT * FROM IMT.Marketing.DealerGeneralPreference WHERE OwnerID = @OwnerId)
				BEGIN
					DECLARE @DealerResults TABLE
					(
						Name VARCHAR(64),
						Address1 VARCHAR(100),
						Address2 VARCHAR(100),
						City VARCHAR(50),
						State VARCHAR(3),
						ZipCode VARCHAR(10),
						PhoneNumber	VARCHAR(50)
					)
					INSERT INTO @DealerResults
					exec IMT.Marketing.DealerGeneralPreference#Create @OwnerHandle -- Make sure we have general preferences for the dealer
					
					
					IF EXISTS(SELECT * FROM @DealerResults)
					BEGIN
						DECLARE @AddressLine1 VARCHAR(500),
								@AddressLine2 VARCHAR(500),
								@City VARCHAR(500),
								@DisplayContactInfoOnWebPDF BIT,
								@StateCode VARCHAR(2),
								@ZipCode VARCHAR(5),
								@Url VARCHAR(500),
								@SalesEmailAddress VARCHAR(500),
								@SalesPhoneNumber VARCHAR(14),
								@ExtendedTagLine VARCHAR(250),
								@DaysValidFor INT,
								@Disclaimer VARCHAR(500),    
								@InsertUser VARCHAR(80);
								
						SELECT TOP 1 @AddressLine1=Address1, @AddressLine2=Address2, @City=City, @StateCode=State, @ZipCode=ZipCode, @SalesPhoneNumber=PhoneNumber
						FROM @DealerResults;
						
						exec IMT.Marketing.DealerGeneralPreference#Insert @OwnerHandle,
							@AddressLine1,
							@AddressLine2,
							@City,
							0,
							@StateCode,
							@ZipCode,
							'',
							'',
							@SalesPhoneNumber,
							'',
							0,
							'',
							'max_user'
					END
				END
	            
				-- Update the dealer's ExtendedTagLine with this new text
				UPDATE IMT.Marketing.DealerGeneralPreference
				SET ExtendedTagLine = @blurbPreText
				WHERE OwnerID = @OwnerId
			END
        END
                

        -- increment counter for next bu
        SET @i = @i + 1
    END
