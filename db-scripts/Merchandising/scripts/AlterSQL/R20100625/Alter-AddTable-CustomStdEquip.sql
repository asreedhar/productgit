SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [settings].[CustomStandardEquipment](
	[businessUnitId] [int] NOT NULL,
	[UsedNew] [int] NOT NULL,
	[OptionCode] [nvarchar](10) NOT NULL,
	[CustomText] [nvarchar](255) NULL
	CONSTRAINT FK_CustomStandardEquipment FOREIGN KEY (businessUnitId) REFERENCES Merchandising.settings.Merchandising (businessUnitID)
	CONSTRAINT PK_CustomStandardEquipment PRIMARY KEY (businessUnitId, UsedNew, OptionCode)
)

GO