ALTER TABLE postings.AdvertisementStatuses
ADD CONSTRAINT [PK_postings_AdvertisementStatuses] PRIMARY KEY CLUSTERED ([statusId])
GO

ALTER TABLE postings.AdvertisementStatuses
ADD CONSTRAINT [UK_postings_AdvertisementStatuses__StatusDescription] UNIQUE (statusDescription)
GO

ALTER TABLE postings.VehicleAdScheduling
ADD CONSTRAINT [PK_VehicleAdScheduling] PRIMARY KEY NONCLUSTERED ([schedulingID])
GO

ALTER TABLE postings.VehicleAdScheduling
ADD CONSTRAINT [FK_VehicleAdScheduling_advertisementStatus__postings_AdvertisementStatuses]
FOREIGN KEY (advertisementStatus) REFERENCES postings.AdvertisementStatuses(statusId)
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_VehicleAdScheduling__SchedulingID_AdvertisementStatus] 
ON postings.VehicleAdScheduling(schedulingID, advertisementStatus)
GO
