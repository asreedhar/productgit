IF NOT EXISTS (SELECT *
				FROM sys.objects o
				JOIN sys.columns c ON o.object_id = c.object_id
				WHERE o.object_id = OBJECT_ID(N'[merchandising].[AutoTraderExSummary]') 
				AND o.type in (N'U')
				AND c.name = 'ViewedMaps')
BEGIN 

ALTER TABLE [merchandising].[AutoTraderExSummary]
ADD [ViewedMaps] int NULL

END
