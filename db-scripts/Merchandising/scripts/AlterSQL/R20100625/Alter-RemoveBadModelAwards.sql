--Add segment column to ModelAwards table
ALTER TABLE Merchandising.merchandising.ModelAwards
ADD segment text NULL
GO

--Populate segment column
UPDATE Merchandising.merchandising.ModelAwards
SET segment = 'Coupe'
WHERE awardText LIKE '%Coupe%'
UPDATE Merchandising.merchandising.ModelAwards
SET segment = 'Sedan'
WHERE awardText LIKE '%Sedan%'
UPDATE Merchandising.merchandising.ModelAwards
SET segment = 'Wagon'
WHERE awardText LIKE '%Wagon%'
UPDATE Merchandising.merchandising.ModelAwards
SET segment = 'Convertible'
WHERE awardText LIKE '%Convertible%'

--Create temporary table to store list of duplicate cars
CREATE TABLE #TempAwards(
	make varchar(30) NOT NULL,
	model varchar(30) NOT NULL,
	modelYear int NOT NULL,
	trim varchar(30) NOT NULL,
	awardText varchar(200) NULL,
	PRIMARY KEY (modelYear,make,model,trim)
)

--Create temporary table to store list of duplicate cars and their associated segments
CREATE TABLE #TempSegments(
	make varchar(30) NOT NULL,
	model varchar(30) NOT NULL,
	modelYear int NOT NULL,
	trim varchar(30) NOT NULL,
	segment varchar(30) NOT NULL
)

--Insert list of duplicate cars into temp table
INSERT INTO #TempAwards (make, model, modelYear, trim)
SELECT make, model, modelYear, trim
FROM Merchandising.merchandising.ModelAwards
WHERE (awardText LIKE '%Coupe%'
		OR awardText LIKE '%Sedan%'
		OR awardText LIKE '%Wagon%'
		OR awardText LIKE '%Convertible%')
GROUP BY make, model, modelYear, trim
HAVING (COUNT(make) > 1)
	AND (COUNT(model) > 1)
	AND (COUNT(modelYear) > 1)
	AND (COUNT(trim) > 1)
ORDER BY  modelYear, make, model, trim

--Insert duplicate cars and their segments into temporary table
INSERT INTO #TempSegments (make, model, modelYear, trim, segment)
SELECT ma.make, ma.model, ma.modelYear, ma.trim, ma.segment
FROM Merchandising.merchandising.ModelAwards ma
JOIN #TempAwards temp
	ON temp.make = ma.make
		AND temp.model = ma.model
		AND temp.modelYear = ma.modelYear
		AND temp.trim = ma.trim
WHERE ma.segment IS NOT NULL
ORDER BY modelYear, make, model, trim

--Eliminate duplicate cars with all their segments
SELECT DISTINCT *
INTO #DistinctSegments
FROM #TempSegments

--Remove vehicles that only have 1 segment
SELECT make, modelYear, model, trim
INTO #DuplicateSegments
FROM #DistinctSegments
GROUP BY make, model, modelYear, trim
HAVING COUNT(segment) > 1

--Delete from table
WHILE EXISTS(SELECT TOP 1 * FROM #DuplicateSegments)
BEGIN
	DELETE FROM Merchandising.merchandising.modelAwards
	WHERE Merchandising.merchandising.modelAwards.make = (SELECT TOP 1 #DuplicateSegments.make FROM #DuplicateSegments)
		AND Merchandising.merchandising.modelAwards.modelYear = (SELECT TOP 1 #DuplicateSegments.modelYear FROM #DuplicateSegments)
		AND Merchandising.merchandising.modelAwards.model = (SELECT TOP 1 #DuplicateSegments.model FROM #DuplicateSegments)
		AND Merchandising.merchandising.modelAwards.trim = (SELECT TOP 1 #DuplicateSegments.trim FROM #DuplicateSegments)
	DELETE FROM #DuplicateSegments
	WHERE #DuplicateSegments.make = (SELECT TOP 1 #DuplicateSegments.make FROM #DuplicateSegments)
		AND #DuplicateSegments.modelYear = (SELECT TOP 1 #DuplicateSegments.modelYear FROM #DuplicateSegments)
		AND #DuplicateSegments.model = (SELECT TOP 1 #DuplicateSegments.model FROM #DuplicateSegments)
		AND #DuplicateSegments.trim = (SELECT TOP 1 #DuplicateSegments.trim FROM #DuplicateSegments)
END

--Clean up temporary tables
DROP TABLE #TempAwards
DROP TABLE #TempSegments
DROP TABLE #DistinctSegments
DROP TABLE #DuplicateSegments

--Delete extra segment column
ALTER TABLE Merchandising.merchandising.ModelAwards
DROP COLUMN segment
GO