SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [settings].[CustomDisclaimer](
	[businessUnitId] [int] NOT NULL,
	[isAppend] [bit] NOT NULL,
	[customText] [nvarchar](255) NULL
	CONSTRAINT FK_CustomDisclaimer FOREIGN KEY (businessUnitId) REFERENCES Merchandising.settings.Merchandising (businessUnitID)
	CONSTRAINT PK_CustomDisclaimer PRIMARY KEY (businessUnitId)
)

GO