UPDATE settings.EdtDestinations
	SET	active = 0
	WHERE description not like 'Cars.com'
	AND description not like 'AutoTrader.com'
	AND description not like 'eBizAutos'