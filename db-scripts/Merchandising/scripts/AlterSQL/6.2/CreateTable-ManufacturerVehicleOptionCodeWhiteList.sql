IF (OBJECT_ID('dbo.ManufacturerVehicleOptionCodeWhiteList') IS NULL)
BEGIN

CREATE TABLE dbo.ManufacturerVehicleOptionCodeWhiteList(
	VehicleManufacturerId int NOT NULL,
	VehicleMakeId int NOT NULL,
	OptionCode varchar(20) NOT NULL,
 CONSTRAINT PK_ManufacturerVehicleOptionCodeWhiteList PRIMARY KEY CLUSTERED 
(
	VehicleManufacturerId ASC,
	VehicleMakeId ASC,
	OptionCode ASC
)
)

END

GO


IF (OBJECT_ID('dbo.FK_ManufacturerVehicleOptionCodeWhiteList_cpoGroups') IS NULL)
ALTER TABLE dbo.ManufacturerVehicleOptionCodeWhiteList  WITH CHECK ADD  CONSTRAINT FK_ManufacturerVehicleOptionCodeWhiteList_cpoGroups FOREIGN KEY(VehicleManufacturerId)
REFERENCES settings.cpoGroups (cpoGroupId)
GO


IF (OBJECT_ID('dbo.FK_ManufacturerVehicleOptionCodeWhiteList_manufacturerCertifications') IS NULL)
ALTER TABLE dbo.ManufacturerVehicleOptionCodeWhiteList  WITH CHECK ADD  CONSTRAINT FK_ManufacturerVehicleOptionCodeWhiteList_manufacturerCertifications FOREIGN KEY(VehicleMakeId)
REFERENCES builder.manufacturerCertifications (certificationTypeId)
GO
