if object_id('settings.PK_cpoGroups') is null

ALTER TABLE [settings].[cpoGroups]
ADD CONSTRAINT [PK_cpoGroups] 
PRIMARY KEY CLUSTERED ( [cpoGroupId] ASC )