--- Add new queries for referrals
DECLARE @QueryType int;
DECLARE @Destination int;
SELECT @QueryType = QueryTypeId FROM settings.GoogleQueryType WHERE Name = 'Referrals';
SELECT @Destination = destinationID FROM settings.EdtDestinations ed WHERE ed.description = 'Undefined';

-- First delete existing 
DECLARE @QueriesToDelete TABLE (
	QueryId int
);

INSERT INTO @QueriesToDelete
SELECT QueryId FROM settings.GoogleQuery WHERE QueryType = @QueryType AND Metrics = 'ga:visits,ga:percentNewVisits,ga:visitBounceRate,ga:avgTimeOnSite,ga:pageviewsPerVisit';

DELETE FROM settings.EdtDestinationQueries WHERE GoogleQueryId IN (SELECT QueryId FROM @QueriesToDelete) AND EdtDestinationId = @Destination;
DELETE FROM settings.GoogleQuery WHERE QueryId IN (SELECT QueryId FROM @QueriesToDelete);

-- Now, insert new records
IF NOT EXISTS ( SELECT * FROM Merchandising.settings.GoogleQuery WHERE [QueryName] IN ('Sources','Keywords','Mobile') )
BEGIN
	INSERT [settings].[GoogleQuery] ([QueryType], [QueryName], [Dimensions], [Filter], [Limit], [Metrics], [Segment], [Sort]) 
	VALUES (@QueryType, N'Sources', N'ga:source,ga:medium', N'ga:keyword!=(not set)', 50, N'ga:visits,ga:bounces,ga:newVisits,ga:timeOnSite,ga:visitors,ga:pageviews,ga:timeOnPage,ga:uniquePageviews,ga:exits,ga:entrances', N'gaid::-5', N'-ga:visits')
	INSERT [settings].[GoogleQuery] ([QueryType], [QueryName], [Dimensions], [Filter], [Limit], [Metrics], [Segment], [Sort]) 
	VALUES (@QueryType, N'Keywords', N'ga:keyword', N'ga:keyword!=(not set)', 50, N'ga:visits,ga:bounces,ga:newVisits,ga:timeOnSite,ga:visitors,ga:pageviews,ga:timeOnPage,ga:uniquePageviews,ga:exits,ga:entrances', N'gaid::-5', N'-ga:visits')
	INSERT [settings].[GoogleQuery] ([QueryType], [QueryName], [Dimensions], [Filter], [Limit], [Metrics], [Segment], [Sort]) 
	VALUES (@QueryType, N'Mobile', N'ga:mobileDeviceInfo', N'ga:keyword!=(not set)', 50, N'ga:visits,ga:bounces,ga:newVisits,ga:timeOnSite,ga:visitors,ga:pageviews,ga:timeOnPage,ga:uniquePageviews,ga:exits,ga:entrances', N'gaid::-5', N'-ga:visits')
END
GO

IF NOT EXISTS (
	SELECT * FROM settings.[EdtDestinationQueries] WHERE GoogleQueryId IN (
		SELECT QueryId FROM settings.[GoogleQuery] WHERE [QueryName] IN  ('Sources','Keywords','Mobile')
	)
)
BEGIN
	DECLARE @Destination int;
	SELECT @Destination = destinationID FROM settings.EdtDestinations ed WHERE ed.description = 'Undefined';
	INSERT [settings].[EdtDestinationQueries] (EdtDestinationId, GoogleQueryId) VALUES (0, (SELECT [QueryId] FROM [settings].[GoogleQuery] WHERE [QueryName] = 'Sources'))
	INSERT [settings].[EdtDestinationQueries] (EdtDestinationId, GoogleQueryId) VALUES (0, (SELECT [QueryId] FROM [settings].[GoogleQuery] WHERE [QueryName] = 'Keywords'))
	INSERT [settings].[EdtDestinationQueries] (EdtDestinationId, GoogleQueryId) VALUES (0, (SELECT [QueryId] FROM [settings].[GoogleQuery] WHERE [QueryName] = 'Mobile'))
END
GO

