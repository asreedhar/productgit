DECLARE @AlreadyDone int;

WITH WLSelect AS (
	select distinct ma.manufacturerId, di.divisionId, op.optionCode
	from vehicleCatalog.chrome.options op
	join vehicleCatalog.chrome.styles st on st.styleID = op.styleId
	join vehicleCatalog.chrome.models mo on mo.modelId = st.modelId
	join vehicleCatalog.chrome.divisions di on di.divisionId = mo.divisionId
	join vehicleCatalog.chrome.manufacturers ma on ma.manufacturerId = di.manufacturerId
	where ma.countryCode=1
		and ma.manufacturerid = 7 -- Chrysler
		and di.countryCode=1
		and op.countryCode=1
		and st.countryCode=1
		AND op.OptionCode IN (
			'RA4',
			'25Z'
		)
)

SELECT @AlreadyDone = COUNT(*) 
FROM merchandising.dbo.ManufacturerVehicleOptionCodeWhiteList WL
JOIN WLSelect ws ON ws.DivisionID = WL.VehicleMakeId 
	AND ws.ManufacturerID = WL.VehicleManufacturerId 
	AND ws.OptionCode = WL.OptionCode

IF @AlreadyDone = 0
BEGIN
	BEGIN TRAN;
	INSERT INTO Merchandising.dbo.ManufacturerVehicleOptionCodeWhiteList (vehicleManufacturerId, vehicleMakeId, optionCode)
	SELECT DISTINCT ma.manufacturerId, di.divisionId, op.optionCode
	FROM vehicleCatalog.chrome.options op
	JOIN vehicleCatalog.chrome.styles st on st.styleID = op.styleId
	JOIN vehicleCatalog.chrome.models mo on mo.modelId = st.modelId
	JOIN vehicleCatalog.chrome.divisions di on di.divisionId = mo.divisionId
	JOIN vehicleCatalog.chrome.manufacturers ma on ma.manufacturerId = di.manufacturerId
	WHERE ma.countryCode=1
		AND ma.manufacturerid = 7 -- Chrysler
		AND di.countryCode=1
		AND op.countryCode=1
		AND st.countryCode=1
		AND op.OptionCode IN (
			'RA4',
			'25Z'
		)
	COMMIT TRAN;
END
ELSE
	SELECT 'Already done'
	
