UPDATE [settings].[GoogleQuery]
SET [Filter] =  N'ga:keyword!=(not set)', [Segment] = N'gaid::-5' WHERE [QueryName] = 'Keywords';

UPDATE [settings].[GoogleQuery]
SET [Filter] = NULL, [Segment] = NULL WHERE [QueryName] = 'Sources';

UPDATE [settings].[GoogleQuery]
SET [Filter] = NULL, [Segment] = NULL WHERE [QueryName] = 'Mobile';
