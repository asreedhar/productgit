INSERT INTO Merchandising.Templates.OptionCodeRules (RuleID, TextToReplace, TextToReplaceWith)
VALUES (14, 'subscription', 'subscription from original in-service date')
GO

-- Using BU code 100150 seems to mean 'all business units' so we're going with that here too
INSERT INTO Merchandising.Templates.OptionCodeRuleMappings (OptionCode, BusinessUnitID, RuleID, InventoryType)
--        List of option codes, All BUs, RuleId, NewAndUsed VehicleType
SELECT '639', 100150, 14, 0 UNION ALL
SELECT '655', 100150, 14, 0 UNION ALL
SELECT '6NL', 100150, 14, 0 UNION ALL
SELECT '7HB', 100150, 14, 0 UNION ALL
SELECT 'ZEC', 100150, 14, 0 UNION ALL
SELECT 'ZMV', 100150, 14, 0 UNION ALL
SELECT 'ZP2', 100150, 14, 0 UNION ALL
SELECT 'ZPP', 100150, 14, 0 UNION ALL
SELECT 'ZPS', 100150, 14, 0 UNION ALL
SELECT 'ZPS-F', 100150, 14, 0 UNION ALL
SELECT 'ZPS-R', 100150, 14, 0 UNION ALL
SELECT 'ZTC', 100150, 14, 0 UNION ALL
SELECT 'ZTE', 100150, 14, 0 UNION ALL
SELECT 'ZVP', 100150, 14, 0 
GO


