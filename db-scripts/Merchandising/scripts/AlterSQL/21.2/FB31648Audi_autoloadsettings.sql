-- audi site and autoload

if EXISTS (
		SELECT * 
			FROM settings.credentialedSites
			WHERE siteId = 8
		)
	begin
		UPDATE merchandising.VehicleAutoLoadSettings
			SET siteID = null, isAutoLoadEnabled = 0
			WHERE siteID = 8
			
		DELETE builder.BuildRequestLog
			WHERE siteID = 8

		DELETE settings.Dealer_SiteCredentials
			WHERE siteID = 8
	
		DELETE settings.credentialedSites
			WHERE siteId = 8
	end
go
-- insert new site
SET IDENTITY_INSERT settings.credentialedSites ON

INSERT INTO settings.credentialedSites
	(siteId, siteName, DealerCodeRequired)
	VALUES(8, 'Access Audi', 0)
go
	
SET IDENTITY_INSERT settings.credentialedSites OFF
go
	
-- set VehicleAutoLoadSettings site for audi
UPDATE merchandising.VehicleAutoLoadSettings
	SET siteId = 8, isAutoLoadEnabled = 1
	WHERE ManufacturerID = 4

