-- FB 31619 - add new autoloadStatusType

if NOT EXISTS (
				SELECT *
					FROM merchandising.builder.AutoloadStatusTypes
					WHERE statusTypeId = -89
	)
		begin
			INSERT INTO merchandising.builder.AutoloadStatusTypes
				(statusTypeId, statusType, statusTypeDescription, requeueMinutes)
				VALUES(-89, 'Cannot Decode Style', 'The style cannot be determined from the build data', 1440)
		end
		


if NOT EXISTS (
				SELECT *
					FROM merchandising.builder.AutoloadStatusTypes
					WHERE statusTypeId = -88
	)
		begin
			INSERT INTO merchandising.builder.AutoloadStatusTypes
				(statusTypeId, statusType, statusTypeDescription, requeueMinutes)
				VALUES(-88, 'Cannot Decode Ext Color', 'The exterior color cannot be determined from the build data', null)
		end
