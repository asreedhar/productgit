-- FB 31866 - optional dealer code for Chrysler spider
UPDATE settings.CredentialedSites
	SET DealerCodeRequired = 2
	WHERE siteId = 3

