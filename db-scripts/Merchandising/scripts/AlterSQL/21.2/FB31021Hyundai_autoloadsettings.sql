-- hyundia site and autoload

if EXISTS (
		SELECT * 
			FROM settings.credentialedSites
			WHERE siteId = 9
		)
	begin
		UPDATE merchandising.VehicleAutoLoadSettings
			SET siteID = null, isAutoLoadEnabled = 0
			WHERE siteID = 9
			
		DELETE builder.BuildRequestLog
			WHERE siteID = 9

		DELETE settings.Dealer_SiteCredentials
			WHERE siteID = 9

		DELETE settings.credentialedSites
			WHERE siteId = 9
	end
go
-- insert new site
SET IDENTITY_INSERT settings.credentialedSites ON

INSERT INTO settings.credentialedSites
	(siteId, siteName, DealerCodeRequired)
	VALUES(9, 'Hyundai Tech Info', 1)
go
	
SET IDENTITY_INSERT settings.credentialedSites OFF
go
	
-- set VehicleAutoLoadSettings site for audi
UPDATE merchandising.VehicleAutoLoadSettings
	SET siteId = 9, isAutoLoadEnabled = 1
	WHERE ManufacturerID = 10


