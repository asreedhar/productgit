-- drop column DealerCodeRequired
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'DealerCodeRequired'
				AND object_id = object_id('settings.credentialedSites'))
	begin
		ALTER TABLE settings.credentialedSites
			DROP COLUMN DealerCodeRequired
	end
go

-- add column DealerCodeRequired
ALTER TABLE settings.credentialedSites
	ADD DealerCodeRequired tinyint null
go

-- update all to zero
UPDATE settings.credentialedSites
	SET DealerCodeRequired = 0
go

-- make non-null
ALTER TABLE settings.credentialedSites
	ALTER COLUMN DealerCodeRequired tinyint not null
go

