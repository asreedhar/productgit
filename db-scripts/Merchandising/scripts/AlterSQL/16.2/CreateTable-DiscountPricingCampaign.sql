USE [Merchandising]
GO

/****** Object:  Table [merchandising].[DiscountPricingCampaign]    Script Date: 07/18/2013 11:32:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[Merchandising].[merchandising].[DiscountPricingCampaign]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [Merchandising].[merchandising].[DiscountPricingCampaign](

	[CampaignID] int IDENTITY(1,1) NOT NULL,
	[BusinessUnitId] int NOT NULL,
	[DiscountType] int NOT NULL,
	[DiscountValue] decimal(9,2) NOT NULL,
	[CreationDate] datetime NOT NULL,
	[ExpirationDate] datetime NOT NULL,
	[CreatedBy] varchar(250) NOT NULL,
	[CampaignFilter] varchar(max) NULL
	
	CONSTRAINT [PK_merchandising_DiscountPricingCampaign] PRIMARY KEY NONCLUSTERED 
	(
		[CampaignID] ASC
	)
)
END

GO

