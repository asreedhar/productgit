ALTER TABLE postings.VehicleAdScheduling 
ADD MSRP INT NULL
GO

ALTER TABLE postings.VehicleAdScheduling 
ADD RebateAmount INT NULL
GO

ALTER TABLE postings.VehicleAdScheduling 
ADD IncentiveAmount INT NULL
GO

