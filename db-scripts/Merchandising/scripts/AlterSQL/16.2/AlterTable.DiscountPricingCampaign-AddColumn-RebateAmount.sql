USE Merchandising
GO

IF NOT EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'RebateAmount' AND Object_ID = Object_ID(N'merchandising.DiscountPricingCampaign'))
BEGIN    
ALTER TABLE merchandising.DiscountPricingCampaign 
	ADD RebateAmount DECIMAL(9,2) DEFAULT 0 NOT NULL
END
GO

