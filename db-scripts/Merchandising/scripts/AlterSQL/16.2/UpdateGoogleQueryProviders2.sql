
update merchandising.settings.googlequery set filter ='ga:pagePath=~detail.aspx\?id=[0-9]{3\,}.+aspx' where queryname = 'Dealer Car Search-VDP-Both';
update merchandising.settings.googlequery set filter ='ga:pagePath=~detail.aspx\?id=[0-9]{3\,}.+aspx;ga:pageTitle=~- New ' where queryname = 'Dealer Car Search-VDP-New';
update merchandising.settings.googlequery set filter ='ga:pagePath=~detail.aspx\?id=[0-9]{3\,}.+aspx;ga:pageTitle=~- Used|- Certified  ' where queryname = 'Dealer Car Search-VDP-Used';
update merchandising.settings.googlequery set filter ='ga:pagePath=~auto[/]used-[0-9]{4\,4}.+[/][0-9]{6\,}[/]|[/]auto[/]new-[0-9]{4\,4}.+[/][0-9]{6\,}[/]' where queryname = 'Dealer eProcess-VDP-Both';
update merchandising.settings.googlequery set filter ='ga:pagePath=~auto[/]new-[0-9]{4\,4}.+[/][0-9]{6\,}[/]' where queryname = 'Dealer eProcess-VDP-New';
update merchandising.settings.googlequery set filter ='ga:pagePath=~auto[/]used-[0-9]{4\,4}.+[/][0-9]{6\,}[/]' where queryname = 'Dealer eProcess-VDP-Used';
update merchandising.settings.googlequery set filter ='ga:pagePath=~new-.+-[0-9]{4\,4}-.+[0-9A-Z]{17}|used-.+-[0-9]{4\,4}-.+[0-9A-Z]{17}' where queryname = 'DealerOn-VDP-Both';
update merchandising.settings.googlequery set filter ='ga:pagePath=~new-.+-[0-9]{4\,4}-.+[0-9A-Z]{17}' where queryname = 'DealerOn-VDP-New';
update merchandising.settings.googlequery set filter ='ga:pagePath=~used-.+-[0-9]{4\,4}-.+[0-9A-Z]{17}' where queryname = 'DealerOn-VDP-Used';
update merchandising.settings.googlequery set filter ='ga:pagePath=~in-stock[/][0-9]{4\,4}.+-[0-9]{6\,}' where queryname = 'DealerPeak-VDP-Both';
update merchandising.settings.googlequery set filter ='ga:pagePath=~new[/].+[/]in-stock[/][0-9]{4\,4}.+-[0-9]{6\,}' where queryname = 'DealerPeak-VDP-New';
update merchandising.settings.googlequery set filter ='ga:pagePath=~used[/].+[/]in-stock[/][0-9]{4\,4}.+-[0-9]{6\,}' where queryname = 'DealerPeak-VDP-Used';
update merchandising.settings.googlequery set filter ='ga:pagePath=~inventory[/]details[/][0-9]{4\,4}-.+[/]\?vehId=' where queryname = 'iPublishers-VDP-Both';
update merchandising.settings.googlequery set filter ='ga:pagePath=~inventory[/]details[/][0-9]{4\,4}-.+New[/]\?vehId=' where queryname = 'iPublishers-VDP-New';
update merchandising.settings.googlequery set filter ='ga:pagePath=~inventory[/]details[/][0-9]{4\,4}-.+Used[/]\?vehId=' where queryname = 'iPublishers-VDP-Used';
update merchandising.settings.googlequery set filter ='ga:eventCategory==Vehicle;ga:eventAction==View;ga:eventLabel=~"NewUsed"' where queryname = 'Dealer Fire-VDP-Both';
update merchandising.settings.googlequery set filter ='ga:eventCategory==Vehicle;ga:eventAction==View;ga:eventLabel=~"NewUsed":"1"' where queryname = 'Dealer Fire-VDP-New';
update merchandising.settings.googlequery set filter ='ga:eventCategory==Vehicle;ga:eventAction==View;ga:eventLabel=~"NewUsed":"2"' where queryname = 'Dealer Fire-VDP-Used';
