IF NOT EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'HasBeenExpired' AND Object_ID = Object_ID(N'merchandising.DiscountPricingCampaign'))
BEGIN
ALTER TABLE Merchandising.merchandising.DiscountPricingCampaign
	ADD HasBeenExpired BIT
END
GO

