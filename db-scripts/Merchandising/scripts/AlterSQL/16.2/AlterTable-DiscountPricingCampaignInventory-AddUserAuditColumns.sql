IF NOT EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'ActivatedBy' AND Object_ID = Object_ID(N'merchandising.DiscountPricingCampaignInventory'))
BEGIN
ALTER TABLE Merchandising.merchandising.DiscountPricingCampaignInventory
	ADD ActivatedBy varchar(250) null
END
GO


IF NOT EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'DeactivatedBy' AND Object_ID = Object_ID(N'merchandising.DiscountPricingCampaignInventory'))
BEGIN
ALTER TABLE Merchandising.merchandising.DiscountPricingCampaignInventory
	ADD DeactivatedBy varchar(250) null
END
GO