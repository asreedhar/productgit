IF EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'DiscountValue' AND Object_ID = Object_ID(N'merchandising.DiscountPricingCampaign'))
BEGIN
	exec sp_rename 'Merchandising.merchandising.DiscountPricingCampaign.DiscountValue', 'DiscountAmount', 'COLUMN'
END
GO