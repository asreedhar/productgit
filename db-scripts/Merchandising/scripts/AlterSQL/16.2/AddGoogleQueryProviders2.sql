declare @googleQuery table (
	QueryId int not null,
	Provider varchar(50) not null,
	QueryType int not null,
	QueryName varchar(50) not null,
	Dimensions varchar(255) null,
	Filter varchar(255) null,
	Limit int null,
	Metrics varchar(255) null,
	Segment varchar(255) null,
	Sort varchar(255) null
	)

declare @edtDestinations table (
	destinationId int not null,
	description varchar(50) not null,
	maxDescriptionLength int not null,
	maxPhotoCount int not null,
	acceptsDescriptions tinyint not null,
	acceptsOptions tinyint not null,
	acceptsPhotos tinyint not null,
	acceptsVideos tinyint not null,
	canReleaseTo bit not null,
	canCollectMetricsFrom bit not null,
	active bit not null,
	isDealerWebSite bit not null,
	hasGoogleVdps bit not null
)

insert into @edtDestinations
select
-1, 'Dealer Fire', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1
union select
-1, 'DealerPeak', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1
union select
-1, 'iPublishers', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1
union select
-1, 'Dealer eProcess', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1
union select
-1, 'Dealer Car Search', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1
union select
-1, 'DealerOn', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1

insert @googleQuery
select 
-1, 'Dealer Fire', 2, 'Dealer Fire-VDP-New', 'ga:year,ga:month', 'ga:eventCategory==Vehicle;ga:eventAction==View;ga:eventLabel=~"NewUsed":"1"', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'Dealer Fire', 3, 'Dealer Fire-VDP-Used', 'ga:year,ga:month', 'ga:eventCategory==Vehicle;ga:eventAction==View;ga:eventLabel=~"NewUsed":"2"', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'Dealer Fire', 4, 'Dealer Fire-VDP-Both', 'ga:year,ga:month', 'ga:eventCategory==Vehicle;ga:eventAction==View', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'DealerPeak', 2, 'DealerPeak-VDP-New', 'ga:year,ga:month', 'ga:pagePath=~new[/].+[/]in-stock[/][0-9]{4,4}.+-[0-9]{6,}', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'DealerPeak', 3, 'DealerPeak-VDP-Used', 'ga:year,ga:month', 'ga:pagePath=~used[/].+[/]in-stock[/][0-9]{4,4}.+-[0-9]{6,}', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'DealerPeak', 4, 'DealerPeak-VDP-Both', 'ga:year,ga:month', 'ga:pagePath=~in-stock[/][0-9]{4,4}.+-[0-9]{6,}', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'iPublishers', 2, 'iPublishers-VDP-New', 'ga:year,ga:month', 'ga:pagePath=~inventory[/]details[/][0-9]{4,4}-.+New[/]?vehId=', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'iPublishers', 3, 'iPublishers-VDP-Used', 'ga:year,ga:month', 'ga:pagePath=~inventory[/]details[/][0-9]{4,4}-.+Used[/]?vehId=', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'iPublishers', 4, 'iPublishers-VDP-Both', 'ga:year,ga:month', 'ga:pagePath=~inventory[/]details[/][0-9]{4,4}-.+[/]?vehId=', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'Dealer eProcess', 2, 'Dealer eProcess-VDP-New', 'ga:year,ga:month', 'ga:pagePath=~auto[/]new-[0-9]{4,4}.+[/][0-9]{6,}[/]', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'Dealer eProcess', 3, 'Dealer eProcess-VDP-Used', 'ga:year,ga:month', 'ga:pagePath=~auto[/]used-[0-9]{4,4}.+[/][0-9]{6,}[/]', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'Dealer eProcess', 4, 'Dealer eProcess-VDP-Both', 'ga:year,ga:month', 'ga:pagePath=~auto[/]used-[0-9]{4,4}.+[/][0-9]{6,}[/]|[/]auto[/]new-[0-9]{4,4}.+[/][0-9]{6,}[/]', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'Dealer Car Search', 2, 'Dealer Car Search-VDP-New', 'ga:year,ga:month', 'ga:pagePath=~detail.aspx?id=[0-9]{3,}.+aspx;ga:pageTitle=~- New ', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'Dealer Car Search', 3, 'Dealer Car Search-VDP-Used', 'ga:year,ga:month', 'ga:pagePath=~detail.aspx?id=[0-9]{3,}.+aspx;ga:pageTitle=~- Used|- Certified  ', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'Dealer Car Search', 4, 'Dealer Car Search-VDP-Both', 'ga:year,ga:month', 'ga:pagePath=~detail.aspx?id=[0-9]{3,}.+aspx', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'DealerOn', 2, 'DealerOn-VDP-New', 'ga:year,ga:month', 'ga:pagePath=~new-.+-[0-9]{4,4}-.+[0-9A-Z]{17}', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'DealerOn', 3, 'DealerOn-VDP-Used', 'ga:year,ga:month', 'ga:pagePath=~used-.+-[0-9]{4,4}-.+[0-9A-Z]{17}', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'
UNION SELECT
-1, 'DealerOn', 4, 'DealerOn-VDP-Both', 'ga:year,ga:month', 'new-.+-[0-9]{4,4}-.+[0-9A-Z]{17}|used-.+-[0-9]{4,4}-.+[0-9A-Z]{17}', 25, 'ga:pageviews', 'gaid::-1', 'ga:year,ga:month'


--Insert google query
insert into merchandising.settings.GoogleQuery
select QueryType, QueryName, Dimensions, Filter, Limit, Metrics, Segment, Sort
from @googleQuery
order by QueryName

Update @googleQuery
SET q.QueryId = g.QueryId
FROM merchandising.settings.GoogleQuery g
JOIN @googleQuery q on g.QueryName = q.QueryName


--insert EdtDestinations
insert into merchandising.settings.EdtDestinations
select description, maxDescriptionLength, maxPhotoCount, acceptsDescriptions,
acceptsOptions, acceptsPhotos, acceptsVideos, canReleaseTo, canCollectMetricsFrom, active, isDealerWebsite, hasGoogleVdps
from @edtDestinations
order by description

update @edtDestinations
set edt.destinationid = ed.destinationid
from merchandising.settings.EdtDestinations ed
join @edtDestinations edt on edt.description = ed.description


--insert EdtDestinationQueries
insert into merchandising.settings.EdtDestinationQueries
select edt.destinationid, gq.QueryId
from @edtDestinations edt
join @googleQuery gq on edt.description = gq.Provider
