USE [Merchandising]
GO

/****** Object:  Table [merchandising].[DiscountPricingCampaignInventory]    Script Date: 07/18/2013 11:32:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[Merchandising].[merchandising].[DiscountPricingCampaignInventory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [Merchandising].[merchandising].[DiscountPricingCampaignInventory](

	[CampaignID] int NOT NULL,
	[InventoryId] int NOT NULL,
	[OriginalNewCarPrice] decimal(9,2) NULL,
	[Active] bit 
	
	CONSTRAINT [PK_merchandising_DiscountPricingCampaignInventory] PRIMARY KEY NONCLUSTERED 
	(
		[CampaignID] ASC,
		[InventoryID] ASC
	)
)
END

GO

