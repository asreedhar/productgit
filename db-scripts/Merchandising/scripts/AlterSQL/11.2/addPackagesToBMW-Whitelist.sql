 
insert Merchandising.chrome.PackageOptionCodeWhitelist
(
	ChromeStyleID,
	ChromeOptionCode
)
select
	o.StyleID,
	o.OptionCode
from VehicleCatalog.Chrome.Options o
where
	o.CountryCode = 1
	and o.OptionCode in ('ZEC', '2NZ')
	and not exists
	(
		select *
		from Merchandising.chrome.PackageOptionCodeWhitelist
		where
			ChromeStyleID = o.StyleID
			and ChromeOptionCode = o.OptionCode
	)
	
-- select * from Merchandising.chrome.PackageOptionCodeWhitelist where ChromeOptionCode	in ('ZEC', '2NZ')