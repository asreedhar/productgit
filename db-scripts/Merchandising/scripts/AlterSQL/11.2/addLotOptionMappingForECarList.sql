use Merchandising

set nocount on
set transaction isolation level read uncommitted



declare
	@xml nvarchar(max),
	@handle int

set @xml = N'
<r L="2250" c="1009"/>
<r L="328173" c="1016"/>
<r L="328173" c="1016"/>
<r L="183912" c="1041"/>
<r L="154663" c="1055"/>
<r L="655" c="1062"/>
<r L="655" c="1062"/>
<r L="655" c="1062"/>
<r L="655" c="1062"/>
<r L="655" c="1062"/>
<r L="655" c="1062"/>
<r L="655" c="1062"/>
<r L="655" c="1062"/>
<r L="655" c="1062"/>
<r L="655" c="1062"/>
<r L="655" c="1062"/>
<r L="725" c="1066"/>
<r L="725" c="1066"/>
<r L="167845" c="1066"/>
<r L="167845" c="1066"/>
<r L="167845" c="1066"/>
<r L="167845" c="1066"/>
<r L="167845" c="1066"/>
<r L="18142" c="1069"/>
<r L="36236" c="1069"/>
<r L="382556" c="1069"/>
<r L="1425" c="1072"/>
<r L="158723" c="1073"/>
<r L="408783" c="1191"/>
<r L="408791" c="1078"/>
<r L="408820" c="1078"/>
<r L="408923" c="1078"/>
<r L="408827" c="1078"/>
<r L="408855" c="1078"/>
<r L="408855" c="1078"/>
<r L="408826" c="1078"/>
<r L="408855" c="1078"/>
<r L="408826" c="1078"/>
<r L="408826" c="1078"/>
<r L="408854" c="1078"/>
<r L="408924" c="1078"/>
<r L="408826" c="1078"/>
<r L="408826" c="1078"/>
<r L="408826" c="1078"/>
<r L="408826" c="1078"/>
<r L="408826" c="1078"/>
<r L="674" c="1078"/>
<r L="408724" c="1156"/>
<r L="408825" c="1084"/>
<r L="75442" c="1211"/>
<r L="382997" c="1103"/>
<r L="184073" c="1123"/>
<r L="391563" c="1123"/>
<r L="408733" c="1123"/>
<r L="408730" c="1123"/>
<r L="408733" c="1123"/>
<r L="408733" c="1123"/>
<r L="391563" c="1123"/>
<r L="408730" c="1123"/>
<r L="408740" c="1123"/>
<r L="408735" c="1123"/>
<r L="408741" c="1123"/>
<r L="408743" c="1123"/>
<r L="408743" c="1123"/>
<r L="408742" c="1123"/>
<r L="408745" c="1123"/>
<r L="408744" c="1123"/>
<r L="27181" c="1123"/>
<r L="408738" c="1123"/>
<r L="750" c="1143"/>
<r L="906" c="1149"/>
<r L="906" c="1149"/>
<r L="906" c="1149"/>
<r L="906" c="1149"/>
<r L="408718" c="1150"/>
<r L="408718" c="1150"/>
<r L="408933" c="1150"/>
<r L="408718" c="1150"/>
<r L="82434" c="1156"/>
<r L="618" c="1156"/>
<r L="618" c="1157"/>
<r L="226832" c="1180"/>
<r L="408864" c="1192"/>
<r L="408933" c="1192"/>
<r L="408864" c="1192"/>
<r L="408933" c="1192"/>
<r L="408933" c="1192"/>
<r L="408933" c="1192"/>
<r L="26221" c="1198"/>
<r L="197165" c="1215"/>
<r L="6635" c="1215"/>
<r L="1396" c="1215"/>
<r L="195437" c="1223"/>
<r L="408883" c="1224"/>
<r L="2660" c="1298"/>
<r L="103001" c="1299"/>
<r L="103001" c="1299"/>
'






set @xml = N'<rows>' + @xml + N'</rows>'

exec sp_xml_preparedocument @handle output, @xml


; with IdMap as
(
	select
		LotOptionId,
		ChromeCategoryId
	from
		openxml(@handle, '/rows/r', 1)
			with
			(
				LotOptionId int '@L',
				ChromeCategoryId int '@c'
			) x
)
, CleanedMap as
(
	select
		LotOptionId,
		ChromeCategoryId = max(ChromeCategoryId)
	from IdMap
	group by LotOptionId
)		
insert Merchandising.merchandising.OptionsMapping
(
	 description,
	 optionid,
	 providerid,
	 chromeCategoryId,
	 chromeDescription,
	 matchPattern,
	 OptionCount
)
select
	ol.Description,
	c.LotOptionId,
	21,
	c.ChromeCategoryId,
	null,
	null,
	0
from
	CleanedMap c
	join Lot.Listing.OptionsList ol
		on c.LotOptionId = ol.OptionID
where not exists
	(
		select *
		from Merchandising.merchandising.OptionsMapping
		where
			providerid = 21
			and optionid = c.LotOptionId
			and chromeCategoryId = c.ChromeCategoryId
	)

print convert(varchar(10), @@rowcount) + ' option maps added to lot mapping table'
	
exec sp_xml_removedocument @handle


select * from Merchandising.merchandising.OptionsMapping where providerid = 21