use Merchandising

set nocount on
set transaction isolation level read uncommitted

declare @whitelist table
(
	VehicleManufacturerId int,
	VehicleMakeId int,
	OptionCode varchar(20)
)

declare
	@xml nvarchar(max),
	@hdoc int

set @xml = N'

<r d="BMW" o="ZEC"/>
<r d="BMW" o="2NZ"/>
'



set @xml = N'<rows>' + @xml + N'</rows>'

exec sp_xml_preparedocument @hdoc output, @xml

insert @whitelist
(
	VehicleManufacturerId ,
	VehicleMakeId,
	OptionCode
)
select distinct
	VehicleManufacturerId = d.ManufacturerID,
	VehicleMakeId = d.DivisionID,
	OptionCode = x.o
from
	openxml(@hdoc, '/rows/r', 1) with (d varchar(50), o varchar(20)) x
	join VehicleCatalog.Chrome.Divisions d
		on x.d = d.DivisionName
		and d.CountryCode = 1
		
exec sp_xml_removedocument @hdoc



insert Merchandising.dbo.ManufacturerVehicleOptionCodeWhiteList
(
	VehicleManufacturerId,
	VehicleMakeId,
	OptionCode
)
select
	VehicleManufacturerId,
	VehicleMakeId,
	OptionCode
from @whitelist w
where not exists
	(
		select *
		from Merchandising.dbo.ManufacturerVehicleOptionCodeWhiteList
		where
			VehicleManufacturerId = w.VehicleManufacturerId
			and VehicleMakeId = w.VehicleMakeId
			and OptionCode = w.OptionCode
	)
print convert(varchar(10), @@rowcount) + ' options added to white list'