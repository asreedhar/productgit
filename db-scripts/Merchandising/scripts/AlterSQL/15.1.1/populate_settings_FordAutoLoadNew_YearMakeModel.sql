INSERT INTO Merchandising.settings.FordAutoLoadNew_YearMakeModel([Year],Make,Model,CreatedOn,CreatedBy)
VALUES
(2012,'Ford','Fiesta',GETDATE(),'zbrown'),
(2012,'Ford','Focus',GETDATE(),'zbrown'),
(2012,'Ford','Fusion',GETDATE(),'zbrown'),
(2012,'Ford','Mustang',GETDATE(),'zbrown'),
(2012,'Ford','Taurus',GETDATE(),'zbrown'),

(2012,'Ford','Escape',GETDATE(),'zbrown'),
(2012,'Ford','Edge',GETDATE(),'zbrown'),
(2012,'Ford','Explorer',GETDATE(),'zbrown'),
(2012,'Ford','Flex',GETDATE(),'zbrown'),
(2012,'Ford','Expedition',GETDATE(),'zbrown'),

(2012,'Ford','F-150',GETDATE(),'zbrown'),
(2012,'Ford','F-250',GETDATE(),'zbrown'),
(2012,'Ford','F-350',GETDATE(),'zbrown'),
(2012,'Ford','F-450',GETDATE(),'zbrown'),
(2012,'Ford','Transit',GETDATE(),'zbrown'),
(2012,'Ford','eseries',GETDATE(),'zbrown'),

-- 2013
(2013,'Ford','Fiesta',GETDATE(),'zbrown'),
(2013,'Ford','Focus',GETDATE(),'zbrown'),
(2013,'Ford','Fusion',GETDATE(),'zbrown'),
(2013,'Ford','Mustang',GETDATE(),'zbrown'),
(2013,'Ford','C-MAX',GETDATE(),'zbrown'),
(2013,'Ford','Taurus',GETDATE(),'zbrown'),

(2013,'Ford','Escape',GETDATE(),'zbrown'),
(2013,'Ford','Edge',GETDATE(),'zbrown'),
(2013,'Ford','Explorer',GETDATE(),'zbrown'),
(2013,'Ford','Flex',GETDATE(),'zbrown'),
(2013,'Ford','Expedition',GETDATE(),'zbrown'),

(2013,'Ford','F-150',GETDATE(),'zbrown'),
(2013,'Ford','F-250',GETDATE(),'zbrown'),
(2013,'Ford','F-350',GETDATE(),'zbrown'),
(2013,'Ford','F-450',GETDATE(),'zbrown'),
(2013,'Ford','Transit',GETDATE(),'zbrown'),
(2013,'Ford','eseries',GETDATE(),'zbrown'),

-- 2014
(2014,'Ford','Mustang',GETDATE(),'zbrown'),
(2014,'Ford','Transit',GETDATE(),'zbrown')
