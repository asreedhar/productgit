USE Merchandising
GO

-- ////////////////////////////////////////////////////////////////////////////

IF NOT EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[settings].[EventTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [settings].[EventTypes] (
	[EventType] int NOT NULL,
	[EventDescription] varchar(128) NOT NULL
	CONSTRAINT [PK_audit_BusinessUnitEventLog] PRIMARY KEY CLUSTERED (
		[EventType] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

END
GO

IF NOT EXISTS ( SELECT * FROM [settings].[EventTypes] WHERE EventType = 10 ) INSERT INTO [settings].[EventTypes] VALUES ( 10, 'Window Sticker Printed' );
IF NOT EXISTS ( SELECT * FROM [settings].[EventTypes] WHERE EventType = 15 ) INSERT INTO [settings].[EventTypes] VALUES ( 15, 'Buyers Guide Printed' );
IF NOT EXISTS ( SELECT * FROM [settings].[EventTypes] WHERE EventType = 20 ) INSERT INTO [settings].[EventTypes] VALUES ( 20, 'Vehicle Photo Added' );
GO

-- ////////////////////////////////////////////////////////////////////////////

IF NOT EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[audit].[BusinessUnitEventLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [audit].[BusinessUnitEventLog] (
	[ID] int IDENTITY(1,1) NOT NULL,
	[CreatedBy] varchar(30) NOT NULL,
	[BusinessUnitID] int NOT NULL,
	[EventDate] datetime NOT NULL,
	[EventType] int NOT NULL
	CONSTRAINT [PK_audit_BusinessUnitEventLog] PRIMARY KEY CLUSTERED (
		[ID] ASC
	) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

END
GO

IF NOT EXISTS (SELECT name FROM sysindexes WHERE name = 'IDX_audit_BusinessUnitEventLog')
BEGIN

CREATE INDEX [IDX_audit_BusinessUnitEventLog] ON [audit].[BusinessUnitEventLog] (
	[BusinessUnitID] ASC,
	[EventType] ASC,
	[EventDate] DESC
)

END
GO

