IF NOT EXISTS (SELECT * FROM DBO.SYSOBJECTS WHERE ID = OBJECT_ID(N'[dashboard].[TimeToMarketTracking#BuReportStart]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [Merchandising].[dashboard].[TimeToMarketTracking#BuReportStart](
    [BusinessUnitId] [int] NOT NULL,
    [FirstPhotoReportStartDate] [DateTime] NULL,
    [ReportStartDate] [DateTime] NULL
    CONSTRAINT [PK_dashboard.TimeToMarketTracking#BuReportStart] PRIMARY KEY CLUSTERED
    (
        [BusinessUnitId] ASC
    )
) ON [PRIMARY]

END
