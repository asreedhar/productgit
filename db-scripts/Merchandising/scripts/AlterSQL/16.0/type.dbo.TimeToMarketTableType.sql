IF EXISTS (SELECT * FROM sys.types WHERE name = 'TimeToMarketTableType' AND is_table_type = 1)
	DROP TYPE dbo.TimeToMarketTableType
GO

BEGIN
    CREATE TYPE dbo.TimeToMarketTableType AS TABLE
    (
        BusinessUnitId int,
        InventoryId int,
        VIN varchar(max),
        GIDFirstDate DateTime,
        PhotosFirstDate DateTime,
        DescriptionFirstDate DateTime,
        PriceFirstDate DateTime,
        AdApprovalFirstDate DateTime,
        CompleteAdFirstDate DateTime,
        MinimumPhotosFirstDate DateTime
    )
END
GO
GRANT EXECUTE ON TYPE::dbo.TimeToMarketTableType TO MerchandisingUser
GO
