declare @BusinessUnitId int
SET @BusinessUnitId = 102019

declare @templateIDs table (
	rowId int IDENTITY(1,1),
	templateId int
)           
insert into @templateIDs (templateId)
SELECT templateId
FROM templates.AdTemplates at
LEFT JOIN
	(select SourceTemplateId
	from templates.AdTemplates
	WHERE businessUnitId = @BusinessUnitId) bt on bt.SourceTemplateId = at.templateId
WHERE at.businessUnitId = 100150 and bt.SourceTemplateId is null
and at.active = 1

declare @ct int, @i int, @currId int, @insId int

--set defaults
select @i=1, @ct = count(*) from @templateIDs

while @i <= @ct
BEGIN
	select @currId = templateId FROM @templateIDs
	WHERE rowId = @i
	
	INSERT INTO [templates].[AdTemplates]
	([name], [businessUnitId], vehicleProfileID, parent, active, SourceTemplateId)
	SELECT [name], @BusinessUnitId, vehicleProfileID, parent, active, templateid
	FROM [templates].[AdTemplates]
	WHERE templateId = @currId
	
	select @insId = SCOPE_IDENTITY()

	INSERT INTO [templates].[AdTemplateItems]
	(templateID, blurbId, [sequence],priority, required, parentID)
	SELECT @insId, blurbId, [sequence],priority,required,parentID
	FROM [templates].[AdTemplateItems]
	WHERE templateId = @currId
	
	select @i = @i + 1
END --end while

