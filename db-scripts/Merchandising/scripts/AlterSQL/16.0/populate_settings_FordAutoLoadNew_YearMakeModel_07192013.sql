INSERT INTO Merchandising.settings.FordAutoLoadNew_YearMakeModel([Year],Make,Model,CreatedOn,CreatedBy)
VALUES

-- 2014 models released as of 7/19/2013. Perviously inserted models (mustang, transit) skipped.
(2014,'Ford','Fiesta',GETDATE(),'zbrown'),
(2014,'Ford','Focus',GETDATE(),'zbrown'),
(2014,'Ford','Fusion',GETDATE(),'zbrown'),
(2014,'Ford','Taurus',GETDATE(),'zbrown'),
-- this record already exists.
--(2014,'Ford','Escape',GETDATE(),'zbrown'),  
(2014,'Ford','Explorer',GETDATE(),'zbrown'),
(2014,'Ford','Flex',GETDATE(),'zbrown')


-- These haven't been released yet.
--(2014,'Ford','C-MAX',GETDATE(),'zbrown'),

--(2014,'Ford','Edge',GETDATE(),'zbrown'),
--(2014,'Ford','Expedition',GETDATE(),'zbrown'),

--(2014,'Ford','F-150',GETDATE(),'zbrown'),
--(2014,'Ford','F-250',GETDATE(),'zbrown'),
--(2014,'Ford','F-350',GETDATE(),'zbrown'),
--(2014,'Ford','F-450',GETDATE(),'zbrown'),
--(2014,'Ford','eseries',GETDATE(),'zbrown')
