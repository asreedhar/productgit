IF NOT EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'MinimumPhotosFirstDate' AND Object_ID = Object_ID(N'dashboard.TimeToMarketTracking'))
BEGIN    
ALTER TABLE [dashboard].[TimeToMarketTracking] 
	ADD MinimumPhotosFirstDate DateTime NULL
END
GO
