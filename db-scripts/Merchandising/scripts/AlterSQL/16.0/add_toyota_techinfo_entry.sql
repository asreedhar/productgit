USE Merchandising
GO

IF NOT EXISTS ( SELECT * FROM [settings].[CredentialedSites] WHERE siteId = 7 ) INSERT INTO [settings].[CredentialedSites] VALUES ( 'Toyota Techinfo' );
GO

UPDATE merchandising.VehicleAutoLoadSettings
SET IsAutoLoadEnabled = 1
WHERE ManufacturerID = 16 -- Toyota
GO

