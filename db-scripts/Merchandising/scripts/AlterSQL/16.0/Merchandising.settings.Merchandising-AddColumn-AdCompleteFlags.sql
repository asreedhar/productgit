IF NOT EXISTS(SELECT * FROM sys.columns
    WHERE Name = N'AdCompleteFlags' AND Object_ID = Object_ID(N'settings.Merchandising'))
BEGIN    
ALTER TABLE settings.Merchandising 
	ADD AdCompleteFlags INT CONSTRAINT DF_AdCompleteFlags DEFAULT 7 NOT NULL
END
GO
