/****** Object:  Table [settings].[BucketAlerts]    Script Date: 04/01/2012 18:22:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [settings].[BucketAlerts](
	[BusinessUnitId] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[Email] [varchar](1000) NOT NULL,
	[NewUsed] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[CreatedBy] [varchar](80) NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[UpdatedBy] [varchar](80) NOT NULL,
 CONSTRAINT [PK_settings.BucketAlerts] PRIMARY KEY CLUSTERED 
(
	[BusinessUnitId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF