use Merchandising
go

if not exists(select *
              from INFORMATION_SCHEMA.columns 
              where TABLE_SCHEMA = 'builder' and TABLE_NAME = 'OptionsConfiguration' and COLUMN_NAME = 'DoNotPostLocked')
begin
    print 'Adding DoNotPostLocked field to builder.OptionsConfiguration'
    alter table builder.OptionsConfiguration
	    add DoNotPostLocked bit not null constraint DF_Merchandising_MaxVersion default (0)
    print 'Done.'
end
go

print 'Setting builder.OptionsConfiguration.DoNotPostLocked to initial value.'
update builder.OptionsConfiguration
    set DoNotPostLocked = doNotPostFlag, 
        lastUpdatedBy = 'AlterSQL',
        lastUpdatedOn = getdate()
    where DoNotPostLocked <> doNotPostFlag
print 'Done.'
go