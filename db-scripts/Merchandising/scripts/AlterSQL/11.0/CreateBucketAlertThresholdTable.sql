/****** Object:  Table [settings].[BucketAlertsThreshold]    Script Date: 04/01/2012 18:23:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [settings].[BucketAlertsThreshold](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BusinessUnitId] [int] NOT NULL,
	[Bucket] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[Limit] [int] NOT NULL,
 CONSTRAINT [PK_BucketAlertsThreshold] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [settings].[BucketAlertsThreshold]  WITH CHECK ADD  CONSTRAINT [FK_BucketAlertsThreshold_BucketAlerts] FOREIGN KEY([BusinessUnitId])
REFERENCES [settings].[BucketAlerts] ([BusinessUnitId])
GO
ALTER TABLE [settings].[BucketAlertsThreshold] CHECK CONSTRAINT [FK_BucketAlertsThreshold_BucketAlerts]