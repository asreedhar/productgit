INSERT INTO Merchandising.Templates.OptionCodeRules (RuleID, TextToReplace, TextToReplaceWith)
VALUES (13, 'steel wheels', 'wheels')
GO

INSERT INTO Merchandising.Templates.OptionCodeRuleMappings (OptionCode, BusinessUnitID, RuleID, InventoryType)
SELECT Distinct(OptionCode), 100150, 13, 0
FROM vehicleCatalog.Chrome.Options
WHERE OptionDesc like '%Steel Wheels%'
GO

