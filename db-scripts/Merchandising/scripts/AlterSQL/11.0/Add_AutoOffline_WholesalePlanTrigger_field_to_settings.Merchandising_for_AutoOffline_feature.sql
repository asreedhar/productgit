use Merchandising
go

if not exists(select *
              from INFORMATION_SCHEMA.columns 
              where TABLE_SCHEMA = 'settings' and TABLE_NAME = 'Merchandising' and COLUMN_NAME = 'AutoOffline_WholesalePlanTrigger')
begin
    print 'Adding AutoOffline_WholesalePlanTrigger to settings.Merchandising'
    alter table settings.Merchandising
        add AutoOffline_WholesalePlanTrigger bit not null constraint DF_Merchandising_AutoOffline_WholesalePlanTrigger default (0)
    print 'Done.'
end
go
