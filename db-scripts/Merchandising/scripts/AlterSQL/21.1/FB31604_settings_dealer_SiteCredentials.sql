-- FB: 31604 - change dealer_siteCredentials to default to slurpee

-- drop default constraint on build request type
IF EXISTS (SELECT * 
				FROM sysobjects
				WHERE NAME ='DF_dealer_SiteCredentials_buildRequestType'
				AND xtype = 'd')
	begin
		ALTER TABLE settings.dealer_SiteCredentials
			DROP CONSTRAINT DF_dealer_SiteCredentials_buildRequestType
	end
go


-- add constraint buildRequestType default = 2
ALTER TABLE settings.dealer_SiteCredentials
	ADD CONSTRAINT DF_dealer_SiteCredentials_buildRequestType DEFAULT 2 FOR buildRequestType
GO

-- update all rows to buildRequestType = 2
UPDATE settings.Dealer_SiteCredentials
	SET buildRequestType = 2
	WHERE buildRequestType != 2

/*
sp_help 'settings.dealer_SiteCredentials'

*/