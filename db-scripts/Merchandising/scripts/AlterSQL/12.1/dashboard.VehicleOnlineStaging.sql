-- drop table dashboard.VehicleOnlineStaging
if object_id('dashboard.VehicleOnlineStaging') is null
begin

	create table dashboard.VehicleOnlineStaging
	(

		BusinessUnitId int null,
		InventoryId int null,
		LoadId int not null,
		RowId bigint not null identity (0,1),
		DestinationId int not null,
		DateCollected datetime not null,
		HasPhoto bit null,
		PhotoCount smallint null,
		DateLoaded datetime not null,
		DatasourceInterfaceId int not null,
		Vin varchar(17) not null,
		StockNumber varchar(15) not null,
		DaysLive smallint null,
		ErrorCode int not null,
		RequestId int null,
			
		constraint PK_VehicleOnlineStaging primary key clustered 
		(
			LoadId,
			RowId
		)
	)

					
	alter table dashboard.VehicleOnlineStaging add constraint FK_VehicleOnlineStaging_StagingErrorCode foreign key (ErrorCode)
		references dashboard.StagingErrorCode (ErrorCode) 

end