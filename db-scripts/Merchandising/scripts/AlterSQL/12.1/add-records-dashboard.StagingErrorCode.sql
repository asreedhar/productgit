

if not exists(select * from Merchandising.dashboard.StagingErrorCode where ErrorCode = 4)

insert Merchandising.dashboard.StagingErrorCode
(
	ErrorCode,
	Description
)
values
(
	4,
	'would-be primary key violation internally in this LoadId'
)