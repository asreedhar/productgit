SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [merchandising].[WebLoaderPhotos](
	[InventoryID] [int] NOT NULL,
	[Handle] [uniqueidentifier] NOT NULL,
	[UserName] [varchar](50) NOT NULL,
	[Received] [datetime] NOT NULL,
	[UpLoaded] [datetime] NULL,
 CONSTRAINT [PK_WebLoaderPhotos] PRIMARY KEY CLUSTERED 
(
	[InventoryID] ASC,
	[Handle] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF