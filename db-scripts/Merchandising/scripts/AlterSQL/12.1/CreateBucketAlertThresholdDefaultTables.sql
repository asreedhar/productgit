
if  (object_id(N'settings.FK_BucketAlertThresholdDefault_BucketAlertThresholdLimitType') is not null)
	alter table settings.BucketAlertThresholdDefault 
		drop constraint FK_BucketAlertThresholdDefault_BucketAlertThresholdLimitType
go

if  (object_id(N'settings.BucketAlertThresholdLimitType') is not null)
	drop table settings.BucketAlertThresholdLimitType
go

create table settings.BucketAlertThresholdLimitType(
	Id tinyint not null,
	[Description] varchar(50) not null,
	constraint PK_BucketAlertThresholdLimitType primary key clustered 
	(
		Id asc
	)with (pad_index  = off, statistics_norecompute  = off, ignore_dup_key = off, allow_row_locks  = on, allow_page_locks  = on) on [PRIMARY]
	) on [PRIMARY]
go

insert settings.BucketAlertThresholdLimitType( Id, Description ) values  ( 1, 'Inventory count')
insert settings.BucketAlertThresholdLimitType( Id, Description ) values  ( 2, 'Percentage of inventory')

if (object_id(N'settings.CK_BucketAlertThresholdDefault_Limit') is not null)
	alter table settings.BucketAlertThresholdDefault drop constraint CK_BucketAlertThresholdDefault_Limit
go

if  (object_id(N'settings.BucketAlertThresholdDefault') is not null)
	drop table settings.BucketAlertThresholdDefault
go

create table settings.BucketAlertThresholdDefault(
	Bucket int NOT NULL,
	Active bit NOT NULL,
	Limit int NOT NULL,
	LimitTypeId tinyint NOT NULL,
	constraint PK_BucketAlertThresholdDefault primary key clustered 
	(
		Bucket asc
	)with (pad_index  = off, statistics_norecompute  = off, ignore_dup_key = off, allow_row_locks  = on, allow_page_locks  = on) on [PRIMARY]
	) on [PRIMARY]
go

alter table settings.BucketAlertThresholdDefault  
	with check add constraint FK_BucketAlertThresholdDefault_BucketAlertThresholdLimitType foreign key(LimitTypeId)
	references settings.BucketAlertThresholdLimitType (Id)
go

alter table settings.BucketAlertThresholdDefault 
	check constraint FK_BucketAlertThresholdDefault_BucketAlertThresholdLimitType
go


alter table settings.BucketAlertThresholdDefault  
	with check add  constraint CK_BucketAlertThresholdDefault_Limit check ((LimitTypeId<>(2) OR Limit>=(0) AND Limit<=(100)))
GO

alter table settings.BucketAlertThresholdDefault 
	check constraint CK_BucketAlertThresholdDefault_Limit
go

insert settings.BucketAlertThresholdDefault( Bucket, active, Limit, LimitTypeId )
select -1, 1, 10, 2 -- NotOnline
union all select 3, 0, 250, 1 -- EquipmentNeeded
union all select 5, 1, 2, 2 -- NoPhotos
union all select 6, 1, 4, 2 -- NoPrice
union all select 7, 1, 10, 2 -- CreateInitialAd
union all select 16, 0, 20, 1 -- DueForRepricing
union all select 17, 1, 500, 1 -- LowActivity
union all select 19, 0, 5, 1 -- TrimNeeded
union all select 20, 1, 500, 1 -- BookValueNeeded
union all select 22, 1, 500, 1 -- NoPackages
union all select 23, 1, 500, 1 -- NoCarfax
union all select 29, 1, 500, 1 -- LowPhotos

