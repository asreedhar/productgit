-- drop table dashboard.VehicleActivity
if object_id('dashboard.VehicleActivity') is null

create table dashboard.VehicleActivity
(

	BusinessUnitId int not null,
	InventoryId int not null,
	DestinationId int not null,
	LogicalDate datetime not null,
	DbIdentity rowversion not null,
	DateCollected datetime not null,
	Month datetime not null,
	
	SearchPageViews int not null,
	DetailPageViews int not null,
	EmailLeads int not null,
	MapsViewed int not null,
	AdPrints int not null,
	PhoneLeads int null,
	WebsiteClicks int null,
	ChatRequests int null,
	PhotoCount int null,
	DaysLive int null,
	VideosViewed int null,
	

	LoadId int not null,
	RequestId int null,
	DateLoaded datetime not null,
	DatasourceInterfaceId int not null,
	

		
	constraint PK_VehicleActivity primary key clustered 
	(
		BusinessUnitId,
		InventoryId,
		DestinationId,
		LogicalDate,
		DbIdentity
	)
)
						