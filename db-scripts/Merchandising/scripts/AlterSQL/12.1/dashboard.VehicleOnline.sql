-- drop table dashboard.VehicleOnline
if object_id('dashboard.VehicleOnline') is null

create table dashboard.VehicleOnline
(

	BusinessUnitId int not null,
	InventoryId int not null,
	LoadId int not null,
	DestinationId int not null,
	DateCollected datetime not null,
	HasPhoto bit null,
	PhotoCount smallint null,
	DaysLive smallint null,
	DateLoaded datetime not null,
	DatasourceInterfaceId int not null,
	RequestId int null,

	constraint PK_VehicleOnline primary key clustered 
	(
		BusinessUnitId,
		InventoryId,
		LoadId
	)
)
						