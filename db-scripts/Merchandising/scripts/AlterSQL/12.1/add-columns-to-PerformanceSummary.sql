if not exists(select * from sys.columns where object_id = object_id('dashboard.PerformanceSummary') and name = 'NewPhoneLeads')
alter table dashboard.PerformanceSummary
add
	NewPhoneLeads int null,
	NewWebsiteClicks int null,
	NewChatRequests int null,
	NewVideosViewed int null,
	
	UsedPhoneLeads int null,
	UsedWebsiteClicks int null,
	UsedChatRequests int null,
	UsedVideosViewed int null
	


if not exists(select * from sys.columns where object_id = object_id('dashboard.PerformanceSummary') and name = 'RequestId')
alter table dashboard.PerformanceSummary
add
	RequestId int null,
	DateCollected datetime not null default (0),
	DateLoaded datetime not null default (0),
	DatasourceInterfaceId int not null default (-1)