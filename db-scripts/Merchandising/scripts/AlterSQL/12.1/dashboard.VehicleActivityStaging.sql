-- drop table dashboard.VehicleActivityStaging
if object_id('dashboard.VehicleActivityStaging') is null
begin
	create table dashboard.VehicleActivityStaging
	(

		BusinessUnitId int not null,
		InventoryId int null,
		DestinationId int not null,
		DateCollected datetime not null,
		Month datetime not null,
		
		SearchPageViews int not null,
		DetailPageViews int not null,
		EmailLeads int not null,
		MapsViewed int not null,
		AdPrints int not null,
		PhoneLeads int null,
		WebsiteClicks int null,
		ChatRequests int null,
		PhotoCount int null,
		DaysLive int null,
		VideosViewed int null,
		

		LoadId int not null,
		RowId bigint not null identity (0,1),
		RequestId int null,
		DateLoaded datetime not null,
		DatasourceInterfaceId int not null,
		
		Vin varchar(17) not null,
		StockNumber varchar(15) null,
		ErrorCode int not null,
			
		constraint PK_VehicleActivityStaging primary key clustered 
		(
			LoadId,
			RowId
		)
	)

	alter table dashboard.VehicleActivityStaging add constraint FK_VehicleActivityStaging_StagingErrorCode foreign key (ErrorCode)
		references dashboard.StagingErrorCode (ErrorCode)
end