
create table settings.SiteBudget
(
    BusinessUnitId int not null,
    DestinationId int not null, 
    DateUpdated datetime not null,
	Budget int
)
go

alter table settings.SiteBudget add constraint PK_SiteBudget
	primary key clustered (BusinessUnitId, DestinationId, DateUpdated)
go

alter table settings.SiteBudget add constraint FK_SiteBudget_EdtDestinations
	foreign key (destinationID) references settings.EdtDestinations (destinationID)
go	
