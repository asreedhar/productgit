; with Hendrick as
(
	select
		BusinessUnitId
	from imt.dbo.BusinessUnitRelationship
	where ParentID = 100068 
)
delete metrics
from
	Merchandising.merchandising.CarsDotComVehOnline metrics
	join Hendrick on
		metrics.BusinessUnitID = Hendrick.BusinessUnitID



; with Hendrick as
(
	select
		BusinessUnitId
	from imt.dbo.BusinessUnitRelationship
	where ParentID = 100068 
)
delete metrics
from
	Merchandising.dashboard.PerformanceSummary metrics
	join Hendrick on
		metrics.BusinessUnitID = Hendrick.BusinessUnitID
where
	metrics.DestinationId = 1
	and metrics.DatasourceInterfaceId <> 2010



; with Hendrick as
(
	select
		BusinessUnitId
	from imt.dbo.BusinessUnitRelationship
	where ParentID = 100068 
)
delete metrics
from
	Merchandising.dashboard.InventoryAdPerformance metrics
	join Hendrick on
		metrics.BusinessUnitID = Hendrick.BusinessUnitID
where
	DestinationId = 1
	and not exists
	(
		select *
		from DBASTAT.dbo.Dataload_History
		where
			Load_ID = metrics.LoadId
			and DatasourceInterfaceID <> 2010
	)
