-- Need to establish a default active value of 1 for the offline bucket.

if exists(select * from Merchandising.settings.BucketAlertThresholdDefault where Bucket = 1)
delete Merchandising.settings.BucketAlertThresholdDefault where Bucket = 1
go

insert Merchandising.settings.BucketAlertThresholdDefault
        ( Bucket, Active, Limit, LimitTypeId )
values  ( 1, -- Bucket - int
          1, -- Active - bit
          999999999, -- Limit - int
          1  -- LimitTypeId - tinyint
          )

