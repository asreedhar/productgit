DECLARE @cpoGroupId INT
DECLARE @certificationTypeId INT

-- create the Fiat cpoGroup (all programs must be in a group)
INSERT INTO settings.cpoGroups
    ( name )
VALUES  ( 'Fiat'  )

SET @cpoGroupId = SCOPE_IDENTITY()

-- create the certified program itself
INSERT INTO builder.manufacturerCertifications
    ( name )
VALUES  ( 'Fiat' )

SET @certificationTypeId = SCOPE_IDENTITY()

INSERT INTO settings.cpoCertifieds
    ( cpoGroupId, certificationTypeId )
VALUES  ( @cpoGroupId, @certificationTypeId )