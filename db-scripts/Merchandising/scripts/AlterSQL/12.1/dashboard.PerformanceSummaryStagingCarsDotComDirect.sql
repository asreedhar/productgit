-- drop table dashboard.PerformanceSummaryStagingCarsDotComDirect
if object_id('dashboard.PerformanceSummaryStagingCarsDotComDirect') is null
begin
	create table dashboard.PerformanceSummaryStagingCarsDotComDirect
	(
		BusinessUnitId int not null,
		InventoryId int not null,
		DestinationId int not null,
		Month datetime not null,
		DateCollected datetime not null,
		
				
		NewSearchPageViews int not null,
		NewDetailPageViews int not null,
		NewEmailLeads int not null,
		NewMapsViewed int not null,
		NewAdPrints int not null,
		
		UsedSearchPageViews int not null,
		UsedDetailPageViews int not null,
		UsedEmailLeads int not null,
		UsedMapsViewed int not null,
		UsedAdPrints int not null,
		
	
		NewPhoneLeads int null,
		NewWebsiteClicks int null,
		NewChatRequests int null,
		NewVideosViewed int null,
		
		UsedPhoneLeads int null,
		UsedWebsiteClicks int null,
		UsedChatRequests int null,
		UsedVideosViewed int null,
		
		LoadId int not null,
		RowId bigint not null,
		
		DateLoaded datetime not null,
		DatasourceInterfaceId int not null,

				
		constraint PK_PerformanceSummaryStagingCarsDotComDirect primary key clustered 
		(
			BusinessUnitId,
			InventoryId,
			DestinationId,
			Month,
			DateCollected
		)
		
	)		

end