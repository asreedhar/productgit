alter table settings.AutoApprove
add EmailBounced bit not null CONSTRAINT DF_settings_AutoApprove_EmailBounced DEFAULT (0)
GO

alter table settings.BucketAlerts
add EmailBounced bit not null CONSTRAINT DF_settings_BucketAlerts_EmailBounced DEFAULT (0)
GO
