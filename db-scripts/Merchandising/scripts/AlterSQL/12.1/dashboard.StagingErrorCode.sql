-- drop table dashboard.StagingErrorCode
if object_id('dashboard.StagingErrorCode') is null
begin

	create table dashboard.StagingErrorCode
	(
		ErrorCode int not null,
		Description varchar(1000)
			
		constraint PK_StagingErrorCode primary key clustered 
		(
			ErrorCode
		)
	)
end


insert dashboard.StagingErrorCode
(
	ErrorCode,
	Description
)
select
	ErrorCode,
	Description
from
	(
		select ErrorCode = 0, Description = 'no error'
		union all select ErrorCode = 1, Description = 'can not look up InventoryId from Vin'
		union all select ErrorCode = 2, Description = 'would-be primary key violation in normalized table'
		union all select ErrorCode = 3, Description = 'data for this BuId/LoadId represents an old Online snapshot and will be ignored'
	) e
where not exists
	(
		select *
		from dashboard.StagingErrorCode
		where
			ErrorCode = e.ErrorCode
	)