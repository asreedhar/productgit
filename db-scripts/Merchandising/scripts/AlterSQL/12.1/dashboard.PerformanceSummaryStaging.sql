-- drop table dashboard.PerformanceSummaryStaging
if object_id('dashboard.PerformanceSummaryStaging') is null
begin
	create table dashboard.PerformanceSummaryStaging
	(
		BusinessUnitId int not null,
		DestinationId int not null,
		Month datetime not null,
		
		NewSearchPageViews int not null,
		NewDetailPageViews int not null,
		NewEmailLeads int not null,
		NewMapsViewed int not null,
		NewAdPrints int not null,
		
		UsedSearchPageViews int not null,
		UsedDetailPageViews int not null,
		UsedEmailLeads int not null,
		UsedMapsViewed int not null,
		UsedAdPrints int not null,
		
	
		NewPhoneLeads int null,
		NewWebsiteClicks int null,
		NewChatRequests int null,
		NewVideosViewed int null,
		
		UsedPhoneLeads int null,
		UsedWebsiteClicks int null,
		UsedChatRequests int null,
		UsedVideosViewed int null,
		
		LoadId int not null,
		RowId bigint not null identity (0,1),
		RequestId int null,
		DateCollected datetime not null,
		DateLoaded datetime not null,
		DatasourceInterfaceId int not null,

		ErrorCode int not null,
		
		constraint PK_PerformanceSummaryStaging primary key clustered 
		(
			LoadId,
			RowId
		)
		
	)		

	alter table dashboard.PerformanceSummaryStaging add constraint FK_PerformanceSummaryStaging_StagingErrorCode foreign key (ErrorCode)
		references dashboard.StagingErrorCode (ErrorCode)
end