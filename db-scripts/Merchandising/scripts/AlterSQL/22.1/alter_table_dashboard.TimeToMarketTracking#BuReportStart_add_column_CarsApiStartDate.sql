-- if column exists ... remove it
IF EXISTS(SELECT * FROM sys.columns WHERE [name] LIKE 'CarsApiStartDate' AND [OBJECT_ID] = object_id('dashboard.TimeToMarketTracking#BuReportStart'))
BEGIN
	ALTER TABLE dashboard.TimeToMarketTracking#BuReportStart
		DROP COLUMN CarsApiStartDate 
END
GO

ALTER TABLE dashboard.TimeToMarketTracking#BuReportStart
	ADD CarsApiStartDate DateTime NULL

GO