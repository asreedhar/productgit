if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[DiscountPricingCampaignExclusionType]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table [merchandising].[DiscountPricingCampaignExclusionType]
GO

CREATE TABLE [merchandising].[DiscountPricingCampaignExclusionType](
	
	[ExclusionId] [tinyint] NOT NULL,
	[ExclusionReason] [varchar](30) NOT NULL
)
GO

ALTER TABLE [merchandising].[DiscountPricingCampaignExclusionType]
	ADD CONSTRAINT PK_DiscountPricingCampaignExclusionType
	PRIMARY KEY (ExclusionId)
GO

-- seed data
INSERT INTO merchandising.DiscountPricingCampaignExclusionType
	VALUES(1, 'Manual Reprice')
