UPDATE settings.CredentialedSites
	SET maxCallsPerBatch = 2, maxRequestsPerCall = 20, batchProcessOnly = 1 
	WHERE siteId = 2 -- GM
	
UPDATE settings.CredentialedSites
	SET maxCallsPerBatch = 2, maxRequestsPerCall = 20, batchProcessOnly = 1 
	WHERE siteId = 3 -- Chrysler

UPDATE settings.CredentialedSites
	SET maxCallsPerBatch = 2, maxRequestsPerCall = 20, batchProcessOnly = 1 
	WHERE siteId = 4 -- vw

UPDATE settings.CredentialedSites
	SET maxCallsPerBatch = 2, maxRequestsPerCall = 20, batchProcessOnly = 1 
	WHERE siteId = 5 -- mazda

UPDATE settings.CredentialedSites
	SET maxCallsPerBatch = 2, maxRequestsPerCall = 20, batchProcessOnly = 1 
	WHERE siteId = 8 -- audi

UPDATE settings.CredentialedSites
	SET batchProcessOnly = 1 
	WHERE siteId = 9 -- hyundai
