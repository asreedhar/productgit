


DECLARE @newRules TABLE (optionCode varchar(20), textToReplace varchar(150))

INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/90 day subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/90-day trial subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/90-day all access trial')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '90-day free trial')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/90-day complementary trial')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/3-month complimentary trial')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/(90) day trial subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/(90) day trial')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(includes 90-day All-Access trial subscription)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/(3) month XM subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/90 days subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/(3) month subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '& 90-day subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/90-day subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/(3) months subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(3) years subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(complimentary 1-year trial subscription)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(90-day trial subscription included)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(one-year trial subscription included)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-year trial subscription to')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(1-year trial subscription included)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/(1) year trial subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/ (90) day trial subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(3) month subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(3) months subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(6) month subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '6-months subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(6) month service subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-year trial subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(1-year trial subscription)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/90 day trial subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/90 day free trial subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/(1)year subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-year trial subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1 year free trial subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(1-year trial data subscription included)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '*XM services include 90-day trial subscription*')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '-inc: 1-year safety plan subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/(1) year trial subscrption')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(complimentary 90-day trial subscription)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/90-day free subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(Complimentary one-year trial subscription)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(includes 90-day trial subscription to XM Select Package)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(1) year Safety Connect trial subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '90-day trial subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(1) year trial subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1 year trial subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '90-day XM Nav Traffic subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'one-year trial subscription included')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/one-year trial subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/(1) yr subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/90-day complimentary trial')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/90-day trial')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/free subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/3-year complimentary subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/(90) day subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/3-month trial subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '-inc: 90 day subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'W/6 MONTH SUBSCRIPTION')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '-inc: (6) months service subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(6) months subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/3-month subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/90-day all-access trial')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/3-year trial')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/free activation & 6-months free subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/free traffic and weather subscription')

DECLARE @textToReplace varchar(150), @optionCode varchar(20)

DECLARE @ruleId int


DECLARE newRuleCursor CURSOR FAST_FORWARD FOR 
	SELECT optionCode, textToReplace
		FROM @newRules
		
OPEN newRuleCursor

FETCH NEXT FROM newRuleCursor
	INTO @optionCode, @textToReplace
	
WHILE @@FETCH_STATUS = 0
	begin
		-- magic happens here!
		
		SET @ruleId = null
		
		-- find the rule?
		SELECT @ruleId = ruleId
			FROM Templates.OptionCodeRules
			WHERE textToReplace = @textToReplace
			AND textToReplaceWith = ''
			
		if @ruleId IS NULL
			begin
			
				-- make new rule
				SELECT @ruleID =  MAX(ruleID)
					FROM Templates.OptionCodeRules
					
				SET @ruleId = @ruleId + 1
			
				INSERT INTO Templates.OptionCodeRules
					(ruleId, textToReplace, textToReplaceWith)
					VALUES(@ruleId, @textToReplace, '')
					
			end
			
			
		-- is the option code tied to this rule
		if NOT EXISTS (SELECT *
							FROM Templates.optioncodeRuleMappings
							WHERE ruleId = @ruleId
							AND optionCode = @optionCode)
			begin
				-- insert code to rule
				INSERT INTO Templates.OptionCodeRuleMappings
					(ruleId, optionCode, businessunitid, inventoryType)
					VALUES(@ruleId, @optionCode, 100150, 2) -- default to 1stLook and used!!!
			end

		/* testing */			
		--SELECT *
		--	FROM Templates.OptionCodeRules
		--	INNER JOIN templates.OptionCodeRuleMappings ON OptionCodeRules.ruleId = OptionCodeRuleMappings.ruleId
		--	WHERE textToReplace = @textToReplace
		--	AND optionCode = @optionCode
	
		
		
		-- magic ends
		
		-- loop
		FETCH NEXT FROM newRuleCursor
			INTO @optionCode, @textToReplace
	end

		
		
CLOSE newRuleCursor		
DEALLOCATE newRuleCursor


/*

*/

