-- FB 32192 Toyota SE

-- drop default constraint on canRequestData
IF EXISTS (SELECT * 
				FROM sysobjects
				WHERE NAME ='DF_credentialedSites_canRequestData'
				AND xtype = 'd')
	begin
		ALTER TABLE settings.credentialedSites
			DROP CONSTRAINT DF_credentialedSites_canRequestData
	end
go

-- drop column batchProcessOnly
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'canRequestData'
				AND object_id = object_id('settings.credentialedSites'))
	begin
		ALTER TABLE settings.credentialedSites
			DROP COLUMN canRequestData
	end
go

-- add column for buildRequestType
ALTER TABLE settings.credentialedSites
	ADD canRequestData BIT NOT NULL
	CONSTRAINT DF_credentialedSites_canRequestData DEFAULT 1
GO

UPDATE settings.credentialedSites
	SET canRequestData = 0
	WHERE CredentialedSites.siteId IN (6, 7) -- turn off Toyota techinfo and Ford Connect
	
-- add SE toyota 
if EXISTS (
		SELECT * 
			FROM settings.credentialedSites
			WHERE siteId = 11
		)
	begin
		DELETE merchandising.VehicleAutoLoadSettings
			WHERE siteID = 11
			
		DELETE builder.BuildRequestLog
			WHERE siteID = 11

		DELETE settings.Dealer_SiteCredentials
			WHERE siteID = 11
	
		DELETE settings.credentialedSites
			WHERE siteId = 11
	end
go

-- insert new site to SE Toyota Dealer Daily
SET IDENTITY_INSERT settings.credentialedSites ON

INSERT INTO settings.credentialedSites
	(siteId, siteName, DealerCodeRequired, batchProcessOnly, maxRequestsPerCall, maxCallsPerBatch )
	VALUES(11, 'SET Dealer Daily', 0, 1, 20, 1)
go
	
SET IDENTITY_INSERT settings.credentialedSites OFF
go


-- merchandising.VehicleAutoLoadSettings

IF EXISTS (SELECT * 
				FROM sysobjects
				WHERE NAME ='PK_VehicleAutoLoadSettings'
				AND xtype = 'PK')
	begin
		ALTER TABLE merchandising.VehicleAutoLoadSettings
			DROP CONSTRAINT PK_VehicleAutoLoadSettings
	end
go

-- delete unused rows
DELETE merchandising.VehicleAutoLoadSettings
	WHERE siteId IS NULL
GO	

INSERT INTO merchandising.VehicleAutoLoadSettings
	(ManufacturerID, IsAutoLoadEnabled, siteId)
	VALUES(16, 1, 11) -- SE Toyota dealer daily
go

ALTER TABLE merchandising.VehicleAutoLoadSettings
	ALTER COLUMN siteID int NOT NULL
GO

ALTER TABLE merchandising.VehicleAutoLoadSettings
	ADD CONSTRAINT PK_VehicleAutoLoadSettings
	PRIMARY KEY (ManufacturerID, siteId)
go

-- update first toyota
UPDATE settings.CredentialedSites
	SET siteName = 'National Dealer Daily'
	WHERE siteId = 10
