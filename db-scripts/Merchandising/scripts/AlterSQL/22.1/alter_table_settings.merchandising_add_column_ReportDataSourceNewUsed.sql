-- if column default ... remove it
IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'D' AND NAME = 'DF_merchandising_ReportDataSourceNew')
	BEGIN
		ALTER TABLE [settings].[merchandising]
			DROP CONSTRAINT DF_merchandising_ReportDataSourceNew
	END
	
IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'D' AND NAME = 'DF_merchandising_ReportDataSourceUsed')
	BEGIN
		ALTER TABLE settings.merchandising
			DROP CONSTRAINT DF_merchandising_ReportDataSourceUsed
	END

-- if column exists ... remove it
IF EXISTS(SELECT * FROM sys.columns WHERE [name] LIKE 'ReportDataSourceNew' AND [OBJECT_ID] = object_id('settings.merchandising'))
BEGIN
	ALTER TABLE settings.merchandising
		DROP COLUMN ReportDataSourceNew 
END
GO

-- if column exists ... remove it
IF EXISTS(SELECT * FROM sys.columns WHERE [name] LIKE 'ReportDataSourceUsed' AND [OBJECT_ID] = object_id('settings.merchandising'))
BEGIN
	ALTER TABLE settings.merchandising
		DROP COLUMN ReportDataSourceUsed 
END
GO

ALTER TABLE settings.merchandising
	ADD ReportDataSourceUsed TINYINT NOT NULL CONSTRAINT DF_merchandising_ReportDataSourceUsed DEFAULT 1,
	ReportDataSourceNew TINYINT NOT NULL CONSTRAINT DF_merchandising_ReportDataSourceNew DEFAULT 1

GO