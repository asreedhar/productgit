-- FB32191 - add priority to vehicleAutoLoadSettings

-- drop column
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'priority'
				AND object_id = object_id('merchandising.VehicleAutoLoadSettings'))
	begin
		ALTER TABLE merchandising.VehicleAutoLoadSettings
			DROP COLUMN priority
	end
go

-- add it
ALTER TABLE merchandising.VehicleAutoLoadSettings
	ADD priority TINYINT NULL
GO
	
UPDATE merchandising.VehicleAutoLoadSettings
	SET priority = 1

UPDATE merchandising.VehicleAutoLoadSettings
	SET priority = 2
	WHERE siteId = 11 -- southeast toyotal

GO	

-- make not null
ALTER TABLE merchandising.VehicleAutoLoadSettings
	ALTER COLUMN priority TINYINT NOT NULL

GO

