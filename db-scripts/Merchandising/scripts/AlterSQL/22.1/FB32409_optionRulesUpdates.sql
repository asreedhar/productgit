
------------- get ride of these BMW only codes, per MFC, time sensitive only on used vehicles
DELETE merchandising.templates.OptionCodeRuleMappings 
	WHERE OptionCodeRuleMappings.ruleId = 14
GO

DELETE  merchandising.templates.optionCodeRules
	WHERE OptionCodeRules.ruleId = 14
GO



-------------- add new phrases for wild card options

DECLARE @newRules TABLE (optionCode varchar(20), textToReplace varchar(150))

INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'a 10-year subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '3-year subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '4-year subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(4) year subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1 year subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/4-year subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/(4) year subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-yr subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'lifetime subscription')

--INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '')

DECLARE @textToReplace varchar(150), @optionCode varchar(20)

DECLARE @ruleId int


DECLARE newRuleCursor CURSOR FAST_FORWARD FOR 
	SELECT optionCode, textToReplace
		FROM @newRules
		
OPEN newRuleCursor

FETCH NEXT FROM newRuleCursor
	INTO @optionCode, @textToReplace
	
WHILE @@FETCH_STATUS = 0
	begin
		-- magic happens here!
		
		SET @ruleId = null
		
		-- find the rule?
		SELECT @ruleId = ruleId
			FROM Templates.OptionCodeRules
			WHERE textToReplace = @textToReplace
			AND textToReplaceWith = ''
			
		if @ruleId IS NULL
			begin
			
				-- make new rule
				SELECT @ruleID =  MAX(ruleID)
					FROM Templates.OptionCodeRules
					
				SET @ruleId = @ruleId + 1
			
				INSERT INTO Templates.OptionCodeRules
					(ruleId, textToReplace, textToReplaceWith)
					VALUES(@ruleId, @textToReplace, '')
					
			end
			
			
		-- is the option code tied to this rule
		if NOT EXISTS (SELECT *
							FROM Templates.optioncodeRuleMappings
							WHERE ruleId = @ruleId
							AND optionCode = @optionCode)
			begin
				-- insert code to rule
				INSERT INTO Templates.OptionCodeRuleMappings
					(ruleId, optionCode, businessunitid, inventoryType)
					VALUES(@ruleId, @optionCode, 100150, 2) -- default to 1stLook and used!!!
			end

		/* testing */			
		--SELECT *
		--	FROM Templates.OptionCodeRules
		--	INNER JOIN templates.OptionCodeRuleMappings ON OptionCodeRules.ruleId = OptionCodeRuleMappings.ruleId
		--	WHERE textToReplace = @textToReplace
		--	AND optionCode = @optionCode
	
		
		
		-- magic ends
		
		-- loop
		FETCH NEXT FROM newRuleCursor
			INTO @optionCode, @textToReplace
	end

		
		
CLOSE newRuleCursor		
DEALLOCATE newRuleCursor


/*

*/

