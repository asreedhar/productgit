-- FB 31043
IF NOT EXISTS (
	SELECT	1
	FROM	[sys].[columns]
	WHERE	[name] LIKE 'GroupBionicHealthReportEmail'
		AND	[object_id] = OBJECT_ID('[settings].[BucketAlerts]')
)
	ALTER TABLE [settings].[BucketAlerts]
		ADD	[GroupBionicHealthReportEmail] [VARCHAR](1000) NOT NULL CONSTRAINT DF_settings_BucketAlerts_GroupBionicHealthReportEmail DEFAULT ('')

IF NOT EXISTS (
	SELECT	1
	FROM	[sys].[columns]
	WHERE	[name] LIKE 'GroupBionicHealthReportEmailBounced'
		AND	[object_id] = OBJECT_ID('[settings].[BucketAlerts]')
)
	ALTER TABLE [settings].[BucketAlerts]
		ADD [GroupBionicHealthReportEmailBounced] [BIT] NOT NULL CONSTRAINT DF_settings_BucketAlerts_GroupBionicHealthReportEmailBounced DEFAULT (0)

IF NOT EXISTS (
	SELECT	*
	FROM	[sysobjects]
	WHERE	[type] = 'D'
		AND	[name] = 'DF_settings_BucketAlerts_GroupBionicHealthReportEmail'
)
	ALTER TABLE [settings].[BucketAlerts]
		ADD CONSTRAINT DF_settings_BucketAlerts_GroupBionicHealthReportEmail DEFAULT ('') FOR [GroupBionicHealthReportEmail]

IF NOT EXISTS (
	SELECT	*
	FROM	[sysobjects]
	WHERE	[type] = 'D'
		AND	[name] = 'DF_settings_BucketAlerts_GroupBionicHealthReportEmailBounced'
)
	ALTER TABLE [settings].[BucketAlerts]
		ADD CONSTRAINT DF_settings_BucketAlerts_GroupBionicHealthReportEmailBounced DEFAULT (0) FOR [GroupBionicHealthReportEmailBounced]
GO