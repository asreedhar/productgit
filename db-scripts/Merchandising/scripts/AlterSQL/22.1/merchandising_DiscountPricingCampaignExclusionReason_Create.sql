if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[DiscountPricingCampaignExclusion]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	drop table [merchandising].[DiscountPricingCampaignExclusion]
GO

CREATE TABLE [merchandising].[DiscountPricingCampaignExclusion](
	
	CampaignID	int	NOT NULL,
	InventoryId	int	NOT NULL,
	ExclusionId tinyint NOT NULL,
	CreationDate  datetime NOT NULL,
	CreatedBy varchar(250) NOT NULL
	
)
GO

ALTER TABLE [merchandising].[DiscountPricingCampaignExclusion]
	ADD CONSTRAINT PK_DiscountPricingCampaignExclusion
	PRIMARY KEY (CampaignID, InventoryID)
GO

ALTER TABLE [merchandising].[DiscountPricingCampaignExclusion]
	ADD CONSTRAINT FK_DiscountPricingCampaignExclusion_DiscountPricingCampaignInventory
	FOREIGN KEY (CampaignID, InventoryID)
	REFERENCES merchandising.DiscountPricingCampaignInventory(CampaignID, InventoryID)
GO

CREATE INDEX IX_DiscountPricingCampaignExclusion_InventoryID ON merchandising.DiscountPricingCampaignExclusion(InventoryID)
GO
