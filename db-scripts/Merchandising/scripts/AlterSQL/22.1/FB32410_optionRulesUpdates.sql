


DECLARE @newRules TABLE (optionCode varchar(20), textToReplace varchar(150))

INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/6-month trial subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-year subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-year complimentary subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1 year subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/(1) year service subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-yr prepaid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-year paid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/ (1) year service subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-Yr SIRIUSXM Radio Service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/(1) year Sirius subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/(1) year free subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-year prepaid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(1) year SIRIUS satellite radio subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-year service subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(1) year Sirius service subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-year subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(1) year Sirius subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(1) year subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(1) year free subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(1) year service subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-year Sirius service subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '5-Year SiriusXM Travel Link Service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1 Year Trial')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '5-Year SiriusXM Traffic Service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '6 Month Trial')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '6 Mo, Trial')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/6-month trial')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1 Yr Trial')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1 Yr, Trial')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-year trial')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1 Year Sirius Travel Link Service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-year Travel Link service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-year service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-year backseat TV service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-Year SIRIUSXM Radio Service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-year prepaid service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-year of service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-year of service & feature tracking')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-year SIRIUS backseat service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-year service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-year prepaid service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-year service provided by Sirius')

INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-year Sirius radio service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-year SIRIUS backseat TV service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-Year SIRIUSXM Travel Link Service')

INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-year complimentary service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-year SIRIUS TV service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-year radio service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-year real-time traffic service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-Year SIRIUSXM Traffic Service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-year Backseat TV service')

INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-year traffic service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-year SIRIUS realtime traffic service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-year Realtime Traffic service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), ' 1-year realtime traffic service')

INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-year Sirius travel link service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-year radio service')

INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'SiriusXM traffic w/1-year Travel Link')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-year Sirius real time traffic service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-year subsription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-year pre-paid')

INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(1) year SiriusXM Travel Link & Traffic service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/1-year service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-Yr SIRIUSXM Traffic Service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-Yr SIRIUSXM Travel Link Service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(1) year SIRIUS service')

--INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '')

DECLARE @textToReplace varchar(150), @optionCode varchar(20)

DECLARE @ruleId int


DECLARE newRuleCursor CURSOR FAST_FORWARD FOR 
	SELECT optionCode, textToReplace
		FROM @newRules
		
OPEN newRuleCursor

FETCH NEXT FROM newRuleCursor
	INTO @optionCode, @textToReplace
	
WHILE @@FETCH_STATUS = 0
	begin
		-- magic happens here!
		
		SET @ruleId = null
		
		-- find the rule?
		SELECT @ruleId = ruleId
			FROM Templates.OptionCodeRules
			WHERE textToReplace = @textToReplace
			AND textToReplaceWith = ''
			
		if @ruleId IS NULL
			begin
			
				-- make new rule
				SELECT @ruleID =  MAX(ruleID)
					FROM Templates.OptionCodeRules
					
				SET @ruleId = @ruleId + 1
			
				INSERT INTO Templates.OptionCodeRules
					(ruleId, textToReplace, textToReplaceWith)
					VALUES(@ruleId, @textToReplace, '')
					
			end
			
			
		-- is the option code tied to this rule
		if NOT EXISTS (SELECT *
							FROM Templates.optioncodeRuleMappings
							WHERE ruleId = @ruleId
							AND optionCode = @optionCode)
			begin
				-- insert code to rule
				INSERT INTO Templates.OptionCodeRuleMappings
					(ruleId, optionCode, businessunitid, inventoryType)
					VALUES(@ruleId, @optionCode, 100150, 2) -- default to 1stLook and used!!!
			end

		/* testing */			
		--SELECT *
		--	FROM Templates.OptionCodeRules
		--	INNER JOIN templates.OptionCodeRuleMappings ON OptionCodeRules.ruleId = OptionCodeRuleMappings.ruleId
		--	WHERE textToReplace = @textToReplace
		--	AND optionCode = @optionCode
	
		
		
		-- magic ends
		
		-- loop
		FETCH NEXT FROM newRuleCursor
			INTO @optionCode, @textToReplace
	end

		
		
CLOSE newRuleCursor		
DEALLOCATE newRuleCursor


/*

*/

