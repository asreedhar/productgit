-- FB 31043
IF EXISTS (
	SELECT	*
	FROM	[sysobjects]
	WHERE	[type] = 'D'
		AND	[name] = 'DF_settings_BucketAlerts_MonthlyEmail'
)
	ALTER TABLE [settings].[BucketAlerts]
		DROP CONSTRAINT DF_settings_BucketAlerts_MonthlyEmail

IF EXISTS (
	SELECT	1
	FROM	[sys].[columns]
	WHERE	[name] LIKE 'MonthlyEmail'
		AND	[object_id] = OBJECT_ID('[settings].[BucketAlerts]')
)
	EXEC SP_RENAME 'Settings.BucketAlerts.MonthlyEmail', 'BionicHealthReportEmail', 'COLUMN'

IF NOT EXISTS (
	SELECT	*
	FROM	[sysobjects]
	WHERE	[type] = 'D'
		AND	[name] = 'DF_settings_BucketAlerts_BionicHealthReportEmail'
)
	ALTER TABLE [settings].[BucketAlerts]
		ADD CONSTRAINT DF_settings_BucketAlerts_BionicHealthReportEmail DEFAULT ('') FOR [BionicHealthReportEmail]

IF EXISTS (
	SELECT	*
	FROM	[sysobjects]
	WHERE	[type] = 'D'
		AND	[name] = 'DF_settings_BucketAlerts_MonthlyEmailBounced'
)
	ALTER TABLE [settings].[BucketAlerts]
		DROP CONSTRAINT DF_settings_BucketAlerts_MonthlyEmailBounced

IF EXISTS (
	SELECT	1
	FROM	[sys].[columns]
	WHERE	[name] LIKE 'MonthlyEmailBounced'
		AND	[object_id] = OBJECT_ID('[settings].[BucketAlerts]')
)
	EXEC SP_RENAME 'Settings.BucketAlerts.MonthlyEmailBounced', 'BionicHealthReportEmailBounced', 'COLUMN'

IF NOT EXISTS (
	SELECT	*
	FROM	[sysobjects]
	WHERE	[type] = 'D'
		AND	[name] = 'DF_settings_BucketAlerts_BionicHealthReportEmailBounced'
)
	ALTER TABLE [settings].[BucketAlerts]
		ADD CONSTRAINT DF_settings_BucketAlerts_BionicHealthReportEmailBounced DEFAULT (0) FOR [BionicHealthReportEmailBounced]
GO