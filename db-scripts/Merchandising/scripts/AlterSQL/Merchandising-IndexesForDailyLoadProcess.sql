create nonclustered index IX_optionid_chromeCategoryId on Merchandising.OptionsMapping
(
	optionid,
	chromeCategoryId
)

go

create nonclustered index IX_inventoryId_chromeStyleId on builder.OptionsConfiguration
(
	inventoryId,
	chromeStyleId
)
go

create nonclustered index IX_inventoryId_optionID on builder.VehicleOptions
(
	inventoryId,
	optionID
)
go