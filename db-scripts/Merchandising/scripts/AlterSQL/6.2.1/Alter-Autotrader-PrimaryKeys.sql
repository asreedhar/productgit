IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[merchandising].[AutoTraderExSummary]') AND name = N'PK_AutoTraderExSummary')
ALTER TABLE [merchandising].[AutoTraderExSummary] DROP CONSTRAINT [PK_AutoTraderExSummary]

ALTER TABLE [merchandising].[AutoTraderExSummary] ADD  CONSTRAINT [PK_AutoTraderExSummary] PRIMARY KEY CLUSTERED 
(
	[businessUnitID] ASC,
	[month] ASC, 
	RequestId ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]



IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[merchandising].[AutoTraderPerfSummary]') AND name = N'PK_AutoTraderPerfSummary')
ALTER TABLE [merchandising].[AutoTraderPerfSummary] DROP CONSTRAINT [PK_AutoTraderPerfSummary]

ALTER TABLE [merchandising].[AutoTraderPerfSummary] ADD  CONSTRAINT [PK_AutoTraderPerfSummary] PRIMARY KEY CLUSTERED 
(
	[businessUnitID] ASC,
	[month] ASC,
	RequestId ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


