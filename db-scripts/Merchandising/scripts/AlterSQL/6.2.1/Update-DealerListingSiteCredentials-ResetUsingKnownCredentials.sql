-- Set a few known dealer listing site credentials...we can't count on any others being good.
-- Not to say that these are guaranteed to be good, but the others are probably all bad.
-- Update 03/25/2011 - we found out that about half are bad.  We will leave the bad ones there, and just replace these we've already collected.

UPDATE dls  
SET userName = 
CASE
WHEN bu.BusinessUnit = 'Bloomington Hyundai Mitsubishi' THEN 'luther1'
WHEN bu.BusinessUnit = 'Brookdale Honda' THEN 'Brookdaleh'
WHEN bu.BusinessUnit = 'Burnsville Volkswagen' THEN 'agadient'
WHEN bu.BusinessUnit = 'Hudson Chrysler Jeep Dodge' THEN 'LutherHudson'
WHEN bu.BusinessUnit = 'Infiniti of Bloomington' THEN ' HAN1311'
WHEN bu.BusinessUnit = 'Luther Brookdale Mazda Mitsubishi' THEN 'vizenor'
WHEN bu.BusinessUnit = 'Luther Family Buick GMC' THEN 'luther455'
WHEN bu.BusinessUnit = 'Luther Jaguar Land Rover Minneapolis' THEN 'Jaguar8905'
WHEN bu.BusinessUnit = 'Luther Nissan Kia' THEN 'luther'
WHEN bu.BusinessUnit = 'Luther Westside Volkswagen' THEN 'westsidevw'
WHEN bu.BusinessUnit = 'North Country Ford Lincoln Mercury' THEN '21858'
WHEN bu.BusinessUnit = 'Rudy Luther Toyota' THEN 'montebion'
WHEN bu.BusinessUnit = 'Toyota City' THEN 'toyotacity612'
WHEN bu.BusinessUnit = 'Luther Park Place Motor Cars' THEN 'jtaylor'
WHEN bu.BusinessUnit = 'White Bear Acura Subaru' THEN 'whitebear1'
WHEN bu.BusinessUnit = 'Bloomington Acura Subaru' THEN 'blooming'
END,
password = 
CASE
WHEN bu.BusinessUnit = 'Bloomington Hyundai Mitsubishi' THEN '1750American'
WHEN bu.BusinessUnit = 'Brookdale Honda' THEN 'Amanda21'
WHEN bu.BusinessUnit = 'Burnsville Volkswagen' THEN 'BVW@55306'
WHEN bu.BusinessUnit = 'Hudson Chrysler Jeep Dodge' THEN 'Mad1syn03'
WHEN bu.BusinessUnit = 'Infiniti of Bloomington' THEN '1311SCOTT '
WHEN bu.BusinessUnit = 'Luther Brookdale Mazda Mitsubishi' THEN 'Abbie1995'
WHEN bu.BusinessUnit = 'Luther Family Buick GMC' THEN 'Luther2010'
WHEN bu.BusinessUnit = 'Luther Jaguar Land Rover Minneapolis' THEN 'Jaguar12'
WHEN bu.BusinessUnit = 'Luther Nissan Kia' THEN 'Metrokia1'
WHEN bu.BusinessUnit = 'Luther Westside Volkswagen' THEN 'Tiguan09'
WHEN bu.BusinessUnit = 'North Country Ford Lincoln Mercury' THEN 'peggy08'
WHEN bu.BusinessUnit = 'Rudy Luther Toyota' THEN '22030Rudy'
WHEN bu.BusinessUnit = 'Toyota City' THEN 'Paula1960'
WHEN bu.BusinessUnit = 'Luther Park Place Motor Cars' THEN 'Chimaera'
WHEN bu.BusinessUnit = 'White Bear Acura Subaru' THEN 'Whitebear1'
WHEN bu.BusinessUnit = 'Bloomington Acura Subaru' THEN 'Acura1234'
END
FROM settings.DealerListingSites dls
JOIN imt.dbo.BusinessUnit bu ON dls.businessUnitID = bu.BusinessUnitID
WHERE 
dls.destinationID = 2 -- AutoTrader.com
AND bu.BusinessUnit IN 
('Bloomington Hyundai Mitsubishi'
,'Brookdale Honda'
,'Burnsville Volkswagen'
,'Hudson Chrysler Jeep Dodge'
,'Infiniti of Bloomington'
,'Luther Brookdale Mazda Mitsubishi'
,'Luther Family Buick GMC'
,'Luther Jaguar Land Rover Minneapolis'
,'Luther Nissan Kia'
,'Luther Westside Volkswagen'
,'North Country Ford Lincoln Mercury'
,'Rudy Luther Toyota'
,'Toyota City'
,'Luther Park Place Motor Cars'
,'White Bear Acura Subaru'
,'Bloomington Acura Subaru')
