if object_id('audit.DealerListingSites') is null

	create table audit.DealerListingSites
	(
		rowId int identity(0,1) not null,
		actionType char(1) not null, -- [I]nsert, [U]pdate, [D]elete, [O]riginal Record/Unknown
		actionDate datetime not null,
		suser_sname nvarchar(128) not null,
		--- base table settings.DealerListingSites schema below but without nullability constraints (no need to break things to audit)

		destinationID int null,
		activeFlag bit null,
		pushFrequency int null,
		businessUnitID int null,
		sendPhotos bit null,
		sendOptions bit null,
		sendDescription bit null,
		sendPricing bit null,
		sendVideos bit null,
		sendNewCars bit null,
		sendUsedCars bit null,
		userName varchar(50) null,
		password varchar(50) null,
		releaseEnabled bit null,
		collectionEnabled bit null,
		lastActiveInventoryCollection datetime NULL,
		lastSoldInventoryCollection datetime NULL,
		ListingSiteDealerId varchar(30) NULL,
		sendOptimalFormat bit null
	)
	
go

if object_id('audit.PK_DealerListingSites') is null
	alter table audit.DealerListingSites add constraint PK_DealerListingSites primary key clustered (rowId)

go

-- actionType 'O' is for Original/Unknown

if not exists(select * from audit.DealerListingSites)
	insert audit.DealerListingSites (actionType, actionDate, suser_sname, destinationID, activeFlag, pushFrequency, businessUnitID, sendPhotos, sendOptions, sendDescription, sendPricing, sendVideos, sendNewCars, sendUsedCars, userName, password, releaseEnabled, collectionEnabled, lastActiveInventoryCollection, lastSoldInventoryCollection, ListingSiteDealerId, sendOptimalFormat)
	select 'O', getdate(), suser_sname(), destinationID, activeFlag, pushFrequency, businessUnitID, sendPhotos, sendOptions, sendDescription, sendPricing, sendVideos, sendNewCars, sendUsedCars, userName, password, releaseEnabled, collectionEnabled, lastActiveInventoryCollection, lastSoldInventoryCollection, ListingSiteDealerId, sendOptimalFormat
	from settings.DealerListingSites 

go
