-- we don't want this old data...only the new from here on out
TRUNCATE TABLE Merchandising.AutoTraderExSummary
TRUNCATE TABLE Merchandising.AutoTraderPerfSummary
TRUNCATE TABLE Merchandising.AutoTraderVehSummary
TRUNCATE TABLE postings.PhoneLeads

-- add requestId
ALTER TABLE Merchandising.AutoTraderExSummary ADD RequestId INT 
ALTER TABLE Merchandising.AutoTraderExSummary ALTER COLUMN RequestId INT NOT NULL
ALTER TABLE Merchandising.AutoTraderPerfSummary ADD RequestId INT 
ALTER TABLE Merchandising.AutoTraderPerfSummary ALTER COLUMN RequestId INT NOT NULL
ALTER TABLE Merchandising.AutoTraderVehSummary ADD RequestId INT
ALTER TABLE Merchandising.AutoTraderVehSummary ALTER COLUMN RequestId INT NOT NULL
ALTER TABLE postings.PhoneLeads ADD RequestId INT 
ALTER TABLE postings.PhoneLeads ALTER COLUMN RequestId INT NOT NULL


ALTER TABLE Merchandising.AutoTraderVehSummary
DROP CONSTRAINT PK_AutoTraderVehSummary

ALTER TABLE merchandising.AutoTraderVehSummary
ADD CONSTRAINT PK_AutoTraderVehSummary PRIMARY KEY CLUSTERED (businessUnitID, RequestId, month, VIN)





