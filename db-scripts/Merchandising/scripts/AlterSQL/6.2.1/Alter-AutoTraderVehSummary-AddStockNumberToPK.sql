
ALTER TABLE Merchandising.AutoTraderVehSummary
ALTER COLUMN StockNumber VARCHAR(50) NOT NULL
go

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[merchandising].[AutoTraderVehSummary]') AND name = N'PK_AutoTraderVehSummary')
ALTER TABLE merchandising.AutoTraderVehSummary
	DROP CONSTRAINT PK_AutoTraderVehSummary
GO
ALTER TABLE merchandising.AutoTraderVehSummary ADD CONSTRAINT
	PK_AutoTraderVehSummary PRIMARY KEY CLUSTERED 
	(
	businessUnitID,
	RequestId,
	month,
	VIN,
	StockNumber
	) 

GO
