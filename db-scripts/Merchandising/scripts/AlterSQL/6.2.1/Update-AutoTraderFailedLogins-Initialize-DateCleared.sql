-- initialize the datecleared to be cleared.  (dateFailed > dateCleared = Not Clear)
UPDATE merchandising.AutoTraderFailedLogins
SET dateCleared = dateFailed
WHERE dateFailed IS NOT NULL