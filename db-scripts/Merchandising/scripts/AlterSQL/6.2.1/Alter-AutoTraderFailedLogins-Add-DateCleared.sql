-- Add a datecleared column to know when a failure has been cleared.
ALTER TABLE merchandising.AutoTraderFailedLogins
ADD dateCleared DATETIME NULL