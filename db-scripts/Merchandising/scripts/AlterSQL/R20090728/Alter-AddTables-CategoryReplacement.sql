if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[CategoryReplacement]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[CategoryReplacement]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [settings].[CategoryReplacement](
	[replacementId] [int] IDENTITY(1,1),
	[replacementType] [int] NOT NULL,		
	[name] [varchar](50),
	[active] [bit] NOT NULL DEFAULT((0)),
	[createdOn] [datetime] NOT NULL DEFAULT (getdate()),
	[createdBy] [varchar](30) NOT NULL DEFAULT(''),
	[lastUpdatedOn] [datetime] NOT NULL DEFAULT(getdate()),
	[lastUpdatedBy] [varchar](30) NOT NULL DEFAULT(''),
	
	CONSTRAINT [PK_CategoryReplacement] PRIMARY KEY  (
		replacementId
	)
)
GO


if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[CategoryReplacement_Item]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[CategoryReplacement_Item]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [settings].[CategoryReplacement_Item](
	[itemId] [int] IDENTITY(1,1),
	[replacementId] [int] NOT NULL,	
	[categoryId] [int] NOT NULL,
	[createdOn] [datetime] NOT NULL DEFAULT (getdate()),
	[createdBy] [varchar](30) NOT NULL DEFAULT(''),
	[lastUpdatedOn] [datetime] NOT NULL DEFAULT(getdate()),
	[lastUpdatedBy] [varchar](30) NOT NULL DEFAULT(''),
	
	
	CONSTRAINT [PK_CategoryReplacement_Item] PRIMARY KEY (
		itemId
	)
)
GO

ALTER TABLE settings.CategoryReplacement_Item
ADD CONSTRAINT FK_CategoryReplacementItem_CategoryReplacement
FOREIGN KEY (replacementId)
REFERENCES settings.CategoryReplacement (replacementId)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[CategoryReplacement_DealerOverride]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[CategoryReplacement_DealerOverride]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [settings].[CategoryReplacement_DealerOverride](
	[overrideId] [int] IDENTITY(1,1),
	[businessUnitId] [int] NOT NULL,	
	[overrideForReplacementId] int NOT NULL,
	[overrideWithReplacementId] int NOT NULL,
	[createdOn] [datetime] NOT NULL DEFAULT (getdate()),
	[createdBy] [varchar](30) NOT NULL DEFAULT(''),
	[lastUpdatedOn] [datetime] NOT NULL DEFAULT(getdate()),
	[lastUpdatedBy] [varchar](30) NOT NULL DEFAULT(''),
	
	CONSTRAINT [PK_CategoryReplacement_DealerOverride] PRIMARY KEY NONCLUSTERED (
		overrideId
	)
)
GO

CREATE CLUSTERED 
INDEX IX_CategoryReplacement_DealerOverride
ON [settings].[CategoryReplacement_DealerOverride] (businessUnitId)
GO

ALTER TABLE settings.CategoryReplacement_DealerOverride
ADD CONSTRAINT FK_CategoryReplacement_DealerOverride_CategoryReplacement__forId
FOREIGN KEY (overrideForReplacementId)
REFERENCES settings.CategoryReplacement (replacementId)
GO

ALTER TABLE settings.CategoryReplacement_DealerOverride
ADD CONSTRAINT FK_CategoryReplacement_DealerOverride_CategoryReplacement__withId
FOREIGN KEY (overrideWithReplacementId)
REFERENCES settings.CategoryReplacement (replacementId)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[CategoryReplacement_Dealer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [settings].[CategoryReplacement_Dealer]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [settings].[CategoryReplacement_Dealer](
	[businessUnitId] [int] NOT NULL,	
	[replacementId] int NOT NULL,	
	[createdOn] [datetime] NOT NULL DEFAULT (getdate()),
	[createdBy] [varchar](30) NOT NULL DEFAULT(''),
	[lastUpdatedOn] [datetime] NOT NULL DEFAULT(getdate()),
	[lastUpdatedBy] [varchar](30) NOT NULL DEFAULT(''),
	
	
	CONSTRAINT [PK_CategoryReplacement_Dealer] PRIMARY KEY NONCLUSTERED (
		businessUnitId, replacementId
	)
)
GO


CREATE CLUSTERED 
INDEX IX_CategoryReplacement_Dealer
ON [settings].[CategoryReplacement_Dealer] (businessUnitId)
GO

ALTER TABLE settings.CategoryReplacement_Dealer
ADD CONSTRAINT FK_CategoryReplacement_Dealer_CategoryReplacement
FOREIGN KEY (replacementId)
REFERENCES settings.CategoryReplacement (replacementId)
GO

--1078 leather
--1074/1075 power seats
--1156/1157 heated seats
--1267/1268 cooled seats
declare @replacementId int, @updater varchar(30)
select @updater = 'jonkelley'

INSERT INTO settings.CategoryReplacement (replacementType, name, active, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (1, 'Heated Seats', 1, getdate(), @updater, getdate(), @updater)
select @replacementId = scope_identity()
INSERT INTO settings.CategoryReplacement_Item (replacementId, categoryId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (@replacementId,1156, getdate(), @updater, getdate(), @updater)
INSERT INTO settings.CategoryReplacement_Item (replacementId, categoryId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (@replacementId,1157, getdate(), @updater, getdate(), @updater)

INSERT INTO settings.CategoryReplacement (replacementType, name, active, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (1, 'Power Seats', 1, getdate(), @updater, getdate(), @updater)
select @replacementId = scope_identity()
INSERT INTO settings.CategoryReplacement_Item (replacementId, categoryId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (@replacementId,1074, getdate(), @updater, getdate(), @updater)
INSERT INTO settings.CategoryReplacement_Item (replacementId, categoryId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (@replacementId,1075, getdate(), @updater, getdate(), @updater)


INSERT INTO settings.CategoryReplacement (replacementType, name, active, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (1, 'Heated Leather Seats', 1, getdate(), @updater, getdate(), @updater)
select @replacementId = scope_identity()
INSERT INTO settings.CategoryReplacement_Item (replacementId, categoryId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (@replacementId,1156, getdate(), @updater, getdate(), @updater)
INSERT INTO settings.CategoryReplacement_Item (replacementId, categoryId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (@replacementId,1157, getdate(), @updater, getdate(), @updater)
INSERT INTO settings.CategoryReplacement_Item (replacementId, categoryId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (@replacementId,1078, getdate(), @updater, getdate(), @updater)


INSERT INTO settings.CategoryReplacement (replacementType, name, active, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (1, 'Heated/Cooled Seats', 1, getdate(), @updater, getdate(), @updater)
select @replacementId = scope_identity()
INSERT INTO settings.CategoryReplacement_Item (replacementId, categoryId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (@replacementId,1156, getdate(), @updater, getdate(), @updater)
INSERT INTO settings.CategoryReplacement_Item (replacementId, categoryId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (@replacementId,1157, getdate(), @updater, getdate(), @updater)
INSERT INTO settings.CategoryReplacement_Item (replacementId, categoryId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (@replacementId,1267, getdate(), @updater, getdate(), @updater)
INSERT INTO settings.CategoryReplacement_Item (replacementId, categoryId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (@replacementId,1268, getdate(), @updater, getdate(), @updater)


INSERT INTO settings.CategoryReplacement (replacementType, name, active, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (1, 'Heated/Cooled Leather Seats', 1, getdate(), @updater, getdate(), @updater)
select @replacementId = scope_identity()
INSERT INTO settings.CategoryReplacement_Item (replacementId, categoryId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (@replacementId,1156, getdate(), @updater, getdate(), @updater)
INSERT INTO settings.CategoryReplacement_Item (replacementId, categoryId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (@replacementId,1157, getdate(), @updater, getdate(), @updater)
INSERT INTO settings.CategoryReplacement_Item (replacementId, categoryId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (@replacementId,1267, getdate(), @updater, getdate(), @updater)
INSERT INTO settings.CategoryReplacement_Item (replacementId, categoryId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (@replacementId,1268, getdate(), @updater, getdate(), @updater)
INSERT INTO settings.CategoryReplacement_Item (replacementId, categoryId, createdOn, createdBy, lastUpdatedOn, lastUpdatedBy) VALUES (@replacementId,1078, getdate(), @updater, getdate(), @updater)
