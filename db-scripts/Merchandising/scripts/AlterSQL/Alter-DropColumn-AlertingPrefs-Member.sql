
DROP INDEX NDX_AlertingPreferences ON settings.AlertingPreferences

ALTER TABLE settings.AlertingPreferences
DROP COLUMN memberId

ALTER TABLE settings.AlertingPreferences
DROP CONSTRAINT PK_AlertingPreferences

ALTER TABLE settings.AlertingPreferences
DROP COLUMN alertingId

ALTER TABLE settings.AlertingPreferences
ADD CONSTRAINT PK_AlertingPreferences
PRIMARY KEY (businessUnitId)