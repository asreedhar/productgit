--	Remove dups, set the correct PKC

SELECT	VehicleId, DataTypeId
INTO	#dups
FROM	Merchandising.builder.VehicleReferenceData
GROUP
BY	VehicleId, DataTypeId HAVING COUNT(*) > 1

SELECT	DISTINCT VRD.*
INTO	#dedup
FROM	builder.VehicleReferenceData VRD
	INNER JOIN #dups D ON VRD.DataTypeId = D.DataTypeId AND VRD.VehicleId = D.VehicleId

DELETE	VRD
FROM	builder.VehicleReferenceData VRD
	INNER JOIN #dups D ON VRD.DataTypeId = D.DataTypeId AND VRD.VehicleId = D.VehicleId

INSERT
INTO	builder.VehicleReferenceData (Value, DataSource, DataTypeId, VehicleId)
SELECT	Value, DataSource, DataTypeId, VehicleId
FROM	#dedup


DROP INDEX ix_VehicleReferenceData_VehicleId_DataTypeId_includes ON builder.VehicleReferenceData 
GO

ALTER TABLE builder.VehicleReferenceData ADD CONSTRAINT PK_builderVehicleReferenceData PRIMARY KEY CLUSTERED (VehicleId, DataTypeId)
