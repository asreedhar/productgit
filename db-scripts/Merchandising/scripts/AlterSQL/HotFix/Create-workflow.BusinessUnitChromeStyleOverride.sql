CREATE TABLE workflow.BusinessUnitChromeStyleOverride (
	BusinessUnitID	INT NOT NULL,
	ChromeStyleID	INT NOT NULL,
	StyleName	VARCHAR(60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	Trim		VARCHAR(35) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CFModelName	VARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CFStyleName	VARCHAR(255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT PK_workflowBusinessUnitChromeStyleOverride PRIMARY KEY CLUSTERED (BusinessUnitID, ChromeStyleID)
	)

GO	
EXEC sp_SetTableDescription 
'workflow.BusinessUnitChromeStyleOverride',
'Table used to store values used by the workflow.Inventory to override Chrome Style reference data per dealer (BUID).  This is meant as a stop-gap solution until a proper abstraction layer between MAX and Chome can be implemented.'

EXEC sp_SetColumnDescription 
'workflow.BusinessUnitChromeStyleOverride.StyleName',
'If set, overrides the Chrome reference StyleName value in workflow.Inventory for the given BUID/StyleID (future consideration)'

EXEC sp_SetColumnDescription 
'workflow.BusinessUnitChromeStyleOverride.Trim',
'If set, overrides the Chrome reference Trim value in workflow.Inventory for the given BUID/StyleID'

EXEC sp_SetColumnDescription 
'workflow.BusinessUnitChromeStyleOverride.CFModelName',
'If set, overrides the Chrome reference CFModelName value in workflow.Inventory for the given BUID/StyleID'

EXEC sp_SetColumnDescription 
'workflow.BusinessUnitChromeStyleOverride.CFStyleName',
'If set, overrides the Chrome reference CFStyleName value in workflow.Inventory for the given BUID/StyleID (future consideration)'



GO

ALTER TABLE workflow.BusinessUnitChromeStyleOverride ADD StyleNameWOTrim VARCHAR(55) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
----------------------------------------------------------------------------------------------------------------------------
--	Add global rules to remove " South Africa" from StyleName, CFStyleName, and StyleNameWOTrim in the reference set:
----------------------------------------------------------------------------------------------------------------------------

INSERT 
INTO	workflow.BusinessUnitChromeStyleOverride (BusinessUnitID, ChromeStyleID, StyleName, Trim, CFModelName, CFStyleName, StyleNameWOTrim)

SELECT	100150, StyleID, REPLACE(StyleName,' South Africa',''), NULL, NULL, REPLACE(CFStyleName,' South Africa',''), REPLACE(StyleNameWOTrim,' South Africa','')
FROM	VehicleCatalog.Chrome.Styles
WHERE	(	StyleName LIKE '% South Africa%' 
		OR CFStyleName LIKE '% South Africa'
		OR StyleNameWOTrim LIKE '% South Africa'
		)
	AND CountryCode = 1