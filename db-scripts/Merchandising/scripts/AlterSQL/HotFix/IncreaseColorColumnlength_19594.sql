
--Increase the length of the table to match the store proc  'VehicleConfiguration#Save' saving to it. Bugfix 19594

ALTER TABLE [builder].[OptionsConfiguration] ALTER COLUMN [extColor1] [varchar](100) ;

ALTER TABLE [builder].[OptionsConfiguration] ALTER COLUMN [extColor2] [varchar](100) ;

ALTER TABLE [builder].[OptionsConfiguration] ALTER COLUMN [intColor] [varchar](100) ;
	