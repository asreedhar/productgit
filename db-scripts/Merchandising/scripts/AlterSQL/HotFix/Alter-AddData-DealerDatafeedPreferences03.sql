DECLARE @BUID INT

SET @BUID=103670 -- Dodgeland of Richland
IF (SELECT COUNT(*) FROM settings.DealerDatafeedPreferences WHERE BusinessUnitID=@BUID AND DatafeedCode='AULtec')=0
	INSERT INTO settings.DealerDatafeedPreferences
	        (BusinessUnitID ,
	         DatafeedCode ,
	         SendHTMLDescription)
	VALUES  (@BUID , -- BusinessUnitID - int
	         'AULtec' , -- DatafeedCode - varchar(20)
	         0  -- SendHTMLDescription - bit
	         )
	         
UPDATE settings.DealerDatafeedPreferences
SET SendHTMLDescription=1
WHERE BusinessUnitID=@BUID
AND DatafeedCode='AULtec'
