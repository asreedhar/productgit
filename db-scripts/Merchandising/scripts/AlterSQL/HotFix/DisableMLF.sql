-- disable GM, Chrysler, Honda MLF for all stores except Penske, Hendrick, and Windy Pontiac

with
Penske as
(			
	select			
		dealer.BusinessUnitID	
	from			
		(		
			select *	
			from IMT.dbo.BusinessUnit bu	
			where	
				bu.Active = 1
				and bu.BusinessUnitTypeID = 1
		) corp		
		join IMT.dbo.BusinessUnitRelationship corpRegion		
			on corp.BusinessUnitID = corpRegion.ParentID	
		join		
		(		
			select *	
			from IMT.dbo.BusinessUnit bu	
			where	
				bu.active = 1
				and bu.BusinessUnitTypeID = 2
		) region		
			on corpRegion.BusinessUnitID = region.BusinessUnitID	
		join IMT.dbo.BusinessUnitRelationship regionDealerGroup		
			on region.BusinessUnitID = regionDealerGroup.ParentID	
		join		
		(		
			select *	
			from IMT.dbo.BusinessUnit bu	
			where	
				bu.active = 1
				and bu.BusinessUnitTypeID = 3
		) dealerGroup		
			on regionDealerGroup.BusinessUnitID = dealerGroup.BusinessUnitID	
		join IMT.dbo.BusinessUnitRelationship dealerGroupDealer		
			on dealerGroup.BusinessUnitID = dealerGroupDealer.ParentID	
		join		
		(		
			select *	
			from IMT.dbo.BusinessUnit bu	
			where	
				bu.active = 1
				and bu.BusinessUnitTypeID = 4
		) dealer		
			on dealerGroupDealer.BusinessUnitID = dealer.BusinessUnitID	
	where			
		corp.BusinessUnit like '%penske%'		
)
, Hendrick as
(
	select			
		dealer.BusinessUnitID,
		dealer.BusinessUnit
	from			
		(		
			select *	
			from IMT.dbo.BusinessUnit bu	
			where	
				bu.active = 1
				and bu.BusinessUnitTypeID = 3
		) dealerGroup		
		join IMT.dbo.BusinessUnitRelationship dealerGroupDealer		
			on dealerGroup.BusinessUnitID = dealerGroupDealer.ParentID	
		join		
		(		
			select *	
			from IMT.dbo.BusinessUnit bu	
			where	
				bu.active = 1
				and bu.BusinessUnitTypeID = 4
		) dealer		
			on dealerGroupDealer.BusinessUnitID = dealer.BusinessUnitID	
	where			
		dealerGroup.BusinessUnit like '%hendrick%'	
)
update at
	set active = 0
from
	Merchandising.templates.AdTemplates at
where 
	(
		(
			-- GM
			at.templateID in (8458, 8459, 8506, 8507, 8462, 8463, 8464, 8465, 8382, 8401, 8402, 8403, 8404, 8456, 8457, 8466, 8496, 8497, 8701, 8702, 8405, 8406, 8407, 8460, 8461, 8498, 8499)
			or at.SourceTemplateId in (8458, 8459, 8506, 8507, 8462, 8463, 8464, 8465, 8382, 8401, 8402, 8403, 8404, 8456, 8457, 8466, 8496, 8497, 8701, 8702, 8405, 8406, 8407, 8460, 8461, 8498, 8499)
		)
		or
		(
			-- Chrysler
			at.templateID in (8564, 8627, 8629, 8630, 8643, 8644, 8651, 8652, 8677, 8678, 8679, 8680, 8681, 8682, 8683, 8684, 8697, 8698, 8699, 8700, 8746, 8747, 8748, 8749, 8792, 8793, 8824, 8825, 8826, 8845, 8852, 8854, 8855, 8858, 8859, 9312, 9313, 9314, 9315)
			or at.SourceTemplateId in (8564, 8627, 8629, 8630, 8643, 8644, 8651, 8652, 8677, 8678, 8679, 8680, 8681, 8682, 8683, 8684, 8697, 8698, 8699, 8700, 8746, 8747, 8748, 8749, 8792, 8793, 8824, 8825, 8826, 8845, 8852, 8854, 8855, 8858, 8859, 9312, 9313, 9314, 9315)
		)
		or
		(
			-- Honda
			at.templateID in (7392, 7393, 7416, 7432, 7466, 7693, 7774, 7806, 7814, 7815)
			or at.SourceTemplateId in (7392, 7393, 7416, 7432, 7466, 7693, 7774, 7806, 7814, 7815)
		)
	)
	and not exists(select * from Penske where BusinessUnitID = at.businessUnitID)
	and not exists(select * from Hendrick where BusinessUnitID = at.businessUnitID)
	and not exists(select * from IMT.dbo.BusinessUnit where BusinessUnitID = at.businessUnitID and BusinessUnit like '%windy city%')
	