DECLARE @BUID INT

SET @BUID=104675 -- Kingdom Chevrolet
IF (SELECT COUNT(*) FROM settings.DealerDatafeedPreferences WHERE BusinessUnitID=@BUID AND DatafeedCode='AULtec')=0
	INSERT INTO settings.DealerDatafeedPreferences
	        (BusinessUnitID ,
	         DatafeedCode ,
	         SendHTMLDescription)
	VALUES  (@BUID , -- BusinessUnitID - int
	         'AULtec' , -- DatafeedCode - varchar(20)
	         0  -- SendHTMLDescription - bit
	         )
	         
UPDATE settings.DealerDatafeedPreferences
SET SendHTMLDescription=1
WHERE BusinessUnitID=@BUID
AND DatafeedCode='AULtec'



SET @BUID=104547 -- Western Avenue Nissan
IF (SELECT COUNT(*) FROM settings.DealerDatafeedPreferences WHERE BusinessUnitID=@BUID AND DatafeedCode='AULtec')=0
	INSERT INTO settings.DealerDatafeedPreferences
	        (BusinessUnitID ,
	         DatafeedCode ,
	         SendHTMLDescription)
	VALUES  (@BUID , -- BusinessUnitID - int
	         'AULtec' , -- DatafeedCode - varchar(20)
	         0  -- SendHTMLDescription - bit
	         )
	         
UPDATE settings.DealerDatafeedPreferences
SET SendHTMLDescription=1
WHERE BusinessUnitID=@BUID
AND DatafeedCode='AULtec'



SET @BUID=103666 -- Beardmore Chevrolet
IF (SELECT COUNT(*) FROM settings.DealerDatafeedPreferences WHERE BusinessUnitID=@BUID AND DatafeedCode='AULtec')=0
	INSERT INTO settings.DealerDatafeedPreferences
	        (BusinessUnitID ,
	         DatafeedCode ,
	         SendHTMLDescription)
	VALUES  (@BUID , -- BusinessUnitID - int
	         'AULtec' , -- DatafeedCode - varchar(20)
	         0  -- SendHTMLDescription - bit
	         )
	         
UPDATE settings.DealerDatafeedPreferences
SET SendHTMLDescription=1
WHERE BusinessUnitID=@BUID
AND DatafeedCode='AULtec'



SET @BUID=102641 -- Red Wing Chevrolet
IF (SELECT COUNT(*) FROM settings.DealerDatafeedPreferences WHERE BusinessUnitID=@BUID AND DatafeedCode='AULtec')=0
	INSERT INTO settings.DealerDatafeedPreferences
	        (BusinessUnitID ,
	         DatafeedCode ,
	         SendHTMLDescription)
	VALUES  (@BUID , -- BusinessUnitID - int
	         'AULtec' , -- DatafeedCode - varchar(20)
	         0  -- SendHTMLDescription - bit
	         )
	         
UPDATE settings.DealerDatafeedPreferences
SET SendHTMLDescription=1
WHERE BusinessUnitID=@BUID
AND DatafeedCode='AULtec'



SET @BUID=104265 -- Bob Lindsay
IF (SELECT COUNT(*) FROM settings.DealerDatafeedPreferences WHERE BusinessUnitID=@BUID AND DatafeedCode='AULtec')=0
	INSERT INTO settings.DealerDatafeedPreferences
	        (BusinessUnitID ,
	         DatafeedCode ,
	         SendHTMLDescription)
	VALUES  (@BUID , -- BusinessUnitID - int
	         'AULtec' , -- DatafeedCode - varchar(20)
	         0  -- SendHTMLDescription - bit
	         )
	         
UPDATE settings.DealerDatafeedPreferences
SET SendHTMLDescription=1
WHERE BusinessUnitID=@BUID
AND DatafeedCode='AULtec'

