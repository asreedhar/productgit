
--Increase the length of the table to match the store proc  'VehicleConfiguration#Save' saving to it. Bugfix 19594

ALTER TABLE [builder].[OptionsConfiguration] ALTER COLUMN [extColor1] [varchar](100) not null;

ALTER TABLE [builder].[OptionsConfiguration] ALTER COLUMN [extColor2] [varchar](100)  not null;

ALTER TABLE [builder].[OptionsConfiguration] ALTER COLUMN [intColor] [varchar](100) not null ;

--Increase the length of the audit table also to match the store proc  'VehicleConfiguration#Save' saving to it. Bugfix 19594

ALTER TABLE [audit].[OptionsConfiguration] ALTER COLUMN [extColor1] [varchar](100) not null;

ALTER TABLE [audit].[OptionsConfiguration] ALTER COLUMN [extColor2] [varchar](100)  not null;

ALTER TABLE [audit].[OptionsConfiguration] ALTER COLUMN [intColor] [varchar](100) not null ;




	