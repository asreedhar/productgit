-- This script inserts all of the 2014 models into the Ford Direct settings table. It should only be run once,
-- although nothing bad will happen if it's run multiple times.
-- ZB, 9/6/2013


--SELECT *
Delete
FROM Merchandising.settings.FordAutoLoadNew_YearMakeModel
WHERE [Year] = 2014
GO

-- insert all 2014 models
INSERT INTO Merchandising.settings.FordAutoLoadNew_YearMakeModel([Year],Make,Model,CreatedOn,CreatedBy)
VALUES

(2014,'Ford','Fiesta',GETDATE(),'zbrown'),
(2014,'Ford','Focus',GETDATE(),'zbrown'),
(2014,'Ford','Fusion',GETDATE(),'zbrown'),
(2014,'Ford','Mustang',GETDATE(),'zbrown'),
(2014,'Ford','C-MAX',GETDATE(),'zbrown'),
(2014,'Ford','Taurus',GETDATE(),'zbrown'),

(2014,'Ford','Escape',GETDATE(),'zbrown'),
(2014,'Ford','Edge',GETDATE(),'zbrown'),
(2014,'Ford','Explorer',GETDATE(),'zbrown'),
(2014,'Ford','Flex',GETDATE(),'zbrown'),
(2014,'Ford','Expedition',GETDATE(),'zbrown'),

(2014,'Ford','F-150',GETDATE(),'zbrown'),
(2014,'Ford','F-250',GETDATE(),'zbrown'),
(2014,'Ford','F-350',GETDATE(),'zbrown'),
(2014,'Ford','F-450',GETDATE(),'zbrown'),
(2014,'Ford','Transit',GETDATE(),'zbrown'),
(2014,'Ford','eseries',GETDATE(),'zbrown')



