
--Hotfix BUGZID 19044

--Update the defaulttemplateid to 2457 (Luxury/Key Features Enhanced+) template

--for all the dealers with MaxAd turned on and defaulttemplateid null
update  [Merchandising].[settings].[MerchandisingDescriptionPreferences]  

set defaulttemplateid = '2457'

FROM [Merchandising].[settings].[MerchandisingDescriptionPreferences] prefs

inner join IMT.dbo.dealerupgrade du on du.businessunitid = prefs.businessunitid 

where defaulttemplateid is null and du.dealerupgradecd = 24 

