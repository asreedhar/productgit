if (object_id('dashboard.CalculateInvDiffQueue') is not null)
	drop table dashboard.CalculateInvDiffQueue
go

create table dashboard.CalculateInvDiffQueue
( 
	idx int not null identity(1,1),
	LoadId int,
	DatasourceInterfaceID int,
	DestinationID int,
	DebugVin varchar(17) null,
	DebugStockNumber varchar(15) null,
	UseVinMigration bit default 0,
	Processed datetime null,
	constraint PK_CalculateInvDiffQueue primary key clustered ( idx )
)
go



