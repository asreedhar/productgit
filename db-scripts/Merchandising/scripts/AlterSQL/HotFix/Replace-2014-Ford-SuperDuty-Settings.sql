DELETE --SELECT *
FROM Merchandising.settings.FordAutoLoadNew_YearMakeModel
WHERE [Year] = 2014 AND Model IN ('F-250', 'F-350', 'F-450')
GO

INSERT INTO 
Merchandising.settings.FordAutoLoadNew_YearMakeModel
        ( Year ,
          Make ,
          Model ,
          CreatedOn ,
          CreatedBy ,
          UpdatedOn ,
          UpdatedBy
        )
VALUES  ( 2014 , -- Year - int
          'Ford' , -- Make - varchar(20)
          'SuperDuty' , -- Model - varchar(30)
          GETDATE() , -- CreatedOn - datetime
          'zbrown' , -- CreatedBy - varchar(32)
          null , -- UpdatedOn - datetime
          null  -- UpdatedBy - varchar(32)
        )
GO