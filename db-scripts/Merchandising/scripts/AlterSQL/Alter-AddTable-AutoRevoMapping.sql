if exists (select * from dbo.sysobjects where id = object_id(N'[merchandising].[AutoRevoMapping]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [merchandising].[AutoRevoMapping]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE TABLE [merchandising].[AutoRevoMapping](
	[autoRevoOptionId] [int] NULL,
	[description] [nvarchar](255) NULL,
	[chromeCategoryId] [int] NULL
)


CREATE CLUSTERED 
INDEX IX_AutoRevoMapping
ON merchandising.AutoRevoMapping (chromeCategoryId)
