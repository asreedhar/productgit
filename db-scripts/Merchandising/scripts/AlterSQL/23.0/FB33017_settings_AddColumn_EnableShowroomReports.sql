-- if column default ... remove it
if EXISTS (SELECT * 
				FROM sysobjects
				WHERE TYPE = 'D'
				AND NAME = 'DF_merchandising_EnableShowroomReports')
	begin
		ALTER TABLE settings.merchandising
			DROP CONSTRAINT DF_merchandising_EnableShowroomReports
	end
	
-- if column exists ... remove it
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'EnableShowroomReports'
				AND object_id = object_id('settings.merchandising'))
	begin
		ALTER TABLE settings.merchandising
			DROP COLUMN EnableShowroomReports
	end

-- add column to allow Showroom to enable book valuations
ALTER TABLE settings.merchandising
	ADD EnableShowroomReports bit NOT NULL CONSTRAINT DF_merchandising_EnableShowroomReports DEFAULT 0
GO

