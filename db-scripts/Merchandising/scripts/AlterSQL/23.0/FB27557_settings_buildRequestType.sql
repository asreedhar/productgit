-- FB 27557 - buildRequestType

-- drop constraint on BuildRequestTypeId
IF EXISTS (SELECT * 
				FROM sysobjects
				WHERE NAME ='FK_credentialedSites_buildRequestType'
				AND xtype = 'F')
	begin
		ALTER TABLE settings.credentialedSites
			DROP CONSTRAINT FK_credentialedSites_buildRequestType
	end
go

-- drop settings.BuildRequestType
if EXISTS (SELECT * 
			FROM dbo.sysobjects 
			WHERE id = object_id(N'[settings].[BuildRequestType]') 
			AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE [settings].[BuildRequestType]
GO

CREATE TABLE settings.BuildRequestType
(
	BuildRequestTypeId tinyint NOT NULL,
	BuildRequestType varchar(50) NOT NULL
	CONSTRAINT PK_settings_BuildRequestType PRIMARY KEY (BuildRequestTypeID) 
)
GO

-- seed settings.BuildRequestType
INSERT INTO settings.BuildRequestType
	VALUES (1, 'Connotate')

INSERT INTO settings.BuildRequestType
	VALUES (2, 'Slurpee')

INSERT INTO settings.BuildRequestType
	VALUES (3, 'Honda eConfig')
