-- FB 32585 - Time sensitive infomation text replacement

DECLARE @newRules TABLE (optionCode varchar(20), textToReplace varchar(150))

INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'Note: SYNC services receive a complimentary 3 year prepaid subscription (1 phone/primary account)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'SYNC Services (3 year complimentary subscription)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/6-month pre-paid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/a 6-year prepaid subscription in the 48 contiguous states')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'NOTE: SYNC Services receive a complimentary 3 year prepaid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/(6) month free trial subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '6-year prepaid subscription 48 contiguous states')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '6-year Prepaid Subscription in the 48 Contiguous States')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'SYNC services receive a complimentary 3 year prepaid subscription (1 phone/primary account)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'SYNC services receive a complimentary 1 year prepaid subscription (1 phone/primary account)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '3-year prepaid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/12 month subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/6-month prepaid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '6-month prepaid subscription 48 contiguous states')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/6 month prepaid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'receive a complimentary (3) year pre-paid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/6-year prepaid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '6-month prepaid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'includes 6 month prepaid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'receive a complimentary one year prepaid subscription (one phone/primary account)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '6 month pre paid subscription (48 contiguous states)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'Note: SYNC services include a complimentary one year prepaid subscription (one phone/primary account)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'Note: Sync services recieves a complimentary (1) year prepaid subscription and 20 operator assist sessions')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '6-month free trial subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/5 year subscription from vehicle sale date as recorded by dealer')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'includes a 5-year pre-paid subscription 48 contiguous states')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'includes 6-month pre-paid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '5-year prepaid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '6-month pre-paid subscription 48 contiguous states')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'SYNC Services receive a complimentary 1 year prepaid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/6-month subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'after 6-month trial expires')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'receive a complimentary 3 year pre-paid subscription (1 phone/primary account)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1 year of Crew Chief subscription service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'after trial expires')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1 year of Crew Chief Service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1-year of Crew Chief subscription service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/5 years of service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '6-month pre-paid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '6 month SIRIUS Travel Link pre-paid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(includes 6 month complimentary subscription)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '-inc: (6) month free trial subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/60 months subscription from vehicle sale date as recorded by dealer')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'includes 5-year pre-paid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'with 5 years of service subscriptions')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '-inc: 6 month SIRIUS subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '-inc: 6 month pre-paid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'receive 3-year pre-paid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'receive a complimentary three year pre paid subscription (one phone/primary account)* ')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(6) month free subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '1 year service subscription of Crew Chief service')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'One year service subscription of')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(6) month Sirius subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(6) month prepaid subscription')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'receive a complimentary three year pre-paid subscription (one phone/primary account)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'receive a complimentary 3-year pre-paid subscription ((1) phone/primary account)')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'one-year service subscription of')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '(6) free trial months')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), 'w/(6) months free trial')




/*
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '')
INSERT INTO @newRules	VALUES(REPLICATE('~', 20), '')
*/


DECLARE @textToReplace varchar(150), @optionCode varchar(20)

DECLARE @ruleId int


DECLARE newRuleCursor CURSOR FAST_FORWARD FOR 
	SELECT optionCode, textToReplace
		FROM @newRules
		
OPEN newRuleCursor

FETCH NEXT FROM newRuleCursor
	INTO @optionCode, @textToReplace
	
WHILE @@FETCH_STATUS = 0
	begin
		-- magic happens here!
		
		SET @ruleId = null
		
		-- find the rule?
		SELECT @ruleId = ruleId
			FROM Templates.OptionCodeRules
			WHERE textToReplace = @textToReplace
			AND textToReplaceWith = ''
			
		if @ruleId IS NULL
			begin
			
				-- make new rule
				SELECT @ruleID =  MAX(ruleID)
					FROM Templates.OptionCodeRules
					
				SET @ruleId = @ruleId + 1
			
				INSERT INTO Templates.OptionCodeRules
					(ruleId, textToReplace, textToReplaceWith)
					VALUES(@ruleId, @textToReplace, '')
					
			end
			
			
		-- is the option code tied to this rule
		if NOT EXISTS (SELECT *
							FROM Templates.optioncodeRuleMappings
							WHERE ruleId = @ruleId
							AND optionCode = @optionCode)
			begin
				-- insert code to rule
				INSERT INTO Templates.OptionCodeRuleMappings
					(ruleId, optionCode, businessunitid, inventoryType)
					VALUES(@ruleId, @optionCode, 100150, 2) -- default to 1stLook and used!!!
			end

		/* testing */			
		--SELECT *
		--	FROM Templates.OptionCodeRules
		--	INNER JOIN templates.OptionCodeRuleMappings ON OptionCodeRules.ruleId = OptionCodeRuleMappings.ruleId
		--	WHERE textToReplace = @textToReplace
		--	AND optionCode = @optionCode
	
		
		
		-- magic ends
		
		-- loop
		FETCH NEXT FROM newRuleCursor
			INTO @optionCode, @textToReplace
	end

		
		
CLOSE newRuleCursor		
DEALLOCATE newRuleCursor
