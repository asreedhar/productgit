

-- add new prolonged delay
if NOT EXISTS (SELECT *
					FROM builder.autoloadStatusTypes
					WHERE statusTypeID = -8
			)
	begin
		INSERT INTO builder.autoloadStatusTypes
			(statusTypeId, statusType, statusTypeDescription, requeueMinutes)
			VALUES(-8, 'ProlongedDelay', 'Prolonged Delay', 43200)
	end
go

-- drop buildRequestLimit
IF  EXISTS (SELECT * 
				FROM sys.objects 
				WHERE object_id = OBJECT_ID(N'[settings].[buildRequestLimit]') AND type in (N'U')
				)
		DROP TABLE [settings].[buildRequestLimit]
GO

-- create buildRequestLimit
CREATE TABLE settings.BuildRequestLimit
(
	siteId			int not null,
	statusTypeId	int not null,
	limit			smallint not null,
	resultStatusId	int not null
)
go

ALTER TABLE settings.BuildRequestLimit
	ADD CONSTRAINT PK_BuildRequestLimit
	PRIMARY KEY NONCLUSTERED (siteId, StatusTypeId)
go

ALTER TABLE settings.BuildRequestLimit
	ADD CONSTRAINT PK_BuildRequestLimit_CredentialedSites
	FOREIGN KEY (siteId)
	REFERENCES settings.CredentialedSites(siteId)
go	

ALTER TABLE settings.BuildRequestLimit
	ADD CONSTRAINT PK_BuildRequestLimit_AutoloadStatusTypes
	FOREIGN KEY (statusTypeId)
	REFERENCES builder.autoloadStatusTypes(statusTypeId)
go	

ALTER TABLE settings.BuildRequestLimit
	ADD CONSTRAINT PK_BuildRequestLimit_ResultStatusId
	FOREIGN KEY (resultStatusId)
	REFERENCES builder.autoloadStatusTypes(statusTypeId)
go

-- delete build request limit for Nissan
DELETE settings.buildRequestLimit
	WHERE siteID = 12
	AND statusTypeID = -98
go

-- add first build request limit for Nissan
INSERT INTO settings.buildRequestLimit
	(
		siteId,
		statusTypeId,
		limit,
		resultStatusId
	)
	VALUES
	(
		12, -- Nissan North America
		-98, -- Vehicle Not Found
		5, -- 5 times (limit)
		-8 -- Prolonged delay
	)
go

-- indexes add to make testing for limt quicker
-- drop index if it exists 
IF  EXISTS (SELECT * 
				FROM sys.indexes 
				WHERE object_id = OBJECT_ID(N'[builder].[buildRequestLog]') 
				AND name = N'IX_builder_buildRequestLog_vin'
			)
	DROP INDEX [IX_builder_buildRequestLog_vin] ON [builder].[buildRequestLog]
GO

-- create on vin on build request log
CREATE INDEX [IX_builder_buildRequestLog_vin] ON [builder].[buildRequestLog] ([vin])
GO

-- update vehicle not found to 48 hours
UPDATE builder.autoloadStatusTypes
	SET requeueMinutes = 2880 -- 48 hours
	WHERE statusTypeID = -98



/*

sp_help 'settings.buildRequestLimit'


sp_help 'settings.CredentialedSites'
sp_help 'builder.autoloadStatusTypes'

sp_help 'builder.buildRequestLog'

SELECT * FROM settings.buildREquestLimit

*/

