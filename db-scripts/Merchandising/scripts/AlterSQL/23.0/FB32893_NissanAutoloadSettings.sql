-- FB 32893 - Nissan autoload

-- delete any data for Nissan
DELETE 
	FROM merchandising.VehicleAutoLoadSettings
	WHERE siteID = 12
go

-- delete any build requests
DELETE 
	FROM builder.buildRequestLog
	WHERE siteID = 12
go


DELETE 
	FROM settings.credentialedSites
	WHERE siteID = 12
go

-- insert site for Honda/Acura eConfig
SET IDENTITY_INSERT settings.credentialedSites ON
go

INSERT INTO settings.credentialedSites
		(
			siteId, 
			siteName, 
			DealerCodeRequired, 
			canRequestData, 
			batchProcessOnly, 
			maxRequestsPerCall, 
			maxCallsPerBatch, 
			buildRequestTypeID
		)
	VALUES
		(
			12, 
			'Nissan North America Net', 
			0, 
			1, 
			1, 
			20, 
			1, 
			2
		)
go
	
SET IDENTITY_INSERT settings.credentialedSites OFF
go

-- set VehicleAutoLoadSettings site for honda, acura
INSERT INTO merchandising.VehicleAutoLoadSettings
			(ManufacturerID, IsAutoLoadEnabled, siteId, priority)
	VALUES	(11, 1, 12, 1)
GO


