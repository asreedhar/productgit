-- FB 32147 - vw option filters 

-- deletes for options filters!!!!
DELETE FROM settings.OptionFilter WHERE ManufactureId = 24
DELETE FROM settings.OptionFilterIncludePhrase WHERE ManufactureId = 24
DELETE FROM settings.OptionFilterExcludePhrase WHERE ManufactureId = 24
DELETE FROM dbo.ManufacturerVehicleOptionCodeWhiteList WHERE VehicleManufacturerId = 24
 
-- insert for settings.OptionFilter
----------------------------------------------------------------
INSERT INTO settings.OptionFilter VALUES(24, 199.00)

-- insert for settings.OptionFilterIncludePhrase
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO settings.OptionFilterIncludePhrase VALUES(24, 'ALARM KIT')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(24, 'Alloy wheels')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(24, 'BlueTooth')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(24, 'ENGINE BLOCK HEATER')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(24, 'Exhaust tip')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(24, 'Ipod')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(24, 'kit')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(24, 'Leather')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(24, 'Package')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(24, 'PKG')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(24, 'REMOTE START')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(24, 'Spoiler')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(24, 'TIRE PRESSURE')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(24, 'TRAILER HITCH')

-- insert for settings.OptionFilterExcludePhrase
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO settings.OptionFilterExcludePhrase VALUES(24, 'CARPETED MATS')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(24, 'delete')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(24, 'DESIRED')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(24, 'discount')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(24, 'FIRST AID')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(24, 'Gas guzzler')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(24, 'LEATHERETTE')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(24, 'Mat kit')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(24, 'PROTECTION +')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(24, 'PROTECTION KIT')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(24, 'Rubber mat')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(24, 'SPECIAL EDITION PKG')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(24, 'WHEEL LOCKS')

 
-- insert for dbo.ManufacturerVehicleOptionCodeWhiteList
-------------------------------------------------------------------------------------------------------------

