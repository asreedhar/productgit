-- FB 27557 - honda API

-- drop default constraint on DealerCredentialsRequired
IF EXISTS (SELECT * 
				FROM sysobjects
				WHERE NAME ='DF_credentialedSites_DealerCredentialsRequired'
				AND xtype = 'd')
	begin
		ALTER TABLE settings.credentialedSites
			DROP CONSTRAINT DF_credentialedSites_DealerCredentialsRequired
	end
go

-- drop column DealerCredentialsRequired
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'DealerCredentialsRequired'
				AND object_id = object_id('settings.credentialedSites'))
	begin
		ALTER TABLE settings.credentialedSites
			DROP COLUMN DealerCredentialsRequired
	end
go

-- drop constraint on BuildRequestTypeId
IF EXISTS (SELECT * 
				FROM sysobjects
				WHERE NAME ='FK_credentialedSites_buildRequestType'
				AND xtype = 'F')
	begin
		ALTER TABLE settings.credentialedSites
			DROP CONSTRAINT FK_credentialedSites_buildRequestType
	end
go

-- drop column BuildRequestTypeId
if EXISTS(SELECT *
				FROM sys.columns
				WHERE name like 'BuildRequestTypeId'
				AND object_id = object_id('settings.credentialedSites'))
	begin
		ALTER TABLE settings.credentialedSites
			DROP COLUMN BuildRequestTypeId
	end
go

-- add column for DealerCredentialsRequired
ALTER TABLE settings.credentialedSites
	ADD DealerCredentialsRequired BIT NOT NULL
	CONSTRAINT DF_credentialedSites_DealerCredentialsRequired DEFAULT 1
GO

-- set DealerCredentialsRequired for inactive sites
UPDATE settings.credentialedSites
	SET DealerCredentialsRequired = 0
	WHERE siteId IN (6, 7)  -- Ford Connect, Toyota Tech Info
	
-- add column for BuildRequestType
ALTER TABLE settings.credentialedSites
	ADD BuildRequestTypeId tinyint NULL
GO

-- add contratin for BuildRequestType
ALTER TABLE settings.credentialedSites
	ADD CONSTRAINT FK_credentialedSites_buildRequestType
	FOREIGN KEY (BuildRequestTypeId)
	REFERENCES settings.buildRequestType(BuildRequestTypeId)
GO
	
-- set build request type
UPDATE settings.credentialedSites
	SET BuildRequestTypeId = 2 -- slurpee
	WHERE DealerCredentialsRequired = 1  -- Ford Connect, Toyota Tech Info
	


-- delete any data for Honda/Acura econfig
DELETE 
	FROM merchandising.VehicleAutoLoadSettings
	WHERE siteID = 13
go

-- delete any build requests
DELETE 
	FROM builder.buildRequestLog
	WHERE siteID = 13
go


DELETE 
	FROM settings.credentialedSites
	WHERE siteID = 13
go

-- insert site for Honda/Acura eConfig
SET IDENTITY_INSERT settings.credentialedSites ON
go

INSERT INTO settings.credentialedSites
	(siteId, siteName, DealerCodeRequired, canRequestData, dealerCredentialsRequired, buildRequestTypeID)
	VALUES
	(13, 'Honda eConfig', 0, 1, 0, 3)
go
	
SET IDENTITY_INSERT settings.credentialedSites OFF
go

-- set VehicleAutoLoadSettings site for honda, acura
INSERT INTO merchandising.VehicleAutoLoadSettings
			(ManufacturerID, IsAutoLoadEnabled, siteId, priority)
	VALUES	(1, 1, 13, 1)
GO

