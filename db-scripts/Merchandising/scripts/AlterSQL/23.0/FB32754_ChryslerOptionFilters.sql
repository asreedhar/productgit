-- FB 32754 - Chrysler option filters

-- deletes for options filters!!!!
DELETE FROM settings.OptionFilter WHERE ManufactureId = 7
DELETE FROM settings.OptionFilterIncludePhrase WHERE ManufactureId = 7
DELETE FROM settings.OptionFilterExcludePhrase WHERE ManufactureId = 7
DELETE FROM dbo.ManufacturerVehicleOptionCodeWhiteList WHERE VehicleManufacturerId = 7
 
-- insert for settings.OptionFilter
----------------------------------------------------------------
INSERT INTO settings.OptionFilter VALUES(7, 199.00)

 
-- insert for settings.OptionFilterIncludePhrase
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'ADJUSTABLE PEDALS')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'ALUMINUM WHEEL')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'AXLE RATIO')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'Engine')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'FOG LAMPS')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'fuel tank')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'GARAGE DOOR')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'Group')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'GVWR:')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'HEATED')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'Navigation')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'Package')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'PASSENGER SEATING')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'PKG')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'Radio')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'REMOTE')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'SECURITY')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'sliding window')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'SPEAKERS')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'SPOILER')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'THEFT')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'TOW MIRRORS')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'Transmission')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(7, 'UCONNECT')

 
-- insert for settings.OptionFilterExcludePhrase
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO settings.OptionFilterExcludePhrase VALUES(7, 'CREDIT')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(7, 'DELETE')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(7, 'discount')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(7, 'PKG SAVINGS')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(7, 'savings')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(7, 'SMOKER')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(7, 'SPARE TIRE')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(7, 'Tax')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(7, 'TIRES')

 

