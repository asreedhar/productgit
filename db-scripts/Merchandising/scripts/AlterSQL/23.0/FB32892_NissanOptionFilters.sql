-- deletes for options filters!!!!
DELETE FROM settings.OptionFilter WHERE ManufactureId = 11
DELETE FROM settings.OptionFilterIncludePhrase WHERE ManufactureId = 11
DELETE FROM settings.OptionFilterExcludePhrase WHERE ManufactureId = 11
DELETE FROM dbo.ManufacturerVehicleOptionCodeWhiteList WHERE VehicleManufacturerId = 11
 
-- insert for settings.OptionFilter
----------------------------------------------------------------
INSERT INTO settings.OptionFilter VALUES(11, 199.00)

 
-- insert for settings.OptionFilterIncludePhrase
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO settings.OptionFilterIncludePhrase VALUES(11, 'A/C VENTS')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(11, 'BUMPER')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(11, 'chrome')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(11, 'CONTROLLED')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(11, 'ENGINE:')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(11, 'KICK PLATES')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(11, 'MIRROR')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(11, 'MOLDING')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(11, 'NISMO')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(11, 'Package')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(11, 'PKG')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(11, 'RADIO:')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(11, 'ROOF')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(11, 'SATELLITE')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(11, 'SENSOR')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(11, 'SPOILER')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(11, 'TRACKING')
INSERT INTO settings.OptionFilterIncludePhrase VALUES(11, 'wheels:')

 
-- insert for settings.OptionFilterExcludePhrase
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO settings.OptionFilterExcludePhrase VALUES(11, 'cargo')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(11, 'delete')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(11, 'First Aid')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(11, 'floor mat')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(11, 'GUZZLER')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(11, 'NOT DESIRED')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(11, 'RUN-FLAT')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(11, 'SUMMER TIRE PACKAGE')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(11, 'Trunk mat')
INSERT INTO settings.OptionFilterExcludePhrase VALUES(11, 'ULTRA HIGH PERFORMANCE TIRE PKG')

 
-- insert for dbo.ManufacturerVehicleOptionCodeWhiteList
-------------------------------------------------------------------------------------------------------------

