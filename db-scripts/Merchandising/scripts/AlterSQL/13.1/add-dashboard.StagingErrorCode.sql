if not exists(select * from Merchandising.dashboard.StagingErrorCode where ErrorCode = 5)

	insert Merchandising.dashboard.StagingErrorCode
	(
		ErrorCode,
		Description
	)
	values
	(
		5,
		'Online records that are older than the file date snapshot'
	)