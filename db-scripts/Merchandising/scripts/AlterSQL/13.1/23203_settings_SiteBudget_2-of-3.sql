use Merchandising
go


; with StoreMaxUpdated as
(
	select
		BusinessUnitId,
		DestinationId,
		MonthApplicable = dateadd(month, datediff(month, 0, DateUpdated), 0),
		MaxDateUpdated = max(DateUpdated)
	from settings.SiteBudget
	group by
		BusinessUnitId,
		DestinationId,
		dateadd(month, datediff(month, 0, DateUpdated), 0)
)
, LastBudgetByMonth as
(
	select
		old.BusinessUnitId,
		old.DestinationId,
		old.Budget,
		old.DateUpdated,
		max.MonthApplicable
	from
		settings.SiteBudget old
		join StoreMaxUpdated max
			on old.BusinessUnitId = max.BusinessUnitId
			and old.DestinationId = max.DestinationId
			and dateadd(month, datediff(month, 0, old.DateUpdated), 0) = max.MonthApplicable
			and DateUpdated = max.MaxDateUpdated
)
insert settings.SiteBudget_New
(
	BusinessUnitId,
	DestinationId,
	MonthApplicable,
	Budget,
	DescriptionText,
	DateUpdated
)
select
	BusinessUnitId,
	DestinationId,
	MonthApplicable,
	Budget,
	null as DescriptionText,
	DateUpdated
from LastBudgetByMonth 

go



