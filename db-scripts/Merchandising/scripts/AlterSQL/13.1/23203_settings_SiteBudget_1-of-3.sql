USE [Merchandising]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [settings].[SiteBudget_New](
	[BusinessUnitId] [int] NOT NULL,
	[DestinationId] [int] NOT NULL,
	[MonthApplicable] [datetime] not null,
	[Budget] [int] NULL,
	[DescriptionText] varchar(32) null,
	[DateUpdated] [datetime] NULL,
 CONSTRAINT [PK_SiteBudget_New] PRIMARY KEY CLUSTERED 
(
	[BusinessUnitId] ASC,
	[DestinationId] ASC,
	[MonthApplicable] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [settings].[SiteBudget_New]  WITH CHECK ADD  CONSTRAINT [FK_SiteBudget_EdtDestinations_New] FOREIGN KEY([DestinationId])
REFERENCES [settings].[EdtDestinations] ([destinationID])
GO
ALTER TABLE [settings].[SiteBudget_New] CHECK CONSTRAINT [FK_SiteBudget_EdtDestinations_New]
GO

