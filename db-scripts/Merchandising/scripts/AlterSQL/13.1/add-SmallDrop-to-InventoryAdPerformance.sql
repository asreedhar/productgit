
if not exists(select * from Merchandising.INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'dashboard' and TABLE_NAME = 'InventoryAdPerformance' and COLUMN_NAME = 'SmallDrop')
	alter table Merchandising.dashboard.InventoryAdPerformance
	add SmallDrop bit not null constraint DF_SmallDrop default (0)