use Merchandising
go

drop table settings.SiteBudget
go

exec sp_rename 'settings.SiteBudget_New', 'SiteBudget'
go

exec sp_rename 'settings.SiteBudget.DateUpdated', 'UpdatedOn', 'COLUMN'
go

