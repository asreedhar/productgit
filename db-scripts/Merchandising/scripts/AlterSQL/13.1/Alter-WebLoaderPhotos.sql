ALTER TABLE Merchandising.merchandising.WebLoaderPhotos
ADD
	Batch UNIQUEIDENTIFIER CONSTRAINT DF_Batch DEFAULT '00000000-0000-0000-0000-000000000000' NOT NULL,
	Position INT CONSTRAINT DF_Position DEFAULT -1 NOT NULL,
	Taken DATETIME CONSTRAINT DF_Taken DEFAULT 0 NOT NULL
GO

ALTER TABLE Merchandising.merchandising.WebLoaderPhotos DROP CONSTRAINT PK_WebLoaderPhotos
GO

ALTER TABLE Merchandising.merchandising.WebLoaderPhotos ADD CONSTRAINT [PK_WebLoaderPhotos] PRIMARY KEY CLUSTERED 
(
	[InventoryID] ASC,
	[Handle] ASC,
	[Batch] ASC
)

GO