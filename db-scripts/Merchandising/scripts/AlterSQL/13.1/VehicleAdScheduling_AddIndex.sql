
-- BUGZID 23973: Scheduled email timeouts

USE [Merchandising]
GO
/****** Object:  Index [test]    Script Date: 11/13/2012 12:31:54 ******/
CREATE NONCLUSTERED INDEX [IX_ActualReleaseTime_AdStatus_AutoRegenerate] ON [postings].[VehicleAdScheduling] 
(
	[actualReleaseTime] ASC,
	[advertisementStatus] ASC,
	[autoRegenerate] ASC
)
