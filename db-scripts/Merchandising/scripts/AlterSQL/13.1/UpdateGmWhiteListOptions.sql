-- SF:68989, Autoload not adding Cooled Seats to 2013 models

INSERT INTO [Merchandising].[dbo].[ManufacturerVehicleOptionCodeWhiteList] ([VehicleManufacturerId],[VehicleMakeId],[OptionCode])
select '6','6','KB6' 
union all
select '6','7','KB6' 
union all
select '6','8','KB6' 
union all
select '6','15','KB6';