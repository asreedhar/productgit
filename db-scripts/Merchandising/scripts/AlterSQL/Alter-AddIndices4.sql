
------------cert config------------------------------------

ALTER TABLE settings.DealerCertificationConfiguration
ADD CONSTRAINT FK_DCC_CertificationTypeId
FOREIGN KEY (certificationTypeId)
REFERENCES builder.manufacturerCertifications (certificationTypeId)
------------publications
CREATE CLUSTERED  
INDEX NDX_Publication
ON postings.Publications (businessUnitId, inventoryId)


ALTER TABLE postings.vehicleAdScheduling
DROP CONSTRAINT PK_VehicleAdScheduling

CREATE CLUSTERED  
INDEX NDX_VehicleAdScheduling
ON postings.VehicleAdScheduling (businessUnitId, inventoryId)

ALTER TABLE settings.Merchandising
ADD CONSTRAINT PK_Merchandising
PRIMARY KEY CLUSTERED (businessUnitId)

ALTER TABLE settings.AlertingPreferences
ADD CONSTRAINT PK_AlertingPreferences
PRIMARY KEY NONCLUSTERED (alertingId)


CREATE CLUSTERED 
INDEX NDX_AlertingPreferences
ON settings.AlertingPreferences
(businessUnitId, memberId)




