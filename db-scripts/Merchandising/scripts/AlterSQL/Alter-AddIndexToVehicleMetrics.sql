
ALTER TABLE postings.vehicleMetrics
DROP CONSTRAINT PK_vehicleMetrics

ALTER TABLE postings.vehicleMetrics
ADD CONSTRAINT PK_VehicleMetrics
PRIMARY KEY NONCLUSTERED (metricID)

CREATE CLUSTERED 
INDEX IX_VehicleMetrics__startDateBusinessUnitIdInventoryId
ON postings.vehicleMetrics (startDate, businessUnitId, inventoryId)
 