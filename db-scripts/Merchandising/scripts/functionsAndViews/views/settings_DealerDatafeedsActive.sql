if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[DealerDatafeedsActive]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [settings].[DealerDatafeedsActive]
GO

/*****************************************************************************************************************
View:		settings.DealerDatafeedsActive

Purpose:	Provide a list of active dealers for the DatafeedCodes that will use Optimal Format

Date		Author		Description
12/9/2010	SVU		Initial Creation
******************************************************************************************************************/

CREATE VIEW [settings].[DealerDatafeedsActive]
AS
SELECT IATPE.DatafeedCode, IAD.BusinessUnitID /*, IAD.InternetAdvertiserID, IAD.InternetAdvertisersDealershipCode*/
FROM IMT.dbo.InternetAdvertiser_ThirdPartyEntity IATPE
JOIN IMT.dbo.InternetAdvertiserDealership IAD ON IATPE.InternetAdvertiserID = IAD.InternetAdvertiserID
WHERE IATPE.DatafeedCode IN ('eBizCustomHendrick', 'AutoUplink')
AND IAD.IsLive=1
AND IAD.Active=1
AND IAD.status=1
UNION ALL
SELECT	DC.DestinationName DataFeedCode, DC.BusinessUnitID
FROM	IMT.Extract.DealerConfiguration DC
WHERE	DC.DestinationName='AULtec'
AND	DC.Active=1