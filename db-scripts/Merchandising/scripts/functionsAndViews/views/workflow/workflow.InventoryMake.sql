if exists ( select   *
            from     dbo.sysobjects
            where    id = object_id(N'workflow.InventoryMake')
                     and objectproperty(id, N'IsView') = 1 ) 
   drop view workflow.InventoryMake
GO

create view workflow.InventoryMake
as
		select inv.InventoryID, coalesce(div.DivisionName, mmg.Make) as Make
		from fldw.dbo.InventoryActive inv
		inner join FLDW.dbo.Vehicle v
			on inv.vehicleid = v.vehicleid
			and inv.businessUnitId = v.businessUnitId
		left join Merchandising.builder.OptionsConfiguration oc
			on oc.BusinessUnitID = inv.BusinessUnitID
			and oc.InventoryId = inv.InventoryId 
		left join IMT.dbo.MakeModelGrouping mmg 
			on mmg.MakeModelGroupingId = v.MakeModelGroupingId
		left join VehicleCatalog.chrome.Styles sty 
			on sty.StyleID = oc.Chromestyleid
			and sty.CountryCode = 1
		left join VehicleCatalog.chrome.Models mdl 
			on mdl.ModelID = sty.ModelID
			and mdl.CountryCode = 1
		left join VehicleCatalog.chrome.Divisions div 
			on div.DivisionID = mdl.DivisionID
			and div.CountryCode = 1
GO


GRANT SELECT ON [workflow].[InventoryMake] TO [firstlook]
GO