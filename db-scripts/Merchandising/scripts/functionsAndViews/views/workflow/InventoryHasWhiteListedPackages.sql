use Merchandising

if object_id('workflow.InventoryHasWhiteListedPackages') is not null
    drop view workflow.InventoryHasWhiteListedPackages
go

create view workflow.InventoryHasWhiteListedPackages
as

with 
WhiteListedPackages as
(
    select ChromeStyleID, ChromeOptionCode
    from Merchandising.chrome.PackageOptionCodeWhitelist wl
),

VehicleOptionsAndPackages as
(
    select BusinessUnitID, InventoryID, OptionCode as ChromeOptionCode
    from Merchandising.builder.VehicleDetailedOptions vo
)

select cs.BusinessUnitID, cs.InventoryID,
    cs.ChromeStyleID, cs.StockNumber, cs.VIN,

    cast(case when (exists (select 1 
                      from WhiteListedPackages
                      where ChromeStyleID = cs.ChromeStyleID) or
                      not exists (select 1 from VehicleOptionsAndPackages where BusinessUnitID = cs.BusinessUnitID and InventoryID = cs.InventoryID)
                      )
         then 1 else 0 end as bit) as MakeHasWhitelistedPackages,
         
    cast(case when exists (select 1
                      from WhiteListedPackages wl
                        inner join VehicleOptionsAndPackages pkg
                          on wl.ChromeOptionCode = pkg.ChromeOptionCode
                      where wl.ChromeStyleID = cs.ChromeStyleID
                        and pkg.BusinessUnitID = cs.BusinessUnitID
                        and pkg.InventoryID = cs.InventoryID)
         then 1 else 0 end as bit) as VehicleHasWhitelistedPackages

from Merchandising.chrome.InventoryChromeStyle cs
