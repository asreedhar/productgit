if object_id('workflow.Inventory') is not null
	drop view workflow.Inventory
go

create view workflow.Inventory
as

with 
ActiveInventory as
(
	select
		inv.BusinessUnitId,
		inv.InventoryID,
		inv.StockNumber,
		
		inv.InventoryReceivedDate, 
		inv.UnitCost, 
		inv.AcquisitionPrice, 
		inv.Certified,
		inv.MileageReceived, 
		inv.TradeOrPurchase, 
		inv.ListPrice,
		inv.InventoryType,
		inv.InventoryStatusCD,
		inv.VehicleLocation,
		inv.LotPrice,
		inv.MSRP,
		inv.PlanReminderDate,
		
		v.Vin, 
		v.VehicleYear, 
		v.VehicleTrim,
		v.VehicleCatalogId,
		v.makeModelGroupingId,
		v.BaseColor
	from
		FLDW.dbo.InventoryActive inv
		inner join FLDW.dbo.Vehicle v
			on inv.vehicleid = v.vehicleid
			and inv.businessUnitId = v.businessUnitId
	where
		inventoryActive = 1
),

VehicleStatusRaw as
(
	select 
		BusinessUnitId, 
		InventoryId, 
		StatusTypeId, 
		StatusLevel
	from Merchandising.builder.VehicleStatus
),

VehicleStatus as
(
	select 
		pvt.BusinessUnitId, 
		pvt.InventoryId,
		pvt.[1] as ExteriorStatus,
		pvt.[2] as InteriorStatus,
		pvt.[3] as PhotoStatus,
		pvt.[4] as PricingStatus,
		pvt.[5] as PostingStatus,
		pvt.[6] as AdReviewStatus
	from 
		VehicleStatusRaw
		pivot
		(
			max(statusLevel)
			for StatusTypeId
			in ([1],[2],[3],[4],[5],[6])
		) pvt
),
Approval as
(
	select
		p.businessUnitID,
		p.inventoryId,
		datePosted = max(p.datePosted)
	from
		FLDW.dbo.InventoryActive ia
		join Merchandising.postings.Publications p
			on ia.BusinessUnitID = p.businessUnitID
			and ia.InventoryID = p.inventoryId
	where p.dateRemoved is null
	group by
		p.businessUnitID,
		p.inventoryId
),
OptionsConfigurationAndVehicleStatus as
(
	select 
		oc.BusinessUnitID,
		oc.InventoryId,
		oc.IsLegacy,
		oc.ChromeStyleID,
		oc.LotLocationId,
		oc.AfterMarketTrim,
		oc.SpecialId,
		oc.DoNotPostFlag,
		oc.OnlineFlag,
		oc.LotDataImportLock,
		substring(coalesce(de.MerchandisingDescription, ''), 1, 110) as DescriptionSample,
		de.LastUpdateListPrice,
		app.DatePosted,
		st.ExteriorStatus, 
		st.InteriorStatus, 
		st.PhotoStatus, 
		st.AdReviewStatus,
		st.PricingStatus,
		st.PostingStatus,
		case
			when oc.DoNotPostFlag = 1 then 8 -- DoNotPost
			when st.PostingStatus = 2 then 32 -- Pending Approval
			when oc.OnlineFlag = 1 or st.PostingStatus >= 3 then 128 -- Online OK
			else 16 -- Web Loading Incomplete
		end as StatusBucket,
		oc.ExtColor1,
		oc.ExteriorColorCode,
		oc.extColor2,
		oc.ExteriorColorCode2,
		oc.intColor,
		oc.InteriorColorCode,
		oc.NoPackages
	from
		Merchandising.builder.OptionsConfiguration oc
		left join VehicleStatus st
			on st.BusinessUnitId = oc.BusinessUnitId
			and st.InventoryId = oc.InventoryId
		left join Merchandising.builder.Descriptions de
			on de.BusinessUnitId = oc.BusinessUnitId
			and de.InventoryId = oc.InventoryId
		left join Approval app
			on app.BusinessUnitID = oc.BusinessUnitID
			and app.InventoryID = oc.InventoryID
),

Settings as
(
	select BU.BusinessUnitID,
		coalesce(M1.LowActivityThreshold, M2.LowActivityThreshold, 0.01) as LowActivityThreshold,
		coalesce(M1.MinSearchCountThreshold, M2.MinSearchCountThreshold, 100) as MinSearchCountThreshold,
		coalesce(M1.DesiredDescriptionLength, M2.DesiredDescriptionLength) as DesiredDescriptionLength,
		coalesce(M1.DesiredPhotoCount, 0) as DesiredPhotoCount,
		coalesce(AP1.maxDaysToApprovedAfterPending, AP2.maxDaysToApprovedAfterPending) as maxDaysToApprovedAfterPending,
		coalesce(AP1.daysToPricingComplete, AP2.daysToPricingComplete) as daysToPricingComplete,
		coalesce(AP1.daysToEquipmentComplete, AP2.daysToEquipmentComplete) as daysToEquipmentComplete,
		coalesce(AP1.daysToPhotosComplete, AP2.daysToPhotosComplete) as daysToPhotosComplete,
		coalesce(AP1.daysToApproval, AP2.daysToApproval) as daysToApproval,
		M1.PriceNewCars
	from
		IMT.dbo.BusinessUnit BU
		left join Merchandising.settings.Merchandising M1 
			on BU.BusinessUnitID = M1.BusinessUnitID
		left join Merchandising.settings.Merchandising M2 
			on M2.BusinessUnitID = 100150
			and M1.BusinessUnitID is null
		left join Merchandising.settings.alertingPreferences AP1 
			on BU.BusinessUnitID = AP1.BusinessUnitID
		left join Merchandising.settings.alertingPreferences AP2 
			on AP2.BusinessUnitID = 100150
			and AP1.BusinessUnitID is null
)
,
AutoWindowSticker AS
(
SELECT DISTINCT InventoryID, AutoPrintTemplate
	FROM imt.WindowSticker.PrintLog
	WHERE AutoPrintTemplate = 1)


SELECT 
	inv.BusinessUnitID,
	inv.InventoryID,
	
	inv.Vin, 
	inv.StockNumber,
	
	oc.StatusBucket,
	inv.InventoryReceivedDate, 
	inv.VehicleYear, 
	coalesce(inv.UnitCost, 0.0) as UnitCost, 
	coalesce(inv.AcquisitionPrice, 0.0) as AcquisitionPrice, 
	inv.Certified,
	coalesce(vci.CertificationNumber, '') as CertifiedID,
	coalesce(div.DivisionName, mmg.Make) as Make, 
	coalesce(BSO.CFModelName, sty.CFModelName,mmg.model) as Model,		-- Model override
	coalesce(BSO.Trim, sty.Trim, inv.VehicleTrim, '') as trim,		-- Trim override
        oc.AfterMarketTrim,
	coalesce(mkt.marketClass, '') as marketClass,
	coalesce(mkt.marketClassId, 0) as marketClassId,
	cast(coalesce(mkt.segmentId, 1) as tinyint) as segmentId,
	coalesce(oc.lotLocationId, 0) as lotLocationId,
	inv.MileageReceived, 
	inv.TradeOrPurchase, 
	coalesce(ListPrice, 0.0) as ListPrice, 
	coalesce(oc.ExtColor1, inv.BaseColor) as BaseColor,
	oc.ExtColor1,
	oc.ExteriorColorCode,
	oc.ExtColor2,
	oc.ExteriorColorCode2,
	oc.IntColor,
	oc.InteriorColorCode,
	inv.VehicleLocation,
	inv.InventoryStatusCD,
	oc.DescriptionSample,
	oc.LastUpdateListPrice,
	oc.DatePosted,
	coalesce(oc.ChromeStyleId, -1) as ChromeStyleId,
	inv.VehicleCatalogId,
	coalesce(oc.exteriorStatus, case when coalesce(div.DivisionName, mmg.Make) = 'Honda' then 2 else 0 end) as exteriorStatus,
	coalesce(oc.interiorStatus, case when coalesce(div.DivisionName, mmg.Make) = 'Honda' then 2 else 0 end) as interiorStatus,
	isnull(oc.photoStatus, 0) as photoStatus, 
	isnull(oc.adReviewStatus, 0) as adReviewStatus,
	isnull(oc.pricingStatus, 0) as pricingStatus, 
	isnull(oc.postingStatus, 0) as postingStatus,
	isnull(oc.onlineFlag, 0) as onlineFlag,
	inv.InventoryType,
	S.DesiredPhotoCount,
	inv.LotPrice,
	cast(inv.MSRP as decimal) as MSRP,
	P.SpecialPrice,
	cast(case when inv.PlanReminderDate is null or inv.PlanReminderDate <= getdate() then 1 else 0 end as bit) as DueForRepricing,
	
       cast
       (
              0 as bit
       ) as lowActivityFlag,
		
	cast
	(
		case
			when exists
			(
				select 1
				from FLDW.dbo.InventoryBookout_F IBO
				    left join Merchandising.settings.MerchandisingDescriptionPreferences MDP
				        on IBO.BusinessUnitID = MDP.businessUnitId
				where IBO.BusinessUnitID = inv.BusinessUnitID
				    and IBO.InventoryID = inv.InventoryID
				    and IBO.ThirdPartyCategoryID = MDP.preferredBookoutCategoryId
				    and IBO.BookoutValueTypeID = 2 -- Final Value
				    and IBO.IsAccurate = 1
				    and IBO.IsValid = 1
				-- check KBB Consumer Guide too - see Market.Pricing.HasUsedKbbConsumerTool for reference
				union all select 1
				from imt.dbo.VehicleBookoutState vbs
				where vbs.BusinessUnitID = inv.BusinessUnitID
				and vbs.vin = inv.vin
				
			) then 1
			else 0
		end as bit
	) as HasCurrentBookValue,
	
	cast(1 as bit) as HasKeyInformation, -- TODO: Remove this field from the result set (See Case 20442)
	
	cast
    (
        case 
            when exists 
            (
                SELECT  *
				FROM IMT.Carfax.Vehicle_Dealer D
				JOIN IMT.Carfax.Vehicle V ON V.VehicleID = D.VehicleID
				JOIN IMT.Carfax.Request R ON R.RequestID = D.RequestID
				JOIN IMT.Carfax.Report E ON E.RequestID = R.RequestID
				WHERE 
					V.VIN = inv.VIN
					AND D.DealerID = inv.BusinessUnitID
					AND E.ExpirationDate > GETDATE()
			) then 1 
			else 0 
		end as bit
	) as HasCarfax,
	
	cast
	(
	    case
	        when exists
	        (
	            select *
	            from 
	                IMT.Carfax.Account A
	                inner join IMT.Carfax.Request R on A.AccountID = R.AccountID
	                inner join IMT.Carfax.Vehicle V on R.VehicleID = V.VehicleID and R.RequestID = V.RequestID
	                inner join IMT.Carfax.Report E on E.RequestID = R.RequestID
	            where
	                V.VIN = inv.VIN
	                and A.AccountTypeID = 2
	                and E.ExpirationDate > getdate()
	        ) then 1
	        else 0
	    end as bit
	) as HasCarfaxAsAdmin,

	als.statusTypeId as autoloadStatusTypeId,
	als.endTime as autoloadEndTime,
	coalesce(oc.NoPackages, cast(0 as bit)) as NoPackages
	,crt.CertifiedProgramId
	,AutoWindowStickerPrinted = IsNull(AutoWindowSticker.AutoPrintTemplate, 0)
	

from
	ActiveInventory inv
	left join IMT.dbo.Inventory_CertificationNumber crt on inv.InventoryId = crt.InventoryID
	left join IMT.dbo.MakeModelGrouping mmg 
		on mmg.MakeModelGroupingId = inv.MakeModelGroupingId
	left join Settings S 
		on inv.BusinessUnitID = S.BusinessUnitID
	left join OptionsConfigurationAndVehicleStatus oc 
		on oc.BusinessUnitID = inv.BusinessUnitID
		and oc.InventoryId = inv.InventoryId 
	left join VehicleCatalog.chrome.Styles sty 
		on sty.StyleID = oc.Chromestyleid
		and sty.CountryCode = 1		  
	left join VehicleCatalog.chrome.Models mdl 
		on mdl.ModelID = sty.ModelID
		and mdl.CountryCode = 1
	left join VehicleCatalog.FirstLook.MarketClass mkt 
		on mkt.MarketClassID = sty.MktClassID
	left join VehicleCatalog.chrome.Divisions div 
		on div.DivisionID = mdl.DivisionID
		and div.CountryCode = 1
	left join IMT.dbo.Inventory_CertificationNumber vci 
		on vci.InventoryID = inv.InventoryId -- TODO: This should be clustered by BU, Inv
	left join Merchandising.builder.NewCarPricing P -- TODO: This should be clustered by BU, Inv
		on P.InventoryID = inv.InventoryId 
		and S.PriceNewCars = 1 
	left join Merchandising.builder.AutoLoadStatus als
		on inv.BusinessUnitID = als.businessUnitID
		and inv.InventoryID = als.inventoryId
		
	LEFT JOIN workflow.BusinessUnitChromeStyleOverride BSO ON inv.BusinessUnitID = BSO.BusinessUnitID AND oc.ChromeStyleId = BSO.ChromeStyleID	
	LEFT OUTER JOIN AutoWindowSticker ON inv.inventoryId = AutoWindowSticker.inventoryId
	
		
GO