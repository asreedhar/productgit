if object_id('dashboard.RawMetrics') is not null
	drop view dashboard.RawMetrics
go

create view dashboard.RawMetrics
as

select
	DatasourceInterfaceId = 13
	, destinationID = 2 -- AutoTrader
	, metrics.Load_ID

	, metrics.businessUnitID
	, metrics.VIN
	, metrics.StockNumber
	
	, metrics.Month
	, LogicalDate
	, DbTimestamp
	
	, SearchPageViews = metrics.AppearedInSearch
	, DetailPageViews = metrics.DetailsPageViews
	, EmailLeads = metrics.EmailsSent
	, MapsViewed = metrics.MapViews
	, AdPrints = metrics.DetailsPagePrinted
	
	, inventoryId
	, RequestId
	
	, DateCollected = isnull(FetchDateInserted, DateLoaded)
from Merchandising.merchandising.AutoTraderVehSummary metrics

			
union all			

			
select
	DatasourceInterfaceId = 2004
	, destinationID = 1  -- Cars.com
	 , metrics.Load_ID

	 
	, metrics.businessUnitID
	, metrics.VIN
	, metrics.StockNumber
	
	, metrics.Month
	, LogicalDate
	, DbTimestamp
		
	, SearchPageViews = metrics.InSearchResult
	, DetailPageViews = metrics.DetailViewed
	, EmailLeads = metrics.EmailSent
	, MapsViewed = metrics.MapViewed
	, AdPrints = metrics.AdsPrinted

	, inventoryId
	, RequestId
	, DateCollected = isnull(FetchDateInserted, DateLoaded)
from Merchandising.merchandising.CarsDotComVehSummary metrics


union all


select
	DatasourceInterfaceId = 2006
	, destinationID = 2  -- AutoTrader
	 , metrics.LoadId
	 
	, businessUnitID
	, metrics.VIN
	, metrics.StockNumber
	
	, Month 
	, LogicalDate = metrics.FeedDate
	, DbTimestamp
	
	, SearchPageViews = metrics.SearchPageViews
	, DetailPageViews = metrics.DetailPageViews
	, EmailLeads = metrics.EmailLeads
	, MapsViewed = metrics.MapsViewed
	, AdPrints = metrics.AdPrints
	
	, inventoryId
	, RequestId = convert(int, null)
	, DateCollected = FeedDate
from Merchandising.merchandising.AutoTraderHendrickVehicleSummary metrics

union all

select
	DatasourceInterfaceId
	, destinationID
	 , metrics.LoadId
	 
	, businessUnitID
	, VIN = convert(varchar(17), null)
	, StockNumber = convert(varchar(15), null)
	
	, Month 
	, LogicalDate
	, DbTimestamp = DbIdentity


	, SearchPageViews = metrics.SearchPageViews
	, DetailPageViews = metrics.DetailPageViews
	, EmailLeads = metrics.EmailLeads
	, MapsViewed = metrics.MapsViewed
	, AdPrints = metrics.AdPrints
	
	, inventoryId
	, RequestId = convert(int, null)
	, DateCollected
from Merchandising.dashboard.VehicleActivity metrics

go

