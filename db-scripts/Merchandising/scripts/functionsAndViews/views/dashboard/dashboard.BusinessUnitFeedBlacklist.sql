if object_id('dashboard.BusinessUnitFeedBlacklist') is not null
	drop view dashboard.BusinessUnitFeedBlacklist
go

create view dashboard.BusinessUnitFeedBlacklist
as

-- exclude Hendrick from getting data scraped from Fetch for AutoTrader
select
	DatasourceInterfaceId = convert(int, 13),
	BusinessUnitId
from imt.dbo.BusinessUnitRelationship
where ParentID = 100068 
      

go

