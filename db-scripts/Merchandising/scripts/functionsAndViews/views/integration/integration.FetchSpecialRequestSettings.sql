if object_id('integration.FetchSpecialRequestSettings') is not null
	drop view integration.FetchSpecialRequestSettings
go

create view integration.FetchSpecialRequestSettings
as

select 
	IsCatchUpDay = 
		case
			when datepart(day, getdate()) in (1,2,3,5,10,15) 
				then cast(1 as bit) 
				else cast(0 as bit)
		end
-- overrides used for testing purposes
--	,IsGapDay = cast(1 as bit) -- always on
--	,IsGapDay = cast(0 as bit) -- always off
	,IsGapDay = 
		case 
			when datepart(day, getdate()) in (8,16,24) 
				then cast(1 as bit) 
				else cast(0 as bit) 
			end
	,MaxDataHost = 
		case 
			when @@SERVERNAME = 'PRODDB02SQL' then 'services.maxanalytics.biz'
			when @@SERVERNAME = 'BETADB01SQL' then 'beta.test.maxanalytics.biz'
			else 'alpha.test.maxanalytics.biz'
		end
	,MaxDataAuthUser = 'max'
	,MaxDataAuthPassword = 'M@xW3llHizous'
go

