if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleConditionReport]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [builder].[VehicleConditionReport]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [builder].[VehicleConditionReport]
AS

select 
	businessUnitId
	,inventoryId
	,[1] as exteriorCondition
	,[2] as paintCondition
,[13] as trimCondition
,[14] as glassCondition
,[7]  as interiorCondtiion
,[8]  as carpetCondition
,[9]  as seatsCondition
,[10]  as headlinerCondition
,[11]  as dashboardCondition

from (
select businessUnitId, inventoryId, vehicleConditionTypeId, vcl.description
from builder.vehicleconditions vc
join builder.vehicleConditionLevels vcl
ON vc.conditionLevel = vcl.levelId
) as a
PIVOT
(
	max([description]) 	
	FOR
	vehicleConditionTypeID IN ([1],[2],[13],[14],[7],[8],[9],[10],[11])
)
AS P

GO