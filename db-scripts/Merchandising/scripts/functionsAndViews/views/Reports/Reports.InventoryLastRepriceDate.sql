if  OBJECT_ID(N'[Reports].[InventoryLastRepriceDate]') is not null
drop view Reports.InventoryLastRepriceDate
go

set ansi_nulls on
go

set quoted_identifier on
go

create view Reports.InventoryLastRepriceDate
as
	select 
		a.BusinessUnitId
		,a.InventoryId 
		,LastRepriceDate = coalesce(max(b.repricedate), max(a.inventoryreceiveddate))
	from imt.dbo.inventory a
	left join Reports.InventoryPriceHistory b 
		on a.inventoryid = b.inventoryid
	group by a.BusinessUnitId, a.inventoryid

go


