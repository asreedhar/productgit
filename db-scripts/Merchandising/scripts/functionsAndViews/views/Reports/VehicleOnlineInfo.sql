if object_id('Reports.VehiclesOnlineInfo') is not null 
	drop view Reports.VehiclesOnlineInfo
go

create view Reports.VehiclesOnlineInfo
as

select
	inv.BusinessUnitId,
	inv.InventoryId,
	inv.StockNumber,
	inv.Vin,
	inv.InventoryReceivedDate,
	inv.VehicleYear,
	inv.Make,
	inv.Model,
	inv.Trim,
	inv.InventoryType,
	inv.Certified,
	ia.AgeInDays,
	coalesce(inv.SpecialPrice, inv.ListPrice) as InternetPrice,
	Approved =
		convert
		(
			bit,
			case
				when exists
				(
					select *
					from MerchandisingInterface.Interface.AdReleases with (nolock)
					where
						businessUnitID = inv.BusinessUnitID
						and inventoryId = inv.InventoryID
				)
				then 1
				else 0
			end
		)
from
	Merchandising.workflow.Inventory inv with (nolock)
	inner join FLDW.dbo.InventoryActive ia with (nolock)
		on 	ia.BusinessUnitId = inv.BusinessUnitId
		and ia.InventoryId = inv.InventoryId
where (inv.statusBucket is null or inv.StatusBucket <> 8) -- Offline / Do not post

go