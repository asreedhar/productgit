if object_id('Reports.MaxAdUsage') is not null
	drop view Reports.MaxAdUsage
go

create view Reports.MaxAdUsage
as

with
ExcludeStores as
(
			  select BusinessUnitID = 101181, BusinessUnitCode = 'BEAVERTO01', BusinessUnit = 'Beaverton Toyota'
	union all select BusinessUnitID = 102186, BusinessUnitCode = 'BROOKDAL01', BusinessUnit = 'Brookdale Buick Pontiac GMC'
	union all select BusinessUnitID = 100391, BusinessUnitCode = 'CAPITOLC01', BusinessUnit = 'Capitol Chevrolet'
	union all select BusinessUnitID = 104311, BusinessUnitCode = 'COCHRANB01', BusinessUnit = 'Cochran Buick GMC Certified'
	union all select BusinessUnitID = 101768, BusinessUnitCode = 'COCHRANG01', BusinessUnit = 'Cochran GM MC Motor Trend Certified'
	union all select BusinessUnitID = 102872, BusinessUnitCode = 'COURTESY11', BusinessUnit = 'Courtesy Ford'
	union all select BusinessUnitID = 105013, BusinessUnitCode = 'CSRANISS01', BusinessUnit = 'NA'
	union all select BusinessUnitID = 105114, BusinessUnitCode = 'FRANKC.V01', BusinessUnit = 'NA'
	union all select BusinessUnitID = 105268, BusinessUnitCode = 'HUFFINES07', BusinessUnit = 'Huffines Hyundai of Plano'
	union all select BusinessUnitID = 105004, BusinessUnitCode = 'NAPAFLM001', BusinessUnit = 'Napa FLM'
	union all select BusinessUnitID = 104022, BusinessUnitCode = 'NORTHWES03', BusinessUnit = 'Northwest Jeep'
	union all select BusinessUnitID = 105002, BusinessUnitCode = 'NUCARCON02', BusinessUnit = 'Nucar Connection'
	union all select BusinessUnitID = 105014, BusinessUnitCode = 'NUCARMOT01', BusinessUnit = 'Nucar Motors, Inc'
	union all select BusinessUnitID = 102559, BusinessUnitCode = 'WILDELEX01', BusinessUnit = 'Wilde Lexus of Sarasota'
	union all select BusinessUnitID = 102785, BusinessUnitCode = 'SATURNOF45', BusinessUnit = 'Smart Center Trevose'
	union all select BusinessUnitID = 100837, BusinessUnitCode = 'MCGRATHI01', BusinessUnit = 'McGrath Imports'
	union all select BusinessUnitID = 104041, BusinessUnitCode = 'GROGANST02', BusinessUnit = 'Grogan''s Towne CDJ'
	union all select BusinessUnitID = 100834, BusinessUnitCode = 'MCGRATHH01', BusinessUnit = 'McGrath Honda of Elgin'
	union all select BusinessUnitID = 101621, BusinessUnitCode = 'WINDYCIT04', BusinessUnit = 'Windy City Pontiac Buick GMC'
	union all select BusinessUnitID = 101590, BusinessUnitCode = 'WINDYCIT01', BusinessUnit = 'Windy City Chevrolet'
	union all select BusinessUnitID = 101620, BusinessUnitCode = 'WINDYCIT03', BusinessUnit = 'Windy City Toyota'
	union all select BusinessUnitID = 101619, BusinessUnitCode = 'WINDYCIT02', BusinessUnit = 'Windy City Honda'
	union all select BusinessUnitID = 103910, BusinessUnitCode = 'GARDENCI02', BusinessUnit = 'Garden City JCD, Inc.'
	union all select BusinessUnitID = 105239, BusinessUnitCode = 'WINDYCIT05', BusinessUnit = 'Windy City BMW'
	union all select BusinessUnitID = 104938, BusinessUnitCode = 'CHUCKHUT02', BusinessUnit = 'Chuck Hutton Chevrolet Co.'
	union all select BusinessUnitID = 105112, BusinessUnitCode = 'CLEOBAYS01', BusinessUnit = 'Cleo Bay Suzuki'
	union all select BusinessUnitID = 105131, BusinessUnitCode = 'CLEOBAYS02', BusinessUnit = 'Cleo Bay Honda'
	union all select BusinessUnitID = 105144, BusinessUnitCode = 'CLEOBAYS03', BusinessUnit = 'Cleo Bay Subaru'
	union all select BusinessUnitID = 105010, BusinessUnitCode = 'LEWESAUT02', BusinessUnit = 'Lewes Auto Mall'
	union all select BusinessUnitID = 105298, BusinessUnitCode = 'STORETWO02', BusinessUnit = 'Store Two Branch Two'
)
, FilteredStores as
(
	select
		bu.BusinessUnitID,
		bu.BusinessUnit,
		bu.BusinessUnitCode
	from
		IMT.dbo.BusinessUnit bu
		join IMT.dbo.DealerUpgrade du					
			on bu.BusinessUnitID = du.BusinessUnitID				
			and du.DealerUpgradeCD = 24 -- Max Ad				
			and du.EffectiveDateActive = 1
	where not exists(select * from ExcludeStores where BusinessUnitID = bu.BusinessUnitID)
)
, FilteredInventory as
(
	select
		ia.BusinessUnitID,
		ia.InventoryID,
		ia.InventoryType
	from
		FilteredStores fs
		join FLDW.dbo.InventoryActive ia
			on fs.BusinessUnitID = ia.BusinessUnitID
			and ia.InventoryActive = 1
			and ia.ListPrice > 0
	where
		not exists
		(
			-- it's a car that has been moved offline
			select *	
			from Merchandising.builder.OptionsConfiguration
			where	
				businessUnitID = ia.BusinessUnitID
				and inventoryId = ia.InventoryID
				and doNotPostFlag = 1
		)
)
, InventoryCounts as
(
	select						
		fi.BusinessUnitID,
		UsedCars =
			sum(case when fi.InventoryType = 2 then 1 else 0 end)
		, UsedCarswAds = 
			sum
			(
				case
					when
						fi.InventoryType = 2 
						and vas2.inventoryId is not null
					then 1
					else 0
				end
			)		
		, NewCars = 
			sum(case when fi.InventoryType = 1 then 1 else 0 end)
		, NewCarswAds =
			sum
			(
				case
					when
						fi.InventoryType = 1 
						and vas2.inventoryId is not null
					then 1
					else 0
				end
			)		
	from
		FilteredInventory fi					
		left join
			(
				select distinct
					vas.businessUnitID,
					vas.inventoryId
				from Merchandising.postings.VehicleAdScheduling vas
				where exists(
						select *
						from FilteredInventory
						where
							BusinessUnitID = vas.businessUnitID
							and InventoryID = vas.inventoryId
					)
			) vas2
				on fi.BusinessUnitID = vas2.businessUnitID
				and fi.InventoryID = vas2.inventoryId
	group by
		fi.BusinessUnitID
)
, WeeklyUsage as
(

	select
		fi2.BusinessUnitID,
		UsedWeek1Ads = sum(case when fi2.InventoryType = 2 and w.WeekNumber = 1 then 1 else 0 end),
		UsedWeek2Ads = sum(case when fi2.InventoryType = 2 and w.WeekNumber = 2 then 1 else 0 end),
		UsedWeek3Ads = sum(case when fi2.InventoryType = 2 and w.WeekNumber = 3 then 1 else 0 end),
		UsedWeek4Ads = sum(case when fi2.InventoryType = 2 and w.WeekNumber = 4 then 1 else 0 end),
		NewWeek1Ads = sum(case when fi2.InventoryType = 1 and w.WeekNumber = 1 then 1 else 0 end),
		NewWeek2Ads = sum(case when fi2.InventoryType = 1 and w.WeekNumber = 2 then 1 else 0 end),
		NewWeek3Ads = sum(case when fi2.InventoryType = 1 and w.WeekNumber = 3 then 1 else 0 end),
		NewWeek4Ads = sum(case when fi2.InventoryType = 1 and w.WeekNumber = 4 then 1 else 0 end)
	from
		FilteredInventory fi2
		join Merchandising.postings.Publications p
			on fi2.BusinessUnitID = p.businessUnitID
			and fi2.InventoryID = p.inventoryId
		join Merchandising.Reports.MaxAdUsageWeeks() w
			on p.datePosted between w.WeekStart and w.WeekEnd
	group by
		fi2.BusinessUnitID
)
select
	fs.BusinessUnit,
	ic.UsedCars,
	ic.UsedCarswAds,
	ic.NewCars,
	ic.NewCarswAds,
	wu.UsedWeek1Ads,
	wu.UsedWeek2Ads,
	wu.UsedWeek3Ads,
	wu.UsedWeek4Ads,
	wu.NewWeek1Ads,
	wu.NewWeek2Ads,
	wu.NewWeek3Ads,
	wu.NewWeek4Ads,
	fs.BusinessUnitCode
from
	FilteredStores fs
	left join InventoryCounts ic
		on fs.BusinessUnitID = ic.BusinessUnitID
	left join WeeklyUsage wu
		on fs.BusinessUnitID = wu.BusinessUnitID

go