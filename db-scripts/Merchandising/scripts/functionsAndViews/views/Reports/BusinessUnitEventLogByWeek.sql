IF OBJECT_ID('[Reports].[BusinessUnitEventLogByWeek]', 'V') IS NOT NULL 
    DROP VIEW [Reports].[BusinessUnitEventLogByWeek]
GO

CREATE VIEW [Reports].[BusinessUnitEventLogByWeek]
AS
    WITH    CTE_BusinessUnitEventLog_NormalizedByWeek ( BusinessUnitID, WeekEnding, EventDescription )
              AS ( SELECT   BusinessUnitID = L.BusinessUnitID
                          , WeekEnding = DATEADD(week, DATEDIFF(week, 0, DATEADD(day, -1, L.EventDate)) + 1, 0)
                          , EventDescription = E.EventDescription
                   FROM     [Merchandising].[audit].[BusinessUnitEventLog] L
                            LEFT JOIN [Merchandising].[settings].[EventTypes] E ON E.EventType = L.EventType
                 )
    SELECT  BusinessUnitID
          , WeekEnding
          , EventDescription
          , [Count] = COUNT(*)
    FROM    CTE_BusinessUnitEventLog_NormalizedByWeek
    GROUP BY BusinessUnitID
          , WeekEnding
          , EventDescription

GO

GRANT SELECT ON [Reports].[BusinessUnitEventLogByWeek] TO [SalesForceExtractorService]
GRANT SELECT ON [Reports].[BusinessUnitEventLogByWeek] TO [FIRSTLOOK\DataManagementReader]
