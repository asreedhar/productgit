use Merchandising

if object_id('Merchandising.Reports.VehicleActivityInfo', 'V') is not null
    drop view Reports.VehicleActivityInfo
go

create view Reports.VehicleActivityInfo
as
select inv.BusinessUnitID,
       inv.InventoryID,
       veh.VIN as VIN,
       inv.StockNumber,
       
       max(veh.VehicleYear) as [Year],
       max(mmg.Make) as Make,
       max(mmg.Model) as Model,
       max(veh.VehicleTrim) as [Trim],
       
       max(inv.MileageReceived) as Mileage,
       max(inv.AgeInDays) as AgeInDays,
       max(inv.InventoryType) as InventoryType,
       max(inv.InventoryReceivedDate) as InventoryReceivedDate,
       max(lrd.lastrepricedate) as LastRepriceDate,
       
       -- Pricing Fields
       max(inv.ListPrice) as InternetPrice,
       case 
        when max(srf.SearchID) is null 
             or max(srf.Units) < 5 
             or coalesce(max(srf.AvgListPrice), 0) = 0 
             or max(inv.ListPrice) > max(srf.AvgListPrice) * max(op.ExcludeHighPriceOutliersMultiplier) 
             or max(inv.ListPrice) < max(srf.AvgListPrice) * max(op.ExcludeLowPriceOutliersMultiplier) then null
        else round(coalesce(max(inv.ListPrice), 0.0) / max(srf.AvgListPrice) * 100, 4)
       end as PctAvgMarketPrice,
       case
        when max(srf.SearchID) is null
             or max(srf.Units) < 5
             or coalesce(max(srf.AvgListPrice), 0) = 0 then null
        else round(max(srf.AvgListPrice), 2)
       end as AvgMarketPrice,
       
       -- Approval Date
       max(vas.ScheduledReleaseTime) as ApprovalDate,
       
       -- Threshold Settings
       coalesce(max(thresholds.LowActivityThreshold), cast(0.01 as real)) as LowActivityThreshold,
       coalesce(max(thresholds.HighActivityThreshold), cast(0.04 as real)) as HighActivityThreshold,
       coalesce(max(thresholds.MinSearchCountThreshold), 100) as MinSearchCountThreshold
       
from FLDW.dbo.InventoryActive inv
     -- Following joins are for filtering out offline vehicles
     left join Merchandising.Builder.OptionsConfiguration oc
        on inv.BusinessUnitID = oc.BusinessUnitID
        and inv.InventoryID = oc.InventoryID
     -- Following joins are for vehicle information
     inner join FLDW.dbo.Vehicle veh
        on inv.VehicleID = veh.VehicleID
        and inv.BusinessUnitID = veh.BusinessUnitID
     inner join IMT.dbo.MakeModelGrouping mmg
        on mmg.MakeModelGroupingID = veh.MakeModelGroupingID
     left join Merchandising.Reports.VehicleActivity#Thresholds thresholds
        on inv.BusinessUnitID = thresholds.BusinessUnitID
     -- Following joins are for "Approval Date"
     left join Merchandising.postings.VehicleAdScheduling vas
        on inv.BusinessUnitID = vas.BusinessUnitID
        and inv.InventoryID = vas.InventoryID
     --last reprice
     left join Reports.InventoryLastRepriceDate lrd
		on lrd.BusinessUnitId = inv.BusinessUnitId
		and lrd.InventoryId = inv.InventoryId
     -- Following joins are only for PctAvgMarketPrice and AvgMarketPrice
     inner join Market.Pricing.[Owner] own
        on own.OwnerTypeID = 1
            and own.OwnerEntityID = inv.BusinessUnitID
     left join Market.Pricing.Search s
        on s.OwnerID = own.OwnerID
            and inv.InventoryID = s.VehicleEntityID
            and s.VehicleEntityTypeID = 1
     left join Market.Pricing.SearchResult_F srf
        on srf.OwnerID = own.OwnerID
            and s.SearchID = srf.SearchID
            and s.DefaultSearchTypeID = srf.SearchTypeID
     left join Market.Pricing.OwnerPreference op
        on own.OwnerID = op.OwnerID
        
where inv.InventoryActive = 1
      and coalesce(oc.DoNotPostFlag, 0) = 0 -- exclude offline vehicles
group by inv.BusinessUnitID, inv.InventoryID, inv.StockNumber, veh.VIN
        
go