if object_id('Reports.VehiclesOnline') is not null 
	drop view Reports.VehiclesOnline
go

create view Reports.VehiclesOnline
as

with Hendrick as
(
	select
		BusinessUnitId
	from imt.dbo.BusinessUnitRelationship with (nolock)
	where ParentID = 100068 
)
, CarsHendrick as
(
	select
		BusinessUnitId
	from imt.dbo.BusinessUnitRelationship bur with (nolock)
	where
		ParentID = 100068
		and exists
		(
			select *
			from Merchandising.settings.DealerListingSites with (nolock)
			where
				businessUnitID = bur.BusinessUnitID
				and destinationID = 1
				and collectionEnabled = 1
		)
)
select
	inv.BusinessUnitId,
	inv.InventoryId,
	inv.StockNumber,
	inv.Vin,
	inv.InventoryReceivedDate,
	inv.VehicleYear,
	inv.Make,
	inv.Model,
	inv.Trim,
	inv.InventoryType,
	Approved =
		convert
		(
			bit,
			case
				when exists
				(
					select *
					from MerchandisingInterface.Interface.AdReleases with (nolock)
					where
						businessUnitID = inv.BusinessUnitID
						and inventoryId = inv.InventoryID
				)
				then 1
				else 0
			end
		),
	
	AtOnline = 
		convert
		(
			bit,
			case
				when
				(
					exists
					(
						select *
						from Merchandising.merchandising.AutoTraderVehOnline at with (nolock)
						where
							at.BusinessUnitID = inv.BusinessUnitID
							and at.VIN = inv.Vin
							and at.RequestId = los.FetchAtRequest
					)
					or 
					(
						exists 
						( 								
							select * from Hendrick h with (nolock)
							where inv.BusinessUnitId = h.BusinessUnitId
						)
						and
						exists
						(
							select *
							from Merchandising.merchandising.AutoTraderHendrickOnline o with (nolock)
							where
								o.BusinessUnitId = inv.BusinessUnitID
								and o.Vin = inv.Vin
								and o.LoadId = los.AtDirectLoad
						)
					)
				) 
				then 1
				else 0
			end
		),

	CarsOnline =
		convert
		(
			bit,
			case
				when
				(
					exists
					(
						select *
						from Merchandising.merchandising.CarsDotComVehOnline cars with (nolock)
						where
							cars.BusinessUnitID = inv.BusinessUnitID
							and cars.VIN = inv.Vin
							and cars.RequestId = los.FetchCarsRequest
					)
					or 
					(
						exists 
						(
							select *
							from CarsHendrick ch with (nolock)
							where ch.BusinessUnitId = inv.BusinessUnitId
						)
						and
						exists
						(
							select *
							from Merchandising.dashboard.VehicleOnline o with (nolock)
							where
								o.BusinessUnitId = inv.BusinessUnitID
								and o.InventoryId = inv.InventoryID
								and o.LoadId = los.CarsDirectLoad
						)
					)
				)
				then 1
				else 0
			end
		),

	HasAtData =
		convert
		(
			bit,
			case
				when
				(
					exists
					(
						-- lists the inventory type of this car
						select *
						from Merchandising.settings.DealerListingSites dls with (nolock)
						where dls.businessUnitID = inv.BusinessUnitID
							and dls.destinationID = 2 -- AutoTrader
							and ((dls.ListNew = 1       and inv.InventoryType = 1) -- New
								or (dls.ListUsed = 1      and inv.InventoryType = 2) -- Used
								or (dls.ListCertified = 1 and inv.Certified = 1))    -- Certified
					)
					and
					(
						(
							-- hendrick store and at least one feed is not stale
							exists
							(
								-- is hendrick store
								select *
								from Hendrick with (nolock)
								where BusinessUnitID = inv.BusinessUnitID
							)
							and
							(
								-- one feed is not stale
								datediff(day, los.FetchAtDateLoaded, getdate()) < isnull(env.SettingValue, 9)
								or
								datediff(day, los.AtDirectDateLoaded, getdate()) < isnull(env.SettingValue, 9)
							)
						)
						or
						(
							-- non-hendrick, have credentials, and the fetch feed is not stale
							not exists
							(
								-- is hendrick store
								select *
								from Hendrick with (nolock)
								where BusinessUnitID = inv.BusinessUnitID
							)
							and exists
							(
								-- have credentials
								select *
								from Merchandising.settings.DealerListingSites dls with (nolock)
								where
									dls.businessUnitID = inv.BusinessUnitID
									and dls.destinationID = 2 -- AutoTrader
									and dls.userName <> ''
									and dls.password <> ''
							)
							and
							(
								-- Fetch feed is not stale
								datediff(day, los.FetchAtDateLoaded, getdate()) < isnull(env.SettingValue, 9)
							)
						)
					)
				)
				then 1
				else 0
			end
		),

	HasCarsData =
		convert
		(
			bit,
			case
				when
				(
					exists
					(
						-- lists the inventory type of this car
						select *
						from Merchandising.settings.DealerListingSites dls with (nolock)
						where dls.businessUnitID = inv.BusinessUnitID
							and dls.destinationID = 1 -- Cars.com
							and ((dls.ListNew = 1       and inv.InventoryType = 1) -- New
								or (dls.ListUsed = 1      and inv.InventoryType = 2) -- Used
								or (dls.ListCertified = 1 and inv.Certified = 1))    -- Certified
					)
					and
					(
						(
							-- hendrick store and at least one feed is not stale
							exists
							(
								-- is hendrick store
								select *
								from CarsHendrick with (nolock)
								where BusinessUnitID = inv.BusinessUnitID
							)
							and
							(
								-- one feed is not stale
								datediff(day, los.FetchCarsDateLoaded, getdate()) < isnull(env.SettingValue, 9)
								or
								datediff(day, los.CarsDirectDateLoaded, getdate()) < isnull(env.SettingValue, 9)
							)
						)
						or
						(
							-- non-hendrick, have credentials, and the fetch feed is not stale
							not exists
							(
								-- is hendrick store
								select *
								from CarsHendrick with (nolock)
								where BusinessUnitID = inv.BusinessUnitID
							)
							and exists
							(
								-- have credentials AND lists the inventory type of this car
								select *
								from Merchandising.settings.DealerListingSites dls with (nolock)
								where
									dls.businessUnitID = inv.BusinessUnitID
									and dls.destinationID = 1 -- Cars.com
									and dls.userName <> ''
									and dls.password <> ''
							)
							and
							(
								-- Fetch feed is not stale
								datediff(day, los.FetchCarsDateLoaded, getdate()) < isnull(env.SettingValue, 9)
							)
						)
					)
				)
				then 1
				else 0
			end
		)
from
	Merchandising.workflow.Inventory inv with (nolock)
	left join Merchandising.dashboard.LastOnlineSnapshot los with (nolock)
		on inv.BusinessUnitID = los.BusinessUnitId
	full join
		(
			select SettingValue
			from Merchandising.settings.Environment
			where SettingName = 'VehicleOnlineStaleDataDays'		
		) env
		on 1 = 1
where inv.StatusBucket <> 8 -- Offline / Do not post

go
