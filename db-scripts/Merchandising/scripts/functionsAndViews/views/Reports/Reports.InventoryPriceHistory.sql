if  OBJECT_ID(N'[Reports].[InventoryPriceHistory]') is not null
drop view Reports.InventoryPriceHistory
go

set ansi_nulls on
go

set quoted_identifier on
go

create view Reports.InventoryPriceHistory
as
select
	Source='AIP'
	,InventoryId
	,RepriceDate = BeginDate
	,NewPrice 
from IMT.dbo.RepriceEvent
where OriginalPrice != 0.0 -- exclude initial price setting
union
select
	'LPH'
	,Inventoryid
	,DMSReferenceDT
	,ListPrice
from IMT.dbo.Inventory_ListPriceHistory        

GO


