if  OBJECT_ID(N'[Reports].[VehicleActivity#SinceLastReprice]') is not null
drop view Reports.VehicleActivity#SinceLastReprice
go

set ansi_nulls on
go

set quoted_identifier on
go

create view Reports.VehicleActivity#SinceLastReprice
as

with iapBeforeReprice
as
(
	select 
		BusinessUnitId
		,InventoryId
		,DestinationId
		,BeforeRepriceMaxReportDate = Date
		,SearchPageViewsBeforeLastReprice = max(SearchPageViews )
		,DetailPageViewsBeforeLastReprice = max(DetailPageViews )
		,EmailLeadsBeforeLastReprice = max(EmailLeads )
		,MapsViewedBeforeLastReprice = max(MapsViewed )
		,AdPrintsBeforeLastReprice = max(AdPrints )
	from Merchandising.dashboard.InventoryAdPerformance iap
	where 
		Date = 
			(
				select Max(Date) 
				from Merchandising.dashboard.InventoryAdPerformance a1
				where a1.BusinessUnitId = iap.BusinessUnitId
				and a1.InventoryId = iap.InventoryId
				and a1.DestinationId = iap.DestinationId
				and a1.Date <= (	
									select max(lastrepricedate)
									from Reports.InventoryLastRepriceDate lrd
									where lrd.BusinessUnitId = a1.BusinessUnitId
									and lrd.InventoryId = a1.InventoryId
									group by lrd.BusinessUnitId, lrd.InventoryId
								)
			)
	group by BusinessUnitId, InventoryId, DestinationId, Date
),
iapAfterReprice
as
(
	select 
		BusinessUnitId
		,InventoryId
		,DestinationId
		,LatestReportDate = Date
		,LatestSearchPageViews = max(SearchPageViews )
		,LatestDetailPageViews = max(DetailPageViews )
		,LatestEmailLeads = max(EmailLeads )
		,LatestMapsViewed = max(MapsViewed )
		,LatestAdPrints = max(AdPrints )
	from Merchandising.dashboard.InventoryAdPerformance iap
	where 
		Date = 
			(
				select Max(Date) 
				from Merchandising.dashboard.InventoryAdPerformance a1
				where a1.BusinessUnitId = iap.BusinessUnitId
				and a1.InventoryId = iap.InventoryId
				and a1.DestinationId = iap.DestinationId
				and a1.Date > (	
									select max(lastrepricedate)
									from Reports.InventoryLastRepriceDate lrd
									where lrd.BusinessUnitId = a1.BusinessUnitId
									and lrd.InventoryId = a1.InventoryId
									group by lrd.BusinessUnitId, lrd.InventoryId
								)
			)
	group by BusinessUnitId, InventoryId, DestinationId, Date
)

select 
	a.BusinessUnitId
	,a.InventoryId
	,a.DestinationId
	,b.BeforeRepriceMaxReportDate
	,b.SearchPageViewsBeforeLastReprice
	,b.DetailPageViewsBeforeLastReprice
	,b.EmailLeadsBeforeLastReprice
	,b.MapsViewedBeforeLastReprice
	,b.AdPrintsBeforeLastReprice
	,a.LatestReportDate
	,a.LatestSearchPageViews
	,a.LatestDetailPageViews
	,a.LatestEmailLeads
	,a.LatestMapsViewed
	,a.LatestAdPrints
	,SearchPageViewsSinceLastReprice = a.LatestSearchPageViews - isnull(b.SearchPageViewsBeforeLastReprice,0)
	,DetailPageViewsSinceLastReprice = a.LatestDetailPageViews - isnull(b.DetailPageViewsBeforeLastReprice,0)
	,EmailLeadsSinceLastReprice = a.LatestEmailLeads - isnull(b.EmailLeadsBeforeLastReprice,0)
	,MapsViewedSinceLastReprice = a.LatestMapsViewed - isnull(b.MapsViewedBeforeLastReprice,0)
	,AdPrintsSinceLastReprice = a.LatestAdPrints - isnull(b.AdPrintsBeforeLastReprice,0)
	,ClickThruRateBeforeLastReprice = 
		case
			when b.SearchPageViewsBeforeLastReprice is null then null	-- no data exists, CTR-Before is null (documenting intention here)
			when b.SearchPageViewsBeforeLastReprice = 0 then 0.0	-- no div by zero
			else 
				case 
					when 
						cast(b.DetailPageViewsBeforeLastReprice as real) 
						/ cast(b.SearchPageViewsBeforeLastReprice as real) < 0.0 then 0.0 -- negative click thru rates are crazy
					else 
						cast(b.DetailPageViewsBeforeLastReprice as real) 
						/ cast(b.SearchPageViewsBeforeLastReprice as real)
				end
		end	
	,ClickThruRateSinceLastReprice = 
		case 
			when a.LatestSearchPageViews - isnull(b.SearchPageViewsBeforeLastReprice,0) = 0 then 0.0	-- no div by zero
			else 
				case 
					when 
						cast(a.LatestDetailPageViews - isnull(b.DetailPageViewsBeforeLastReprice,0) as real) 
						/ cast(a.LatestSearchPageViews - isnull(b.SearchPageViewsBeforeLastReprice,0) as real) < 0.0 then 0.0 -- negative click thru rates are crazy
					else 
						cast(a.LatestDetailPageViews - isnull(b.DetailPageViewsBeforeLastReprice,0) as real) 
						/ cast(a.LatestSearchPageViews - isnull(b.SearchPageViewsBeforeLastReprice,0) as real)
				end
		end	
from iapAfterReprice a
left join iapBeforeReprice b
	on b.BusinessUnitId = a.BusinessUnitId
	and b.InventoryId = a.InventoryId
	and b.DestinationId = a.DestinationId


go


