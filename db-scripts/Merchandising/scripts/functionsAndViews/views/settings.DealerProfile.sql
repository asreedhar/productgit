if exists (select * from dbo.sysobjects where id = object_id(N'[settings].[DealerProfile]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [settings].[DealerProfile]
GO


CREATE VIEW [settings].[DealerProfile]
AS
with ActiveMaxAdDealers as
(
	select bu.BusinessUnitID
	from imt.dbo.BusinessUnit bu 
	join imt.dbo.DealerUpgrade du on du.BusinessUnitID = bu.BusinessUnitID
	where du.DealerUpgradeCD = 24 -- MAX AD (Wanamaker)
	and du.EffectiveDateActive = 1
	and bu.Active = 1
)
,WebsitePDFDealers as
(
	select bu.BusinessUnitID
	from imt.dbo.BusinessUnit bu 
	where exists (
		select * 
		from imt.dbo.DealerUpgrade du 
		where du.BusinessUnitID = bu.BusinessUnitID and du.EffectiveDateActive = 1
		and du.DealerUpgradeCD = 23 -- MAX Merchandising
	)
	and exists (
		select * 
		from imt.dbo.DealerUpgrade du 
		where du.BusinessUnitID = bu.BusinessUnitID and du.EffectiveDateActive = 1
		and du.DealerUpgradeCD = 25 -- WebSite PDF
	)
)
, DealerLogos as
(
	SELECT  p.PhotoURL, bu.BusinessUnitID
	FROM    imt.dbo.BusinessUnit bu
	JOIN    imt.dbo.BusinessUnitPhotos bup ON bup.BusinessUnitID = bu.BusinessUnitID 
	JOIN    imt.dbo.Photos p ON p.PhotoID = bup.PhotoID
	JOIN    Market.Pricing.Owner mpo ON mpo.OwnerEntityId = bu.BusinessUnitID
	WHERE   mpo.OwnerTypeId = 1 
	AND 	(p.SequenceNumber = 1 OR p.SequenceNumber = 0)
)
,Website2Dealers as
(
	select msm.businessUnitID
	from Merchandising.settings.Merchandising msm
	where msm.MAXForWebsite20 = 1
)
,MobileShowroomDealers as
(
	select msm.businessUnitID
	from Merchandising.settings.Merchandising msm
	where msm.maxForSmartphone = 1
)
,DigitalShowroomDealers as
(
	select msm.businessUnitID
	from Merchandising.settings.Merchandising msm
	where msm.MaxDigitalShowroom = 1
)
,BookValuationsDealers as
(
	select msm.businessUnitID
	from Merchandising.settings.Merchandising msm
	where msm.EnableShowroomBookValuations = 1
)
,PhoneMappingDealers as
(
	SELECT dpm.*
	FROM [Merchandising].[settings].[DealerPhoneMapping] DPM
	JOIN [Merchandising].[settings].[Merchandising] M ON DPM.[BusinessUnitID] = M.[BusinessUnitID]
		AND M.[UsePhoneMapping] = 1
)
,NewVehiclesDealers as
(
	SELECT msm.businessUnitId
	from Merchandising.settings.Merchandising msm
	where msm.vehiclesInShowroom in (0,1)
)
,UsedVehiclesDealers as
(
	SELECT msm.businessUnitId
	from Merchandising.settings.Merchandising msm
	where msm.vehiclesInShowroom in (0,2)
)
,ProfitMaxDealers AS
(
	SELECT BusinessUnitID
	FROM imt.dbo.dealerpreference_pricing
	WHERE IsNewPing = 1
)

select 
bu.BusinessUnitID
,BusinessUnitName = bu.BusinessUnit
,BusinessUnitCode = bu.BusinessUnitCode
,OwnerHandle = o.Handle
,GroupId = bup.BusinessUnitID

-- dealer general preference values
,[Address] = isnull(dgp.AddressLine1, '')
,City = isnull(dgp.City, '')
,[Description] = isnull(dgp.ExtendedTagLine, '')
,Email = isnull(dgp.SalesEmailAddress, '')
,Phone = (case
	when isnull(dpm.provisionedphone, '') <> '' and ISNULL(dpm.Purchased, 0) = 1
	then isnull(dpm.provisionedphone, '')
	else isnull(dgp.SalesPhoneNumber, '')
end)
,Url = isnull(dgp.url, '')
,[State] = isnull(dgp.StateCode, '')
,ZipCode = isnull(dgp.ZipCode, '')

-- Max upgrades
,HasMaxForWebsite1 = case when exists (select * from WebsitePDFDealers x where x.BusinessUnitID = bu.BusinessUnitID) then cast(1 as bit) else cast(0 as bit) end
,HasMaxForWebsite2 = case when exists (select * from Website2Dealers x where x.BusinessUnitID = bu.BusinessUnitID) then cast(1 as bit) else cast(0 as bit) end
,HasMobileShowroom = case when exists (select * from MobileShowroomDealers x where x.BusinessUnitID = bu.BusinessUnitID) then cast(1 as bit) else cast(0 as bit) end
,HasShowroom = case when exists (select * from DigitalShowroomDealers x where x.BusinessUnitID = bu.BusinessUnitID) then cast(1 as bit) else cast(0 as bit) end
,HasShowroomBookValuations = case when exists (select * from BookValuationsDealers x where x.BusinessUnitID = bu.BusinessUnitID) then cast(1 as bit) else cast(0 as bit) end
-- Added LogoUrl in 20.0.0 -TH
,LogoUrl = isnull(dl.PhotoUrl, '')
,HasNewVehiclesEnabled = case when exists (select * from NewVehiclesDealers x where x.BusinessUnitID = bu.BusinessUnitID) then cast(1 as bit) else cast(0 as bit) end
,HasUsedVehiclesEnabled = case when exists (select * from UsedVehiclesDealers x where x.BusinessUnitID = bu.BusinessUnitID) then cast(1 as bit) else cast(0 as bit) END
,HasProfitMAX = case when exists (select * from ProfitMaxDealers x where x.BusinessUnitID = bu.BusinessUnitID) then cast(1 as bit) else cast(0 as bit) END

from ActiveMaxAdDealers mad
join imt.dbo.BusinessUnit bu on mad.BusinessUnitID = bu.BusinessUnitID
join imt.dbo.BusinessUnitRelationship bur on bur.BusinessUnitID = bu.BusinessUnitID
join imt.dbo.BusinessUnit bup on bur.ParentID = bup.BusinessUnitID
join Market.pricing.Owner o on o.OwnerTypeID = 1 and o.OwnerEntityID = bu.BusinessUnitID
left join imt.Marketing.DealerGeneralPreference dgp on dgp.OwnerID = o.OwnerID
left join DealerLogos dl ON dl.BusinessUnitID = bu.BusinessUnitID
left Join PhoneMappingDealers dpm ON dpm.BusinessUnitID = bu.BusinessUnitID
where bu.Active = 1 -- dealer is active
and bup.Active = 1 -- parent bu is active

