use Merchandising

if object_id('chrome.ManufacturerSpecificCategoryDescriptions') is not null
    drop view chrome.ManufacturerSpecificCategoryDescriptions
go

create view chrome.ManufacturerSpecificCategoryDescriptions
as


with
Manufacturers as
(
	select ChromeStyleId, 
		   divisionid 
	from VehicleCatalog.Chrome.YearMakeModelStyle ymms
	where ymms.CountryCode = 1 and ymms.divisionid in ('27') -- Mercedes
)

,Generics as
(
	select SGE.ChromeStyleID, 
		   CS.CategoryId ,
		   CS.CategoryTypeFilter, 
		   CS.UserFriendlyName 
	from VehicleCatalog.Chrome.Categories CS 
	LEFT JOIN  VehicleCatalog.Chrome.StyleGenericEquipment SGE
	   ON SGE.CountryCode = CS.CountryCode
	      AND SGE.CategoryID = CS.CategoryID
    where CS.CountryCode = 1 AND CS.CategoryTypeFilter = 'Drivetrain'
)

,ManufacturerDescriptions as
(
	select styleid as ChromeStyleId, 
		   categorylist, standard as description 
	from VehicleCatalog.Chrome.Standards 
	where  len(categorylist) = 5
					and headerid = 1236  -- Mechanical
					and countrycode = 1
)

select 
	gen.chromestyleid, 
	gen.categoryid,
	gen.CategoryTypeFilter, 
	gen.UserFriendlyName, 
	md.description, 
	man.divisionid from Generics gen 
inner join ManufacturerDescriptions md 
	on gen.ChromeStyleID = md.ChromeStyleId 
		and md.categorylist =   CONVERT(VARCHAR(4),gen.categoryid) + 'C' 
inner join Manufacturers man 
	on man.chromestyleid = md.chromestyleid




