if object_id('chrome.InventoryChromeStyle') is not null
    drop view chrome.InventoryChromeStyle
go

create view chrome.InventoryChromeStyle
as

with 
VinPatternRank as
(
    select veh.BusinessUnitID, veh.VehicleID, VINPatternID, Veh.Vin,
        len(replace(vp.VINPattern, '*', '')) as PatternRank
    from FLDW.dbo.Vehicle veh
        inner join VehicleCatalog.Chrome.VINPattern vp
          on vp.VINPattern_Prefix = substring(veh.VIN, 1, 8) + substring(veh.VIN, 10, 1)
            and substring(veh.VIN, 11, 7) like replace(substring(vp.VINPattern, 11, 7), '*', '_')
    where vp.CountryCode = 1
),

LotChromeStyle as
(
    select VPR.VehicleId, VPR.BusinessUnitId, max(VPSM.ChromeStyleId) as ChromeStyleId
    from VinPatternRank VPR
        inner join VehicleCatalog.Chrome.VINPatternStyleMapping VPSM
            on VPR.VINPatternID = VPSM.VINPatternID
        inner join Lot.Listing.Vehicle LV
            on VPR.VIN = LV.VIN and VPSM.ChromeStyleID = LV.ChromeStyleID
    where VPSM.CountryCode = 1
        and LV.ChromeStyleId <> -1 and LV.ChromeStyleId <> 0
    group by VPR.VehicleId, VPR.BusinessUnitId
),

VinPatternRankWithMax as
(
    select vpr.BusinessUnitID, vpr.VehicleID, max(vpr.PatternRank) as MaxPatternRank
    from VinPatternRank vpr
    group by vpr.BusinessUnitID, vpr.VehicleId
),

VinPatternTopRank as
(
    select vpr.BusinessUnitID, vpr.VehicleID, vpr.VINPatternID
    from VinPatternRankWithMax mvpr
        inner join VinPatternRank vpr 
          on mvpr.BusinessUnitID = vpr.BusinessUnitID
            and mvpr.VehicleID = vpr.VehicleID
            and mvpr.MaxPatternRank = vpr.PatternRank
),

VinPatternToChromeStyleId as
(
    select vpr.BusinessUnitID, vpr.VehicleID, max(vpsm.ChromeStyleID) as ChromeStyleID
    from VinPatternTopRank vpr
        left join VehicleCatalog.Chrome.VINPatternStyleMapping vpsm
          on vpr.VINPatternID = vpsm.VINPatternID
    where vpsm.CountryCode = 1 
    group by vpr.BusinessUnitID, vpr.VehicleID
    having count(distinct vpsm.ChromeStyleID) = 1
),

VehicleCatalogToChromeStyleId as
(
    select cm.VehicleCatalogID, max(vpsm.ChromeStyleID) as ChromeStyleId
    from VehicleCatalog.Firstlook.VehicleCatalog_ChromeMapping cm
        inner join VehicleCatalog.Chrome.VINPatternStyleMapping vpsm
          on cm.Chrome_VinMappingId = vpsm.VinMappingId
    where vpsm.CountryCode = 1 
    group by cm.VehicleCatalogID
    having count(distinct vpsm.ChromeStyleId) = 1
)

SELECT	IA.BusinessUnitID, 
	IA.InventoryID, IA.StockNumber, V.VIN,
	OC.ChromeStyleID
	
FROM	FLDW.dbo.InventoryActive IA
	INNER JOIN FLDW.dbo.Vehicle V ON IA.BusinessUnitID = V.BusinessUnitID AND IA.VehicleID = V.VehicleID
	INNER JOIN builder.OptionsConfiguration OC ON IA.BusinessUnitID = OC.BusinessUnitID AND IA.InventoryID = OC.InventoryID
	
WHERE	IA.InventoryActive = 1
	AND OC.ChromeStyleId <> -1
	AND OC.ChromeStyleId < 900000		-- allow ADS7-sourced ChromeStyleIDs to be redecoded

UNION ALL

SELECT	IA.BusinessUnitID, 
	IA.InventoryID, IA.StockNumber, V.VIN,
	COALESCE(VMCS.ChromeStyleID,			-- Regional rules
		 VMC.ChromeStyleID,			-- VIN ModelCode
		 VC.ChromeStyleId,			-- VehicleCatalog,
		 VP.ChromeStyleId,			-- VinPattern
		 L.ChromeStyleId,			-- Lot
		 NULLIF(VDS.ChromeStyleID,0),		-- VDS
		 OC.ChromeStyleID,			-- If ADS7 or -1, retain
		-1					-- default
		) AS ChromeStyleId

FROM 	FLDW.dbo.InventoryActive IA
    	INNER JOIN FLDW.dbo.Vehicle V ON IA.BusinessUnitID = V.BusinessUnitID AND IA.VehicleID = V.VehicleID
    
    	INNER JOIN IMT.dbo.BusinessUnit BU ON IA.BusinessUnitID = BU.BusinessUnitID
        
	LEFT JOIN builder.OptionsConfiguration OC ON IA.BusinessUnitID = OC.BusinessUnitID AND IA.InventoryID = OC.InventoryID
    	
	LEFT JOIN VehicleCatalogToChromeStyleId VC ON V.VehicleCatalogID = VC.VehicleCatalogID
	LEFT JOIN VinPatternToChromeStyleId VP ON V.BusinessUnitID = VP.BusinessUnitID  AND V.VehicleID = VP.VehicleID
	LEFT JOIN LotChromeStyle L ON V.BusinessUnitID = L.BusinessUnitID AND V.VehicleID = L.VehicleID
	LEFT JOIN VehicleCatalog.VDS.Vehicle VDS ON V.Vin = VDS.VIN
	
	OUTER APPLY VehicleCatalog.Chrome.GetStyleIDByVINModelCode(V.Vin, V.ModelCode) VMC        
	OUTER APPLY VehicleCatalog.Chrome.GetRegionalStyleID(V.Vin, V.ModelCode, BU.State, IA.InventoryType) VMCS
      
WHERE	IA.InventoryActive = 1
    	AND (	COALESCE(OC.ChromeStyleID, -1) = -1
    		OR OC.ChromeStyleID >= 900000
    		)
    	
GO

DECLARE @Description VARCHAR(1000) = 
'For all active inventory in FLDW, this view returns a Chrome StyleID as set in builder.OptionsConfiguration or with a best-guess derivation using various methods (...)'   

EXEC dbo.sp_SetViewDescription 'chrome.InventoryChromeStyle', @Description		
