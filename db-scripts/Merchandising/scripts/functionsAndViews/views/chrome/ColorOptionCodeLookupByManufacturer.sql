IF (OBJECT_ID('chrome.ColorOptionCode#LookupByManufacturer') IS NOT NULL)
	DROP VIEW chrome.ColorOptionCode#LookupByManufacturer 
GO

CREATE VIEW chrome.ColorOptionCode#LookupByManufacturer
AS

SELECT  DISTINCT
		COALESCE(C.Ext1ManCode, C.Ext1Code) OptionCode,
		C.Ext1Desc OptionDesc,
		0 OptionKind,
		S.StyleID,
		F.ManufacturerID,
		F.ManufacturerName,
		C.CountryCode
FROM	VehicleCatalog.Chrome.Colors C
JOIN    VehicleCatalog.Chrome.Styles S ON C.CountryCode = S.CountryCode AND C.StyleID = S.StyleID
JOIN    VehicleCatalog.Chrome.Models M ON S.CountryCode = M.CountryCode AND S.ModelID = M.ModelID
JOIN    VehicleCatalog.Chrome.Divisions D ON M.CountryCode = D.CountryCode AND M.DivisionID = D.DivisionID
JOIN    VehicleCatalog.Chrome.Manufacturers F ON D.CountryCode = F.CountryCode AND D.ManufacturerID = F.ManufacturerID
WHERE	LEN(COALESCE(C.Ext1ManCode, C.Ext1Code)) > 0  and C.CountryCode = 1

UNION ALL

SELECT  DISTINCT
        COALESCE(C.IntManCode, C.IntCode) OptionCode,
		C.IntDesc OptionDesc,
        1 OptionKind,
		S.StyleID,
		F.ManufacturerID,
		F.ManufacturerName,
		C.CountryCode
FROM    VehicleCatalog.Chrome.Colors C
JOIN    VehicleCatalog.Chrome.Styles S ON C.CountryCode = S.CountryCode AND C.StyleID = S.StyleID
JOIN    VehicleCatalog.Chrome.Models M ON S.CountryCode = M.CountryCode AND S.ModelID = M.ModelID
JOIN    VehicleCatalog.Chrome.Divisions D ON M.CountryCode = D.CountryCode AND M.DivisionID = D.DivisionID
JOIN    VehicleCatalog.Chrome.Manufacturers F ON D.CountryCode = F.CountryCode AND D.ManufacturerID = F.ManufacturerID
WHERE	LEN(COALESCE(C.IntManCode, C.IntCode)) > 0  and C.CountryCode = 1