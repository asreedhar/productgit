if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[InventoryManufacturerMake]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [builder].[InventoryManufacturerMake]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [builder].[InventoryManufacturerMake]
AS

	select i.InventoryID, m.MakeID, m.Make as MakeName, mfr.ManufacturerID, mfr.ManufacturerName
	from IMT.dbo.Inventory i WITH (NOLOCK)
	join IMT.dbo.tbl_Vehicle v WITH (NOLOCK) on i.VehicleID = v.VehicleID
	join imt.dbo.MakeModelGrouping mmg on v.MakeModelGroupingID = mmg.MakeModelGroupingID
	join VehicleCatalog.Firstlook.Make m on mmg.Make = m.Make
	join VehicleCatalog.Chrome.Divisions d on d.DivisionName = m.Make 
	join VehicleCatalog.Chrome.Manufacturers mfr on d.CountryCode = mfr.CountryCode AND d.ManufacturerID = mfr.ManufacturerID
	where d.CountryCode = 1


GO