if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[VehicleDataAndStatus]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [builder].[VehicleDataAndStatus]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [builder].[VehicleDataAndStatus]
AS


SELECT 
oc.[businessUnitID]
      ,oc.[inventoryId]
      ,oc.[stockNumber]
      ,[isLegacy]
      ,oc.[chromeStyleID]
      ,de.templateID
      ,[lotLocationId]
      ,[afterMarketTrim]
      ,[vehicleCondition]
      ,[tireTread]
      ,[reconditioningText]
      ,[reconditioningValue]
      ,[extColor1]
      ,[extColor2]
      ,[intColor]
      ,[certificationTypeId]
      ,[specialId]
      ,[afterMarketCategoryString]
      ,[additionalInfoText]
      ,[doNotPostFlag]
      ,[onlineFlag]
      ,oc.[createdOn]
      ,oc.[createdBy]
      ,oc.[lastUpdatedOn]
      ,oc.[lastUpdatedBy]
      ,oc.[version],
ISNULL(st.exteriorStatus,0) as exteriorStatus, 
ISNULL(st.interiorStatus,0) as interiorStatus, 
ISNULL(st.pricingStatus,0) as pricingStatus, 
ISNULL(st.postingStatus,0) as postingStatus,
st2.daysPending,
CASE WHEN st.exteriorStatus > 0 THEN 1 ELSE 0 END +
CASE WHEN st.interiorStatus > 0 THEN 1 ELSE 0 END +
CASE WHEN st.pricingStatus > 0 THEN 1 ELSE 0 END as stepsComplete,

oc.lowActivityFlag,
CASE 
WHEN doNotPostFlag = 1 THEN 8                        --doNotPost bucket
WHEN st.postingStatus = 2 THEN 32                    --pending approval bucket
--WHEN (onlineFlag = 1 AND st.postingStatus = 1) OR (st.postingStatus = 3 AND 
--	(dayRecordsInAggregate > 5 AND  --need a min number of days to collect...
--		sumSearchViewed > 100 AND   --needs a min number of searches for clickthru to matter
--		clickThruRatio < 0.02)) THEN 64           --online need attn bucket
WHEN onlineFlag = 1 OR st.postingStatus = 3 THEN 128   --online ok
ELSE 16                                              --web loading incomplete
END as statusBucket,

CASE WHEN len(COALESCE(pub.MerchandisingDescription,de.MerchandisingDescription)) < 100 THEN 1 ELSE 0 END as descriptionAlert,
CASE WHEN (pub.pricePosted < 0.01) THEN 1 ELSE 0 END as pricingAlert,
CASE WHEN len(COALESCE(pub.MerchandisingDescription,de.MerchandisingDescription)) < 100 
			OR (pub.pricePosted < 0.01) 
			OR  (lowActivityFlag = 1)
		THEN 1 ELSE 0 END as remerchandisingFlag


    FROM builder.OptionsConfiguration oc 
    LEFT JOIN (
		SELECT InventoryId, BusinessUnitId, 
			ISNULL([1], 0) as exteriorStatus,
			ISNULL([2], 0) as interiorStatus,
			ISNULL([4], 0) as pricingStatus,
			ISNULL([5], 0) as postingStatus
			FROM 
			(SELECT InventoryId, BusinessUnitId, StatusTypeId, StatusLevel
				FROM builder.VehicleStatus
				) vs
			PIVOT
			(
			max(statusLevel)
			FOR StatusTypeId
			IN ([1],[2],[4],[5])
			) as pvt
	) st ON st.inventoryId = oc.inventoryId AND st.businessUnitId = oc.businessUnitId
	LEFT JOIN (SELECT datediff(d, lastUpdatedOn, getdate()) as daysPending, 
				statusTypeId, 
				statusLevel, inventoryId, businessUnitId
			FROM builder.VehicleStatus
			WHERE statusTypeId = 5 --posting status
			AND statusLevel = 2 --2 is pending
			) as st2
			ON st2.inventoryid = oc.inventoryId 
			AND st2.businessUnitId = oc.businessUnitId
			
	LEFT JOIN builder.Descriptions de ON de.inventoryId = oc.inventoryId AND de.businessUnitId = oc.businessUnitId
	LEFT JOIN postings.Publications pub ON pub.inventoryId = oc.inventoryId AND pub.businessUnitId = oc.businessUnitId
    
	WHERE pub.dateRemoved IS NULL --when no pub exists yet or it is up (null when not removed from web yet)
GO