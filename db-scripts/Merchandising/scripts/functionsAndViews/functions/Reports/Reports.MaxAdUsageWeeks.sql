if object_id('Reports.MaxAdUsageWeeks') is not null
	drop function Reports.MaxAdUsageWeeks
go

/*
	select * from Reports.MaxAdUsageWeeks()
*/
create function Reports.MaxAdUsageWeeks()
returns @Weeks table
(
	WeekNumber tinyint identity(1,1) not null,
	WeekLabel datetime not null,
	WeekStart datetime not null,
	WeekEnd datetime not null
)
as
begin
	declare 
		@today datetime,
		@lastMonday datetime
	

	select
		@today = dateadd(day, 0, datediff(day, 0, getdate())),
		@lastMonday = dateadd(day, (-5 - datepart(weekday, @today)) % 7, @today)


	insert @Weeks
	(
		WeekLabel,
		WeekStart,
		WeekEnd
	)
			  select WeekLabel = dateadd(week, -1, @lastMonday), WeekStart = dateadd(week, -1, @lastMonday), WeekEnd = dateadd(day, -1, @lastMonday)
	union all select WeekLabel = dateadd(week, -2, @lastMonday), WeekStart = dateadd(week, -2, @lastMonday), WeekEnd = dateadd(day, -8, @lastMonday)
	union all select WeekLabel = dateadd(week, -3, @lastMonday), WeekStart = dateadd(week, -3, @lastMonday), WeekEnd = dateadd(day, -15, @lastMonday)
	union all select WeekLabel = dateadd(week, -4, @lastMonday), WeekStart = dateadd(week, -4, @lastMonday), WeekEnd = dateadd(day, -22, @lastMonday)
	

	return
end


go