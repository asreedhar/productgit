
if exists (select * from dbo.sysobjects where id = object_id(N'[builder].[ConstructGetAutoPhotoUrls]') and xtype in (N'FN', N'IF', N'TF'))
drop function [builder].[ConstructGetAutoPhotoUrls]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Kelley, Jonathan>
-- Create date: <4/16/09>
-- Description:	<Generates the photo urls for a getAuto photo code>
-- =============================================
CREATE FUNCTION [builder].[ConstructGetAutoPhotoUrls] (@photoCode nvarchar(5), @vin nvarchar(17),  @photoType nvarchar(1) = '')
   RETURNS @tbl TABLE (url varchar(200) NOT NULL) AS
BEGIN
   
   
   declare @urlStart nvarchar(80)
   if @photoCode <> '' BEGIN
	   
		SELECT @urlStart = 
			CASE 
			WHEN @photoType = 's' OR @photoType = 'S'
				THEN 'http://photos.vehicledata.com/ss/'
				ELSE 'http://imgs.getauto.com/imgs/ag/ga/'
			END 
			+ substring(@vin,14,2) + '/' + substring(@vin,16,2) + '/'
	
	   
	   insert into @tbl
	   	SELECT CAST(@urlStart + CAST(Value as nvarchar) + @photoType + '/' + @vin + '-' + CAST(Value as nvarchar) + @photoType + '.jpg' as nvarchar(200))
		FROM 
			EdgeScorecard.dbo.GetIntegerRange(1,CAST(REPLACE(REPLACE(@photoCode,'D',''),'F','') AS INT))
   END
  RETURN
END

GO

GRANT SELECT ON [builder].[ConstructGetAutoPhotoUrls] TO MerchandisingUser 
GO
