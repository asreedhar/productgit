USE [Merchandising]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[GetSnippetStyles]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Snippets].[GetSnippetStyles]

USE [Merchandising]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [Snippets].[GetSnippetStyles]
(     
@MarketingId            INT
)

RETURNS VARCHAR(8000)
BEGIN
    DECLARE @List VARCHAR(MAX)
      
    SELECT  @List = COALESCE(@List + ',', '') + CAST(mv.chromeStyleId AS VARCHAR)
    FROM Snippets.Marketing_Vehicle mv
   -- JOIN Snippets.Tag t ON mt.tagId = t.id
    WHERE mv.marketingId = @MarketingId
   -- ORDER BY DESCRIPTION
            
    RETURN(@List)
END
