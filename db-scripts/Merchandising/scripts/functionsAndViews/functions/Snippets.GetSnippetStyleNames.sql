USE [Merchandising]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[GetSnippetStyleNames]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Snippets].[GetSnippetStyleNames]

USE [Merchandising]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [Snippets].[GetSnippetStyleNames]
(     
	@ChromeStyleIds VARCHAR(MAX)
)

RETURNS VARCHAR(8000)
BEGIN
    DECLARE @List VARCHAR(MAX)
      
    SELECT @List = COALESCE(@List + ',', '') + styles.StyleName
    FROM VehicleCatalog.Chrome.Styles styles
    WHERE styles.StyleID IN (SELECT Data FROM dbo.split(@ChromeStyleIds, ','))
    ORDER BY styles.StyleName
            
    RETURN(@List)
END
