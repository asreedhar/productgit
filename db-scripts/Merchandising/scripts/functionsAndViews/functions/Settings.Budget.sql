USE [Merchandising]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Settings].[Budget]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Settings].[Budget]

USE [Merchandising]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [Settings].[Budget](@BusinessUnitID INT, @DestinationID INT, @Month DATETIME)
RETURNS INT
AS
BEGIN
	DECLARE @result INT

    Select @result = b.budget 
	From (
			Select BusinessUnitID, DestinationId, MAX(MonthApplicable) as MonthApplicable
			From settings.SiteBudget
			Where BusinessUnitId = @BusinessUnitID
			  and MonthApplicable <= @Month
			  and DestinationId = @DestinationID
			  Group By BusinessUnitID, DestinationId
		) as x 
	Inner Join settings.SiteBudget b 
		On x.DestinationId = b.DestinationId 
		And x.MonthApplicable = b.MonthApplicable
		And x.BusinessUnitID = b.BusinessUnitID
		
	RETURN (@result)
END
