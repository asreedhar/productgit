use Merchandising

if object_id('chrome.GetVinPatternForVin') is not null 
    drop function chrome.GetVinPatternForVin
GO

create function chrome.GetVinPatternForVin(@vin varchar(17))
returns table
as return
with VinPatternRank as
(
    select VINPatternID, 
        rank() over (order by len(replace(vp.VINPattern, '*', '')) desc) as PatternRank
    from VehicleCatalog.Chrome.VINPattern vp
    where vp.CountryCode = 1
        and vp.VINPattern_Prefix = substring(@vin, 1, 8) + substring(@vin, 10, 1)
        and substring(@vin, 11, 7) like replace(substring(vp.VINPattern, 11, 7), '*', '_')
)

select @vin as Vin, VinPatternID
from VinPatternRank vpr
where vpr.PatternRank = 1

GO

grant select on chrome.GetVinPatternForVin to MerchandisingUser 
GO