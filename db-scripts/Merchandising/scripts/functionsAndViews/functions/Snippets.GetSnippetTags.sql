USE [Merchandising]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Snippets].[GetSnippetTags]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [Snippets].[GetSnippetTags]

USE [Merchandising]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [Snippets].[GetSnippetTags]
(     
@MarketingId            INT
)

RETURNS VARCHAR(8000)
BEGIN
    DECLARE @List VARCHAR(MAX)
      
    SELECT  @List = COALESCE(@List + ',', '') + CAST(mt.tagId AS VARCHAR)
    FROM Snippets.Marketing_Tag mt
    JOIN Snippets.Tag t ON mt.tagId = t.id
    WHERE mt.marketingId = @MarketingId
    ORDER BY DESCRIPTION
            
    RETURN(@List)
END
