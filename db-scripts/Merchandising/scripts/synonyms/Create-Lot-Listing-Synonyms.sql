
IF EXISTS ( SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'Listing.Vehicle') ) 
        DROP SYNONYM Listing.Vehicle

CREATE SYNONYM Listing.Vehicle FOR Lot.Listing.Vehicle
GO

----------------------------------------------------------------------------------------

IF EXISTS ( SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'Listing.Color') ) 
        DROP SYNONYM Listing.Color

CREATE SYNONYM Listing.Color FOR Lot.Listing.Color
GO

----------------------------------------------------------------------------------------

IF EXISTS ( SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'Listing.Transmission') ) 
        DROP SYNONYM Listing.Transmission

CREATE SYNONYM Listing.Transmission FOR Lot.Listing.Transmission
GO

----------------------------------------------------------------------------------------

IF EXISTS ( SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'Listing.VehicleOption') ) 
        DROP SYNONYM Listing.VehicleOption

CREATE SYNONYM Listing.VehicleOption FOR Lot.Listing.VehicleOption
GO

----------------------------------------------------------------------------------------

IF EXISTS ( SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'Listing.Make') ) 
        DROP SYNONYM Listing.Make

CREATE SYNONYM Listing.Make FOR Lot.Listing.Make
GO

----------------------------------------------------------------------------------------

IF EXISTS ( SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'Listing.OptionsList') ) 
        DROP SYNONYM Listing.OptionsList

CREATE SYNONYM Listing.OptionsList FOR Lot.Listing.OptionsList
GO

----------------------------------------------------------------------------------------

IF EXISTS ( SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'Listing.Model') ) 
        DROP SYNONYM Listing.Model

CREATE SYNONYM Listing.Model FOR Lot.Listing.Model
GO

----------------------------------------------------------------------------------------

IF EXISTS ( SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'Listing.Trim') ) 
        DROP SYNONYM Listing.Trim

CREATE SYNONYM Listing.Trim FOR Lot.Listing.Trim
GO

----------------------------------------------------------------------------------------

IF EXISTS ( SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'Listing.Provider') ) 
        DROP SYNONYM Listing.Provider

CREATE SYNONYM Listing.Provider FOR Lot.Listing.Provider
GO