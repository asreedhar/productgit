
/****** Object:  Synonym [Interface].[Advertisement#Release]    Script Date: 03/13/2009 17:46:01 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Advertisement#Release]'))
DROP SYNONYM [Interface].[Advertisement#Release]
GO
/****** Object:  Synonym [Interface].[BookoutList#Fetch]    Script Date: 03/13/2009 17:46:01 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[BookoutList#Fetch]'))
DROP SYNONYM [Interface].[BookoutList#Fetch]
GO
/****** Object:  Synonym [Interface].[Dealers#Fetch]    Script Date: 03/13/2009 17:46:01 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Dealers#Fetch]'))
DROP SYNONYM [Interface].[Dealers#Fetch]
GO
/****** Object:  Synonym [Interface].[InventoryList#Fetch]    Script Date: 03/13/2009 17:46:01 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[InventoryList#Fetch]'))

DROP SYNONYM [Interface].[InventoryList#Fetch]
GO
/****** Object:  Synonym [Interface].[InventoryItem#Fetch]    Script Date: 03/13/2009 17:46:01 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[InventoryItem#Fetch]'))
DROP SYNONYM [Interface].[InventoryItem#Fetch]
GO
/****** Object:  Synonym [Interface].[Inventory#Update]    Script Date: 03/13/2009 17:46:01 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Inventory#Update]'))
DROP SYNONYM [Interface].[Inventory#Update]
GO
/****** Object:  Synonym [Interface].[Member#Fetch]    Script Date: 03/13/2009 17:46:01 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Member#Fetch]'))
DROP SYNONYM [Interface].[Member#Fetch]
GO
/****** Object:  Synonym [Interface].[MemberLogin#Fetch]    Script Date: 03/13/2009 17:46:01 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[MemberLogin#Fetch]'))

DROP SYNONYM [Interface].[MemberLogin#Fetch]
GO
/****** Object:  Synonym [Interface].[MemberList#Fetch]    Script Date: 03/13/2009 17:46:01 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[MemberList#Fetch]'))
DROP SYNONYM [Interface].[MemberList#Fetch]
GO
/****** Object:  Synonym [Interface].[Photos#Fetch]    Script Date: 03/13/2009 17:46:01 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Photos#Fetch]'))
DROP SYNONYM [Interface].[Photos#Fetch]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Photo#Publish]'))
DROP SYNONYM [Interface].[Photo#Publish]
GO
/****** Object:  Synonym [Interface].[OnlineDescription#Fetch]    Script Date: 03/13/2009 17:46:01 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[OnlineDescription#Fetch]'))
DROP SYNONYM [Interface].[OnlineDescription#Fetch]
GO
/****** Object:  Synonym [Interface].[HistoricalInventoryList#Fetch]    Script Date: 03/13/2009 17:46:01 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[HistoricalInventoryList#Fetch]'))
DROP SYNONYM [Interface].[HistoricalInventoryList#Fetch]
GO
/****** Object:  Synonym [Interface].[CurrentAdvertisementId#Fetch]    Script Date: 03/13/2009 17:46:01 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[CurrentAdvertisementId#Fetch]'))
DROP SYNONYM [Interface].[CurrentAdvertisementId#Fetch]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[AllListingOptions#Fetch]'))
DROP SYNONYM [Interface].[AllListingOptions#Fetch]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[VehicleListingOptions#Fetch]'))
DROP SYNONYM [Interface].[VehicleListingOptions#Fetch]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[VehicleListingDetails#Fetch]'))
DROP SYNONYM [Interface].[VehicleListingDetails#Fetch]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[ConditionInformation#Release]'))
DROP SYNONYM [Interface].[ConditionInformation#Release]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[VehicleExportStatus#Update]'))
DROP SYNONYM [Interface].[VehicleExportStatus#Update]
GO

CREATE SYNONYM Interface.Advertisement#Release FOR MerchandisingInterface.Interface.Advertisement#Release
GO

CREATE SYNONYM Interface.BookoutList#Fetch FOR MerchandisingInterface.Interface.BookoutList#Fetch
GO

CREATE SYNONYM Interface.Dealers#Fetch FOR MerchandisingInterface.Interface.Dealers#Fetch
GO

CREATE SYNONYM Interface.InventoryList#Fetch FOR MerchandisingInterface.Interface.InventoryList#Fetch
GO

CREATE SYNONYM Interface.InventoryItem#Fetch FOR MerchandisingInterface.Interface.InventoryItem#Fetch
GO


CREATE SYNONYM Interface.Inventory#Update FOR MerchandisingInterface.Interface.Inventory#Update
GO

CREATE SYNONYM Interface.Member#Fetch FOR MerchandisingInterface.Interface.Member#Fetch
GO

CREATE SYNONYM Interface.MemberLogin#Fetch FOR MerchandisingInterface.Interface.MemberLogin#Fetch
GO

CREATE SYNONYM Interface.MemberList#Fetch FOR MerchandisingInterface.Interface.MemberList#Fetch
GO


CREATE SYNONYM Interface.Photos#Fetch FOR MerchandisingInterface.Interface.Photos#Fetch
GO

CREATE SYNONYM [Interface].[OnlineDescription#Fetch] FOR MerchandisingInterface.Interface.[OnlineDescription#Fetch]
GO

CREATE SYNONYM [Interface].[HistoricalInventoryList#Fetch] FOR MerchandisingInterface.Interface.[HistoricalInventoryList#Fetch]
GO

CREATE SYNONYM [Interface].[Photo#Publish] FOR MerchandisingInterface.Interface.[Photo#Publish]
GO

CREATE SYNONYM [Interface].[CurrentAdvertisementId#Fetch] FOR MerchandisingInterface.Interface.[CurrentAdvertisementId#Fetch] 
GO

CREATE SYNONYM [Interface].[AllListingOptions#Fetch] FOR MerchandisingInterface.[Interface].[AllListingOptions#Fetch] 
GO

CREATE SYNONYM [Interface].[VehicleListingOptions#Fetch] FOR MerchandisingInterface.[Interface].[VehicleListingOptions#Fetch] 
GO

CREATE SYNONYM [Interface].[VehicleListingDetails#Fetch] FOR MerchandisingInterface.[Interface].[VehicleListingDetails#Fetch] 
GO

CREATE SYNONYM [Interface].[ConditionInformation#Release] FOR MerchandisingInterface.[Interface].[ConditionInformation#Release] 
GO

CREATE SYNONYM [Interface].[VehicleExportStatus#Update] FOR MerchandisingInterface.[Interface].[VehicleExportStatus#Update]
GO



---------synonyms for merchandising integration-------syn to another syn in merchandisingInterface
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Advertisement#Fetch]'))
DROP SYNONYM [Interface].[Advertisement#Fetch]
GO

CREATE SYNONYM [Interface].[Advertisement#Fetch] FOR MerchandisingInterface.[Interface].[Advertisement#Fetch]
GO
-----
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Advertisement#Insert]'))
DROP SYNONYM [Interface].[Advertisement#Insert] 
GO

CREATE SYNONYM [Interface].[Advertisement#Insert] FOR MerchandisingInterface.[Interface].[Advertisement#Insert]
GO
-----
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Advertisement_Properties#Insert]'))
DROP SYNONYM [Interface].[Advertisement_Properties#Insert] 
GO

CREATE SYNONYM [Interface].[Advertisement_Properties#Insert] FOR MerchandisingInterface.[Interface].[Advertisement_Properties#Insert]
GO
-----
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Advertisement_ExtendedProperties#Insert]'))
DROP SYNONYM [Interface].[Advertisement_ExtendedProperties#Insert] 
GO

CREATE SYNONYM [Interface].[Advertisement_ExtendedProperties#Insert] FOR MerchandisingInterface.[Interface].[Advertisement_ExtendedProperties#Insert]
GO
-----

if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[VehicleAdvertisement#Assign]'))
DROP SYNONYM [Interface].[VehicleAdvertisement#Assign] 
GO

CREATE SYNONYM [Interface].[VehicleAdvertisement#Assign] FOR MerchandisingInterface.[Interface].[VehicleAdvertisement#Assign]
GO
-----

if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[HighInternetPrice#Fetch]'))
DROP SYNONYM [Interface].[HighInternetPrice#Fetch] 
GO

CREATE SYNONYM [Interface].[HighInternetPrice#Fetch] FOR MerchandisingInterface.[Interface].[HighInternetPrice#Fetch]
GO
GRANT EXECUTE ON [Interface].[HighInternetPrice#Fetch] TO MerchandisingUser
GO
-----

if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[HighInternetPrice#Select]'))
DROP SYNONYM [Interface].[HighInternetPrice#Select] 
GO

CREATE SYNONYM [Interface].[HighInternetPrice#Select] FOR MerchandisingInterface.[Interface].[HighInternetPrice#Select]
GO
GRANT EXECUTE ON [Interface].[HighInternetPrice#Select] TO MerchandisingUser
GO
-----

if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Inventory#Listing]'))
DROP SYNONYM [Interface].[Inventory#Listing]
GO

CREATE SYNONYM [Interface].[Inventory#Listing] FOR MerchandisingInterface.[Interface].[Inventory#Listing]
GO

-----original market interfaces
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[ListingProviderMakeModel]'))
DROP SYNONYM [Interface].[ListingProviderMakeModel] 
GO

CREATE SYNONYM [Interface].[ListingProviderMakeModel] FOR MerchandisingInterface.[Interface].[ListingProviderMakeModel]
GO
-----
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[HandleLookup_Vehicle]'))
DROP SYNONYM [Interface].[HandleLookup_Vehicle] 
GO

CREATE SYNONYM [Interface].[HandleLookup_Vehicle] FOR MerchandisingInterface.[Interface].[HandleLookup_Vehicle]
GO
-----
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[OwnerHandle]'))
DROP SYNONYM [Interface].[OwnerHandle] 
GO

CREATE SYNONYM [Interface].[OwnerHandle] FOR MerchandisingInterface.[Interface].[OwnerHandle]
GO
-----
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[MarketPricing]'))
DROP SYNONYM [Interface].[MarketPricing] 
GO

CREATE SYNONYM [Interface].[MarketPricing] FOR MerchandisingInterface.[Interface].[MarketPricing]
GO

------
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[Advertisement#Delete]'))
DROP SYNONYM [Interface].[Advertisement#Delete]
GO

CREATE SYNONYM [Interface].[Advertisement#Delete] FOR MerchandisingInterface.[Interface].[Advertisement#Delete]
GO

------
if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[GetAutoVehicle]'))
DROP SYNONYM [Interface].[GetAutoVehicle]
GO

CREATE SYNONYM [Interface].[GetAutoVehicle] FOR MarketStaging.GetAuto.Vehicle
GO


----

if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[BusinessUnit]'))
DROP SYNONYM [Interface].[BusinessUnit] 
GO

CREATE SYNONYM [Interface].[BusinessUnit] FOR [IMT].dbo.[BusinessUnit]
GO


----

if exists (select * from dbo.sysobjects where id = object_id(N'[Interface].[BusinessUnitRelationship]'))
DROP SYNONYM [Interface].[BusinessUnitRelationship] 
GO

CREATE SYNONYM [Interface].[BusinessUnitRelationship] FOR [IMT].dbo.[BusinessUnitRelationship]
GO