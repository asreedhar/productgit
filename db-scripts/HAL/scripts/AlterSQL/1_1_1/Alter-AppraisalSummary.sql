
-- add a new booklet preferences
-- (a) new columns
-- (b) default values
-- (c) not null constraints
-- (d) update view
ALTER TABLE dbo.AppraisalReviewBookletPreference ADD IncludeAppraisalSummary BIT
ALTER TABLE dbo.AppraisalReviewBookletPreference ADD IncludeAppraisalSummaryHighMileage BIT
GO

UPDATE dbo.AppraisalReviewBookletPreference SET IncludeAppraisalSummary = 0
UPDATE dbo.AppraisalReviewBookletPreference SET IncludeAppraisalSummaryHighMileage = 0
GO

ALTER TABLE dbo.AppraisalReviewBookletPreference ALTER COLUMN IncludeAppraisalSummary BIT NOT NULL
ALTER TABLE dbo.AppraisalReviewBookletPreference ALTER COLUMN IncludeAppraisalSummaryHighMileage BIT NOT NULL
GO

-- update booklet summary to reflect updated preferences
-- (a) new columns
-- (b) default values
-- (c) not null constraints
-- (d) update view
ALTER TABLE dbo.AppraisalReviewBookletSummary ADD IncludeAppraisalSummary BIT
ALTER TABLE dbo.AppraisalReviewBookletSummary ADD IncludeAppraisalSummaryHighMileage BIT
ALTER TABLE dbo.AppraisalReviewBookletSummary ADD NumberOfAppraisalSummaries INT
ALTER TABLE dbo.AppraisalReviewBookletSummary ADD NumberOfAppraisalSummariesWithHighMileage INT
ALTER TABLE dbo.AppraisalReviewBookletSummary ADD AppraisalCreatedDaysThreshold INT
GO

UPDATE dbo.AppraisalReviewBookletSummary SET IncludeAppraisalSummary = 0
UPDATE dbo.AppraisalReviewBookletSummary SET IncludeAppraisalSummaryHighMileage = 0
UPDATE dbo.AppraisalReviewBookletSummary SET NumberOfAppraisalSummaries = 0
UPDATE dbo.AppraisalReviewBookletSummary SET NumberOfAppraisalSummariesWithHighMileage = 0
GO

UPDATE	ARBS
SET		ARBS.AppraisalCreatedDaysThreshold = ARP.AppraisalCreatedDaysThreshold
FROM	dbo.AppraisalReviewBookletSummary ARBS
JOIN	dbo.AppraisalReviewBooklet ARB ON ARBS.AppraisalReviewBookletID = ARB.AppraisalReviewBookletID
JOIN	dbo.FutureTask FT ON FT.FutureTaskID = ARB.FutureTaskID
JOIN	dbo.AppraisalReviewPreferences ARP ON FT.BusinessUnitID = ARP.BusinessUnitID
GO

ALTER TABLE dbo.AppraisalReviewBookletSummary ALTER COLUMN IncludeAppraisalSummary BIT NOT NULL
ALTER TABLE dbo.AppraisalReviewBookletSummary ALTER COLUMN IncludeAppraisalSummaryHighMileage BIT NOT NULL
ALTER TABLE dbo.AppraisalReviewBookletSummary ALTER COLUMN AppraisalCreatedDaysThreshold INT NOT NULL
GO
