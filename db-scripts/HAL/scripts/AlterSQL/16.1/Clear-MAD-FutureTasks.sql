/* update query to cancel FutureTasks with status Submitted, Accepted, Processing, or Error in HAL.FutureTasks table -  */
USE HAL
UPDATE dbo.FutureTask 
SET FutureTaskStatusID = 6, Acknowledged = GETDATE()
WHERE 
	FutureTaskStatusID IN (1,2,3,4)
	AND Created < DATEADD(dd,-1,GETDATE())