-- BUGZID: 18155

ALTER VIEW [dbo].[book_vehicle_option_values] (
	[book_vehicle_option_id],
	[book_vehicle_option_value_type_id],
	[value]
)
AS
SELECT
	ThirdPartyVehicleOptionID,
	ThirdPartyOptionValueTypeID,
	Value
FROM
	[IMT].dbo.ThirdPartyVehicleOptionValues
