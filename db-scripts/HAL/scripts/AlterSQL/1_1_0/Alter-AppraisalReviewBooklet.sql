
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].AppraisalReviewBookletScheduleInterval') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.AppraisalReviewBookletScheduleInterval
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].AppraisalReviewBookletSchedule') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.AppraisalReviewBookletSchedule
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].AppraisalReviewBookletPreference') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.AppraisalReviewBookletPreference
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].AppraisalReviewBookletSummary') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.AppraisalReviewBookletSummary
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].AppraisalReviewBooklet') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.AppraisalReviewBooklet
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].FutureTask') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.FutureTask
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].FutureTaskStatus') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.FutureTaskStatus
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].FutureTaskType') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.FutureTaskType
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Interval]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.Interval
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[IntervalTypes]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.IntervalTypes
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[MonthDays]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.MonthDays
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[WeekDays]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.WeekDays
GO

CREATE TABLE dbo.WeekDays (
	[WeekDay]   INT         NOT NULL,
	[Name]      VARCHAR(50) NOT NULL,
	[ShortName] CHAR(3)     NOT NULL,
	CONSTRAINT PK_WeekDay PRIMARY KEY (
		[WeekDay]
	)
)
GO

EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enumeration of week days. Reference table.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'WeekDays'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key column.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'WeekDays', @level2type=N'COLUMN', @level2name=N'WeekDay'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full name.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'WeekDays', @level2type=N'COLUMN', @level2name=N'Name'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Abbreviated name.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'WeekDays', @level2type=N'COLUMN', @level2name=N'ShortName'
GO

INSERT INTO dbo.WeekDays ([WeekDay],[Name],[ShortName]) VALUES (1, 'Sunday', 'Sun')
INSERT INTO dbo.WeekDays ([WeekDay],[Name],[ShortName]) VALUES (2, 'Monday', 'Mon')
INSERT INTO dbo.WeekDays ([WeekDay],[Name],[ShortName]) VALUES (3, 'Tuesday', 'Tue')
INSERT INTO dbo.WeekDays ([WeekDay],[Name],[ShortName]) VALUES (4, 'Wednesday', 'Wed')
INSERT INTO dbo.WeekDays ([WeekDay],[Name],[ShortName]) VALUES (5, 'Thursday', 'Thu')
INSERT INTO dbo.WeekDays ([WeekDay],[Name],[ShortName]) VALUES (6, 'Friday', 'Fri')
INSERT INTO dbo.WeekDays ([WeekDay],[Name],[ShortName]) VALUES (7, 'Saturday', 'Sat')
GO

CREATE TABLE dbo.MonthDays (
	[MonthDay]  INT         NOT NULL,
	[Name]      VARCHAR(50) NOT NULL,
	CONSTRAINT PK_MonthDay PRIMARY KEY (
		[MonthDay]
	)
)
GO

EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enumeration of days in month. Reference table.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'MonthDays'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key column.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'MonthDays', @level2type=N'COLUMN', @level2name=N'MonthDay'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Full (ordinal) name.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'MonthDays', @level2type=N'COLUMN', @level2name=N'Name'
GO

INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (1, '1st')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (2, '2nd')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (3, '3rd')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (4, '4th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (5, '5th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (6, '6th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (7, '7th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (8, '8th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (9, '9th')
GO

INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (10, '10th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (11, '11th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (12, '12th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (13, '13th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (14, '14th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (15, '15th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (16, '16th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (17, '17th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (18, '18th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (19, '19th')
GO

INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (20, '20th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (21, '21st')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (22, '22nd')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (23, '23rd')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (24, '24th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (25, '25th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (26, '26th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (27, '27th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (28, '28th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (29, '29th')
GO

INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (30, '30th')
INSERT INTO dbo.MonthDays ([MonthDay],[Name]) VALUES (31, '31st')
GO

CREATE TABLE dbo.IntervalTypes (
	IntervalTypeID INT         NOT NULL,
	[Name]         VARCHAR(50) NOT NULL,
	CONSTRAINT PK_IntervalType PRIMARY KEY (
		IntervalTypeID
	)
)
GO

EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enumeration of interval types. Reference table.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'IntervalTypes'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key column.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'IntervalTypes', @level2type=N'COLUMN', @level2name=N'IntervalTypeID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Human readable description of the interval type.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'IntervalTypes', @level2type=N'COLUMN', @level2name=N'Name'
GO

INSERT INTO dbo.IntervalTypes (IntervalTypeID, [Name]) VALUES (1, 'One Time')
INSERT INTO dbo.IntervalTypes (IntervalTypeID, [Name]) VALUES (2, 'Weekly')
INSERT INTO dbo.IntervalTypes (IntervalTypeID, [Name]) VALUES (3, 'Monthly')
INSERT INTO dbo.IntervalTypes (IntervalTypeID, [Name]) VALUES (4, 'Yearly')
GO

CREATE TABLE dbo.Interval (
	IntervalID     INT IDENTITY(1,1) NOT NULL,
	IntervalTypeID INT NOT NULL,
	DayOfWeek      INT NULL,
	DayOfMonth     INT NULL,
	DayOfYear      SMALLDATETIME NULL,
	CONSTRAINT PK_Interval PRIMARY KEY (
		IntervalID
	),
	CONSTRAINT FK_Interval_IntervalType FOREIGN KEY (
		IntervalTypeID
	)
	REFERENCES IntervalTypes (
		IntervalTypeID
	),
	CONSTRAINT FK_Interval_DayOfWeek FOREIGN KEY (
		DayOfWeek
	)
	REFERENCES WeekDays (
		[WeekDay]
	),
	CONSTRAINT FK_Interval_DayOfMonth FOREIGN KEY (
		DayOfMonth
	)
	REFERENCES MonthDays (
		[MonthDay]
	)
)
GO

EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Instance data. An interval defines a [time-centric] periodicity, e.g. Every Tuesday [weekly], Every 15th of the month [monthly], Every July 4th [annually].', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'Interval'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key column.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'Interval', @level2type=N'COLUMN', @level2name=N'IntervalID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Interval type.  Foreign key to [reference] IntervalType.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'Interval', @level2type=N'COLUMN', @level2name=N'IntervalTypeID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Day of the week. Foreign key to [reference] WeekDays.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'Interval', @level2type=N'COLUMN', @level2name=N'DayOfWeek'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Day of the month. Foreign key to [reference] DayOfMonth.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'Interval', @level2type=N'COLUMN', @level2name=N'DayOfMonth'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Day of the year as a date.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'Interval', @level2type=N'COLUMN', @level2name=N'DayOfYear'
GO

CREATE TABLE dbo.AppraisalReviewBookletSchedule (
	AppraisalReviewBookletScheduleID INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID                   INT               NOT NULL,
	HourOfDay                        INT               NOT NULL,
	CONSTRAINT PK_AppraisalReviewBookletSchedule PRIMARY KEY NONCLUSTERED (
		AppraisalReviewBookletScheduleID
	),
	CONSTRAINT UK_AppraisalReviewBookletSchedule_BusinessUnit UNIQUE CLUSTERED (
		BusinessUnitID
	),
	CONSTRAINT CK_AppraisalReviewBookletSchedule_HourOfDay CHECK (
		HourOfDay BETWEEN 0 AND 23
	)
)
GO

EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Parent table for a business units appraisal review booklet automatic print schedule.  The schedule is a collection of intervals joined to the parent.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletSchedule'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletSchedule', @level2type=N'COLUMN', @level2name=N'AppraisalReviewBookletScheduleID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Owner of the schedule.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletSchedule', @level2type=N'COLUMN', @level2name=N'BusinessUnitID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A 24 hour value representing the time by which the booklet is expected to have been generated for each interval.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletSchedule', @level2type=N'COLUMN', @level2name=N'HourOfDay'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A business unit can have only one appraisal review booklet schedule.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletSchedule', @level2type=N'CONSTRAINT', @level2name=N'UK_AppraisalReviewBookletSchedule_BusinessUnit'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Only allow valid 24 hour values (00-23).', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletSchedule', @level2type=N'CONSTRAINT', @level2name=N'CK_AppraisalReviewBookletSchedule_HourOfDay'
GO

CREATE TABLE dbo.AppraisalReviewBookletScheduleInterval (
	AppraisalReviewBookletScheduleID INT NOT NULL,
	IntervalID                       INT NOT NULL,
	CONSTRAINT PK_AppraisalReviewBookletScheduleInterval PRIMARY KEY CLUSTERED (
		AppraisalReviewBookletScheduleID,
		IntervalID
	),
	CONSTRAINT FK_AppraisalReviewBookletScheduleInterval_AppraisalReviewBookletSchedule FOREIGN KEY (
		AppraisalReviewBookletScheduleID
	)
	REFERENCES AppraisalReviewBookletSchedule (
		AppraisalReviewBookletScheduleID
	),
	CONSTRAINT FK_AppraisalReviewBookletScheduleInterval_IntervalID FOREIGN KEY (
		IntervalID
	)
	REFERENCES Interval (
		IntervalID
	)
)
GO

EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Many to many join table between schedule and its intervals.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletScheduleInterval'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key to schedule thats owns the interval.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletScheduleInterval', @level2type=N'COLUMN', @level2name=N'AppraisalReviewBookletScheduleID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key to interval.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletScheduleInterval', @level2type=N'COLUMN', @level2name=N'IntervalID'
GO

CREATE TABLE dbo.AppraisalReviewBookletPreference (
	AppraisalReviewBookletPreferenceID    INT IDENTITY(1,1) NOT NULL,
	BusinessUnitID                        INT               NOT NULL,
	IncludeAppraisalReviewSummary         BIT               NOT NULL,
	IncludeAppraisalReviewDetail          BIT               NOT NULL,
	IncludeAppraisalReviewFollowUpSummary BIT               NOT NULL,
	IncludeAppraisalReviewFollowUpDetail  BIT               NOT NULL,
	EmailNotification                     BIT               NOT NULL,
	EmailAddress                          VARCHAR(255)      NULL,
	Created                               DATETIME          NOT NULL,
	LastModified                          DATETIME          NOT NULL,
	CONSTRAINT PK_AppraisalReviewBookletPreference PRIMARY KEY NONCLUSTERED (
		AppraisalReviewBookletPreferenceID
	),
	CONSTRAINT UK_AppraisalReviewBookletPreference_BusinessUnit UNIQUE CLUSTERED (
		BusinessUnitID
	),
	CONSTRAINT CK_AppraisalReviewBookletPreference_LastModified CHECK (
		LastModified >= Created
	),
	CONSTRAINT CK_AppraisalReviewBookletPreference_EmailAddress CHECK (
		(EmailNotification = 0) OR (EmailNotification = 1 AND EmailAddress IS NOT NULL)
	)
)
GO

EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Default appraisal review booklet generation preferences.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletPreference'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletPreference', @level2type=N'COLUMN', @level2name=N'AppraisalReviewBookletPreferenceID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Owner of the preference record.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletPreference', @level2type=N'COLUMN', @level2name=N'BusinessUnitID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Boolean indicating if the booklet will contain the second look summary pages.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletPreference', @level2type=N'COLUMN', @level2name=N'IncludeAppraisalReviewSummary'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Boolean indicating if the booklet will contain the second look detail pages.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletPreference', @level2type=N'COLUMN', @level2name=N'IncludeAppraisalReviewDetail'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Boolean indicating if the booklet will contain the follow up summary pages.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletPreference', @level2type=N'COLUMN', @level2name=N'IncludeAppraisalReviewFollowUpSummary'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Boolean indicating if the booklet will contain the follow up detail pages.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletPreference', @level2type=N'COLUMN', @level2name=N'IncludeAppraisalReviewFollowUpDetail'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Boolean indicating if the business unit has recipient(s) who want notification of successful booklet generation.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletPreference', @level2type=N'COLUMN', @level2name=N'EmailNotification'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The address to which booklet generation notification is sent if notification is enabled.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletPreference', @level2type=N'COLUMN', @level2name=N'EmailAddress'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The time the preference record was created.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletPreference', @level2type=N'COLUMN', @level2name=N'Created'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The time the preference record was updated.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletPreference', @level2type=N'COLUMN', @level2name=N'LastModified'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A business unit may only have one preference record.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletPreference', @level2type=N'CONSTRAINT', @level2name=N'UK_AppraisalReviewBookletPreference_BusinessUnit'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The email address must be present if they request notification.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletPreference', @level2type=N'CONSTRAINT', @level2name=N'CK_AppraisalReviewBookletPreference_EmailAddress'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A record must be modified after it was created.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletPreference', @level2type=N'CONSTRAINT', @level2name=N'CK_AppraisalReviewBookletPreference_LastModified'
GO

CREATE TABLE dbo.FutureTaskStatus (
	[FutureTaskStatusID] INT          NOT NULL,
	[Name]               VARCHAR(255) NOT NULL,
	CONSTRAINT PK_FutureTaskStatus PRIMARY KEY CLUSTERED (
		[FutureTaskStatusID]
	),
	CONSTRAINT FK_FutureTaskStatus_Name UNIQUE NONCLUSTERED (
		[Name]
	)
)
GO

EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enumeration of future task statuses.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FutureTaskStatus'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary Key.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FutureTaskStatus', @level2type=N'COLUMN', @level2name=N'FutureTaskStatusID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Human readable description of status.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FutureTaskStatus', @level2type=N'COLUMN', @level2name=N'Name'
GO

INSERT INTO dbo.FutureTaskStatus ([FutureTaskStatusID], [Name]) VALUES (1, 'Submitted')
INSERT INTO dbo.FutureTaskStatus ([FutureTaskStatusID], [Name]) VALUES (2, 'Accepted')
INSERT INTO dbo.FutureTaskStatus ([FutureTaskStatusID], [Name]) VALUES (3, 'Processing')
INSERT INTO dbo.FutureTaskStatus ([FutureTaskStatusID], [Name]) VALUES (4, 'Error')
INSERT INTO dbo.FutureTaskStatus ([FutureTaskStatusID], [Name]) VALUES (5, 'Completed')
INSERT INTO dbo.FutureTaskStatus ([FutureTaskStatusID], [Name]) VALUES (6, 'Canceled')
GO

CREATE TABLE dbo.FutureTaskType (
	[FutureTaskTypeID] INT          NOT NULL,
	[Name]             VARCHAR(255) NOT NULL,
	CONSTRAINT PK_FutureTaskType PRIMARY KEY CLUSTERED (
		[FutureTaskTypeID]
	),
	CONSTRAINT FK_FutureTaskType_Name UNIQUE NONCLUSTERED (
		[Name]
	)
)
GO

EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enumeration of future task types.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FutureTaskType'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FutureTaskType', @level2type=N'COLUMN', @level2name=N'FutureTaskTypeID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Human readable description of future task type.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FutureTaskType', @level2type=N'COLUMN', @level2name=N'Name'
GO

INSERT INTO dbo.FutureTaskType ([FutureTaskTypeID], [Name]) VALUES (1, 'Appraisal Review Booklet')
GO

CREATE TABLE dbo.FutureTask (
	FutureTaskID         INT IDENTITY(1,1) NOT NULL,
	FutureTaskTypeID     INT NOT NULL,
	FutureTaskStatusID   INT NOT NULL,
	BusinessUnitID       INT NOT NULL,
	MemberID             INT NULL,
	Title                VARCHAR(255) NOT NULL,
	UnitsOfWork          INT NOT NULL,
	UnitsOfWorkCompleted INT NOT NULL,
	Created              DATETIME NOT NULL,
	Commenced            DATETIME NULL,
	Completed            DATETIME NULL,
	Acknowledged         DATETIME NULL,
	CONSTRAINT PK_FutureTask PRIMARY KEY CLUSTERED (
		FutureTaskID
	),
	CONSTRAINT FK_FutureTask_FutureTaskType FOREIGN KEY (
		FutureTaskTypeID
	)
	REFERENCES FutureTaskType (
		FutureTaskTypeID
	),
	CONSTRAINT FK_FutureTask_FutureTaskStatus FOREIGN KEY (
		FutureTaskStatusID
	)
	REFERENCES FutureTaskStatus (
		FutureTaskStatusID
	),
	CONSTRAINT CK_FutureTask_UnitsOfWorkCompleted CHECK (
		UnitsOfWorkCompleted >= 0 AND UnitsOfWorkCompleted <= UnitsOfWork
	),
	CONSTRAINT CK_FutureTask_Commenced CHECK (
		Commenced IS NULL OR Commenced >= Created
	),
	CONSTRAINT CK_FutureTask_Completed CHECK (
		Completed IS NULL OR Completed >= Commenced
	),
	CONSTRAINT CK_FutureTask_Acknowledged CHECK (
		Acknowledged IS NULL OR Acknowledged >= Completed
	)
)
GO

EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Represents the state of an asynchronous computation. The result of the computation is stored in a derived table.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FutureTask'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FutureTask', @level2type=N'COLUMN', @level2name=N'FutureTaskID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key. Type of work associated with the task.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FutureTask', @level2type=N'COLUMN', @level2name=N'FutureTaskTypeID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key. The status of the task.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FutureTask', @level2type=N'COLUMN', @level2name=N'FutureTaskStatusID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The owner of the task (and respective output).', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FutureTask', @level2type=N'COLUMN', @level2name=N'BusinessUnitID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The user who submitted the task.  If null, an offline application generated the task.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FutureTask', @level2type=N'COLUMN', @level2name=N'MemberID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Title describing the task performed.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FutureTask', @level2type=N'COLUMN', @level2name=N'Title'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The total number of units of work required to complete the task.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FutureTask', @level2type=N'COLUMN', @level2name=N'UnitsOfWork'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The number of units of work completed out of the total.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FutureTask', @level2type=N'COLUMN', @level2name=N'UnitsOfWorkCompleted'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date and time at which the task (requst for work) was created.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FutureTask', @level2type=N'COLUMN', @level2name=N'Created'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date and time at which the work associated with a task was started.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FutureTask', @level2type=N'COLUMN', @level2name=N'Commenced'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date and time the work associated with the task was completed.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FutureTask', @level2type=N'COLUMN', @level2name=N'Completed'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Date and time a user acknowledged the task.  If the output was a document, then acknowledging it would, perhaps, be the act of downloading.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'FutureTask', @level2type=N'COLUMN', @level2name=N'Acknowledged'
GO

CREATE TABLE dbo.AppraisalReviewBooklet (
	AppraisalReviewBookletID              INT IDENTITY(1,1) NOT NULL,
	FutureTaskID                          INT               NOT NULL,
	Booklet                               TEXT              NULL,
	CONSTRAINT PK_AppraisalReviewBooklet PRIMARY KEY NONCLUSTERED (
		AppraisalReviewBookletID
	),
	CONSTRAINT FK_AppraisalReviewBooklet_FutureTask FOREIGN KEY (
		FutureTaskID
	)
	REFERENCES FutureTask (
		FutureTaskID
	),
	CONSTRAINT UK_AppraisalReviewBooklet_FutureTask UNIQUE CLUSTERED (
		FutureTaskID
	)
)
GO

EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Result of the asynchronous generation of an appraisal review booklet.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBooklet'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBooklet', @level2type=N'COLUMN', @level2name=N'AppraisalReviewBookletID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key. Referenced row encapsulates the state of the asynchronous generation. When the state is "complete" then the booklet column is not null.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBooklet', @level2type=N'COLUMN', @level2name=N'FutureTaskID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Relative path to the generated booklet. Should be the booklet except RoR cannot do BLOBs.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBooklet', @level2type=N'COLUMN', @level2name=N'Booklet'
GO

CREATE TABLE dbo.AppraisalReviewBookletSummary (
	AppraisalReviewBookletSummaryID       INT IDENTITY(1,1) NOT NULL,
	AppraisalReviewBookletID              INT               NOT NULL,
	NumberOfAppraisalReviews              INT               NULL,
	NumberOfAppraisalReviewFollowUps      INT               NULL,
	IncludeAppraisalReviewSummary         BIT               NOT NULL,
	IncludeAppraisalReviewDetail          BIT               NOT NULL,
	IncludeAppraisalReviewFollowUpSummary BIT               NOT NULL,
	IncludeAppraisalReviewFollowUpDetail  BIT               NOT NULL,
	EmailNotification                     BIT               NOT NULL,
	EmailAddress                          VARCHAR(255)      NULL,
	NumberOfPages                         INT               NULL,
	SizeOfBooklet                         INT               NULL,
	CONSTRAINT PK_AppraisalReviewBookletSummary PRIMARY KEY NONCLUSTERED (
		AppraisalReviewBookletSummaryID
	),
	CONSTRAINT FK_AppraisalReviewBookletSummary_AppraisalReviewBooklet FOREIGN KEY (
		AppraisalReviewBookletID
	)
	REFERENCES AppraisalReviewBooklet (
		AppraisalReviewBookletID
	),
	CONSTRAINT CK_AppraisalReviewBookletSummary_EmailAddress CHECK (
		(EmailNotification = 0) OR (EmailNotification = 1 AND EmailAddress IS NOT NULL)
	)
)
GO

EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Parameters for the appraisal review booklet future task. Also contains derived data about the generated booklet (size, pages etc).', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletSummary'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletSummary', @level2type=N'COLUMN', @level2name=N'AppraisalReviewBookletSummaryID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key. References placeholder for generated booklet.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletSummary', @level2type=N'COLUMN', @level2name=N'AppraisalReviewBookletID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of "second look" appraisals in the document.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletSummary', @level2type=N'COLUMN', @level2name=N'NumberOfAppraisalReviews'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Number of "follow up" appraisals in the document.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletSummary', @level2type=N'COLUMN', @level2name=N'NumberOfAppraisalReviewFollowUps'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Whether the generated document will have the "second look" summary pages.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletSummary', @level2type=N'COLUMN', @level2name=N'IncludeAppraisalReviewSummary'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Whether the generated document will have the "second look" detail pages.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletSummary', @level2type=N'COLUMN', @level2name=N'IncludeAppraisalReviewDetail'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Whether the generated document will have the "follow up" summary pages.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletSummary', @level2type=N'COLUMN', @level2name=N'IncludeAppraisalReviewFollowUpSummary'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Whether the generated document will have the "follow up" detail pages.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletSummary', @level2type=N'COLUMN', @level2name=N'IncludeAppraisalReviewFollowUpDetail'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Whether or not email notification will be sent.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletSummary', @level2type=N'COLUMN', @level2name=N'EmailNotification'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If notification is to be sent it is to this address.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletSummary', @level2type=N'COLUMN', @level2name=N'EmailAddress'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The (approximate) number of pages in the document', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletSummary', @level2type=N'COLUMN', @level2name=N'NumberOfPages'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The size of the document in bytes.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'AppraisalReviewBookletSummary', @level2type=N'COLUMN', @level2name=N'SizeOfBooklet'
GO
