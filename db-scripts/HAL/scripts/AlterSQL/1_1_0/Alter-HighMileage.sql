
-- lose column as is replaced by a row in IMT.dbo.InsightTarget
ALTER TABLE dbo.AppraisalReviewPreferences DROP COLUMN ClosingRateTarget
GO

-- add a new appraisal preference
-- (a) new column
-- (b) default value
-- (c) not null constraint
-- (d) update view
ALTER TABLE dbo.AppraisalReviewPreferences ADD EnabledHighMileage BIT
GO

UPDATE dbo.AppraisalReviewPreferences SET EnabledHighMileage = 0
GO

ALTER TABLE dbo.AppraisalReviewPreferences ALTER COLUMN EnabledHighMileage BIT NOT NULL
GO

-- add a new booklet preferences
-- (a) new columns
-- (b) default values
-- (c) not null constraints
-- (d) update view
ALTER TABLE dbo.AppraisalReviewBookletPreference ADD IncludeAppraisalReviewHighMileageSummary BIT
ALTER TABLE dbo.AppraisalReviewBookletPreference ADD IncludeAppraisalReviewHighMileageDetail BIT
GO

UPDATE dbo.AppraisalReviewBookletPreference SET IncludeAppraisalReviewHighMileageSummary = 0
UPDATE dbo.AppraisalReviewBookletPreference SET IncludeAppraisalReviewHighMileageDetail = 0
GO

ALTER TABLE dbo.AppraisalReviewBookletPreference ALTER COLUMN IncludeAppraisalReviewHighMileageSummary BIT NOT NULL
ALTER TABLE dbo.AppraisalReviewBookletPreference ALTER COLUMN IncludeAppraisalReviewHighMileageDetail BIT NOT NULL
GO

-- update booklet summary to reflect updated preferences
-- (a) new columns
-- (b) default values
-- (c) not null constraints
-- (d) update view
ALTER TABLE dbo.AppraisalReviewBookletSummary ADD IncludeAppraisalReviewHighMileageSummary BIT
ALTER TABLE dbo.AppraisalReviewBookletSummary ADD IncludeAppraisalReviewHighMileageDetail BIT
ALTER TABLE dbo.AppraisalReviewBookletSummary ADD NumberOfAppraisalReviewsWithHighMileage INT
GO

UPDATE dbo.AppraisalReviewBookletSummary SET IncludeAppraisalReviewHighMileageSummary = 0
UPDATE dbo.AppraisalReviewBookletSummary SET IncludeAppraisalReviewHighMileageDetail = 0
UPDATE dbo.AppraisalReviewBookletSummary SET NumberOfAppraisalReviewsWithHighMileage = 0
GO

ALTER TABLE dbo.AppraisalReviewBookletSummary ALTER COLUMN IncludeAppraisalReviewHighMileageSummary BIT NOT NULL
ALTER TABLE dbo.AppraisalReviewBookletSummary ALTER COLUMN IncludeAppraisalReviewHighMileageDetail BIT NOT NULL
GO
