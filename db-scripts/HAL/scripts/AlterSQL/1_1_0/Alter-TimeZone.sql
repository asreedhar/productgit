
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].TimeZone') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.TimeZone
GO

CREATE TABLE dbo.TimeZone (
	[TimeZoneID] INT IDENTITY(1,1) NOT NULL,
	[Name]       VARCHAR(30)       NOT NULL,
	[Code]       VARCHAR(4)        NOT NULL,
	[AltCode]    VARCHAR(5)        NULL,
	[UTC]        SMALLINT          NOT NULL,
	CONSTRAINT PK_TimeZone PRIMARY KEY (
		TimeZoneID
	)
)
GO

EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enumeration of time zones (for USA). Reference table.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'TimeZone'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key column.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'TimeZone', @level2type=N'COLUMN', @level2name=N'TimeZoneID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Official name of time zone.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'TimeZone', @level2type=N'COLUMN', @level2name=N'Name'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Standard abbreviation of time zone.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'TimeZone', @level2type=N'COLUMN', @level2name=N'Code'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Alternate abbreviation of time zone.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'TimeZone', @level2type=N'COLUMN', @level2name=N'AltCode'
GO

INSERT INTO dbo.TimeZone ([Name],[Code],[AltCode],[UTC]) VALUES ('Atlantic Standard Time', 'AST', 'EST+1', -4)
INSERT INTO dbo.TimeZone ([Name],[Code],[AltCode],[UTC]) VALUES ('Atlantic Daylight Time', 'ADT', NULL, -3)
INSERT INTO dbo.TimeZone ([Name],[Code],[AltCode],[UTC]) VALUES ('Eastern  Standard Time', 'EST', NULL, -5)
INSERT INTO dbo.TimeZone ([Name],[Code],[AltCode],[UTC]) VALUES ('Eastern  Daylight Time', 'EDT', NULL, -4)
INSERT INTO dbo.TimeZone ([Name],[Code],[AltCode],[UTC]) VALUES ('Central  Standard Time', 'CST', NULL, -6)
INSERT INTO dbo.TimeZone ([Name],[Code],[AltCode],[UTC]) VALUES ('Central  Daylight Time', 'CDT', NULL, -5)
INSERT INTO dbo.TimeZone ([Name],[Code],[AltCode],[UTC]) VALUES ('Mountain Standard Time', 'MST', NULL, -7)
INSERT INTO dbo.TimeZone ([Name],[Code],[AltCode],[UTC]) VALUES ('Mountain Daylight Time', 'MDT', NULL, -6)
INSERT INTO dbo.TimeZone ([Name],[Code],[AltCode],[UTC]) VALUES ('Pacific Standard Time', 'PST', NULL, -8)
INSERT INTO dbo.TimeZone ([Name],[Code],[AltCode],[UTC]) VALUES ('Pacific Daylight Time', 'PDT', NULL, -7)
INSERT INTO dbo.TimeZone ([Name],[Code],[AltCode],[UTC]) VALUES ('Alaska Standard Time', 'AKST', 'PST-1', -8)
INSERT INTO dbo.TimeZone ([Name],[Code],[AltCode],[UTC]) VALUES ('Alaska Daylight Time', 'AKDT', NULL, -8)
INSERT INTO dbo.TimeZone ([Name],[Code],[AltCode],[UTC]) VALUES ('Hawaii-Aleutian Standard Time', 'HAST', 'PST-2', -10)
INSERT INTO dbo.TimeZone ([Name],[Code],[AltCode],[UTC]) VALUES ('Hawaii-Aleutian Daylight Time','HADT',  NULL, -9)
INSERT INTO dbo.TimeZone ([Name],[Code],[AltCode],[UTC]) VALUES ('Samoa Standard Time', 'SST', 'PST-4', -11)
INSERT INTO dbo.TimeZone ([Name],[Code],[AltCode],[UTC]) VALUES ('Chamorro Standard Time', 'ChST', NULL, +10)
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].DayLightSavingSchedule') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.DayLightSavingSchedule
GO

CREATE TABLE dbo.DayLightSavingSchedule (
	[DayLightSavingScheduleID] INT IDENTITY(1,1) NOT NULL,
	[Year]                     INT               NOT NULL,
	[Begin]                    DATETIME          NOT NULL,
	[End]                      DATETIME          NOT NULL,
	CONSTRAINT PK_DayLightSavingSchedule PRIMARY KEY (
		[DayLightSavingScheduleID]
	),
	CONSTRAINT UK_DayLightSavingSchedule UNIQUE (
		[Year]
	)
)
GO

EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Enumeration of day light saving schedules (for USA). Reference table.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'DayLightSavingSchedule'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Primary key column.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'DayLightSavingSchedule', @level2type=N'COLUMN', @level2name=N'DayLightSavingScheduleID'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Year for the daylight saving schedule.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'DayLightSavingSchedule', @level2type=N'COLUMN', @level2name=N'Year'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When day light saving comes into effect.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'DayLightSavingSchedule', @level2type=N'COLUMN', @level2name=N'Begin'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'When day light saving is taken out of effect.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'DayLightSavingSchedule', @level2type=N'COLUMN', @level2name=N'End'
EXECUTE sys.sp_addextendedproperty @name=N'MS_Description', @value=N'A year can have only one day light saving interval.', @level0type=N'SCHEMA', @level0name=N'dbo', @level1type=N'TABLE', @level1name=N'DayLightSavingSchedule', @level2type=N'CONSTRAINT', @level2name=N'UK_DayLightSavingSchedule'
GO

INSERT INTO dbo.DayLightSavingSchedule ([Year], [Begin], [End]) VALUES (2007, '03/11/2007', '11/04/2007')
INSERT INTO dbo.DayLightSavingSchedule ([Year], [Begin], [End]) VALUES (2008, '03/09/2008', '11/02/2008')
INSERT INTO dbo.DayLightSavingSchedule ([Year], [Begin], [End]) VALUES (2009, '03/08/2009', '11/01/2009')
INSERT INTO dbo.DayLightSavingSchedule ([Year], [Begin], [End]) VALUES (2010, '03/14/2010', '11/07/2010')
INSERT INTO dbo.DayLightSavingSchedule ([Year], [Begin], [End]) VALUES (2011, '03/13/2011', '11/06/2011')
INSERT INTO dbo.DayLightSavingSchedule ([Year], [Begin], [End]) VALUES (2012, '03/11/2012', '11/04/2012')
INSERT INTO dbo.DayLightSavingSchedule ([Year], [Begin], [End]) VALUES (2013, '03/10/2013', '11/03/2013')
INSERT INTO dbo.DayLightSavingSchedule ([Year], [Begin], [End]) VALUES (2014, '03/09/2014', '11/02/2014')
INSERT INTO dbo.DayLightSavingSchedule ([Year], [Begin], [End]) VALUES (2015, '03/08/2015', '11/01/2015')
GO
