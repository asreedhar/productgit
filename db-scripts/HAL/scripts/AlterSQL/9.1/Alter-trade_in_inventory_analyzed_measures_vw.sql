ALTER VIEW [dbo].[trade_in_inventory_analyzed_measures] (
	id,
	business_unit_id,
	measure,
	measure_from,
	measure_upto,
	measure_numerator,
	measure_denominator
)
AS
SELECT I.BusinessUnitID AS [id]
    ,I.BusinessUnitID AS [business_unit_id]
    ,I.PercentAnalyzed AS [measure]
    ,P.BeginDate AS [measure_from]
    ,P.EndDate AS [measure_upto]
    ,I.VehiclesAnalyzed AS [measure_numerator]
    ,I.TotalVehicles AS [measure_denominator]
FROM FLDW.dbo.InventoryTradeIns_A1 I
JOIN FLDW.dbo.Period_D P ON I.PeriodID = P.PeriodID
