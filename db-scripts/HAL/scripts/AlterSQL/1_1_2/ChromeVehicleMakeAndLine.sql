
INSERT INTO dbo.VehicleMake (
	Name,
	VehicleCategoryID
)
SELECT	Name,
		VehicleCategoryID
FROM	dbo.GetVehicleMake
WHERE	Name NOT IN (SELECT Name FROM VehicleMake)
GO

INSERT INTO dbo.VehicleLine (
	VehicleMakeID,
	MakeModelGroupingID,
	Name,
	QualifiedName
)
SELECT	VehicleMakeID,
		MakeModelGroupingID,
		Name,
		QualifiedName
FROM	dbo.GetVehicleLine
WHERE	MakeModelGroupingID NOT IN (SELECT MakeModelGroupingID FROM VehicleLine)
GO

