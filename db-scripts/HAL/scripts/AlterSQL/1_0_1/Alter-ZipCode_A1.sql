if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ZipCode_A1]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[ZipCode_A1]
GO

CREATE TABLE dbo.ZipCode_A1 (
	ZipCodeID1 INT NOT NULL,
	ZipCodeID2 INT NOT NULL,
	Distance   INT NOT NULL
)
GO

CREATE INDEX IDX_ZipCode_A1_Full ON ZipCode_A1 (
	ZipCodeID1,
	ZipCodeID2
)
GO

CREATE INDEX IDX_ZipCode_A1_Right ON ZipCode_A1 (
	ZipCodeID2
)
GO

IF (	SELECT	TableIndex
	FROM	ViewStatus
	WHERE	ViewName = 'ZipCode_A1'
	) = 0
	
	INSERT
	INTO	ZipCode_A1
	SELECT	*
	FROM	ZipCode_A1#0
ELSE
	INSERT
	INTO	ZipCode_A1
	SELECT	*
	FROM	ZipCode_A1#1

GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ZipCode_A1#0]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.ZipCode_A1#0
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ZipCode_A1#1]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
DROP TABLE dbo.ZipCode_A1#1
GO