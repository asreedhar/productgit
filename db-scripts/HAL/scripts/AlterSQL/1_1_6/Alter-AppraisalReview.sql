--USE hal
GO
--*****************************************************
--Create new rule names
--*****************************************************
INSERT INTO dbo.AppraisalReviewRules (AppraisalReviewRuleId, NAME)
SELECT 7, 'Below JDPower SellingPrice'
INSERT INTO dbo.AppraisalReviewRules (AppraisalReviewRuleId, NAME)
SELECT 8, 'Below JDPower TradeACV'
INSERT INTO dbo.AppraisalReviewRules (AppraisalReviewRuleId, NAME)
SELECT 9, 'Below PING II Avg Market Value'
GO

--*****************************************************
--new database default
--*****************************************************
CREATE DEFAULT dbo.AppraisalReviewPreferencesRuleDisabled AS 0
GO

--*****************************************************
--add flags for new rules, default to disabled
--*****************************************************
ALTER TABLE dbo.AppraisalReviewPreferences
	ADD EnabledRule7 BIT NULL
ALTER TABLE dbo.AppraisalReviewPreferences
	ADD EnabledRule8 BIT NULL
ALTER TABLE dbo.AppraisalReviewPreferences
	ADD EnabledRule9 BIT NULL
ALTER TABLE dbo.AppraisalReviewPreferences
	ADD ValuationDiscrepancyThresholdRule7 INT NULL
ALTER TABLE dbo.AppraisalReviewPreferences
	ADD ValuationDiscrepancyThresholdRule8 INT NULL
ALTER TABLE dbo.AppraisalReviewPreferences
	ADD ValuationDiscrepancyThresholdRule9 INT NULL
ALTER TABLE dbo.AppraisalReviewPreferences
	ADD Rule7JDPowerGeographicAggregationLevel TINYINT NULL
ALTER TABLE dbo.AppraisalReviewPreferences
	ADD Rule8JDPowerGeographicAggregationLevel TINYINT NULL
ALTER TABLE dbo.AppraisalReviewPreferences
	ADD Rule7JDPowerPeriodId TINYINT NULL --references FLDW.JDPower.Period_D
ALTER TABLE dbo.AppraisalReviewPreferences
	ADD Rule8JDPowerPeriodId TINYINT NULL --references FLDW.JDPower.Period_D

--bind defaults
EXEC sp_bindefault @defname = 'dbo.AppraisalReviewPreferencesRuleDisabled', @objname = 'dbo.AppraisalReviewPreferences.EnabledRule7'
EXEC sp_bindefault @defname = 'dbo.AppraisalReviewPreferencesRuleDisabled', @objname = 'dbo.AppraisalReviewPreferences.EnabledRule8'
EXEC sp_bindefault @defname = 'dbo.AppraisalReviewPreferencesRuleDisabled', @objname = 'dbo.AppraisalReviewPreferences.EnabledRule9'
GO
--set values for existing data
UPDATE dbo.AppraisalReviewPreferences
SET	EnabledRule7 = 0,
	EnabledRule8 = 0,
	EnabledRule9 = 0

--redefine columns as NOT NULL
ALTER TABLE dbo.AppraisalReviewPreferences
	ALTER COLUMN EnabledRule7 BIT NOT NULL
ALTER TABLE dbo.AppraisalReviewPreferences
	ALTER COLUMN EnabledRule8 BIT NOT NULL
ALTER TABLE dbo.AppraisalReviewPreferences
	ALTER COLUMN EnabledRule9 BIT NOT NULL

GO
--*****************************************************
--add reference values to dbo.AppraisalReviews
--*****************************************************
ALTER TABLE Hal.dbo.AppraisalReviews
	ADD JDPowerACV DECIMAL(9,2) NULL
ALTER TABLE Hal.dbo.AppraisalReviews
	ADD JDPowerAvgSellingPrice DECIMAL(9,2) NULL
ALTER TABLE Hal.dbo.AppraisalReviews
	ADD PINGAvgMarketPrice DECIMAL(9,2) NULL
ALTER TABLE Hal.dbo.AppraisalReviews
	ADD Rule7 BIT
ALTER TABLE Hal.dbo.AppraisalReviews
	ADD Rule8 BIT
ALTER TABLE Hal.dbo.AppraisalReviews
	ADD Rule9 BIT

GO

--set flags on existing records to false
UPDATE Hal.dbo.AppraisalReviews
SET	Rule7 = 0,
	Rule8 = 0,
	Rule9 = 0

GO
ALTER TABLE hal.dbo.AppraisalReviewPreferences 
	ADD CONSTRAINT DFAppraisalReviewPreferences__Rule7JDPowerPeriodId 
	DEFAULT 1 
	FOR Rule7JDPowerPeriodId
ALTER TABLE hal.dbo.AppraisalReviewPreferences 
	ADD CONSTRAINT DFAppraisalReviewPreferences__Rule8JDPowerPeriodId 
	DEFAULT 1 
	FOR Rule8JDPowerPeriodId
ALTER TABLE hal.dbo.AppraisalReviewPreferences 
	ADD CONSTRAINT DFAppraisalReviewPreferences__Rule7JDPowerGeographicAggregationLevel 
	DEFAULT 2 
	FOR Rule7JDPowerGeographicAggregationLevel
ALTER TABLE hal.dbo.AppraisalReviewPreferences 
	ADD CONSTRAINT DFAppraisalReviewPreferences__Rule8JDPowerGeographicAggregationLevel 
	DEFAULT 2 
	FOR Rule8JDPowerGeographicAggregationLevel
go
UPDATE hal.dbo.AppraisalReviewPreferences
SET	rule7jdpowerperiodid = 1,
	rule8jdpowerperiodid = 1,
	rule7jdpowergeographicaggregationlevel = 2,
	rule8jdpowergeographicaggregationlevel = 2

	