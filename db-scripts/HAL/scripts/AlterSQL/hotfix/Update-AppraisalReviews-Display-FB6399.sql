
-- THIS UPDATE CORRECTLY SETS THE DISPLAY FLAG FOR MAKE-A-DEALS RECTIFYING DEFECTS
-- FB 6399 AND SF 28802

DECLARE @MonthID INT, @DayID INT

SELECT	@MonthID     = CAST(CONVERT(CHAR(6), GETDATE(), 112) AS INT),
	@DayID       = CAST(CONVERT(CHAR(8), GETDATE(), 112) AS INT)

UPDATE	R
SET	Display = 1
FROM	dbo.AppraisalReviews R
WHERE	MonthID = @MonthID
AND	DayID = @DayID
AND	Display = 0
AND	FollowUp = 0
AND	NOT EXISTS (
		SELECT	1
		FROM	dbo.AppraisalReviewFollowUps F
		WHERE	F.BusinessUnitID = R.BusinessUnitID
		AND	F.AppraisalID = R.AppraisalID
		AND	F.DayID = R.DayID
		AND	AppraisalReviewFollowUpActionID IS NOT NULL
	)
