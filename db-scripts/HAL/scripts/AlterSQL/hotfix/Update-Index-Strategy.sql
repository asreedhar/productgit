------------------------------------------------------------------------------------------------------
-- AppraisalReviewBookletSummary
------------------------------------------------------------------------------------------------------
--Drop non clustered PK
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[AppraisalReviewBookletSummary]') AND name = N'PK_AppraisalReviewBookletSummary')
ALTER TABLE [dbo].[AppraisalReviewBookletSummary] DROP CONSTRAINT [PK_AppraisalReviewBookletSummary]

-- Create clustered PK
ALTER TABLE [dbo].[AppraisalReviewBookletSummary] ADD  CONSTRAINT [PK_AppraisalReviewBookletSummary] PRIMARY KEY CLUSTERED 
(
	[AppraisalReviewBookletSummaryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = ON, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

-----------------------------------------------------------------------------------------------------------
-- TABLE [dbo].[AppraisalReviewPreferences]
--------------------------------------------------------------------------------------------------------------

--Drop non-clustered index and create it as a clustered index
CREATE CLUSTERED INDEX [IDX_AppraisalReviewPreferences] ON [dbo].[AppraisalReviewPreferences] 
(
[BusinessUnitID] ASC

)WITH (DROP_EXISTING = ON) ON [PRIMARY]

-----------------------------------------------------------------------------------------------------------
-- VehicleLine
--------------------------------------------------------------------------------------------------------------
-- Drop non-clustered index and create it as a clustered index

/****** Object:  Index [IDX_VehicleLine_MakeModelGrouping]    Script Date: 02/08/2010 12:19:22 ******/
CREATE CLUSTERED INDEX [IDX_VehicleLine_MakeModelGrouping] ON [dbo].[VehicleLine] 
(
	[MakeModelGroupingID] ASC
)WITH (DROP_EXISTING = ON) ON [PRIMARY]

------------------------------------------------------------------------------------------------------
--	Appraisals_A1
------------------------------------------------------------------------------------------------------
-----------------
--Appraisals_A1#0
-----------------
--Drop non-clustered index and create it as a clustered index
CREATE CLUSTERED INDEX [IDX_Appraisals_A1#0] ON [dbo].[Appraisals_A1#0] 
(
	[BusinessUnitID] ASC,
	[ThirdPartyID] ASC,
	[ThirdPartyCategoryID] ASC
)WITH (DROP_EXISTING = ON) ON [PRIMARY]

--Drop Redundant Index
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Appraisals_A1#0]') AND name = N'IDX_Appraisals_A1#0_Appraisal')
DROP INDEX [IDX_Appraisals_A1#0_Appraisal] ON [dbo].[Appraisals_A1#0] WITH ( ONLINE = OFF )

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Appraisals_A1#0]') AND name = N'IDX_Appraisals_A1#0_BusinessUnit')
DROP INDEX [IDX_Appraisals_A1#0_BusinessUnit] ON [dbo].[Appraisals_A1#0] WITH ( ONLINE = OFF )

-----------------
--Appraisals_A1#1
-----------------
--Drop Redundant Index
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Appraisals_A1#1]') AND name = N'IDX_Appraisals_A1#1_Appraisal')
DROP INDEX [IDX_Appraisals_A1#1_Appraisal] ON [dbo].[Appraisals_A1#1] WITH ( ONLINE = OFF )

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Appraisals_A1#1]') AND name = N'IDX_Appraisals_A1#1_BusinessUnit')
DROP INDEX [IDX_Appraisals_A1#1_BusinessUnit] ON [dbo].[Appraisals_A1#1] WITH ( ONLINE = OFF )

--Drop non-clustered index and create it as a clustered index
CREATE CLUSTERED INDEX [IDX_Appraisals_A1#1] ON [dbo].[Appraisals_A1#1] 
(
	[BusinessUnitID] ASC,
	[ThirdPartyID] ASC,
	[ThirdPartyCategoryID] ASC
)WITH (DROP_EXISTING = ON) ON [PRIMARY]

------------------------------------------------------------------------------------------------------
--	Appraisals_A3
------------------------------------------------------------------------------------------------------
-----------------
--[Appraisals_A3#0]
-----------------

-- Create clustered Index on BusinessUnitID
create  clustered index IDX_Appraisals_A3#0_BusinessUnitID on Appraisals_A3#0 (BusinessUnitID)

-----------------
--[Appraisals_A3#1]
-----------------
-- Create clustered Index on BusinessUnitID
create  clustered index IDX_Appraisals_A3#1_BusinessUnitID on Appraisals_A3#1 (BusinessUnitID)

------------------------------------------------------------------------------------------------------
--	Inventory_A2
------------------------------------------------------------------------------------------------------
-----------------
--[Inventory_A2#0]
-----------------

-- Create clustered Index on BusinessUnitID
create  clustered index IDX_Inventory_A2#0_BusinessUnitID on Inventory_A2#0 (BusinessUnitID)

-----------------
--[Inventory_A2#1]
-----------------

-- Create clustered Index on BusinessUnitID
create  clustered index IDX_Inventory_A2#1_BusinessUnitID on Inventory_A2#1 (BusinessUnitID)

------------------------------------------------------------------------------------------------------
--	Inventory_A3
------------------------------------------------------------------------------------------------------
-----------------
--Inventory_A3#0
-----------------

--Drop non-clustered index and create it as a clustered index

CREATE CLUSTERED INDEX [IDX_Inventory_A3#0] ON [dbo].[Inventory_A3#0] 
(
	[BusinessUnitID] ASC,
	[GroupingDescriptionID] ASC,
	[ModelYear] ASC
)WITH (DROP_EXISTING = ON) ON [PRIMARY]

--Drop Redundant Index
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Inventory_A3#0]') AND name = N'IDX_Inventory_A3#0_BusinessUnit')
DROP INDEX [IDX_Inventory_A3#0_BusinessUnit] ON [dbo].[Inventory_A3#0] WITH ( ONLINE = OFF )

-----------------
--Inventory_A3#1
-----------------

--Drop non-clustered index and create it as a clustered index

CREATE CLUSTERED INDEX [IDX_Inventory_A3#1] ON [dbo].[Inventory_A3#1] 
(
	[BusinessUnitID] ASC,
	[GroupingDescriptionID] ASC,
	[ModelYear] ASC
)WITH (DROP_EXISTING = ON) ON [PRIMARY]

--Drop Redundant Index
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Inventory_A3#1]') AND name = N'IDX_Inventory_A3#1_BusinessUnit')
DROP INDEX [IDX_Inventory_A3#1_BusinessUnit] ON [dbo].[Inventory_A3#1] WITH ( ONLINE = OFF )

------------------------------------------------------------------------------------------------------
--	Inventory_A5
------------------------------------------------------------------------------------------------------
-----------------
--Inventory_A5#0
-----------------

--Drop non-clustered index and create it as a clustered index

CREATE CLUSTERED INDEX [IDX_Inventory_A5#0] ON [dbo].[Inventory_A5#0] 
(
	[BusinessUnitID] ASC,
	[GroupingDescriptionID] ASC,
	[ModelYear] ASC
)WITH (DROP_EXISTING = ON) ON [PRIMARY]

-----------------
--Inventory_A5#1
-----------------

--Drop non-clustered index and create it as a clustered index

CREATE CLUSTERED INDEX [IDX_Inventory_A5#1] ON [dbo].[Inventory_A5#1] 
(
	[BusinessUnitID] ASC,
	[GroupingDescriptionID] ASC,
	[ModelYear] ASC
)WITH (DROP_EXISTING = ON) ON [PRIMARY]

------------------------------------------------------------------------------------------------------
-- Inventory_A7
------------------------------------------------------------------------------------------------------
-----------------
-- [Inventory_A7#0]
-----------------

--Drop non clustered PK

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Inventory_A7#0]') AND name = N'PK_Inventory_A7#0')
ALTER TABLE [dbo].[Inventory_A7#0] DROP CONSTRAINT [PK_Inventory_A7#0]

-- Create clustered PK
ALTER TABLE [dbo].[Inventory_A7#0] ADD  CONSTRAINT [PK_Inventory_A7#0] PRIMARY KEY CLUSTERED 
(
	[InventoryID] ASC
) ON [PRIMARY]

-----------------
-- [Inventory_A7#1
-----------------

--Drop non clustered PK
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Inventory_A7#1]') AND name = N'PK_Inventory_A7#1')
ALTER TABLE [dbo].[Inventory_A7#1] DROP CONSTRAINT [PK_Inventory_A7#1]

-- Create clustered PK
ALTER TABLE [dbo].[Inventory_A7#1] ADD  CONSTRAINT [PK_Inventory_A7#1] PRIMARY KEY CLUSTERED 
(
	[InventoryID] ASC
) ON [PRIMARY]

--------------------------------------------------------------------------------------
-- ZipCode_A1
-----------------------------------------------------------------------------------------
--Drop non-clustered index and create it as a clustered index

CREATE NONCLUSTERED INDEX [IDX_ZipCode_A1_Full] ON [dbo].[ZipCode_A1] 
(
	[ZipCodeID1] ASC,
	[ZipCodeID2] ASC
)WITH (DROP_EXISTING = ON) ON [PRIMARY]

-- Drop redundant non-clustered index

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ZipCode_A1]') AND name = N'IDX_ZipCode_A1_Right')
DROP INDEX [IDX_ZipCode_A1_Right] ON [dbo].[ZipCode_A1] WITH ( ONLINE = OFF )

GO



