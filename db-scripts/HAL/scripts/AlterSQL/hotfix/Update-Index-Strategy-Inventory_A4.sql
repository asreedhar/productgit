DROP INDEX dbo.Inventory_A4#0.IDX_Inventory_A4#0
DROP INDEX dbo.Inventory_A4#1.IDX_Inventory_A4#1

------------------------------------------------------------------------------------
--	There is an edge-case in the loader that allows non-unique values; need to 
--	investigate, as this should not be the case.  Go with non-unique for now
------------------------------------------------------------------------------------

CREATE CLUSTERED INDEX IXC_Inventory_A4#0 ON dbo.Inventory_A4#0(BusinessUnitID, GroupingDescriptionID, ModelYear)
CREATE CLUSTERED INDEX IXC_Inventory_A4#1 ON dbo.Inventory_A4#1(BusinessUnitID, GroupingDescriptionID, ModelYear)