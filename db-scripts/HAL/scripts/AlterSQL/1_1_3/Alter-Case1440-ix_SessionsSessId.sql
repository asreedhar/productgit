/*
**	The table has a clustered index on Id (not SessId).
**	This means that every call to read session data is doing a table scan.  
**	As there is no process to clean out this data over time 
**	(sessions more than a day or two old should probably be deleted), 
**	this call was getting more and more expensive.

**	I added an index to Session.SessId to turn the call into an Index Seek instead of a table scan.
*/

DECLARE @BuildIndex INT
SET @BuildIndex = 1
--********************************************
--Sessions
IF (EXISTS(	SELECT * FROM SYS.Indexes 
		WHERE	NAME = 'IX_SessionsSessId' 
			AND OBJECT_ID = OBJECT_ID('hal.dbo.Sessions')))
	DROP INDEX dbo.Sessions.IX_SessionsSessId
IF (@BuildIndex = 1)
	CREATE INDEX IX_SessionsSessId 
	on dbo.Sessions
	(SessId) 
	--INCLUDE (Included non-indexed columns)
	on [primary] --specify filegroup
--********************************************
