----------------------------------------------------------------------------------------
--
--	TEST THAT THE NEW VIEWS REPLACE THE OLD EXACTLY
--
----------------------------------------------------------------------------------------
--	CREATE PRODUCTION VERSIONS OF THE VIEWS
----------------------------------------------------------------------------------------



CREATE VIEW dbo.GetAppraisals_V1#Prod
(
	BusinessUnitID,
	AppraisalID,
	ThirdPartyID,
	ThirdPartyCategoryID,
	VehicleID,
	MakeModelGroupingID,
	ModelYear,
	Mileage,
	DateCreated,
	AppraisalValue,
	BookoutValue,
	CustomerOffer,
	AppraisalTypeID
)
AS
SELECT
	A.BusinessUnitID,
	A.AppraisalID,
	T.ThirdPartyID,
	B.ThirdPartyCategoryID,
	A.VehicleID,
	V.MakeModelGroupingID,
	V.VehicleYear,
	A.Mileage,
	A.DateCreated,
	AZ.Value,
	B.Value,
	AFO.AppraisalFormOffer,
	A.AppraisalTypeID
FROM
	[IMT].dbo.Appraisals A WITH (NOLOCK)
JOIN 	dbo.BusinessUnit BU ON A.BusinessUnitID = BU.BusinessUnitID	
JOIN	[FLDW].dbo.Appraisal_F AZ ON AZ.AppraisalID = A.AppraisalID AND AZ.BusinessUnitID = A.BusinessUnitID
JOIN	[FLDW].dbo.AppraisalBookout_F B ON B.AppraisalID = A.AppraisalID AND B.BusinessUnitID = A.BusinessUnitID
JOIN	[IMT].dbo.ThirdPartyCategories T ON T.ThirdPartyCategoryID = B.ThirdPartyCategoryID
JOIN	[FLDW].dbo.Vehicle V ON A.BusinessUnitID = V.BusinessUnitID AND A.VehicleID = V.VehicleID
LEFT JOIN [IMT].dbo.AppraisalFormOptions AFO ON AFO.AppraisalID = A.AppraisalID
LEFT JOIN [IMT].dbo.AppraisalActions AA ON A.AppraisalID = AA.AppraisalID
JOIN	dbo.UserContext UC ON UC.UserContextID = 1
WHERE
	A.DateCreated BETWEEN DATEADD(WW,-5,DATEADD(DD,1,UC.RunDate)) AND DATEADD(DD,1,UC.RunDate)
AND	COALESCE(AA.AppraisalActionTypeID,5) IN (4,5)
--AND	((T.ThirdPartyID <> 3) OR (T.ThirdPartyID = 3 AND B.BookoutValueTypeID = 2)) -- kbb retail only

AND	B.BookoutValueTypeID = 2	-- WGH 03/10/2009 Limit to "Final Value" as all books can have multiple values now

AND	NOT EXISTS (
		SELECT
			1
		FROM
			[IMT].dbo.Inventory I WITH (NOLOCK)
			JOIN dbo.BusinessUnit BU ON I.BusinessUnitID = BU.BusinessUnitID			
		WHERE
			A.VehicleID = I.VehicleID
		AND	I.BusinessUnitID = A.BusinessUnitID
		AND	DATEDIFF(DD, [IMT].dbo.ToDate(A.DateCreated), I.InventoryReceivedDate) >= 0
	)


GO



CREATE VIEW dbo.GetAppraisalReviews_AppraisalToNAAAValue#Prod(
	BusinessUnitID,
	AppraisalID,
	Rule1, Rule2, Rule3, Rule4, Rule5, Rule6,
	AppraisalValue,
	CustomerOffer,
	BookValue,
	BookThirdPartyID,
	BookThirdPartyCategoryID,
	NAAAValue,
	NADAValue,
	StockedUnitsModelYear,
	StockedUnitsModel,
	OptimalUnitsModelYear,
	OptimalUnitsModel,
	TargetBusinessUnitID,
	AppraisalDate
)
AS
WITH VicMapping AS (
	SELECT V.VehicleID, COALESCE(V.VIC, VIC.VIC) VIC
	FROM	[IMT].dbo.Vehicle V WITH (NOLOCK)
	LEFT JOIN	(
			SELECT	VINPrefix
			FROM	[VehicleUC].dbo.VINPrefix_VIC
			GROUP
			BY	VINPrefix
			HAVING	COUNT(*) = 1
		) R ON R.VINPrefix = Left(V.VIN,8) + '0' + SUBSTRING(V.VIN, 10, 1)
	LEFT JOIN	[VehicleUC].dbo.VINPrefix_VIC VIC ON VIC.VINPrefix = R.VINPrefix
	WHERE COALESCE(V.VIC, VIC.VIC) IS NOT NULL
)
SELECT DISTINCT
	P.BusinessUnitID,
	S.AppraisalID,
	0 Rule1, 1 Rule2, 0 Rule3, 0 Rule4, 0 Rule5, 0 Rule6,
	S.AppraisalValue,
	S.CustomerOffer,
	NULL BookValue,
	NULL BookThirdPartyID,
	NULL BookThirdPartyCategoryID,
	A.AveSalePrice NAAAValue,
	NULL NADAValue,
	NULL StockedUnitsModelYear,
	NULL StockedUnitsModel,
	NULL OptimalUnitsModelYear,
	NULL OptimalUnitsModel,
	NULL TargetBusinessUnitID,
	S.DateCreated
FROM
	dbo.Appraisals_A1 S
JOIN	VicMapping VIC ON VIC.VehicleID = S.VehicleID
JOIN	dbo.AppraisalReviewPreferences P ON S.BusinessUnitID = P.BusinessUnitID
JOIN	[IMT].dbo.DealerPreference DP ON DP.BusinessUnitID = P.BusinessUnitID
JOIN	[AuctionNet].dbo.AuctionTransaction_A2 A ON
	(
			A.VIC                 = VIC.VIC
		AND	A.AreaID              = DP.AuctionAreaID
		AND	A.PeriodID            = 12
		AND	A.LowMileageRange     = ROUND(S.Mileage,-4)
		AND	A.HighMileageRange    = ROUND(S.Mileage,-4)+9999
	)
JOIN	dbo.UserContext UC ON UC.UserContextID = 1
WHERE
	(A.AveSalePrice - S.AppraisalValue) >= P.ValuationDiscrepancyThresholdRule2
AND	S.AppraisalTypeId = 1 --- nk: FB 2551 - do not include type purchase only trade-in		
AND	S.DateCreated >= DATEADD(DD,-31,UC.RunDate)



GO


CREATE VIEW dbo.GetAppraisalReviews_Understocked#Prod (
	BusinessUnitID,
	AppraisalID,
	Rule1, Rule2, Rule3, Rule4, Rule5, Rule6,
	AppraisalValue,
	CustomerOffer,
	BookValue,
	BookThirdPartyID,
	BookThirdPartyCategoryID,
	NAAAValue,
	NADAValue,
	StockedUnitsModelYear,
	StockedUnitsModel,
	OptimalUnitsModelYear,
	OptimalUnitsModel,
	TargetBusinessUnitID,
	AppraisalDate
)
AS
SELECT DISTINCT
	BU.BusinessUnitID,
	A.AppraisalID,
	0, 0, 0,
	CASE WHEN BU.BusinessUnitID = BU2.BusinessUnitID THEN 1 ELSE 0 END,
	CASE WHEN BU.BusinessUnitID <> BU2.BusinessUnitID THEN 1 ELSE 0 END,
	0,
	A.AppraisalValue,
	A.CustomerOffer,
	NULL BookValue,
	NULL BookThirdPartyID,
	NULL BookThirdPartyCategoryID,
	NULL NAAAValue,
	NULL NADAValue,
	I4.StockedUnitsModelYear,
	I4.StockedUnitsModel,
	I4.OptimalUnitsModelYear,
	I4.OptimalUnitsModel,
	CASE WHEN BU.BusinessUnitID = BU2.BusinessUnitID THEN NULL ELSE BU2.BusinessUnitID END,
	A.DateCreated
FROM
	dbo.Appraisals_A1 A
JOIN	dbo.AppraisalReviewPreferences P ON A.BusinessUnitID = P.BusinessUnitID
JOIN	dbo.BusinessUnit BU on A.BusinessUnitID = BU.BusinessUnitID
JOIN	dbo.ZipCode ZC ON BU.ZipCode = ZC.ZipCode
JOIN	[IMT].dbo.BusinessUnitRelationship BR1 on BU.BusinessUnitID = BR1.BusinessUnitID
JOIN	[IMT].dbo.BusinessUnitRelationship BR2 on BR1.ParentID = BR2.ParentID
JOIN	dbo.BusinessUnit BU2 on BR2.BusinessUnitID = BU2.BusinessUnitID
JOIN	dbo.ZipCode ZC2 ON BU2.ZipCode = ZC2.ZipCode
JOIN	[IMT].dbo.MakeModelGrouping MMG ON A.MakeModelGroupingID = MMG.MakeModelGroupingID
JOIN	[IMT].dbo.tbl_GroupingDescription GD ON MMG.GroupingDescriptionID = GD.GroupingDescriptionID
JOIN	dbo.Inventory_A4 I4 ON I4.BusinessUnitID = BU2.BusinessUnitID AND I4.GroupingDescriptionID = GD.GroupingDescriptionID AND COALESCE(I4.ModelYear,A.ModelYear) = A.ModelYear
LEFT JOIN dbo.ZipCode_A1 ZCD1 ON (ZC.ZipCodeID = ZCD1.ZipCodeID1 AND ZC2.ZipCodeID = ZCD1.ZipCodeID2)
LEFT JOIN dbo.ZipCode_A1 ZCD2 ON (ZC.ZipCodeID = ZCD2.ZipCodeID2 AND ZC2.ZipCodeID = ZCD2.ZipCodeID1)
JOIN	dbo.UserContext UC ON UC.UserContextID = 1
JOIN	[IMT].dbo.DealerRisk R1 ON BU.BusinessUnitID = R1.BusinessUnitID
JOIN	[IMT].dbo.DealerRisk R2 ON BU2.BusinessUnitID = R2.BusinessUnitID
WHERE
	A.DateCreated >= DATEADD(DD,-31,UC.RunDate)
AND	A.AppraisalTypeId = 1 --- nk: FB 2551 - do not include type purchase only trade-in	
AND	I4.CIACategoryID IN (1,2,3)
AND	COALESCE(ZCD1.Distance,ZCD2.Distance,0) < P.MaxDistanceFromDealer
AND	A.Mileage < CASE WHEN BU.BusinessUnitID = BU2.BusinessUnitID THEN R1.HighMileageThreshold ELSE R2.HighMileageThreshold END
AND	CASE WHEN BU.BusinessUnitID = BU2.BusinessUnitID THEN 1 ELSE COALESCE((OptimalUnitsModelYear - StockedUnitsModelYear), (OptimalUnitsModel - StockedUnitsModel), 0) END > 0

go

----------------------------------------------------------------------------------------
--	MAKE SURE FLDW IS UP TO DATE
----------------------------------------------------------------------------------------
EXEC FLDW.dbo.LoadWarehouseTables @TableTypeID = 3, @TableID = 5

----------------------------------------------------------------------------------------
--	LOAD TEMP VIEWS
----------------------------------------------------------------------------------------

SELECT	* 
INTO	#GetAppraisals_V1
FROM	dbo.GetAppraisals_V1 

SELECT	* 
INTO	#GetAppraisals_V1#Prod
FROM	dbo.GetAppraisals_V1#Prod

SELECT	* 
INTO	#AppraisalReviews_AppraisalToNAAAValue
FROM	dbo.GetAppraisalReviews_AppraisalToNAAAValue


SELECT	* 
INTO	#AppraisalReviews_AppraisalToNAAAValue#Prod
FROM	dbo.GetAppraisalReviews_AppraisalToNAAAValue#Prod

SELECT	* 
INTO	#AppraisalReviews_Understocked
FROM	dbo.GetAppraisalReviews_Understocked

SELECT	* 
INTO	#AppraisalReviews_Understocked#Prod
FROM	dbo.GetAppraisalReviews_Understocked#Prod

----------------------------------------------------------------------------------------
--	COMPARE
----------------------------------------------------------------------------------------

SELECT	*
FROM	#GetAppraisals_V1 B 
	FULL OUTER JOIN #GetAppraisals_V1#Prod P ON B.BusinessUnitID = P.BusinessUnitID and B.AppraisalID = P.AppraisalID AND B.ThirdPartyCategoryID = P.ThirdPartyCategoryID
	
WHERE	CHECKSUM(b.BusinessUnitID,
        b.AppraisalID,
        b.ThirdPartyID,
        b.ThirdPartyCategoryID,
        b.VehicleID,
        b.MakeModelGroupingID,
        b.ModelYear,
        b.Mileage,
        b.AppraisalValue,
        b.BookoutValue,
        b.CustomerOffer,
        b.AppraisalTypeID,
        b.DateCreated)	
        <> CHECKSUM(P.BusinessUnitID,
        P.AppraisalID,
        P.ThirdPartyID,
        P.ThirdPartyCategoryID,
        P.VehicleID,
        P.MakeModelGroupingID,
        P.ModelYear,
        P.Mileage,
        P.AppraisalValue,
        P.BookoutValue,
        P.CustomerOffer,
        P.AppraisalTypeID,
        CAST(P.DateCreated AS SMALLDATETIME))
	


SELECT	*
FROM	#AppraisalReviews_AppraisalToNAAAValue B
	FULL OUTER JOIN #AppraisalReviews_AppraisalToNAAAValue#Prod P ON B.BusinessUnitID = P.BusinessUnitID and B.AppraisalID = P.AppraisalID
	
WHERE	CHECKSUM(b.BusinessUnitID,
        b.AppraisalID,
        b.Rule1,
        b.Rule2,
        b.Rule3,
        b.Rule4,
        b.Rule5,
        b.Rule6,
        b.AppraisalValue,
        b.CustomerOffer,
        b.BookValue,
        b.BookThirdPartyID,
        b.BookThirdPartyCategoryID,
        b.NAAAValue,
        b.NADAValue,
        b.StockedUnitsModelYear,
        b.StockedUnitsModel,
        b.OptimalUnitsModelYear,
        b.OptimalUnitsModel,
        b.TargetBusinessUnitID,
        B.AppraisalDate
        )  <>
        
        CHECKSUM(P.BusinessUnitID,
        P.AppraisalID,
        P.Rule1,
        P.Rule2,
        P.Rule3,
        P.Rule4,
        P.Rule5,
        P.Rule6,
        CAST(P.AppraisalValue AS INT),
        P.CustomerOffer,
        P.BookValue,
        P.BookThirdPartyID,
        P.BookThirdPartyCategoryID,
        P.NAAAValue,
        P.NADAValue,
        P.StockedUnitsModelYear,
        P.StockedUnitsModel,
        P.OptimalUnitsModelYear,
        P.OptimalUnitsModel,
        P.TargetBusinessUnitID,
        CAST(P.AppraisalDate AS SMALLDATETIME))
----------------------------------------------------------------------------------------
--	ONE ROW -- NEW PROCESS FIXES A BUG IN THE OLD CODE THAT ALLOWED AN EMPTY STRING
--	FROM IMT.dbo.Vehicle.VIC TO HAVE PRECEDENCE OVER A DECODED VIC
----------------------------------------------------------------------------------------


SELECT	COUNT(*)
FROM	#AppraisalReviews_Understocked B
	FULL OUTER JOIN #AppraisalReviews_Understocked#Prod P ON B.BusinessUnitID = P.BusinessUnitID and B.AppraisalID = P.AppraisalID AND COALESCE(B.TargetBusinessUnitID,0) = COALESCE(P.TargetBusinessUnitID,0)
WHERE	CHECKSUM(b.BusinessUnitID,
        b.AppraisalID,
        b.Rule1,
        b.Rule2,
        b.Rule3,
        b.Rule4,
        b.Rule5,
        b.Rule6,
        b.AppraisalValue,
        b.CustomerOffer,
        b.BookValue,
        b.BookThirdPartyID,
        b.BookThirdPartyCategoryID,
        b.NAAAValue,
        b.NADAValue,
        b.StockedUnitsModelYear,
        b.StockedUnitsModel,
        b.OptimalUnitsModelYear,
        b.OptimalUnitsModel,
        b.TargetBusinessUnitID,
        B.AppraisalDate
        )
	<>
	CHECKSUM(P.BusinessUnitID,
        P.AppraisalID,
        P.Rule1,
        P.Rule2,
        P.Rule3,
        P.Rule4,
        P.Rule5,
        P.Rule6,
        CAST(P.AppraisalValue AS INT),
        P.CustomerOffer,
        P.BookValue,
        P.BookThirdPartyID,
        P.BookThirdPartyCategoryID,
        P.NAAAValue,
        P.NADAValue,
        P.StockedUnitsModelYear,
        P.StockedUnitsModel,
        P.OptimalUnitsModelYear,
        P.OptimalUnitsModel,
        P.TargetBusinessUnitID,
        CAST(P.AppraisalDate AS SMALLDATETIME)
        )




