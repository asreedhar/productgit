IF NOT EXISTS (	SELECT	1
		FROM	sys.database_principals
		WHERE	type_desc = 'DATABASE_ROLE'
			AND NAME = 'DataManagement'
		)

	CREATE ROLE [DataManagement] AUTHORIZATION [dbo]

GO

GRANT EXEC ON dbo.ConfigureDealershipForMAX TO DataManagement
GO

GRANT EXEC ON dbo.GuidebookCorrectionForMax TO DataManagement
GO