
--
-- The views which build the entries for appraisal review. One rule, one view is the rule folks. SBW
--

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAppraisalReviews_AppraisalToBookValue]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetAppraisalReviews_AppraisalToBookValue]
GO

CREATE VIEW dbo.GetAppraisalReviews_AppraisalToBookValue(
	BusinessUnitID,
	AppraisalID,
	Rule1, Rule2, Rule3, Rule4, Rule5, Rule6,
	AppraisalValue,
	CustomerOffer,
	BookValue,
	BookThirdPartyID,
	BookThirdPartyCategoryID,
	NAAAValue,
	NADAValue,
	StockedUnitsModelYear,
	StockedUnitsModel,
	OptimalUnitsModelYear,
	OptimalUnitsModel,
	TargetBusinessUnitID,
	AppraisalDate
)
AS
SELECT
	S.BusinessUnitID,
	S.AppraisalID,
	1, 0, 0, 0, 0, 0,
	S.AppraisalValue,
	S.CustomerOffer,
	S.BookoutValue,
	S.ThirdPartyID,
	S.ThirdPartyCategoryID,
	NULL NAAAValue,
	NULL NADAValue,
	NULL StockedUnitsModelYear,
	NULL StockedUnitsModel,
	NULL OptimalUnitsModelYear,
	NULL OptimalUnitsModel,
	NULL TargetBusinessUnitID,
	S.DateCreated
FROM
	dbo.AppraisalReviewBookoutPreferences PM
JOIN	dbo.AppraisalReviewPreferences P ON PM.BusinessUnitID = P.BusinessUnitID
JOIN	dbo.Appraisals_A1 S ON (PM.BusinessUnitID = S.BusinessUnitID AND PM.ThirdPartyID = S.ThirdPartyID and PM.ThirdPartyCategoryID = S.ThirdPartyCategoryID)
                            AND S.AppraisalTypeId = 1 --- nk: FB 2551 - do not include type purchase only trade-in
JOIN	dbo.VehicleLine VL ON S.MakeModelGroupingID = VL.MakeModelGroupingID AND VL.VehicleMakeID = PM.VehicleMakeID
JOIN	dbo.UserContext UC ON UC.UserContextID = 1
WHERE
	S.DateCreated >= DATEADD(DD,-31,UC.RunDate)
AND	(S.BookoutValue - S.AppraisalValue) >= P.ValuationDiscrepancyThresholdRule1
GO
