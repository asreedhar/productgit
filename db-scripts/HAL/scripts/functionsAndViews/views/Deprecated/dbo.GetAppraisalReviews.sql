
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAppraisalReviews]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetAppraisalReviews]
GO

CREATE VIEW dbo.GetAppraisalReviews (
	BusinessUnitID,
	AppraisalID,
	Rule1,
	Rule2,
	Rule3,
	Rule4,
	Rule5,
	Rule6,
	AppraisalValue,
	CustomerOffer,
	BookValue,
	BookThirdPartyID,
	BookThirdPartyCategoryID,
	NAAAValue,
	NADAValue,
	StockedUnitsModelYear,
	StockedUnitsModel,
	OptimalUnitsModelYear,
	OptimalUnitsModel,
	AppraisalDate
)
AS
SELECT
	BusinessUnitID,
	AppraisalID,
	CASE WHEN SUM(CAST(Rule1 AS TINYINT)) > 0 THEN 1 ELSE 0 END,
	CASE WHEN SUM(CAST(Rule2 AS TINYINT)) > 0 THEN 1 ELSE 0 END,
	CASE WHEN SUM(CAST(Rule3 AS TINYINT)) > 0 THEN 1 ELSE 0 END,
	CASE WHEN SUM(CAST(Rule4 AS TINYINT)) > 0 THEN 1 ELSE 0 END,
	CASE WHEN SUM(CAST(Rule5 AS TINYINT)) > 0 THEN 1 ELSE 0 END,
	CASE WHEN SUM(CAST(Rule6 AS TINYINT)) > 0 THEN 1 ELSE 0 END,
	MAX(COALESCE(AppraisalValue,0)),
	MAX(COALESCE(CustomerOffer,0)),
	MAX(COALESCE(BookValue,0)),
	MAX(BookThirdPartyID),
	MAX(BookThirdPartyCategoryID),
	MAX(COALESCE(NAAAValue,0)),
	MAX(COALESCE(NADAValue,0)),
	MAX(COALESCE(StockedUnitsModelYear,0)),
	MAX(COALESCE(StockedUnitsModel,0)),
	MAX(COALESCE(OptimalUnitsModelYear,0)),
	MAX(COALESCE(OptimalUnitsModel,0)),
	MAX(COALESCE(AppraisalDate,getDate()))
FROM
(
	SELECT
		*
	FROM
		dbo.GetAppraisalReviews_AppraisalToBookValue
UNION ALL
	SELECT
		*
	FROM
		dbo.GetAppraisalReviews_AppraisalToNAAAValue
UNION ALL
	SELECT
		*
	FROM
		dbo.GetAppraisalReviews_BookToNADALoanValue
UNION ALL
	SELECT
		*
	FROM
		dbo.GetAppraisalReviews_Understocked
UNION ALL
	SELECT
		*
	FROM
		dbo.GetAppraisalReviews_LowOffer
) TMP
GROUP BY
	BusinessUnitID,
	AppraisalID
GO

-- SELECT * FROM dbo.GetAppraisalReviews WHERE BusinessUnitID = 100147
