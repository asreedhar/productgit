
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAppraisalReviewBusinessUnits]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetAppraisalReviewBusinessUnits]
GO

CREATE VIEW dbo.GetAppraisalReviewBusinessUnits (
	AppraisalID,
	BusinessUnitID,
	TargetBusinessUnitID
)
AS
SELECT
	AppraisalID,
	BusinessUnitID,
	TargetBusinessUnitID
FROM
	dbo.GetAppraisalReviews_Understocked
WHERE
	TargetBusinessUnitID IS NOT NULL
GO
