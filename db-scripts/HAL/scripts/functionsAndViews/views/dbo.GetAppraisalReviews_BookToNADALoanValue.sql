

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAppraisalReviews_BookToNADALoanValue]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetAppraisalReviews_BookToNADALoanValue]
GO

CREATE VIEW dbo.GetAppraisalReviews_BookToNADALoanValue(
	BusinessUnitID,
	AppraisalID,
	Rule1, Rule2, Rule3, Rule4, Rule5, Rule6,
	AppraisalValue,
	CustomerOffer,
	BookValue,
	BookThirdPartyID,
	BookThirdPartyCategoryID,
	NAAAValue,
	NADAValue,
	StockedUnitsModelYear,
	StockedUnitsModel,
	OptimalUnitsModelYear,
	OptimalUnitsModel,
	TargetBusinessUnitID,
	AppraisalDate
)
AS
SELECT
	P.BusinessUnitID,
	Book.AppraisalID,
	0, 0, 1, 0, 0, 0,
	Book.AppraisalValue,
	Book.CustomerOffer,
	Book.BookoutValue,
	NULL BookThirdPartyID,
	NULL BookThirdPartyCategoryID,
	NULL NAAAValue,
	NADA.BookoutValue,
	NULL StockedUnitsModelYear,
	NULL StockedUnitsModel,
	NULL OptimalUnitsModelYear,
	NULL OptimalUnitsModel,
	NULL TargetBusinessUnitID,
	NADA.DateCreated
FROM
	dbo.Appraisals_A1 NADA
JOIN	dbo.Appraisals_A1 Book ON NADA.AppraisalID = Book.AppraisalID
JOIN	[IMT].dbo.DealerPreference DP ON DP.BusinessUnitID = NADA.BusinessUnitID
JOIN	dbo.AppraisalReviewPreferences P ON NADA.BusinessUnitID = P.BusinessUnitID
JOIN	dbo.UserContext UC ON UC.UserContextID = 1
WHERE
	NADA.DateCreated >= DATEADD(DD,-31,UC.RunDate)
AND	NADA.AppraisalTypeId = 1 --- nk: FB 2551 - do not include type purchase only trade-in
AND	NADA.ThirdPartyID = 2
AND	NADA.ThirdPartyCategoryID = 3
AND	Book.ThirdPartyID <> 2
AND	Book.ThirdPartyCategoryID = CASE WHEN DP.GuideBookID = Book.ThirdPartyID THEN DP.BookOutPreferenceId ELSE DP.GuideBook2BookOutPreferenceId END
AND	(NADA.BookoutValue - Book.BookoutValue) >= P.ValuationDiscrepancyThresholdRule3
GO

-- SELECT * FROM GetAppraisalReviews_BookToNADALoanValue WHERE BusinessUnitID = 100215 -- < 1s
