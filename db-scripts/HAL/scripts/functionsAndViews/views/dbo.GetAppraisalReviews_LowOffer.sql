
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAppraisalReviews_LowOffer]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetAppraisalReviews_LowOffer]
GO

CREATE VIEW dbo.GetAppraisalReviews_LowOffer(
	BusinessUnitID,
	AppraisalID,
	Rule1, Rule2, Rule3, Rule4, Rule5, Rule6,
	AppraisalValue,
	CustomerOffer,
	BookValue,
	BookThirdPartyID,
	BookThirdPartyCategoryID,
	NAAAValue,
	NADAValue,
	StockedUnitsModelYear,
	StockedUnitsModel,
	OptimalUnitsModelYear,
	OptimalUnitsModel,
	TargetBusinessUnitID,
	AppraisalDate
)
AS
SELECT DISTINCT
	A.BusinessUnitID,
	A.AppraisalID,
	0, 0, 0, 0, 0, 1,
	A.AppraisalValue,
	A.CustomerOffer,
	NULL BookValue,
	NULL BookThirdPartyID,
	NULL BookThirdPartyCategoryID,
	NULL NAAAValue,
	NULL NADAValue,
	NULL StockedUnitsModelYear,
	NULL StockedUnitsModel,
	NULL OptimalUnitsModelYear,
	NULL OptimalUnitsModel,
	NULL TargetBusinessUnitID,
	A.DateCreated
FROM
	dbo.Appraisals_A1 A
JOIN	dbo.AppraisalReviewPreferences P ON A.BusinessUnitID = P.BusinessUnitID
JOIN	dbo.UserContext UC ON UC.UserContextID = 1
WHERE
	A.DateCreated >= DATEADD(DD,-31,UC.RunDate)
AND	A.AppraisalTypeId = 1 --- nk: FB 2551 - do not include type purchase only trade-in	
AND	(A.AppraisalValue - COALESCE(NULLIF(A.CustomerOffer,0),A.AppraisalValue)) >= P.ValuationDiscrepancyThresholdRule6
GO

-- SELECT * FROM dbo.GetAppraisalReviews_LowOffer WHERE BusinessUnitID = 100215 -- < 1s
