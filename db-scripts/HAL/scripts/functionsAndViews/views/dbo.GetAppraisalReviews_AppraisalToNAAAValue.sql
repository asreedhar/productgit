
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAppraisalReviews_AppraisalToNAAAValue]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetAppraisalReviews_AppraisalToNAAAValue]
GO

CREATE VIEW dbo.GetAppraisalReviews_AppraisalToNAAAValue(
	BusinessUnitID,
	AppraisalID,
	Rule1, Rule2, Rule3, Rule4, Rule5, Rule6,
	AppraisalValue,
	CustomerOffer,
	BookValue,
	BookThirdPartyID,
	BookThirdPartyCategoryID,
	NAAAValue,
	NADAValue,
	StockedUnitsModelYear,
	StockedUnitsModel,
	OptimalUnitsModelYear,
	OptimalUnitsModel,
	TargetBusinessUnitID,
	AppraisalDate
)
AS

SELECT	P.BusinessUnitID,
	A.AppraisalID,
	0 Rule1, 1 Rule2, 0 Rule3, 0 Rule4, 0 Rule5, 0 Rule6,
	A.Value,
	A.AppraisalFormOffer,
	NULL BookValue,
	NULL BookThirdPartyID,
	NULL BookThirdPartyCategoryID,
	COALESCE(AT1.AveSalePrice,AT2.AveSalePrice) NAAAValue,
	NULL NADAValue,
	NULL StockedUnitsModelYear,
	NULL StockedUnitsModel,
	NULL OptimalUnitsModelYear,
	NULL OptimalUnitsModel,
	NULL TargetBusinessUnitID,
	A.DateCreated

FROM	[FLDW].dbo.Appraisal_F A
	
	INNER JOIN [IMT].dbo.Vehicle V WITH (NOLOCK) ON A.VehicleID = V.VehicleID
	INNER JOIN dbo.AppraisalReviewPreferences P ON A.BusinessUnitID = P.BusinessUnitID
	INNER JOIN [IMT].dbo.DealerPreference DP ON DP.BusinessUnitID = P.BusinessUnitID		
	INNER JOIN dbo.UserContext UC ON UC.UserContextID = 1
	LEFT JOIN (	 SELECT	VINPrefix, MAX(VIC) AS VIC
			 FROM	[VehicleUC].dbo.VINPrefix_VIC 
			 GROUP
			 BY	VINPrefix
			 HAVING	COUNT(DISTINCT VIC) = 1
			) VIC ON VIC.VINPrefix = Left(V.VIN,8) + '0' + SUBSTRING(V.VIN, 10, 1)

	LEFT JOIN [AuctionNet].dbo.AuctionTransaction_A2 AT1 ON AT1.VIC = V.VIC
								AND AT1.AreaID = DP.AuctionAreaID
								AND AT1.PeriodID = 12
								AND AT1.LowMileageRange = ROUND(A.Mileage,-4)
								AND AT1.HighMileageRange = ROUND(A.Mileage,-4)+9999

	LEFT JOIN [AuctionNet].dbo.AuctionTransaction_A2 AT2 ON AT2.VIC = VIC.VIC
								AND AT2.AreaID = DP.AuctionAreaID
								AND AT2.PeriodID = 12
								AND AT2.LowMileageRange = ROUND(A.Mileage,-4)
								AND AT2.HighMileageRange = ROUND(A.Mileage,-4)+9999
					
				
WHERE	A.AppraisalTypeId = 1 --- nk: FB 2551 - do not include type purchase only trade-in		
	AND A.DateCreated >= DATEADD(DD,-31,UC.RunDate)
	AND (COALESCE(AT1.AveSalePrice,AT2.AveSalePrice) - A.Value) >= P.ValuationDiscrepancyThresholdRule2
	AND (AT1.VIC IS NOT NULL OR AT2.VIC IS NOT NULL)	-- make sure we have a value
	AND EXISTS (SELECT 1 FROM Appraisals_A1 A1 WHERE A1.AppraisalTypeId = 1 AND A.AppraisalID = A1.AppraisalID)	-- need to align the filters with Appraisals_A1

GO