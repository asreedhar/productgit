
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[GetAppraisalReviews_Understocked]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[GetAppraisalReviews_Understocked]
GO

CREATE VIEW dbo.GetAppraisalReviews_Understocked (
	BusinessUnitID,
	AppraisalID,
	Rule1, Rule2, Rule3, Rule4, Rule5, Rule6,
	AppraisalValue,
	CustomerOffer,
	BookValue,
	BookThirdPartyID,
	BookThirdPartyCategoryID,
	NAAAValue,
	NADAValue,
	StockedUnitsModelYear,
	StockedUnitsModel,
	OptimalUnitsModelYear,
	OptimalUnitsModel,
	TargetBusinessUnitID,
	AppraisalDate
)
AS
SELECT	DISTINCT
        BA.BusinessUnitID,
	A.AppraisalID,
	0 Rule1, 0 Rule2, 0 Rule3,
	CASE WHEN BA.BusinessUnitID = BB.BusinessUnitID THEN 1 ELSE 0 END Rule4,
	CASE WHEN BA.BusinessUnitID <> BB.BusinessUnitID THEN 1 ELSE 0 END Rule5,
	0 Rule6,
	A.AppraisalValue,
	A.CustomerOffer,
	NULL BookValue,
	NULL BookThirdPartyID,
	NULL BookThirdPartyCategoryID,
	NULL NAAAValue,
	NULL NADAValue,
	I4.StockedUnitsModelYear,
	I4.StockedUnitsModel,
	I4.OptimalUnitsModelYear,
	I4.OptimalUnitsModel,
	CASE WHEN BA.BusinessUnitID = BB.BusinessUnitID THEN NULL ELSE BB.BusinessUnitID END TargetBusinessUnitID,
	A.DateCreated AppraisalDate

FROM	dbo.Appraisals_A1 A
        -- siblings & self
        JOIN    [IMT].dbo.BusinessUnit BA ON BA.BusinessUnitID = A.BusinessUnitID
        JOIN    [IMT].dbo.BusinessUnitRelationship UP ON UP.BusinessUnitID = BA.BusinessUnitID
        JOIN    [IMT].dbo.BusinessUnitRelationship DN ON DN.ParentID = UP.ParentID
        JOIN    [IMT].dbo.BusinessUnit BB ON BB.BusinessUnitID = DN.BusinessUnitID
        JOIN    dbo.ZipCode BZ ON BZ.ZipCode = BB.ZipCode
        -- stocking
	INNER JOIN [IMT].dbo.MakeModelGrouping MMG ON A.MakeModelGroupingID = MMG.MakeModelGroupingID
	INNER JOIN dbo.Inventory_A4 I4 ON I4.BusinessUnitID = BB.BusinessUnitID AND I4.GroupingDescriptionID = MMG.GroupingDescriptionID AND COALESCE(I4.ModelYear,A.ModelYear) = A.ModelYear
	-- preferences
	INNER JOIN dbo.AppraisalReviewPreferences P ON A.BusinessUnitID = P.BusinessUnitID
	INNER JOIN [IMT].dbo.DealerRisk R1 ON BA.BusinessUnitID = R1.BusinessUnitID
	INNER JOIN [IMT].dbo.DealerRisk R2 ON BB.BusinessUnitID = R2.BusinessUnitID
	-- distance
	CROSS APPLY dbo.GetDistanceBucket(P.MaxDistanceFromDealer) DB
	-- date
	CROSS APPLY dbo.GetRunDate(1) UC

WHERE	A.DateCreated >= DATEADD(DD,-31,UC.RunDate)
	AND	A.AppraisalTypeId = 1 --- nk: FB 2551 - do not include type purchase only trade-in
	AND	I4.CIACategoryID IN (1,2,3)
	AND	A.Mileage < CASE WHEN BA.BusinessUnitID = BB.BusinessUnitID THEN R1.HighMileageThreshold ELSE R2.HighMileageThreshold END
	AND	CASE WHEN BA.BusinessUnitID = BB.BusinessUnitID THEN 1 ELSE COALESCE((OptimalUnitsModelYear - StockedUnitsModelYear), (OptimalUnitsModel - StockedUnitsModel), 0) END > 0
        AND EXISTS (
                SELECT  1
                FROM    Market.Pricing.DistanceBucketBoundingRectangles BR
                WHERE   BR.ZipCode = BA.ZipCode
                AND     BR.DistanceBucketID = DB.DistanceBucketID
                AND     BZ.Lat BETWEEN BR.BottomRightLatitude AND BR.TopLeftLatitude
		AND     BZ.Long BETWEEN BR.TopLeftLongitude AND BR.BottomRightLongitude
        )       -- enforce max distance from dealer
GO
