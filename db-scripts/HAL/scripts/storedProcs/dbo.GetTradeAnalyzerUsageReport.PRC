USE [HAL]
GO

/****** Object:  StoredProcedure [dbo].[GetTradeAnalyzerUsageReport]    Script Date: 07/25/2014 15:34:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTradeAnalyzerUsageReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetTradeAnalyzerUsageReport]
GO

USE [HAL]
GO

/****** Object:  StoredProcedure [dbo].[GetTradeAnalyzerUsageReport]    Script Date: 07/25/2014 15:34:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Dhiresh Mehta
-- Create date: 2014-07-25
-- Last Modified By : Dhiresh Mehta
-- Last Modified Date : 2014-07-29
-- Description:	This stored procedure returns the Data for Trade Analyzer Usage %
-- =============================================


CREATE PROCEDURE [dbo].[GetTradeAnalyzerUsageReport]
	@SelectedDealerId VARCHAR(1000)  
    
AS
BEGIN
	
	DECLARE 
	@LocalSelectedDealerId VARCHAR(1000)   
	
	SET @LocalSelectedDealerId = @SelectedDealerId
	
	
	DECLARE  @TempTable TABLE (GROUPRANK INT, BusinessUnitId INT,BusinessUnit VARCHAR(MAX),measure_numerator INT,NotAnalyzed INT, measure_denominator INT ,Analyzed FLOAT)	
	
	INSERT INTO @TempTable
	EXEC Reports.[dbo].[Fetch#TradeAnalyzerUsageReportByDealer] @Period=1,@SelectedDealerID=@LocalSelectedDealerId,@TradeInType=0,@IsSummary=1
	
	/*SELECT TOP 1 ROUND(CAST (measure_numerator AS FLOAT)/CAST (measure_denominator AS FLOAT),2) as measure,measure_numerator,measure_denominator FROM @TempTable where BusinessUnitID >0*/
	
	SELECT TOP 1 
	CASE WHEN measure_denominator  > 0 
		THEN ROUND(CAST (measure_numerator AS FLOAT)/CAST(measure_denominator AS FLOAT),2) 
		ELSE 0
	END AS measure,
	measure_numerator,
	measure_denominator
	FROM @TempTable where BusinessUnitID >0
	
	
	
END





GO


