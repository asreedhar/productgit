 IF objectproperty(object_id('dbo.PurgeInactiveSessions'), 'isProcedure') = 1
    DROP PROCEDURE dbo.PurgeInactiveSessions

GO
/******************************************************************************
**
**  Procedure: PurgeInactiveSessions
**  Description: Delete sessions that have not been updated in more than @Days
**        
**
**  Return values:  none
** 
**  Input parameters:   @Days INT	Number of days history to retain, default is 7
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  2008.07.03  ffoiii    Procedure created.
**
*******************************************************************************/
CREATE PROCEDURE dbo.PurgeInactiveSessions
@Days INT = 7

AS

--DEFAULT ENVIRONMENT AND VARIABLE SETTINGS
SET NOCOUNT ON
SET ANSI_NULLS ON 
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET ANSI_WARNINGS OFF

DECLARE @sErrMsg VARCHAR(255),
	@bLocalTran bit,
	@sprocname sysname,
	@bTrue bit,
	@bFalse bit
SELECT	@sProcName = object_name(@@procid),
	@bTrue = 1,
	@bFalse = 0,
	@sErrMsg = ''
SET	@bLocalTran = @bFalse

--if the procedure contains any IUD statement, there should be an explicit transaction
--BEGIN TRAN @procname
--SET @bLocalTran = @bTrue

--END DEFAULT ENVIRONMENT AND VARIABLE SETTINGS



--delete any session that has not been updated in seven days
DELETE dbo.Sessions
WHERE updated_at < DATEADD(dd,-1 * @Days,GETDATE()) 


--all commands successful
IF(@bLocalTran = @bTrue)
	BEGIN
		COMMIT TRAN @sProcName
	END

--exit the procedure
RETURN 0

--Error encountered
Failed:
IF (@bLocalTran = @bTrue)
	BEGIN
		ROLLBACK TRAN @sProcName
	END
IF (@sErrMsg = '')
	BEGIN
		SET @sErrMsg = 'Unspecified Error.'
	END
EXEC DBASTAT..Log_Event @Log_Code = 'E', @sp_Name = @sProcName, @Log_Text = @sErrMsg

RETURN -2
 