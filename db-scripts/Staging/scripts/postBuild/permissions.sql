
--
-- These will normally be placed in individual object creation files.
-- Since these are tables and do not get recreated, they need to be added here.
--
GRANT SELECT ON [AULtec].[Vehicle] TO [VehicleReader]
GRANT SELECT ON [CDMData].[Vehicle] TO [VehicleReader]
GRANT SELECT ON [DealerPeak].[Vehicle] TO [VehicleReader]
GRANT SELECT ON [eBizAutos].[Vehicle] TO [VehicleReader]
--GRANT EXEC ON [GetAuto].[GetImageURLs] TO [VehicleReader]
GRANT SELECT ON [GetAuto].[SystemWarranties] TO [VehicleReader]
GRANT SELECT ON [GetAuto].[Vehicle] TO [VehicleReader]
GRANT SELECT ON [GetAuto].[LotData] TO [VehicleReader]
GRANT SELECT ON [HomeNet].[Vehicle] TO [VehicleReader]

GO