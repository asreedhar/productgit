IF NOT EXISTS ( SELECT  1
                FROM    sys.database_principals
                WHERE   type_desc = 'SQL_USER'
                        AND NAME = 'StagingReader' )
	AND EXISTS (	SELECT 1
			FROM sys.server_principals
                	WHERE   type_desc = 'SQL_LOGIN'
                        	AND NAME = 'StagingReader' ) BEGIN
                        	            
        CREATE USER [StagingReader] FOR LOGIN [StagingReader]

	EXEC sp_addrolemember N'VehicleReader', N'StagingReader'
	
	
	GRANT SELECT ON Chrome.Options#Stg TO StagingReader
	GRANT SELECT ON Chrome.BodyStyles#Stg TO StagingReader
	GRANT SELECT ON Chrome.Categories#Stg TO StagingReader
	GRANT SELECT ON Chrome.CategoryHeaders#Stg TO StagingReader
	GRANT SELECT ON Chrome.CITypes#Stg TO StagingReader
	GRANT SELECT ON Chrome.Colors#Stg TO StagingReader
	GRANT SELECT ON Chrome.ConsInfo#Stg TO StagingReader
	GRANT SELECT ON Chrome.Divisions#Stg TO StagingReader
	GRANT SELECT ON Chrome.Manufacturers#Stg TO StagingReader
	GRANT SELECT ON Chrome.MktClass#Stg TO StagingReader
	GRANT SELECT ON Chrome.Models#Stg TO StagingReader
	GRANT SELECT ON Chrome.OptHeaders#Stg TO StagingReader
	GRANT SELECT ON Chrome.OptKinds#Stg TO StagingReader
	GRANT SELECT ON Chrome.Prices#Stg TO StagingReader
	GRANT SELECT ON Chrome.Standards#Stg TO StagingReader
	GRANT SELECT ON Chrome.StdHeaders#Stg TO StagingReader
	GRANT SELECT ON Chrome.StyleCats#Stg TO StagingReader
	GRANT SELECT ON Chrome.Styles#Stg TO StagingReader
	GRANT SELECT ON Chrome.Subdivisions#Stg TO StagingReader
	GRANT SELECT ON Chrome.TechSpecs#Stg TO StagingReader
	GRANT SELECT ON Chrome.TechTitleHeader#Stg TO StagingReader
	GRANT SELECT ON Chrome.TechTitles#Stg TO StagingReader
	GRANT SELECT ON Chrome.Category#Stg TO StagingReader
	GRANT SELECT ON Chrome.StyleGenericEquipment#Stg TO StagingReader
	GRANT SELECT ON Chrome.StyleWheelBase#Stg TO StagingReader
	GRANT SELECT ON Chrome.VINEquipment#Stg TO StagingReader
	GRANT SELECT ON Chrome.VINPattern#Stg TO StagingReader
	GRANT SELECT ON Chrome.VINPatternStyleMapping#Stg TO StagingReader
	GRANT SELECT ON Chrome.YearMakeModelStyle#Stg TO StagingReader
	
	
	
	--RAWS
	GRANT SELECT ON Staging.Chrome.Options#Raw TO StagingReader
	GRANT SELECT ON Chrome.BodyStyles#Raw TO StagingReader
	GRANT SELECT ON Chrome.Categories#Raw TO StagingReader
	GRANT SELECT ON Chrome.CategoryHeaders#Raw TO StagingReader
	GRANT SELECT ON Chrome.CITypes#Raw TO StagingReader
	GRANT SELECT ON Chrome.Colors#Raw TO StagingReader
	GRANT SELECT ON Chrome.ConsInfo#Raw TO StagingReader
	GRANT SELECT ON Chrome.Divisions#Raw TO StagingReader
	GRANT SELECT ON Chrome.Manufacturers#Raw TO StagingReader
	GRANT SELECT ON Chrome.MktClass#Raw TO StagingReader
	GRANT SELECT ON Chrome.Models#Raw TO StagingReader
	GRANT SELECT ON Chrome.OptHeaders#Raw TO StagingReader
	GRANT SELECT ON Chrome.OptKinds#Raw TO StagingReader
	GRANT SELECT ON Chrome.Prices#Raw TO StagingReader
	GRANT SELECT ON Chrome.Standards#Raw TO StagingReader
	GRANT SELECT ON Chrome.StdHeaders#Raw TO StagingReader
	GRANT SELECT ON Chrome.StyleCats#Raw TO StagingReader
	GRANT SELECT ON Chrome.Styles#Raw TO StagingReader
	GRANT SELECT ON Chrome.Subdivisions#Raw TO StagingReader
	GRANT SELECT ON Chrome.TechSpecs#Raw TO StagingReader
	GRANT SELECT ON Chrome.TechTitleHeader#Raw TO StagingReader
	GRANT SELECT ON Chrome.TechTitles#Raw TO StagingReader
	GRANT SELECT ON Chrome.Category#Raw TO StagingReader
	GRANT SELECT ON Chrome.StyleGenericEquipment#Raw TO StagingReader
	GRANT SELECT ON Chrome.StyleWheelBase#Raw TO StagingReader
	GRANT SELECT ON Chrome.VINEquipment#Raw TO StagingReader
	GRANT SELECT ON Chrome.VINPattern#Raw TO StagingReader
	GRANT SELECT ON Chrome.VINPatternStyleMapping#Raw TO StagingReader
	GRANT SELECT ON Chrome.YearMakeModelStyle#Raw TO StagingReader

END

