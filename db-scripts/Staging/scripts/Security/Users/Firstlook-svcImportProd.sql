IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'FIRSTLOOK\svcImportProd') BEGIN 

	IF NOT EXISTS(SELECT 1 FROM sys.database_principals DP WHERE name = 'FIRSTLOOK\svcImportProd' AND type_desc = 'WINDOWS_USER') 
			CREATE USER [FIRSTLOOK\svcImportProd] FOR LOGIN [FIRSTLOOK\svcImportProd] WITH DEFAULT_SCHEMA=[FIRSTLOOK\svcImportProd]
			
	GRANT INSERT ON dbo.FileWatcher TO [FIRSTLOOK\svcImportProd]
	GRANT EXEC ON dbo.FileWatcher#Insert TO [FIRSTLOOK\svcImportProd]
	GRANT INSERT, DELETE ON AULtec.Photos TO [FIRSTLOOK\svcImportProd]
	

	GRANT EXEC ON SCHEMA::Listing TO [FIRSTLOOK\svcImportProd]	
	GRANT SELECT, INSERT,UPDATE, DELETE ON SCHEMA::Listing TO [FIRSTLOOK\svcImportProd]
END	