if object_id('postings.CarsDotComPhoneLeads#Fetch') is not null
	DROP PROCEDURE postings.CarsDotComPhoneLeads#Fetch
GO

CREATE PROCEDURE postings.CarsDotComPhoneLeads#Fetch
(
	@LoadID INT,
	@RequestID INT
) AS

SELECT businessUnitId, LeadDate, CallerPhone, CallerZip, CallStatus, CallDurationMinutes, CallDurationSeconds, Load_ID, ExtractRequestHandle, FetchDateInserted, DateStaged
FROM 	postings.CarsDotComPhoneLeads tbl
	INNER JOIN Datafeeds.Extract.FetchDotCom#Request r
		ON r.Handle = tbl.ExtractRequestHandle

WHERE 
	tbl.Load_ID = @LoadID
	AND r.RequestId = @RequestID
	AND NOT EXISTS (SELECT	1
			FROM 	Datafeeds.Extract.FetchDotCom#Error err
			WHERE 	err.RequestID = r.RequestId
				AND err.BusinessUnitId = tbl.businessUnitID
				AND err.[Month] = tbl.LeadDate
				AND err.ErrorTypeID = 4
				)
GO

