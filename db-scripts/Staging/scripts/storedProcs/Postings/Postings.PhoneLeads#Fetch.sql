IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'postings.PhoneLeads#Fetch') AND type in (N'P', N'PC'))
DROP PROCEDURE postings.PhoneLeads#Fetch
GO

CREATE PROCEDURE postings.PhoneLeads#Fetch (
	@LoadID INT,
	@RequestID INT
) AS

select
	ph.businessUnitId,
	ph.LeadDate,
	ph.CallerPhone,
	ph.InventoryType
from 
	postings.PhoneLeads ph
	join Datafeeds.Extract.FetchDotCom#Request r
		on r.Handle = ph.ExtractRequestHandle
where
	ph.Load_ID = @LoadID
	and r.RequestId = @RequestID
	and not exists
	(
		select *
		from Datafeeds.Extract.FetchDotCom#Error err
		where
			err.RequestID = r.RequestId
			and err.BusinessUnitId = ph.businessUnitID
			and err.[Month] = ph.LeadDate
			and err.ErrorTypeID = 4
	)
	


go