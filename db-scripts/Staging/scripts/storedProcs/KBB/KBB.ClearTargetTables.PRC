SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[KBB].[ClearTargetTables]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [KBB].[ClearTargetTables]
GO

CREATE PROC KBB.ClearTargetTables
------------------------------------------------------------------------------------------------
--	
--	Truncates or deletes the KBB target tables depending on FK relationships (truncate
--	if possible)
--
------------------------------------------------------------------------------------------------
--
--	03/31/2008	WGH	Initial design/development.
--	1.5.11 		ewimmer added truncation of new tbls (region, regionadjustmenttypepricetype, regiongroupadjustment, regionzipcode,vehiclegroup, zipcodes
--	10/28/2013	WGH	4.1 schema updates
--
------------------------------------------------------------------------------------------------
AS
SET NOCOUNT ON 
DECLARE @ErrorMessage NVARCHAR(4000)
DECLARE @ErrorSeverity INT
DECLARE @ErrorState INT
	
BEGIN TRY

	DELETE FROM KBB.Region
	TRUNCATE TABLE KBB.RegionAdjustmentTypePriceType
	TRUNCATE TABLE KBB.RegionGroupAdjustment
	TRUNCATE TABLE KBB.RegionZipCode
	TRUNCATE TABLE KBB.VehicleGroup
	DELETE FROM KBB.ZipCode

	TRUNCATE TABLE KBB.OptionSpecificationValue
	TRUNCATE TABLE KBB.RegionBasePrice
	TRUNCATE TABLE KBB.ProgramContext
	
	TRUNCATE TABLE KBB.MileageGroupAdjustment
	
	DELETE FROM KBB.MileageRange
--	DELETE FROM KBB.RegionState
	DELETE FROM KBB.VehicleCategory
	
	DELETE FROM KBB.OptionRelationshipSet
	DELETE FROM KBB.OptionRelationship
	
	TRUNCATE TABLE KBB.OptionRegionPriceAdjustment
	TRUNCATE TABLE KBB.VehiclePriceRange
	
	DELETE FROM KBB.PriceType
	DELETE FROM KBB.VehicleRegion
	TRUNCATE TABLE KBB.VehicleOptionCategory
	TRUNCATE TABLE KBB.VINOptionEquipment
	DELETE FROM KBB.VehicleOption
	
	
	DELETE FROM KBB.VINVehiclePattern
	TRUNCATE TABLE KBB.SpecificationValue
	DELETE FROM KBB.Vehicle
	DELETE FROM KBB.Trim
	DELETE FROM KBB.ModelYear
	DELETE FROM KBB.Model          
	DELETE FROM KBB.Make
	DELETE FROM KBB.Year
	
	DELETE FROM KBB.Specification
	DELETE FROM KBB.DataVersion
	
	
	DELETE FROM KBB.Category
	DELETE FROM KBB.VINOptionEquipmentPattern
	
END TRY
BEGIN CATCH

	SELECT	@ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE()
	
	RAISERROR (	@ErrorMessage, 
			@ErrorSeverity, 
			@ErrorState 
			)

END CATCH