
IF EXISTS (SELECT 1 FROM sys.procedures where object_id=OBJECT_ID('Listing.VehicleOption#Merge'))
	DROP PROCEDURE  Listing.VehicleOption#Merge 
GO

CREATE PROC Listing.VehicleOption#Merge
------------------------------------------------------------------------------------------------
--
--	Merges any changed options for the passed provider into Listing.VehicleOption
--
---Parameters-----------------------------------------------------------------------------------
--
@ProviderID	INT,
@LogID		INT = NULL
--
---History--------------------------------------------------------------------------------------
--	
--	WGH	10/13/2014	Initial design/dev 
--		03/02/2015	Added 50-option cap since this data isn't really used anywhere.  
--				Note that the change detection still spans the entire option 
--				text, so we will get false change events.  But still beats 
---				storing gigs of ultimately superfluous option data 
--	
------------------------------------------------------------------------------------------------
AS

SET NOCOUNT ON 

DECLARE @Step 		VARCHAR(100),
	@ProcName	SYSNAME
	
SET @ProcName = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)	

----------------------------------------------------------------------------------------
SET @Step = 'Started, ProviderID = ' + CAST(@ProviderID AS VARCHAR)
----------------------------------------------------------------------------------------

EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID OUTPUT


BEGIN TRY

	----------------------------------------------------------------------------------------
	SET @Step = 'Load #VehicleChangeSet'
	----------------------------------------------------------------------------------------

	CREATE TABLE #VehicleChangeSet	(SetKeyHash BINARY(16) NOT NULL, UniqueKeyHash BINARY(16) NOT NULL, Options VARCHAR(MAX), OptionsHash BINARY(16))
	CREATE CLUSTERED INDEX #IXC_VehicleChangeSet ON #VehicleChangeSet(SetKeyHash, UniqueKeyHash)
	
	INSERT
	INTO	#VehicleChangeSet (SetKeyHash, UniqueKeyHash)
	SELECT	DISTINCT Vehicle.SetKeyHash, Vehicle.UniqueKeyHash
	FROM	Staging.Listing.Vehicle
		
		INNER JOIN (	SELECT	SetKeyHash, UniqueKeyHash
				FROM	Staging.Listing.Vehicle
				WHERE	[_ProviderID] = @ProviderID
				GROUP
				BY	SetKeyHash, UniqueKeyHash
				HAVING	COUNT(*) = 1
				) DD ON Vehicle.SetKeyHash = DD.SetKeyHash
					AND Vehicle.UniqueKeyHash = DD.UniqueKeyHash
	
		LEFT JOIN Staging.Listing.Vehicle_History ON Vehicle.[_ProviderID] = Vehicle_History.[_ProviderID] 
								AND Vehicle.SetKeyHash  = Vehicle_History.SetKeyHash 
								AND Vehicle.UniqueKeyHash = Vehicle_History.UniqueKeyHash
	WHERE	Vehicle._ProviderID = @ProviderID
		AND (	Vehicle_History.RowHash IS NULL
			OR Vehicle.Rowhash <> Vehicle_History.RowHash
			)
		AND Vehicle.OptionsHash <> COALESCE(Vehicle_History.OptionsHash, 0x00)
		
	SET @Step = @Step + ', ' + CAST(@@ROWCOUNT AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID	
			
	UPDATE STATISTICS #VehicleChangeSet

	----------------------------------------------------------------------------------------
	SET @Step = 'Delete Listing.VehicleOption (1)'
	----------------------------------------------------------------------------------------
	
	DELETE	VO
	FROM	Listing.VehicleOption VO
		INNER JOIN #VehicleChangeSet VCS ON VO.SetKeyHash = VCS.SetKeyHash AND VO.UniqueKeyHash = VCS.UniqueKeyHash
	WHERE	VO.ProviderID = @ProviderID
	
	SET @Step = @Step + ', ' + CAST(@@ROWCOUNT AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID	

	----------------------------------------------------------------------------------------
	SET @Step = 'Insert Listing.VehicleOption'
	----------------------------------------------------------------------------------------
	
	INSERT
	INTO	Listing.VehicleOption (ProviderID, SetKeyHash, UniqueKeyHash, Ordinal, Description)
	SELECT	V._ProviderID, V.SetKeyHash, V.UniqueKeyHash, SD.Rank, LEFT(Value, 500)
	FROM	#VehicleChangeSet VCS
		INNER JOIN Listing.Vehicle V ON VCS.SetKeyHash = V.SetKeyHash AND VCS.UniqueKeyHash = V.UniqueKeyHash
		INNER JOIN Listing.Provider P ON V._ProviderID = P.ProviderID
		CROSS APPLY Utility.String.SplitTrimOrderedSetMaxLength(V.Options, P.OptionDelimiter, 500) SD
	
	WHERE	V._ProviderID = @ProviderID
		AND SD.Rank <= 50
	
	SET @Step = @Step + ', ' + CAST(@@ROWCOUNT AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID

	----------------------------------------------------------------------------------------
	SET @Step = 'Delete Listing.VehicleOption (2)'
	----------------------------------------------------------------------------------------
		
	DELETE	VO
	FROM	Listing.VehicleOption VO
	WHERE	VO.ProviderID = @ProviderID
		AND NOT EXISTS (SELECT	1
				FROM	Listing.Vehicle V
				WHERE	VO.ProviderID = V.[_ProviderID]
					AND VO.SetKeyHash = V.SetKeyHash
					AND VO.UniqueKeyHash = V.UniqueKeyHash
					)
					
	SET @Step = @Step + ', ' + CAST(@@ROWCOUNT AS VARCHAR) + ' row(s) affected.  Complete.'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID	

END TRY
BEGIN CATCH

	EXEC dbo.sp_ErrorHandler @Message = @Step, @LogID = @LogID

END CATCH
GO


EXEC dbo.sp_SetStoredProcedureDescription 'Listing.VehicleOption#Merge',
'Merges any changed options for the passed type-one provider into Listing.VehicleOption.'


/*	Alternate version that can handle an initial load...painfully slow tho'


	--SELECT	DISTINCT Vehicle.SetKeyHash, Vehicle.UniqueKeyHash
	--FROM	Listing.Vehicle
		
	--	INNER JOIN (	SELECT	SetKeyHash, UniqueKeyHash
	--			FROM	Listing.Vehicle
	--			WHERE	[_ProviderID] = @ProviderID
	--			GROUP
	--			BY	SetKeyHash, UniqueKeyHash
	--			HAVING	COUNT(*) = 1
	--			) DD ON Vehicle.SetKeyHash = DD.SetKeyHash
	--				AND Vehicle.UniqueKeyHash = DD.UniqueKeyHash
	
	--	LEFT JOIN (	Listing.Vehicle_History 
	--			INNER JOIN Listing.VehicleOption ON Vehicle_History.[_ProviderID] = VehicleOption.ProviderID
	--								AND Vehicle_History.SetKeyHash = VehicleOption.SetKeyHash
	--								AND Listing.Vehicle_History.UniqueKeyHash = Listing.VehicleOption.UniqueKeyHash
	--			) ON Vehicle.[_ProviderID] = Vehicle_History.[_ProviderID] 
	--							AND Vehicle.SetKeyHash  = Vehicle_History.SetKeyHash 
	--							AND Vehicle.UniqueKeyHash = Vehicle_History.UniqueKeyHash
	--WHERE	Vehicle._ProviderID = @ProviderID
	--	AND (	Vehicle_History.RowHash IS NULL
	--		OR Vehicle.Rowhash <> Vehicle_History.RowHash
	--		)
	--	AND (	Vehicle_History.OptionsHash IS NULL 
	--		OR Vehicle.OptionsHash <> Vehicle_History.OptionsHash
	--		)




*/