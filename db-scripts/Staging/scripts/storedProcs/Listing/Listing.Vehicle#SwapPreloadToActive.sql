IF EXISTS (SELECT 1 FROM sys.procedures where object_id=OBJECT_ID('Listing.Vehicle#SwapPreloadToActive'))
	DROP PROCEDURE  Listing.Vehicle#SwapPreloadToActive 
go

---History--------------------------------------------------------------------------------------
--	
--	02/28/2014	JBF	Updates for new partitioning scheme
--	05/12/2014	WGH	Added explicit partition index reorg		
--	02/15/2015	WGH	Updated indexes
--	07/03/2015	WGH	Remove REORG of IX_ListingVehicle_Preload__SetUniqueKeyHash
--				should never be used when querying a swapped-in provider's vehicles
--
------------------------------------------------------------------------------------------------
CREATE PROCEDURE Listing.Vehicle#SwapPreloadToActive 
(
	@ProviderId tinyint
)
AS 
BEGIN
	DECLARE @err BIT = 0

	------------------------------------------------------------------------------------------------
	--	reorganize targeted indexes on preload before swap
	------------------------------------------------------------------------------------------------
	
	ALTER INDEX IXC_ListingVehiclePreload__VinAuditID ON Listing.Vehicle_Preload REORGANIZE PARTITION=@ProviderID		
	--ALTER INDEX IX_ListingVehicle_Preload__SetUniqueKeyHash ON Listing.Vehicle_Preload REORGANIZE PARTITION=@ProviderID
	
	BEGIN TRANSACTION
		TRUNCATE TABLE Listing.Vehicle_Rolloff  
		ALTER TABLE Listing.Vehicle_History SWITCH PARTITION $PARTITION.[PF_PartitionProvider] (@ProviderId) TO Listing.Vehicle_Rolloff 
		ALTER TABLE Listing.Vehicle SWITCH PARTITION $PARTITION.[PF_PartitionProvider] (@ProviderId) TO Listing.Vehicle_History PARTITION $PARTITION.[PF_PartitionProvider] (@ProviderId)
		ALTER TABLE Listing.Vehicle_Preload SWITCH PARTITION $PARTITION.[PF_PartitionProvider] (@ProviderId) TO Listing.Vehicle PARTITION $PARTITION.[PF_PartitionProvider] (@ProviderId) 
	COMMIT
 
END