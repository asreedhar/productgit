SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'Listing.SetLoadStatus') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Listing.SetLoadStatus
GO



---History--------------------------------------------------------------------------------------
--	
--	02/28/2014	JBF		Updates for new partitioning scheme
--
CREATE PROCEDURE Listing.SetLoadStatus
(
	@LoadID		INT,		-- DBASTAT.dbo.Dataload_History.Load_ID
	@ProviderID	TINYINT,	-- Staging.Listing.Provider
	@FileType	VARCHAR(20)	-- Datafeeds.dbo.DatafeedFileType
)
AS 
BEGIN
	DECLARE @err BIT = 0
	
	DECLARE @Step 			VARCHAR(100),
		@ProviderFeedTypeID	TINYINT,
		@rc			INT
	----------------------------------------------------------------------------------------
	SET @Step = 'Gather provider info'
	----------------------------------------------------------------------------------------
	
	SELECT	@ProviderFeedTypeID 	= ProviderFeedTypeID
	FROM	Listing.Provider
	WHERE	ProviderID = @ProviderID
 
	----------------------------------------------------------------------------------------
	SET @Step = 'Update Listing.Provider'
	----------------------------------------------------------------------------------------  

	UPDATE	Listing.Provider
	
	SET	ActiveLoadID		= @LoadID,
		ResponseFilePending	= CASE WHEN @ProviderFeedTypeID = 2 AND @FileType = 'ListLevel' THEN 1 ELSE 0 END,
		LastUpdateDT		= GETDATE(),
		LastUpdateUser		= SUSER_SNAME()
		 
	WHERE	ProviderID = @ProviderID
END
