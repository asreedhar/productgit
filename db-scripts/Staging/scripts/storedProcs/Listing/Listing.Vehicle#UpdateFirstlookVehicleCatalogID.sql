SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'Listing.Vehicle#UpdateFirstlookVehicleCatalogID') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Listing.Vehicle#UpdateFirstlookVehicleCatalogID
GO
---History--------------------------------------------------------------------------------------
--	
--	02/28/2014	JBF		Updates for new partitioning scheme
--
------------------------------------------------------------------------------------------------
CREATE PROCEDURE [Listing].[Vehicle#UpdateFirstlookVehicleCatalogID]  
(@ProviderID int, @LoadID INT, @PartitionStatusCD INT = 2)
AS 
BEGIN

	UPDATE v

	SET 	FirstlookVehicleCatalogID	= COALESCE(VC2T.VehicleCatalogID, VC2.VehicleCatalogID, VC1.VehicleCatalogID)	
		
	FROM	Staging.Listing.Vehicle_Preload v	
		LEFT JOIN	VehicleCatalog.Firstlook.VehicleCatalog VC1 ON 
					VC1.IsInFullDecodingSet = 1
				AND VC1.VehicleCatalogLevelID = 1
				AND LEFT(v.VIN, 8) + SUBSTRING(v.VIN, 10, 1) = VC1.SquishVin
				AND v.VIN LIKE VC1.VINPattern
	
		LEFT JOIN VehicleCatalog.Firstlook.VehicleCatalog VC2 ON 
					VC2.IsInFullDecodingSet = 1
				AND VC2.VehicleCatalogLevelID = 2
				AND VC2.IsDistinctSeries = 1
				AND VC1.SquishVIN = VC2.SquishVIN
				AND VC1.VehicleCatalogID = VC2.ParentID
				AND CASE WHEN ISNULL(v.Trim,'') IN ('','N/A') THEN COALESCE(VC1.Series,'') ELSE v.Trim END = VC2.Series
	
		LEFT JOIN VehicleCatalog.Firstlook.VINPatternPriority VPP ON  
			COALESCE(VC1.VINPattern,VC2.VINPattern) = VPP.VINPattern 
			AND v.VIN LIKE VPP.PriorityVINPattern  
			AND VC1.CountryCode = VPP.CountryCode 
	
		LEFT JOIN VehicleCatalog.Firstlook.VehicleCatalog VC2T ON
				VC2T.IsInFullDecodingSet = 1
			AND VC2T.VehicleCatalogLevelID = 2
			AND VC1.SquishVIN = VC2T.SquishVIN
			AND VC1.VehicleCatalogID = VC2T.ParentID
			AND VC2T.IsDistinctSeriesTransmission = 1
			AND (	(CASE WHEN ISNULL(v.Trim,'') IN ('','N/A') THEN COALESCE(VC1.Series,'') ELSE v.Trim END = VC2T.Series)		-- EXACT MATCH
				OR (VC1.Series = VC2T.Series)			-- NO NEED TO MATCH ON @Trim IF VC Series' ARE EQUAL
				)									
			AND Market.Listing.GetStandardizedTransmission(v.Transmission) = VC2T.Transmission 
		
		LEFT JOIN (	VehicleCatalog.Firstlook.Model MD 
				INNER JOIN VehicleCatalog.Firstlook.Line L ON MD.LineID = L.LineID
				INNER JOIN VehicleCatalog.Firstlook.Make MK ON L.MakeID = MK.MakeID
				) ON COALESCE(VC2T.ModelID, VC2.ModelID, VC1.ModelID) = MD.ModelID
				
		LEFT JOIN IMT.dbo.MakeModelGrouping MMG ON COALESCE(VC2T.ModelID, VC2.ModelID, VC1.ModelID) = MMG.ModelID
	
		LEFT JOIN VehicleCatalog.Categorization.ModelConfiguration_VehicleCatalog mcvc 
				ON mcvc.VehicleCatalogID = COALESCE(VC2T.VehicleCatalogID, VC2.VehicleCatalogID, VC1.VehicleCatalogID)	
		
		LEFT JOIN VehicleCatalog.Categorization.ModelConfiguration mc
			ON mc.ModelConfigurationID=mcvc.ModelConfigurationID
		
		LEFT JOIN VehicleCatalog.Categorization.Model m ON mc.ModelID=m.ModelID
		
		LEFT JOIN VehicleCatalog.Categorization.Configuration c ON c.ConfigurationID=mc.ConfigurationID	
												
	WHERE  	VPP.PriorityVINPattern IS NULL  	
		AND _ProviderID = @ProviderID   
		AND _LoadID = @LoadID 
END

GO


