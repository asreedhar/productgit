SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'Manheim.TruncateLoad') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure Manheim.TruncateLoad
GO

CREATE PROC Manheim.TruncateLoad
AS
SET NOCOUNT ON

TRUNCATE TABLE Manheim.Sales 
TRUNCATE TABLE Manheim.Listings 
TRUNCATE TABLE Manheim.ListingsOptions 

BULK INSERT Manheim.Sales FROM '\\PRODAUX03\Datafeeds\Auction-Standard\PRESALES--Sales.txt'	
WITH (FIRSTROW = 2, FIELDTERMINATOR = '|', MAXERRORS = 10 )
BULK INSERT Manheim.Listings FROM '\\PRODAUX03\Datafeeds\Auction-Standard\PRESALES--Listings.txt'	
WITH (FIRSTROW = 2, FIELDTERMINATOR = '|', MAXERRORS = 10 )
BULK INSERT Manheim.ListingsOptions FROM '\\PRODAUX03\Datafeeds\Auction-Standard\PRESALES--ListingsOptions.txt'	
WITH (FIRSTROW = 2, FIELDTERMINATOR = '|', MAXERRORS = 10 )


BULK INSERT Manheim.Sales FROM '\\PRODAUX03\Datafeeds\Auction-Standard\SIMULCAST--Sales.txt'	
WITH (FIRSTROW = 2, FIELDTERMINATOR = '|', MAXERRORS = 10 )
BULK INSERT Manheim.Listings FROM '\\PRODAUX03\Datafeeds\Auction-Standard\SIMULCAST--Listings.txt'	
WITH (FIRSTROW = 2, FIELDTERMINATOR = '|', MAXERRORS = 10 )
BULK INSERT Manheim.ListingsOptions FROM '\\PRODAUX03\Datafeeds\Auction-Standard\SIMULCAST--ListingsOptions.txt'	
WITH (FIRSTROW = 2, FIELDTERMINATOR = '|', MAXERRORS = 10 )

BULK INSERT Manheim.Listings FROM '\\PRODAUX03\Datafeeds\Auction-Standard\DEALEREXCHANGE--Listings.txt'	
WITH (FIRSTROW = 2, FIELDTERMINATOR = '|', MAXERRORS = 10 )
BULK INSERT Manheim.ListingsOptions FROM '\\PRODAUX03\Datafeeds\Auction-Standard\DEALEREXCHANGE--ListingsOptions.txt'	
WITH (FIRSTROW = 2, FIELDTERMINATOR = '|', MAXERRORS = 10 )

GO
