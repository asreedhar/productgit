IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[Aultec].[Photos#Merge]')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE [Aultec].[Photos#Merge]
GO
CREATE PROCEDURE [Aultec].[Photos#Merge]
    (
      @BusinessUnitId AS INTEGER
    , @xml AS XML
    )
AS 
    BEGIN
        WITH    photos
                  AS ( SELECT   s.value('../../vin[1]', 'varchar(500)') AS VIN
                              , s.value('.[1]', 'varchar(500)') AS Url
                              , ROW_NUMBER() OVER ( PARTITION BY s.value('../../vin[1]', 'varchar(500)') ORDER BY ( SELECT 1 ) ) AS Ordinal
                       FROM     @xml.nodes('/vinPhotos/vinPhoto/urls/url') AS B ( S )
                     ) ,
                InventoryWithPhotos
                  AS ( SELECT   i.BusinessUnitId
                              , i.InventoryId
                              , p.Url
                              , p.Ordinal
                       FROM     [IMT].[dbo].[tbl_Vehicle] v
                                INNER JOIN [IMT].[dbo].[Inventory] i ON [v].[VehicleID] = [i].[VehicleID]
                                INNER JOIN photos p ON v.VIN = p.Vin
                       WHERE    i.[BusinessUnitID] = @BusinessUnitId
                                AND i.[InventoryActive] = 1
                     ) ,
                thing
                  AS ( SELECT   *
                       FROM     [Staging].[Aultec].[Photos]
                       WHERE    [BusinessUnitId] = @BusinessUnitId
                     )
            MERGE thing AS tgt
                USING 
                    ( SELECT BusinessUnitId, InventoryId, Url, Ordinal FROM InventoryWithPhotos
                    ) AS src ( BusinessUnitId, InventoryId, Url, Ordinal )
                ON ( src.BusinessUnitId = tgt.BusinessUnitId
                     AND src.InventoryId = tgt.InventoryId
                     AND src.Url = tgt.Url
					 AND src.Ordinal = tgt.Ordinal
                   )
                WHEN NOT MATCHED BY TARGET 
                    THEN INSERT (
                                  BusinessUnitId
                                , InventoryId
                                , Url
                                , Ordinal
                                )
                         VALUES ( src.BusinessUnitId
                                , src.InventoryId
                                , src.Url
                                , src.[Ordinal]
                                )
                WHEN NOT MATCHED BY SOURCE 
                    THEN DELETE ;
    END
GO

IF EXISTS(SELECT 1 FROM sys.database_principals DP WHERE name = 'FIRSTLOOK\svcImportProd' AND type_desc = 'WINDOWS_USER')
	GRANT EXEC ON Aultec.Photos#Merge TO [FIRSTLOOK\svcImportProd]
