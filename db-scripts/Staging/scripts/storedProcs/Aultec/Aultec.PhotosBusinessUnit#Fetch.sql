IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'[Aultec].[PhotosBusinessUnit#Fetch]')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE [Aultec].[PhotosBusinessUnit#Fetch]
GO
CREATE PROCEDURE [AULtec].[PhotosBusinessUnit#Fetch]
AS 
    SELECT  [bu].[BusinessUnitID]
          , [bu].[BusinessUnitCode]
    FROM    [IMT].[dbo].[BusinessUnit] bu
            INNER JOIN [IMT].[Extract].[DealerConfiguration] dc ON [bu].[BusinessUnitID] = dc.[BusinessUnitID]
            INNER JOIN [IMT].[Extract].[DealerConfigurationFlag] dcf ON ( dc.[Flags] & POWER(2, [dcf].[BitPosition]) ) = POWER(2, [dcf].[BitPosition])
    WHERE   [DC].[Active] = 1
            AND [dcf].[Name] = 'AULTecPhotoImport'
GO

IF EXISTS(SELECT 1 FROM sys.database_principals DP WHERE name = 'FIRSTLOOK\svcImportProd' AND type_desc = 'WINDOWS_USER')
	GRANT EXEC ON [Aultec].[PhotosBusinessUnit#Fetch] TO [FIRSTLOOK\svcImportProd]
	
GO

EXEC dbo.sp_SetStoredProcedureDescription 'AULtec.PhotosBusinessUnit#Fetch',
'Returns hardcoded list of BUIDs for AULtec photo data collection...will be updated to be data-driven once the management infrastructure is in place.'	