SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id('[Chrome].[Divisions#Merge]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [Chrome].[Divisions#Merge]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**
**  Procedure: Chrome.Divisions#Merge
**  Description: Merges data from new files into  staging tbl
**   
**        
**
**  Return values:  

** 
**  Input parameters:   none
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  12.22.11     EWimmer   Procedure created.
**
*******************************************************************************/
CREATE PROCEDURE [Chrome].[Divisions#Merge]
AS

BEGIN

DECLARE @RowCnt INT
DECLARE @RowsInserted INT
DECLARE @RowsUpdated INT
DECLARE @RowsDeleted INT



SET NOCOUNT ON;

select @RowCnt = COUNT(1) from Staging.chrome.Divisions#Raw WITH (NOLOCK)

IF @RowCnt > 0 
BEGIN

BEGIN TRY
BEGIN TRANSACTION

--updates into tbl variables 
--select into tbl variable
DECLARE @is_update TABLE (CountryCode tinyint, DivisionID int)

INSERT INTO @is_update
SELECT Stg.CountryCode, Stg.DivisionID
FROM Staging.chrome.Divisions#Raw AS Stg WITH (NOLOCK)
INNER JOIN (SELECT  CountryCode, DivisionID, ManufacturerID, DivisionName
				  FROM Staging.chrome.Divisions#Stg WITH (NOLOCK)) AS App
ON Stg.CountryCode = App.CountryCode
AND Stg.DivisionID = App.DivisionID
AND (
                                ( ISNULL(stg.ManufacturerID,0) !=  ISNULL(App.ManufacturerID,0))  
                           OR ( ISNULL(stg.DivisionName,'NDN') !=  ISNULL(App.DivisionName,'NDN'))            
       )

--ewimmer 12.16.11 update existing rows with chg data 
UPDATE a
SET IS_Update = 1
FROM Staging.chrome.Divisions#Raw a WITH (NOLOCK), @is_update AS b
WHERE a.CountryCode = b.CountryCode
AND a.DivisionID = b.DivisionID

 SELECT @RowsUpdated = @@ROWCOUNT


--News into tbl variables 
DECLARE @is_new TABLE (CountryCode tinyint, DivisionID int)

INSERT INTO @is_new
SELECT  Stg.CountryCode, Stg.DivisionID
FROM Staging.chrome.Divisions#Raw AS Stg WITH (NOLOCK)
LEFT OUTER  JOIN (SELECT   CountryCode,DivisionID
							FROM Staging.chrome.Divisions#Stg WITH (NOLOCK)) AS App
ON Stg.CountryCode = App.CountryCode
AND Stg.DivisionID = App.DivisionID
WHERE App.CountryCode IS NULL AND App.DivisionID IS NULL


--ewimmer 12.16.11 update existing rows with chg data 
UPDATE a
SET IS_New = 1
FROM Staging.chrome.Divisions#Raw a WITH (NOLOCK), @is_new AS b
WHERE a.CountryCode = b.CountryCode
AND a.DivisionID = b.DivisionID

SELECT @RowsInserted = @@ROWCOUNT


--Deletes

DECLARE @is_deletes TABLE  (CountryCode tinyint, DivisionID int)

INSERT INTO @is_deletes
SELECT App.CountryCode, App.DivisionID
FROM Staging.chrome.Divisions#Stg AS App WITH (NOLOCK)
LEFT OUTER  JOIN (SELECT CountryCode, DivisionID
							FROM Staging.chrome.Divisions#Raw WITH (NOLOCK)) AS Stg
ON Stg.CountryCode = App.CountryCode
AND Stg.DivisionID = App.DivisionID
WHERE Stg.CountryCode IS NULL AND Stg.DivisionID IS NULL 


--ewimmer 12.16.11 update existing rows with chg data 
UPDATE a
SET IS_Delete = 1
FROM Staging.chrome.Divisions#Stg a WITH (NOLOCK), @is_deletes AS b
WHERE  a.CountryCode = b.CountryCode
AND a.DivisionID = b.DivisionID



SELECT @RowsDeleted = @@ROWCOUNT
 

--ewimmer load logging tbl  
INSERT INTO  [Chrome].[Data#Log]
(RunDt, TblName, SPName, RowsUpdated, RowsInserted, RowsDeleted,LoadDttm)
 VALUES (convert(varchar(10),GETDATE(), 101), 'Chrome.Divisions','Chrome.Divisions#Merge', ISNULL(@RowsUpdated, 0), ISNULL( @RowsInserted, 0),ISNULL( @RowsDeleted, 0),  GETDATE())
 
   COMMIT TRANSACTION

END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0 
ROLLBACK TRAN 
EXEC dbo.sp_ErrorHandler
END CATCH

END
END
GO
