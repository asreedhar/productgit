IF OBJECTPROPERTY(OBJECT_ID('Chrome.MergeRawDatasets'), 'IsProcedure') = 1 
	DROP PROC Chrome.MergeRawDatasets
GO
CREATE PROC Chrome.MergeRawDatasets
------------------------------------------------------------------------------------------------
--
--	Called by the Chrome Load process, this proc "merges" the raw CA data into the raw US 
--	set to create a unified NA set, prioritizing US VINPatterns over CA.  Ultimately, this
--	is loaded to VehicleCatalog.Chrome by the SSIS packages.
--
---History--------------------------------------------------------------------------------------
--	
--	WGH	23.1	04/10/2015	Initial design/dev 
--
------------------------------------------------------------------------------------------------	
@LogID INT = NULL
AS

SET NOCOUNT ON
DECLARE @RowCount	INT,
	@Step		VARCHAR(255),
	@ProcName	SYSNAME
	
SET @ProcName = OBJECT_SCHEMA_NAME(@@PROCID) + '.' + OBJECT_NAME(@@PROCID)	

EXEC dbo.sp_LogEvent 'I', @ProcName, 'Started', @LogID OUTPUT


BEGIN TRY

	------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Get set of CA VINPatterns superseded by US'
	------------------------------------------------------------------------------------------------------------------------

	SELECT  S.VINPatternID, VINPattern_Prefix, VINPattern
	INTO	#VINPatterns
	FROM    Chrome.VINPattern#Raw S
	WHERE   CountryCode = 2
		AND EXISTS ( SELECT     1
			     FROM       Chrome.VINPattern#Raw T
			     WHERE      T.CountryCode = 1
					AND S.VINPattern_Prefix = T.VINPattern_Prefix
					AND S.VINPattern = T.VINPattern
					)

	
	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
	
	
	------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Delete Chrome.VINPatternStyleMapping#Raw'
	------------------------------------------------------------------------------------------------------------------------

	DELETE	VPSM
	FROM	#VINPatterns D
		INNER JOIN Chrome.VINPattern#Raw VP ON VP.CountryCode = 2 AND D.VINPatternID = VP.VINPatternID
		INNER JOIN Chrome.VINPatternStyleMapping#Raw VPSM ON VP.CountryCode = VPSM.CountryCode AND VP.VINPatternID = VPSM.VINPatternID

	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
	
	------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Delete Chrome.VINPattern#Raw'
	------------------------------------------------------------------------------------------------------------------------

	DELETE	VP
	FROM	#VINPatterns D
		INNER JOIN Chrome.VINPattern#Raw VP ON VP.CountryCode = 2 AND D.VINPatternID = VP.VINPatternID

	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
	
	------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Move CA Chrome.VINPattern#Raw to the US set'
	------------------------------------------------------------------------------------------------------------------------

	UPDATE	Chrome.VINPattern#Raw 
	SET	CountryCode = 1
	WHERE	CountryCode = 2

	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
	
	------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Move CA Chrome.VINPatternStyleMapping#Raw to the US set'
	------------------------------------------------------------------------------------------------------------------------
	
	UPDATE	Chrome.VINPatternStyleMapping#Raw
	SET	CountryCode = 1
	WHERE	CountryCode = 2

	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
	
	------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Move CA Chrome.YearMakeModelStyle#Raw to the US set'
	------------------------------------------------------------------------------------------------------------------------

	UPDATE	Chrome.YearMakeModelStyle#Raw 
	SET	CountryCode = 1
	WHERE	CountryCode = 2

	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
	
	------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Insert missing reference data into US set from CA for Chrome.CategoryHeaders#Raw'
	------------------------------------------------------------------------------------------------------------------------

	INSERT
	INTO	Chrome.CategoryHeaders#Raw (CountryCode,CategoryHeaderID,CategoryHeader,Sequence,LoadDttm)
	SELECT	1,CategoryHeaderID,CategoryHeader,Sequence,LoadDttm
	FROM	Chrome.CategoryHeaders#Raw S
	WHERE	CountryCode = 2
		AND NOT EXISTS (SELECT	1
				FROM	Chrome.CategoryHeaders#Raw T
				WHERE	T.CountryCode = 1
					AND S.CategoryHeaderID = T.CategoryHeaderID
					)

	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
	
	------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Insert missing reference data into US set from CA for Chrome.MktClass#Raw'
	------------------------------------------------------------------------------------------------------------------------
	
	INSERT
	INTO	Chrome.MktClass#Raw (CountryCode,MktClassID,MarketClass,LoadDttm)
	SELECT	1,MktClassID,MarketClass,LoadDttm
	FROM	Chrome.MktClass#Raw S
	WHERE	CountryCode = 2
		AND NOT EXISTS (SELECT	1
				FROM	Chrome.MktClass#Raw T
				WHERE	T.CountryCode = 1
					AND S.MktClassID = T.MktClassID
					)

	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
						

	--INSERT
	--INTO	Chrome.CITypes (CountryCode,TypeID,Type)
	--SELECT	1,TypeID,Type
	--FROM	Chrome.CITypes#Raw S
	--WHERE	CountryCode = 2
	--	AND NOT EXISTS (SELECT	1
	--			FROM	Chrome.CITypes#Raw T
	--			WHERE	T.CountryCode = 1
	--				AND S.TypeID = T.TypeID
	--				)


	------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Insert missing reference data into US set from CA for Chrome.Manufacturers#Raw'
	------------------------------------------------------------------------------------------------------------------------
	
	INSERT
	INTO	Chrome.Manufacturers#Raw (CountryCode,ManufacturerID,ManufacturerName,LoadDttm)
	SELECT	1,ManufacturerID,ManufacturerName,LoadDttm
	FROM	Chrome.Manufacturers#Raw S
	WHERE	CountryCode = 2
		AND NOT EXISTS (SELECT	1
				FROM	Chrome.Manufacturers#Raw T
				WHERE	T.CountryCode = 1
					AND S.ManufacturerID = T.ManufacturerID
					)

	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
						
	------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Insert missing reference data into US set from CA for Chrome.Divisions#Raw'
	------------------------------------------------------------------------------------------------------------------------
	
	INSERT
	INTO	Chrome.Divisions#Raw (CountryCode,DivisionID,ManufacturerID,DivisionName,LoadDttm)
	SELECT	1,DivisionID,ManufacturerID,DivisionName,LoadDttm
	FROM	Chrome.Divisions#Raw S
	WHERE	CountryCode = 2
		AND NOT EXISTS (SELECT	1
				FROM	Chrome.Divisions#Raw T
				WHERE	T.CountryCode = 1
					AND S.DivisionID = T.DivisionID
					)

	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
		
	------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Insert missing reference data into US set from CA for Chrome.Subdivisions#Raw'
	------------------------------------------------------------------------------------------------------------------------
	
	INSERT
	INTO	Chrome.Subdivisions#Raw (CountryCode,ModelYear,DivisionID,SubdivisionID,HistSubdivisionID,SubdivisionName,LoadDttm)
	SELECT	1,ModelYear,DivisionID,SubdivisionID,HistSubdivisionID,SubdivisionName, LoadDttm
	FROM	Chrome.Subdivisions#Raw S
	WHERE	CountryCode = 2
		AND NOT EXISTS (SELECT	1
				FROM	Chrome.Subdivisions#Raw T
				WHERE	T.CountryCode = 1
					AND S.SubdivisionID = T.SubdivisionID
					)
	
	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID

	------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Insert missing reference data into US set from CA for Chrome.Category#Raw'
	------------------------------------------------------------------------------------------------------------------------
	
	
	INSERT
	INTO	Chrome.Category#Raw (CountryCode,CategoryID,Description,CategoryUTF,CategoryType, LoadDttm)
	SELECT	1,CategoryID,Description,CategoryUTF,CategoryType, LoadDttm
	FROM	Chrome.Category#Raw S
	WHERE	CountryCode = 2
		AND NOT EXISTS (SELECT	1
				FROM	Chrome.Category#Raw T
				WHERE	T.CountryCode = 1
					AND S.CategoryID = T.CategoryID
					)

	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
	
	------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Insert missing reference data into US set from CA for Chrome.OptHeaders#Raw'
	------------------------------------------------------------------------------------------------------------------------
	
	INSERT
	INTO	Chrome.OptHeaders#Raw (CountryCode,HeaderID,Header, LoadDttm)
	SELECT	1,HeaderID,Header, LoadDttm
	FROM	Chrome.OptHeaders#Raw S
	WHERE	CountryCode = 2
		AND NOT EXISTS (SELECT	1
				FROM	Chrome.OptHeaders#Raw T
				WHERE	T.CountryCode = 1
					AND S.HeaderID = T.HeaderID
					)

	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID

	------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Insert missing reference data into US set from CA for Chrome.OptKinds#Raw'
	------------------------------------------------------------------------------------------------------------------------
	
	INSERT
	INTO	Chrome.OptKinds#Raw (CountryCode,OptionKindID,OptionKind, LoadDttm)
	SELECT	1,OptionKindID,OptionKind, LoadDttm
	FROM	Chrome.OptKinds#Raw S
	WHERE	CountryCode = 2
		AND NOT EXISTS (SELECT	1
				FROM	Chrome.OptKinds#Raw T
				WHERE	T.CountryCode = 1
					AND S.OptionKindID = T.OptionKindID
					)

	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
	
	------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Insert missing reference data into US set from CA for Chrome.TechTitleHeader#Raw'
	------------------------------------------------------------------------------------------------------------------------
	
	INSERT
	INTO	Chrome.TechTitleHeader#Raw (CountryCode,TechTitleHeaderID,TechTitleHeaderText,Sequence, LoadDttm)
	SELECT	1,TechTitleHeaderID,TechTitleHeaderText,Sequence, LoadDttm
	FROM	Chrome.TechTitleHeader#Raw S
	WHERE	CountryCode = 2
		AND NOT EXISTS (SELECT	1
				FROM	Chrome.TechTitleHeader#Raw T
				WHERE	T.CountryCode = 1
					AND S.TechTitleHeaderID = T.TechTitleHeaderID
					)
	
	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
	
	------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Insert missing reference data into US set from CA for Chrome.StdHeaders#Raw'
	------------------------------------------------------------------------------------------------------------------------
	
	INSERT
	INTO	Chrome.StdHeaders#Raw (CountryCode,HeaderID,Header,LoadDttm)
	SELECT	1,HeaderID,Header,LoadDttm
	FROM	Chrome.StdHeaders#Raw S
	WHERE	CountryCode = 2
		AND NOT EXISTS (SELECT	1
				FROM	Chrome.StdHeaders#Raw T
				WHERE	T.CountryCode = 1
					AND S.HeaderID = T.HeaderID
					)

	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
	
	------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Insert missing reference data into US set from CA for Chrome.Categories#Raw'
	------------------------------------------------------------------------------------------------------------------------
	
	INSERT
	INTO	Chrome.Categories#Raw (CountryCode,CategoryID,Category,CategoryTypeFilter,CategoryHeaderID,UserFriendlyName,LoadDttm)
	SELECT	1,CategoryID,Category,CategoryTypeFilter,CategoryHeaderID,UserFriendlyName,LoadDttm
	FROM	Chrome.Categories#Raw S
	WHERE	CountryCode = 2
		AND NOT EXISTS (SELECT	1
				FROM	Chrome.Categories#Raw T
				WHERE	T.CountryCode = 1
					AND S.CategoryID = T.CategoryID
					)

	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
	
	------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Insert missing reference data into US set from CA for Chrome.Models#Raw'
	------------------------------------------------------------------------------------------------------------------------
	
	INSERT
	INTO	Chrome.Models#Raw (CountryCode,ModelID,HistModelID,ModelYear,DivisionID,SubdivisionID,ModelName,EffectiveDate,ModelComment,Availability,LoadDttm)
	SELECT	1,ModelID,HistModelID,ModelYear,DivisionID,SubdivisionID,ModelName,EffectiveDate,ModelComment,Availability,LoadDttm
	FROM	Chrome.Models#Raw S
	WHERE	CountryCode = 2
		AND NOT EXISTS (SELECT	1
				FROM	Chrome.Models#Raw T
				WHERE	T.CountryCode = 1
					AND S.ModelID = T.ModelID
					)

	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
	
	------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Insert missing reference data into US set from CA for Chrome.TechTitles#Raw'
	------------------------------------------------------------------------------------------------------------------------
	
	INSERT
	INTO	Chrome.TechTitles#Raw (CountryCode,TitleID,Sequence,Title,TechTitleHeaderID,LoadDttm)
	SELECT	1,TitleID,Sequence,Title,TechTitleHeaderID,LoadDttm
	FROM	Chrome.TechTitles#Raw S
	WHERE	CountryCode = 2
		AND NOT EXISTS (SELECT	1
				FROM	Chrome.TechTitles#Raw T
				WHERE	T.CountryCode = 1
					AND S.TitleID = T.TitleID
					)

	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
	
	----------------------------------------------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Insert the CA styles'
	----------------------------------------------------------------------------------------------------------------------------------------------------------------

	INSERT
	INTO	Chrome.Styles#Raw (CountryCode,StyleID,HistStyleID,ModelID,ModelYear,Sequence,StyleCode,FullStyleCode,StyleName,TrueBasePrice,Invoice,MSRP,Destination,StyleCVCList,MktClassID,StyleNameWOTrim,Trim,PassengerCapacity,PassengerDoors,ManualTrans,AutoTrans,FrontWD,RearWD,AllWD,FourWD,StepSide,Caption,Availability,PriceState,AutoBuilderStyleID,CFModelName,CFStyleName,CFDrivetrain,CFBodyType,LoadDttm)
	SELECT	1, StyleID,HistStyleID,ModelID,ModelYear,Sequence,StyleCode,FullStyleCode,StyleName,TrueBasePrice,Invoice,MSRP,Destination,StyleCVCList,MktClassID,StyleNameWOTrim,Trim,PassengerCapacity,PassengerDoors,ManualTrans,AutoTrans,FrontWD,RearWD,AllWD,FourWD,StepSide,Caption,Availability,PriceState,AutoBuilderStyleID,CFModelName,CFStyleName,CFDrivetrain,CFBodyType,LoadDttm
	FROM	Chrome.Styles#Raw S
	WHERE	CountryCode = 2

	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID
	

	----------------------------------------------------------------------------------------------------------------------------------------------------------------
	SET @Step = 'Update the rest related to style.  if they cannot be reached via vp->vpsm, so be it'
	----------------------------------------------------------------------------------------------------------------------------------------------------------------

	UPDATE	Chrome.ConsInfo#Raw 
	SET	CountryCode = 1
	WHERE	CountryCode = 2

	UPDATE	Chrome.BodyStyles#Raw
	SET	CountryCode = 1
	WHERE	CountryCode = 2


	UPDATE	Chrome.StyleCats#Raw
	SET	CountryCode = 1
	WHERE	CountryCode = 2

	UPDATE	Chrome.Colors#Raw 
	SET	CountryCode = 1
	WHERE	CountryCode = 2

	UPDATE	Chrome.TechSpecs#Raw
	SET	CountryCode = 1
	WHERE	CountryCode = 2


	UPDATE	Chrome.Options#Raw
	SET	CountryCode = 1
	WHERE	CountryCode = 2

	UPDATE	Chrome.Prices#Raw
	SET	CountryCode = 1
	WHERE	CountryCode = 2

	UPDATE	Chrome.Standards#Raw
	SET	CountryCode = 1
	WHERE	CountryCode = 2

	UPDATE	Chrome.StyleGenericEquipment#Raw
	SET	CountryCode = 1
	WHERE	CountryCode = 2

	UPDATE	Chrome.StyleWheelBase#Raw
	SET	CountryCode = 1
	WHERE	CountryCode = 2


	--------------------------------------------------------------------------------
	SET @Step = 'Remove deleted VinPatternIDs'
	--------------------------------------------------------------------------------

	DELETE	T
	FROM	Chrome.VINEquipment#Raw T
		INNER JOIN #VINPatterns VP ON T.CountryCode = 2 AND T.VinPatternID = VP.VinPatternID

	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID	

	--------------------------------------------------------------------------------
	SET @Step = 'Remove deleted VinPatternIDs'
	--------------------------------------------------------------------------------
	
	UPDATE	Chrome.VINEquipment#Raw
	SET	CountryCode = 1
	WHERE	CountryCode = 2

	SET @RowCount = @@ROWCOUNT
	SET @Step = @Step + ', ' + CAST(@RowCount AS VARCHAR) + ' row(s) affected'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID	
	
	
	--------------------------------------------------------------------
	SET @Step = 'Delete any remaining CA rows'
	--------------------------------------------------------------------

	DELETE Chrome.Category#Raw WHERE CountryCode = 2

	DELETE Chrome.Categories#Raw WHERE CountryCode = 2
	DELETE Chrome.CategoryHeaders#Raw WHERE CountryCode = 2

	DELETE Chrome.CITypes#Raw WHERE CountryCode = 2


	DELETE Chrome.Subdivisions#Raw WHERE CountryCode = 2
	DELETE Chrome.Divisions#Raw WHERE CountryCode = 2
	
	DELETE Chrome.Manufacturers#Raw WHERE CountryCode = 2

	DELETE Chrome.OptHeaders#Raw WHERE CountryCode = 2
	DELETE Chrome.OptKinds#Raw WHERE CountryCode = 2

	DELETE Chrome.TechTitles#Raw WHERE CountryCode = 2

	DELETE Chrome.TechTitleHeader#Raw WHERE CountryCode = 2
	DELETE Chrome.StdHeaders#Raw WHERE CountryCode = 2


	DELETE Chrome.Styles#Raw WHERE CountryCode = 2
	DELETE Chrome.MktClass#Raw WHERE CountryCode = 2
	DELETE Chrome.Models#Raw WHERE CountryCode = 2
	

	SET @Step = 'Finished'
	EXEC dbo.sp_LogEvent 'I', @ProcName, @Step, @LogID	
		
END TRY
BEGIN CATCH

	EXEC sp_ErrorHandler @Message = @Step
		
END CATCH
