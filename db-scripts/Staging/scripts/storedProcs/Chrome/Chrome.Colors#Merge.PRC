SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id('[Chrome].[Colors#Merge]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [Chrome].[Colors#Merge]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**
**  Procedure: Chrome.Colors#Merge
**  Description: Merges data from new files into  staging tbl
**   
**        
**
**  Return values:  

** 
**  Input parameters:   none
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  12.22.11     EWimmer   Procedure created.
**
*******************************************************************************/
CREATE PROCEDURE [Chrome].[Colors#Merge]
AS

BEGIN

DECLARE @RowCnt INT
DECLARE @RowsInserted INT
DECLARE @RowsUpdated INT
DECLARE @RowsDeleted INT


SET NOCOUNT ON;

select @RowCnt = COUNT(1) from Staging.chrome.Colors#Raw WITH (NOLOCK) 

IF @RowCnt > 0 
BEGIN

BEGIN TRY
BEGIN TRANSACTION

--updates into tbl variables 
--select into tbl variable
DECLARE @is_update TABLE (CountryCode tinyint, StyleID int, Ext1Code VARCHAR(50), Ext2Code VARCHAR(50), IntCode VARCHAR(50), Condition VARCHAR(300))

INSERT INTO @is_update
SELECT Stg.CountryCode, Stg.StyleID, Stg.Ext1Code, Stg.Ext2Code, Stg.IntCode, Stg.Condition
FROM Staging.chrome.Colors#Raw AS Stg WITH (NOLOCK) 
INNER JOIN (SELECT  CountryCode, StyleID, Ext1Code, Ext2Code, IntCode, Ext1ManCode, Ext2ManCode, IntManCode, OrderCode, AsTwoTone, Ext1Desc, Ext2Desc, IntDesc, Condition, GenericExtColor, GenericExt2Color,                           Ext1RGBHex, Ext2RGBHex, Ext1MfrFullCode, Ext2MfrFullCode
				  FROM Staging.chrome.Colors#Stg WITH (NOLOCK) ) AS App
ON Stg.CountryCode = App.CountryCode
AND Stg.StyleID = App.StyleID
AND ISNULL(Stg.Ext1Code,'NE') =ISNULL(App.Ext1Code,'NE')
AND ISNULL(Stg.Ext2Code,'NET')=ISNULL(App.Ext2Code,'NET')
AND ISNULL(Stg.IntCode,'NC') =ISNULL(App.IntCode,'NC')
AND ISNULL(Stg.Condition,'NCO')=ISNULL(App.Condition,'NCO')
AND (
                                ( ISNULL(stg.Ext1ManCode,'NEM') !=  ISNULL(App.Ext1ManCode,'NEM'))
                             Or ( ISNULL(stg.Ext2ManCode, 'NEMC') !=  ISNULL(App.Ext2ManCode, 'NEMC'))
                             Or ( ISNULL(stg.IntManCode,'IMC') !=  ISNULL(App.IntManCode,'IMC'))
                             Or ( ISNULL(stg.OrderCode,'OC') !=  ISNULL(App.OrderCode,'OC'))
                             Or ( ISNULL(stg.AsTwoTone,'N') !=   ISNULL(App.AsTwoTone,'N'))
                             Or ( ISNULL(stg.Ext1Desc,'NE') !=  ISNULL(App.Ext1Desc,'NE'))
                             Or ( ISNULL(stg.Ext2Desc,'NED') !=  ISNULL(App.Ext2Desc,'NED'))
                             Or ( ISNULL(stg.IntDesc,'NID') !=  ISNULL(App.IntDesc,'NID'))
                             Or ( ISNULL(stg.GenericExtColor,'NG') !=  ISNULL(App.GenericExtColor,'NG'))
                             Or ( ISNULL(stg.Ext1RGBHex,'NR') !=  ISNULL(App.Ext1RGBHex,'NR'))
                              Or ( ISNULL(stg.Ext2RGBHex,'NRF') !=  ISNULL(App.Ext2RGBHex,'NRF'))
                               Or ( ISNULL(stg.Ext1MfrFullCode,'NM') !=  ISNULL(App.Ext1MfrFullCode,'NM'))
                                Or ( ISNULL(stg.Ext2MfrFullCode,'NMF') != ISNULL(App.Ext2MfrFullCode,'NMF') )
                             
       )

--ewimmer 12.16.11 update existing rows with chg data 
UPDATE a
SET IS_Update = 1
FROM Staging.chrome.Colors#Raw a WITH (NOLOCK) , @is_update AS b
WHERE a.CountryCode = b.CountryCode
AND a.StyleID = b.StyleID
AND ISNULL(a.Ext1Code,'NE') =ISNULL(b.Ext1Code,'NE')
AND ISNULL(a.Ext2Code,'NET')=ISNULL(b.Ext2Code,'NET')
AND ISNULL(a.IntCode,'NC') =ISNULL(b.IntCode,'NC')
AND ISNULL(a.Condition,'NCO')=ISNULL(b.Condition,'NCO')


 SELECT @RowsUpdated = @@ROWCOUNT


--News into tbl variables 
DECLARE @is_new TABLE (CountryCode tinyint, StyleID int, Ext1Code VARCHAR(50), Ext2Code VARCHAR(50), IntCode VARCHAR(50), Condition VARCHAR(300))

INSERT INTO @is_new
SELECT  Stg.CountryCode, Stg.StyleID, Stg.Ext1Code, Stg.Ext2Code, Stg.IntCode, Stg.Condition
FROM Staging.chrome.Colors#Raw AS Stg WITH (NOLOCK) 
LEFT OUTER  JOIN (SELECT   CountryCode,StyleID, Ext1Code, Ext2Code, IntCode, Condition
							FROM Staging.chrome.Colors#Stg WITH (NOLOCK)) AS App
ON Stg.CountryCode = App.CountryCode
AND Stg.StyleID = App.StyleID
AND ISNULL(Stg.Ext1Code,'NE') =ISNULL(App.Ext1Code,'NE')
AND ISNULL(Stg.Ext2Code,'NET')=ISNULL(App.Ext2Code,'NET')
AND ISNULL(Stg.IntCode,'NC') =ISNULL(App.IntCode,'NC')
AND ISNULL(Stg.Condition,'NCO')=ISNULL(App.Condition,'NCO')
WHERE App.CountryCode IS NULL AND App.StyleID IS NULL AND  App.Ext1Code is null AND App.Ext2Code IS NULL AND App.IntCode IS NULL AND App.Condition IS NULL


--ewimmer 12.16.11 update existing rows with chg data 
UPDATE a
SET IS_New = 1
FROM Staging.chrome.Colors#Raw a WITH (NOLOCK), @is_new AS b
WHERE a.CountryCode = b.CountryCode
AND a.StyleID = b.StyleID
AND ISNULL(a.Ext1Code,'NE') =ISNULL(b.Ext1Code,'NE')
AND ISNULL(a.Ext2Code,'NET')=ISNULL(b.Ext2Code,'NET')
AND ISNULL(a.IntCode,'NC') =ISNULL(b.IntCode,'NC')
AND ISNULL(a.Condition,'NCO')=ISNULL(b.Condition,'NCO')

SELECT @RowsInserted = @@ROWCOUNT


--Deletes

DECLARE @is_deletes TABLE  (CountryCode tinyint, StyleID int, Ext1Code VARCHAR(50), Ext2Code VARCHAR(50), IntCode VARCHAR(50), Condition VARCHAR(300))

INSERT INTO @is_deletes
SELECT App.CountryCode, App.StyleID, App.Ext1Code, App.Ext2Code, App.IntCode, App.Condition
FROM Staging.chrome.Colors#Stg AS App WITH (NOLOCK) 
LEFT OUTER  JOIN (SELECT CountryCode, StyleID, Ext1Code, Ext2Code, IntCode, Condition
							FROM Staging.chrome.Colors#Raw WITH (NOLOCK) ) AS Stg
ON Stg.CountryCode = App.CountryCode
AND Stg.StyleID = App.StyleID
AND ISNULL(Stg.Ext1Code,'NE') =ISNULL(App.Ext1Code,'NE')
AND ISNULL(Stg.Ext2Code,'NET')=ISNULL(App.Ext2Code,'NET')
AND ISNULL(Stg.IntCode,'NC') =ISNULL(App.IntCode,'NC')
AND ISNULL(Stg.Condition,'NCO')=ISNULL(App.Condition,'NCO')
WHERE Stg.CountryCode IS NULL AND Stg.StyleID IS NULL AND Stg.Ext1Code is null AND Stg.Ext2Code IS NULL AND Stg.IntCode IS NULL AND Stg.Condition IS NULL


--ewimmer 12.16.11 update existing rows with chg data 
UPDATE a
SET IS_Delete = 1
FROM Staging.chrome.Colors#Stg a WITH (NOLOCK), @is_deletes AS b
WHERE a.CountryCode = b.CountryCode
AND a.StyleID = b.StyleID
AND ISNULL(a.Ext1Code,'NE') =ISNULL(b.Ext1Code,'NE')
AND ISNULL(a.Ext2Code,'NET')=ISNULL(b.Ext2Code,'NET')
AND ISNULL(a.IntCode,'NC') =ISNULL(b.IntCode,'NC')
AND ISNULL(a.Condition,'NCO')=ISNULL(b.Condition,'NCO')



SELECT @RowsDeleted = @@ROWCOUNT
 

--ewimmer load logging tbl  
INSERT INTO  [Chrome].[Data#Log]
(RunDt, TblName, SPName, RowsUpdated, RowsInserted, RowsDeleted,LoadDttm)
 VALUES (convert(varchar(10),GETDATE(), 101), 'Chrome.Colors','Chrome.Colors#Merge', ISNULL(@RowsUpdated, 0), ISNULL( @RowsInserted, 0),ISNULL( @RowsDeleted, 0),  GETDATE())
 
 COMMIT TRANSACTION

END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0 
ROLLBACK TRAN 
EXEC dbo.sp_ErrorHandler
END CATCH  

END
END
GO
