SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id('[Chrome].[Styles#Merge]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [Chrome].[Styles#Merge]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**
**  Procedure: Chrome.Styles#Merge
**  Description: Merges data from new files into  staging tbl
**   
**  Return values:  

** 
**  Input parameters:   none
**
**  Output parameters:  none
**
**  Rows returned: none
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:       Author:   Description:
**  ----------  --------  -------------------------------------------
**  12.21.11     EWimmer   Procedure created.
**
*******************************************************************************/


CREATE PROCEDURE [Chrome].[Styles#Merge]
AS

BEGIN

DECLARE @RowCnt INT
DECLARE @RowsInserted INT
DECLARE @RowsUpdated INT
DECLARE @RowsDeleted INT


SET NOCOUNT ON;

select @RowCnt = COUNT(1) from Staging.chrome.Styles#Raw WITH (NOLOCK)

IF @RowCnt > 0 
BEGIN

BEGIN TRY
BEGIN TRANSACTION

--updates into tbl variables 
--select into tbl variable
DECLARE @is_update TABLE (CountryCode tinyint,StyleID int)

INSERT INTO @is_update
SELECT Stg.CountryCode, Stg.StyleID
FROM Staging.chrome.Styles#Raw AS Stg WITH (NOLOCK)
INNER JOIN (SELECT  CountryCode, StyleID, HistStyleID, ModelID, ModelYear, Sequence, StyleCode, FullStyleCode, StyleName, TrueBasePrice, Invoice, MSRP, Destination, StyleCVCList, 
                   MktClassID,StyleNameWOTrim, Trim, PassengerCapacity, PassengerDoors, ManualTrans, AutoTrans, FrontWD, RearWD, AllWD, FourWD, StepSide, Caption, Availability, PriceState, AutoBuilderStyleID, 
                   CFModelName, CFStyleName, CFDrivetrain, CFBodyType
				FROM Staging.chrome.Styles#Stg WITH (NOLOCK)) AS App
ON Stg.CountryCode = App.CountryCode
AND Stg.StyleID = App.StyleID
AND (
                                ( ISNULL(stg.HistStyleID, 0) !=  ISNULL(App.HistStyleID, 0))
                             Or ( ISNULL(stg.ModelID, 0) !=  ISNULL(App.ModelID,0))
                             Or ( ISNULL(stg.ModelYear,0) !=  ISNULL(App.ModelYear,0))
                             Or ( ISNULL(stg.Sequence, 0) !=  ISNULL(App.Sequence,0))
                             Or ( ISNULL(stg.StyleCode, 'NoStCd') !=  ISNULL(App.StyleCode, 'NoStCd'))
                             Or ( ISNULL(stg.FullStyleCode, 'NoFSC') !=  ISNULL(App.FullStyleCode, 'NoFSC'))
                             Or ( ISNULL(stg.StyleName, 'NoStNm') !=  ISNULL(App.StyleName, 'NoStNm'))
                             Or ( ISNULL(stg.TrueBasePrice, 'NoTP') !=  ISNULL(App.TrueBasePrice, 'NoTP'))
                             Or ( ISNULL(stg.Invoice,0) !=  ISNULL(App.Invoice,0))
                             Or ( ISNULL(stg.MSRP,0) !=  ISNULL(App.MSRP,0))
                             Or ( ISNULL(stg.Destination,0) !=  ISNULL(App.Destination,0))
                             Or ( ISNULL(stg.StyleCVCList,'NoSC') !=  ISNULL(App.StyleCVCList,'NoSC'))
                             Or ( ISNULL(stg.MktClassID,0) !=  ISNULL(App.MktClassID,0))
                             Or ( ISNULL(stg.StyleNameWOTrim,'SNNT') !=  ISNULL(App.StyleNameWOTrim,'SNNT'))
                             Or ( ISNULL(stg.Trim,'NT') !=  ISNULL(App.Trim,'NT'))
                             Or ( ISNULL(stg.PassengerCapacity,0) !=  ISNULL(App.PassengerCapacity,0))
                             Or ( ISNULL(stg.PassengerDoors,0) !=  ISNULL(App.PassengerDoors,0))
                             Or ( ISNULL(stg.ManualTrans,'N') !=  ISNULL(App.ManualTrans,'N'))
                             Or ( ISNULL(stg.AutoTrans,'N') !=  ISNULL(App.AutoTrans,'N'))
                             Or ( ISNULL(stg.FrontWD,'N') !=  ISNULL(App.FrontWD,'N'))
                             Or ( ISNULL(stg.RearWD,'N') !=  ISNULL(App.RearWD,'N'))
                             Or ( ISNULL(stg.AllWD,'N') !=  ISNULL(App.AllWD,'N'))
                             Or ( ISNULL(stg.FourWD,'N') !=  ISNULL(App.FourWD,'N'))
                             Or ( ISNULL(stg.StepSide,'N') !=  ISNULL(App.StepSide,'N'))
                             Or ( ISNULL(stg.Caption,'NC') !=  ISNULL(App.Caption,'NC'))
                             Or ( ISNULL(stg.Availability,'NA') !=  ISNULL(App.Availability,'NA'))
                             Or ( ISNULL(stg.PriceState,'NPS') !=  ISNULL(App.PriceState,'NPS'))
                             Or ( ISNULL(stg.AutoBuilderStyleID,'NAV') !=  ISNULL(App.AutoBuilderStyleID,'NAV'))
                             Or ( ISNULL(stg.CFModelName,'CFMN') !=  ISNULL(App.CFModelName,'CFMN'))
                             Or ( ISNULL(stg.CFStyleName,'CSMN') !=  ISNULL(App.CFStyleName,'CSMN'))
                             Or ( ISNULL(stg.CFDrivetrain,'NOCT') !=  ISNULL(App.CFDrivetrain,'NOCT'))
                             Or ( ISNULL(stg.CFBodyType,'NOBT') !=  ISNULL(App.CFBodyType,'NOBT'))
 		 )


--ewimmer 12.16.11 update existing rows with chg data 
UPDATE a
SET IS_Update = 1
FROM Staging.chrome.Styles#Raw a WITH (NOLOCK), @is_update AS b
WHERE  a.CountryCode = b.CountryCode
AND a.StyleID = b.StyleID

 SELECT @RowsUpdated = @@ROWCOUNT


--News into tbl variables 
DECLARE @is_new TABLE (CountryCode tinyint,StyleID int)

INSERT INTO @is_new
SELECT stg.CountryCode,stg.StyleID
FROM Staging.chrome.Styles#Raw AS Stg WITH (NOLOCK)
LEFT OUTER  JOIN (SELECT  CountryCode,StyleID
							FROM Staging.chrome.Styles#Stg WITH (NOLOCK)) AS App
ON Stg.CountryCode = App.CountryCode
AND Stg.StyleID = App.StyleID
WHERE App.countrycode  IS null  AND App.styleid IS NULL


--ewimmer 12.16.11 update existing rows with chg data 
UPDATE a
SET IS_New = 1
FROM Staging.chrome.Styles#Raw a WITH (NOLOCK), @is_new AS b
WHERE  a.CountryCode = b.CountryCode
AND a.StyleID = b.StyleID

 SELECT @RowsInserted = @@ROWCOUNT


--Deletes

DECLARE @is_deletes TABLE (CountryCode tinyint,StyleID int)

INSERT INTO @is_deletes
SELECT App.CountryCode,App.StyleID
FROM Staging.chrome.Styles#Stg AS App WITH (NOLOCK)
LEFT OUTER  JOIN (SELECT  CountryCode,StyleID
							FROM Staging.chrome.Styles#Raw WITH (NOLOCK)) AS Stg
ON Stg.CountryCode = App.CountryCode
AND Stg.StyleID = App.StyleID
WHERE Stg.countrycode is null and Stg.styleid IS null

--ewimmer 12.16.11 update existing rows with chg data 
UPDATE a
SET IS_Delete = 1
FROM Staging.chrome.Styles#Stg a WITH (NOLOCK), @is_deletes AS b
WHERE  a.CountryCode = b.CountryCode
AND a.StyleID = b.StyleID


 SELECT @RowsDeleted = @@ROWCOUNT
 

--ewimmer load logging tbl  
INSERT INTO  [Chrome].[Data#Log]
(RunDt, TblName, SPName, RowsUpdated, RowsInserted, RowsDeleted,LoadDttm)
 VALUES (convert(varchar(10),GETDATE(), 101), 'Chrome.Styles','Chrome.Styles#Raw', ISNULL(@RowsUpdated, 0), ISNULL( @RowsInserted, 0),ISNULL( @RowsDeleted, 0),  GETDATE())
 
 COMMIT TRANSACTION

END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0 
ROLLBACK TRAN 
EXEC dbo.sp_ErrorHandler
END CATCH 

END
END
GO
