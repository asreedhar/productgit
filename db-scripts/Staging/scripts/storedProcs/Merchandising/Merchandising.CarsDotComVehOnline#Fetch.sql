if object_id('Merchandising.CarsDotComVehOnline#Fetch') is not null
	drop procedure Merchandising.CarsDotComVehOnline#Fetch
GO

CREATE PROCEDURE Merchandising.CarsDotComVehOnline#Fetch (
	@LoadID INT,
	@RequestID INT
) AS

SELECT tbl.*
FROM Staging.Merchandising.CarsDotComVehOnline tbl
	INNER JOIN Datafeeds.Extract.FetchDotCom#Request r
		ON r.Handle = tbl.ExtractRequestHandle
WHERE
	tbl.Load_ID = @LoadID
	and r.RequestId = @RequestID
	AND NOT EXISTS (
	SELECT 1
	FROM Datafeeds.Extract.FetchDotCom#Error err
	WHERE err.RequestID = r.RequestId
		AND err.BusinessUnitId = tbl.businessUnitID
		AND err.VIN = tbl.VIN
		AND err.StockNumber = tbl.StockNumber)
	AND NOT EXISTS (
	SELECT 1
	FROM Datafeeds.Extract.FetchDotCom#Error err
	WHERE err.RequestID = r.RequestId
		AND err.BusinessUnitId = tbl.businessUnitID
		AND err.ErrorTypeID = 4)
GO


