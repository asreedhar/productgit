
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[Merchandising].[FetchDotComImport#Validate]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE [Merchandising].[FetchDotComImport#Validate]
GO

CREATE PROCEDURE Merchandising.FetchDotComImport#Validate
	@LoadID INT,
    @RequestId INT ,
    @DatasourceInterfaceID INT
AS 
    BEGIN
        SET NOCOUNT ON

        DECLARE @DEBUG BIT

        --SET @DEBUG = 1	-- will print messages
        SET @DEBUG = 0 --  normal operation

        IF @DEBUG = 1 
            PRINT 'Merchandising.FetchDotComImport#Validate, ' +
				'@LoadID=' + CAST(@LoadID AS VARCHAR) +
				'@RequestId=' + CAST(@RequestId AS VARCHAR) +
				', @DatasourceInterfaceID=' + CAST(@DatasourceInterfaceID AS VARCHAR)

        BEGIN TRY

            DECLARE @RowsUpdated INT
            DECLARE @TotalStagingRowCount INT
            DECLARE @RequestHandle UNIQUEIDENTIFIER

            SELECT  @RequestHandle = Handle
            FROM    Datafeeds.Extract.FetchDotCom#Request
            WHERE   RequestId = @RequestId
            
            IF @RequestHandle IS NULL 
                RAISERROR('Invalid RequestId passed.  Cannot find corresponding RequestHandle.  RequestId: %d ', 16, 1,@RequestId)

	    -- Get the number of dealers in this request
            DECLARE @NumberOfDealersInRequest INT
            SELECT  @NumberOfDealersInRequest = COUNT(BusinessUnitId)
            FROM    Datafeeds.Extract.FetchDotCom#RequestDealer
            WHERE   RequestId = @RequestId

            IF @DEBUG = 1 
                PRINT '  Total Number Of Dealers In Request:'
                    + CAST(@NumberOfDealersInRequest AS VARCHAR)
            
	    -- Create API to sub procedures
            CREATE TABLE #Staging
                (
                  businessUnitID INT NOT NULL ,
                  [month] DATETIME NULL ,
                  ExtractRequestHandle UNIQUEIDENTIFIER NOT NULL ,
                  VIN VARCHAR(50) NULL ,
                  StockNumber VARCHAR(50) NULL
                )

	    -- Load this tables staging data for this request
            INSERT  INTO #Staging
                    ( businessUnitID ,
                      month ,
                      ExtractRequestHandle ,
                      VIN ,
                      StockNumber 
                    )
                    EXEC Merchandising.FetchDotComImport#GetStaging
						@LoadID,
						@RequestId,
                        @DatasourceInterfaceID

            SELECT  @TotalStagingRowCount = @@Rowcount
            
            IF @DEBUG = 1 
                PRINT '  Total Staging Row Count:'
                    + CAST(@TotalStagingRowCount AS VARCHAR)

            IF @NumberOfDealersInRequest > 0
                AND @TotalStagingRowCount > 0 
                BEGIN

                    DECLARE @TotalErrorCount INT
		    
                    BEGIN TRANSACTION

		    -- Call sub proc to get errors
                    INSERT  INTO Datafeeds.Extract.FetchDotCom#Error
                            ( RequestId ,
                              BusinessUnitId ,
                              Month ,
                              DatasourceInterfaceID ,
                              ErrorTypeID ,
                              VIN ,
                              StockNumber
	                      )
                            EXEC Merchandising.FetchDotComImportError#Check
								@RequestId,
                                @DatasourceInterfaceID

                    SET @TotalErrorCount = @@ROWCOUNT
		    
                    COMMIT TRANSACTION
                    
                    IF @DEBUG = 1 
                        PRINT '  Total Error Count: '
                            + CAST(@TotalErrorCount AS VARCHAR)

                END
            ELSE 
                IF @DEBUG = 1 
                    BEGIN
                        IF @NumberOfDealersInRequest = 0 
                            PRINT '  No dealers in request'
                    
                        IF @TotalStagingRowCount = 0 
                            PRINT '  No data staged'
                    END
    
        END TRY
        BEGIN CATCH

            IF @DEBUG = 1 
                PRINT 'Error was raised.'

            IF @@TRANCOUNT > 0 
                ROLLBACK TRANSACTION
            
            DECLARE @ErrorMessage NVARCHAR(4000)
            DECLARE @ErrorSeverity INT
            DECLARE @ErrorState INT

            SELECT  @ErrorMessage = ERROR_MESSAGE() ,
                    @ErrorSeverity = ERROR_SEVERITY() ,
                    @ErrorState = ERROR_STATE()

            RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               )
    
        END CATCH

    END


