IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Merchandising.CarsDotComPerfSummary#Fetch') AND type in (N'P', N'PC'))
DROP PROCEDURE Merchandising.CarsDotComPerfSummary#Fetch
GO

CREATE PROCEDURE Merchandising.CarsDotComPerfSummary#Fetch (
	@LoadID INT,
	@RequestID INT
) AS

SELECT
	tbl.businessUnitID,
	tbl.month,
	tbl.InSearchResult,
	tbl.RequestDetail,
	tbl.SearchDealer,
	tbl.ViewVideo,
	tbl.EmailDealerNew,
	tbl.EmailDealerUsed,
	tbl.PhoneProspectNew,
	tbl.PhoneProspectsUsed,
	tbl.ChatProspectsNew,
	tbl.ChatProspectsUsed,
	tbl.ClickThru,
	tbl.PrintAD,
	tbl.ViewMap,
	tbl.TotalContacts,
	tbl.PctListPriceUsed,
	tbl.PctListCommentUsed,
	tbl.PctListPhotoUsed,
	tbl.PctSinglePhotoUsed,
	tbl.pctMultiPhotoUsed,
	tbl.PctListVideoUsed,
	tbl.PctListPriceNew,
	tbl.PctListCommentNew,
	tbl.PctListPhotoNew,
	tbl.PctSinglePhotoNew,
	tbl.pctMultiPhotoNew,
	tbl.PctListVideoNew,
	tbl.FetchDateInserted
FROM Staging.Merchandising.CarsDotComPerfSummary tbl
	INNER JOIN Datafeeds.Extract.FetchDotCom#Request r
		ON r.Handle = tbl.ExtractRequestHandle
WHERE
	tbl.Load_ID = @LoadID
	AND r.RequestId = @RequestID
	AND NOT EXISTS (
	SELECT 1
	FROM Datafeeds.Extract.FetchDotCom#Error err
	WHERE err.RequestID = r.RequestId
		AND err.BusinessUnitId = tbl.businessUnitID
		AND err.[Month] = tbl.[Month]
		AND err.ErrorTypeID in (4, 5))
GO

