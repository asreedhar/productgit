IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Merchandising.CarsDotComFeedError#Fetch') AND type in (N'P', N'PC'))
DROP PROCEDURE Merchandising.CarsDotComFeedError#Fetch
GO

CREATE PROCEDURE Merchandising.CarsDotComFeedError#Fetch (
	@LoadID INT,
	@RequestID INT
) AS

SELECT
	tbl.ErrorDate,
	tbl.BusinessUnitID
FROM Staging.Merchandising.CarsDotComFeedError tbl
	INNER JOIN Datafeeds.Extract.FetchDotCom#Request r
		ON r.Handle = tbl.ExtractRequestHandle
WHERE 
	tbl.Load_ID = @LoadID
	AND r.RequestId = @RequestID
	AND (
			ErrorType LIKE '%password reset%'
			OR ErrorType LIKE '%account has been locked%'
			OR ErrorType LIKE '%user has been disabled%'
			OR ErrorType LIKE '%password are incorrect%'
			OR ErrorType LIKE '%deactivated your account%'
		)
	AND NOT EXISTS (
		SELECT 1
		FROM Datafeeds.Extract.FetchDotCom#Error err
		WHERE err.RequestID = r.RequestId
			AND err.BusinessUnitId = tbl.businessUnitID
			AND err.[Month] = tbl.[Month]
			AND err.ErrorTypeID = 4)
GO
