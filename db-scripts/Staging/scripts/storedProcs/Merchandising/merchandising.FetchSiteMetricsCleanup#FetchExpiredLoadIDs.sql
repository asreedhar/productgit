if object_id('merchandising.FetchSiteMetricsCleanup#FetchExpiredLoadIDs') is not null
	drop procedure merchandising.FetchSiteMetricsCleanup#FetchExpiredLoadIDs
go


/*

	exec Staging.Merchandising.FetchSiteMetricsCleanup#FetchExpiredLoadIDs
		@DatasourceInterfaceID = 14,
		@DaysOldToExpire = 179


	select * from Datafeeds.dbo.DatafeedFileType where DatafeedCode = 'FetchDotCom-Response' order by DatasourceInterfaceID	
*/

create procedure merchandising.FetchSiteMetricsCleanup#FetchExpiredLoadIDs
	@DatasourceInterfaceID int,
	@DaysOldToExpire int
as
set nocount on
set transaction isolation level read uncommitted


if @DatasourceInterfaceID = 9
	select distinct
		stage.Load_ID
	from
		Staging.postings.PhoneLeads stage
		join Datafeeds.Extract.FetchDotCom#Request r
			on stage.ExtractRequestHandle = r.Handle
	where
		datediff(day, r.DateLoaded, getdate()) > @DaysOldToExpire

else if @DatasourceInterfaceID = 10
	select distinct
		stage.Load_ID
	from
		Staging.Merchandising.AutoTraderFetchError stage
		join Datafeeds.Extract.FetchDotCom#Request r
			on stage.ExtractRequestHandle = r.Handle
	where
		datediff(day, r.DateLoaded, getdate()) > @DaysOldToExpire

else if @DatasourceInterfaceID = 11
	select distinct
		stage.Load_ID
	from
		Staging.Merchandising.AutoTraderExSummary stage
		join Datafeeds.Extract.FetchDotCom#Request r
			on stage.ExtractRequestHandle = r.Handle
	where
		datediff(day, r.DateLoaded, getdate()) > @DaysOldToExpire
		
else if @DatasourceInterfaceID = 12
	select distinct
		stage.Load_ID
	from
		Staging.Merchandising.AutoTraderPerfSummary stage
		join Datafeeds.Extract.FetchDotCom#Request r
			on stage.ExtractRequestHandle = r.Handle
	where
		datediff(day, r.DateLoaded, getdate()) > @DaysOldToExpire		

else if @DatasourceInterfaceID = 13
	select distinct
		stage.Load_ID
	from
		Staging.Merchandising.AutoTraderVehSummary stage
		join Datafeeds.Extract.FetchDotCom#Request r
			on stage.ExtractRequestHandle = r.Handle
	where
		datediff(day, r.DateLoaded, getdate()) > @DaysOldToExpire

else if @DatasourceInterfaceID = 14
	select distinct
		stage.Load_ID
	from
		Staging.Merchandising.AutoTraderVehOnline stage
		join Datafeeds.Extract.FetchDotCom#Request r
			on stage.ExtractRequestHandle = r.Handle
	where
		datediff(day, r.DateLoaded, getdate()) > @DaysOldToExpire

else if @DatasourceInterfaceID = 2000
	select distinct
		stage.Load_ID
	from
		Staging.Merchandising.CarsDotComVehOnline stage
		join Datafeeds.Extract.FetchDotCom#Request r
			on stage.ExtractRequestHandle = r.Handle
	where
		datediff(day, r.DateLoaded, getdate()) > @DaysOldToExpire
		
else if @DatasourceInterfaceID = 2001
	select distinct
		stage.Load_ID
	from
		Staging.postings.CarsDotComPhoneLeads stage
		join Datafeeds.Extract.FetchDotCom#Request r
			on stage.ExtractRequestHandle = r.Handle
	where
		datediff(day, r.DateLoaded, getdate()) > @DaysOldToExpire		

else if @DatasourceInterfaceID = 2002
	select distinct
		stage.Load_ID
	from
		Staging.Merchandising.CarsDotComExSummary stage
		join Datafeeds.Extract.FetchDotCom#Request r
			on stage.ExtractRequestHandle = r.Handle
	where
		datediff(day, r.DateLoaded, getdate()) > @DaysOldToExpire		

else if @DatasourceInterfaceID = 2003
	select distinct
		stage.Load_ID
	from
		Staging.Merchandising.CarsDotComPerfSummary stage
		join Datafeeds.Extract.FetchDotCom#Request r
			on stage.ExtractRequestHandle = r.Handle
	where
		datediff(day, r.DateLoaded, getdate()) > @DaysOldToExpire

else if @DatasourceInterfaceID = 2004
	select distinct
		stage.Load_ID
	from
		Staging.Merchandising.CarsDotComVehSummary stage
		join Datafeeds.Extract.FetchDotCom#Request r
			on stage.ExtractRequestHandle = r.Handle
	where
		datediff(day, r.DateLoaded, getdate()) > @DaysOldToExpire		
		
else if @DatasourceInterfaceID = 2005
	select distinct
		stage.Load_ID
	from
		Staging.Merchandising.CarsDotComFeedError stage
		join Datafeeds.Extract.FetchDotCom#Request r
			on stage.ExtractRequestHandle = r.Handle
	where
		datediff(day, r.DateLoaded, getdate()) > @DaysOldToExpire		

else
	raiserror('Staging.merchandising.FetchSiteMetricsCleanup#FetchExpiredLoadIDs switches on DatasourceInterfaceId.  No code found for DatasourceInterfaceId %d!', 16, 1, @DatasourceInterfaceID)
		
go




