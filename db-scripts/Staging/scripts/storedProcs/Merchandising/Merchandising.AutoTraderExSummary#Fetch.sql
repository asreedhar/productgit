IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[AutoTraderExSummary#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Merchandising].[AutoTraderExSummary#Fetch]
GO

CREATE PROCEDURE Merchandising.AutoTraderExSummary#Fetch (
	@LoadID INT,
	@RequestID INT
) AS

SELECT 
		tbl.businessUnitID,
        tbl.month,
        tbl.AvgDayNew,
        tbl.AvgDayUsed,
        tbl.AvgDayTotal,
        tbl.TimesSeenInSearch,
        tbl.BannerAdImpressions,
        tbl.FindDealerSearchImpressions,
        tbl.TotalExposure,
        tbl.DetailPagesViewed,
        tbl.PrintableAdsRequested,
        tbl.ViewedWebsiteOrInventory,
        tbl.FindDealerClickThrus,
        tbl.BannerAdClickThrus,
        tbl.TotalActivity,
        tbl.CallsToDealership,
        tbl.EmailsToDealership,
        tbl.SecureCreditApplications,
        tbl.TotalTrackableProspects,
        tbl.TotalShoppersInArea,
        tbl.ViewedMaps,
        tbl.Load_ID,
        tbl.FetchDateInserted
FROM Staging.Merchandising.AutoTraderExSummary tbl
	INNER JOIN Datafeeds.Extract.FetchDotCom#Request r
		ON r.Handle = tbl.ExtractRequestHandle
where
	tbl.Load_ID = @LoadID
	and r.RequestId = @RequestID
	AND NOT EXISTS (
	SELECT 1
	FROM Datafeeds.Extract.FetchDotCom#Error err
	WHERE err.RequestID = r.RequestId
		AND err.BusinessUnitId = tbl.businessUnitID
		AND err.[Month] = tbl.[Month]
		AND err.ErrorTypeID in (4, 5)
	)
	
GO

