IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[Merchandising].[FetchDotComImportError#Check]')
                    AND type IN ( N'P', N'PC' ) ) 
    DROP PROCEDURE [Merchandising].[FetchDotComImportError#Check]
GO

CREATE PROCEDURE Merchandising.FetchDotComImportError#Check
    @RequestId INT ,
    @DatasourceInterfaceID INT
AS 
    BEGIN
        SET NOCOUNT ON

        DECLARE @PrimaryKeyViolators TABLE
            (
              RequestId INT NOT NULL ,
              BusinessUnitId INT NOT NULL ,
              Month SMALLDATETIME NULL , -- Month can be NULL for VehicleOnline
              DatasourceInterfaceId INT NOT NULL ,
              VIN VARCHAR(50) NULL ,
              StockNumber VARCHAR(50) NULL
            )

	-- Load Primary Key violations based on the type of datasource.
	-- VehicleSummary use VIN and StockNumber data, all others do not.

        IF @DatasourceInterfaceID in (13, 2004) -- VehicleSummary datasource
            BEGIN
                INSERT  INTO @PrimaryKeyViolators
                        ( RequestId ,
                          BusinessUnitId ,
                          Month ,
                          DatasourceInterfaceId ,
                          VIN ,
                          StockNumber
	                  )
                        SELECT  @RequestId ,
                                stg.businessUnitID ,
                                stg.month ,
                                @DatasourceInterfaceID ,
                                stg.VIN ,
                                stg.StockNumber
                        FROM    #Staging stg
                                INNER JOIN ( SELECT ExtractRequestHandle ,
                                                    businessUnitID ,
                                                    Month ,
                                                    VIN ,
                                                    StockNumber ,
                                                    COUNT(1) DupeCount
                                             FROM   #Staging
                                             GROUP BY ExtractRequestHandle ,
                                                    businessUnitID ,
                                                    Month ,
                                                    VIN ,
                                                    StockNumber
                                             HAVING COUNT(1) > 1
                                           ) qry ON stg.ExtractRequestHandle = qry.ExtractRequestHandle
                                                    AND stg.businessUnitID = qry.businessUnitID
                                                    AND stg.Month = qry.MONTH
                                                    AND stg.VIN = qry.VIN
                                                    AND stg.StockNumber = qry.StockNumber
            END
        ELSE IF @DatasourceInterfaceID in (14, 2000) -- VehicleOnline files do not contain MONTH
			BEGIN
				INSERT  INTO @PrimaryKeyViolators
                        ( RequestId ,
                          BusinessUnitId ,
                          DatasourceInterfaceId ,
                          VIN ,
                          StockNumber
	                  )
                        SELECT  @RequestId ,
                                stg.businessUnitID ,
                                @DatasourceInterfaceID ,
                                stg.VIN ,
                                stg.StockNumber
                        FROM    #Staging stg
                                INNER JOIN ( SELECT ExtractRequestHandle ,
                                                    businessUnitID ,
                                                    VIN ,
                                                    StockNumber ,
                                                    COUNT(1) DupeCount
                                             FROM   #Staging
                                             GROUP BY ExtractRequestHandle ,
                                                    businessUnitID ,
                                                    VIN ,
                                                    StockNumber
                                             HAVING COUNT(1) > 1
                                           ) qry ON stg.ExtractRequestHandle = qry.ExtractRequestHandle
                                                    AND stg.businessUnitID = qry.businessUnitID
                                                    AND stg.VIN = qry.VIN
                                                    AND stg.StockNumber = qry.StockNumber

			END
		ELSE
            BEGIN 
                INSERT  INTO @PrimaryKeyViolators
                        ( RequestId ,
                          BusinessUnitId ,
                          Month ,
                          DatasourceInterfaceId ,
                          VIN ,
                          StockNumber
                        )
                        SELECT  @RequestId ,
                                stg.businessUnitID ,
                                stg.month ,
                                @DatasourceInterfaceID ,
                                NULL ,
                                NULL
                        FROM    #Staging stg
                                INNER JOIN ( SELECT ExtractRequestHandle ,
                                                    businessUnitID ,
                                                    Month ,
                                                    COUNT(1) DupeCount
                                             FROM   #Staging
                                             GROUP BY ExtractRequestHandle ,
                                                    businessUnitID ,
                                                    Month
                                             HAVING COUNT(1) > 1
                                           ) qry ON stg.ExtractRequestHandle = qry.ExtractRequestHandle
                                                    AND stg.businessUnitID = qry.businessUnitID
                                                    AND stg.Month = qry.MONTH
            END
	   
        -- BusinessUnit/Period requested, but not received
        SELECT  rd.RequestId ,
                rd.businessUnitId ,
                rd.Month ,
                @DatasourceInterfaceID ,
                3 ,	-- ErrorTypeId
                NULL ,	-- VIN
                NULL	-- StockNumber
        FROM    Datafeeds.Extract.FetchDotCom#Request r
                JOIN Datafeeds.Extract.FetchDotCom#RequestDealer rd ON r.RequestId = rd.RequestId
                LEFT JOIN #Staging stg ON rd.businessUnitID = stg.BusinessUnitId
                                          AND (rd.Month = stg.MONTH OR (rd.Month IS NULL AND stg.MONTH IS NULL))
                                          AND r.Handle = stg.ExtractRequestHandle
        WHERE   stg.BusinessUnitId IS NULL
			  AND r.RequestId = @RequestId
        UNION
        -- BusinessUnit/Period received, but not requested
        SELECT  @RequestId ,
                stg.businessUnitID ,
                stg.month ,
                @DatasourceInterfaceID ,
                4 ,	-- ErrorTypeId
                NULL ,	-- VIN
                NULL	-- StockNumber
        FROM    Datafeeds.Extract.FetchDotCom#Request r
                JOIN Datafeeds.Extract.FetchDotCom#RequestDealer rd ON r.RequestId = rd.RequestId
                RIGHT JOIN #Staging stg ON rd.businessUnitID = stg.BusinessUnitId
                                           AND (rd.Month = stg.MONTH OR (rd.Month IS NULL AND stg.MONTH IS NULL))
                                           AND r.Handle = stg.ExtractRequestHandle
        WHERE   rd.BusinessUnitId IS NULL
        UNION
        -- Would-Be Primary Key Violations
        SELECT  RequestId ,
                BusinessUnitId ,
                Month ,
                DatasourceInterfaceId ,
                5 ,
                VIN ,
                StockNumber
        FROM    @PrimaryKeyViolators

 
    END


