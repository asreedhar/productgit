IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Merchandising.AutoTraderPerfSummary#Fetch') AND type in (N'P', N'PC'))
DROP PROCEDURE Merchandising.AutoTraderPerfSummary#Fetch
GO

CREATE PROCEDURE Merchandising.AutoTraderPerfSummary#Fetch (
	@LoadID INT,
	@RequestID INT
) AS

SELECT
		tbl.businessUnitID,
        tbl.month,
        tbl.CallsToDealership,
        tbl.ViewedWebsiteOrInventory,
        tbl.SecureCreditApplications,
        tbl.UsedVehiclesSearched,
        tbl.UsedDetailPagesViewed,
        tbl.UsedViewedMaps,
        tbl.UsedEmailsToDealership,
        tbl.UsedPrintableAds,
        tbl.NewVehiclesSearched,
        tbl.NewDetailPagesViewed,
        tbl.NewViewedMaps,
        tbl.NewEmailsToDealership,
        tbl.NewPrintableAds,
        tbl.SearchImpressions,
        tbl.ClickThrus,
        tbl.ViewedMaps,
        tbl.EmailsToDealership,
        tbl.AskedForMoreDetails,
        tbl.UsedAvgDailyVehicles,
        tbl.UsedPctWithPrice,
        tbl.UsedPctWithComments,
        tbl.UsedPctWithPhotos,
        tbl.UsedPctMultiplePhotos,
        tbl.UsedPctSinglePhoto,
        tbl.UsedPrimarySource,
        tbl.NewAvgDailyVehicles,
        tbl.NewPctWithPrice,
        tbl.NewPctWithComments,
        tbl.NewPctWithPhotos,
        tbl.NewPctMultiplePhotos,
        tbl.NewPctSinglePhoto,
        tbl.NewPrimarySource,
        tbl.PartnerSelectedMarkets,
        tbl.PartnerBannerAdImpressions1,
        tbl.PartnerBannerAdImpressions2,
        tbl.PartnerBannerAdClickThrus1,
        tbl.PartnerBannerAdClickThrus2,
        tbl.FetchDateInserted,
        tbl.Load_ID
FROM Staging.Merchandising.AutoTraderPerfSummary tbl
	INNER JOIN Datafeeds.Extract.FetchDotCom#Request r
		ON r.Handle = tbl.ExtractRequestHandle
WHERE
	tbl.Load_ID = @LoadID
	AND r.RequestId = @RequestID
	AND NOT EXISTS (
	SELECT 1
	FROM Datafeeds.Extract.FetchDotCom#Error err
	WHERE err.RequestID = r.RequestId
		AND err.BusinessUnitId = tbl.businessUnitID
		AND err.[Month] = tbl.[Month]
		AND err.ErrorTypeID in (4, 5))
GO

