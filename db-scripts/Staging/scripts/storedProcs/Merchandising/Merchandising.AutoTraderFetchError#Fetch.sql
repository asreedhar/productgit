IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Merchandising.AutoTraderFetchError#Fetch') AND type in (N'P', N'PC'))
DROP PROCEDURE Merchandising.AutoTraderFetchError#Fetch
GO

CREATE PROCEDURE Merchandising.AutoTraderFetchError#Fetch (
	@LoadID INT,
	@RequestID INT
) AS

SELECT tbl.*
FROM Staging.Merchandising.AutoTraderFetchError tbl
	INNER JOIN Datafeeds.Extract.FetchDotCom#Request r
		ON r.Handle = tbl.ExtractRequestHandle
WHERE 
	tbl.Load_ID = @LoadID
	AND r.RequestId = @RequestID
	AND ErrorType = 'Login Failed'
	AND NOT EXISTS (
	SELECT 1
	FROM Datafeeds.Extract.FetchDotCom#Error err
	WHERE err.RequestID = r.RequestId
		AND err.BusinessUnitId = tbl.businessUnitID
		AND err.[Month] = tbl.[Month]
		AND err.ErrorTypeID = 4)
GO
