IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Merchandising].[CarsDotComExSummary#Fetch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [Merchandising].[CarsDotComExSummary#Fetch]
GO

CREATE PROCEDURE Merchandising.CarsDotComExSummary#Fetch (
	@LoadID INT,
	@RequestID INT
) AS

SELECT 
	tbl.businessUnitID,
	tbl.month,
	tbl.InSearchResult,
	tbl.SearchToDetailRate,
	tbl.ViewDetail,
	tbl.Contacts,
	tbl.Sold,
	tbl.FetchDateInserted
FROM Staging.Merchandising.CarsDotComExSummary tbl
	INNER JOIN Datafeeds.Extract.FetchDotCom#Request r
		ON r.Handle = tbl.ExtractRequestHandle
where
	tbl.Load_ID = @LoadID
	and r.RequestId = @RequestID
	AND NOT EXISTS (
	SELECT 1
	FROM Datafeeds.Extract.FetchDotCom#Error err
	WHERE err.RequestID = r.RequestId
		AND err.BusinessUnitId = tbl.businessUnitID
		AND err.[Month] = tbl.[Month]
		AND err.ErrorTypeID in (4, 5)
	)
	
GO

