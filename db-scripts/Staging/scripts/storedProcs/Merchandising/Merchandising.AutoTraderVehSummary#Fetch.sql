IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Merchandising.AutoTraderVehSummary#Fetch') AND type in (N'P', N'PC'))
DROP PROCEDURE Merchandising.AutoTraderVehSummary#Fetch
GO

CREATE PROCEDURE Merchandising.AutoTraderVehSummary#Fetch (
	@LoadID INT,
	@RequestID INT
) AS

SELECT tbl.*
FROM Staging.Merchandising.AutoTraderVehSummary tbl
	INNER JOIN Datafeeds.Extract.FetchDotCom#Request r
		ON r.Handle = tbl.ExtractRequestHandle
WHERE
	tbl.Load_ID = @LoadID
	AND r.RequestId = @RequestID
	AND NOT EXISTS (
	SELECT 1
	FROM Datafeeds.Extract.FetchDotCom#Error err
	WHERE err.RequestID = r.RequestId
		AND err.BusinessUnitId = tbl.businessUnitID
		AND err.[Month] = tbl.[Month]
		AND err.VIN = tbl.VIN
		AND err.StockNumber = tbl.StockNumber)
	AND NOT EXISTS (
	SELECT 1
	FROM Datafeeds.Extract.FetchDotCom#Error err
	WHERE err.RequestID = r.RequestId
		AND err.BusinessUnitId = tbl.businessUnitID
		AND err.[Month] = tbl.[Month]
		AND err.ErrorTypeID = 4)
GO

