if object_id('Merchandising.FetchDotComImport#GetStaging') is not null 
	drop procedure Merchandising.FetchDotComImport#GetStaging
go



create procedure Merchandising.FetchDotComImport#GetStaging
	@LoadID int,
	@RequestId int,
	@DatasourceInterfaceID int
as 
set nocount on

declare @RequestHandle uniqueidentifier

select @RequestHandle = Handle
from
Datafeeds.Extract.FetchDotCom#Request
where RequestId = @RequestId


 
if @DatasourceInterfaceID = 9 -- CallTracking	%CallTracking%.csv	9
begin
	return -- nothing to do for this datasource
end

else if @DatasourceInterfaceID = 10 -- ErrorReport	%ErrorReport%.csv	10
begin
	return -- nothing to do for this datasource
end

else if @DatasourceInterfaceID = 11 -- ExecutiveSummary	%ExecutiveSummary%.csv	11
begin
	select
		stage.businessUnitID,
		stage.month,
		stage.ExtractRequestHandle,
		null,
		null
	from
		Staging.Merchandising.AutoTraderExSummary stage
		join Datafeeds.Extract.FetchDotCom#Request r
			on stage.ExtractRequestHandle = r.Handle
	where
		stage.Load_ID = @LoadID
		and stage.ExtractRequestHandle = @RequestHandle
end

else if @DatasourceInterfaceID = 12 -- PerformanceSummary	%PerformanceSummary%.csv	12
begin
	select
		stage.businessUnitID,
		stage.month,
		stage.ExtractRequestHandle,
		null,
		null
	from
		Staging.Merchandising.AutoTraderPerfSummary stage
		join Datafeeds.Extract.FetchDotCom#Request r
			on stage.ExtractRequestHandle = r.Handle
	where
		stage.Load_ID = @LoadID
		and stage.ExtractRequestHandle = @RequestHandle 
end

else if @DatasourceInterfaceID = 13 -- VehicleSummary	%VehicleSummary%.csv	13
begin
	select
		stage.businessUnitID,
		stage.month,
		stage.ExtractRequestHandle,
		stage.VIN,
		stage.StockNumber
	from
		Staging.Merchandising.AutoTraderVehSummary stage
		join Datafeeds.Extract.FetchDotCom#Request r
			on stage.ExtractRequestHandle = r.Handle
	where
		stage.Load_ID = @LoadID
		and stage.ExtractRequestHandle = @RequestHandle 
end 

else if @DatasourceInterfaceID = 14 -- VehicleOnline	%VehicleOnline%.csv	14
begin
	select
		stage.businessUnitID,
		null,
		stage.ExtractRequestHandle,
		stage.VIN,
		stage.StockNumber
	from
		Staging.Merchandising.AutoTraderVehOnline stage
		join Datafeeds.Extract.FetchDotCom#Request r
			on stage.ExtractRequestHandle = r.Handle
	where
		stage.Load_ID = @LoadID
		and stage.ExtractRequestHandle = @RequestHandle 
end 

else if @DatasourceInterfaceID = 2000 -- CarsDotComVehOnline Cars%VehicleOnline%.csv 2000
begin
	select
		stage.businessUnitID,
		null,
		stage.ExtractRequestHandle,
		stage.VIN,
		stage.StockNumber
	from
	Staging.Merchandising.CarsDotComVehOnline stage
	join Datafeeds.Extract.FetchDotCom#Request r
		on stage.ExtractRequestHandle = r.Handle
	where
		stage.Load_ID = @LoadID
		and stage.ExtractRequestHandle = @RequestHandle 
end 
 
else if @DatasourceInterfaceID = 2001 -- Cars-CallTracking	Cars%CallTracking%.csv	2001
begin
	return -- nothing to do for this datasource
end 

else if @DatasourceInterfaceID = 2002 -- Cars-ExecSummary	Cars%ExecutiveSummary%.csv	2002
begin
	select
		stage.businessUnitID,
		stage.month,
		stage.ExtractRequestHandle,
		null,
		null
	from
		Staging.Merchandising.CarsDotComExSummary stage
		join Datafeeds.Extract.FetchDotCom#Request r
			on stage.ExtractRequestHandle = r.Handle
	where
		stage.Load_ID = @LoadID
		and stage.ExtractRequestHandle = @RequestHandle
end 

else if @DatasourceInterfaceID = 2003 -- Cars-PerfSummary	Cars%PerformanceSummary%.csv	2003
begin
	select
		stage.businessUnitID,
		stage.month,
		stage.ExtractRequestHandle,
		null,
		null
	from
		Staging.Merchandising.CarsDotComPerfSummary stage
		join Datafeeds.Extract.FetchDotCom#Request r
			on stage.ExtractRequestHandle = r.Handle
	where
		stage.Load_ID = @LoadID
		and stage.ExtractRequestHandle = @RequestHandle
end 
else if @DatasourceInterfaceID = 2004 -- Cars-VehicleSummary	Cars%VehicleSummary%.csv	2004
begin
	select
		stage.businessUnitID,
		stage.month,
		stage.ExtractRequestHandle,
		stage.VIN,
		stage.StockNum
	from
	Staging.Merchandising.CarsDotComVehSummary stage
	join Datafeeds.Extract.FetchDotCom#Request r
		on stage.ExtractRequestHandle = r.Handle
	where
		stage.Load_ID = @LoadID
		and stage.ExtractRequestHandle = @RequestHandle 
end

else if @DatasourceInterfaceID = 2005 -- Cars-FeedError Cars%FeedError%.csv	2005
begin
	return -- nothing to do for this datasource
end

else
begin
	raiserror('@DatasourceInterfaceID %d has no validation code written!', 16, 1, @DatasourceInterfaceID)
end


go