IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Merchandising.CarsDotComVehSummary#Fetch') AND type in (N'P', N'PC'))
DROP PROCEDURE Merchandising.CarsDotComVehSummary#Fetch
GO

CREATE PROCEDURE Merchandising.CarsDotComVehSummary#Fetch (
	@LoadID INT,
	@RequestID INT
) AS

select
	tbl.businessUnitID,
	tbl.month,
	tbl.StockNum,
	tbl.Year,
	tbl.Make,
	Vin = left(tbl.VIN, 25),
	[Type] = left(tbl.Type, 5),
	tbl.Price,
	tbl.Age,
	tbl.InSearchResult,
	tbl.DetailViewed,
	tbl.VideoViewed,
	tbl.AdsPrinted,
	tbl.MapViewed,
	tbl.ClickThru,
	tbl.EmailSent,
	tbl.Chat,
	tbl.Total,
	tbl.PhotoCount,
	tbl.FetchDateInserted
FROM Staging.Merchandising.CarsDotComVehSummary tbl
	INNER JOIN Datafeeds.Extract.FetchDotCom#Request r
		ON r.Handle = tbl.ExtractRequestHandle
WHERE
	tbl.Load_ID = @LoadID
	AND r.RequestId = @RequestID
	AND NOT EXISTS (
	SELECT 1
	FROM Datafeeds.Extract.FetchDotCom#Error err
	WHERE err.RequestID = r.RequestId
		AND err.BusinessUnitId = tbl.businessUnitID
		AND err.[Month] = tbl.[Month]
		AND err.VIN = tbl.VIN
		AND err.StockNumber = tbl.StockNum)
	AND NOT EXISTS (
	SELECT 1
	FROM Datafeeds.Extract.FetchDotCom#Error err
	WHERE err.RequestID = r.RequestId
		AND err.BusinessUnitId = tbl.businessUnitID
		AND err.[Month] = tbl.[Month]
		AND err.ErrorTypeID = 4)
GO

