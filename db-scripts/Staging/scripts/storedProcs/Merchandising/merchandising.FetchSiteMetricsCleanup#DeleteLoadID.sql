if object_id('merchandising.FetchSiteMetricsCleanup#DeleteLoadID') is not null
	drop procedure merchandising.FetchSiteMetricsCleanup#DeleteLoadID
go


/*
	select * from Datafeeds.dbo.DatafeedFileType where DatafeedCode = 'FetchDotCom-Response' order by DatasourceInterfaceID	
*/

create procedure merchandising.FetchSiteMetricsCleanup#DeleteLoadID
	@DatasourceInterfaceID int,
	@Load_ID int
as
set nocount on
set transaction isolation level read uncommitted


if @DatasourceInterfaceID = 9
	delete Staging.postings.PhoneLeads where Load_ID = @Load_ID

else if @DatasourceInterfaceID = 10
	delete Staging.Merchandising.AutoTraderFetchError where Load_ID = @Load_ID

else if @DatasourceInterfaceID = 11
	delete Staging.Merchandising.AutoTraderExSummary where Load_ID = @Load_ID
		
else if @DatasourceInterfaceID = 12
	delete Staging.Merchandising.AutoTraderPerfSummary where Load_ID = @Load_ID

else if @DatasourceInterfaceID = 13
	delete Staging.Merchandising.AutoTraderVehSummary where Load_ID = @Load_ID

else if @DatasourceInterfaceID = 14
	delete Staging.Merchandising.AutoTraderVehOnline where Load_ID = @Load_ID

else if @DatasourceInterfaceID = 2000
	delete Staging.Merchandising.CarsDotComVehOnline where Load_ID = @Load_ID
		
else if @DatasourceInterfaceID = 2001
	delete Staging.postings.CarsDotComPhoneLeads where Load_ID = @Load_ID

else if @DatasourceInterfaceID = 2002
	delete Staging.Merchandising.CarsDotComExSummary where Load_ID = @Load_ID

else if @DatasourceInterfaceID = 2003
	delete Staging.Merchandising.CarsDotComPerfSummary where Load_ID = @Load_ID

else if @DatasourceInterfaceID = 2004
	delete Staging.Merchandising.CarsDotComVehSummary where Load_ID = @Load_ID
		
else if @DatasourceInterfaceID = 2005
	delete Staging.Merchandising.CarsDotComFeedError where Load_ID = @Load_ID

else
	raiserror('Staging.merchandising.FetchSiteMetricsCleanup#DeleteLoadID switches on DatasourceInterfaceId.  No code found for DatasourceInterfaceId %d!', 16, 1, @DatasourceInterfaceID)
		
go




