IF OBJECTPROPERTY(OBJECT_ID('GetAuto.Image#Extract'), 'isProcedure') = 1
	DROP PROCEDURE GetAuto.Image#Extract

GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
/******************************************************************************
**
**  Procedure: GetAuto.Image#Extract
**  Description: Reads staged GetAuto data parsing the Photos field for available images.
**		Based on the value in the field and whether or not the photo has been seen
**		before (based on url/provider combination), the entry in IMT.dbo.Photos is
**		either created or updated.  This process is restricted to active inventory
**		items for active BusinessUnits and an appropriate entry is created in 
**		IMT.dbo.InventoryPhoto for new images.
**
**  Return values:  none
** 
**  Input parameters:  @OriginatingLogId int --optional
**			
**  Output parameters:  
**
*******************************************************************************
**  Version History
*******************************************************************************
**  Date:	Author: Description:
**  ----------  -------	-------------------------------------------------------
**  2009.04.23  ffoiii	Procedure created.
**  2010.11.05  wgh	Added deletes, reformatted
**  2011.01.07  wgh	Added SequenceNumber
**  2011.04.19  wgh	Changed status code #7 update statement to reflect 
**			usage of PhotoUpdated column by the photo processor
**  2011.05.09  wgh     Added de-duping to extract.
**  2011.05.12	bjr	Changed status code #7 update statement to set PhotoUpdated
**			to 0 to force rescrape because GetAuto does not update their
**			If-Modified-Since http header consistently
**  2011.08.16	wgh	Updated for AULtec integration		
**  2012.05.11	dbo	Updated join to GetAuto.Vehicle to filter on dealer's 
**			photo provider dealer code	
**		wgh	Replaced ref to IMT.dbo.PhotoProviderDealership with
**			DBASTAT.Staging.Photo#ProviderDealershipCodeProperties for
**			consistency w/the standard photo loader	
**  2012.06.08  wgh	Added empty-extract failsafe terminator
**  2012.08.22  wgh	Added UPDATE of IMT.dbo.Inventory.ImageModifiedDate
**  2012.10.14  wgh	Move transaction to wrap only the insert/update/delete ops
**  2014.04.01  wgh 	Bugzid 29630: Fix redundant IP inserts on VINs duplicated 
**			over multiple active inventory rows
*******************************************************************************/
CREATE PROCEDURE GetAuto.Image#Extract
@OriginatingLogId INT = NULL,
@Debug INT = 0
AS 
SET NOCOUNT ON
DECLARE @Rowcount INT
DECLARE @Err INT
DECLARE @bLocalTran BIT
DECLARE @sProcname SYSNAME
DECLARE @bTrue BIT
DECLARE @bFalse BIT
DECLARE	@sErrMsg VARCHAR(255)

SELECT  @sProcName = OBJECT_NAME(@@procid),
	@bTrue = 1,
	@bFalse = 0,
	@sErrMsg = ''

SET @bLocalTran = @bFalse

-- NEED TO RETHINK THE LOCAL TRANSACTION AS THIS RUNS DURING THE DAY

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

--END DEFAULT ENVIRONMENT AND VARIABLE SETTINGS

--PROC BODY HERE

BEGIN TRY

	--additional required variables
	DECLARE @PhotoTypeId INT
	DECLARE @PhotoProviderId INT
	DECLARE @ProviderUpdateDate DATETIME
	DECLARE @msg VARCHAR(1000)

	--initialize variables
	SELECT  @PhotoTypeId = PhotoTypeId
	FROM	IMT.dbo.PhotoTypes t
	WHERE   t.Description = 'Inventory'
	
	IF ( @PhotoTypeId IS NULL ) 
		RAISERROR('Unable to determine PhotoTypeId for type ''Inventory''.',16,1)
	
	SELECT  @PhotoProviderId = PhotoProviderId
	FROM	IMT.dbo.PhotoProvider_ThirdPartyEntity
	WHERE   DataFeedCode = 'GetAutoImage'
	
	IF ( @PhotoProviderId IS NULL ) 
		RAISERROR('Unable to determine DataFeedCode for PhotoProvider ''GetAutoImage''.',16,1)

	SELECT  @ProviderUpdateDate = LastStatusChange
	FROM	market.listing.provider
	WHERE   Provider.Description = 'GetAuto'
	
	IF ( @ProviderUpdateDate IS NULL ) 
		RAISERROR('Unable to determine last provider update date for provider ''GetAuto''',16,1)	

	SELECT  @msg = 'Starting: GetAutoImage processor' + CASE WHEN @Debug = @bTrue THEN ' Debug mode, no records will be modified.' ELSE '' END
	EXEC DBASTAT.dbo.Log_Event 'I', @sProcName, @msg, @OriginatingLogId OUTPUT


	CREATE TABLE #image (
		  InventoryID		INT,
		  PhotoStatusId		INT,
		  PhotoURL		VARCHAR(255),
		  OriginalPhotoId	INT,
		  SequenceNumber	SMALLINT,
		  DatePhotosUpdated	SMALLDATETIME,
		  _DeltaFlag		TINYINT
		)

	--------------------------------------------------------------------------------
	SET @msg = 'Generate list of all available images'
	--------------------------------------------------------------------------------

	INSERT  
	INTO	#image (InventoryID, PhotoURL, OriginalPhotoId, SequenceNumber, DatePhotosUpdated, _DeltaFlag )
	
	SELECT  IA.InventoryID,
		imgurl.url,
		P.PhotoId,
		imgurl.SequenceNumber,
		gav.DatePhotosUpdated,
		CASE WHEN P.PhotoId IS NULL 
		     THEN 1							-- Add
		     WHEN imgurl.SequenceNumber <> P.SequenceNumber 
		     	  OR I.ImageModifiedDate <> CAST(GAV.DatePhotosUpdated AS SMALLDATETIME)	-- Update 
		     THEN 2
		     ELSE 0							-- no operation (needed to determine deletes)
		END

		
	FROM	IMT.dbo.BusinessUnit BU
		INNER JOIN DBASTAT.Staging.Photo#ProviderDealershipCodeProperties ppd ON ppd.BusinessUnitId = bu.BusinessUnitId
		INNER JOIN FLDW.dbo.InventoryActive IA ON IA.businessunitid = bu.businessunitid
		INNER JOIN IMT.dbo.Inventory I ON IA.BusinessUnitID = I.BusinessUnitID AND IA.InventoryID = I.InventoryID		-- need this for ImageModifiedDate, should add to FLDW
		 
		INNER JOIN IMT.dbo.Vehicle v ON V.VehicleID = IA.VehicleID
		INNER JOIN Staging.GetAuto.Vehicle gav ON gav.VIN = V.VIN 
								AND CASE WHEN ISNUMERIC(PPD.PhotoProvidersDealershipCode) = 1
									 THEN CAST(PPD.PhotoProvidersDealershipCode AS INT) 
									 ELSE 0
								     END = GAV.DealerID
								     		
		CROSS APPLY Staging.GetAuto.ImageURL#Generate(gav.DealerID, CAST(gav.Photos AS SMALLINT), gav.vin, 'c') imgurl
		LEFT JOIN (	IMT.dbo.InventoryPhotos ip
				INNER JOIN IMT.dbo.Photos p ON ip.photoid = P.photoid
				) ON ip.InventoryID = IA.InventoryID
					AND P.PhotoProviderId = @PhotoProviderId
					AND P.OriginalPhotoUrl = imgurl.url
	WHERE   bu.active = 1
		AND ppd.PhotoProviderId = @PhotoProviderid
			
	SELECT  @RowCount = @@RowCount

	IF ( @RowCount = 0 ) 
		RAISERROR('Extract from staging is empty; terminating process as a failsafe.', 16,1)
		
	SELECT  @msg = CAST(@Rowcount AS VARCHAR) + ' rows inserted into extract.'
	EXEC dbastat.dbo.Log_Event 'I', @sprocname, @msg, @OriginatingLogId, @Debug			

		
	IF ( @Debug = @bTrue ) 
		SELECT  *
		FROM	#image
	ELSE  BEGIN

		BEGIN TRAN @sProcname
		
		----------------------------------------------------------------------------------------
		SET @msg = 'Mark IMT.dbo.Photos for updates'
		----------------------------------------------------------------------------------------
		
		UPDATE  IMT.dbo.Photos
		
		SET	SequenceNumber	= i.SequenceNumber,
			IsPrimaryPhoto	= CASE WHEN i.SequenceNumber = 1 THEN 1 ELSE 0 end
			
		FROM	IMT.dbo.Photos p
			INNER JOIN #image i ON i.OriginalPhotoId = P.PhotoId
			
		WHERE	_DeltaFlag = 2
		
		SELECT  @RowCount = @@RowCount

		SELECT  @msg = CAST(@Rowcount AS VARCHAR) + ' images updated.'
		EXEC dbastat.dbo.Log_Event 'I', @sprocname, @msg, @OriginatingLogId, @Debug

		----------------------------------------------------------------------------------------
		SET @msg = 'Determine delete candidates'
		----------------------------------------------------------------------------------------
		
		INSERT
		INTO	#image (InventoryID, OriginalPhotoId, _DeltaFlag)
		SELECT	IP.InventoryID, P.PhotoID, 3
		FROM	IMT.dbo.InventoryPhotos IP
			INNER JOIN IMT.dbo.Photos P ON IP.PhotoID = P.PhotoID
			INNER JOIN FLDW.dbo.InventoryActive IA ON IP.InventoryID = IA.InventoryID			
		WHERE	P.PhotoProviderID = @PhotoProviderId
			AND NOT EXISTS (SELECT	1
					FROM	#image I
					WHERE	IP.InventoryID = I.InventoryID
						AND P.PhotoID = I.OriginalPhotoId
						)	
		SELECT  @RowCount = @@RowCount

		SELECT  @msg = CAST(@Rowcount AS VARCHAR) + ' delete candidates found.'
		EXEC dbastat.dbo.Log_Event 'I', @sprocname, @msg, @OriginatingLogId, @Debug
		
		----------------------------------------------------------------------------------------
		SET @msg = 'Process deletes for InventoryPhotos'
		----------------------------------------------------------------------------------------
						
		DELETE	IP		
		FROM	IMT.dbo.InventoryPhotos IP
			INNER JOIN #image I ON IP.InventoryID = I.InventoryID 
						AND IP.PhotoID = I.OriginalPhotoId
		WHERE	I._DeltaFlag = 3
		
		SELECT  @RowCount = @@RowCount

		SELECT  @msg = CAST(@Rowcount AS VARCHAR) + ' row(s) deleted from InventoryPhotos.'
		EXEC dbastat.dbo.Log_Event 'I', @sprocname, @msg, @OriginatingLogId, @Debug
									
		----------------------------------------------------------------------------------------
		SET @msg = 'Process deletes for Photos'
		----------------------------------------------------------------------------------------
											
		DELETE	P		
		FROM	IMT.dbo.Photos P
			INNER JOIN #image I ON P.PhotoID = I.OriginalPhotoId
		WHERE	I._DeltaFlag = 3
			AND NOT EXISTS (SELECT	1
					FROM	IMT.dbo.InventoryPhotos IP
					WHERE	I.OriginalPhotoId = IP.PhotoID
					)
											
		SELECT  @RowCount = @@RowCount

		SELECT  @msg = CAST(@Rowcount AS VARCHAR) + ' row(s) deleted from Photos.'
		EXEC dbastat.dbo.Log_Event 'I', @sprocname, @msg, @OriginatingLogId, @Debug
		
		----------------------------------------------------------------------------------------
		SET @msg = 'Insert into IMT.dbo.Photos'
		----------------------------------------------------------------------------------------
		
		CREATE TABLE #InventoryPhotos(PhotoID INT, InventoryID INT)
		
		INSERT  
		INTO	IMT.dbo.Photos (PhotoTypeId, PhotoProviderId, PhotoStatusCode,PhotoURL, OriginalPhotoURL, DateCreated, IsPrimaryPhoto, SequenceNumber, LoadID )
		
		OUTPUT 	INSERTED.PhotoID, INSERTED.LoadID INTO #InventoryPhotos	-- can't directly insert into IMT.dbo.InventoryPhotos
		
		SELECT  @PhotoTypeId,
			@PhotoProviderId,
			3,
			i.PhotoUrl,
			i.PhotoURL,
			GETDATE(),
			CASE WHEN SequenceNumber = 1 THEN 1 ELSE 0 END,
			SequenceNumber,
			i.InventoryID		-- overload LoadID to store the InventoryID since there isn't a LoadID for generated URLs
		FROM	#image i
		WHERE   _DeltaFlag = 1
		
		SELECT  @RowCount = @@RowCount
		

		SELECT  @msg = CAST(@Rowcount AS VARCHAR) + ' images inserted.'
		EXEC dbastat.dbo.Log_Event 'I', @sprocname, @msg, @OriginatingLogId, @Debug

		----------------------------------------------------------------------------------------
		SET @msg = 'Insert into IMT.dbo.InventoryPhotos'
		----------------------------------------------------------------------------------------
		
		INSERT  
		INTO	IMT.dbo.InventoryPhotos ( InventoryID, PhotoId )
		SELECT  IP.InventoryID,
			IP.PhotoId
		FROM	#InventoryPhotos IP 
				
		SELECT  @RowCount = @@RowCount
		
		SELECT  @msg = CAST(@Rowcount AS VARCHAR) + ' relationships created in IMT.dbo.InventoryPhotos.'
		EXEC DBASTAT.dbo.Log_Event 'I', @sprocname, @msg, @OriginatingLogId, @Debug

		----------------------------------------------------------------------------------------
		SET @msg = 'Update IMT.dbo.Photos.LoadID'
		----------------------------------------------------------------------------------------
		
		UPDATE	P
		SET	LoadID	= NULL
		FROM	IMT.dbo.Photos P
			INNER JOIN #InventoryPhotos IP ON P.PhotoID = IP.PhotoID

		SELECT  @msg = @msg + ', ' + CAST(@@RowCount AS VARCHAR) + ' row(s) affected.'
		EXEC DBASTAT.dbo.Log_Event 'I', @sprocname, @msg, @OriginatingLogId, @Debug
				
		----------------------------------------------------------------------------------------
		SET @msg = 'Update IMT.dbo.Inventory.ImageModifiedDate'
		----------------------------------------------------------------------------------------
		
		UPDATE	I WITH (ROWLOCK)
		SET	ImageModifiedDate = P.DatePhotosUpdated
		FROM	IMT.dbo.Inventory I 
			INNER JOIN #image P ON I.InventoryID = P.InventoryID
		WHERE	_DeltaFlag IN (1,2)
		
		SELECT  @msg = CAST(@@RowCount AS VARCHAR) + ' rows updated in IMT.dbo.Inventory.'
		EXEC DBASTAT.dbo.Log_Event 'I', @sprocname, @msg, @OriginatingLogId, @Debug
						 
						 
		------------------------------------------------------------------------------------------------
		-- Success
		------------------------------------------------------------------------------------------------
	
		COMMIT TRAN @sProcName
		
	END

	SELECT  @msg = 'Finished: GetAutoImage processor.' + CASE WHEN @Debug = @bTrue THEN ' Debug mode, no records modified.' ELSE '' END
	EXEC dbastat.dbo.Log_Event 'I', @sprocname, @msg, @OriginatingLogId, @Debug



END TRY

------------------------------------------------------------------------------------------------
-- Failure
------------------------------------------------------------------------------------------------

BEGIN CATCH

	IF XACT_STATE() <> 0
		ROLLBACK TRAN @sProcName

	EXEC dbo.sp_ErrorHandler @Message = @sErrMsg

END CATCH
