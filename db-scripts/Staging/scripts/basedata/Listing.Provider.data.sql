--	These providers were created in SPOC

IF NOT EXISTS (SELECT 1 FROM Listing.Provider WHERE 1=1 AND [ProviderID] = 29 )
	INSERT Listing.Provider ([ProviderID],[DatafeedCode],[ActiveLoadID],[ProviderFeedTypeID],[ProviderCode],[ResponseFilePending],[LastUpdateDT],[LastUpdateUser],[DeleteThresholdPct],[IsVideoUrlProvider],[OptionDelimiter],[ImageUrlDelimiter])
	VALUES (29,'Spidey',6036711,2,'Spidey',0,'Dec  4 2014 12:00AM','FIRSTLOOK\sqlsvcbeta',33,0,NULL,NULL)
ELSE
	UPDATE Listing.Provider
	SET  [DatafeedCode] = 'Spidey',[ActiveLoadID] = 0,[ProviderFeedTypeID] = 2,[ProviderCode] = 'Spidey',[ResponseFilePending] = 0,[LastUpdateDT] = 'Dec  4 2014 12:00AM',[LastUpdateUser] = 'FIRSTLOOK\sqlsvcbeta',[DeleteThresholdPct] = 33,[IsVideoUrlProvider] = 0,[OptionDelimiter] = NULL,[ImageUrlDelimiter] = NULL 
	WHERE 1=1  AND [ProviderID] = 29 

IF NOT EXISTS (SELECT 1 FROM Listing.Provider WHERE 1=1 AND [ProviderID] = 30 )
	INSERT Listing.Provider ([ProviderID],[DatafeedCode],[ActiveLoadID],[ProviderFeedTypeID],[ProviderCode],[ResponseFilePending],[LastUpdateDT],[LastUpdateUser],[DeleteThresholdPct],[IsVideoUrlProvider],[OptionDelimiter],[ImageUrlDelimiter])
	VALUES (30,'MarketCheck',6038409,2,'MarketCheck',0,'Dec  4 2014  1:29PM','FIRSTLOOK\sqlsvcbeta',NULL,0,NULL,NULL)
ELSE
	UPDATE Listing.Provider
	SET  [DatafeedCode] = 'MarketCheck',[ActiveLoadID] = 0,[ProviderFeedTypeID] = 2,[ProviderCode] = 'MarketCheck',[ResponseFilePending] = 0,[LastUpdateDT] = 'Dec  4 2014  1:29PM',[LastUpdateUser] = 'FIRSTLOOK\sqlsvcbeta',[DeleteThresholdPct] = NULL,[IsVideoUrlProvider] = 0,[OptionDelimiter] = NULL,[ImageUrlDelimiter] = NULL 
	WHERE 1=1  AND [ProviderID] = 30 

