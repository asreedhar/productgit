ALTER TABLE Staging.Listing.Vehicle	ALTER COLUMN DealerCode VARCHAR(50) NULL
ALTER TABLE Staging.Listing.Vehicle	ALTER COLUMN ExteriorColorCode VARCHAR(20) NULL
ALTER TABLE Staging.Listing.Vehicle	ALTER COLUMN InteriorColorCode VARCHAR(20) NULL

UPDATE Staging.Listing.Provider
	SET IsVideoUrlProvider=1
WHERE Providerid=6

DELETE Staging.Listing.Provider WHERE ProviderID=25