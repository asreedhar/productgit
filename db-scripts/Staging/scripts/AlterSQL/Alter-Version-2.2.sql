--------------------------------------------------------------------------------
--	DataVersion
--------------------------------------------------------------------------------

ALTER TABLE KBB.DataVersion ADD VehicleTypeId INT NOT NULL
ALTER TABLE KBB.DataVersion ADD StartDate SMALLDATETIME NOT NULL
ALTER TABLE KBB.DataVersion ADD EndDate SMALLDATETIME NOT NULL
ALTER TABLE KBB.DataVersion ADD Edition NVARCHAR(20) NOT NULL

ALTER TABLE KBB.DataVersion DROP CONSTRAINT PK_KBBDataVersion 
GO
ALTER TABLE KBB.DataVersion ADD CONSTRAINT PK_KBBDataVersion PRIMARY KEY CLUSTERED 
(
	DataVersionId ASC,
	VehicleTypeId ASC
) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)

GO

--------------------------------------------------------------------------------
--	Category
--------------------------------------------------------------------------------

ALTER TABLE KBB.Category ADD Note NVARCHAR(500) NULL
ALTER TABLE KBB.Category ADD ExclusivityFlag BIT NULL
GO

--------------------------------------------------------------------------------
--	Vehicle
--------------------------------------------------------------------------------

ALTER TABLE KBB.Vehicle ADD LowVolume BIT NULL CONSTRAINT DF_KBBVehicle__LowVolume DEFAULT (0)	 -- NEEDS TO BE NULLABLE FOR SSIS, NOT SURE WHY, SEEMS LIKE A BUG
GO

--------------------------------------------------------------------------------
--	VehicleOption
--------------------------------------------------------------------------------

ALTER TABLE KBB.VehicleOption ADD StartDate DATETIME NULL
ALTER TABLE KBB.VehicleOption ADD EndDate DATETIME NULL
GO