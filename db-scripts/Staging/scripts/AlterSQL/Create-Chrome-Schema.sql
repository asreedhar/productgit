CREATE SCHEMA Chrome
GO

/****** Object:  Table [Chrome].[Prices#Stg]    Script Date: 12/30/2011 11:21:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Prices#Stg](
	[PricesStgID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NOT NULL,
	[Sequence] [int] NOT NULL,
	[OptionCode] [varchar](20) NULL,
	[PriceRuleDesc] [varchar](200) NULL,
	[Condition] [varchar](100) NULL,
	[Invoice] [money] NULL,
	[MSRP] [money] NULL,
	[PriceState] [varchar](10) NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Styles#Raw]    Script Date: 12/30/2011 11:21:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [Chrome].[Styles#Raw](
	[StylesRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NOT NULL,
	[HistStyleID] [int] NULL,
	[ModelID] [int] NULL,
	[ModelYear] [int] NULL,
	[Sequence] [int] NULL,
	[StyleCode] [varchar](20) NULL,
	[FullStyleCode] [varchar](20) NULL,
	[StyleName] [varchar](60) NULL,
	[TrueBasePrice] [char](10) NULL,
	[Invoice] [real] NULL,
	[MSRP] [real] NULL,
	[Destination] [real] NULL,
	[StyleCVCList] [varchar](200) NULL,
	[MktClassID] [tinyint] NULL,
	[StyleNameWOTrim] [varchar](55) NULL,
	[Trim] [varchar](35) NULL,
	[PassengerCapacity] [int] NULL,
	[PassengerDoors] [int] NULL,
	[ManualTrans] [char](1) NULL,
	[AutoTrans] [char](1) NULL,
	[FrontWD] [char](1) NULL,
	[RearWD] [char](1) NULL,
	[AllWD] [char](1) NULL,
	[FourWD] [char](1) NULL,
	[StepSide] [char](1) NULL,
	[Caption] [varchar](255) NULL,
	[Availability] [varchar](50) NULL,
	[PriceState] [varchar](30) NULL,
	[AutoBuilderStyleID] [varchar](20) NULL,
	[CFModelName] [varchar](255) NULL,
	[CFStyleName] [varchar](255) NULL,
	[CFDrivetrain] [varchar](255) NULL,
	[CFBodyType] [varchar](255) NULL,
	[Is_New] [int] NULL,
	[Is_Update] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Styles#Stg]    Script Date: 12/30/2011 11:21:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [Chrome].[Styles#Stg](
	[StylesStgID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NOT NULL,
	[HistStyleID] [int] NULL,
	[ModelID] [int] NULL,
	[ModelYear] [int] NULL,
	[Sequence] [int] NULL,
	[StyleCode] [varchar](20) NULL,
	[FullStyleCode] [varchar](20) NULL,
	[StyleName] [varchar](60) NULL,
	[TrueBasePrice] [char](10) NULL,
	[Invoice] [real] NULL,
	[MSRP] [real] NULL,
	[Destination] [real] NULL,
	[StyleCVCList] [varchar](200) NULL,
	[MktClassID] [tinyint] NULL,
	[StyleNameWOTrim] [varchar](55) NULL,
	[Trim] [varchar](35) NULL,
	[PassengerCapacity] [int] NULL,
	[PassengerDoors] [int] NULL,
	[ManualTrans] [char](1) NULL,
	[AutoTrans] [char](1) NULL,
	[FrontWD] [char](1) NULL,
	[RearWD] [char](1) NULL,
	[AllWD] [char](1) NULL,
	[FourWD] [char](1) NULL,
	[StepSide] [char](1) NULL,
	[Caption] [varchar](255) NULL,
	[Availability] [varchar](50) NULL,
	[PriceState] [varchar](30) NULL,
	[AutoBuilderStyleID] [varchar](20) NULL,
	[CFModelName] [varchar](255) NULL,
	[CFStyleName] [varchar](255) NULL,
	[CFDrivetrain] [varchar](255) NULL,
	[CFBodyType] [varchar](255) NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Models#Raw]    Script Date: 12/30/2011 11:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [Chrome].[Models#Raw](
	[ModelsRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[ModelID] [int] NOT NULL,
	[HistModelID] [int] NULL,
	[ModelYear] [int] NULL,
	[DivisionID] [int] NULL,
	[SubdivisionID] [int] NULL,
	[ModelName] [varchar](50) NULL,
	[EffectiveDate] [datetime] NULL,
	[ModelComment] [varchar](100) NULL,
	[Availability] [varchar](50) NULL,
	[Is_New] [int] NULL,
	[Is_Update] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Models#Stg]    Script Date: 12/30/2011 11:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [Chrome].[Models#Stg](
	[ModelsStgID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[ModelID] [int] NOT NULL,
	[HistModelID] [int] NULL,
	[ModelYear] [int] NULL,
	[DivisionID] [int] NULL,
	[SubdivisionID] [int] NULL,
	[ModelName] [varchar](50) NULL,
	[EffectiveDate] [datetime] NULL,
	[ModelComment] [varchar](100) NULL,
	[Availability] [varchar](50) NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Subdivisions#Raw]    Script Date: 12/30/2011 11:22:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [Chrome].[Subdivisions#Raw](
	[SubdivisionsRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[ModelYear] [int] NULL,
	[DivisionID] [int] NULL,
	[SubdivisionID] [int] NOT NULL,
	[HistSubdivisionID] [int] NULL,
	[SubdivisionName] [varchar](50) NULL,
	[Is_New] [int] NULL,
	[Is_Update] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Subdivisions#Stg]    Script Date: 12/30/2011 11:22:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [Chrome].[Subdivisions#Stg](
	[SubdivisionsStgID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[ModelYear] [int] NULL,
	[DivisionID] [int] NULL,
	[SubdivisionID] [int] NOT NULL,
	[HistSubdivisionID] [int] NULL,
	[SubdivisionName] [varchar](50) NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[OptHeaders#Raw]    Script Date: 12/30/2011 11:20:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[OptHeaders#Raw](
	[OptionHeadersRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[HeaderID] [int] NOT NULL,
	[Header] [varchar](500) NULL,
	[Is_Update] [int] NULL,
	[Is_New] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[OptKinds#Raw]    Script Date: 12/30/2011 11:21:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[OptKinds#Raw](
	[OptKindsRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[OptionKindID] [int] NOT NULL,
	[OptionKind] [varchar](100) NULL,
	[Is_Update] [int] NULL,
	[Is_New] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[StdHeaders#Raw]    Script Date: 12/30/2011 11:21:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[StdHeaders#Raw](
	[StdHeadersRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[HeaderID] [int] NOT NULL,
	[Header] [varchar](80) NULL,
	[Is_Update] [int] NULL,
	[Is_New] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Categories#Raw]    Script Date: 12/30/2011 11:20:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Categories#Raw](
	[CategoriesRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[CategoryID] [int] NOT NULL,
	[Category] [varchar](100) NULL,
	[CategoryTypeFilter] [varchar](50) NULL,
	[CategoryHeaderID] [int] NULL,
	[UserFriendlyName] [varchar](100) NULL,
	[Is_Update] [int] NULL,
	[Is_New] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[CategoryHeaders#Raw]    Script Date: 12/30/2011 11:20:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[CategoryHeaders#Raw](
	[CategoryHeadersRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[CategoryHeaderID] [int] NOT NULL,
	[CategoryHeader] [varchar](100) NULL,
	[Sequence] [int] NULL,
	[Is_Update] [int] NULL,
	[Is_New] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[CITypes#Raw]    Script Date: 12/30/2011 11:20:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[CITypes#Raw](
	[CITypesRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[TypeID] [int] NOT NULL,
	[Type] [varchar](100) NULL,
	[Is_Update] [int] NULL,
	[Is_New] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[TechTitles#Raw]    Script Date: 12/30/2011 11:22:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[TechTitles#Raw](
	[TechTitlesRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[TitleID] [int] NOT NULL,
	[Sequence] [int] NULL,
	[Title] [varchar](100) NULL,
	[TechTitleHeaderID] [int] NULL,
	[Is_Update] [int] NULL,
	[Is_New] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[TechTitleHeader#Raw]    Script Date: 12/30/2011 11:22:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[TechTitleHeader#Raw](
	[TechTitleHeaderRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[TechTitleHeaderID] [int] NOT NULL,
	[TechTitleHeaderText] [varchar](100) NULL,
	[Sequence] [int] NULL,
	[Is_Update] [int] NULL,
	[Is_New] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Divisions#Raw]    Script Date: 12/30/2011 11:20:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Divisions#Raw](
	[DivisionsRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[DivisionID] [int] NOT NULL,
	[ManufacturerID] [int] NOT NULL,
	[DivisionName] [varchar](50) NOT NULL,
	[Is_Update] [int] NULL,
	[Is_New] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Manufacturers#Raw]    Script Date: 12/30/2011 11:20:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Manufacturers#Raw](
	[ManufacturersRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[ManufacturerID] [int] NOT NULL,
	[ManufacturerName] [varchar](50) NULL,
	[Is_Update] [int] NULL,
	[Is_New] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[MktClass#Raw]    Script Date: 12/30/2011 11:20:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[MktClass#Raw](
	[MktClassRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[MktClassID] [tinyint] NOT NULL,
	[MarketClass] [varchar](100) NULL,
	[Is_Update] [int] NULL,
	[Is_New] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Version#Stg]    Script Date: 12/30/2011 11:22:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Version#Stg](
	[VersionStgID] [int] IDENTITY(1,1) NOT NULL,
	[Product] [varchar](50) NULL,
	[DataVersion] [datetime] NULL,
	[DataReleaseID] [int] NULL,
	[SchemaName] [varchar](20) NULL,
	[SchemaVersion] [varchar](10) NULL,
	[Country] [varchar](2) NULL,
	[Language] [varchar](2) NULL,
	[SourcePath] [varchar](100) NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_ChromeVersion__DateCreated]  DEFAULT (getdate()),
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[OptHeaders#Stg]    Script Date: 12/30/2011 11:20:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[OptHeaders#Stg](
	[OptionHeadersRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[HeaderID] [int] NOT NULL,
	[Header] [varchar](500) NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[OptKinds#Stg]    Script Date: 12/30/2011 11:21:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[OptKinds#Stg](
	[OptKindsRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[OptionKindID] [int] NOT NULL,
	[OptionKind] [varchar](100) NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[StdHeaders#Stg]    Script Date: 12/30/2011 11:21:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[StdHeaders#Stg](
	[StdHeadersRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[HeaderID] [int] NOT NULL,
	[Header] [varchar](80) NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Categories#Stg]    Script Date: 12/30/2011 11:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Categories#Stg](
	[CategoriesRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[CategoryID] [int] NOT NULL,
	[Category] [varchar](100) NULL,
	[CategoryTypeFilter] [varchar](50) NULL,
	[CategoryHeaderID] [int] NULL,
	[UserFriendlyName] [varchar](100) NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[CategoryHeaders#Stg]    Script Date: 12/30/2011 11:20:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[CategoryHeaders#Stg](
	[CategoryHeadersRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[CategoryHeaderID] [int] NOT NULL,
	[CategoryHeader] [varchar](100) NULL,
	[Sequence] [int] NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[CITypes#Stg]    Script Date: 12/30/2011 11:20:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[CITypes#Stg](
	[CITypesRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[TypeID] [int] NOT NULL,
	[Type] [varchar](100) NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[TechTitles#Stg]    Script Date: 12/30/2011 11:22:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[TechTitles#Stg](
	[TechTitlesRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[TitleID] [int] NOT NULL,
	[Sequence] [int] NULL,
	[Title] [varchar](100) NULL,
	[TechTitleHeaderID] [int] NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[TechTitleHeader#Stg]    Script Date: 12/30/2011 11:22:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[TechTitleHeader#Stg](
	[TechTitleHeaderRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[TechTitleHeaderID] [int] NOT NULL,
	[TechTitleHeaderText] [varchar](100) NULL,
	[Sequence] [int] NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Divisions#Stg]    Script Date: 12/30/2011 11:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Divisions#Stg](
	[DivisionsRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[DivisionID] [int] NOT NULL,
	[ManufacturerID] [int] NOT NULL,
	[DivisionName] [varchar](50) NOT NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Manufacturers#Stg]    Script Date: 12/30/2011 11:20:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Manufacturers#Stg](
	[ManufacturersRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[ManufacturerID] [int] NOT NULL,
	[ManufacturerName] [varchar](50) NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[MktClass#Stg]    Script Date: 12/30/2011 11:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[MktClass#Stg](
	[MktClassRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[MktClassID] [tinyint] NOT NULL,
	[MarketClass] [varchar](100) NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Data#Log]    Script Date: 12/30/2011 11:20:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Data#Log](
	[DataID] [int] IDENTITY(1,1) NOT NULL,
	[RunDt] [varchar](10) NOT NULL,
	[TblName] [nvarchar](50) NOT NULL,
	[SPName] [nvarchar](100) NULL,
	[RowsUpdated] [int] NULL,
	[RowsInserted] [int] NULL,
	[RowsDeleted] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Options#Raw]    Script Date: 12/30/2011 11:21:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Options#Raw](
	[OptionsRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NULL,
	[HeaderID] [int] NULL,
	[Sequence] [int] NULL,
	[OptionCode] [varchar](20) NULL,
	[OptionDesc] [varchar](2000) NULL,
	[OptionKindID] [int] NULL,
	[CategoryList] [varchar](300) NULL,
	[TypeFilter] [varchar](50) NULL,
	[Availability] [varchar](30) NULL,
	[PON] [varchar](2000) NULL,
	[ExtDescription] [varchar](2000) NULL,
	[SupportedLogic] [varchar](2000) NULL,
	[UnsupportedLogic] [varchar](2000) NULL,
	[PriceNotes] [varchar](2000) NULL,
	[Is_New] [int] NULL,
	[Is_Update] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Options#Stg]    Script Date: 12/30/2011 11:21:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Options#Stg](
	[OptionsStgID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NULL,
	[HeaderID] [int] NULL,
	[Sequence] [int] NULL,
	[OptionCode] [varchar](20) NULL,
	[OptionDesc] [varchar](2000) NULL,
	[OptionKindID] [int] NULL,
	[CategoryList] [varchar](300) NULL,
	[TypeFilter] [varchar](50) NULL,
	[Availability] [varchar](30) NULL,
	[PON] [varchar](2000) NULL,
	[ExtDescription] [varchar](2000) NULL,
	[SupportedLogic] [varchar](2000) NULL,
	[UnsupportedLogic] [varchar](2000) NULL,
	[PriceNotes] [varchar](2000) NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Standards#Raw]    Script Date: 12/30/2011 11:21:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Standards#Raw](
	[StandardRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NOT NULL,
	[HeaderID] [int] NOT NULL,
	[Sequence] [int] NOT NULL,
	[Standard] [varchar](2000) NULL,
	[CategoryList] [varchar](300) NULL,
	[Is_New] [int] NULL,
	[Is_Update] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Standards#Stg]    Script Date: 12/30/2011 11:21:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Standards#Stg](
	[StandardStgID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NOT NULL,
	[HeaderID] [int] NOT NULL,
	[Sequence] [int] NOT NULL,
	[Standard] [varchar](2000) NULL,
	[CategoryList] [varchar](300) NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[StyleCats#Raw]    Script Date: 12/30/2011 11:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[StyleCats#Raw](
	[StyleCatsRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NULL,
	[CategoryID] [int] NULL,
	[FeatureType] [varchar](1) NULL,
	[Sequence] [int] NULL,
	[State] [varchar](1) NULL,
	[Is_New] [int] NULL,
	[Is_Update] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[StyleCats#Stg]    Script Date: 12/30/2011 11:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[StyleCats#Stg](
	[StyleCatsStgID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NULL,
	[CategoryID] [int] NULL,
	[FeatureType] [varchar](1) NULL,
	[Sequence] [int] NULL,
	[State] [varchar](1) NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[ConsInfo#Raw]    Script Date: 12/30/2011 11:20:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[ConsInfo#Raw](
	[ConsInfoRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NOT NULL,
	[TypeID] [int] NOT NULL,
	[Text] [varchar](8000) NULL,
	[Is_New] [int] NULL,
	[Is_Update] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[ConsInfo#Stg]    Script Date: 12/30/2011 11:20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[ConsInfo#Stg](
	[ConsInfoStgID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NOT NULL,
	[TypeID] [int] NOT NULL,
	[Text] [varchar](8000) NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[BodyStyles#Raw]    Script Date: 12/30/2011 11:19:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [Chrome].[BodyStyles#Raw](
	[BodyStylesRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NOT NULL,
	[BodyStyle] [varchar](50) NOT NULL,
	[IsPrimary] [char](1) NULL,
	[Is_New] [int] NULL,
	[Is_Update] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[BodyStyles#Stg]    Script Date: 12/30/2011 11:19:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [Chrome].[BodyStyles#Stg](
	[BodyStylesStgID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NOT NULL,
	[BodyStyle] [varchar](50) NOT NULL,
	[IsPrimary] [char](1) NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[TechSpecs#Raw]    Script Date: 12/30/2011 11:22:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[TechSpecs#Raw](
	[TechSpecsRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NULL,
	[TitleID] [int] NULL,
	[Sequence] [int] NULL,
	[Text] [varchar](100) NULL,
	[Condition] [varchar](100) NULL,
	[Is_New] [int] NULL,
	[Is_Update] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[TechSpecs#Stg]    Script Date: 12/30/2011 11:22:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[TechSpecs#Stg](
	[TechSpecsStgID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NULL,
	[TitleID] [int] NULL,
	[Sequence] [int] NULL,
	[Text] [varchar](100) NULL,
	[Condition] [varchar](100) NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Colors#Raw]    Script Date: 12/30/2011 11:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Colors#Raw](
	[ColorsRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NOT NULL,
	[Ext1Code] [varchar](50) NULL,
	[Ext2Code] [varchar](50) NULL,
	[IntCode] [varchar](50) NULL,
	[Ext1ManCode] [varchar](50) NULL,
	[Ext2ManCode] [varchar](50) NULL,
	[IntManCode] [varchar](50) NULL,
	[OrderCode] [varchar](50) NULL,
	[AsTwoTone] [char](1) NULL,
	[Ext1Desc] [varchar](50) NULL,
	[Ext2Desc] [varchar](50) NULL,
	[IntDesc] [varchar](50) NULL,
	[Condition] [varchar](300) NULL,
	[GenericExtColor] [varchar](50) NULL,
	[GenericExt2Color] [varchar](50) NULL,
	[Ext1RGBHex] [char](6) NULL,
	[Ext2RGBHex] [char](6) NULL,
	[Ext1MfrFullCode] [varchar](50) NULL,
	[Ext2MfrFullCode] [varchar](50) NULL,
	[Is_New] [int] NULL,
	[Is_Update] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Colors#Stg]    Script Date: 12/30/2011 11:20:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Colors#Stg](
	[ColorsStgID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NOT NULL,
	[Ext1Code] [varchar](50) NULL,
	[Ext2Code] [varchar](50) NULL,
	[IntCode] [varchar](50) NULL,
	[Ext1ManCode] [varchar](50) NULL,
	[Ext2ManCode] [varchar](50) NULL,
	[IntManCode] [varchar](50) NULL,
	[OrderCode] [varchar](50) NULL,
	[AsTwoTone] [char](1) NULL,
	[Ext1Desc] [varchar](50) NULL,
	[Ext2Desc] [varchar](50) NULL,
	[IntDesc] [varchar](50) NULL,
	[Condition] [varchar](300) NULL,
	[GenericExtColor] [varchar](50) NULL,
	[GenericExt2Color] [varchar](50) NULL,
	[Ext1RGBHex] [char](6) NULL,
	[Ext2RGBHex] [char](6) NULL,
	[Ext1MfrFullCode] [varchar](50) NULL,
	[Ext2MfrFullCode] [varchar](50) NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [Chrome].[Prices#Raw]    Script Date: 12/30/2011 11:21:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[Prices#Raw](
	[PricesRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[StyleID] [int] NOT NULL,
	[Sequence] [int] NOT NULL,
	[OptionCode] [varchar](20) NULL,
	[PriceRuleDesc] [varchar](200) NULL,
	[Condition] [varchar](100) NULL,
	[Invoice] [money] NULL,
	[MSRP] [money] NULL,
	[PriceState] [varchar](10) NULL,
	[Is_New] [int] NULL,
	[Is_Update] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO

--vin tbls
--raws
CREATE TABLE [Chrome].[StyleWheelBase#Raw](
[StyleWheelBaseRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[ChromeStyleID] [int] NOT NULL,
	[WheelBase] [decimal](9, 2) NOT NULL,
	[Is_New] [int] NULL,
	[Is_Update] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]


CREATE TABLE [Chrome].[StyleGenericEquipment#Raw](
[StyleGenericEquipmentRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[ChromeStyleID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,
	[StyleAvailability] [varchar](10) NOT NULL,
	[Is_New] [int] NULL,
	[Is_Update] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]

CREATE TABLE [Chrome].[VINPatternStyleMapping#Raw](
	[VINPatternStyleMappingRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[VINMappingID] [int] NOT NULL,
	[ChromeStyleID] [int] NULL,
	[VINPatternID] [int] NULL,
	[Is_New] [int] NULL,
	[Is_Update] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]

CREATE UNIQUE NONCLUSTERED INDEX [IX_VINPatternStyleMapping#Raw] ON [Chrome].[VINPatternStyleMapping#Raw] 
(
	[CountryCode] ASC,
	[VINMappingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


CREATE TABLE [Chrome].[VINEquipment#Raw](
[VINEquipmentRawID] [int] IDENTITY(1,1) NOT NULL,
[CountryCode] [tinyint] NOT NULL,
[VINPatternID] [int] NOT NULL,
[CategoryID] [int] NOT NULL,
[VINAvailability] [varchar](10) NULL,
[Is_New] [int] NULL,
[Is_Update] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]


CREATE TABLE [Chrome].[Category#Raw](
[CategoryRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[CategoryID] [int] NOT NULL,
	[Description] [varchar](255) NULL,
	[CategoryUTF] [varchar](50) NULL,
	[CategoryType] [varchar](20) NULL,
[Is_New] [int] NULL,
[Is_Update] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]

CREATE TABLE [Chrome].[YearMakeModelStyle#Raw](
[YearMakeModelStyleRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[ChromeStyleID] [int] NOT NULL,
	[Country] [varchar](2) NULL,
	[Year] [int] NULL,
	[DivisionName] [varchar](50) NULL,
	[SubdivisionName] [varchar](50) NULL,
	[ModelName] [varchar](50) NULL,
	[StyleName] [varchar](50) NULL,
	[TrimName] [varchar](50) NULL,
	[MfrStyleCode] [varchar](38) NULL,
	[FleetOnly] [varchar](1) NULL,
	[AvailableInNVD] [varchar](1) NULL,
	[DivisionID] [int] NULL,
	[SubdivisionID] [int] NULL,
	[ModelID] [int] NULL,
	[AutoBuilderStyleID] [varchar](30) NULL,
	[HistoricalStyleID] [int] NULL,
[Is_New] [int] NULL,
[Is_Update] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]



CREATE TABLE [Chrome].[VINPattern#Raw](
[VINPatternRawID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[VINPatternID] [int] NOT NULL,
	[VINPattern] [varchar](17) NULL,
	[Country] [varchar](50) NULL,
	[Year] [int] NULL,
	[VINDivisionName] [varchar](50) NULL,
	[VINModelName] [varchar](50) NULL,
	[VINStyleName] [varchar](50) NULL,
	[EngineTypeCategoryID] [int] NULL,
	[EngineSize] [varchar](50) NULL,
	[EngineCID] [varchar](50) NULL,
	[FuelTypeCategoryID] [int] NULL,
	[ForcedInductionCategoryID] [int] NULL,
	[TransmissionTypeCategoryID] [int] NULL,
	[ManualTransAvail] [varchar](1) NULL,
	[AutoTransAvail] [varchar](1) NULL,
	[GVWRRange] [varchar](50) NULL,
	[Is_New] [int] NULL,
[Is_Update] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
	



--stg
CREATE TABLE [Chrome].[StyleWheelBase#Stg](
[StyleWheelBaseStgID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[ChromeStyleID] [int] NOT NULL,
	[WheelBase] [decimal](9, 2) NOT NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]


CREATE TABLE [Chrome].[StyleGenericEquipment#Stg](
[StyleGenericEquipmentStgID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[ChromeStyleID] [int] NOT NULL,
	[CategoryID] [int] NOT NULL,
	[StyleAvailability] [varchar](10) NOT NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]

CREATE TABLE [Chrome].[VINPatternStyleMapping#Stg](
	[VINPatternStyleMappingStgID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[VINMappingID] [int] NOT NULL,
	[ChromeStyleID] [int] NULL,
	[VINPatternID] [int] NULL,
	[Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]

CREATE UNIQUE NONCLUSTERED INDEX [IX_VINPatternStyleMapping#Stg] ON [Chrome].[VINPatternStyleMapping#Stg] 
(
	[CountryCode] ASC,
	[VINMappingID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

CREATE TABLE [Chrome].[VINEquipment#Stg](
[VINEquipmentStgID] [int] IDENTITY(1,1) NOT NULL,
[CountryCode] [tinyint] NOT NULL,
[VINPatternID] [int] NOT NULL,
[CategoryID] [int] NOT NULL,
[VINAvailability] [varchar](10) NULL,
  [Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]

CREATE TABLE [Chrome].[Category#Stg](
[CategoryStgID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[CategoryID] [int] NOT NULL,
	[Description] [varchar](255) NULL,
	[CategoryUTF] [varchar](50) NULL,
	[CategoryType] [varchar](20) NULL,
  [Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]

CREATE TABLE [Chrome].[YearMakeModelStyle#Stg](
[YearMakeModelStyleStgID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[ChromeStyleID] [int] NOT NULL,
	[Country] [varchar](2) NULL,
	[Year] [int] NULL,
	[DivisionName] [varchar](50) NULL,
	[SubdivisionName] [varchar](50) NULL,
	[ModelName] [varchar](50) NULL,
	[StyleName] [varchar](50) NULL,
	[TrimName] [varchar](50) NULL,
	[MfrStyleCode] [varchar](38) NULL,
	[FleetOnly] [varchar](1) NULL,
	[AvailableInNVD] [varchar](1) NULL,
	[DivisionID] [int] NULL,
	[SubdivisionID] [int] NULL,
	[ModelID] [int] NULL,
	[AutoBuilderStyleID] [varchar](30) NULL,
	[HistoricalStyleID] [int] NULL,
  [Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]

CREATE TABLE [Chrome].[VINPattern#Stg](
[VINPatternStgID] [int] IDENTITY(1,1) NOT NULL,
	[CountryCode] [tinyint] NOT NULL,
	[VINPatternID] [int] NOT NULL,
	[VINPattern] [varchar](17) NULL,
	[Country] [varchar](50) NULL,
	[Year] [int] NULL,
	[VINDivisionName] [varchar](50) NULL,
	[VINModelName] [varchar](50) NULL,
	[VINStyleName] [varchar](50) NULL,
	[EngineTypeCategoryID] [int] NULL,
	[EngineSize] [varchar](50) NULL,
	[EngineCID] [varchar](50) NULL,
	[FuelTypeCategoryID] [int] NULL,
	[ForcedInductionCategoryID] [int] NULL,
	[TransmissionTypeCategoryID] [int] NULL,
	[ManualTransAvail] [varchar](1) NULL,
	[AutoTransAvail] [varchar](1) NULL,
	[GVWRRange] [varchar](50) NULL,
  [Is_Delete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]


