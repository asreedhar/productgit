IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'Chrome.ADS7Response')
                    AND type IN ( N'U' ) ) 
    DROP TABLE Chrome.ADS7Response
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'Chrome.ADS7Error')
                    AND type IN ( N'U' ) ) 
    DROP TABLE Chrome.ADS7Error
GO
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[Chrome].[StyleIdentity]')
                    AND type IN ( N'U' ) ) 
    DROP TABLE [Chrome].[StyleIdentity]
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE Chrome.ADS7Response
(
  VIN CHAR(17) NOT NULL
, Response XML NOT NULL
, ResponseDate DATETIME NOT NULL CONSTRAINT DF_ResponseDate
                        DEFAULT GETDATE()
)

GO
CREATE NONCLUSTERED INDEX [Idx_VIN] ON [Chrome].[ADS7Response] ([VIN]) ON [PRIMARY]
GO

CREATE TABLE Chrome.ADS7Error
(
  VIN CHAR(17) NOT NULL
, ErrorMessage VARCHAR(100) NOT NULL
, ErrorDate DATETIME NOT NULL  CONSTRAINT DF_ErrorDate
                     DEFAULT GETDATE()
)
GO

CREATE NONCLUSTERED INDEX [IDX_ErrorDate] ON [Chrome].[ADS7Error] ([ErrorDate]) INCLUDE ([VIN]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [Idx_VIN] ON [Chrome].[ADS7Error] ([VIN]) ON [PRIMARY]
GO



CREATE TABLE [Chrome].[StyleIdentity](
	[WebStyleID] [int] IDENTITY(900000,1) NOT NULL,
	[OrigStyleID] [int] NULL,
	[ModelID] [int] NULL,
	[ModelYear] [int] NOT NULL,
	[MktClassID] [tinyint] NULL,
	[CFModelName] [varchar](255) NULL,
	[CFBodyType] [varchar](255) NULL,
	[VinPattern] [char](17) NOT NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]
