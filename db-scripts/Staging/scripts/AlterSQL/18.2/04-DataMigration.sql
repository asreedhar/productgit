SET IDENTITY_INSERT Listing.Vehicle_tmp ON
INSERT Listing.Vehicle_tmp WITH (TABLOCK)
        (  
		 [_LoadID]
       ,[_AuditID]
       ,[_ProviderRowID]
       ,[_ProviderID]
       ,[_PartitionStatusCD]
       ,[_ProcessStatusCD]
       ,Source
       ,StockNumber
       ,StockType
       ,DealerCode
       ,DealerType
       ,VIN
       ,Make
       ,Model
       ,ModelYear
       ,Trim
       ,Mileage
       ,ExteriorColor
       ,InteriorColor
       ,ExteriorColorCode
       ,InteriorColorCode
       ,DriveTrain
       ,Engine
       ,Transmission
       ,FuelType
       ,ChromeStyleId
       ,MSRP
       ,InvoicePrice
       ,ListPrice
       ,ListingDate
       ,SellerID
       ,SellerName
       ,SellerAddress1
       ,SellerAddress2
       ,SellerCity
       ,SellerState
       ,SellerZipCode
       ,SellerURL
       ,SellerPhoneNumber
       ,SellerEMail
       ,Certified
       ,CertificationNumber
       ,Options
       ,OptionCodes
       ,SellerDescription
       ,LotLocation
       ,TagNumber
       ,TagState
       ,ImageURLs
       ,ImageModifiedDate
       ,ImageCount
       ,[_LastUpdateLoadID]
       ,FirstlookVehicleCatalogID
       ,VideoURL
       ,BodyStyle
       ,Doors
       ,EngineSize
       ,WheelBase
       ,TruckBed
       ,ModelCode
       ,PackageCodes
       ,SourceRecordType
	   )
SELECT  [_LoadID]
       ,[_AuditID]
       ,[_ProviderRowID]
       ,[_ProviderID]
       ,[_PartitionStatusCD]
       ,[_ProcessStatusCD]
       ,Source
       ,StockNumber
       ,StockType
       ,DealerCode
       ,DealerType
       ,VIN
       ,Make
       ,Model
       ,ModelYear
       ,Trim
       ,Mileage
       ,ExteriorColor
       ,InteriorColor
       ,ExteriorColorCode
       ,InteriorColorCode
       ,DriveTrain
       ,Engine
       ,Transmission
       ,FuelType
       ,ChromeStyleId
       ,MSRP
       ,InvoicePrice
       ,ListPrice
       ,ListingDate
       ,SellerID
       ,SellerName
       ,SellerAddress1
       ,SellerAddress2
       ,SellerCity
       ,SellerState
       ,SellerZipCode
       ,SellerURL
       ,SellerPhoneNumber
       ,SellerEMail
       ,Certified
       ,CertificationNumber
       ,Options
       ,OptionCodes
       ,SellerDescription
       ,LotLocation
       ,TagNumber
       ,TagState
       ,ImageURLs
       ,ImageModifiedDate
       ,ImageCount
       ,[_LastUpdateLoadID]
       ,FirstlookVehicleCatalogID
       ,VideoURL
       ,BodyStyle
       ,Doors
       ,EngineSize
       ,WheelBase
       ,TruckBed
       ,ModelCode
       ,PackageCodes
       ,SourceRecordType
FROM Listing.Vehicle v
WHERE [_PartitionStatusCD]=1
ORDER BY [_ProviderID],[_LoadID],[_AuditID]
IF @@ERROR= 0 
BEGIN
	EXEC sp_rename 'Listing.Vehicle','VehicleOld'
	EXEC sp_rename 'Listing.Vehicle_tmp','Vehicle'
END