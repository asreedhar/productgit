/*ProviderID 19 Support*/
CREATE INDEX IX_Listing_Vehicle_ProviderId_ProviderRowID_SellerId ON Staging.Listing.Vehicle([_ProviderID],[_ProviderRowID],SellerID)
 WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderId)
 
 CREATE INDEX IX_Listing_VehiclePreload_ProviderId_ProviderRowID_SellerId ON Staging.Listing.Vehicle_Preload([_ProviderID],[_ProviderRowID],SellerID)
 WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderId)

 CREATE INDEX IX_Listing_VehicleHistory_ProviderId_ProviderRowID_SellerId ON Staging.Listing.Vehicle_History([_ProviderID],[_ProviderRowID],SellerID)
 WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderId)

 /*ProviderID 26,27 support (goes away???)*/
 CREATE INDEX IX_Listing_Vehicle_ProviderId_VIN ON Staging.Listing.Vehicle([_ProviderID],VIN)
 WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderId)
 
 CREATE INDEX IX_Listing_VehiclePreload_ProviderId_VIN ON Staging.Listing.Vehicle_Preload([_ProviderID],VIN)
 WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderId)

 CREATE INDEX IX_Listing_VehicleHistory_ProviderId_VIN ON Staging.Listing.Vehicle_History([_ProviderID],VIN)
 WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderId)

 
 /*ProviderID NOT 19,26,27 support (goes away???)*/
 CREATE INDEX IX_Listing_Vehicle_ProviderId_Stock_Seller_Phone ON Staging.Listing.Vehicle([_ProviderID],StockNumber,SellerName,SellerPhoneNumber)
 WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderId)
 
 CREATE INDEX IX_Listing_VehiclePreload_ProviderId_Stock_Seller_Phone ON Staging.Listing.Vehicle_Preload([_ProviderID],StockNumber,SellerName,SellerPhoneNumber)
 WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderId)

 CREATE INDEX IX_Listing_VehicleHistory_ProviderId_Stock_Seller_Phone ON Staging.Listing.Vehicle_History([_ProviderID],StockNumber,SellerName,SellerPhoneNumber)
 WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderId)

/*
TEST Swap Code

DECLARE @ProviderId TINYINT=27
TRUNCATE TABLE Listing.Vehicle_Rolloff
ALTER TABLE Listing.Vehicle_Preload SWITCH PARTITION $PARTITION.[PF_PartitionProvider] (@ProviderId) TO Listing.Vehicle_Rolloff
TRUNCATE TABLE Listing.Vehicle_Rolloff
ALTER TABLE Listing.Vehicle_History SWITCH PARTITION $PARTITION.[PF_PartitionProvider] (@ProviderId) TO Listing.Vehicle_Rolloff
ALTER TABLE Listing.Vehicle SWITCH PARTITION $PARTITION.[PF_PartitionProvider] (@ProviderId) TO Listing.Vehicle_History PARTITION $PARTITION.[PF_PartitionProvider] (@ProviderId) 	
ALTER TABLE Listing.Vehicle_History SWITCH PARTITION $PARTITION.[PF_PartitionProvider] (@ProviderId) TO Listing.Vehicle_Preload PARTITION $PARTITION.[PF_PartitionProvider] (@ProviderId) 	
ALTER TABLE Listing.Vehicle_Preload SWITCH PARTITION $PARTITION.[PF_PartitionProvider] (@ProviderId) TO Listing.Vehicle PARTITION $PARTITION.[PF_PartitionProvider] (@ProviderId) 	

*/				