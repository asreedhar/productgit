ALTER DATABASE Staging ADD FILEGROUP Listing 
GO
DECLARE @FileName VARCHAR(MAX)= 
(SELECT LEFT(physical_name,LEN(physical_name)-CHARINDEX('\',REVERSE(physical_name))+1) FROM Staging.sys.database_files WHERE file_id=1) +  'listdat.ndf'
DECLARE @sql NVARCHAR(max) = N'ALTER DATABASE Staging ADD FILE (NAME = listdat,FILENAME = '''+ @FileName + ''',SIZE = 8096MB,MAXSIZE = UNLIMITED,FILEGROWTH = 100MB) TO FILEGROUP Listing'

SELECT @sql

EXEC sp_executesql @sql


CREATE PARTITION FUNCTION [PF_PartitionProvider](tinyint) AS RANGE LEFT FOR VALUES (0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B)
GO

CREATE PARTITION SCHEME [PS_ListingProviderId] AS PARTITION [PF_PartitionProvider] TO ([Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing], [Listing])
GO


SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
Go
CREATE TABLE [Listing].[Vehicle_tmp](
	[_LoadID] [int] NOT NULL,
	[_AuditID] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[_ProviderRowID] [varchar](10) NULL,
	[_ProviderID] [tinyint] NOT NULL,
	[_PartitionStatusCD] [tinyint] NOT NULL,
	[_ProcessStatusCD] [tinyint] NOT NULL,
	[Source] [varchar](20) NULL,
	[StockNumber] [varchar](30) NULL,
	[StockType] [varchar](20) NULL,
	[DealerCode] [varchar](50) NULL,
	[DealerType] [varchar](30) NULL,
	[VIN] [varchar](17) NULL,
	[Make] [varchar](50) NULL,
	[Model] [varchar](75) NULL,
	[ModelYear] [varchar](4) NULL,
	[Trim] [varchar](50) NULL,
	[Mileage] [varchar](10) NULL,
	[ExteriorColor] [varchar](50) NULL,
	[InteriorColor] [varchar](50) NULL,
	[ExteriorColorCode] [varchar](20) NULL,
	[InteriorColorCode] [varchar](20) NULL,
	[DriveTrain] [varchar](50) NULL,
	[Engine] [varchar](50) NULL,
	[Transmission] [varchar](50) NULL,
	[FuelType] [varchar](50) NULL,
	[ChromeStyleId] [varchar](10) NULL,
	[MSRP] [varchar](10) NULL,
	[InvoicePrice] [varchar](10) NULL,
	[ListPrice] [varchar](10) NULL,
	[ListingDate] [varchar](30) NULL,
	[SellerID] [varchar](20) NULL,
	[SellerName] [varchar](75) NULL,
	[SellerAddress1] [varchar](200) NULL,
	[SellerAddress2] [varchar](100) NULL,
	[SellerCity] [varchar](50) NULL,
	[SellerState] [varchar](50) NULL,
	[SellerZipCode] [varchar](10) NULL,
	[SellerURL] [varchar](1000) NULL,
	[SellerPhoneNumber] [varchar](50) NULL,
	[SellerEMail] [varchar](100) NULL,
	[Certified] [varchar](25) NULL,
	[CertificationNumber] [varchar](10) NULL,
	[Options] [varchar](8000) NULL,
	[OptionCodes] [varchar](1000) NULL,
	[SellerDescription] [varchar](2000) NULL,
	[LotLocation] [varchar](50) NULL,
	[TagNumber] [varchar](10) NULL,
	[TagState] [varchar](3) NULL,
	[ImageURLs] [varchar](8000) NULL,
	[ImageModifiedDate] [varchar](30) NULL,
	[ImageCount] [varchar](5) NULL,
	[_LastUpdateLoadID] [int] NULL,
	[FirstlookVehicleCatalogID] [int] NULL,
	[VideoURL] [varchar](250) NULL,
	[BodyStyle] [varchar](100) NULL,
	[Doors] [varchar](10) NULL,
	[EngineSize] [varchar](10) NULL,
	[WheelBase] [varchar](10) NULL,
	[TruckBed] [varchar](20) NULL,
	[ModelCode] [varchar](10) NULL,
	[PackageCodes] [varchar](100) NULL,
	[SourceRecordType] [varchar](100) NULL,
 CONSTRAINT [PK_ListingVehicle_1] PRIMARY KEY CLUSTERED 
(
	[_ProviderID] ASC,
	[_LoadID] ASC,
	[_AuditID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90)
) ON PS_ListingProviderId (_ProviderId)

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'DataloadID as registered in DBASTAT.dbo.Dataload_History.' , @level0type=N'SCHEMA',@level0name=N'Listing', @level1type=N'TABLE',@level1name=N'Vehicle_tmp', @level2type=N'COLUMN',@level2name=N'_LoadID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identity column uniquely identifying each row as staged from a Listing provider' , @level0type=N'SCHEMA',@level0name=N'Listing', @level1type=N'TABLE',@level1name=N'Vehicle_tmp', @level2type=N'COLUMN',@level2name=N'_AuditID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Optional UID as received by the provider.  Currently, not used in the system' , @level0type=N'SCHEMA',@level0name=N'Listing', @level1type=N'TABLE',@level1name=N'Vehicle_tmp', @level2type=N'COLUMN',@level2name=N'_ProviderID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Deprecated with new partition scheme' , @level0type=N'SCHEMA',@level0name=N'Listing', @level1type=N'TABLE',@level1name=N'Vehicle_tmp', @level2type=N'COLUMN',@level2name=N'_PartitionStatusCD'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Staging Process status.  For fetch.com request/response, 1=Insert, 2=Update, 3=Delete.' , @level0type=N'SCHEMA',@level0name=N'Listing', @level1type=N'TABLE',@level1name=N'Vehicle_tmp', @level2type=N'COLUMN',@level2name=N'_ProcessStatusCD'
GO

ALTER TABLE [Listing].[Vehicle_tmp] REBUILD WITH (DATA_COMPRESSION=PAGE);


