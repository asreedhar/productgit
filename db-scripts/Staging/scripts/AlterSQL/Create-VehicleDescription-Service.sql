
-- drop support objects
IF OBJECT_ID('Autodata.VehicleDescriptionErrors') IS NOT NULL
	DROP TABLE Autodata.VehicleDescriptionErrors

IF OBJECT_ID('Autodata.VehicleDescriptionDialogs') IS NOT NULL
	DROP TABLE Autodata.VehicleDescriptionDialogs

IF OBJECT_ID('Autodata.VehicleDescription#Send') IS NOT NULL
	DROP PROCEDURE Autodata.VehicleDescription#Send

GO
-- create Errors table
CREATE TABLE Autodata.VehicleDescriptionErrors
(
	Id BIGINT IDENTITY(1, 1) PRIMARY KEY,
	ErrorProcedure NVARCHAR(126) NOT NULL,
	ErrorLine INT NOT NULL,
	ErrorNumber INT NOT NULL,
	ErrorMessage NVARCHAR(MAX) NOT NULL,
	ErrorSeverity INT NOT NULL,
	ErrorState INT NOT NULL,
	VehicleDescriptionedData XML NOT NULL,
	ErrorDate DATETIME NOT NULL CONSTRAINT DF_VehicleDescriptionErrors_ErrorDate DEFAULT (GETUTCDATE())
)

GO

-- Table that will hold dialog id for database
-- These dialogs will be reused. 
CREATE TABLE Autodata.VehicleDescriptionDialogs
(
	DbId INT NOT NULL, 
	DialogId UNIQUEIDENTIFIER NOT NULL
)

GO

	