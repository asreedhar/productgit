ALTER TABLE CDMData.Vehicle ALTER COLUMN OptionCodes VARCHAR(2000) NULL

ALTER TABLE CDMData.Vehicle ALTER COLUMN PackageCodes VARCHAR(2000) NULL

ALTER TABLE CDMData.Vehicle ALTER COLUMN ImageURLs VARCHAR(8000) NULL


ALTER TABLE AULtec.Vehicle ALTER COLUMN OptionCodes VARCHAR(2000) NULL

ALTER TABLE AULtec.Vehicle ALTER COLUMN PackageCodes VARCHAR(2000) NULL

ALTER TABLE AULtec.Vehicle ALTER COLUMN ImageURLs VARCHAR(8000) NULL
GO