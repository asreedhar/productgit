CREATE TABLE [Autodata].[Acode](
      [VIN] [char](17) NOT NULL,
      [Acode] [char](13) NULL,
      [_LoadID] [int] NOT NULL,
      [_LoadDt] [datetime] NULL,
CONSTRAINT [PK_Acode] PRIMARY KEY CLUSTERED 
(
      [VIN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
 
GO
