
ALTER TABLE Autodata.AcodeChromeMapping ADD
	_LoadDt datetime NULL,
	_ModifiedDt datetime NULL
GO

ALTER TABLE Autodata.AcodeChromeMapping DROP CONSTRAINT PK_AutodataAcodeChromeMapping
GO

ALTER TABLE Autodata.AcodeChromeMapping ADD CONSTRAINT PK_AutodataAcodeChromeMapping PRIMARY KEY CLUSTERED (AD_VEH_ID, STYLEID, MFGCODE)
GO


ALTER TABLE Autodata.Vehicle ADD
	_LoadDt datetime NULL,
	_ModifiedDt datetime NULL
GO

ALTER TABLE Autodata.Equipment ADD
	_LoadDt datetime NULL,
	_ModifiedDt datetime NULL
GO

ALTER TABLE Autodata.OptionDescription ALTER COLUMN SALESORDERABLE char (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL

GO

ALTER TABLE Autodata.OptionDescription ADD
	_LoadDt datetime NULL,
	_ModifiedDt datetime NULL
GO

ALTER TABLE Autodata.Dealer ADD
	_LoadDt datetime NULL,
	_ModifiedDt datetime NULL
GO