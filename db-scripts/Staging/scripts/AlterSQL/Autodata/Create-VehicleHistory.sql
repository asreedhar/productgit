CREATE TABLE Autodata.VehicleHistory (
BAC INT NOT NULL,
VIN CHAR (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
STOCK_NUMBER VARCHAR (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
NEW_OR_USED CHAR (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
MODEL_YEAR SMALLINT NULL,
MODEL_DESC VARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
PKG_CODE INT NULL,
MILEAGE INT NULL,
VEH_PROD_CD VARCHAR (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
ENGINE_DESC VARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
TRANS_DESC VARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
BODY_STYLE_DESC VARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
DRIVETRAIN VARCHAR (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
MSRP INT NULL,
EMPLOYEE_DISCOUNT_PRICE INT NULL,
SUPPLIER_DISCOUNT_PRICE INT NULL,
DESTINATION SMALLINT NULL,
SELLING_SOURCE_CODE VARCHAR (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
NAMEPLATE VARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
GVWR SMALLINT NULL,
INTERIOR_DESC VARCHAR (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
EXTERIOR_COLOR VARCHAR (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
MERCH_MODEL_DESGTR VARCHAR (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
STD_FEATURES VARCHAR (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
OPT_EQ_CODES VARCHAR (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
OTHER_EQ_CODES VARCHAR (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
STDBARS_IDS VARCHAR (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
DIO VARCHAR (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
ALL_EQ_CODES VARCHAR (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
SPECIAL_PAINT_CD VARCHAR (4000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
ENG_CODE INT NULL,
TRANS_CODE INT NULL,
EXT_COLOR_1 INT NULL,
EXT_COLOR_2 INT NULL,
INT_TRM_CODE INT NULL,
PRICE INT NULL,
VEHICLE_TYPE_CD SMALLINT NULL,
_LoadID INT NOT NULL,
ACODE CHAR (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
_LoadDt DATETIME NULL,
_ModifiedDt DATETIME NULL,
_InsertDt SMALLDATETIME NOT NULL
)
GO
ALTER TABLE [Autodata].[VehicleHistory] ADD CONSTRAINT [PK_AutodataVehicleHistory] PRIMARY KEY CLUSTERED  ([BAC], [VIN], [_InsertDt]) WITH (FILLFACTOR=95) ON [Archive]
GO
