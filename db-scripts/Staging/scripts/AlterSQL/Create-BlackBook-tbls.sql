IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Chrome].[BlackBookRequiredOptions#Raw]') AND type in (N'U'))
DROP TABLE [Chrome].[BlackBookRequiredOptions#Raw]
GO


CREATE TABLE [Chrome].[BlackBookRequiredOptions#Raw](
	[RequiredOptionRawID] [int] IDENTITY(1,1) NOT NULL,
	[RequiredOptionID] [int] NULL,
	[MappingID] [int] NULL,
	[OptionCode] [varchar](20) NULL,
	[UOC] [varchar](5) NULL,
	[AddDeduct] [varchar](2) NULL,
	[Is_New] [int] NULL,
	[Is_Update] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]

GO




IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Chrome].[BlackBookRequiredOptions#Stg]') AND type in (N'U'))
DROP TABLE [Chrome].[BlackBookRequiredOptions#Stg]
GO

CREATE TABLE [Chrome].[BlackBookRequiredOptions#Stg](
	[RequiredOptionStgID] [int] IDENTITY(1,1) NOT NULL,
	[RequiredOptionID] [int] NULL,
	[MappingID] [int] NULL,
	[OptionCode] [varchar](20) NULL,
	[UOC] [varchar](5) NULL,
	[AddDeduct] [varchar](2) NULL,
	[IsDelete] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Chrome].[BlackBookVehicleMap#Raw]') AND type in (N'U'))
DROP TABLE [Chrome].[BlackBookVehicleMap#Raw]
GO

CREATE TABLE [Chrome].[BlackBookVehicleMap#Raw](
	[VehicleMapRawID] [int] IDENTITY(1,1) NOT NULL,
	[MappingID] [int] NULL,
	[StyleId] [int] NULL,
	[UVC] [int] NULL,
	[Is_New] [int] NULL,
	[Is_Update] [int] NULL,
	[LoadDttm] [datetime] NULL
) ON [PRIMARY]

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Chrome].[BlackBookVehicleMap#Stg]') AND type in (N'U'))
DROP TABLE [Chrome].[BlackBookVehicleMap#Stg]
GO

CREATE TABLE [Chrome].[BlackBookVehicleMap#Stg](
	[VehicleMapStgID] [int] IDENTITY(1,1) NOT NULL,
	[MappingID] [int] NULL,
	[StyleId] [int] NULL,
	[UVC] [int] NULL,
	[IsDelete] [int] NULL,
	[LoadDttm] [datetime] NULL
) ON [PRIMARY]

GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Chrome].[BlackBookVersionInfo#Raw]') AND type in (N'U'))
DROP TABLE [Chrome].[BlackBookVersionInfo#Raw]
GO

CREATE TABLE [Chrome].[BlackBookVersionInfo#Raw](
	[VersionInfoRawID] [int] IDENTITY(1,1) NOT NULL,
	[MappingVersion] [varchar](50) NULL,
	[ChromeDataDate] [varchar](50) NULL,
	[ChromeDataReleaseID] [varchar](50) NULL,
	[BBPeriod] [varchar](50) NULL,
	[DisplayPeriod] [varchar](50) NULL,
	[SourcePath] [nvarchar](100) NULL,
	[LoadDttm] [datetime] NULL
) ON [PRIMARY]

GO


