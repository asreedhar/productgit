
/****** Object:  Schema [HondaNA]    Script Date: 6/19/2013 4:52:46 PM ******/
CREATE SCHEMA [HondaNA]
GO
/****** Object:  Table [HondaNA].[Dealer]    Script Date: 6/19/2013 4:52:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [HondaNA].[Dealer](
	[DealerNumber] [int] NOT NULL,
	[Make] [varchar](50) NULL,
	[DealerName] [varchar](255) NOT NULL,
	[PhysicalAddress] [varchar](255) NULL,
	[PhysicalCity] [varchar](255) NULL,
	[PhysicalState] [varchar](255) NULL,
	[PhysicalZip] [int] NULL,
	[MailingAddress] [varchar](255) NULL,
	[MailingCity] [varchar](255) NULL,
	[MailingState] [varchar](255) NULL,
	[MailingZip] [int] NULL,
	[URL] [varchar](max) NULL,
	[Latitude] [decimal](9, 6) NULL,
	[Longitude] [decimal](9, 6) NULL,
	[Active] [bit] NOT NULL,
	[CreateLoadId] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[ModifyLoadId] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK__Dealer__14188367558B7A75] PRIMARY KEY CLUSTERED 
(
	[DealerNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [HondaNA].[Dealer#Raw]    Script Date: 6/19/2013 4:52:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [HondaNA].[Dealer#Raw](
	[LoadId] [int] NOT NULL,
	[Make] [varchar](50) NULL,
	[Dealer_Id] [int] NOT NULL,
	[DealerNumber] [int] NULL,
	[DealerName] [nvarchar](255) NULL,
	[PhysicalAddress] [nvarchar](255) NULL,
	[PhysicalCity] [nvarchar](255) NULL,
	[PhysicalState] [nvarchar](255) NULL,
	[PhysicalZip] [int] NULL,
	[MailingAddress] [nvarchar](255) NULL,
	[MailingCity] [nvarchar](255) NULL,
	[MailingState] [nvarchar](255) NULL,
	[MailingZip] [int] NULL,
	[URL] [nvarchar](max) NULL,
	[Latitude] [decimal](9, 6) NULL,
	[Longitude] [decimal](9, 6) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [HondaNA].[Department]    Script Date: 6/19/2013 4:52:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [HondaNA].[Department](
	[DealerNumber] [int] NOT NULL,
	[Name] [varchar](255) NOT NULL,
	[Cd] [varchar](255) NULL,
	[Email] [varchar](255) NULL,
	[Fax] [varchar](255) NULL,
	[FaxCountryCode] [varchar](255) NULL,
	[FaxExtension] [varchar](255) NULL,
	[FirstName] [varchar](255) NULL,
	[LastName] [varchar](255) NULL,
	[OperationHour_consolidated] [varchar](255) NULL,
	[Phone] [varchar](255) NULL,
	[PhoneCountryCode] [varchar](255) NULL,
	[PhoneExtension] [varchar](255) NULL,
	[SequenceNo] [int] NULL,
	[Active] [bit] NULL,
	[CreateLoadId] [int] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyLoadId] [int] NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Department_1] PRIMARY KEY CLUSTERED 
(
	[DealerNumber] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [HondaNA].[Department#Raw]    Script Date: 6/19/2013 4:52:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HondaNA].[Department#Raw](
	[LoadId] [int] NOT NULL,
	[Departments_Id] [int] NOT NULL,
	[Cd] [nvarchar](255) NULL,
	[Email] [nvarchar](255) NULL,
	[Fax] [nvarchar](255) NULL,
	[FaxCountryCode] [nvarchar](255) NULL,
	[FaxExtension] [nvarchar](255) NULL,
	[FirstName] [nvarchar](255) NULL,
	[LastName] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[OperationHour_consolidated] [nvarchar](255) NULL,
	[Phone] [nvarchar](255) NULL,
	[PhoneCountryCode] [nvarchar](255) NULL,
	[PhoneExtension] [nvarchar](255) NULL,
	[SequenceNo] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [HondaNA].[Departments#Raw]    Script Date: 6/19/2013 4:52:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HondaNA].[Departments#Raw](
	[LoadId] [int] NOT NULL,
	[Dealer_Id] [int] NOT NULL,
	[Departments_Id] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [HondaNA].[Inventory]    Script Date: 6/19/2013 4:52:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [HondaNA].[Inventory](
	[VIN] [nchar](17) NOT NULL,
	[DealerNumber] [int] NOT NULL,
	[CreateLoadId] [int] NOT NULL,
	[Mileage] [int] NULL,
	[MSRP] [decimal](28, 10) NULL,
	[DealerPrice] [decimal](28, 10) NULL,
	[CertificationDate] [varchar](255) NULL,
	[StockNumber] [varchar](255) NULL,
	[XMRadio] [varchar](255) NULL,
	[DealerInstalledAccessories] [varchar](255) NULL,
	[CarFaxHistoryURL] [varchar](max) NULL,
	[StockThumbImageURL] [varchar](max) NULL,
	[StockLargeImageURL] [varchar](max) NULL,
	[Active] [bit] NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[InactiveDate] [datetime] NULL,
 CONSTRAINT [PK_Inventory] PRIMARY KEY CLUSTERED 
(
	[VIN] ASC,
	[DealerNumber] ASC,
	[CreateLoadId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [HondaNA].[Photo]    Script Date: 6/19/2013 4:52:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [HondaNA].[Photo](
	[VIN] [char](17) NOT NULL,
	[DealerNumber] [int] NOT NULL,
	[Sequence] [int] NOT NULL,
	[TYPE] [char](1) NOT NULL,
	[URL] [varchar](max) NOT NULL,
	[CreateLoadId] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[ModifyDate] [datetime] NULL,
 CONSTRAINT [PK_Photo] PRIMARY KEY CLUSTERED 
(
	[VIN] ASC,
	[DealerNumber] ASC,
	[Sequence] ASC,
	[TYPE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [HondaNA].[Photo#Raw]    Script Date: 6/19/2013 4:52:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HondaNA].[Photo#Raw](
	[LoadId] [int] NOT NULL,
	[Photos_Id] [int] NOT NULL,
	[Sequence] [int] NULL,
	[Type] [nvarchar](255) NULL,
	[URL] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [HondaNA].[Photos#Raw]    Script Date: 6/19/2013 4:52:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HondaNA].[Photos#Raw](
	[LoadId] [int] NOT NULL,
	[Photos_Id] [int] NOT NULL,
	[VIN_Id] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [HondaNA].[Vehicle]    Script Date: 6/19/2013 4:52:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [HondaNA].[Vehicle](
	[VIN] [nchar](17) NOT NULL,
	[Model_ID] [varchar](255) NULL,
	[ModelYear] [smallint] NULL,
	[ModelGroupName] [varchar](255) NULL,
	[Trim] [varchar](255) NULL,
	[MfgColorCd] [varchar](255) NULL,
	[IntColorCd] [varchar](255) NULL,
	[NumberOfDoors] [int] NULL,
	[Transmission] [varchar](255) NULL,
	[BaseColor] [varchar](255) NULL,
	[ExteriorColor] [varchar](255) NULL,
	[InteriorColor] [varchar](255) NULL,
	[InteriorMaterial] [varchar](255) NULL,
	[CreateLoadId] [int] NOT NULL,
	[CreateDate] [datetime] NULL,
	[ModifyDate] [datetime] NULL,
	[Make] [varchar](50) NULL,
 CONSTRAINT [PK_Vehicle_1] PRIMARY KEY CLUSTERED 
(
	[VIN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [HondaNA].[VIN#Raw]    Script Date: 6/19/2013 4:52:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HondaNA].[VIN#Raw](
	[LoadId] [int] NOT NULL,
	[VINs_Id] [int] NOT NULL,
	[VIN_Id] [int] NOT NULL,
	[Model_ID] [nvarchar](255) NULL,
	[VIN_NO] [nchar](17) NULL,
	[ModelYear] [smallint] NULL,
	[ModelGroupName] [nvarchar](255) NULL,
	[Trim] [nvarchar](255) NULL,
	[MfgColorCd] [nvarchar](255) NULL,
	[IntColorCd] [nvarchar](255) NULL,
	[NumberOfDoors] [int] NULL,
	[Transmission] [nvarchar](255) NULL,
	[BaseColor] [nvarchar](255) NULL,
	[ExteriorColor] [nvarchar](255) NULL,
	[InteriorColor] [nvarchar](255) NULL,
	[InteriorMaterial] [nvarchar](255) NULL,
	[Mileage] [int] NULL,
	[MSRP] [decimal](28, 10) NULL,
	[DealerPrice] [decimal](28, 10) NULL,
	[CertificationDate] [nvarchar](255) NULL,
	[StockNumber] [nvarchar](255) NULL,
	[XMRadio] [nvarchar](255) NULL,
	[DealerInstalledAccessories] [nvarchar](255) NULL,
	[CarFaxHistoryURL] [nvarchar](max) NULL,
	[StockThumbImageURL] [nvarchar](max) NULL,
	[StockLargeImageURL] [nvarchar](max) NULL,
	[Make] [nvarchar](255) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [HondaNA].[VINs#Raw]    Script Date: 6/19/2013 4:52:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HondaNA].[VINs#Raw](
	[LoadId] [int] NOT NULL,
	[Dealer_Id] [int] NOT NULL,
	[VINs_Id] [int] NOT NULL
) ON [PRIMARY]

GO
