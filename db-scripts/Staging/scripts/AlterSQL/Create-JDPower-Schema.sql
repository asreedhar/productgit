CREATE SCHEMA JDPower
GO

CREATE TABLE JDPower.Rating ( 
	Year smallint NOT NULL,
	Make varchar(50) NOT NULL,
	Model varchar(50) NOT NULL,
	JDPowerSegment varchar(50) NULL,
	AutoRatingsPageURL varchar(255) NULL,
	PredictedReliabilityRating float NULL,
	IQSOverallRating float NULL,
	APEALOverallRating float NULL,
	VDSOverallRating float NULL,
	CreateDate datetime CONSTRAINT DF_JDPowerRating_CreateDate DEFAULT GetDate() NULL
)

GO
