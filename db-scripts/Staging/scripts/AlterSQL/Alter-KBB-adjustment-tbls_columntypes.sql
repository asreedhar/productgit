ALTER TABLE Kbb.MileageGroupAdjustment
ALTER COLUMN Adjustment DECIMAL(18, 6)
GO

ALTER TABLE Kbb.RegionGroupAdjustment
ALTER COLUMN Adjustment DECIMAL(18, 4)
GO