CREATE TABLE [Aultec].[Photos]
    (
      BusinessUnitId INT NOT NULL
    , InventoryId INT NOT NULL
    , Ordinal INT NOT NULL
    , Url VARCHAR(500) NOT NULL
    )
