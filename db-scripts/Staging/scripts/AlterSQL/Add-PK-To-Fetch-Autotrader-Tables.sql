
set nocount on

-- fix the names of the default constraint for DateStaged

	declare @command varchar(max)
	declare @commands table (command varchar(max))

	insert @commands (command)
	select 
	--	 s.name, t.name, dc.*
		'alter table [' + s.name + '].[' + t.name + '] drop constraint [' + dc.name + ']' + char(13) + char(10) +
		'alter table [' + s.name + '].[' + t.name + '] add constraint [DF_' + t.name + '_DateStaged] default (getdate()) for DateStaged' 
	from
		sys.default_constraints dc
		join sys.tables t
			on dc.parent_object_id = t.object_id
		join sys.schemas s
			on dc.schema_id = s.schema_id
	where
		s.name in ('Merchandising', 'postings')
		and t.name in
		(
			  'AutoTraderExSummary'
			, 'AutoTraderFetchError'
			, 'AutoTraderPerfSummary'
			, 'AutoTraderVehOnline'
			, 'AutoTraderVehSummary'
			, 'PhoneLeads'
			, 'CarsDotComExSummary'
			, 'CarsDotComFeedError'
			, 'CarsDotComPerfSummary'
			, 'CarsDotComVehOnline'
			, 'CarsDotComVehSummary'
			, 'CarsDotComPhoneLeads'
		)
		and exists
		(
			select * from sys.columns c where object_id = t.object_id and name = 'DateStaged'
		)
	order by
		s.name,
		t.name

	while (1=1)
	begin
		select top 1 @command = command from @commands
		if (@@rowcount <> 1) break
		delete @commands where command = @command
		
		exec (@command)
	end

	go



-- add an identity column to the tables that don't have one

	if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'Merchandising' and TABLE_NAME = 'AutoTraderExSummary' and COLUMN_NAME = 'insertId')
		alter table Merchandising.AutoTraderExSummary add insertId bigint identity(1,1) not null
	go

	if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'Merchandising' and TABLE_NAME = 'AutoTraderPerfSummary' and COLUMN_NAME = 'insertId')
		alter table Merchandising.AutoTraderPerfSummary add insertId bigint identity(1,1) not null
	go

	if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'Merchandising' and TABLE_NAME = 'AutoTraderVehOnline' and COLUMN_NAME = 'insertId')
		alter table Merchandising.AutoTraderVehOnline add insertId bigint identity(1,1) not null
	go

	if not exists(select * from INFORMATION_SCHEMA.columns where TABLE_SCHEMA = 'Merchandising' and TABLE_NAME = 'AutoTraderVehSummary' and COLUMN_NAME = 'insertId')
		alter table Merchandising.AutoTraderVehSummary add insertId bigint identity(1,1) not null
	go




-- fix nullable columns
	alter table Merchandising.AutoTraderFetchError alter column Load_ID int not null
	go
	

-- add a PK to the staging tables

	if object_id('Merchandising.PK_AutoTraderExSummary') is null
		alter table Merchandising.AutoTraderExSummary add constraint PK_AutoTraderExSummary primary key clustered (Load_ID, ExtractRequestHandle, insertId)
	go

	if object_id('Merchandising.PK_AutoTraderFetchError') is null
		alter table Merchandising.AutoTraderFetchError add constraint PK_AutoTraderFetchError primary key clustered (Load_ID, ExtractRequestHandle, AutoTraderFetchErrorID)
	go

	if object_id('Merchandising.PK_AutoTraderPerfSummary') is null
		alter table Merchandising.AutoTraderPerfSummary add constraint PK_AutoTraderPerfSummary primary key clustered (Load_ID, ExtractRequestHandle, insertId)
	go

	if object_id('Merchandising.PK_AutoTraderVehOnline') is null
		alter table Merchandising.AutoTraderVehOnline add constraint PK_AutoTraderVehOnline primary key clustered (Load_ID, ExtractRequestHandle, insertId)
	go

	if object_id('Merchandising.PK_AutoTraderVehSummary') is null
		alter table Merchandising.AutoTraderVehSummary add constraint PK_AutoTraderVehSummary primary key clustered (Load_ID, ExtractRequestHandle, insertId)
	go

	if object_id('postings.PK_PhoneLeads') is null
		alter table postings.PhoneLeads add constraint PK_PhoneLeads primary key clustered (Load_ID, ExtractRequestHandle, leadId)
	go


