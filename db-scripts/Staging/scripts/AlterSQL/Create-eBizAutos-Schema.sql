CREATE SCHEMA eBizAutos
GO

CREATE TABLE eBizAutos.Vehicle	(
		Dealer_ID	INT,		--	eBizAutos Dealer ID
		Company_Name	VARCHAR(50),	--	Company Name
		Company_Address	VARCHAR(50),	--	Company Address
		Company_City	VARCHAR(35),	--	Company City
		Company_State	VARCHAR(2),	--	Company State
		Company_Zip	VARCHAR(5),	--	Company Zip Code
		Company_Phone	VARCHAR(20),	--	Company Phone Number
		Listing_ID	INT,		--	eBizAutos Listing ID
		VIN_No		VARCHAR(17),	--	Vehicle VIN
		New_Used	VARCHAR(10),	--	New/Used Status
		Stock_No	VARCHAR(30),	--	Stock Number
		Year		VARCHAR(4),	--	Year
		Make		VARCHAR(40),	--	Make
		Model		VARCHAR(150),	--	Model
		Body_Style	VARCHAR(100),	--	Body Style
		Doors		VARCHAR(8),	--	Number of Doors
		Trim		VARCHAR(100),	--	Trim/Submodel
		Ext_Color	VARCHAR(100),	--	Exterior Color
		Int_Color	VARCHAR(100),	--	Interior Color
		Int_Surface	VARCHAR(20),	--	Interior Surface
		Engine		VARCHAR(50),	--	Engine Description
		Fuel		VARCHAR(12),	--	Fuel Type
		Drivetrain	VARCHAR(20),	--	Drivetrain
		Transmission	VARCHAR(100),	--	Transmission
		Mileage		INT,		--	Mileage
		Internet_Price	INT,		--	Price
		Certified	BIT,		--	Certified/CPO
		Options		VARCHAR(8000),	--	Options List (comma separated)
		Description	VARCHAR(8000),	--	Description
		Photo_URLs	VARCHAR(8000),	--	Photo URLs (comma separated)
		Date_In_Stock	VARCHAR(10),	--	Date In Stock
		ChromeStyleID	INT
		
		)


GO
