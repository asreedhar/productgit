
DECLARE @AssignmentExpressionID SMALLINT

INSERT
INTO	Listing.AssignmentExpression (Description, ColumnName, Expression)
SELECT	'Parse SellerID from SellerURL', 'SellerID', 'CASE WHEN NULLIF(SellerURL,'''') IS NULL THEN ''0'' ELSE SUBSTRING(SellerURL,CHARINDEX(''&dealer_id='', SellerURL) + 11, CHARINDEX(''&car_year='', SellerURL) -CHARINDEX(''&dealer_id='', SellerURL) - 11) END'

SET @AssignmentExpressionID = SCOPE_IDENTITY()

INSERT
INTO	Listing.ProviderVehicleAssignmentExpression (ProviderID, AssignmentExpressionID, ExecutionOrder)
SELECT	ProviderID, @AssignmentExpressionID, 1
FROM	Listing.Provider P	
WHERE	ProviderCode = 'HerbieCardinal' 

GO

UPDATE	Listing.Vehicle
SET	SellerID	= CASE WHEN NULLIF(SellerURL,'') IS NULL THEN '0' ELSE SUBSTRING(SellerURL,CHARINDEX('&dealer_id=', SellerURL) + 11, CHARINDEX('&car_year=', SellerURL) -CHARINDEX('&dealer_id=', SellerURL) - 11) END
WHERE	_PartitionStatusCD = 1
	AND _ProviderID = 19

GO

ALTER TABLE Listing.Vehicle ADD _LastUpdateLoadID INT NULL

GO

DROP INDEX Listing.Vehicle.IX_ListingVehicle__ProviderID_ProviderRowID	

CREATE NONCLUSTERED INDEX IX_ListingVehicle__ProviderID_ProviderRowIDSellerID ON Listing.Vehicle (_PartitionStatusCD, _ProviderID, _ProviderRowID, SellerID) WITH (FILLFACTOR=90, ONLINE=ON) ON PS_ListingVehiclePartitionStatus (_PartitionStatusCD)
GO


ALTER TABLE Listing.Vehicle_Rolloff ADD _LastUpdateLoadID INT NULL

	