
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [Listing].[DeltaLog](
	[LoadId] [int] NOT NULL,
	[ProviderId] [int] NOT NULL,
	[SourceRecordType] [varchar](100) NULL,
	[ProcessStatusCD] [tinyint] NULL,
	[VIN] [varchar](17) NULL,
	[SellerName] [varchar](75) NULL,
	[SellerZipCode] [varchar](10) NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


