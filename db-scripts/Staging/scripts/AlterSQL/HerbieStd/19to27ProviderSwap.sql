--EXEC Utility.dbo.ScriptData @schema = 'Listing', -- varchar(100)
--    @table = 'Provider', -- varchar(100)
--    @Where = ' where providerid in (19,27) ', -- varchar(max)
--    @DB = 'Market', -- varchar(100)
--    @IfNotExists = 1, -- bit
--    @Update = 1, -- bit
--    @IDInsert = NULL, -- bit
--    @UseTransaction = NULL, -- bit
--    @ScriptDatabase = 1, -- bit
--    @Debug = 0 -- bit


SELECT * FROM Staging.Listing.Provider WHERE ProviderID IN (19,27)

UPDATE Staging.Listing.ProviderSET  [DatafeedCode] = 'Herbie',[ActiveLoadID] = null,[ProviderFeedTypeID] = 2,[ProviderCode] = 'HerbieCardinal',[ResponseFilePending] = 0,[LastUpdateDT] = 'Apr 17 2014  2:38PM'	,[LastUpdateUser] = 'FIRSTLOOK\sqlsvcbeta',[DeleteThresholdPct] = NULL,[IsVideoUrlProvider] = 0 WHERE 1=1  AND [ProviderID] = 27 
UPDATE Staging.Listing.ProviderSET  [DatafeedCode] = 'HerbieStd',[ActiveLoadID] = null,[ProviderFeedTypeID] = 3,[ProviderCode] = 'Cardinal',[ResponseFilePending] = 0,[LastUpdateDT] = 'Apr 18 2014  6:58AM',[LastUpdateUser] = 'FIRSTLOOK\sqlsvcbeta',[DeleteThresholdPct] = null,[IsVideoUrlProvider] = 0 WHERE 1=1  AND [ProviderID] = 19 


TRUNCATE TABLE Listing.Vehicle_Rolloff  
ALTER TABLE Listing.Vehicle_History SWITCH PARTITION $PARTITION.[PF_PartitionProvider] (19) TO Listing.Vehicle_Rolloff 
TRUNCATE TABLE Listing.Vehicle_Rolloff  
ALTER TABLE Listing.Vehicle_preload SWITCH PARTITION $PARTITION.[PF_PartitionProvider] (19) TO Listing.Vehicle_Rolloff 

/*Empty 27 and move Cardinal Custom (19) Active set (for request continuity)*/
TRUNCATE TABLE Listing.Vehicle_Rolloff  
ALTER TABLE Listing.Vehicle SWITCH PARTITION $PARTITION.[PF_PartitionProvider] (27) TO Listing.Vehicle_Rolloff 
TRUNCATE TABLE Listing.Vehicle_Rolloff  
ALTER TABLE Listing.Vehicle SWITCH PARTITION $PARTITION.[PF_PartitionProvider] (19) TO Listing.Vehicle_Rolloff 

UPDATE Listing.Vehicle_Rolloff SET _providerid=27 

/*
Make Rolloff match vehicle to swap data back in
*/
CREATE NONCLUSTERED INDEX [IX_Listing_Vehicle_ProviderId_VIN_1] ON [Listing].Vehicle_Rolloff
(	[_ProviderID] ASC,	[VIN] ASC )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

CREATE NONCLUSTERED INDEX [IX_Listing_Vehicle_ProviderId_Stock_Seller_Phone_1] ON [Listing].Vehicle_Rolloff
(	[_ProviderID] ASC,	[StockNumber] ASC,	[SellerName] ASC,	[SellerPhoneNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
CREATE NONCLUSTERED INDEX [IX_Listing_Vehicle_ProviderId_ProviderRowID_SellerId_1] ON [Listing].Vehicle_Rolloff
([_ProviderID] ASC,[_ProviderRowID] ASC,[SellerID] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
ALTER TABLE Listing.Vehicle_Rolloff ADD CONSTRAINT CK_Vehicle_Rolloff CHECK (_ProviderId=27)

ALTER TABLE Listing.Vehicle_Rolloff SWITCH TO Listing.Vehicle PARTITION $PARTITION.[PF_PartitionProvider] (27) 

/*Undo Chages to rolloff*/
ALTER TABLE Listing.Vehicle_Rolloff DROP  CONSTRAINT CK_Vehicle_Rolloff 
DROP INDEX [IX_Listing_Vehicle_ProviderId_ProviderRowID_SellerId_1] ON [Listing].Vehicle_Rolloff
DROP INDEX [IX_Listing_Vehicle_ProviderId_Stock_Seller_Phone_1] ON [Listing].Vehicle_Rolloff
DROP INDEX [IX_Listing_Vehicle_ProviderId_VIN_1] ON [Listing].Vehicle_Rolloff



GO
UPDATE pvx SET ProviderID=27 
FROM 	Listing.ProviderVehicleAssignmentExpression PVX
		--INNER JOIN Listing.AssignmentExpression X ON PVX.AssignmentExpressionID = X.ID
	WHERE	PVX.ProviderID  =19

INSERT Market.Listing.Provider ( ProviderID,Description,Priority,ProviderStatusID,DataloadID,LastStatusChange,ChangedByUser,OptionDelimiter)
VALUES  ( 26, 'HerbieStd',101,0,0, GETDATE(),'FIRSTLOOK\bfultz', '|')
INSERT Market.Listing.Provider ( ProviderID,Description,Priority,ProviderStatusID,DataloadID,LastStatusChange,ChangedByUser,OptionDelimiter)
VALUES  ( 27, 'CardinalStd',102,0,0, GETDATE(),'FIRSTLOOK\bfultz', '|')


UPDATE p
	SET ProviderStatusID=0,Priority=102
FROM Market.listing.PROVIDER p
WHERE ProviderID=19
UPDATE p
	SET ProviderStatusID=1,Priority=3
FROM Market.listing.PROVIDER p
WHERE ProviderID=27

UPDATE Market.Listing.Provider	SET  [Description] = 'CardinalStd',[Priority] = 3,[ProviderStatusID] = 1,[DataloadID] = 0		,[LastStatusChange] = 'Apr 21 2014  1:32PM',[ChangedByUser] = 'FIRSTLOOK\bfultz',[OptionDelimiter] = '|' 	WHERE 1=1  AND [ProviderID] = 19 

	UPDATE Market.Listing.Provider	SET  [Description] = 'HerbieCardinal',[Priority] = 102,[ProviderStatusID] = 0,[DataloadID] = NULL,[LastStatusChange] = 'Apr 13 2014 12:45AM',[ChangedByUser] = 'FIRSTLOOK\sqlsvcprod02'	,[OptionDelimiter] = ',' 	WHERE 1=1  AND [ProviderID] = 27
