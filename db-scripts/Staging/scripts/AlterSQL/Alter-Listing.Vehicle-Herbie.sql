ALTER TABLE Listing.Vehicle ALTER COLUMN Certified VARCHAR(25) NULL

ALTER TABLE Listing.Vehicle ALTER COLUMN SellerURL VARCHAR(500) NULL

INSERT
INTO	Listing.Provider (ProviderID, DatafeedCode, ActiveLoadID)
SELECT	12, 'Herbie', 0


CREATE TABLE Listing.ProviderFeedType (
		ProviderFeedTypeID	TINYINT NOT NULL CONSTRAINT PK_ListingProviderFeedType PRIMARY KEY CLUSTERED,
		Description		VARCHAR(50) NOT NULL
		)

INSERT	
INTO	Listing.ProviderFeedType (ProviderFeedTypeID, Description)
SELECT	1, 'Full Extract'
UNION 
SELECT	2, 'Request/Response'	

ALTER TABLE Listing.Provider ADD ProviderFeedTypeID TINYINT NULL
GO
UPDATE	Listing.Provider
SET	ProviderFeedTypeID = CASE WHEN ProviderID = 12 THEN 2 ELSE 1 END

ALTER TABLE Listing.Provider ALTER COLUMN ProviderFeedTypeID TINYINT NOT NULL 

ALTER TABLE Listing.Provider ADD CONSTRAINT FK_ListingProvider__ProviderFeedType FOREIGN KEY (ProviderFeedTypeID) REFERENCES Listing.ProviderFeedType(ProviderFeedTypeID)

GO

------------------------------------------------------------------
--	
--	Changing the partition status "types"
--	Status #1 will contain the active "image" of the listing
--	data.  Partition #2 will contain data to be processed.
--	This may be a full extract (e.g. TCUV or AIM) or a delta
--	file (Herbie)
--
------------------------------------------------------------------

EXEC sp_SetColumnDescription 
	'Listing.Vehicle._PartitionStatusCD', 
	'Partition status.  Currently, 0 = Archive, 1 = Active Image, 2 = To Be Processed'
GO	

EXEC sp_SetColumnDescription 
	'Listing.Vehicle._ProcessStatusCD', 
	'Staging Process status.  For fetch.com request/response, 1=Insert, 2=Update, 3=Delete.'
GO

				
ALTER TABLE Listing.Provider ADD ProviderCode VARCHAR(20) NULL
GO
EXEC sp_SetColumnDescription 
	'Listing.Provider.ProviderCode', 
	'Provider Code.  Used to distinguish between distinct listing sources sent via the same feed that share the same data shape/process type (e.g. Herbie*)'
GO

UPDATE	Listing.Provider
SET	ProviderCode = 'Herbie'
WHERE	DatafeedCode = 'Herbie'				

INSERT
INTO	Listing.Provider (ProviderID, DatafeedCode, ActiveLoadID, ProviderFeedTypeID, ProviderCode)
SELECT	13, 'Herbie', 0, 2, 'HerbiePlus'

GO
INSERT
INTO	Listing.Provider (ProviderID, DatafeedCode, ActiveLoadID, ProviderFeedTypeID, ProviderCode)
SELECT	14, 'Herbie', 0, 2, 'Herbie3'

INSERT
INTO	Listing.Provider (ProviderID, DatafeedCode, ActiveLoadID, ProviderFeedTypeID, ProviderCode)
SELECT	17, 'Herbie', 0, 2, 'Herbie4'

GO

ALTER TABLE Listing.Vehicle ALTER COLUMN Model VARCHAR(75) NULL
GO

ALTER TABLE Listing.Provider ADD ResponseFilePending BIT NOT NULL CONSTRAINT DF_ListingProvider__ResponseFilePending DEFAULT (0)

EXEC dbo.sp_SetColumnDescription 'Listing.Provider.ResponseFilePending', 'Flags whether there is a response file pending from fetch that needs to be processed.  If a response file is pending, all subsequent list-level files will be "badded" until a response file is processed.'

ALTER TABLE Listing.Provider ADD LastUpdateDT SMALLDATETIME NULL
ALTER TABLE Listing.Provider ADD LastUpdateUser SYSNAME NULL 

GO

IF EXISTS (	SELECT	1
		FROM	sys.indexes I
		WHERE	object_id = OBJECT_id('Listing.Vehicle')
			AND name = 'IX_ListingVehicle__ProviderIDStockSeller'
		)

	DROP INDEX Listing.Vehicle.IX_ListingVehicle__ProviderIDStockSeller
	
CREATE NONCLUSTERED INDEX IX_ListingVehicle__ProviderIDStockSeller ON Listing.Vehicle(_PartitionStatusCD, _ProviderID, StockNumber, SellerName, SellerCity, SellerState, SellerPhoneNumber) WITH(DROP_EXISTING=OFF, FILLFACTOR=90, ONLINE = ON) ON PS_ListingVehiclePartitionStatus(_PartitionStatusCD)

IF EXISTS (	SELECT	1
		FROM	sys.indexes I
		WHERE	object_id = OBJECT_id('Listing.Vehicle')
			AND name = 'IX_ListingVehicle__PartitionProviderVIN'
		)

	DROP INDEX Listing.Vehicle.IX_ListingVehicle__PartitionProviderVIN
		
CREATE NONCLUSTERED INDEX IX_ListingVehicle__PartitionProviderVIN ON Listing.Vehicle (_PartitionStatusCD, _ProviderID, VIN) WITH(DROP_EXISTING=OFF, FILLFACTOR=90, ONLINE = ON) ON PS_ListingVehiclePartitionStatus (_PartitionStatusCD) 

GO
IF OBJECT_ID('[Listing].[Vehicle_Rolloff]') IS NOT NULL DROP TABLE [Listing].[Vehicle_Rolloff]

CREATE TABLE [Listing].[Vehicle_Rolloff]
(
[_LoadID] [int] NOT NULL,
[_AuditID] [int] NOT NULL,
[_ProviderRowID] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[_ProviderID] [tinyint] NOT NULL,
[_PartitionStatusCD] [tinyint] NOT NULL,
[_ProcessStatusCD] [tinyint] NOT NULL,
[Source] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StockNumber] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StockType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DealerCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DealerType] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VIN] [varchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Make] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModelYear] [varchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Trim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Mileage] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExteriorColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InteriorColor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExteriorColorCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InteriorColorCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DriveTrain] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Engine] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Transmission] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FuelType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChromeStyleId] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSRP] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvoicePrice] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListPrice] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ListingDate] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerID] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerName] [varchar] (75) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerAddress1] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerAddress2] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerCity] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerState] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerZipCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerURL] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerPhoneNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerEMail] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Certified] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CertificationNumber] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Options] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OptionCodes] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SellerDescription] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LotLocation] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TagNumber] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TagState] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImageURLs] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImageModifiedDate] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ImageCount] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [Archive]

ALTER TABLE [Listing].Vehicle_Rolloff ADD CONSTRAINT [CK_ListingVehicle_Rolloff___PartitionStatusCD] CHECK (([_PartitionStatusCD]=(2) OR [_PartitionStatusCD]=(1) OR [_PartitionStatusCD]=(0)))

ALTER TABLE [Listing].Vehicle_Rolloff ADD CONSTRAINT [PK_Listing] PRIMARY KEY CLUSTERED  ([_PartitionStatusCD], [_ProviderID], [_AuditID]) ON [Archive]

GO
ALTER TABLE Listing.Vehicle_Rolloff ALTER COLUMN Model VARCHAR(75) NULL

ALTER TABLE Listing.Vehicle_Rolloff ALTER COLUMN Certified VARCHAR(25) NULL

ALTER TABLE Listing.Vehicle_Rolloff ALTER COLUMN SellerURL VARCHAR(500) NULL

ALTER TABLE Listing.Vehicle_Rolloff DROP CONSTRAINT [PK_Listing]

ALTER TABLE [Listing].[Vehicle_Rolloff] ADD CONSTRAINT [PK_Listing] PRIMARY KEY CLUSTERED  ([_PartitionStatusCD], [_ProviderID], [_LoadID], [_AuditID]) ON [Archive]
GO 
