CREATE SCHEMA Manheim
GO
CREATE TABLE Manheim.Sales (
	[Channel.Code]			VARCHAR(10) NOT NULL,
	[Auction.Id]			VARCHAR(10) NOT NULL,
	[Auction.Name]			VARCHAR(50) NOT NULL,
	[Auction.Description]		VARCHAR(50) NOT NULL,
	[Auction.Consignor]		VARCHAR(10) NOT NULL,
	[Auction.Lane]			VARCHAR(10) NOT NULL,
	[Auction.SaleDate]		SMALLDATETIME NOT NULL,	
	[Auction.SaleNumber]		VARCHAR(10) NOT NULL,
	[Auction.NumberOfVehicles]	SMALLINT NOT NULL
	)
	
CREATE TABLE Manheim.Listings (
	[Uid]						UNIQUEIDENTIFIER NOT NULL,	
	[Consignor]					VARCHAR(10) NOT NULL,
	[SaleDate]					SMALLDATETIME NOT NULL,
	[Channel]					VARCHAR(10) NOT NULL,
	[Comments]					VARCHAR(100) NOT NULL,
	[DateModified]					SMALLDATETIME NULL,		-- should ask Manheim why we get NULL values
	[PhysicalLocation]				VARCHAR(50) NOT NULL,
	[SaleInfo.LaneNumber]				VARCHAR(10) NOT NULL,
	[SaleInfo.RunNumber]				VARCHAR(50) NOT NULL,
	[SaleInfo.SaleNumber]				VARCHAR(10) NOT NULL,
	[SaleInfo.SaleYear]				VARCHAR(50) NOT NULL,
	[Seller.Address.City]				VARCHAR(50) NOT NULL,
	[Seller.Address.Country]			VARCHAR(50) NOT NULL,
	[Seller.Address.PostalCode]			VARCHAR(50) NOT NULL,
	[Seller.Address.StateProviceRegion]		VARCHAR(50) NOT NULL,
	[Vehicle.VehicleType.Year.Id]			VARCHAR(50) NOT NULL,
	[Vehicle.VehicleType.Year.Name]			VARCHAR(50) NOT NULL,
	[Vehicle.VehicleType.Make.Id]			VARCHAR(50) NOT NULL,
	[Vehicle.VehicleType.Make.Name]			VARCHAR(50) NOT NULL,
	[Vehicle.VehicleType.Model.Id]			VARCHAR(50) NOT NULL,
	[Vehicle.VehicleType.Model.Name]		VARCHAR(50) NOT NULL,
	[Vehicle.VehicleType.Body.Id]			VARCHAR(50) NOT NULL,
	[Vehicle.VehicleType.Body.Name]			VARCHAR(50) NOT NULL,
	[Vehicle.VehicleType.Mid]			VARCHAR(50) NOT NULL,
	[Vehicle.VehicleType.VinInfo.VIN]		CHAR(17) NOT NULL,
	[Vehicle.VehicleType.VinInfo.VinPrefix8]	VARCHAR(50) NOT NULL,
	[Vehicle.VehicleType.VinInfo.YearCharacter]	VARCHAR(50) NOT NULL,
	[Vehicle.VehicleDetail.Description]		VARCHAR(50) NOT NULL,
	[Vehicle.VehicleDetail.InvoicePrice]		VARCHAR(50) NOT NULL,
	[Vehicle.VehicleDetail.Mileage]			VARCHAR(50) NOT NULL,
	[Vehicle.VehicleDetail.MsrPrice]		VARCHAR(50) NOT NULL,
	[Vehicle.VehicleDetail.RetailPrice]		VARCHAR(50) NOT NULL,
	[Vehicle.VehicleDetail.StockNumber]		VARCHAR(50) NOT NULL,
	[FacilitatingAuction]				VARCHAR(50) NOT NULL,
	[DeepLink]					VARCHAR(200) NOT NULL
	)
	
CREATE CLUSTERED INDEX IXC_ManheimListings  ON  Manheim.Listings(Channel, [Consignor], PhysicalLocation, [SaleInfo.LaneNumber], [SaleInfo.RunNumber], [SaleInfo.SaleNumber], [Vehicle.VehicleType.VinInfo.VIN])
	
CREATE TABLE Manheim.ListingsOptions (
	Uid			UNIQUEIDENTIFIER NOT NULL,
	OptionCode		VARCHAR(20) NOT NULL,
	OptionDisplay		VARCHAR(50) NOT NULL,
	OptionValue		VARCHAR(50) NOT NULL
	)

CREATE CLUSTERED INDEX IXC_ManheimsOptions  ON  Manheim.ListingsOptions(Uid, OptionCode)

CREATE TABLE Manheim.Locations (
	[Auction.Name]			VARCHAR(100) NOT NULL,
	City				VARCHAR(50) NOT NULL,
	[State]				VARCHAR(3) NOT NULL,
	Zip				VARCHAR(10) NOT NULL
	)
GO	

CREATE VIEW Manheim.VehicleAttributes
AS

SELECT	Uid, ExteriorColor, Transmission, Engine
FROM	(	SELECT	Uid, OptionCode, OptionDisplay 
		FROM	Manheim.ListingsOptions
		WHERE	OptionCode IN ('ExteriorColor', 'Transmission','Engine')
	) P
PIVOT
(	MAX(P.OptionDisplay)
FOR	[OptionCode] IN ([ExteriorColor],[Transmission], [Engine])
) AS PVT

GO


CREATE VIEW Manheim.VehicleOptionList
AS
SELECT  LO1.Uid,
	OptionList	= LEFT(STUFF((	SELECT	DISTINCT COALESCE(',' + CASE WHEN OptionValue = 'Y'
								THEN OptionDisplay
								WHEN OptionValue = 'NONE'
								THEN NULL
								ELSE OptionDisplay + ' ' + OptionCode
--								ELSE OptionValue + ' ' + OptionDisplay
								END,
							 '')		   
							   
					FROM	Manheim.ListingsOptions LO2
					WHERE	OptionCode NOT IN ('ExteriorColor', 'Transmission','Engine')
						AND LO1.Uid = LO2.Uid
					FOR XML PATH('')
				),1,1,''),1000) 

FROM	Manheim.ListingsOptions LO1

GROUP
BY	LO1.Uid

GO

CREATE VIEW Manheim.Listings_Extract
--------------------------------------------------------------------------------
--
--	FIND THE MOST RECENT LISTING PER NATURAL KEY
--	MAKING SOME ASSUMPTIONS AS WE ARE RECEIVING DUPS IN THE DATA
--	BEST WE CAN DO AT THIS TIME
--	
--------------------------------------------------------------------------------
AS

SELECT	L.Uid,
	L.Consignor,
	L.SaleDate,
	L.Channel,
	L.Comments,
	L.DateModified,
	L.PhysicalLocation,
	L.[SaleInfo.LaneNumber],
	L.[SaleInfo.RunNumber],
	L.[SaleInfo.SaleNumber],
	L.[SaleInfo.SaleYear],
	L.[Seller.Address.City],
	L.[Seller.Address.Country],
	L.[Seller.Address.PostalCode],
	L.[Seller.Address.StateProviceRegion],
	L.[Vehicle.VehicleType.Year.Id],
	L.[Vehicle.VehicleType.Year.Name],
	L.[Vehicle.VehicleType.Make.Id],
	L.[Vehicle.VehicleType.Make.Name],
	L.[Vehicle.VehicleType.Model.Id],
	L.[Vehicle.VehicleType.Model.Name],
	L.[Vehicle.VehicleType.Body.Id],
	L.[Vehicle.VehicleType.Body.Name],
	L.[Vehicle.VehicleType.Mid],
	L.[Vehicle.VehicleType.VinInfo.VIN],
	L.[Vehicle.VehicleType.VinInfo.VinPrefix8],
	L.[Vehicle.VehicleType.VinInfo.YearCharacter],
	L.[Vehicle.VehicleDetail.Description],
	L.[Vehicle.VehicleDetail.InvoicePrice],
	L.[Vehicle.VehicleDetail.Mileage],
	L.[Vehicle.VehicleDetail.MsrPrice],
	L.[Vehicle.VehicleDetail.RetailPrice],
	L.[Vehicle.VehicleDetail.StockNumber],
	L.FacilitatingAuction,
	L.DeepLink,
	VA.ExteriorColor,
	VA.Transmission,
	VA.Engine, 
	VOL.OptionList
FROM	Manheim.Listings L
	INNER JOIN (	SELECT	L.Channel, L.[Consignor], L.PhysicalLocation, L.[SaleInfo.LaneNumber], L.[SaleInfo.RunNumber], L.[SaleInfo.SaleNumber], [Vehicle.VehicleType.VinInfo.VIN], 
				MostRecentDateModified	= MAX([DateModified]),
				HighestMileage		= MAX([Vehicle.VehicleDetail.Mileage])
			FROM	Manheim.Listings L
			GROUP 
			BY	L.Channel, L.[Consignor], L.PhysicalLocation, L.[SaleInfo.LaneNumber], L.[SaleInfo.RunNumber], L.[SaleInfo.SaleNumber], [Vehicle.VehicleType.VinInfo.VIN] 
			) MRL ON	L.Channel				= MRL.Channel
					AND L.[Consignor]			= MRL.[Consignor]
					AND L.PhysicalLocation			= MRL.PhysicalLocation
					AND L.[SaleInfo.LaneNumber]		= MRL.[SaleInfo.LaneNumber]
					AND L.[SaleInfo.RunNumber]		= MRL.[SaleInfo.RunNumber]
					AND L.[SaleInfo.SaleNumber]		= MRL.[SaleInfo.SaleNumber]
					AND L.[Vehicle.VehicleType.VinInfo.VIN] = MRL.[Vehicle.VehicleType.VinInfo.VIN] 
					AND L.DateModified			= MRL.MostRecentDateModified
					AND L.[Vehicle.VehicleDetail.Mileage]	= MRL.HighestMileage
	LEFT JOIN Manheim.VehicleAttributes VA ON L.Uid = VA.Uid
	LEFT JOIN Manheim.VehicleOptionList VOL ON L.Uid = VOL.Uid					

GO

--------------------------------------------------------------------------------
--
--	Don't hate me because I am a hack.  Refactored loader due late Q2 2011.
--
--------------------------------------------------------------------------------

CREATE PROC Manheim.TruncateLoad
AS
SET NOCOUNT ON

TRUNCATE TABLE Manheim.Sales 
TRUNCATE TABLE Manheim.Listings 
TRUNCATE TABLE Manheim.ListingsOptions 

BULK INSERT Manheim.Sales FROM '\\PRODDB03SQL\Datafeeds\Auction-Standard\PRESALES--Sales.txt'	
WITH (FIRSTROW = 2, FIELDTERMINATOR = '|', MAXERRORS = 10 )
BULK INSERT Manheim.Listings FROM '\\PRODDB03SQL\Datafeeds\Auction-Standard\PRESALES--Listings.txt'	
WITH (FIRSTROW = 2, FIELDTERMINATOR = '|', MAXERRORS = 10 )
BULK INSERT Manheim.ListingsOptions FROM '\\PRODDB03SQL\Datafeeds\Auction-Standard\PRESALES--ListingsOptions.txt'	
WITH (FIRSTROW = 2, FIELDTERMINATOR = '|', MAXERRORS = 10 )


BULK INSERT Manheim.Sales FROM '\\PRODDB03SQL\Datafeeds\Auction-Standard\SIMULCAST--Sales.txt'	
WITH (FIRSTROW = 2, FIELDTERMINATOR = '|', MAXERRORS = 10 )
BULK INSERT Manheim.Listings FROM '\\PRODDB03SQL\Datafeeds\Auction-Standard\SIMULCAST--Listings.txt'	
WITH (FIRSTROW = 2, FIELDTERMINATOR = '|', MAXERRORS = 10 )
BULK INSERT Manheim.ListingsOptions FROM '\\PRODDB03SQL\Datafeeds\Auction-Standard\SIMULCAST--ListingsOptions.txt'	
WITH (FIRSTROW = 2, FIELDTERMINATOR = '|', MAXERRORS = 10 )

BULK INSERT Manheim.Listings FROM '\\PRODDB03SQL\Datafeeds\Auction-Standard\DEALEREXCHANGE--Listings.txt'	
WITH (FIRSTROW = 2, FIELDTERMINATOR = '|', MAXERRORS = 10 )
BULK INSERT Manheim.ListingsOptions FROM '\\PRODDB03SQL\Datafeeds\Auction-Standard\DEALEREXCHANGE--ListingsOptions.txt'	
WITH (FIRSTROW = 2, FIELDTERMINATOR = '|', MAXERRORS = 10 )

GO

IF EXISTS (SELECT 1 FROM sys.database_principals WHERE name = 'StagingReader' AND type_desc = 'SQL_USER') BEGIN
	GRANT SELECT ON Manheim.Listings TO StagingReader
	GRANT SELECT ON Manheim.ListingsOptions	TO StagingReader
	GRANT SELECT ON Manheim.Locations TO StagingReader
	GRANT SELECT ON Manheim.Sales TO StagingReader
	GRANT SELECT ON Manheim.Listings_Extract TO StagingReader
END	
	