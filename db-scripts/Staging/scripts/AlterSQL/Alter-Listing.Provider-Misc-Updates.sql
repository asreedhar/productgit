ALTER TABLE Listing.Provider ADD DeleteThresholdPct TINYINT NULL
GO
EXEC sp_SetColumnDescription 
	'Listing.Provider.DeleteThresholdPct', 
	'Delta Process Delete Short-Circuit Threshold.  When processing a list-level file, if the percentage of vehicles marked as delete exceeds the delete threshold, the load process short-circuits processing of the file in case the file was incomplete.' 
GO

UPDATE	Listing.Provider
SET 	DeleteThresholdPct = 25
WHERE	ProviderFeedTypeID = 2	-- Herbie

GO

CREATE TABLE Listing.AssignmentExpression (
	ID		SMALLINT IDENTITY(1,1) NOT NULL CONSTRAINT PK_ListingAssignmentExpression PRIMARY KEY,
	Description	VARCHAR(100) NOT NULL,
	ColumnName	SYSNAME NOT NULL,
	Expression	VARCHAR(1000) NOT NULL,
	AssignmentExpressionID AS (ID)
	)


CREATE TABLE Listing.ProviderVehicleAssignmentExpression (
	ProviderID			TINYINT NOT NULL,
	AssignmentExpressionID		SMALLINT NOT NULL,
	ExecutionOrder			SMALLINT NOT NULL CONSTRAINT DF_ListingProviderVehicleAssignmentExpression__ExecutionOrder DEFAULT (0),
	CONSTRAINT PK_ListingProviderVehicleAssignmentExpression PRIMARY KEY CLUSTERED (ProviderID, AssignmentExpressionID),
	CONSTRAINT FK_ListingProviderVehicleAssignmentExpression__Provider FOREIGN KEY (ProviderID) REFERENCES Listing.Provider(ProviderID),
	CONSTRAINT FK_ListingProviderVehicleAssignmentExpression__AssignmentExpression FOREIGN KEY (AssignmentExpressionID) REFERENCES Listing.AssignmentExpression(ID)
	)

GO

DECLARE @AssignmentExpressionID SMALLINT
	
INSERT
INTO	Listing.AssignmentExpression (Description, ColumnName, Expression)
SELECT	'Parse _ProviderRowID from SellerURL', '_ProviderRowID', 'SUBSTRING(SellerURL,CHARINDEX(''&car_id='', SellerURL) + 8, CHARINDEX(''&dealer_id='', SellerURL) -CHARINDEX(''&car_id='', SellerURL) - 8)'

SET @AssignmentExpressionID = SCOPE_IDENTITY()

INSERT
INTO	Listing.ProviderVehicleAssignmentExpression (ProviderID, AssignmentExpressionID, ExecutionOrder)
SELECT	ProviderID, @AssignmentExpressionID, 1
FROM	Listing.Provider P	
WHERE	ProviderCode = 'HerbieCardinal' 	


GO

CREATE NONCLUSTERED INDEX IX_ListingVehicle__ProviderID_ProviderRowID ON Listing.Vehicle (_PartitionStatusCD, _ProviderID, _ProviderRowID ) WITH (FILLFACTOR=90) ON PS_ListingVehiclePartitionStatus (_PartitionStatusCD)
GO	
