SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO

CREATE SCHEMA Listing
GO

DECLARE @sql NVARCHAR(500)
SELECT @sql = 'ALTER DATABASE [' + DB_NAME() + '] ADD FILEGROUP Archive; ALTER DATABASE [' + DB_NAME() + '] ADD FILE ( NAME = N''Archive'', FILENAME = N''' + Utility.String.GetPathName(physical_name) + '\' + DB_NAME() + '_Archive.ndf' + ''', SIZE = 51200KB , FILEGROWTH = 10%) TO FILEGROUP [Archive]'
FROM	sys.database_files DF
WHERE	name = 'data'

EXEC sp_executesql @sql
GO


/*
SELECT * FROM sys.database_files DF
DROP TABLE Lot.Vehicle

DROP PARTITION SCHEME PS_LotVehicleLoadID
DROP PARTITION FUNCTION PF_LoadID


*/

CREATE PARTITION FUNCTION PF_PartitionStatus(TINYINT)
AS RANGE RIGHT FOR VALUES (1)
GO
CREATE PARTITION SCHEME PS_ListingVehiclePartitionStatus
AS PARTITION PF_PartitionStatus TO ([Archive], [Primary])
GO				      


CREATE TABLE Listing.Vehicle (
	_LoadID			INT NOT NULL,
	_AuditID		INT NOT NULL IDENTITY (1,1),
	_ProviderRowID		VARCHAR(10),
	_ProviderID		TINYINT NOT NULL,
	_PartitionStatusCD	TINYINT NOT NULL,
	_ProcessStatusCD	TINYINT NOT NULL,
	---------------------------------------------------------
	Source			VARCHAR(20) NULL,		
	StockNumber	 	VARCHAR(30) NULL,
	StockType	 	VARCHAR(20) NULL,
	DealerCode	 	VARCHAR(20) NULL,
	DealerType	 	VARCHAR(30) NULL,	
	VIN	 		VARCHAR(17) NULL,
	Make	 		VARCHAR(50) NULL,
	Model	 		VARCHAR(50) NULL,
	ModelYear		VARCHAR(4) NULL,	
	Trim	 		VARCHAR(50) NULL,
	Mileage			VARCHAR(10) NULL,
	ExteriorColor	 	VARCHAR(50) NULL,
	InteriorColor	 	VARCHAR(50) NULL,
	ExteriorColorCode	VARCHAR(10) NULL,
	InteriorColorCode	VARCHAR(10) NULL,
	DriveTrain	 	VARCHAR(50) NULL,
	Engine	 		VARCHAR(50) NULL,
	Transmission	 	VARCHAR(50) NULL,	
	FuelType	 	VARCHAR(50) NULL,
	ChromeStyleId		VARCHAR(10),
	MSRP			VARCHAR(10) NULL,	
	InvoicePrice		VARCHAR(10) NULL,	
	ListPrice		VARCHAR(10) NULL,
	ListingDate		VARCHAR(25) NULL,
	SellerID		VARCHAR(20) NULL,
	SellerName	 	VARCHAR(75) NULL,
	SellerAddress1	 	VARCHAR(100) NULL,
	SellerAddress2	 	VARCHAR(100) NULL,	
	SellerCity		VARCHAR(50) NULL,
	SellerState	 	VARCHAR(50) NULL,
	SellerZipCode	 	VARCHAR(10) NULL,
	SellerURL	 	VARCHAR(200) NULL,
	SellerPhoneNumber	VARCHAR(50) NULL,	
	SellerEMail	 	VARCHAR(100) NULL,
	Certified		VARCHAR(5) NULL,
	CertificationNumber	VARCHAR(10) NULL,
	Options	 		VARCHAR(8000) NULL,
	OptionCodes	 	VARCHAR(500) NULL,
	SellerDescription	VARCHAR(2000) NULL,
	LotLocation	 	VARCHAR(50) NULL,
	TagNumber	 	VARCHAR(10) NULL,
	TagState		VARCHAR(3) NULL,
	ImageURLs	 	VARCHAR(8000) NULL,	
	ImageModifiedDate	VARCHAR(25) NULL,	
	ImageCount		VARCHAR(5) NULL,
	---------------------------------------------
	CONSTRAINT PK_ListingVehicle PRIMARY KEY CLUSTERED (_PartitionStatusCD, _ProviderID, _AuditID) ON PS_ListingVehiclePartitionStatus(_PartitionStatusCD)
) 

GO

ALTER TABLE Listing.Vehicle  WITH CHECK 
ADD CONSTRAINT CK_ListingVehicle___PartitionStatusCD CHECK(_PartitionStatusCD IN (0,1,2))
GO

EXEC sp_SetColumnDescription 
	'Listing.Vehicle._LoadID', 
	'DataloadID as registered in DBASTAT.dbo.Dataload_History.'
GO	

EXEC sp_SetColumnDescription 
	'Listing.Vehicle._AuditID', 
	'Identity column uniquely identifying each row as staged from a Listing provider'
GO	

EXEC sp_SetColumnDescription 
	'Listing.Vehicle._ProviderID', 
	'Optional UID as received by the provider.  Currently, not used in the system'
GO	

EXEC sp_SetColumnDescription 
	'Listing.Vehicle._PartitionStatusCD', 
	'Partition status.  Currently, 0 = Archive, 1 = Active, 2 = Loading'
GO	

EXEC sp_SetColumnDescription 
	'Listing.Vehicle._ProcessStatusCD', 
	'Staging Process status.  Currently unused.'
GO	

CREATE TABLE Listing.Provider (
	ProviderID	TINYINT NOT NULL CONSTRAINT PK_ListingProvider PRIMARY KEY CLUSTERED,
	DatafeedCode	VARCHAR(20) NOT NULL,
	ActiveLoadID	INT NOT NULL
	)
GO

INSERT
INTO	Listing.Provider (ProviderID, DatafeedCode, ActiveLoadID)
SELECT	1, 'AutoTrader_Vehicles', 0
UNION
SELECT	2, 'GetAuto', 0
UNION
SELECT	3, 'n/a', 0
UNION
SELECT	4, 'CarsDotCom', 0
UNION
SELECT	5, 'eBizAutosListing', 0
UNION
SELECT	6, 'HomeNetListing', 0
UNION
SELECT	7, 'CDMDataListing', 0
UNION
SELECT	8, 'AULtecListing', 0
UNION
SELECT	9, 'DealerPeakListing', 0
UNION
SELECT	10, 'TCUV', 0
UNION 
SELECT	11, 'AIM-Listing', 0

GO
EXEC sp_SetTableDescription 
	'Listing.Provider',
	'Master list of all listing providers supported by the standard staging load process.'
GO

EXEC sp_SetColumnDescription 
	'Listing.Provider.ProviderID', 
	'Master ProviderID.  The respective Listing.Provider IDs in Lot and Market must be registered here first'
	
EXEC sp_SetColumnDescription 
	'Listing.Provider.DatafeedCode', 
	'DatafeedCode code FK to Datafeeds.dbo.Datafeed.DatafeedCode'
	
EXEC sp_SetColumnDescription 
	'Listing.Provider.ActiveLoadID', 
	'Currently active legacy dataload ID as registered in DBASTAT.dbo.Dataload_History.  If zero, nothing has been staged.'
GO	

/*



IF  EXISTS (SELECT * FROM sys.partition_schemes WHERE name = N'PS_ListingVehiclePartitionStatus')
	DROP PARTITION SCHEME [PS_ListingVehiclePartitionStatus]

IF  EXISTS (SELECT * FROM sys.partition_functions WHERE name = N'PF_PartitionStatus')
	DROP PARTITION FUNCTION [PF_PartitionStatus]

IF EXISTS(SELECT 1 FROM sys.schemas S WHERE name = 'Listing')
	DROP SCHEMA Listing

IF EXISTS(SELECT 1 FROM sys.database_files WHERE name = 'Archive')
	ALTER DATABASE [Staging]  REMOVE FILE [Archive]

IF EXISTS (SELECT 1 FROM sys.filegroups F WHERE name = 'Archive')
	ALTER DATABASE [Staging] REMOVE FILEGROUP [Archive]
*/	
	