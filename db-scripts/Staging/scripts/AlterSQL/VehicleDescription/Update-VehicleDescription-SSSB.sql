--------------------------------------------------------------------------------
--
--	Renove SSSB objects from Staging, as they have been superseded by a 
--	common sender service in VehicleCatalog
--
--------------------------------------------------------------------------------

IF OBJECT_ID('Autodata.VehicleDescriptionErrors') IS NOT NULL 
    DROP TABLE Autodata.VehicleDescriptionErrors

IF OBJECT_ID('Autodata.VehicleDescriptionDialogs') IS NOT NULL 
    DROP TABLE Autodata.VehicleDescriptionDialogs



	
IF EXISTS ( SELECT  *
            FROM    sys.services
            WHERE   name = N'//Staging/VehicleDescription/DataSender' ) 
    DROP SERVICE [//Staging/VehicleDescription/DataSender]

GO

IF EXISTS ( SELECT  *
            FROM    sys.service_contracts
            WHERE   NAME = N'//VehicleDescription/Contract' ) 
    DROP CONTRACT [//VehicleDescription/Contract]

IF EXISTS ( SELECT  *
            FROM    sys.service_message_types
            WHERE   NAME = N'//VehicleDescription/Message' ) 
    DROP MESSAGE TYPE [//VehicleDescription/Message]

GO

IF EXISTS ( SELECT  *
            FROM    sys.routes
            WHERE   name = N'RouteDataSender' ) 
    DROP ROUTE [RouteDataSender]
GO

IF EXISTS ( SELECT  *
            FROM    dbo.sysobjects
            WHERE   id = OBJECT_ID(N'Autodata.VehicleMaster#Send')
                    AND OBJECTPROPERTY(id, N'IsProcedure') = 1 ) 
    DROP PROCEDURE Autodata.VehicleDescription#Send
GO

IF EXISTS ( SELECT  *
            FROM    sys.service_queues
            WHERE   name = N'InitiatorVehicleDescriptionQueue' ) 
    DROP QUEUE [dbo].[InitiatorVehicleDescriptionQueue]
GO
	