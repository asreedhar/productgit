ALTER TABLE KBB.Vehicle ADD CPOAdjustment BIT NULL
ALTER TABLE KBB.Vehicle ADD MileageGroupId INT NULL
ALTER TABLE KBB.Vehicle ADD BlueBookName NVARCHAR(100) NULL
ALTER TABLE KBB.Vehicle ADD ShortName NVARCHAR(50) NULL
GO

CREATE TABLE KBB.MileageGroupAdjustment (
		 MileageRangeId INT NOT NULL,
		 MileageGroupId INT NOT NULL,
		 MileageGroupDisplayName NVARCHAR(255) NOT NULL,
		 Adjustment FLOAT NOT NULL,
		 AdjustmentTypeId INT NOT NULL,
		 AdjustmentTypeDisplayName NVARCHAR(25) NOT NULL,
		 CONSTRAINT PK_MileageGroupAdjustment PRIMARY KEY CLUSTERED (MileageRangeId, MileageGroupId)            
		)

GO

DROP TABLE KBB.MileageValueAdjustment
GO
			
ALTER TABLE KBB.Make ALTER COLUMN DisplayName NVARCHAR(30) NULL
GO

ALTER TABLE KBB.Model ALTER COLUMN DisplayName NVARCHAR(50) NULL
ALTER TABLE KBB.Model ALTER COLUMN Description NVARCHAR(50) NULL

ALTER TABLE KBB.Model ADD MarketName NVARCHAR(50) NULL
ALTER TABLE KBB.Model ADD ShortName NVARCHAR(50) NULL
GO


ALTER TABLE KBB.Trim ALTER COLUMN DisplayName NVARCHAR(255) NULL

GO

DROP TABLE KBB.ValueRange

GO

ALTER TABLE KBB.Vehicle ALTER COLUMN DisplayName NVARCHAR(255) NULL
--ALTER TABLE KBB.Vehicle ALTER COLUMN AvailabilityStatusStartDate SMALLDATETIME NULL
GO

DROP TABLE KBB.VehicleMapping 	-- LEGACY MAPPING
DROP TABLE KBB.VehicleOptionMapping
GO

ALTER TABLE KBB.VehicleOption ALTER COLUMN OptionAvailabilityDisplayName NVARCHAR(15) NULL
ALTER TABLE KBB.VehicleOption ALTER COLUMN OptionAvailabilityCode NCHAR(1) NULL
GO

CREATE TABLE KBB.ProgramContext (
		ContextId		INT NOT NULL,
		ContextValueId		INT NOT NULL,
		ContextDisplayName	NVARCHAR(25) NOT NULL,
		DisplayName		NVARCHAR(200) NOT NULL,
		Attribute		NVARCHAR(3000) NOT NULL,
		AttributeValue		NVARCHAR(3000) NOT NULL,
		CONSTRAINT PK_ProgramContext PRIMARY KEY CLUSTERED (ContextID, ContextValueId)
		)
GO		
if exists (select * from dbo.sysobjects where id = object_id(N'[KBB].[MileageValueAdjustment_Extract]') and OBJECTPROPERTY(id, N'IsView') = 1)
	DROP VIEW KBB.MileageValueAdjustment_Extract

if exists (select * from dbo.sysobjects where id = object_id(N'[KBB].[ValueRange_Extract]') and OBJECTPROPERTY(id, N'IsView') = 1)
	DROP VIEW KBB.ValueRange_Extract

GO