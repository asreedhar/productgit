
CREATE SCHEMA Postings
go
-- DROP TABLE [Postings].[PhoneLeads]
CREATE TABLE [Postings].[PhoneLeads](
                [leadId] [int] IDENTITY(1,1) NOT NULL,
                [businessUnitId] [int] NULL,
                [sourceId] [int] NULL,
                [LeadDate] [datetime] NULL,
                [CallerPhone] [varchar](20) NULL,
                [CallerZip] [int] NULL,
                [CallStatus] [varchar](50) NULL,
                [CallDurationMinutes] [int] NULL,
                [CallDurationSeconds] [int] NULL,
                [Load_ID] int NOT NULL, -- Server01.DBAStat.dbo.Dataload_History.Load_ID
                [ExtractRequestHandle] uniqueidentifier NOT NULL, -- Server03.Datafeeds.Extract.FetchDotCom#Request.Handle
                FetchExecutionID INT NOT NULL, -- Fetch.com's record identifier
                FetchDateInserted DATETIME NOT NULL,
                DateStaged DATETIME NOT NULL DEFAULT (GETDATE())
) ON [PRIMARY]
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.postings.PhoneLeads.Load_ID',
	@Description=N'Maps to Server01.DBAStat.dbo.Dataload_History.Load_ID'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.postings.PhoneLeads.ExtractRequestHandle',
	@Description=N'Maps to Server03.Datafeeds.Extract.FetchDotCom#Request.Handle'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.postings.PhoneLeads.FetchExecutionID',
	@Description=N'Fetch.com''s record identifier'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.postings.PhoneLeads.FetchDateInserted',
	@Description=N'The DateInserted value from the source files.'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.postings.PhoneLeads.DateStaged',
	@Description=N'The date this record was loaded into the staging database.'
GO

CREATE SCHEMA Merchandising
go
-- DROP TABLE [Merchandising].[AutoTraderExSummary]
CREATE TABLE [Merchandising].[AutoTraderExSummary](
                [businessUnitID] [int] NOT NULL,
                [month] [datetime] NOT NULL,
                [AvgDayNew] [int] NULL,
                [AvgDayUsed] [int] NULL,
                [AvgDayTotal] [int] NULL,
                [TimesSeenInSearch] [int] NULL,
                [BannerAdImpressions] [int] NULL,
                [FindDealerSearchImpressions] [int] NULL,
                [TotalExposure] [int] NULL,
                [DetailPagesViewed] [int] NULL,
                [PrintableAdsRequested] [int] NULL,
                [ViewedWebsiteOrInventory] [int] NULL,
                [FindDealerClickThrus] [int] NULL,
                [BannerAdClickThrus] [int] NULL,
                [TotalActivity] [int] NULL,
                [CallsToDealership] [int] NULL,
                [EmailsToDealership] [int] NULL,
                [SecureCreditApplications] [int] NULL,
                [TotalTrackableProspects] [int] NULL,
                [TotalShoppersInArea] [int] NULL,
                [ViewedMaps] [int] NULL,
                [Load_ID] int NOT NULL, -- Server01.DBAStat.dbo.Dataload_History.Load_ID
                [ExtractRequestHandle] uniqueidentifier NOT NULL, -- Server03.Datafeeds.Extract.FetchDotCom#Request.Handle
                FetchExecutionID INT NOT NULL, -- Fetch.com's record identifier
                FetchDateInserted DATETIME NOT NULL,
                DateStaged DATETIME NOT NULL DEFAULT (GETDATE())
) ON [PRIMARY]
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.AutoTraderExSummary.Load_ID',
	@Description=N'Maps to Server01.DBAStat.dbo.Dataload_History.Load_ID'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.AutoTraderExSummary.ExtractRequestHandle',
	@Description=N'Maps to Server03.Datafeeds.Extract.FetchDotCom#Request.Handle'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.AutoTraderExSummary.FetchExecutionID',
	@Description=N'Fetch.com''s record identifier'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.AutoTraderExSummary.FetchDateInserted',
	@Description=N'The DateInserted value from the source files.'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.AutoTraderExSummary.DateStaged',
	@Description=N'The date this record was loaded into the staging database.'
GO

-- DROP TABLE [Merchandising].[AutoTraderFetchError]
CREATE TABLE [Merchandising].[AutoTraderFetchError] (
	AutoTraderFetchErrorID INT IDENTITY(1,1) PRIMARY KEY, -- This table is just a log and does not have a natural key
    [ExtractRequestHandle] UNIQUEIDENTIFIER NOT NULL, -- Server03.Datafeeds.Extract.FetchDotCom#Request.Handle
    [BusinessUnitID] INT NOT NULL,
    [Month] DATETIME NOT NULL,
    [FetchExecutionID] INT NOT NULL, -- Fetch.com's record identifier
    [FeedLogin] VARCHAR(50),
    [ErrorDate] DATETIME,
    [ErrorType] VARCHAR(50),
    [Load_ID] INT, -- Server01.DBAStat.dbo.Dataload_History.Load_ID
    FetchDateInserted DATETIME NOT NULL,
    DateStaged DATETIME NOT NULL DEFAULT (GETDATE())
) ON [PRIMARY]
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.AutoTraderFetchError.Load_ID',
	@Description=N'Maps to Server01.DBAStat.dbo.Dataload_History.Load_ID'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.AutoTraderFetchError.ExtractRequestHandle',
	@Description=N'Maps to Server03.Datafeeds.Extract.FetchDotCom#Request.Handle'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.AutoTraderFetchError.FetchExecutionID',
	@Description=N'Fetch.com''s record identifier'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.AutoTraderFetchError.FetchDateInserted',
	@Description=N'The DateInserted value from the source files.'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.AutoTraderFetchError.DateStaged',
	@Description=N'The date this record was loaded into the staging database.'
GO



-- DROP TABLE [merchandising].[AutoTraderPerfSummary]
CREATE TABLE [merchandising].[AutoTraderPerfSummary](
                [businessUnitID] [int] NOT NULL,
                [month] [datetime] NOT NULL,
                [CallsToDealership] [int] NULL,
                [ViewedWebsiteOrInventory] [int] NULL,
                [SecureCreditApplications] [int] NULL,
                [UsedVehiclesSearched] [int] NULL,
                [UsedDetailPagesViewed] [int] NULL,
                [UsedViewedMaps] [int] NULL,
                [UsedEmailsToDealership] [int] NULL,
                [UsedPrintableAds] [int] NULL,
                [NewVehiclesSearched] [int] NULL,
                [NewDetailPagesViewed] [int] NULL,
                [NewViewedMaps] [int] NULL,
                [NewEmailsToDealership] [int] NULL,
                [NewPrintableAds] [int] NULL,
                [SearchImpressions] [int] NULL,
                [ClickThrus] [int] NULL,
                [ViewedMaps] [int] NULL,
                [EmailsToDealership] [int] NULL,
                [AskedForMoreDetails] [int] NULL,
                [UsedAvgDailyVehicles] [int] NULL,
                [UsedPctWithPrice] [decimal](3, 0) NULL,
                [UsedPctWithComments] [decimal](3, 0) NULL,
                [UsedPctWithPhotos] [decimal](3, 0) NULL,
                [UsedPctMultiplePhotos] [decimal](3, 0) NULL,
                [UsedPctSinglePhoto] [decimal](3, 0) NULL,
                [UsedPrimarySource] [varchar](50) NULL,
                [NewAvgDailyVehicles] [int] NULL,
                [NewPctWithPrice] [decimal](3, 0) NULL,
                [NewPctWithComments] [decimal](3, 0) NULL,
                [NewPctWithPhotos] [decimal](3, 0) NULL,
                [NewPctMultiplePhotos] [decimal](3, 0) NULL,
                [NewPctSinglePhoto] [decimal](3, 0) NULL,
                [NewPrimarySource] [varchar](50) NULL,
                [PartnerSelectedMarkets] [varchar](50) NULL,
                [PartnerBannerAdImpressions1] [int] NULL,
                [PartnerBannerAdImpressions2] [int] NULL,
                [PartnerBannerAdClickThrus1] [int] NULL,
                [PartnerBannerAdClickThrus2] [int] NULL,
                [Load_ID] int NOT NULL, -- Server01.DBAStat.dbo.Dataload_History.Load_ID
                [ExtractRequestHandle] uniqueidentifier NOT NULL, -- Server03.Datafeeds.Extract.FetchDotCom#Request.Handle
                FetchExecutionID INT NOT NULL, -- Fetch.com's record identifier
                FetchDateInserted DATETIME NOT NULL,
                DateStaged DATETIME NOT NULL DEFAULT (GETDATE())
) ON [PRIMARY]
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.AutoTraderPerfSummary.Load_ID',
	@Description=N'Maps to Server01.DBAStat.dbo.Dataload_History.Load_ID'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.AutoTraderPerfSummary.ExtractRequestHandle',
	@Description=N'Maps to Server03.Datafeeds.Extract.FetchDotCom#Request.Handle'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.AutoTraderPerfSummary.FetchExecutionID',
	@Description=N'Fetch.com''s record identifier'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.AutoTraderPerfSummary.FetchDateInserted',
	@Description=N'The DateInserted value from the source files.'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.AutoTraderPerfSummary.DateStaged',
	@Description=N'The date this record was loaded into the staging database.'
GO

-- DROP TABLE [merchandising].[AutoTraderVehSummary]
CREATE TABLE [merchandising].[AutoTraderVehSummary](
                [businessUnitID] [int] NOT NULL,
                [month] [datetime] NOT NULL,
                [VIN] [varchar](50) NOT NULL,
                [Type] [varchar](50) NULL,
                [YearMakeModel] [varchar](75) NULL,
                [StockNumber] [varchar](50) NOT NULL,
                [Price] [money] NULL,
                [AppearedInSearch] [int] NULL,
                [DetailsPageViews] [int] NULL,
                [MapViews] [int] NULL,
                [EmailsSent] [int] NULL,
                [DetailsPagePrinted] [int] NULL,
                [HasSpotLightAds] [bit] NULL,
                PhotoCount INT NULL,
                VehicleInfoAvailable BIT NULL,
                [Load_ID] int NOT NULL, -- Server01.DBAStat.dbo.Dataload_History.Load_ID
                [ExtractRequestHandle] uniqueidentifier NOT NULL, -- Server03.Datafeeds.Extract.FetchDotCom#Request.Handle
                FetchExecutionID INT NOT NULL, -- Fetch.com's record identifier
                FetchDateInserted DATETIME NOT NULL,
                DateStaged DATETIME NOT NULL DEFAULT (GETDATE())
) ON [PRIMARY]
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.AutoTraderVehSummary.Load_ID',
	@Description=N'Maps to Server01.DBAStat.dbo.Dataload_History.Load_ID'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.AutoTraderVehSummary.ExtractRequestHandle',
	@Description=N'Maps to Server03.Datafeeds.Extract.FetchDotCom#Request.Handle'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.AutoTraderVehSummary.FetchExecutionID',
	@Description=N'Fetch.com''s record identifier'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.AutoTraderVehSummary.FetchDateInserted',
	@Description=N'The DateInserted value from the source files.'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.AutoTraderVehSummary.DateStaged',
	@Description=N'The date this record was loaded into the staging database.'
GO


/*
ALTER TABLE Merchandising.AutoTraderExSummary ADD ErrorTypeID TINYINT 
ALTER TABLE Merchandising.AutoTraderFetchError ADD ErrorTypeID TINYINT 
ALTER TABLE Merchandising.AutoTraderPerfSummary ADD ErrorTypeID TINYINT 
ALTER TABLE Merchandising.AutoTraderVehSummary ADD ErrorTypeID TINYINT 
ALTER TABLE postings.PhoneLeads ADD ErrorTypeID TINYINT 
*/



GO

