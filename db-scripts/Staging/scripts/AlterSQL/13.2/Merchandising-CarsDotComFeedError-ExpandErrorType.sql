-- expand the column size to store the longer error messages we get back from connotate

alter table Staging.Merchandising.CarsDotComFeedError
alter column ErrorType varchar(200) null
