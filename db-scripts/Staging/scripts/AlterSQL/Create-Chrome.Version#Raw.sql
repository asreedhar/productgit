CREATE TABLE [Chrome].[Version#Raw](
	[VersionStgID] [int] IDENTITY(1,1) NOT NULL,
	[Product] [varchar](50) NULL,
	[DataVersion] [datetime] NULL,
	[DataReleaseID] [int] NULL,
	[SchemaName] [varchar](20) NULL,
	[SchemaVersion] [varchar](10) NULL,
	[Country] [varchar](2) NULL,
	[Language] [varchar](2) NULL,
	[SourcePath] [varchar](100) NULL,
	[DateCreated] [datetime] NOT NULL CONSTRAINT [DF_ChromeVersionraw__DateCreated]  DEFAULT (getdate()),
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]

GO
IF EXISTS (SELECT 1 FROM sys.database_principals WHERE name = 'StagingReader' AND type_desc = 'SQL_USER')
	GRANT SELECT ON Chrome.Version#Raw TO StagingReader