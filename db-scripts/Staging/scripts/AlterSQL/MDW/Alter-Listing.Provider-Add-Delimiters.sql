ALTER TABLE Listing.Provider ADD OptionDelimiter VARCHAR(2) NULL
ALTER TABLE Listing.Provider ADD ImageUrlDelimiter VARCHAR(2) NULL
GO

UPDATE	P
SET	OptionDelimiter	= MP.OptionDelimiter
FROM	Listing.Provider P
	INNER JOIN Market.Listing.Provider MP ON P.ProviderID = MP.ProviderID


UPDATE	P
SET	ImageUrlDelimiter = ID.ImageUrlDelimiter
FROM	Listing.Provider P
	INNER JOIN (	SELECT	ProviderID = 6, ImageUrlDelimiter = ','	
			UNION
			SELECT	10, ';'
			UNION
			SELECT	21, '|'
			UNION
			SELECT	23, ','
			UNION
			SELECT	24, ','
			UNION
			SELECT	19, '||'		-- Cardinals
			UNION
			SELECT	27, '|'
			) ID ON P.ProviderID = ID.ProviderID

			
UPDATE	P 
SET	OptionDelimiter = '||', ImageUrlDelimiter = '|'
FROM	Staging.Listing.Provider P
WHERE	ProviderID = 19


----------------------------------------------------------------
-- 	Update existing providers for MDW extraction
----------------------------------------------------------------

UPDATE	Listing.Provider
SET	DatafeedCode = 'GetAutoListing'
WHERE	DatafeedCode = 'GetAuto'


INSERT
INTO	Listing.Provider (ProviderID, DatafeedCode, ActiveLoadID, ProviderFeedTypeID, ResponseFilePending, IsVideoUrlProvider, OptionDelimiter, ImageUrlDelimiter)
SELECT	15, 'Firstlook', 0, 1, 0, 1, ', ', ','