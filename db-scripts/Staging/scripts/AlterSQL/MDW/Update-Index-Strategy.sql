DROP TABLE Listing.VehicleOld
GO
ALTER TABLE Listing.Vehicle DROP CONSTRAINT PK_ListingVehicle_1 
GO
DROP INDEX Listing.Vehicle.IX_Listing_Vehicle_ProviderId_ProviderRowID_SellerId
GO
DROP INDEX Listing.Vehicle.IX_Listing_Vehicle_ProviderId_VIN 
GO
DROP INDEX Listing.Vehicle.IX_Listing_Vehicle_ProviderId_Stock_Seller_Phone 
GO

ALTER TABLE Listing.Vehicle ADD CONSTRAINT PK_ListingVehicle PRIMARY KEY NONCLUSTERED  (_ProviderID, _LoadID, _AuditID) WITH (FILLFACTOR=90, DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderID)
GO
CREATE NONCLUSTERED INDEX IX_ListingVehicle__HerbieCardinal ON Listing.Vehicle (_ProviderRowID, SellerID) WHERE _ProviderID = 27 WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderID)
GO
CREATE NONCLUSTERED INDEX IX_ListingVehicle__SellerNameZipCodeVIN ON Listing.Vehicle (SellerName, SellerZipCode, VIN) WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderID)
GO
CREATE CLUSTERED INDEX IXC_ListingVehicle__VinAuditID ON Listing.Vehicle (_ProviderID, VIN, _AuditID) WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderID)
GO
CREATE NONCLUSTERED INDEX IX_ListingVehicle__Herbie ON Listing.Vehicle (StockNumber, SellerName, SellerPhoneNumber)  WHERE _ProviderID = 12 WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderID)
GO


ALTER TABLE Listing.Vehicle_Preload DROP CONSTRAINT PK_ListingVehiclePreload 
GO
DROP INDEX Listing.Vehicle_Preload.IX_Listing_VehiclePreload_ProviderId_ProviderRowID_SellerId
GO
DROP INDEX Listing.Vehicle_Preload.IX_Listing_VehiclePreload_ProviderId_VIN 
GO
DROP INDEX Listing.Vehicle_Preload.IX_Listing_VehiclePreload_ProviderId_Stock_Seller_Phone 
GO

ALTER TABLE Listing.Vehicle_Preload ADD CONSTRAINT PK_ListingVehiclePreload PRIMARY KEY NONCLUSTERED  (_ProviderID, _LoadID, _AuditID) WITH (FILLFACTOR=90, DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderID)
GO
CREATE NONCLUSTERED INDEX IX_ListingVehiclePreload__HerbieCardinal ON Listing.Vehicle_Preload (_ProviderRowID, SellerID) WHERE _ProviderID = 27 WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderID)
GO
CREATE NONCLUSTERED INDEX IX_ListingVehiclePreload__SellerNameZipCodeVIN ON Listing.Vehicle_Preload (SellerName, SellerZipCode, VIN) WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderID)
GO
CREATE CLUSTERED INDEX IXC_ListingVehiclePreload__VinAuditID ON Listing.Vehicle_Preload (_ProviderID, VIN, _AuditID) WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderID)
GO
CREATE NONCLUSTERED INDEX IX_ListingVehiclePreload__Herbie ON Listing.Vehicle_Preload (StockNumber, SellerName, SellerPhoneNumber)  WHERE _ProviderID = 12 WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderID)
GO



ALTER TABLE Listing.Vehicle_History DROP CONSTRAINT PK_ListingVehicleHistory 
GO
DROP INDEX Listing.Vehicle_History.IX_Listing_VehicleHistory_ProviderId_ProviderRowID_SellerId
GO
DROP INDEX Listing.Vehicle_History.IX_Listing_VehicleHistory_ProviderId_VIN 
GO
DROP INDEX Listing.Vehicle_History.IX_Listing_VehicleHistory_ProviderId_Stock_Seller_Phone 
GO


ALTER TABLE Listing.Vehicle_History ADD CONSTRAINT PK_ListingVehicleHistory PRIMARY KEY NONCLUSTERED  (_ProviderID, _LoadID, _AuditID) WITH (FILLFACTOR=90, DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderID)
GO
CREATE NONCLUSTERED INDEX IX_ListingVehicleHistory__HerbieCardinal ON Listing.Vehicle_History (_ProviderRowID, SellerID) WHERE _ProviderID = 27 WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderID)
GO
CREATE NONCLUSTERED INDEX IX_ListingVehicleHistory__SellerNameZipCodeVIN ON Listing.Vehicle_History (SellerName, SellerZipCode, VIN) WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderID)
GO
CREATE CLUSTERED INDEX IXC_ListingVehicleHistory__VinAuditID ON Listing.Vehicle_History (_ProviderID, VIN, _AuditID) WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderID)
GO
CREATE NONCLUSTERED INDEX IX_ListingVehicleHistory__Herbie ON Listing.Vehicle_History (StockNumber, SellerName, SellerPhoneNumber)  WHERE _ProviderID = 12 WITH (DATA_COMPRESSION = PAGE) ON PS_ListingProviderId (_ProviderID)
GO


