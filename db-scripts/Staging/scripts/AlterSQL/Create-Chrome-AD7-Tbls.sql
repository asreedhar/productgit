IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Chrome].[ADS7Models#Raw]') AND type in (N'U'))
DROP TABLE [Chrome].[ADS7Models#Raw]
GO

CREATE TABLE [Chrome].[ADS7Models#Raw](
	[VinPattern] [varchar](17) NULL,
	[ModelYear] [int] NULL,
	[Division] [varchar](100) NULL,
	[Model] [varchar](100) NULL,
	[SubdivisionName] [varchar](100) NULL,
	[DivisionID] [int] NULL,
	[SubdivisionID] [int] NULL,
	[ModelID] [int] NULL,
	[VinModelName] [varchar](100) NULL,
	[LoadDttm] [datetime] NOT NULL,
	[LoadStatus] [char](1) NULL
) ON [PRIMARY]

GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Chrome].[ADS7VinPattern#Raw]') AND type in (N'U'))
DROP TABLE [Chrome].[ADS7VinPattern#Raw]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[ADS7VinPattern#Raw](
	[VinPattern] [varchar](17) NULL,
	[StyleId] [int] NULL,
	[MFRModelCode] [varchar](100) NULL,
	[ModelYear] [int] NULL,
	[BestStyleName] [varchar](100) NULL,
	[Division] [varchar](100) NULL,
	[Model] [varchar](100) NULL,
	[PassDoors] [int] NULL,
	[ModelFleet] [varchar](10) NULL,
	[StyleName] [varchar](100) NULL,
	[AltBodyType] [varchar](100) NULL,
	[DriveTrain] [varchar](100) NULL,
	[BodyType] [varchar](100) NULL,
	[BodyTypePrimary] [varchar](10) NULL,
	[BodyTypeid] [int] NULL,
	[SubdivisionName] [varchar](100) NULL,
	[DivisionID] [int] NULL,
	[SubdivisionID] [int] NULL,
	[ModelID] [int] NULL,
	[VinModelName] [varchar](100) NULL,
	[StyleNameWOTrim] [varchar](100) NULL,
	[MFRStyleName] [varchar](100) NULL,
	[BasePrice] [varchar](20) NULL,
	[Invoice] [decimal](18, 2) NULL,
	[msrp] [decimal](18, 2) NULL,
	[Destination] [int] NULL,
	[MarketClassID] [int] NULL,
	[MarketClass] [varchar](100) NULL,
	[Trim] [varchar](50) NULL,
	[FleetOnly] [varchar](50) NULL,
	[Enginesize] [decimal](18, 1) NULL,
	[FuelType] [int] NULL,
	[EngineType] [int] NULL,
	[EngineCID] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Chrome].[VinMappingIdentity]') AND type in (N'U'))
DROP TABLE [Chrome].[VinMappingIdentity]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Chrome].[VinMappingIdentity](
	[VinMappingID] [int] IDENTITY(100000000,1) NOT NULL,
	[ChromeStyleID] [int] NOT NULL,
	[VinPatternID] [int] NOT NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[Chrome].[VinPatternIdentity]') AND type in (N'U'))
DROP TABLE [Chrome].[VinPatternIdentity]
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [Chrome].[VinPatternIdentity](
	[WebDataVinPatternID] [int] IDENTITY(900000000,1) NOT NULL,
	[VinPattern] [char](17) NOT NULL,
	[OrigVinPatternID] [int] NULL,
	[LoadDttm] [datetime] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
IF EXISTS (SELECT 1 FROM sys.database_principals WHERE name = 'StagingReader' AND type_desc = 'SQL_USER')
	GRANT SELECT ON Chrome.VinPatternIdentity TO StagingReader