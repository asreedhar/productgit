INSERT
INTO	Listing.Provider (ProviderID, DatafeedCode, ActiveLoadID, ProviderFeedTypeID, ProviderCode)
SELECT	19, 'Herbie', 0, 2, 'HerbieCardinal'

GO

ALTER TABLE Listing.Vehicle ALTER COLUMN SellerURL VARCHAR(1000) NULL

ALTER TABLE Listing.Vehicle_Rolloff ALTER COLUMN SellerURL VARCHAR(1000) NULL