DELETE
FROM	Listing.ProviderVehicleAssignmentExpression 
WHERE	ProviderID = 19
	AND AssignmentExpressionID IN (1,2)

DECLARE @ID INT

INSERT
INTO	Listing.AssignmentExpression (Description, ColumnName, Expression)
SELECT	'Parse _ProviderRowID from listingId in SellerURL', '_ProviderRowID', 'CASE WHEN CHARINDEX(''&listingId='', SellerURL) > 0  THEN  SUBSTRING(SellerURL,  CHARINDEX(''&listingId='', SellerURL) + 11,    CHARINDEX(''&'', SellerURL, CHARINDEX(''&listingId='', SellerURL)+1) - (CHARINDEX(''&listingId='', SellerURL) + 11)) ELSE ''0'' END'
SET @ID = SCOPE_IDENTITY()

INSERT
INTO	Listing.ProviderVehicleAssignmentExpression  (ProviderID, AssignmentExpressionID, ExecutionOrder)
SELECT	19, @ID, 1

INSERT
INTO	Listing.AssignmentExpression (Description, ColumnName, Expression)
SELECT	'Set SellerID to "0"', 'SellerID', '''0'''

SET @ID = SCOPE_IDENTITY()

INSERT
INTO	Listing.ProviderVehicleAssignmentExpression  (ProviderID, AssignmentExpressionID, ExecutionOrder)
SELECT	19, @ID, 1


UPDATE	Listing.Vehicle
SET	_ProviderRowID = CASE WHEN CHARINDEX('&listingId=', SellerURL) > 0  THEN  SUBSTRING(SellerURL,  CHARINDEX('&listingId=', SellerURL) + 11,    CHARINDEX('&', SellerURL, CHARINDEX('&listingId=', SellerURL)+1) - (CHARINDEX('&listingId=', SellerURL) + 11)) ELSE '0' END
WHERE	_PartitionStatusCD = 1
	AND _ProviderID = 19
	
	
UPDATE	Listing.Vehicle
SET	SellerID = '0'
WHERE	_PartitionStatusCD = 1
	AND _ProviderID = 19
	