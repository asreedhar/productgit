ALTER TABLE KBB.RegionZipCode ADD RegionTypeId INT NULL
ALTER TABLE KBB.RegionZipCode ADD RegionTypeDisplayName NVARCHAR(50) NULL

CREATE TABLE KBB.VehiclePriceRange (
	VehicleId	INT NOT NULL,
	PriceTypeId	INT NOT	NULL,
	RangeLow	DECIMAL(18,4) NOT NULL,
	RangeHigh	DECIMAL(18,4) NOT NULL,
	CONSTRAINT PK_KBBVehiclePriceRange PRIMARY KEY CLUSTERED (VehicleId, PriceTypeId),
	CONSTRAINT FK_KBBVehiclePriceRange__KBBVehicle FOREIGN KEY (VehicleID) REFERENCES KBB.Vehicle(VehicleId),
	CONSTRAINT FK_KBBVehiclePriceRange__KBBPriceType FOREIGN KEY (PriceTypeId) REFERENCES KBB.PriceType( PriceTypeId)
	)