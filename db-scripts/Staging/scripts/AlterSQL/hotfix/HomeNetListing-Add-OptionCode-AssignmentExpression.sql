INSERT
INTO	Listing.AssignmentExpression (Description, ColumnName, Expression)
SELECT	'Parse OptionCodes from PackageCodes', 'OptionCodes', 'NULLIF(REPLACE(PackageCodes, '' '' , '',''),'''')'

INSERT
INTO	Listing.ProviderVehicleAssignmentExpression (ProviderID, AssignmentExpressionID, ExecutionOrder)
SELECT	6, SCOPE_IDENTITY(), 1


UPDATE	Listing.Vehicle
SET	OptionCodes = NULLIF(REPLACE(PackageCodes, ' ' , ','),'')
WHERE	_PartitionStatusCD = 1
	AND _ProviderID = 6