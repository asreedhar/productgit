DROP INDEX Listing.Vehicle.IX_ListingVehicle__ProviderIDStockSeller 

CREATE NONCLUSTERED INDEX IX_ListingVehicle__ProviderIDStockSeller ON Listing.Vehicle (_PartitionStatusCD, _ProviderID, StockNumber, SellerName, SellerPhoneNumber) WITH (FILLFACTOR=95) ON PS_ListingVehiclePartitionStatus (_PartitionStatusCD)
GO