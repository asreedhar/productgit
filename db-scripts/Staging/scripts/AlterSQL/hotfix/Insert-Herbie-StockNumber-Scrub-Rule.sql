INSERT
INTO	Listing.AssignmentExpression (Description, ColumnName, Expression)
SELECT	'Scrub Stock Number', 'StockNumber', 'REPLACE(StockNumber, ''Stock# '', '''')'

INSERT
INTO	Listing.ProviderVehicleAssignmentExpression (ProviderID, AssignmentExpressionID, ExecutionOrder)
SELECT	SCOPE_IDENTITY(), 5, 1
