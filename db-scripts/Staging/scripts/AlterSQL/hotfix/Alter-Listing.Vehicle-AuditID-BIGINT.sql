ALTER TABLE Listing.Vehicle DROP CONSTRAINT PK_ListingVehicle
ALTER TABLE Listing.Vehicle ALTER COLUMN _AuditID BIGINT NOT NULL 
ALTER TABLE Listing.Vehicle ADD CONSTRAINT PK_ListingVehicle PRIMARY KEY CLUSTERED  (_PartitionStatusCD, _ProviderID, _LoadID, _AuditID) WITH (FILLFACTOR=90) ON PS_ListingVehiclePartitionStatus (_PartitionStatusCD)
GO

ALTER TABLE Listing.Vehicle_Rolloff DROP CONSTRAINT PK_Listing
ALTER TABLE Listing.Vehicle_Rolloff ALTER COLUMN _AuditID BIGINT NOT NULL 
ALTER TABLE Listing.Vehicle_Rolloff ADD CONSTRAINT PK_ListingVehicle_Rolloff PRIMARY KEY CLUSTERED  (_PartitionStatusCD, _ProviderID, _LoadID, _AuditID) ON Archive
GO