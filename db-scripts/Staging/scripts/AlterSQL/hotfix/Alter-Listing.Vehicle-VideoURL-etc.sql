ALTER TABLE Listing.Vehicle ADD VideoURL VARCHAR(250) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD VideoURL VARCHAR(250) NULL

ALTER TABLE Listing.Vehicle ADD BodyStyle VARCHAR(100) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD BodyStyle VARCHAR(100) NULL

ALTER TABLE Listing.Vehicle ADD Doors VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD Doors VARCHAR(10) NULL

ALTER TABLE Listing.Vehicle ADD EngineSize VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD EngineSize VARCHAR(10) NULL

ALTER TABLE Listing.Vehicle ADD WheelBase VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD WheelBase VARCHAR(10) NULL

ALTER TABLE Listing.Vehicle ADD TruckBed VARCHAR(20) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD TruckBed VARCHAR(20) NULL

ALTER TABLE Listing.Vehicle ADD ModelCode VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD ModelCode VARCHAR(10) NULL

ALTER TABLE Listing.Vehicle ADD PackageCodes VARCHAR(100) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD PackageCodes VARCHAR(100) NULL

INSERT
INTO	Listing.Provider (ProviderID, DatafeedCode, ProviderFeedTypeID, ActiveLoadID)
SELECT	24, 'DealerDotCom', 1, 0

ALTER TABLE Listing.Provider ADD IsVideoUrlProvider BIT NOT NULL CONSTRAINT DF_ListingProvider__IsVideoUrlProvider DEFAULT (0)
GO
UPDATE	Listing.Provider
SET	IsVideoUrlProvider = 1
WHERE	ProviderID IN (22,24)	