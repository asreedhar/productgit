USE [Market]
GO
IF OBJECT_ID('[Listing].[DriveTrainBackup]', 'U') IS NOT NULL 
    DROP TABLE [Listing].[DriveTrainBackup]
GO
CREATE TABLE [Listing].[DriveTrainBackup]
    (
      [NewDriveTrainID] TINYINT NOT NULL IDENTITY(1, 1)
    , [DriveTrainID] TINYINT NOT NULL PRIMARY KEY
    , [DriveTrain] VARCHAR(50) NOT NULL
    )
INSERT  INTO [Listing].[DriveTrainBackup]( [DriveTrainID] , [DriveTrain] )
        SELECT  [DriveTrainID], [DriveTrain]
        FROM    [Listing].[DriveTrain]

USE [Market]
GO
ALTER TABLE [Listing].[Vehicle] DROP CONSTRAINT [FK_Listing_Vehicle__ListingDriveTrain]

TRUNCATE TABLE [Listing].[DriveTrain]

DBCC CHECKIDENT ('[Listing].[DriveTrain]', RESEED, 1);

SET IDENTITY_INSERT [Listing].[DriveTrain] ON
INSERT  INTO Listing.DriveTrain ( DriveTrainId, DriveTrain )
	SELECT [NewDriveTrainID], [DriveTrain] FROM [Listing].[DriveTrainBackup]
SET IDENTITY_INSERT [Listing].[DriveTrain] OFF

UPDATE  v
SET     v.DriveTrainId = b.[NewDriveTrainID]
FROM    [Listing].[Vehicle] v
        INNER JOIN [Listing].[DriveTrainBackup] b ON v.[DriveTrainID] = b.[DriveTrainID]

ALTER TABLE [Listing].[Vehicle]
	ADD CONSTRAINT [FK_Listing_Vehicle__ListingDriveTrain] FOREIGN KEY([DriveTrainID]) 
			REFERENCES [Listing].[DriveTrain] ([DriveTrainID])

UPDATE  v
SET     v.DriveTrainId = b.[NewDriveTrainID]
FROM    [Listing].[Vehicle_History] v
        INNER JOIN [Listing].[DriveTrainBackup] b ON v.[DriveTrainID] = b.[DriveTrainID]

