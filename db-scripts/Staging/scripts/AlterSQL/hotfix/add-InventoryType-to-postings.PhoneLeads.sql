if object_id('postings.PhoneLeads') is not null
	drop table postings.PhoneLeads
	
	
CREATE TABLE [postings].[PhoneLeads]
(
[leadId] [int] NOT NULL IDENTITY(1, 1),
[businessUnitId] [int] NULL,
[destinationId] [int] NULL,
InventoryType tinyint not null,
[LeadDate] [datetime] NULL,
[CallerPhone] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CallerZip] [int] NULL,
[CallStatus] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CallDurationMinutes] [int] NULL,
[CallDurationSeconds] [int] NULL,
[Load_ID] [int] NOT NULL,
[ExtractRequestHandle] [uniqueidentifier] NOT NULL,
[FetchExecutionID] [int] NOT NULL,
[FetchDateInserted] [datetime] NOT NULL,
[DateStaged] [datetime] NOT NULL CONSTRAINT [DF_PhoneLeads_DateStaged] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [postings].[PhoneLeads] ADD CONSTRAINT [PK_PhoneLeads] PRIMARY KEY CLUSTERED  ([Load_ID], [ExtractRequestHandle], [leadId]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Maps to Server01.DBAStat.dbo.Dataload_History.Load_ID', 'SCHEMA', N'postings', 'TABLE', N'PhoneLeads', 'COLUMN', N'Load_ID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Maps to Server03.Datafeeds.Extract.FetchDotCom#Request.Handle', 'SCHEMA', N'postings', 'TABLE', N'PhoneLeads', 'COLUMN', N'ExtractRequestHandle'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Fetch.com''s record identifier', 'SCHEMA', N'postings', 'TABLE', N'PhoneLeads', 'COLUMN', N'FetchExecutionID'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The DateInserted value from the source files.', 'SCHEMA', N'postings', 'TABLE', N'PhoneLeads', 'COLUMN', N'FetchDateInserted'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The date this record was loaded into the staging database.', 'SCHEMA', N'postings', 'TABLE', N'PhoneLeads', 'COLUMN', N'DateStaged'
GO
