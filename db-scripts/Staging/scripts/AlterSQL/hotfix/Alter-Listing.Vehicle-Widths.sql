ALTER TABLE Listing.Vehicle ALTER COLUMN ImageModifiedDate VARCHAR(30) NULL

ALTER TABLE Listing.Vehicle ALTER COLUMN OptionCodes VARCHAR(1000) NULL

ALTER TABLE Listing.Vehicle_Rolloff ALTER COLUMN ImageModifiedDate VARCHAR(30) NULL

ALTER TABLE Listing.Vehicle_Rolloff ALTER COLUMN OptionCodes VARCHAR(1000) NULL
