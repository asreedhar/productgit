/*

drop table KBB.ZipCode
drop table KBB.Region
drop table KBB.RegionAdjustmentTypePriceType
drop table KBB.RegionGroupAdjustment
drop table KBB.RegionZipCode
drop table KBB.VehicleGroup

*/

-----------------------------------------------------------------------------------------------------------------------
--  KBB.Region
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE KBB.Region
(
    RegionID		int NOT NULL,
	RegionTypeID	int NOT NULL,
    DisplayName  	varchar(50) NULL,
	RegionTypeDisplayName 	varchar(25) NULL,

    CONSTRAINT PK_Kbb_Region 
    PRIMARY KEY CLUSTERED (RegionID)    
)
ON [PRIMARY]
GO

-----------------------------------------------------------------------------------------------------------------------
--  KBB.RegionAdjustmentTypePriceType
--    Foreign Keys: 
--		- KBB.PriceType
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE KBB.RegionAdjustmentTypePriceType
(
    RegionAdjustmentTypeID      int NOT NULL,
	PriceTypeID 				int NOT NULL,
    DisplayName             	varchar(50) NOT NULL,
 
    CONSTRAINT PK_Kbb_RegionAdjustmentTypePriceType 
    PRIMARY KEY CLUSTERED (RegionAdjustmentTypeID, PriceTypeID),
	
	CONSTRAINT FK_Kbb_RegionAdjustmentTypePriceType__PriceType
    FOREIGN KEY (PriceTypeID) REFERENCES KBB.PriceType (PriceTypeID)
) 
ON [PRIMARY]
GO

-----------------------------------------------------------------------------------------------------------------------
--  KBB.RegionGroupAdjustment
--    Foreign Keys: 
--		- KBB.Region
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE KBB.RegionGroupAdjustment
(
    RegionID                    	int NOT NULL,
	GroupID							int NOT NULL,
	RegionAdjustmentTypeID			int NOT NULL,
	AdjustmentTypeID				int NOT NULL,
	AdjustmentTypeRoundingTypeID	int NOT NULL,
	Adjustment						float NOT NULL,		-- decimal(5, 4) NOT NULL,	
	AdjustmentTypeDisplayName		varchar(25) NOT NULL,
	
    CONSTRAINT PK_Kbb_RegionGroupAdjustment 
    PRIMARY KEY CLUSTERED (RegionID, GroupID, RegionAdjustmentTypeID),
	
	CONSTRAINT FK_Kbb_RegionGroupAdjustment__Region
    FOREIGN KEY (RegionID) REFERENCES KBB.Region (RegionID)
)
ON [PRIMARY]
GO

-----------------------------------------------------------------------------------------------------------------------
--  KBB.ZipCode
--    Foreign Keys: None
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE KBB.ZipCode
(
    ZipCodeID  		int NOT NULL,
	ZipCode			char(5) NOT NULL,
 
    CONSTRAINT PK_Kbb_ZipCode 
    PRIMARY KEY CLUSTERED (ZipCodeID) 
)
ON [PRIMARY]
GO

-----------------------------------------------------------------------------------------------------------------------
--  KBB.RegionZipCode
--    Foreign Keys: 
--		- KBB.Region
--		- KBB.ZipCode
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE KBB.RegionZipCode
(
    RegionID      	int NOT NULL,
	ZipCodeID      	int NOT NULL,

    CONSTRAINT PK_Kbb_RegionZipCode
    PRIMARY KEY CLUSTERED (RegionID, ZipCodeID),
	
	CONSTRAINT FK_Kbb_RegionZipCode__Region
    FOREIGN KEY (RegionID) REFERENCES KBB.Region (RegionID),
	
	CONSTRAINT FK_Kbb_RegionZipCode__ZipCode
    FOREIGN KEY (ZipCodeID) REFERENCES KBB.ZipCode (ZipCodeID)
)
ON [PRIMARY]
GO

-----------------------------------------------------------------------------------------------------------------------
--  KBB.VehicleGroup
--    Foreign Keys: 
--		- KBB.Vehicle
-----------------------------------------------------------------------------------------------------------------------
CREATE TABLE KBB.VehicleGroup
(
    VehicleID              	int NOT NULL,
    GroupID              	int NOT NULL,
	GroupTypeID             int NOT NULL,
    DisplayName     		varchar(50) NOT NULL,
    GroupTypeDisplayName   	varchar(50) NOT NULL,
 
    CONSTRAINT PK_Kbb_VehicleGroup
    PRIMARY KEY CLUSTERED (VehicleID, GroupID),   
	
	/*
	CONSTRAINT FK_Kbb_VehicleGroup__Vehicle
    FOREIGN KEY (VehicleID) REFERENCES KBB.Vehicle (VehicleID)
	*/
) 
ON [PRIMARY]
GO

-- drops ----------------------------------------------------------------------------------------------------------------
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[KBB].[RegionState]') AND type in (N'U'))
	DROP TABLE [KBB].[RegionState]
GO



