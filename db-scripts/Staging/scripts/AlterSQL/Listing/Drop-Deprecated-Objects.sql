ALTER TABLE Listing.Vehicle DROP COLUMN _PartitionStatusCD
ALTER TABLE Listing.Vehicle_History DROP COLUMN _PartitionStatusCD
ALTER TABLE Listing.Vehicle_Preload DROP COLUMN _PartitionStatusCD
ALTER TABLE Listing.Vehicle_Rolloff DROP COLUMN _PartitionStatusCD
GO


IF EXISTS (SELECT 1 FROM sys.procedures where object_id=OBJECT_ID('Listing.Vehicle#SetPartitionStatus'))
	DROP PROCEDURE Listing.Vehicle#SetPartitionStatus 
