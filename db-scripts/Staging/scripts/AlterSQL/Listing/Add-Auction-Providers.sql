------------------------------------------------------------------------------------------------
--	Add new provider feed type.  May be able to merge with #3 at some point
------------------------------------------------------------------------------------------------

INSERT
INTO	Listing.ProviderFeedType (ProviderFeedTypeID, Description)
SELECT	4, 'Auction Baseline/Incremental'
GO
        
------------------------------------------------------------------------------------------------
--	Add new provider
------------------------------------------------------------------------------------------------

INSERT
INTO	Listing.Provider(ProviderID, DatafeedCode, ActiveLoadID, ProviderFeedTypeID, ProviderCode, ResponseFilePending, DeleteThresholdPct, IsVideoUrlProvider, OptionDelimiter, ImageUrlDelimiter)
SELECT	28, 'ADESA_AIF', 0, 4, NULL, 0, NULL, 0, NULL, NULL


INSERT 
INTO	Listing.AssignmentExpression (Description, ColumnName, Expression)
SELECT	'Convert ADESA Status to standard SourceRecordType', 'SourceRecordType', 'CASE SourceRecordType WHEN ''Removed'' THEN ''0'' ELSE ''1'' END'

INSERT
INTO	Listing.ProviderVehicleAssignmentExpression (ProviderID, AssignmentExpressionID, ExecutionOrder)
SELECT	28, SCOPE_IDENTITY(), 1
GO


ALTER TABLE Listing.ProviderFeedType ADD ParseProviderCode BIT NOT NULL CONSTRAINT DF_ListingProviderFeedType__ParseProviderCode DEFAULT 0

GO
UPDATE Listing.ProviderFeedType
SET	ParseProviderCode = 1
WHERE	ProviderFeedTypeID IN (2,3)


------------------------------------------------------------------------------------------------
--	Add the new target columns
------------------------------------------------------------------------------------------------

--WITH TableNames AS
--(
--	SELECT	'Listing.Vehicle' AS TableName
--	UNION 
--	SELECT	'Listing.Vehicle_History'
--	UNION 
--	SELECT	'Listing.Vehicle_Preload'
--	UNION
--	SELECT	'Listing.Vehicle_Rolloff'
--)
--SELECT	'ALTER TABLE ' + TableName + ' ADD ' + Name + ' VARCHAR(' + CAST(MaxStringWidth AS VARCHAR) + ') NULL'
----SELECT	'ALTER TABLE ' + TableName + ' DROP COLUMN ' + Name 
--FROM	TableNames T
--	INNER JOIN DBASTAT.dbo.DatasourceInterfaceColumn DIC ON DIC.DatasourceInterfaceID = 5 AND ColumnID BETWEEN 60 AND 60
	
--ORDER
--BY	TableName, ColumnID

ALTER TABLE Listing.Vehicle ADD Cylinders VARCHAR(5) NULL
ALTER TABLE Listing.Vehicle_History ADD Cylinders VARCHAR(5) NULL
ALTER TABLE Listing.Vehicle_Preload ADD Cylinders VARCHAR(5) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD Cylinders VARCHAR(5) NULL

ALTER TABLE Listing.Vehicle ADD OdometerUofM VARCHAR(64) NULL
ALTER TABLE Listing.Vehicle ADD AuctionStartDate VARCHAR(30) NULL
ALTER TABLE Listing.Vehicle ADD AuctionEndDate VARCHAR(30) NULL
ALTER TABLE Listing.Vehicle ADD AuctionVendor VARCHAR(60) NULL
ALTER TABLE Listing.Vehicle ADD AuctionLotNumber VARCHAR(255) NULL
ALTER TABLE Listing.Vehicle ADD AuctionRunNumber VARCHAR(255) NULL
ALTER TABLE Listing.Vehicle ADD AuctionLane VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle ADD AuctionSaleType VARCHAR(64) NULL
ALTER TABLE Listing.Vehicle ADD AuctionOpenClosedFlag VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle ADD AuctionDealerCodes VARCHAR(40) NULL
ALTER TABLE Listing.Vehicle ADD AuctionCurrentBid VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle ADD AuctionBuyPrice VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle ADD ImageViewerURL VARCHAR(1024) NULL
ALTER TABLE Listing.Vehicle ADD AuctionVehicleType VARCHAR(64) NULL
ALTER TABLE Listing.Vehicle ADD AuctionListingCategory VARCHAR(80) NULL
ALTER TABLE Listing.Vehicle ADD GenericFlag01 VARCHAR(1) NULL
ALTER TABLE Listing.Vehicle ADD GenericFlag02 VARCHAR(1) NULL
ALTER TABLE Listing.Vehicle ADD GenericFlag03 VARCHAR(1) NULL
ALTER TABLE Listing.Vehicle ADD GenericFlag04 VARCHAR(1) NULL
ALTER TABLE Listing.Vehicle ADD GenericFlag05 VARCHAR(1) NULL
ALTER TABLE Listing.Vehicle ADD GenericInt01 VARCHAR(9) NULL
ALTER TABLE Listing.Vehicle ADD GenericInt02 VARCHAR(9) NULL
ALTER TABLE Listing.Vehicle_History ADD OdometerUofM VARCHAR(64) NULL
ALTER TABLE Listing.Vehicle_History ADD AuctionStartDate VARCHAR(30) NULL
ALTER TABLE Listing.Vehicle_History ADD AuctionEndDate VARCHAR(30) NULL
ALTER TABLE Listing.Vehicle_History ADD AuctionVendor VARCHAR(60) NULL
ALTER TABLE Listing.Vehicle_History ADD AuctionLotNumber VARCHAR(255) NULL
ALTER TABLE Listing.Vehicle_History ADD AuctionRunNumber VARCHAR(255) NULL
ALTER TABLE Listing.Vehicle_History ADD AuctionLane VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle_History ADD AuctionSaleType VARCHAR(64) NULL
ALTER TABLE Listing.Vehicle_History ADD AuctionOpenClosedFlag VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle_History ADD AuctionDealerCodes VARCHAR(40) NULL
ALTER TABLE Listing.Vehicle_History ADD AuctionCurrentBid VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle_History ADD AuctionBuyPrice VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle_History ADD ImageViewerURL VARCHAR(1024) NULL
ALTER TABLE Listing.Vehicle_History ADD AuctionVehicleType VARCHAR(64) NULL
ALTER TABLE Listing.Vehicle_History ADD AuctionListingCategory VARCHAR(80) NULL
ALTER TABLE Listing.Vehicle_History ADD GenericFlag01 VARCHAR(1) NULL
ALTER TABLE Listing.Vehicle_History ADD GenericFlag02 VARCHAR(1) NULL
ALTER TABLE Listing.Vehicle_History ADD GenericFlag03 VARCHAR(1) NULL
ALTER TABLE Listing.Vehicle_History ADD GenericFlag04 VARCHAR(1) NULL
ALTER TABLE Listing.Vehicle_History ADD GenericFlag05 VARCHAR(1) NULL
ALTER TABLE Listing.Vehicle_History ADD GenericInt01 VARCHAR(9) NULL
ALTER TABLE Listing.Vehicle_History ADD GenericInt02 VARCHAR(9) NULL
ALTER TABLE Listing.Vehicle_Preload ADD OdometerUofM VARCHAR(64) NULL
ALTER TABLE Listing.Vehicle_Preload ADD AuctionStartDate VARCHAR(30) NULL
ALTER TABLE Listing.Vehicle_Preload ADD AuctionEndDate VARCHAR(30) NULL
ALTER TABLE Listing.Vehicle_Preload ADD AuctionVendor VARCHAR(60) NULL
ALTER TABLE Listing.Vehicle_Preload ADD AuctionLotNumber VARCHAR(255) NULL
ALTER TABLE Listing.Vehicle_Preload ADD AuctionRunNumber VARCHAR(255) NULL
ALTER TABLE Listing.Vehicle_Preload ADD AuctionLane VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle_Preload ADD AuctionSaleType VARCHAR(64) NULL
ALTER TABLE Listing.Vehicle_Preload ADD AuctionOpenClosedFlag VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle_Preload ADD AuctionDealerCodes VARCHAR(40) NULL
ALTER TABLE Listing.Vehicle_Preload ADD AuctionCurrentBid VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle_Preload ADD AuctionBuyPrice VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle_Preload ADD ImageViewerURL VARCHAR(1024) NULL
ALTER TABLE Listing.Vehicle_Preload ADD AuctionVehicleType VARCHAR(64) NULL
ALTER TABLE Listing.Vehicle_Preload ADD AuctionListingCategory VARCHAR(80) NULL
ALTER TABLE Listing.Vehicle_Preload ADD GenericFlag01 VARCHAR(1) NULL
ALTER TABLE Listing.Vehicle_Preload ADD GenericFlag02 VARCHAR(1) NULL
ALTER TABLE Listing.Vehicle_Preload ADD GenericFlag03 VARCHAR(1) NULL
ALTER TABLE Listing.Vehicle_Preload ADD GenericFlag04 VARCHAR(1) NULL
ALTER TABLE Listing.Vehicle_Preload ADD GenericFlag05 VARCHAR(1) NULL
ALTER TABLE Listing.Vehicle_Preload ADD GenericInt01 VARCHAR(9) NULL
ALTER TABLE Listing.Vehicle_Preload ADD GenericInt02 VARCHAR(9) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD OdometerUofM VARCHAR(64) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD AuctionStartDate VARCHAR(30) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD AuctionEndDate VARCHAR(30) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD AuctionVendor VARCHAR(60) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD AuctionLotNumber VARCHAR(255) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD AuctionRunNumber VARCHAR(255) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD AuctionLane VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD AuctionSaleType VARCHAR(64) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD AuctionOpenClosedFlag VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD AuctionDealerCodes VARCHAR(40) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD AuctionCurrentBid VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD AuctionBuyPrice VARCHAR(10) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD ImageViewerURL VARCHAR(1024) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD AuctionVehicleType VARCHAR(64) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD AuctionListingCategory VARCHAR(80) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD GenericFlag01 VARCHAR(1) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD GenericFlag02 VARCHAR(1) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD GenericFlag03 VARCHAR(1) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD GenericFlag04 VARCHAR(1) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD GenericFlag05 VARCHAR(1) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD GenericInt01 VARCHAR(9) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD GenericInt02 VARCHAR(9) NULL

ALTER TABLE Listing.Vehicle ADD UniqueKeyHash BINARY(16) NULL 
ALTER TABLE Listing.Vehicle_History ADD UniqueKeyHash BINARY(16) NULL
ALTER TABLE Listing.Vehicle_Preload ADD UniqueKeyHash BINARY(16) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD UniqueKeyHash BINARY(16) NULL

ALTER TABLE Listing.Vehicle ADD OptionsHash BINARY(16) NULL 
ALTER TABLE Listing.Vehicle_History ADD OptionsHash BINARY(16) NULL
ALTER TABLE Listing.Vehicle_Preload ADD OptionsHash BINARY(16) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD OptionsHash BINARY(16) NULL




CREATE NONCLUSTERED INDEX [IX_ListingVehicle__UniqueKeyHash] ON [Listing].[Vehicle] (UniqueKeyHash) WHERE (_ProviderID IN (28,12)) WITH (DATA_COMPRESSION = PAGE) ON [PS_ListingProviderId] (_ProviderID)
GO
CREATE NONCLUSTERED INDEX [IX_ListingVehicle_Preload__UniqueKeyHash] ON Listing.Vehicle_Preload (UniqueKeyHash) WHERE (_ProviderID IN (28,12)) WITH (DATA_COMPRESSION = PAGE) ON [PS_ListingProviderId] (_ProviderID)
GO
CREATE NONCLUSTERED INDEX [IX_ListingVehicle_History__UniqueKeyHash] ON Listing.Vehicle_History (UniqueKeyHash) WHERE (_ProviderID IN (28,12)) WITH (DATA_COMPRESSION = PAGE) ON [PS_ListingProviderId] (_ProviderID)
GO

------------------------------------------------------------------------------------------------
--	Unique Key Expressions for Legacy Providers
------------------------------------------------------------------------------------------------

--INSERT 
--INTO	Listing.AssignmentExpression (Description, ColumnName, Expression)
--SELECT	'Herbie Cardinal Unique Key Hash', 'UniqueKeyHash', 'HASHBYTES(''MD5'', CAST(_ProviderRowID AS VARCHAR) + ''~'' + CAST(SellerID AS VARCHAR))'

--INSERT
--INTO	Listing.ProviderVehicleAssignmentExpression (ProviderID, AssignmentExpressionID, ExecutionOrder)
--SELECT	27, SCOPE_IDENTITY(), 3
--GO


INSERT 
INTO	Listing.AssignmentExpression (Description, ColumnName, Expression)
SELECT	'Herbie Legacy Unique Key Hash', 'UniqueKeyHash', 'HASHBYTES(''MD5'', StockNumber + ''~'' + SellerName + ''~'' + SellerPhoneNumber)'

INSERT
INTO	Listing.ProviderVehicleAssignmentExpression (ProviderID, AssignmentExpressionID, ExecutionOrder)
SELECT	12, SCOPE_IDENTITY(), 2
GO
/*	This should be in DBASTAT
------------------------------------------------------------------------------------------------
--	Herbie Legacy UniqueKey Mapping
------------------------------------------------------------------------------------------------
INSERT
INTO	DBASTAT.dbo.FeedInterfaceMappingColumns (FeedInterfaceMappingID, InterfaceColumnID, DatafeedColumnID, StatusCode)
	
SELECT	FeedInterfaceMappingID, 83, DFTC.ColumnID, 1
FROM	DBASTAT.dbo.FeedInterfaceMapping FIM
	INNER JOIN  Datafeeds.dbo.DatafeedFileTypeColumn DFTC ON DFTC.DatafeedCode = 'Herbie'
								AND DFTC.FileType = 'Listing'
								AND ColumnName IN ('SellerName','StockNumber', 'PhoneNumber')
WHERE	FIM.DatafeedCode = 'Herbie'
*/
------------------------------------------------------------------------------------------------
--	ADESA Export Updates
------------------------------------------------------------------------------------------------

EXEC sp_rename @objname = 'Listing.Vehicle.GenericFlag01', @newname = 'GenericString01', @objtype = 'COLUMN'
EXEC sp_rename @objname = 'Listing.Vehicle_Preload.GenericFlag01', @newname = 'GenericString01', @objtype = 'COLUMN'
EXEC sp_rename @objname = 'Listing.Vehicle_History.GenericFlag01', @newname = 'GenericString01', @objtype = 'COLUMN'
EXEC sp_rename @objname = 'Listing.Vehicle_Rolloff.GenericFlag01', @newname = 'GenericString01', @objtype = 'COLUMN'
EXEC sp_rename @objname = 'Listing.Vehicle.GenericFlag02', @newname = 'GenericString02', @objtype = 'COLUMN'
EXEC sp_rename @objname = 'Listing.Vehicle_Preload.GenericFlag02', @newname = 'GenericString02', @objtype = 'COLUMN'
EXEC sp_rename @objname = 'Listing.Vehicle_History.GenericFlag02', @newname = 'GenericString02', @objtype = 'COLUMN'
EXEC sp_rename @objname = 'Listing.Vehicle_Rolloff.GenericFlag02', @newname = 'GenericString02', @objtype = 'COLUMN'
EXEC sp_rename @objname = 'Listing.Vehicle.GenericFlag03', @newname = 'GenericString03', @objtype = 'COLUMN'
EXEC sp_rename @objname = 'Listing.Vehicle_Preload.GenericFlag03', @newname = 'GenericString03', @objtype = 'COLUMN'
EXEC sp_rename @objname = 'Listing.Vehicle_History.GenericFlag03', @newname = 'GenericString03', @objtype = 'COLUMN'
EXEC sp_rename @objname = 'Listing.Vehicle_Rolloff.GenericFlag03', @newname = 'GenericString03', @objtype = 'COLUMN'
EXEC sp_rename @objname = 'Listing.Vehicle.GenericFlag04', @newname = 'GenericString04', @objtype = 'COLUMN'
EXEC sp_rename @objname = 'Listing.Vehicle_Preload.GenericFlag04', @newname = 'GenericString04', @objtype = 'COLUMN'
EXEC sp_rename @objname = 'Listing.Vehicle_History.GenericFlag04', @newname = 'GenericString04', @objtype = 'COLUMN'
EXEC sp_rename @objname = 'Listing.Vehicle_Rolloff.GenericFlag04', @newname = 'GenericString04', @objtype = 'COLUMN'
EXEC sp_rename @objname = 'Listing.Vehicle.GenericFlag05', @newname = 'GenericString05', @objtype = 'COLUMN'
EXEC sp_rename @objname = 'Listing.Vehicle_Preload.GenericFlag05', @newname = 'GenericString05', @objtype = 'COLUMN'
EXEC sp_rename @objname = 'Listing.Vehicle_History.GenericFlag05', @newname = 'GenericString05', @objtype = 'COLUMN'
EXEC sp_rename @objname = 'Listing.Vehicle_Rolloff.GenericFlag05', @newname = 'GenericString05', @objtype = 'COLUMN'



ALTER TABLE Listing.Vehicle ALTER COLUMN GenericString01 VARCHAR(50) NULL
ALTER TABLE Listing.Vehicle ALTER COLUMN GenericString02 VARCHAR(50) NULL
ALTER TABLE Listing.Vehicle ALTER COLUMN GenericString03 VARCHAR(50) NULL
ALTER TABLE Listing.Vehicle ALTER COLUMN GenericString04 VARCHAR(50) NULL
ALTER TABLE Listing.Vehicle ALTER COLUMN GenericString05 VARCHAR(50) NULL
ALTER TABLE Listing.Vehicle_History ALTER COLUMN GenericString01 VARCHAR(50) NULL
ALTER TABLE Listing.Vehicle_History ALTER COLUMN GenericString02 VARCHAR(50) NULL
ALTER TABLE Listing.Vehicle_History ALTER COLUMN GenericString03 VARCHAR(50) NULL
ALTER TABLE Listing.Vehicle_History ALTER COLUMN GenericString04 VARCHAR(50) NULL
ALTER TABLE Listing.Vehicle_History ALTER COLUMN GenericString05 VARCHAR(50) NULL
ALTER TABLE Listing.Vehicle_Preload ALTER COLUMN GenericString01 VARCHAR(50) NULL
ALTER TABLE Listing.Vehicle_Preload ALTER COLUMN GenericString02 VARCHAR(50) NULL
ALTER TABLE Listing.Vehicle_Preload ALTER COLUMN GenericString03 VARCHAR(50) NULL
ALTER TABLE Listing.Vehicle_Preload ALTER COLUMN GenericString04 VARCHAR(50) NULL
ALTER TABLE Listing.Vehicle_Preload ALTER COLUMN GenericString05 VARCHAR(50) NULL
ALTER TABLE Listing.Vehicle_Rolloff ALTER COLUMN GenericString01 VARCHAR(50) NULL
ALTER TABLE Listing.Vehicle_Rolloff ALTER COLUMN GenericString02 VARCHAR(50) NULL
ALTER TABLE Listing.Vehicle_Rolloff ALTER COLUMN GenericString03 VARCHAR(50) NULL
ALTER TABLE Listing.Vehicle_Rolloff ALTER COLUMN GenericString04 VARCHAR(50) NULL
ALTER TABLE Listing.Vehicle_Rolloff ALTER COLUMN GenericString05 VARCHAR(50) NULL


-- align with interface and package

ALTER TABLE Listing.Vehicle ALTER COLUMN SellerCity VARCHAR(100) NULL
ALTER TABLE Listing.Vehicle_History ALTER COLUMN SellerCity VARCHAR(100) NULL
ALTER TABLE Listing.Vehicle_Preload ALTER COLUMN SellerCity VARCHAR(100) NULL
ALTER TABLE Listing.Vehicle_Rolloff ALTER COLUMN SellerCity VARCHAR(100) NULL
