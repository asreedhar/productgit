CREATE TABLE [Listing].VehicleOption_Rolloff
(
[ProviderID] [tinyint] NOT NULL,
[SetKeyHash] [binary] (16) NOT NULL,
[UniqueKeyHash] [binary] (16) NOT NULL,
[Ordinal] [smallint] NOT NULL,
[Description] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON Listing
GO


ALTER TABLE [Listing].VehicleOption_Rolloff ADD CONSTRAINT [PK_ListingVehicleOption_Rolloff] PRIMARY KEY CLUSTERED  ([ProviderID], [SetKeyHash], [UniqueKeyHash], [Ordinal]) 
GO