
UPDATE	Listing.ProviderFeedType
SET	Description		= 'Set Merge',
	ParseProviderCode	= 0
WHERE	ProviderFeedTypeID = 2

GO

ALTER TABLE Listing.Vehicle ADD SetKeyHash BINARY(16) NULL	
ALTER TABLE Listing.Vehicle_History ADD SetKeyHash BINARY(16) NULL
ALTER TABLE Listing.Vehicle_Preload ADD SetKeyHash BINARY(16) NULL CONSTRAINT DF_ListingVehicle_Preload__SetKeyHash DEFAULT (0x00)
ALTER TABLE Listing.Vehicle_Rolloff ADD SetKeyHash BINARY(16) NULL
GO

DROP INDEX Listing.Vehicle.IX_ListingVehicle__UniqueKeyHash
CREATE NONCLUSTERED INDEX IX_ListingVehicle__SetUniqueKeyHash ON Listing.Vehicle (SetKeyHash, UniqueKeyHash) INCLUDE (RowHash) WITH (ONLINE=ON) ON PS_ListingProviderId (_ProviderID) 
GO

DROP INDEX Listing.Vehicle_History.IX_ListingVehicle_History__UniqueKeyHash
CREATE NONCLUSTERED INDEX IX_ListingVehicle_History__SetUniqueKeyHash ON Listing.Vehicle_History (SetKeyHash, UniqueKeyHash) INCLUDE (RowHash) WITH (ONLINE=ON) ON PS_ListingProviderId (_ProviderID) 
GO

DROP INDEX Listing.Vehicle_Preload.IX_ListingVehicle_Preload__UniqueKeyHash
CREATE NONCLUSTERED INDEX IX_ListingVehicle_Preload__SetUniqueKeyHash ON Listing.Vehicle_Preload (SetKeyHash, UniqueKeyHash) INCLUDE (RowHash) WITH (ONLINE=ON) ON PS_ListingProviderId (_ProviderID) 
GO

UPDATE	Listing.Vehicle
SET	SetKeyHash = 0x00
GO