IF EXISTS (SELECT 1 FROM sys.indexes I WHERE object_id = OBJECT_id('Listing.Vehicle') AND name = 'IX_ListingVehicle__Herbie')
	DROP INDEX Listing.Vehicle.IX_ListingVehicle__Herbie
	
IF EXISTS (SELECT 1 FROM sys.indexes I WHERE object_id = OBJECT_id('Listing.Vehicle_History') AND name = 'IX_ListingVehicleHistory__Herbie')
	DROP INDEX Listing.Vehicle_History.IX_ListingVehicleHistory__Herbie

IF EXISTS (SELECT 1 FROM sys.indexes I WHERE object_id = OBJECT_id('Listing.Vehicle_Preload') AND name = 'IX_ListingVehiclePreload__Herbie')
	DROP INDEX Listing.Vehicle_Preload.IX_ListingVehiclePreload__Herbie
