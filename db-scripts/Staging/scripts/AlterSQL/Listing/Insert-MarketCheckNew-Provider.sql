
INSERT
INTO	Listing.Provider (ProviderID, DatafeedCode, ActiveLoadID,ProviderFeedTypeID, ProviderCode, ResponseFilePending,
                        DeleteThresholdPct, IsVideoUrlProvider, OptionDelimiter, ImageUrlDelimiter, SetValidationFlags)
SELECT	31, 'MarketCheckNew', 0, 2, 'MarketCheckNew', 0, NULL, 0, '|', '|', 1
 
GO
 
WITH Flags (ProviderID, Name, BitPosition, Description) AS (
SELECT	31, 'MAPI.Exclude', 1, 'Exclude vehicle from MAPI extract'
)
INSERT
INTO	Listing.VehicleValidationFlag (ProviderID, Name, BitPosition, Description)
SELECT	Flags.ProviderID, Name, BitPosition, Description
FROM	Flags 
	INNER JOIN Listing.Provider ON Flags.ProviderID = Provider.ProviderID
GO
 