INSERT 
INTO	Listing.AssignmentExpression (Description, ColumnName, Expression)
SELECT	'TCUV MultiMap ImageURL cleanup', 'ImageURLs', 'NULLIF(CASE WHEN RIGHT(ImageURLs,1)=''~'' THEN SUBSTRING(ImageURLs, 1, LEN(ImageURLs)-1) ELSE REPLACE(ImageURLs, ''~'', '';'') END, '''')'

INSERT
INTO	Listing.ProviderVehicleAssignmentExpression (ProviderID, AssignmentExpressionID, ExecutionOrder)
SELECT	10, SCOPE_IDENTITY(), 1
