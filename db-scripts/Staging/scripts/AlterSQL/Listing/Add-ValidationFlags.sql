
ALTER TABLE Listing.Vehicle ADD ValidationFlags BINARY(2) NOT NULL CONSTRAINT DF_ListingVehicle__ValidationFlags DEFAULT (0x0000)


ALTER TABLE Listing.Vehicle_History ADD ValidationFlags BINARY(2) NOT NULL CONSTRAINT DF_ListingVehicle_History__ValidationFlags DEFAULT (0x0000)
ALTER TABLE Listing.Vehicle_Preload ADD ValidationFlags BINARY(2) NOT NULL CONSTRAINT DF_ListingVehicle_Preload__ValidationFlags DEFAULT (0x0000)
ALTER TABLE Listing.Vehicle_Rolloff ADD ValidationFlags BINARY(2) NOT NULL CONSTRAINT DF_ListingVehicle_Rolloff__ValidationFlags DEFAULT (0x0000)
GO


EXEC sp_SetColumnDescription 'Listing.Vehicle.ValidationFlags', 'Collection of boolean flags as defined in Listing.VehicleValidationFlag.  Currently limited to 16 (two bytes) per provider.  Needs to be normalized (sorry, quick hit)'
GO

CREATE TABLE Listing.VehicleValidationFlag (
	ProviderID		TINYINT NOT NULL,
	Name			VARCHAR(50) NOT NULL,
	BitPosition		TINYINT NOT NULL,
	Description		VARCHAR(100) NULL,
	CONSTRAINT PK_StagingVehicleValidationFlag PRIMARY KEY CLUSTERED (ProviderID, Name)
	)
GO

ALTER TABLE Listing.VehicleValidationFlag ADD CONSTRAINT FK_ListingVehicle__ProviderID FOREIGN KEY (ProviderID) REFERENCES Listing.Provider (ProviderID)
GO

EXEC sp_SetTableDescription 'Listing.VehicleValidationFlag', 'Reference table containing flag definitions for the flags stored in Listing.Vehicle.ValidationFlags'
EXEC sp_SetColumnDescription 'Listing.VehicleValidationFlag.ProviderID', 'ProviderID associated with the flag'
EXEC sp_SetColumnDescription 'Listing.VehicleValidationFlag.Name', 'Name of the flag (unique per provider)'
EXEC sp_SetColumnDescription 'Listing.VehicleValidationFlag.BitPosition', 'Zero-based ordinal position within the ValidationFlags column'
EXEC sp_SetColumnDescription 'Listing.VehicleValidationFlag.Description', 'Long description of the flag'
GO

WITH Flags (ProviderID, Name, BitPosition, Description) AS (
SELECT	4, 'PING.Exclude', 0, 'Exclude vehicle from PING extract'
UNION
SELECT	4, 'MAPI.Exclude', 1, 'Exclude vehicle from MAPI extract'
UNION
SELECT	29, 'PING.Exclude', 0, 'Exclude vehicle from PING extract'
UNION
SELECT	29, 'MAPI.Exclude', 1, 'Exclude vehicle from MAPI extract'
UNION
SELECT	29, 'Sold', 2, 'Marked as Sold by the provider'
UNION 
SELECT	29, 'Cross-site', 3, 'Cross-Site listing (duplicate)'
UNION 
SELECT	29, 'Intra-site duplicate', 4, 'Intra-site duplicate (the "loser")'
UNION
SELECT	30, 'PING.Exclude', 0, 'Exclude vehicle from PING extract'
UNION
SELECT	30, 'MAPI.Exclude', 1, 'Exclude vehicle from MAPI extract'
UNION
SELECT 	10, 'PING.Exclude', 0, 'Exclude vehicle from PING extract'
UNION 
SELECT	11, 'PING.Exclude', 0, 'Exclude vehicle from PING extract'
UNION 
SELECT	23, 'PING.Exclude', 0, 'Exclude vehicle from PING extract'
UNION 
SELECT	2, 'PING.Exclude', 0, 'Exclude vehicle from PING extract'
UNION
SELECT	21, 'PING.Exclude', 0, 'Exclude vehicle from PING extract'
)
INSERT
INTO	Listing.VehicleValidationFlag (ProviderID, Name, BitPosition, Description)
SELECT	Flags.ProviderID, Name, BitPosition, Description
FROM	Flags 
	INNER JOIN Listing.Provider ON Flags.ProviderID = Provider.ProviderID
GO


--IF EXISTS (SELECT 1 FROM sys.indexes I WHERE object_id = OBJECT_id('Listing.Vehicle') AND name = 'IX_ListingVehicle__SetUniqueKeyHash')
--	DROP INDEX Listing.Vehicle.IX_ListingVehicle__SetUniqueKeyHash
	
--CREATE NONCLUSTERED INDEX IX_ListingVehicle__SetUniqueKeyHash ON Listing.Vehicle (SetKeyHash, UniqueKeyHash) INCLUDE (RowHash, OptionsHash, ValidationFlags) ON PS_ListingProviderId (_ProviderID)
--GO
	
ALTER TABLE Listing.Provider ADD SetValidationFlags BIT NOT NULL CONSTRAINT DF_ListingProvider__SetValidationFlags DEFAULT 0

GO

UPDATE	Listing.Provider
SET	SetValidationFlags = 1
WHERE	DatafeedCode IN ('CarsDotCom', 'Spidey', 'MarketCheck', 'TCUV', 'AIM-Listing', 'CarSoup', 'GetAutoListing', 'eCarList')

GO	

ALTER TABLE Listing.Provider ADD ProcessOptions AS (CAST(CASE WHEN OptionDelimiter IS NOT NULL THEN 1 ELSE 0 END AS BIT))
