ALTER TABLE Listing.Provider DROP COLUMN ProcessOptions
GO
ALTER TABLE Listing.Provider ADD ProcessOptions BIT NOT NULL CONSTRAINT DF_ListingProvider__ProcessOptions DEFAULT 0
GO

UPDATE	Listing.Provider
SET	ProcessOptions = CASE WHEN ProviderID <> 31 AND  [OptionDelimiter] IS NOT NULL THEN (1) ELSE (0) END
GO

EXEC sp_SetColumnDescription 'Listing.Provider.ProcessOptions', 'Flags whether to save normalized options to Listing.VehicleOption (currently used only by Ping)'