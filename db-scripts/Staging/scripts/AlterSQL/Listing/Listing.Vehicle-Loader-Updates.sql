CREATE TABLE Listing.Vehicle#LoadMetrics (
	LoadID			INT NOT NULL,
	SetKeyHash		BINARY(16) NOT NULL,
	InsertRowCount		INT NOT NULL,
	UpdateRowCount		INT NOT NULL,
	DeleteRowCount		INT NOT NULL,
	ExcludedRowCount	INT NULL,
	ExceededDeleteThreshold BIT NOT NULL CONSTRAINT DF_ListingVehicle#LoadMetrics__ExceededDeleteThreshold DEFAULT (0),
	CONSTRAINT PK_ListingVehicle#LoadMetrics PRIMARY KEY CLUSTERED (LoadID, SetKeyHash)
	)

GO

CREATE CLUSTERED INDEX IDX_ListingDeltaLog ON Listing.DeltaLog (LoadId, VIN)
GO
	

ALTER TABLE Listing.Vehicle ALTER COLUMN GenericString01 VARCHAR(100) NULL
ALTER TABLE Listing.Vehicle_History ALTER COLUMN GenericString01 VARCHAR(100) NULL
ALTER TABLE Listing.Vehicle_Preload ALTER COLUMN GenericString01 VARCHAR(100) NULL
ALTER TABLE Listing.Vehicle_Rolloff ALTER COLUMN GenericString01 VARCHAR(100) NULL

GO

ALTER TABLE Listing.Vehicle ALTER COLUMN GenericString02 VARCHAR(100) NULL
ALTER TABLE Listing.Vehicle_History ALTER COLUMN GenericString02 VARCHAR(100) NULL
ALTER TABLE Listing.Vehicle_Preload ALTER COLUMN GenericString02 VARCHAR(100) NULL
ALTER TABLE Listing.Vehicle_Rolloff ALTER COLUMN GenericString02 VARCHAR(100) NULL

GO

ALTER TABLE Listing.Vehicle ALTER COLUMN GenericString03 VARCHAR(100) NULL
ALTER TABLE Listing.Vehicle_History ALTER COLUMN GenericString03 VARCHAR(100) NULL
ALTER TABLE Listing.Vehicle_Preload ALTER COLUMN GenericString03 VARCHAR(100) NULL
ALTER TABLE Listing.Vehicle_Rolloff ALTER COLUMN GenericString03 VARCHAR(100) NULL

GO

ALTER TABLE Listing.Vehicle ALTER COLUMN GenericString04 VARCHAR(100) NULL
ALTER TABLE Listing.Vehicle_History ALTER COLUMN GenericString04 VARCHAR(100) NULL
ALTER TABLE Listing.Vehicle_Preload ALTER COLUMN GenericString04 VARCHAR(100) NULL
ALTER TABLE Listing.Vehicle_Rolloff ALTER COLUMN GenericString04 VARCHAR(100) NULL

GO

ALTER TABLE Listing.Vehicle ALTER COLUMN GenericString05 VARCHAR(100) NULL
ALTER TABLE Listing.Vehicle_History ALTER COLUMN GenericString05 VARCHAR(100) NULL
ALTER TABLE Listing.Vehicle_Preload ALTER COLUMN GenericString05 VARCHAR(100) NULL
ALTER TABLE Listing.Vehicle_Rolloff ALTER COLUMN GenericString05 VARCHAR(100) NULL

GO

ALTER TABLE Listing.Vehicle ADD ReferenceDate VARCHAR(30) NULL
ALTER TABLE Listing.Vehicle_History ADD ReferenceDate VARCHAR(30) NULL
ALTER TABLE Listing.Vehicle_Preload ADD ReferenceDate VARCHAR(30) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD ReferenceDate VARCHAR(30) NULL

GO
