INSERT
INTO	Listing.AssignmentExpression (Description, ColumnName, Expression )
SELECT	'Decode Trim from Year-Make-Model-Trim-Segment', 'Trim', 'NULLIF(REPLACE(LEFT(LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(SUBSTRING(GenericString01,6, 100), MK.Make, ''''), MD.Model, '''') , S.Segment, ''''))), 50), ''  '','' ''), '''')'

INSERT
INTO	Listing.ProviderVehicleAssignmentExpression (ProviderID, AssignmentExpressionID, ExecutionOrder)
SELECT	29, SCOPE_IDENTITY(), 1
FROM	Listing.Provider
WHERE	ProviderID = 29


/*

CREATE SCHEMA Spidey

DROP TABLE Spidey.ddc_sitemap_dealer_spider

CREATE TABLE Spidey.ddc_sitemap_dealer_spider (
	url		VARCHAR(100) NOT NULL,
	org		VARCHAR(100) NOT NULL,
	street_address	VARCHAR(100) NULL,
	locality	VARCHAR(100) NULL,
	region		VARCHAR(50) NULL,
	postal_code	VARCHAR(10) NULL,
	phone1		VARCHAR(20)  NULL,
	type		VARCHAR(20) NULL,
	time_stamp_utc	DATETIME NOT NULL,
	insert_date	DATE NOT NULL CONSTRAINT DF_ddc_sitemap_dealer_spider__insert_date DEFAULT GETDATE(),
	delete_date	DATE NULL,
	active		BIT NOT NULL CONSTRAINT DF_ddc_sitemap_dealer_spider__active DEFAULT 1,
	)
	

ALTER TABLE Spidey.ddc_sitemap_dealer_spider ADD CONSTRAINT PK_Spideyddc_sitemap_dealer_spider PRIMARY KEY CLUSTERED (active, url, insert_date)

--	UQ to enforce one status change per day

ALTER TABLE Spidey.ddc_sitemap_dealer_spider ADD CONSTRAINT UQ_Spideyddc_sitemap_dealer_spider UNIQUE (url, insert_date)





CREATE PROC Spidey.ddc_sitemap_dealer_spider#Delete
@url		VARCHAR(100)
AS

UPDATE	Spidey.ddc_sitemap_dealer_spider
SET	delete_date	= GETDATE(),
	active		= 0
WHERE	url = @url


CREATE PROC Spidey.ddc_sitemap_dealer_spider#Update
@url		VARCHAR(100),
----------------------------
@locality	VARCHAR(100),
@org		VARCHAR(100),
@street_address	VARCHAR(100),
@region		VARCHAR(50),
@postal_code	VARCHAR(10),
@phone1		VARCHAR(20),
@type		VARCHAR(20),
@time_stamp_utc DATETIME
AS

UPDATE	Spidey.ddc_sitemap_dealer_spider
SET	type		= @type,
	locality	= @locality,
	org		= @org,
	phone1		= @phone1,
	postal_code	= @postal_code,
	region		= @region,
	street_address	= @street_address,
	time_stamp_utc	= CASE WHEN ISDATE(@time_stamp_utc) = 1 THEN CAST(@time_stamp_utc AS datetime) ELSE NULL END	
WHERE	url = @url
	AND active = 1

*/