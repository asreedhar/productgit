INSERT
INTO	Listing.AssignmentExpression (Description, ColumnName, Expression)
SELECT	'Mileage Scrubber', 'Mileage', 'CASE WHEN Mileage LIKE ''%[^0-9.]%'' THEN NULL ELSE Mileage END'

INSERT
INTO	Listing.ProviderVehicleAssignmentExpression (ProviderID, AssignmentExpressionID, ExecutionOrder)
SELECT	30, SCOPE_IDENTITY(), 2
FROM	Listing.Provider
WHERE	ProviderID = 30


UPDATE	FIM
SET	DatafeedFileType = 'SetMerge' 
FROM	DBASTAT.dbo.FeedInterfaceMapping FIM
WHERE	DatafeedCode = 'MarketCheck'