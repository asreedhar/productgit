ALTER TABLE Listing.Vehicle ADD RowHash BINARY(16) NULL	
ALTER TABLE Listing.Vehicle_History ADD RowHash BINARY(16) NULL
ALTER TABLE Listing.Vehicle_Preload ADD RowHash BINARY(16) NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD RowHash BINARY(16) NULL
GO

DROP INDEX Listing.Vehicle.IX_ListingVehicle__UniqueKeyHash
CREATE NONCLUSTERED INDEX IX_ListingVehicle__UniqueKeyHash ON Listing.Vehicle (UniqueKeyHash) INCLUDE (RowHash) WITH (ONLINE=ON) ON PS_ListingProviderId (_ProviderID) 
GO

DROP INDEX Listing.Vehicle_History.IX_ListingVehicle_History__UniqueKeyHash
CREATE NONCLUSTERED INDEX IX_ListingVehicle_History__UniqueKeyHash ON Listing.Vehicle_History (UniqueKeyHash) INCLUDE (RowHash) WITH (ONLINE=ON) ON PS_ListingProviderId (_ProviderID) 
GO

DROP INDEX Listing.Vehicle_Preload.IX_ListingVehicle_Preload__UniqueKeyHash
CREATE NONCLUSTERED INDEX IX_ListingVehicle_Preload__UniqueKeyHash ON Listing.Vehicle_Preload (UniqueKeyHash) INCLUDE (RowHash) WITH (ONLINE=ON) ON PS_ListingProviderId (_ProviderID) 
GO
