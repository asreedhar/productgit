
IF EXISTS (SELECT 1 FROM sys.indexes I WHERE object_id = OBJECT_id('Listing.Vehicle_History') AND name = 'IX_ListingVehicle_History__SetUniqueKeyHash')
	DROP INDEX Listing.Vehicle_History.IX_ListingVehicle_History__SetUniqueKeyHash
	
CREATE NONCLUSTERED INDEX IX_ListingVehicle_History__SetUniqueKeyHash ON Listing.Vehicle_History (SetKeyHash, UniqueKeyHash) INCLUDE (RowHash, OptionsHash) ON PS_ListingProviderId (_ProviderID)
GO

IF EXISTS (SELECT 1 FROM sys.indexes I WHERE object_id = OBJECT_id('Listing.Vehicle') AND name = 'IX_ListingVehicle__SetUniqueKeyHash')
	DROP INDEX Listing.Vehicle.IX_ListingVehicle__SetUniqueKeyHash
	
CREATE NONCLUSTERED INDEX IX_ListingVehicle__SetUniqueKeyHash ON Listing.Vehicle (SetKeyHash, UniqueKeyHash) INCLUDE (RowHash, OptionsHash) ON PS_ListingProviderId (_ProviderID)
GO

IF EXISTS (SELECT 1 FROM sys.indexes I WHERE object_id = OBJECT_id('Listing.Vehicle_Preload') AND name = 'IX_ListingVehicle_Preload__SetUniqueKeyHash')
	DROP INDEX Listing.Vehicle_Preload.IX_ListingVehicle_Preload__SetUniqueKeyHash
	
CREATE NONCLUSTERED INDEX IX_ListingVehicle_Preload__SetUniqueKeyHash ON Listing.Vehicle_Preload (SetKeyHash, UniqueKeyHash) INCLUDE (RowHash, OptionsHash) ON PS_ListingProviderId (_ProviderID)
GO

IF OBJECT_ID('Listing.VehicleOption') IS NOT NULL DROP TABLE Listing.VehicleOption

CREATE TABLE Listing.VehicleOption (
	ProviderID		TINYINT NOT NULL,
	SetKeyHash		BINARY(16) NOT NULL,
	UniqueKeyHash		BINARY(16) NOT NULL,
	Ordinal			SMALLINT NOT NULL,		-- LIMIT IT, SHOULD IT BE A TINYINT???
	Description		VARCHAR(500) NOT NULL,
	DescriptionRank		SMALLINT NOT NULL
	)
GO

ALTER TABLE Listing.VehicleOption ADD CONSTRAINT PK_ListingVehicleOption PRIMARY KEY CLUSTERED (ProviderID, SetKeyHash, UniqueKeyHash, Ordinal) ON [PS_ListingProviderId] (ProviderID) 
GO

ALTER TABLE Listing.VehicleOption ADD CONSTRAINT FK_ListingVehicleOption__Provider FOREIGN KEY (ProviderID) REFERENCES Listing.Provider(ProviderID)
GO

EXEC dbo.sp_SetTableDescription 'Listing.VehicleOption', 'Stores normalized option values for Listing.Vehicle'
GO
EXEC dbo.sp_SetColumnDescription 'Listing.VehicleOption.DescriptionRank', 'Stores the rank by ordinal position of the description within the set of options for a vehicle.  Useful for de-duplication'
