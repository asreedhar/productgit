ALTER TABLE Listing.Vehicle ADD LastChangeDate DATE NULL


ALTER TABLE Listing.Vehicle_History ADD LastChangeDate DATE NULL
ALTER TABLE Listing.Vehicle_Preload ADD LastChangeDate DATE NULL
ALTER TABLE Listing.Vehicle_Rolloff ADD LastChangeDate DATE NULL
GO

 
GO

UPDATE	V
SET	LastChangeDate	= CAST(DH.ProcessDT AS DATE)
FROM	Staging.Listing.Vehicle V
	INNER JOIN DBASTAT.dbo.Dataload_History DH ON COALESCE(V.[_LastUpdateLoadID], V._LoadID ) = DH.Load_ID

GO	
ALTER TABLE Listing.Vehicle DROP COLUMN _LastUpdateLoadID
ALTER TABLE Listing.Vehicle_History DROP COLUMN _LastUpdateLoadID
ALTER TABLE Listing.Vehicle_Preload DROP COLUMN _LastUpdateLoadID
ALTER TABLE Listing.Vehicle_Rolloff DROP COLUMN _LastUpdateLoadID	