-- drop table Staging.postings.CarsDotComPhoneLeads
CREATE TABLE [Postings].[CarsDotComPhoneLeads](
                [insertId] [bigint] IDENTITY(1,1) NOT NULL,
                [businessUnitId] [int] NULL,
                [LeadDate] [datetime] NULL,
                [CallerPhone] [varchar](20) NULL,
                [CallerZip] [int] NULL,
                [CallStatus] [varchar](50) NULL,
                [CallDurationMinutes] [int] NULL,
                [CallDurationSeconds] [int] NULL,
                [Load_ID] int NOT NULL, -- Server01.DBAStat.dbo.Dataload_History.Load_ID
                [ExtractRequestHandle] uniqueidentifier NOT NULL, -- Server03.Datafeeds.Extract.FetchDotCom#Request.Handle
                FetchDateInserted DATETIME NOT NULL,
                DateStaged DATETIME NOT NULL constraint DF_CarsDotComPhoneLeads_DateStaged DEFAULT (GETDATE())
) ON [PRIMARY]
GO

alter table postings.CarsDotComPhoneLeads add constraint PK_CarsDotComPhoneLeads primary key clustered (Load_ID, ExtractRequestHandle, insertId)
go


EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.postings.CarsDotComPhoneLeads.Load_ID',
	@Description=N'Maps to Server01.DBAStat.dbo.Dataload_History.Load_ID'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.postings.CarsDotComPhoneLeads.ExtractRequestHandle',
	@Description=N'Maps to Server03.Datafeeds.Extract.FetchDotCom#Request.Handle'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.postings.CarsDotComPhoneLeads.FetchDateInserted',
	@Description=N'The DateInserted value from the source files.'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.postings.CarsDotComPhoneLeads.DateStaged',
	@Description=N'The date this record was loaded into the staging database.'
go

