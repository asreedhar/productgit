-- drop table Merchandising.CarsDotComPerfSummary
CREATE TABLE [merchandising].[CarsDotComPerfSummary](
				[insertId] [bigint] IDENTITY(1,1) NOT NULL,
                [businessUnitID] [int] NOT NULL,
                [month] [datetime] NOT NULL
				, InSearchResult int
				, RequestDetail int
				, SearchDealer int
				, ViewVideo int
				, EmailDealerNew int
				, EmailDealerUsed int
				, PhoneProspectNew int
				, PhoneProspectsUsed int
				, ChatProspectsNew int
				, ChatProspectsUsed int
				, ClickThru int
				, PrintAD int
				, ViewMap int
				, TotalContacts int
				, PctListPriceUsed int
				, PctListCommentUsed int
				, PctListPhotoUsed int
				, PctSinglePhotoUsed int
				, pctMultiPhotoUsed int
				, PctListVideoUsed int
				, PctListPriceNew int
				, PctListCommentNew int
				, PctListPhotoNew int
				, PctSinglePhotoNew int
				, pctMultiPhotoNew int
				, PctListVideoNew int,
                [Load_ID] int NOT NULL, -- Server01.DBAStat.dbo.Dataload_History.Load_ID
                [ExtractRequestHandle] uniqueidentifier NOT NULL, -- Server03.Datafeeds.Extract.FetchDotCom#Request.Handle
                FetchDateInserted DATETIME NOT NULL,
                DateStaged DATETIME NOT NULL constraint DF_CarsDotComPerfSummary_DateStaged DEFAULT (GETDATE())
) ON [PRIMARY]
GO

alter table Merchandising.CarsDotComPerfSummary add constraint PK_CarsDotComPerfSummary primary key clustered (Load_ID, ExtractRequestHandle, insertId)
go


EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.CarsDotComPerfSummary.Load_ID',
	@Description=N'Maps to Server01.DBAStat.dbo.Dataload_History.Load_ID'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.CarsDotComPerfSummary.ExtractRequestHandle',
	@Description=N'Maps to Server03.Datafeeds.Extract.FetchDotCom#Request.Handle'
GO

EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.CarsDotComPerfSummary.FetchDateInserted',
	@Description=N'The DateInserted value from the source files.'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.CarsDotComPerfSummary.DateStaged',
	@Description=N'The date this record was loaded into the staging database.'
GO