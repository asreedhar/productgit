-- drop table Merchandising.CarsDotComExSummary
CREATE TABLE [Merchandising].[CarsDotComExSummary](
				[insertId] [bigint] IDENTITY(1,1) NOT NULL,
                [businessUnitID] [int] NOT NULL,
                [month] [datetime] NOT NULL,
                FetchDateInserted DATETIME NOT NULL,
                InSearchResult int not null,
                SearchToDetailRate tinyint not null,
                ViewDetail int not null,
                Contacts int not null,
                Sold int not null,
                [Load_ID] int NOT NULL, -- Server01.DBAStat.dbo.Dataload_History.Load_ID
                [ExtractRequestHandle] uniqueidentifier NOT NULL, -- Server03.Datafeeds.Extract.FetchDotCom#Request.Handle
                DateStaged DATETIME NOT NULL constraint DF_CarsDotComExSummary_DateStaged DEFAULT (GETDATE())
) ON [PRIMARY]
GO

alter table Merchandising.CarsDotComExSummary add constraint PK_CarsDotComExSummary primary key clustered (Load_ID, ExtractRequestHandle, insertId)
go

EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.CarsDotComExSummary.Load_ID',
	@Description=N'Maps to Server01.DBAStat.dbo.Dataload_History.Load_ID'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.CarsDotComExSummary.ExtractRequestHandle',
	@Description=N'Maps to Server03.Datafeeds.Extract.FetchDotCom#Request.Handle'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.CarsDotComExSummary.FetchDateInserted',
	@Description=N'The DateInserted value from the source files.'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.CarsDotComExSummary.DateStaged',
	@Description=N'The date this record was loaded into the staging database.'
GO
