-- DROP TABLE [merchandising].[CarsDotComVehSummary]
CREATE TABLE [merchandising].[CarsDotComVehSummary](
				insertId bigint not null identity (1,1),
                [businessUnitID] [int] NOT NULL,
                [month] [datetime] NOT NULL
				, StockNum varchar(25)
				, Year smallint
				, Make varchar(150)
				, VIN varchar(25)
				, Type varchar(5)
				, Price int
				, Age int
				, InSearchResult int
				, DetailViewed int
				, VideoViewed int
				, AdsPrinted int
				, MapViewed int
				, ClickThru int
				, EmailSent int
				, Chat int
				, Total int
				, PhotoCount int ,
                [Load_ID] int NOT NULL, -- Server01.DBAStat.dbo.Dataload_History.Load_ID
                [ExtractRequestHandle] uniqueidentifier NOT NULL, -- Server03.Datafeeds.Extract.FetchDotCom#Request.Handle
                FetchDateInserted DATETIME NOT NULL,
                DateStaged DATETIME NOT NULL constraint DF_CarsDotComVehSummary_DateStaged DEFAULT (GETDATE())
) ON [PRIMARY]
GO

alter table [merchandising].[CarsDotComVehSummary] add constraint PK_CarsDotComVehSummary primary key clustered (Load_ID, ExtractRequestHandle, insertId)
go

EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.CarsDotComVehSummary.Load_ID',
	@Description=N'Maps to Server01.DBAStat.dbo.Dataload_History.Load_ID'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.CarsDotComVehSummary.ExtractRequestHandle',
	@Description=N'Maps to Server03.Datafeeds.Extract.FetchDotCom#Request.Handle'
GO

EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.CarsDotComVehSummary.FetchDateInserted',
	@Description=N'The DateInserted value from the source files.'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.CarsDotComVehSummary.DateStaged',
	@Description=N'The date this record was loaded into the staging database.'
GO