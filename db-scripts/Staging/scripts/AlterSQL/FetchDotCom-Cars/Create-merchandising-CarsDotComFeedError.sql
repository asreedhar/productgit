-- DROP TABLE [Merchandising].[CarsDotComFeedError]
CREATE TABLE [Merchandising].[CarsDotComFeedError] (
	[insertId] [bigint] IDENTITY(1,1) NOT NULL,
    FetchDateInserted DATETIME NOT NULL,
    [FeedLogin] VARCHAR(50),
    [ErrorDate] DATETIME,
    [ErrorType] VARCHAR(50),
    [ExtractRequestHandle] UNIQUEIDENTIFIER NOT NULL, -- Server03.Datafeeds.Extract.FetchDotCom#Request.Handle
    [Month] DATETIME NOT NULL,
    [BusinessUnitID] INT NOT NULL,
    [Load_ID] int not null, -- Server01.DBAStat.dbo.Dataload_History.Load_ID
    DateStaged DATETIME NOT NULL constraint DF_CarsDotComFeedError_DateStaged DEFAULT (GETDATE())
) ON [PRIMARY]
GO

alter table Merchandising.CarsDotComFeedError add constraint PK_CarsDotComFeedError primary key clustered (Load_ID, ExtractRequestHandle, insertId)
go

EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.CarsDotComFeedError.Load_ID',
	@Description=N'Maps to Server01.DBAStat.dbo.Dataload_History.Load_ID'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.CarsDotComFeedError.ExtractRequestHandle',
	@Description=N'Maps to Server03.Datafeeds.Extract.FetchDotCom#Request.Handle'
GO

EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.CarsDotComFeedError.FetchDateInserted',
	@Description=N'The DateInserted value from the source files.'
GO
EXEC dbo.sp_SetColumnDescription
	@ObjectName=N'Staging.Merchandising.CarsDotComFeedError.DateStaged',
	@Description=N'The date this record was loaded into the staging database.'
GO