SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

-- drop table [Merchandising].[CarsDotComVehOnline]
CREATE TABLE [Merchandising].[CarsDotComVehOnline](
	[insertId] [bigint] IDENTITY(1,1) NOT NULL,
	[BusinessUnitID] [int] NOT NULL,
	[VIN] [varchar](50) NULL,
	[Type] [varchar](50) NULL,
	[YearMakeModel] [varchar](75) NULL,
	[StockNumber] [varchar](50) NULL,
	[HasPhoto] [bit] NULL,
	[Price] [money] NULL,
	[MarketAvgPrice] [money] NULL,
	[Mileage] [int] NULL,
	[MarketAvgMileage] [int] NULL,
	[ExpirationDate] [datetime] NULL,
	[Source] [varchar](100) NULL,
	[PhotoCount] [int] NULL,
	[DaysLive] [int] NULL,
	[Load_ID] [int] NOT NULL,
	[ExtractRequestHandle] [uniqueidentifier] NOT NULL,
	[FetchDateInserted] [datetime] NOT NULL,
	[DateStaged] [datetime] NOT NULL
) ON [PRIMARY]

GO

alter table Merchandising.CarsDotComVehOnline add constraint PK_CarsDotComVehOnline primary key clustered (Load_ID, ExtractRequestHandle, insertId)
go


SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maps to Server01.DBAStat.dbo.Dataload_History.Load_ID' , @level0type=N'SCHEMA',@level0name=N'Merchandising', @level1type=N'TABLE',@level1name=N'CarsDotComVehOnline', @level2type=N'COLUMN',@level2name=N'Load_ID'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Maps to Server03.Datafeeds.Extract.FetchDotCom#Request.Handle' , @level0type=N'SCHEMA',@level0name=N'Merchandising', @level1type=N'TABLE',@level1name=N'CarsDotComVehOnline', @level2type=N'COLUMN',@level2name=N'ExtractRequestHandle'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The DateInserted value from the source files.' , @level0type=N'SCHEMA',@level0name=N'Merchandising', @level1type=N'TABLE',@level1name=N'CarsDotComVehOnline', @level2type=N'COLUMN',@level2name=N'FetchDateInserted'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The date this record was loaded into the staging database.' , @level0type=N'SCHEMA',@level0name=N'Merchandising', @level1type=N'TABLE',@level1name=N'CarsDotComVehOnline', @level2type=N'COLUMN',@level2name=N'DateStaged'
GO

ALTER TABLE [Merchandising].[CarsDotComVehOnline] ADD  CONSTRAINT [DF_Merchandising_CarsDotComVehOnline_DateStaged]  DEFAULT (getdate()) FOR [DateStaged]
GO


