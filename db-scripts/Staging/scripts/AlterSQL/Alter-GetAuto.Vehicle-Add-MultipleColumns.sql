--
-- Modify GetAuto.Vehicle - Add
--
ALTER TABLE GetAuto.Vehicle
ADD VehicleClass VARCHAR(50) NULL
, OEMColorCodeInterior VARCHAR(50) NULL
, GenericColorInterior VARCHAR(50) NULL
, WholesalePrice INT NULL
, MSRP INT NULL
, InternetSpecial VARCHAR(1) NULL
, DatePhotosUpdated DATETIME NULL
, LeaseResidual DECIMAL(9,2) NULL

--
-- Modify GetAuto.Vehicle - Change
--
ALTER TABLE GetAuto.Vehicle
ALTER COLUMN OemColorCode VARCHAR(50)

GO