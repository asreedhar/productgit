package biz.firstlook.ping.persist.hibernate;

import java.util.List;

import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.ping.model.Provider;
import biz.firstlook.ping.persist.ProviderDao;

public class ProviderHibernate extends GenericHibernate<Provider,Integer> implements
		ProviderDao {

	public ProviderHibernate() {
		super();
	}

	public Provider findByCode(String code) {
		Provider example = new Provider();
		example.setCode( code );
		List<Provider> providers = findByExample( example );
		if( providers == null || providers.isEmpty() ) {
			throw new RuntimeException("No provders found for code: " + code );
		}
		return providers.get(0);
	}

	public List<Provider> findAvailable() {
		Provider example = new Provider();
		example.setAvailable(true);
		List<Provider> providers = findByExample( example );
		return providers;
	}

}
