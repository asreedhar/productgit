package biz.firstlook.ping.persist;

import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.persist.common.GenericDAO;
import biz.firstlook.ping.model.MakeCode;
import biz.firstlook.ping.model.Provider;

public interface MakeCodeDao extends GenericDAO<MakeCode, Integer> {

	@Transactional(readOnly=false)
	void deleteByProvider(Provider provider);
}
