package biz.firstlook.ping.persist;

import biz.firstlook.persist.common.GenericDAO;
import biz.firstlook.ping.model.PingSystem;

public interface PingSystemDao extends GenericDAO<PingSystem, Integer> {
}
