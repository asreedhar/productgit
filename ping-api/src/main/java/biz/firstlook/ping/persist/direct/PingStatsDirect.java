package biz.firstlook.ping.persist.direct;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import biz.firstlook.ping.model.MappingStat;

public class PingStatsDirect extends JdbcTemplate {
	
	private MappingStatusQuery mappingStatusQuery;

	public PingStatsDirect() {}
	
	@SuppressWarnings("unchecked")
	public List<MappingStat> getMappingStats() {
		List<MappingStat> stats = mappingStatusQuery.execute();
		return stats;
	}

	public MappingStatusQuery getMappingStatusQuery() {
		return mappingStatusQuery;
	}

	public void setMappingStatusQuery(MappingStatusQuery mappingStatusQuery) {
		this.mappingStatusQuery = mappingStatusQuery;
	}
	
	

	
	
	
}
