package biz.firstlook.ping.persist;

import java.util.List;

import biz.firstlook.persist.common.GenericDAO;
import biz.firstlook.ping.model.MakeModelGrouping;

public interface MakeModelGroupingDao extends GenericDAO<MakeModelGrouping, Integer> {
	public List<MakeModelGrouping> findByMakeModel(String make, String model);
}
