package biz.firstlook.ping.persist.hibernate;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;

import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.ping.model.MakeCode;
import biz.firstlook.ping.model.Provider;
import biz.firstlook.ping.persist.MakeCodeDao;

public class MakeCodeHibernate extends GenericHibernate<MakeCode,Integer> implements
		MakeCodeDao {
	
	private static Logger log = Logger.getLogger( MakeCodeHibernate.class );

	public MakeCodeHibernate() {
		super();
	}

	public void deleteByProvider(final Provider provider) {
		log.info( "Deleting all codes by provider: " + provider );	
		getHibernateTemplate().execute( new HibernateCallback() {
			@SuppressWarnings("unchecked")
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria crit = session.createCriteria( MakeCode.class )
				    .createCriteria("provider")
				    	.add( Restrictions.eq( "id", provider.getId() ) );
				List<MakeCode> codes = new ArrayList<MakeCode>( crit.list() );
				makeTransient( codes );
				return new MakeCode();
			}
		});
	}

}
