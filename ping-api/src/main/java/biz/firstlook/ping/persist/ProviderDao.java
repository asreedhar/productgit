package biz.firstlook.ping.persist;

import java.util.List;

import biz.firstlook.persist.common.GenericDAO;
import biz.firstlook.ping.model.Provider;

public interface ProviderDao extends GenericDAO<Provider, Integer> {
	public Provider findByCode(String code);
	public List<Provider> findAvailable();
}
