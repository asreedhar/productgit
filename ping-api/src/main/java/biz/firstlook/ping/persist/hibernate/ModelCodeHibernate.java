package biz.firstlook.ping.persist.hibernate;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.HibernateCallback;

import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.ping.model.MakeModelGrouping;
import biz.firstlook.ping.model.ModelCode;
import biz.firstlook.ping.model.Provider;
import biz.firstlook.ping.persist.ModelCodeDao;

public class ModelCodeHibernate extends GenericHibernate<ModelCode,Integer> implements
		ModelCodeDao {

	private static Logger log = Logger.getLogger( ModelCodeHibernate.class );
	
	public ModelCodeHibernate() {
		super();
	}

	public void deleteByProvider(final Provider provider) {
		log.info( "Deleting all codes by provider: " + provider );	
		getHibernateTemplate().execute( new HibernateCallback() {
			@SuppressWarnings("unchecked")
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria crit = session.createCriteria( ModelCode.class )
				    .createCriteria("provider")
				    	.add( Restrictions.eq( "id", provider.getId() ) );
				List<ModelCode> codes = new ArrayList<ModelCode>( crit.list() );
				makeTransient( codes );
				return new ModelCode();
			}
		});
	}

	@SuppressWarnings("unchecked")
	public List<ModelCode> findByProvider(final Provider provider) {
		log.info( "Finding all codes by provider: " + provider );	
		List<ModelCode> codes = (List<ModelCode>) getHibernateTemplate().execute( new HibernateCallback() {
			@SuppressWarnings("unchecked")
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria crit = session.createCriteria( ModelCode.class )
				    .createCriteria("provider")
				    	.add( Restrictions.eq( "id", provider.getId() ) );
				return new ArrayList<ModelCode>( crit.list() );
			}
		});
		return codes;
	}

	public ModelCode findByMakeModelGroupingProvider(final MakeModelGrouping makeModelGrouping, final Provider provider) {
		log.info( "Finding all codes by provider: " + provider + ", and make model Gropuing: " + makeModelGrouping );	
		ModelCode code = (ModelCode) getHibernateTemplate().execute( new HibernateCallback() {
			@SuppressWarnings("unchecked")
			public Object doInHibernate(Session session) throws HibernateException, SQLException {
				Criteria crit = session.createCriteria( ModelCode.class );
				crit.createCriteria("provider")
				    .add( Restrictions.eq( "id", provider.getId() ) );
				crit.createCriteria("groupings")
					.add( Restrictions.eq( "id", makeModelGrouping.getId() ) );
				return (ModelCode) crit.uniqueResult();
			}
		});
		return code;
	}
	
}
