package biz.firstlook.ping.persist.direct;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import org.springframework.jdbc.object.MappingSqlQueryWithParameters;

import biz.firstlook.ping.model.MappingStat;

public class MappingStatusQuery extends MappingSqlQueryWithParameters {

	@Override
	protected Object mapRow(ResultSet rs, int rowNum, Object[] parameters,Map context) throws SQLException {
		MappingStat stat = new MappingStat();
		stat.setNumVehicles( rs.getInt(1) );
		stat.setMakeModelGroupingId( rs.getInt(2));
		stat.setMake( rs.getString(3) );
		stat.setModel( rs.getString(4) );
		stat.setSegment( rs.getString( 6 ) );
		stat.setCodeCount( rs.getInt(7) );
		return stat;
	}

}
