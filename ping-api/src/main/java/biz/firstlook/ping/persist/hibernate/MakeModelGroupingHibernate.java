package biz.firstlook.ping.persist.hibernate;

import java.util.List;

import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.ping.model.MakeModelGrouping;
import biz.firstlook.ping.persist.MakeModelGroupingDao;

public class MakeModelGroupingHibernate extends GenericHibernate<MakeModelGrouping,Integer> implements
		MakeModelGroupingDao {

	public MakeModelGroupingHibernate() {
		super();
	}

	public List<MakeModelGrouping> findByMakeModel(String make, String model) {
		MakeModelGrouping mmg = new MakeModelGrouping();
		mmg.setMake( make );
		mmg.setModel( model );
		return findByExample( mmg );
	}
	
}
