package biz.firstlook.ping.persist;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.persist.common.GenericDAO;
import biz.firstlook.ping.model.MakeModelGrouping;
import biz.firstlook.ping.model.ModelCode;
import biz.firstlook.ping.model.Provider;

public interface ModelCodeDao extends GenericDAO<ModelCode, Integer> {

	@Transactional(readOnly=false)
	void deleteByProvider(Provider provider);

	List<ModelCode> findByProvider(Provider provider);

	ModelCode findByMakeModelGroupingProvider(MakeModelGrouping makeModelGrouping, Provider provider);
}
