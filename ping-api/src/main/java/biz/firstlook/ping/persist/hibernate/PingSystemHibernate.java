package biz.firstlook.ping.persist.hibernate;

import biz.firstlook.persist.common.hibernate.GenericHibernate;
import biz.firstlook.ping.model.PingSystem;
import biz.firstlook.ping.persist.PingSystemDao;

public class PingSystemHibernate extends GenericHibernate<PingSystem,Integer> implements
		PingSystemDao {

	public PingSystemHibernate() {
		super();
	}

}
