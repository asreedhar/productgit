package biz.firstlook.ping.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;


@Entity
@Table(name="MakeModelGrouping")
public class MakeModelGrouping {
	
	@Id
	@Column(name="MakeModelGroupingID")
	private Integer id;
	
	private Integer groupingDescriptionId;
	private String make;
	private String model;
	private Integer segmentId;
	
	public MakeModelGrouping() {}


	public Integer getSegmentId() {
		return segmentId;
	}


	public void setSegmentId(Integer segmentId) {
		this.segmentId = segmentId;
	}

	public Integer getGroupingDescriptionId() {
		return groupingDescriptionId;
	}

	public void setGroupingDescriptionId(Integer groupingDescriptionId) {
		this.groupingDescriptionId = groupingDescriptionId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	} 
	
	public String toString() {
		return ReflectionToStringBuilder.reflectionToString(this);
	}

}
