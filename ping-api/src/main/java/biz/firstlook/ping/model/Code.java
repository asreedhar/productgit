package biz.firstlook.ping.model;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
    name="type",
    discriminatorType=DiscriminatorType.STRING
)
@Table(name="PING_Code")
public abstract class Code {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private String name;
	private String value;
	
	@ManyToOne
	@JoinColumn(name="providerId")
	private Provider provider;
	
	public Code() {}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}
	
	public String toString() {
		return new ToStringBuilder(this).append("type", getClass().getSimpleName()).append( "id", id ).append("name", name).append("value", value).append("provider", provider).toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if( obj instanceof Code && obj != null ) {
			Code other = (Code) obj;
			return (new EqualsBuilder().append( name, other.getName())
						.append( value, other.getValue() )
						.append( provider, other.getProvider() )).isEquals();
		}
		return false;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append( name ).append( value ).append( provider ).hashCode();
	}

	
	
}
