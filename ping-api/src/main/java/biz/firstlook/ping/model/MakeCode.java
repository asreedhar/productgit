package biz.firstlook.ping.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue("K")
public class MakeCode extends Code {

	@OneToMany(mappedBy="make",cascade=CascadeType.ALL)
	private Set<ModelCode> models;
	
	public MakeCode() {}
	
	public MakeCode(String value, String name) {
		this.setValue( value );
		this.setName( name );
	}

	public Set<ModelCode> getModels() {
		return models;
	}

	public void setModels(Set<ModelCode> models) {
		this.models = models;
	}
	
	

}
