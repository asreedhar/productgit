package biz.firstlook.ping.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

@Entity
@DiscriminatorValue("D")
public class ModelCode extends Code {
	
	@ManyToOne
	@JoinColumn(name="parentId")
	private MakeCode make;
	
    @ManyToMany(
       targetEntity=MakeModelGrouping.class,
       cascade={CascadeType.PERSIST, CascadeType.MERGE}
    )
    @JoinTable(
       name="PING_GroupingCodes",
       joinColumns={@JoinColumn(name="CodeId")},
       inverseJoinColumns={@JoinColumn(name="MakeModelGroupingId")}
    )
    private Set<MakeModelGrouping> groupings = new HashSet<MakeModelGrouping>();
	
	public ModelCode() {}

	public ModelCode(String value, String name, MakeCode make) {
		this.setValue( value );
		this.setName( name );
		this.setMake( make );
	}

	public MakeCode getMake() {
		return make;
	}

	public void setMake(MakeCode make) {
		this.make = make;
	}

	public Set<MakeModelGrouping> getGroupings() {
		return groupings;
	}

	public void setGroupings(Set<MakeModelGrouping> groupings) {
		this.groupings = groupings;
	}

	@Override
	public boolean equals(Object obj) {
		if( obj instanceof ModelCode && obj != null && super.equals(obj)) {
			ModelCode other = (ModelCode) obj;
			return (new EqualsBuilder().append( make, other.getMake() )).isEquals();
		}
		return false;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append( super.hashCode() ).append( make ).hashCode();
	}
	
	

}
