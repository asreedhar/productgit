package biz.firstlook.ping.model;

public class MappingStat {

	private Integer numVehicles;
	private Integer makeModelGroupingId;
	private String make;
	private String model;
	private String segment;
	private Integer codeCount;
	
	public MappingStat() {}

	public Integer getCodeCount() {
		return codeCount;
	}

	public void setCodeCount(Integer codeCount) {
		this.codeCount = codeCount;
	}

	public String getSegment() {
		return segment;
	}

	public void setSegment(String segment) {
		this.segment = segment;
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public Integer getMakeModelGroupingId() {
		return makeModelGroupingId;
	}

	public void setMakeModelGroupingId(Integer makeModelGroupingId) {
		this.makeModelGroupingId = makeModelGroupingId;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Integer getNumVehicles() {
		return numVehicles;
	}

	public void setNumVehicles(Integer numVehicles) {
		this.numVehicles = numVehicles;
	}
	
	
}
