package biz.firstlook.ping.model;

public class Price {
	
	private ModelCode model;
	private Integer year;
	private Integer distance;
	private Integer mileage;
	private Integer price;
	private String modelText;
	
	public Price() {
		price = 0;
	}

	public Integer getDistance() {
		return distance;
	}

	public void setDistance(Integer distance) {
		this.distance = distance;
	}

	public Integer getMileage() {
		return mileage;
	}

	public void setMileage(Integer mileage) {
		this.mileage = mileage;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public ModelCode getModel() {
		return model;
	}

	public void setModel(ModelCode model) {
		this.model = model;
	}

	public String getModelText() {
		return modelText;
	}

	public void setModelText(String modelText) {
		this.modelText = modelText;
	}
	
	

}
