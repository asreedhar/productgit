package biz.firstlook.api.ping.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.ParsePosition;
import java.util.List;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;

import biz.firstlook.api.ping.Prices;
import biz.firstlook.api.ping.util.HttpClientCallback;
import biz.firstlook.api.ping.util.HttpClientTemplate;
import biz.firstlook.ping.model.MakeCode;
import biz.firstlook.ping.model.ModelCode;

public abstract class PriceProvider  {
	
	protected String substitutedUrl;
	
	public PriceProvider() {}
	
	public String subUrl( String url, String makeCode, String makeName, String model, Integer year, Integer distance, String zip ) {
		substitutedUrl = url.replace( "%make%", makeCode )
			.replace( "%makeName%", makeName )
			.replace( "%distance%", distance.toString())
			.replace( "%year%", year.toString() )
			.replace( "%zip%", zip )
			.replace( "%model%", model );
		return substitutedUrl;
	}
	
	protected String subUrl( String url, String make  ) {
		return url.replace( "%make%", make );
	}
	
	protected String getPage( String url ) throws Exception {
		HttpClientTemplate template = new HttpClientTemplate();
		GetMethod getMethod = new GetMethod(url);
		return makeRequest(template, getMethod);
	}

	private String makeRequest(HttpClientTemplate template, HttpMethod method) {
		String content = "";
		try
		{
			content = template.executeMethodWithCallback( method, new HttpClientCallback<String>()
			{

				public String doInHttpClient( int statusCode, HttpMethod method ) throws HttpException
				{
					BufferedReader stream = null;
					try
					{
						stream = new BufferedReader( new InputStreamReader( method.getResponseBodyAsStream() ) );
						String textLine = null;
						StringBuffer sb = new StringBuffer();
						while ( (textLine = stream.readLine()) != null )
						{
							sb.append( textLine.trim() );
						}
						
						return sb.toString();
					}
					catch ( IOException e )
					{
						throw new HttpException( "Error Trying to read data: ", e );
					}
					finally
					{
						try
						{
							stream.close();
						}
						catch ( IOException e )
						{
							e.printStackTrace();
						}
					}
				}
			} );
			
		}
		catch ( HttpException e )
		{
			e.printStackTrace();
		}
		return content;
	}
	
	
	public List<MakeCode> fetchMakes() throws Exception {
		String makesPage = retrieveMakes();
		return parseMakes( makesPage );
	}

	abstract List<MakeCode> parseMakes(String makesPage);

	protected String retrieveMakes() throws Exception {
	  return getPage( getMakeUrl() );
	}

	abstract Prices parseCars(String page);
	abstract Prices parsePrices(String page, Prices prices) throws Exception;
	
	public List<ModelCode> fetchModels(MakeCode make) throws Exception {
	  String modelsPage = getPage( subUrl( getModelsUrl(), make.getValue() ) );
	  return parseModels(modelsPage, make);
	}

	abstract List<ModelCode> parseModels(String modelsPage, MakeCode make);
	
	public abstract String getProviderCode();
	public abstract String getMakeUrl();
	public abstract String getModelsUrl();
	public abstract String getSearchUrl();
	
	protected String retrieveSearch(String makeCode, String makeName, String model,Integer year,Integer distance,String zip) throws Exception {
	  return getPage( subUrl( getSearchUrl(), makeCode, makeName, model, year, distance, zip ) );
	}
	  
	protected Prices fetchSearch(String makeCode, String makeName, String model, Integer year, Integer distance, String zip ) throws Exception {
	  String page = retrieveSearch(makeCode, makeName, model,year,distance,zip);
	  Prices prices = parseCars(page);
	  return parsePrices( page, prices );
	}
	
	public Prices getQuote( ModelCode modelCode, Integer year, Integer distance, String zip ) throws Exception {
		Prices prices = fetchSearch( modelCode.getMake().getValue(),modelCode.getMake().getName(), modelCode.getValue(), year, distance, zip );
		prices.setUrl( subUrl( getSearchUrl(), modelCode.getMake().getValue(), modelCode.getMake().getName(), modelCode.getValue(), year, distance, zip ) );
		return prices;
	}

	protected String getSubstitutedUrl() {
		return substitutedUrl;
	}

	protected int readInteger(String text, int defaultValue) {
		if (text != null) {
			Number number = new DecimalFormat().parse(text, new ParsePosition(0));
			if (number != null) {
				return number.intValue();
			}
		}
		return defaultValue;
	}

}
