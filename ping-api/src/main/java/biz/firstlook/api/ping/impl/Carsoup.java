package biz.firstlook.api.ping.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpClientParams;
import org.apache.log4j.Logger;

import biz.firstlook.api.ping.Prices;
import biz.firstlook.ping.model.MakeCode;
import biz.firstlook.ping.model.ModelCode;
import biz.firstlook.ping.model.Price;

public class Carsoup extends PriceProvider
{
	public static final String CODE = "carsoup";
	private static Logger log = Logger.getLogger(Carsoup.class);
	
	@Override
	public String getProviderCode()
	{
		return CODE;
	}
	
	@Override
	public String getMakeUrl()
	{
		return "http://imageserver.carsoup.com/includes/VehMfgList.js";
	}
	@Override
	public String getModelsUrl() 
	{
		return "http://imageserver.carsoup.com/includes/VehModelList.js";
	}

	private String searchurl = "http://www.carsoup.com/used/summary.asp?makeid=%make%&" +
								"modelid=%model%&minyear=%year%&maxyear=%year%" +
								"&invtypeid=2&certified=0&radius=%distance%&zipcode=%zip%&SortCol=Distance&" +
								"sort=Asc&zipCodeSearch=true&7Days=&WithPrices=&WithPhotos=&AllMarkets=&minPrice=" +
								"&maxPrice=&minMileage=&maxMileage=&dealerID=&rDealerGroupID=&vehicleTypeID=1" +
								"&bodyStyleTypeID=&thrifties=0&WithHandicap=&BL=&&btnZip.x=16&btnZip.y=0";
	@Override
	public String getSearchUrl()
	{
		return  searchurl;
	}

	@Override
	public List<MakeCode> parseMakes(String page)
	{
		log.info("Parsing Make Codes for Carsoup");
		
		//sets pattern to any entry with an array indexer
		Pattern p = Pattern.compile("m_avMfg\\[\\d+\\]\\s+=\\s+\"([^#]+)#([^\"]+)\";");

		//setup return list
		List<MakeCode> codes = new ArrayList<MakeCode>();

		Matcher match = p.matcher(page);
		while (match.find())
		{
			//split each make#id
			MakeCode code = new MakeCode();
			code.setName(match.group(1));
			code.setValue(match.group(2));
			codes.add(code);
		}
		
		return codes;
	}
	@Override
	public List<ModelCode> parseModels(String page, MakeCode make)
	{
		log.info("Parsing Model Codes for Carsoup");
		
		//sets pattern to any entry with an array indexer
		Pattern p = Pattern.compile("m_avModel\\[" + make.getValue() + "\\]\\s+=\\s+'([^']+)';");

		//setup return list
		List<ModelCode> codes = new ArrayList<ModelCode>();

		Matcher match = p.matcher(page);
		if (match.find())
		{
			for(String s : match.group(1).split(","))
			{
				//split each make#id
				ModelCode code = new ModelCode();
				code.setName(s.split("#")[0]);
				code.setValue(s.split("#")[1]);
				code.setMake(make);
				codes.add(code);
			}
		}
		log.info("Parsed " + codes.size() + " model codes");

		return codes;
	}
	
	private String PostPage(String url, String PostData)
	{
		HttpClient client = new HttpClient();
		 
		client.getParams().setBooleanParameter( HttpClientParams.ALLOW_CIRCULAR_REDIRECTS, true);
		PostMethod method = new PostMethod(url);
	    method.setRequestHeader("Cookie", "MarketID=10; domain=carsoup.com; path=/");

		for(String s : PostData.split("&"))
		{
			if(s.indexOf("=") > -1)
			{
				if(s.split("=").length == 1)
					method.addParameter(s.split("=")[0], "");
				else
					method.addParameter(s.split("=")[0], s.split("=")[1]);

			}
		}
        
	    BufferedReader br = null;

	    try
	    {
	    	int returnCode = client.executeMethod(method);

			if(returnCode == HttpStatus.SC_NOT_IMPLEMENTED) 
			{
				System.err.println("The Post method is not implemented by this URI");
				// still consume the response body
				method.getResponseBodyAsString();
			} 
			else 
			{
				br = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
				StringBuffer sb = new StringBuffer();
				String readLine;
				while(((readLine = br.readLine()) != null)) 
				{
					sb.append(readLine);
				}
				return sb.toString();
			}
	    } 
	    catch (Exception e) 
	    {
	    	System.err.println(e);
	    } 
	    finally 
	    {
	    	method.releaseConnection();
	    	if(br != null) try { br.close(); } catch (Exception fe) {}
	    }
	    return "";
	}
	private String GetPostUrl()
	{
		return substitutedUrl.substring(0, substitutedUrl.indexOf("invtypeid"));
	}
	private String GetPostData()
	{
		return substitutedUrl.substring(substitutedUrl.indexOf("invtypeid"));
	}
	
	@Override
	Prices parseCars(String page)
	{
		//Search must be posted with 'SearchId' and 'ResortId'
		//variables to enforce searching against zip code restrictions
		
		Pattern regex;
		Matcher match;

		
		page = PostPage(GetPostUrl(), "");
		
		
		regex = Pattern.compile("<input type=hidden name=SearchID value=(.*) />");
        match = regex.matcher(page);
        match.find();
        String searchid = match.group(1);
        searchid = searchid.substring(0, searchid.indexOf(" />"));
        
        regex = Pattern.compile("<input type=hidden name=ResortID value=(.*) />");
        match = regex.matcher(page);
        match.find();
        String resortid = match.group(1);
        resortid = resortid.substring(0, resortid.indexOf(" />"));

        page = PostPage(GetPostUrl(), GetPostData() + "&SearchID=" + searchid + "&ResortID=" + resortid);
        

        String f = "\" onclick=\"return PostCarSoupForm('" + searchurl + "&SearchID=" + searchid + "&ResortID=" + resortid + "'); return false;";
        searchurl = f;
        
        
        log.info("Parsing Cars for Carsoup");
		
		Prices prices = new Prices();
		List<Price> priceList = new ArrayList<Price>();
		
		int num = 0;
        if(page.indexOf("Your search found no vehicles") > -1)
        {
        	prices.setMatches(0);
        }
        else
        {
    		regex = Pattern.compile("<b>(\\d+) vehicles match your criteria</b>");
    		match = regex.matcher(page);
    		match.find();
            num = Integer.parseInt(match.group(1));
            prices.setMatches(num);
        }

        regex = Pattern.compile("class=usedBody>");
        String[] g = regex.split(page);
        List<String> veh = new ArrayList<String>();
        for(int i = 1; i < g.length - 1; i++)
        {
        	veh.add(g[i]);
        }

        String[] vehicles = new String[veh.size()];
        vehicles = veh.toArray(vehicles);
        
        for (int i = 0; i < num; i++)
        {
            try
            {
            	Price price = new Price();
    			
                regex = Pattern.compile(">(.*)</a>");
                match = regex.matcher(vehicles[0 + (i * 6)]);
                if(match.find())
                	price.setModelText(match.group(1));

                regex = Pattern.compile("(\\d+)");
                match = regex.matcher(vehicles[1 + (i * 6)]);
                if(match.find())
                	price.setYear(Integer.parseInt(match.group(1)));
                
                regex = Pattern.compile("(\\d+)");
                match = regex.matcher(vehicles[3 + (i * 6)].replace(",", ""));
                if(match.find())
                	price.setMileage(Integer.parseInt(match.group(1)));

            	regex = Pattern.compile("(\\d+)");
                match = regex.matcher(vehicles[4 + (i * 6)].replace(",", "").replace("$", ""));
                if(match.find())
                {
                	if(Integer.parseInt(match.group(1)) > 0)
            		{
            			price.setPrice(Integer.parseInt(match.group(1)));
            		}
                }
                
                regex = Pattern.compile("(\\d+) mi.");
                match = regex.matcher(vehicles[5 + (i * 6)]);
                if(match.find())
                	price.setDistance(Integer.parseInt(match.group(1)));
	            
                if( price != null )
	            {
					priceList.add( price );
	            }
            }
            catch(Exception e)
            {
            	
            }
        }
    	prices.setPriceList( priceList );
		log.info("Parsed: " + priceList.size() + " Cars for Carsoup");
		return prices;
	}
	@Override
	Prices parsePrices(String page, Prices prices) throws Exception
	{
		log.info("Parsing Prices for Carsoup");
		prices.setLowest( prices.calculateLowest() );
		prices.setHighest( prices.calculateHighest() );
		prices.setAverage( prices.calculateAverage() );
		log.info("Parsed Prices for Carsoup");
		return prices;
	}
}
