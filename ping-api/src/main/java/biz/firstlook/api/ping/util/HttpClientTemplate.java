package biz.firstlook.api.ping.util;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;

public class HttpClientTemplate
{

private HttpClient httpClient;

private int socket_timeout = 10000;
private int connection_timeout = 20000;
private int max_connections = 2;
private int max_total_connections = 20;

public HttpClientTemplate()
{
    HttpConnectionManagerParams httpConnectionManagerParams = new HttpConnectionManagerParams();
    httpConnectionManagerParams.setDefaultMaxConnectionsPerHost( max_connections );
    httpConnectionManagerParams.setMaxTotalConnections( max_total_connections );
    httpConnectionManagerParams.setSoTimeout(socket_timeout);
    httpConnectionManagerParams.setConnectionTimeout(connection_timeout); 
    
	httpClient = new HttpClient( new MultiThreadedHttpConnectionManager() );
	httpClient.getHttpConnectionManager().setParams(httpConnectionManagerParams);
}

public <T> T executeMethodWithCallback( HttpMethod method, HttpClientCallback<T> callback ) throws HttpException
{
	try
	{
		int statusCode = httpClient.executeMethod( method );
		return callback.doInHttpClient( statusCode, method );
	}
	catch ( HttpException e )
	{
		throw new HttpException( e.getMessage(), e );
	}
	catch ( IOException e )
	{
		throw new HttpException( e.getMessage(), e );
	}
	finally
	{
		method.releaseConnection();
	}
}

}