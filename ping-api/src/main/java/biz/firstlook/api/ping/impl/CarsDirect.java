package biz.firstlook.api.ping.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import biz.firstlook.api.ping.Prices;
import biz.firstlook.ping.model.MakeCode;
import biz.firstlook.ping.model.ModelCode;
import biz.firstlook.ping.model.Price;

public class CarsDirect extends PriceProvider {

	public static final String CODE = "carsDirect";
	private static Logger log = Logger.getLogger(CarsDirect.class);

	@Override
	public String getProviderCode()
	{
		return CODE;
	}
	
	@Override
	public String getMakeUrl() {
		return "http://www.carsdirect.com/used_cars/advanced_search";
	}

	@Override
	public String getModelsUrl() {
		return "http://www.carsdirect.com/used_cars/advanced_search";
	}

	@Override
	public String getSearchUrl() {
//		return "http://www.carsdirect.com/used_cars/listings?natser=true&used_model=&model_group_id=%model%&used_make=%makeName%&model_select=%model%&price_range1=0&price_range2=999999&start_year=%year%&end_year=%year%&zipcode=%zip%&radius=%distance%&cpo_only=null";
		return "http://www.carsdirect.com/used_cars/listings?natser=true&used_model=%model%&model_group_id=&used_make=%make%&model_select=%model%&price_range1=0&price_range2=999999&start_year=%year%&end_year=%year%&zipcode=%zip%&radius=%distance%&cpo_only=null";
	}

	@Override
	List<MakeCode> parseMakes(String page) {

		log.info("Parsing Make Codes for Cars Direct");
		List<MakeCode> codes = new ArrayList<MakeCode>();

	    Pattern re = Pattern.compile("var mm = \\[ 0,(.*) \\];");
	    Matcher match = re.matcher( page );
	    match.find();
	    String data = match.group(1);

	    data = data.substring(0, 10) + "X" + data.substring(10);
	    String[] elements = data.split(",(0|1),0,");

	    for( String element : elements ) {

	    	if( element.length() != 1 ) {
	    		String[] tokens = element.split(",");
	    		String make = tokens[0].replace("'", "" );
	    		String makeId = tokens[1];
	    		MakeCode code = new MakeCode( makeId, make );
	    		codes.add( code );    		
	    	}
	    }
		log.info("Parsed " + codes.size() + " make codes");

		return codes;
	}

	@Override
	List<ModelCode> parseModels(String page, MakeCode make) {
		log.info("Parsing Model Codes for Direct");
		List<ModelCode> codes = new ArrayList<ModelCode>();

	    Pattern re = Pattern.compile("var mm = \\[ 0,(.*) \\];");
	    Matcher match = re.matcher( page );
	    match.find();
	    String data = match.group(1);

	    data = data.substring(0, 10) + "X" + data.substring(10);
	    String[] elements = data.split(",(0|1),0,");

	    for( String element : elements ) {

	    	if( element.length() != 1 ) {
	    		String[] tokens = element.split(",");
	    		String makeId = tokens[1];
	    		if( makeId.equals( make.getValue() ) ) {
	    			String[] modelTokens = Arrays.asList( tokens ).subList( 2, tokens.length ).toArray(new String[0]);
	    			for( int i = 0; i < modelTokens.length; i++ ) {
	    				if( i % 4 == 2 ) {
	    					codes.add( new ModelCode( modelTokens[i+1].replace("'", ""), modelTokens[i].replace("'", ""), make ) );
	    				}
	    			}
	    		}
	    	}
	    }
		log.info("Parsed " + codes.size() + " model codes");

		return codes;
	}

	@Override
	Prices parseCars(String page) {
		Prices prices = new Prices();
		List<Price> priceList = new ArrayList<Price>();
		log.info("Parsing Cars for Cars Direct");

		Pattern re = Pattern.compile( "(\\d{1,3})-(\\d{1,3})</b> of <b>(\\d{1,4})");
		Matcher match = re.matcher(page);
		match.find();

		String[] elements = page.split("class=\"year\"");
		
		for( String element : elements ) {
			re = Pattern.compile("rowspan=\"2\">(\\d{4})</td>");
			match = re.matcher( element );
			Price price = null;
			if( match.find() ) {
				price = new Price();
				price.setYear( Integer.parseInt( match.group(1) ) );
			}

			re = Pattern.compile("<b>([\\w\\s\\d-]*)</b></a>");
			match = re.matcher( element );
			if( match.find() ) {
				price.setModelText( match.group(1) );
			}
			
			re = Pattern.compile("class=\"mi\">(\\d{1,3},\\d{1,3})</td>");
			match = re.matcher( element );
			if( match.find() ) {
				price.setMileage( Integer.parseInt( match.group(1).replace(",","") ) );
			}

			re = Pattern.compile("class=\"price\"> \\$(\\d{1,3},\\d{1,3}) </td>");
			match = re.matcher( element );
			if( match.find() ) {
				price.setPrice( Integer.parseInt( match.group(1).replace(",","") ) );
			}

			re = Pattern.compile("class=\"px10\">(\\d{1,3}) Mile");
			match = re.matcher( element );
			if( match.find() ) {
				price.setDistance( Integer.parseInt( match.group(1).replace(",","") ) );
			}

			if( price != null ) {
				priceList.add( price );
			}
		}
		prices.setPriceList( priceList );
		log.info("Parsed: " + priceList.size() + " Cars for Cars Direct");
		return prices;
	}

	@Override
	Prices parsePrices(String page, Prices prices) throws Exception {

		Pattern re = Pattern.compile( "(\\d{1,3})-(\\d{1,3})</b> of <b>(\\d{1,4})");
		Matcher match = re.matcher(page);
		match.find();
		String matches = match.group(3);
		
		prices.setMatches( Integer.parseInt(matches) );
		
		log.info("Parsing Prices for CarsDirect");
		prices.setLowest( prices.calculateLowest() );
		prices.setHighest( prices.calculateHighest() );
		prices.setAverage( prices.calculateAverage() );

		log.info("Parsed Prices for Cars Direct");

		return prices;
	}

}
