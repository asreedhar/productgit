package biz.firstlook.api.ping;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import biz.firstlook.api.ping.impl.Autotrader;
import biz.firstlook.api.ping.impl.CarsDirect;
import biz.firstlook.api.ping.impl.CarsDotCom;
import biz.firstlook.api.ping.impl.Carsoup;
import biz.firstlook.api.ping.impl.PriceProvider;
import biz.firstlook.ping.model.MakeCode;
import biz.firstlook.ping.model.MakeModelGrouping;
import biz.firstlook.ping.model.ModelCode;
import biz.firstlook.ping.model.PingSystem;
import biz.firstlook.ping.model.Provider;
import biz.firstlook.ping.persist.MakeCodeDao;
import biz.firstlook.ping.persist.MakeModelGroupingDao;
import biz.firstlook.ping.persist.ModelCodeDao;
import biz.firstlook.ping.persist.PingSystemDao;
import biz.firstlook.ping.persist.ProviderDao;

public class PingService {
	
	private static Logger log = Logger.getLogger(PingService.class);

	private PingSystemDao pingSystemDao;
	private ProviderDao providerDao;
	private MakeCodeDao makeCodeDao;
	private ModelCodeDao modelCodeDao;
	private MakeModelGroupingDao makeModelGroupingDao;
	
	private Map<Provider,PriceProvider> priceProviders = new HashMap<Provider,PriceProvider>();
	
	public PingService() {}
	
	public void initialize() {
		log.info( "Initializing Ping Service" );
		List<Provider> providers = providerDao.findAll();
		
		log.info( "Creating Price Providers" );
		PriceProvider atPP = new Autotrader();
		PriceProvider cdcPP = new CarsDotCom();
		PriceProvider cdPP = new CarsDirect();
		PriceProvider csPP = new Carsoup();
		
		Map<String, PriceProvider> priceProviderCodeMapping = new HashMap<String, PriceProvider>();
		priceProviderCodeMapping.put(atPP.getProviderCode(), atPP);
		priceProviderCodeMapping.put(cdcPP.getProviderCode(), cdcPP);
		priceProviderCodeMapping.put(cdPP.getProviderCode(), cdPP);
		priceProviderCodeMapping.put(csPP.getProviderCode(), csPP);
		
		if(providers != null) {
			for(Provider provider : providers)
			{
				String providerCode = provider.getCode(); 
				if( providerCode != null )
				{
					PriceProvider pp = priceProviderCodeMapping.get(providerCode);
					if(pp != null)
					{
						priceProviders.put(provider, pp);
					}
				}
			}
		}
	}

	public boolean isPingActive() {
		PingSystem system = pingSystemDao.findAll().get(0);
		log.debug( "Is PING Active? " + system.getActive() );
		return system.getActive();		
	}
	
	public List<Provider> getAvailableProviders() {
		List<Provider> providers = providerDao.findAvailable();
		log.debug( "Providers: " + ArrayUtils.toString( providers.toArray() ) );
		return providers;
	}
	
	public Provider getProvider(String code) {
		if(code == null)
			throw new NullPointerException("Expected a PriceProvider code");
		
		List<Provider> providers = new ArrayList<Provider>(priceProviders.keySet());
		for(Provider p : providers)
		{
			if(code.equalsIgnoreCase(p.getCode())) {
				return p;
			}
		}
		
		//new provider since initialization
		Provider p = providerDao.findByCode( code.toLowerCase() );
		if(p != null)
			log.warn("Provider added after initialization.  The PriceProvider for this Provider is not available.");
		
		return p;
	}
	
	public PriceProvider getPriceProvider(String code) {
		if(code == null)
			throw new NullPointerException("Expected a PriceProvder code");

		Provider p = getProvider( code );
		PriceProvider pp = null;
		if( p != null ) {
			pp = priceProviders.get( p );
			if( pp == null ) {
				log.error(MessageFormat.format("Looking for provider {0}, but the PriceProvider is not implemented.", p.getName()));
			} else if (pp.getProviderCode().equals(Carsoup.CODE)) {
				pp = new Carsoup(); //Carsoup searchUrl is stateful!
				priceProviders.put(p, pp);
			}
		}
		return pp;
	}
	
	public ProviderDao getProviderDao() {
		return providerDao;
	}

	public void setProviderDao(ProviderDao providerDao) {
		this.providerDao = providerDao;
	}

	public PingSystemDao getPingSystemDao() {
		return pingSystemDao;
	}

	public void setPingSystemDao(PingSystemDao pingSystemDao) {
		this.pingSystemDao = pingSystemDao;
	}

	public void resetProviderCodes(Provider provider) {
		log.info( "Deleting all model and make codes for provider: " + provider );
		modelCodeDao.deleteByProvider( provider );
		makeCodeDao.deleteByProvider( provider );
	}

	public MakeCodeDao getMakeCodeDao() {
		return makeCodeDao;
	}

	public void setMakeCodeDao(MakeCodeDao makeCodeDao) {
		this.makeCodeDao = makeCodeDao;
	}

	public ModelCodeDao getModelCodeDao() {
		return modelCodeDao;
	}

	public void setModelCodeDao(ModelCodeDao modelCodeDao) {
		this.modelCodeDao = modelCodeDao;
	}

	public void storeCodes(List<MakeCode> makes) {
		log.info( "Storing " + makes.size() + " make codes");
		for( MakeCode make : makes ) {
			log.debug( "Persisting Make Code: " + make );
			makeCodeDao.makePersistent( make );
		}
		
	}

	public List<ModelCode> getModelCodes(Provider provider) {
		return modelCodeDao.findByProvider( provider );
	}

	@Transactional
	public void mapModelCode(ModelCode model) {
		model = modelCodeDao.findById( model.getId() );
		log.info( "Mapping Model Code: " + model );
		List<MakeModelGrouping> mmgs = makeModelGroupingDao.findByMakeModel( model.getMake().getName(), model.getName());
		if( mmgs == null || mmgs.size() == 0 ) {
			log.info( "No Mapping found for Make: " + model.getMake().getName() + ", Model: " + model.getName() );
		} else {
			log.info( "MMG Matches Found" );
			for( MakeModelGrouping mmg : mmgs ) {
				log.info( "Adding MMG: " + mmg );
				model.getGroupings().add( mmg );
			}
			log.info( "Persisting model Code" );
			modelCodeDao.makePersistent( model );
		}
		
	}

	public MakeModelGroupingDao getMakeModelGroupingDao() {
		return makeModelGroupingDao;
	}

	public void setMakeModelGroupingDao(MakeModelGroupingDao makeModelGroupingDao) {
		this.makeModelGroupingDao = makeModelGroupingDao;
	}

	public MakeModelGrouping getMakeModelGrouping(Integer makeModelGroupingId) {
		return makeModelGroupingDao.findById( makeModelGroupingId );
	}

	public ModelCode getModelCode(MakeModelGrouping makeModelGrouping, Provider provider) {
		return modelCodeDao.findByMakeModelGroupingProvider( makeModelGrouping, provider );
	}
}
