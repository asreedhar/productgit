package biz.firstlook.api.ping.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.Logger;

import biz.firstlook.ping.model.Price;

public class CarsDirectHttpSucker
{

private String make;
private String model;
private String zip;
private String distance;

private static Logger log = Logger.getLogger(CarsDirectHttpSucker.class);
private Integer totalNumberOfListings = -1;
private Integer currentPageNumber = -1;
private Integer firstDisplayedListing = -1;
private Integer lastDisplayedListing = -1;
private Integer numberOfListingsPerPage = -1;
private Integer numberOfPages = -1;
private Integer searchGroupId = -1;

	
public CarsDirectHttpSucker(String make, String model, String zip, String distance) {
	this.make = make;
	this.model = model;
	this.zip = zip;
	this.distance = distance;
}

public String getNextStream() {

	String id = (Math.floor(Math.random() * 10001) + "_" + new Date().getTime()).toString();
	
	String content = "";
	
	HttpClientTemplate template = new HttpClientTemplate();
	
	//Generate dwr callID, taken from engine.js in dwr.jar
//	double random = Math.floor(Math.random() * 10001);
//	String id = (random + "_" + new Date().getTime()).toString();

	HttpMethod method = new GetMethod();
	method.setPath( "http://www.carsdirect.com/used_cars/dwr/exec/UsedCarSearchManager.nextPage.dwr" );
	Set<NameValuePair> queryParams = new HashSet< NameValuePair >();
	queryParams.add( new NameValuePair( "callCount", "1" ) );
	queryParams.add( new NameValuePair( "c0-scriptName", "UsedCarSearchManager" ) );
	queryParams.add( new NameValuePair( "c0-methodName", "nextPage" ) );
	queryParams.add( new NameValuePair( "c0-id", id ) );

	
	queryParams.add( new NameValuePair( "c0-e1", "null:null" ) );
	queryParams.add( new NameValuePair( "c0-e2", "Object:{ascendingSort:reference:c0-e3, currentPageNumber:reference:c0-e4, firstDisplayedListing:reference:c0-e5, lastDisplayedListing:reference:c0-e6, numberOfListingsPerPage:reference:c0-e7, numberOfPages:reference:c0-e8, showFirstLink:reference:c0-e9, showLastLink:reference:c0-e10, showNextLink:reference:c0-e11, showPreviousLink:reference:c0-e12, sortedColumn:reference:c0-e13, totalNumberOfListings:reference:c0-e14}" ) );
	queryParams.add( new NameValuePair( "c0-e3", "boolean:false" ) );
	queryParams.add( new NameValuePair( "c0-e4", "number:" + currentPageNumber ) );
	queryParams.add( new NameValuePair( "c0-e5", "number:" + firstDisplayedListing ) );  //
	queryParams.add( new NameValuePair( "c0-e6", "number:" + lastDisplayedListing ) );
	queryParams.add( new NameValuePair( "c0-e7", "number:" + numberOfListingsPerPage ) );
	queryParams.add( new NameValuePair( "c0-e8", "number:" + numberOfPages ) );
	queryParams.add( new NameValuePair( "c0-e9", "boolean:false" ) );
	queryParams.add( new NameValuePair( "c0-e10", "boolean:true") );
	queryParams.add( new NameValuePair( "c0-e11", "boolean:true") );
	queryParams.add( new NameValuePair( "c0-e12", "boolean:false") );
	queryParams.add( new NameValuePair( "c0-e13", "string:default" ) );
	queryParams.add( new NameValuePair( "c0-e14", "number:" + totalNumberOfListings ) );
	queryParams.add( new NameValuePair( "c0-e15", "Object:{city:reference:c0-e16, debuggingString:reference:c0-e17, latitude:reference:c0-e18, longitude:reference:c0-e19, state:reference:c0-e20, streetAddress:reference:c0-e21, zipcode:reference:c0-e22}") );
	queryParams.add( new NameValuePair( "c0-e16", "null:null" ) );
	queryParams.add( new NameValuePair( "c0-e17", "string:%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%0ATargeted%20To%3A%20ZIPCODE%0ATargeted%20From%3A%20ZIP_POSITION%0A-----------------%0AAddress%3A%20null%0ACity%3A%20null%0AState%3A%20null%0AZipcode%3A%2060607%0A-----------------%0ALatitude%3A%2041.8759%0ALongitude%3A%20-87.6511%0A%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D%3D" ) );
	queryParams.add( new NameValuePair( "c0-e18", "number:41.8759" ) ); // latitude
	queryParams.add( new NameValuePair( "c0-e19", "number:-87.6511" ) ); // longitude
	queryParams.add( new NameValuePair( "c0-e20", "null:null" ) );
	queryParams.add( new NameValuePair( "c0-e21", "null:null" ) );
	queryParams.add( new NameValuePair( "c0-e22", "string:" + zip ) );
	queryParams.add( new NameValuePair( "c0-e23", "null:null") );
	queryParams.add( new NameValuePair( "c0-e24", "string:" + make ) );
	queryParams.add( new NameValuePair( "c0-e25", "string:" + model ) );
	queryParams.add( new NameValuePair( "c0-e26", "number:" + searchGroupId ) );//l???
	queryParams.add( new NameValuePair( "c0-e27", "number:" + distance ) );
	queryParams.add( new NameValuePair( "c0-e28", "string:Make" ) );
	queryParams.add( new NameValuePair( "c0-e29", "null:null" ) );
	queryParams.add( new NameValuePair( "c0-e30", "string:" + model + "%604%60269%600%600%60false%7CTrusted%20Dealer%607%600%600%600%60false%7CPrivate%20Party%607%601%600%600%60false%7CDealer%607%602%600%600%60false%7C" ) );
	queryParams.add( new NameValuePair( "c0-e31", "string:" + zip ) );
	queryParams.add( new NameValuePair( "c0-e32", "string:" + model + "%604%60269%600%600%60false%7CTrusted%20Dealer%607%600%600%600%60false%7CPrivate%20Party%607%601%600%600%60false%7CDealer%607%602%600%600%60false%7C" ) );
	queryParams.add( new NameValuePair( "c0-e33", "string:" + zip ) );
	queryParams.add( new NameValuePair( "c0-e34", "string:Search%20Results%3A%20" + make + "%20" + model + "%20Used%20Cars%20in%2060607" ) );
	
	
	queryParams.add( new NameValuePair( "c0-param0", "Object:{dealerId:reference:c0-e1, listingLogicInfo:reference:c0-e2, locationInfo:reference:c0-e15, log:reference:c0-e23, make:reference:c0-e24, model:reference:c0-e25, searchGroupId:reference:c0-e26, searchRadius:reference:c0-e27, searchType:reference:c0-e28, sellerId:reference:c0-e29, verticalSearchQuery:reference:c0-e30, zipcode:reference:c0-e31, queryString:reference:c0-e32, zip:reference:c0-e33, tagLine:reference:c0-e34}" ) );
	
	queryParams.add( new NameValuePair( "xml", "true" ) );
	method.setQueryString( queryParams.toArray( new NameValuePair[queryParams.size()] ) );
	
	content = makeRequest(content, template, method);
	return content;
}

public String getInitalStream() {
	String id = (Math.floor(Math.random() * 10001) + "_" + new Date().getTime()).toString();

	
	String content = "";
	
	HttpClientTemplate template = new HttpClientTemplate();
	
	//Generate dwr callID, taken from engine.js in dwr.jar


	HttpMethod method = new GetMethod();
	method.setPath( "http://www.carsdirect.com/used_cars/dwr/exec/UsedCarSearchManager.loadListingsWithModel.dwr" );
	Set<NameValuePair> queryParams = new HashSet< NameValuePair >();
	queryParams.add( new NameValuePair( "callCount", "1" ) );
	queryParams.add( new NameValuePair( "c0-scriptName", "UsedCarSearchManager" ) );
	queryParams.add( new NameValuePair( "c0-methodName", "loadListingsWithModel" ) );
	queryParams.add( new NameValuePair( "c0-id", id ) );
	queryParams.add( new NameValuePair( "c0-param0", "string:" + make ) );
	queryParams.add( new NameValuePair( "c0-param1", "string:" + model ) );
	queryParams.add( new NameValuePair( "c0-param2", "number:" + zip ) );
	queryParams.add( new NameValuePair( "c0-param3", "number:" + distance ) );
	queryParams.add( new NameValuePair( "xml", "true" ) );
	method.setQueryString( queryParams.toArray( new NameValuePair[queryParams.size()] ) );
	
	content = makeRequest(content, template, method);
	return content;
}

private String makeRequest(String content, HttpClientTemplate template, HttpMethod method) {
	try
	{
		content = template.executeMethodWithCallback( method, new HttpClientCallback< String >()
		{

			public String doInHttpClient( int statusCode, HttpMethod method ) throws HttpException
			{
				BufferedReader stream = null;
				try
				{
					stream = new BufferedReader( new InputStreamReader( method.getResponseBodyAsStream() ) );
							
					// The rest of the response is content.
					String textLine = null;
					StringBuffer sb = new StringBuffer();
					while ( (textLine = stream.readLine()) != null )
					{
						sb.append( textLine.trim() );
					}
					
					return sb.toString();
				}
				catch ( IOException e )
				{
					throw new HttpException( "Error Trying to read data: ", e );
				}
				finally
				{
					try
					{
						stream.close();
					}
					catch ( IOException e )
					{
						e.printStackTrace();
					}
				}
			}
		} );
		
	}
	catch ( HttpException e )
	{
		e.printStackTrace();
	}
	return content;
}


public void doStuff() {
	String stream = "";
	for (int i = 0; i < 2; i++) {
		if (i == 0) {
			stream = getInitalStream(); 
		} else {
			stream = getNextStream();
		}
		System.out.println(stream);
		
		// Get top level vars ('var s27=My Shirona;')
		HashMap<Integer, String> variables = new HashMap<Integer, String>();
		
		// Top level var that encapsulated a car ('var s27={};')
		ArrayList<Integer> carVars = new ArrayList<Integer>();
		
		// Lines which define values for Top Level Cars above
		// In Dot notation ('s43.dateAddedToFavorites=s46') Always referebce a topLevel Var
		ArrayList<String> detailLines = new ArrayList<String>();
		
		String[] vars = stream.split(";");
		for (String line : vars) {
//		line = line.replaceAll("\"", "");
			Pattern re = Pattern.compile("var s(\\d*)=(\")?(.*\\s*.*)(\")?", Pattern.MULTILINE);
			Matcher match = re.matcher(line);
			if (match.find()) {
				Integer var = new Integer(match.group(1));
				String value = match.group(3);
				variables.put(var, value);
				if (value.equals("{}")) {
					carVars.add(var);
				}
//				System.out.println("Var : " + var + " with value: " + value);
			} else {
				detailLines.add(line);
			}	
		}
		
		// Bag up details into group
		HashMap<Integer, HashMap<String, String>> carDetails = new HashMap<Integer, HashMap<String,String>>();
		for (Integer carIndex : carVars) {
			HashMap<String, String> detail = new HashMap<String, String>();
			
			for (String line : detailLines) {
				// E.G. : line = 's43.dateAddedToFavorites=s46'
				String[] data = line.split("\\.");
				if (data.length < 2 || (line.indexOf("DWREngine") != -1)) continue;  // bad data (E.g. s[12]=s34;)
				
				if (i == 0) {
					if (locateAndLoadPageDriverVariables(variables, line)) {
						continue;
					}
				}
				
				Integer detailCarIndex = new Integer(data[0].substring(1, data[0].length()));  // strip s
				
				if (detailCarIndex.intValue() == carIndex.intValue()) { // verify
					String[] detailValue = data[1].split("=");
					if (detailValue[0].equals("mileage")) {
						String value = variables.get(new Integer(detailValue[1].substring(1, detailValue[1].length())));
						detail.put("mileage", value.replaceAll("\"", ""));
					} else if (detailValue[0].equals("year")) {
						String value = variables.get(new Integer(detailValue[1].substring(1, detailValue[1].length())));
						detail.put("year", value.replaceAll("\"", ""));
					} else if (detailValue[0].equals("model")) {
						String value = variables.get(new Integer(detailValue[1].substring(1, detailValue[1].length())));
						detail.put("model", value.replaceAll("\"", ""));
					} else if (detailValue[0].equals("price")) {
						String value = variables.get(new Integer(detailValue[1].substring(1, detailValue[1].length())));
						detail.put("price", value.replaceAll("\"", ""));
					} else if (detailValue[0].equals("distanceFromDealer")) {
						String value = variables.get(new Integer(detailValue[1].substring(1, detailValue[1].length())));
						detail.put("distanceFromDealer", value.replaceAll("\"", ""));
					} else if (detailValue[0].equals("listingId")) {
						String value = variables.get(new Integer(detailValue[1].substring(1, detailValue[1].length())));
						System.out.println(value);
					}
				}
			}
			if (detail.size() > 0) {
				carDetails.put(carIndex, detail);
			}
		}
		
		List<Price> priceList = new ArrayList<Price>();
		for (HashMap detailMap : carDetails.values()) {
			Price price = new Price();
			price.setDistance(getStringValueAsInteger((String)detailMap.get("distanceFromDealer")));
			price.setMileage(getStringValueAsInteger((String)detailMap.get("mileage")));
			price.setYear(getStringValueAsInteger((String)detailMap.get("year")));
			price.setModelText((String)detailMap.get("model"));
			price.setPrice(getStringValueAsInteger((String)detailMap.get("price")));
			priceList.add(price);
		}
		
	}

}

/**
 * 
 * E.g. : 	
 * 			dataline = s43.dateAddedToFavorites=s46
 * 			data[1] = [dateAddedToFavorites=s46]
 * 			data[1].split('=') = [ dateAddedToFavorites, s46 ] = importantVariable
 * 
 */
private boolean locateAndLoadPageDriverVariables(HashMap<Integer, String> variables, String dataline) {
	boolean importantVarFound = false;
	String[] data = dataline.split("\\.");
	String[] importantVariable = data[1].split("=");
	if (data[1].indexOf("totalNumberOfListings") != -1) {
		int oldVal = totalNumberOfListings.intValue();
		totalNumberOfListings = getImportantValue(variables, importantVariable);
		if (oldVal != -1 && totalNumberOfListings.intValue() != oldVal) {
			log.error("CarsDirect is changing the values for the Page Driver Vars!!!");
		}
		importantVarFound = true;
	} else if (data[1].indexOf("currentPageNumber") != -1) {
		int oldVal = currentPageNumber.intValue();
		currentPageNumber = getImportantValue(variables, importantVariable);
		if (oldVal != -1 && currentPageNumber.intValue() != oldVal) {
			log.error("CarsDirect is changing the values for the Page Driver Vars!!!");
		}
		importantVarFound = true;
	}else if (data[1].indexOf("firstDisplayedListing") != -1) {
		firstDisplayedListing = getImportantValue(variables, importantVariable);
		importantVarFound = true;
	}else if (data[1].indexOf("lastDisplayedListing") != -1) {
		lastDisplayedListing = getImportantValue(variables, importantVariable);
		importantVarFound = true;
	}else if (data[1].indexOf("numberOfListingsPerPage") != -1) {
		numberOfListingsPerPage = getImportantValue(variables, importantVariable);
		importantVarFound = true;
	}else if (data[1].indexOf("numberOfPages") != -1) {
		numberOfPages = getImportantValue(variables, importantVariable);
		importantVarFound = true;
	} else if (data[1].indexOf("searchGroupId") != -1) {
		searchGroupId = getImportantValue(variables, importantVariable);
		importantVarFound = true;
	}
	return importantVarFound;
}

private Integer getImportantValue(HashMap<Integer, String> variables, String[] importantVariable) {
	return new Integer(variables.get(new Integer(importantVariable[1].substring(1, importantVariable[1].length()))));
}



private Integer getStringValueAsInteger(String toParse) {
	Integer result = 0; 
	if (toParse != null) {
		Float asFloat = new Float(toParse);
		result = asFloat.intValue();
	} else {
//		System.err.println("Null value found");
	}
	return result;
}

/**
 * @param args
 */
public static void main( String[] args )
{
	String make = "Toyota";
	String model = "Camry";
	String zip = "60607";
	String distance = "75";
	
	CarsDirectHttpSucker sucker = new CarsDirectHttpSucker(make, model, zip, distance);
	sucker.doStuff();
	
}

}




























