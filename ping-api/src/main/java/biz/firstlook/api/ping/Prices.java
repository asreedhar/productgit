package biz.firstlook.api.ping;

import java.util.List;

import biz.firstlook.ping.model.Price;

public class Prices {
	
	private List<Price> priceList;
	private Integer lowest;
	private Integer highest;
	private Integer average;
	private Integer matches;
	private String url;
	
	public Prices() {}

	public List<Price> getPriceList() {
		return priceList;
	}

	public void setPriceList(List<Price> priceList) {
		this.priceList = priceList;
	}

	public Integer getAverage() {
		return average;
	}

	public void setAverage(Integer average) {
		this.average = average;
	}

	public Integer getHighest() {
		return highest;
	}

	public void setHighest(Integer highest) {
		this.highest = highest;
	}

	public Integer getLowest() {
		return lowest;
	}

	public void setLowest(Integer lowest) {
		this.lowest = lowest;
	}

	public Integer getMatches() {
		return matches;
	}

	public void setMatches(Integer matches) {
		this.matches = matches;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer calculateHighest() {
		Integer highest = -1;
		for( Price price : priceList ) {
			if( price.getPrice() != null && price.getPrice() > highest ) {
				highest = price.getPrice();
			}
		}
		return highest;		
	}
	
	public Integer calculateLowest() {
		Integer lowest = Integer.MAX_VALUE;
		for( Price price : priceList ) {
			if( price.getPrice() != null && price.getPrice() < lowest ) {
				lowest = price.getPrice();
			}
		}
		return lowest;		
	}
	
	public Integer calculateAverage() {
		Integer sum = 0;
		Integer count = 0;
		for( Price price : priceList ) {
			if( price.getPrice() != null ) {
				sum += price.getPrice();
				count++;
			}
		}
		Integer average;
		if (count == 0) {
			average= 0;
		} else {
			average= sum / count;
		}
		return average;		
	}

}
