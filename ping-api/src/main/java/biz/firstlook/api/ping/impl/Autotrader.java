package biz.firstlook.api.ping.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import biz.firstlook.api.ping.Prices;
import biz.firstlook.ping.model.MakeCode;
import biz.firstlook.ping.model.ModelCode;

public class Autotrader extends PriceProvider {

	/**
	 * This name should match the name in the database as a Provider
	 */
	public static final String CODE = "autotrader";
	
	private static Logger log = Logger.getLogger(Autotrader.class);
	
	@Override
	public String getProviderCode() {
		return CODE;
	}
	
	@Override
	public String getMakeUrl() {
		return "http://www.autotrader.com/fyc/index.jsp";
	}

	@Override
	public String getModelsUrl() {
		return "http://www.autotrader.com/fyc/index.jsp?make=%make%";
	}

	@Override
	public String getSearchUrl() {
		return "http://www.autotrader.com/fyc/searchresults.jsp?advanced=&num_records=100&certified=&isp=y&search=y&lang=en&search_type=used&make=%make%&model=%model%&start_year=%year%&end_year=%year%&min_price=&max_price=&distance=%distance%&address=%zip%&rdpage=100";
	}

	@Override
	public List<MakeCode> parseMakes(String page) {

		log.info("Parsing Make Codes for Autotrader");
		Pattern p = Pattern.compile("<select name=\"make\"");
		String[] strings = p.split(page);

		p = Pattern.compile("</select>");
		String options = p.split(strings[1])[0];

		Pattern option = Pattern
				.compile("<option value=\"(.*)\">(.*)</option>");
		Matcher matcher = option.matcher(options);
		List<MakeCode> codes = new ArrayList<MakeCode>();
		while (matcher.find()) {
			MakeCode code = new MakeCode();
			code.setName(matcher.group(2).trim());
			code.setValue(matcher.group(1).trim());
			codes.add(code);
		}
		log.info("Parsed " + codes.size() + " make codes");

		return codes;
	}

	@Override
	public List<ModelCode> parseModels(String page, MakeCode make) {
		log.info("Parsing Model Codes for Autotrader");
		Pattern p = Pattern.compile("<select name=\"model\"");
		String[] strings = p.split(page);

		p = Pattern.compile("</select>");
		String options = p.split(strings[1])[0];

		Pattern option = Pattern
				.compile("<option value=\"(.*)\">(.*)</option>");
		Matcher matcher = option.matcher(options);
		List<ModelCode> codes = new ArrayList<ModelCode>();
		while (matcher.find()) {
			ModelCode code = new ModelCode();
			code.setName(matcher.group(2).trim());
			code.setValue(matcher.group(1).trim());
			code.setMake(make);
			codes.add(code);
		}
		log.info("Parsed " + codes.size() + " model codes");

		return codes;
	}

	@Override
	Prices parseCars(String page) {
		log.info("Parsing Cars for Autotrader");
		Prices prices = new Prices();
		try {
			Pattern re = Pattern.compile("<p id=\"results\">We found(\\s)?(<a href=\"#listings-header\">)?<strong>(.*)</strong>(\\s)?used", Pattern.MULTILINE);
			Matcher match = re.matcher(page);
			if (match.find()) {
				log.debug("Found : " + match.group(3) + " matches");
				prices.setMatches(new Integer(match.group(3)));
			}
			re = Pattern.compile("<div id=\"price-range\"><p>\\$(.*)</p><p>\\$(.*)</p><p>\\$(.*)</p></div></div><div ", Pattern.MULTILINE);
			match = re.matcher(page);
			if (match.find()) {
				log.debug("High: " + match.group(1) + ", Low: " + match.group(2) + ", Avg: " + match.group(3));
				prices.setHighest(new Integer(match.group(1).replace(",", "")));
				prices.setLowest(new Integer(match.group(2).replace(",", "")));
				prices.setAverage(new Integer(match.group(3).replace(",", "")));
			}
		}catch (Exception e) {
			log.warn("Error parsing a price quote: " + getSubstitutedUrl(), e);
		}
		return prices;
	}

	@Override
	Prices parsePrices(String page, Prices prices) throws Exception {

		if (prices == null || prices.getMatches() == null || prices.getMatches() <= 0) {
			throw new Exception("No matches found for autotrader");
		}
		
		log.info("Parsed Prices for Autotrader");
		prices.setMatches(prices.getMatches());
		prices.setAverage( prices.getAverage() );
		prices.setHighest( prices.getHighest());
		prices.setLowest( prices.getLowest());

		return prices;
	}
}
