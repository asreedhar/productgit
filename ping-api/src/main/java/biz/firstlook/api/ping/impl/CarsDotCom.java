package biz.firstlook.api.ping.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import biz.firstlook.api.ping.Prices;
import biz.firstlook.ping.model.MakeCode;
import biz.firstlook.ping.model.ModelCode;
import biz.firstlook.ping.model.Price;

public class CarsDotCom extends PriceProvider {

	public static final String CODE = "carsDotCom";
	private static Logger log = Logger.getLogger(CarsDotCom.class);

	@Override
	public String getProviderCode()
	{
		return CODE;
	}
	
	@Override
	public String getMakeUrl() {
		return "http://www.cars.com/includes/js/makemodels-used.js";
	}

	@Override
	public String getModelsUrl() {
		return "http://www.cars.com/go/search/mmy.jsp?cid=&dlid=&dgid=&amid=&ms=&nf=&newFlag=N&skipYears=false&yearType=popular&initial=false&debug=&aff=national&vehicleType=&certifiedOnly=&mkid=%make%&mdid=&year=&zipcode=";
	}

	@Override
	public String getSearchUrl() {
		return "http://www.cars.com/go/search/search_results.jsp?ct=65&tracktype=usedcc&searchType=21&cid=&dlid=&dgid=&amid=&cname=&rd=&ddrd=30&zc=%zip%&makeid=%make%&modelid=%model%&pageNumber=&numResultsPerPage=50&largeNumResultsPerPage=0&sortorder=ascending&sortfield=DISTANCE%20ascending,PRICE%20descending&certifiedOnly=false&criteria=K-|E-|M-_%make%_|H-|D-_%model%_|N-N|R-%distance%|I-1,7|P-DISTANCE%20ascending,PRICE%20descending|Q-ascending|Y-_%year%_|X-popular|Z-%zip%&aff=national";
	}

	@Override
	List<MakeCode> parseMakes(String page) {

		log.info("Parsing Make Codes for CarsDotCom");
		List<MakeCode> codes = new ArrayList<MakeCode>();

		Pattern re = Pattern.compile("(D|K)\\((\\d*),\"(.*)\"\\)");

		String[] elements = page.split(";");
		for (String element : elements) {
			Matcher match = re.matcher(element);
			if (match.find()) {
				String type = match.group(1);
				String id = match.group(2);
				String name = match.group(3);

				if (type.equals("K")) {
					codes.add(new MakeCode(id, name));
				}
			}
		}

		log.info("Parsed " + codes.size() + " make codes");

		return codes;
	}

	@Override
	List<ModelCode> parseModels(String page, MakeCode make) {
		log.info("Parsing Model Codes for CarsDotCom");
		List<ModelCode> codes = new ArrayList<ModelCode>();

		Pattern re = Pattern.compile("md.options\\[0\\]=o\\;");
		Matcher match = re.matcher(page);
		match.find();
		page = page.substring(match.end());

		re = Pattern.compile("md.options\\[0\\]");
		match = re.matcher(page);
		match.find();
		page = page.substring(0, match.start());

		String[] elements = page.split("md.options");
		for (String element : elements) {
			re = Pattern
					.compile("o.value=\\'(\\d*)\\';o.text=\\'([\\w\\s-]*)\\';");
			match = re.matcher(element);
			if (match.find()) {
				String modelId = match.group(1);
				String name = match.group(2);
				ModelCode code = new ModelCode(modelId, name, make);
				codes.add(code);
			}
		}

		log.info("Parsed " + codes.size() + " model codes");

		return codes;
	}

	@Override
	Prices parseCars(String page)
	{
		Prices prices = new Prices();
		List<Price> priceList = new ArrayList<Price>();
		log.info("Parsing Cars for CarsDotCom");
		
        Pattern regex = Pattern.compile("<strong>(.*)</strong> of (.*) Vehicles", Pattern.MULTILINE);
        Matcher match = regex.matcher(page);
        match.find();
        prices.setMatches(Integer.parseInt(match.group(2).replace(",", "")));

        regex = Pattern.compile("<!-- Start of Individual Vehicle Module -->");
        String[] listings = regex.split(page);

        for(int i = 1; i < listings.length; i++)
        {
            regex = Pattern.compile("<div class=\"priceColumn columnSortHighlight\"><span >(.*)</span></div>", Pattern.MULTILINE);
            match = regex.matcher(listings[i]);
            if(match.find())
            {
	            try
	            {
	            	int p = Integer.parseInt(match.group(1).substring(0, match.group(1).indexOf("</span>")).replace(",", "").replace("$", ""));
	                Price price = new Price();
	            	price.setPrice(p);
	            	if (price != null)
	            	{
						priceList.add(price);
	            	}
	            }
	            catch(Exception e)
	            {}
	            finally
	            {}
            }
        }
        
		prices.setPriceList(priceList);

		log.info("Parsed: " + priceList.size() + " Cars for CarsDotCom");
		return prices;
	}

	@Override
	Prices parsePrices(String page, Prices prices) {

		log.info("Parsing Prices for CarsDotCom");
	    prices.setMatches( prices.getMatches() );
	    
		prices.setLowest(prices.calculateLowest());
		prices.setHighest(prices.calculateHighest());
		prices.setAverage(prices.calculateAverage());

		log.info("Parsed Prices for CarsDotCom");

		return prices;
	}

}
