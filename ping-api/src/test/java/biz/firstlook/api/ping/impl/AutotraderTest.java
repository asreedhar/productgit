package biz.firstlook.api.ping.impl;

import java.util.List;

import junit.framework.TestCase;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.BasicConfigurator;

import biz.firstlook.api.ping.Prices;
import biz.firstlook.ping.model.MakeCode;
import biz.firstlook.ping.model.ModelCode;


public class AutotraderTest extends TestCase {
	
	private MakeCode toyota;
	private ModelCode camry;
	
	@Override
	protected void setUp() throws Exception {
		BasicConfigurator.configure();
		toyota = new MakeCode( "TOYOTA", "Toyota" );
		camry = new ModelCode( "CAMRY", "Camry", toyota );
	}

	@Override
	protected void tearDown() throws Exception {
		BasicConfigurator.resetConfiguration();
	}

/*	public void testMakes() throws Exception {
		Autotrader at = new Autotrader();
		List<MakeCode> makeCodes = at.fetchMakes();
		assertNotNull( makeCodes );
		assertTrue( makeCodes.contains( toyota ) );
		
	}

	public void testModels() throws Exception {
		
		Autotrader at = new Autotrader();
		List<ModelCode> modelCodes = at.fetchModels( toyota );
		assertNotNull( modelCodes );
		assertTrue( modelCodes.contains( camry ) );
	}
	
	public void testSearch() throws Exception {
		Autotrader at = new Autotrader();
		Prices prices = at.getQuote( camry, 1999, 300, "60202" );
		assertNotNull( prices );
		
	} */
	
	public void testMakeParsing() throws Exception {
		String makeData = IOUtils.toString( getClass().getResourceAsStream("autotrader.make.data"));
		Autotrader at = new Autotrader();
		List<MakeCode> codes = at.parseMakes( makeData );
		assertNotNull( codes );
		assertTrue( codes.contains(toyota));
		
	}

	public void testModelParsing() throws Exception {
		String modelData = IOUtils.toString( getClass().getResourceAsStream("autotrader.model.data"));
		Autotrader at = new Autotrader();
		List<ModelCode> codes = at.parseModels( modelData, toyota );
		assertNotNull( codes );
		assertTrue( codes.contains(camry));
	}
	
	public void testCarsParsing() throws Exception {
		String priceData = IOUtils.toString( getClass().getResourceAsStream("autotrader.price.data"));
		Autotrader at = new Autotrader();
		Prices prices = at.parseCars( priceData );
		assertNotNull( prices );
		assertEquals( 12, prices.getMatches().intValue());
	}

/*	public void testPriceParsing() throws Exception {
		String priceData = IOUtils.toString( getClass().getResourceAsStream("autotrader.price.data"));
		Autotrader at = new Autotrader();
		Prices prices = at.parsePrices( priceData, new Prices() );
		assertNotNull( prices );
		assertEquals( new Integer( 11995 ), prices.getLowest() );
		assertEquals( new Integer( 19998 ), prices.getHighest() );
		assertEquals( new Integer( 16484 ), prices.getAverage() );
		assertEquals( new Integer( 105 ), prices.getMatches() );
	} */
}
