package biz.firstlook.api.ping;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import junit.framework.TestCase;
import biz.firstlook.api.ping.impl.Autotrader;
import biz.firstlook.api.ping.impl.CarsDirect;
import biz.firstlook.api.ping.impl.CarsDotCom;
import biz.firstlook.api.ping.impl.Carsoup;
import biz.firstlook.api.ping.impl.PriceProvider;
import biz.firstlook.ping.model.Provider;
import biz.firstlook.ping.persist.ProviderDao;


public class PingServiceTest extends TestCase {

	private PingService pingService = new PingService();
	private MockProviderDao providerDao = new MockProviderDao();
	
	public PingServiceTest() {
		super();
	}

	public PingServiceTest(String name) {
		super(name);
	}
	
	@Override
	protected void setUp() throws Exception {
		reset();
	}
	
	private void reset() {
		providerDao = new MockProviderDao();
		pingService = new PingService();
		pingService.setProviderDao(providerDao);
	}
	
	private Provider getProvider() {
		Provider p = new Provider();
		//purposely setting rediculous values.
		p.setId(Integer.MAX_VALUE);
		p.setAvailable(false);
		p.setCode(Autotrader.CODE);
		p.setName("Cars.com");
		p.setSortOrder(Integer.MIN_VALUE);
		return p;
	}

	/**
	 * Expecting no exceptions to be thrown on initialization.
	 */
	public void testInitialize() {
		providerDao.providers.add(new Provider());
		pingService.setProviderDao(providerDao);
		pingService.initialize();
		
		reset();
		providerDao.providers.add(getProvider());
		pingService.initialize();
		
		reset();
		providerDao.providers = null;
		pingService.initialize();
	}
	
	public void testGetProvider() {
		Provider expected = getProvider();
		providerDao.providers.add(expected);
		pingService.initialize();
		Provider actual = pingService.getProvider(Autotrader.CODE);
		assertEquals(expected, actual);
		
		try
		{
			actual = pingService.getProvider(null);
		} catch (NullPointerException e) {
			assertEquals("Expected a PriceProvider code", e.getMessage());
			return;
		}
		fail();
	}
	
	/**
	 * We expect for the listed PriceProviders that the instances retrieved from pingService are the same ones every time.
	 */
	public void testGetPriceProvider() {
		testGetPriceProviderHelper(Autotrader.CODE);
		testGetPriceProviderHelper(CarsDotCom.CODE);	
		testGetPriceProviderHelper(CarsDirect.CODE);
	}
	
	private void testGetPriceProviderHelper(String providerCode) {
		reset();
		Provider p = getProvider();
		p.setCode(providerCode);
		providerDao.providers.add(p);
		pingService.initialize();
		
		PriceProvider pp1 = pingService.getPriceProvider(providerCode);
		PriceProvider pp2 = pingService.getPriceProvider(providerCode);
		assertSame(pp1, pp2);
	}
	
	/**
	 * We expect to recieve new instances of the Carsoup PriceProvider on every getPriceProvider invocation using the Carsoup code.
	 */
	public void testGetCarsoupPriceProvider() {
		Provider p = getProvider();
		p.setCode(Carsoup.CODE);
		providerDao.providers.add(p);
		pingService.initialize();
		
		PriceProvider pp1 = pingService.getPriceProvider(Carsoup.CODE);
		PriceProvider pp2 = pingService.getPriceProvider(Carsoup.CODE);
		assertNotSame(pp1, pp2);
	}
	
	class MockProviderDao implements ProviderDao {
		
		List<Provider> providers = new ArrayList<Provider>(); 

		public List<Provider> findAvailable() {
			List<Provider> available = new ArrayList<Provider>();
			for(Provider p : providers) {
				if(p.getAvailable()) {
					available.add(p);
				}
			}
			return available;
		}

		public Provider findByCode(String code) {
			for(Provider p : providers) {
				if(code.equalsIgnoreCase(p.getCode())) {
					return p;
				}
			}
			return null;
		}

		public List<Provider> findAll() {
			return providers;
		}

		public List<Provider> findByExample(Provider exampleInstance,
				String... excludeProperty) {
			// TODO Auto-generated method stub
			return null;
		}

		public Provider findById(Integer id) {
			for(Provider p : providers) {
				if(id.equals(p.getId())) {
					return p;
				}
			}
			return null;
		}

		public Provider findById(Integer id, boolean lock) {
			return findById(id);
		}

		public Provider findOneByExample(Provider exampleInstance,
				String... excludeProperty) {
			// TODO Auto-generated method stub
			return null;
		}

		public Provider makePersistent(Provider entity) {
			// TODO Auto-generated method stub
			return null;
		}

		public Collection<Provider> makePersistent(Collection<Provider> entity) {
			// TODO Auto-generated method stub
			return null;
		}

		public void makeTransient(Provider entity) {
			// TODO Auto-generated method stub
		}

		public void makeTransient(Collection<Provider> entity) {
			// TODO Auto-generated method stub
		}
	}
}
