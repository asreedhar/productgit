package biz.firstlook.ping.persist;

import java.util.HashSet;
import java.util.List;

import biz.firstlook.ping.model.MakeCode;
import biz.firstlook.ping.model.MakeModelGrouping;
import biz.firstlook.ping.model.ModelCode;
import biz.firstlook.ping.model.Provider;

public class ModelCodeDaoTest extends BaseSpringTestCase {

	public MakeCodeDao makeCodeDao;
	public ModelCodeDao modelCodeDao;
	public ProviderDao providerDao;
	public MakeModelGroupingDao makeModelGroupingDao;
	
	public ModelCodeDaoTest(String name) {
		super(name);
	}

	public void testGet() throws Exception {
		List<ModelCode> modelCodes = modelCodeDao.findAll();
		assertNotNull( modelCodes );
	}
	
	public void testA() throws Exception {
		Provider testProvider = new Provider();
		testProvider.setName( "TEST" );
		testProvider = providerDao.makePersistent( testProvider );
	
		MakeCode kCode = new MakeCode();
		kCode.setName( "Toyota");
		kCode.setValue( "TOYO" );
		kCode.setProvider( testProvider );
		kCode = makeCodeDao.makePersistent( kCode );
		
		ModelCode dCode = new ModelCode();
		dCode.setName( "CAMRY" );
		dCode.setValue( "112" );
		dCode.setMake( kCode );
		dCode.setProvider( testProvider );

		List<MakeModelGrouping> mmgs = makeModelGroupingDao.findByMakeModel("Toyota", "Camry");
		dCode.setGroupings( new HashSet<MakeModelGrouping>( mmgs ));
		modelCodeDao.makePersistent( dCode );
		
	}
	
}
