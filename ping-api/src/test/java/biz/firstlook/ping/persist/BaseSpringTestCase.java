package biz.firstlook.ping.persist;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Properties;

import javax.naming.NamingException;

import junit.framework.TestCase;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;

//From a July 14, 2004 post on Memory Dump
public abstract class BaseSpringTestCase extends TestCase {
	protected static final String SESSION_FACTORY = 
		"ping-persist-sessionFactory";

	private ApplicationContext context;

	private SessionFactory sessionFactory;

	public BaseSpringTestCase(String name) {
		super(name);
	}

	protected void setUp() throws Exception {

		String[] contexts = 
			new String[] { "biz/firstlook/ping/persist/applicationContext-hibernate.xml" };
		
		createDataSource();
		context = new ClassPathXmlApplicationContext( contexts, context );
		initDependencies();

		sessionFactory = (SessionFactory) context.getBean(SESSION_FACTORY);
		Session session = SessionFactoryUtils.getSession(sessionFactory, true);
		session.setFlushMode(FlushMode.NEVER);
		TransactionSynchronizationManager.bindResource(sessionFactory,
				new SessionHolder(session));
	}

	// Tim: This is almost an auto-wire by name but not really.
	// Essentially every test case property name should correspond to a
	// bean defined in either the real context or the test context. I like
	// Spring
	private void initDependencies() {
		Field[] fields = this.getClass().getFields();
		for (int i = 0; i < fields.length; i++) {
			Field field = fields[i];
			int modifiers = field.getModifiers();
			if (!Modifier.isStatic(modifiers)) {
				String fieldName = field.getName();
				try {
					Object bean = context.getBean("ping-persist-" + fieldName);
					field.set(this, bean);
				} catch (Exception e) {
				}
			}
		}
	}

	protected void createDataSource() throws IOException, NamingException {
		SimpleNamingContextBuilder builder = SimpleNamingContextBuilder
				.getCurrentContextBuilder();

		if (builder == null) {

			Properties properties = new Properties();
			properties.load(BaseSpringTestCase.class.getResourceAsStream(
					"database.properties"));

			BasicDataSource ds = new BasicDataSource();

			ds.setDriverClassName(properties.getProperty("driverClassName"));
			ds.setUsername(properties.getProperty("username"));
			ds.setPassword(properties.getProperty("password"));
			ds.setUrl(properties.getProperty("url"));

			builder = new SimpleNamingContextBuilder();
			builder.bind("java:comp/env/jdbc/IMT", ds);
			builder.activate();
		}
	}

	@SuppressWarnings("deprecation")
	protected void tearDown() throws Exception {
		SessionHolder sessionHolder = (SessionHolder) TransactionSynchronizationManager
				.unbindResource(sessionFactory);
		SessionFactoryUtils.releaseSession(sessionHolder.getSession(),
				sessionFactory);
		super.tearDown();
	}
}
