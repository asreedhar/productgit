package biz.firstlook.ping.persist;

import java.util.List;

import biz.firstlook.ping.model.MakeCode;

public class MakeCodeDaoTest extends BaseSpringTestCase {

	public MakeCodeDao makeCodeDao;
	
	public MakeCodeDaoTest(String name) {
		super(name);
	}

	public void testGet() throws Exception {
		List<MakeCode> makeCodes = makeCodeDao.findAll();
		assertNotNull( makeCodes );
	}
	
}
