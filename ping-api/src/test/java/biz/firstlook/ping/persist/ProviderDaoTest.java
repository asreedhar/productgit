package biz.firstlook.ping.persist;

import java.util.List;

import biz.firstlook.ping.model.Provider;

public class ProviderDaoTest extends BaseSpringTestCase {

	public ProviderDao providerDao;
	
	public ProviderDaoTest(String name) {
		super(name);
	}

	public void testGet() throws Exception {
		List<Provider> providers = providerDao.findAll();
		assertNotNull( providers );
	}
	
}
